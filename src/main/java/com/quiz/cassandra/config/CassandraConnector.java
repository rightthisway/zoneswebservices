package com.quiz.cassandra.config;

import static java.lang.System.out;

import java.util.Date;

import org.springframework.beans.factory.InitializingBean;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.CodecRegistry;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TypeCodec;
import com.quiz.cassandra.dao.implementation.CassRtfApiTrackingDAO;
import com.zonesws.webservices.utils.ZonesProperty;
 
public class CassandraConnector implements InitializingBean {
	
	private static ZonesProperty properties;
	private static Cluster cluster;
	private static Session session;
	private static String node;//"34.234.156.58";
	private static int port;// = 9042;
	private static String keySpace;// = "rtfquizmasterks";
	private static String userName;
	private static String password;
	
	 public ZonesProperty getProperties() {
		return properties;
	 }

	 public void setProperties(ZonesProperty zonesProperties) {
		properties = zonesProperties;
	  }
	
	/*
		Production details:
		HOSTED on : 100.24.150.226:9042, 10.0.1.171:9042
		Userid: cassandra
		Password: cassandra
		Keyspace: rtfquizmasterks
		Cluster name: RTFQuizMasterLinuxProdCluster
	*/
	
	/*
	 	Sandbox Details:
		HOSTED on : 34.234.156.58:9042, 10.0.1.236:9042
		Userid: cassandra
		Password: cassandra
		Keyspace: rtfquizmasterks
		Cluster name: RTFQuizMasterLinuxCluster
	*/

	 
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Cassandra After Property Set invoked : "+ new Date());
		
		try {
			node = properties.getCassandraIp();
			port = properties.getCassandraPort();
			keySpace = properties.getCassandraKeySpace();
			userName = properties.getCassandraUserName();
			password = properties.getCassandraPassword();
			System.out.println("node: "+node+", port:"+port+" :keySpace: "+keySpace+", UserName:"+userName+", password:"+password+" : sess: "+session);
			connect();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public  static void connect() {
		
		CodecRegistry codecRegistry = new CodecRegistry();
		codecRegistry.register(new DateCodec(TypeCodec.date(), Date.class));
		
		System.out.println("node: "+node+", port:"+port);
		
		cluster = Cluster.builder().addContactPoint(node).withPort(port)//.withClusterName("RTFQuizMasterLinuxProdCluster")
				.withCredentials(userName, password).withCodecRegistry(codecRegistry).build();
		final Metadata metadata = cluster.getMetadata();
		out.printf("Connected to cluster: %s\n", metadata.getClusterName());
		for (final Host host : metadata.getAllHosts()) {
			out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect(keySpace);
	}
 
	public static Session getSession() {
		return session;
	}
	
	 /*public static Session getDataStaxSession() {
	        synchronized (DataStaxPlugin.class) {
	            try {
	                if (instance == null) {
	                    instance = new DataStaxPlugin();
	                    instance.connect();
	                }

	                return session;
	            } finally {
	            }
	        }
	    }*/

	 
	public void close() {
		cluster.close();
	}

	public static String getKeySpace() {
		return keySpace;
	}
	public static void main(String[] args) {
		connect();
		CassRtfApiTrackingDAO dao = new CassRtfApiTrackingDAO();
		dao.getAll();
		
	}
	
}