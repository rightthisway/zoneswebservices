package com.quiz.cassandra.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestGrandWinner;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CassFbCallBackTracking;
import com.quiz.cassandra.data.CassRtfApiTracking;
import com.quiz.cassandra.data.CassSuperFanWinner;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContestStat;
import com.quiz.cassandra.list.AnswerCountInfo;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CassJoinContestInfo;
import com.quiz.cassandra.list.CommonRespInfo;
import com.quiz.cassandra.list.ContApplyLifeInfo;
import com.quiz.cassandra.list.ContSummaryInfo;
import com.quiz.cassandra.list.ContWinnerRewardsInfo;
import com.quiz.cassandra.list.ContestValidateAnsInfo;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.list.HallOfFameDtls;
import com.quiz.cassandra.list.HallOfFameInfo;
import com.quiz.cassandra.list.LiveCustomerListInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.CassCustomerUpload;
import com.quiz.cassandra.utils.TrackingUtil;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestParticipants;
import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizFirebaseCallBackTracking;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.rtfquiz.webservices.enums.QuizSummaryType;
import com.rtfquiz.webservices.enums.RewardType;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.rtfquiz.webservices.utils.QuizContestUtil;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.ContestMigrationStats;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCassandra;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.RtfConfigContestClusterNodes;
import com.zonesws.webservices.data.WebServiceTrackingCassandra;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.sms.TwilioSMSServices;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({//"/GetOTP","/ValidateOTP","/QuizCustomerLogin","/QuizCustomerSignUp",
	"/GetDashboardInfo","/ContValidateAns","/JoinContest","/ExitContest","/ContApplyLife","/MigrateCustomerDataFromSQLToCassandra", 
	"/MigrateContestDataFromCassandraToSQL","/TruncateCassandraContestData","/CompContestWinnerRewards","/GetGrandWinners","/GetContestSummary",
	"/GetContCustCount","/GetLiveCustList","/GetQuestAnsCount","/GetHallOfFame","/MigrateApiTrackingToSql","/UpdateCustContStats","/RemoveCassContCacheData",
	"/ForceMigrationToSqlFromCass",
	"/GetDashboardInfoOne","/GetDashboardInfoTwo","/GetDashboardInfoThree","/AddSuperFanWinners","/SendSuperFanWinner"})
public class CassContestController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(CassContestController.class);
	
	 @Autowired
	 ServletContext context; 
	 
	 public void setServletContext(ServletContext servletContext) {
		 this.context = servletContext;
	 }
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private static SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static DecimalFormat percentageDF = new DecimalFormat("#.##");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	@RequestMapping(value = "/MigrateCustomerDataFromSQLToCassandra")
	public @ResponsePayload DashboardInfo migrateCustomerDataFromSQLToCassandra(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		try {
			Integer processedCut = CassCustomerUpload.startUpload();
			dashboardInfo.setSts(1);
			resMsg = "Customer Data's are migrated from sql server to cassandra. Total Transfered Customers :"+processedCut;
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating customer data's from sql to cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while migrating customer data's from sql to cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("MIGRATING CUSTOMER DATA FROM SQL TO CASSANDRA :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	}
	
	
	@RequestMapping(value = "/MigrateContestDataFromCassandraToSQL")
	public @ResponsePayload DashboardInfo migrateContestDataFromCassandraToSQL(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "", contestIdStr = request.getParameter("contestId");
		try {
			Integer contestId = Integer.parseInt(contestIdStr);
			migrateToSqlFromCassandra(contestId);
			
			dashboardInfo.setSts(1);
			resMsg = "Contest Data's are migrated from cassandra to sql server for contest id :"+contestIdStr;
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating contest data from cassandra to sql server";
			e.printStackTrace();
			error.setDesc("Error occured while migrating contest data from cassandra to sql server");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("MIGRATING CONTEST DATA FROM CASSANDRA TO SQL  :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	}
	
	@RequestMapping(value = "/TruncateCassandraContestData")
	public @ResponsePayload DashboardInfo refreshCassandraDatabaseForContest(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		try {
			String contestIdStr = request.getParameter("contestId");
			truncateCassandraContestData(contestIdStr);
			
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg("Cassandra Contest data deleted successfully.!");
		}catch(Exception e){
			e.printStackTrace();
			error.setDesc("Error occured while deleting contest data");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} 
		return dashboardInfo;
	}
	
	public void truncateCassandraContestData(String contestIdStr) {
		
		try {
			/*Integer contestId = Integer.parseInt(contestIdStr.trim());
			
			List<ContestMigrationStats> migrationStats = DAORegistry.getContestMigrationStatsDAO().getAllStatsByContestId(contestId);
			
			if(null == migrationStats) {
				return;
			}
			
			boolean isCompleted = false;
			for (ContestMigrationStats obj : migrationStats) {
				if(obj.getStatus().equals("COMPLETED")) {
					isCompleted = true;
					break;
				}
			}
			if(isCompleted) {
				CassandraDAORegistry.getCustContAnswersDAO().truncate();
				CassandraDAORegistry.getCassContestGrandWinnerDAO().truncate();
				CassandraDAORegistry.getCassContestWinnersDAO().truncate();
				//CassandraDAORegistry.getContestParticipantsDAO().truncate();
			}*/
			
			CassandraDAORegistry.getCustContAnswersDAO().truncate();
			CassandraDAORegistry.getCassContestGrandWinnerDAO().truncate();
			CassandraDAORegistry.getCassContestWinnersDAO().truncate();
		}catch(Exception e){
			return;
		}
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		
		/*MigrateApiTrackingUtil trackingUtil = new MigrateApiTrackingUtil();
		Thread t = new Thread(trackingUtil);
		t.start();*/
		
	}
	
	public void migrateToSqlFromCassandra(Integer contestId) {
		
		Date start = new Date();
		
		ContestMigrationStats migrationStatObj = new ContestMigrationStats();
		migrationStatObj.setContestId(contestId);
		migrationStatObj.setStartDate(start);
		migrationStatObj.setStatus("RUNNING");
		DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
		
		/*QuizContestParticipants participant = null;*/
		QuizCustomerContestAnswers customerAnswers = null;
		QuizContestWinners contestWinner = null;
		ContestGrandWinner contestGrandWinner = null;
		
		/*List<QuizContestParticipants> toBeSavedParticipants = new ArrayList<QuizContestParticipants>();*/
		List<QuizCustomerContestAnswers> toBeSavedCustAnsList = new ArrayList<QuizCustomerContestAnswers>();
		List<QuizContestWinners> toBeSavedContWinnerList = new ArrayList<QuizContestWinners>();
		List<ContestGrandWinner> toBeSavedGrandWinnerList = new ArrayList<ContestGrandWinner>();
		
		/*List<ContestParticipants> cassContPrticipantsList = CassandraDAORegistry.getContestParticipantsDAO().getAllParticipantsByContestId(contestId);*/
		List<CustContAnswers> cassCustAnsList = CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContId(contestId);
		List<CassContestWinners> cassContWinnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		List<CassContestGrandWinner> grandWinnerList = CassandraDAORegistry.getCassContestGrandWinnerDAO().getContestGrandWinnersByContestId(contestId);
		
		for (CassContestGrandWinner obj : grandWinnerList) {
			try {
				contestGrandWinner = new ContestGrandWinner();
				contestGrandWinner.setContestId(obj.getCoId());
				contestGrandWinner.setCreatedDate(new Date(obj.getCrDated()));
				contestGrandWinner.setCustomerId(obj.getCuId());
				contestGrandWinner.setExpiryDate(new Date(obj.getExDate()));
				contestGrandWinner.setStatus(WinnerStatus.ACTIVE);
				contestGrandWinner.setRewardTickets(obj.getrTix());
				toBeSavedGrandWinnerList.add(contestGrandWinner);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		/*for (ContestParticipants obj : cassContPrticipantsList) {
			try {
				participant = new QuizContestParticipants();
				participant.setContestId(contestId);
				participant.setCustomerId(obj.getCuId());
				participant.setPlatform(obj.getPfm());
				participant.setIpAddress(obj.getIpAdd());
				participant.setJoinDateTime(new Date(obj.getJnDate()));
				if(null != obj.getExDate()) {
					participant.setExitDateTime(new Date(obj.getExDate()));
				} 
				participant.setStatus(obj.getStatus());
				toBeSavedParticipants.add(participant);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}*/
		
		Map<Integer, CustContestStat> contestCustAnsMap = new HashMap<Integer, CustContestStat>(); 
		
		for (CustContAnswers obj : cassCustAnsList) {
			try {
				customerAnswers = new QuizCustomerContestAnswers();
				customerAnswers.setCustomerId(obj.getCuId());
				customerAnswers.setContestId(contestId);
				customerAnswers.setQuestionId(obj.getqId());
				customerAnswers.setQuestionSNo(obj.getqNo());
				if(null != obj.getAns()) {
					customerAnswers.setAnswer(obj.getAns());
				}
				customerAnswers.setIsCorrectAnswer(obj.getIsCrt());
				customerAnswers.setCreatedDateTime(new Date(obj.getCrDate()));
				if(null != obj.getUpDate()) {
					customerAnswers.setUpdatedDateTime(new Date(obj.getUpDate()));
				}
				customerAnswers.setIsLifeLineUsed(obj.getIsLife());
				customerAnswers.setAnswerRewards(null != obj.getaRwds()?obj.getaRwds():null);
				customerAnswers.setCumulativeRewards(obj.getCuRwds());
				customerAnswers.setCumulativeLifeLineUsed(obj.getCuLife());
				
				try {
					customerAnswers.setRetryCount(obj.getRetryCount());
					customerAnswers.setIsAutoCreated(obj.getIsAutoCreated());
					if(null != obj.getFbCallbackTime()) {
						Long fbCallBackTime = Long.valueOf(obj.getFbCallbackTime());
						customerAnswers.setFbCallbackTime(new Date(fbCallBackTime));
					}
					if(null != obj.getAnswerTime()) {
						Long answerTime = Long.valueOf(obj.getAnswerTime());
						customerAnswers.setAnswerTime(new Date(answerTime));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				toBeSavedCustAnsList.add(customerAnswers);
				
				Integer livesUsed = 0;
				
				if(null != obj.getIsLife() && obj.getIsLife()) {
					livesUsed = 1;
				}
				Double ansRewards = 0.00;
				if(null != obj.getaRwds()) {
					ansRewards = obj.getaRwds();
				}
				
				CustContestStat custContestStat = contestCustAnsMap.get(obj.getCuId());
				
				if(null == custContestStat) {
					custContestStat = new CustContestStat();
					custContestStat.setCumulativeAnsRewards(ansRewards);
					custContestStat.setLivesUsed(livesUsed);
				}else {
					custContestStat.setCumulativeAnsRewards(custContestStat.getCumulativeAnsRewards()+ansRewards);
					custContestStat.setLivesUsed(custContestStat.getLivesUsed() + livesUsed);
				}
				
				contestCustAnsMap.put(obj.getCuId(), custContestStat);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		Map<Integer, QuizContestWinners> contestWinnerMap = new HashMap<Integer, QuizContestWinners>(); 
		
		for (CassContestWinners obj : cassContWinnersList) {
			try {
				contestWinner = new QuizContestWinners();
				contestWinner.setCustomerId(obj.getCuId());
				contestWinner.setContestId(contestId);
				contestWinner.setCreatedDateTime(new Date(obj.getCrDated()));
				contestWinner.setRewardTickets(obj.getrTix());
				contestWinner.setRewardType(RewardType.POINTS);
				contestWinner.setRewardPoints(obj.getrPoints());
				contestWinner.setRewardRank(0);
				toBeSavedContWinnerList.add(contestWinner);
				contestWinnerMap.put(obj.getCuId(), contestWinner);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		/*if(null != toBeSavedParticipants && !toBeSavedParticipants.isEmpty()) {
			QuizDAORegistry.getQuizContestParticipantsDAO().saveAll(toBeSavedParticipants);
		} */
		
		if(null != toBeSavedCustAnsList && !toBeSavedCustAnsList.isEmpty()) {
			QuizDAORegistry.getQuizCustomerContestAnswersDAO().saveAll(toBeSavedCustAnsList);
		}
		
		if(null != toBeSavedContWinnerList && !toBeSavedContWinnerList.isEmpty()) {
			QuizDAORegistry.getQuizContestWinnersDAO().saveAll(toBeSavedContWinnerList);
		}
		
		if(null != toBeSavedGrandWinnerList && !toBeSavedGrandWinnerList.isEmpty()) {
			QuizDAORegistry.getContestGrandWinnerDAO().saveAll(toBeSavedGrandWinnerList);
		}
		
		
		Integer livesBeforeContest = null, livesUsed =null, livesAfterContest = null;
		Double contestQuesRewards=null,contestWinnerRewards=null;
		
		CustomerCassandra customerCassandra = null;
		CustomerLoyaltyTracking loyaltyRewardTracking = null;
		
		Date createDate = new Date();
		
		for(Integer custId : contestCustAnsMap.keySet()) {
			try {
				livesBeforeContest = 0;
				livesUsed =0;
				livesAfterContest = 0;
				
				contestQuesRewards=0.00;
				contestWinnerRewards=0.00;
				
				Customer customer = CustomerUtil.getCustomerById(custId);
				CustContestStat contestStatObj = contestCustAnsMap.get(custId);
				QuizContestWinners contestWinnerObj = contestWinnerMap.get(custId);
				if(null != contestWinnerObj) {
					contestWinnerRewards = contestWinnerObj.getRewardPoints();
				}
				contestQuesRewards = contestStatObj.getCumulativeAnsRewards();
				
				livesBeforeContest = customer.getQuizCustomerLives();
				livesUsed = contestStatObj.getLivesUsed();
				livesAfterContest = livesBeforeContest - livesUsed;
				/*
				System.out.println("CASSANDRA MIGRATION :- CustomerId: "+custId+", QueRewards : "+contestQuesRewards+", WinnerRewards: "+contestWinnerRewards
						+", LivesBeforeGame: "+livesBeforeContest+", LivesUsed: "+livesUsed+", LivesAfterGame: "+livesAfterContest);*/
				
				
				customerCassandra = new CustomerCassandra(custId, contestId, createDate);
				customerCassandra.setContestQusReward(contestQuesRewards);
				customerCassandra.setContestWinnerReward(contestWinnerRewards);
				customerCassandra.setCumulativeContestRewards(contestQuesRewards + contestWinnerRewards);
				customerCassandra.setLivesAfterContest(livesAfterContest);
				customerCassandra.setLivesBeforeContest(livesBeforeContest);
				customerCassandra.setLivesUsed(livesUsed);
				
				DAORegistry.getCustomerCassandraDAO().save(customerCassandra);
				
				customer.setQuizCustomerLives(livesAfterContest);
				
				DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),livesAfterContest);
				CustomerUtil.updatedCustomerUtil(customer);
				
				loyaltyRewardTracking = new CustomerLoyaltyTracking();
				loyaltyRewardTracking.setContestId(contestId);
				loyaltyRewardTracking.setCreatedBy("AUTO");
				loyaltyRewardTracking.setCreatedDate(createDate);
				loyaltyRewardTracking.setCustomerId(custId);
				loyaltyRewardTracking.setRewardPoints(customerCassandra.getCumulativeContestRewards());
				loyaltyRewardTracking.setRewardType("CONTEST");
				
				DAORegistry.getCustomerLoyaltyTrackingDAO().saveOrUpdate(loyaltyRewardTracking);
				
				Double contestRewards = customerCassandra.getCumulativeContestRewards();
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custId);
				
				if(null == customerLoyalty) {
					continue;	
				}
				
				//System.out.println("Active Rewards Before Game: "+customerLoyalty.getActivePointsAsDouble());
				
				customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble() + contestRewards);
				customerLoyalty.setContestRewardDollarsDouble(customerLoyalty.getContestRewardDollarsDouble() + contestRewards);
				customerLoyalty.setLastUpdate(new Date());
				customerLoyalty.setLatestEarnedPointsAsDouble(contestRewards);
				customerLoyalty.setLatestSpentPointsAsDouble(0.00);
				customerLoyalty.setTotalEarnedPointsAsDouble(customerLoyalty.getTotalEarnedPointsAsDouble()+contestRewards);
				
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), customer.getQuizCustomerLives(), customerLoyalty.getActivePointsAsDouble());
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				//System.out.println("Active Rewards After Game: "+customerLoyalty.getActivePointsAsDouble());
				
				//System.out.println("------------------------------------------------------------------------------------------------------");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		migrationStatObj.setEndDate(new Date());
		migrationStatObj.setStatus("COMPLETED");
		DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
		
		try {
			CassContestUtil.updateCustomerPostContestDetailsinSQL(contestId);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	public static String updatePostMigrationStatusonServlet(Integer contestId) throws Exception {
		
		Map<String, String> map = URLUtil.getParameterMap();
		map.put("coId", ""+contestId);
		
		/*String data = URLUtil.getObject(map, URLUtil.apiServerBaseUrl+"rtfcontest/PostMigrationStatsUpdate.json");
		//Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonCustomDateSerializer()).create();
		//JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
		//CommonRespInfo commonRespInfo = gson.fromJson(((JsonObject)jsonObject.get("commonRespInfo")), CommonRespInfo.class);
		
		ObjectMapper objectMapper = new ObjectMapper();
		CommonRespInfo commonRespInfo = objectMapper.readValue(data, CommonRespInfo.class);
		
		if(commonRespInfo.getSts() != 1){
			if(commonRespInfo.getErr() != null) {
				return commonRespInfo.getErr().getDesc();		
			}
		}*/
		String resMsg = "";
		CommonRespInfo commonRespInfo = null;
		List<RtfConfigContestClusterNodes> clusterNodeList = URLUtil.rtfContestClusterNodesList;
		/*if(URLUtil.isProductionEnvironment) {
			clusterNodeList.add(new RtfConfigContestClusterNodes(0,"http://10.0.0.34:8081/rtfcontest/",1));
			clusterNodeList.add(new RtfConfigContestClusterNodes(0,"http://10.0.0.34:8082/rtfcontest/",1));
		}*/
		for (RtfConfigContestClusterNodes rtfContestNodes : clusterNodeList) {
			String data = URLUtil.getObject(map, rtfContestNodes.getUrl()+"PostMigrationStatsUpdate.json");
			ObjectMapper objectMapper = new ObjectMapper();
			commonRespInfo = objectMapper.readValue(data, CommonRespInfo.class);
			if(commonRespInfo.getSts() != 1){
				if(commonRespInfo.getErr() != null) {
					String msg = rtfContestNodes.getUrl()+":"+commonRespInfo.getErr().getDesc(); 
					resMsg = resMsg + msg + ",";
					System.out.println("POST MIG ERROR : "+msg +" : "+data);
				}
			}
		}
		
		if(resMsg.equals("")) {
			resMsg = null;
		}
		
		return null;
	}
	
	@RequestMapping(value = "/MigrateApiTrackingToSql")
	public @ResponsePayload DashboardInfo migrateApiTrackingToSql(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		try {
			migrateTrackingDetailsToSQL();
			dashboardInfo.setSts(1);
			resMsg = "Contest Data's are migrated from cassandra to sql server for contest id :";
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating contest data from cassandra to sql server";
			e.printStackTrace();
			error.setDesc("Error occured while migrating contest data from cassandra to sql server");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("MIGRATING CONTEST DATA FROM CASSANDRA TO SQL  :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	}
	
	public void migrateTrackingDetailsToSQL() {
		List<CassRtfApiTracking> trackingList = CassandraDAORegistry.getCassRtfApiTrackingDAO().getAll();
		WebServiceTrackingCassandra obj = null; 
		for (CassRtfApiTracking cassObj : trackingList) {
			try {
				obj = new WebServiceTrackingCassandra();
				obj.setActionResult(cassObj.getActionResult());
				obj.setApiName(cassObj.getApiName());
				obj.setContestId(null != cassObj.getContestId()?cassObj.getContestId():null);
				obj.setCustId(null != cassObj.getCustId()?cassObj.getCustId():null);
				obj.setCustIPAddr(cassObj.getCustIPAddr());
				obj.setDescription(cassObj.getDescription());
				obj.setDeviceInfo(cassObj.getDeviceInfo());
				obj.setDeviceType(cassObj.getDeviceType());
				obj.setPlatForm(cassObj.getPlatForm());
				obj.setSessionId(cassObj.getSessionId());
				obj.setAppVersion(null != cassObj.getAppVerion()?cassObj.getAppVerion():"");
				obj.setResponseStatus(null != cassObj.getResponseStatus()?cassObj.getResponseStatus():null);
				obj.setQuestionNo(null != cassObj.getQuestionNo()?cassObj.getQuestionNo():"");
				obj.setRetryCount(cassObj.getRetryCount());
				obj.setAnsCount(cassObj.getAnsCount());
				try {
					if(null != cassObj.getStartDateStr() && !cassObj.getStartDateStr().isEmpty()) {
						obj.setStartDate(df.parse(cassObj.getStartDateStr()));
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					if(null != cassObj.getEndDateStr() && !cassObj.getEndDateStr().isEmpty()) {
						obj.setEndDate(df.parse(cassObj.getEndDateStr()));
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					if(null != cassObj.getFbCallbackTime() && !cassObj.getFbCallbackTime().isEmpty()) {
						Long fbCallBackTime = Long.valueOf(cassObj.getFbCallbackTime());
						obj.setFbCallbackTime(new Date(fbCallBackTime));
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					if(null != cassObj.getDeviceAPIStartTime() && !cassObj.getDeviceAPIStartTime().isEmpty()) {
						Long deviceAPIStartTime = Long.valueOf(cassObj.getDeviceAPIStartTime());
						obj.setDeviceAPIStartTime(new Date(deviceAPIStartTime));
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				DAORegistry.getWebServiceTrackingCassandraDAO().save(obj);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		//System.out.println("Started Truncate RTF API TRACKING Start: "+new Date());
		CassandraDAORegistry.getCassRtfApiTrackingDAO().truncate();
		//System.out.println("Started Truncate RTF API TRACKING END: "+new Date());
		
		migrateFbCallBackTrackingDetailsToSQL();
	}
	 
	public void migrateFbCallBackTrackingDetailsToSQL() {
		List<CassFbCallBackTracking> trackingList = CassandraDAORegistry.getCassFbCallBackTrackingDAO().getAllFbCallBackTrackings();
		QuizFirebaseCallBackTracking firebaseTracking = null; 
		for (CassFbCallBackTracking cassObj : trackingList) {
			try {
				firebaseTracking.setCustomerId(cassObj.getCuId());
				firebaseTracking.setContestId(cassObj.getCoId());
				firebaseTracking.setQuestionNo(cassObj.getqNo());
				firebaseTracking.setIpAddress(cassObj.getIpAddr());
				firebaseTracking.setDeviceModel(cassObj.getdModel());
				firebaseTracking.setOsVersion(cassObj.getOsVer());
				firebaseTracking.setPlatform(cassObj.getPlatform());
				firebaseTracking.setCallBackType(cassObj.getCbType());
				firebaseTracking.setMessage(cassObj.getMessage());
				
				try {
					if(null != cassObj.getCreateDate() && cassObj.getCreateDate() > 0) {
						firebaseTracking.setCreateDate(new Date(cassObj.getCreateDate()));
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				QuizDAORegistry.getQuizFirebaseCallBackTrackingDAO().save(firebaseTracking);
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		//System.out.println("Started Truncate RTF API TRACKING Start: "+new Date());
		CassandraDAORegistry.getCassFbCallBackTrackingDAO().truncate();
		//System.out.println("Started Truncate RTF API TRACKING END: "+new Date());
	}
	
	@RequestMapping(value = "/ForceMigrationToSqlFromCass")
	public @ResponsePayload DashboardInfo forceMigrationToSqlFromCass(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "", contestIdStr =request.getParameter("contestId");
		String isEndedSuccessfullyStr = request.getParameter("isEndedSuccessfully");
		try {
			CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
			Integer contestId = Integer.parseInt(contestIdStr.trim());
			if(null == isEndedSuccessfullyStr || isEndedSuccessfullyStr.isEmpty()) {
				isEndedSuccessfullyStr = "false";
			}
			Boolean contestEndSuccessfuly = Boolean.valueOf(isEndedSuccessfullyStr);
			
			ContestMigrationStats migrationStatObj = null;
			
			if(contestEndSuccessfuly) {
				migrationStatObj = new ContestMigrationStats();
				migrationStatObj.setContestId(contestId);
				migrationStatObj.setStartDate(start);
				migrationStatObj.setStatus("RUNNING");
				DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
			}
			
			QuizContestParticipants participant = null;
			
			List<QuizContestParticipants> toBeSavedParticipants = new ArrayList<QuizContestParticipants>();
			List<ContestParticipants> cassContPrticipantsList = CassandraDAORegistry.getContestParticipantsDAO().getAllParticipantsByContestId(contestId);
			
			Map<Integer, Boolean> participantIdMap =  new HashMap<Integer, Boolean>();
			for (ContestParticipants obj : cassContPrticipantsList) {
				try {
					participant = new QuizContestParticipants();
					participant.setContestId(contestId);
					participant.setCustomerId(obj.getCuId());
					participant.setPlatform(obj.getPfm());
					participant.setIpAddress(obj.getIpAdd());
					participant.setJoinDateTime(new Date(obj.getJnDate()));
					if(null != obj.getExDate() && obj.getExDate() > 0) {
						participant.setExitDateTime(new Date(obj.getExDate()));
					} 
					participant.setStatus(obj.getStatus());
					toBeSavedParticipants.add(participant);
					
					participantIdMap.put(obj.getCuId(), true);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(null != toBeSavedParticipants && !toBeSavedParticipants.isEmpty()) {
				//System.out.println("TobeSaved Participants: "+toBeSavedParticipants.size());
				QuizDAORegistry.getQuizContestParticipantsDAO().saveAll(toBeSavedParticipants);
				//System.out.println("Started Truncate Contest Participants Start: "+new Date());
				CassandraDAORegistry.getContestParticipantsDAO().truncate();
				//System.out.println("Started Truncate Contest Participants End: "+new Date());
				if(null != participantIdMap && !participantIdMap.isEmpty() && participantIdMap.size() > 0) {
					for (Integer custId : participantIdMap.keySet()) {
						try {
							Customer customer = CustomerUtil.getCustomerById(custId);
							if(null != customer) {
								Integer lives = customer.getQuizCustomerLives() + 1;
								try {
									DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),lives);
									CustomerUtil.updatedCustomerUtil(customer);
								}catch(Exception e) {
									e.printStackTrace();
								}
								CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(custId, lives);
							}
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			} 
			
			if(contestEndSuccessfuly) {
				migrationStatObj.setEndDate(new Date());
				migrationStatObj.setStatus("COMPLETED");
				DAORegistry.getContestMigrationStatsDAO().update(migrationStatObj);
			}
			migrateTrackingDetailsToSQL();
			dashboardInfo.setSts(1);
			resMsg = "Contest Data's are migrated from cassandra to sql server:";
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating contest data from cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while migrating contest data from cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("FORCE MIGRATING CONTEST DATA FROM CASSANDRA TO SQL  :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		
		return dashboardInfo;
	
	}
	
	public static void main(String[] args) {
		Integer var = 500;
		
		System.out.println(var % 500);
	}
	
	
	@RequestMapping(value = "/GetDashboardInfo", method=RequestMethod.POST)
	public @ResponsePayload DashboardInfo getDashboardInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		String appVersion = request.getParameter("aVn");
		Integer customerId=null;
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				dashboardInfo.setErr(authError);
				dashboardInfo.setSts(0);
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
				return dashboardInfo;
			}*/
			
			
			//String productTypeStr = request.getParameter("productType");
			/*if(TextUtil.isEmptyOrNull(platForm)){
				resMsg = "Application Platform is mandatory";
				error.setDesc("Application Platform is mandatory");
				dashboardInfo.setErr(error);
				dashboardInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
				return dashboardInfo;
			}
			if(TextUtil.isEmptyOrNull(deviceType)){
				resMsg = "Device Type is mandatory";
				error.setDesc("Device Type is mandatory");
				dashboardInfo.setErr(error);
				dashboardInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
				return dashboardInfo;
			}*/
			/*ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					resMsg = "Please send valid application platform";
					error.setDesc("Please send valid application platform");
					dashboardInfo.setErr(error);
					dashboardInfo.setSts(0);
					TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.GETDASHBOARDINFO,resMsg);
					return dashboardInfo;
				}
			}*/
			CassCustomer customer = null;
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				 
				try {
					customerId = Integer.parseInt(customerIdStr.trim());
					customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
					if(customer == null) {
						resMsg = "Customer Id is Invalid:"+customerIdStr;
						error.setDesc("Customer Id is Invalid");
						dashboardInfo.setErr(error);
						dashboardInfo.setSts(0);
						return dashboardInfo;
					}
				} catch(Exception e) {
					resMsg = "Customer Id Not Exist:"+customerIdStr;
					error.setDesc("Customer Id Not Exist");
					dashboardInfo.setErr(error);
					dashboardInfo.setSts(0);
					return dashboardInfo;
				}
			}
			
			dashboardInfo.setSts(1);
			dashboardInfo.setCust(customer);
			dashboardInfo.sethRwds(CassContestUtil.HIDE_DASHBOARD_REWARDS);
			//dashboardInfo.setMessage("");
			
			resMsg = "Success:"+customerIdStr;
			//TrackingUtils.contestDashboardAPITracking(request, ,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Dashboard Information";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Dashboard Information");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, 
					customerId, start, new Date(), request.getHeader("X-Forwarded-For"),appVersion);
			log.info("GETDASH INFO: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		
		return dashboardInfo;
	
	}
	
	@RequestMapping(value = "/ContValidateAns", method=RequestMethod.POST)
	public @ResponsePayload ContestValidateAnsInfo validateQuizAnswers(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ContestValidateAnsInfo quizAnswerInfo =new ContestValidateAnsInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String questionNoStr = request.getParameter("qNo");
		String questionIdStr = request.getParameter("qId");
		String answerOption = request.getParameter("ans");
		Integer customerId = null,contestId = null;
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				quizAnswerInfo.setErr(authError);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg);
				return quizAnswerInfo;
			}*/
			  
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				resMsg = "CustomerId is mandatory";
				error.setDesc("CustomerId is mandatory");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg);
				return quizAnswerInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				resMsg = "Contest Id is mandatory";
				error.setDesc("Contest Id is mandatory");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg);
				return quizAnswerInfo;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				resMsg = "Question Id is mandatory";
				error.setDesc("Question Id is mandatory");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg);
				return quizAnswerInfo;
			}*/
			/*String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String questionNoStr = request.getParameter("questionNo");
			String questionIdStr = request.getParameter("questionId");
			String isContestant = request.getParameter("contestant");
			String answerOption = request.getParameter("answer");*/
			
			/*if(isContestant == null || isContestant.isEmpty()){
				resMsg = "Contestant is mandatory";
				error.setDesc("Contestant is mandatory");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				 
				return quizAnswerInfo;
			}
			if(isContestant.equals("Y")) {
				if(answerOption == null || answerOption.isEmpty()){
					resMsg = "Answer is mandatory";
					error.setDesc("Answer is mandatory");
					quizAnswerInfo.setErr(error);
					quizAnswerInfo.setSts(0);
					TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}
			}*/
			/*if(TextUtil.isEmptyOrNull(questionNoStr)){
				resMsg = "Question NO is mandatory";
				error.setDesc("Question NO is mandatory");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,resMsg);
				return quizAnswerInfo;
			}*/
			
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Customer Id:"+customerIdStr;
				error.setDesc("Invalid Customer Id");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Contest Id:"+contestIdStr;
				error.setDesc("Invalid Contest Id");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			Integer questionId = null;
			try{
				questionId = Integer.parseInt(questionIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question Id:"+questionIdStr;
				error.setDesc("Invalid Question Id");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			Integer questionNo = null;
			try{
				questionNo = Integer.parseInt(questionNoStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question No:"+questionNoStr;
				error.setDesc("Invalid Question No");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			QuizContest contest = CassContestUtil.getCurrentContestByContestId(contestId);
			if(contest == null){
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if(customer == null){
				resMsg = "Customer Id is Invalid:"+customerIdStr;
				error.setDesc("Custoemr Id is Invalid");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			
			/*QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				error.setDesc("Question Id is Invalid");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTVALIDATEANS,"Question Id is Invalid");
				return quizAnswerInfo;
			}*/
			//Fix	
			//////
			QuizContestQuestions quizQuestion = QuizContestUtil.getContestCurrentQuestions(contest.getId());
			if(quizQuestion == null || !quizQuestion.getId().equals(questionId)) {
				resMsg = "Question Id is Invalid:"+questionIdStr;
				error.setDesc("Question Id is Invalid");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			if(!quizQuestion.getQuestionSNo().equals(questionNo)) {//|| !quizQuestion.getContestId().equals(contest.getId()
				resMsg = "Question No or Id is Invalid:"+questionNoStr;
				error.setDesc("Question No or Id is Invalid");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			///////
			if(quizQuestion.getIsAnswerCountComputed()) {
				resMsg = "Answer Time Limit was Reached:"+questionNoStr;
				error.setDesc("Answer Time Limit was Reached.");
				quizAnswerInfo.setErr(error);
				quizAnswerInfo.setSts(0);
				return quizAnswerInfo;
			}
			
			CustContAnswers lastAnswer = null;
			//comment for production performance			
			if(quizQuestion.getQuestionSNo() > 1) {
				lastAnswer = CassContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				if(lastAnswer == null || questionNo != (lastAnswer.getqNo()+1)) {
					resMsg = "You didn't answer last question:"+questionNoStr;
					error.setDesc("You didn't answer last question.");
					quizAnswerInfo.setErr(error);
					quizAnswerInfo.setSts(0);
					return quizAnswerInfo;
				}else if(null != lastAnswer) {
					
					if((null != lastAnswer.getIsCrt() && lastAnswer.getIsCrt()) || (null != lastAnswer.getIsLife()
							&& lastAnswer.getIsLife())) {
						
					}else {
						resMsg = "You didn't answer last question:"+questionNoStr;
						error.setDesc("You didn't answer last question.");
						quizAnswerInfo.setErr(error);
						quizAnswerInfo.setSts(0);
						return quizAnswerInfo;
					}
					
				}
			} else {
				lastAnswer = CassContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				if(lastAnswer != null) {
					resMsg = "You have already answered for this question:"+questionNoStr;
					error.setDesc("You have already answered for this question.");
					quizAnswerInfo.setErr(error);
					quizAnswerInfo.setSts(0);
					return quizAnswerInfo;
				}
			}
			Double cumulativeRewards = 0.0;
			Integer cumulativeLifeLineUsed = 0;
			if(lastAnswer != null) {
				if(lastAnswer.getCuRwds() != null) {
					cumulativeRewards = lastAnswer.getCuRwds();
				}
				if(lastAnswer.getCuLife() != null) {
					cumulativeLifeLineUsed = lastAnswer.getCuLife();
				}
			}
			
			if(answerOption.equalsIgnoreCase(quizQuestion.getAnswer())) {
				quizAnswerInfo.setIsCrt(Boolean.TRUE);
				
				if(contest.getNoOfQuestions().equals(questionNo)) {
					CassContestUtil.updateContestWinners(contest.getId(), customerId);
				}
			}
			/*QuizCustomerContestAnswers custContAns = new QuizCustomerContestAnswers();
			custContAns.setCustomerId(customerId);
			custContAns.setContestId(contestId);
			custContAns.setQuestionId(questionId);
			custContAns.setQuestionSNo(questionNo);
			custContAns.setAnswer(answerOption);
			custContAns.setIsCorrectAnswer(quizAnswerInfo.getIsCrt());
			custContAns.setCreatedDateTime(new Date());
			custContAns.setIsLifeLineUsed(false);*/
			
			CustContAnswers custContAns = new CustContAnswers();
			custContAns.setCuId(customerId);
			custContAns.setCoId(contestId);
			custContAns.setqId(questionId);
			custContAns.setqNo(questionNo);
			custContAns.setAns(answerOption);
			custContAns.setIsCrt(quizAnswerInfo.getIsCrt());
			custContAns.setCrDate(new Date().getTime());
			custContAns.setIsLife(false);
		
			if(quizAnswerInfo.getIsCrt()) {
				custContAns.setaRwds(quizQuestion.getQuestionRewards());
				if(quizQuestion.getQuestionRewards() != null) {
					cumulativeRewards = cumulativeRewards + quizQuestion.getQuestionRewards();	
				}
			}
			custContAns.setCuRwds(cumulativeRewards);
			custContAns.setCuLife(cumulativeLifeLineUsed);
			
			

			CassandraDAORegistry.getCustContAnswersDAO().save(custContAns);
			
			CassContestUtil.updateCustomerAnswers(custContAns);
			
			log.info("Quiz Question validation : QNO:"+questionNoStr+": "+customerIdStr+":"+contestIdStr+":"+answerOption+":"+questionNoStr+":"+custContAns.getIsCrt()+":"+new Date());
			
			//quizAnswerInfo.setQuizContestQuestion(quizQuestion);
			quizAnswerInfo.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success:"+questionNoStr+":"+questionIdStr+":"+answerOption;
			
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Error occured while Fetching Validate Answer Info.";
			error.setDesc("Error occured while Fetching Validate Answer Info.");
			quizAnswerInfo.setErr(error);
			quizAnswerInfo.setSts(0);
			return quizAnswerInfo;
		} finally {
			TrackingUtil.contestValidateAnswerAPITracking(request.getHeader("x-platform"), null, request.getHeader("deviceId"), WebServiceActionType.CONTVALIDATEANS, resMsg, 
					questionNoStr, questionIdStr, answerOption, contestId, customerId, start, new Date(), request.getHeader("X-Forwarded-For"));
			log.info("CASS VALID ANSWER IOS: QNO:"+questionNoStr+" : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :ans: "+answerOption+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+quizAnswerInfo);
		}
		
		//log.info("QUIZ VALID ANS : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" :ans: "+request.getParameter("answer")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizAnswerInfo;
	
	}
	
	@RequestMapping(value = "/JoinContest", method=RequestMethod.POST)
	public @ResponsePayload CassJoinContestInfo quizJoinContest(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CassJoinContestInfo joinContInfo =new CassJoinContestInfo();
		CassError error = new CassError();
		Date start = new Date();
		String customerIdStr = request.getParameter("cuId");
		//String contestIdStr = request.getParameter("contestId");
		String platForm = request.getParameter("pfm");
		//String productTypeStr = request.getParameter("productType");
		String loginIp = request.getParameter("lIp");
		Integer customerId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				joinContInfo.setErr(authError);
				joinContInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.JOINCONTEST,authError.getDescription());
				return joinContInfo;
			}*/
			
			
			 
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					resMsg = "Please send valid application platform:"+platForm;
					error.setDesc("Please send valid application platform");
					joinContInfo.setErr(error);
					joinContInfo.setSts(0);
					return joinContInfo;
				}
			}
			
			String contestType="MOBILE";
			if(!applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType="WEB";
			}
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Customer Id:"+customerIdStr;
				error.setDesc("Invalid Customer Id");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				return joinContInfo;
			}
			
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			if(customer == null) {
				resMsg = "Customer Id is Invalid:"+customerIdStr;
				error.setDesc("Customer Id is Invalid");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				return joinContInfo;
			}
			
			/*if(customer.getPhone() == null || !customer.getIsOtp()) {
				error.setDesc("Verify your Phone with OTP to Join Contest.");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				TrackingUtil.contestAPITracking(platForm, null, request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, 
						"Verify your Phone with OTP to Join Contest.", null, customerId, start, new Date(), request.getHeader("X-Forwarded-For"));
				return joinContInfo;
			}*/
			QuizContest contest =CassContestUtil.getCurrentStartedContest(contestType);
			if(contest == null) {
				resMsg = "contest is Not Yet Started:"+contestType;
				error.setDesc("contest is Not Yet Started.");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				return joinContInfo;
			}
			//joinContInfo.setContestId(contest.getId());

			if(contest != null && contest.getStatus().equals("STARTED")) {
				joinContInfo = CassContestUtil.updateJoinContestCustomersCount(joinContInfo,contest.getId(), customer);
				
				//Tamil : commented due to performance
				//CassContestUtil.updateExistingContestParticipantsStatusByJoinContest(customerId);
				//CassandraDAORegistry.getContestParticipantsDAO().updateExistingContestParticipantsByJoinContest(customer.getId(), contest.getId());
					
					ContestParticipants participant = new ContestParticipants();
					participant.setCoId(contest.getId());
					participant.setCuId(customerId);
					participant.setPfm(applicationPlatForm.toString());
					participant.setIpAdd(loginIp);
					participant.setJnDate(new Date().getTime());
					participant.setStatus("ACTIVE");
					CassandraDAORegistry.getContestParticipantsDAO().save(participant);
				
					CassContestUtil.updateContestParticipantsMapByCustId(participant);
				
				//if(contest.getStatus().equals("STARTED")) {
//					Double contesTAnserRewards = QuizContestUtil.getCustomerContestAnswerRewards(contest.getId(), customer.getId());
//					joinContInfo.setContestAnswerRewards(contesTAnserRewards);
					/*if(contesTAnserRewards != null) {
						joinContInfo.setContestAnswerRewards(TicketUtil.getRoundedValueString(contesTAnserRewards));
					}*/
				//}
					log.info("JOIN : "+customerIdStr+":"+joinContInfo.getLqNo()+":"+joinContInfo.getCaRwds()+":"+new Date());
			}
			
			joinContInfo.setSts(1);
			resMsg = "Success:"+customerIdStr+":"+contest.getId()+":"+contestType;
			
		}catch(Exception e){
			resMsg = "Error occured while Fetching Join contest Details.";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Join contest Details.");
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			//TrackingUtils.contestAPITracking(request, WebServiceActionType.JOINCONTEST,"Error occured while Fetching Join contest Details.");
			return joinContInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.JOINCONTEST, resMsg, null, 
					customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS EXIT : "+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		log.info("CASS JOIN : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+joinContInfo);
		
		return joinContInfo;
	}
	
	@RequestMapping(value = "/ExitContest", method=RequestMethod.POST)
	public @ResponsePayload CassJoinContestInfo quizExitContest(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CassJoinContestInfo joinContInfo =new CassJoinContestInfo();
		CassError error = new CassError();
		Date start = new Date();
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String platForm = request.getParameter("pfm");
		Integer customerId = null,contestId = null;
		String resMsg = "";
		String deviceType = request.getParameter("dyType");
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				joinContInfo.setErr(authError);
				joinContInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.EXITCONTEST,authError.getDescription());
				return joinContInfo;
			}*/
			
			
			//String productTypeStr = request.getParameter("productType");
			/*if(TextUtil.isEmptyOrNull(platForm)){
				error.setDesc("Application Platform is mandatory");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.EXITCONTEST,"Application Platform is mandatory");
				return joinContInfo;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDesc("Please send valid application platform");
					joinContInfo.setErr(error);
					joinContInfo.setSts(0);
					TrackingUtils.contestAPITracking(request, WebServiceActionType.EXITCONTEST,"Please send valid application platform");
					return joinContInfo;
				}
			}*/
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.EXITCONTEST,"Customer Id is mandatory");
				return joinContInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDesc("Contest Id is mandatory");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.EXITCONTEST,"Contest Id is mandatory");
				return joinContInfo;
			}*/
			
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Customer Id:"+customerIdStr;
				error.setDesc("Invalid Customer Id");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				return joinContInfo;
			}
			
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Contest Id:"+contestIdStr;
				error.setDesc("Invalid Contest Id");
				joinContInfo.setErr(error);
				joinContInfo.setSts(0);
				return joinContInfo;
			}

			joinContInfo = CassContestUtil.updateQuizExitCustomersCount(joinContInfo,contestId, customerId);
			joinContInfo.setSts(1);
			try {
				CassContestUtil.updateExistingContestParticipantsStatusByExitContest(customerId);
				//CassandraDAORegistry.getContestParticipantsDAO().updateContestParticipantsExistContest(customerId, contestId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			resMsg = "Success:"+contestIdStr+":"+customerIdStr;
		}catch(Exception e){
			resMsg = "Error occured while Fetching Exit contest Details.";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Exit contest Details.");
			joinContInfo.setErr(error);
			joinContInfo.setSts(0);
			return joinContInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.EXITCONTEST, resMsg, contestId, 
					customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS EXIT : "+customerIdStr+" : coId: "+contestIdStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		
		return joinContInfo;
	}
	
	@RequestMapping(value = "/ContApplyLife", method=RequestMethod.POST)
	public @ResponsePayload ContApplyLifeInfo contApplyLife(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ContApplyLifeInfo applyLifeInfo =new ContApplyLifeInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String questionNoStr = request.getParameter("qNo");
		String questionIdStr = request.getParameter("qId");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer contestId = null;
		Integer customerId = null;
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				applyLifeInfo.setErr(authError);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			}*/
			
			//String isContestant = request.getParameter("contestant");
			//String answerOption = request.getParameter("answer");
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				resMsg = "CustomerId is mandatory";
				error.setDesc("CustomerId is mandatory");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				resMsg = "Contest Id is mandatory";
				error.setDesc("Contest Id is mandatory");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				resMsg = "Question Id is mandatory";
				error.setDesc("Question Id is mandatory");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			}
			if(TextUtil.isEmptyOrNull(questionNoStr)){
				resMsg = "Question NO is mandatory";
				error.setDesc("Question NO is mandatory");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			}*/
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null){
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				return applyLifeInfo;
			}
			
			QuizContestQuestions quizQuestion = CassContestUtil.getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				resMsg = "Question Id is Invalid:"+questionIdStr;
				error.setDesc("Question Id is Invalid");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				return applyLifeInfo;
			}
			contestId = contest.getId();
			
			Integer questionNo = Integer.parseInt(questionNoStr);
			if(!quizQuestion.getQuestionSNo().equals(questionNo) || !quizQuestion.getContestId().equals(contest.getId())) {
				resMsg = "Question No or Id is Invalid:"+questionNoStr;
				error.setDesc("Question No or Id is Invalid");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				return applyLifeInfo;
			}
			if(quizQuestion.getQuestionSNo() >= contest.getNoOfQuestions()) {
				resMsg = "LifeLine can't be used for Last question:"+questionNoStr;
				error.setDesc("LifeLine can't be used for Last question.");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				return applyLifeInfo;
			}
			
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				resMsg = "Customer Id is Invalid:"+customerIdStr;
				error.setDesc("Customer Id is Invalid.");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				return applyLifeInfo;
			}
			customerId = customer.getId();
//commented due to getting data from cache itself and uncomment when not maintaining data in cache			
/*Db Table validation blog begins */			
			/*CustContAnswers customerAnswers = CassandraDAORegistry.getCustContAnswersDAO().
					getCustContAnswersByCustIdAndQuestId(Integer.parseInt(customerIdStr), Integer.parseInt(questionIdStr));
			if(customerAnswers == null) {
				//error.setDesc("Customer not yet answered this question. ");
				//applyLifeInfo.setErr(error);
				//applyLifeInfo.setSts(0);
				//TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,"Customer not yet answered this question.");
				//return applyLifeInfo;
				
				 customerAnswers = new CustContAnswers();
				 customerAnswers.setId(UUID.randomUUID());
				 customerAnswers.setCuId(Integer.parseInt(customerIdStr));
				 customerAnswers.setCoId(Integer.parseInt(contestIdStr));
				 customerAnswers.setqId(Integer.parseInt(questionIdStr));
				 customerAnswers.setqNo(Integer.parseInt(questionNoStr));
				 //customerAnswers.setAnswer(answerOption);
				 customerAnswers.setIsCrt(false);
				 customerAnswers.setCrDate(new Date());
				 customerAnswers.setIsLife(false);
				 
			}else if(customerAnswers.getIsCrt()) {
				resMsg = "You already selected correct answer for this question.";
				error.setDesc("You already selected correct answer for this question ");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			} 
			
			CustContAnswers latestCustAns = CassContestUtil.getCustomerAnswers(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
			if (latestCustAns != null && latestCustAns.getCuLife() != null && latestCustAns.getCuLife() > 0) {
				
				resMsg = "You already Used lifeline for this conteset.";
				error.setDesc("You already Used lifeline for this conteset.");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			}*/
/*SQL validation blog ends */

//commented due to getting data from cache itself and uncomment when not maintaining data in cache
/*Cache data validation blog begins */
			CustContAnswers customerAnswers = CassContestUtil.getCustomerAnswers(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
			if (customerAnswers != null) { 
				if(customerAnswers.getCuLife() != null && customerAnswers.getCuLife() > 0) {
				
					resMsg = "You already Used lifeline for this conteset:"+questionNoStr;
					error.setDesc("You already Used lifeline for this conteset.");
					applyLifeInfo.setErr(error);
					applyLifeInfo.setSts(0);
					return applyLifeInfo;
				} else if (customerAnswers.getqNo().equals(questionNo) && customerAnswers.getIsCrt()) {
					resMsg = "You already selected correct answer for this question:"+questionNoStr;
					error.setDesc("You already selected correct answer for this question ");
					applyLifeInfo.setErr(error);
					applyLifeInfo.setSts(0);
					return applyLifeInfo;
				} else if (customerAnswers.getqNo()<(questionNo-1)) {
					resMsg = "You didn't answered last question:"+questionNoStr;
					error.setDesc("You didn't answered last question.");
					applyLifeInfo.setErr(error);
					applyLifeInfo.setSts(0);
					return applyLifeInfo;
				}
			} else if (questionNo != 1) {
				resMsg = "You didn't answered last question:"+questionNoStr;
				error.setDesc("You didn't answered last question.");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				return applyLifeInfo;
			}
			
			if(customerAnswers == null || customerAnswers.getqNo()< questionNo) {
				 customerAnswers = new CustContAnswers();
				 customerAnswers.setCuId(Integer.parseInt(customerIdStr));
				 customerAnswers.setCoId(Integer.parseInt(contestIdStr));
				 customerAnswers.setqId(Integer.parseInt(questionIdStr));
				 customerAnswers.setqNo(Integer.parseInt(questionNoStr));
				 //customerAnswers.setAnswer(answerOption);
				 customerAnswers.setIsCrt(false);
				 customerAnswers.setCrDate(new Date().getTime());
				 customerAnswers.setIsLife(false);
				 customerAnswers.setCuLife(0);
				 customerAnswers.setCuRwds(0.0);
			}
/*Cache data validation blog ends */
			
			/*QuizCustomerContestAnswers lifeLineUsedcustomers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().
					getLifeLineUsedContestAnswerByCustomerIdandContestId(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
					
			if(lifeLineUsedcustomers != null) {
				resMsg = "You already Used lifeline for this conteset.";
				error.setDesc("You already Used lifeline for this conteset.");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.CONTAPPLYLIFE,resMsg);
				return applyLifeInfo;
			}*/
			
			if(customer.getqLive() == null || customer.getqLive() <= 0) {
				resMsg = "You do not have lifeline to use:"+customer.getqLive();
				error.setDesc("You do not have lifeline to use.");
				applyLifeInfo.setErr(error);
				applyLifeInfo.setSts(0);
				return applyLifeInfo;
			}
			
			customer.setqLive(customer.getqLive()-1);
			//CustomerUtil.updatedCustomerUtil(customer);
			//CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(customer.getId(), customer.getqLive(),customer);
			
			Integer cumulativeLifeLineUsed = 0;
			if(customerAnswers.getCuLife() != null) {
				cumulativeLifeLineUsed = customerAnswers.getCuLife();
			}
			cumulativeLifeLineUsed = cumulativeLifeLineUsed + 1;
			
			customerAnswers.setIsLife(true);
			customerAnswers.setUpDate(new Date().getTime());
			customerAnswers.setCuLife(cumulativeLifeLineUsed);
			
			CassandraDAORegistry.getCustContAnswersDAO().save(customerAnswers);
			
			//QuizContestUtil.updateCustomerLifeLineUsage(customerAnswers);
			CassContestUtil.updateCustomerAnswerMap(customerAnswers);
			
			applyLifeInfo.setIsLifeUsed(Boolean.TRUE);
			applyLifeInfo.setSts(1);
			applyLifeInfo.setMsg("LifeLine Applied Successfully.");
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success:"+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr;
		}catch(Exception e){
			resMsg = "Error occured while Applying Customer LifeLine.";
			e.printStackTrace();
			error.setDesc("Error occured while Applying Customer LifeLine.");
			applyLifeInfo.setErr(error);
			applyLifeInfo.setSts(0);
			return applyLifeInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.CONTAPPLYLIFE, resMsg, contestId,
					customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS APLY LIFE : "+request.getParameter("cuId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg+":"+applyLifeInfo);
		}
		return applyLifeInfo;
	
	}
	@RequestMapping(value = "/CompContestWinnerRewards", method=RequestMethod.POST)
	public @ResponsePayload ContWinnerRewardsInfo compContestWinnerRewards(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ContWinnerRewardsInfo contestWinnerRewards =new ContWinnerRewardsInfo();
		CassError error = new CassError();
		Date start = new Date();
		//String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String resMsg = "";
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer contestId = null;
		try {
			log.info("CASS WINNER REWARDs : "+": "+contestIdStr+" INSIDE");
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				contestWinnerRewards.setErr(authError);
				contestWinnerRewards.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.COMPCONTESTWINNERREWARDS,authError.getDescription());
				return contestWinnerRewards;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				contestWinnerRewards.setErr(error);
				contestWinnerRewards.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.COMPCONTESTWINNERREWARDS,"Customer Id is mandatory");
				return contestWinnerRewards;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDesc("Contest Id is mandatory");
				contestWinnerRewards.setErr(error);
				contestWinnerRewards.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.COMPCONTESTWINNERREWARDS,"Contest Id is mandatory");
				return contestWinnerRewards;
			}*/
			//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				contestWinnerRewards.setErr(error);
				contestWinnerRewards.setSts(0);
				return contestWinnerRewards;
			}
			contestId = quizContest.getId();
			/*Double contestAPITracking = 0.0;
			if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
				rewardsPerCustomer = QuizContestUtil.updateContestWinnersRewadPoints(Integer.parseInt(contestIdStr), Integer.parseInt(customerIdStr));	
			} else {
				QuizContestUtil.getContestWinnerRewardPoints(quizContest.getId());
			}*/
			Double rewardsPerCustomer = CassContestUtil.getContestWinnerRewardPoints(quizContest.getId());
			if(rewardsPerCustomer == null || rewardsPerCustomer <= 0.0) {
				rewardsPerCustomer = CassContestUtil.updateContestWinnersRewadPoints(quizContest.getId());
				
				QuizContestUtil.refreshContestSummaryData(quizContest.getId());
			}
			
			//quizContest.setProcessStatus(ContestProcessStatus.WINNERCOMPUTED);
			//QuizContestUtil.updateQuizContest(quizContest);
			
			//ansCountInfo.setQuizContestQuestion(quizQuestion);
			contestWinnerRewards.setSts(1);
			contestWinnerRewards.setMsg("Customer Rewards Computed successfully.");
			contestWinnerRewards.setWinnerRwds(rewardsPerCustomer);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			log.info("CASS WINNER REWARDs : "+contestIdStr+" SUCCESS");
			resMsg = "Success:"+rewardsPerCustomer;
		}catch(Exception e){
			resMsg = "Error occured while Fetching Customer Reward Compute Details.";
			e.printStackTrace();
			log.info("CASS WINNER REWARDs : "+": "+contestIdStr+" EXECEPTION");
			error.setDesc("Error occured while Fetching Customer Reward Compute Details.");
			contestWinnerRewards.setErr(error);
			contestWinnerRewards.setSts(0);
			return contestWinnerRewards;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.COMPCONTESTWINNERREWARDS, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS UPD REWRD COMP : "+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		
		return contestWinnerRewards;
	
	}
	
	@RequestMapping(value = "/GetGrandWinners", method=RequestMethod.POST)
	public @ResponsePayload ContSummaryInfo getQuizGrandWinners(HttpServletRequest request,HttpServletResponse response,Model model){
		ContSummaryInfo contSummary =new ContSummaryInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		String contestIdStr = request.getParameter("coId");
		Integer contestId = null;
		
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				contSummary.setErr(authError);
				contSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,authError.getDescription());
				return contSummary;
			}*/
			
			//String customerIdStr = request.getParameter("customerId");
			//String summaryTypeStr = request.getParameter("summaryType");
			
			/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
				error.setDesc("Summary Type is mandatory");
				contSummary.setErr(error);
				contSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Summary Type is mandatory");
				return contSummary;
			}
			QuizSummaryType quizSummaryType = null;
			try {
				quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
				
			} catch(Exception e) {
				error.setDesc("Summary Type is Invalid");
				contSummary.setErr(error);
				contSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Summary Type is Invalid");
				return contSummary;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				contSummary.setErr(error);
				contSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Customer Id is mandatory");
				return contSummary;
			}*/
			
			//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				resMsg="Contest Id is mandatory:"+contestIdStr;
				error.setDesc("Contest Id is mandatory");
				contSummary.setErr(error);
				contSummary.setSts(0);
				return contSummary;
			}
			
			//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				resMsg="Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				contSummary.setErr(error);
				contSummary.setSts(0);
				return contSummary;
			}
			contestId = quizContest.getId();
			
			/*List<QuizContestWinners> grandWinners = null;
			if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
				grandWinners = QuizContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));	
			} else {
				grandWinners = QuizContestUtil.getContestGrandWinners(quizContest.getId());
			}*/
			List<CassContestWinners> grandWinners = CassContestUtil.getContestGrandWinners(quizContest.getId());
			if(grandWinners == null || grandWinners.isEmpty()) {
				Date startGrand = new Date();
				grandWinners = CassContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));
				/*try {
					Date startOne = new Date();
				//QuizContestUtil.forceSummaryRefreshTable();
				//QuizContestUtil.refreshSummaryDataTable();
				QuizContestUtil.refreshSummaryDataCache();
				
				log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestIdStr+" : "+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					Date startOne = new Date();
					//QuizDAORegistry.getQuizSummaryManagerDAO().updateCustomerPromoCodeAndContestORderStats();
					QuizCustomerPromocodeandContestOrderStatsScheduler.processCustomerPromoCodeAndContestOrderStats();
				log.info("Time to Update Cust PRomo and co stats Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestIdStr+" : "+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}*/
				log.info("Time Grand Full Compute : "+(new Date().getTime()-startGrand.getTime())+" : "+ new Date());
			} else {
				log.info("Grand Winner Else  : "+(new Date().getTime()));
			}
			 
			//quizContest.setProcessStatus(ContestProcessStatus.GRANDWINNERCOMPUTED);
			//QuizContestUtil.updateQuizContest(quizContest);
			
			contSummary.setWinners(grandWinners);
			contSummary.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg="Success:"+contestIdStr;
		}catch(Exception e){
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Grand Winners.");
			contSummary.setErr(error);
			contSummary.setSts(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Error occured while Fetching Grand Winners.");
			return contSummary;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZGRANDWINNERS, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS GRANDWINNER : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		
		return contSummary;
	
	}
	
	@RequestMapping(value = "/GetContestSummary", method=RequestMethod.POST)
	public @ResponsePayload ContSummaryInfo getContestSummary(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ContSummaryInfo contestSummary =new ContSummaryInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer contestId = null;
		//String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		//String summaryTypeStr = request.getParameter("sType");
		try {
			
			/*QuizSummaryType quizSummaryType = null;
			try {
				quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
				
			} catch(Exception e) {
				resMsg = "Summary Type is Invalid";
				error.setDesc("Summary Type is Invalid");
				contestSummary.setErr(error);
				contestSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return contestSummary;
			}*/
			
			/*if(!quizSummaryType.equals(QuizSummaryType.CONTEST)) {
				Error authError = authorizationValidation(request);
				if(authError != null) {
					resMsg = authError.getDescription();
					quizContestSummary.setErr(authError);
					quizContestSummary.setSts(0);
					TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
					return quizContestSummary;
				}
			}*/
			
			
			/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
				resMsg = "Summary Type is mandatory";
				error.setDesc("Summary Type is mandatory");
				quizContestSummary.setErr(error);
				quizContestSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return quizContestSummary;
			}*/
			
						
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				resMsg = "Customer Id is mandatory";
				error.setDesc("Customer Id is mandatory");
				quizContestSummary.setErr(error);
				quizContestSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return quizContestSummary;
			}*/
						
			//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
				if(TextUtil.isEmptyOrNull(contestIdStr)){
					resMsg = "Contest Id is mandatory:"+contestIdStr;
					error.setDesc("Contest Id is mandatory");
					contestSummary.setErr(error);
					contestSummary.setSts(0);
					return contestSummary;
				}
				
				//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
				QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
				if(quizContest == null) {
					resMsg = "Contest Id is Invalid:"+contestIdStr;
					error.setDesc("Contest Id is Invalid");
					contestSummary.setErr(error);
					contestSummary.setSts(0);
					return contestSummary;
				}
				//List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByContestId(Integer.parseInt(contestIdStr));
				List<CassContestWinners> contestWinners = CassContestUtil.getContestSummaryData(quizContest.getId());
				contestSummary.setWinners(contestWinners);
				if(contestWinners != null) {
					contestSummary.setwCount(contestWinners.size());	
				}
				
				
				/*if(Integer.parseInt(customerIdStr) != 0) {
					QuizContestWinners contestWinner = QuizContestUtil.getQuizContestWinnerByCustomerIdAndContestId(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
					quizContestSummary.setQuizContestWinner(contestWinner);
				}*/
				
			//} 
				/*else if(quizSummaryType.equals(QuizSummaryType.TILLDATE)) {
				
				List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByTillDate();
				QuizContestWinners contestWinner = null;
				if(contestWinners != null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						Integer customerId = Integer.parseInt(customerIdStr);
						for (QuizContestWinners contestWinnerObj : contestWinners) {
							if(contestWinnerObj.getCustomerId().equals(customerId)) {
								contestWinner = contestWinnerObj;
							}
						}
					}
				}
				if(contestWinner == null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						contestWinner = QuizContestUtil.getQuizContestWinnersByTillDateAndCustomerId(Integer.parseInt(customerIdStr));
					}
				}
				quizContestSummary.setQuizContestWinners(contestWinners);
				quizContestSummary.setQuizContestWinner(contestWinner);
				//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
			} else if(quizSummaryType.equals(QuizSummaryType.THISWEEK)) {

				//This Week starts from Sunday to till date
				Calendar cal = Calendar.getInstance();
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				dayOfWeek = 1-dayOfWeek;
				cal.add(Calendar.DAY_OF_MONTH, dayOfWeek);
				String startDateStr = dbDateFormat.format(new Date(cal.getTimeInMillis()))+" 00:00:00";
				String toDateStr = dbDateFormat.format(new Date())+" 23:59:59";
				
				List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByThisWeek(startDateStr, toDateStr);
				QuizContestWinners contestWinner = null;
				if(contestWinners != null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						Integer customerId = Integer.parseInt(customerIdStr);
						for (QuizContestWinners contestWinnerObj : contestWinners) {
							if(contestWinnerObj.getCustomerId().equals(customerId)) {
								contestWinner = contestWinnerObj;
							}
						}
					}
				}
				if(contestWinner == null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						contestWinner = QuizContestUtil.getQuizContestWinnersByThisWeekAndCustomerId(startDateStr, toDateStr,Integer.parseInt(customerIdStr));
					}
				}
				quizContestSummary.setQuizContestWinners(contestWinners);
				quizContestSummary.setQuizContestWinner(contestWinner);
				//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
			}*/
			
				contestSummary.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success";
			resMsg = "Success:"+contestSummary.getwCount();
			//TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Contest Summary.";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Contest Summary.");
			contestSummary.setErr(error);
			contestSummary.setSts(0);
			return contestSummary;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.QUIZCONTESTSUMMARY, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS SUMMARY : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : stype: "+request.getParameter("summaryType")+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		}
		return contestSummary;
	
	}
	@RequestMapping(value = "/GetContCustCount", method=RequestMethod.POST)
	public @ResponsePayload CassJoinContestInfo getContCustCount(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CassJoinContestInfo joinContestInfo =new CassJoinContestInfo();
		CassError error = new CassError();
		
		String contestIdStr = request.getParameter("coId");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		String resMsg = "";
		Integer contestId = null;
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizJoinContestInfo.setErr(authError);
				quizJoinContestInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,authError.getDescription());
				return quizJoinContestInfo;
			}*/
			
			
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				resMsg = "Contest Id is mandatory:"+contestIdStr;
				error.setDesc("Contest Id is mandatory");
				joinContestInfo.setErr(error);
				joinContestInfo.setSts(0);
				return joinContestInfo;
			}
			
			QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				joinContestInfo.setErr(error);
				joinContestInfo.setSts(0);
				return joinContestInfo;
			}
			contestId = quizContest.getId();
			if(quizContest.getStatus()== null || !quizContest.getStatus().equals("STARTED")) {
				resMsg = "Contest Not Yet Started:"+contestIdStr;
				error.setDesc("Contest Not Yet Started");
				joinContestInfo.setErr(error);
				joinContestInfo.setSts(0);
				return joinContestInfo;
			}

			Integer totalCustomersCount = CassContestUtil.getContestCustomersCount(quizContest.getId());
			joinContestInfo.settCount(totalCustomersCount);
			joinContestInfo.setSts(1);
			
			resMsg = "Success:"+totalCustomersCount;
			//TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,"Success: "+totalCustomersCount);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Contest customers count.";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Contest customers count.");
			joinContestInfo.setErr(error);
			joinContestInfo.setSts(0);
			return joinContestInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.CONTCUSTCOUNT, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS CUT COUNT DTLS : "+" : coId: "+contestIdStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		
		return joinContestInfo;
	}
	
	@RequestMapping(value = "/GetLiveCustList", method=RequestMethod.POST)
	public @ResponsePayload LiveCustomerListInfo getLiveCustList(HttpServletRequest request,HttpServletResponse response,Model model){
		
		LiveCustomerListInfo liveCustListInfo = new LiveCustomerListInfo();
		CassError error = new CassError();
		Date start = new Date();
		String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String pageNoStr = request.getParameter("pNo");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		String resMsg = "";
		Integer contestId = null;
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				liveCustListInfo.setErr(authError);
				liveCustListInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,authError.getDescription());
				return liveCustListInfo;
			}*/
			 
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				liveCustListInfo.setErr(error);
				liveCustListInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Customer Id is mandatory");
				return liveCustListInfo;
			}
			
			if(TextUtil.isEmptyOrNull(questionNoStr)){
				error.setDesc("Question No is mandatory");
				liveCustListInfo.setErr(error);
				liveCustListInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Question No is mandatory");
				return liveCustListInfo;
			}
			if(TextUtil.isEmptyOrNull(pageNoStr)){
				error.setDesc("Page No is mandatory");
				liveCustListInfo.setErr(error);
				liveCustListInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Page No is mandatory");
				return liveCustListInfo;
			}*/
			Integer pageNo = 1;
			try {
				pageNo = Integer.parseInt(pageNoStr);
			} catch(Exception e) {
				resMsg = "Page No is Invalid:"+pageNoStr;
				error.setDesc("Page No is Invalid");
				liveCustListInfo.setErr(error);
				liveCustListInfo.setSts(0);
				return liveCustListInfo;
			}
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDesc("Contest Id is mandatory");
				liveCustListInfo.setErr(error);
				liveCustListInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETLIVECUSTLIST,"Contest Id is mandatory");
				return liveCustListInfo;
			}*/
			
			//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest quizContest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				liveCustListInfo.setErr(error);
				liveCustListInfo.setSts(0);
				return liveCustListInfo;
			}
			contestId = quizContest.getId();
			Integer maxRows = 25;
			liveCustListInfo = CassContestUtil.getLiveCustomerDetailsInfo(Integer.parseInt(contestIdStr),liveCustListInfo,pageNo,maxRows);
			 
			
			//liveCustListInfo.setQuizContestWinners(grandWinners);
			resMsg = "Success:"+contestIdStr+":coId:"+contestIdStr+":pNo:"+pageNoStr;
			liveCustListInfo.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Live Customer List.";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Live Customer List.");
			liveCustListInfo.setErr(error);
			liveCustListInfo.setSts(0);
			return liveCustListInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETLIVECUSTLIST, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS CUT COUNT DTLS : "+customerIdStr+" : coId: "+contestIdStr+" :pNo: "+pageNoStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		
		return liveCustListInfo;
	
	}
	
	@RequestMapping(value = "/GetQuestAnsCount", method=RequestMethod.POST)
	public @ResponsePayload AnswerCountInfo getQuestAnsCount(HttpServletRequest request,HttpServletResponse response,Model model){
		
		log.info("Inside Get Questions 1 : "+" : "+ new Date() );
		AnswerCountInfo ansCountInfo =new AnswerCountInfo();
		CassError error = new CassError();
		Date start = new Date();
		Date temp = new Date();
		//String customerIdStr = request.getParameter("cuId");
		String contestIdStr = request.getParameter("coId");
		String questionNoStr = request.getParameter("qNo");
		String questionIdStr = request.getParameter("qId");
		//String summaryTypeStr = request.getParameter("type");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		String resMsg = "";
		Integer contestId = null;
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				ansCountInfo.setErr(authError);
				ansCountInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,authError.getDescription());
				return ansCountInfo;
			}*/
			
			/*
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				ansCountInfo.setErr(error);
				ansCountInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Customer Id is mandatory");
				return ansCountInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDesc("Contest Id is mandatory");
				ansCountInfo.setErr(error);
				ansCountInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Contest Id is mandatory");
				return ansCountInfo;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				error.setDesc("Question Id is mandatory");
				ansCountInfo.setErr(error);
				ansCountInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Question Id is mandatory");
				return ansCountInfo;
			}
			if(TextUtil.isEmptyOrNull(questionNoStr)){
				error.setDesc("Question No is mandatory");
				ansCountInfo.setErr(error);
				ansCountInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Question No is mandatory");
				return ansCountInfo;
			}*/
			log.info("Inside Get Questions before contest fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			temp = new Date();
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null){
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				ansCountInfo.setErr(error);
				ansCountInfo.setSts(0);
				return ansCountInfo;
			}
			contestId = contest.getId();
			
			/*QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				error.setDesc("Question Id is Invalid");
				ansCountInfo.setErr(error);
				ansCountInfo.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Question Id is Invalid");
				return ansCountInfo;
			}*/
//////			
			log.info("Inside Get Questions after contest fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			temp = new Date();
			QuizContestQuestions quizQuestion = QuizContestUtil.getContestCurrentQuestions(contest.getId());
			if(quizQuestion == null || !quizQuestion.getId().equals(Integer.parseInt(questionIdStr))) {
				resMsg = "Question Id is Invalid:"+questionIdStr;
				error.setDesc("Question Id is Invalid");
				ansCountInfo.setErr(error);
				ansCountInfo.setSts(0);
				return ansCountInfo;
			}
			quizQuestion.setIsAnswerCountComputed(true);
			QuizContestUtil.updateContestQuestions(quizQuestion);
			
			ansCountInfo.setcAnswer(quizQuestion.getAnswer());
			ansCountInfo.setQuRwds(quizQuestion.getQuestionRewards());
			
			log.info("Inside Get Questions before count fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			temp = new Date();
			ansCountInfo = CassContestUtil.getQuestAnsCount(contest.getId(), quizQuestion.getId(), ansCountInfo);
			
			/*List<Object[]> result = QuizDAORegistry.getQuizQueryManagerDAO().getContestAnswersCountByQuestionId(quizQuestion.getId());
			if(result != null) {
				for (Object[] object : result) {
					String answer = (String)object[1];
					Integer count = (Integer)object[0];
					if(answer != null) {
						if(answer.equals("A")) {
							ansCountInfo.setaCount(count);
						} else if(answer.equals("B")) {
							ansCountInfo.setbCount(count);
						} else if(answer.equals("C")) {
							ansCountInfo.setcCount(count);
						} else if(answer.equals("D")) {
							ansCountInfo.setOptionDCount(count);
						}
					}
				}
			}*/
			log.info("Inside Get Questions after count fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				quizContestDetails.setErr(error);
				quizContestDetails.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Customer Id is mandatory");
				return quizContestDetails;
			}*/
			
			/*QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerById(Integer.parseInt(customerIdStr));
			if(quizCustomer == null){
				error.setDesc("Customer not valid");
				quizContestDetails.setErr(error);
				quizContestDetails.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETQUESTANSCOUNT,"Customer not valid");
				return quizContestDetails;
			}*/
			
			resMsg = "Success:"+questionIdStr;
			//ansCountInfo.setQuizContestQuestion(quizQuestion);
			ansCountInfo.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Answer Count Details.";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Answer Count Details.");
			ansCountInfo.setErr(error);
			ansCountInfo.setSts(0);
			return ansCountInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETQUESTANSCOUNT, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS ANS COUNT : "+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		}
		
		return ansCountInfo;
	
	}
	
	@RequestMapping(value = "/GetHallOfFame", method=RequestMethod.POST)
	public @ResponsePayload HallOfFameInfo GetHallOfFame(HttpServletRequest request,HttpServletResponse response,Model model){
		
		HallOfFameInfo hallOfFameInfo =new HallOfFameInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		//String customerIdStr = request.getParameter("cuId");
		//String contestIdStr = request.getParameter("coId");
		String summaryTypeStr = request.getParameter("type");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer contestId = null;
		try {
			
			QuizSummaryType quizSummaryType = null;
			try {
				quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
				
			} catch(Exception e) {
				resMsg = "Summary Type is Invalid";
				error.setDesc("Summary Type is Invalid");
				hallOfFameInfo.setErr(error);
				hallOfFameInfo.setSts(0);
				return hallOfFameInfo;
			}
			
			/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
				resMsg = "Summary Type is mandatory";
				error.setDesc("Summary Type is mandatory");
				quizContestSummary.setErr(error);
				quizContestSummary.setSts(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return quizContestSummary;
			}*/
			
						

			if(quizSummaryType.equals(QuizSummaryType.TILLDATE)) {
				
				List<HallOfFameDtls> hallOfFameList = CassContestUtil.getHallOfFameForTillDateData();
				hallOfFameInfo.setList(hallOfFameList);
				if(hallOfFameList != null) {
					hallOfFameInfo.setwCount(hallOfFameList.size());
				}
				
			} else if(quizSummaryType.equals(QuizSummaryType.THISWEEK)) {

				//This Week starts from Sunday to till date
				/*Calendar cal = Calendar.getInstance();
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				dayOfWeek = 1-dayOfWeek;
				cal.add(Calendar.DAY_OF_MONTH, dayOfWeek);
				String startDateStr = dbDateFormat.format(new Date(cal.getTimeInMillis()))+" 00:00:00";
				String toDateStr = dbDateFormat.format(new Date())+" 23:59:59";*/
				
				List<HallOfFameDtls> hallOfFameList = CassContestUtil.getHallOfFameForThisWeekData();
				hallOfFameInfo.setList(hallOfFameList);
				if(hallOfFameList != null) {
					hallOfFameInfo.setwCount(hallOfFameList.size());
				}
				//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
			}
			
			hallOfFameInfo.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success";
		}catch(Exception e){
			resMsg = "Error occured while Fetching Hall Of Fame Data.";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching  Hall Of Fame Data.");
			hallOfFameInfo.setErr(error);
			hallOfFameInfo.setSts(0);
			return hallOfFameInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETHALLOFFAME, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS HALLOFFAME : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : stype: "+request.getParameter("summaryType")+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		}
		return hallOfFameInfo;
	
	}
	@RequestMapping(value = "/UpdateCustContStats", method=RequestMethod.POST)
	public @ResponsePayload CommonRespInfo updateCustContStats(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CommonRespInfo commonRespInfo = new CommonRespInfo();
		CassError error = new CassError();
		Date start = new Date();
		String contestIdStr = request.getParameter("coId");
		String resMsg = "";
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer contestId = null;
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizCustomerStatsInfo.setError(authError);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.UPDATECUSTCONTESTSTATS,authError.getDescription());
				return quizCustomerStatsInfo;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.UPDATECUSTCONTESTSTATS,"Contest Id is mandatory");
				return quizCustomerStatsInfo;
			}*/
			
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null) {
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			contestId = contest.getId();
			
			if(contest.getIsCustomerStatsUpdated()) {
				resMsg = "Customer Stats Already Updated for This Contest.";
				error.setDesc("Customer Stats Already Updated for This Contest.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			
			Boolean flag = CassContestUtil.updateCustomerContestDetailsinCassandra(contest.getId());
			if(!flag) {
				resMsg = "Error occured while Updating CustomerUtil Contest stats.";
				error.setDesc("Error occured while Updating CustomerUtil Contest stats.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			
			
			try {
				//Migrate Data from Cassandra to Sql Database while ending contest.
				migrateToSqlFromCassandra(contest.getId());
			}catch(Exception e) {
				resMsg = "Error occured while Migrating Contest Data to SQL from Cassandra.";
				e.printStackTrace();
				error.setDesc("Error occured while Migrating Contest Data to SQL from Cassandra.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			String msg = "";
			try {
				msg = updatePostMigrationStatusonServlet(contestId);
				if(msg != null) {
					resMsg = "Error occured while Invoking Update Post Migration status API to Servlet."+msg;
					error.setDesc("Error occured while Invoking Update Post Migration status API to Servlet.");
					commonRespInfo.setErr(error);
					commonRespInfo.setSts(0);
					return commonRespInfo;
				}
			} catch (Exception e) {
				resMsg = "Error occured while updating Post Migration status to Servlet."+msg;
				e.printStackTrace();
				error.setDesc("Error occured while updating Post Migration status to Servlet.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			/*try {
				//Truncate table data after migrating contest data from cassandra to sql.
				truncateCassandraContestData(contestIdStr);
			}catch(Exception e) {
				e.printStackTrace();
			}*/
			
			resMsg = "Success";
			commonRespInfo.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
		}catch(Exception e){
			resMsg = "Error occured while Updating Customer Contest stats.";
			e.printStackTrace();
			error.setDesc("Error occured while Updating Customer Contest stats.");
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);
			return commonRespInfo;
		}finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.UPDATECUSTCONTESTSTATS, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS UPD CUST CONT STATS : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		return commonRespInfo;
	
	}
	@RequestMapping(value = "/RemoveCassContCacheData", method=RequestMethod.POST)
	public @ResponsePayload CommonRespInfo removeCassContCacheData(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CommonRespInfo commonRespInfo = new CommonRespInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		String contestIDStr = request.getParameter("coId");
		Integer contestId = null;
		try {
			//String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			if(TextUtil.isEmptyOrNull(contestIDStr)){
				resMsg = "Contest Id is mandatory";
				error.setDesc(resMsg);
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIDStr));
			//QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIDStr));
			if(contest == null) {
				resMsg ="Contest Id is Invalid";
				error.setDesc(resMsg);
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			contestId = contest.getId();
			
			log.info("RESETCONTESTAPI: REMOVING DATA FROM CACHE- Begins:"+new Date());
			CassContestUtil.clearCacheDataByContestId(Integer.parseInt(contestIDStr));
			QuizContestUtil.clearCacheDataByContestId(Integer.parseInt(contestIDStr));
			log.info("RESETCONTESTAPI: REMOVING DATA FROM CACHE- Ends:"+new Date());
			
			truncateCassandraContestData(contestIDStr);
			
			resMsg = "Contest Cache Data Removed successfully.";
			commonRespInfo.setMsg("Contest Cache Data Removed successfully.");
			//quizCustomerDetails.setError(error);
			commonRespInfo.setSts(1);
		}catch(Exception e){
			resMsg = "Error occured while removing contest cache data :";
			e.printStackTrace();
			error.setDesc(resMsg);
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);
			//TrackingUtils.contestAPITracking(request, WebServiceActionType.REMOVECASSCONTESTCACHE,"Error Occured While Removing Contest Cache Data.");
			return commonRespInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.REMOVECASSCONTESTCACHE, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS REMOVE CACHE : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+":"+resMsg);
		}
		
		
		return commonRespInfo;
	}
	@RequestMapping(value = "/GetDashboardInfoOne", method=RequestMethod.POST)
	public @ResponsePayload DashboardInfo getDashboardInfoOne(HttpServletRequest request,HttpServletResponse response,Model model){
		
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer customerId=null;
		try {
			dashboardInfo.setSts(1);
			resMsg = "Success:"+customerIdStr;
			//TrackingUtils.contestDashboardAPITracking(request, ,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Dashboard Information";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Dashboard Information");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} finally {
			//TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, customerId, start, new Date(), request.getHeader("X-Forwarded-For"));
			log.info("GETDASH INFO: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	}
	@RequestMapping(value = "/GetDashboardInfoTwo", method=RequestMethod.POST)
	public @ResponsePayload DashboardInfo getDashboardInfoTwo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer customerId=null;
		try {
			CassCustomer customer = null;
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				 
				try {
					customerId = Integer.parseInt(customerIdStr.trim());
					customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
					if(customer == null) {
						resMsg = "Customer Id is Invalid:"+customerIdStr;
						error.setDesc("Customer Id is Invalid");
						dashboardInfo.setErr(error);
						dashboardInfo.setSts(0);
						return dashboardInfo;
					}
				} catch(Exception e) {
					resMsg = "Customer Id Not Exist:"+customerIdStr;
					error.setDesc("Customer Id Not Exist");
					dashboardInfo.setErr(error);
					dashboardInfo.setSts(0);
					return dashboardInfo;
				}
			}
			
			dashboardInfo.setSts(1);
			dashboardInfo.setCust(customer);
			dashboardInfo.sethRwds(CassContestUtil.HIDE_DASHBOARD_REWARDS);
			//dashboardInfo.setMessage("");
			
			resMsg = "Success:"+customerIdStr;
			//TrackingUtils.contestDashboardAPITracking(request, ,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Dashboard Information";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Dashboard Information");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} finally {
			//TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, customerId, start, new Date(), request.getHeader("X-Forwarded-For"));
			log.info("GETDASH INFO: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		
		return dashboardInfo;
	
	}
	
	@RequestMapping(value = "/GetDashboardInfoThree", method=RequestMethod.POST)
	public @ResponsePayload DashboardInfo getDashboardInfoThree(HttpServletRequest request,HttpServletResponse response,Model model){
		
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("cuId");
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer customerId=null;
		try {
			CassCustomer customer = null;
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				 
				try {
					customerId = Integer.parseInt(customerIdStr.trim());
					customer = null;//CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
					if(customer == null) {
						resMsg = "Customer Id is Invalid:"+customerIdStr;
						error.setDesc("Customer Id is Invalid");
						dashboardInfo.setErr(error);
						dashboardInfo.setSts(0);
						return dashboardInfo;
					}
				} catch(Exception e) {
					resMsg = "Customer Id Not Exist:"+customerIdStr;
					error.setDesc("Customer Id Not Exist");
					dashboardInfo.setErr(error);
					dashboardInfo.setSts(0);
					return dashboardInfo;
				}
			}
			
			dashboardInfo.setSts(1);
			dashboardInfo.setCust(customer);
			dashboardInfo.sethRwds(CassContestUtil.HIDE_DASHBOARD_REWARDS);
			//dashboardInfo.setMessage("");
			
			resMsg = "Success:"+customerIdStr;
			//TrackingUtils.contestDashboardAPITracking(request, ,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Dashboard Information";
			e.printStackTrace();
			error.setDesc("Error occured while Fetching Dashboard Information");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.GETDASHBOARDINFO, resMsg, null, 
					customerId, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("GETDASH INFO: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		
		return dashboardInfo;
	
	}
	
	
	@RequestMapping(value = "/AddSuperFanWinners")
	public @ResponsePayload DashboardInfo addSuperFanWinners(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		String resMsg = "";
		String coIdStr = request.getParameter("coId");
		String minNoOfChances = request.getParameter("minNoOfChances");
		try {
			CassandraDAORegistry.getCassSuperFanWinnerDAO().truncate();
			Integer noOfChances = Integer.parseInt(minNoOfChances.trim());
			Integer coId = Integer.parseInt(coIdStr);
			List<QuizSuperFanStat> superFanStatsList = QuizDAORegistry.getQuizSuperFanStatDAO().getSuperFanWinnersByChances(noOfChances);
			Long now = new Date().getTime();
			Integer noOfSuperFanStats = 0, totalINserted = 0, totalFailedInsert = 0;
			if(null != superFanStatsList && !superFanStatsList.isEmpty()) {
				noOfSuperFanStats = superFanStatsList.size();
				for (QuizSuperFanStat obj : superFanStatsList) {
					try {
						CassSuperFanWinner cassSuperFanWinner = new CassSuperFanWinner(coId, obj.getCustomerId(), now,4);
						CassandraDAORegistry.getCassSuperFanWinnerDAO().save(cassSuperFanWinner);
						totalINserted++;
					}catch(Exception e){
						totalFailedInsert++;
					}
				}
			}
			resMsg = "Success: TotalSuperFans: "+noOfSuperFanStats+", Inserted: "+totalINserted+", Failed: "+totalFailedInsert;
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while inserting super fan winners to cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while inserting super fan winners to cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} 
		
		return dashboardInfo;
	
	}
	
	
	@RequestMapping(value = "/SendSuperFanWinner")
	public @ResponsePayload DashboardInfo sendSuperFanWinnerMessage(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		String resMsg = "";
		try {
			List<CassSuperFanWinner> list = CassandraDAORegistry.getCassSuperFanWinnerDAO().getSuperFanWinnersByContestId(100);
			Integer noOfSuperFanStats = 0, totalINserted = 0, totalFailedInsert = 0, exceptionCount = 0, processing =0;
			if(null != list && !list.isEmpty()) {
				noOfSuperFanStats = list.size();
				for (CassSuperFanWinner obj : list) {
					try {
						CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(obj.getCuId());
						if(cassCustomer != null) {
							TwilioSMSServices.sendSuperFanWinnerMessage(cassCustomer.getPhone());
							totalINserted++;
						}else {
							totalFailedInsert++;
						}
					}catch(Exception e){
						exceptionCount++;
					}
					processing++;
					
					System.out.println("Total : "+noOfSuperFanStats+", Processing : "+processing+", Success: "+totalINserted+", TotalFailed: "+totalFailedInsert+", Exception: "+exceptionCount);
				}
			}
			resMsg = "Success: TotalSuperFans: "+noOfSuperFanStats+", Inserted: "+totalINserted+", Failed: "+totalFailedInsert;
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while inserting super fan winners to cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while inserting super fan winners to cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} 
		
		return dashboardInfo;
	
	}
	
}	