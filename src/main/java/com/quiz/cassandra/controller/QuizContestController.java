package com.quiz.cassandra.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.CassCustomerUtil;
import com.rtfquiz.jdbc.service.JDBCConnection;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestEventRequest;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.CustomerContestQuestion;
import com.rtfquiz.webservices.data.CustomerPromoRewardDetails;
import com.rtfquiz.webservices.data.QuizConfigSettings;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestParticipants;
import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerFriend;
import com.rtfquiz.webservices.data.QuizCustomerInvitedFriends;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.rtfquiz.webservices.data.QuizFirebaseCallBackTracking;
import com.rtfquiz.webservices.data.QuizOTPTracking;
import com.rtfquiz.webservices.enums.ContestProcessStatus;
import com.rtfquiz.webservices.enums.FriendStatus;
import com.rtfquiz.webservices.enums.QuizMessageTextType;
import com.rtfquiz.webservices.enums.QuizSummaryType;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.rtfquiz.webservices.utils.QuizConfigSettingsUtil;
import com.rtfquiz.webservices.utils.QuizContestStartNotification;
import com.rtfquiz.webservices.utils.QuizContestUtil;
import com.rtfquiz.webservices.utils.QuizCustomerLifeLineEarnedNotificationUtil;
import com.rtfquiz.webservices.utils.QuizGrandWinnerTicketsUpcomingUtil;
import com.rtfquiz.webservices.utils.QuizGrandWinnerTicketsUtil;
import com.rtfquiz.webservices.utils.RTFBotsUtil;
import com.rtfquiz.webservices.utils.list.BotDetail;
import com.rtfquiz.webservices.utils.list.ContestEventResponse;
import com.rtfquiz.webservices.utils.list.ContestTicketsApiResponse;
import com.rtfquiz.webservices.utils.list.CustomerFriendDetails;
import com.rtfquiz.webservices.utils.list.CustomerNotificationDetails;
import com.rtfquiz.webservices.utils.list.CustomerPhoneSearchInfo;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.rtfquiz.webservices.utils.list.CustomerStatistics;
import com.rtfquiz.webservices.utils.list.CustomerStats;
import com.rtfquiz.webservices.utils.list.CustomerStatsDetails;
import com.rtfquiz.webservices.utils.list.QuizAnswerCountDetails;
import com.rtfquiz.webservices.utils.list.QuizContestFriendRequestDetails;
import com.rtfquiz.webservices.utils.list.QuizContestInfo;
import com.rtfquiz.webservices.utils.list.QuizContestSummaryInfo;
import com.rtfquiz.webservices.utils.list.QuizContestWinnerRewards;
import com.rtfquiz.webservices.utils.list.QuizCustomerCountDetailInfo;
import com.rtfquiz.webservices.utils.list.QuizCustomerDetails;
import com.rtfquiz.webservices.utils.list.QuizCustomerLifeLineUsage;
import com.rtfquiz.webservices.utils.list.QuizCustomerReferralCodeInfo;
import com.rtfquiz.webservices.utils.list.QuizCustomerSearchInfo;
import com.rtfquiz.webservices.utils.list.QuizCustomerStatsInfo;
import com.rtfquiz.webservices.utils.list.QuizDiscoverPageInfo;
import com.rtfquiz.webservices.utils.list.QuizGenericResponse;
import com.rtfquiz.webservices.utils.list.QuizInviteFriendsInfo;
import com.rtfquiz.webservices.utils.list.QuizJoinContestInfo;
import com.rtfquiz.webservices.utils.list.QuizMessageTextsInfo;
import com.rtfquiz.webservices.utils.list.QuizOTPDetails;
import com.rtfquiz.webservices.utils.list.QuizPromoCodeInfo;
import com.rtfquiz.webservices.utils.list.QuizQuestionInfo;
import com.rtfquiz.webservices.utils.list.QuizSettings;
import com.rtfquiz.webservices.utils.list.QuizValidateAnswerInfo;
import com.rtfquiz.webservices.utils.list.QuizVideoSourceUrlInfo;
import com.rtfquiz.webservices.utils.list.RedeemRewardTixPromoInfo;
import com.rtfquiz.webservices.utils.list.SendInviteFriendEmailInfo;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CategoryTicket;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerLoginHistory;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.OrderPaymentBreakup;
import com.zonesws.webservices.data.Property;
import com.zonesws.webservices.data.QuizAffiliateSetting;
import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;
import com.zonesws.webservices.data.RtfPromotionalEarnLifeOffers;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CustomerType;
import com.zonesws.webservices.enums.InvoiceStatus;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.PromotionalType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.enums.SignupType;
import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.filter.ReferralCodeGenerator;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.sms.TwilioSMSServices;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.PaginationUtil;
import com.zonesws.webservices.utils.PasswordUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.CustomerDetails;
import com.zonesws.webservices.utils.list.NormalSearchResult;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/GetOTP","/ValidateOTP","/QuizCustomerLogin","/QuizCustomerSignUp","/GetQuizContestInfo",
	"/GetQuizQuestionInfo","/ValidateQuizAnswer","/GetQuestionAnswers","/GetQuizContestSummary",
	"/UpdateCustomerReferralCode","/ApplyCustomerLifeLine","/RequestShow","/SubmitTrivia","/CreateWinnerOrder",
	"/UpdateCustomersProfileImage","/UpdateCustomerLifeLine","/RefreshCustomerCache",
	"/RemoveContestCacheData","/RefreshContestCacheData",
	"/GetQuizVideoSourceUrl","/GetQuizContestCustomersCount","/QuizJoinContest","/QuizExitContest",
	"/UpdateContestWinnerRewards","/GetQuizGrandWinners","/GetCustomerCountDetails","/QuizCustomersAutoSearch","/GetDiscoverPageData",
	"/GetCustomerStatsDetails","/UpdateCustomerContestStats","/AcceptRejectFriendRequest","/SendFriendRequest","/GetCustomerPhoneSearch",
	"/QuizUserIdSetup","/GetContestPromoCode","/SendPromoCodeEmail","/GetContestEvents","/UpdateStartQuizContest","/UpdateEndQuizContest",
	"/GetQuizMessageTexts","/QuizRemoveDuplicatePhoneMap","/GetQuestionLifeLineUsedCount","/RefreshSummaryData",
	"/ManualRefreshSummaryData","/SendQuizManualNotification","/ApplyRewardEarnLife","/RedeemRewardTixPromo","/SendInviteFriendsEmail",
	"/SendStaticEmailForDollarRevert","/AddContestTickets","/RemoveAllOrderedContestTickets",
	"/UpdateCustomerSignUpDetails","/ValidatePhoneNoLogin","/CustomerLoginByPhoneNo","/FirebaseCallBackTracking",
	"/GetCustomerStatsByUserId","/GetBotsReport","/ValidateQuizAnswerTest","/GetOneSignalCustomers","/LoadTest",
	"/ResetTestVariable","/LoadBotsToCache","/RefreshCustomerLoyaltyMap","/RefreshNextContestList"
	//"/GetServerTime","/CustomerEmailValidation","/GetCustomerProfilePic",
	//"/CustomerLogout","/SendRTFSMS","/PaypalTracking","/CustomerLogin","/SendEmail"
	})
public class QuizContestController implements ServletContextAware{
	
	private static Logger log = LoggerFactory.getLogger(QuizContestController.class);
	
	private static Integer apiHitCount = 0;
	
	 @Autowired
	 ServletContext context; 
	 
	 public void setServletContext(ServletContext servletContext) {
		 this.context = servletContext;
	 }
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private static SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private static DecimalFormat percentageDF = new DecimalFormat("#.##");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	@RequestMapping(value = "/RemoveContestCacheData", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo removeContestCacheData(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizcontestDetails = new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			String contestIDStr = request.getParameter("contestId");
			//String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			if(TextUtil.isEmptyOrNull(contestIDStr)){
				error.setDescription("Contest Id is mandatory");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREMOVECACHE,"Contest Id is mandatory");
				return quizcontestDetails;
			}
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIDStr));
			if(contest == null) {
				error.setDescription("Contest Id is Invalid");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREMOVECACHE,"Contest Id is Invalid");
				return quizcontestDetails;
			}
			log.info("RESETCONTESTAPI: REMOVING DATA FROM CACHE- Begins:"+new Date());
			//CassContestUtil.clearCacheDataByContestId(Integer.parseInt(contestIDStr));
			QuizContestUtil.clearCacheDataByContestId(Integer.parseInt(contestIDStr));
			log.info("RESETCONTESTAPI: REMOVING DATA FROM CACHE- Ends:"+new Date());
			
			QuizContestUtil.refreshNextContestList("MOBILE");
			QuizContestUtil.refreshNextContestList("WEB");
			
			quizcontestDetails.setMessage("Contest Cache Data Removed successfully.");
			//quizCustomerDetails.setError(error);
			quizcontestDetails.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while removing contest cache data :");
			quizcontestDetails.setError(error);
			quizcontestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREMOVECACHE,"Error Occured While Removing Contest Cache Data.");
			return quizcontestDetails;
		}
		log.info("QUIZ REMOVE CACHE : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime()));
		
		return quizcontestDetails;
	}
	@RequestMapping(value = "/RefreshContestCacheData", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo refreshContestCacheData(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizcontestDetails = new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			String contestIDStr = request.getParameter("contestId");
			//String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			if(TextUtil.isEmptyOrNull(contestIDStr)){
				error.setDescription("Contest Id is mandatory");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREFRESHCACHE,"Contest Id is mandatory");
				return quizcontestDetails;
			}
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIDStr));
			if(contest == null) {
				error.setDescription("Contest Id is Invalid");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREFRESHCACHE,"Contest Id is Invalid");
				return quizcontestDetails;
			}
			log.info("REFRESHCONTESTCACHE: REMOVING DATA FROM CACHE- Begins:"+new Date());
			QuizContestUtil.refreshCacheDataByContestId(contest.getId());
			log.info("REFRESHCONTESTCACHE: REMOVING DATA FROM CACHE- Ends:"+new Date());
			
			quizcontestDetails.setMessage("Contest Cache Data Refreshed successfully.");
			//quizCustomerDetails.setError(error);
			quizcontestDetails.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Refreshing contest cache data :");
			quizcontestDetails.setError(error);
			quizcontestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREFRESHCACHE,"Error Occured While Refreshing Contest Cache Data.");
			return quizcontestDetails;
		}
		log.info("QUIZ REFRESH CACHE : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime()));
		
		return quizcontestDetails;
	}
	@RequestMapping(value = "/UpdateCustomerLifeLine", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails updateCustomerLifeLine(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerDetails quizCustomerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			//QuizContestUtil.refreshSummaryDataTable();
			String customerIdStr = request.getParameter("customerId");
			//String contestIDStr = request.getParameter("contestId");
			String lifeCountStr = request.getParameter("lifeCount");
			//String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizCustomerDetails.setError(error);
				quizCustomerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Customer Id is mandatory");
				return quizCustomerDetails;
			}
			/*if(TextUtil.isEmptyOrNull(contestIDStr)){
				error.setDescription("Contest Id is mandatory");
				quizCustomerDetails.setError(error);
				quizCustomerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Contest Id is mandatory");
				return quizCustomerDetails;
			}*/
			
			if(TextUtil.isEmptyOrNull(lifeCountStr)){
				error.setDescription("Life Count is mandatory");
				quizCustomerDetails.setError(error);
				quizCustomerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Life Count is mandatory");
				return quizCustomerDetails;
			}
			Integer count = Integer.parseInt(lifeCountStr);
			Customer customer =CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizCustomerDetails.setError(error);
				quizCustomerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Customer Id is Invalid");
				return quizCustomerDetails;
			}
			
			/*if(contestIDStr != null && !contestIDStr.isEmpty()) {
				QuizCustomerContestAnswers answers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().
				getLifeLineUsedContestAnswerByCustomerIdandContestId(Integer.parseInt(customerIdStr), Integer.parseInt(contestIDStr));
				if(answers == null) {
					error.setDescription("LifeLine is not yet used for this Contest");
					quizCustomerDetails.setError(error);
					quizCustomerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"LifeLine is not yet used for this Contest");
					return quizCustomerDetails;
				}
				answers.setIsLifeLineUsed(false);
				QuizDAORegistry.getQuizCustomerContestAnswersDAO().update(answers);
			}*/

			customer.setQuizCustomerLives(count);
			CustomerUtil.updatedCustomerUtil(customer);
			DAORegistry.getCustomerDAO().update(customer);
			
			/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.updateCustomerLives(customer);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
			
			quizCustomerDetails.setCustomer(customer);
			quizCustomerDetails.setMessage("Customer LifeLine Updated Successfully: ");
			//quizCustomerDetails.setError(error);
			quizCustomerDetails.setStatus(1);
			return quizCustomerDetails;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Test Customers Profile Created :");
			quizCustomerDetails.setError(error);
			quizCustomerDetails.setStatus(0);
			return quizCustomerDetails;
		}
	}
	@RequestMapping(value = "/RefreshCustomerCache", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails refreshCustomerCache(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerDetails quizCustomerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			String customerIdStr = request.getParameter("customerId");
			//String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizCustomerDetails.setError(error);
				quizCustomerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Customer Id is mandatory");
				return quizCustomerDetails;
			}
			
			Customer customer =CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizCustomerDetails.setError(error);
				quizCustomerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Customer Id is Invalid");
				return quizCustomerDetails;
			}
			CustomerUtil.init();
			
			//quizCustomerDetails.setCustomer(customer);
			quizCustomerDetails.setMessage("Customer Cache Refreshed Successfully: ");
			//quizCustomerDetails.setError(error);
			quizCustomerDetails.setStatus(1);
			return quizCustomerDetails;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Refreshing customer cache data :");
			quizCustomerDetails.setError(error);
			quizCustomerDetails.setStatus(0);
			return quizCustomerDetails;
		}
	}
	@RequestMapping(value = "/UpdateCustomersProfileImage", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerDetails getCustomerInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerDetails quizCustomerDetails =new QuizCustomerDetails();
		Error error = new Error();
		try {
			QuizContestUtil.updateCustomerProfileImages();
			
			error.setDescription("Test Customers Profile Created : ");
			quizCustomerDetails.setError(error);
			quizCustomerDetails.setStatus(0);
			return quizCustomerDetails;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Test Customers Profile Created :");
			quizCustomerDetails.setError(error);
			quizCustomerDetails.setStatus(0);
			return quizCustomerDetails;
		}
	}
	@RequestMapping(value = "/GetQuizVideoSourceUrl", method=RequestMethod.POST)
	public @ResponsePayload QuizVideoSourceUrlInfo getCloudFrontUrl(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizVideoSourceUrlInfo quizCloudFrontUrlInfo =new QuizVideoSourceUrlInfo();
		Error error = new Error();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizCloudFrontUrlInfo.setError(authError);
				quizCloudFrontUrlInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,authError.getDescription());
				return quizCloudFrontUrlInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizCloudFrontUrlInfo.setError(error);
				quizCloudFrontUrlInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Customer Id is mandatory");
				return quizCloudFrontUrlInfo;
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				quizCloudFrontUrlInfo.setError(error);
				quizCloudFrontUrlInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Application Platform is mandatory");
				return quizCloudFrontUrlInfo;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					quizCloudFrontUrlInfo.setError(error);
					quizCloudFrontUrlInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please send valid application platform");
					return quizCloudFrontUrlInfo;
				}
			}
			Property property = DAORegistry.getPropertyDAO().get("quiz.video.source.url");
			if(property == null || property.getValue() == null) {
				error.setDescription("Cloud Front Url is not yet declared.");
				quizCloudFrontUrlInfo.setError(error);
				quizCloudFrontUrlInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Cloud Front Url is not yet declared.");
				return quizCloudFrontUrlInfo;
			}
			quizCloudFrontUrlInfo.setVideoSourceUrl(property.getValue());

			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Cloudfront Url.");
			quizCloudFrontUrlInfo.setError(error);
			quizCloudFrontUrlInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Error occured while Fetching Cloudfront Url");
			return quizCloudFrontUrlInfo;
		}
		
		return quizCloudFrontUrlInfo;
	
	}
	@RequestMapping(value = "/UpdateStartQuizContest", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo updateStartQuizContest(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizContestDetails =new QuizContestInfo();
		Error error = new Error();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizContestDetails.setError(authError);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,authError.getDescription());
				return quizContestDetails;
			}
			
			String contestIdStr = request.getParameter("contestId");
			String platForm = request.getParameter("platForm");
			String deviceType = request.getParameter("deviceType");
			//String productTypeStr = request.getParameter("productType");
			/*if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Application Platform is mandatory");
				return quizContestDetails;
			}
			if(TextUtil.isEmptyOrNull(deviceType)){
				error.setDescription("Device Type is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Device Type is mandatory");
				return quizContestDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					quizContestDetails.setError(error);
					quizContestDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"Please send valid application platform");
					return quizContestDetails;
				}
			}*/
			
			/*Property property = DAORegistry.getPropertyDAO().get("quiz.video.source.url");
			if(property == null || property.getValue() == null) {
				error.setDescription("Quiz Source Video Url is not yet declared.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"Quiz Source Video Url is not yet declared.");
				return quizContestDetails;
			}*/
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Contest Id is mandatory");
				return quizContestDetails;
			}
			QuizConfigSettings quizConfigSettings = QuizDAORegistry.getQuizConfigSettingsDAO().getConfigSettings();
			if(quizConfigSettings == null) {
				error.setDescription("Quiz Configuration Settings not yet declared.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Quiz Configuration Settings not yet declared.");
				return quizContestDetails;
			}
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			if(contest == null) {
				error.setDescription("Quiz Contest Id is Invalid.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Quiz Contest Id is Invalid.");
				return quizContestDetails;
			}
			
			Boolean flag = CassContestUtil.refreshStartContestData(contest.getContestType());
			if(!flag) {
				error.setDescription("Contest Not Yet Started.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Contest Not Yet Started.");
				return quizContestDetails;
			}
			contest.setProcessStatus(ContestProcessStatus.STARTED);
			
			QuizContestUtil.updateQuizContest(contest);
			
			CassContestUtil.HIDE_DASHBOARD_REWARDS = true;
			QuizContestUtil.refreshNextContestList("MOBILE");
			QuizContestUtil.refreshNextContestList("WEB");
			
			quizContestDetails.setStatus(1);
			quizContestDetails.setMessage("Quiz Contest Started.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Start Contest Information.");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Error occured while Fetching Start Contest Information.");
			return quizContestDetails;
		}
		return quizContestDetails;
	
	}
	@RequestMapping(value = "/UpdateEndQuizContest", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo updateEndQuizContest(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizContestDetails =new QuizContestInfo();
		Error error = new Error();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizContestDetails.setError(authError);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ENDQUIZCONTEST,authError.getDescription());
				return quizContestDetails;
			}*/
			
			String contestIdStr = request.getParameter("contestId");
			String platForm = request.getParameter("platForm");
			String deviceType = request.getParameter("deviceType");
			//String productTypeStr = request.getParameter("productType");
			/*if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Application Platform is mandatory");
				return quizContestDetails;
			}
			if(TextUtil.isEmptyOrNull(deviceType)){
				error.setDescription("Device Type is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STARTQUIZCONTEST,"Device Type is mandatory");
				return quizContestDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					quizContestDetails.setError(error);
					quizContestDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"Please send valid application platform");
					return quizContestDetails;
				}
			}*/
			
			/*Property property = DAORegistry.getPropertyDAO().get("quiz.video.source.url");
			if(property == null || property.getValue() == null) {
				error.setDescription("Quiz Source Video Url is not yet declared.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"Quiz Source Video Url is not yet declared.");
				return quizContestDetails;
			}*/
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ENDQUIZCONTEST,"Contest Id is mandatory");
				return quizContestDetails;
			}
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr.trim()));
			if(contest == null) {
				error.setDescription("Quiz Contest Id is Invalid.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ENDQUIZCONTEST,"Quiz Contest Id is Invalid.");
				return quizContestDetails;
			}
			
			CassContestUtil.refreshEndContestData();
			
			contest.setProcessStatus(ContestProcessStatus.ENDCONTEST);
			QuizContestUtil.updateQuizContest(contest);
			
			QuizContestUtil.refreshNextContestList("MOBILE");
			QuizContestUtil.refreshNextContestList("WEB");
			
			/* Change Hall of fame summary page max data size to 100 - By Ulaganathan on 11/27/2018 PM - Start */
			try {
				Integer hallOfFamePageMaxDataSize = QuizContestUtil.getHallOfFamePageMaxDataSize();
				if(hallOfFamePageMaxDataSize != 100) {
					QuizContestUtil.setHallOfFamePageMaxDataSize(100);
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* Change Hall of fame summary page max data size to 100 - By Ulaganathan on 11/27/2018 PM - Ends */
			
			quizContestDetails.setStatus(1);
			quizContestDetails.setMessage("Quiz Contest Ended.");
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.ENDQUIZCONTEST,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Start Contest Information.");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.ENDQUIZCONTEST,"Error occured while Fetching Start Contest Information.");
			return quizContestDetails;
		}
		return quizContestDetails;
	}
	@RequestMapping(value = "/GetQuizContestInfo", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo getContestInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizContestDetails =new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("customerId");
		String platForm = request.getParameter("platForm");
		String deviceType = request.getParameter("deviceType");
		try {
			
			if(null != platForm && !platForm.isEmpty() && platForm.equals("DESKTOP_SITE")) {
				
			}else {
				error.setDescription("Please update to our latest build to experience our new promotions.!");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"UPDATE THE BUILD");
				return quizContestDetails;
			}
			
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				quizContestDetails.setError(authError);
				quizContestDetails.setStatus(0);
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}*/
			
			
			//String productTypeStr = request.getParameter("productType");
			/*if(TextUtil.isEmptyOrNull(platForm)){
				resMsg = "Application Platform is mandatory";
				error.setDescription("Application Platform is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}
			if(TextUtil.isEmptyOrNull(deviceType)){
				resMsg = "Device Type is mandatory";
				error.setDescription("Device Type is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}*/
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					resMsg = "Please send valid application platform";
					error.setDescription("Please send valid application platform");
					quizContestDetails.setError(error);
					quizContestDetails.setStatus(0);
					TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
					return quizContestDetails;
				}
			}
			String contestType="MOBILE";
			if(!applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				//Show Mobile Contest Now on Desktop
				contestType="WEB";
			}
			
			
			//Please update to our latest build to experience our new promotions


			
			Integer customerId = null;
			Customer customer = null;
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				 
				try {
					customerId = Integer.parseInt(customerIdStr.trim());
					customer = CustomerUtil.getCustomerById(customerId);
					//customer = DAORegistry.getCustomerDAO().get(customerId);
					if(customer == null) {
						resMsg = "Customer Id is Invalid";
						error.setDescription("Customer Id is Invalid");
						quizContestDetails.setError(error);
						quizContestDetails.setStatus(0);
						TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
						return quizContestDetails;
					}
				} catch(Exception e) {
					resMsg = "Customer Id Not Exist";
					error.setDescription("Customer Id Not Exist");
					quizContestDetails.setError(error);
					quizContestDetails.setStatus(0);
					TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
					return quizContestDetails;
				}
			}
			
			/*Property property = DAORegistry.getPropertyDAO().get("quiz.video.source.url");
			if(property == null || property.getValue() == null) {
				error.setDescription("Quiz Source Video Url is not yet declared.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"Quiz Source Video Url is not yet declared.");
				return quizContestDetails;
			}*/
			//QuizConfigSettings quizConfigSettings = QuizDAORegistry.getQuizConfigSettingsDAO().getConfigSettings();
			QuizConfigSettings quizConfigSettings = QuizConfigSettingsUtil.getConfigSettings();
			if(quizConfigSettings == null) {
				resMsg = "Quiz Configuration Settings not yet declared.";
				error.setDescription("Quiz Configuration Settings not yet declared.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}
			quizContestDetails.setVideoSourceUrl(quizConfigSettings.getVideoSourceUrl());
			//quizContestDetails.setAppSyncUrl(quizConfigSettings.getAppSyncUrl());
			//quizContestDetails.setAppSyncToken(quizConfigSettings.getAppSyncToken());
			//quizContestDetails.setStartingCountdownVideoUrl(properties.getRtfQuizVideo()+""+quizConfigSettings.getStartingCountdownVideoUrl());
			
			//quizContestDetails.setSourceId(quizConfigSettings.getSourceId());
			//quizContestDetails.setPartnerId(quizConfigSettings.getPartnerId());
			//quizContestDetails.setEntryId(quizConfigSettings.getEntryId());
			//quizContestDetails.setLiveStreamUrl(quizConfigSettings.getLiveStreamUrl());
			//quizContestDetails.setUiConfId(quizConfigSettings.getUiConfId());
			
			//if(applicationPlatForm.equals(ApplicationPlatForm.IOS)){
				//quizContestDetails.setJwPlayerLicenceKey(quizConfigSettings.getJwPlayerLicenceKeyIOS());
			//} else if (applicationPlatForm.equals(ApplicationPlatForm.ANDROID)) {
				//quizContestDetails.setJwPlayerLicenceKey(quizConfigSettings.getJwPlayerLicenceKeyAndroid());
			//}
			
			Integer pendingRequestCount = null;
			if(customer != null) {
				//2018-12-16 Added as per amit request to consider all dshboard users to be otp verified users.
				customer.setIsOtpVerified(Boolean.TRUE);
				
				quizContestDetails.setCustomer(customer);
				if(customer.getCustImagePath() != null) {
					quizContestDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
				} else {
					quizContestDetails.setCustomerProfilePicWebView(null);
				}
				//Need to Fix it				
				
				/*CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
				//customer.setQuizNoOfPointsWon(customerLoyalty.getActivePointsAsDouble());
				if(customerLoyalty != null) {
					quizContestDetails.setTotalActiveRewards(customerLoyalty.getActivePointsAsDouble());
				}*/
				
				//pendingRequestCount = DAORegistry.getQueryManagerDAO().getAllPendingFriendsRequestCountByCustomerId(customer.getId());
				pendingRequestCount = 0;
			}
			if(pendingRequestCount != null) {
				quizContestDetails.setPendingRequests(pendingRequestCount);
			} else {
				quizContestDetails.setPendingRequests(0);
			}
			
			Date todayDate = dbDateTimeFormat.parse(dbDateFormat.format(new Date())+" 00:00");
			QuizContest quizContest = null;//QuizDAORegistry.getQuizContestDAO().getNextQuizContest(todayDate,contestType);
			//List<QuizContest> contestList = QuizDAORegistry.getQuizContestDAO().getUpCommingQuizContests(todayDate, contestType);
			List<QuizContest> contestList = QuizContestUtil.getNextContestList(contestType);
			if(contestList != null && contestList.size() > 0) {
				quizContest = contestList.get(0);
				for (QuizContest quizContestObj : contestList) {
					if(quizContestObj.getStatus().equals("STARTED")) {
						quizContest = quizContestObj;
					}
				}
			}
			
			String labelMsg = "PLAY GAME";
			Boolean showJoinButton = false;
			/*if(!deviceType.equals("IPAD") && !deviceType.equals("TABLET")) {
				labelMsg = "";
				if(customer == null) {
					labelMsg = "PLAY GAME";
					showJoinButton = true;
				} else if(!customer.getIsOtpVerified()) {
					labelMsg = "PLAY GAME";
					showJoinButton = true;
				} else if (quizContest != null && quizContest.getStatus().equals("STARTED")) {
					labelMsg = "JOIN";
					showJoinButton = true;
				}
			}*/
			quizContestDetails.setJoinButtonLabel(labelMsg);
			quizContestDetails.setShowJoinButton(showJoinButton);
			
			
			Integer contestPromoCodesCount = 0;//QuizDAORegistry.getQuizQueryManagerDAO().getAllValidContestPromoCodesCount();
			quizContestDetails.setcPromoCodesCount(contestPromoCodesCount);
			
			if(quizContest == null) {
				resMsg = "Next Contest Not Yet Declared.";
				quizContestDetails.setJoinButtonPopupMsg("Next Contest will be Announced Shortly.");
				quizContestDetails.setGrandPriceText("Next Contest will be Announced Shortly.");
				quizContestDetails.setContestStartTime("TBD");
				//error.setDescription("Next Contest Not Yet Declared.");
				//quizContestDetails.setError(error);
				quizContestDetails.setStatus(1);
				quizContestDetails.setMessage("Next Contest will be Announced Shortly.");
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}
			SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");
			//timeFt.setTimeZone(TimeZone.getTimeZone("UTC"));
			//quizContestDetails.setNextContestStartDateTime(quizContest.getContestStartDateTime());
			quizContestDetails.setContestStartDate(quizContest.getContestStartDate());
			quizContestDetails.setContestStartTime(quizContest.getContestStartTime());
			quizContestDetails.setContestName(quizContest.getContestName());
			quizContestDetails.setContestId(quizContest.getId());
			quizContestDetails.setMaxFreeTixWinners(quizContest.getMaxFreeTicketWinners());
			//quizContestDetails.setGrandPriceText(quizContest.getFreeTicketsPerWinner()+" Free Tickets "+quizContest.getContestName());
			quizContestDetails.setGrandPriceText(quizContest.getContestName());
			
			String grandWinerText = "";
			if(quizContest.getPromoExpiryDate() != null) {
				String hours = QuizContestUtil.computehoursBetweenDates(new Date(), quizContest.getPromoExpiryDate());
				grandWinerText = "Expires in " + hours + " hours";
			}
			quizContestDetails.setGrandWinerExpiryText(grandWinerText);
			//quizContestDetails.setContestPriceText("4 Free tickets: Hamilton ");

//			//quizContestDetails.setNoOfQuestions(quizContest.getNoOfQuestions());
			
			quizContestDetails.setStatus(1);
			quizContestDetails.setContestTotalRewards(quizContest.getTotalRewards());
			quizContestDetails.setFreeTicketsPerWinner(quizContest.getFreeTicketsPerWinner());
			quizContestDetails.setContestList(contestList);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			
			String joinButtomMsg = "Next Contest will start ";
			if(quizContestDetails.getContestStartTime() != null) { 
					if(quizContestDetails.getContestStartTime().contains("Today")) {
						joinButtomMsg = joinButtomMsg + quizContestDetails.getContestStartTime().replace("Today", "Today at");
					} else if(quizContestDetails.getContestStartTime().contains("Tomorrow")) {
						joinButtomMsg = joinButtomMsg + quizContestDetails.getContestStartTime().replace("Tomorrow", "Tomorrow at");
					} else {
						joinButtomMsg = joinButtomMsg + "at "+quizContestDetails.getContestStartTime();
					}
			}
			quizContestDetails.setJoinButtonPopupMsg(joinButtomMsg);
			//quizContestDetails.setJoinButtonPopupMsg("Next Contest will start at "+quizContestDetails.getContestStartDate()+" "+quizContestDetails.getContestStartTime());
			
			if(customer != null) {
				CustomerLoyalty customerLoyalty  = null;
				if(quizContest != null && quizContest.getStatus().equals("STARTED")) {
					/*Double contesTAnserRewards = QuizContestUtil.getCustomerContestAnswerRewards(quizContest.getId(), customer.getId());
					quizContestDetails.setContestAnswerRewards(contesTAnserRewards);*/
					
					boolean isExistingContestant = QuizContestUtil.isExistingContestant(quizContest.getId(), customer.getId());
					//boolean isExistingContestant = QuizDAORegistry.getQuizContestParticipantsDAO().isExistingContestant(customer.getId(), quizContest.getId());
					quizContestDetails.setIsExistingContestant(isExistingContestant);
					
					customerLoyalty = QuizContestUtil.getCustomerLoyalty(customer.getId());
				} else {
					customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
				}
				
				//customer.setQuizNoOfPointsWon(customerLoyalty.getActivePointsAsDouble());
				if(customerLoyalty != null) {
					quizContestDetails.setTotalActiveRewards(customerLoyalty.getActivePointsAsDouble());
				}
			}
			if(quizContest.getStatus().equals("STARTED")) {
				quizContestDetails.setIsContestStarted(Boolean.TRUE);
				
				/* Change Hall of fame summary page max data size to 5 - By Ulaganathan on 11/27/2018 PM - Start */
				try {
					
					Integer hallOfFamePageMaxDataSize = QuizContestUtil.getHallOfFamePageMaxDataSize();
					if(hallOfFamePageMaxDataSize != 5) {
						QuizContestUtil.setHallOfFamePageMaxDataSize(5);
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				/* Change Hall of fame summary page max data size to 5 - By Ulaganathan on 11/27/2018 PM - Ends */
				
			} else {
				quizContestDetails.setIsContestStarted(Boolean.FALSE);
			}
			
			resMsg = "Success";
			if(customer != null) {
				resMsg += " :otp:"+customer.getIsOtpVerified()+":"+customer.getId();
			}
			//TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Contest Information";
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Contest Information.");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
			return quizContestDetails;
		} finally {
			log.info("GETQUIZ INFO IOS: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		
		return quizContestDetails;
	
	}
	
	 
	@RequestMapping(value = "/FirebaseCallBackTracking", method=RequestMethod.POST)
	public @ResponsePayload QuizGenericResponse firebaseCallBackTracking(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizGenericResponse quizGenericResponseInfo =new QuizGenericResponse();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizGenericResponseInfo.setError(authError);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,authError.getDescription());
				return quizGenericResponseInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			String loginIp = request.getParameter("loginIp");
			String questionNoStr = request.getParameter("questionNo");
			String osVersionStr = request.getParameter("osVersion");
			String deviceModelStr = request.getParameter("deviceModel");
			String callBackType = request.getParameter("type");
			String message = request.getParameter("message");
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Application Platform is mandatory");
				return quizGenericResponseInfo;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					quizGenericResponseInfo.setError(error);
					quizGenericResponseInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Please send valid application platform");
					return quizGenericResponseInfo;
				}
			}
			
			/*if(TextUtil.isEmptyOrNull(questionNoStr)){
				error.setDescription("Question No is mandatory");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Question No is mandatory");
				return quizGenericResponseInfo;
			}*/
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Customer Id is mandatory");
				return quizGenericResponseInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Contest Id is mandatory");
				return quizGenericResponseInfo;
			}
			/*if(TextUtil.isEmptyOrNull(callBackType)){
				error.setDescription("Type is mandatory");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Type is mandatory");
				return quizGenericResponseInfo;
			}*/
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			Integer customerId = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Customer Id");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Invalid Customer Id");
				return quizGenericResponseInfo;
			}
			
			/*Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Customer Id is Invalid");
				return quizGenericResponseInfo;
			}*/
			Integer contestId = null;
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Contest Id");
				quizGenericResponseInfo.setError(error);
				quizGenericResponseInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Invalid Contest Id");
				return quizGenericResponseInfo;
			}

			QuizFirebaseCallBackTracking firebaseTracking = new QuizFirebaseCallBackTracking();
			firebaseTracking.setCustomerId(customerId);
			firebaseTracking.setContestId(contestId);
			if(questionNoStr != null && !questionNoStr.isEmpty()) {
				firebaseTracking.setQuestionNo(Integer.parseInt(questionNoStr));				
			}
			firebaseTracking.setIpAddress(loginIp);
			firebaseTracking.setDeviceModel(deviceModelStr);
			firebaseTracking.setOsVersion(osVersionStr);
			firebaseTracking.setPlatform(platForm);
			firebaseTracking.setCreateDate(new Date());
			firebaseTracking.setCallBackType(callBackType);
			firebaseTracking.setMessage(message);
			QuizDAORegistry.getQuizFirebaseCallBackTrackingDAO().save(firebaseTracking);
			
			quizGenericResponseInfo.setStatus(1);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Firebase Tracking.");
			quizGenericResponseInfo.setError(error);
			quizGenericResponseInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.FIREBASECALLBACKTRACKING,"Error occured while Updating Firebase Tracking.");
			return quizGenericResponseInfo;
		}
		//log.info("QUIZ FIRE TRACK : "+request.getParameter("customerId")+" : coId: "+quizGenericResponseInfo.getContestId()+" : coId: "+quizGenericResponseInfo.getContestId()+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+quizGenericResponseInfo);
		
		return quizGenericResponseInfo;
	}
	@RequestMapping(value = "/QuizJoinContest", method=RequestMethod.POST)
	public @ResponsePayload QuizJoinContestInfo quizJoinContest(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizJoinContestInfo quizJoinContestInfo =new QuizJoinContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizJoinContestInfo.setError(authError);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,authError.getDescription());
				return quizJoinContestInfo;
			}*/
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			String loginIp = request.getParameter("loginIp");
			
			
			
			if(null != platForm && !platForm.isEmpty() && platForm.equals("DESKTOP_SITE")) {
				
			}else {
				error.setDescription("Please update to our latest build to experience our new promotions.!");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"UPDATE THE BUILD");
				return quizJoinContestInfo;
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Application Platform is mandatory");
				return quizJoinContestInfo;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					quizJoinContestInfo.setError(error);
					quizJoinContestInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Please send valid application platform");
					return quizJoinContestInfo;
				}
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Customer Id is mandatory");
				return quizJoinContestInfo;
			}
			String contestType="MOBILE";
			if(!applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				contestType="WEB";
			}
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			Integer customerId = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Customer Id");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Invalid Customer Id");
				return quizJoinContestInfo;
			}
			
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Customer Id is Invalid");
				return quizJoinContestInfo;
			}
			
			if(customer.getPhone() == null || !customer.getIsOtpVerified()) {
				error.setDescription("Verify your Phone with OTP to Join Contest.");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Verify your Phone with OTP to Join Contest.");
				return quizJoinContestInfo;
			}
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().getCurrentStartedContest(contestType);
			if(contest == null) {
				error.setDescription("contest is Not Yet Started.");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"contest is Not Yet Started.");
				return quizJoinContestInfo;
			}
			quizJoinContestInfo.setContestId(contest.getId());

			if(contest != null && contest.getStatus().equals("STARTED")) {
				quizJoinContestInfo = QuizContestUtil.updateQuizJoinContestCustomersCount(quizJoinContestInfo,contest.getId(), customerId);
				
				if(contest.getPromoCode() != null && contest.getDiscountPercentage() != null) {
					quizJoinContestInfo.setEpShowPromoCode(true);
					quizJoinContestInfo.setEpPromoCode(contest.getPromoCode());
					quizJoinContestInfo.setEpPromoRefId(contest.getPromoRefId());
					quizJoinContestInfo.setEpPromoRefName(contest.getPromoRefName());
					String discText = "GET "+percentageDF.format(contest.getDiscountPercentage())+"% OFF";
					quizJoinContestInfo.setEpDiscText(discText);
					if(contest.getPromoRefType() != null && contest.getPromoRefType().equals("ALL")) {
						quizJoinContestInfo.setEpPromoRefType("PARENT");
					} else {
						quizJoinContestInfo.setEpPromoRefType(contest.getPromoRefType());
					}
					QuizDAORegistry.getQuizContestParticipantsDAO().updateExistingQuizContestParticipantsByJoinContest(customer.getId(), contest.getId());
					
					QuizContestParticipants participant = new QuizContestParticipants();
					participant.setContestId(contest.getId());
					participant.setCustomerId(customerId);
					participant.setPlatform(applicationPlatForm.toString());
					participant.setIpAddress(loginIp);
					participant.setJoinDateTime(new Date());
					participant.setStatus("ACTIVE");
					QuizDAORegistry.getQuizContestParticipantsDAO().save(participant);
				}
				
				
				//if(contest.getStatus().equals("STARTED")) {
//					Double contesTAnserRewards = QuizContestUtil.getCustomerContestAnswerRewards(contest.getId(), customer.getId());
//					quizJoinContestInfo.setContestAnswerRewards(contesTAnserRewards);
					/*if(contesTAnserRewards != null) {
						quizJoinContestInfo.setContestAnswerRewards(TicketUtil.getRoundedValueString(contesTAnserRewards));
					}*/
				//}
					log.info("JOIN : "+customerIdStr+":"+contestIdStr+":"+quizJoinContestInfo.getLastAnsweredQuestionNo()+":"+quizJoinContestInfo.getContestAnswerRewards()+":"+quizJoinContestInfo.getIsExistingContestant()+":"+new Date());
			}
			
			quizJoinContestInfo.setStatus(1);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Join contest Details.");
			quizJoinContestInfo.setError(error);
			quizJoinContestInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZJOINCONTEST,"Error occured while Fetching Join contest Details.");
			return quizJoinContestInfo;
		}
		log.info("QUIZ JOIN : "+request.getParameter("customerId")+" : coId: "+quizJoinContestInfo.getContestId()+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+quizJoinContestInfo);
		
		return quizJoinContestInfo;
	}
	
	@RequestMapping(value = "/QuizExitContest", method=RequestMethod.POST)
	public @ResponsePayload QuizJoinContestInfo quizExitContest(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizJoinContestInfo quizJoinContestInfo =new QuizJoinContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			boolean disable = true;
			if(disable) { 
				error.setDescription("Please update to our latest build to experience our new promotions.!");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"UPDATE THE BUILD");
				return quizJoinContestInfo;
			}

			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizJoinContestInfo.setError(authError);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,authError.getDescription());
				return quizJoinContestInfo;
			}*/
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			//String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			/*if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Application Platform is mandatory");
				return quizJoinContestInfo;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					quizJoinContestInfo.setError(error);
					quizJoinContestInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Please send valid application platform");
					return quizJoinContestInfo;
				}
			}*/
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Customer Id is mandatory");
				return quizJoinContestInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Contest Id is mandatory");
				return quizJoinContestInfo;
			}
			Integer customerId = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Customer Id");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Invalid Customer Id");
				return quizJoinContestInfo;
			}
			Integer contestId = null;
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Contest Id");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Invalid Contest Id");
				return quizJoinContestInfo;
			}

			quizJoinContestInfo = QuizContestUtil.updateQuizExitCustomersCount(quizJoinContestInfo,contestId, customerId);
			quizJoinContestInfo.setStatus(1);
			try {
				QuizDAORegistry.getQuizContestParticipantsDAO().updateQuizContestParticipantsExistContest(customerId, contestId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Exit contest Details.");
			quizJoinContestInfo.setError(error);
			quizJoinContestInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZEXITCONTEST,"Error occured while Fetching Exit contest Details.");
			return quizJoinContestInfo;
		}
		log.info("QUIZ EXIT : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return quizJoinContestInfo;
	}
	@RequestMapping(value = "/GetQuizContestCustomersCount", method=RequestMethod.POST)
	public @ResponsePayload QuizJoinContestInfo getQuizContestCustomersCount(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizJoinContestInfo quizJoinContestInfo =new QuizJoinContestInfo();
		Error error = new Error();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizJoinContestInfo.setError(authError);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,authError.getDescription());
				return quizJoinContestInfo;
			}*/
			
			String contestIdStr = request.getParameter("contestId");
			
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,"Contest Id is mandatory");
				return quizJoinContestInfo;
			}
			
			QuizContest quizContest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,"Contest Id is Invalid");
				return quizJoinContestInfo;
			}
			if(quizContest.getStatus()== null || !quizContest.getStatus().equals("STARTED")) {
				error.setDescription("Contest Not Yet Started");
				quizJoinContestInfo.setError(error);
				quizJoinContestInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,"Contest Not Yet Started");
				return quizJoinContestInfo;
			}

			Integer totalCustomersCount = QuizContestUtil.getContestCustomersCount(quizContest.getId());
			quizJoinContestInfo.setTotalUsersCount(totalCustomersCount);
			quizJoinContestInfo.setStatus(1);
			
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,"Success: "+totalCustomersCount);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Contest customers count.");
			quizJoinContestInfo.setError(error);
			quizJoinContestInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTCUSTOMERCOUNT,"Error occured while Fetching Contest customers count");
			return quizJoinContestInfo;
		}
		
		return quizJoinContestInfo;
	}
	@RequestMapping(value = "/UpdateCustomerReferralCode", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerReferralCodeInfo updateCustomerReferralCode(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerReferralCodeInfo quizReferalInfo =new QuizCustomerReferralCodeInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizReferalInfo.setError(authError);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,authError.getDescription());
				return quizReferalInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(referralcodeStr)){
				error.setDescription("Referral Code is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Referral Code is mandatory");
				return quizReferalInfo;
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Customer Id is mandatory");
				return quizReferalInfo;
			}
			
			Boolean isRtfReferalCode = false;
			RtfPromotionalEarnLifeOffers rtfReferral = null;
			Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
			if(referralCustomer == null) {
				
				rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getActiveRtfPromotionEarnLifeOfferByPromoCode(referralcodeStr);
				Date today = new Date();
				if(rtfReferral != null && rtfReferral.getStartDate().compareTo(today)<=0 && rtfReferral.getEndDate().compareTo(today)>=0 ) {
					isRtfReferalCode = true;
				} else {
					error.setDescription("Referral Code is Invalid");
					quizReferalInfo.setError(error);
					quizReferalInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Referral Code is Invalid");
					return quizReferalInfo;
				}
			}
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Customer Id is Invalid");
				return quizReferalInfo;
			}
			if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
				error.setDescription("You can't use your own referral code.");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"You can't use your own referral code.");
				return quizReferalInfo;
			}
		
			QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(Integer.parseInt(customerIdStr));
			if(existReferralTracking != null) {
				error.setDescription("You have already Used Referral Code");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"You have already Used Referral Code");
				return quizReferalInfo;
			}
			/*QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerIdandReferralCode(customer.getId(), referralcodeStr);
			if(existReferralTracking != null) {
				error.setDescription("Customer already updated with This Referral Code");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Customer already updated with This Referral Code");
				return quizReferalInfo;
			}*/
			
			if(isRtfReferalCode) {
				QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
				referraltracking.setCustomerId(customer.getId());
				referraltracking.setReferralCode(referralcodeStr);
				//referraltracking.setReferralCustomerId();
				referraltracking.setUpdatedDateTime(new Date());
				referraltracking.setIsAffiliateReferral(false);
				referraltracking.setRtfPromotionalId(rtfReferral.getId());
				referraltracking.setCreatedDateTime(new Date());
				QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
				
				Integer customerLives = customer.getQuizCustomerLives();
				if(customerLives == null) {
					customerLives=0;
				}
				customerLives = customerLives+rtfReferral.getMaxLifePerCustomer();
				customer.setQuizCustomerLives(customerLives);	
				
				CustomerUtil.updatedCustomerUtil(customer);
				DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
				
				/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
				try {
					CassCustomerUtil.updateCustomerLives(customer);
				}catch(Exception e) {
					e.printStackTrace();
				}
				/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
				
				//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(customer);
			} else {

				QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
				
				Integer customerLives = referralCustomer.getQuizCustomerLives();
				if(customerLives == null) {
					customerLives=0;
				}
				
				Integer tireOneCustLives = customer.getQuizCustomerLives();
				if(tireOneCustLives == null) {
					tireOneCustLives=0;
				}
				
				boolean showPrimaryCustNotification = true,showTireOneCustNotification = true;;
				
				QuizAffiliateSetting quizAffiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getActiveAffiliateByCustomerId(referralCustomer.getId());
				
				if(null != quizAffiliateSetting) {
					
					referraltracking.setIsAffiliateReferral(true);
					referraltracking.setAffiliateReferralStatus("ACTIVE");
					
					if(null != quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() && quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() > 0) {
						referraltracking.setLivesToPrimaryCustomer(true);
						customerLives = customerLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
					}else {
						referraltracking.setLivesToPrimaryCustomer(false);
						customerLives = customerLives + 0;
						showPrimaryCustNotification = false;
					}
					
					if(null != quizAffiliateSetting.getNoOfLivesToTireOneCustomer() && quizAffiliateSetting.getNoOfLivesToTireOneCustomer() > 0) {
						referraltracking.setLivesToTireOneCustomer(true);
						tireOneCustLives = tireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
					}else {
						referraltracking.setLivesToTireOneCustomer(false);
						tireOneCustLives = tireOneCustLives + 0;
						showTireOneCustNotification = false;
					}
					
				}else {
					referraltracking.setIsAffiliateReferral(false); 
					referraltracking.setLivesToPrimaryCustomer(true);
					referraltracking.setLivesToTireOneCustomer(true);
					
					customerLives++;
					tireOneCustLives++;
				}
				referraltracking.setYearEndRewardApplied(false);
				referraltracking.setCustomerId(customer.getId());
				referraltracking.setReferralCode(referralcodeStr);
				referraltracking.setReferralCustomerId(referralCustomer.getId());
				referraltracking.setCreatedDateTime(new Date());
				referraltracking.setUpdatedDateTime(new Date());
				QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
				
				if(showPrimaryCustNotification) {
					referralCustomer.setQuizCustomerLives(customerLives);
					DAORegistry.getCustomerDAO().updateQuizCustomerLives(referralCustomer.getId(),customerLives);
					CustomerUtil.updatedCustomerUtil(referralCustomer);
					
					QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
					
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
					try {
						CassCustomerUtil.updateCustomerLives(referralCustomer);
					}catch(Exception e) {
						e.printStackTrace();
					}
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
				}
				
				if(showTireOneCustNotification) {
					customer.setQuizCustomerLives(tireOneCustLives);	
					DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),tireOneCustLives);
					CustomerUtil.updatedCustomerUtil(customer);
					QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
					
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
					try {
						CassCustomerUtil.updateCustomerLives(customer);
					}catch(Exception e) {
						e.printStackTrace();
					}
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
				}
			}
			
			
			
			/*Integer customerLives = customer.getQuizCustomerLives();
			if(customerLives == null) {
				customerLives=0;
			}
			customerLives++;
			customer.setQuizCustomerLives(customerLives);	
			
			CustomerUtil.updatedCustomerUtil(customer);
			DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);*/
			
			quizReferalInfo.setQuizCustomerLives(customer.getQuizCustomerLives());
			quizReferalInfo.setStatus(1);
			quizReferalInfo.setMessage("Referal Code Updated and Customer Lives Added Successfully.");
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Referral Code.");
			quizReferalInfo.setError(error);
			quizReferalInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Error occured while Updating Referral Code.");
			return quizReferalInfo;
		}
		log.info("QUIZ UPDT REFCODE : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizReferalInfo;
	
	}
	@RequestMapping(value = "/GetQuizQuestionInfo", method=RequestMethod.POST)
	public @ResponsePayload QuizQuestionInfo getQuestionInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		log.info("Inside Get Question s 1 : "+ new Date());
		QuizQuestionInfo quizQuestionInfo =new QuizQuestionInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizQuestionInfo.setError(authError);
				quizQuestionInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,authError.getDescription());
				return quizQuestionInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String questionIdStr = request.getParameter("questionNo");
			
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizQuestionInfo.setError(error);
				quizQuestionInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Contest Id is mandatory");
				return quizQuestionInfo;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				error.setDescription("Question NO is mandatory");
				quizQuestionInfo.setError(error);
				quizQuestionInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Question NO is mandatory");
				return quizQuestionInfo;
			}
			QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizQuestionInfo.setError(error);
				quizQuestionInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Contest Id is Invalid");
				return quizQuestionInfo;
			}
			QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionByContestIdandQuestionSlNo(quizContest.getId(), Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				error.setDescription("Contest Id or Question NO is Invalid");
				quizQuestionInfo.setError(error);
				quizQuestionInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Contest Id or Question NO is Invalid");
				return quizQuestionInfo;
			}
			quizQuestionInfo.setNoOfQuestions(quizContest.getNoOfQuestions());
			
			QuizContestQuestions previousquestion = QuizContestUtil.getContestCurrentQuestions(quizContest.getId());
			if((previousquestion == null && quizQuestion.getQuestionSNo() != 1) ||
					(previousquestion != null && (previousquestion.getQuestionSNo() + 1) != quizQuestion.getQuestionSNo())) {
				error.setDescription("Question NO is Mismatch with Previous Question.");
				quizQuestionInfo.setError(error);
				quizQuestionInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Question NO is Mismatch with Previous Question.");
				return quizQuestionInfo;
			}
			QuizContestUtil.updateContestQuestions(quizQuestion);
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Customer Id is mandatory");
				return quizContestDetails;
			}*/
			
			/*QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerById(Integer.parseInt(customerIdStr));
			if(quizCustomer == null){
				error.setDescription("Customer not valid");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GQUIZQUESTIONINFO,"Customer not valid");
				return quizContestDetails;
			}*/
			
			quizQuestionInfo.setQuizContestQuestion(quizQuestion);
			quizQuestionInfo.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Question Info.");
			quizQuestionInfo.setError(error);
			quizQuestionInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZQUESTIONINFO,"Error occured while Fetching Question Info.");
			return quizQuestionInfo;
		}
		log.info("QUIZ QUEST INFO : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizQuestionInfo;
	
	}
	
	@RequestMapping(value = "/ValidateQuizAnswer", method=RequestMethod.POST)
	public @ResponsePayload QuizValidateAnswerInfo validateQuizAnswers(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizValidateAnswerInfo quizAnswerInfo =new QuizValidateAnswerInfo();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("customerId");
		String contestIdStr = request.getParameter("contestId");
		String questionNoStr = request.getParameter("questionNo");
		String questionIdStr = request.getParameter("questionId");
		//String isContestant = request.getParameter("contestant");
		String answerOption = request.getParameter("answer");
		String isContestant = "Y";
		try {
			
			boolean disable = true;
			if(disable) { 
				error.setDescription("Please update to our latest build to experience our new promotions.!");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,"UPDATE THE BUILD",
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
				 
			}
			
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				quizAnswerInfo.setError(authError);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg);
				return quizAnswerInfo;
			}*/
			  
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				resMsg = "CustomerId is mandatory";
				error.setDescription("CustomerId is mandatory");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg);
				return quizAnswerInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				resMsg = "Contest Id is mandatory";
				error.setDescription("Contest Id is mandatory");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg);
				return quizAnswerInfo;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				resMsg = "Question Id is mandatory";
				error.setDescription("Question Id is mandatory");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg);
				return quizAnswerInfo;
			}*/
			/*String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String questionNoStr = request.getParameter("questionNo");
			String questionIdStr = request.getParameter("questionId");
			String isContestant = request.getParameter("contestant");
			String answerOption = request.getParameter("answer");*/
			
			/*if(isContestant == null || isContestant.isEmpty()){
				resMsg = "Contestant is mandatory";
				error.setDescription("Contestant is mandatory");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				 
				return quizAnswerInfo;
			}
			if(isContestant.equals("Y")) {
				if(answerOption == null || answerOption.isEmpty()){
					resMsg = "Answer is mandatory";
					error.setDescription("Answer is mandatory");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}
			}*/
			/*if(TextUtil.isEmptyOrNull(questionNoStr)){
				resMsg = "Question NO is mandatory";
				error.setDescription("Question NO is mandatory");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg);
				return quizAnswerInfo;
			}*/
			Integer customerId = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Customer Id";
				error.setDescription("Invalid Customer Id");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Integer contestId = null;
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Contest Id";
				error.setDescription("Invalid Contest Id");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Integer questionId = null;
			try{
				questionId = Integer.parseInt(questionIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question Id";
				error.setDescription("Invalid Question Id");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Integer questionNo = null;
			try{
				questionNo = Integer.parseInt(questionNoStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question No";
				error.setDescription("Invalid Question No");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(contestId);
			if(contest == null){
				resMsg = "Contest Id is Invalid";
				error.setDescription("Contest Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer == null){
				resMsg = "Customer Id is Invalid";
				error.setDescription("Custoemr Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			
			/*QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				error.setDescription("Question Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,"Question Id is Invalid");
				return quizAnswerInfo;
			}*/
			//Fix	
			//////
			QuizContestQuestions quizQuestion = QuizContestUtil.getContestCurrentQuestions(contest.getId());
			if(quizQuestion == null || !quizQuestion.getId().equals(questionId)) {
				resMsg = "Question Id is Invalid";
				error.setDescription("Question Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			if(!quizQuestion.getQuestionSNo().equals(questionNo)) {//|| !quizQuestion.getContestId().equals(contest.getId()
				resMsg = "Question No or Id is Invalid";
				error.setDescription("Question No or Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			///////
			if(quizQuestion.getIsAnswerCountComputed()) {
				resMsg = "Answer Time Limit was Reached.";
				error.setDescription("Answer Time Limit was Reached.");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			
			QuizCustomerContestAnswers lastAnswer = null;
			//comment for production performance			
			if(quizQuestion.getQuestionSNo() > 1) {
				lastAnswer = QuizContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				if(lastAnswer == null || questionNo != (lastAnswer.getQuestionSNo()+1)) {
					resMsg = "You didn't answer last question.";
					error.setDescription("You didn't answer last question.");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}else if(null != lastAnswer) {
					
					if((null != lastAnswer.getIsCorrectAnswer() && lastAnswer.getIsCorrectAnswer()) || (null != lastAnswer.getIsLifeLineUsed()
							&& lastAnswer.getIsLifeLineUsed())) {
						
					}else {
						resMsg = "You didn't answer last question.";
						error.setDescription("You didn't answer last question.");
						quizAnswerInfo.setError(error);
						quizAnswerInfo.setStatus(0);
						TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
								questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
						return quizAnswerInfo;
					}
					
				}
			} else {
				lastAnswer = QuizContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				if(lastAnswer != null) {
					resMsg = "You have already answered for this question.";
					error.setDescription("You have already answered for this question.");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}
			}
			if(isContestant.equals("Y")) {
				Double cumulativeRewards = 0.0;
				Integer cumulativeLifeLineUsed = 0;
				if(lastAnswer != null) {
					if(lastAnswer.getCumulativeRewards() != null) {
						cumulativeRewards = lastAnswer.getCumulativeRewards();
					}
					if(lastAnswer.getCumulativeLifeLineUsed() != null) {
						cumulativeLifeLineUsed = lastAnswer.getCumulativeLifeLineUsed();
					}
				}
				
				if(answerOption.equalsIgnoreCase(quizQuestion.getAnswer())) {
					quizAnswerInfo.setIsCorrectAnswer(Boolean.TRUE);
					
					if(contest.getNoOfQuestions().equals(questionNo)) {
						QuizContestUtil.updateContestWinners(contest.getId(), customerId);
					}
				}
				QuizCustomerContestAnswers customerAnswers = new QuizCustomerContestAnswers();
				customerAnswers.setCustomerId(customerId);
				customerAnswers.setContestId(contestId);
				customerAnswers.setQuestionId(questionId);
				customerAnswers.setQuestionSNo(questionNo);
				customerAnswers.setAnswer(answerOption);
				customerAnswers.setIsCorrectAnswer(quizAnswerInfo.getIsCorrectAnswer());
				customerAnswers.setCreatedDateTime(new Date());
				customerAnswers.setIsLifeLineUsed(false);
			
				if(quizAnswerInfo.getIsCorrectAnswer()) {
					customerAnswers.setAnswerRewards(quizQuestion.getQuestionRewards());
					if(quizQuestion.getQuestionRewards() != null) {
						cumulativeRewards = cumulativeRewards + quizQuestion.getQuestionRewards();	
					}
				}
				customerAnswers.setCumulativeRewards(cumulativeRewards);
				customerAnswers.setCumulativeLifeLineUsed(cumulativeLifeLineUsed);
				
				

				/*int isCrtAns = customerAnswers.getIsCorrectAnswer() ? 1 : 0;
				int isLifeApplied = customerAnswers.getIsLifeLineUsed() ? 1 : 0;
				
				String query = "INSERT INTO customer_contest_answers (customer_id,contest_id,question_id,question_sl_no,answer,created_datetime,is_correct_answer,"
						+ "is_lifeline_used,updated_datetime,answer_rewards,cumulative_rewards,cumulative_lifeline_used) "
						+ "VALUES ("+customerAnswers.getCustomerId()+","+customerAnswers.getContestId()+","+customerAnswers.getQuestionId()+","+customerAnswers.getQuestionSNo()+","
						+ "'"+customerAnswers.getAnswer()+"',getdate(),"+isCrtAns+","+isLifeApplied+",getdate()"
						+ ","+customerAnswers.getAnswerRewards()+","+customerAnswers.getCumulativeRewards()+","+customerAnswers.getCumulativeLifeLineUsed()+" )";
				
				//Connection connection = getConnection();
				Connection connection = JDBCConnection.getQuizConnection();  
				
				ResultSet rs= null;
				Statement insertStatement =null;
				try {
					connection.setAutoCommit(false);
					insertStatement = connection.createStatement();
					insertStatement.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
					rs = insertStatement.getGeneratedKeys();
					Integer id =null;
					if(rs.next()){
						id = rs.getInt(1);
					}
					connection.commit();
					//System.out.println("Generated ID : "+id);
					customerAnswers.setId(id);
					 
				} catch (Exception e) {
					e.printStackTrace();
					connection.rollback();
					resMsg = "Exception Occured While Inserting Data into Answer Table.";
					error.setDescription("Exception Occured While Inserting Data into Answer Table.");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}finally{
					if(rs!=null){
						rs.close();
					}
					if(insertStatement!=null){
						insertStatement.close();
					}
				}*/
				
				QuizDAORegistry.getQuizCustomerContestAnswersDAO().save(customerAnswers);
				
				QuizContestUtil.updateCustomerAnswers(customerAnswers);
				
				log.info("Quiz Question validation : QNO:"+questionNoStr+": "+customerIdStr+":"+contestIdStr+":"+answerOption+":"+questionNoStr+":"+customerAnswers.getIsCorrectAnswer()+":"+new Date());
				
			}
			//quizAnswerInfo.setCorrectAnswer(quizQuestion.getAnswer());
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,"Customer Id is mandatory");
				return quizContestDetails;
			}*/
			
			/*QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerById(Integer.parseInt(customerIdStr));
			if(quizCustomer == null){
				error.setDescription("Customer not valid");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,"Customer not valid");
				return quizContestDetails;
			}*/
			
			//quizAnswerInfo.setQuizContestQuestion(quizQuestion);
			quizAnswerInfo.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success";
			TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
					questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Error occured while Fetching Validate Answer Info.";
			error.setDescription("Error occured while Fetching Validate Answer Info.");
			quizAnswerInfo.setError(error);
			quizAnswerInfo.setStatus(0);
			TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
					questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
			return quizAnswerInfo;
		} finally {
			log.info("QUIZ VALID ANSWER IOS: QNO:"+questionNoStr+" : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :ans: "+answerOption+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+quizAnswerInfo);
		}
		
		//log.info("QUIZ VALID ANS : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" :ans: "+request.getParameter("answer")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizAnswerInfo;
	
	}
	
	 
	@RequestMapping(value = "/ApplyCustomerLifeLine", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerLifeLineUsage applyCustomerLifeLine(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerLifeLineUsage lifeLineUsageInfo =new QuizCustomerLifeLineUsage();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				lifeLineUsageInfo.setError(authError);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String questionNoStr = request.getParameter("questionNo");
			String questionIdStr = request.getParameter("questionId");
			//String isContestant = request.getParameter("contestant");
			//String answerOption = request.getParameter("answer");
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				resMsg = "CustomerId is mandatory";
				error.setDescription("CustomerId is mandatory");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				resMsg = "Contest Id is mandatory";
				error.setDescription("Contest Id is mandatory");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				resMsg = "Question Id is mandatory";
				error.setDescription("Question Id is mandatory");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			if(TextUtil.isEmptyOrNull(questionNoStr)){
				resMsg = "Question NO is mandatory";
				error.setDescription("Question NO is mandatory");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}*/
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null){
				resMsg = "Contest Id is Invalid";
				error.setDescription("Contest Id is Invalid");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			
			QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				resMsg = "Question Id is Invalid";
				error.setDescription("Question Id is Invalid");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			Integer questionNo = Integer.parseInt(questionNoStr);
			if(!quizQuestion.getQuestionSNo().equals(questionNo) || !quizQuestion.getContestId().equals(contest.getId())) {
				resMsg = "Question No or Id is Invalid";
				error.setDescription("Question No or Id is Invalid");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			if(quizQuestion.getQuestionSNo() >= contest.getNoOfQuestions()) {
				resMsg = "LifeLine can't be used for Last question.";
				error.setDescription("LifeLine can't be used for Last question.");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			
			//Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerIdStr));
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				resMsg = "Customer Id is Invalid.";
				error.setDescription("Customer Id is Invalid.");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			QuizCustomerContestAnswers customerAnswers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().
											getCustomerContestAnswerByCustomerIdandQuestionId(Integer.parseInt(customerIdStr), Integer.parseInt(questionIdStr));
			if(customerAnswers == null) {
				/*error.setDescription("Customer not yet answered this question. ");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,"Customer not yet answered this question.");
				return lifeLineUsageInfo;*/
				
				 customerAnswers = new QuizCustomerContestAnswers();
				 customerAnswers.setCustomerId(Integer.parseInt(customerIdStr));
				 customerAnswers.setContestId(Integer.parseInt(contestIdStr));
				 customerAnswers.setQuestionId(Integer.parseInt(questionIdStr));
				 customerAnswers.setQuestionSNo(Integer.parseInt(questionNoStr));
				 //customerAnswers.setAnswer(answerOption);
				 customerAnswers.setIsCorrectAnswer(false);
				 customerAnswers.setCreatedDateTime(new Date());
				 customerAnswers.setIsLifeLineUsed(false);
			}
			if(customerAnswers.getIsCorrectAnswer()) {
				resMsg = "You already selected correct answer for this question.";
				error.setDescription("You already selected correct answer for this question ");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			
			if(customer.getQuizCustomerLives() == null || customer.getQuizCustomerLives() <= 0) {
				resMsg = "You do not have lifeline to use";
				error.setDescription("You do not have lifeline to use.");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			
			QuizCustomerContestAnswers lifeLineUsedcustomers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().
			getLifeLineUsedContestAnswerByCustomerIdandContestId(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
			
			if(lifeLineUsedcustomers != null) {
				resMsg = "You already Used lifeline for this conteset.";
				error.setDescription("You already Used lifeline for this conteset.");
				lifeLineUsageInfo.setError(error);
				lifeLineUsageInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
				return lifeLineUsageInfo;
			}
			
			customer.setQuizCustomerLives(customer.getQuizCustomerLives()-1);
			CustomerUtil.updatedCustomerUtil(customer);
			DAORegistry.getCustomerDAO().update(customer);
			
			Integer cumulativeLifeLineUsed = 0;
			if(customerAnswers.getCumulativeLifeLineUsed() != null) {
				cumulativeLifeLineUsed = customerAnswers.getCumulativeLifeLineUsed();
			}
			cumulativeLifeLineUsed = cumulativeLifeLineUsed + 1;
			
			customerAnswers.setIsLifeLineUsed(true);
			customerAnswers.setUpdatedDateTime(new Date());
			customerAnswers.setCumulativeLifeLineUsed(cumulativeLifeLineUsed);
			QuizDAORegistry.getQuizCustomerContestAnswersDAO().saveOrUpdate(customerAnswers);
			
			QuizContestUtil.updateCustomerLifeLineUsage(customerAnswers);
			QuizContestUtil.updateCustomerAnswerMap(customerAnswers);
			
			lifeLineUsageInfo.setIsLifeLineUsed(Boolean.TRUE);
			lifeLineUsageInfo.setStatus(1);
			lifeLineUsageInfo.setMessage("LifeLine Applied Successfully.");
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success";
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Applying Customer LifeLine.";
			e.printStackTrace();
			error.setDescription("Error occured while Applying Customer LifeLine.");
			lifeLineUsageInfo.setError(error);
			lifeLineUsageInfo.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZAPPLYLIFELINE,resMsg);
			return lifeLineUsageInfo;
		} finally {
			log.info("QUIZ APLY LIFE IOS: "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+lifeLineUsageInfo);
		}
		return lifeLineUsageInfo;
	
	}
	
	 
	
	@RequestMapping(value = "/GetQuestionAnswers", method=RequestMethod.POST)
	public @ResponsePayload QuizAnswerCountDetails getQuestionAnswer(HttpServletRequest request,HttpServletResponse response,Model model){
		
		log.info("Inside Get Questions 1 : "+" : "+ new Date() );
		QuizAnswerCountDetails quizAnswerCountDetails =new QuizAnswerCountDetails();
		Error error = new Error();
		Date start = new Date();
		Date temp = new Date();
		String customerIdStr = request.getParameter("customerId");
		String contestIdStr = request.getParameter("contestId");
		String questionNoStr = request.getParameter("questionNo");
		String questionIdStr = request.getParameter("questionId");
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizAnswerCountDetails.setError(authError);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,authError.getDescription());
				return quizAnswerCountDetails;
			}*/
			
			/*
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Customer Id is mandatory");
				return quizAnswerCountDetails;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Contest Id is mandatory");
				return quizAnswerCountDetails;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				error.setDescription("Question Id is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Question Id is mandatory");
				return quizAnswerCountDetails;
			}
			if(TextUtil.isEmptyOrNull(questionNoStr)){
				error.setDescription("Question No is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Question No is mandatory");
				return quizAnswerCountDetails;
			}*/
			log.info("Inside Get Questions before contest fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			temp = new Date();
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null){
				error.setDescription("Contest Id is Invalid");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Contest Id is Invalid");
				return quizAnswerCountDetails;
			}
			
			/*QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				error.setDescription("Question Id is Invalid");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Question Id is Invalid");
				return quizAnswerCountDetails;
			}*/
//////			
			log.info("Inside Get Questions after contest fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			temp = new Date();
			QuizContestQuestions quizQuestion = QuizContestUtil.getContestCurrentQuestions(contest.getId());
			if(quizQuestion == null || !quizQuestion.getId().equals(Integer.parseInt(questionIdStr))) {
				error.setDescription("Question Id is Invalid");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Question Id is Invalid");
				return quizAnswerCountDetails;
			}
			quizQuestion.setIsAnswerCountComputed(true);
			QuizContestUtil.updateContestQuestions(quizQuestion);
			
			quizAnswerCountDetails.setCorrectAnswer(quizQuestion.getAnswer());
			quizAnswerCountDetails.setQuestionRewards(quizQuestion.getQuestionRewards());
			
			log.info("Inside Get Questions before count fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			temp = new Date();
			List<Object[]> result = QuizDAORegistry.getQuizQueryManagerDAO().getContestAnswersCountByQuestionId(quizQuestion.getId());
			if(result != null) {
				for (Object[] object : result) {
					String answer = (String)object[1];
					Integer count = (Integer)object[0];
					if(answer != null) {
						if(answer.equals("A")) {
							quizAnswerCountDetails.setOptionACount(count);
						} else if(answer.equals("B")) {
							quizAnswerCountDetails.setOptionBCount(count);
						} else if(answer.equals("C")) {
							quizAnswerCountDetails.setOptionCCount(count);
						} /*else if(answer.equals("D")) {
							quizAnswerCountDetails.setOptionDCount(count);
						}*/
					}
				}
			}
			log.info("Inside Get Questions after count fetch : "+contestIdStr+" : "+questionIdStr+" : "+questionNoStr+" :tm: "+(new Date().getTime()-temp.getTime())+" : "+ new Date() );
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Customer Id is mandatory");
				return quizContestDetails;
			}*/
			
			/*QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerById(Integer.parseInt(customerIdStr));
			if(quizCustomer == null){
				error.setDescription("Customer not valid");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Customer not valid");
				return quizContestDetails;
			}*/
			
			//quizAnswerCountDetails.setQuizContestQuestion(quizQuestion);
			quizAnswerCountDetails.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Answer Count Details.");
			quizAnswerCountDetails.setError(error);
			quizAnswerCountDetails.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Error occured while Fetching Answer Count Details.");
			return quizAnswerCountDetails;
		}
		log.info("QUIZ ANS COUNT : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return quizAnswerCountDetails;
	
	}
	@RequestMapping(value = "/GetQuestionLifeLineUsedCount", method=RequestMethod.POST)
	public @ResponsePayload QuizAnswerCountDetails getQuestionLifeLineUsedCount(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizAnswerCountDetails quizAnswerCountDetails =new QuizAnswerCountDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizAnswerCountDetails.setError(authError);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,authError.getDescription());
				return quizAnswerCountDetails;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String questionNoStr = request.getParameter("questionNo");
			String questionIdStr = request.getParameter("questionId");
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Customer Id is mandatory");
				return quizAnswerCountDetails;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Contest Id is mandatory");
				return quizAnswerCountDetails;
			}
			if(TextUtil.isEmptyOrNull(questionIdStr)){
				error.setDescription("Question Id is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Question Id is mandatory");
				return quizAnswerCountDetails;
			}
			if(TextUtil.isEmptyOrNull(questionNoStr)){
				error.setDescription("Question No is mandatory");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Question No is mandatory");
				return quizAnswerCountDetails;
			}
			*/
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null){
				error.setDescription("Contest Id is Invalid");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Contest Id is Invalid");
				return quizAnswerCountDetails;
			}
			
			/*QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				error.setDescription("Question Id is Invalid");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONANSWERS,"Question Id is Invalid");
				return quizAnswerCountDetails;
			}*/
//////			
			QuizContestQuestions quizQuestion = QuizContestUtil.getContestCurrentQuestions(contest.getId());
			if(quizQuestion == null || !quizQuestion.getId().equals(Integer.parseInt(questionIdStr))) {
				error.setDescription("Question Id is Invalid");
				quizAnswerCountDetails.setError(error);
				quizAnswerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Question Id is Invalid");
				return quizAnswerCountDetails;
			}
			
			Integer lifeLineUsedCount = QuizDAORegistry.getQuizQueryManagerDAO().getContestLifeLineUsageCountByQuestionId(quizQuestion.getId());
			quizAnswerCountDetails.setLifeLineUsedCount(lifeLineUsedCount);
			
			//quizAnswerCountDetails.setQuizContestQuestion(quizQuestion);
			quizAnswerCountDetails.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Success : count:"+lifeLineUsedCount);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Answer Count Details.");
			quizAnswerCountDetails.setError(error);
			quizAnswerCountDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETQUESTIONLIFELINECOUNT,"Error occured while Fetching Answer Count Details.");
			return quizAnswerCountDetails;
		}
		log.info("QUIZ LifeLine COUNT : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return quizAnswerCountDetails;
	
	}
	@RequestMapping(value = "/UpdateContestWinnerRewards", method=RequestMethod.POST)
	public @ResponsePayload QuizContestWinnerRewards updateContestWinnerRewards(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestWinnerRewards contestWinnerRewards =new QuizContestWinnerRewards();
		Error error = new Error();
		Date start = new Date();
		String customerIdStr = request.getParameter("customerId");
		String contestIdStr = request.getParameter("contestId");
		try {
			log.info("QUIZ WINNER REWARDs : "+customerIdStr+": "+contestIdStr+" INSIDE");
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				contestWinnerRewards.setError(authError);
				contestWinnerRewards.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATECONTESTWINNERREWARDS,authError.getDescription());
				return contestWinnerRewards;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				contestWinnerRewards.setError(error);
				contestWinnerRewards.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECONTESTWINNERREWARDS,"Customer Id is mandatory");
				return contestWinnerRewards;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				contestWinnerRewards.setError(error);
				contestWinnerRewards.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECONTESTWINNERREWARDS,"Contest Id is mandatory");
				return contestWinnerRewards;
			}*/
			//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest quizContest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				contestWinnerRewards.setError(error);
				contestWinnerRewards.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECONTESTWINNERREWARDS,"Contest Id is Invalid");
				return contestWinnerRewards;
			}
			/*Double contestAPITracking = 0.0;
			if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
				rewardsPerCustomer = QuizContestUtil.updateContestWinnersRewadPoints(Integer.parseInt(contestIdStr), Integer.parseInt(customerIdStr));	
			} else {
				QuizContestUtil.getContestWinnerRewardPoints(quizContest.getId());
			}*/
			Double rewardsPerCustomer = QuizContestUtil.getContestWinnerRewardPoints(quizContest.getId());
			if(rewardsPerCustomer == null || rewardsPerCustomer <= 0.0) {
				rewardsPerCustomer = QuizContestUtil.updateContestWinnersRewadPoints(quizContest.getId(), Integer.parseInt(customerIdStr));
				
				QuizContestUtil.refreshContestSummaryData(quizContest.getId());
			}
			
			quizContest.setProcessStatus(ContestProcessStatus.WINNERCOMPUTED);
			QuizContestUtil.updateQuizContest(quizContest);
			
			//quizAnswerCountDetails.setQuizContestQuestion(quizQuestion);
			contestWinnerRewards.setStatus(1);
			contestWinnerRewards.setMessage("Customer Rewards Computed successfully.");
			contestWinnerRewards.setRewardsPerWinner(rewardsPerCustomer);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			log.info("QUIZ WINNER REWARDs : "+customerIdStr+": "+contestIdStr+" SUCCESS");
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECONTESTWINNERREWARDS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			log.info("QUIZ WINNER REWARDs : "+customerIdStr+": "+contestIdStr+" EXECEPTION");
			error.setDescription("Error occured while Fetching Customer Reward Compute Details.");
			contestWinnerRewards.setError(error);
			contestWinnerRewards.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECONTESTWINNERREWARDS,"Error occured while Fetching Customer Reward Compute Details.");
			return contestWinnerRewards;
		}
		log.info("QUIZ UPD REWRD COMP : "+customerIdStr+" : coId: "+contestIdStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return contestWinnerRewards;
	
	}
	 
	
	
	@RequestMapping(value = "/GetQuizContestSummary", method=RequestMethod.POST)
	public @ResponsePayload QuizContestSummaryInfo getQuizContestSummary(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestSummaryInfo quizContestSummary =new QuizContestSummaryInfo();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("customerId");
		String contestIdStr = request.getParameter("contestId");
		String summaryTypeStr = request.getParameter("summaryType");
		try {
			
			QuizSummaryType quizSummaryType = null;
			try {
				quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
				
			} catch(Exception e) {
				resMsg = "Summary Type is Invalid";
				error.setDescription("Summary Type is Invalid");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return quizContestSummary;
			}
			
			if(!quizSummaryType.equals(QuizSummaryType.CONTEST)) {
				Error authError = authorizationValidation(request);
				if(authError != null) {
					resMsg = authError.getDescription();
					quizContestSummary.setError(authError);
					quizContestSummary.setStatus(0);
					TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
					return quizContestSummary;
				}
			}
			
			
			/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
				resMsg = "Summary Type is mandatory";
				error.setDescription("Summary Type is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return quizContestSummary;
			}*/
			
						
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				resMsg = "Customer Id is mandatory";
				error.setDescription("Customer Id is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
				return quizContestSummary;
			}*/
						
			if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
				if(TextUtil.isEmptyOrNull(contestIdStr)){
					resMsg = "Contest Id is mandatory";
					error.setDescription("Contest Id is mandatory");
					quizContestSummary.setError(error);
					quizContestSummary.setStatus(0);
					TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
					return quizContestSummary;
				}
				
				//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
				QuizContest quizContest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
				if(quizContest == null) {
					resMsg = "Contest Id is Invalid";
					error.setDescription("Contest Id is Invalid");
					quizContestSummary.setError(error);
					quizContestSummary.setStatus(0);
					TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
					return quizContestSummary;
				}
				//List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByContestId(Integer.parseInt(contestIdStr));
				List<QuizContestWinners> contestWinners = QuizContestUtil.getContestSummaryData(quizContest.getId());
				quizContestSummary.setQuizContestWinners(contestWinners);
				quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
				
				/*if(Integer.parseInt(customerIdStr) != 0) {
					QuizContestWinners contestWinner = QuizContestUtil.getQuizContestWinnerByCustomerIdAndContestId(Integer.parseInt(customerIdStr), Integer.parseInt(contestIdStr));
					quizContestSummary.setQuizContestWinner(contestWinner);
				}*/
				
			} else if(quizSummaryType.equals(QuizSummaryType.TILLDATE)) {
				
				List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByTillDate();
				QuizContestWinners contestWinner = null;
				if(contestWinners != null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						Integer customerId = Integer.parseInt(customerIdStr);
						for (QuizContestWinners contestWinnerObj : contestWinners) {
							if(contestWinnerObj.getCustomerId().equals(customerId)) {
								contestWinner = contestWinnerObj;
							}
						}
					}
				}
				if(contestWinner == null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						contestWinner = QuizContestUtil.getQuizContestWinnersByTillDateAndCustomerId(Integer.parseInt(customerIdStr));
					}
				}
				quizContestSummary.setQuizContestWinners(contestWinners);
				quizContestSummary.setQuizContestWinner(contestWinner);
				//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
			} else if(quizSummaryType.equals(QuizSummaryType.THISWEEK)) {

				//This Week starts from Sunday to till date
				Calendar cal = Calendar.getInstance();
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				dayOfWeek = 1-dayOfWeek;
				cal.add(Calendar.DAY_OF_MONTH, dayOfWeek);
				String startDateStr = dbDateFormat.format(new Date(cal.getTimeInMillis()))+" 00:00:00";
				String toDateStr = dbDateFormat.format(new Date())+" 23:59:59";
				
				List<QuizContestWinners> contestWinners = QuizContestUtil.getQuizContestWinnersByThisWeek(startDateStr, toDateStr);
				QuizContestWinners contestWinner = null;
				if(contestWinners != null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						Integer customerId = Integer.parseInt(customerIdStr);
						for (QuizContestWinners contestWinnerObj : contestWinners) {
							if(contestWinnerObj.getCustomerId().equals(customerId)) {
								contestWinner = contestWinnerObj;
							}
						}
					}
				}
				if(contestWinner == null) {
					if(!TextUtil.isEmptyOrNull(customerIdStr)){
						contestWinner = QuizContestUtil.getQuizContestWinnersByThisWeekAndCustomerId(startDateStr, toDateStr,Integer.parseInt(customerIdStr));
					}
				}
				quizContestSummary.setQuizContestWinners(contestWinners);
				quizContestSummary.setQuizContestWinner(contestWinner);
				//quizContestSummary.setContestWinnersCount(quizContest.getWinnersCount());
			}
			
			quizContestSummary.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success";
			//TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Contest Summary.";
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Contest Summary.");
			quizContestSummary.setError(error);
			quizContestSummary.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCONTESTSUMMARY,resMsg);
			return quizContestSummary;
		} finally {
			log.info("QUIZ SUMMARY IOS: "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : stype: "+request.getParameter("summaryType")+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		}
		return quizContestSummary;
	
	}
	
	@RequestMapping(value = "/GetQuizGrandWinners", method=RequestMethod.POST)
	public @ResponsePayload QuizContestSummaryInfo getQuizGrandWinners(HttpServletRequest request,HttpServletResponse response,Model model){
		QuizContestSummaryInfo quizContestSummary =new QuizContestSummaryInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizContestSummary.setError(authError);
				quizContestSummary.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,authError.getDescription());
				return quizContestSummary;
			}
			
			//String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			//String summaryTypeStr = request.getParameter("summaryType");
			
			/*if(TextUtil.isEmptyOrNull(summaryTypeStr)){
				error.setDescription("Summary Type is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Summary Type is mandatory");
				return quizContestSummary;
			}
			QuizSummaryType quizSummaryType = null;
			try {
				quizSummaryType = QuizSummaryType.valueOf(summaryTypeStr);
				
			} catch(Exception e) {
				error.setDescription("Summary Type is Invalid");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Summary Type is Invalid");
				return quizContestSummary;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Customer Id is mandatory");
				return quizContestSummary;
			}*/
			
			//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Contest Id is mandatory");
				return quizContestSummary;
			}
			
			//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest quizContest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Contest Id is Invalid");
				return quizContestSummary;
			}
			
			/*List<QuizContestWinners> grandWinners = null;
			if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
				grandWinners = QuizContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));	
			} else {
				grandWinners = QuizContestUtil.getContestGrandWinners(quizContest.getId());
			}*/
			List<QuizContestWinners> grandWinners = QuizContestUtil.getContestGrandWinners(quizContest.getId());
			if(grandWinners == null || grandWinners.isEmpty()) {
				Date startGrand = new Date();
				grandWinners = QuizContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));
				/*try {
					Date startOne = new Date();
				//QuizContestUtil.forceSummaryRefreshTable();
				//QuizContestUtil.refreshSummaryDataTable();
				QuizContestUtil.refreshSummaryDataCache();
				
				log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestIdStr+" : "+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					Date startOne = new Date();
					//QuizDAORegistry.getQuizSummaryManagerDAO().updateCustomerPromoCodeAndContestORderStats();
					QuizCustomerPromocodeandContestOrderStatsScheduler.processCustomerPromoCodeAndContestOrderStats();
				log.info("Time to Update Cust PRomo and co stats Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestIdStr+" : "+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}*/
				log.info("Time Grand Full Compute : "+(new Date().getTime()-startGrand.getTime())+" : "+ new Date());
			} else {
				log.info("Grand Winner Else  : "+(new Date().getTime()));
			}
			 
			quizContest.setProcessStatus(ContestProcessStatus.GRANDWINNERCOMPUTED);
			QuizContestUtil.updateQuizContest(quizContest);
			
			quizContestSummary.setQuizContestWinners(grandWinners);
			quizContestSummary.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Grand Winners.");
			quizContestSummary.setError(error);
			quizContestSummary.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGRANDWINNERS,"Error occured while Fetching Grand Winners.");
			return quizContestSummary;
		}
		log.info("QUIZ GRANDWINNER : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizContestSummary;
	
	}
	
	@RequestMapping(value = "/RefreshSummaryData", method=RequestMethod.POST)
	public @ResponsePayload QuizContestSummaryInfo refreshSummaryData(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestSummaryInfo quizContestSummary =new QuizContestSummaryInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizContestSummary.setError(authError);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,authError.getDescription());
				return quizContestSummary;
			}
			
			//String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			//String summaryTypeStr = request.getParameter("summaryType");
			
			//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Contest Id is mandatory");
				return quizContestSummary;
			}
			
			QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Contest Id is Invalid");
				return quizContestSummary;
			}
			
			/*List<QuizContestWinners> grandWinners = null;
			if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
				grandWinners = QuizContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));	
			} else {
				grandWinners = QuizContestUtil.getContestGrandWinners(quizContest.getId());
			}*/
			QuizContestUtil.refreshSummaryDataCache();
			//QuizContestUtil.refreshSummaryDataTable();
			
			quizContestSummary.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Refreshing Contest summary data.");
			quizContestSummary.setError(error);
			quizContestSummary.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Error occured while Refreshing Contest summary data.");
			return quizContestSummary;
		}
		log.info("QUIZ SUMMARY REFRESH : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizContestSummary;
	
	}
	
	@RequestMapping(value = "/ManualRefreshSummaryData", method=RequestMethod.POST)
	public @ResponsePayload QuizContestSummaryInfo forceManualrefreshSummaryData(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestSummaryInfo quizContestSummary =new QuizContestSummaryInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizContestSummary.setError(authError);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,authError.getDescription());
				return quizContestSummary;
			}*/
			
			//String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			//String summaryTypeStr = request.getParameter("summaryType");
			
			//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Contest Id is mandatory");
				return quizContestSummary;
			}*/
			
			/*QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Contest Id is Invalid");
				return quizContestSummary;
			}*/
			
			/*List<QuizContestWinners> grandWinners = null;
			if(quizContest.getProcessStatus().equals(ContestProcessStatus.STARTED)) {
				grandWinners = QuizContestUtil.computeContestGrandWinners(Integer.parseInt(contestIdStr));	
			} else {
				grandWinners = QuizContestUtil.getContestGrandWinners(quizContest.getId());
			}*/
			
			QuizContestUtil.forceManualefreshSummaryDataTable();
			
			quizContestSummary.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Manual Refreshing Contest summary data.");
			quizContestSummary.setError(error);
			quizContestSummary.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREFRESHSUMMARYDATA,"Error occured while MAnual Refreshing Contest summary data.");
			return quizContestSummary;
		}
		log.info("QUIZ SUMMARY Force Manual REFRESH : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizContestSummary;
	
	}
	@RequestMapping(value = "/SendQuizManualNotification", method=RequestMethod.POST)
	public @ResponsePayload QuizContestSummaryInfo sendQuizManualNotification(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestSummaryInfo quizContestSummary =new QuizContestSummaryInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizContestSummary.setError(authError);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,authError.getDescription());
				return quizContestSummary;
			}
			
			//String customerIdStr = request.getParameter("customerId");
			//String contestIdStr = request.getParameter("contestId");
			String message = request.getParameter("message");
			
			//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Contest Id is mandatory");
				return quizContestSummary;
			}*/
			if(TextUtil.isEmptyOrNull(message)){
				error.setDescription("Message is Mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Message is Mandatory");
				return quizContestSummary;
			}
			
			/*QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Contest Id is Invalid");
				return quizContestSummary;
			}*/

			QuizContestStartNotification.sendBulkNotifications(message, null, true);
			
			List<CustomerNotificationDetails> custNotificationList = DAORegistry.getQueryManagerDAO().getAllOtpVerifiedCustomersNotificationDeviceDetails();
			QuizContestStartNotification.sendQuizContestNotifications(custNotificationList, message, null, true);
			
			quizContestSummary.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Success");
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending manual notification.");
			quizContestSummary.setError(error);
			quizContestSummary.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Error occured while sending manual notification.");
			return quizContestSummary;
		}
		log.info("QUIZ MAnual NOTIFIcation : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : msg: "+request.getParameter("message")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizContestSummary;
	
	}
	@RequestMapping(value = "/GetCustomerCountDetails", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerCountDetailInfo getCustomerCountDetails(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerCountDetailInfo quizCustomerCountDetails = new QuizCustomerCountDetailInfo();
		Error error = new Error();
		Date start = new Date();
		String customerIdStr = request.getParameter("customerId");
		String contestIdStr = request.getParameter("contestId");
		String pageNoStr = request.getParameter("pageNo");
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizCustomerCountDetails.setError(authError);
				quizCustomerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,authError.getDescription());
				return quizCustomerCountDetails;
			}*/
			 
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizCustomerCountDetails.setError(error);
				quizCustomerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Customer Id is mandatory");
				return quizCustomerCountDetails;
			}
			
			if(TextUtil.isEmptyOrNull(questionNoStr)){
				error.setDescription("Question No is mandatory");
				quizCustomerCountDetails.setError(error);
				quizCustomerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Question No is mandatory");
				return quizCustomerCountDetails;
			}
			if(TextUtil.isEmptyOrNull(pageNoStr)){
				error.setDescription("Page No is mandatory");
				quizCustomerCountDetails.setError(error);
				quizCustomerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Page No is mandatory");
				return quizCustomerCountDetails;
			}*/
			Integer pageNo = 1;
			try {
				pageNo = Integer.parseInt(pageNoStr);
			} catch(Exception e) {
				error.setDescription("Page No is Invalid");
				quizCustomerCountDetails.setError(error);
				quizCustomerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Page No is Invalid");
				return quizCustomerCountDetails;
			}
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizCustomerCountDetails.setError(error);
				quizCustomerCountDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Contest Id is mandatory");
				return quizCustomerCountDetails;
			}*/
			
			//QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest quizContest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizCustomerCountDetails.setError(error);
				quizCustomerCountDetails.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Contest Id is Invalid");
				return quizCustomerCountDetails;
			}
			Integer maxRows = 200;
			quizCustomerCountDetails = QuizContestUtil.getCustomerCountDetailsInfo(Integer.parseInt(contestIdStr),quizCustomerCountDetails,pageNo,maxRows);
			 
			
			//quizCustomerCountDetails.setQuizContestWinners(grandWinners);
			
			quizCustomerCountDetails.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Count Details.");
			quizCustomerCountDetails.setError(error);
			quizCustomerCountDetails.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZCUSTOMERCOUNTDETAILS,"Error occured while Fetching Customer Count Details.");
			return quizCustomerCountDetails;
		}
		log.info("QUIZ CUT COUNT DTLS : "+customerIdStr+" : coId: "+contestIdStr+" :pNo: "+pageNoStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizCustomerCountDetails;
	
	}
	
	@RequestMapping(value = "/UpdateCustomerContestStats", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerStatsInfo updateCustomerContestStats(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerStatsInfo quizCustomerStatsInfo = new QuizCustomerStatsInfo();
		Error error = new Error();
		Date start = new Date();
		String contestIdStr = request.getParameter("contestId");
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizCustomerStatsInfo.setError(authError);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECUSTOMERSTATS,authError.getDescription());
				return quizCustomerStatsInfo;
			}
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATECUSTOMERSTATS,"Contest Id is mandatory");
				return quizCustomerStatsInfo;
			}*/
			
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null) {
				error.setDescription("Contest Id is Invalid");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECUSTOMERSTATS,"Contest Id is Invalid");
				return quizCustomerStatsInfo;
			}
			if(contest.getIsCustomerStatsUpdated()) {
				error.setDescription("Customer Stats Already Updated for This Contest.");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECUSTOMERSTATS,"Customer Stats Already Updated for This Contest.");
				return quizCustomerStatsInfo;
			}
			
			Boolean flag = QuizContestUtil.updateCustomerContestDetails(contest.getId());
			//Boolean flag = CassContestUtil.updateCustomerContestDetailsinCassandra(contest.getId());
			if(!flag) {
				error.setDescription("Error occured while Updating CustomerUtil Contest stats.");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECUSTOMERSTATS,"Error occured while Updating CustomerUtil Contest stats.");
				return quizCustomerStatsInfo;
			}
			
			quizCustomerStatsInfo.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECUSTOMERSTATS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Customer Contest stats.");
			quizCustomerStatsInfo.setError(error);
			quizCustomerStatsInfo.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZUPDATECUSTOMERSTATS,"Error occured while Updating Customer Contest stats.");
			return quizCustomerStatsInfo;
		}
		log.info("QUIZ UPD CUST CONT STATS : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizCustomerStatsInfo;
	
	}
	
	@RequestMapping(value = "/GetOTP", method=RequestMethod.POST)
	public @ResponsePayload QuizOTPDetails generateOTP(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizOTPDetails quizOTPDetails =new QuizOTPDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizOTPDetails.setError(authError);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,authError.getDescription());
				return quizOTPDetails;
			}*/
			
			String phone = request.getParameter("phone");
			String resendOTP = request.getParameter("resendOTP");
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Phone no is mandatory");
				quizOTPDetails.setError(error);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Phone no is mandatory");
				return quizOTPDetails;
			}
			
			phone = phone.trim();
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){
				error.setDescription("Please enter valid 10 digit phone number.");
				quizOTPDetails.setError(error);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Please enter valid 10 digit phone number.");
				return quizOTPDetails;
			}
			
			//phone =  QuizSettings.validatePhoneNo(phone);
			
			if(!TextUtil.isEmptyOrNull(resendOTP) && resendOTP.equalsIgnoreCase("true")){
				QuizDAORegistry.getQuizOTPTrackingDAO().deleteAllOTPByPhoneNo(phone);
			}
			
			QuizOTPTracking otpTracking = generateOTPTracking(phone, error);
			if(otpTracking == null) {
				quizOTPDetails.setError(error);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,error.getDescription());
				
				log.info("QUIZ GET OTP 00 : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
				return quizOTPDetails;
			}
			
			quizOTPDetails.setOtp(otpTracking.getResponseOtp());
			quizOTPDetails.setExpiryTimeInSeconds(QuizSettings.otpLiveSeconds);
			quizOTPDetails.setStatus(1);
			quizOTPDetails.setOtpTrackingId(otpTracking.getId());
			quizOTPDetails.setMessage("OTP Generated and Sent Successfully.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending OTP.");
			quizOTPDetails.setError(error);
			quizOTPDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Error occured while sending OTP.");
			return quizOTPDetails;
		}
		log.info("QUIZ GET OTP : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizOTPDetails;
	
	}
	
	@RequestMapping(value = "/ValidateOTP", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails validateLoginOTP(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,authError.getDescription());
				return customerDetails;
			}*/
			
			String productTypeStr = request.getParameter("productType");
			//String signUpTypeStr = request.getParameter("signUpType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId = request.getParameter("notificationRegId");
			String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");

			String customerId = request.getParameter("customerId");
			String verificationCode = request.getParameter("verificationCode");
			String otpTrackingIdStr = request.getParameter("otpTrackingId");
			
			ApplicationPlatForm applicationPlatForm=null;
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please send valid application platform");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please send phone no");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please send phone no");
				return customerDetails;
			}
			phone = phone.trim();
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(verificationCode)){
				error.setDescription("Please enter your OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please enter your OTP");
				return customerDetails;
			}
			
			Integer otpTrackingId = null;
			if(!TextUtil.isEmptyOrNull(otpTrackingIdStr)){
				try {
					otpTrackingId = Integer.parseInt(otpTrackingIdStr.trim());
				}catch(Exception e) {
					error.setDescription("Please enter valid OTP");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please enter valid OTP");
					return customerDetails;
				}
			}
			
			List<QuizOTPTracking> allActiveOtpList = QuizDAORegistry.getQuizOTPTrackingDAO().getActiveOTPByPhoneNo(phone);
			
			if(null == allActiveOtpList || allActiveOtpList.isEmpty()) {
				error.setDescription("You have entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"You have entered wrong OTP");
				return customerDetails;
			}
			
			QuizOTPTracking otpTracking = null;
			for (QuizOTPTracking quizOTPTracking : allActiveOtpList) {
				if(quizOTPTracking.getOtp().equals(verificationCode)){
					otpTracking = quizOTPTracking;
				}
			}
			
			if(otpTracking == null){
				error.setDescription("You entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,"You have entered wrong OTP");
				return customerDetails;
			}
			
			Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
			if(null == customer){
				error.setDescription("The customer id you sent is incorrect. Please try again. ");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"The customer id you sent is incorrect. Please try again. ");
				return customerDetails;
			}
			
			Date sentDate = otpTracking.getCreatedDate();
			Date curDate = new Date();
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sentDate);
			calendar.add(Calendar.SECOND, QuizSettings.otpLiveSeconds);
			
			if(calendar.getTime().before(curDate)){
				error.setDescription("OTP Expired. Please Click Resend");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"You entered wrong OTP");
				return customerDetails;
			} 
			
			otpTracking.setStatus("VALIDATED");
			otpTracking.setVerifiedDate(new Date());
			QuizDAORegistry.getQuizOTPTrackingDAO().update(otpTracking);
			
			Customer duplicatePhoneCustomer = DAORegistry.getCustomerDAO().getOtpVerifiedCustomerByPoneAndProductTypeExceptThisCustomer(phone, ProductType.REWARDTHEFAN, customer.getId());
			if(duplicatePhoneCustomer != null) {
				customerDetails.setIsOtherCustPhoneNo(true);
				customerDetails.setOtherCustEmail(duplicatePhoneCustomer.getEmail());
				customerDetails.setStatus(1);
				String message = "This Phone No Already Mapped with Account "+duplicatePhoneCustomer.getEmail()+". Do you want to reaplce it with this new Account?";
				customerDetails.setMessage(message);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Phone No Exist with different Email.");
				return customerDetails;
			}
			
			if(customer.getPhone() == null || !customer.getPhone().equals(phone.trim()) || !customer.getIsOtpVerified()) {
				customer.setIsOtpVerified(true);
				customer.setPhone(phone);
				CustomerUtil.updatedCustomerUtil(customer);
				DAORegistry.getCustomerDAO().update(customer);
			}
			//QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerByPhone(otpTracking.getPhone());
			//if(null != quizCustomer){
			//	customerDetails.setQuizCustomer(quizCustomer);
			//	customerDetails.setShowUserInfoPopup(false);
			//}else{
			//	customerDetails.setShowUserInfoPopup(true);
			//}
			
			//CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			//Get CustomerAddress by customer id and add it to the response
			//Start			
			/*List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
			List<UserAddress> billingAddress = new ArrayList<UserAddress>();
			List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
			
			if(userAddress != null){
				for(UserAddress address : userAddress){
					if(address.getAddressType() == AddressType.BILLING_ADDRESS)
						billingAddress.add(address);
					else
						shippingAddress.add(address);					
				}
			}*/					
			//End
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			CustomerLoginHistory loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(SignupType.REWARDTHEFAN);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			//loginHistory.setSocialAccessToken(fbAccessToken);			
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			/*if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}
				
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
			}*/
			
			if(customer.getUserId() == null || customer.getUserId().equals("")) {
				customerDetails.setShowUserIdPopup(true);
			} 
			/*boolean customerPic = false;
			String profilePicPrefix = URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = AdminController.readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			//customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			//if(null !=shippingAddress && shippingAddress.size() >2){
			//	Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			//}
			//customerDetails.getCustomer().setShippingAddress(shippingAddress);
			
			customerDetails.setStatus(1);
			customerDetails.setMessage("OTP successfully verified");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Oops, Somthing is up while validating OTP");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Oops, Somthing is up while validating OTP");
			return customerDetails;
		}
		log.info("QUIZ VALIDATE OTP : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return customerDetails;
	
	}
	@RequestMapping(value = "/QuizRemoveDuplicatePhoneMap", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails quizRemoveDuplicatePhoneMap(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,authError.getDescription());
				return customerDetails;
			}*/
			
			String productTypeStr = request.getParameter("productType");
			//String signUpTypeStr = request.getParameter("signUpType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId = request.getParameter("notificationRegId");
			String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");

			String customerId = request.getParameter("customerId");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Notification RegistrationID is mandatory");
				return customerDetails;
			}	*/
			}
			
			
			//if(!TextUtil.isEmptyOrNull(customerPromoAlert) && customerPromoAlert.equals("YES")){
			//	customerDetails.setShowPromoAert(true);
			//	customerDetails.setPromoAlertMessage(TextUtil.getCustomerPromoAlertText());
			//}
			
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Please send custoemr id");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Please send custoemr id");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please send phone no");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Please send phone no");
				return customerDetails;
			}
			
			Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
			if(null == customer){
				error.setDescription("The customer id you sent is incorrect. Please try again. ");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"The customer id you sent is incorrect. Please try again. ");
				return customerDetails;
			}
			Customer duplicatePhoneCustomer = DAORegistry.getCustomerDAO().getOtpVerifiedCustomerByPoneAndProductTypeExceptThisCustomer(phone, ProductType.REWARDTHEFAN, customer.getId());
			if(duplicatePhoneCustomer != null) {
				duplicatePhoneCustomer.setIsOtpVerified(false);
				CustomerUtil.updatedCustomerUtil(duplicatePhoneCustomer);
				DAORegistry.getCustomerDAO().update(duplicatePhoneCustomer);
			}
			
			if(customer.getPhone() == null || !customer.getPhone().equals(phone.trim()) || !customer.getIsOtpVerified()) {
				customer.setIsOtpVerified(true);
				customer.setPhone(phone);
				CustomerUtil.updatedCustomerUtil(customer);
				DAORegistry.getCustomerDAO().update(customer);
			}
			//QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerByPhone(otpTracking.getPhone());
			//if(null != quizCustomer){
			//	customerDetails.setQuizCustomer(quizCustomer);
			//	customerDetails.setShowUserInfoPopup(false);
			//}else{
			//	customerDetails.setShowUserInfoPopup(true);
			//}
			
			//CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			//Get CustomerAddress by customer id and add it to the response
			//Start			
			/*List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
			List<UserAddress> billingAddress = new ArrayList<UserAddress>();
			List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
			
			if(userAddress != null){
				for(UserAddress address : userAddress){
					if(address.getAddressType() == AddressType.BILLING_ADDRESS)
						billingAddress.add(address);
					else
						shippingAddress.add(address);					
				}
			}*/					
			//End
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			CustomerLoginHistory loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(SignupType.REWARDTHEFAN);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			//loginHistory.setSocialAccessToken(fbAccessToken);			
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			/*if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}
				
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
			}*/
			
			if(customer.getUserId() == null || customer.getUserId().equals("")) {
				customerDetails.setShowUserIdPopup(true);
			} 
			/*boolean customerPic = false;
			String profilePicPrefix = URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = AdminController.readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			//customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			//if(null !=shippingAddress && shippingAddress.size() >2){
			//	Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			//}
			//customerDetails.getCustomer().setShippingAddress(shippingAddress);
			
			customerDetails.setStatus(1);
			customerDetails.setMessage("OTP successfully verified");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Oops, Somthing is up while validating OTP");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREMOVEDUPPHONEMAP,"Oops, Somthing is up while validating OTP");
			return customerDetails;
		}
		log.info("QUIZ REMOVE PHONE MAP : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return customerDetails;
	
	}
	@RequestMapping(value="/QuizUserIdSetup",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails quizUserIdSetup(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,authError.getDescription());
				return customerDetails;
			}*/
			
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			//String deviceId = request.getParameter("deviceId");
			//String notificationRegId = request.getParameter("notificationRegId");
			//String loginIp = request.getParameter("loginIp");
			//String phone = request.getParameter("phone");
			String userId = request.getParameter("userId");
			String customerId = request.getParameter("customerId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Please send valid product type");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(userId)){
				error.setDescription("Please enter valid user id.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Please enter valid user id.");
				return customerDetails;
			} else {
				userId = userId.trim();
				if(userId.length()< 5 || userId.length() > 12) {
					error.setDescription("Please select user id within 5 to 12 letters or numbers.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Please select user id within 5 to 12 letters or numbers.");
					return customerDetails;
				}
				if(!userId.matches("[a-zA-Z0-9]*")) {
					error.setDescription("Please select user id with letters and/or numbers only.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Please select user id with letters and/or numbers only.");
					return customerDetails;
				}
				Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(userId,productType);
				RtfPromotionalEarnLifeOffers rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
				if(tempCustomer != null || rtfReferral != null) {
					error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Whoops! This userId is already signed up. Please choose a different UserId.");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Customer Id is mandatory");
				return customerDetails;
			}
			Customer customer =CustomerUtil.getCustomerById(Integer.parseInt(customerId));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Customer Id is Invalid");
				return customerDetails;
			}
			if(!TextUtil.isEmptyOrNull(referralcodeStr)){
				//error.setDescription("Referral Code is mandatory");
				//customerDetails.setError(error);
				//customerDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Referral Code is mandatory");
				//return customerDetails;
			//} else {
			
				Boolean isRtfReferalCode = false;
				RtfPromotionalEarnLifeOffers rtfReferral = null;
				Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
				if(referralCustomer == null) {
					
					rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getActiveRtfPromotionEarnLifeOfferByPromoCode(referralcodeStr);
					Date today = new Date();
					if(rtfReferral != null && rtfReferral.getStartDate().compareTo(today)<=0 && rtfReferral.getEndDate().compareTo(today)>=0 ) {
						isRtfReferalCode = true;
					} else {
						error.setDescription("Referral Code is Invalid");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Referral Code is Invalid");
						return customerDetails;
					}
				}
				
				if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("You can't use your own referral code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"You can't use your own referral code.");
					return customerDetails;
				}
				if(userId != null && userId.equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("User Id and Referal code can't be the same.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"User Id and Referal code can't be the same.");
					return customerDetails;
				}
			
				QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
				if(existReferralTracking != null) {
					error.setDescription("Customer already Used Referral Code");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Customer already Used Referral Code");
					return customerDetails;
				}
				/*QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerIdandReferralCode(customer.getId(), referralcodeStr);
				if(existReferralTracking != null) {
					error.setDescription("Customer already updated with This Referral Code");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Customer already updated with This Referral Code");
					return customerDetails;
				}*/
				
				if(isRtfReferalCode) {
					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setReferralCode(referralcodeStr);
					//referraltracking.setReferralCustomerId(referralCustomer.getId());
					referraltracking.setRtfPromotionalId(rtfReferral.getId());
					referraltracking.setUpdatedDateTime(new Date());
					referraltracking.setIsAffiliateReferral(false);
					referraltracking.setCreatedDateTime(new Date());
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					
					Integer customerLives = customer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					customerLives=customerLives+rtfReferral.getMaxLifePerCustomer();
					customer.setQuizCustomerLives(customerLives);	
					
					
					DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
					CustomerUtil.updatedCustomerUtil(customer);
					
					//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(customer);
				} else {

					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					
					Integer customerLives = referralCustomer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					
					Integer tireOneCustLives = customer.getQuizCustomerLives();
					if(tireOneCustLives == null) {
						tireOneCustLives=0;
					}
					
					boolean showPrimaryCustNotification = true,showTireOneCustNotification = true;;
					
					QuizAffiliateSetting quizAffiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getActiveAffiliateByCustomerId(referralCustomer.getId());
					
					if(null != quizAffiliateSetting) {
						
						referraltracking.setIsAffiliateReferral(true);
						referraltracking.setAffiliateReferralStatus("ACTIVE");
						
						if(null != quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() && quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() > 0) {
							referraltracking.setLivesToPrimaryCustomer(true);
							customerLives = customerLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
						}else {
							referraltracking.setLivesToPrimaryCustomer(false);
							customerLives = customerLives + 0;
							showPrimaryCustNotification = false;
						}
						
						if(null != quizAffiliateSetting.getNoOfLivesToTireOneCustomer() && quizAffiliateSetting.getNoOfLivesToTireOneCustomer() > 0) {
							referraltracking.setLivesToTireOneCustomer(true);
							tireOneCustLives = tireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
						}else {
							referraltracking.setLivesToTireOneCustomer(false);
							tireOneCustLives = tireOneCustLives + 0;
							showTireOneCustNotification = false;
						}
						
					}else {
						referraltracking.setIsAffiliateReferral(false); 
						referraltracking.setLivesToPrimaryCustomer(true);
						referraltracking.setLivesToTireOneCustomer(true);
						
						customerLives++;
						tireOneCustLives++;
					}
					
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setYearEndRewardApplied(false);
					referraltracking.setReferralCode(referralcodeStr);
					referraltracking.setReferralCustomerId(referralCustomer.getId());
					referraltracking.setCreatedDateTime(new Date());
					referraltracking.setUpdatedDateTime(new Date());
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					
					if(showPrimaryCustNotification) {
						referralCustomer.setQuizCustomerLives(customerLives);
						DAORegistry.getCustomerDAO().updateQuizCustomerLives(referralCustomer.getId(),customerLives);
						CustomerUtil.updatedCustomerUtil(referralCustomer);
						QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
						
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
						try {
							CassCustomerUtil.updateCustomerLives(referralCustomer);
						}catch(Exception e) {
							e.printStackTrace();
						}
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
					}
					
					if(showTireOneCustNotification) {
						customer.setQuizCustomerLives(tireOneCustLives);	
						DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),tireOneCustLives);
						CustomerUtil.updatedCustomerUtil(customer);
						QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
					}
				}
			}
			customer.setUserId(userId);
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			CustomerUtil.updatedCustomerUtil(customer);
				
			customerDetails.setCustomerId(customer.getId());
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			//customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			//if(null !=shippingAddress && shippingAddress.size() >2){
			//	Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			//}
			//customerDetails.getCustomer().setShippingAddress(shippingAddress);
			
			//customerDetails.setCustomerLoyalty(customerLoyalty);
			//customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Success");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Quiz User Id Setup details.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Error occured while Fetching Quiz User Id Setup details.");
			return customerDetails;
		}
		log.info("QUIZ USER ID SETUP : "+request.getParameter("customerId")+" : uId: "+request.getParameter("userId")+" : refCo: "+request.getParameter("referralCode")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return customerDetails;
	}
	
	@RequestMapping(value="/QuizCustomerLogin",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails quizCustomerLogin(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,authError.getDescription());
				return customerDetails;
			}
			
			String loginPassword = request.getParameter("password");
			String loginEmail = request.getParameter("userName");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId=request.getParameter("notificationRegId");
			//String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			if(TextUtil.isEmptyOrNull(loginEmail)){
				error.setDescription("Please enter your Email or userId");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter your Email or userId");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(loginPassword)){
				error.setDescription("Please enter an Email and Password to continue");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter an Email and Password to continue");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please enter a phone number");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter a phone number");
				return customerDetails;
			}
			phone = phone.trim();
			//String tempPhone = phone;
			//tempPhone =  QuizSettings.validatePhoneNo(tempPhone);
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			
			
			//Get customer by email or user id
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserNameByProductType(loginEmail,productType);
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmailByProductType(loginEmail,productType);
			
				//if(null == customer){
				//error.setDescription("The email or user id you entered is incorrect. Please try again. ");
				//customerDetails.setError(error);
				//customerDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"The email or user id you entered is incorrect. Please try again.");
				//return customerDetails;
				//}
			boolean showOtpPopup = true;
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmail(loginEmail);
			
			//executed only when customer is already registered with us from RTW and RTW2 database
			if(customer == null || (customer.getProductType().equals(ProductType.RTW) || 
					customer.getProductType().equals(ProductType.RTW2) || customer.getProductType().equals(ProductType.TIXCITY))){

				if(loginPassword.trim().length() < 8 || loginPassword.trim().length() > 20){//tempPhone.length() < 10
					error.setDescription("Password Length must be in between 8-20.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Password Length must be in between 8-20.");
					return customerDetails;
				}
				//loginPassword = loginPassword.replace(" +", "");
				//String password = PasswordUtil.generateEncrptedPassword(loginPassword);
				CustomerLoyalty customerLoyalty = null;
				
				if(customer == null) {
					customer = new Customer();
					
					//11-21-2018 3:22 PM - Give 1 life to all new registration. Given by Mitul - Done By Ulaganathan
					customer.setQuizCustomerLives(1);
				} else {
					//customer.setIsEmailed(true);
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					
					try{
						//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
						DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(customer.getId());
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				customer.setBrokerId(1001);
				customer.setIsClient(true);
				customer.setProductType(productType);
				customer.setSignupType(SignupType.REWARDTHEFAN);
				customer.setCustomerName("");
				customer.setLastName("");
				//customer.setReferralCode(null!=referralCode?referralCode:"");
				customer.setEmail(loginEmail);
				customer.setEncryptPassword(loginPassword);
				customer.setUserName(loginEmail);
				customer.setSignupDate(new Date());
				customer.setCustomerType(CustomerType.WEB_CUSTOMER);
				customer.setApplicationPlatForm(applicationPlatForm);
				customer.setDeviceId(null != deviceId?deviceId:"");
				customer.setNotificationRegId(null!=notificationRegId?notificationRegId:"");
				customer.setSocialAccountId("");
				customer.setUserId(null);
				customer.setPhone(phone);
				customer.setFirstTimeLogin("Yes");
				
				customer.setReferrerCode(loginEmail);
				customer.setLastUpdatedDate(new Date());
				//Save Customer
				DAORegistry.getCustomerDAO().saveOrUpdate(customer);
				
				if(customer.getCustImagePath() == null) {
					String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
					customer.setCustImagePath(fileName);
					DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
					//CustomerUtil.updatedCustomerUtil(customer);
				}
				
				/*Add new Customer to Customer Util - Begins*/
				try{
					CustomerUtil.updatedCustomerUtil(customer);
				}catch(Exception e){
					e.printStackTrace();
				}
				/*Add new Customer to Customer Util - Ends*/
				
				if(customerLoyalty == null) {
					customerLoyalty = new CustomerLoyalty();
					customerLoyalty.setCustomerId(customer.getId());
					customerLoyalty.setActivePointsAsDouble(0.00);
					customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
					customerLoyalty.setLatestSpentPointsAsDouble(0.00);
					customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
					customerLoyalty.setTotalSpentPointsAsDouble(0.00);
					customerLoyalty.setLastUpdate(new Date());
					customerLoyalty.setLastNotifiedTime(new Date());
					customerLoyalty.setPendingPointsAsDouble(0.00);
					customerLoyalty.setTotalReferralDollars(0.00);
					customerLoyalty.setLastReferralDollars(0.00);
					customerLoyalty.setTotalAffiliateReferralDollars(0.00);
					customerLoyalty.setLastAffiliateReferralDollars(0.00);
					//Save Customer Loyalty Information
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
				}
				
				/*CustomerLoginHistory loginHistory = new CustomerLoginHistory();
				loginHistory.setCustomerId(customer.getId());
				loginHistory.setApplicationPlatForm(applicationPlatForm);
				loginHistory.setLastLoginTime(new Date());
				loginHistory.setLoginType(SignupType.REWARDTHEFAN);
				loginHistory.setDeviceId(deviceId);
				loginHistory.setNotificationRegId(notificationRegId);
				loginHistory.setLoginIp(loginIp);
				loginHistory.setSocialAccessToken(null);	
				//Save Customer Login History
				DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);*/
				
				if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
						applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
					
					CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
					
					if(null == customerDeviceDetails){
						customerDeviceDetails = new CustomerDeviceDetails();
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
						customerDeviceDetails.setCreatedDate(new Date());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setStatus("ACTIVE");
						customerDeviceDetails.setDeviceId(deviceId);
						customerDeviceDetails.setNotificationRegId(notificationRegId);
						customerDeviceDetails.setLoginIp(loginIp);
					}else{
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setNotificationRegId(notificationRegId);
					}
					//Save Customer Device Details
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				
					//Add Customer Latest Device Details into CustomerDeviceUtils -  Begins
					try{
						CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
					}catch(Exception e){
						e.printStackTrace();
					}
					//Add Customer Latest Device Details into CustomerDeviceUtils -  Ends
				}
				String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+loginEmail.split("@")[0]);
				boolean isEmailSent = true;
				String toEmail = customer.getEmail();
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("promotionalCode", custPromoCode);
				mailMap.put("userName",customer.getEmail());
				
				//inline(image in mail body) image add code
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				
				try{
					//if(productType.equals(productType.REWARDTHEFAN)) {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
								"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
					//} else {
					//	isEmailSent = false;
					//	toEmail = "";
					//}
				}catch(Exception e){
					e.printStackTrace();
					isEmailSent = false;
					toEmail = "";
				}
				
				/*Calendar calendar = new GregorianCalendar();
				calendar.add(Calendar.YEAR, 1);
				RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
				custPromoOffer.setCreatedDate(new Date());
				custPromoOffer.setCustomerId(customer.getId());
				custPromoOffer.setDiscount(25.00);
				custPromoOffer.setStartDate(new Date());
				custPromoOffer.setEndDate(calendar.getTime());
				custPromoOffer.setFlatOfferOrderThreshold(1.00);
				custPromoOffer.setIsFlatDiscount(false);
				custPromoOffer.setMaxOrders(1);
				custPromoOffer.setModifiedDate(new Date());
				custPromoOffer.setPromoCode(custPromoCode);
				custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
				custPromoOffer.setNoOfOrders(0);
				custPromoOffer.setStatus("ENABLED");
				custPromoOffer.setIsEmailSent(isEmailSent);
				custPromoOffer.setToEmail(toEmail);*/
				//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
			} else {
				String dbPassword = customer.getPassword().replace(" +", "");
				loginPassword = loginPassword.replace(" +", "");
				String password = PasswordUtil.generateEncrptedPassword(loginPassword);
				
				if(!password.equals(dbPassword)){
					error.setDescription("The password you entered is incorrect.Click Forgot Password to retrieve login information");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Click Forgot Email or Password to retrieve login information");
					return customerDetails;
				}
				List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
				if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
					customer.setFirstTimeLogin("No");
				}else{
					customer.setFirstTimeLogin("Yes");
				}
				
				if(customer.getPhone() != null && customer.getPhone().equals(phone) &&
						customer.getIsOtpVerified()) {
					showOtpPopup = false;
				}
				
				CustomerLoginHistory loginHistory = new CustomerLoginHistory();
				loginHistory.setCustomerId(customer.getId());
				loginHistory.setApplicationPlatForm(applicationPlatForm);
				loginHistory.setLastLoginTime(new Date());
				loginHistory.setLoginType(SignupType.REWARDTHEFAN);
				loginHistory.setDeviceId(deviceId);
				loginHistory.setNotificationRegId(notificationRegId);
				loginHistory.setLoginIp(loginIp);
				//loginHistory.setSocialAccessToken(fbAccessToken);			
				DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
				
				if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
						applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
					CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
					if(null == customerDeviceDetails){
						customerDeviceDetails = new CustomerDeviceDetails();
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
						customerDeviceDetails.setCreatedDate(new Date());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setStatus("ACTIVE");
						customerDeviceDetails.setDeviceId(deviceId);
						customerDeviceDetails.setNotificationRegId(notificationRegId);
						customerDeviceDetails.setLoginIp(loginIp);
						DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
					}else{
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setNotificationRegId(notificationRegId);
						DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
					}
					try{
						CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
			}
			
			
			//boolean customerPic = false;
			//String profilePicPrefix = URLUtil.DP_PRFEIX_CODE;
			//if(customer != null ){
			//	Map<String, Boolean> map = AdminController.readCustomerPic(customer.getId());
			//	String getExt = null;
			//	for(Map.Entry<String, Boolean> entry: map.entrySet()){
			//		getExt = entry.getKey();
			//		customerPic = entry.getValue();
			//	}
			//	if(customerPic){
			//		customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
			//		TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
			//	}else{
			//		customerDetails.setCustomerProfilePicWebView(null);
			//	}
			//}
			
			//CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			//customerDetails.setCustomerLoyalty(customerLoyalty);
			//customerDetails.setCustomer(customer);
			//customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			//if(null !=shippingAddress && shippingAddress.size() >2){
			//	Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			//}
			//customerDetails.getCustomer().setShippingAddress(shippingAddress);
			
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				showOtpPopup = true;
			}
			customerDetails.setShowOtpPopup(showOtpPopup);
			if(showOtpPopup) {
				QuizOTPTracking otpTracking = generateOTPTracking(phone, error);
				if(otpTracking == null) {
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,error.getDescription());
					
					log.info("QUIZ LOGIN 00 : "+request.getParameter("customerId")+" : uName: "+request.getParameter("userName")+" : phone: "+request.getParameter("phone")+" : shotpPop: "+customerDetails.getShowOtpPopup()+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
					
					return customerDetails;
				}
				customerDetails.setLoginOTP(otpTracking.getResponseOtp());
				customerDetails.setOtpExpiryTimeInSeconds(QuizSettings.otpLiveSeconds);
				customerDetails.setOtpTrackingId(otpTracking.getId());
				customerDetails.setCustomerId(customer.getId());
				
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setStatus(1);
				customerDetails.setMessage("Login OTP sent successfully.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Login OTP sent successfully");
			} else {
				if(customer.getUserId() == null || customer.getUserId().equals("")) {
					customerDetails.setShowUserIdPopup(true);
				}
				if(customer.getCustImagePath() != null) {
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
				} else {
					customerDetails.setCustomerProfilePicWebView(null);
				}
				CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
				customerDetails.setCustomerLoyalty(customerLoyalty);
				customerDetails.setCustomer(customer);
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setMessage("Quiz Customer Logged in successfully.");
				customerDetails.setStatus(1);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Logged in successfully");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Oops Somthing Went Wrong. Please Login Again!");
			return customerDetails;
		}
		log.info("QUIZ LOGIN : "+request.getParameter("customerId")+" : uName: "+request.getParameter("userName")+" : phone: "+request.getParameter("phone")+" : shotpPop: "+customerDetails.getShowOtpPopup()+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return customerDetails;
	}
	
	@RequestMapping(value = "/CustomerLoginByPhoneNo", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerLoginByPhoneNo(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,authError.getDescription());
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			//String deviceId = request.getParameter("deviceId");
			//String notificationRegId=request.getParameter("notificationRegId");
			//String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please enter a phone number");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter a phone number");
				return customerDetails;
			}
			phone = phone.trim();
			//String tempPhone = phone;
			//tempPhone =  QuizSettings.validatePhoneNo(tempPhone);
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			
			/*if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				showOtpPopup = true;
			}
			customerDetails.setShowOtpPopup(showOtpPopup);*/
			
			QuizOTPTracking otpTracking = generateOTPTracking(phone, error);
			if(otpTracking == null) {
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,error.getDescription());
				
				log.info("QUIZ LOGIN 00 : "+request.getParameter("customerId")+" : uName: "+request.getParameter("userName")+" : phone: "+request.getParameter("phone")+" : shotpPop: "+customerDetails.getShowOtpPopup()+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
				
				return customerDetails;
			}
			customerDetails.setLoginOTP(otpTracking.getResponseOtp());
			customerDetails.setOtpExpiryTimeInSeconds(QuizSettings.otpLiveSeconds);
			customerDetails.setOtpTrackingId(otpTracking.getId());
			
			customerDetails.setStatus(1);
			customerDetails.setMessage("Login OTP sent successfully.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Login OTP sent successfully");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Oops Somthing Went Wrong. Please Login Again!");
			return customerDetails;
		}
		log.info("QUIZ PHONE NOLOGIN : "+" : phone: "+request.getParameter("phone")+" : shotpPop: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return customerDetails;
	}
	@RequestMapping(value="/ValidatePhoneNoLogin",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails validatePhoneNoLogin(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,authError.getDescription());
				return customerDetails;
			}
			
			//String loginPassword = request.getParameter("password");
			//String loginEmail = request.getParameter("userName");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId=request.getParameter("notificationRegId");
			//String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");
			String verificationCode = request.getParameter("verificationCode");
			String otpTrackingIdStr = request.getParameter("otpTrackingId");
			
			resMsg = request.getParameter("phone")+":vc:"+request.getParameter("verificationCode")+":ti:"+request.getParameter("otpTrackingId");
		 
			
			ProductType productType = ProductType.REWARDTHEFAN;
			try{
				productType = ProductType.valueOf(productTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid product type");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,"Please send valid product type");
				return customerDetails;
			}
			
			 
			ApplicationPlatForm applicationPlatForm=null;
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,"Please send valid application platform");
				return customerDetails;
			}
			
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			/*if(TextUtil.isEmptyOrNull(loginEmail)){
				error.setDescription("Please enter your Email or userId");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter your Email or userId");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(loginPassword)){
				error.setDescription("Please enter an Email and Password to continue");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter an Email and Password to continue");
				return customerDetails;
			}*/
			if(TextUtil.isEmptyOrNull(phone)){
				resMsg = "Please enter a phone number:" + resMsg;
				error.setDescription("Please enter a phone number");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			phone = phone.trim();
			//String tempPhone = phone;
			//tempPhone =  QuizSettings.validatePhoneNo(tempPhone);
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				resMsg = "Please enter valid 10 digit phone number:" + resMsg;
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(verificationCode)){
				resMsg = "Please enter your OTP:" + resMsg;
				error.setDescription("Please enter your OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			
			List<QuizOTPTracking> allActiveOtpList = QuizDAORegistry.getQuizOTPTrackingDAO().getActiveOTPByPhoneNo(phone);
			
			if(null == allActiveOtpList || allActiveOtpList.isEmpty()) {
				error.setDescription("You have entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"You have entered wrong OTP");
				return customerDetails;
			}
			
			QuizOTPTracking otpTracking = null;
			for (QuizOTPTracking quizOTPTracking : allActiveOtpList) {
				if(quizOTPTracking.getOtp().equals(verificationCode)){
					otpTracking = quizOTPTracking;
				}
			}
			
			if(otpTracking == null){
				resMsg = "You entered wrong OTP:" + resMsg;
				error.setDescription("You entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			
			Date sentDate = otpTracking.getCreatedDate();
			Date curDate = new Date();
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sentDate);
			calendar.add(Calendar.SECOND, QuizSettings.otpLiveSeconds);
			
			if(calendar.getTime().before(curDate)){
				resMsg = "OTP Expired. Please Click Resend:" + resMsg;
				error.setDescription("OTP Expired. Please Click Resend");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			} 
			
			otpTracking.setStatus("VALIDATED");
			otpTracking.setVerifiedDate(new Date());
			QuizDAORegistry.getQuizOTPTrackingDAO().update(otpTracking);
			
			//Get customer by email or user id
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserNameByProductType(loginEmail,productType);
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmailByProductType(loginEmail,productType);
			
				//if(null == customer){
				//error.setDescription("The email or user id you entered is incorrect. Please try again. ");
				//customerDetails.setError(error);
				//customerDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"The email or user id you entered is incorrect. Please try again.");
				//return customerDetails;
				//}
			//boolean showOtpPopup = true;
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmail(loginEmail);
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByPhoneAndProductType(phone, productType);
			
			//executed only when customer is already registered with us from RTW and RTW2 database
			if(customer == null || (customer.getProductType().equals(ProductType.RTW) || 
					customer.getProductType().equals(ProductType.RTW2) || customer.getProductType().equals(ProductType.TIXCITY))){

				/*if(loginPassword.trim().length() < 8 || loginPassword.trim().length() > 20){//tempPhone.length() < 10
					error.setDescription("Password Length must be in between 8-20.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Password Length must be in between 8-20.");
					return customerDetails;
				}*/
				//loginPassword = loginPassword.replace(" +", "");
				//String password = PasswordUtil.generateEncrptedPassword(loginPassword);
				CustomerLoyalty customerLoyalty = null;
				
				if(customer == null) {
					customer = new Customer();
					
					//11-21-2018 3:22 PM - Give 1 life to all new registration. Given by Mitul - Done By Ulaganathan
					customer.setQuizCustomerLives(1);
				} else {
					//customer.setIsEmailed(true);
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					
					try{
						//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
						DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(customer.getId());
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				customer.setBrokerId(1001);
				customer.setIsClient(true);
				customer.setProductType(productType);
				customer.setSignupType(SignupType.REWARDTHEFAN);
				customer.setCustomerName("");
				customer.setLastName("");
				//customer.setReferralCode(null!=referralCode?referralCode:"");
				//customer.setEmail(loginEmail);
				//customer.setEncryptPassword(loginPassword);
				//customer.setUserName(loginEmail);
				customer.setSignupDate(new Date());
				customer.setCustomerType(CustomerType.WEB_CUSTOMER);
				customer.setApplicationPlatForm(applicationPlatForm);
				customer.setDeviceId(null != deviceId?deviceId:"");
				customer.setNotificationRegId(null!=notificationRegId?notificationRegId:"");
				customer.setSocialAccountId("");
				customer.setUserId(null);
				customer.setPhone(phone);
				customer.setFirstTimeLogin("Yes");
				customer.setIsOtpVerified(Boolean.TRUE);
				
				//customer.setReferrerCode(loginEmail);
				customer.setLastUpdatedDate(new Date());
				//Save Customer
				DAORegistry.getCustomerDAO().saveOrUpdate(customer);
				
				if(customer.getCustImagePath() == null) {
					String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
					customer.setCustImagePath(fileName);
					DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
					//CustomerUtil.updatedCustomerUtil(customer);
				}
				
				/*Add new Customer to Customer Util - Begins*/
				try{
					CustomerUtil.updatedCustomerUtil(customer);
				}catch(Exception e){
					e.printStackTrace();
				}
				/*Add new Customer to Customer Util - Ends*/
				
				if(customerLoyalty == null) {
					customerLoyalty = new CustomerLoyalty();
					customerLoyalty.setCustomerId(customer.getId());
					customerLoyalty.setActivePointsAsDouble(0.00);
					customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
					customerLoyalty.setLatestSpentPointsAsDouble(0.00);
					customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
					customerLoyalty.setTotalSpentPointsAsDouble(0.00);
					customerLoyalty.setLastUpdate(new Date());
					customerLoyalty.setLastNotifiedTime(new Date());
					customerLoyalty.setPendingPointsAsDouble(0.00);
					customerLoyalty.setTotalReferralDollars(0.00);
					customerLoyalty.setLastReferralDollars(0.00);
					customerLoyalty.setTotalAffiliateReferralDollars(0.00);
					customerLoyalty.setLastAffiliateReferralDollars(0.00);
					//Save Customer Loyalty Information
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
				}
				
				/*CustomerLoginHistory loginHistory = new CustomerLoginHistory();
				loginHistory.setCustomerId(customer.getId());
				loginHistory.setApplicationPlatForm(applicationPlatForm);
				loginHistory.setLastLoginTime(new Date());
				loginHistory.setLoginType(SignupType.REWARDTHEFAN);
				loginHistory.setDeviceId(deviceId);
				loginHistory.setNotificationRegId(notificationRegId);
				loginHistory.setLoginIp(loginIp);
				loginHistory.setSocialAccessToken(null);	
				//Save Customer Login History
				DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);*/
				
				/*if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
						applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
					
					CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
					
					if(null == customerDeviceDetails){
						customerDeviceDetails = new CustomerDeviceDetails();
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
						customerDeviceDetails.setCreatedDate(new Date());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setStatus("ACTIVE");
						customerDeviceDetails.setDeviceId(deviceId);
						customerDeviceDetails.setNotificationRegId(notificationRegId);
						customerDeviceDetails.setLoginIp(loginIp);
					}else{
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setNotificationRegId(notificationRegId);
					}
					//Save Customer Device Details
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				
					//Add Customer Latest Device Details into CustomerDeviceUtils -  Begins
					try{
						CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
					}catch(Exception e){
						e.printStackTrace();
					}
					//Add Customer Latest Device Details into CustomerDeviceUtils -  Ends
				}*/
				/*String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+loginEmail.split("@")[0]);
				boolean isEmailSent = true;
				String toEmail = customer.getEmail();
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("promotionalCode", custPromoCode);
				mailMap.put("userName",customer.getEmail());
				
				//inline(image in mail body) image add code
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				
				try{
					//if(productType.equals(productType.REWARDTHEFAN)) {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
								"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
					//} else {
					//	isEmailSent = false;
					//	toEmail = "";
					//}
				}catch(Exception e){
					e.printStackTrace();
					isEmailSent = false;
					toEmail = "";
				}*/
				
				/*Calendar calendar = new GregorianCalendar();
				calendar.add(Calendar.YEAR, 1);
				RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
				custPromoOffer.setCreatedDate(new Date());
				custPromoOffer.setCustomerId(customer.getId());
				custPromoOffer.setDiscount(25.00);
				custPromoOffer.setStartDate(new Date());
				custPromoOffer.setEndDate(calendar.getTime());
				custPromoOffer.setFlatOfferOrderThreshold(1.00);
				custPromoOffer.setIsFlatDiscount(false);
				custPromoOffer.setMaxOrders(1);
				custPromoOffer.setModifiedDate(new Date());
				custPromoOffer.setPromoCode(custPromoCode);
				custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
				custPromoOffer.setNoOfOrders(0);
				custPromoOffer.setStatus("ENABLED");
				custPromoOffer.setIsEmailSent(isEmailSent);
				custPromoOffer.setToEmail(toEmail);*/
				//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
			} else {
				 
			}
			if(customer.getIsOtpVerified() == null || !customer.getIsOtpVerified()) {
				customer.setIsOtpVerified(Boolean.TRUE);
				DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			}
			
			CustomerLoginHistory loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(SignupType.REWARDTHEFAN);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			//loginHistory.setSocialAccessToken(fbAccessToken);			
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
				}
				DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(customer.getUserId() == null || customer.getUserId().equals("") ||
					customer.getEmail() == null || customer.getEmail().equals("")) {
				customerDetails.setShowUserIdPopup(true);
			}
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			customerDetails.setCustomerId(customer.getId());
			customerDetails.setMessage("Quiz Customer Logged in successfully.");
			customerDetails.setStatus(1);
			
			resMsg = "Logged in successfully:" + resMsg;
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
			
		} catch (Exception e) {
			resMsg = "Oops Somthing Went Wrong. Please Login Again!:" + resMsg;
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
			return customerDetails;
		}
		log.info("QUIZ VALID PHONE LOGIN : "+resMsg+":"+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return customerDetails;
	}
	
	@RequestMapping(value="/UpdateCustomerSignUpDetails",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails updateCustomerSignUpDetails(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,authError.getDescription());
				return customerDetails;
			}
			
			String loginPassword = request.getParameter("password");
			String loginEmail = request.getParameter("email");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			//String deviceId = request.getParameter("deviceId");
			//String notificationRegId = request.getParameter("notificationRegId");
			//String loginIp = request.getParameter("loginIp");
			//String phone = request.getParameter("phone");
			String userId = request.getParameter("userId");
			String customerId = request.getParameter("customerId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid product type");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(loginEmail)){
				error.setDescription("Please enter Valid Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter Valid Email");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(loginPassword)){
				error.setDescription("Please enter Valid Password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter Valid Password");
				return customerDetails;
			}
			if(loginPassword.trim().length() < 8 || loginPassword.trim().length() > 20){//tempPhone.length() < 10
				error.setDescription("Password Length must be in between 8-20.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Password Length must be in between 8-20.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer Id is mandatory");
				return customerDetails;
			}
			Customer customer =CustomerUtil.getCustomerById(Integer.parseInt(customerId));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer Id is Invalid");
				return customerDetails;
			}
			Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByEmailAndProductTypeExceptThisCustomer(loginEmail,productType,customer.getId());
			if(tempCustomer != null) {
				error.setDescription("This EmailId is already signed up. Please choose a different EmailId.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"This EmailId is already signed up. Please choose a different EmailId.");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(userId)){
				error.setDescription("Please enter valid user id.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter valid user id.");
				return customerDetails;
			}
			userId = userId.trim();
			if(userId.length()< 5 || userId.length() > 12) {
				error.setDescription("Please select user id within 5 to 12 letters or numbers.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select user id within 5 to 12 letters or numbers.");
				return customerDetails;
			}
			if(!userId.matches("[a-zA-Z0-9]*")) {
				error.setDescription("Please select user id with letters and/or numbers only.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select user id with letters and/or numbers only.");
				return customerDetails;
			}
			//Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(userId,productType);
			tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductTypeExceptThisCustomer(userId, productType, customer.getId());
			RtfPromotionalEarnLifeOffers rtfReferralTemp = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
			if(tempCustomer != null || rtfReferralTemp != null) {
				error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Whoops! This userId is already signed up. Please choose a different UserId.");
				return customerDetails;
			}
			
			if(!TextUtil.isEmptyOrNull(referralcodeStr)){
				//error.setDescription("Referral Code is mandatory");
				//customerDetails.setError(error);
				//customerDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Referral Code is mandatory");
				//return customerDetails;
			//} else {
			
				Boolean isRtfReferalCode = false;
				RtfPromotionalEarnLifeOffers rtfReferral = null;
				Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
				if(referralCustomer == null) {
					
					rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getActiveRtfPromotionEarnLifeOfferByPromoCode(referralcodeStr);
					Date today = new Date();
					if(rtfReferral != null && rtfReferral.getStartDate().compareTo(today)<=0 && rtfReferral.getEndDate().compareTo(today)>=0 ) {
						isRtfReferalCode = true;
					} else {
						error.setDescription("Referral Code is Invalid");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Referral Code is Invalid");
						return customerDetails;
					}
				}
				
				if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("You can't use your own referral code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You can't use your own referral code.");
					return customerDetails;
				}
				if(userId != null && userId.equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("User Id and Referal code can't be the same.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"User Id and Referal code can't be the same.");
					return customerDetails;
				}
			
				QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
				if(existReferralTracking != null) {
					error.setDescription("Customer already Used Referral Code");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer already Used Referral Code");
					return customerDetails;
				}
				/*QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerIdandReferralCode(customer.getId(), referralcodeStr);
				if(existReferralTracking != null) {
					error.setDescription("Customer already updated with This Referral Code");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Customer already updated with This Referral Code");
					return customerDetails;
				}*/
				
				if(isRtfReferalCode) {
					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setReferralCode(referralcodeStr);
					//referraltracking.setReferralCustomerId(referralCustomer.getId());
					referraltracking.setRtfPromotionalId(rtfReferral.getId());
					referraltracking.setCreatedDateTime(new Date());
					referraltracking.setUpdatedDateTime(new Date());
					referraltracking.setIsAffiliateReferral(false);
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					
					Integer customerLives = customer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					customerLives=customerLives+rtfReferral.getMaxLifePerCustomer();
					customer.setQuizCustomerLives(customerLives);	
					
					
					DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
					CustomerUtil.updatedCustomerUtil(customer);
					
					//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(customer);
				} else {
					
					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					
					Integer customerLives = referralCustomer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					
					Integer tireOneCustLives = customer.getQuizCustomerLives();
					if(tireOneCustLives == null) {
						tireOneCustLives=0;
					}
					
					boolean showPrimaryCustNotification = true,showTireOneCustNotification = true;;
					
					QuizAffiliateSetting quizAffiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getActiveAffiliateByCustomerId(referralCustomer.getId());
					
					if(null != quizAffiliateSetting) {
						
						referraltracking.setIsAffiliateReferral(true);
						referraltracking.setAffiliateReferralStatus("ACTIVE");
						
						if(null != quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() && quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() > 0) {
							referraltracking.setLivesToPrimaryCustomer(true);
							customerLives = customerLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
						}else {
							referraltracking.setLivesToPrimaryCustomer(false);
							customerLives = customerLives + 0;
							showPrimaryCustNotification = false;
						}
						
						if(null != quizAffiliateSetting.getNoOfLivesToTireOneCustomer() && quizAffiliateSetting.getNoOfLivesToTireOneCustomer() > 0) {
							referraltracking.setLivesToTireOneCustomer(true);
							tireOneCustLives = tireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
						}else {
							referraltracking.setLivesToTireOneCustomer(false);
							tireOneCustLives = tireOneCustLives + 0;
							showTireOneCustNotification = false;
						}
						
					}else {
						referraltracking.setIsAffiliateReferral(false); 
						referraltracking.setLivesToPrimaryCustomer(true);
						referraltracking.setLivesToTireOneCustomer(true);
						
						customerLives++;
						tireOneCustLives++;
					}
					
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setReferralCode(referralcodeStr);
					referraltracking.setYearEndRewardApplied(false);
					referraltracking.setReferralCustomerId(referralCustomer.getId());
					referraltracking.setCreatedDateTime(new Date());
					referraltracking.setUpdatedDateTime(new Date());
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					 
					
					if(showPrimaryCustNotification) {
						referralCustomer.setQuizCustomerLives(customerLives);
						DAORegistry.getCustomerDAO().updateQuizCustomerLives(referralCustomer.getId(),customerLives);
						CustomerUtil.updatedCustomerUtil(referralCustomer);
						
						QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
						
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
						try {
							CassCustomerUtil.updateCustomerLives(referralCustomer);
						}catch(Exception e) {
							e.printStackTrace();
						}
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
					}
					
					if(showTireOneCustNotification) {
						customer.setQuizCustomerLives(tireOneCustLives);	
						DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),tireOneCustLives);
						CustomerUtil.updatedCustomerUtil(customer);
						QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
					}
				}
			}
			
			customer.setEncryptPassword(loginPassword);
			customer.setEmail(loginEmail);
			customer.setUserName(loginEmail);
			customer.setReferrerCode(loginEmail);
			customer.setUserId(userId);
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			CustomerUtil.updatedCustomerUtil(customer);
			
			//String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+loginEmail.split("@")[0]);
			boolean isEmailSent = true;
			//String toEmail = customer.getEmail();
			Map<String,Object> mailMap = new HashMap<String,Object>();
			//mailMap.put("promotionalCode", custPromoCode);
			mailMap.put("userId",customer.getUserId());
			
			try{
				//inline(image in mail body) image add code
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				
				//if(productType.equals(productType.REWARDTHEFAN)) {
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
							"mail-rewardfan-customer-welcome-phoneno-login.html", mailMap, "text/html", null,mailAttachment,null);
				//} else {
				//	isEmailSent = false;
				//	toEmail = "";
				//}
			}catch(Exception e){
				e.printStackTrace();
				isEmailSent = false;
				//toEmail = "";
			}
				
			customerDetails.setCustomerId(customer.getId());
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			//customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			//if(null !=shippingAddress && shippingAddress.size() >2){
			//	Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			//}
			//customerDetails.getCustomer().setShippingAddress(shippingAddress);
			
			//customerDetails.setCustomerLoyalty(customerLoyalty);
			//customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Success");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Quiz User Id Setup details.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Error occured while Fetching Quiz User Id Setup details.");
			return customerDetails;
		}
		finally {
			log.info("QUIZ UPD CUST PHONE LOGIN : "+request.getParameter("customerId")+" : uId: "+request.getParameter("userId")+" : email: "+request.getParameter("email")+" : refCo: "+request.getParameter("referralCode")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());			
		}
		
		return customerDetails;
	}
	
	@RequestMapping(value="/QuizCustomerSignUp",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails quizCustomerSignUp(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,authError.getDescription());
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String signUpTypeStr = request.getParameter("signUpType");
			String name = request.getParameter("name");
			String lastName =request.getParameter("lastName");
			//String referralCode = request.getParameter("referralCode");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId = request.getParameter("notificationRegId");
			String socialAccountId = request.getParameter("socialAccountId");
			String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			String customerPromoAlert = request.getParameter("customerPromoAlert");
			String phone = request.getParameter("phone");
			String userId = request.getParameter("userId");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(signUpTypeStr)){
				error.setDescription("SignUp Type is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"SignUp Type is mandatory");
				return customerDetails;
			}
			SignupType signupType=null;
			if(!TextUtil.isEmptyOrNull(signUpTypeStr)){
				try{
					signupType = SignupType.valueOf(signUpTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid signup type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please send valid signup type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
			}
			
			if(TextUtil.isEmptyOrNull(userId)){
				error.setDescription("Please enter valid user id.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please enter valid user id.");
				return customerDetails;
			} else {
				userId = userId.trim();
				if(userId.length()< 5 || userId.length() > 12) {
					error.setDescription("Please select user id within 5 to 12 letters or numbers.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please select user id within 5 to 12 letters or numbers.");
					return customerDetails;
				}
				if(!userId.matches("[a-zA-Z0-9]*")) {
					error.setDescription("Please select user id with letters and/or numbers only.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please select user id with letters and/or numbers only.");
					return customerDetails;
				}
				Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(userId,productType);
				RtfPromotionalEarnLifeOffers rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
				if(tempCustomer != null || rtfReferral != null) {
					error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Whoops! This userId is already signed up. Please choose a different UserId.");
					return customerDetails;
				}
			}		
			
			/*if(TextUtil.isEmptyOrNull(lastName)){
				error.setDescription("Customer LastName is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Customer LastName is mandatory");
				return customerDetails;
			}*/
			
			if(TextUtil.isEmptyOrNull(name)){
				error.setDescription("Please enter your First Name");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please enter your First Name");
				return customerDetails;
			}
			if(!TextUtil.isEmptyOrNull(customerPromoAlert) && customerPromoAlert.equals("YES")){
				customerDetails.setShowPromoAert(true);
				customerDetails.setPromoAlertMessage(TextUtil.getCustomerPromoAlertText());
			}
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please enter a phone number");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please enter a phone number");
				return customerDetails;
			}
			phone =  QuizSettings.validatePhoneNo(phone);
			if(phone.length() < 10){
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			
			String finalPassword = password;
			CustomerLoyalty customerLoyalty = null;
			Customer validateCustomer = null;
			CustomerLoginHistory loginHistory = null;
			CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			

			if(TextUtil.isEmptyOrNull(email)){
				error.setDescription("Please enter your Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please enter your Email");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(password)){
				error.setDescription("Please choose a Password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Please choose a Password");
				return customerDetails;
			}
			finalPassword = password;
			
			validateCustomer = DAORegistry.getCustomerDAO().getCustomerByPhoneAndProductType(phone, productType);
			if(null != validateCustomer){
				error.setDescription("Whoops! This phone no is already signed up. Please choose a different phone no.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Whoops! This phone no is already signed up. Please choose a different phone no.");
				return customerDetails;
			}
			
			validateCustomer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
			
			//executed only when customer is already registered with us from RTW and RTW2 database
			if(validateCustomer != null && (validateCustomer.getProductType().equals(ProductType.RTW) || 
					validateCustomer.getProductType().equals(ProductType.RTW2) || validateCustomer.getProductType().equals(ProductType.TIXCITY))){
				
				validateCustomer.setProductType(ProductType.REWARDTHEFAN);
				validateCustomer.setSignupType(SignupType.REWARDTHEFAN);
				validateCustomer.setSignupDate(new Date());
				validateCustomer.setIsEmailed(true);
				validateCustomer.setApplicationPlatForm(applicationPlatForm);
				validateCustomer.setCustomerName(name);
				validateCustomer.setLastName(null!=lastName?lastName:"");
				/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
				String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
				Long referrerNumber =  Long.valueOf(property.getValue());
				referrerNumber++;  
				validateCustomer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
				property.setValue(String.valueOf(referrerNumber));
				DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
				validateCustomer.setReferrerCode(validateCustomer.getEmail());
				validateCustomer.setEncryptPassword(finalPassword);
				validateCustomer.setBrokerId(1001);
				validateCustomer.setIsClient(true);
				
				validateCustomer.setUserId(userId);
				validateCustomer.setPhone(phone);
				validateCustomer.setLastUpdatedDate(new Date());
				
				DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
				
				if(validateCustomer.getCustImagePath() == null) {
					String fileName = URLUtil.getCustomerDefaultprofilePicName(validateCustomer.getId());
					validateCustomer.setCustImagePath(fileName);
					
					CustomerUtil.updatedCustomerUtil(validateCustomer);
					DAORegistry.getCustomerDAO().updateCustImagePath(fileName, validateCustomer.getId());
				}
				
				String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(validateCustomer.getId()+""+email.split("@")[0]);
				
				Calendar calendar = new GregorianCalendar();
				calendar.add(Calendar.YEAR, 1);
				
				RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
				custPromoOffer.setCreatedDate(new Date());
				custPromoOffer.setCustomerId(validateCustomer.getId());
				custPromoOffer.setDiscount(25.00);
				custPromoOffer.setStartDate(new Date());
				custPromoOffer.setEndDate(calendar.getTime());
				custPromoOffer.setFlatOfferOrderThreshold(1.00);
				custPromoOffer.setIsFlatDiscount(false);
				custPromoOffer.setMaxOrders(1);
				custPromoOffer.setModifiedDate(new Date());
				custPromoOffer.setPromoCode(custPromoCode);
				custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
				custPromoOffer.setNoOfOrders(0);
				custPromoOffer.setStatus("ENABLED");
				
				boolean isEmailSent = true;
				String toEmail = validateCustomer.getEmail();
				try{
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("promotionalCode", custPromoCode);
					mailMap.put("userName",validateCustomer.getEmail());
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, validateCustomer.getEmail(), 
							null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
				    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
					
				}catch(Exception e1){
					e1.printStackTrace();
					isEmailSent = false;
					toEmail = "";
				}
				custPromoOffer.setIsEmailSent(isEmailSent);
				custPromoOffer.setToEmail(toEmail);
				//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
				
				
				//List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(validateCustomer.getId());
				//List<UserAddress> billingAddress = new ArrayList<UserAddress>();
				//List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
				
				//if(userAddress != null){
				//	for(UserAddress address : userAddress){
				//		if(address.getAddressType() == AddressType.BILLING_ADDRESS)
				//			billingAddress.add(address);
				//		else
				//			shippingAddress.add(address);					
				//	}
				//}	
				
				customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
				
				if(null != customerLoyalty){
					
				}else{
					customerLoyalty = new CustomerLoyalty();
					customerLoyalty.setCustomerId(validateCustomer.getId());
					customerLoyalty.setActivePointsAsDouble(0.00);
					customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
					customerLoyalty.setLatestSpentPointsAsDouble(0.00);
					customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
					customerLoyalty.setTotalSpentPointsAsDouble(0.00);
					customerLoyalty.setLastUpdate(new Date());
					customerLoyalty.setLastNotifiedTime(new Date());
					customerLoyalty.setPendingPointsAsDouble(0.00);
					customerLoyalty.setTotalReferralDollars(0.00);
					customerLoyalty.setLastReferralDollars(0.00);
					customerLoyalty.setTotalAffiliateReferralDollars(0.00);
					customerLoyalty.setLastAffiliateReferralDollars(0.00);
					
					//Save Customer Loyalty Information
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
				}
				
				/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
				try {
					CassCustomerUtil.addCustomer(validateCustomer, customerLoyalty);
				}catch(Exception e) {
					e.printStackTrace();
				}
				/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
				
				
				//customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
				
				//customerDetails.setCustomerLoyalty(customerLoyalty);
				//customerDetails.setCustomer(validateCustomer);
				//customerDetails.getCustomer().setBillingAddress(billingAddress);
				//if(null !=shippingAddress && shippingAddress.size() >2){
				//	Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
				//}
				//customerDetails.getCustomer().setShippingAddress(shippingAddress);
				
				QuizOTPTracking otpTracking = generateOTPTracking(phone, error);
				if(otpTracking == null) {
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,error.getDescription());
					return customerDetails;
				}
				customerDetails.setLoginOTP(otpTracking.getResponseOtp());
				customerDetails.setOtpExpiryTimeInSeconds(QuizSettings.otpLiveSeconds);
				customerDetails.setOtpTrackingId(otpTracking.getId());
				customerDetails.setCustomerId(validateCustomer.getId());
				customerDetails.setMessage("Quiz Customer Signed Up and OTP Generated and Sent Successfully.");
				
				customerDetails.setStatus(1);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Success");
				return customerDetails;
			}else{
				if(null != validateCustomer){
					error.setDescription("Whoops! This email is already signed up. Please choose a different Email.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Whoops! This email is already signed up. Please choose a different Email.");
					return customerDetails;
				}
			}
			
			Customer customer = new Customer();
			
			//11-21-2018 3:22 PM - Give 1 life to all new registration. Given by Mitul - Done By Ulaganathan
			customer.setQuizCustomerLives(1);
			
			customer.setBrokerId(1001);
			customer.setIsClient(true);
			customer.setProductType(productType);
			customer.setSignupType(signupType);
			customer.setCustomerName(name);
			customer.setLastName(null!=lastName?lastName:"");
			//customer.setReferralCode(null!=referralCode?referralCode:"");
			customer.setEmail(email);
			customer.setEncryptPassword(finalPassword);
			customer.setUserName(email);
			customer.setSignupDate(new Date());
			customer.setCustomerType(CustomerType.WEB_CUSTOMER);
			customer.setApplicationPlatForm(applicationPlatForm);
			customer.setDeviceId(null != deviceId?deviceId:"");
			customer.setNotificationRegId(null!=notificationRegId?notificationRegId:"");
			customer.setSocialAccountId(null != socialAccountId?socialAccountId:"");
			
			customer.setUserId(userId);
			customer.setPhone(phone);
			
			/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
			String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
			Long referrerNumber =  Long.valueOf(property.getValue());
			referrerNumber++;  
			customer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
			property.setValue(String.valueOf(referrerNumber));
			DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
			customer.setReferrerCode(email);
			customer.setLastUpdatedDate(new Date());
			//Save Customer
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			
			if(customer.getCustImagePath() == null) {
				String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
				customer.setCustImagePath(fileName);
				DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
				CustomerUtil.updatedCustomerUtil(customer);
			}
			//property.setValue(String.valueOf(referrerNumber));
			
			//Save Customer Referral Code Auto Number
			//DAORegistry.getPropertyDAO().saveOrUpdate(property);
			
			/*Add new Customer to Customer Util - Begins*/
			try{
				CustomerUtil.updatedCustomerUtil(customer);
			}catch(Exception e){
				e.printStackTrace();
			}
			/*Add new Customer to Customer Util - Ends*/
			
			
			customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			if(null != customerLoyalty){
				
			}else{
				customerLoyalty = new CustomerLoyalty();
				customerLoyalty.setCustomerId(customer.getId());
				customerLoyalty.setActivePointsAsDouble(0.00);
				customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
				customerLoyalty.setLatestSpentPointsAsDouble(0.00);
				customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
				customerLoyalty.setTotalSpentPointsAsDouble(0.00);
				customerLoyalty.setLastUpdate(new Date());
				customerLoyalty.setLastNotifiedTime(new Date());
				customerLoyalty.setPendingPointsAsDouble(0.00);
				customerLoyalty.setTotalReferralDollars(0.00);
				customerLoyalty.setLastReferralDollars(0.00);
				customerLoyalty.setTotalAffiliateReferralDollars(0.00);
				customerLoyalty.setLastAffiliateReferralDollars(0.00);
				
				//Save Customer Loyalty Information
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			}
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(signupType);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			loginHistory.setSocialAccessToken(fbAccessToken);	
			
			//Save Customer Login History
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
				}
				//Save Customer Device Details
				DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
			
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Begins*/
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Ends*/
			}
			
			String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+email.split("@")[0]);
			
			Calendar calendar = new GregorianCalendar();
			calendar.add(Calendar.YEAR, 1);
			
			RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
			custPromoOffer.setCreatedDate(new Date());
			custPromoOffer.setCustomerId(customer.getId());
			custPromoOffer.setDiscount(25.00);
			custPromoOffer.setStartDate(new Date());
			custPromoOffer.setEndDate(calendar.getTime());
			custPromoOffer.setFlatOfferOrderThreshold(1.00);
			custPromoOffer.setIsFlatDiscount(false);
			custPromoOffer.setMaxOrders(1);
			custPromoOffer.setModifiedDate(new Date());
			custPromoOffer.setPromoCode(custPromoCode);
			custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
			custPromoOffer.setNoOfOrders(0);
			custPromoOffer.setStatus("ENABLED");
			
			boolean isEmailSent = true;
			String toEmail = customer.getEmail();
			
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("promotionalCode", custPromoCode);
			mailMap.put("userName",customer.getEmail());
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			
			try{
				switch (productType) {
					case REWARDTHEFAN:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
						break;
					
					case ZONETICKETS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case MINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case LASTROWMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case VIPMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.fromEmail, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
	
					default:
						break;
				}
			
			}catch(Exception e){
				e.printStackTrace();
				isEmailSent = false;
				toEmail = "";
			}
			
			custPromoOffer.setIsEmailSent(isEmailSent);
			custPromoOffer.setToEmail(toEmail);
			//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
			
			/*boolean customerPic = false;
			String profilePicPrefix =URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = AdminController.readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			
			QuizOTPTracking otpTracking = generateOTPTracking(phone, error);
			if(otpTracking == null) {
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,error.getDescription());
				return customerDetails;
			}
			customerDetails.setLoginOTP(otpTracking.getResponseOtp());
			customerDetails.setOtpExpiryTimeInSeconds(QuizSettings.otpLiveSeconds);
			customerDetails.setOtpTrackingId(otpTracking.getId());
			customerDetails.setCustomerId(customer.getId());
			customerDetails.setMessage("Quiz Customer Signed Up and OTP Generated and Sent Successfully.");
			
			//customerDetails.setCustomerLoyalty(customerLoyalty);
			//customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while creating customer.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERSIGNUP,"Error occured while creating customer");
			return customerDetails;
		}
	}
	
	@RequestMapping(value = "/GetContestPromoCode", method=RequestMethod.POST)
	public @ResponsePayload QuizPromoCodeInfo getContestPromoCode(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizPromoCodeInfo quizPromoCodeInfo =new QuizPromoCodeInfo();
		Error error = new Error();
		Date start = new Date();
		String platForm = request.getParameter("platForm");
		String customerIdStr = request.getParameter("customerId");
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizPromoCodeInfo.setError(authError);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,authError.getDescription());
				return quizPromoCodeInfo;
			}*/
			
			
			
			/*if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Application Platform is mandatory");
				return quizPromoCodeInfo;
			}*/
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					quizPromoCodeInfo.setError(error);
					quizPromoCodeInfo.setStatus(0);
					TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Please send valid application platform");
					return quizPromoCodeInfo;
				}
			}
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Customer Id is mandatory");
				return quizPromoCodeInfo;
			}*/
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Customer Id");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Invalid Customer Id");
				return quizPromoCodeInfo;
			}
			try {
				customer = CustomerUtil.getCustomerById(customerId);
				//customer = DAORegistry.getCustomerDAO().get(customerId);
				if(customer == null) {
					error.setDescription("Customer Id is Invalid");
					quizPromoCodeInfo.setError(error);
					quizPromoCodeInfo.setStatus(0);
					TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Customer Id is Invalid");
					return quizPromoCodeInfo;
				}
			} catch(Exception e) {
				error.setDescription("Customer Id Not Exist");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Customer Id Not Exist");
				return quizPromoCodeInfo;
			}
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Contest Id is mandatory");
				return quizPromoCodeInfo;
			}*/
			//String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			//Date now = new Date();
			/*List<QuizPromoCodeDetail> promoList = QuizDAORegistry.getQuizQueryManagerDAO().getAllValidContestPromoCodes(Integer.parseInt(customerIdStr));
			if(promoList != null && !promoList.isEmpty()) {
				for (QuizPromoCodeDetail quizPromoCodeDetail : promoList) {
					String timeDiff = QuizContestUtil.computeDaysHoursMinutes(now, quizPromoCodeDetail.getPromoExpiryDate());
					String promoText = "<font "+fontSizeStr+" color=#FFFFFF><b>"+quizPromoCodeDetail.getPromoCode()+"</b> for " +
							""+percentageDF.format(quizPromoCodeDetail.getDiscountPercentage())+"% off " +
							"<b>"+quizPromoCodeDetail.getPromoRefName()+"</b> expires in "+timeDiff+"</font>";
					quizPromoCodeDetail.setPromoText(promoText);
				}
			} else {
				if(customer.getIsProomoCode()) {
					customer.setIsProomoCode(false);
					DAORegistry.getCustomerDAO().update(customer);
					CustomerUtil.updatedCustomerUtil(customer);
				}
			}*/
			
			List<CustomerPromoRewardDetails> promoList = QuizDAORegistry.getQuizQueryManagerDAO().getAllValidContestPromoCodesForPromoPage(Integer.parseInt(customerIdStr));
			if(promoList == null || promoList.isEmpty()) {
				if(customer.getIsProomoCode()) {
					customer.setIsProomoCode(false);
					DAORegistry.getCustomerDAO().update(customer);
					CustomerUtil.updatedCustomerUtil(customer);
				}
			} else {
				for (CustomerPromoRewardDetails customerPromoRewardDetails : promoList) {
					if(customerPromoRewardDetails.getPromoRefType() != null && customerPromoRewardDetails.getPromoRefType().equals("ALL")) {
						customerPromoRewardDetails.setPromoRefType("PARENT");
					}
				}
			}
			quizPromoCodeInfo.setPromoCodeList(promoList);
			quizPromoCodeInfo.setStatus(1);
			//quizPromoCodeInfo.setPromoCodeList(promoList);
			
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Getting Contest Promocode.");
			quizPromoCodeInfo.setError(error);
			quizPromoCodeInfo.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCONTESTPROMOCODE,"Error occured while Getting Contest Promocode.");
			return quizPromoCodeInfo;
		}
		log.info("QUIZ CONT PROMOCODE : "+customerIdStr+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizPromoCodeInfo;
	}
	
	@RequestMapping(value = "/ApplyRewardEarnLife", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerReferralCodeInfo applyRewardEarnLife(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerReferralCodeInfo quizReferalInfo =new QuizCustomerReferralCodeInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizReferalInfo.setError(authError);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,authError.getDescription());
				return quizReferalInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String promoRefIdStr = request.getParameter("promoRefId");
			
			if(TextUtil.isEmptyOrNull(promoRefIdStr)){
				error.setDescription("Promo Ref Id is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,"Promo Ref Id is mandatory");
				return quizReferalInfo;
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,"Customer Id is mandatory");
				return quizReferalInfo;
			}
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,"Customer Id is Invalid");
				return quizReferalInfo;
			}
			CustomerPromoRewardDetails custPromoRefDtl = QuizDAORegistry.getCustomerPromoRewardDetailsDAO().getEarnLifePromoRewardByPromoRefIdandCustomerId(Integer.parseInt(promoRefIdStr), customer.getId());
			if(custPromoRefDtl == null ) {
				error.setDescription("Promo Ref Id is Invalid");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,"Promo Ref is Invalid");
				return quizReferalInfo;
			}
			if(!custPromoRefDtl.getStatus().equals("ACTIVE")) {
				error.setDescription("Reward LifeLine Promo Already Applied.");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,"Reward LifeLine Promo Already Applied.");
				return quizReferalInfo;
			}
			
			
			/*QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
			referraltracking.setCustomerId(customer.getId());
			referraltracking.setReferralCode(promoRewardIdStr);
			//referraltracking.setReferralCustomerId();
			referraltracking.setRtfPromotionalId(rtfReferral.getId());
			referraltracking.setCreatedDateTime(new Date());
			QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);*/
			
			Integer customerLives = customer.getQuizCustomerLives();
			if(customerLives == null) {
				customerLives=0;
			}
			customerLives = customerLives+custPromoRefDtl.getEarnLifeCount();
			customer.setQuizCustomerLives(customerLives);	
			
			CustomerUtil.updatedCustomerUtil(customer);
			DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
			
			/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.updateCustomerLives(customer);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
			
			custPromoRefDtl.setApplyDate(new Date());
			custPromoRefDtl.setStatus("COMPLETED");
			QuizDAORegistry.getCustomerPromoRewardDetailsDAO().update(custPromoRefDtl);
			
			//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(customer);
			
			quizReferalInfo.setQuizCustomerLives(customer.getQuizCustomerLives());
			quizReferalInfo.setStatus(1);
			quizReferalInfo.setMessage("Reward LifeLine Applied and Customer Lives Added Successfully.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Reward LifeLine.");
			quizReferalInfo.setError(error);
			quizReferalInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZAPPLYREWARDEARNLIFE,"Error occured while Updating Reward LifeLine.");
			return quizReferalInfo;
		}
		log.info("QUIZ UPDT Reward LifeLine : "+request.getParameter("customerId")+" :prefId: "+request.getParameter("promoRefId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizReferalInfo;
	
	}
	@RequestMapping(value = "/RedeemRewardTixPromo", method=RequestMethod.POST)
	public @ResponsePayload RedeemRewardTixPromoInfo redeemRewardTixPromo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		RedeemRewardTixPromoInfo redeemRewardTixInfo =new RedeemRewardTixPromoInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				redeemRewardTixInfo.setError(authError);
				redeemRewardTixInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,authError.getDescription());
				return redeemRewardTixInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String promoRefIdStr = request.getParameter("promoRefId");
			
			if(TextUtil.isEmptyOrNull(promoRefIdStr)){
				error.setDescription("Promo Ref Id is mandatory");
				redeemRewardTixInfo.setError(error);
				redeemRewardTixInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Promo Ref Id is mandatory");
				return redeemRewardTixInfo;
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				redeemRewardTixInfo.setError(error);
				redeemRewardTixInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Customer Id is mandatory");
				return redeemRewardTixInfo;
			}
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				redeemRewardTixInfo.setError(error);
				redeemRewardTixInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Customer Id is Invalid");
				return redeemRewardTixInfo;
			}
			CustomerPromoRewardDetails custPromoRefDtl = QuizDAORegistry.getCustomerPromoRewardDetailsDAO().getRewardTixPromoRewardByPromoRefIdandCustomerId(Integer.parseInt(promoRefIdStr), customer.getId());
			if(custPromoRefDtl == null) {
				error.setDescription("Promo Ref Id is Invalid");
				redeemRewardTixInfo.setError(error);
				redeemRewardTixInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Promo Ref is Invalid");
				return redeemRewardTixInfo;
			}
			if(!custPromoRefDtl.getStatus().equals("ACTIVE")) {
				error.setDescription("Reward Tix Promo Already Redeemd.");
				redeemRewardTixInfo.setError(error);
				redeemRewardTixInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Reward Tix Promo Already Redeemd.");
				return redeemRewardTixInfo;
			}
			
			
			custPromoRefDtl.setApplyDate(new Date());
			custPromoRefDtl.setStatus("APPLIED");
			QuizDAORegistry.getCustomerPromoRewardDetailsDAO().update(custPromoRefDtl);
			
			//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(customer);
			
			SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			String custPromoCode = custPromoRefDtl.getPromoCode();
			boolean isEmailSent = true;
			String toEmail = customer.getEmail();
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("promotionalCode", custPromoRefDtl.getPromoText()+" of "+custPromoRefDtl.getPromoName());
			mailMap.put("userName",customer.getEmail());
			//mailMap.put("expiryDate",dt.format(custPromoRefDtl.getPrmoExpiryDateStr()));
			mailMap.put("contestName",custPromoRefDtl.getPromoName());
			//mailMap.put("anchorTagHTML",URLUtil.getContestPromoCodeRedirectionUrl(contest.getPromoRefType(), contest.getPromoRefId(), contest.getPromoRefName()));
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForContestEmail();
			
			try{
				String subject = custPromoRefDtl.getPromoText()+" of "+custPromoRefDtl.getPromoName()+" Promo Redeemd successfully.";
				
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
						null,URLUtil.bccEmails, subject,
						"mail-reward-tix-redeem-promo.html", mailMap, "text/html", null,mailAttachment,null);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Error occured while Sending Reward Promo Tix email.");
				redeemRewardTixInfo.setError(error);
				redeemRewardTixInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Error occured while Sending Reward Promo Tix email.");
				return redeemRewardTixInfo;
			}
			
			redeemRewardTixInfo.setStatus(1);
			redeemRewardTixInfo.setMessage("Reward Tix Promo Claimed Successfully.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Reward Tix Promo.");
			redeemRewardTixInfo.setError(error);
			redeemRewardTixInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREDEEMREWARDTIXPROMO,"Error occured while Updating Reward Tix Promo.");
			return redeemRewardTixInfo;
		}
		log.info("QUIZ UPDT Reward Tix Promo : "+request.getParameter("customerId")+" :prefId: "+request.getParameter("promoRefId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return redeemRewardTixInfo;
	
	}
	
	@RequestMapping(value="/SendInviteFriendsEmail",method=RequestMethod.POST)
	public @ResponsePayload SendInviteFriendEmailInfo sendInviteFriendsEmail(HttpServletRequest request, Model model,HttpServletResponse response){
		SendInviteFriendEmailInfo sendInviteFrindEmail =new SendInviteFriendEmailInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				sendInviteFrindEmail.setError(authError);
				sendInviteFrindEmail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDINVITEFRIENDEMAIL,authError.getDescription());
				return sendInviteFrindEmail;
			}
			
			String toEmail = request.getParameter("toEmail");
			String customerIdStr = request.getParameter("customerId");
			String processType = request.getParameter("processType");
			
			if(TextUtil.isEmptyOrNull(toEmail)){
				error.setDescription("Please enter valid email address");
				sendInviteFrindEmail.setError(error);
				sendInviteFrindEmail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDINVITEFRIENDEMAIL,"Please enter valid email address");
				return sendInviteFrindEmail;
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please login or signup to invite friends");
				sendInviteFrindEmail.setError(error);
				sendInviteFrindEmail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDINVITEFRIENDEMAIL,"Please login or signup to invite friends");
				return sendInviteFrindEmail;
			}
			if(TextUtil.isEmptyOrNull(processType)){
				error.setDescription("Please enter valid process type");
				sendInviteFrindEmail.setError(error);
				sendInviteFrindEmail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDINVITEFRIENDEMAIL,"Please enter valid process type");
				return sendInviteFrindEmail;
			}
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer = CustomerUtil.getCustomerById(customerId);
				
				if(customer == null){
					error.setDescription("Customer is not recognized");
					sendInviteFrindEmail.setError(error);
					sendInviteFrindEmail.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDINVITEFRIENDEMAIL,"Customer is not recognized");
					return sendInviteFrindEmail;
				}
				
			}catch(Exception e){
				error.setDescription("Customer is not recognized");
				sendInviteFrindEmail.setError(error);
				sendInviteFrindEmail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDINVITEFRIENDEMAIL,"Customer is not recognized");
				return sendInviteFrindEmail;
			}
			
			if(processType.equals("INVITEFRIEND")) {
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("userId", customer.getUserId());
				mailMap.put("iosLink", "https://itunes.apple.com/in/app/reward-the-fan/id1140367203?mt=8\\n");
				mailMap.put("androidLink", "https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en_IN");
				
				//inline(image in mail body) image add code
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForReferralEmailTemplate();
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmailForPromo, toEmail, 
							null,URLUtil.bccEmails, "Reward The Fan Trivia Game Invitation!",
				    		"email-invite-friend-request.html", mailMap, "text/html", null,mailAttachment,null);
					
				}catch(Exception e){
					e.printStackTrace();
				}
			} else if (processType.equals("SHAREMYRANK")) {
				
				QuizContestWinners winner = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDateAndCustomerId(customerId);
				Integer rank = 0,tixCount=0;
				Double points=0.0;
				if(winner != null) {
					rank = winner.getRewardRank();
					points = winner.getRewardPoints();
					tixCount = winner.getRewardTickets();
				}
				String messageText="I just earned $"+TicketUtil.getRoundedValueString(points)+" Reward Dollars, " +
						" and won "+tixCount+" Tix by playing Reward The Fan! Im ranked #"+rank+" !";//
				
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("userId", customer.getUserId());
				mailMap.put("rankMsg", messageText);
				
				//inline(image in mail body) image add code
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForReferralEmailTemplate();
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmailForPromo, toEmail, 
							null,URLUtil.bccEmails, "Reward The Fan Trivia - Share My Rank!",
				    		"email-contest-share-my-rank.html", mailMap, "text/html", null,mailAttachment,null);
					
				}catch(Exception e){
					e.printStackTrace();
				}
			} else {
				error.setDescription("Invalid Process Type");
				sendInviteFrindEmail.setError(error);
				sendInviteFrindEmail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDINVITEFRIENDEMAIL,"Invalid Process Type");
				return sendInviteFrindEmail;
			}
			sendInviteFrindEmail.setMessage("Your Friend Invitation has been successfully sent");
			sendInviteFrindEmail.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Success");
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("It seems something is wrong with email which you have entered. Please verify again.");
			sendInviteFrindEmail.setError(error);
			sendInviteFrindEmail.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Error occured while Emailing Invite Friend Email.");
			return sendInviteFrindEmail;
		}
		log.info("QUIZ SEND INVITE FRND EMAIL : "+request.getParameter("customerId")+" :prosType: "+request.getParameter("processType")+" :toemail: "+request.getParameter("toEmail")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return sendInviteFrindEmail;
	}
	
	@RequestMapping(value = "/SendPromoCodeEmail", method=RequestMethod.POST)
	public @ResponsePayload QuizPromoCodeInfo sendPromoCodeEmail(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizPromoCodeInfo quizPromoCodeInfo =new QuizPromoCodeInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizPromoCodeInfo.setError(authError);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,authError.getDescription());
				return quizPromoCodeInfo;
			}*/
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			
			/*if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,"Customer Id is mandatory");
				return quizPromoCodeInfo;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,"Contest Id is mandatory");
				return quizPromoCodeInfo;
			}*/
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,"Customer Id is Invalid");
				return quizPromoCodeInfo;
			}
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null) {
				error.setDescription("Contest Id is Invalid");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,"Contest Id is Invalid");
				return quizPromoCodeInfo;
			}
			
			SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			String custPromoCode = contest.getPromoCode();
			boolean isEmailSent = true;
			String toEmail = customer.getEmail();
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("promotionalCode", custPromoCode);
			mailMap.put("userName",customer.getEmail());
			mailMap.put("expiryDate",dt.format(contest.getPromoExpiryDate()));
			mailMap.put("contestName",contest.getContestName());
			
			String promoRefType = contest.getPromoRefType();
			if(contest.getPromoRefType() != null && contest.getPromoRefType().equals("ALL")) {
				promoRefType = "PARENT";
			} 
			mailMap.put("anchorTagHTML",URLUtil.getContestPromoCodeRedirectionUrl(promoRefType, contest.getPromoRefId(), contest.getPromoRefName()));
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForContestEmail();
			
			try{
				String subject = "Promotion Code for "+contest.getContestName();
				
				mailManager.sendMailNow("text/html",URLUtil.fromEmailForPromo, customer.getEmail(), 
						null,URLUtil.bccEmails, subject,
						"mail-send-contest-promo-code.html", mailMap, "text/html", null,mailAttachment,null);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Error occured while Sending Contest Promocode email.");
				quizPromoCodeInfo.setError(error);
				quizPromoCodeInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,"Error occured while Sending Contest Promocode email.");
				return quizPromoCodeInfo;
			}
			
			quizPromoCodeInfo.setStatus(1);
			quizPromoCodeInfo.setMessage("PromoCode Email sent Successfully");
			//quizPromoCodeInfo.setPromoCodeList(promoList);
			
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Sending Contest Promocode in email.");
			quizPromoCodeInfo.setError(error);
			quizPromoCodeInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDPROMOCODEEMAIL,"Error occured while Sending Contest Promocode in email");
			return quizPromoCodeInfo;
		}
		log.info("QUIZ PROMO EMAIL: "+request.getParameter("customerId")+" :coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return quizPromoCodeInfo;
	}
	
	
	
	public static Error authorizationValidation(HttpServletRequest request) throws Exception {
		Error error = new Error();
		HttpSession session = request.getSession();
		String ip =(String)session.getAttribute("ip");
		String configIdString = request.getParameter("configId");
		Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
		
		if(!isAuthrozed){
			error.setDescription("You are not authorized.");
			//quizOTPDetails.setError(error);
			//quizOTPDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
			return error;
		}
		
		if(configIdString!=null && !configIdString.isEmpty()){
			/*try {
				if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
					error.setDescription("You are not authorized.");
					//quizOTPDetails.setError(error);
					//quizOTPDetails.setStatus(0);
					//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
					return error;
				
				}
			} catch (Exception e) {
				error.setDescription("You are not authorized.");
				//quizOTPDetails.setError(error);
				//quizOTPDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
				return error;
			}*/
		}else{
			error.setDescription("You are not authorized.");
			//quizOTPDetails.setError(error);
			//quizOTPDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
			return error;
		}
		return null;
	}
	
	public QuizOTPTracking generateOTPTracking(String phone,Error error) throws Exception {
		
		String otp = getOneTimePassword();
		Integer minute = QuizSettings.otpLiveSeconds / 60;
		
		QuizOTPTracking otpTracking = new QuizOTPTracking();
		String messageId = null;
		if(URLUtil.isProductionEnvironment) {
			messageId = TwilioSMSServices.sendOTP(phone, otp,minute);	
		} else {
			 messageId = phone+"-"+otp;//TwilioSMSServices.sendOTP(phone, otp,minute);
			 otpTracking.setResponseOtp(otp);
		}
		
		if(messageId == null){
			error.setDescription("Please enter valid phone number to verify OTP.");
			//customerDetails.setError(error);
			//customerDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Phone no is mandatory");
			return null;
		}
		
		otpTracking.setPhone(phone);
		otpTracking.setOtp(otp);
		otpTracking.setCreatedDate(new Date());
		otpTracking.setStatus("ACTIVE");
		otpTracking.setMessageId(messageId);
		QuizDAORegistry.getQuizOTPTrackingDAO().save(otpTracking);
		
		return otpTracking;
	}
	 
	
	
	@RequestMapping(value = "/CreateWinnerOrder", method=RequestMethod.POST)
	public @ResponsePayload QuizGenericResponse createOrderForWinner(HttpServletRequest request,HttpServletResponse response){
		QuizGenericResponse genericResponse =new QuizGenericResponse();
		genericResponse.setIsShippingAddress(true);
		Error error = new Error();
		Date start = new Date();
		try {
			String customerIdStr = request.getParameter("customerId");
			String eventIdStr = request.getParameter("eventId");
			String contestIdStr = request.getParameter("contestId");
			String platform = request.getParameter("platForm");
			String shippingAddressIdStr = request.getParameter("shippingAddressId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Customer Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send Customer Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Event Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send Event Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Contest Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send Contest Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(platform)){
				genericResponse.setStatus(0);
				error.setDescription("Please send platform.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send platform.");
				genericResponse.setError(error);
				return genericResponse;
			}
			
			
			ApplicationPlatForm platForm = null;
			Integer customerId=null;
			Integer eventId=null;
			Integer contestId=null;
			try {
				platForm = ApplicationPlatForm.valueOf(platform);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid platform.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid platform.");
				genericResponse.setError(error);
				return genericResponse;
			}
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Customer Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid Customer Id.");
				return genericResponse;
			}
			try {
				eventId = Integer.parseInt(eventIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Event Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid Event Id.");
				return genericResponse;
			}
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Contest Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid Contest Id.");
				return genericResponse;
			}
			
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer== null){
				genericResponse.setStatus(0);
				error.setDescription("Customer not found with given Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Customer not found with given Id.");
				return genericResponse;
			}
			Event event = DAORegistry.getEventDAO().get(eventId);
			if(event== null){
				genericResponse.setStatus(0);
				error.setDescription("Event not found with given Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Event not found with given Id.");
				return genericResponse;
			}
			CustomerLoyalty loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			ContestGrandWinner winner = QuizDAORegistry.getContestGrandWinnerDAO().getGrandWinnerByCustomerId(customerId,contestId);
			if(winner== null){
				genericResponse.setStatus(0);
				error.setDescription("Winner Record is not found for given customer.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Winner Record is not found for given customer.");
				return genericResponse;
			}
			
			
			if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please choose valid shipping address.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please choose valid shipping address");
				return genericResponse;
			}
			
			Integer shippingAddressId = null;
			UserAddress shipping = null;
			try {
				shippingAddressId = Integer.parseInt(shippingAddressIdStr.trim());
				shipping = DAORegistry.getUserAddressDAO().get(shippingAddressId);
			}catch(Exception e){
				genericResponse.setStatus(0);
				error.setDescription("Please choose valid shipping address.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please choose valid shipping address");
				return genericResponse;
			}
			
			
			Date today = new Date();
			
			CategoryTicketGroup tgGroup = new CategoryTicketGroup();
			tgGroup.setEventId(event.getEventId());
			
			tgGroup.setOriginalPrice(0.00);
			tgGroup.setSectionRange("TBD");
			tgGroup.setRowRange("TBD");
			tgGroup.setStatus(TicketStatus.SOLD);
			tgGroup.setShippingMethod("ETICKETS");
			tgGroup.setCreatedBy("Contest");
			tgGroup.setCreatedDate(today);
			tgGroup.setLastUpdatedDate(today);
			tgGroup.setSoldPrice(0.00);
			tgGroup.setProducttype(ProductType.REWARDTHEFAN);
			tgGroup.setSoldQuantity(winner.getRewardTickets());
			tgGroup.setQuantity(winner.getRewardTickets());
			tgGroup.setInternalNotes("Contest Ticket");
			tgGroup.setFacePrice(0.00);
			tgGroup.setCost(0.00);
			tgGroup.setWholeSalePrice(0.00);
			tgGroup.setRetailPrice(0.00);
			tgGroup.setBroadcast(true);
			tgGroup.setOriginalTax(0.00);
			tgGroup.setOriginalTaxPerc(0.00);
			tgGroup.setActualSection("TBD");
			tgGroup.setRow(null);
			tgGroup.setLongSection(null);
			tgGroup.setBrokerId(1001);
			
			/*CustomerOrderRequest orderRequest = new CustomerOrderRequest();
			//Setting up Ticket Original Price
			orderRequest.setTicketPrice(originalTicketPrice);
			orderRequest.setTicketOnDayBeforeTheEvent(false);
			orderRequest.setSessionId(sessionId);
			orderRequest.setOrderId(customerOrder.getId());
			orderRequest.setOrderLoyaltyHistoryId(customerLoyaltyHistory.getId());
			orderRequest.setOrderLoyaltyInfoId(customerLoyalty.getId());
			orderRequest.setOrderTotal(serverOrderTotal);
			orderRequest.setOrderType(orderType);
			orderRequest.setOriginalOrderTotal(originalOrderTotal);
			orderRequest.setAppPlatForm(customerOrder.getAppPlatForm());
			orderRequest.setCategoryTicketGroupId(categoryTicketGroupId);
			orderRequest.setCustomerId(customerId);
			orderRequest.setEventId(eventId);
			orderRequest.setInvoiceId(-1);
			orderRequest.setIsLongSale(false);
			orderRequest.setIsLoyalFan(isFanPrice);
			orderRequest.setPrimaryPayAmt(primaryPayAmt);
			orderRequest.setPrimaryPaymentMethod(primaryPaymentMethod);
			orderRequest.setPrimaryTransactionId(primaryTransactionId);
			orderRequest.setSecondaryPayAmt(secondaryPayAmt);
			orderRequest.setSecondaryPaymentMethod(secondaryPaymentMethod);
			orderRequest.setSecondaryTransactionId(secondaryTransactionId);
			orderRequest.setQuantity(customerOrder.getQty());
			orderRequest.setSoldPrice(soldPrice);
			orderRequest.setThirdPayAmt(customerOrder.getThirdPayAmtAsDouble());
			orderRequest.setThirdPaymentMethod(customerOrder.getThirdPaymentMethod());
			orderRequest.setThirdTransactionId(customerOrder.getThirdTransactionId());
			orderRequest.setTicketGroupId(null);
			
			String customerIpAddress = "";
			if(customerOrder.getAppPlatForm().equals(ApplicationPlatForm.IOS) || 
					customerOrder.getAppPlatForm().equals(ApplicationPlatForm.ANDROID)){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			orderRequest.setIpAddress(customerIpAddress);
			orderRequest.setStatus(OrderStatus.PAYMENT_PENDING);
			orderRequest.setCreatedTime(new Date());
			orderRequest.setUpdatedTime(new Date());*/
			
			
			
			Invoice invoice = new Invoice();
			invoice.setInvoiceTotal(0.00);
			invoice.setCustomerId(customerId);
			invoice.setStatus(InvoiceStatus.Outstanding);
			invoice.setInvoiceType("CONTEST");
			invoice.setCreatedDate(today);
			invoice.setLastUpdated(today);
			invoice.setCreatedBy(customer.getUserId());
			invoice.setRealTixMap("NO");
			invoice.setLastUpdatedBy(customer.getUserId());
			invoice.setIsSent(false);
			invoice.setTicketCount(winner.getRewardTickets());
			invoice.setShippingMethodId(1);
			invoice.setRealTixDelivered(false);
			invoice.setBrokerId(1001);
			
			
			CustomerOrder order = new CustomerOrder();
			order.setCustomer(customer);
			order.setQty(winner.getRewardTickets());
			order.setStatus(OrderStatus.ACTIVE);
			order.setSection("TBD");
			order.setEventId(event.getEventId());
			order.setEventName(event.getEventName());
			order.setEventDateTemp(event.getEventDate());
			order.setEventTime(event.getEventTime());
			order.setVenueId(event.getVenueId());
			order.setVenueName(event.getVenueName());
			order.setDiscountAmount(0.00);
			order.setCreateDateTemp(today);
			order.setLastUpdatedDateTemp(today);
			order.setProductType(ProductType.REWARDTHEFAN);
			order.setShippingMethod("ETICKETS");
			order.setVenueCity(event.getCity());
			order.setVenueState(event.getState());
			order.setVenueCountry(event.getCountry());
			order.setSectionDescription("");
			order.setOriginalOrderTotal(0.00);
			order.setPrimaryPaymentMethod(PaymentMethod.CONTEST_WINNER);
			
			Property property =  DAORegistry.getPropertyDAO().get("rtf.contest.order.payment.suffix");
			Long primaryTrxId =  Long.valueOf(property.getValue());
			primaryTrxId++;
			String trxId = PaginationUtil.contestOrderPaymentPrefix+"-"+contestId+"-"+primaryTrxId;
			property.setValue(String.valueOf(primaryTrxId));
			
			order.setPrimaryTransactionId(trxId);
			order.setVenueCategory(event.getVenueCategoryName());
			order.setAppPlatForm(platForm);
			order.setIsInvoiceSent(false);
			order.setOrderType(OrderType.CONTEST);
			order.setOrderTotalAsDouble(0.00);
			order.setPrimaryPayAmtAsDouble(0.00);
			order.setSecondaryPayAmtAsDouble(0.00);
			order.setTaxesAsDouble(0.00);
			order.setTicketSoldPriceAsDouble(0.00);
			order.setTicketPriceAsDouble(0.00);
			order.setThirdPayAmtAsDouble(0.00);
			order.setIsLongSale(false);
			order.setIsTicketDownloadSent(false);
			order.setIsOrderCancelSent(false);
			order.setBrokerId(1001);
			order.setBrokerServicePerc(0.00);
			order.setIsPromoCodeApplied(false);
			order.setShowDiscPriceArea(false);
			order.setNormalTixPrice(0.00);
			order.setDiscountedTixPrice(0.00);
			Date date = new DateTime(event.getEventDate()).minusDays(1).toDate();
			order.setShippingDateTemp(date);
			order.setTicketOnDayBeforeTheEvent(false);
			
			/*Ulaganathan : Order Payment breakups - Begins*/
			OrderPaymentBreakup paymentBreakUp = new OrderPaymentBreakup();
			paymentBreakUp.setPaymentAmount(0.00);
			paymentBreakUp.setPaymentMethod(PaymentMethod.CONTEST_WINNER);
			paymentBreakUp.setTransactionId(trxId);
			paymentBreakUp.setDoneBy("Customer");
			paymentBreakUp.setOrderSequence(1);
			paymentBreakUp.setPaymentDate(new Date());
			/*Ulaganathan : Order Payment breakups - Ends*/
			
			//List<UserAddress> billings = DAORegistry.getUserAddressDAO().getUserAddressInfo(customerId, AddressType.BILLING_ADDRESS);
			Boolean isShippingAddress = true,updateCustTopLevelObj = false;
			if(shipping == null ){
				genericResponse.setIsShippingAddress(false);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Customer Shipping address is not found.");
				isShippingAddress =false;
			}
			
			CustomerOrderDetail orderDetail = new CustomerOrderDetail();
			orderDetail.setBillingAddress1("");
			orderDetail.setBillingAddress2("");
			orderDetail.setBillingCity("");
			orderDetail.setBillingCountry("");
			orderDetail.setBillingCountryId(null);
			orderDetail.setBillingEmail("");
			orderDetail.setBillingFirstName("");
			orderDetail.setBillingLastName("");
			orderDetail.setBillingPhone1("");
			orderDetail.setBillingPhone2("");
			orderDetail.setBillingState("");
			orderDetail.setBillingStateId(null);
			orderDetail.setBillingZipCode("");
			if(isShippingAddress){
				
				if(null == customer.getCustomerName() || customer.getCustomerName().isEmpty()){
					customer.setCustomerName(shipping.getFirstName());
					customer.setLastName(shipping.getLastName());
					
					updateCustTopLevelObj = true;
				}
				
				orderDetail.setShippingAddress1(shipping.getAddressLine1());
				orderDetail.setShippingAddress2(shipping.getAddressLine2());
				orderDetail.setShippingCity(shipping.getCity());
				orderDetail.setShippingCountry(shipping.getCountryName());
				orderDetail.setShippingCountryId(shipping.getCountry().getId());
				orderDetail.setShippingEmail(shipping.getEmail());
				orderDetail.setShippingFirstName(shipping.getFirstName());
				orderDetail.setShippingLastName(shipping.getLastName());
				orderDetail.setShippingPhone1(shipping.getPhone1());
				orderDetail.setShippingPhone2(shipping.getPhone2());
				orderDetail.setShippingState(shipping.getStateName());
				orderDetail.setShippingStateId(shipping.getState().getId());
				orderDetail.setShippingZipCode(shipping.getZipCode());
			}else{
				orderDetail.setShippingAddress1("");
				orderDetail.setShippingAddress2("");
				orderDetail.setShippingCity("");
				orderDetail.setShippingCountry("");
				orderDetail.setShippingCountryId(null);
				orderDetail.setShippingEmail("");
				orderDetail.setShippingFirstName("");
				orderDetail.setShippingLastName("");
				orderDetail.setShippingPhone1("");
				orderDetail.setShippingPhone2("");
				orderDetail.setShippingState("");
				orderDetail.setShippingStateId(null);
				orderDetail.setShippingZipCode("");
			}
			
			CustomerLoyaltyHistory history = new CustomerLoyaltyHistory();
			history.setCustomerId(customerId);
			history.setOrderTotal(0.00);
			history.setRewardConversionRate(0.01);
			history.setRewardEarnAmount(0.00);
			history.setRewardSpentAmount(0.00);
			history.setInitialRewardAmount(loyalty.getActivePointsAsDouble());
			history.setBalanceRewardAmount(loyalty.getActivePointsAsDouble());
			history.setDollarConversionRate(0.00);
			history.setOrderType(OrderType.CONTEST);
			history.setRewardStatus(RewardStatus.ACTIVE);
			history.setUpdatedDate(today);
			history.setCreateDate(today);
			history.setPointsEarnedAsDouble(0.00);
			history.setPointsSpentAsDouble(0.00);
			history.setInitialRewardPointsAsDouble(loyalty.getActivePointsAsDouble());
			history.setBalanceRewardPointsAsDouble(loyalty.getActivePointsAsDouble());
			history.setIsRewardsEmailSent(true);
			history.setEmailDescription("Email Not require.");
			
			try {
				DAORegistry.getInvoiceDAO().save(invoice);
				
				tgGroup.setInvoiceId(invoice.getId());
				DAORegistry.getCategoryTicketGroupDAO().save(tgGroup);
				
				order.setCategoryTicketGroupId(tgGroup.getId());
				DAORegistry.getCustomerOrderDAO().save(order);
				
				paymentBreakUp.setOrderId(order.getId());
				DAORegistry.getOrderPaymentBreakupDAO().save(paymentBreakUp);
				
				orderDetail.setOrderId(order.getId());
				DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
				
				history.setOrderId(order.getId());
				DAORegistry.getCustomerLoyaltyHistoryDAO().save(history);
				
				invoice.setCustomerOrderId(order.getId());
				DAORegistry.getInvoiceDAO().update(invoice);
				
				//update contest order payment transaction id 
				DAORegistry.getPropertyDAO().update(property);
				
				if(updateCustTopLevelObj){
					DAORegistry.getCustomerDAO().update(customer);
					try{
						CustomerUtil.updatedCustomerUtil(customer);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				List<CategoryTicket> tickets = new ArrayList<CategoryTicket>();
				for(int i=0;i<order.getQty();i++){
					CategoryTicket catTicket = new CategoryTicket();
					catTicket.setActualPrice(0.00);
					catTicket.setIsProcessed(true);
					catTicket.setSoldPrice(0.00);
					catTicket.setUserName("AUTO");
					catTicket.setCategoryTicketGroupId(tgGroup.getId());
					catTicket.setInvoiceId(invoice.getId());
					tickets.add(catTicket);
				}
				DAORegistry.getCategoryTicketDAO().saveAll(tickets);
				
				/*try{
					MapUtil.copySVGMapandText(event.getVenueId(), event.getVenueCategoryName(),order.getId());
				}catch(Exception e){
					e.printStackTrace();
				}*/
				
				winner.setStatus(WinnerStatus.ORDERED);
				winner.setOrderId(order.getId());
				QuizDAORegistry.getContestGrandWinnerDAO().update(winner);
				
				//Tamil: refresh customer is_contest_order for dash board
				List<ContestGrandWinner> grandWiners = QuizDAORegistry.getQuizQueryManagerDAO().getAllActiveGrandWinnersByCustomerId(customerId);
				Boolean isContestOrder = false;
				if(grandWiners != null && !grandWiners.isEmpty()) {
					isContestOrder = true;
				}
				if(!customer.getIsContestOrder().equals(isContestOrder)) {
					customer.setIsContestOrder(isContestOrder);
					DAORegistry.getCustomerDAO().update(customer);
					try{
						CustomerUtil.updatedCustomerUtil(customer);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				genericResponse.setStatus(1);
				genericResponse.setOrderId(order.getId());
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Error Occured while creating order.");
				genericResponse.setMessage("Your Order is created, OrderNo is "+order.getId());
				log.info("QUIZ ORDER CREATE 1 : "+request.getParameter("customerId")+" :coId: "+request.getParameter("contestId")+" :coId: "+request.getParameter("eventId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
				return genericResponse;
				
				
			} catch (Exception e) {
				e.printStackTrace();
				genericResponse.setStatus(0);
				error.setDescription("Error Occured while creating order.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Error Occured while creating order.");
				genericResponse.setError(error);
				return genericResponse;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("QUIZ ORDER CREATE 2 : "+request.getParameter("customerId")+" :coId: "+request.getParameter("contestId")+" :coId: "+request.getParameter("eventId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return genericResponse;
	}
	
	@RequestMapping(value = "/SubmitTrivia", method=RequestMethod.POST)
	public @ResponsePayload QuizGenericResponse submitContestQuestion(HttpServletRequest request,HttpServletResponse response){
		QuizGenericResponse genericResponse =new QuizGenericResponse();
		Error error = new Error();
		Date start = new Date();
		try {
			String questionText = request.getParameter("questionText");
			String optionA = request.getParameter("optionA");
			String optionB = request.getParameter("optionB");
			String optionC = request.getParameter("optionC");
			String customerIdStr = request.getParameter("customerId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Customer Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Customer Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			Integer customerId=null;
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Customer Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send valid Customer Id.");
				return genericResponse;
			}
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer== null){
				genericResponse.setStatus(0);
				error.setDescription("Customer not found with given Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Customer not found with given Id.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(questionText)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Question Text.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Question Text.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(optionA)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Answer.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Answer.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(optionB)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Option B.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Option B.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(optionC)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Option C.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Option C.");
				return genericResponse;
			}
			
			CustomerContestQuestion question = new CustomerContestQuestion();
			question.setQuestionText(questionText);
			question.setOptionA(optionA);
			question.setOptionB(optionB);
			question.setOptionC(optionC);
			question.setCreatedDate(new Date());
			question.setAnswer("A");
			question.setSubmittedBy(customer.getUserId());
			
			QuizDAORegistry.getCustomerContestQuestionDAO().save(question);
			genericResponse.setStatus(1);
			genericResponse.setMessage("Your Trivia Question submitted.");
			return genericResponse;
			
		} catch (Exception e) {
			e.printStackTrace();
			genericResponse.setStatus(0);
			error.setDescription("Error Occured while Submitting trivia.");
			genericResponse.setError(error);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Error Occured while Submitting trivia.");
		}
		log.info("QUIZ SBT TRIVIA : "+request.getParameter("customerId")+" :qtex: "+request.getParameter("questionText")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return genericResponse;
	}
	
	
	@RequestMapping(value = "/RequestShow", method=RequestMethod.POST)
	public @ResponsePayload QuizGenericResponse requestShow(HttpServletRequest request,HttpServletResponse response){
		QuizGenericResponse genericResponse =new QuizGenericResponse();
		Error error = new Error();
		Date start = new Date();
		try {
			String eventTitle = request.getParameter("eventTitle");
			String eventDescription = request.getParameter("eventDescription");
			String eventLocation = request.getParameter("eventLocation");
			String socialMediaLink = request.getParameter("socialMediaLink");
			String customerIdStr = request.getParameter("customerId");
			String contactName = request.getParameter("contactName");
			String contactEmail = request.getParameter("contactEmail");
			String contactPhone = request.getParameter("contactPhone");
			String eventDateStr = request.getParameter("eventDate");
			String noOfTicketsStr = request.getParameter("noOfTickets");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Customer Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Customer Id.");
				return genericResponse;
			}
			Integer customerId=null;
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Customer Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send valid Customer Id.");
				return genericResponse;
			}
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer== null){
				genericResponse.setStatus(0);
				error.setDescription("Customer not found with given Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Customer not found with given Id.");
				return genericResponse;
			}
			
			if(TextUtil.isEmptyOrNull(eventTitle)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Event Title.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Event Title.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(eventDateStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Event Date.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Event Date.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(eventDescription)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Event Description.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Event Description.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(eventLocation)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Event Location.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Event Location.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(socialMediaLink)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Social Media Link.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Social Media Link.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(contactEmail)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Contact Email.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Contact Email.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(contactName)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Contact Name.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Contact Name.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(contactPhone)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Contact Phone.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Contact Phone.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(noOfTicketsStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send # of tickets.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send # of tickets.");
				return genericResponse;
			}
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Date eventDate = null;
			Integer tickets = null;
			try {
				eventDate = df.parse(eventDateStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send Contact Phone.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send Contact Phone.");
				return genericResponse;
			}
			try {
				tickets = Integer.parseInt(noOfTicketsStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid # of tickets.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Please send valid # of tickets.");
				return genericResponse;
			}
			
			ContestEventRequest eventRequest = new ContestEventRequest();
			eventRequest.setContactEmail(contactEmail);
			eventRequest.setNoOfTickets(tickets);
			eventRequest.setContactName(contactName);
			eventRequest.setContactPhone(contactPhone);
			eventRequest.setCreatedBy(customer.getUserId());
			eventRequest.setCreatedDate(new Date());
			eventRequest.setEventDate(eventDate);
			eventRequest.setEventDescription(eventDescription);
			eventRequest.setEventLocation(eventLocation);
			eventRequest.setEventTitle(eventTitle);
			eventRequest.setSocialLink(socialMediaLink);
			
			QuizDAORegistry.getContestEventRequestDAO().save(eventRequest);
			genericResponse.setStatus(1);
			genericResponse.setMessage("Your Trivia show request is submitted.");
			
			log.info("QUIZ REQ SHOW : "+request.getParameter("customerId")+" :coId: "+request.getParameter("eventTitle")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
			
			return genericResponse;
			
			
		} catch (Exception e) {
			e.printStackTrace();
			genericResponse.setStatus(0);
			error.setDescription("Error Occured while Submitting Show Request.");
			genericResponse.setError(error);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZREQUESTSHOW,"Error Occured while Submitting Show Request.");
		}
		return genericResponse;
	}
	
	
	
	
	
	
	public static String getOneTimePassword() throws Exception {
		Random random = new Random();
		return ""+random.nextInt(9)+""+random.nextInt(9)+""+random.nextInt(9)+""+random.nextInt(9);
	}
	
	public static void main1(String[] args) {
		
		Date date = new Date();
		Date dateTwo = new Date();
		dateTwo.setMinutes(dateTwo.getMinutes()-7100);
		
		String text = QuizContestUtil.computeDaysHoursMinutes(dateTwo, date);
		log.info(text+" : "+date+" : "+dateTwo);
		/*long secs = (date.getTime() - dateTwo.getTime()) / 1000;
		long days = secs / 86400;
		secs = secs % 86400;
		long hours = secs / 3600;    
		secs = secs % 3600;
		long mins = secs / 60;
		secs = secs % 60;
		log.info(" :days : "+days+" :hours : "+hours+" :mins :  "+mins +" : "+date+" : "+dateTwo);*/
		
		//int value = random.nextInt(9);
		/*String otp = "";
		for (int i = 0; i < 10; i++) {
			Random random = new Random();
		otp = ""+random.nextInt(9)+""+random.nextInt(9)+""+random.nextInt(9)+""+random.nextInt(9);
		log.info(""+otp);
		}*/
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 4);
		log.info(cal.get(Calendar.DAY_OF_WEEK)+" : "+cal.getTime());
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		dayOfWeek = 1-dayOfWeek;
		cal.add(Calendar.DAY_OF_MONTH, dayOfWeek);
		log.info(""+cal.getTime());
		
	}
	

	
	@RequestMapping(value = "/GetContestEvents", method=RequestMethod.POST)
	public @ResponsePayload ContestEventResponse getContestEvents(HttpServletRequest request,HttpServletResponse response,Model model){
		ContestEventResponse apiResponse =new ContestEventResponse();
		Error error = new Error();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				apiResponse.setError(authError);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,authError.getDescription());
				return apiResponse;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String artistIdStr = request.getParameter("artistId");
			String searchKey = request.getParameter("searchKey");
			String state = request.getParameter("state");
			String platForm = request.getParameter("platForm");
			String pageNumberStr = request.getParameter("pageNumber");
			String startDateStr = request.getParameter("startDate");
			String endDateStr = request.getParameter("endDate");
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Customer Id is mandatory");
				return apiResponse;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Contest Id is mandatory");
				return apiResponse;
			}
			
			Integer contestId = null;
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Contest Id");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Invalid Contest Id");
				return apiResponse;
			}
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			if(contest == null) {
				error.setDescription("Contest Id is Invalid");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Contest Id is Invalid");
				return apiResponse;
			}
			
			String contestName = null != contest.getExtendedName() && !contest.getExtendedName().isEmpty()?contest.getExtendedName():contest.getContestName();
			apiResponse.setContestName(contestName);
			apiResponse.setQuantity(contest.getFreeTicketsPerWinner());
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Customer Id is Invalid");
				return apiResponse;
			}
			
			Integer artistId = null;
			if(!TextUtil.isEmptyOrNull(artistIdStr)){
				
				try{
					artistId = Integer.parseInt(artistIdStr.trim());
				}catch(Exception e){
					error.setDescription("Invalid artist Id");
					apiResponse.setError(error);
					apiResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Invalid artist Id");
					return apiResponse;
				}
			}
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			
			Date eventStartDate = null, eventEndDate=null;
			if(startDateStr !=null && !startDateStr.isEmpty()){
				try{
					eventStartDate = df.parse(startDateStr.trim());
				}catch(Exception e){
					error.setDescription("Valid Start Date Format is MM/dd/yyyy");
					apiResponse.setError(error);
					apiResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Valid Start Date Format is MM/dd/yyyy");
					return apiResponse;
				}
			}
			
			if(endDateStr !=null && !endDateStr.isEmpty()){
				try{
					eventEndDate = df.parse(endDateStr.trim());
				}catch(Exception e){
					error.setDescription("Valid End Date Format is MM/dd/yyyy");
					apiResponse.setError(error);
					apiResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Valid End Date Format is MM/dd/yyyy");
					return apiResponse;
				}
			}
			if(TextUtil.isEmptyOrNull(pageNumberStr)){
				error.setDescription("Page Number is mandatory.");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Page Number is mandatory");
				return apiResponse;
			}
			
			Integer pageNumber = null;
			try{
				pageNumber = Integer.parseInt(pageNumberStr.trim());
			}catch(Exception e){
				error.setDescription("Page Number Should be Integer");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Page Number Should be Integer");
				return apiResponse;
			}
			List<Event> events = new ArrayList<Event>();
			Integer maxRows = PaginationUtil.contestTicketsMaxRow;;//
			
			/*Map<Integer, Boolean> favouriteEventMap = new HashMap<Integer, Boolean>();
			Map<Integer, Boolean> favouriteArtistMap = new HashMap<Integer, Boolean>();
			List<Integer> unFavoritedEventIds = new ArrayList<Integer>();
			if(null != customerIdStr && !customerIdStr.isEmpty()){
				favouriteEventMap = CustomerFavoriteEventUtil.getFavoriteEventFlagByCustomerId(Integer.parseInt(customerIdStr.trim()));
				favouriteArtistMap = CustomerFavoriteUtil.getFavoriteArtistByCustomerId(Integer.parseInt(customerIdStr.trim()));
				unFavoritedEventIds = CustomerFavoriteEventUtil.getUnFavoritedEventIdsByCustomerId(Integer.parseInt(customerIdStr.trim()));
				customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim())); //DAORegistry.getCustomerDAO().get(Integer.parseInt(customerIdStr.trim()));
			}*/
			
			int totalEventCount = 0;
			
			
			events = DAORegistry.getEventDAO().getAllContestEventsByCriteria(contestId, pageNumber, maxRows, eventStartDate, eventEndDate,state,searchKey);

			if(pageNumber <= 1 && (null == events || events.isEmpty())) {
				String zones = "";
				if(null != contest.getZone() && !contest.getZone().isEmpty()){
					String[] temp = contest.getZone().split(",");
					int i=0;
					for (String zone : temp) {
						if(i==0){
							zones = "'"+zone+"'";
							i++;
							continue;
						}
						zones = zones+",'"+zone+"'";
						i++;
					}
					
				}
				Double tixPrice = 0.00;
				if(null != contest.getSingleTicketPrice() && contest.getSingleTicketPrice() > 0){
					tixPrice = contest.getSingleTicketPrice();
				}
				
				events = DAORegistry.getEventDAO().getAllEventsByContestId(contestId, pageNumber, maxRows, 
						eventStartDate, eventEndDate,state,searchKey,zones,tixPrice,contest.getFreeTicketsPerWinner());
			}
			
			NormalSearchResult normalSearchResult = new NormalSearchResult();
			List<Event> normalSearchEvents = new ArrayList<Event>();
			int maxEventCount = 0,i=0;
			
			for (Event event : events) {
				
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}
				
				List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
				if(null == artistIds || artistIds.isEmpty()){
					continue;
				}
				event.setArtistIds(artistIds);
				
				//MapUtil.getEventSvgDetails(event);
				event.setEventImageUrl(applicationPlatForm);
				URLUtil.getShareLinkURL(event, customer,applicationPlatForm);*/
				
				totalEventCount = event.getTotalEvents();
				
				normalSearchEvents.add(event);
				maxEventCount++;
				i++;
			}
			 
			boolean showMoreEvents;
			if(maxRows <= maxEventCount) {
				showMoreEvents = true;
			} else {
				showMoreEvents = false;
			}
			//normalSearchResult.setMaxEventsPerPage(maxRows);
			normalSearchResult.setTotalEventPages(PaginationUtil.getTotalNoOfPages(totalEventCount, maxRows));
			normalSearchResult.setTotalEventCount(maxEventCount);
			normalSearchResult.setEventShowMore(showMoreEvents);
			normalSearchResult.setEvents(normalSearchEvents);
			apiResponse.setStatus(1);
			apiResponse.setContestId(contestId);
			apiResponse.setNormalSearchResult(normalSearchResult);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching contest event Details.");
			apiResponse.setError(error);
			apiResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Error occured while Fetching contest event Details.");
			return apiResponse;
		}
		
		return apiResponse;
	}
	
	
	@RequestMapping(value = "/SendStaticEmailForDollarRevert", method=RequestMethod.GET)
	public @ResponsePayload QuizPromoCodeInfo sendStaticEmail(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizPromoCodeInfo quizPromoCodeInfo =new QuizPromoCodeInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			
			SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			/*List<String> emailIds = new ArrayList<String>();
			emailIds.add("douglascarr@gmail.com");
			emailIds.add("msb574@nyu.edu");
			emailIds.add("bethanysrose@gmail.com");
			emailIds.add("winkdtm@gmail.com");
			emailIds.add("doc.status@gmail.com");
			emailIds.add("bpsfinest@gmail.com");*/

			String email = request.getParameter("email");
			String subject = request.getParameter("subject");
			
			if(null == subject || subject.isEmpty()) {
				subject = "Welcome to a Reward the Fan";
			}
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("email", email);
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForContestEmail();
			try{
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
						null,URLUtil.bccEmails, subject, "mail-send-contest-static-email-dollar-revert.html", 
						mailMap, "text/html", null,mailAttachment,null);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			quizPromoCodeInfo.setStatus(1);
			quizPromoCodeInfo.setMessage("Email sent Successfully To: "+email);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Sending Contest Promocode in email.");
			quizPromoCodeInfo.setError(error);
			quizPromoCodeInfo.setStatus(0);
			return quizPromoCodeInfo;
		}
		
		return quizPromoCodeInfo;
	}
	
	
	@RequestMapping(value = "/AddContestTickets", method=RequestMethod.GET)
	public @ResponsePayload ContestTicketsApiResponse addContestTickets(HttpServletRequest request,HttpServletResponse response,Model model){
		ContestTicketsApiResponse dtoResponse =new ContestTicketsApiResponse();
		Error error = new Error();
		try {
			String contestIdStr = request.getParameter("contestId");
			if(TextUtil.isEmptyOrNull(contestIdStr)) {
				error.setDescription("Contest id is mandatory.Quick Attention Required!");
				dtoResponse.setStatus(1);
				dtoResponse.setError(error);
				return dtoResponse;
			}
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr.trim()));
			if(contest == null) {
				error.setDescription("Invalid contest.Quick Attention Required!");
				dtoResponse.setStatus(1);
				dtoResponse.setError(error);
				return dtoResponse;
			}
			try {
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", PROC BEGIN TIME: "+new Date());
				Boolean isDone = DAORegistry.getEventDAO().callContestGrandWinnerProcedure(contest.getId(),0);
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", PROC END TIME: "+new Date());
			}catch(Exception e) {
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", EXCEPTION: "+new Date());
				e.printStackTrace();
				error.setDescription("Exception while adding conest tickets.Quick Attention Required!");
				dtoResponse.setError(error);
				dtoResponse.setStatus(0);
				return dtoResponse;
				
			}
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Tickets added successfully.!");
			dtoResponse.setContestId(Integer.parseInt(contestIdStr.trim()));
			dtoResponse.setQuizContest(contest);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while adding conest tickets.Quick Attention Required!");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		
		return dtoResponse;
	}
	
	@RequestMapping(value = "/RemoveAllOrderedContestTickets", method=RequestMethod.GET)
	public @ResponsePayload ContestTicketsApiResponse removeAllOrderedContestTickets(HttpServletRequest request,HttpServletResponse response,Model model){
		ContestTicketsApiResponse dtoResponse =new ContestTicketsApiResponse();
		Error error = new Error();
		try {
			 
			QuizGrandWinnerTicketsUtil util = new QuizGrandWinnerTicketsUtil();
			util.init();
			
			QuizGrandWinnerTicketsUpcomingUtil util2 = new QuizGrandWinnerTicketsUpcomingUtil(); 
			util2.init();
			
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Tickets removed successfully.!");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while removing conest tickets.Quick Attention Required!");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		
		return dtoResponse;
	}
	
	@RequestMapping(value = "/GetCustomerStatsByUserId", method=RequestMethod.POST)
	public @ResponsePayload CustomerStats getCustomerSummaryByUserId(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			String userIdStr = request.getParameter("userId");
			if(TextUtil.isEmptyOrNull(userIdStr)) {
				error.setDescription("User id is mandatory.");
				dtoResponse.setStatus(0);
				dtoResponse.setError(error);
				return dtoResponse;
			}

			Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userIdStr);
			
			if(null == customer) {
				error.setDescription("Invalid Customer.UserId is not valid.");
				dtoResponse.setStatus(0);
				dtoResponse.setError(error);
				return dtoResponse;
			}
			
			List<Integer> customerIds = new ArrayList<Integer>();
			Set<Integer> tireOneCustomerIds = new HashSet<Integer>();
			
			customerIds.add(customer.getId());
			
			tireOneCustomerIds.add(customer.getId());
			
			QuizCustomerReferralTracking rootTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
			
			if(null != rootTracking) {
				
				QuizCustomerReferralTracking primaryCustTracking  = null;
				
				boolean getTracking = true;
				
				customerIds.add(rootTracking.getReferralCustomerId());
				
				Integer primaryCustomerId = rootTracking.getReferralCustomerId();
				
				while (getTracking) {
					
					primaryCustTracking  = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(primaryCustomerId);
					
					if(null == primaryCustTracking || !tireOneCustomerIds.add(primaryCustTracking.getCustomerId())) {
						getTracking = false;
					}else {
						customerIds.add(primaryCustTracking.getReferralCustomerId());
						primaryCustomerId = rootTracking.getReferralCustomerId();
					}
				}
				
			}
			
			List<Integer> botIds = RTFBotsUtil.getAllBots();
			
			CustomerStatistics statistics = null;
			List<CustomerStatistics> list = new ArrayList<CustomerStatistics>();
			
			Integer totalQuestionCount = QuizDAORegistry.getQuizQueryManagerDAO().getTotalNoOfContestQuestions();
			
			SimpleDateFormat dateFormat =  new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			for (Integer customerId : customerIds) {
				
				try {
					Customer customerObj = DAORegistry.getCustomerDAO().get(customerId);
					
					statistics = new CustomerStatistics();
					statistics.setFullName(customerObj.getCustomerName()+" "+customerObj.getLastName());
					statistics.setCustomerId(customerObj.getId());
					statistics.setUserId(customerObj.getUserId());
					statistics.setEmail(customerObj.getEmail());
					statistics.setPhoneNo(customerObj.getPhone());
					statistics.setIsOtpVerified(customerObj.getIsOtpVerified()?"YES":"NO");
					statistics.setReferrerCode(customerObj.getUserId());
					statistics.setSignupDate(dateFormat.format(customerObj.getSignupDate()));
					statistics.setSignUpDeviceId(customerObj.getDeviceId());
					statistics.setQuizCustomerLives(customerObj.getQuizCustomerLives());
					//statistics.setQuizNoOfPointsWon(customerObj.getQuizNoOfPointsWon());
					//statistics.setQuizNoOfTicketsWon(customerObj.getQuizNoOfTicketsWon());
					statistics.setTotalContestPlayed(customerObj.getTotalContestPlayed());
					statistics.setTotalContestWins(customerObj.getTotalContestWins());
					
					Integer correctAnswerCount = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getCorrectAnsweredQuestionByCustomer(customerObj.getId());
					Integer ansQuesCount = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getNoOfQuesAnsweredByCustomer(customerObj.getId());
					
					statistics.setNoOfQuestionAnswered(ansQuesCount);
					statistics.setCorrectAnsQuestions(correctAnswerCount);
					statistics.setTotalNoOfQuestions(totalQuestionCount);
					
					Integer totalNoOfReferrals = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getTotalNoOfReferrals(customerObj.getId());
					
					statistics.setNoOfCustomerReferred(totalNoOfReferrals);
					
					CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerObj.getId());
					
					Double activeRewards = customerLoyalty.getActivePointsAsDouble();
					if(null == activeRewards) {
						activeRewards = 0.0;
					}
					
					Double pendingRewards = customerLoyalty.getPendingPointsAsDouble();
					if(null == pendingRewards) {
						pendingRewards = 0.0;
					}
					
					Double referralPoints = customerLoyalty.getTotalReferralDollars();
					if(null == referralPoints) {
						referralPoints = 0.0;
					}
					
					Double spentPoints = DAORegistry.getCustomerLoyaltyHistoryDAO().getSpentPoints(customerObj.getId());
					if(null == spentPoints) {
						spentPoints = 0.0;
					}
					
					Double contestGamePoints = DAORegistry.getCustomerLoyaltyTrackingDAO().getContestPoints(customerObj.getId());
					if(null == contestGamePoints) {
						contestGamePoints = 0.0;
					}
					
					Double purchasePoints = (activeRewards + spentPoints) - (referralPoints + contestGamePoints); 
					
					if(purchasePoints <= 0) {
						purchasePoints = 0.00;
					}else {
						purchasePoints = TicketUtil.getRoundedUpValue(purchasePoints);
					}
					
					activeRewards = TicketUtil.getRoundedUpValue(activeRewards);
					spentPoints = TicketUtil.getRoundedUpValue(spentPoints);
					referralPoints = TicketUtil.getRoundedUpValue(referralPoints);
					contestGamePoints = TicketUtil.getRoundedUpValue(contestGamePoints);
					pendingRewards = TicketUtil.getRoundedUpValue(pendingRewards);
					
					System.out.println("activeRewards: "+activeRewards+", spentPoints: "+spentPoints+", referralPoints: "+referralPoints+", contestGamePoints: "+contestGamePoints+", contestGamePoints: "+contestGamePoints);
					
					statistics.setCurrentActiveRewards(activeRewards);
					statistics.setContestGameRewards(contestGamePoints);
					statistics.setPendingPurchase(pendingRewards);
					statistics.setPurchaseRewards(purchasePoints);
					statistics.setReferralRewards(referralPoints);
					statistics.setSpendRewards(spentPoints);
					
					if(null != botIds && botIds.contains(customerObj.getId())) {
						statistics.setIsBot("YES");
					}else {
						statistics.setIsBot("NO");
					}
					
					list.add(statistics);
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
			
			dtoResponse.setCustomerStatistics(list);
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Customer Stats were successfully fectched.");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while fecthing customer stats.Quick Attention Required!");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		
		return dtoResponse;
	}
	
	@RequestMapping(value = "/GetBotsReport", method=RequestMethod.POST)
	public @ResponsePayload CustomerStats getBotsReport(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			Map<String, Object> resultMap = RTFBotsUtil.getAllBotsDetails();
			dtoResponse.setBotList((List<BotDetail>)resultMap.get("BotList"));
			dtoResponse.setReferrlCodeList(new TreeSet<String>((Set<String>) resultMap.get("ReferralCodeList")));
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Bots Report were successfully fectched.");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while fecthing Bots Report.Quick Attention Required!");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		return dtoResponse;
	}
	
	
	@RequestMapping(value = "/LoadBotsToCache")
	public @ResponsePayload CustomerStats loadBotsToCache(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			RTFBotsUtil.init();
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Bots are loaded to cache.");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error while adding bots to cache.");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		return dtoResponse;
	}
	
	/*@RequestMapping(value = "/ForceUpdateHallOfFamePageMaxUserCount", method=RequestMethod.GET)
	public @ResponsePayload CustomerStats forceUpdateHallOfFamePageMaxUserCount(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			
			String maxCount = request.getParameter("pageMaxCount");
			
			Property property = DAORegistry.getPropertyDAO().get("rtf.halloffame.page.max.user.count");
			
			property.set
		
			dtoResponse.setBotList((List<BotDetail>)resultMap.get("BotList"));
			dtoResponse.setReferrlCodeList(new TreeSet<String>((Set<String>) resultMap.get("ReferralCodeList")));
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Bots Report were successfully fectched.");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while fecthing Bots Report.Quick Attention Required!");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		return dtoResponse;
	}*/
	
	
	@RequestMapping(value = "/ValidateQuizAnswerTest", method=RequestMethod.POST)
	public @ResponsePayload QuizValidateAnswerInfo validateQuizAnswersTEst(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizValidateAnswerInfo quizAnswerInfo =new QuizValidateAnswerInfo();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("customerId");
		String contestIdStr = request.getParameter("contestId");
		String questionNoStr = request.getParameter("questionNo");
		String questionIdStr = request.getParameter("questionId");
		String isContestant = request.getParameter("contestant");
		String answerOption = request.getParameter("answer");
		try {
			
			if(isContestant == null || isContestant.isEmpty()){
				resMsg = "Contestant is mandatory";
				error.setDescription("Contestant is mandatory");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				 
				return quizAnswerInfo;
			}
			if(isContestant.equals("Y")) {
				if(answerOption == null || answerOption.isEmpty()){
					resMsg = "Answer is mandatory";
					error.setDescription("Answer is mandatory");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}
			}
			 
			Integer customerId = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Customer Id";
				error.setDescription("Invalid Customer Id");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Integer contestId = null;
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Contest Id";
				error.setDescription("Invalid Contest Id");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Integer questionId = null;
			try{
				questionId = Integer.parseInt(questionIdStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question Id";
				error.setDescription("Invalid Question Id");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Integer questionNo = null;
			try{
				questionNo = Integer.parseInt(questionNoStr.trim());
			}catch(Exception e){
				resMsg = "Invalid Question No";
				error.setDescription("Invalid Question No");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(contestId);
			if(contest == null){
				resMsg = "Contest Id is Invalid";
				error.setDescription("Contest Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer == null){
				resMsg = "Customer Id is Invalid";
				error.setDescription("Custoemr Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			
			/*QuizContestQuestions quizQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(Integer.parseInt(questionIdStr));
			if(quizQuestion == null) {
				error.setDescription("Question Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,"Question Id is Invalid");
				return quizAnswerInfo;
			}*/
			//Fix	
			//////
			QuizContestQuestions quizQuestion = QuizContestUtil.getContestCurrentQuestionsTest(contest.getId());
			if(quizQuestion == null || !quizQuestion.getId().equals(questionId)) {
				resMsg = "Question Id is Invalid";
				error.setDescription("Question Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			if(!quizQuestion.getQuestionSNo().equals(questionNo)) {//|| !quizQuestion.getContestId().equals(contest.getId()
				resMsg = "Question No or Id is Invalid";
				error.setDescription("Question No or Id is Invalid");
				quizAnswerInfo.setError(error);
				quizAnswerInfo.setStatus(0);
				TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
						questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
				return quizAnswerInfo;
			}
			 
			QuizCustomerContestAnswers lastAnswer = null;
			//comment for production performance			
			/*if(quizQuestion.getQuestionSNo() > 1) {
				lastAnswer = QuizContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				if(lastAnswer == null || questionNo != (lastAnswer.getQuestionSNo()+1)) {
					resMsg = "You didn't answer last question.";
					error.setDescription("You didn't answer last question.");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}else if(null != lastAnswer) {
					
					if((null != lastAnswer.getIsCorrectAnswer() && lastAnswer.getIsCorrectAnswer()) || (null != lastAnswer.getIsLifeLineUsed()
							&& lastAnswer.getIsLifeLineUsed())) {
						
					}else {
						resMsg = "You didn't answer last question.";
						error.setDescription("You didn't answer last question.");
						quizAnswerInfo.setError(error);
						quizAnswerInfo.setStatus(0);
						TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
								questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
						return quizAnswerInfo;
					}
					
				}
			} else {*/
				lastAnswer = QuizContestUtil.getCustomerAnswers(customer.getId(), contest.getId());
				if(lastAnswer != null) {
					/*resMsg = "You have already answered for this question.";
					error.setDescription("You have already answered for this question.");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITracking(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;*/
				}
			//}
			if(isContestant.equals("Y")) {
				Double cumulativeRewards = 0.0;
				Integer cumulativeLifeLineUsed = 0;
				if(lastAnswer != null) {
					if(lastAnswer.getCumulativeRewards() != null) {
						cumulativeRewards = lastAnswer.getCumulativeRewards();
					}
					if(lastAnswer.getCumulativeLifeLineUsed() != null) {
						cumulativeLifeLineUsed = lastAnswer.getCumulativeLifeLineUsed();
					}
				}
				
				if(answerOption.equalsIgnoreCase(quizQuestion.getAnswer())) {
					quizAnswerInfo.setIsCorrectAnswer(Boolean.TRUE);
					
					if(contest.getNoOfQuestions().equals(questionNo)) {
						QuizContestUtil.updateContestWinners(contest.getId(), customerId);
					}
				}
				QuizCustomerContestAnswers customerAnswers = new QuizCustomerContestAnswers();
				customerAnswers.setCustomerId(customerId);
				customerAnswers.setContestId(contestId);
				customerAnswers.setQuestionId(questionId);
				customerAnswers.setQuestionSNo(questionNo);
				customerAnswers.setAnswer(answerOption);
				customerAnswers.setIsCorrectAnswer(quizAnswerInfo.getIsCorrectAnswer());
				customerAnswers.setCreatedDateTime(new Date());
				customerAnswers.setIsLifeLineUsed(false);
			
				if(quizAnswerInfo.getIsCorrectAnswer()) {
					customerAnswers.setAnswerRewards(quizQuestion.getQuestionRewards());
					if(quizQuestion.getQuestionRewards() != null) {
						cumulativeRewards = cumulativeRewards + quizQuestion.getQuestionRewards();	
					}
				}
				customerAnswers.setCumulativeRewards(cumulativeRewards);
				customerAnswers.setCumulativeLifeLineUsed(cumulativeLifeLineUsed);
				
				//QuizDAORegistry.getQuizCustomerContestAnswersDAO().save(customerAnswers);
				
				
				int isCrtAns = customerAnswers.getIsCorrectAnswer() ? 1 : 0;
				int isLifeApplied = customerAnswers.getIsLifeLineUsed() ? 1 : 0;
				
				String query = "INSERT INTO customer_contest_answers (customer_id,contest_id,question_id,question_sl_no,answer,created_datetime,is_correct_answer,"
						+ "is_lifeline_used,updated_datetime,answer_rewards,cumulative_rewards,cumulative_lifeline_used) "
						+ "VALUES ("+customerAnswers.getCustomerId()+","+customerAnswers.getContestId()+","+customerAnswers.getQuestionId()+","+customerAnswers.getQuestionSNo()+","
						+ "'"+customerAnswers.getAnswer()+"',getdate(),"+isCrtAns+","+isLifeApplied+",getdate()"
						+ ","+customerAnswers.getAnswerRewards()+","+customerAnswers.getCumulativeRewards()+","+customerAnswers.getCumulativeLifeLineUsed()+" )";
				
				//Connection connection = getConnection();
				Connection connection = JDBCConnection.getQuizConnection();  
				
				ResultSet rs= null;
				Statement insertStatement =null;
				try {
					connection.setAutoCommit(false);
					insertStatement = connection.createStatement();
					insertStatement.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
					rs = insertStatement.getGeneratedKeys();
					Integer id =null;
					if(rs.next()){
						id = rs.getInt(1);
					}
					connection.commit();
					//System.out.println("Generated ID : "+id);
					customerAnswers.setId(id);
					 
				} catch (Exception e) {
					e.printStackTrace();
					connection.rollback();
					resMsg = "Exception Occured While Inserting Data into Answer Table.";
					error.setDescription("Exception Occured While Inserting Data into Answer Table.");
					quizAnswerInfo.setError(error);
					quizAnswerInfo.setStatus(0);
					TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
							questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
					return quizAnswerInfo;
				}finally{
					if(rs!=null){
						rs.close();
					}
					if(insertStatement!=null){
						insertStatement.close();
					}
				}
				
				QuizContestUtil.updateCustomerAnswers(customerAnswers);
				
				log.info("Quiz Question validation : QNO:"+questionNoStr+": "+customerIdStr+":"+contestIdStr+":"+answerOption+":"+questionNoStr+":"+customerAnswers.getIsCorrectAnswer()+":"+new Date());
				
			}
			 
			quizAnswerInfo.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			resMsg = "Success";
			TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
					questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
		}catch(Exception e){
			e.printStackTrace();
			resMsg = "Error occured while Fetching Validate Answer Info.";
			error.setDescription("Error occured while Fetching Validate Answer Info.");
			quizAnswerInfo.setError(error);
			quizAnswerInfo.setStatus(0);
			TrackingUtils.contestValidateAnswerAPITrackingJDBC(request, WebServiceActionType.QUIZVALIDATEANSWERS,resMsg,
					questionNoStr, questionIdStr, isContestant, answerOption, contestIdStr,customerIdStr);
			return quizAnswerInfo;
		} finally {
			log.info("QUIZ VALID ANSWER IOS: QNO:"+questionNoStr+" : "+customerIdStr+" : coId: "+contestIdStr+" :qNo: "+questionNoStr+" :ans: "+answerOption+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+" : "+quizAnswerInfo);
		}
		
		//log.info("QUIZ VALID ANS : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" :qNo: "+request.getParameter("questionNo")+" :ans: "+request.getParameter("answer")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizAnswerInfo;
	
	}
	
	
	@RequestMapping(value = "/GetOneSignalCustomers", method=RequestMethod.POST)
	public @ResponsePayload CustomerStats getBotsReport(HttpServletRequest request,HttpServletResponse response){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			   URL url = new URL("https://onesignal.com/api/v1/players/csv_export?app_id=e15a0313-b54a-41ed-af32-a95991037a40");
			   HttpURLConnection con = (HttpURLConnection)url.openConnection();
			   con.setUseCaches(false);
			   con.setDoOutput(true);
			   con.setDoInput(true);

			   String apiKey = "ZjVhNTE5ZGItNjY2OC00ZWZkLWEzYmMtMDc0MDQ5ZWQwMWFm";
			   
			   con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			   con.setRequestProperty("Authorization", "Basic "+apiKey);
			   con.setRequestMethod("POST");
		
			   int httpResponse = con.getResponseCode();
			   System.out.println("httpResponse: " + httpResponse);
			   System.out.println("Content Lenght : "+con.getContentLength());
			  
			   
			   BufferedReader  br = new BufferedReader(new InputStreamReader((con.getInputStream())));
			   StringBuilder sb = new StringBuilder();
			   String output;
			   while ((output = br.readLine()) != null) {
			     sb.append(output);
			   }
			   
			   JSONObject json = new JSONObject(sb.toString());
			   String fileUrl =  json.getString("csv_file_url");
			   System.out.println(sb.toString());
			   con.disconnect();
			   List<String> ids = DAORegistry.getQueryManagerDAO().getAllOneSignalNotifications();
			   Thread.sleep(25000);
			   
			    URL fileReqest = new URL(fileUrl);
			    HttpURLConnection con1 = (HttpURLConnection)fileReqest.openConnection();
			    con1.setUseCaches(false);
				con1.setDoOutput(true);
				//con1.setDoInput(true);
			    con1.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			    con1.setRequestProperty("Authorization", "Basic "+apiKey);
			    con1.setRequestMethod("GET");
			    
			    
			    InputStream fileInputStream = fileReqest.openStream();
		        BufferedInputStream bis = new BufferedInputStream(fileInputStream);
		        File file = new File("//rtw//RewardTheFan//notificationUsersZip.csv.gz");
		        FileOutputStream fis = new FileOutputStream(file);
		        byte[] buffer = new byte[1024];
		        int count=0;
		        while((count = bis.read(buffer,0,1024)) != -1)
		        {
		            fis.write(buffer, 0, count);
		        }
		        fis.close();
		        bis.close();
		        con1.disconnect();
		        
		        File deCompressedFile = new File("//rtw//RewardTheFan//notificationUsers.csv");
		        FileInputStream fileIn = new FileInputStream(file);
		        GZIPInputStream gZIPInputStream = new GZIPInputStream(fileIn);
		        FileOutputStream fileOutputStream = new FileOutputStream(deCompressedFile);
		        int bytes_read;
		        while ((bytes_read = gZIPInputStream.read(buffer)) > 0) {
		        	fileOutputStream.write(buffer, 0, bytes_read);

		        }
		        gZIPInputStream.close();
		        fileOutputStream.close();
		        
		        //StringBuffer sql = new StringBuffer("INSERT INTO onesignal_notification (notification_id) values ");
		        BufferedReader reader = new BufferedReader(new FileReader(deCompressedFile));
		        String line = "";
		        String notificationId = "";
		        boolean isHeaderRow = true;
		        if(ids==null){
		        	 dtoResponse.setStatus(0);
				     dtoResponse.setMessage("No onesignal notification ids found in system.");
		        }
		        int skippedCount = 0;
		        int insertCount = 0;
		        while((line = reader.readLine()) !=null){
		        	String row[] = line.split(",");
		        	if(row.length > 1 && !isHeaderRow){
		        		notificationId = row[1];
		        		if(notificationId!=null && !notificationId.isEmpty() && !ids.contains(notificationId.trim())){
		        			DAORegistry.getQueryManagerDAO().insertOneSignalNotifications(notificationId);
		        			insertCount++;
		        			//System.out.println("INSERTED");
		        		}else{
		        			skippedCount++;
		        			//System.out.println("SKIPPED");
		        		}
		        			
		        		
		        		//sql.append("('"+notificationId+"'),");
		        	}
		        	isHeaderRow = false;
		        }
		        System.out.println("INSERTED NEW RECORDS : "+insertCount);
		        System.out.println("SKIPPED OLD RECORDS : "+skippedCount);
		        //sql.replace(sql.lastIndexOf(","), sql.lastIndexOf(","), ";");
		        //DAORegistry.getQueryManagerDAO().deleteOneSignalNotifications();
		        //List<CustomerNotificationDetails>  list = DAORegistry.getQueryManagerDAO().getAllOtpVerifiedCustomersNotificationDeviceDetails();
		        dtoResponse.setStatus(1);
		        dtoResponse.setMessage(insertCount+" New Records are inserted into database");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Donwloading/Inserting onesignal notifications ids.");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		return dtoResponse;
	}
	
	@RequestMapping(value = "/ResetTestVariable")
	public @ResponsePayload DashboardInfo resetTestVariable(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		Date start = new Date();
		try {
			apiHitCount = 0;
			dashboardInfo.setSts(apiHitCount);
			dashboardInfo.setMsg("Success");
		}catch(Exception e){
			return dashboardInfo;
		} finally {
			log.info("LOAD ResetTestVariable: "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	
	}
	@RequestMapping(value = "/RefreshNextContestList", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo refreshNextContestList(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizcontestDetails = new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			//String contestIDStr = request.getParameter("contestId");
			String cType = request.getParameter("cType");
			//String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(cType)){
				error.setDescription("Contest Type is mandatory");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.NEXTCONTESTLISTREFRESH,"Contest Type is mandatory");
				 
			}
			log.info("REFRESHCONTESTLIST: Updating Data in caches:"+new Date());
			QuizContestUtil.refreshNextContestList(cType);
			log.info("REFRESHCONTESTLIST: Updating Data in cache- Ends:"+new Date());
			
			quizcontestDetails.setMessage("Contest Cache Data Refreshed successfully.");
			//quizCustomerDetails.setError(error);
			quizcontestDetails.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error Occured While Refreshing Contest List.");
			quizcontestDetails.setError(error);
			quizcontestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.NEXTCONTESTLISTREFRESH,"Error Occured While Refreshing Contest List.");
			return quizcontestDetails;
		}
		log.info("REFRESHCONTESTLIST : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime()));
		
		return quizcontestDetails;
	}
	
	@RequestMapping(value = "/RefreshCustomerLoyaltyMap", method=RequestMethod.GET)
	public @ResponsePayload QuizContestInfo refreshCustomerLoyaltyMap(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizcontestDetails = new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			//String contestIDStr = request.getParameter("contestId");
			//String cType = request.getParameter("cType");
			//String productTypeStr = request.getParameter("productType");
			
			
			log.info("CUSTLOYALTYMAPREFRESH: Updating Data in caches:"+new Date());
			QuizContestUtil.refreshCustomerLoyaltyMapForContest();
			log.info("CUSTLOYALTYMAPREFRESH: Updating Data in cache- Ends:"+new Date());
			
			quizcontestDetails.setMessage("CUSTLOYALTYMAPREFRESH completed successfully.");
			//quizCustomerDetails.setError(error);
			quizcontestDetails.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error Occured While Refreshing Contest List.");
			quizcontestDetails.setError(error);
			quizcontestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.NEXTCONTESTLISTREFRESH,"Error Occured While Refreshing Contest List.");
			return quizcontestDetails;
		}
		log.info("REFRESHCONTESTLIST : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime()));
		
		return quizcontestDetails;
	} 
	@RequestMapping(value = "/LoadTest")
	public @ResponsePayload DashboardInfo testing(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		try {
			apiHitCount++;
			Thread.sleep(1000);
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg("Success: "+apiHitCount);
		}catch(Exception e){
			e.printStackTrace();
			error.setDesc("Error in LoadTest API");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("LOAD TEST: apiHitCount:"+apiHitCount+", Date:"+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	
	}
}	