package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassContestGrandWinner;

 
public class CassContestGrandWinnerDAO implements com.quiz.cassandra.dao.service.CassContestGrandWinnerDAO {

 
	public void save(CassContestGrandWinner obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_grand_winners  (cuid,coid,rwdtix ,crdated,expdate ,orderid ,status) "
				+ " VALUES (?,?, ?,?, ?,?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getrTix(),obj.getCrDated(),obj.getExDate(),obj.getoId(),obj.getStatus());
 
		CassandraConnector.getSession().executeAsync(statement);
	}
	
	public void saveAll(List<CassContestGrandWinner> grandWinners) {
		
		for (CassContestGrandWinner obj : grandWinners) {
			Statement statement = new SimpleStatement("INSERT INTO contest_grand_winners  (cuid,coid,rwdtix ,crdated,expdate ,orderid ,status) "
					+ " VALUES (?,?, ?,?, ?,?,?)", 
					obj.getCuId(),obj.getCoId(),obj.getrTix(),obj.getCrDated(),obj.getExDate(),obj.getoId(),obj.getStatus());
	 
			CassandraConnector.getSession().executeAsync(statement);
		}
	}

	public List<CassContestGrandWinner> getContestGrandWinnersByContestId(Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_grand_winners WHERE coid=?", contestId);
		   List<CassContestGrandWinner> contestWinners = new ArrayList<CassContestGrandWinner>();
			
		   if(results != null) {
			   for (Row row : results) {
				   contestWinners.add( new CassContestGrandWinner(
						   	row.getInt("coid"), 
						   	 row.getInt("cuid"),
				    		  row.getInt("rwdtix"),
				    		  row.getLong("crdated"),
				    		  row.getInt("orderid"),
				    		  row.getString("status"),
				    		  row.getLong("expdate")));
			   }
		   }
		   return contestWinners;
		}
	
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_grand_winners");
	}
	
  
}