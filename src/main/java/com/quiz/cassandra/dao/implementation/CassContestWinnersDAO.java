package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassContestWinners;

 
public class CassContestWinnersDAO implements com.quiz.cassandra.dao.service.CassContestWinnersDAO {

 
	public void save(CassContestWinners obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_winners (cuid,coid,crdated,rwdtix,rwdpnts) "
				+ " VALUES (?,?, ?,?, ?)", 
				obj.getCuId(),obj.getCoId(),obj.getCrDated(),obj.getrTix(),obj.getrPoints());
 
		CassandraConnector.getSession().executeAsync(statement);
	}
	
	public void saveAll(List<CassContestWinners> winners) {
		
		for (CassContestWinners obj : winners) {
			Statement statement = new SimpleStatement("INSERT INTO contest_winners (cuid,coid,crdated,rwdtix,rwdpnts) "
					+ " VALUES (?,?, ?,?, ?)", 
					obj.getCuId(),obj.getCoId(),obj.getCrDated(),obj.getrTix(),obj.getrPoints());
	 
			CassandraConnector.getSession().executeAsync(statement);
		}
	}

	public List<CassContestWinners> getContestWinnersByContestId(Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_winners");
		   List<CassContestWinners> contestWinners = new ArrayList<CassContestWinners>();
			
		   if(results != null) {
			   for (Row row : results) {
				   contestWinners.add( new CassContestWinners(
						   	 row.getInt("coid"),
						   	 row.getInt("cuid"),
				    		  row.getInt("rwdtix"),
				    		  row.getDouble("rwdpnts"),
				    		  row.getLong("crdated")));
			   }
		   }
		   return contestWinners;
		}
	
	/*public void updateContestWinnersRewards(Integer contestId,Double rewardPoints) {
		
		SimpleStatement statement = new SimpleStatement("UPDATE contest_winners SET rwdpnts=? WHERE coid=?)",rewardPoints,contestId); 
		CassandraConnector.getSession().executeAsync(statement);
	}*/
	/*public void updateContestWinnersRewards(List<CassContestWinners> winners) {
		
		for (CassContestWinners winner : winners) {
			SimpleStatement statement = new SimpleStatement("UPDATE contest_winners SET rwdpnts=? WHERE coid=? and cuid=?)",new Date(),winner.getCoId(),winner.getCuId()); 
			CassandraConnector.getSession().executeAsync(statement);
		}
		
	}*/
	
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_winners");
	}
	
  
}