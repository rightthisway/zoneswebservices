package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassCustomer;
import com.rtf.ecomerce.pojo.GenRewardDVO;
import com.zonesws.webservices.data.Customer;

 
public class CassCustomerDAO implements com.quiz.cassandra.dao.service.CassCustomerDAO {
	
	
	public void saveCustomer(CassCustomer obj) {
		CassandraConnector.getSession().execute(
				"INSERT INTO customer (customer_id,user_id,email,phone,image_path,is_otp_verified,is_blocked,active_reward_points,magicwand,"
				+ "no_of_lives, rtf_points,super_fan_chances,sf_ques_level,rtf_cur_val_bal) VALUES (?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?)",
				obj.getId(), obj.getuId(), obj.getEmail(), obj.getPhone(),obj.getImgP(),obj.getIsOtp(), obj.getIsBlocked(),
				obj.getcRewardDbl(),obj.getMagicWand(), obj.getqLive(),obj.getRtfPoints(),obj.getSfcCount(),obj.getSfQuesLevel(),obj.getLoyaltyPoints());
	}
  
	public void saveAll(List<CassCustomer> objList) {
		
		Session session = CassandraConnector.getSession();		
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session.prepare("INSERT INTO customer (customer_id,user_id,email,phone,image_path,is_otp_verified,"
				+ "is_blocked,active_reward_points,magicwand,no_of_lives, rtf_points,super_fan_chances,sf_ques_level,rtf_cur_val_bal) VALUES (?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?)");
		for (CassCustomer obj : objList) {
			batchStatement.add(preparedStatement.bind(obj.getId(), obj.getuId(), obj.getEmail(), obj.getPhone(),obj.getImgP(),obj.getIsOtp(), obj.getIsBlocked(),
					obj.getcRewardDbl(),obj.getMagicWand(), obj.getqLive(),obj.getRtfPoints(),obj.getSfcCount(),obj.getSfQuesLevel(),obj.getLoyaltyPoints()));
		}
		try {
			ResultSet rs = session.execute(batchStatement);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void updateCustomerLives(Integer customerId, Integer quizLives) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ? where customer_id = ? ", quizLives, customerId);
	}
	public void updateCustomerLivesAndRtfPoints(Integer customerId, Integer quizLives,Integer rtfPoints) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ?,rtf_points=? where customer_id = ? ", quizLives,rtfPoints, customerId);
	}
	public void updateCustomerLives(Integer customerId, Integer quizLives,CassCustomer customer) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ? where customer_id = ?  ", quizLives, customerId);
	}
	
	public void updateProfilePicName(Integer customerId, String imgPath) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set image_path = ? where customer_id = ? ", imgPath, customerId);
	}
	public void updateCutomerBlockedStatus(Boolean blockedStatus, Integer customerId) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set is_blocked = ? where customer_id = ? ", blockedStatus, customerId);
	}

	public void updateCustomerRewardsAndLives(Integer customerId, Integer quizLives,Double rewards) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ?,active_reward_points=? where customer_id = ?  ", quizLives,rewards, customerId);
	}
	public void updateCustomerRewardsAndLivesAndPoints(Integer customerId, Integer quizLives,Double rewards,Integer rtfPoints) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set no_of_lives = ?,active_reward_points=?,rtf_points=? where customer_id = ?  ", quizLives,rewards,rtfPoints, customerId);
	}
	
	public void updateCustomerRewardsAndLives(List<Customer> objList) {
		Session session = CassandraConnector.getSession();		
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session.prepare("UPDATE customer set no_of_lives = ?,active_reward_points=? where customer_id = ?");
		for (Customer obj : objList) {
			batchStatement.add(preparedStatement.bind(obj.getQuizCustomerLives(), obj.getRewardDollar(), obj.getId()));
		}
		try {
			session.execute(batchStatement);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	public void updateCustomerRewardsByCustomerId(Integer customerId, Double rewards) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set active_reward_points=? where customer_id = ?  ", rewards, customerId);
	}
	public void updateCustomerRewardsAndRtfPointsByCustomerId(Integer customerId, Double rewards,Integer rtfPoints) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set active_reward_points=?,rtf_points=? where customer_id = ?  ", rewards,rtfPoints, customerId);
	}
	public void updateCustomerRtfPointsByCustomerId(Integer customerId, Integer rtfPoints) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set rtf_points=? where customer_id = ?  ", rtfPoints, customerId);
	}
	public void updateCustomerSuperFanTotalChances(Integer customerId, Integer totalChances) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set super_fan_chances = ? where customer_id = ? ", totalChances, customerId);
	}
 
	public CassCustomer getCustomerById(Integer customerId){
	   final ResultSet results = CassandraConnector.getSession().execute(
	      "SELECT * from customer WHERE customer_id = ?", customerId);
	    CassCustomer customer = null;
	   if(results != null) {
		   Row row = results.one();
		   if(row != null) {
			   customer =  new CassCustomer(
			    		  row.getInt("customer_id"),
			    		  row.getString("user_id"),
			    		  row.getString("email"),
			    		  row.getString("phone"),
			    		  row.getInt("no_of_lives"),
			    		  row.getDouble("active_reward_points"),
			    		  //row.getDecimal("active_reward_points")!= null?row.getDecimal("active_reward_points").doubleValue():null,
			    		  row.getString("image_path"),
			    		  //Boolean.valueOf(String.valueOf(row.getInt("is_otp_verified"))
			    		  row.getBool("is_otp_verified"),
			    		  row.getInt("super_fan_chances") , row.getInt("magicwand"),row.getInt("rtf_points"),
			    		  row.getBool("is_blocked"),row.getInt("sf_ques_level"),row.getInt("rtf_cur_val_bal"));
		   }
	   }
	   return customer;
	}
	
	public List<CassCustomer> getAll(){
		   ResultSet results = CassandraConnector.getSession().execute("SELECT * from customer");
		   List<CassCustomer> customerList = new ArrayList<CassCustomer>();
	
		   if(results != null) {
			   for (Row row : results) {
				    
				   customerList.add( new CassCustomer(
				    		  row.getInt("customer_id"),
				    		  row.getString("user_id"),
				    		  row.getString("email"),
				    		  row.getString("phone"),
				    		  row.getInt("no_of_lives"),
				    		  row.getDouble("active_reward_points"),
				    		  row.getString("image_path"),
				    		  row.getBool("is_otp_verified"),
				    		  row.getTimestamp("created_datetime"),
				    		  row.getTimestamp("last_modified_datetime"),
				    		  row.getInt("super_fan_chances") , 
				    		  row.getInt("magicwand"),
				    		  row.getInt("rtf_points"),
				    		  row.getBool("is_blocked"),
				    		  row.getInt("sf_ques_level"),row.getInt("rtf_cur_val_bal")));
			   }
		   }
		   return customerList;
	}
	
	public void deleteCustomer(Integer customerId){
	   final String deleteString = "DELETE FROM customer WHERE customer_id = ?";
	   CassandraConnector.getSession().execute(deleteString,customerId);
	}
	
	public void deleteAll(){
	   final String deleteString = "TRUNCATE customer";
	   CassandraConnector.getSession().execute(deleteString);
	}
	
	public void getCount(){
	   final ResultSet results = CassandraConnector.getSession().execute("SELECT count(*) as tot from customer ");
	   if(results != null) {
		   Row row = results.one();
		   if(row != null) {
			   System.out.println(row.getInt("tot"));
		   }
	   }
	}
	
	public void updateCustomerSfQuestionLevel(Integer customerId, Integer quesLevel) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set sf_ques_level = ? where customer_id = ? ", quesLevel, customerId);
	}
	
	public void updateCustomerMagicWand(Integer customerId, Integer magicWands) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set magicwand = ? where customer_id = ? ", magicWands, customerId);
	}
	public void updateCustomerMagicWandAndRtfPoints(Integer customerId, Integer magicWands,Integer rtfPoints) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set magicwand = ?,rtf_points=? where customer_id = ? ", magicWands,rtfPoints, customerId);
	}
	public void updateCustomerLoyaltyPoints(Integer customerId,Integer loyaltyPoints) {
		CassandraConnector.getSession().execute(
				"UPDATE customer set rtf_cur_val_bal=? where customer_id = ? ", loyaltyPoints, customerId);
	}
	
	public void resetCustomerSuperFanLevels() {
		CassandraConnector.getSession().execute(
				"UPDATE customer set sf_ques_level = ? ", 0);
	}
	
	public void updateCustomerLoyaltyPoints(List<GenRewardDVO> objList) throws Exception {
		Session session = CassandraConnector.getSession();		
		BatchStatement batchStatement = new BatchStatement();
		PreparedStatement preparedStatement = session.prepare("UPDATE customer set rtf_cur_val_bal=? where customer_id = ?");
		for (GenRewardDVO obj : objList) {
			batchStatement.add(preparedStatement.bind(obj.getUpdLoyltyPoints(), obj.getCustomerId()));
		}
		try {
			session.execute(batchStatement);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}