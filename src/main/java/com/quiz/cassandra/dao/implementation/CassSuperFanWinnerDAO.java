package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassSuperFanWinner;

 
public class CassSuperFanWinnerDAO implements com.quiz.cassandra.dao.service.CassSuperFanWinnerDAO {

  
	public List<CassSuperFanWinner> getSuperFanWinnersByContestId(Integer contestId){
	   ResultSet results = CassandraConnector.getSession().execute("SELECT * from contest_superfan_winners");
	   List<CassSuperFanWinner> contestWinners = new ArrayList<CassSuperFanWinner>();
		
	   if(results != null) {
		   for (Row row : results) {
			   contestWinners.add( new CassSuperFanWinner(
					   	 row.getInt("coid"),
					   	 row.getInt("cuid"), 
			    		 row.getLong("crdated"),
			    		 row.getInt("no_of_chances")));
		   }
	   }
	   return contestWinners;
	}
	 
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_superfan_winners");
	}
	
	public void save(CassSuperFanWinner obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_superfan_winners (cuid,coid,crdated,no_of_chances) VALUES (?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getCrDated(),obj.getNoOfWinningChance());
		CassandraConnector.getSession().executeAsync(statement);
	}
	
}