package com.quiz.cassandra.dao.implementation;

import com.quiz.cassandra.dao.service.CassContestGrandWinnerDAO;
import com.quiz.cassandra.dao.service.CassContestWinnersDAO;
import com.quiz.cassandra.dao.service.CassCustomerDAO;
import com.quiz.cassandra.dao.service.CassFbCallBackTrackingDAO;
import com.quiz.cassandra.dao.service.CassRtfApiTrackingDAO;
import com.quiz.cassandra.dao.service.CassSuperFanWinnerDAO;
import com.quiz.cassandra.dao.service.ContestParticipantsDAO;
import com.quiz.cassandra.dao.service.ContestPasswordAuthDAO;
import com.quiz.cassandra.dao.service.CustContAnswersDAO;
import com.quiz.cassandra.dao.service.CustContDtlsDAO;
import com.quiz.cassandra.dao.service.JackpotWinnerDAO;
import com.quiz.cassandra.dao.service.SuperFanContCustLevelsDAO;



public class CassandraDAORegistry {
	
	private static CassCustomerDAO cassCustomerDAO;
	private static CassRtfApiTrackingDAO cassRtfApiTrackingDAO;
	private static CustContAnswersDAO custContAnswersDAO;
	private static ContestParticipantsDAO contestParticipantsDAO;
	private static CassContestWinnersDAO cassContestWinnersDAO;
	private static CassContestGrandWinnerDAO cassContestGrandWinnerDAO;
	private static CassSuperFanWinnerDAO cassSuperFanWinnerDAO;
	private static JackpotWinnerDAO jackpotWinnerDAO;
	private static CassFbCallBackTrackingDAO cassFbCallBackTrackingDAO;
	private static CustContDtlsDAO custContDtlsDAO;
	private static SuperFanContCustLevelsDAO superFanContCustLevelsDAO;
	private static ContestPasswordAuthDAO contestPasswordAuthDAO;
	
	
	public static final CassCustomerDAO getCassCustomerDAO() {
		return cassCustomerDAO;
	}

	public final void setCassCustomerDAO(CassCustomerDAO cassCustomerDAO) {
		CassandraDAORegistry.cassCustomerDAO = cassCustomerDAO;
	}

	public static final CassRtfApiTrackingDAO getCassRtfApiTrackingDAO() {
		return cassRtfApiTrackingDAO;
	}

	public final void setCassRtfApiTrackingDAO(CassRtfApiTrackingDAO cassRtfApiTrackingDAO) {
		CassandraDAORegistry.cassRtfApiTrackingDAO = cassRtfApiTrackingDAO;
	}

	public static final CustContAnswersDAO getCustContAnswersDAO() {
		return custContAnswersDAO;
	}

	public final void setCustContAnswersDAO(CustContAnswersDAO custContAnswersDAO) {
		CassandraDAORegistry.custContAnswersDAO = custContAnswersDAO;
	}

	public static final ContestParticipantsDAO getContestParticipantsDAO() {
		return contestParticipantsDAO;
	}

	public final void setContestParticipantsDAO(ContestParticipantsDAO contestParticipantsDAO) {
		CassandraDAORegistry.contestParticipantsDAO = contestParticipantsDAO;
	}

	public static final CassContestWinnersDAO getCassContestWinnersDAO() {
		return cassContestWinnersDAO;
	}

	public final void setCassContestWinnersDAO(CassContestWinnersDAO cassContestWinnersDAO) {
		CassandraDAORegistry.cassContestWinnersDAO = cassContestWinnersDAO;
	}

	public static final CassContestGrandWinnerDAO getCassContestGrandWinnerDAO() {
		return cassContestGrandWinnerDAO;
	}

	public final void setCassContestGrandWinnerDAO(CassContestGrandWinnerDAO cassContestGrandWinnerDAO) {
		CassandraDAORegistry.cassContestGrandWinnerDAO = cassContestGrandWinnerDAO;
	}

	public static final CassSuperFanWinnerDAO getCassSuperFanWinnerDAO() {
		return cassSuperFanWinnerDAO;
	}

	public final void setCassSuperFanWinnerDAO(CassSuperFanWinnerDAO cassSuperFanWinnerDAO) {
		CassandraDAORegistry.cassSuperFanWinnerDAO = cassSuperFanWinnerDAO;
	}

	public static final JackpotWinnerDAO getJackpotWinnerDAO() {
		return jackpotWinnerDAO;
	}

	public final void setJackpotWinnerDAO(JackpotWinnerDAO jackpotWinnerDAO) {
		CassandraDAORegistry.jackpotWinnerDAO = jackpotWinnerDAO;
	}

	public static final CassFbCallBackTrackingDAO getCassFbCallBackTrackingDAO() {
		return cassFbCallBackTrackingDAO;
	}

	public final void setCassFbCallBackTrackingDAO(CassFbCallBackTrackingDAO cassFbCallBackTrackingDAO) {
		CassandraDAORegistry.cassFbCallBackTrackingDAO = cassFbCallBackTrackingDAO;
	}

	public static final CustContDtlsDAO getCustContDtlsDAO() {
		return custContDtlsDAO;
	}

	public final void setCustContDtlsDAO(CustContDtlsDAO custContDtlsDAO) {
		CassandraDAORegistry.custContDtlsDAO = custContDtlsDAO;
	}

	public static final SuperFanContCustLevelsDAO getSuperFanContCustLevelsDAO() {
		return superFanContCustLevelsDAO;
	}

	public final void setSuperFanContCustLevelsDAO(SuperFanContCustLevelsDAO superFanContCustLevelsDAO) {
		CassandraDAORegistry.superFanContCustLevelsDAO = superFanContCustLevelsDAO;
	}

	public static final ContestPasswordAuthDAO getContestPasswordAuthDAO() {
		return contestPasswordAuthDAO;
	}

	public final void setContestPasswordAuthDAO(ContestPasswordAuthDAO contestPasswordAuthDAO) {
		CassandraDAORegistry.contestPasswordAuthDAO = contestPasswordAuthDAO;
	}

		
}
