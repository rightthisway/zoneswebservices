package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.ContestParticipants;

 
public class ContestParticipantsDAO implements com.quiz.cassandra.dao.service.ContestParticipantsDAO {

 
	public void save(ContestParticipants obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_participants (cuid,coid,ipadd,plform,jndate,exdate,status,uid) "
				+ " VALUES (?, ?,?, ?,?, ?,?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getIpAdd(),obj.getPfm(),obj.getJnDate(),obj.getExDate(),obj.getStatus(),obj.getuId());
 
		CassandraConnector.getSession().executeAsync(statement);
	}

	/*public List<ContestParticipants> getAllParticipantsByContestIdAndContestId(Integer customerId,Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_participants  WHERE cuid = ? and coid=?  ALLOW FILTERING", customerId,contestId);
		   List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
			
		   if(results != null) {
			   for (Row row : results) {
				   participants.add( new ContestParticipants(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getString("ipadd"),
				    		  row.getString("plform"),
						   	  row.getTimestamp("jndate"),
				    		  row.getTimestamp("exdate"),
				    		  row.getString("status"),
				    		  row.getTimestamp("crdated")));
			   }
		   }
		   return participants;
	}*/
	
	public List<ContestParticipants> getAllParticipantsByContestId(Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_participants");
		   List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
			
		   if(results != null) {
			   for (Row row : results) {
				   participants.add( new ContestParticipants(
				    		  row.getInt("coid"),
				    		  row.getInt("cuid"),
				    		  row.getString("plform"),
				    		  row.getString("ipadd"),
						   	  row.getLong("jndate"),
				    		  row.getLong("exdate"),
				    		  row.getString("status"),
				    		  row.getString("uid")));
			   }
		   }
		   return participants;
	}
	
	public List<ContestParticipants> getAllParticipants(){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_participants");
		   List<ContestParticipants> participants = new ArrayList<ContestParticipants>();
			
		   if(results != null) {
			   for (Row row : results) {
				   participants.add( new ContestParticipants(
						   row.getInt("coid"),
				    		  row.getInt("cuid"),
				    		  row.getString("plform"),
				    		  row.getString("ipadd"),
						   	  row.getLong("jndate"),
				    		  row.getLong("exdate"),
				    		  row.getString("status"),
				    		  row.getString("uid")));
			   }
		   }
		   return participants;
	}
	
	/*public void updateContestParticipantsExistContest(Integer customerId,Integer contestId) {
		SimpleStatement statement = new SimpleStatement("UPDATE contest_participants SET status='EXIT', exdate=? WHERE coid=? and cuid=? ALLOW FILTERING)",new Date(),contestId,customerId); 
		CassandraConnector.getSession().executeAsync(statement);
	}
	public void updateExistingContestParticipantsByJoinContest(Integer customerId,Integer contestId) {
		SimpleStatement statement = new SimpleStatement("UPDATE contest_participants SET status='EXIT' WHERE coid=? and cuid=? ALLOW FILTERING)",contestId,customerId); 
		CassandraConnector.getSession().executeAsync(statement);
	}
	public void updateContestParticipantsForContestResetByContestId(Integer contestId) {
		SimpleStatement statement = new SimpleStatement("UPDATE contest_participants SET status='EXIT', exdate=? WHERE coid=? ALLOW FILTERING)",new Date(),contestId); 
		CassandraConnector.getSession().executeAsync(statement);
	}*/
	
	public void truncate() {
		System.out.println("Truncate contest_participants Table");
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_participants");
	}
  
}