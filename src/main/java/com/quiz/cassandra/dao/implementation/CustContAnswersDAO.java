package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CustContAnswers;

 
public class CustContAnswersDAO implements com.quiz.cassandra.dao.service.CustContAnswersDAO {

 
	public void save(CustContAnswers obj) {
		Statement statement = new SimpleStatement("INSERT INTO cust_contest_ans(cuid,coid,qid,qno,ans,iscrt,islife,"
				+ "crdated,updated,arwds,curwds,culife) VALUES (?,?, ?,?, ?,?, ?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getqNo(),obj.getAns(),obj.getIsCrt(),obj.getIsLife(),
				obj.getCrDate(),obj.getUpDate(),obj.getaRwds(),obj.getCuRwds(),obj.getCuLife());
 
		CassandraConnector.getSession().executeAsync(statement);
	}

	public void delete(CustContAnswers ans) throws Exception {
		//Statement statement = new SimpleStatement("delete cust_contest_ans where coid=?",contestId);
		Statement statement = new SimpleStatement("delete from cust_contest_ans where cuid=?  and coid=? and  qno=?",ans.getCuId(),ans.getCoId(),ans.getqNo());
 
		CassandraConnector.getSession().executeAsync(statement);
	}
	
	/*public List<CustContAnswers> getCustContAnswersByCustIdAndContId(Integer customerId,Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans WHERE cuid = ? and coid=? ALLOW FILTERING", customerId,contestId);
		   List<CustContAnswers> custContAnsList = new ArrayList<CustContAnswers>();
			
		   if(results != null) {
			   for (Row row : results) {
				   custContAnsList.add( new CustContAnswers(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getTimestamp("crdated"),
				    		  row.getTimestamp("updated")));
			   }
		   }
		   return custContAnsList;
		}
	public CustContAnswers getCustContAnswersByCustIdAndQuestId(Integer customerId,Integer questionId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans WHERE cuid = ? and qid=? ALLOW FILTERING", customerId,questionId);
			
		   CustContAnswers custContAns = null;
		   if(results != null) {
			   Row row = results.one();
			   if (row != null) {
				   custContAns =  new CustContAnswers(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getTimestamp("crdated"),
				    		  row.getTimestamp("updated"));
			   }
		   }
		   return custContAns;
		}*/
	
	
	public List<CustContAnswers> getCustContAnswersByContId(Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans");
		   List<CustContAnswers> custContAnsList = new ArrayList<CustContAnswers>();
		   if(results != null) {
			   for (Row row : results) {
				   custContAnsList.add( new CustContAnswers(
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getLong("crdated"),
				    		  row.getLong("updated"),
				    		  row.getString("fb_callback_time"),
				    		  row.getString("ans_time"),
				    		  row.getInt("rt_count"),
				    		  row.getBool("auto_created")));
			   }
		   }
		   return custContAnsList;
		}
	
	public List<CustContAnswers> getAll(){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans ");
		   List<CustContAnswers> custContAnsList = new ArrayList<CustContAnswers>();
		   if(results != null) {
			   for (Row row : results) {
				   custContAnsList.add( new CustContAnswers(
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getLong("crdated"),
				    		  row.getLong("updated"),
				    		  row.getString("fb_callback_time"),
				    		  row.getString("ans_time"),
				    		  row.getInt("rt_count"),
				    		  row.getBool("auto_created")));
			   }
		   }
		   return custContAnsList;
		}
  
	/*public List<CustContAnswers> getAllCustContAnswersByQuestId(Integer questionId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from cust_contest_ans WHERE qid=? ALLOW FILTERING",questionId);
		   List<CustContAnswers> custContAnsList = new ArrayList<CustContAnswers>();
			
		   if(results != null) {
			   for (Row row : results) {
				   custContAnsList.add( new CustContAnswers(
						   	 row.getUUID("id"),
				    		  row.getInt("cuid"),
				    		  row.getInt("coid"),
				    		  row.getInt("qid"),
				    		  row.getInt("qno"),
				    		  row.getString("ans"),
				    		  row.getBool("iscrt"),
				    		  row.getBool("islife"),
				    		  row.getDouble("arwds"),
				    		  row.getDouble("curwds"),
				    		  row.getInt("culife"),
						   	  row.getTimestamp("crdated"),
				    		  row.getTimestamp("updated")));
			   }
		   }
		   return custContAnsList;
		}
	*/
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE cust_contest_ans");
	}
}