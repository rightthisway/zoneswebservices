package com.quiz.cassandra.dao.implementation;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.quiz.cassandra.config.CassandraConnector;
import com.quiz.cassandra.data.CassContestGrandWinner;

 
public class JackpotWinnerDAO implements com.quiz.cassandra.dao.service.JackpotWinnerDAO {

 
	public void save(CassContestGrandWinner obj) {
		Statement statement = new SimpleStatement("INSERT INTO contest_jackpot_winners (cuid,coid,qid,crdated ,status,jackpot_text) "
				+ " VALUES (?,?, ?,?, ?,?)", 
				obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getCrDated(),obj.getStatus(),obj.getJackpotSt());
		CassandraConnector.getSession().executeAsync(statement);
	}
	
	public void saveAll(List<CassContestGrandWinner> grandWinners) {
		
		for (CassContestGrandWinner obj : grandWinners) {
			Statement statement = new SimpleStatement("INSERT INTO contest_jackpot_winners  (cuid,coid,qid,crdated ,status,jackpot_text) "
					+ " VALUES (?,?,?,?,?,?)", 
					obj.getCuId(),obj.getCoId(),obj.getqId(),obj.getCrDated(),obj.getStatus(),obj.getJackpotSt());
			CassandraConnector.getSession().executeAsync(statement);
		}
	}

	public List<CassContestGrandWinner> getJackpotWinnersByContestId(Integer contestId,Integer qId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_jackpot_winners WHERE coid=? and qid=? ALLOW FILTERING", contestId,qId);
		   List<CassContestGrandWinner> contestWinners = new ArrayList<CassContestGrandWinner>();
		   if(results != null) {
			   for (Row row : results) {
				   contestWinners.add( new CassContestGrandWinner(
						   	row.getInt("coid"), 
						   	 row.getInt("cuid"), 
						   	 row.getInt("qid"), 
				    		  row.getLong("crdated"), 
				    		  row.getString("status"),
				    		  row.getString("jackpot_text")));
			   }
		   }
		   return contestWinners;
		}
	
	public List<CassContestGrandWinner> getMegaJackpotWinnersByContestId(Integer contestId){
		   final ResultSet results = CassandraConnector.getSession().execute(
		      "SELECT * from contest_jackpot_winners");
		   List<CassContestGrandWinner> contestWinners = new ArrayList<CassContestGrandWinner>();
		   if(results != null) {
			   for (Row row : results) {
				   contestWinners.add( new CassContestGrandWinner(
						   	row.getInt("coid"), 
						   	 row.getInt("cuid"), 
						   	 row.getInt("qid"), 
				    		  row.getLong("crdated"), 
				    		  row.getString("status"),
				    		  row.getString("jackpot_text")));
			   }
		   }
		   return contestWinners;
		}
	
	public void truncate() {
		CassandraConnector.getSession().executeAsync("TRUNCATE TABLE contest_jackpot_winners");
	}
	
  
}