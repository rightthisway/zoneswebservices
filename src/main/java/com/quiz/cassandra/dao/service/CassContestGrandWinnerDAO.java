package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassContestGrandWinner;

public interface CassContestGrandWinnerDAO  {
	
	public void save(CassContestGrandWinner obj);
	public List<CassContestGrandWinner> getContestGrandWinnersByContestId(Integer contestId);
	public void saveAll(List<CassContestGrandWinner> winners);
	public void truncate();

}
