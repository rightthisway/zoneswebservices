package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassContestWinners;

public interface CassContestWinnersDAO  {
	
	public void save(CassContestWinners obj);
	public List<CassContestWinners> getContestWinnersByContestId(Integer contestId);
	/*public void updateContestWinnersRewards(Integer contestId,Double rewardPoints);*/
	public void saveAll(List<CassContestWinners> winners);
	public void truncate();

}
