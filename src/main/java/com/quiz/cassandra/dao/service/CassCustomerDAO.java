package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassCustomer;
import com.rtf.ecomerce.pojo.GenRewardDVO;
import com.zonesws.webservices.data.Customer;

public interface CassCustomerDAO  {
	public void saveCustomer(CassCustomer obj);
	public void saveAll(List<CassCustomer> objList);
	public void updateCustomerLives(Integer customerId, Integer quizLives,CassCustomer customer);
	public CassCustomer getCustomerById(Integer customerId);
	public List<CassCustomer> getAll();
	public void deleteCustomer(Integer customerId);
	public void updateCustomerLives(Integer customerId, Integer quizLives);
	public void updateCustomerLivesAndRtfPoints(Integer customerId, Integer quizLives,Integer rtfPoints);
	public void updateProfilePicName(Integer customerId, String imgPath);
	public void updateCutomerBlockedStatus(Boolean blockedStatus, Integer customerId);
	public void deleteAll();
	public void updateCustomerRewardsAndLives(Integer customerId, Integer quizLives,Double rewards);
	public void updateCustomerRewardsAndLivesAndPoints(Integer customerId, Integer quizLives,Double rewards,Integer rtfPoints);
	public void updateCustomerRewardsAndLives(List<Customer> objList);
	public void updateCustomerRewardsByCustomerId(Integer customerId, Double rewards);
	public void updateCustomerRewardsAndRtfPointsByCustomerId(Integer customerId, Double rewards,Integer rtfPoints);
	public void updateCustomerRtfPointsByCustomerId(Integer customerId, Integer rtfPoints);
	public void updateCustomerSuperFanTotalChances(Integer customerId, Integer totalChances);
	public void updateCustomerSfQuestionLevel(Integer customerId, Integer quesLevel);
	public void resetCustomerSuperFanLevels();
	public void updateCustomerMagicWand(Integer customerId, Integer magicWands);
	public void updateCustomerMagicWandAndRtfPoints(Integer customerId, Integer magicWands,Integer rtfPoints);
	public void updateCustomerLoyaltyPoints(Integer customerId,Integer loyaltyPoints);
	public void updateCustomerLoyaltyPoints(List<GenRewardDVO> objList) throws Exception;
}
