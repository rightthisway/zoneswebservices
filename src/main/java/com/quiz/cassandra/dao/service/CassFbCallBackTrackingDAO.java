package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassFbCallBackTracking;

public interface CassFbCallBackTrackingDAO  {
	
	public void save(CassFbCallBackTracking obj);
	public List<CassFbCallBackTracking> getAllFbCallBackTrackings();
	public void truncate();
}
