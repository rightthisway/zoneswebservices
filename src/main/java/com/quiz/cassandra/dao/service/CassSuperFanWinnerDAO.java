package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassSuperFanWinner;

public interface CassSuperFanWinnerDAO  {
	public List<CassSuperFanWinner> getSuperFanWinnersByContestId(Integer contestId); 
	public void truncate();
	public void save(CassSuperFanWinner obj);
}
