package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.ContestParticipants;

public interface ContestParticipantsDAO  {
	
	public void save(ContestParticipants obj);
	/*public List<ContestParticipants> getAllParticipantsByContestIdAndContestId(Integer customerId,Integer contestId);*/
	/*public void updateContestParticipantsExistContest(Integer customerId,Integer contestId);
	public void updateExistingContestParticipantsByJoinContest(Integer customerId,Integer contestId);
	public void updateContestParticipantsForContestResetByContestId(Integer contestId);*/
	public List<ContestParticipants> getAllParticipantsByContestId(Integer contestId);
	public void truncate();
	public List<ContestParticipants> getAllParticipants();

}
