package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CustContAnswers;

public interface CustContAnswersDAO  {
	
	public void save(CustContAnswers obj);
	/*public List<CustContAnswers> getCustContAnswersByCustIdAndContId(Integer customerId,Integer contestId);
	public CustContAnswers getCustContAnswersByCustIdAndQuestId(Integer customerId,Integer questionId);*/
	public List<CustContAnswers> getCustContAnswersByContId(Integer contestId);
	/*public List<CustContAnswers> getAllCustContAnswersByQuestId(Integer questionId);*/
	public void truncate();
}
