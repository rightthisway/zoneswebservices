package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.CassContestGrandWinner;

public interface JackpotWinnerDAO  {
	
	public void save(CassContestGrandWinner obj);
	public List<CassContestGrandWinner> getJackpotWinnersByContestId(Integer contestId,Integer qId);
	public void saveAll(List<CassContestGrandWinner> winners);
	public void truncate();
	public List<CassContestGrandWinner> getMegaJackpotWinnersByContestId(Integer contestId);

}
