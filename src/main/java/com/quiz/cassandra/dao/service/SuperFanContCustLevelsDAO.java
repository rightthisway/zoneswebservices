package com.quiz.cassandra.dao.service;

import java.util.List;

import com.quiz.cassandra.data.SuperFanContCustLevels;

public interface SuperFanContCustLevelsDAO  {
	
	public void save(SuperFanContCustLevels obj) throws Exception;
	public List<SuperFanContCustLevels> getAllSuperFanContCustLevels();
	public void truncate() throws Exception;
}
