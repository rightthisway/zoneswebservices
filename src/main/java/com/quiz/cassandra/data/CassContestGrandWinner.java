package com.quiz.cassandra.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ContestGrandWinner")
public class CassContestGrandWinner implements Serializable{

	private Integer coId;
	private Integer qId;
	private Integer cuId;
	private Integer rTix;
	private Double rPoints;
	
	@JsonIgnore
	private Long crDated;
	
	@JsonIgnore
	private Integer oId;
	@JsonIgnore
	private String status;
	@JsonIgnore
	private Long exDate;
	@JsonIgnore
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	private String jackpotSt;
	
	
	
	public CassContestGrandWinner() { }
	
	public CassContestGrandWinner(Integer coId, Integer cuId, Integer rTix, Long crDated, Integer oId,
			String status, Long exDate) {
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.crDated = crDated;
		this.oId = oId;
		this.status = status;
		this.exDate = exDate;
	}
	
	public CassContestGrandWinner(Integer coId, Integer cuId, Integer qId, Long crDated,String status,String jackpotSt) {
		this.coId = coId;
		this.cuId = cuId;
		this.qId = qId;
		this.crDated = crDated;
		this.status = status;
		this.jackpotSt = jackpotSt;
	}
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getrTix() {
		return rTix;
	}
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}
	public Double getrPoints() {
		return rPoints;
	}
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}
	public Long getCrDated() {
		return crDated;
	}
	public void setCrDated(Long crDated) {
		this.crDated = crDated;
	}
	public Integer getoId() {
		return oId;
	}
	public void setoId(Integer oId) {
		this.oId = oId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getExDate() {
		return exDate;
	}
	public void setExDate(Long exDate) {
		this.exDate = exDate;
	}

	public Integer getqId() {
		return qId;
	}

	public void setqId(Integer qId) {
		this.qId = qId;
	}

	public String getJackpotSt() {
		if(null == jackpotSt) {
			jackpotSt = "";
		}
		return jackpotSt;
	}

	public void setJackpotSt(String jackpotSt) {
		this.jackpotSt = jackpotSt;
	}
}
