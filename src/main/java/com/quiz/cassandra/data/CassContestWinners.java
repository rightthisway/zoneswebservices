package com.quiz.cassandra.data;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.enums.RewardType;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("ContestWinners")
public class CassContestWinners  implements Serializable{
	
	private Integer coId;
	private Integer cuId;
	private Integer rTix;
	private Double rPoints;
	
	@JsonIgnore
	private Long crDated;
	
	@JsonIgnore
	private String imgP;//profile image path
	private String imgU;// profile image URL
	
	private String uId;
	private String rPointsSt;
	
	public CassContestWinners( ) { }
	
	public CassContestWinners(Integer coId, Integer cuId, Integer rTix, Double rPoints, Long crDated) {
		this.coId = coId;
		this.cuId = cuId;
		this.rTix = rTix;
		this.rPoints = rPoints;
		this.crDated = crDated;
	}
	
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public Integer getrTix() {
		return rTix;
	}
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	public Double getrPoints() {
		if(rPoints == null) {
			rPoints = 0.0;
		}
		return rPoints;
	}
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	public Long getCrDated() {
		return crDated;
	}
	public void setCrDated(Long crDated) {
		this.crDated = crDated;
	}

	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getImgP() {
		return imgP;
	}
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	public String getImgU() {
		imgU = URLUtil.profilePicWebURByImageName(this.imgP);
		return imgU;
	}
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	public String getrPointsSt() {
		if(rPoints != null) {
			try {
				rPointsSt = TicketUtil.getRoundedValueString(rPoints);
			} catch (Exception e) {
				rPointsSt="0.00";
				e.printStackTrace();
			}
		} else {
			rPointsSt="0.00";
		}
		return rPointsSt;
	}
	public void setrPointsSt(String rPointsSt) {
		this.rPointsSt = rPointsSt;
	}



}
