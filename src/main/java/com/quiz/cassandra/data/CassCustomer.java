package com.quiz.cassandra.data;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;

@XStreamAlias("Cust")
public class CassCustomer {
	
	private Integer id;
	private String uId;//UserId
	
	@JsonIgnore
	private String email;
	@JsonIgnore
	private String phone;
	
	private Integer qLive;//customer quiz lives
	@JsonIgnore
	private Double cRewardDbl;//active rewards
	private String cReward;//active rewards
	@JsonIgnore
	private String imgP;//profile image path
	private String imgU;// profile image URL
	
	private Boolean isOtp;
	private Date createdDate;
	private Date lastUpdatedDate;
	
	private Integer sfcCount=0;
	private Integer magicWand = 0;
	private Integer rtfPoints=0;
	private Boolean isBlocked;	
	private Integer sfQuesLevel;
	private Integer loyaltyPoints;
	

	public CassCustomer(Integer id, String userId, String email, String phone, Integer qLive,Double cRewardDbl,String imgPath,
			Boolean isOtp,Integer sfcCount , Integer magicWand,Integer rtfPoints,Boolean isBlocked, Integer sfQuesLevel,Integer loyaltyPoints) {
		super();
		this.id = id;
		this.uId = userId;
		this.email = email;
		this.phone = phone;
		this.qLive = qLive;
		this.cRewardDbl = cRewardDbl;
		this.imgP =  imgPath;
		this.isOtp = isOtp;
		this.sfcCount = sfcCount;
		this.magicWand = magicWand;
		this.rtfPoints=rtfPoints;
		this.isBlocked=isBlocked;
		this.sfQuesLevel=sfQuesLevel;
		this.loyaltyPoints = loyaltyPoints;
	}
	
	public CassCustomer(Integer id, String userId, String email, String phone, Integer qLive,Double cRewardDbl,String imgPath,Boolean isOtp,
			Date createdDate,Date lastUpdatedDate,Integer sfcCount , Integer magicWand,Integer rtfPoints,Boolean isBlocked, Integer sfQuesLevel,Integer loyaltyPoints) {
		super();
		this.id = id;
		this.uId = userId;
		this.email = email;
		this.phone = phone;
		this.qLive = qLive;
		this.cRewardDbl = cRewardDbl;
		this.imgP =  imgPath;
		this.isOtp = isOtp;
		this.createdDate=createdDate;
		this.lastUpdatedDate=lastUpdatedDate;
		this.sfcCount = sfcCount;
		this.magicWand = magicWand;
		this.rtfPoints=rtfPoints;
		this.isBlocked=isBlocked;
		this.sfQuesLevel=sfQuesLevel;
		this.loyaltyPoints = loyaltyPoints;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public Integer getqLive() {
		if(qLive == null) {
			qLive = 0;
		}
		return qLive;
	}
	public void setqLive(Integer qLive) {
		this.qLive = qLive;
	}
	public String getcReward() {
		if(cRewardDbl == null) {
			cReward = "0.00";
		} else {
			try {
				cReward =  TicketUtil.getRoundedValueString(cRewardDbl);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cReward = "0.00";
			}
		}
		return cReward;
	}
	public void setcReward(String cReward) {
		this.cReward = cReward;
	}
	
	
	public Double getcRewardDbl() {
		return cRewardDbl;
	}

	public void setcRewardDbl(Double cRewardDbl) {
		this.cRewardDbl = cRewardDbl;
	}

	public String getImgP() {
		return imgP;
	}
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}
	public String getImgU() {
		imgU = URLUtil.profilePicWebURByImageName(this.imgP);
		return imgU;
	}
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}
	public Boolean getIsOtp() {
		return isOtp;
	}
	public void setIsOtp(Boolean isOtp) {
		this.isOtp = isOtp;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Integer getSfcCount() {
		if(sfcCount == null)sfcCount = 0;
		return sfcCount;
	}

	public void setSfcCount(Integer sfcCount) {
		this.sfcCount = sfcCount;
	}
	
	public Integer getMagicWand() {
		if(magicWand == null) magicWand = 0;
		return magicWand;
	}
	public void setMagicWand(Integer magicWand) {
		this.magicWand = magicWand;
	}

	public Boolean getIsBlocked() {
		if(isBlocked == null)isBlocked= false;
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public Integer getRtfPoints() {
		if(rtfPoints == null) rtfPoints = 0;
		return rtfPoints;
	}

	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}

	public Integer getSfQuesLevel() {
		return sfQuesLevel;
	}

	public void setSfQuesLevel(Integer sfQuesLevel) {
		this.sfQuesLevel = sfQuesLevel;
	}
	
	public Integer getLoyaltyPoints() {
		return loyaltyPoints;
	}
	public void setLoyaltyPoints(Integer loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}
	
}
