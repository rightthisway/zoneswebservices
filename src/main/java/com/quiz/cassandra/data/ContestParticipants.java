package com.quiz.cassandra.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("contPart")
public class ContestParticipants {

	
	private Integer coId;
	private Integer cuId;
	private String pfm;
	private String ipAdd;
	private Long jnDate;
	private Long exDate;
	private String status;
	private String uId;
	
	
	public ContestParticipants() { }
	
	public ContestParticipants(Integer coId, Integer cuId, String pfm, String ipAdd, Long jnDate, Long exDate,
			String status,String uId) {
		this.coId = coId;
		this.cuId = cuId;
		this.pfm = pfm;
		this.ipAdd = ipAdd;
		this.jnDate = jnDate;
		this.exDate = exDate;
		this.status = status;
		this.uId=uId;
	}


	public Integer getCoId() {
		return coId;
	}

	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public Integer getCuId() {
		return cuId;
	}

	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public String getPfm() {
		if(null == pfm) {
			pfm = "";
		}
		return pfm;
	}

	public void setPfm(String pfm) {
		this.pfm = pfm;
	}

	public String getIpAdd() {
		if(null == ipAdd) {
			ipAdd = "";
		}
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

	public Long getJnDate() {
		return jnDate;
	}

	public void setJnDate(Long jnDate) {
		this.jnDate = jnDate;
	}

	public Long getExDate() {
		return exDate;
	}

	public void setExDate(Long exDate) {
		this.exDate = exDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	
}
