package com.quiz.cassandra.data;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("ContestPasswordAuth")
public class ContestPasswordAuth  implements Serializable{
	
	private Integer cuId;
	private Integer coId;
	private String pwd;
	private Boolean isAuth = false;

	@JsonIgnore
	private Long crDate;
	@JsonIgnore
	private Long upDate;
	
	
	public ContestPasswordAuth() {}
	
	public ContestPasswordAuth(Integer cuId, Integer coId, String password, Boolean isAuth,Long crDate,Long upDate) {
		this.cuId = cuId;
		this.coId = coId;
		this.pwd = password;
		this.isAuth = isAuth;
		this.crDate = crDate;
		this.upDate = upDate;
	}
	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	
	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Boolean getIsAuth() {
		return isAuth;
	}

	public void setIsAuth(Boolean isAuth) {
		this.isAuth = isAuth;
	}

	public Long getCrDate() {
		return crDate;
	}

	public void setCrDate(Long crDate) {
		this.crDate = crDate;
	}

	public Long getUpDate() {
		return upDate;
	}

	public void setUpDate(Long upDate) {
		this.upDate = upDate;
	}

}
