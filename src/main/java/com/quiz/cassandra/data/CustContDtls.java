package com.quiz.cassandra.data;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("CustContAnswers")
public class CustContDtls  implements Serializable{
	
	private Integer cuId;
	private Integer coId;
	private Integer lqNo;
	private Boolean isLqCrt;
	private Boolean isLqLife;
	private Double cuRwds;
	private Integer cuLife;
	private Double sflRwds;
	
	public CustContDtls() {
		
	}
	
	public CustContDtls(Integer cuId, Integer coId, Integer lqNo, Boolean isLqCrt, Boolean isLqLife, Double cuRwds,
			Integer cuLife,Double sflRwds) {
		super();
		this.cuId = cuId;
		this.coId = coId;
		this.lqNo = lqNo;
		this.isLqCrt = isLqCrt;
		this.isLqLife = isLqLife;
		this.cuRwds = cuRwds;
		this.cuLife = cuLife;
		this.sflRwds = sflRwds;
	}
	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	public Integer getLqNo() {
		return lqNo;
	}
	public void setLqNo(Integer lqNo) {
		this.lqNo = lqNo;
	}
	
	public Boolean getIsLqCrt() {
		if(isLqCrt == null) {
			isLqCrt = false;
		}
		return isLqCrt;
	}
	public void setIsLqCrt(Boolean isLqCrt) {
		this.isLqCrt = isLqCrt;
	}
	public Boolean getIsLqLife() {
		if(isLqLife == null) {
			isLqLife = false;
		}
		return isLqLife;
	}
	public void setIsLqLife(Boolean isLqLife) {
		this.isLqLife = isLqLife;
	}
	public Double getCuRwds() {
		if(cuRwds == null) {
			cuRwds=0.0;
		}
		return cuRwds;
	}
	public void setCuRwds(Double cuRwds) {
		this.cuRwds = cuRwds;
	}
	public Integer getCuLife() {
		if(cuLife == null) {
			cuLife = 0;
		}
		return cuLife;
	}
	public void setCuLife(Integer cuLife) {
		this.cuLife = cuLife;
	}
	public Double getSflRwds() {
		if(sflRwds == null) {
			sflRwds = 0.0;
		}
		return sflRwds;
	}

	public void setSflRwds(Double sflRwds) {
		this.sflRwds = sflRwds;
	}
	
}
