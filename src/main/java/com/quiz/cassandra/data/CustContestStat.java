package com.quiz.cassandra.data;

import java.io.Serializable;

/**
 * represents CustContestStat entity
 * @author Ulaganathan
 *
 */
public class CustContestStat  implements Serializable{
	
	private Integer livesUsed;
	private Double cumulativeAnsRewards;
	
	public Integer getLivesUsed() {
		return livesUsed;
	}
	public void setLivesUsed(Integer livesUsed) {
		this.livesUsed = livesUsed;
	}
	public Double getCumulativeAnsRewards() {
		return cumulativeAnsRewards;
	}
	public void setCumulativeAnsRewards(Double cumulativeAnsRewards) {
		this.cumulativeAnsRewards = cumulativeAnsRewards;
	}

}
