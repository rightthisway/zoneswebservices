package com.quiz.cassandra.data;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents SuperFanContCustLevels entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("SuperFanContCustLevels")
public class SuperFanContCustLevels  implements Serializable{
	
	private Integer cuId;
	private Integer coId;
	private Integer qNo;
	private Integer sfStars;
	
	public SuperFanContCustLevels() {
		
	}
	
	public SuperFanContCustLevels(Integer cuId, Integer coId, Integer qNo, Integer sfStars) {
		super();
		this.cuId = cuId;
		this.coId = coId;
		this.qNo = qNo;
		this.sfStars = sfStars;
	}
	
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	public Integer getqNo() {
		if(qNo == null) {
			qNo = 0;
		}
		return qNo;
	}
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}
	public Integer getSfStars() {
		if(sfStars == null) {
			sfStars = 0;
		}
		return sfStars;
	}
	public void setSfStars(Integer sfStars) {
		this.sfStars = sfStars;
	}
	
	
}
