package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("QuizAnswerCountDetails")
public class AnswerCountInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private String cAnswer;
	private Integer aCount=0;
	private Integer bCount=0;
	private Integer cCount=0;
	//private Integer optionDCount=0;
	private Double quRwds=0.0;
	
	private Integer lifeCount=0;

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public CassError getErr() {
		return err;
	}

	public void setErr(CassError err) {
		this.err = err;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getcAnswer() {
		if(cAnswer == null) {
			cAnswer = "";
		}
		return cAnswer;
	}

	public void setcAnswer(String cAnswer) {
		this.cAnswer = cAnswer;
	}

	public Integer getaCount() {
		if(aCount == null) {
			aCount = 0;
		}
		return aCount;
	}

	public void setaCount(Integer aCount) {
		this.aCount = aCount;
	}

	public Integer getbCount() {
		if(bCount == null) {
			bCount = 0;
		}
		return bCount;
	}

	public void setbCount(Integer bCount) {
		this.bCount = bCount;
	}

	public Integer getcCount() {
		if(cCount == null) {
			cCount = 0;
		}
		return cCount;
	}

	public void setcCount(Integer cCount) {
		this.cCount = cCount;
	}

	public Double getQuRwds() {
		if(quRwds == null) {
			quRwds = 0.0;
		}
		return quRwds;
	}

	public void setQuRwds(Double quRwds) {
		this.quRwds = quRwds;
	}

	public Integer getLifeCount() {
		if(lifeCount == null) {
			lifeCount = 0;
		}
		return lifeCount;
	}

	public void setLifeCount(Integer lifeCount) {
		this.lifeCount = lifeCount;
	}
	
	
	
	
}
