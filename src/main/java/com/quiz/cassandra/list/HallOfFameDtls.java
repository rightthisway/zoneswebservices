package com.quiz.cassandra.list;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.enums.RewardType;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("HallOfFameDtls")
public class HallOfFameDtls  implements Serializable{
	
	@JsonIgnore
	private UUID id;
	private Integer cuId;
	private Integer rTix;
	private Double rPoints;
	
	@JsonIgnore
	private String imgP;//profile image path
	private String imgU;// profile image URL
	
	private String uId;
	private String rPointsSt;
	private Integer rRank;
	
	public HallOfFameDtls( ) { }
	
	public HallOfFameDtls(UUID id, Integer cuId, Integer rTix, Double rPoints) {
		this.id = id;
		this.cuId = cuId;
		this.rTix = rTix;
		this.rPoints = rPoints;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}

	public Integer getrTix() {
		return rTix;
	}
	public void setrTix(Integer rTix) {
		this.rTix = rTix;
	}

	public Double getrPoints() {
		return rPoints;
	}
	public void setrPoints(Double rPoints) {
		this.rPoints = rPoints;
	}

	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getImgP() {
		return imgP;
	}
	public void setImgP(String imgP) {
		this.imgP = imgP;
	}

	public String getImgU() {
		imgU = URLUtil.profilePicWebURByImageName(this.imgP);
		return imgU;
	}
	public void setImgU(String imgU) {
		this.imgU = imgU;
	}

	public String getrPointsSt() {
		if(rPoints != null) {
			try {
				rPointsSt = TicketUtil.getRoundedValueString(rPoints);
			} catch (Exception e) {
				rPointsSt="0.00";
				e.printStackTrace();
			}
		} else {
			rPointsSt="0.00";
		}
		return rPointsSt;
	}
	public void setrPointsSt(String rPointsSt) {
		this.rPointsSt = rPointsSt;
	}

	public Integer getrRank() {
		if(rRank == null) {
			rRank = 0;
		}
		return rRank;
	}

	public void setrRank(Integer rRank) {
		this.rRank = rRank;
	}



}
