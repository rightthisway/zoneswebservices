package com.quiz.cassandra.list;

import java.util.ArrayList;
import java.util.List;

import com.quiz.cassandra.data.CassCustomer;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("DbdInfo")
public class JackpotCreditResponse {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private CassCustomer cust;
	private Boolean hRwds;
	private List<ContestGrandWinner> jWinList=new ArrayList<>();
	
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public CassCustomer getCust() {
		return cust;
	}
	public void setCust(CassCustomer cust) {
		this.cust = cust;
	}
	public Boolean gethRwds() {
		if(hRwds == null) {
			hRwds = false;
		}
		return hRwds;
	}
	public void sethRwds(Boolean hRwds) {
		this.hRwds = hRwds;
	}
	public final List<ContestGrandWinner> getjWinList() {
		return jWinList;
	}
	public final void setjWinList(List<ContestGrandWinner> jWinList) {
		this.jWinList = jWinList;
	}

}
