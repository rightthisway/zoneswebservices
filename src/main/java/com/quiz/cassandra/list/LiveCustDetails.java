package com.quiz.cassandra.list;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.quiz.cassandra.data.CassCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.URLUtil;
@XStreamAlias("LiveCustDtls")
public class LiveCustDetails implements Serializable{
		
	
		private Integer cuId;
		private String uId;
		@JsonIgnore
		private String imgP;
		private String imgU;

		
		
		public LiveCustDetails() {
			
		}

		public LiveCustDetails(Integer cuId, String uId, String imgP) {
			this.cuId = cuId;
			this.uId = uId;
			this.imgP = imgP;
		}
		
		public LiveCustDetails(CassCustomer customer) {
			this.cuId = customer.getId();
			this.uId = customer.getuId();
			this.imgP = customer.getImgP();
		}

		public Integer getCuId() {
			if(cuId == null) {
				cuId=0;
			}
			return cuId;
		}

		public void setCuId(Integer cuId) {
			this.cuId = cuId;
		}

		public String getuId() {
			if(uId == null) {
				uId="";
			}
			return uId;
		}

		public void setuId(String uId) {
			this.uId = uId;
		}

		public String getImgP() {
			return imgP;
		}

		public void setImgP(String imgP) {
			this.imgP = imgP;
		}

		public String getImgU() {
			if(imgU == null) {
				imgU="";
			} else {
				imgU = URLUtil.profilePicWebURByImageName(this.imgP);
			}
			return imgU;
		}

		public void setImgU(String imgU) {
			this.imgU = imgU;
		}
	
		
	}