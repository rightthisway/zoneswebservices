package com.quiz.cassandra.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("LiveCustomerListInfo")
public class LiveCustomerListInfo {
	
	private Integer sts;
	private CassError err; 
	private String msg;
	private Integer tCount = 0;
	private Boolean hMore = false;
	List<LiveCustDetails> custList;
	private Integer maxPerPage = 0;
	
	
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public CassError getErr() {
		return err;
	}
	public void setErr(CassError err) {
		this.err = err;
	}
	public String getMsg() {
		if(msg == null) {
			msg = "";
		}
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer gettCount() {
		if(tCount == null) {
			tCount = 0;
		}
		return tCount;
	}
	public void settCount(Integer tCount) {
		this.tCount = tCount;
	}
	public Boolean gethMore() {
		if(hMore == null) {
			hMore = false;
		}
		return hMore;
	}
	public void sethMore(Boolean hMore) {
		this.hMore = hMore;
	}
	public List<LiveCustDetails> getCustList() {
		return custList;
	}
	public void setCustList(List<LiveCustDetails> custList) {
		this.custList = custList;
	}
	public Integer getMaxPerPage() {
		if(null == maxPerPage) {
			maxPerPage = 0;
		}
		return maxPerPage;
	}
	public void setMaxPerPage(Integer maxPerPage) {
		this.maxPerPage = maxPerPage;
	}
	
	
	
	
}
