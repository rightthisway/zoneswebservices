package com.quiz.cassandra.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizCreditProcess")
public class QuizCreditProcess {
	
	private Integer status;
	private Integer contestId;
	private Error error; 
	private String message;
	private String reProcessUrl;
	private Integer totalParticipants;
	private Integer processedParticipants;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		if(null == message) {
			message = "";
		}
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReProcessUrl() {
		if(null == reProcessUrl) {
			reProcessUrl = "";
		}
		return reProcessUrl;
	}
	public void setReProcessUrl(String reProcessUrl) {
		this.reProcessUrl = reProcessUrl;
	}
	public Integer getTotalParticipants() {
		return totalParticipants;
	}
	public void setTotalParticipants(Integer totalParticipants) {
		this.totalParticipants = totalParticipants;
	}
	public Integer getProcessedParticipants() {
		return processedParticipants;
	}
	public void setProcessedParticipants(Integer processedParticipants) {
		this.processedParticipants = processedParticipants;
	}
	
}
