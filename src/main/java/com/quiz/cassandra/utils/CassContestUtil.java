package com.quiz.cassandra.utils;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestGrandWinner;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.AnswerCountInfo;
import com.quiz.cassandra.list.CassJoinContestInfo;
import com.quiz.cassandra.list.HallOfFameDtls;
import com.quiz.cassandra.list.LiveCustDetails;
import com.quiz.cassandra.list.LiveCustomerListInfo;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.utils.RTFBotsUtil;

/**
 * 
 * @author Tamil
 *
 */
public class CassContestUtil {	
	
	
	static QuizContest startedContest = null;
	static Map<Integer,QuizContestQuestions> contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
	static  Map<Integer,CustContAnswers> customerContestAnsMap = new ConcurrentHashMap<Integer,CustContAnswers>();
	static Map<Integer,List<Integer>> customersCountMap = new ConcurrentHashMap<Integer, List<Integer>>();
	static Map<Integer,CassCustomer> liveCustoemrDtlMap = new ConcurrentHashMap<Integer, CassCustomer>();
	//static List<CassContestWinners> contestSummaryList = new ArrayList<CassContestWinners>();
	static Map<Integer,ContestParticipants> contParticipantsMap = new ConcurrentHashMap<Integer, ContestParticipants>();
	
	static List<HallOfFameDtls> thisWeekHallOfFameList = new ArrayList<HallOfFameDtls>();
	static List<HallOfFameDtls> tillDateHallOfFameList = new ArrayList<HallOfFameDtls>();
	
	private static Logger log = LoggerFactory.getLogger(CassContestUtil.class);
	
	static Integer contestSummaryDataSize = 100;
	static Integer contestWinnersSummaryDataSize = 200;
	
	static Integer hallOfFamePageMaxDataSize = 100;
	
	public static Boolean HIDE_DASHBOARD_REWARDS = Boolean.FALSE;
	
	
	public static Integer getHallOfFamePageMaxDataSize() {
		return hallOfFamePageMaxDataSize;
	}
	
	public static void updatePostMigrationStats() throws Exception {
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		CassContestUtil.hallOfFamePageMaxDataSize = 100;
		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
	}
	
	public static void clearCacheDataByContestId(Integer contestId) throws Exception {
		
		contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
		startedContest = null;
		customerContestAnsMap = new ConcurrentHashMap<Integer, CustContAnswers>();
		customersCountMap.remove(contestId);
		//contestSummaryList = new ArrayList<CassContestWinners>();
		liveCustoemrDtlMap = new ConcurrentHashMap<Integer,CassCustomer>();
		contParticipantsMap = new ConcurrentHashMap<Integer,ContestParticipants>();
		
		refreshHallOfFameForThisWeekData();
		refreshHallOfFameTillDateData();
		
		//QuizDAORegistry.getQuizContestParticipantsDAO().updateContestParticipantsForContestResetByContestId(contestId);
		//forceManualefreshSummaryDataTable();
		//QuizConfigSettingsUtil.forceUpdateConfigSettings();
		
	}
	
	public static void refreshCacheDataByContestId(Integer contestId) throws Exception {
		
	}
	
	public static QuizContest getCurrentContestByContestId(Integer contestId) {
		if(startedContest == null) {
			return QuizDAORegistry.getQuizContestDAO().get(contestId);
		}
		if(startedContest != null && startedContest.getId().equals(contestId)) {
			return startedContest;
		}
		return null;
	}
	public static Boolean refreshStartContestData(String contestType) {
		QuizContest contest = QuizDAORegistry.getQuizContestDAO().getCurrentStartedContest(contestType);
		if(contest != null) {
			startedContest = contest;
			
			List<QuizContestQuestions> questionsList = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionsByContestId(contest.getId());
			for (QuizContestQuestions question : questionsList) {
				contestQuestionMAp.put(question.getId(), question);
			}
			return true;
		}
		return false;
	}
	public static void refreshEndContestData() {
		startedContest = null;
		contestQuestionMAp = new ConcurrentHashMap<Integer,QuizContestQuestions>();
	}
	public static QuizContest getCurrentStartedContest(String contestType) {
		return startedContest;
	}
	public static QuizContestQuestions getQuizContestQuestionById(Integer questionId) {
		return contestQuestionMAp.get(questionId);
		//return QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionById(questionId);
	}
	
	public static ContestParticipants getContestParticipantsByCustId(Integer customerId) {
		return contParticipantsMap.get(customerId);
	}
	public static ContestParticipants updateContestParticipantsMapByCustId(ContestParticipants contPArticipants) {
		return contParticipantsMap.put(contPArticipants.getCuId(),contPArticipants);
	}
	
	public static void updateExistingContestParticipantsStatusByJoinContest(Integer customerId) {
		ContestParticipants contParticipant = contParticipantsMap.get(customerId);
		if(contParticipant != null) {
			contParticipant.setStatus("EXIT");
			//contParticipant.setExDate(new Date());
			CassandraDAORegistry.getContestParticipantsDAO().save(contParticipant);
		}
	}
	public static void updateExistingContestParticipantsStatusByExitContest(Integer customerId) {
		ContestParticipants contParticipant = contParticipantsMap.remove(customerId);
		if(contParticipant != null) {
			contParticipant.setStatus("EXIT");
			contParticipant.setExDate(new Date().getTime());
			CassandraDAORegistry.getContestParticipantsDAO().save(contParticipant);
		}
	}
	
	public static CassJoinContestInfo updateJoinContestCustomersCount(CassJoinContestInfo joinContestInfo,Integer contestId,CassCustomer customer) throws Exception {
		try {
		//Integer count=0;
		//boolean isExistingContestant=false;
		List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		if(contestCount.contains(customer.getId())) {
			//isExistingContestant = true;
		} else {
			contestCount.add(customer.getId());
		}
		//count = contestCount.size();
		customersCountMap.put(contestId, contestCount);

		liveCustoemrDtlMap.put(customer.getId(), customer);
		
//to capture visitors count		
		/*Set<Integer> visitosList = contestVisitorsCountMap.get(contestId);
		if(visitosList == null) {
			visitosList = new HashSet<Integer>();
		}
		if(!visitosList.add(customerId)) {
			isExistingContestant = true;
		}
		contestVisitorsCountMap.put(contestId,visitosList);*/
//end		
		
		//joinContestInfo.setTotalUsersCount(count);
		//joinContestInfo.setIsExistingContestant(isExistingContestant);
		
		
		CustContAnswers customerAnswer = getCustomerAnswers(customer.getId(), contestId);
		if(customerAnswer != null) {
			joinContestInfo.setLqNo(customerAnswer.getqNo());
			if((customerAnswer.getIsCrt() != null && customerAnswer.getIsCrt()) || 
					(customerAnswer.getIsLife() != null && customerAnswer.getIsLife())){
				joinContestInfo.setIsLstCrt(Boolean.TRUE);	
			} else {
				joinContestInfo.setIsLstCrt(Boolean.FALSE);
			}
			joinContestInfo.setCaRwds(customerAnswer.getCuRwds());
			if(customerAnswer.getCuLife() != null && customerAnswer.getCuLife() > 0) {
				joinContestInfo.setIsLifeUsed(Boolean.TRUE);
			} else {
				joinContestInfo.setIsLifeUsed(Boolean.FALSE);
			}
			
			//isExistingContestant = true;
			
		} else {
			joinContestInfo.setLqNo(0);
			joinContestInfo.setIsLstCrt(Boolean.FALSE);
			joinContestInfo.setCaRwds(0.0);
		}
		
		
		//if(!isExistingContestant) {
		//	isExistingContestant = QuizDAORegistry.getQuizContestParticipantsDAO().isExistingContestant(customerId, contestId);
		//}
		//quizJoinContestInfo.setIsExistingContestant(isExistingContestant);
		
		
//		quizJoinContestInfo.setIsContestLifeLineUsed(QuizContestUtil.checkCustomerContestLifeLineUsage(customerId, contestId));
		
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return joinContestInfo;
	}
	
	public static CassJoinContestInfo updateQuizExitCustomersCount(CassJoinContestInfo joinContestInfo,Integer contestId,Integer customerId) throws Exception {
		//Integer count=0;
		//boolean isExistingContestant=false;
		List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		//if(contestCount.remove(customerId)) {
		//	isExistingContestant = true;
		//}
		contestCount.remove(customerId);
		//count = contestCount.size();
		customersCountMap.put(contestId, contestCount);

		liveCustoemrDtlMap.remove(customerId);
		
		//quizJoinContestInfo.setTotalUsersCount(count);
		
		//quizJoinContestInfo.setIsExistingContestant(isExistingContestant);
		
		/*QuizCustomerContestAnswers customerAnswer = getCustomerAnswers(customerId, contestId);
		if(customerAnswer != null) {
			quizJoinContestInfo.setLastAnsweredQuestionNo(customerAnswer.getQuestionSNo());
			quizJoinContestInfo.setIsLastAnswerCorrect(customerAnswer.getIsCorrectAnswer());
		} else {
			quizJoinContestInfo.setLastAnsweredQuestionNo(0);
			quizJoinContestInfo.setIsLastAnswerCorrect(Boolean.FALSE);
		}
		quizJoinContestInfo.setIsContestLifeLineUsed(QuizContestUtil.checkCustomerContestLifeLineUsage(customerId, contestId));*/
		
		return joinContestInfo;
	}
	
	public static Integer getContestCustomersCount(Integer contestId) throws Exception {
		Integer count=0;
		List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount != null) {
			count = contestCount.size();
		}
		
		return count;
	}
	public static Boolean updateContestWinners(Integer contestId,Integer customerId) {
		
		/*QuizContest contest = getQuizContestByContestId(contestId);
		if(contest == null || contest.getProcessStatus() != null) {
			return false;
		}*/
		
		CassContestWinners contestWinner = new CassContestWinners();
		contestWinner.setCoId(contestId);
		contestWinner.setCuId(customerId);
		contestWinner.setCrDated(new Date().getTime());
		contestWinner.setrTix(0);
		contestWinner.setrPoints(0.0);
		
		CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinner);
		
		/*QuizContestWinners contestWinner = new QuizContestWinners();
		contestWinner.setCustomerId(customerId);
		contestWinner.setContestId(contestId);
		contestWinner.setCreatedDateTime(new Date());
		
		contestWinner.setRewardTickets(0);
		contestWinner.setRewardType(RewardType.POINTS);
		contestWinner.setRewardPoints(0.0);
		contestWinner.setRewardRank(0);
		QuizDAORegistry.getQuizContestWinnersDAO().save(contestWinner);*/

		//DAORegistry.getCustomerDAO().update(customer);
		//CassCustomerUtil.updatedCustomerUtil(customer);
		
/*		Map<Integer,QuizContestWinners> contestwinnersMap = qcu.contestWinnersMap.get(contestId);
		if(contestwinnersMap == null) {
			contestwinnersMap = new ConcurrentHashMap<Integer,QuizContestWinners>();
		}
		contestwinnersMap.put(customerId,contestWinner);
		qcu.contestWinnersMap.put(contestId, contestwinnersMap);
		
		//contestMap.put(contestId, contest);
		log.info( "[updateContestWinners -  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + contestwinnersMap.size());
*/		return true;
	}
	public static CustContAnswers getCustomerAnswers(Integer customerId,Integer contestId) throws Exception {
		return customerContestAnsMap.get(customerId);
	}	
	public static Boolean updateCustomerAnswerMap(CustContAnswers customerAnswers) throws Exception {
		customerContestAnsMap.put(customerAnswers.getCuId(), customerAnswers);
		return true;
	}
	

	public static Boolean updateCustomerAnswers(CustContAnswers customerAnswers) throws Exception {
		boolean flag=true;
		customerContestAnsMap.put(customerAnswers.getCuId(), customerAnswers);

		//capture customer contest answer rewards
		/*if(customerAnswers.getaRwds() != null && customerAnswers.getaRwds() > 0) {
			Map<Integer,Double> customerRewardsMap = customerContestAnswerRewardsMap.get(customerAnswers.getContestId());
			if(customerRewardsMap == null) {
				customerRewardsMap = new ConcurrentHashMap<Integer, Double>();
			}
			Double rewards = customerRewardsMap.get(customerAnswers.getCustomerId());
			if(rewards == null) {
				rewards = 0.0;
			}
			rewards = rewards + customerAnswers.getAnswerRewards();
			rewards = TicketUtil.getNormalRoundedValue(rewards);
			customerRewardsMap.put(customerAnswers.getCustomerId(), rewards);
			customerContestAnswerRewardsMap.put(customerAnswers.getContestId(),customerRewardsMap);
		}*/
		
		//Capture each questions correct and wrong answers count
		//Map<Integer,Map<String,Integer>> contestCountMap = questionOptionsCountMap.get(customerAnswers.getContestId());
		//if(contestCountMap == null) {
		//	contestCountMap = new ConcurrentHashMap<Integer, Map<String,Integer>>();
		//}
		//Map<String,Integer> questionCountMap = contestCountMap.get(customerAnswers.getQuestionSNo());
		//if(questionCountMap == null) {
		//	questionCountMap = new ConcurrentHashMap<String, Integer>();
		//}
		//Integer count = questionCountMap.get(customerAnswers.getAnswer());
		//if(count == null) {
		//	count=0;
		//}
		//count++;
		//questionCountMap.put(customerAnswers.getAnswer(), count);
		//contestCountMap.put(customerAnswers.getQuestionSNo(), questionCountMap);
		//questionOptionsCountMap.put(customerAnswers.getContestId(), contestCountMap);
		
		return flag;
	}
	
	public static Double getContestWinnerRewardPoints(Integer contestId) {
		
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		if(winnersList != null && !winnersList.isEmpty()) {
			return winnersList.get(0).getrPoints();
		}
		
		return null;
	}
	public static Double updateContestWinnersRewadPoints(Integer contestId) {
		
		Double rewardsPerWinner = 0.0;
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		if(winnersList != null && !winnersList.isEmpty()) {
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			Integer winnersCount = winnersList.size();
			Double contestTotalRewards = contest.getTotalRewards();
			
			rewardsPerWinner = Math.floor((contestTotalRewards/winnersCount)*100)/100;
			for (CassContestWinners contestWinner : winnersList) {
				contestWinner.setrPoints(rewardsPerWinner);
//				contestwinnersMap.put(contestWinner.getCustomerId(), contestWinner);
				
				CassandraDAORegistry.getCassContestWinnersDAO().save(contestWinner);
			}
			
			//QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(winnersList);
			contest.setPointsPerWinner(rewardsPerWinner);
			
			QuizDAORegistry.getQuizContestDAO().update(contest);
			
			System.out.println("REWARD COMPUTE : "+winnersCount+" : "+rewardsPerWinner);
		}
		//log.info( "[updateContestWinnersRewadPoints -  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  );
		return rewardsPerWinner;
	}
	
public static List<CassContestWinners> computeContestGrandWinners(Integer contestId) {
		
		List<CassContestWinners> grandWinnersList = new ArrayList<CassContestWinners>();
		Date start = new Date();
		long process =0,postPros=0,dbUpdate=0,finalPros=0;
		List<CassContestWinners> winnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		if(winnersList != null && winnersList.size() > 0) {
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			
			Integer winnersCount = winnersList.size();
			Integer grandwinnersCount = contest.getMaxFreeTicketWinners();
			
			if(winnersCount >= grandwinnersCount) {
				
				List<Integer> botCustIds = RTFBotsUtil.getAllBots();
				
				/*if(null == botCustIds || botCustIds.isEmpty()) {
					try {
						RTFBotsUtil.init();
						botCustIds = RTFBotsUtil.getAllBots();
					}catch(Exception e) {
						e.printStackTrace();
					}
				}*/
				
				List<CassContestWinners> realWinners = new ArrayList<CassContestWinners>();
				Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
				
				boolean considerAllWinnerasGrandWinner = false;
				
				if(null != botCustIds && !botCustIds.isEmpty()) {
					
					for (CassContestWinners winner : winnersList) {
						winnersMap.put(winner.getCuId(), winner);
					}
					for(Integer botCutId: botCustIds) {
						CassContestWinners winnerObj = winnersMap.remove(botCutId);
						if(null != winnerObj) {
							continue;
						}
					}
					
					if(null == winnersMap || winnersMap.isEmpty() || winnersMap.size() <= 0) {
						realWinners.addAll(winnersList);
					}else {
						realWinners.addAll(winnersMap.values());
						winnersCount = realWinners.size();
						if(winnersCount <= 0) {
							realWinners.addAll(winnersList);
						}else if(winnersCount < grandwinnersCount) {
							considerAllWinnerasGrandWinner = true;
							/*grandWinnersList.addAll(realWinners);
							grandwinnersCount = grandwinnersCount - realWinners.size();*/
						} 
					}
					
				}else {
					realWinners.addAll(winnersList);
				}
				
				if(considerAllWinnerasGrandWinner) {
					
					List<CassContestWinners> tempWinners = new ArrayList<CassContestWinners>();
					
					int realWinnerCountTemp = 0;
					if(null != realWinners && !realWinners.isEmpty() && realWinners.size() > 0) {
						realWinnerCountTemp = realWinners.size();
					}
					
					boolean considerAllBots = false;
					
					if(realWinnerCountTemp == 0) {
						 //Consider all bots for winner list
						considerAllBots = true;
					}else{
						tempWinners.addAll(realWinners);
						considerAllBots = false;
					}
					int expectedCount = grandwinnersCount - realWinnerCountTemp;
					
					Set<Integer> botCustIdsIndex = new HashSet<Integer>();
					
					winnersCount = winnersList.size();
					
					if(!considerAllBots && expectedCount > 0) {
						for(int i=0;i<expectedCount;i++) {
							Boolean flag = true;
							while(flag) {
								Random random = new Random();
								int index = random.nextInt(winnersCount);
								if(botCustIdsIndex.add(index)) {
									CassContestWinners contestWinner = winnersList.get(index);
									tempWinners.add(contestWinner);
									flag = false;
								}
							}
							 
						}
					}
					
					if(considerAllBots) {
						Set<Integer> indexList = new HashSet<Integer>();
						for(int i=0;i<grandwinnersCount;i++) {
							Boolean flag = true;
							while(flag) {
								Random random = new Random();
								int index = random.nextInt(winnersCount);
								if(indexList.add(index)) {
									CassContestWinners contestWinner = winnersList.get(index);
									grandWinnersList.add(contestWinner);
									flag = false;
								}
							}
						}
					}else {
						grandWinnersList.addAll(tempWinners);
					}
					
				} else {
					
					winnersCount = realWinners.size();
					
					Set<Integer> indexList = new HashSet<Integer>();
					for(int i=0;i<grandwinnersCount;i++) {
						Boolean flag = true;
						while(flag) {
							Random random = new Random();
							int index = random.nextInt(winnersCount);
							if(indexList.add(index)) {
								CassContestWinners contestWinner = realWinners.get(index);
								grandWinnersList.add(contestWinner);
								flag = false;
							}
						}
					}
				} 
			} else {
				grandWinnersList.addAll(winnersList);
			}
			process = (new Date().getTime()-start.getTime());
			Integer expairyDays = 1;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, expairyDays);
			Date expiryDate = new Date(cal.getTimeInMillis());
			
			List<CassContestGrandWinner> grandWinners = new ArrayList<CassContestGrandWinner>();
			for (CassContestWinners contestWinner : grandWinnersList) {
				contestWinner.setrTix(contest.getFreeTicketsPerWinner());
				
				CassContestGrandWinner grandWinner = new CassContestGrandWinner();
				grandWinner.setCuId(contestWinner.getCuId());
				grandWinner.setCoId(contestWinner.getCoId());
				grandWinner.setrTix(contest.getFreeTicketsPerWinner());
				grandWinner.setStatus("ACTIVE");
				grandWinner.setCrDated(new Date().getTime());
				grandWinner.setExDate(expiryDate.getTime());
				
				grandWinners.add(grandWinner);
			}
			postPros = new Date().getTime()-(start.getTime()+process);
			CassandraDAORegistry.getCassContestWinnersDAO().saveAll(grandWinnersList);
			CassandraDAORegistry.getCassContestGrandWinnerDAO().saveAll(grandWinners);
			//QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(grandWinnersList);
			//QuizDAORegistry.getContestGrandWinnerDAO().saveAll(grandWinners);

			dbUpdate = new Date().getTime()-(start.getTime()+process+postPros);
			//contest.setTicketWinnersCount(grandWinnersList.size());
			//QuizDAORegistry.getQuizContestDAO().update(contest);
		}
		
		for (CassContestWinners contestWinner : grandWinnersList) {
			CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
			if(customer != null) {
				contestWinner.setuId(customer.getuId());
				//contestWinner.setCustomerName(customer.getCustomerName());
				//contestWinner.setCustomerLastName(customer.getLastName());
				contestWinner.setCuId(customer.getId());
				contestWinner.setImgP(customer.getImgP());
				
			}
		}
		finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
		log.info("Grand Winner Compute : "+contestId+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
//		log.info( "[computeContestGrandWinners - grandWinnersList size  ] called on  IP --  "  + "[ " +  new QuizContestUtil().getIPAddress() + " ] "  + grandWinnersList.size() );
		return grandWinnersList;
	}
	public static List<CassContestWinners> getContestGrandWinners(Integer contestId) {
	
		/*List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestWinners> list =   CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		for (CassContestWinners contestWinner : list) {
			if(contestWinner.getrTix() > 0) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(contestWinner.getCuId());
				contestWinner.setuId(customer.getuId());
				//contestWinner.setCustomerName(customer.getCustomerName());
				//contestWinner.setCustomerLastName(customer.getLastName());
				contestWinner.setCuId(customer.getId());
				contestWinner.setImgP(customer.getImgP());
			
				winners.add(contestWinner);
			}
		}
		return winners;*/
		
		List<CassContestWinners> winners = new ArrayList<CassContestWinners>();
		List<CassContestGrandWinner> list =   CassandraDAORegistry.getCassContestGrandWinnerDAO().getContestGrandWinnersByContestId(contestId);
		for (CassContestGrandWinner grandWinner : list) {
			if(grandWinner.getrTix() > 0) {
				CassCustomer customer = CassCustomerUtil.getCustomerById(grandWinner.getCuId());
				
				CassContestWinners winner = new CassContestWinners();
				winner.setCoId(grandWinner.getCoId());
				winner.setrPoints(0.0);
				winner.setrTix(grandWinner.getrTix());
				winner.setuId(customer.getuId());
				winner.setCuId(customer.getId());
				winner.setImgP(customer.getImgP());
			
				winners.add(winner);
			}
		}
		return winners;
	}
	
	public static List<CassContestWinners> getContestSummaryData(Integer contestId)  {
		try {
			List<CassContestWinners> contestWinners = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
			if(contestWinners != null && !contestWinners.isEmpty()) {
				for (CassContestWinners winners : contestWinners) {
					CassCustomer customer = CassCustomerUtil.getCustomerById(winners.getCuId());
					if(customer != null) {
						winners.setCuId(customer.getId());
						winners.setuId(customer.getuId());
						winners.setImgP(customer.getImgP());
					}
				}
			}
			//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
			return contestWinners;
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
public static LiveCustomerListInfo getLiveCustomerDetailsInfo(Integer contestId,LiveCustomerListInfo liveCustListInfo,Integer pageNo,Integer maxRows) throws Exception {
		
		Integer totalCount=0;
		Boolean hasMoreCustomers = false;
		List<LiveCustDetails> customersList = null;
		
		List<Integer> customerIdList = null;
		List<Integer> contestCountList = customersCountMap.get(contestId);
		if(contestCountList != null) {
			totalCount = contestCountList.size();
			Integer fromIndex = maxRows*(pageNo - 1);
			Integer toIndex = maxRows*pageNo;
			if(totalCount > fromIndex) {
				if(totalCount <= toIndex) {
					toIndex = totalCount;
				} else {
					hasMoreCustomers = true;
				}
				customerIdList = new ArrayList<Integer>(contestCountList.subList(fromIndex, toIndex));
			}
			if(customerIdList != null) {
				LiveCustDetails customerDtl = null;
				customersList = new ArrayList<LiveCustDetails>();
				for (Integer customerId : customerIdList) {
					CassCustomer customer = liveCustoemrDtlMap.get(customerId);
					if(customer == null) {
						customer = CassCustomerUtil.getCustomerById(customerId);						
					}
					if(customer != null) {
						customerDtl = new LiveCustDetails(customer.getId(),customer.getuId(),customer.getImgP());
						customersList.add(customerDtl);
					}
					
				}
			}
		}
		/*MaxPerPage - Only for Mamta - Added on 11/21/20118 07:27 AM by Ulaganathan*/
		liveCustListInfo.setMaxPerPage(totalCount);
		liveCustListInfo.settCount(totalCount);
		liveCustListInfo.sethMore(hasMoreCustomers);
		liveCustListInfo.setCustList(customersList);
		
		return liveCustListInfo;
	}
	
public static AnswerCountInfo getQuestAnsCount(Integer contestId,Integer questionId,AnswerCountInfo ansCountInfo) throws Exception {
	
	Integer aCount=0,bCount=0,cCount=0;
	//List<CustContAnswers> custansList = CassandraDAORegistry.getCustContAnswersDAO().getAllCustContAnswersByQuestId(questionId);
	List<CustContAnswers> custansList = new ArrayList<CustContAnswers>(customerContestAnsMap.values());	
	
	if(custansList != null && !custansList.isEmpty()) {
		for (CustContAnswers cans : custansList) {
			if(cans.getqId().equals(questionId) && cans.getAns() != null) {
				if(cans.getAns().equalsIgnoreCase("A")) {
					aCount = aCount + 1;
				} else if(cans.getAns().equalsIgnoreCase("B")) {
					bCount = bCount + 1;
				} else if(cans.getAns().equalsIgnoreCase("C")) {
					cCount = cCount + 1;
				}
			}
		}
	}
	ansCountInfo.setaCount(aCount);
	ansCountInfo.setbCount(bCount);
	ansCountInfo.setcCount(cCount);
	
	return ansCountInfo;
}
public static boolean refreshHallOfFameForThisWeekData()  {
	try { 
		//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize, null, null, null);
		List<HallOfFameDtls> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getHallOfFameByDateFromView(hallOfFamePageMaxDataSize);
		if(winnersList != null) {
			thisWeekHallOfFameList = new ArrayList<HallOfFameDtls>(winnersList);
		}
		
		
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;
}
public static boolean refreshHallOfFameTillDateData()  {
	
	 
	try {
		//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDate(contestSummaryDataSize);
		List<HallOfFameDtls> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getHallOfFameByTillDateFromView(hallOfFamePageMaxDataSize);
		if(winnersList != null) {
			tillDateHallOfFameList = new ArrayList<HallOfFameDtls>(winnersList);
		}
		
	}catch(Exception e) {
		e.printStackTrace();
		return false;
	}
	return true;
}

public static List<HallOfFameDtls> getHallOfFameForThisWeekData()  {
	try {
		if(thisWeekHallOfFameList == null || thisWeekHallOfFameList.isEmpty()) {
			refreshHallOfFameForThisWeekData();
		}
		//return  new ArrayList<QuizContestWinners>(thisWeekSummaryList);
		return thisWeekHallOfFameList;
		
	}catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}
public static List<HallOfFameDtls> getHallOfFameForTillDateData()  {
	try {
		if(tillDateHallOfFameList == null || tillDateHallOfFameList.isEmpty()) {
			refreshHallOfFameTillDateData();
		}
		//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
		return tillDateHallOfFameList;
		
	}catch(Exception e) {
		e.printStackTrace();
		return null;
	}
}

public static Boolean updateContestRewardsToCassCustomer(Integer contestId) throws Exception {
	
	List<CassContestWinners> winnersList =  CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId); 
	if(customerContestAnsMap != null) {
		Map<Integer, CassContestWinners> winnersMap = new HashMap<Integer, CassContestWinners>();
		if(winnersList != null) {
			for (CassContestWinners winner : winnersList) {
				winnersMap.put(winner.getCuId(), winner);
			}
		}
		
		List<Integer> keyList = new ArrayList<>(customerContestAnsMap.keySet());
		for (Integer custId : keyList) {
			Double rewards = 0.0;
			Integer livesUsed = 0;
			CustContAnswers custAns = customerContestAnsMap.remove(custId);
			rewards = custAns.getCuRwds();
			livesUsed = custAns.getCuLife();
		
			CassContestWinners winner = winnersMap.remove(custId);
			if(winner != null) {
				rewards = rewards + winner.getrPoints();
			}
			if(rewards > 0 || livesUsed > 0) {
				CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(custId);
				
				rewards = rewards + customer.getcRewardDbl();
				Integer quizLives = customer.getqLive() - livesUsed;
				
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), quizLives, rewards);
			}
		}
	}
	return true;
}
public static Boolean updateCustomerContestDetailsinCassandra(Integer contestId) throws Exception {

	QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
	if(!contest.getIsCustomerStatsUpdated()) {
		Date start = new Date();
		Date startOne = new Date();
		
		startOne = new Date();
		//updateContestRewardsToCassCustomer(contestId);
		log.info("Time to Update CONT REWARDS to Customer : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		try {
			startOne = new Date();
		//QuizContestUtil.forceSummaryRefreshTable();
		//QuizContestUtil.refreshSummaryDataTable();
		//QuizContestUtil.refreshSummaryDataCache();
		//QuizContestUtil.forceManualefreshSummaryDataTable();
		log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("Time to Update ALL CONTEST STATS : "+(new Date().getTime()-start.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		contest.setIsCustomerStatsUpdated(true);
		QuizDAORegistry.getQuizContestDAO().update(contest);
		return true;
	}
	return false;
}

public static Boolean updateCustomerPostContestDetailsinSQL(Integer contestId) throws Exception {

	//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
	//if(!contest.getIsCustomerStatsUpdated()) {
		//Date fromDate = new Date();
		Date start = new Date();
		Date startOne = new Date();
		QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestDataByContestId(contestId);
		System.out.println("Time to Update Cust COntest Data : " +(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		startOne = new Date();
		QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestWinsDataByContestId(contestId);
		System.out.println("Time to Update Cust COntest Wins Data : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		/*Commented - this funtion commented in mobile screen itself. not in use */
		//startOne = new Date();
		//QuizCustomerPromocodeandContestOrderStatsScheduler.processCustomerPromoCodeAndContestOrderStats();
		//log.info("Time to Update Cust PRomo and co stats Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		startOne = new Date();
		//CustomerUtil.updateCustomerUtilForContestCustomers(contestId, fromDate);
		//log.info("Time to Update CUST UTIL for STATS : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		try {
			startOne = new Date();
		//QuizContestUtil.forceSummaryRefreshTable();
		//QuizContestUtil.refreshSummaryDataTable();
		//QuizContestUtil.refreshSummaryDataCache();

		//QuizContestUtil.forceManualefreshSummaryDataTable();
		//log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Time to Update ALL CONTEST STATS : "+(new Date().getTime()-start.getTime())+" : contestId : "+contestId+" : "+new Date());
		
		//contest.setIsCustomerStatsUpdated(true);
		//QuizDAORegistry.getQuizContestDAO().update(contest);
		return true;
	//}
	//return false;
}

	public static Map<Integer,Double> GetContestQuestionLevelCumulativeRewardsMap(QuizContest contest) throws Exception {
		
		Map<Integer,Double> questionLevelRewards = new HashMap<Integer,Double>();
		List<QuizContestQuestions> questionsList = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionsByContestId(contest.getId());
		Map<Integer,QuizContestQuestions> tempQuestSlNoMap = new HashMap<Integer,QuizContestQuestions>();
		for (QuizContestQuestions question : questionsList) {
			tempQuestSlNoMap.put(question.getQuestionSNo(), question);
		}
		Double rewards = 0.0;
		for (int i=1;i<=contest.getNoOfQuestions();i++) {
			QuizContestQuestions question = tempQuestSlNoMap.get(i);
			if(question != null && question.getQuestionRewards() != null) {
				rewards = rewards + question.getQuestionRewards();
			}
			questionLevelRewards.put(i, rewards);
		}
		return questionLevelRewards;
	}

	public static Map<Integer,SuperFanContCustLevels> GetSuperFanContCustLevelMap(QuizContest contest) throws Exception {
		
		Map<Integer,SuperFanContCustLevels> sfContCustLevelsMap = new HashMap<Integer,SuperFanContCustLevels>();
		List<SuperFanContCustLevels> sfContCustLevelsList = CassandraDAORegistry.getSuperFanContCustLevelsDAO().getAllSuperFanContCustLevels();
		for (SuperFanContCustLevels sfContCustLevel : sfContCustLevelsList) {
			sfContCustLevelsMap.put(sfContCustLevel.getCuId(), sfContCustLevel);
		}
		return sfContCustLevelsMap;
	}

}
