package com.quiz.cassandra.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.PollingCustomerRewards;
import com.zonesws.webservices.jobs.CustomerUtil;
 
public class CassCustomerUpload extends QuartzJobBean implements StatefulJob {
	
	private static boolean isMigrated = false;
	 
	public static Integer startUpload() {
		
		List<Customer> customers = DAORegistry.getCustomerDAO().getAllRewardTheFanCustomers();
		List<CustomerLoyalty> loyaltyList = DAORegistry.getCustomerLoyaltyDAO().getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints();
		Map<Integer, CustomerLoyalty> loyaltyMap = new HashMap<Integer, CustomerLoyalty>();
		for (CustomerLoyalty customerLoyalty : loyaltyList) {
			loyaltyMap.put(customerLoyalty.getCustomerId(), customerLoyalty);
		}
		
		Collection<QuizSuperFanStat> superFanStatsList = QuizDAORegistry.getQuizSuperFanStatDAO().getAll();
		Map<Integer, QuizSuperFanStat> superFanStatMap = new HashMap<Integer, QuizSuperFanStat>();
		
		for (QuizSuperFanStat obj : superFanStatsList) {
			superFanStatMap.put(obj.getCustomerId(), obj);
		}
		/*Collection<PollingCustomerRewards> magicWandList = DAORegistry.getPollingCustomerRewardsDAO().getAllCustomerWithMagicWands();
		Map<Integer, PollingCustomerRewards> magicWandMap = new HashMap<Integer, PollingCustomerRewards>();
		for (PollingCustomerRewards obj : magicWandList) {
			magicWandMap.put(obj.getCustId(), obj);
		}*/
		
		CassCustomer cassCustomer = null;
		Integer processedCut = 0, totalCustomers=customers.size();
		
		List<CassCustomer> list = new ArrayList<CassCustomer>();
		
		Double reward = null;
		Integer totalNoOfChances = null;
		Integer magicWand = null;
		
		for (Customer customer : customers) {
			try {
				CustomerLoyalty loyaltyObj = loyaltyMap.get(customer.getId());
				reward = 0.00;
				totalNoOfChances = 0;
				magicWand  = customer.getMagicWands();
				if(null != loyaltyObj) {
					reward = loyaltyObj.getActivePointsAsDouble();
				}
				QuizSuperFanStat superFanStat = superFanStatMap.get(customer.getId());
				if(superFanStat != null) {
					totalNoOfChances = superFanStat.getTotalNoOfChances();
				}
				
				cassCustomer = new CassCustomer(customer.getId(), customer.getUserId(), customer.getEmail(), 
						customer.getPhone(), customer.getQuizCustomerLives(), reward, null != customer?customer.getCustImagePath():"", 
								customer.getIsOtpVerified(),totalNoOfChances, magicWand,customer.getRtfPoints(),customer.getIsBlocked(),
								customer.getSfQuesLevel(),customer.getLoyaltyPoints());
				list.add(cassCustomer);
				
				if(processedCut != 0 && processedCut % 60 == 0) {
					CassandraDAORegistry.getCassCustomerDAO().saveAll(list);
					list = new ArrayList<CassCustomer>();
				}
				processedCut++;
				
				CustomerUtil.updatedCustomerUtil(customer);
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(null != list && !list.isEmpty()) {
			CassandraDAORegistry.getCassCustomerDAO().saveAll(list);
		}
		
		isMigrated = true;
		
		System.out.println("CassCustomerUpload: Cur Processed Count: "+processedCut+", Total Cust : "+totalCustomers+", isMigrated : "+isMigrated);
		return processedCut;
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		System.out.println("CassCustomerUpload Excute Internal Method invoked : "+ new Date());
		try {
			if(isMigrated) {
				return;
			}
			startUpload();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
  
	
}