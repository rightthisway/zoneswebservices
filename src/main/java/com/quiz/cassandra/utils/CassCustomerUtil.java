package com.quiz.cassandra.utils;

import java.util.Date;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.PollingCustomerRewards;

public class CassCustomerUtil {
	
	public static CassCustomer getCustomerById(Integer customerId){
		try {
			CassCustomer customer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
			return customer;	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void addCustomerOld(Customer customer, CustomerLoyalty customerLoyalty){
		try {
			Integer totalNoChances = 0;
			Integer magicWand = 0;
			QuizSuperFanStat superFanStat = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(customer.getId());
			
			if(null != superFanStat) {
				totalNoChances = superFanStat.getTotalNoOfChances();
			}
			PollingCustomerRewards pollingCustomerRewards = DAORegistry.getPollingCustomerRewardsDAO().getCustomerMagicWandStat(customer.getId()); 
			if(null != pollingCustomerRewards) {
				magicWand = pollingCustomerRewards.getMagicWand();
				if(magicWand == null) magicWand = 0 ;
			}
			
			/*CassCustomer obj = new CassCustomer(customer.getId(), customer.getUserId(), customer.getEmail(), 
					customer.getPhone(), customer.getQuizCustomerLives(), null != customerLoyalty?customerLoyalty.getActivePointsAsDouble():0.00, 
					customer.getCustImagePath(), customer.getIsOtpVerified(),totalNoChances , magicWand);
			CassandraDAORegistry.getCassCustomerDAO().saveCustomer(obj);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void addCustomer(Customer customer, CustomerLoyalty customerLoyalty){
		try {
			Integer totalNoChances = 0;
			Integer magicWand = 0;
			QuizSuperFanStat superFanStat = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(customer.getId());
			
			if(null != superFanStat) {
				totalNoChances = superFanStat.getTotalNoOfChances();
			}
			/*PollingCustomerRewards pollingCustomerRewards = DAORegistry.getPollingCustomerRewardsDAO().getCustomerMagicWandStat(customer.getId()); 
			if(null != pollingCustomerRewards) {
				magicWand = pollingCustomerRewards.getMagicWand();
				if(magicWand == null) magicWand = 0 ;
			}*/
			magicWand = customer.getMagicWands();
			
			CassCustomer obj = new CassCustomer(customer.getId(), customer.getUserId(), customer.getEmail(), 
					customer.getPhone(), customer.getQuizCustomerLives(), null != customerLoyalty?customerLoyalty.getActivePointsAsDouble():0.00, 
					customer.getCustImagePath(), customer.getIsOtpVerified(),customer.getSignupDate(),null,totalNoChances , magicWand,customer.getRtfPoints(),customer.getIsBlocked(),0,customer.getLoyaltyPoints());
			CassandraDAORegistry.getCassCustomerDAO().saveCustomer(obj);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void updateCustomerLives(Customer customer){
		try {
			CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(customer.getId(), customer.getQuizCustomerLives());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	 
	
	 

}
