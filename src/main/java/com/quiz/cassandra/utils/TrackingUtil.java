package com.quiz.cassandra.utils;

import java.util.Date;
import java.util.UUID;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassRtfApiTracking;
import com.zonesws.webservices.enums.WebServiceActionType;

public class TrackingUtil {

	
	public static void contestValidateAnswerAPITracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			String questionNo, String questionId, String answer, Integer contestId,Integer custId, Date startDate, Date endDate,String ip){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			String description =  actionResult+" :qNo : "+questionNo+" :qId: "+questionId+ " :ans:"+answer;
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, description, "","",0);
			CassandraDAORegistry.getCassRtfApiTrackingDAO().save(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST VALIDATE ANSWER API - CASS");
		} 
	}
	
	public static void contestAPITracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			Integer contestId,Integer custId, Date startDate, Date endDate,String ip, String appVersion){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			
			if(appVersion == null) {
				appVersion = "";
			}
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, actionResult, "",appVersion,0);
			
			CassandraDAORegistry.getCassRtfApiTrackingDAO().save(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST API - CASS");
		} 
	}
	public static void contestCommonAPITracking(String platForm,String deviceType, String sessionId,WebServiceActionType actionType,String actionResult,
			Integer contestId,Integer custId, Date startDate, Date endDate,String ip){
		try{
			if(actionResult == null) {
				actionResult = "";
			}
			
			CassRtfApiTracking obj = new CassRtfApiTracking(UUID.randomUUID(), startDate, endDate, ip, actionType.toString(), platForm, deviceType, 
					sessionId, actionResult, custId, contestId, actionResult, "","",0);
			
			CassandraDAORegistry.getCassRtfApiTrackingDAO().save(obj);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST API - CASS");
		} 
	}
}
