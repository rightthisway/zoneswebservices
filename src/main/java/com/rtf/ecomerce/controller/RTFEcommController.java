package com.rtf.ecomerce.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.list.CommonRespInfo;
import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.CoupenCode;
import com.rtf.ecomerce.data.CustCoupenCode;
import com.rtf.ecomerce.data.CustomerCart;
import com.rtf.ecomerce.data.CustomerLoyaltyPointTranx;
import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.data.OrderRefundRequestForm;
import com.rtf.ecomerce.data.RtfOrderRefund;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtf.ecomerce.data.SellerProdItemsVariValue;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.pojo.CartProductPOJO;
import com.rtf.ecomerce.pojo.CustomerCartProdDtls;
import com.rtf.ecomerce.pojo.GenRewardDTO;
import com.rtf.ecomerce.pojo.GenRewardDVO;
import com.rtf.ecomerce.pojo.LivtWinnerUploadDTO;
import com.rtf.ecomerce.pojo.RtfOrderRefundResponse;
import com.rtf.ecomerce.resp.CheckoutResp;
import com.rtf.ecomerce.resp.CustCoupenCodeResp;
import com.rtf.ecomerce.resp.CustomerCartResp;
import com.rtf.ecomerce.resp.GenericResp;
import com.rtf.ecomerce.resp.OrderResp;
import com.rtf.ecomerce.resp.ProductResp;
import com.rtf.ecomerce.util.CustRewardUploadUtil;
import com.rtf.ecomerce.util.EcommUtil;
import com.rtf.ecomerce.util.GsonCustomConfig;
import com.rtf.ecomerce.util.RTFEcommEmailScheduler;
import com.rtf.ecomerce.util.TaxJarUtil;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.rtfquiz.webservices.data.RtfGiftCardRespose;
import com.rtfquiz.webservices.enums.RedeemType;
import com.rtfquiz.webservices.sqldao.implementation.SQLDaoUtil;
import com.stripe.Stripe;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Refund;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCardInfo;
import com.zonesws.webservices.data.CustomerOrderCreditCard;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.GiftCardBrand;
import com.zonesws.webservices.data.RtfConfigContestClusterNodes;
import com.zonesws.webservices.data.RtfGiftCard;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.data.RtfGiftCardQuantity;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.utils.CountryUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.RtfOrderConfirmation;
import com.zonesws.webservices.utils.list.StripeCredentials;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/GetCustCart","/GetProductDetails","/RemoveProdFromCart","/createCustProdOrder","/liveCreateCustProdOrder","/MyProOrders","/GetInventoryDetails","/sendRTFLIVEMail",
	"/GetProducts","/AddProdToCart","/AddProdToCartLive","/GetMyCoupenCodes","/GetCartInventoryDetails","/GetRtfGiftCardsByBrand","/createRtfGiftCardOrder","/SendOrderDelEm","/SendTestEm",
	"/proOrderDtls","/SaveCustCode","/SaveCustCart","/Checkout","/deleteCart","/livtWinnerUpload","/submitRefundRequestForm","/livtCustRewardUpload","/OrderProdRefund","/LiveCheckout"})
public class RTFEcommController {

	
	@Autowired
	ServletContext context; 
	private MailManager mailManager;
	private ZonesProperty properties;
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	
	@RequestMapping(value="/GetRtfGiftCardsByBrand",method=RequestMethod.POST)
	public @ResponsePayload RtfGiftCardRespose GetGiftCardsByBrand(HttpServletRequest request,HttpServletResponse response){
		RtfGiftCardRespose cards =new RtfGiftCardRespose();
		Error error = new Error();
		//String pageNoStr = request.getParameter("pNo");
		String customerIdStr = request.getParameter("cId");
		String giftCardBrandIdStr = request.getParameter("gfcbId");
		String platForm = request.getParameter("pfm"); 
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see tickets.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Please login to see Gift Cards");
				return cards;
			}
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Customer is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Customer is not recognized");
				return cards;
			}
			
			Integer giftCardBrandId = null;
			GiftCardBrand giftCardBrand = null;
			try {
				giftCardBrandId=Integer.parseInt(giftCardBrandIdStr);
				giftCardBrand = DAORegistry.getGiftCardBrandDAO().get(giftCardBrandId);
				if(giftCardBrand==null){
					error.setDescription("Gift Card Brand is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Gift Card Brand is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Gift Card Brand is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Gift Card Brand is not recognized");
				return cards;
			}
			
			List<RtfGiftCard> list =DAORegistry.getQueryManagerDAO().getAvailableGiftCardsByBrand(giftCardBrandId); 
			for(RtfGiftCard card : list){
				List<RtfGiftCardQuantity> qList = new ArrayList<RtfGiftCardQuantity>();
				for(int i=1;i<=card.getMaxThresholdQty();i++){
					RtfGiftCardQuantity quantity = new RtfGiftCardQuantity();
					quantity.setTotalAmount(card.getAmount()*i);

					quantity.setActPrice((card.getAmount()*i));
					quantity.setReqLoyaltyPoints((card.getReqLPnts()*i));
					Double discPrice = (card.getAmount() - card.getPricewLPnts())*i;
					String discText = "Get discount of $"+discPrice+" by using "+quantity.getReqLoyaltyPoints()+" REWARD POINTS";
					quantity.setLpDiscMsg(discText);
					quantity.setActPriceStr(TicketUtil.getRoundedValueString(quantity.getActPrice()));
					quantity.setPopupMsgActPrice("Total dollars spend : $"+quantity.getActPriceStr());
					quantity.setPopupMsgLoyaltyPoints("Total reward points spend : "+quantity.getReqLoyaltyPoints());
					
					quantity.setRedeemPopupMsg("");
					
					quantity.setQty(i);
					qList.add(quantity);
				}
				if(card.getMaxThresholdQty() >= 1) {
					Double discPrice = (card.getAmount() - card.getPricewLPnts());
					String discText = "Get discount of $"+discPrice+" by using "+card.getReqLPnts()+" REWARD POINTS";
					card.setLpDiscMsg(discText);
					card.setActPriceStr(TicketUtil.getRoundedValueString(card.getAmount()));
					card.setPopupMsgActPrice("Total dollars spend : $"+card.getActPriceStr());
					card.setPopupMsgLoyaltyPoints("Total reward points spend : "+card.getReqLPnts());
				}
				card.setImageUrl(AWSFileService.getGiftCardBrandImage(giftCardBrand.getImageUrl()));
				card.setQuantity(qList);
			}
			 
			cards.setSts(1);
			cards.setCards(list);
			return cards;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while getting gift cards.");
			cards.setErr(error);
			cards.setSts(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Error occured while getting gift cards");
			return cards;
		}
	}
	
	@RequestMapping(value="/createRtfGiftCardOrder",method=RequestMethod.POST)
	public RtfOrderConfirmation createRtfGiftCardOrder(HttpServletRequest request){
		Error error = new Error();
		RtfOrderConfirmation resp = new RtfOrderConfirmation();
		try {
			String custIdStr = request.getParameter("custId");
			String gcIdStr = request.getParameter("gcId");
			String qtyStr = request.getParameter("qty");
			//String redeemTypeStr = request.getParameter("redeemType"); //dollars, dollarswithLpoints
			String primPayMethod = request.getParameter("primPayMethod");
			String secPayMethod = request.getParameter("secPayMethod");
			String primPayAmtStr = request.getParameter("primPayAmt");
			String secPayAmtStr = request.getParameter("secPayAmt");
			String payToken = request.getParameter("payToken");
			String shipIdStr = request.getParameter("shipId");
			//String isLpAppliend = request.getParameter("lpApplied");
			
			
			//String orderTotalStr = request.getParameter("orderTotal");
			//String loyaltyPointsStr = request.getParameter("loyaltyPoints");
			
		
			String configIdString = request.getParameter("configId");
			String platForm = request.getParameter("platForm");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(gcIdStr)){
				error.setDescription("Invalid gift cardid");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid gift cardid");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(primPayMethod)){
				error.setDescription("Invalid payment method");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid payment method");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shipIdStr)){
				error.setDescription("Invalid shipping address");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid shipping address");
				return resp;
			}
			
			Integer custId = 0;
			Integer qty=0;
			//Double orderTotal = 0.00;
			Integer loyaltyPoints = 0;
			Integer shipId = 0;
			Double pPayAmt=0.0;
			PaymentMethod pPayMethod = null;
			PaymentMethod sPayMethod = null;
			ApplicationPlatForm appPlatForm = null;
			Integer gcvId = null;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid Customer ID");
				return resp;
			}
			try {
				shipId = Integer.parseInt(shipIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid shipping address ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid shipping address ID");
				return resp;
			}
			
			try{
				appPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFGIFTCARDORDER,"Please send valid application platform");
				return resp;
			}
			
			try {
				pPayMethod = PaymentMethod.valueOf(primPayMethod);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid primary payment method");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid primary payment method");
				return resp;
			}
			
			if(secPayMethod!=null && !secPayMethod.isEmpty()){
				try {
					sPayMethod = PaymentMethod.valueOf(secPayMethod);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid secondary payment method");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid secondary payment method");
					return resp;
				}
			}
			try {
				pPayAmt = Double.parseDouble(primPayAmtStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Primary Payment Amount");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid Primary Payment Amount");
				return resp;
			}
			if(secPayAmtStr!=null && !secPayAmtStr.isEmpty()){
				try {
					loyaltyPoints = Integer.parseInt(secPayAmtStr);
					//sPayAmt = Double.parseDouble(secPayAmtStr);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid reward points");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid reward points");
					return resp;
				}
			}
			
			if((sPayMethod == null && loyaltyPoints > 0)){
				sPayMethod = PaymentMethod.REWARD_POINTS;
			}
			
			try {
				qty = Integer.parseInt(qtyStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid quantity");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid quantity");
				return resp;
			}
			UserAddress shipAddr = DAORegistry.getUserAddressDAO().get(shipId);
			if(shipAddr==null){
				error.setDescription("Shipping address not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Shipping address not found");
				return resp;
			}
			
			Customer cust = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid customer details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Invalid customer details");
				return resp;
			}
			if(loyaltyPoints > 0 && cust.getLoyaltyPoints() < loyaltyPoints){
				error.setDescription("You don't have enough reward points to make order");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"You don't have enought reward points to make order");
				return resp;
			}
			RtfGiftCard gcCard = null;
			RtfGiftCardQuantity giftCardQty = null;
			try {
				gcvId=Integer.parseInt(gcIdStr);
				giftCardQty = DAORegistry.getRtfGiftCardQuantityDAO().get(gcvId);
				gcCard = DAORegistry.getRtfGiftCardDAO().get(giftCardQty.getCardId());
				System.out.println("gcCard :"+gcCard +" :"+gcCard.getStatus()+" :giftCardQty:"+giftCardQty);
				if(gcCard == null || !gcCard.getStatus() || giftCardQty == null){
					error.setDescription("Gift card id is not recognized.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Gift card id is not recognized.");
					return resp;
				}
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Gift card id is not recognized.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Gift card id is not recognized.");
				return resp;
			}
			if(qty > giftCardQty.getFreeQty()){
				error.setDescription("We�re sorry, this "+gcCard.getTitle()+" gift card has already been redeemed by another fan");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"We�re sorry, this "+gcCard.getTitle()+" gift card has already been redeemed by another fan");
				return resp;
			}
			
			
			Integer reqLoyaltyPoints = giftCardQty.getReqLoyaltyPoints()*qty;
			Double reqPAyAmt = 0.0;
			Double originalAmt = giftCardQty.getAmount()*qty;
			
			if(loyaltyPoints > 0) {
				if((loyaltyPoints > reqLoyaltyPoints || loyaltyPoints < reqLoyaltyPoints)){
					error.setDescription("Reward points total mismatching.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"reward points total mismatching.");
					return resp;
				}
				reqPAyAmt = giftCardQty.getPricewLPnts() * qty;
			} else {
				reqPAyAmt = giftCardQty.getAmount() * qty;
			}
			if(pPayAmt >reqPAyAmt || pPayAmt < reqPAyAmt) {
				error.setDescription("Gift Card Total Amount mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Gift Card Total Amount mismatching.");
				return resp;
			}
			
			RtfGiftCardOrder order  = new RtfGiftCardOrder();
			order.setCardDescription(gcCard.getDescription());
			order.setCardId(gcvId);
			order.setCardTitle(gcCard.getTitle());
			order.setCreatedBy(cust.getUserId());
			order.setCreatedDate(new Date());
			order.setCustomerId(cust.getId());
			order.setIsEmailSent(false);
			order.setIsOrderFulfilled(false);
			order.setIsOrderFilledMailSent(false);
			order.setOrderType(OrderType.REGULAR);
			order.setOriginalCost(originalAmt);
			order.setQuantity(qty);
			order.setRedeemedRewards(0.0);
			order.setStatus("OUTSTANDING");
			order.setShippingAddressId(shipAddr.getId());
			order.setRedeemType(RedeemType.dollars);
			order.setpPayType(primPayMethod);
			order.setsPayType(secPayMethod);
			
			RtfGiftCardQuantity validationObj = DAORegistry.getRtfGiftCardQuantityDAO().get(gcvId);
			if(validationObj.getFreeQty() == 0 || qty > validationObj.getFreeQty()){
				error.setDescription("We�re sorry, this "+gcCard.getTitle()+" gift card has already been redeemed by another fan");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Gift Card Total Amount mismatching.");
				return resp;
			}
			
			Integer freeQty = validationObj.getFreeQty()-qty;
			Integer usedQty = validationObj.getUsedQty()+qty;
			
			validationObj.setFreeQty(freeQty);
			validationObj.setUsedQty(usedQty);
			
			DAORegistry.getRtfGiftCardQuantityDAO().update(validationObj);
			DAORegistry.getRtfGiftCardOrderDAO().save(order);
			
			try {
				UserAddress shippingAddr = DAORegistry.getUserAddressDAO().get(shipId);
				CustomerOrderDetail orderDetail = new CustomerOrderDetail();
				orderDetail.setShippingAddress1(shippingAddr.getAddressLine1());
				orderDetail.setShippingAddress2(shippingAddr.getAddressLine2());
				orderDetail.setShippingCountryId(shippingAddr.getCountry().getId());
				orderDetail.setShippingStateId(shippingAddr.getState().getId());
				orderDetail.setShippingCity(shippingAddr.getCity());
				orderDetail.setShippingCountry(shippingAddr.getCountryName());
				orderDetail.setShippingEmail(shippingAddr.getEmail());
				orderDetail.setShippingFirstName(shippingAddr.getFirstName());
				orderDetail.setShippingLastName(shippingAddr.getLastName());
				orderDetail.setCtyPhCode(null != shippingAddr.getCtyPhCode() && !shippingAddr.getCtyPhCode().isEmpty()?shippingAddr.getCtyPhCode():CountryUtil.defaultCountryPhoneCode);
				orderDetail.setShippingPhone1(shippingAddr.getPhone1());
				orderDetail.setShippingPhone2(shippingAddr.getPhone2());
				orderDetail.setShippingState(shippingAddr.getStateName());
				orderDetail.setShippingZipCode(shippingAddr.getZipCode());
				orderDetail.setOrderId(order.getId());
				orderDetail.setOrderType("GIFTCARD");
				DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			CustomerLoyaltyPointTranx loyaltyPointTrax =  null;
			String sTranxId = null;
			//Double pPayAmt = orderTotal - loyaltyPoints;
			Double transAmt = (pPayAmt * 100);
			
			if(sPayMethod!=null && loyaltyPoints > 0){
				loyaltyPointTrax = new CustomerLoyaltyPointTranx();
				loyaltyPointTrax.setCrBy(cust.getUserId());
				loyaltyPointTrax.setCrDate(new Date());
				loyaltyPointTrax.setCustId(custId);
				loyaltyPointTrax.setEarnPoints(0);
				loyaltyPointTrax.setSpentPoints(loyaltyPoints);
				loyaltyPointTrax.setBeforePoints(cust.getLoyaltyPoints());
				loyaltyPointTrax.setAfterPoints(cust.getLoyaltyPoints()- loyaltyPoints);
				loyaltyPointTrax.setStatus("PENDING");
				loyaltyPointTrax.setOrderId(order.getId());
				loyaltyPointTrax.setTranxType("GIFTCARD");
				
				//EcommDAORegistry.getLoylalty
			}
			System.out.println("PROD : custId ---->"+custId);
			System.out.println("PROD : orderId ---->"+order.getId());
			System.out.println("PROD : token ---->"+payToken);
			System.out.println("PROD : pPayMethod ---->"+primPayMethod);
			System.out.println("PROD : sPayMethod ---->"+secPayMethod);
			System.out.println("PROD : transactionAmountStr ---->"+transAmt);
			System.out.println("PROD : custloyaltyPoints ---->"+cust.getLoyaltyPoints());
			System.out.println("PROD : reqrewardPoints ---->"+secPayAmtStr);
			
			String description = "Payment for giftcard order Id-"+order.getId();
			CustomerCardInfo customerCardInfo = null;
			CustomerOrderCreditCard orderCreditCard = null;
			Map<String, Object> chargeParams = new HashMap<String, Object>();
			chargeParams.put("amount", transAmt.intValue());
			chargeParams.put("currency", "usd");
			chargeParams.put("description", description);
			
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			//Stripe.apiKey = key;
			Stripe.apiKey = "sk_test_HZ0yhYYAoU6tyGbi0zAopfae";
			switch (pPayMethod) {
				case CREDITCARD:
					try {
						List<CustomerCardInfo> customerCardInfos = DAORegistry.getCustomerCardInfoDAO().getAllActiveStripeRegisteredCardsByCustomerId(custId); 
						String stripeCustId ="";
						
						if(null != customerCardInfos && !customerCardInfos.isEmpty()){
							for (CustomerCardInfo cardInfo : customerCardInfos) {
								stripeCustId = cardInfo.getsCustId();
								break;
							}
						}
						
						com.stripe.model.Customer stripeCustomer = null;
						Card card = null;
						if(null != stripeCustId && !stripeCustId.isEmpty() ){
							System.out.println("GC SC : Stripe Customer Id----->"+stripeCustId);
							stripeCustomer = com.stripe.model.Customer.retrieve(stripeCustId);
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("source", payToken);
							card = (Card)stripeCustomer.getSources().create(params);
							System.out.println("GC SC : Stripe Customer card Id----->"+card.getId());
						}else{
							String custName = cust.getCustomerName()+" "+cust.getLastName();
							Map<String, Object> customerParams = new HashMap<String, Object>();
							customerParams.put("source", payToken);
							customerParams.put("description", "Customer Name:"+custName+", Customer Email :"+cust.getEmail());
							
							Map<String, String> initialMetadata = new HashMap<String, String>();
						    initialMetadata.put("customer_id", String.valueOf(cust.getId()));
						    initialMetadata.put("customer_email", cust.getEmail());
						    initialMetadata.put("customer_name", custName);
						    customerParams.put("metadata", initialMetadata);
						    
						    stripeCustomer = com.stripe.model.Customer.create(customerParams);
						    System.out.println("GC NS : Stripe Source ----->"+stripeCustomer.getSources());
						    card = (Card) stripeCustomer.getSources().getData().get(0);
						    
						    
						    System.out.println("GC NS : Stripe Customer Id----->"+stripeCustomer.getId());
						    System.out.println("GC NS: Stripe Customer Card Id----->"+card.getId());
						}
						
						
						customerCardInfo = new CustomerCardInfo();
						customerCardInfo.setCustomerId(custId);
						customerCardInfo.setsCustCardId(card.getId());
						customerCardInfo.setsCustId(stripeCustomer.getId());
						customerCardInfo.setShowToCustomer(false);
						customerCardInfo.setCardType(card.getBrand());
						customerCardInfo.setCreateDate(new Date());
						customerCardInfo.setLastUpdated(new Date());
						customerCardInfo.setMonth(card.getExpMonth());
						customerCardInfo.setYear(card.getExpYear());
						customerCardInfo.setLastFourDigit(card.getLast4());
						customerCardInfo.setStatus(Status.ACTIVE);
						
						chargeParams.put("customer", stripeCustomer.getId());
						chargeParams.put("card", card.getId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
					
				case IPAY:
					if(TextUtil.isEmptyOrNull(payToken)){
						error.setDescription("Stripe token is mandatory.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFGIFTCARDORDER,"Product Type is mandatory");
						return resp;
					}
					chargeParams.put("source", payToken);
					break;
				
				case GOOGLEPAY:
					if(TextUtil.isEmptyOrNull(payToken)){
						error.setDescription("Stripe token is mandatory.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFGIFTCARDORDER,"Stripe token is mandatory");
						return resp;
					}
					chargeParams.put("source", payToken);
					break;
	
	
				default:
					break;
					
			}
			System.out.println("GC : INSIDE STRIPE TRANSACTION");
			Charge charge = null;
			try {
				Map<String, String> initialMetadata = new HashMap<String, String>();
				initialMetadata.put("Platform", String.valueOf(appPlatForm));
				initialMetadata.put("Source", String.valueOf(pPayMethod));
				initialMetadata.put("CustomerName", cust.getCustomerName()+" "+cust.getLastName());
				initialMetadata.put("CustomerEmail", cust.getEmail());
			/*    initialMetadata.put("EventName", event.getEventName());
			    initialMetadata.put("EventDate", event.getEventDateTime());
			    initialMetadata.put("VenueName", event.getVenueName());*/
			    initialMetadata.put("Quantity",String.valueOf( qty));
			    chargeParams.put("metadata", initialMetadata);
			    
				System.out.println("GC : STRIPE TRANSACTION - BEGINS: Payment Method :"+primPayMethod+", token :"+payToken);
				charge = Charge.create(chargeParams);
				
				
				if(charge.getStatus().equalsIgnoreCase("succeeded")){
					System.out.println("GC : STRIPE TRANSACTION STATUS :"+charge.getStatus()+", TRXID :"+charge.getId());
					try{
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod,description, transAmt, payToken, charge.getId(), charge.getStatus());
					}catch (Exception e) {
						e.printStackTrace();
					}
					try{
						if(customerCardInfo != null){
							
							DAORegistry.getCustomerCardInfoDAO().saveOrUpdate(customerCardInfo);
							
							orderCreditCard = new CustomerOrderCreditCard();
							orderCreditCard.setCustomerId(custId);
							orderCreditCard.setCardId(customerCardInfo.getId());
							orderCreditCard.setTicketGroupId(-1);
							orderCreditCard.setTrxAmount(transAmt);
							orderCreditCard.setTransactionId(charge.getId());
							DAORegistry.getCustomerOrderCreditCardDAO().saveOrUpdate(orderCreditCard);
							
						}
					}catch (Exception e) {
						e.printStackTrace();
					}
					
				}else{
					String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():charge.getStatus();
					System.out.println("GC : STRIPE TRANSACTION FAILURE : "+failure);
					try{
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod, description, transAmt, 	payToken, "", failure);
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					error.setDescription(failure);
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,charge.getFailureMessage());
					return resp;
				}
				System.out.println("GC : STRIPE TRANSACTION - ENDS: Payment Method :"+primPayMethod+", token :"+payToken+", description: "+description);
			} catch (Exception e) {
				e.printStackTrace();
				try{	
					
					String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():e.getMessage();
					DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod, description, transAmt, payToken, "", failure);
					
				}catch (Exception e1) {
					e1.printStackTrace();
				}
				error.setDescription(e.getMessage());
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFGIFTCARDORDER,"Exception Occurred");
				return resp;
			}
			
			
			order.setStatus("ACTIVE");
			order.setpTrnxId(charge.getId()); 
			if(sPayMethod!=null && loyaltyPoints > 0){
				sTranxId = "REWARD POINTS_"+order.getId();
				order.setsTrnxId(sTranxId);
				loyaltyPointTrax.setStatus("ACTIVE");
				cust.setLoyaltyPoints(cust.getLoyaltyPoints() - loyaltyPoints);
				EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().update(loyaltyPointTrax);
				
				DAORegistry.getCustomerDAO().update(cust);
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(custId, cust.getLoyaltyPoints());
				
				CustomerUtil.updatedCustomerUtil(cust);
			}
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 5);

			DAORegistry.getRtfGiftCardOrderDAO().update(order);
			
			
			//order.setOrderProds(orderProdList);
			//resp.setOrder(order);
			resp.setoId(order.getId());
			resp.setMsg("Order created successfully.");
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while creating customer cart order");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFGIFTCARDORDER,"Error occured while creating customer cart order");
			return resp;
		}
	}
	@RequestMapping(value="/MyProOrders",method=RequestMethod.POST)
	public OrderResp getMyProOrders(HttpServletRequest request){
		Error error = new Error();
		OrderResp resp = new OrderResp();
		try {
			String custIdStr = request.getParameter("custId");
			String configIdString = request.getParameter("configId");
			String platform = request.getParameter("platForm");
			String ip = request.getHeader("x-forwarded-for");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.MYPRODORDERS,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.MYPRODORDERS,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(ip)){
				ip="127.0.0.1";
			}
			if(TextUtil.isEmptyOrNull(platform)){
				platform="IOS";
			}
			
			Integer custId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.MYPRODORDERS,"Invalid Customer ID");
				return resp;
			}
			
			List<CustomerOrder> orders = EcommDAORegistry.getCustomerProdOrderDAO().getAllCustomerOrdersByCustId(custId);
			if(orders==null || orders.isEmpty()){
				error.setDescription("No orders found.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.MYPRODORDERS,"No orders found");
				return resp;
			}
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 5);
			
			for(CustomerOrder or : orders){
				List<OrderProducts> products = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderId(or.getId());
				or.setDelvDateStr(dateTimeFormat1.format(cal.getTime()));
				or.setOrdProducts(products);
				or.setDelvStatus("PENDING");
				Boolean isPending = false;
				Boolean isCompleted = false;
				Boolean isCancelled = false;
				for(OrderProducts prod : products){
					if(prod.getStatus() == null || prod.getStatus().equalsIgnoreCase("ACCEPTED") || prod.getStatus().equalsIgnoreCase("ACTIVE")){
						isPending = true;
					}else if(prod.getStatus().equalsIgnoreCase("FULFILLED")){
						prod.setDelFileUrl(URLUtil.getProductDownloadUrl(request, prod.getId(), prod.getOrderId(), configIdString, custId, platform, ip));
						prod.setShippingStatus("DELIVERED");
						isCompleted = true;
					}else if(prod.getStatus().equalsIgnoreCase("VOIDED")){
						prod.setShippingStatus("CANCELLED");
						isCancelled = true;
					}
					or.setNoOfItems(or.getNoOfItems() + prod.getQty());
				}
				
				if(isPending){
					or.setDelvStatus("PENDING");
				}else if(isCompleted){
					or.setDelvStatus("DELIVERED");
				}else if(isCancelled){
					or.setDelvStatus("CANCELLED");
				}
				
				if(products!=null && !products.isEmpty()){
					or.setpImgUrl(products.get(0).getpImgUrl());
				}else{
					or.setpImgUrl("http://34.202.149.190:8083/saasimages/uploads/AMIT202002CID_1583941871101.jpg");
				}
			}
			resp.setOrders(orders);
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting customer orders");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.MYPRODORDERS,"Error occured while gettting customer orders");
			return resp;
		}
	}
	
	@RequestMapping(value="/proOrderDtls",method=RequestMethod.POST)
	public OrderResp getProOrderDtls(HttpServletRequest request){
		Error error = new Error();
		OrderResp resp = new OrderResp();
		try {
			String custIdStr = request.getParameter("custId");
			String orderIdStr = request.getParameter("orderId");
			String configIdString = request.getParameter("configId");
			String platform = request.getParameter("platForm");
			String ip = request.getHeader("x-forwarded-for");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODORDERDTLS,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Invalid Order ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODORDERDTLS,"Invalid Order ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODORDERDTLS,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(ip)){
				ip="127.0.0.1";
			}
			if(TextUtil.isEmptyOrNull(platform)){
				platform="IOS";
			}
			
			Integer orderId = 0;
			try {
				orderId = Integer.parseInt(orderIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Order ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODORDERDTLS,"Invalid Order ID");
				return resp;
			}
			Integer custId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODORDERDTLS,"Invalid Customer ID");
				return resp;
			}
			
			CustomerOrder order = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
			if(order==null){
				error.setDescription("Order Id Not Exists");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODORDERDTLS,"Order Id Not Exists");
				return resp;
			}
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 5);
			
			List<OrderProducts> products = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderId(order.getId());
			order.setDelvStatus("PENDING");
			for(OrderProducts prod : products){
				order.setNoOfItems(order.getNoOfItems() + prod.getQty());
				 if(prod.getStatus().equalsIgnoreCase("FULFILLED")){
					prod.setDelFileUrl(URLUtil.getProductDownloadUrl(request, prod.getId(), prod.getOrderId(), configIdString, custId, platform, ip));
					prod.setShippingStatus("DELIVERED");
				}else if(prod.getStatus().equalsIgnoreCase("VOIDED")){
					prod.setShippingStatus("CANCELLED");
					order.setDelvStatus("CANCELLED");
				}
			}
			
			order.setDelvDateStr(dateTimeFormat1.format(cal.getTime()));
			order.setOrdProducts(products);
			
			if(products!=null && !products.isEmpty()){
				order.setpImgUrl(products.get(0).getpImgUrl());
			}else{
				order.setpImgUrl("http://34.202.149.190:8083/saasimages/uploads/AMIT202002CID_1583941871101.jpg");
			}
			resp.setOrder(order);
			//resp.setOrders(orders);
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting customer orders");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODORDERDTLS,"Error occured while gettting customer orders");
			return resp;
		}
	}
	
	@RequestMapping(value="/GetProducts",method=RequestMethod.POST)
	public ProductResp getProducts(HttpServletRequest request){
		Error error = new Error();
		ProductResp resp = new ProductResp();
		try {
			String custIdStr = request.getParameter("custId");
			String configIdString = request.getParameter("configId");
			
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"You are not Authorized");
				return resp;
			}
			
			List<SellerProductsItems> products = EcommDAORegistry.getSellerProductsItemsDAO().getProductsByStatus("ACTIVE");
			if(products==null || products.isEmpty()){
				error.setDescription("New products arriving soon.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Products are not available at this moment.");
				return resp;
			}
			
			Date today = new Date();
			List<SellerProductsItems> finProds = new ArrayList<SellerProductsItems>();
			for(SellerProductsItems prod : products){
				Double codeDiscount = 0.00;
				if(prod.getIsGameProd()!=null && prod.getIsGameProd()){
					List<CoupenCode> codes = EcommDAORegistry.getEcommQueryManagerDAO().getProductCouponCodes(prod.getSpiId(),"CONTEST");
					for(CoupenCode cCode : codes){
						if(cCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
							codeDiscount =  EcommUtil.getNormalRoundedValue((prod.getPsMinPrice()*cCode.getCpnDisc())/100);
						}else{
							codeDiscount = EcommUtil.getNormalRoundedValue(cCode.getCpnDisc());
						}
						prod.setInGameDiscPrice(EcommUtil.getNormalRoundedValue(prod.getPsMinPrice() - codeDiscount));
						break;
					}
					if(prod.getInGameDiscPrice()!=null && prod.getInGameDiscPrice() > 0){
						prod.setGameProdDesc("GAME SHOW ITEM");
						prod.setInGameDiscStr("Game Winners Pay");
					}
				}
				if(prod.getpExpDate().after(today)){
					finProds.add(prod);
				}
			}
			resp.setProds(finProds);
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting products.");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETPRODUCTS,"Error occured while gettting products.");
			return resp;
		}
	}
	
	
	
	
	@RequestMapping(value="/GetProductDetails",method=RequestMethod.POST)
	public ProductResp getProductDetails(HttpServletRequest request){
		Error error = new Error();
		ProductResp resp = new ProductResp();
		try {
			String custIdStr = request.getParameter("custId");
			String prodIdStr = request.getParameter("prodId");
			String configIdString = request.getParameter("configId");
			
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(prodIdStr)){
				error.setDescription("Invalid product id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product id");
				return resp;
			}
			
			Integer custId = 0;
			Integer prodId = 0;
			try {
				prodId = Integer.parseInt(prodIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product details");
				return resp;
			}
			
			Customer cust  =  null;
			if(!TextUtil.isEmptyOrNull(custIdStr)){
				try {
					custId = Integer.parseInt(custIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid product details");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product details");
					return resp;
				}
				
				cust  = DAORegistry.getCustomerDAO().get(custId);
				if(cust == null){
					error.setDescription("Invalid logged in customer details.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid logged in customer details.");
					return resp;
				}
			}
			
			
			SellerProductsItems product = EcommDAORegistry.getSellerProductsItemsDAO().getProductsById(prodId);
			if(product==null){
				error.setDescription("Product is no longer available seems like its sold out.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Product is no longer available seems like its sold out.");
				return resp;
			}
			
			SellerProdItemsVariInventory inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getAvailableInventoryByProdId(product.getSpiId());
			if(inv==null){
				product.setIsSoldOut(true);
				product.setUpdDate(new Date());
				EcommDAORegistry.getSellerProductsItemsDAO().update(product);
				error.setDescription("Product is no longer available seems like its sold out.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Product is no longer available seems like its sold out.");
				return resp;
			}
			
			
			List<SellerProdItemsVariValue> finalValues = null;
			List<SellerProdItemsVariName> options = EcommDAORegistry.getSellerProdItemsVariNameDAO().getAllProductVariants(product.getSpiId());
			for(SellerProdItemsVariName opt : options){
				List<SellerProdItemsVariValue> values = EcommDAORegistry.getSellerProdItemsVariValueDAO().getOptionValues(opt.getSlrpiVarId());
				finalValues = new ArrayList<SellerProdItemsVariValue>();
				if(values!=null && !values.isEmpty()){
					for(SellerProdItemsVariValue val : values){
						if(inv.getVvId1() != null && val.getSlrpiVarValId().equals(inv.getVvId1())){
							finalValues.add(val);
						}
						if(inv.getVvId2() != null && val.getSlrpiVarValId().equals(inv.getVvId2())){
							finalValues.add(val);
						}
						if(inv.getVvId3() != null && val.getSlrpiVarValId().equals(inv.getVvId3())){
							finalValues.add(val);
						}
						if(inv.getVvId4() != null && val.getSlrpiVarValId().equals(inv.getVvId4())){
							finalValues.add(val);
						}
					}
					for(SellerProdItemsVariValue val : values){
						if(!finalValues.contains(val)){
							finalValues.add(val);
						}
					}
				}
				opt.setValues(finalValues);
			} 
			
			
			/*if(optList.size() == 1){
				inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByOptionValue(optList.get(0), null, null, null);
			}else if(optList.size() == 2){
				inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByOptionValue(optList.get(0), optList.get(1), null, null);
			}else if(optList.size() ==3){
				inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByOptionValue(optList.get(0), optList.get(1), optList.get(2), null);
			}else if(optList.size() ==4){
				inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByOptionValue(optList.get(0), optList.get(1), optList.get(2), optList.get(3));
			}*/
			
			
			Double codeDiscount = 0.00;
			List<String> offerList = new ArrayList<String>();
			if(!inv.getIsRwdPointProd()){
				resp.setOfferHdr("Offers and Discounts");
				List<CoupenCode> codes = EcommDAORegistry.getEcommQueryManagerDAO().getProductCouponCodes(prodId, "PUBLIC");
				for(CoupenCode code : codes){
					offerList.addAll(EcommUtil.getOfferList(code.getDiscType(), code.getCpnDisc(), inv.getSellPrice(),code.getCpnCode()));
					break;
				}
				List<CoupenCode> custCodes = EcommDAORegistry.getEcommQueryManagerDAO().getProductCouponCodes(prodId, "CONTEST");
				for(CoupenCode code : custCodes){
					offerList.addAll(EcommUtil.getOfferList(code.getDiscType(), code.getCpnDisc(), inv.getSellPrice(),""));
					if(product.getIsGameProd()!=null && product.getIsGameProd()){
						if(code.getDiscType().equalsIgnoreCase("PERCENTAGE")){
							codeDiscount =  EcommUtil.getNormalRoundedValue((inv.getSellPrice()*code.getCpnDisc())/100);
						}else{
							codeDiscount = EcommUtil.getNormalRoundedValue(code.getCpnDisc());
						}
						product.setInGameDiscStr("Game Winners Pay");
						product.setInGameDiscPrice(EcommUtil.getNormalRoundedValue(inv.getSellPrice() - codeDiscount));
						inv.setInGameDiscStr("Game Winners Pay");
						inv.setInGameDiscPrice(EcommUtil.getNormalRoundedValue(inv.getSellPrice() - codeDiscount));
						
						product.setInGameDiscPrice(inv.getInGameDiscPrice());
						product.setPsMinPrice(inv.getSellPrice());
						product.setpRegMinPrice(inv.getRegPrice());
						
					}
					break;
				}
			}
			
			if(inv == null || inv.getAvlQty() <= 0){
				resp.setInvStatus("Sold out");
				resp.setAvlQty(inv.getAvlQty());
			}
			if(inv.getReqLPnts()!=null && inv.getReqLPnts() > 0 && inv.getPriceAftLPnts() > 0 && !inv.getIsRwdPointProd()){
				offerList.add("Get additional discount $"+EcommUtil.getRoundedValueString(inv.getSellPrice()-inv.getPriceAftLPnts())+" off on use of "+inv.getReqLPnts()+" reward point for each quantity.");
			}
			
			if(product.getMaxCustQty() < inv.getAvlQty()){
				resp.setAvlQty(product.getMaxCustQty() );
			}else{
				resp.setAvlQty(inv.getAvlQty());
			}
			resp.setMaxQtyMsg("Maximum quantity limit reached for this product.");
			resp.setOfferList(offerList);
			resp.setOptions(options);
			resp.setInv(inv);
			resp.setProd(product);
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting product details");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETPRODUCTS,"Error occured while gettting product details");
			return resp;
		}
	}
	
	
	
	@RequestMapping(value="/GetInventoryDetails",method=RequestMethod.POST)
	public ProductResp getInventoryDetails(HttpServletRequest request){
		Error error = new Error();
		ProductResp resp = new ProductResp();
		try {
			String custIdStr = request.getParameter("custId");
			String qtyStr = request.getParameter("qty");
			String prodIdStr = request.getParameter("prodId");
			String opValId1 = request.getParameter("opValId1");
			String opValId2 = request.getParameter("opValId2");
			String opValId3 = request.getParameter("opValId3");
			String opValId4 = request.getParameter("opValId4");
			String configIdString = request.getParameter("configId");
			
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(prodIdStr)){
				error.setDescription("Invalid product id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product id");
				return resp;
			}
			
			Integer custId = 0;
			Integer prodId = 0;
			Integer cartSize = 0;
			Customer cust  = null;
			try {
				prodId = Integer.parseInt(prodIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product details");
				return resp;
			}
			
			if(!TextUtil.isEmptyOrNull(custIdStr)){
				try {
					custId = Integer.parseInt(custIdStr);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				cust  = DAORegistry.getCustomerDAO().get(custId);
				if(cust == null){
					error.setDescription("Invalid logged in customer details.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid logged in customer details.");
					return resp;
				}
			}
			
			
			
			SellerProductsItems product = EcommDAORegistry.getSellerProductsItemsDAO().getProductsById(prodId);
			if(product==null){
				error.setDescription("Product not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Product not found");
				return resp;
			}
			
			if(opValId1 == null || opValId1.isEmpty()){
				error.setDescription("Invalid product variant details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product variant details");
				return resp;
			}
			
			Integer valId1 = 0;
			Integer valId2 = 0;
			Integer valId3 = 0;
			Integer valId4 = 0;
			try {
				valId1 = Integer.parseInt(opValId1);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product variant value id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product variant value id");
				return resp;
			}
			try {
				valId2 = Integer.parseInt(opValId2);
			} catch (Exception e) {
			}
			try {
				valId3 = Integer.parseInt(opValId3);
			} catch (Exception e) {
			}
			try {
				valId4 = Integer.parseInt(opValId4);
			} catch (Exception e) {
			}
			
			System.out.println("PRODUCT ID:"+prodId+"   |   valId1:"+valId1+"    |    valId2:"+valId2+"   |   valId3:"+valId3+"   |   valId4"+valId4);
			SellerProdItemsVariInventory inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByOptionValue(valId1, valId2, valId3, valId4);
			if(inv == null || inv.getAvlQty() <= 0){
				resp.setInvStatus("Sold out");
				resp.setAvlQty(0);
			}
			if(product.getMaxCustQty() < inv.getAvlQty()){
				resp.setAvlQty(product.getMaxCustQty() );
			}else{
				resp.setAvlQty(inv.getAvlQty());
			}
			
			Double codeDiscount = 0.00;
			List<CoupenCode> custCodes = EcommDAORegistry.getEcommQueryManagerDAO().getProductCouponCodes(prodId, "CONTEST");
			for(CoupenCode code : custCodes){
				if(product.getIsGameProd()!=null && product.getIsGameProd()){
					if(code.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						codeDiscount =  EcommUtil.getNormalRoundedValue((inv.getSellPrice()*code.getCpnDisc())/100);
					}else{
						codeDiscount = EcommUtil.getNormalRoundedValue(code.getCpnDisc());
					}
					product.setInGameDiscStr("Game Winners Pay");
					product.setInGameDiscPrice(EcommUtil.getNormalRoundedValue(product.getPsMinPrice() - codeDiscount));
					inv.setInGameDiscStr("Game Winners Pay");
					inv.setInGameDiscPrice(EcommUtil.getNormalRoundedValue(inv.getSellPrice() - codeDiscount));
					
					product.setInGameDiscPrice(inv.getInGameDiscPrice());
					product.setPsMinPrice(inv.getSellPrice());
					product.setpRegMinPrice(inv.getRegPrice());
				}
				break;
			}
			
			
			if(cust!=null){
				CustomerCart cart = EcommDAORegistry.getCustomerCartDAO().getCustomerCartByInventoryId(product.getSpiId(),inv.getSpivInvId(), custId);
				if(cart!=null){
					cartSize = cart.getQty();
				}
			}
			
			resp.setMaxQtyMsg("Maximum quantity limit reached for this product.");
			if(cartSize == resp.getAvlQty() || cartSize.equals(resp.getAvlQty())){
				resp.setMaxQtyMsg("The maximum quantity of this item has already been added to your cart.");
			}
			
			product.setpRegMinPrice(inv.getRegPrice());
			product.setPsMinPrice(inv.getSellPrice());
			
			resp.setProd(product);
			resp.setInv(inv);
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting product inventory details.");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETPRODUCTS,"Error occured while gettting product details");
			return resp;
		}
	}
	
	
	@RequestMapping(value="/createCustProdOrder",method=RequestMethod.POST)
	public OrderResp createCustProdOrder(HttpServletRequest request){
		Error error = new Error();
		OrderResp resp = new OrderResp();
		try {
			String custIdStr = request.getParameter("custId");
			String orderTotalStr = request.getParameter("orderTotal");
			String loyaltyPointsStr = request.getParameter("loyaltyPoints");
			String payToken = request.getParameter("payToken");
			String shipIdStr = request.getParameter("shipId");
			String primPayMethod = request.getParameter("primPayMethod");
			String secPayMethod = request.getParameter("secPayMethod");
			String configIdString = request.getParameter("configId");
			String platForm = request.getParameter("platForm");
			String shippingCostStr = request.getParameter("shippingCost");
			String taxFeesStr = request.getParameter("taxFees");
			String blFName = request.getParameter("blFName");
			String blZip = request.getParameter("blzip");
			String itemsStr = request.getParameter("noOfItems");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(primPayMethod)){
				error.setDescription("Invalid payment method");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid payment method");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shipIdStr)){
				error.setDescription("Invalid shipping address");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid shipping address");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(orderTotalStr)){
				error.setDescription("Invalid order total");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order total");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(itemsStr)){
				error.setDescription("Invalid order iteams");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order iteams");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shippingCostStr)){
				error.setDescription("Invalid Shipping charges");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Shipping charges");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(taxFeesStr)){
				error.setDescription("Invalid tax and fees");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid tax and fees");
				return resp;
			}
			
			Integer custId = 0;
			Double orderTotal = 0.00;
			Integer loyaltyPoints = 0;
			Integer noOfItem = 0;
			Integer shipId = 0;
			Double shippingCost =0.00;
			Double taxFees = 0.00;
			PaymentMethod pPayMethod = null;
			PaymentMethod sPayMethod = null;
			ApplicationPlatForm appPlatForm = null;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Customer ID");
				return resp;
			}
			try {
				shippingCost = Double.parseDouble(shippingCostStr);
				taxFees = Double.parseDouble(taxFeesStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid shipping charges or tax & fees");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Customer ID");
				return resp;
			}
			try {
				orderTotal = Double.parseDouble(orderTotalStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid order total");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order total");
				return resp;
			}
			
			try {
				shipId = Integer.parseInt(shipIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid shipping address ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid shipping address ID");
				return resp;
			}
			
			if(loyaltyPointsStr!=null && !loyaltyPointsStr.isEmpty()){
				try {
					loyaltyPoints = Integer.parseInt(loyaltyPointsStr);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid reward points");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid reward points");
					return resp;
				}
			}
			try{
				appPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Please send valid application platform");
				return resp;
			}
			
			try {
				pPayMethod = PaymentMethod.valueOf(primPayMethod);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid primary payment method");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid primary payment method");
				return resp;
			}
			
			UserAddress blAddr = null;
			Integer blAddrId = 0;
			if(appPlatForm.equals(ApplicationPlatForm.IOS) && pPayMethod.equals(PaymentMethod.CREDITCARD)){
				
				String blLName = request.getParameter("blLName");
				String blAddr1 = request.getParameter("blAddr1");
				String blAddr2 = request.getParameter("blAddr2");
				String blCity = request.getParameter("blCity");
				String blState = request.getParameter("blState");
				String blCountry = request.getParameter("blCountry");
				
				String blEmail = request.getParameter("blEmail");
				String blPhone = request.getParameter("blPhone");
				//System.out.println("BILLING DETAILS : "+blFName+" | "+blLName+" | "+blAddr1+" | "+blAddr2+" | "+blCity+" | "+blState+" | "+blCountry+" | "+blZip+" | "+blEmail+" | "+blPhone);
			}else if(appPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE)){
				/*if(TextUtil.isEmptyOrNull(blAddrIdStr)){
					error.setDescription("Invalid billing address details");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid billing address details");
					return resp;
				}
				
				try {
					blAddrId = Integer.parseInt(blAddrIdStr);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid billing address id");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid billing address id");
					return resp;
				}
				blAddr = DAORegistry.getUserAddressDAO().get(blAddrId);
				if(blAddr==null){
					error.setDescription("Invalid billing address details");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid billing address details");
					return resp;
				}*/
			}
			
			
			
			try {
				noOfItem = Integer.parseInt(itemsStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid order items");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order items");
				return resp;
			}
			
			
			
			if(secPayMethod!=null && !secPayMethod.isEmpty()){
				try {
					sPayMethod = PaymentMethod.valueOf(secPayMethod);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid secondary payment method");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid secondary payment method");
					return resp;
				}
			}
			
			Customer cust = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid customer details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid customer details");
				return resp;
			}
			
			UserAddress shipAddr = DAORegistry.getUserAddressDAO().get(shipId);
			if(shipAddr==null){
				error.setDescription("Shipping address not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Shipping address not found");
				return resp;
			}
			
			if(loyaltyPoints > 0 && cust.getLoyaltyPoints() < loyaltyPoints){
				error.setDescription("You don't have enough reward points to make order");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"You don't have enought reward points to make order");
				return resp;
			}
			
			List<CustomerCart> cartProds = EcommDAORegistry.getCustomerCartDAO().getAllCustomerCartByCustomerId(custId);
			if(cartProds == null || cartProds.isEmpty()){
				error.setDescription("Customer cart is empty.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Customer cart is empty.");
				return resp;
			}
			
			/*if(cartProds.size() > noOfItem || cartProds.size() <  noOfItem){
				error.setDescription("No of items in order does not match with number of items in cart.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"No of items in order does not match with number of items in cart.");
				return resp;
			}*/
			Date today = new Date();
			List<Integer> invIds = new ArrayList<Integer>();
			Map<Integer, SellerProdItemsVariInventory> invMap = new HashMap<Integer, SellerProdItemsVariInventory>();
			Map<Integer, SellerProductsItems> prodMap = new HashMap<Integer, SellerProductsItems>();
			for(CustomerCart cart: cartProds){
				invIds.add(cart.getSpivInvId());
			}
			List<SellerProdItemsVariInventory> inventories = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByIds(invIds);
			for(SellerProdItemsVariInventory inv: inventories){
				SellerProductsItems prod = EcommDAORegistry.getSellerProductsItemsDAO().getProductById(inv.getSpiId());
				if(prod == null || prod.getpExpDate().before(today) || prod.getStatus().equalsIgnoreCase("EXPIRED") || prod.getIsSoldOut()){
					error.setDescription("One or more product in cart is out of stock.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"One or more product in cart is expired");
					return resp;
				}
				prodMap.put(prod.getSpiId(), prod);
				invMap.put(inv.getSpivInvId(), inv);
			}
			
			
			
			Double grossTotal = 0.00;
			Integer totalQty = 0;
			
			Double origTotal = 0.00;
			Integer finalLoyaltyPoints = 0;
			Integer cartnoOfItems=0;
			boolean isRewardPointsOrder = false,isOnlyGiftcardORder = true;
			System.out.println("============== CREATE ORDER START ===============");
			System.out.println("CUSTOMER : "+cust.getId()+"   |   "+cust.getEmail());
			for(CustomerCart cart: cartProds){
				Double custCodediscount = 0.00;
				Double codeDiscount = 0.00;
				Integer reqLoyaltyPoints = 0;
				Double finalPrice = 0.00;
				Double sellPrice = 0.00;
				SellerProdItemsVariInventory inv = invMap.get(cart.getSpivInvId());
				
				if(inv.getIsRwdPointProd() && (inv.getProdType() != null && inv.getProdType().equalsIgnoreCase("PRODUCT"))) {
					isRewardPointsOrder = true;
				}
				if(inv.getProdType() == null || !(inv.getProdType().equalsIgnoreCase("GIFTCARD") || inv.getProdType().equalsIgnoreCase("DIGITALPRODUCT"))) {
					isOnlyGiftcardORder = false;
				}
				
				origTotal = origTotal + (inv.getSellPrice() * cart.getQty());
				if(cart.getIsLoyPntUsed() !=null && cart.getIsLoyPntUsed()){
					sellPrice = inv.getPriceAftLPnts();
					reqLoyaltyPoints = inv.getReqLPnts();
				}else if(cart.getIsRwdPointProd()){
					sellPrice = 0.00;
					reqLoyaltyPoints = inv.getReqLPnts();
				}else{
					sellPrice = inv.getSellPrice();
				}
				finalPrice = sellPrice;
				if(cart.getCustCode()!=null && !cart.getCustCode().isEmpty()){
					List<CustCoupenCode> custCodes = EcommDAORegistry.getEcommQueryManagerDAO().getCustomerProductCouponCodes(cart.getSpiId(), "CONTEST", custId, cart.getCustCode());
					CustCoupenCode custCode = null;
					if(custCodes!=null && !custCodes.isEmpty()){
						custCode = custCodes.get(0);
					}
					if(custCode==null){
						error.setDescription("Applied winner discount code not found or its expired");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Applied winner discount code not found or its expired");
						return resp;
					}
					
					
					if(custCode!=null && custCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						custCodediscount =(inv.getSellPrice()  *custCode.getDisc())/100;
						//finalPrice = EcommUtil.getNormalRoundedValue(finalPrice - custCodediscount);
					}else if(custCode!=null && custCode.getDiscType().equalsIgnoreCase("FLAT")){
						custCodediscount = custCode.getDisc();
						//finalPrice = EcommUtil.getNormalRoundedValue(finalPrice - custCodediscount);
					}
				}
				
				if(cart.getCode()!=null && !cart.getCode().isEmpty()){
					CoupenCode code = EcommDAORegistry.getCoupenCodeDAO().getProductCodesByCode(cart.getCode());
					if(code == null){
						error.setDescription("Applied discount code("+cart.getCode()+") not found or its expired.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Applied discount code("+cart.getCode()+") not found or its expired.");
						return resp;
					}
					if(code!=null && code.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						codeDiscount =  (inv.getSellPrice()*code.getCpnDisc())/100;
						//finalPrice = finalPrice - codeDiscount;
					}else if(code!=null && code.getDiscType().equalsIgnoreCase("FLAT")){
						codeDiscount = code.getCpnDisc();
						//finalPrice = EcommUtil.getNormalRoundedValue(finalPrice - codeDiscount);
					}
				}
				custCodediscount = EcommUtil.getNormalRoundedValue(custCodediscount*cart.getQty());
				codeDiscount = EcommUtil.getNormalRoundedValue(codeDiscount*cart.getQty());
				finalPrice =  EcommUtil.getNormalRoundedValue((finalPrice * cart.getQty()) - (custCodediscount + codeDiscount));
				finalLoyaltyPoints += (reqLoyaltyPoints * cart.getQty());
				inv.setCodeDisc(codeDiscount);
				inv.setCustCodeDisc(custCodediscount);
				inv.setAvlQty(inv.getAvlQty()-cart.getQty());
				inv.setSoldQty(inv.getSoldQty()+cart.getQty());
				inv.setFinalPrice(finalPrice);
				invMap.put(inv.getSpivInvId(), inv);
				grossTotal += inv.getFinalPrice();
				cartnoOfItems += cart.getQty();
				
				//System.out.println("FINAL PRICE : "+inv.getFinalPrice()+"  |   QTY :  "+cart.getQty()+"   |   CODE DISC : "+codeDiscount+"  |  CUSTCODE DISC : "+custCodediscount+"  |  LOYALTY POINT USED : "+cart.getIsLoyPntUsed());
				System.out.println("PRODTOTAL : "+inv.getFinalPrice()+"   |   CODEDISC : "+inv.getCodeDisc()+"  |  CUSTDISC : "+inv.getCustCodeDisc()+"  | RPDISC : 0000  |   QTY :  "+cart.getQty()+"   |   SELLPRICE : "+sellPrice);
			}
			
			grossTotal = EcommUtil.getNormalRoundedValue(grossTotal+shippingCost+taxFees);
			orderTotal = EcommUtil.getNormalRoundedValue(orderTotal);
			System.out.println("ORDER TOTAL : "+orderTotal+"    |    "+grossTotal);
			System.out.println("LOYALTY POINTS : "+loyaltyPoints+"    |    "+finalLoyaltyPoints);
			if(loyaltyPoints > 0 && (loyaltyPoints > finalLoyaltyPoints || loyaltyPoints < finalLoyaltyPoints)){
				error.setDescription("Reward points total mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Reward points total mismatching.");
				return resp;
			}
			
			Double diff = orderTotal - grossTotal;
			
			if(orderTotal > grossTotal || orderTotal < grossTotal ){
				error.setDescription("Order total mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Order total mismatch");
				return resp;
			}
			if(noOfItem > cartnoOfItems || noOfItem < cartnoOfItems) {
				error.setDescription("Order Quantity mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Order Quantity mismatching.");
				return resp;
			}
			Double finalShippingFees = EcommUtil.computeShippingFees(cartnoOfItems,shipAddr.getCountryName(),isRewardPointsOrder,isOnlyGiftcardORder);
			if(shippingCost < finalShippingFees || shippingCost > finalShippingFees ) {
				error.setDescription("shipping charge is mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"shipping charge is mismatching");
				return resp;
			}
			
			CustomerLoyaltyPointTranx loyaltyPointTrax =  null;
			String sTranxId = null;
			Double pPayAmt = orderTotal;
			Double transAmt = (pPayAmt * 100);
			
			CustomerOrder order = new CustomerOrder();
			order.setShAddr1(shipAddr.getAddressLine1());
			order.setShAddr2(shipAddr.getAddressLine2());
			order.setShCity(shipAddr.getCity());
			order.setShState(shipAddr.getStateName());
			order.setShCountry(shipAddr.getCountryName());
			order.setShEmail(shipAddr.getEmail());
			order.setShFName(shipAddr.getFirstName());
			order.setShLName(shipAddr.getLastName());
			order.setShPhone(shipAddr.getPhone1());
			order.setShZip(shipAddr.getZipCode());
			
			
			order.setCrDate(new Date());
			order.setCustId(custId);
			order.setGrosTot(origTotal);
			order.setDisc(0.00);
			order.setIsEmailed(false);
			order.setNetTot(orderTotal);
			order.setStatus("PAYMENT_PENDING");
			order.setOrderType("REGULAR");
			if(pPayMethod.equals(PaymentMethod.REWARD_POINTS)){
				order.setpPayAmt(Double.valueOf(loyaltyPoints));
				order.setsPayAmt(0.00);
			}else{
				order.setpPayAmt(pPayAmt);
				order.setsPayAmt(Double.valueOf(loyaltyPoints));
			}
			
			order.setpPayType(pPayMethod.toString());
			order.setsPayType(sPayMethod!=null?sPayMethod.toString():null);
			order.setpTrnxId("prmtrnxID");
			order.setsTrnxId(sPayMethod!=null?"REWARDPOINTS":null);
			order.setShCharges(shippingCost);
			order.setTax(taxFees);


			EcommDAORegistry.getCustomerProdOrderDAO().save(order);
			
			
			loyaltyPointTrax = new CustomerLoyaltyPointTranx();
			loyaltyPointTrax.setCrBy(cust.getUserId());
			loyaltyPointTrax.setCrDate(new Date());
			loyaltyPointTrax.setCustId(custId);
			Integer earnPoints = EcommUtil.getIntegerValueRoundedUp(orderTotal);
			loyaltyPointTrax.setEarnPoints(earnPoints.intValue());
			loyaltyPointTrax.setSpentPoints(loyaltyPoints);
			loyaltyPointTrax.setBeforePoints(cust.getLoyaltyPoints());
			loyaltyPointTrax.setAfterPoints((cust.getLoyaltyPoints()+earnPoints)-loyaltyPoints);
			loyaltyPointTrax.setStatus("PENDING");
			loyaltyPointTrax.setOrderId(order.getId());
			loyaltyPointTrax.setTranxType("PROD_ORDER");
			EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
			
			
			
			
			List<OrderProducts> orderProdList = new ArrayList<OrderProducts>();
			for(CustomerCart op : cartProds){
				SellerProdItemsVariInventory inv = invMap.get(op.getSpivInvId());
				SellerProductsItems prod = prodMap.get(inv.getSpiId());
				
				OrderProducts custProd = new OrderProducts();
				custProd.setCustId(custId);
				custProd.setOrderId(order.getId());
				custProd.setpCpnCode(op.getCode());
				custProd.setCpnCodeDisc(EcommUtil.getNormalRoundedValue(inv.getCodeDisc()));
				custProd.setCustCpnCode(op.getCustCode());
				custProd.setCustcpnCodeDisc(EcommUtil.getNormalRoundedValue(inv.getCustCodeDisc()));
				custProd.setExpDelDate(null);
				custProd.setfSelPrice(EcommUtil.getNormalRoundedValue(inv.getFinalPrice()));
				custProd.setpDesc(prod.getpDesc());
				custProd.setpImgUrl(inv.getpImg());
				custProd.setpLongDesc(prod.getpLongDesc());
				custProd.setpName(prod.getpName());
				custProd.setPriceWithLPnts(EcommUtil.getNormalRoundedValue(inv.getPriceAftLPnts()));
				custProd.setQty(op.getQty());
				custProd.setSellerId(prod.getSellerId());
				custProd.setQtyType(inv.getdUnitTyp());
				custProd.setProdVariant(inv.getvOptNameValCom());
				custProd.setRegPrice(EcommUtil.getNormalRoundedValue(inv.getRegPrice()*op.getQty()));
				custProd.setSelPrice(EcommUtil.getNormalRoundedValue(inv.getSellPrice()*op.getQty()));
				custProd.setShCharges(EcommUtil.getNormalRoundedValue(shippingCost));
				custProd.setTax(EcommUtil.getNormalRoundedValue(taxFees));
				custProd.setShClass(null);
				custProd.setShippingStatus(EcommUtil.calculateShippingStatus(order.getCrDate()));
				custProd.setShMethod(null);
				custProd.setShTrackId(null);
				custProd.setIsTrackNoUpdated(false);
				custProd.setIsDelEmailSent(false);
				custProd.setSpiId(prod.getSpiId());
				custProd.setSpivInvId(inv.getSpivInvId());
				custProd.setStatus("ACTIVE");
				custProd.setUsedLPnts(0);
				if((op.getIsLoyPntUsed()!=null && op.getIsLoyPntUsed()) || (inv.getIsRwdPointProd()!=null && inv.getIsRwdPointProd())){
					custProd.setUsedLPnts(inv.getReqLPnts());
				}
				custProd.setProdType(inv.getProdType());
				custProd.setIsRwdPointProd(inv.getIsRwdPointProd());
				orderProdList.add(custProd);
			}
			
			EcommDAORegistry.getOrderProductSetDAO().saveAll(orderProdList);
			
			
			System.out.println("PROD : custId ---->"+custId);
			System.out.println("PROD : orderId ---->"+order.getId());
			System.out.println("PROD : token ---->"+payToken);
			System.out.println("PROD : pPayMethod ---->"+primPayMethod);
			System.out.println("PROD : sPayMethod ---->"+secPayMethod);
			System.out.println("PROD : transactionAmountStr ---->"+transAmt);
			System.out.println("PROD : custrewardPoints ---->"+cust.getLoyaltyPoints());
			System.out.println("PROD : reqrewardPoints ---->"+loyaltyPointsStr);
			
			String description = "Payment for order Id-"+order.getId();
			CustomerCardInfo customerCardInfo = null;
			CustomerOrderCreditCard orderCreditCard = null;
			Map<String, Object> chargeParams = new HashMap<String, Object>();
			chargeParams.put("amount", transAmt.intValue());
			chargeParams.put("currency", "usd");
			chargeParams.put("description", description);
			
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			Stripe.apiKey = key;
			//Stripe.apiKey = "sk_test_HZ0yhYYAoU6tyGbi0zAopfae";
			switch (pPayMethod) {
				case CREDITCARD:
					try {
						if(TextUtil.isEmptyOrNull(payToken)){
							error.setDescription("Stripe token is mandatory.");
							resp.setError(error);
							resp.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Product Type is mandatory");
							return resp;
						}
						List<CustomerCardInfo> customerCardInfos = DAORegistry.getCustomerCardInfoDAO().getAllActiveStripeRegisteredCardsByCustomerId(custId); 
						String stripeCustId ="";
						
						if(null != customerCardInfos && !customerCardInfos.isEmpty()){
							for (CustomerCardInfo cardInfo : customerCardInfos) {
								stripeCustId = cardInfo.getsCustId();
								break;
							}
						}
						
						com.stripe.model.Customer stripeCustomer = null;
						Card card = null;
						if(null != stripeCustId && !stripeCustId.isEmpty() ){
							System.out.println("PROD SC : Stripe Customer Id----->"+stripeCustId);
							stripeCustomer = com.stripe.model.Customer.retrieve(stripeCustId);
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("source", payToken);
							card = (Card)stripeCustomer.getSources().create(params);
							System.out.println("PROD SC : Stripe Customer card Id----->"+card.getId());
						}else{
							String custName = cust.getCustomerName()+" "+cust.getLastName();
							Map<String, Object> customerParams = new HashMap<String, Object>();
							customerParams.put("source", payToken);
							customerParams.put("description", "Customer Name:"+custName+", Customer Email :"+cust.getEmail());
							
							Map<String, String> initialMetadata = new HashMap<String, String>();
						    initialMetadata.put("customer_id", String.valueOf(cust.getId()));
						    initialMetadata.put("customer_email", cust.getEmail());
						    initialMetadata.put("customer_name", custName);
						    customerParams.put("metadata", initialMetadata);
						    
						    stripeCustomer = com.stripe.model.Customer.create(customerParams);
						    System.out.println("PROD NS : Stripe Source ----->"+stripeCustomer.getSources());
						    card = (Card) stripeCustomer.getSources().getData().get(0);
						    
						    
						    System.out.println("PROD NS : Stripe Customer Id----->"+stripeCustomer.getId());
						    System.out.println("PROD NS: Stripe Customer Card Id----->"+card.getId());
						}
						
						
						customerCardInfo = new CustomerCardInfo();
						customerCardInfo.setCustomerId(custId);
						customerCardInfo.setsCustCardId(card.getId());
						customerCardInfo.setsCustId(stripeCustomer.getId());
						customerCardInfo.setShowToCustomer(false);
						customerCardInfo.setCardType(card.getBrand());
						customerCardInfo.setCreateDate(new Date());
						customerCardInfo.setLastUpdated(new Date());
						customerCardInfo.setMonth(card.getExpMonth());
						customerCardInfo.setYear(card.getExpYear());
						customerCardInfo.setLastFourDigit(card.getLast4());
						customerCardInfo.setStatus(Status.ACTIVE);
						
						chargeParams.put("customer", stripeCustomer.getId());
						chargeParams.put("card", card.getId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
					
				case IPAY:
					if(TextUtil.isEmptyOrNull(payToken)){
						error.setDescription("Stripe token is mandatory.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Product Type is mandatory");
						return resp;
					}
					chargeParams.put("source", payToken);
					break;
				
				case GOOGLEPAY:
					if(TextUtil.isEmptyOrNull(payToken)){
						error.setDescription("Stripe token is mandatory.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Stripe token is mandatory");
						return resp;
					}
					chargeParams.put("source", payToken);
					break;
				case REWARD_POINTS:
					break;	
				
				default:
					break;
					
			}
			if(pPayMethod.equals(PaymentMethod.CREDITCARD) || pPayMethod.equals(PaymentMethod.IPAY)){
				System.out.println("PROD : INSIDE STRIPE TRANSACTION");
				Charge charge = null;
				try {
					Map<String, String> initialMetadata = new HashMap<String, String>();
					initialMetadata.put("Platform", String.valueOf(appPlatForm));
					initialMetadata.put("Source", String.valueOf(pPayMethod));
					initialMetadata.put("CustomerName", cust.getCustomerName()+" "+cust.getLastName());
					initialMetadata.put("CustomerEmail", cust.getEmail());
				/*    initialMetadata.put("EventName", event.getEventName());
				    initialMetadata.put("EventDate", event.getEventDateTime());
				    initialMetadata.put("VenueName", event.getVenueName());*/
				    initialMetadata.put("Quantity",String.valueOf( totalQty));
				    chargeParams.put("metadata", initialMetadata);
				    
					System.out.println("PROD : STRIPE TRANSACTION - BEGINS: Payment Method :"+primPayMethod+", token :"+payToken);
					charge = Charge.create(chargeParams);
					
					
					if(charge.getStatus().equalsIgnoreCase("succeeded")){
						System.out.println("PROD : STRIPE TRANSACTION STATUS :"+charge.getStatus()+", TRXID :"+charge.getId());
						try{
							DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod,description, transAmt, payToken, charge.getId(), charge.getStatus());
						}catch (Exception e) {
							e.printStackTrace();
						}
						try{
							if(customerCardInfo != null){
								
								DAORegistry.getCustomerCardInfoDAO().saveOrUpdate(customerCardInfo);
								
								orderCreditCard = new CustomerOrderCreditCard();
								orderCreditCard.setCustomerId(custId);
								orderCreditCard.setCardId(customerCardInfo.getId());
								orderCreditCard.setTicketGroupId(-1);
								orderCreditCard.setTrxAmount(transAmt);
								orderCreditCard.setTransactionId(charge.getId());
								DAORegistry.getCustomerOrderCreditCardDAO().saveOrUpdate(orderCreditCard);
								
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
						
					}else{
						String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():charge.getStatus();
						System.out.println("PROD : STRIPE TRANSACTION FAILURE : "+failure);
						try{
							DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod, description, transAmt, 	payToken, "", failure);
						}catch (Exception e) {
							e.printStackTrace();
						}
						
						error.setDescription(failure);
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,charge.getFailureMessage());
						return resp;
					}
					System.out.println("PROD : STRIPE TRANSACTION - ENDS: Payment Method :"+primPayMethod+", token :"+payToken+", description: "+description);
				} catch (Exception e) {
					e.printStackTrace();
					try{	
						
						String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():e.getMessage();
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod, description, transAmt, payToken, "", failure);
						
					}catch (Exception e1) {
						e1.printStackTrace();
					}
					error.setDescription(e.getMessage());
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Exception Occurred");
					return resp;
				}
				order.setpTrnxId(charge.getId()); 
			}
			
			order.setStatus("ACTIVE");
			
			if(sPayMethod!=null && loyaltyPoints > 0){
				sTranxId = "REWARDPOINTS_"+order.getId();
				order.setsTrnxId(sTranxId);
				loyaltyPointTrax.setStatus("ACTIVE");
			}else if(pPayMethod.equals(PaymentMethod.REWARD_POINTS)){
				sTranxId = "REWARDPOINTS_"+order.getId();
				order.setpTrnxId(sTranxId);
				loyaltyPointTrax.setStatus("ACTIVE");
			}
			
			cust.setLoyaltyPoints((cust.getLoyaltyPoints()+earnPoints) - loyaltyPoints);
			EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().update(loyaltyPointTrax);
			DAORegistry.getCustomerDAO().update(cust);
			try {
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(cust.getId(), cust.getLoyaltyPoints());
				System.out.println("UPDATED ORDER POINT IN CASSANDRA : "+cust.getLoyaltyPoints());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 5);
			
			//order.setDeliveryStatus("PENDING");
			//order.setDeliveryDateStr(dateTimeFormat1.format(cal.getTime()));
			EcommDAORegistry.getCustomerProdOrderDAO().update(order);
			EcommDAORegistry.getCustomerCartDAO().deleteAll(cartProds);
			CustomerUtil.updatedCustomerUtil(cust);
			for(Entry<Integer, SellerProdItemsVariInventory> map : invMap.entrySet()){
				SellerProdItemsVariInventory inv = map.getValue();
				inv.setUpdDate(today);
				EcommDAORegistry.getSellerProdItemsVariInventoryDAO().update(inv);
			}
			
			
			try {
				QuizCustomerReferralTracking referral = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(cust.getId());
				if(referral!=null && !referral.getIsPurchased()){
					CustomerLoyaltyPointTranx loyaltyPointTranx = EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().getCustomerLoyaltyPointTranxByCustIdAndRewardType(referral.getReferralCustomerId(), "REFERRAL_PURCHASE");
					if(loyaltyPointTranx==null){
						Customer refCustomer = DAORegistry.getCustomerDAO().get(referral.getReferralCustomerId());
						if(refCustomer!=null){
							loyaltyPointTranx = new CustomerLoyaltyPointTranx();
							loyaltyPointTrax.setCrBy(cust.getUserId());
							loyaltyPointTrax.setCrDate(new Date());
							loyaltyPointTrax.setCustId(refCustomer.getId());
							loyaltyPointTrax.setEarnPoints(1000);
							loyaltyPointTrax.setSpentPoints(0);
							loyaltyPointTrax.setBeforePoints(refCustomer.getLoyaltyPoints());
							loyaltyPointTrax.setAfterPoints(refCustomer.getLoyaltyPoints()+1000);
							loyaltyPointTrax.setStatus("ACTIVE");
							loyaltyPointTrax.setOrderId(order.getId());
							loyaltyPointTrax.setTranxType("REFERRAL_PURCHASE");
							EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
							
							referral.setIsPurchased(true);
							QuizDAORegistry.getQuizCustomerReferralTrackingDAO().update(referral);
							
							refCustomer.setLoyaltyPoints(refCustomer.getLoyaltyPoints()+loyaltyPointTrax.getEarnPoints());
							DAORegistry.getCustomerDAO().update(refCustomer);
							try {
								CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(refCustomer.getId(), refCustomer.getLoyaltyPoints());
								System.out.println("UPDATED REFERRAL ORDER POINT IN CASSANDRA : "+cust.getLoyaltyPoints());
							} catch (Exception e) {
								e.printStackTrace();
							}
							CustomerUtil.updatedCustomerUtil(refCustomer);
						}
					}
				}
				System.out.println("================= CREAT ORDER END ================= ");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			order.setNoOfItems(noOfItem);
			resp.setOrder(order);
			resp.setProds(orderProdList);
			resp.setMessage("Order created successfully.");
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while creating customer cart order");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Error occured while creating customer cart order");
			return resp;
		}
	}
	
	
	
	
	@RequestMapping(value="/liveCreateCustProdOrder",method=RequestMethod.POST)
	public OrderResp liveCreateCustProdOrder(HttpServletRequest request){
		Error error = new Error();
		OrderResp resp = new OrderResp();
		try {
			String custIdStr = request.getParameter("custId");
			String orderTotalStr = request.getParameter("orderTotal");
			String loyaltyPointsStr = request.getParameter("loyaltyPoints");
			String payToken = request.getParameter("payToken");
			String shipIdStr = request.getParameter("shipId");
			String primPayMethod = request.getParameter("primPayMethod");
			String secPayMethod = request.getParameter("secPayMethod");
			String configIdString = request.getParameter("configId");
			String platForm = request.getParameter("platForm");
			String shippingCostStr = request.getParameter("shippingCost");
			String taxFeesStr = request.getParameter("taxFees");
			String blFName = request.getParameter("blFName");
			String blZip = request.getParameter("blzip");
			String itemsStr = request.getParameter("noOfItems");
			String prodIdStr = request.getParameter("prodId");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(prodIdStr)){
				error.setDescription("Invalid product ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid product ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(primPayMethod)){
				error.setDescription("Invalid payment method");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid payment method");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shipIdStr)){
				error.setDescription("Invalid shipping address");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid shipping address");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(orderTotalStr)){
				error.setDescription("Invalid order total");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order total");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(itemsStr)){
				error.setDescription("Invalid order iteams");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order iteams");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shippingCostStr)){
				error.setDescription("Invalid Shipping charges");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Shipping charges");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(taxFeesStr)){
				error.setDescription("Invalid tax and fees");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid tax and fees");
				return resp;
			}
			
			Integer custId = 0;
			Double orderTotal = 0.00;
			Integer loyaltyPoints = 0;
			Integer noOfItem = 0;
			Integer shipId = 0;
			Double shippingCost =0.00;
			Double taxFees = 0.00;
			PaymentMethod pPayMethod = null;
			PaymentMethod sPayMethod = null;
			ApplicationPlatForm appPlatForm = null;
			Integer prodId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Customer ID");
				return resp;
			}
			try {
				prodId = Integer.parseInt(prodIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid product ID");
				return resp;
			}
			try {
				shippingCost = Double.parseDouble(shippingCostStr);
				taxFees = Double.parseDouble(taxFeesStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid shipping charges or tax & fees");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid Customer ID");
				return resp;
			}
			try {
				orderTotal = Double.parseDouble(orderTotalStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid order total");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order total");
				return resp;
			}
			
			try {
				shipId = Integer.parseInt(shipIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid shipping address ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid shipping address ID");
				return resp;
			}
			
			if(loyaltyPointsStr!=null && !loyaltyPointsStr.isEmpty()){
				try {
					loyaltyPoints = Integer.parseInt(loyaltyPointsStr);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid reward points");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid reward points");
					return resp;
				}
			}
			try{
				appPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Please send valid application platform");
				return resp;
			}
			
			try {
				pPayMethod = PaymentMethod.valueOf(primPayMethod);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid primary payment method");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid primary payment method");
				return resp;
			}
			
			UserAddress blAddr = null;
			Integer blAddrId = 0;
			try {
				noOfItem = Integer.parseInt(itemsStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid order items");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid order items");
				return resp;
			}
			
			
			
			if(secPayMethod!=null && !secPayMethod.isEmpty()){
				try {
					sPayMethod = PaymentMethod.valueOf(secPayMethod);
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Invalid secondary payment method");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid secondary payment method");
					return resp;
				}
			}
			
			Customer cust = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid customer details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid customer details");
				return resp;
			}
			
			UserAddress shipAddr = DAORegistry.getUserAddressDAO().get(shipId);
			if(shipAddr==null){
				error.setDescription("Shipping address not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Shipping address not found");
				return resp;
			}
			
			if(loyaltyPoints > 0 && cust.getLoyaltyPoints() < loyaltyPoints){
				error.setDescription("You don't have enough reward points to make order");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"You don't have enought reward points to make order");
				return resp;
			}
			
			SellerProductsItems prod = EcommDAORegistry.getSellerProductsItemsDAO().getProductById(prodId);
			SellerProdItemsVariInventory inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getAvailableInventoryByProdId(prodId);
			if(prod == null){
				error.setDescription("Invalid product details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid product details.");
				return resp;
			}
			if(inv == null){
				error.setDescription("Invalid product inventory details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Invalid product inventory details.");
				return resp;
			}
			
			Date today = new Date();
			Double grossTotal = 0.00;
			Integer totalQty = 0;
			
			Double origTotal = 0.00;
			Integer finalLoyaltyPoints = 0;
			boolean isRewardPointsOrder = false,isOnlyGiftcardORder = true;
			System.out.println("============== CREATE ORDER START ===============");
			System.out.println("CUSTOMER : "+cust.getId()+"   |   "+cust.getEmail());
			grossTotal =  EcommUtil.getNormalRoundedValue((prod.getPsMinPrice())*noOfItem);
			if(prod.getIsRwdPointProd() != null && prod.getIsRwdPointProd() && loyaltyPoints > 0){
				finalLoyaltyPoints = (prod.getReqRwdPoint()*noOfItem);
				if(loyaltyPoints < finalLoyaltyPoints){
					error.setDescription("You do not have enough reward points.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid product details.");
					return resp;
				}
				grossTotal = EcommUtil.getNormalRoundedValue((prod.getPsMinPrice())*noOfItem)-(finalLoyaltyPoints/100);
			}
			
			grossTotal = EcommUtil.getNormalRoundedValue(grossTotal+shippingCost+taxFees);
			orderTotal = EcommUtil.getNormalRoundedValue(orderTotal);
			System.out.println("ORDER TOTAL : "+orderTotal+"    |    "+grossTotal);
			System.out.println("LOYALTY POINTS : "+loyaltyPoints+"    |    "+finalLoyaltyPoints);
			if(loyaltyPoints > 0 && (loyaltyPoints > finalLoyaltyPoints || loyaltyPoints < finalLoyaltyPoints)){
				error.setDescription("Reward points total mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Reward points total mismatching.");
				return resp;
			}
			
			if(orderTotal > grossTotal || orderTotal < grossTotal ){
				error.setDescription("Order total mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Order total mismatch");
				return resp;
			}
			
			Double finalShippingFees = EcommUtil.computeShippingFees(noOfItem,shipAddr.getCountryName(),isRewardPointsOrder,isOnlyGiftcardORder);
			if(shippingCost < finalShippingFees || shippingCost > finalShippingFees ) {
				error.setDescription("shipping charge is mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"shipping charge is mismatching");
				return resp;
			}
			
			CustomerLoyaltyPointTranx loyaltyPointTrax =  null;
			String sTranxId = null;
			Double pPayAmt = orderTotal;
			Double transAmt = (pPayAmt * 100);
			
			CustomerOrder order = new CustomerOrder();
			order.setShAddr1(shipAddr.getAddressLine1());
			order.setShAddr2(shipAddr.getAddressLine2());
			order.setShCity(shipAddr.getCity());
			order.setShState(shipAddr.getStateName());
			order.setShCountry(shipAddr.getCountryName());
			order.setShEmail(shipAddr.getEmail());
			order.setShFName(shipAddr.getFirstName());
			order.setShLName(shipAddr.getLastName());
			order.setShPhone(shipAddr.getPhone1());
			order.setShZip(shipAddr.getZipCode());
			
			
			order.setCrDate(new Date());
			order.setCustId(custId);
			order.setGrosTot(origTotal);
			order.setDisc(0.00);
			order.setIsEmailed(false);
			order.setNetTot(orderTotal);
			order.setStatus("PAYMENT_PENDING");
			order.setOrderType("REGULAR");
			if(pPayMethod.equals(PaymentMethod.REWARD_POINTS)){
				order.setpPayAmt(Double.valueOf(loyaltyPoints));
				order.setsPayAmt(0.00);
			}else{
				order.setpPayAmt(pPayAmt);
				order.setsPayAmt(Double.valueOf(loyaltyPoints));
			}
			
			order.setpPayType(pPayMethod.toString());
			order.setsPayType(sPayMethod!=null?sPayMethod.toString():null);
			order.setpTrnxId("prmtrnxID");
			order.setsTrnxId(sPayMethod!=null?"REWARDPOINTS":null);
			order.setShCharges(shippingCost);
			order.setTax(taxFees);


			EcommDAORegistry.getCustomerProdOrderDAO().save(order);
			
			
			loyaltyPointTrax = new CustomerLoyaltyPointTranx();
			loyaltyPointTrax.setCrBy(cust.getUserId());
			loyaltyPointTrax.setCrDate(new Date());
			loyaltyPointTrax.setCustId(custId);
			Integer earnPoints = 0;
			loyaltyPointTrax.setEarnPoints(earnPoints.intValue());
			loyaltyPointTrax.setSpentPoints(loyaltyPoints);
			loyaltyPointTrax.setBeforePoints(cust.getLoyaltyPoints());
			loyaltyPointTrax.setAfterPoints((cust.getLoyaltyPoints()+earnPoints)-loyaltyPoints);
			loyaltyPointTrax.setStatus("PENDING");
			loyaltyPointTrax.setOrderId(order.getId());
			loyaltyPointTrax.setTranxType("PROD_ORDER");
			EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
			
			List<OrderProducts> orderProds = new ArrayList<OrderProducts>();
			OrderProducts custProd = new OrderProducts();
			custProd.setCustId(custId);
			custProd.setOrderId(order.getId());
			custProd.setpCpnCode(null);
			custProd.setCpnCodeDisc(0.0);
			custProd.setCustCpnCode(null);
			custProd.setCustcpnCodeDisc(0.0);
			custProd.setExpDelDate(null);
			custProd.setfSelPrice(EcommUtil.getNormalRoundedValue(prod.getPsMinPrice()));
			custProd.setpDesc(prod.getpDesc());
			custProd.setpImgUrl(inv.getpImg());
			custProd.setpLongDesc(prod.getpLongDesc());
			custProd.setpName(prod.getpName());
			custProd.setPriceWithLPnts(EcommUtil.getNormalRoundedValue(prod.getPsMinPrice()-(finalLoyaltyPoints/100)));
			custProd.setQty(noOfItem);
			custProd.setSellerId(prod.getSellerId());
			custProd.setQtyType(inv.getdUnitTyp());
			custProd.setProdVariant(inv.getvOptNameValCom());
			custProd.setRegPrice(EcommUtil.getNormalRoundedValue(inv.getRegPrice()*noOfItem));
			custProd.setSelPrice(EcommUtil.getNormalRoundedValue(inv.getSellPrice()*noOfItem));
			custProd.setShCharges(EcommUtil.getNormalRoundedValue(shippingCost));
			custProd.setTax(EcommUtil.getNormalRoundedValue(taxFees));
			custProd.setShClass(null);
			custProd.setShippingStatus(EcommUtil.calculateShippingStatus(order.getCrDate()));
			custProd.setShMethod(null);
			custProd.setShTrackId(null);
			custProd.setIsTrackNoUpdated(false);
			custProd.setIsDelEmailSent(false);
			custProd.setSpiId(prod.getSpiId());
			custProd.setSpivInvId(inv.getSpivInvId());
			custProd.setStatus("ACTIVE");
			custProd.setUsedLPnts(0);
			if((prod.getIsRwdPointProd()!=null && prod.getIsRwdPointProd()) || (inv.getIsRwdPointProd()!=null && inv.getIsRwdPointProd())){
				if(loyaltyPoints > 0){
					custProd.setUsedLPnts(inv.getReqLPnts());
				}
				
			}
			custProd.setProdType(inv.getProdType());
			custProd.setIsRwdPointProd(inv.getIsRwdPointProd());
			EcommDAORegistry.getOrderProductSetDAO().save(custProd);
			orderProds.add(custProd);
			
			
			System.out.println("PROD : custId ---->"+custId);
			System.out.println("PROD : orderId ---->"+order.getId());
			System.out.println("PROD : token ---->"+payToken);
			System.out.println("PROD : pPayMethod ---->"+primPayMethod);
			System.out.println("PROD : sPayMethod ---->"+secPayMethod);
			System.out.println("PROD : transactionAmountStr ---->"+transAmt);
			System.out.println("PROD : custrewardPoints ---->"+cust.getLoyaltyPoints());
			System.out.println("PROD : reqrewardPoints ---->"+loyaltyPointsStr);
			
			String description = "Payment for order Id-"+order.getId();
			CustomerCardInfo customerCardInfo = null;
			CustomerOrderCreditCard orderCreditCard = null;
			Map<String, Object> chargeParams = new HashMap<String, Object>();
			chargeParams.put("amount", transAmt.intValue());
			chargeParams.put("currency", "usd");
			chargeParams.put("description", description);
			
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			Stripe.apiKey = key;
			//Stripe.apiKey = "sk_test_HZ0yhYYAoU6tyGbi0zAopfae";
			switch (pPayMethod) {
				case CREDITCARD:
					try {
						if(TextUtil.isEmptyOrNull(payToken)){
							error.setDescription("Stripe token is mandatory.");
							resp.setError(error);
							resp.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Product Type is mandatory");
							return resp;
						}
						List<CustomerCardInfo> customerCardInfos = DAORegistry.getCustomerCardInfoDAO().getAllActiveStripeRegisteredCardsByCustomerId(custId); 
						String stripeCustId ="";
						
						if(null != customerCardInfos && !customerCardInfos.isEmpty()){
							for (CustomerCardInfo cardInfo : customerCardInfos) {
								stripeCustId = cardInfo.getsCustId();
								break;
							}
						}
						
						com.stripe.model.Customer stripeCustomer = null;
						Card card = null;
						if(null != stripeCustId && !stripeCustId.isEmpty() ){
							System.out.println("PROD SC : Stripe Customer Id----->"+stripeCustId);
							stripeCustomer = com.stripe.model.Customer.retrieve(stripeCustId);
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("source", payToken);
							card = (Card)stripeCustomer.getSources().create(params);
							System.out.println("PROD SC : Stripe Customer card Id----->"+card.getId());
						}else{
							String custName = cust.getCustomerName()+" "+cust.getLastName();
							Map<String, Object> customerParams = new HashMap<String, Object>();
							customerParams.put("source", payToken);
							customerParams.put("description", "Customer Name:"+custName+", Customer Email :"+cust.getEmail());
							
							Map<String, String> initialMetadata = new HashMap<String, String>();
						    initialMetadata.put("customer_id", String.valueOf(cust.getId()));
						    initialMetadata.put("customer_email", cust.getEmail());
						    initialMetadata.put("customer_name", custName);
						    customerParams.put("metadata", initialMetadata);
						    
						    stripeCustomer = com.stripe.model.Customer.create(customerParams);
						    System.out.println("PROD NS : Stripe Source ----->"+stripeCustomer.getSources());
						    card = (Card) stripeCustomer.getSources().getData().get(0);
						    
						    
						    System.out.println("PROD NS : Stripe Customer Id----->"+stripeCustomer.getId());
						    System.out.println("PROD NS: Stripe Customer Card Id----->"+card.getId());
						}
						
						
						customerCardInfo = new CustomerCardInfo();
						customerCardInfo.setCustomerId(custId);
						customerCardInfo.setsCustCardId(card.getId());
						customerCardInfo.setsCustId(stripeCustomer.getId());
						customerCardInfo.setShowToCustomer(false);
						customerCardInfo.setCardType(card.getBrand());
						customerCardInfo.setCreateDate(new Date());
						customerCardInfo.setLastUpdated(new Date());
						customerCardInfo.setMonth(card.getExpMonth());
						customerCardInfo.setYear(card.getExpYear());
						customerCardInfo.setLastFourDigit(card.getLast4());
						customerCardInfo.setStatus(Status.ACTIVE);
						
						chargeParams.put("customer", stripeCustomer.getId());
						chargeParams.put("card", card.getId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
					
				case IPAY:
					if(TextUtil.isEmptyOrNull(payToken)){
						error.setDescription("Stripe token is mandatory.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Product Type is mandatory");
						return resp;
					}
					chargeParams.put("source", payToken);
					break;
				
				case GOOGLEPAY:
					if(TextUtil.isEmptyOrNull(payToken)){
						error.setDescription("Stripe token is mandatory.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Stripe token is mandatory");
						return resp;
					}
					chargeParams.put("source", payToken);
					break;
				case REWARD_POINTS:
					break;	
				
				default:
					break;
					
			}
			if(pPayMethod.equals(PaymentMethod.CREDITCARD) || pPayMethod.equals(PaymentMethod.IPAY)){
				System.out.println("PROD : INSIDE STRIPE TRANSACTION");
				Charge charge = null;
				try {
					Map<String, String> initialMetadata = new HashMap<String, String>();
					initialMetadata.put("Platform", String.valueOf(appPlatForm));
					//initialMetadata.put("Source", String.valueOf(pPayMethod));
					initialMetadata.put("CustomerName", cust.getCustomerName()+" "+cust.getLastName());
					initialMetadata.put("CustomerEmail", cust.getEmail());
				/*    initialMetadata.put("EventName", event.getEventName());
				    initialMetadata.put("EventDate", event.getEventDateTime());
				    initialMetadata.put("VenueName", event.getVenueName());*/
				    initialMetadata.put("Quantity",String.valueOf( totalQty));
				    chargeParams.put("metadata", initialMetadata);
				    
					System.out.println("PROD : STRIPE TRANSACTION - BEGINS: Payment Method :"+primPayMethod+", token :"+payToken);
					charge = Charge.create(chargeParams);
					
					
					if(charge.getStatus().equalsIgnoreCase("succeeded")){
						System.out.println("PROD : STRIPE TRANSACTION STATUS :"+charge.getStatus()+", TRXID :"+charge.getId());
						try{
							DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod,description, transAmt, payToken, charge.getId(), charge.getStatus());
						}catch (Exception e) {
							e.printStackTrace();
						}
						try{
							if(customerCardInfo != null){
								
								DAORegistry.getCustomerCardInfoDAO().saveOrUpdate(customerCardInfo);
								
								orderCreditCard = new CustomerOrderCreditCard();
								orderCreditCard.setCustomerId(custId);
								orderCreditCard.setCardId(customerCardInfo.getId());
								orderCreditCard.setTicketGroupId(-1);
								orderCreditCard.setTrxAmount(transAmt);
								orderCreditCard.setTransactionId(charge.getId());
								DAORegistry.getCustomerOrderCreditCardDAO().saveOrUpdate(orderCreditCard);
								
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
						
					}else{
						String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():charge.getStatus();
						System.out.println("PROD : STRIPE TRANSACTION FAILURE : "+failure);
						try{
							DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod, description, transAmt, 	payToken, "", failure);
						}catch (Exception e) {
							e.printStackTrace();
						}
						
						error.setDescription(failure);
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,charge.getFailureMessage());
						return resp;
					}
					System.out.println("PROD : STRIPE TRANSACTION - ENDS: Payment Method :"+primPayMethod+", token :"+payToken+", description: "+description);
				} catch (Exception e) {
					e.printStackTrace();
					try{	
						
						String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():e.getMessage();
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(primPayMethod, description, transAmt, payToken, "", failure);
						
					}catch (Exception e1) {
						e1.printStackTrace();
					}
					error.setDescription(e.getMessage());
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PRODUCTORDER,"Exception Occurred");
					return resp;
				}
				order.setpTrnxId(charge.getId()); 
			}
			
			order.setStatus("ACTIVE");
			
			if(sPayMethod!=null && loyaltyPoints > 0){
				sTranxId = "REWARDPOINTS_"+order.getId();
				order.setsTrnxId(sTranxId);
				loyaltyPointTrax.setStatus("ACTIVE");
			}else if(pPayMethod.equals(PaymentMethod.REWARD_POINTS)){
				sTranxId = "REWARDPOINTS_"+order.getId();
				order.setpTrnxId(sTranxId);
				loyaltyPointTrax.setStatus("ACTIVE");
			}
			
			cust.setLoyaltyPoints((cust.getLoyaltyPoints()+earnPoints) - loyaltyPoints);
			EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().update(loyaltyPointTrax);
			DAORegistry.getCustomerDAO().update(cust);
			try {
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(cust.getId(), cust.getLoyaltyPoints());
				System.out.println("UPDATED ORDER POINT IN CASSANDRA : "+cust.getLoyaltyPoints());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 5);
			
			//order.setDeliveryStatus("PENDING");
			//order.setDeliveryDateStr(dateTimeFormat1.format(cal.getTime()));
			EcommDAORegistry.getCustomerProdOrderDAO().update(order);
			CustomerUtil.updatedCustomerUtil(cust);
			inv.setUpdDate(today);
			inv.setAvlQty(inv.getAvlQty()-noOfItem);
			EcommDAORegistry.getSellerProdItemsVariInventoryDAO().update(inv);
			
			
			try {
				QuizCustomerReferralTracking referral = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(cust.getId());
				if(referral!=null && !referral.getIsPurchased()){
					CustomerLoyaltyPointTranx loyaltyPointTranx = EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().getCustomerLoyaltyPointTranxByCustIdAndRewardType(referral.getReferralCustomerId(), "REFERRAL_PURCHASE");
					if(loyaltyPointTranx==null){
						Customer refCustomer = DAORegistry.getCustomerDAO().get(referral.getReferralCustomerId());
						if(refCustomer!=null){
							loyaltyPointTranx = new CustomerLoyaltyPointTranx();
							loyaltyPointTrax.setCrBy(cust.getUserId());
							loyaltyPointTrax.setCrDate(new Date());
							loyaltyPointTrax.setCustId(refCustomer.getId());
							loyaltyPointTrax.setEarnPoints(1000);
							loyaltyPointTrax.setSpentPoints(0);
							loyaltyPointTrax.setBeforePoints(refCustomer.getLoyaltyPoints());
							loyaltyPointTrax.setAfterPoints(refCustomer.getLoyaltyPoints()+1000);
							loyaltyPointTrax.setStatus("ACTIVE");
							loyaltyPointTrax.setOrderId(order.getId());
							loyaltyPointTrax.setTranxType("REFERRAL_PURCHASE");
							EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
							
							referral.setIsPurchased(true);
							QuizDAORegistry.getQuizCustomerReferralTrackingDAO().update(referral);
							
							refCustomer.setLoyaltyPoints(refCustomer.getLoyaltyPoints()+loyaltyPointTrax.getEarnPoints());
							DAORegistry.getCustomerDAO().update(refCustomer);
							try {
								CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(refCustomer.getId(), refCustomer.getLoyaltyPoints());
								System.out.println("UPDATED REFERRAL ORDER POINT IN CASSANDRA : "+cust.getLoyaltyPoints());
							} catch (Exception e) {
								e.printStackTrace();
							}
							CustomerUtil.updatedCustomerUtil(refCustomer);
						}
					}
				}
				System.out.println("================= CREAT ORDER END ================= ");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			order.setNoOfItems(noOfItem);
			resp.setOrder(order);
			resp.setProds(orderProds);
			resp.setMessage("Order created successfully.");
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while creating customer cart order");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.PRODUCTORDER,"Error occured while creating customer cart order");
			return resp;
		}
	}
	
	
	
	
	@RequestMapping(value="/GetMyCoupenCodes",method=RequestMethod.POST)
	public CustCoupenCodeResp getMyCoupenCodes(HttpServletRequest request){
		Error error = new Error();
		CustCoupenCodeResp resp = new CustCoupenCodeResp();
		try {
			String custIdStr = request.getParameter("custId");
			String configIdString = request.getParameter("configId");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETMYCOUPENCODES,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETMYCOUPENCODES,"You are not Authorized");
				return resp;
			}
			
			Integer custId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETMYCOUPENCODES,"Invalid Customer ID");
				return resp;
			}
			
			Customer cust  = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid logged in customer details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETMYCOUPENCODES,"Invalid logged in customer details.");
				return resp;
			}
			
			List<CustCoupenCode> coupenCodeList = EcommDAORegistry.getCustomerCoupenCodeDAO().getAllActiveCustomerCoupenCodesByCustId(custId);
			if(coupenCodeList==null || coupenCodeList.isEmpty()){
				error.setDescription("No discount codes available at this moment.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETMYCOUPENCODES,"No Coupen Codes Found.");
				return resp;
			}
			resp.setList(coupenCodeList);
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting cust coupen Codes.");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYCOUPENCODES,"Error occured while gettting products.");
			return resp;
		}
	}
	
	@RequestMapping(value="/AddProdToCart",method=RequestMethod.POST)
	public CustomerCartResp addProdFromCart(HttpServletRequest request){
		Error error = new Error();
		CustomerCartResp resp = new CustomerCartResp();
		
		String custIdStr = request.getParameter("custId");
		String spiIdStr = request.getParameter("spiId");
		String spivInvIdStr = request.getParameter("spivInvId");
		String qtyStr = request.getParameter("qty");
		//String selPriceStr = request.getParameter("selPrice");
		String configIdString = request.getParameter("configId");
		try {
			
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(spivInvIdStr)){
				error.setDescription("Invalid Product Inventory Id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Invalid Product Inventory Id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(spiIdStr)){
				error.setDescription("Invalid Product Item Id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"Invalid Product Item Id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(qtyStr)){
				error.setDescription("Invalid Quantity");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Invalid Quantity");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"You are not Authorized");
				return resp;
			}
			
			Integer custId = 0;
			Integer spivInvId = 0;
			Integer spiId = 0;
			Integer qty = 0;
			try {
				custId = Integer.parseInt(custIdStr);
				spivInvId = Integer.parseInt(spivInvIdStr);
				qty = Integer.parseInt(qtyStr);
				spiId = Integer.parseInt(spiIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Product Inventory ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Invalid Product Inventory ID");
				return resp;
			}
			
			if(qty <= 0){
				error.setDescription("Please select product quantity.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Please select product quantity.");
				return resp;
			}
			
			
			Customer cust = DAORegistry.getCustomerDAO().get(custId);
			if(cust==null){
				error.setDescription("Invalid logged in customer details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Invalid logged in customer details");
				return resp;
			}
			
			SellerProductsItems product = EcommDAORegistry.getSellerProductsItemsDAO().getProductsById(spiId);
			if(product==null){
				error.setDescription("Product details not found in system.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Product details not found in system.");
				return resp;
			}
			if(product.getIsSoldOut()){
				error.setDescription("Selected product is sold out.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product is sold out.");
				return resp;
			}
			
			System.out.println("spivInvId: "+spivInvId+" :spivInvIdStr: "+spivInvIdStr+" :custId:"+custId+" :custIdStr:"+custIdStr);
			SellerProdItemsVariInventory inventoryObj = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByInventoryId(spivInvId);
			if(inventoryObj == null) {
				error.setDescription("Product Inventory Id Not Exists");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Product Inventory Id Not Exists");
				return resp;
			}
			Integer maxAvlQty = product.getMaxCustQty();
			if(maxAvlQty > inventoryObj.getAvlQty()){
				maxAvlQty = inventoryObj.getAvlQty();
			}
			
			if(qty > inventoryObj.getAvlQty()){
				error.setDescription("Selected product having only "+inventoryObj.getAvlQty()+" quantity available.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product having only "+inventoryObj.getAvlQty()+" available.");
				return resp;
			}
			
			if(inventoryObj.getIsRwdPointProd()){
				Integer reqPnt = (inventoryObj.getReqLPnts() * qty);
				if(cust.getLoyaltyPoints() < reqPnt){
					error.setDescription("You do not have enough reward points to purchase this item.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"You do not have enough reward point to purchase this item.");
					return resp;
				}
			}
			
			CustomerCart cartProd = EcommDAORegistry.getCustomerCartDAO().getCustomerCartByInventoryId(product.getSpiId(),spivInvId,cust.getId());
			if(cartProd!=null){
				Integer totalQty = qty + cartProd.getQty();
				if(totalQty > maxAvlQty){
					error.setDescription("Quantity Limit reached per customer.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product is already in your cart with maximum quantity limit.");
					return resp;
				}
				cartProd.setQty(cartProd.getQty()+qty);
				cartProd.setUpdDate(new Date());
				EcommDAORegistry.getCustomerCartDAO().update(cartProd);
			}else {
				if(qty > maxAvlQty){
					error.setDescription("Quantity Limit reached per customer.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product is already in your cart with maximum quantity limit.");
					return resp;
				}
				
				System.out.println("-------------- ADD CART QUANTITY LIMIT VALIDATION START --------------------");
				Integer remQty = EcommUtil.validateDailyProductOrderQtyLimit(custId, inventoryObj.getSpivInvId(), qty, product.getMaxCustQty());
				System.out.println("REMAINING QUANTITY : "+remQty+"   |   "+qty);
				if(remQty <= 0){
					error.setDescription("You have already purchase this item daily limit max quantity.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product is already in your cart with maximum quantity limit.");
					return resp;
				}
				
				if(remQty < qty){
					error.setDescription("This product daily max quantity limit is "+product.getMaxCustQty()+", You have already purchase this product with quantity "+(product.getMaxCustQty()-remQty));
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product is already in your cart with maximum quantity limit.");
					return resp;
				}
				System.out.println("-------------- ADD CART QUANTITY LIMIT VALIDATION END --------------------");
				
				cartProd = new CustomerCart();
				cartProd.setCustId(custId);
				cartProd.setSellerId(inventoryObj.getSellerId());
				cartProd.setSpiId(inventoryObj.getSpiId());
				cartProd.setSpivInvId(inventoryObj.getSpivInvId());
				cartProd.setQty(qty);
				cartProd.setSelPrice(inventoryObj.getSellPrice());
				cartProd.setIsPlacedOrder(false);
				cartProd.setStatus("ACTIVE");
				cartProd.setUpdDate(new Date());
				cartProd.setUpdBy(cust.getUserId());
				cartProd.setIsRwdPointProd(inventoryObj.getIsRwdPointProd());
				cartProd.setReqRwdPoint(inventoryObj.getReqLPnts());
				EcommDAORegistry.getCustomerCartDAO().save(cartProd);
			}
			//resp.setSubTotal(EcommUtil.computeOrderSubTotal(custId));
			resp.setMessage("Product added to cart.");
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while adding product to cart");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDTOCUSTCART,"Error occured while adding product to cart");
			return resp;
		} finally {
			System.out.println("final :spivInvIdStr: "+spivInvIdStr+" :custIdStr:"+custIdStr);
		}
	}
	@RequestMapping(value="/RemoveProdFromCart",method=RequestMethod.POST)
	public CustomerCartResp removeProdFromCart(HttpServletRequest request){
		Error error = new Error();
		CustomerCartResp resp = new CustomerCartResp();
		try {
			String custIdStr = request.getParameter("custId");
			String spiIdStr = request.getParameter("spiId");
			String spivInvIdStr = request.getParameter("spivInvId");
			String configIdString = request.getParameter("configId");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(spivInvIdStr)){
				error.setDescription("Invalid Product Inventory Id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"Invalid Product Inventory Id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(spiIdStr)){
				error.setDescription("Invalid Product Item Id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"Invalid Product Item Id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"You are not Authorized");
				return resp;
			}
			
			Integer custId = 0;
			Integer spivInvId = 0;
			Integer spiId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
				spivInvId = Integer.parseInt(spivInvIdStr);
				spiId = Integer.parseInt(spiIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Product Inventory Id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"Invalid Product Inventory Id");
				return resp;
			}
			
			Customer cust = DAORegistry.getCustomerDAO().get(custId);
			if(cust==null){
				error.setDescription("Invalid customer details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"Invalid customer details");
				return resp;
			}
			
			//Product prod = DAORegistry.getProductDAO().getProductByIdAndMercId(prodId, mercId);
			CustomerCart cartProd = EcommDAORegistry.getCustomerCartDAO().getCustomerCartByInventoryId(spiId,spivInvId,cust.getId());
			if(cartProd!=null){
				//cartProd.setStatus("DELETED");
				//cartProd.setUpdDate(new Date());
				//cartProd.setUpdBy(cust.getUserId());
				
				EcommDAORegistry.getCustomerCartDAO().delete(cartProd);
			}
			
			/*List<CustomerCart> cartProds = DAORegistry.getCustomerCartDAO().getAllCustomerCartProducts(custId, "ACTIVE");
			if(cartProds==null || cartProds.isEmpty()){
				error.setDescription("Your cart is empty");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REMOVEFROMCUSTCART,"Your cart is empty");
				return resp;
			}
			
			Double grossTotal = 0.00;
			for(CustomerCart cart : cartProds){
				if(cart.getBuyPrice() <= 0){
					continue;
				}
				grossTotal += (cart.getBuyPrice() * cart.getBuyUnits());
			}*/
			
			//resp.setOrderTotal(grossTotal);
			//resp.setProds(cartProds);
			//resp.setItemTxt("Number of Items : "+cartProds.size());
			System.out.println("============================ REMOVE CUSTOMER CART START ============================");
			resp.setSubTotal(EcommUtil.computeOrderSubTotal(custId));
			System.out.println("============================ REMOVE CUSTOMER CART END ============================");
			resp.setMessage("Product is removed from your cart.");
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while removing product from cart");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVEFROMCUSTCART,"Error occured while removing product from cart");
			return resp;
		}
	}
	
	@RequestMapping(value="/GetCustCart",method=RequestMethod.POST)
	public CustomerCartResp getCustCart(HttpServletRequest request){
		Error error = new Error();
		CustomerCartResp resp = new CustomerCartResp();
		try {
			String custIdStr = request.getParameter("custId");
			String configIdString = request.getParameter("configId");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETCUSTCART,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETCUSTCART,"You are not Authorized");
				return resp;
			}
			
			Integer custId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETCUSTCART,"Invalid Customer ID");
				return resp;
			}
			
			Customer cust = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid Customer login details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETCUSTCART,"Invalid Customer login details");
				return resp;
			}
			
			Double subTotal = 0.0;
			List<CustomerCartProdDtls> custProdDtlsList = EcommDAORegistry.getEcommQueryManagerDAO().getCustCartProductInventoryDetails(custId);
			if(custProdDtlsList == null || custProdDtlsList.isEmpty()) {
				error.setDescription("You have no products selected in your cart.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETCUSTCART,"Your cart is empty.");
				return resp;
			} else {
				Date today = new Date();
				for (CustomerCartProdDtls cartDtlObj : custProdDtlsList) {
					CustomerCart cart  = EcommDAORegistry.getCustomerCartDAO().get(cartDtlObj.getCrtId());
					if(cartDtlObj.getSpivInvId() == null){
						cartDtlObj.setSpivInvId(-1);
					}
					cart.setCustCode(null);
					if(cartDtlObj.getCustMaxQty() < cartDtlObj.getAvlQty()){
						cartDtlObj.setAvlQty(cartDtlObj.getCustMaxQty());
					}
					cartDtlObj.setIsOutStk(false);
					if((cartDtlObj.getAvlQty() <= 0 && cartDtlObj.getSpivInvId()>0) || cartDtlObj.getPstatus().equalsIgnoreCase("EXPIRED") || cartDtlObj.getpExpDate().before(today) || cartDtlObj.getIsSoldOut()) {
						cartDtlObj.setIsOutStk(true);
						cartDtlObj.setAvlQty(0);
						cartDtlObj.setOutStkTxt("Sold out");
						cartDtlObj.setValMsg("One or more product is out of stock.");
					}
					
					
					SellerProdItemsVariInventory inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByInventoryId(cartDtlObj.getSpivInvId());
					List<SellerProdItemsVariName> options = EcommDAORegistry.getSellerProdItemsVariNameDAO().getAllProductVariants(cart.getSpiId());
					for(SellerProdItemsVariName opt : options){
						List<SellerProdItemsVariValue> values = EcommDAORegistry.getSellerProdItemsVariValueDAO().getOptionValues(opt.getSlrpiVarId());
						opt.setSelectedVal(0);
						if(inv !=null){
							if(inv.getVnId1() != null && opt.getSlrpiVarId().equals(inv.getVnId1())){
								opt.setSelectedVal(inv.getVvId1());
							}else if(inv.getVnId2() != null && opt.getSlrpiVarId().equals(inv.getVnId2())){
								opt.setSelectedVal(inv.getVvId2());
							}else if(inv.getVnId3() != null && opt.getSlrpiVarId().equals(inv.getVnId3())){
								opt.setSelectedVal(inv.getVvId3());
							}else if(inv.getVnId4() != null && opt.getSlrpiVarId().equals(inv.getVnId4())){
								opt.setSelectedVal(inv.getVvId4());
							}
						}
						opt.setValues(values);
					}
					
					SellerProductsItems prod = null;
					if(inv == null){
						inv = new SellerProdItemsVariInventory();
						prod = EcommDAORegistry.getSellerProductsItemsDAO().getProductById(cart.getSpiId());
						cartDtlObj.setpName(prod.getpName());
						cartDtlObj.setpImg(prod.getpImgUrl());
						cartDtlObj.setSpiId(prod.getSpiId());
						cartDtlObj.setPriceAftLPnts(prod.getPsMinPrice());
						cartDtlObj.setReqLPnts(0);
						cartDtlObj.setIsLoyPntUsed(false);
						cartDtlObj.setValMsg("Please complete all product variants selection.");
						cartDtlObj.setSellPrice(prod.getPsMinPrice());
						cartDtlObj.setRegPrice(prod.getpRegMinPrice());
						cartDtlObj.setSpivInvId(-1);
						inv.setRegPrice(prod.getpRegMinPrice());
						inv.setSellPrice(prod.getPsMinPrice());
						inv.setFinalPrice(prod.getPsMinPrice());
						inv.setIsRwdPointProd(prod.getIsRwdPointProd());
						inv.setReqLPnts(prod.getReqRwdPoint());
						inv.setSpivInvId(-1);
						inv.setIsRwdPointProd(prod.getIsRwdPointProd());
						inv.setProdType(prod.getProdType());
						inv.setReqLPnts(prod.getReqRwdPoint());
						inv.setpImg(prod.getpImgUrl());
						cartDtlObj.setDiscPerc(inv.getDiscPerc());
						inv.setAvlQty(1);
					}
					
					
					
					Double custCodediscount = 0.00;
					Double codeDiscount = 0.00;
					Double loyaltyPointDisc = 0.00;
					Integer reqLPonts = 0;
					
					CustCoupenCode custCCode = null;
					if(cart.getCustCode()!=null && !cart.getCustCode().isEmpty()){
						custCCode = EcommDAORegistry.getCustomerCoupenCodeDAO().getActiveCustomerCoupenCodesByCustIdAndCode(custId, cart.getCustCode());
					}else{
						List<CustCoupenCode> custCodes = EcommDAORegistry.getEcommQueryManagerDAO().getCustomerProductCouponCodes(cart.getSpiId(), "CONTEST", custId,null);
						if(custCodes!=null && !custCodes.isEmpty()){
							custCCode = custCodes.get(0);
						}
					}
					if(!inv.getIsRwdPointProd()){
						resp.setOfferHdr("Offers and Discounts");
						if(custCCode!=null){
							if(custCCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
								custCodediscount = EcommUtil.getNormalRoundedValue(cart.getQty()*(inv.getSellPrice()*custCCode.getDisc())/100);
								cartDtlObj.setCustCodeStr("You earned a "+custCCode.getDisc().intValue()+"% GAME SHOW discount: $"+custCodediscount);
							}else{
								custCodediscount = EcommUtil.getNormalRoundedValue(cart.getQty()*custCCode.getDisc());
								cartDtlObj.setCustCodeStr("You earned a $"+custCodediscount+" GAME SHOW discount");
							}
							
							cart.setCustCode(custCCode.getCpncde());
							cartDtlObj.setCustCode(custCCode.getCpncde());
						}
						
						if(cart.getCode()!=null && !cart.getCode().isEmpty()){
							CoupenCode cCode = EcommDAORegistry.getCoupenCodeDAO().getProductCodesByCode(cart.getCode());
							if(cCode!=null){
								if(cCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
									codeDiscount =  EcommUtil.getNormalRoundedValue(cart.getQty()*(inv.getSellPrice()*cCode.getCpnDisc())/100);
								}else{
									codeDiscount = EcommUtil.getNormalRoundedValue(cart.getQty()*cCode.getCpnDisc());
								}
								cartDtlObj.setCode(cCode.getCpnCode());
								cartDtlObj.setCodeStr("Discount code "+cart.getCode()+" applied on this product with discount $"+codeDiscount);
							}
						}else{
							cart.setCode(null);
							List<CoupenCode> codes = EcommDAORegistry.getEcommQueryManagerDAO().getProductCouponCodes(inv.getSpiId(),"PUBLIC");
							for(CoupenCode cCode : codes){
								if(cCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
									codeDiscount =  EcommUtil.getNormalRoundedValue(cart.getQty()*(inv.getSellPrice()*cCode.getCpnDisc())/100);
								}else{
									codeDiscount = EcommUtil.getNormalRoundedValue(cart.getQty()*cCode.getCpnDisc());
								}
								cartDtlObj.setCode(cCode.getCpnCode());
								cart.setCode(cCode.getCpnCode());
								cartDtlObj.setCodeStr("Discount code "+cart.getCode()+" applied on this product with discount $"+codeDiscount);
								break;
							}
						}
					}
					
					
					if(cart.getIsLoyPntUsed() != null && cart.getIsLoyPntUsed()){
						reqLPonts += reqLPonts+(inv.getReqLPnts()*cartDtlObj.getCrtQty());
						inv.setFinalPrice(EcommUtil.getNormalRoundedValue((inv.getPriceAftLPnts()*cartDtlObj.getCrtQty())-(codeDiscount+custCodediscount)));
					}else{
						inv.setFinalPrice(EcommUtil.getNormalRoundedValue((inv.getSellPrice()*cartDtlObj.getCrtQty())-(codeDiscount+custCodediscount)));
					}
					
					if(reqLPonts > cust.getLoyaltyPoints()){
						cartDtlObj.setValMsg("You do not have enough reward points to apply to this order, Your current reward point balance is "+cust.getLoyaltyPoints()+" reward points.");
					}
					
					if(inv.getPriceAftLPnts() > 0 && inv.getReqLPnts() > 0 && !inv.getIsRwdPointProd()){
						loyaltyPointDisc =  EcommUtil.getNormalRoundedValue((inv.getSellPrice() - inv.getPriceAftLPnts()) * cart.getQty());
						cartDtlObj.setLoyaltyPointStr("Get additional discount $"+EcommUtil.getRoundedValueString(loyaltyPointDisc)+" on use of "+inv.getReqLPnts() * cart.getQty()+" reward point.");
					}
					
					subTotal = subTotal + inv.getFinalPrice();
					inv.setLytpntDisc(loyaltyPointDisc);
					inv.setCodeDisc(codeDiscount);
					inv.setReqLPnts(inv.getReqLPnts() * cartDtlObj.getCrtQty());
					inv.setCustCodeDisc(custCodediscount);
					inv.setTotRegPrice(inv.getRegPrice()*cartDtlObj.getCrtQty());
					if(cartDtlObj.getIsOutStk()){
						inv.setFinalPrice(inv.getSellPrice());
					}
					cartDtlObj.setInv(inv);
					cartDtlObj.setDiscPerc(inv.getDiscPerc());
					cartDtlObj.setOptions(options);
					cart.setIsRwdPointProd(inv.getIsRwdPointProd());
					cart.setReqRwdPoint(inv.getReqLPnts());
					EcommDAORegistry.getCustomerCartDAO().update(cart);
				}
				resp.setList(custProdDtlsList);
				System.out.println("============================ GET CUSTOMER CART START ============================");
				resp.setSubTotal(EcommUtil.computeOrderSubTotal(custId));
				System.out.println("============================ GET CUSTOMER CART END ============================");
			}
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting customer cart");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTCART,"Error occured while gettting customer cart");
			return resp;
		}
	}
	
	
	
	
	@RequestMapping(value="/GetCartInventoryDetails",method=RequestMethod.POST)
	public CustomerCartResp getCartInventoryDetails(HttpServletRequest request){
		Error error = new Error();
		CustomerCartResp resp = new CustomerCartResp();
		CustomerCartProdDtls cartDtlObj = new CustomerCartProdDtls();
		resp.setCart(cartDtlObj);
		try {
			String custIdStr = request.getParameter("custId");
			String invIdStr = request.getParameter("invId");
			String prodIdStr = request.getParameter("prodId");
			String qtyStr = request.getParameter("qty");
			String opValId1 = request.getParameter("opValId1");
			String opValId2 = request.getParameter("opValId2");
			String opValId3 = request.getParameter("opValId3");
			String opValId4 = request.getParameter("opValId4");
			String custCode = request.getParameter("custCode");
			String code = request.getParameter("code");
			String isLoyPntUsedStr = request.getParameter("isLoyPntUsed");
			String configIdString = request.getParameter("configId");
			
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Invalid Customer ID");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid Customer ID");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"You are not Authorized");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(prodIdStr)){
				error.setDescription("Invalid product id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(invIdStr)){
				error.setDescription("Invalid product inventory details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product inventory details.");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(isLoyPntUsedStr)){
				error.setDescription("Invalid purchase with reward points selection.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid purchase with reward points selection.");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(qtyStr)){
				error.setDescription("Invalid quantity selection.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid quantity selection.");
				return resp;
			}
			
			Integer custId = 0;
			Integer prodId = 0;
			Integer invId = 0;
			Integer qty = 0;
			Boolean isLoyPntUsed = false;;
			Customer cust  = null;
			try {
				invId = Integer.parseInt(invIdStr);
				custId = Integer.parseInt(custIdStr);
				prodId = Integer.parseInt(prodIdStr);
				qty = Integer.parseInt(qtyStr);
				isLoyPntUsed = Boolean.valueOf(isLoyPntUsedStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product details");
				return resp;
			}
			
			if(qty <= 0){
				error.setDescription("Quantity must be greater than 0");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Quantity must be greater than 0");
				return resp;
			}
			
			cust  = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid logged in customer details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid logged in customer details.");
				return resp;
			}
			
			SellerProductsItems product = EcommDAORegistry.getSellerProductsItemsDAO().getProductById(prodId);
			if(product==null){
				error.setDescription("Product not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Product not found");
				return resp;
			}
			
			if(opValId1 == null || opValId1.isEmpty()){
				error.setDescription("Invalid product variant details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product variant details");
				return resp;
			}
			
			Integer valId1 = 0;
			Integer valId2 = 0;
			Integer valId3 = 0;
			Integer valId4 = 0;
			try {
				valId1 = Integer.parseInt(opValId1);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product variant value id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Invalid product variant value id");
				return resp;
			}
			try {
				valId2 = Integer.parseInt(opValId2);
			} catch (Exception e) {
			}
			try {
				valId3 = Integer.parseInt(opValId3);
			} catch (Exception e) {
			}
			try {
				valId4 = Integer.parseInt(opValId4);
			} catch (Exception e) {
			}
			
			Double custCodediscount = 0.00;
			Double codeDiscount = 0.00;
			Double loyaltyPointDisc = 0.00;
			Double subTotal = 0.00;
			Date today = new Date();
			
			SellerProdItemsVariInventory inv = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByOptionValue(valId1, valId2, valId3, valId4);
			
			System.out.println("PROD ID : "+prodId+"  |  "+valId1+"  |  "+valId2+"  |  "+valId3+"  |  "+valId4);
			System.out.println("INV ID : "+invId+" | "+inv.getSpivInvId());
			System.out.println("CUST ID : "+custId+"   |   "+cust.getEmail()+"   |   "+custCode+"   |   "+code);
			
			CustomerCart cart = EcommDAORegistry.getCustomerCartDAO().getCustomerCartByInventoryId(product.getSpiId(),invId,cust.getId());
			CustomerCart oldCart = EcommDAORegistry.getCustomerCartDAO().getCustomerCartByInventoryId(product.getSpiId(),inv.getSpivInvId(),cust.getId());
			if(oldCart!=null && ((cart.getIsLoyPntUsed() ==null && !isLoyPntUsed) || (cart.getIsLoyPntUsed() !=null && cart.getIsLoyPntUsed().equals(isLoyPntUsed)))){
				if(((code == null || code.isEmpty()) && (cart.getCode()==null || cart.getCode().isEmpty())) || (code!=null && cart.getCode()!=null && code.equalsIgnoreCase(cart.getCode()))){
					if(cart.getQty().equals(qty) || cart.getQty() == qty){
						System.out.println("SAME VARIANT NOT ALLOWED.");
						error.setDescription("Selected product variant is already there on your cart.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Selected product variant is already there on your cart.");
						return resp;
					}
				}
			}
			
			//if(!cart.getQty().equals(qty) && invId.equals(inv.getSpivInvId())){
				System.out.println("-------------- CHANGE CART QUANTITY LIMIT VALIDATION START --------------------");
				Integer remQty = EcommUtil.validateDailyProductOrderQtyLimit(custId, inv.getSpivInvId(), qty, product.getMaxCustQty());
				System.out.println("REMAINING QUANTITY : "+remQty+"   |   "+qty);
				if(remQty <= 0){
					error.setDescription("You have already purchase this item daily limit max quantity.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product is already in your cart with maximum quantity limit.");
					return resp;
				}
				
				if(remQty < qty){
					error.setDescription("This product daily max quantity limit is "+product.getMaxCustQty()+", You have already purchase this product with quantity "+(product.getMaxCustQty()-remQty));
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.ADDTOCUSTCART,"Selected product is already in your cart with maximum quantity limit.");
					return resp;
				}
				System.out.println("-------------- CHANGE CART QUANTITY LIMIT VALIDATION END --------------------");
			//}
			
			cart.setIsLoyPntUsed(isLoyPntUsed);
			cart.setQty(qty);
			cartDtlObj.setIsOutStk(false);
			cartDtlObj.setRegPrice(inv.getRegPrice());
			cartDtlObj.setSellPrice(inv.getSellPrice());
			cartDtlObj.setReqLPnts(inv.getReqLPnts());
			cartDtlObj.setPriceAftLPnts(inv.getPriceAftLPnts());
			if(product.getMaxCustQty() < inv.getAvlQty()){
				cartDtlObj.setAvlQty(product.getMaxCustQty() );
			}else{
				cartDtlObj.setAvlQty(inv.getAvlQty());
			}
			if(inv == null || inv.getAvlQty() <= 0  || qty >  inv.getAvlQty() || product.getStatus().equalsIgnoreCase("EXPIRED") || product.getpExpDate().before(today) || product.getIsSoldOut()){
				cartDtlObj.setIsOutStk(true);
				cartDtlObj.setAvlQty(0);
				cartDtlObj.setOutStkTxt("Sold out");
				cartDtlObj.setValMsg("One or more product is out of stock.");
			}
			
			
			if((custCode!=null && !custCode.isEmpty() && !cartDtlObj.getIsOutStk()) || invId < 0){
				List<CustCoupenCode> cCodes = null;
				if(invId < 0){
					cCodes = EcommDAORegistry.getEcommQueryManagerDAO().getCustomerProductCouponCodes(cart.getSpiId(), "CONTEST", custId,null);
				}else{
					cCodes = EcommDAORegistry.getEcommQueryManagerDAO().getCustomerProductCouponCodes(prodId, "CONTEST", custId, custCode);
				}
				
				CustCoupenCode cCode = null;
				System.out.println("CUSTCODE SIZE : "+cCodes.size());
				if(cCodes!=null && !cCodes.isEmpty()){
					cCode = cCodes.get(0);
				}
				
				if(cCode!=null && !inv.getIsRwdPointProd()){
					if(cCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						custCodediscount = EcommUtil.getNormalRoundedValue(cart.getQty()*(inv.getSellPrice()*cCode.getDisc())/100);
						cartDtlObj.setCustCodeStr("You earned a "+cCode.getDisc().intValue()+"% GAME SHOW discount: $"+custCodediscount);
					}else{
						custCodediscount = EcommUtil.getNormalRoundedValue(cart.getQty()*cCode.getDisc());
						cartDtlObj.setCustCodeStr("You earned a $"+custCodediscount+" GAME SHOW discount");
					}
					cartDtlObj.setCustCode(custCode);
					cart.setCustCode(custCode);
				}else{
					cart.setCustCode(null);
				}
			}
			
			if(code!=null && !code.isEmpty() && !cartDtlObj.getIsOutStk() && !inv.getIsRwdPointProd()){
				CoupenCode cCode = EcommDAORegistry.getEcommQueryManagerDAO().getProductCouponCodes(product.getSpiId(), "PUBLIC", code);
				if(cCode!=null){
					if(cCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						codeDiscount =  EcommUtil.getNormalRoundedValue(cart.getQty()*(inv.getSellPrice()*cCode.getCpnDisc())/100);
					}else{
						codeDiscount = EcommUtil.getNormalRoundedValue(cart.getQty()*cCode.getCpnDisc());
					}
					cartDtlObj.setCodeStr("Discount code "+code+" applied on this product with discount $"+codeDiscount);
					cart.setCode(code);
					cartDtlObj.setCode(code);
				}else{
					cartDtlObj.setValMsg("Entered discount code not found.");
					cart.setCode(null);
				}
			}else if(invId < 0){
				cart.setCode(null);
				List<CoupenCode> codes = EcommDAORegistry.getEcommQueryManagerDAO().getProductCouponCodes(inv.getSpiId(),"PUBLIC");
				for(CoupenCode cCode : codes){
					if(cCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						codeDiscount =  EcommUtil.getNormalRoundedValue(cart.getQty()*(inv.getSellPrice()*cCode.getCpnDisc())/100);
					}else{
						codeDiscount = EcommUtil.getNormalRoundedValue(cart.getQty()*cCode.getCpnDisc());
					}
					cartDtlObj.setCode(cCode.getCpnCode());
					cart.setCode(cCode.getCpnCode());
					cartDtlObj.setCodeStr("Discount code "+cart.getCode()+" applied on this product with discount $"+codeDiscount);
					break;
				}
			}
			
			Integer reqPoints = inv.getReqLPnts() * cart.getQty();
			
			if(cart.getIsLoyPntUsed() != null && cart.getIsLoyPntUsed()){
				if(reqPoints < cust.getLoyaltyPoints()){
					inv.setFinalPrice(EcommUtil.getNormalRoundedValue((inv.getPriceAftLPnts()*cart.getQty())-(codeDiscount+custCodediscount)));
				}else{
					cart.setIsLoyPntUsed(false);
					cartDtlObj.setValMsg("You do not have enough reward points to apply to this order, Your current reward point balance is "+cust.getLoyaltyPoints()+" reward points.");
					inv.setFinalPrice(EcommUtil.getNormalRoundedValue((inv.getSellPrice()*cart.getQty())-(codeDiscount+custCodediscount)));
					System.out.println("You do not have enough reward points ");
					error.setDescription("You do not have enough reward points to apply to this order, Your current reward point balance is "+cust.getLoyaltyPoints()+" reward points.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Selected product variant is already there on your cart.");
					return resp;
				}
			}else{
				if(inv.getIsRwdPointProd()){
					if(reqPoints > cust.getLoyaltyPoints()){
						cartDtlObj.setValMsg("You do not have enough reward points to apply to this order, Your current reward point balance is "+cust.getLoyaltyPoints()+" reward points.");
						System.out.println("You do not have enough reward points ");
						error.setDescription("You do not have enough reward points to apply to this order, Your current reward point balance is "+cust.getLoyaltyPoints()+" reward points.");
						resp.setError(error);
						resp.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.GETPRODUCTS,"Selected product variant is already there on your cart.");
						return resp;
					}
				}
				cart.setIsLoyPntUsed(false);
				inv.setFinalPrice(EcommUtil.getNormalRoundedValue((inv.getSellPrice()*cart.getQty())-(codeDiscount+custCodediscount)));
			}
			
			if(inv.getPriceAftLPnts() > 0 && inv.getReqLPnts() > 0 && !inv.getIsRwdPointProd()){
				loyaltyPointDisc =  EcommUtil.getNormalRoundedValue((inv.getSellPrice() - inv.getPriceAftLPnts()) * cart.getQty());
				cartDtlObj.setLoyaltyPointStr("Get additional discount $"+EcommUtil.getRoundedValueString(loyaltyPointDisc)+" on use of "+inv.getReqLPnts() * cart.getQty()+" reward points.");
			}
			
			cartDtlObj.setCrtQty(cart.getQty());
			//subTotal = subTotal + inv.getFinalPrice();
			inv.setCustCodeDisc(custCodediscount);
			inv.setReqLPnts(inv.getReqLPnts() * cartDtlObj.getCrtQty());
			inv.setCodeDisc(codeDiscount);
			inv.setLytpntDisc(loyaltyPointDisc);
			inv.setTotRegPrice(inv.getRegPrice()*cart.getQty());
			cart.setSpivInvId(inv.getSpivInvId());
			cart.setUpdDate(new Date());
			cart.setIsRwdPointProd(inv.getIsRwdPointProd());
			cart.setReqRwdPoint(inv.getReqLPnts());
			cartDtlObj.setDiscPerc(inv.getDiscPerc());
			if(cartDtlObj.getIsOutStk()){
				inv.setFinalPrice(inv.getSellPrice());
			}
			EcommDAORegistry.getCustomerCartDAO().update(cart);
			cartDtlObj.setIsLoyPntUsed(cart.getIsLoyPntUsed());
			cartDtlObj.setInv(inv);
			System.out.println("============================ GET CUSTOMER CART INVENTORY START ============================");
			resp.setSubTotal(EcommUtil.computeOrderSubTotal(custId));
			System.out.println("============================ GET CUSTOMER CART INVENTORY END ============================");
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting customer cart inventory details.");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETPRODUCTS,"Error occured while gettting customer cart inventory details.");
			return resp;
		}
	}
	
	
	
	@RequestMapping(value="/Checkout",method=RequestMethod.POST)
	public @ResponsePayload CheckoutResp checkout(HttpServletRequest request,HttpServletResponse response){
		CheckoutResp resp =new CheckoutResp();
		Error error = new Error();
		String loyaltyPointsStr = request.getParameter("loyaltyPoints");
		String noOfItemStr = request.getParameter("noOfItem");
		String custIdStr = request.getParameter("custId");
		String shipAddIdStr = request.getParameter("shipAddId");
		String orderTotalStr = request.getParameter("orderTotal");
		
		try {
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Customer id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(noOfItemStr)){
				error.setDescription("Invalid No of items");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid No of items");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(loyaltyPointsStr)){
				error.setDescription("Invalid reward points.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid reward points.");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shipAddIdStr)){
				error.setDescription("Invalid Shipping address id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid Shipping address id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(orderTotalStr)){
				error.setDescription("Order total is mendatory.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Order total is mendatory.");
				return resp;
			}
			Integer custId = 0;
			Integer noOfItem = 0;
			Integer shipId = 0;
			Double orderTotal = 0.00;
			Integer loyaltyPoints = 0;
			try {
				custId = Integer.parseInt(custIdStr);
				loyaltyPoints = Integer.parseInt(loyaltyPointsStr);
				shipId = Integer.parseInt(shipAddIdStr);
				noOfItem = Integer.parseInt(noOfItemStr);
				orderTotal = Double.parseDouble(orderTotalStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid order details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid order details");
				return resp;
			}
			
			Customer cust  = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid customer details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid customer details.");
				return resp;
			}

			UserAddress shipAddr = DAORegistry.getUserAddressDAO().get(shipId);
			if(shipAddr==null){
				error.setDescription("Shipping address not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Shipping address not found");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shipAddr.getZipCode())) {
				error.setDescription("Zip Code Mandatory in Shipping address");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Zip Code Mandatory in Shipping address");
				return resp;
			}
			
			if(loyaltyPoints > cust.getLoyaltyPoints()){
				error.setDescription("Customer does not have enough reward points to make this order");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer does not have enough reward points to make this order");
				return resp;
			}
			
			Map<Integer,CartProductPOJO> cartMap = new HashMap<Integer,CartProductPOJO>();
			List<CustomerCart> carts = EcommDAORegistry.getCustomerCartDAO().getAllCustomerCartByCustomerId(cust.getId());
			if(carts == null){
				error.setDescription("Cart details not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Cart details not found");
				return resp;
			}
			boolean isRewardPointsOrder = false;
			boolean isOnlyGiftcardORder = true;
			List<Integer> invIds = new ArrayList<Integer>();
			CartProductPOJO prod = null;
			Double oTotal = 0.00;
			for(CustomerCart cart: carts){
				prod = new CartProductPOJO();
				prod.setCrtId(cart.getCrtId());
				prod.setCustId(cart.getCustId());
				prod.setSpiId(cart.getSpiId());
				prod.setSpivInvId(cart.getSpivInvId());
				
				Double custCodediscount = 0.00;
				if(cart.getCustCode()!=null && !cart.getCustCode().isEmpty()){
					CustCoupenCode custCCode = EcommDAORegistry.getCustomerCoupenCodeDAO().getActiveCustomerCoupenCodesByCustIdAndCode(custId, cart.getCustCode());
					if(custCCode!=null){
						if(custCCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
							custCodediscount = EcommUtil.getNormalRoundedValue(cart.getQty()*(cart.getSelPrice()*custCCode.getDisc())/100);
						}else{
							custCodediscount = EcommUtil.getNormalRoundedValue(cart.getQty()*custCCode.getDisc());
						}
					}
				}
				Double codeDiscount = 0.00;
				if(cart.getCode()!=null && !cart.getCode().isEmpty()){
					CoupenCode cCode = EcommDAORegistry.getCoupenCodeDAO().getProductCodesByCode(cart.getCode());
					if(cCode!=null){
						if(cCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
							codeDiscount =  EcommUtil.getNormalRoundedValue(cart.getQty()*(cart.getSelPrice()*cCode.getCpnDisc())/100);
						}else{
							codeDiscount = EcommUtil.getNormalRoundedValue(cart.getQty()*cCode.getCpnDisc());
						}
					}
				}
				prod.setIsPointUsed(cart.getIsLoyPntUsed());
				prod.setTotalDisc(custCodediscount + codeDiscount);
				prod.setpPrice(cart.getSelPrice());
				prod.setQty(cart.getQty());
				prod.setReqPoints(cart.getReqRwdPoint());
				cartMap.put(prod.getSpivInvId(), prod);
				invIds.add(cart.getSpivInvId());
			}
			List<SellerProdItemsVariInventory> inventories = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByIds(invIds);
			for(SellerProdItemsVariInventory inv: inventories){
				prod = cartMap.get(inv.getSpivInvId());
				if(prod !=null){
					prod.setpImg(inv.getpImg());
					prod.setpVariantDtl(inv.getvOptNameValCom());
					prod.setpName(inv.getpName());
					if(prod.getIsPointUsed() != null && prod.getIsPointUsed()){
						Integer reqLPonts = (inv.getReqLPnts()*prod.getQty());
						prod.setReqPoints(reqLPonts);
						prod.setpPrice(EcommUtil.getNormalRoundedValue((inv.getPriceAftLPnts()*prod.getQty())-(prod.getTotalDisc())));
					}else{
						Integer reqLPonts = (inv.getReqLPnts()*prod.getQty());
						prod.setReqPoints(reqLPonts);
						prod.setpPrice(EcommUtil.getNormalRoundedValue((inv.getSellPrice()*prod.getQty())-(prod.getTotalDisc())));
					}
					oTotal = oTotal + prod.getpPrice();
					cartMap.put(inv.getSpivInvId(), prod);
				}
				if(inv.getIsRwdPointProd() && (inv.getProdType() != null && inv.getProdType().equalsIgnoreCase("PRODUCT"))) {
					isRewardPointsOrder = true;
				}
				if(inv.getProdType() == null || !(inv.getProdType().equalsIgnoreCase("GIFTCARD") || inv.getProdType().equalsIgnoreCase("DIGITALPRODUCT"))) {
					isOnlyGiftcardORder = false;
				}
			}
			
			/*if(cart.size() > noOfItem || cart.size() < noOfItem){
				error.setDescription("No of item not matching with actual item in cart");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"No of item not matching with actual item in cart");
				return resp;
			}*/
			resp.setShippingCharge(EcommUtil.computeShippingFees(noOfItem,shipAddr.getCountryName(),isRewardPointsOrder,isOnlyGiftcardORder));
			try {
				Double taxAmt = TaxJarUtil.calculateTaxAmount(properties.getTaxjarKey(),orderTotal, shipAddr.getZipCode());
				if(taxAmt == null) {
					error.setDescription("something went wrong while calculating tax amount");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"something went wrong while calculating tax amount");
					return resp;
				}
				resp.setTax(taxAmt);
				
				
				StripeCredentials stripeCredentials= DAORegistry.getQueryManagerDAO().getStripeCredentials();
				if(stripeCredentials == null || stripeCredentials.getPublishableKey()==null || stripeCredentials.getPublishableKey().isEmpty()){
					error.setDescription("Error occured while connecting to stripe.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while connecting to stripe.");
					return resp;
				}
				resp.setPublishableKey(stripeCredentials.getPublishableKey());
				
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Error occured while Calculating Tax Amount");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while Calculating Tax Amount");
				return resp;
				//resp.setTax(2.00);
			}
			System.out.println("============================ CHECKOUT START ============================");
			System.out.println("CHECKOUT : ORDER TOTAL = "+orderTotal+"       |        "+oTotal);
			resp.setOrderTotal(EcommUtil.getNormalRoundedValue(EcommUtil.computeOrderSubTotal(custId)+resp.getTax()+resp.getShippingCharge()));
			System.out.println("============================ CHECKOUT END ============================");
			resp.setProds(new ArrayList<CartProductPOJO>(cartMap.values()));
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while checkout cart");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while checkout cart");
			return resp;
		}
	}
	
	
	
	
	@RequestMapping(value="/LiveCheckout",method=RequestMethod.POST)
	public @ResponsePayload CheckoutResp liveCheckout(HttpServletRequest request,HttpServletResponse response){
		CheckoutResp resp =new CheckoutResp();
		Error error = new Error();
		String loyaltyPointsStr = request.getParameter("loyaltyPoints");
		String noOfItemStr = request.getParameter("noOfItems");
		String custIdStr = request.getParameter("custId");
		String shipAddIdStr = request.getParameter("shipAddId");
		String orderTotalStr = request.getParameter("orderTotal");
		String prodIdStr = request.getParameter("prodId");
		
		
		try {
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Customer id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(noOfItemStr)){
				error.setDescription("Invalid No of items");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid No of items");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(loyaltyPointsStr)){
				loyaltyPointsStr = "0";
			}
			if(TextUtil.isEmptyOrNull(prodIdStr)){
				error.setDescription("Invalid product id.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid reward points.");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shipAddIdStr)){
				error.setDescription("Invalid Shipping address id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid Shipping address id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(orderTotalStr)){
				error.setDescription("Order total is mendatory.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Order total is mendatory.");
				return resp;
			}
			Integer custId = 0;
			Integer qty = 0;
			Integer shipId = 0;
			Double orderTotal = 0.00;
			Integer loyaltyPoints = 0;
			Integer prodId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
				loyaltyPoints = Integer.parseInt(loyaltyPointsStr);
				shipId = Integer.parseInt(shipAddIdStr);
				qty = Integer.parseInt(noOfItemStr);
				prodId = Integer.parseInt(prodIdStr);
				orderTotal = Double.parseDouble(orderTotalStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid order details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid order details");
				return resp;
			}
			
			Customer cust  = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Invalid customer details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid customer details.");
				return resp;
			}

			UserAddress shipAddr = DAORegistry.getUserAddressDAO().get(shipId);
			if(shipAddr==null){
				error.setDescription("Shipping address not found");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Shipping address not found");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(shipAddr.getZipCode())) {
				error.setDescription("Zip Code Mandatory in Shipping address");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Zip Code Mandatory in Shipping address");
				return resp;
			}
			
			if(loyaltyPoints > cust.getLoyaltyPoints()){
				error.setDescription("Customer does not have enough reward points to make this order");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer does not have enough reward points to make this order");
				return resp;
			}
			
			SellerProductsItems prod = EcommDAORegistry.getSellerProductsItemsDAO().getProductById(prodId);
			if(prod == null){
				error.setDescription("Invalid product details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid product details.");
				return resp;
			}
			boolean isRewardPointsOrder = false;
			boolean isOnlyGiftcardORder = true;
			double oTotal =  EcommUtil.getNormalRoundedValue((prod.getPsMinPrice())*qty);
			if(prod.getIsRwdPointProd() != null && prod.getIsRwdPointProd() && loyaltyPoints > 0){
				Integer reqLPonts = (prod.getReqRwdPoint()*qty);
				if(loyaltyPoints < reqLPonts){
					error.setDescription("You do not have enough reward points.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid product details.");
					return resp;
				}
				oTotal = EcommUtil.getNormalRoundedValue((prod.getPsMinPrice())*qty)-(reqLPonts/100);
			}
			
			if(oTotal > orderTotal || oTotal < orderTotal){
				error.setDescription("Order total mismatching.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Order total mismatching.");
				return resp;
			}
			
			resp.setShippingCharge(EcommUtil.computeShippingFees(qty,shipAddr.getCountryName(),isRewardPointsOrder,isOnlyGiftcardORder));
			try {
				Double taxAmt = TaxJarUtil.calculateTaxAmount(properties.getTaxjarKey(),orderTotal, shipAddr.getZipCode());
				if(taxAmt == null) {
					error.setDescription("something went wrong while calculating tax amount");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"something went wrong while calculating tax amount");
					return resp;
				}
				resp.setTax(taxAmt);
				
				
				StripeCredentials stripeCredentials= DAORegistry.getQueryManagerDAO().getStripeCredentials();
				if(stripeCredentials == null || stripeCredentials.getPublishableKey()==null || stripeCredentials.getPublishableKey().isEmpty()){
					error.setDescription("Error occured while connecting to stripe.");
					resp.setError(error);
					resp.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while connecting to stripe.");
					return resp;
				}
				resp.setPublishableKey(stripeCredentials.getPublishableKey());
				oTotal = oTotal + resp.getShippingCharge() + resp.getTax();
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Error occured while Calculating Tax Amount");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while Calculating Tax Amount");
				return resp;
			}
			System.out.println("============================ CHECKOUT START ============================");
			System.out.println("CHECKOUT : ORDER TOTAL = "+orderTotal+"       |        "+oTotal);
			resp.setOrderTotal(oTotal);
			System.out.println("============================ CHECKOUT END ============================");
			resp.setProd(prod);
			resp.setNoOfItems(qty);
			resp.setStatus(1);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while checkout.");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while checkout cart");
			return resp;
		}
	}
	
	
	
	
	
	@RequestMapping(value="/SaveCustCart",method=RequestMethod.POST)
	public @ResponsePayload GenericResp saveCustCart(HttpServletRequest request,HttpServletResponse response){
		GenericResp resp =new GenericResp();
		Error error = new Error();
		String custIdStr = request.getParameter("custId");
		String prodIdStr = request.getParameter("prodId");
		String qtyStr = request.getParameter("qty");
		String priceStr = request.getParameter("price");
		String invIdStr = request.getParameter("invId");
		System.out.println("ADD CART CALLED FROM SAAS :    custId = "+custIdStr+"  |    prodID = "+prodIdStr+"   |   Qty = "+qtyStr+"   |   price = "+priceStr+"   |   invId = "+invIdStr);
		try {
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Customer id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(prodIdStr)){
				error.setDescription("Product id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(invIdStr)){
				error.setDescription("Product inventory id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product inventory id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(qtyStr)){
				error.setDescription("Quantity is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Quantity is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(qtyStr)){
				error.setDescription("Price is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Price is mendatory");
				return resp;
			}
			
			//Integer custId = 0;
			Integer prodId = 0;
			Integer invId = 0;
			Integer qty = 0;
			try {
				invId = Integer.parseInt(invIdStr);
				//custId = Integer.parseInt(custIdStr);
				prodId = Integer.parseInt(prodIdStr);
				qty = Integer.parseInt(qtyStr);
				//price = Double.parseDouble(priceStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid product details");
				return resp;
			}
			
			//Customer cust  = DAORegistry.getCustomerDAO().getCustomerByEmail(custIdStr);
			Customer cust  = DAORegistry.getCustomerDAO().getCustomerByUserId(custIdStr);
			if(cust == null){
				error.setDescription("Invalid customer details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid customer details.");
				return resp;
			}
			
			
			SellerProductsItems product = EcommDAORegistry.getSellerProductsItemsDAO().getProductsById(prodId);
			if(product == null){
				error.setDescription("Product not found in system");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product not found in system");
				return resp;
			}
			
			if(!product.getIsVarient()){
				List<SellerProdItemsVariInventory> invs = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getAllInventoryByProdId(product.getSpiId());
				if(invs !=null && invs.size() > 0){
					invId = invs.get(0).getSpivInvId();
				}
			}
			
			CustomerCart cart = EcommDAORegistry.getCustomerCartDAO().getCustomerCartByProdIdInventoryId(prodId,invId, cust.getId());
			if(cart != null){
				error.setDescription("Product inventory already in cart");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product inventory already in cart");
				return resp;
			}
			
			cart = new CustomerCart();
			cart.setCustId(cust.getId());
			cart.setIsLoyPntUsed(false);
			cart.setIsPlacedOrder(false);
			cart.setQty(qty);
			cart.setIsRwdPointProd(product.getIsRwdPointProd());
			cart.setReqRwdPoint(product.getReqRwdPoint());
			cart.setSellerId(product.getSellerId());
			cart.setSelPrice(product.getPsMinPrice());
			cart.setSpiId(prodId);
			cart.setSpivInvId(invId);
			cart.setStatus("ACTIVE");
			cart.setUpdBy("SAAS");
			cart.setUpdDate(new Date());
			
			EcommDAORegistry.getCustomerCartDAO().save(cart);
			resp.setStatus(1);
			resp.setMessage("Cart data saved successfully.");
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while saving customer cart");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while saving customer cart");
			return resp;
		}
	}
	
	
	
	
	
	
	@RequestMapping(value="/deleteCart",method=RequestMethod.POST)
	public @ResponsePayload GenericResp deleteCart(HttpServletRequest request,HttpServletResponse response){
		GenericResp resp =new GenericResp();
		Error error = new Error();
		String custIdStr = request.getParameter("custId");
		String prodIdStr = request.getParameter("prodId");
		System.out.println("REMOVE CART CALLED FROM SAAS :    custId = "+custIdStr+"  |    prodID = "+prodIdStr);
		
		try {
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Customer id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(prodIdStr)){
				error.setDescription("Product id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product id is mendatory");
				return resp;
			}
			
			//Integer custId = 0;
			Integer prodId = 0;
			try {
				//custId = Integer.parseInt(custIdStr);
				prodId = Integer.parseInt(prodIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid product details");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid product details");
				return resp;
			}
			
			//Customer cust  = DAORegistry.getCustomerDAO().getCustomerByEmail(custIdStr);
			Customer cust  = DAORegistry.getCustomerDAO().getCustomerByUserId(custIdStr);
			if(cust == null){
				error.setDescription("Invalid customer details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid customer details.");
				return resp;
			}
			
			
			SellerProductsItems product = EcommDAORegistry.getSellerProductsItemsDAO().getProductsById(prodId);
			if(product == null){
				error.setDescription("Product not found in system");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product not found in system");
				return resp;
			}
			Integer invId = -1;
			if(!product.getIsVarient()){
				List<SellerProdItemsVariInventory> invs = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getAllInventoryByProdId(product.getSpiId());
				if(invs !=null && invs.size() > 0){
					invId = invs.get(0).getSpivInvId();
				}
			}
			
			CustomerCart cart = EcommDAORegistry.getCustomerCartDAO().getCustomerCartByProdIdInventoryId(prodId,invId, cust.getId());
			if(cart != null){
				EcommDAORegistry.getCustomerCartDAO().delete(cart);
				resp.setStatus(1);
				resp.setMessage("Cart data saved successfully.");
				return resp;
			}
			error.setDescription("Product inventory already in cart");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product inventory already in cart");
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while deleting customer cart");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while deleting customer cart");
			return resp;
		}
	}
	
	
	
	@RequestMapping(value="/SaveCustCode",method=RequestMethod.POST)
	public @ResponsePayload GenericResp saveCustCode(HttpServletRequest request,HttpServletResponse response){
		GenericResp resp =new GenericResp();
		Error error = new Error();
		String custIdStr = request.getParameter("custId");
		String code = request.getParameter("code");
		String coIdStr = request.getParameter("coId");
		String qNoStr = request.getParameter("qNo");
		System.out.println("ADD CODE CALLED FROM SAAS :    custId = "+custIdStr+"  |    code = "+code+"   |   qNo = "+qNoStr);
		try {
			if(TextUtil.isEmptyOrNull(custIdStr)){
				error.setDescription("Customer id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(code)){
				error.setDescription("Product id is mendatory");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Product id is mendatory");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(coIdStr)){
				error.setDescription("Invalid contest id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid contest id");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(qNoStr)){
				error.setDescription("Invalid question no.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid question no.");
				return resp;
			}
			
			Integer qNo = 0;
			try {
				qNo = Integer.parseInt(qNoStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid question no.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid question no.");
				return resp;
			}
			
			//Customer cust  = DAORegistry.getCustomerDAO().getCustomerByEmail(custIdStr);
			Customer cust  = DAORegistry.getCustomerDAO().getCustomerByUserId(custIdStr);
			if(cust == null){
				error.setDescription("Invalid customer details.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Invalid customer details.");
				return resp;
			}
			
			
			CustCoupenCode custCode = EcommDAORegistry.getCustomerCoupenCodeDAO().getActiveCustomerCoupenCodesByCustIdAndCode(cust.getId(), code);
			/*if(custCode != null){
				error.setDescription("Customer coupon code already saved");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer coupon code already saved");
				return resp;
			}*/
			
			CoupenCode cCode  = EcommDAORegistry.getCoupenCodeDAO().getContestCodesByCode(code.trim());
			if(cCode == null){
				error.setDescription("Customer coupon code does not exist in RTF.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Customer coupon code does not exist in RTF.");
				return resp;
			}
			
			//custCode = EcommDAORegistry.getCustomerCoupenCodeDAO().getActiveCustomerCoupenCodesByCustIdCodeAndCoId(cust.getId(), coIdStr);
			if(custCode == null){
				custCode = new CustCoupenCode();
				custCode.setCrDate(new Date());
			}
			custCode.setCoId(coIdStr);
			custCode.setCpncde(code);
			custCode.setCustId(cust.getId());
			custCode.setDisc(cCode.getCpnDisc());
			custCode.setDiscType(cCode.getDiscType());
			custCode.setExpDate(cCode.getExpDate());
			custCode.setqNo(qNo);
			custCode.setStatus(cCode.getStatus());
			EcommDAORegistry.getCustomerCoupenCodeDAO().saveOrUpdate(custCode);
			resp.setStatus(1);
			resp.setMessage("Customer coupon code saved successfully.");
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Customer coupon code saved successfully.");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.SAVECUSTCART,"Error occured while saving customer cart");
			return resp;
		}
	}
	
	@RequestMapping(value="/livtWinnerUpload",method=RequestMethod.POST)
	public @ResponsePayload GenericResp livtWinnerUpload(HttpServletRequest request,HttpServletResponse response){
		GenericResp resp =new GenericResp();
		Error error = new Error();
		String winnersStr = request.getParameter("winners");
		
		try {
			if(TextUtil.isEmptyOrNull(winnersStr)){
				error.setDescription("Invalid Winner Upload Data");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFLIVTWINNERS,"Invalid Winner Upload Data");
				return resp;
			}
			LivtWinnerUploadDTO winnerUploadObj = null;
			try {
			
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(winnersStr, JsonObject.class);
				winnerUploadObj = gson.fromJson(((JsonObject) jsonObject), LivtWinnerUploadDTO.class);
				System.out.println("winner upload jsonObject : "+jsonObject.get("custList")+" : "+winnerUploadObj+ " :: "+winnersStr);
			} catch(Exception e) {
				error.setDescription("Invalid Winner Upload Data");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFLIVTWINNERS,"Invalid Winner Upload Data");
				return resp;
			}
			if(winnerUploadObj == null) {
				error.setDescription("Invalid Winner Upload Data");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFLIVTWINNERS,"Invalid Winner Upload Data");
				return resp;
			}
			System.out.println("winner upload contest start date : "+" : "+winnerUploadObj.getConStartDat()+ " :: "+winnersStr);
			
			SQLDaoUtil.uploadContestWinnerData(winnerUploadObj);			
			System.out.println("Winenrs Upload Size : "+winnerUploadObj.getCoId()+" : "+winnerUploadObj.getCustList().size());
			
			SQLDaoUtil.updateLivtHallOfFameData();
			
			String resMsg = "";
			CommonRespInfo commonRespInfo = null;
			List<RtfConfigContestClusterNodes> clusterNodeList = URLUtil.rtfContestClusterNodesList;
			/*if(URLUtil.isProductionEnvironment) {
				clusterNodeList.add(new RtfConfigContestClusterNodes(0,"http://10.0.0.34:8081/rtfcontest/",1));
				clusterNodeList.add(new RtfConfigContestClusterNodes(0,"http://10.0.0.34:8082/rtfcontest/",1));
			}*/
			try {
				Map<String, String> map = URLUtil.getParameterMap();
				//map.put("coId", ""+contestId);
				for (RtfConfigContestClusterNodes rtfContestNodes : clusterNodeList) {
					try {
						String data = URLUtil.getObject(map, rtfContestNodes.getUrl()+"RefreshHallOfFameData.json");
						String url =rtfContestNodes.getUrl()+"RefreshHallOfFameData.json";
						System.out.println("HALL OF FAME UPDATE Res : "+url +" : "+data);
						ObjectMapper objectMapper = new ObjectMapper();
						commonRespInfo = objectMapper.readValue(data, CommonRespInfo.class);
						if(commonRespInfo.getSts() != 1){
							if(commonRespInfo.getErr() != null) {
								String msg = rtfContestNodes.getUrl()+":"+commonRespInfo.getErr().getDesc(); 
								resMsg = resMsg + msg + ",";
								System.out.println("HALL OF FAME UPDATE ERROR : "+msg +" : "+data);
							}
						}
						System.out.println("Hall Of Fame Data Update Call : "+rtfContestNodes.getUrl()+" : "+ new Date());
					} catch (Exception e) {
						e. printStackTrace();
					}
				}
			} catch (Exception e) {
				error.setDescription("Error while calling rtflivt halloffame refresh");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFLIVTWINNERS,"Error while calling rtflivt halloffame refresh");
				return resp;
			}
			
			
			if(resMsg.equals("")) {
				resMsg = "Success";
			}
			System.out.println("Hall Of Fame Data Update Call Final :"+resMsg); 
			resp.setStatus(1);
			resp.setMessage(resMsg);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Uploading winners");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFLIVTWINNERS,"Error occured while Uploading winners");
			return resp;
		}
	}
	
	@RequestMapping(value="/livtCustRewardUpload",method=RequestMethod.POST)
	public @ResponsePayload GenericResp livtCustRewardUpload(HttpServletRequest request,HttpServletResponse response){
		GenericResp resp =new GenericResp();
		Error error = new Error();
		String custRewdStr = request.getParameter("custRwds");
		
		try {
			if(TextUtil.isEmptyOrNull(custRewdStr)){
				error.setDescription("Invalid Customer Reward Data");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFCUSTRWDS,"Invalid Customer Reward Data");
				return resp;
			}
			GenRewardDTO rewardDto = null;
			try {
			
				Gson gson = GsonCustomConfig.getGsonBuilder();
				JsonObject jsonObject = gson.fromJson(custRewdStr, JsonObject.class);
				rewardDto = gson.fromJson(((JsonObject) jsonObject), GenRewardDTO.class);
				System.out.println("winner upload jsonObject : "+jsonObject.get("custList")+" : "+custRewdStr+ " :: "+custRewdStr);
			} catch(Exception e) {
				error.setDescription("Invalid Winner Upload Data");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFCUSTRWDS,"Invalid Winner Upload Data");
				return resp;
			}
			if(rewardDto == null) {
				error.setDescription("Invalid Customer Reward Data");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFCUSTRWDS,"Invalid Customer Reward Data");
				return resp;
			}
			List<GenRewardDVO> list = rewardDto.getLst();
			if(list == null || list.size() <= 0) {
				error.setDescription("Invalid Customer Reward Data");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFCUSTRWDS,"Invalid Customer Reward Data");
				return resp;
			}
			//List<GenRewardDVO> rewardList = new ArrayList<GenRewardDVO>();
			try {
				for (GenRewardDVO genRewardDVO : list) {
					try {
						genRewardDVO.setCustomerId(Integer.valueOf(genRewardDVO.getCuId()));
						//rewardList.add(genRewardDVO);
					} catch(Exception e) {
					}
				}
			} catch(Exception e) {
			}
			
			int proceddSize = CustRewardUploadUtil.uploadCustRewardstoRtf(rewardDto);		
			System.out.println("Customer Reward Upload Size : "+rewardDto.getCoId()+" :paramsize: "+rewardDto.getLst().size()+" :procedsize: "+proceddSize+" :"+new Date());
			
			resp.setStatus(1);
			resp.setMessage("Success");
			
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Uploading Customer Reward");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.UPLOADRTFCUSTRWDS,"Error occured while Uploading Customer Reward");
			return resp;
		}
	}
	@RequestMapping(value="/submitRefundRequestForm",method=RequestMethod.POST)
	public @ResponsePayload GenericResp submitRefundForm(HttpServletRequest request,HttpServletResponse response){
		GenericResp resp =new GenericResp();
		Error error = new Error();
		String custIdStr = request.getParameter("custId");
		String orderNoStr = request.getParameter("orderNo");
		String orderDateStr = request.getParameter("orderDate");
		String refundReason = request.getParameter("msg");
		
		try {
			Integer customerId = null;
			try {
				customerId = Integer.parseInt(custIdStr);
			} catch(Exception e) {
				error.setDescription("Invalid Customer Id");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Invalid Customer Id");
				return resp;
			}
			Integer orderId = null;
			try {
				orderId = Integer.parseInt(orderNoStr);
			} catch(Exception e) {
				error.setDescription("Invalid Order No");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Invalid Order No");
				return resp;
			}
			Date orderDate = null;
			try {
				orderDate = dateTimeFormat1.parse(orderDateStr);
			} catch(Exception e) {
				error.setDescription("Invalid Order Date");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Invalid Order Date");
				return resp;
			}
			if(TextUtil.isEmptyOrNull(refundReason)){
				error.setDescription("Enter Valid Refund Reason");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Enter Valid Refund Reason");
				return resp;
			}
			if(refundReason.length() > 2400) {
				error.setDescription("Enter refund reson in 2400 letters.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Enter refund reson in 2400 letters.");
				return resp;
			}
			CustomerOrder custOrder = EcommDAORegistry.getCustomerProdOrderDAO().getCustomerOrderByOrderIdAndCustomerId(orderId, customerId);
			if(custOrder == null) {
				error.setDescription("Order Id Not Exist.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Order Id Not Exist.");
				return resp;
			}
			OrderRefundRequestForm refundform = EcommDAORegistry.getOrderRefundRequestFormDAO().getOrderRefundRequestFormByOrderId(orderId);
			if(refundform != null) {
				error.setDescription("Refund Already Requested for this Order.");
				resp.setError(error);
				resp.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Refund Already Requested for this Order.");
				return resp;
			}
			refundform = new OrderRefundRequestForm();
			refundform.setCustId(customerId);
			refundform.setOrderId(orderId);
			refundform.setOrderDate(orderDate);
			refundform.setRefundRemarks(refundReason);
			refundform.setRfndRqstDate(new Date());
			refundform.setStatus("ACTIVE");
			refundform.setIsEmailed(false);
			EcommDAORegistry.getOrderRefundRequestFormDAO().save(refundform);
			
			resp.setStatus(1);
			resp.setMessage("Refund Request Submitted Successfully.");
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while submitting refund request form");
			resp.setError(error);
			resp.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.REFUNDREQUEST,"Error occured while submitting refund request form");
			return resp;
		}
	}
	@RequestMapping(value="/OrderProdRefund",method=RequestMethod.POST)
	public RtfOrderRefundResponse orderProdRefund(HttpServletRequest request,HttpServletResponse reponse){
		RtfOrderRefundResponse response = new RtfOrderRefundResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			/*if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						response.setError(error);
						response.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFORDERREFUND,"You are not authorized");
						return response;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					response.setError(error);
					response.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFORDERREFUND,"You are not authorized");
					return response;
				}
			}else{
				error.setDescription("You are not authorized.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFORDERREFUND,"You are not authorized");
				return response;
			}*/
			
			String orderIdStr = request.getParameter("orderId");
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("OrderId is mandatory.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"OrderId is mandatory.");
				return response;
			}
			Integer orderId = null;
			try {
				orderId = Integer.parseInt(orderIdStr);
			} catch(Exception e) {
				error.setDescription("OrderId is Invalid.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"OrderId is Invalid.");
				return response;
			}
			String orderProdIdStr = request.getParameter("orderProdId");
			if(TextUtil.isEmptyOrNull(orderProdIdStr)){
				error.setDescription("OrderId Product Id is mandatory.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"OrderId Product Id is mandatory.");
				return response;
			}
			Integer orderProdId = null;
			try {
				orderProdId = Integer.parseInt(orderProdIdStr);
			} catch(Exception e) {
				error.setDescription("OrderId Product Id is Invalid.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"OrderId Product Id is Invalid.");
				return response;
			}
			CustomerOrder order = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
			if(order == null) {
				error.setDescription("OrderId is Invalid.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"OrderId is Invalid.");
				return response;
			}
			
			OrderProducts orderProduct = EcommDAORegistry.getOrderProductSetDAO().get(orderProdId);
			if(orderProduct == null || orderProduct.getStatus().equals("VOIDED") ) {
				error.setDescription("Order PRoduct already voided");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"Order PRoduct already voided");
				return response;
			}
			
			

			String refundType = request.getParameter("refundType");
			if(TextUtil.isEmptyOrNull(refundType)){
				error.setDescription("Refund Type is mandatory.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"Refund Type is mandatory.");
				return response;
			}
			
			String refundAmount = request.getParameter("refundAmount");
			if(TextUtil.isEmptyOrNull(refundAmount)){
				error.setDescription("Refund Amount is mandatory.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFORDERREFUND,"Refund Amount is mandatory.");
				return response;
			}
			
			if(refundType.equals(PaymentMethod.CREDITCARD.toString()) || refundType.equals(PaymentMethod.GOOGLEPAY.toString())
					|| order.getpPayType().equals(PaymentMethod.IPAY.toString())){
			//if(refundType.equals(PaymentMethod.CREDITCARD.toString())) {
				response = createStripRefund(orderId, refundAmount, refundType, request,orderProdId);
			}
			if(response.getRtfORderRefund() != null ) {
				RtfOrderRefund refundObj = response.getRtfORderRefund();
				
				Integer spendPoints = EcommUtil.getIntegerValueRoundedUp(orderProduct.getfSelPrice());
				
				refundObj.setFinalSellPrice(orderProduct.getfSelPrice());
				refundObj.setRefundLoyaltyPoints(orderProduct.getUsedLPnts());
				refundObj.setDeductOrderEarnedPoints(spendPoints);
				refundObj.setOrderQty(orderProduct.getQty());
				refundObj.setUsedLoyaltyPoints(orderProduct.getUsedLPnts());

				EcommDAORegistry.getRtfOrderRefundDAO().save(refundObj);
				
				Customer cust = DAORegistry.getCustomerDAO().get(order.getCustId());
				
				CustomerLoyaltyPointTranx loyaltyPointTrax = new CustomerLoyaltyPointTranx();
				loyaltyPointTrax.setCrBy(cust.getUserId());
				loyaltyPointTrax.setCrDate(new Date());
				loyaltyPointTrax.setCustId(order.getCustId());
				loyaltyPointTrax.setEarnPoints(orderProduct.getUsedLPnts());
				loyaltyPointTrax.setSpentPoints(spendPoints);
				loyaltyPointTrax.setBeforePoints(cust.getLoyaltyPoints());
				loyaltyPointTrax.setAfterPoints((cust.getLoyaltyPoints()+orderProduct.getUsedLPnts())-spendPoints);
				loyaltyPointTrax.setStatus("PENDING");
				loyaltyPointTrax.setOrderId(order.getId());
				loyaltyPointTrax.setTranxType("PROD_ORDER_REFUND");
				
				EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
				cust.setLoyaltyPoints((cust.getLoyaltyPoints()+orderProduct.getUsedLPnts())-spendPoints);
				
				EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
				DAORegistry.getCustomerDAO().update(cust);
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(cust.getId(), cust.getLoyaltyPoints());
					System.out.println("UPDATED ORDER POINT IN CASSANDRA : "+cust.getLoyaltyPoints());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			return response;
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while doing paypal Refund");
			response.setError(error);
			response.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFORDERREFUND,"Error occured while doing paypal Refund");
			return response;
		}
	}
	public RtfOrderRefundResponse createStripRefund(Integer orderId,String refundAmount,String refundType,HttpServletRequest request,Integer orderProdId){
		Error error = new Error();
		RtfOrderRefundResponse rtfOrderRefundResponse = new RtfOrderRefundResponse();
		try {			
			CustomerOrder order = EcommDAORegistry.getCustomerProdOrderDAO().getACustomerOrderByOrderId(orderId);
			if(order==null){
				error.setDescription("Order not found.");
				rtfOrderRefundResponse.setError(error);
				rtfOrderRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ECOMSTRIPEREFUND,"Order not found.");
				return rtfOrderRefundResponse;
			}
			
			String charge = "";
			if((order.getpPayType().equals(PaymentMethod.CREDITCARD.toString()) || order.getpPayType().equals(PaymentMethod.GOOGLEPAY.toString()) 
					|| order.getpPayType().equals(PaymentMethod.IPAY.toString())) && order.getpTrnxId()!=null
					&& !order.getpTrnxId().isEmpty()){
				charge = order.getpTrnxId();
			}
			
			if(TextUtil.isEmptyOrNull(charge)){
				error.setDescription("No Creditcard transaction found on given order.");
				rtfOrderRefundResponse.setError(error);
				rtfOrderRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ECOMSTRIPEREFUND,"No Creditcard transaction found on given order.");
				return rtfOrderRefundResponse;
			}
			
			Map<String, Object> refundParams = new HashMap<String, Object>();
			refundParams.put("charge", charge);
			
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			Stripe.apiKey = key;
			Refund refund = null;
			
			Double amount = Double.parseDouble(refundAmount);
			amount  = TicketUtil.getRoundedValue(amount);
			amount  = amount*100;
			refundParams.put("amount", amount.intValue());
			refund = Refund.create(refundParams);
			
			RtfOrderRefund stripeRefund = null;
			if(refund!=null && refund.getStatus().equals("succeeded")){
				stripeRefund = new RtfOrderRefund();
				Double amt = (refund.getAmount()/100.00);
				stripeRefund.setOrderId(order.getId());
				stripeRefund.setRefundAmount(amt);
				stripeRefund.setRemarks(refund.getReason());
				stripeRefund.setRefundId(refund.getId());
				stripeRefund.setStatus(refund.getStatus());
				stripeRefund.setTransactionId(refund.getCharge());
				stripeRefund.setRefundType(order.getpPayType());
				stripeRefund.setRefundDate(new Date(TimeUnit.SECONDS.toMillis(refund.getCreated())));
				stripeRefund.setOrderProdId(orderProdId);
			}
			rtfOrderRefundResponse.setRtfORderRefund(stripeRefund);
			rtfOrderRefundResponse.setStatus(1);
			return rtfOrderRefundResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting Stripe refunds");
			rtfOrderRefundResponse.setError(error);
			rtfOrderRefundResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.ECOMSTRIPEREFUND,"Error occured while gettting Stripe refunds");
			return rtfOrderRefundResponse;
		}
	}
	
	
	
	
	
	@RequestMapping(value="/SendOrderDelEm",method=RequestMethod.GET)
	public GenericResp sendOrderDelEm(HttpServletRequest request,HttpServletResponse reponse){
		GenericResp response = new GenericResp();
		Error error = new Error();
		try {
			RTFEcommEmailScheduler email = new RTFEcommEmailScheduler();
			email.sendOrderDeliveredInfoEmails();
			response.setMessage("EMAIL SENT");
			response.setStatus(1);
			return response;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending email.");
			response.setError(error);
			response.setStatus(0);
			return response;
		}
	}
	
	@RequestMapping(value="/SendTestEm",method=RequestMethod.GET)
	public GenericResp sendTestEmail(HttpServletRequest request,HttpServletResponse reponse){
		GenericResp response = new GenericResp();
		Error error = new Error();
		try {
			String custIdStr  =request.getParameter("custId");
			String template = request.getParameter("temp");
			if(custIdStr == null || custIdStr.isEmpty()){
				error.setDescription("Invalid Customer ID");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			if(template == null || template.isEmpty()){
				error.setDescription("Invalid template");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			
			Integer custId = 0;
			try {
				custId = Integer.parseInt(custIdStr);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Invalid Customer ID..");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			Customer cust = DAORegistry.getCustomerDAO().get(custId);
			if(cust == null){
				error.setDescription("Custmer not found.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			RTFEcommEmailScheduler email = new RTFEcommEmailScheduler();
			email.testEmail(cust,template);
			response.setMessage("EMAIL SENT");
			response.setStatus(1);
			return response;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending email.");
			response.setError(error);
			response.setStatus(0);
			return response;
		}
	}
	
	
	
	
	@RequestMapping(value="/sendRTFLIVEMail",method=RequestMethod.POST)
	public GenericResp sendRTFLIVEMail(HttpServletRequest request,HttpServletResponse reponse){
		GenericResp response = new GenericResp();
		Error error = new Error();
		try {
			String firstName  =request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String compName = request.getParameter("compName");
			String msg = request.getParameter("msg");
			String compURL = request.getParameter("compURL");
			if(firstName == null || firstName.isEmpty()){
				error.setDescription("First name is mendatory.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			if(lastName == null || lastName.isEmpty()){
				error.setDescription("Last name is mendatory.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			if(email == null || email.isEmpty()){
				error.setDescription("Email is mendatory.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			if(phone == null || phone.isEmpty()){
				error.setDescription("Phone is mendatory.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			if(compName == null || compName.isEmpty()){
				error.setDescription("Company name is mendatory.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			if(msg == null || msg.isEmpty()){
				error.setDescription("Message is mendatory.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			if(compURL == null || compURL.isEmpty()){
				error.setDescription("Company URL is mendatory.");
				response.setError(error);
				response.setStatus(0);
				return response;
			}
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("fName",firstName);
			mailMap.put("lName",lastName);
			mailMap.put("email",email);
			mailMap.put("phone",phone);
			mailMap.put("compName",compName);
			mailMap.put("compURL",compURL);
			mailMap.put("msg",msg);
			
			MailAttachment[] mailAttachment = Util.getEmailAttachmentRTFLIVEContactUS();
			mailManager.sendRTFLIVEMailNow(true, "info@rtflive.com", email, null, null, "RTF Live Contact us", "rtf-live-contact.html", mailMap, "text/html", null, mailAttachment, null);
			mailManager.sendRTFLIVEMailNow(true, "info@rtflive.com", "info@rtflive.com", null, "mitulsanghani.mca@gmail.com,amit.raut@rtflive.com,info@rtflive.com", "RTF Live Contact us", "rtf-live-contact_details.html", mailMap, "text/html", null, mailAttachment, null);
			//mailManager.sendMailUsingGMAILSMTP("smtp.gmail.com",465,true,"info@rtflive.com","Zonezata#1441","info@rtflive.com",null,email,null,null, "RTF Live Contact us","rtf-live-contact.html",mailMap, "text/html", custAttachment,mailAttachment,null);
			response.setMessage("EMAIL SENT");
			response.setStatus(1);
			return response;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending email.");
			response.setError(error);
			response.setStatus(0);
			return response;
		}
	}
	
	
}
