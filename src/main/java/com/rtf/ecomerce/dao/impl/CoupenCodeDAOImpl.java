package com.rtf.ecomerce.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.rtf.ecomerce.dao.service.CoupenCodeDAO;
import com.rtf.ecomerce.data.CoupenCode;

public class CoupenCodeDAOImpl extends HibernateDAO<Integer, CoupenCode> implements CoupenCodeDAO{

	public List<CoupenCode> getAllProductCodesByIds(List<Integer> ids){
		Session session = getSession();
		Criteria cri =session.createCriteria(CoupenCode.class);
		List<CoupenCode> list = null;
		try {
			cri.add(Restrictions.eq("status", "ACTIVE"));
			if(ids.isEmpty()){
				cri.add(Restrictions.eq("ccId", -1111));
			}else{
				cri.add(Restrictions.in("ccId", ids));
			}
			cri.add(Restrictions.eq("couType", "PUBLIC"));
			list = cri.list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return list;
	}
	
	
	public CoupenCode getProductCodesByCode(String code){
		Session session = getSession();
		Criteria cri = session.createCriteria(CoupenCode.class);
		CoupenCode cCode = null;
		try {
			cri.add(Restrictions.eq("status", "ACTIVE"));
			cri.add(Restrictions.eq("cpnCode", code));
			cri.add(Restrictions.eq("couType", "PUBLIC"));
			cCode = (CoupenCode) cri.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return cCode;
	}
	
	
	
	
	public CoupenCode getContestCodesByCode(String code){
		Session session = getSession();
		Criteria cri = session.createCriteria(CoupenCode.class);
		CoupenCode cCode = null;
		try {
			cri.add(Restrictions.eq("status", "ACTIVE"));
			cri.add(Restrictions.eq("cpnCode", code));
			cri.add(Restrictions.eq("couType", "CONTEST"));
			cCode = (CoupenCode) cri.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return cCode;
	}
	
	
	
	public List<CoupenCode> getDiscountCodeToExpire(Date today){
		return find("FROM CoupenCode WHERE status=? AND expDate < ?",new Object[]{"ACTIVE",today});
	}
}
