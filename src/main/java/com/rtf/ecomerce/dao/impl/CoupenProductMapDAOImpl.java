package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.dao.service.CoupenProductMapDAO;
import com.rtf.ecomerce.data.CoupenProductMap;

public class CoupenProductMapDAOImpl extends HibernateDAO<Integer, CoupenProductMap> implements CoupenProductMapDAO{

	@Override
	public List<CoupenProductMap> getAllProductCodeMap(Integer prodId) {
		return find("FROM CoupenProductMap WHERE spiId=?",new Object[]{prodId});
	}

}


