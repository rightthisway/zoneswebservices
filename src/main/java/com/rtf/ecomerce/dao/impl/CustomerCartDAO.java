package com.rtf.ecomerce.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.rtf.ecomerce.data.CustomerCart;

public class CustomerCartDAO extends HibernateDAO<Integer, CustomerCart> implements com.rtf.ecomerce.dao.service.CustomerCartDAO{

	public CustomerCart getCustomerCartByInventoryId(Integer prodId,Integer inventoryId,Integer custId) throws Exception {
		return findSingle("FROM CustomerCart WHERE spiId=? AND spivInvId=? AND custId=?",new Object[]{prodId,inventoryId,custId});
	}
	public List<CustomerCart> getAllCustomerCartByCustomerId(Integer customerId) throws Exception {
		return find("FROM CustomerCart WHERE custId=? order by updDate desc",new Object[]{customerId});
	}
	
	public List<CustomerCart> getAllCustomerCartByProductId(Integer prodId) throws Exception {
		return find("FROM CustomerCart WHERE spiId=? order by updDate desc",new Object[]{prodId});
	}
	
	@Override
	public List<CustomerCart> getAllCustomerCartByInvIds(Integer custId, List<Integer> invIds) {
		Session session = getSession();
		Criteria cri = session.createCriteria(CustomerCart.class);
		List<CustomerCart>  list = null;
		try {
			cri.add(Restrictions.eq("custId", custId));
			if(invIds.isEmpty()){
				cri.add(Restrictions.eq("spivInvId", -1111));
			}else{
				cri.add(Restrictions.in("spivInvId", invIds));
			}
			cri.add(Restrictions.eq("status", "ACTIVE"));
			list = cri.list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	@Override
	public Integer removeAllCustomerCartByPoductId(Integer prodId)  throws Exception{
		Session session = getSession();
		Integer count =  0;
		Query query= session.createSQLQuery("DELETE FROM rtf_customer_cart WHERE slrpitms_id="+prodId);
		try {
			count = query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(session.isOpen()){
				session.close();
			}
		}
		return count;
	}
	
	
	public CustomerCart getCustomerCartByProdIdInventoryId(Integer prodId,Integer inventoryId,Integer custId)  {
		return findSingle("FROM CustomerCart WHERE spivInvId=? AND spiId=? AND custId=?",new Object[]{inventoryId,prodId,custId});
	}
	@Override
	public List<CustomerCart> getAllCustomerCartToEmail(Date date) throws Exception {
		return find("FROM CustomerCart WHERE updDate<? AND lastNotified is null",new Object[]{date});
	}

}
