package com.rtf.ecomerce.dao.impl;

import java.util.Date;
import java.util.List;

import com.rtf.ecomerce.dao.service.SellerProductsItemsDAO;
import com.rtf.ecomerce.data.CustCoupenCode;
import com.rtf.ecomerce.data.SellerProductsItems;

public class CustomerCoupenCodeDAO extends HibernateDAO<Integer, CustCoupenCode> implements com.rtf.ecomerce.dao.service.CustomerCoupenCodeDAO{

	public List<CustCoupenCode> getAllActiveCustomerCoupenCodesByCustId(Integer custId) throws Exception {
		return find("FROM CustCoupenCode WHERE custId=? AND status='ACTIVE' and expDate>?",new Object[]{custId,new Date()});
	}
	
	public CustCoupenCode getActiveCustomerCoupenCodesByCustIdAndCode(Integer custId,String code) throws Exception {
		return findSingle("FROM CustCoupenCode WHERE custId=? AND status='ACTIVE' and cpncde=? and expDate>?",new Object[]{custId,code,new Date()});
	}
	
	public List<CustCoupenCode> getAllActiveCustomerCoupenCodesByCode(String code) {
		return find("FROM CustCoupenCode WHERE status='ACTIVE' and cpncde=?",new Object[]{code});
	}
	
	public CustCoupenCode getActiveCustomerCoupenCodesByCustIdCodeAndCoId(Integer custId,String coId) throws Exception {
		return findSingle("FROM CustCoupenCode WHERE custId=? AND status='ACTIVE' and coId=? ",new Object[]{custId,coId});
	}

}
