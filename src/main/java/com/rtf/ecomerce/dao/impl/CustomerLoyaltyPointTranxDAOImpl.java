package com.rtf.ecomerce.dao.impl;

import com.rtf.ecomerce.dao.service.CustomerLoyaltyPointTranxDAO;
import com.rtf.ecomerce.data.CustomerLoyaltyPointTranx;
import com.rtfquiz.webservices.data.RtfRewardConfig;
import com.zonesws.webservices.enums.SourceType;

public class CustomerLoyaltyPointTranxDAOImpl extends HibernateDAO<Integer, CustomerLoyaltyPointTranx> implements CustomerLoyaltyPointTranxDAO{

	public CustomerLoyaltyPointTranx getCustomerLoyaltyPointTranxByCustIdAndRewardType(Integer customerId,String rewardType) {
		return findSingle("FROM CustomerLoyaltyPointTranx WHERE custId=? and tranxType=? ",new Object[]{customerId,rewardType});
	}
}
