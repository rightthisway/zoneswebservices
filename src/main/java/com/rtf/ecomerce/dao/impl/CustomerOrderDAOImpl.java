package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.dao.service.CustomerOrderDAO;
import com.rtf.ecomerce.data.CustomerOrder;

public class CustomerOrderDAOImpl extends HibernateDAO<Integer, CustomerOrder> implements CustomerOrderDAO{

	
	public List<CustomerOrder> getAllCustomerOrdersByCustId(Integer custId) throws Exception {
		return find("FROM CustomerOrder WHERE custId=? AND status=? order by crDate desc",new Object[]{custId,"ACTIVE"});
	}
	public CustomerOrder getACustomerOrderByOrderId(Integer orderId) throws Exception {
		return findSingle("FROM CustomerOrder WHERE id=? ",new Object[]{orderId});
	}
	public CustomerOrder getCustomerOrderByOrderIdAndCustomerId(Integer orderId,Integer custId) throws Exception {
		return findSingle("FROM CustomerOrder WHERE id=? and custId=? ",new Object[]{orderId,custId});
	}
	
	public List<CustomerOrder> getCustomerOrdersToEmail() throws Exception {
		return find("FROM CustomerOrder WHERE isEmailed=? AND status=? order by crDate desc",new Object[]{false,"ACTIVE"});
	}
	
}
