package com.rtf.ecomerce.dao.impl;

import com.rtf.ecomerce.dao.service.CustomerCartDAO;
import com.rtf.ecomerce.dao.service.CustomerCoupenCodeDAO;
import com.rtf.ecomerce.dao.service.OrderRefundRequestFormDAO;
import com.rtf.ecomerce.dao.service.RtfOrderRefundDAO;

public class EcommDAORegistry {

	
	public static EcommQueryManagerDAO ecommQueryManagerDAO;
	public static SellerDetailsDAOImpl sellerDetailsDAO;
	public static SellerProductsItemsDAOImpl sellerProductsItemsDAO;
	public static CustomerCoupenCodeDAO customerCoupenCodeDAO;
	public static SellerProdItemsVariNameDAOImpl sellerProdItemsVariNameDAO;
	public static SellerProdItemsVariValueDAOImpl sellerProdItemsVariValueDAO;
	public static SellerProdItemsVariInventoryDAOImpl sellerProdItemsVariInventoryDAO;
	public static CustomerCartDAO customerCartDAO;
	public static CustomerLoyaltyPointTranxDAOImpl customerLoyaltyPointTranxDAO;
	public static CustomerOrderDAOImpl customerProdOrderDAO;
	public static OrderProductSetDAOImpl orderProductSetDAO;
	public static CoupenCodeDAOImpl coupenCodeDAO;
	public static CoupenProductMapDAOImpl coupenProductMapDAO;
	public static OrderRefundRequestFormDAO orderRefundRequestFormDAO;
	public static EcommWebServiceTrackingDAOImpl ecommWebServiceTrackingDAO;
	public static RtfOrderRefundDAO rtfOrderRefundDAO;

	public static SellerProductsItemsDAOImpl getSellerProductsItemsDAO() {
		return sellerProductsItemsDAO;
	}

	public final void setSellerProductsItemsDAO(SellerProductsItemsDAOImpl sellerProductsItemsDAO) {
		EcommDAORegistry.sellerProductsItemsDAO = sellerProductsItemsDAO;
	}

	public static CustomerCoupenCodeDAO getCustomerCoupenCodeDAO() {
		return customerCoupenCodeDAO;
	}
	public final void setCustomerCoupenCodeDAO(CustomerCoupenCodeDAO customerCoupenCodeDAO) {
		EcommDAORegistry.customerCoupenCodeDAO = customerCoupenCodeDAO;
	}

	public static SellerProdItemsVariNameDAOImpl getSellerProdItemsVariNameDAO() {
		return sellerProdItemsVariNameDAO;
	}

	public final void setSellerProdItemsVariNameDAO(SellerProdItemsVariNameDAOImpl sellerProdItemsVariNameDAO) {
		EcommDAORegistry.sellerProdItemsVariNameDAO = sellerProdItemsVariNameDAO;
	}

	public static SellerProdItemsVariValueDAOImpl getSellerProdItemsVariValueDAO() {
		return sellerProdItemsVariValueDAO;
	}

	public final void setSellerProdItemsVariValueDAO(SellerProdItemsVariValueDAOImpl sellerProdItemsVariValueDAO) {
		EcommDAORegistry.sellerProdItemsVariValueDAO = sellerProdItemsVariValueDAO;
	}

	public static SellerProdItemsVariInventoryDAOImpl getSellerProdItemsVariInventoryDAO() {
		return sellerProdItemsVariInventoryDAO;
	}

	public final void setSellerProdItemsVariInventoryDAO(
			SellerProdItemsVariInventoryDAOImpl sellerProdItemsVariInventoryDAO) {
		EcommDAORegistry.sellerProdItemsVariInventoryDAO = sellerProdItemsVariInventoryDAO;
	}

	public static EcommQueryManagerDAO getEcommQueryManagerDAO() {
		return ecommQueryManagerDAO;
	}

	public final void setEcommQueryManagerDAO(EcommQueryManagerDAO ecommQueryManagerDAO) {
		EcommDAORegistry.ecommQueryManagerDAO = ecommQueryManagerDAO;
	}

	public static CustomerCartDAO getCustomerCartDAO() {
		return customerCartDAO;
	}

	public final void setCustomerCartDAO(CustomerCartDAO customerCartDAO) {
		EcommDAORegistry.customerCartDAO = customerCartDAO;
	}

	public static CustomerLoyaltyPointTranxDAOImpl getCustomerLoyaltyPointTranxDAO() {
		return customerLoyaltyPointTranxDAO;
	}

	public final void setCustomerLoyaltyPointTranxDAO(CustomerLoyaltyPointTranxDAOImpl customerLoyaltyPointTranxDAO) {
		EcommDAORegistry.customerLoyaltyPointTranxDAO = customerLoyaltyPointTranxDAO;
	}

	public static CustomerOrderDAOImpl getCustomerProdOrderDAO() {
		return customerProdOrderDAO;
	}

	public final void setCustomerProdOrderDAO(CustomerOrderDAOImpl customerProdOrderDAO) {
		EcommDAORegistry.customerProdOrderDAO = customerProdOrderDAO;
	}

	public static OrderProductSetDAOImpl getOrderProductSetDAO() {
		return orderProductSetDAO;
	}
	public final void setOrderProductSetDAO(OrderProductSetDAOImpl orderProductSetDAO) {
		EcommDAORegistry.orderProductSetDAO = orderProductSetDAO;
	}

	public static CoupenCodeDAOImpl getCoupenCodeDAO() {
		return coupenCodeDAO;
	}
	public final void setCoupenCodeDAO(CoupenCodeDAOImpl coupenCodeDAO) {
		EcommDAORegistry.coupenCodeDAO = coupenCodeDAO;
	}

	public static CoupenProductMapDAOImpl getCoupenProductMapDAO() {
		return coupenProductMapDAO;
	}
	public final void setCoupenProductMapDAO(CoupenProductMapDAOImpl coupenProductMapDAO) {
		EcommDAORegistry.coupenProductMapDAO = coupenProductMapDAO;
	}

	public static OrderRefundRequestFormDAO getOrderRefundRequestFormDAO() {
		return orderRefundRequestFormDAO;
	}

	public final void setOrderRefundRequestFormDAO(OrderRefundRequestFormDAO orderRefundRequestFormDAO) {
		EcommDAORegistry.orderRefundRequestFormDAO = orderRefundRequestFormDAO;
	}

	public static EcommWebServiceTrackingDAOImpl getEcommWebServiceTrackingDAO() {
		return ecommWebServiceTrackingDAO;
	}

	public final void setEcommWebServiceTrackingDAO(EcommWebServiceTrackingDAOImpl ecommWebServiceTrackingDAO) {
		EcommDAORegistry.ecommWebServiceTrackingDAO = ecommWebServiceTrackingDAO;
	}

	public static RtfOrderRefundDAO getRtfOrderRefundDAO() {
		return rtfOrderRefundDAO;
	}

	public final void setRtfOrderRefundDAO(RtfOrderRefundDAO rtfOrderRefundDAO) {
		EcommDAORegistry.rtfOrderRefundDAO = rtfOrderRefundDAO;
	}

	public static SellerDetailsDAOImpl getSellerDetailsDAO() {
		return sellerDetailsDAO;
	}

	public final void setSellerDetailsDAO(SellerDetailsDAOImpl sellerDetailsDAO) {
		EcommDAORegistry.sellerDetailsDAO = sellerDetailsDAO;
	}
	
	
	
	
}
