package com.rtf.ecomerce.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;

import com.rtf.ecomerce.data.CoupenCode;
import com.rtf.ecomerce.data.CustCoupenCode;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.pojo.CustomerCartProdDtls;
import com.rtf.ecomerce.util.DateFormatUtil;

public class EcommQueryManagerDAO {

	
	private SessionFactory sessionFactory;
	private static String zonesApiLinkedServer;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public static String getZonesApiLinkedServer() {
		return zonesApiLinkedServer;
	}
	
	public List<CustomerCartProdDtls> getCustCartProductInventoryDetails(Integer custId) throws Exception {
		
		String queryStr = "select cc.crtID as crtId,cc.qty as crtQty,itm.max_cust_qty as custMaxQty," + 
				" inv.slrinv_id as spivInvId,inv.slrpitms_id as spiId,inv.pbrand as pBrand,inv.pname as pName," +
				" inv.pimg as pImg,inv.vopt_name_val_com as optValDtl,inv.preg_prc as regPrice,inv.psel_prc as sellPrice,"
				+ " inv.avl_qty as avlQty,inv.req_lty_pnts as reqLPnts,inv.pprc_aft_lty_pnts as priceAftLPnts,itm.is_variant as isVariant, " + 
				" itm.pstatus as pstatus,itm.pexp_date as pExpDate,cc.is_with_lty_pnts as isLoyPntUsed, itm.is_sold_out as isSoldOut " + 
				" from  rtf_customer_cart  cc " + 
				" left join rtf_seller_product_items_variants_inventory inv on cc.slrinv_id=inv.slrinv_id" + 
				" inner join  rtf_seller_product_items_mstr itm on itm.slrpitms_id=cc.slrpitms_id" + 
				" where cc.cust_id="+custId+" order by cc.crtID desc";
		
		SQLQuery query = null;
		List<CustomerCartProdDtls> cartDtls = null;
		Session querySession = null;
		try {
			querySession = sessionFactory.openSession();
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("crtId",Hibernate.INTEGER);
			query.addScalar("crtQty",Hibernate.INTEGER);
			query.addScalar("custMaxQty",Hibernate.INTEGER);
			query.addScalar("spivInvId",Hibernate.INTEGER);
			query.addScalar("spiId",Hibernate.INTEGER);
			query.addScalar("pBrand",Hibernate.STRING);
			query.addScalar("pName",Hibernate.STRING);
			query.addScalar("pImg",Hibernate.STRING);
			query.addScalar("optValDtl",Hibernate.STRING);
			query.addScalar("regPrice",Hibernate.DOUBLE);
			query.addScalar("sellPrice",Hibernate.DOUBLE);
			query.addScalar("avlQty",Hibernate.INTEGER);
			query.addScalar("reqLPnts",Hibernate.INTEGER);
			query.addScalar("priceAftLPnts",Hibernate.DOUBLE);
			query.addScalar("isLoyPntUsed",Hibernate.BOOLEAN);
			query.addScalar("isSoldOut",Hibernate.BOOLEAN);
			query.addScalar("isVariant",Hibernate.BOOLEAN);
			query.addScalar("pstatus",Hibernate.STRING);
			query.addScalar("pExpDate",Hibernate.TIMESTAMP);
			cartDtls = query.setResultTransformer(Transformers.aliasToBean(CustomerCartProdDtls.class)).list();
			return cartDtls;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
		}
	}
	
	
	
	
	
	public List<CoupenCode> getProductCouponCodes(Integer prodId,String type) throws Exception {
		String queryStr = "select ccm.ccID AS ccId,ccm.cou_code AS cpnCode,ccm.cou_discount AS cpnDisc,ccm.discountype AS discType "+ 
									"from rtf_coupon_code_mstr ccm "+  
									"join rtf_coupon_product_map cpm on ccm.ccID = cpm.ccID "+ 
									"where ccm.cou_type='"+type+"' and cou_status='ACTIVE' and cpm.slrpitms_id="+prodId+" order by ccm.cou_discount desc"; 
		
		SQLQuery query = null;
		List<CoupenCode> codeList = null;
		Session querySession = null;
		try {
			querySession = sessionFactory.openSession();
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("ccId",Hibernate.INTEGER);
			query.addScalar("cpnCode",Hibernate.STRING);
			query.addScalar("cpnDisc",Hibernate.DOUBLE);
			query.addScalar("discType",Hibernate.STRING);
			codeList = query.setResultTransformer(Transformers.aliasToBean(CoupenCode.class)).list();
			return codeList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
		}
	}
	
	
	
	
	
	public CoupenCode getProductCouponCodes(Integer prodId,String type,String code) throws Exception {
		String queryStr = "select ccm.ccID AS ccId,ccm.cou_code AS cpnCode,ccm.cou_discount AS cpnDisc,ccm.discountype AS discType "+ 
									"from rtf_coupon_code_mstr ccm "+  
									"join rtf_coupon_product_map cpm on ccm.ccID = cpm.ccID "+ 
									"where ccm.cou_type='"+type+"' and cou_status='ACTIVE' and cpm.slrpitms_id="+prodId+" and ccm.cou_code='"+code+"' order by ccm.cou_discount desc"; 
		
		SQLQuery query = null;
		List<CoupenCode> codeList = null;
		Session querySession = null;
		try {
			querySession = sessionFactory.openSession();
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("ccId",Hibernate.INTEGER);
			query.addScalar("cpnCode",Hibernate.STRING);
			query.addScalar("cpnDisc",Hibernate.DOUBLE);
			query.addScalar("discType",Hibernate.STRING);
			codeList = query.setResultTransformer(Transformers.aliasToBean(CoupenCode.class)).list();
			if(codeList!=null && !codeList.isEmpty()){
				return codeList.get(0);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
		}
	}
	
	
	
	
	
	public List<CustCoupenCode> getCustomerProductCouponCodes(Integer prodId,String type,Integer custId,String code) throws Exception {
		String queryStr = "select ccm.cccId AS cccId,ccm.cou_code AS cpncde,ccm.cou_discount AS disc,ccm.discountype AS discType "+
									"from rtf_cust_coupon_code_mstr ccm "+
									"join rtf_coupon_code_mstr ccm1 on ccm.cou_code=ccm1.cou_code "+
									"join rtf_coupon_product_map cpm on ccm1.ccID = ccm1.ccID and cpm.cou_code = ccm.cou_code "+ 
									"where ccm1.cou_type='"+type+"' and ccm1.cou_status='ACTIVE' AND ccm.cpnstatus='ACTIVE' and cpm.slrpitms_id="+prodId+
									" AND ccm.custid="+custId;
		
		
		if(code!=null && !code.isEmpty()){
			queryStr += " AND ccm.cou_code ='"+code+"'";
		}
		
		queryStr += " order by ccm1.cou_discount desc";
		SQLQuery query = null;
		List<CustCoupenCode> codeList = null;
		Session querySession = null;
		try {
			querySession = sessionFactory.openSession();
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("cccId",Hibernate.INTEGER);
			query.addScalar("cpncde",Hibernate.STRING);
			query.addScalar("disc",Hibernate.DOUBLE);
			query.addScalar("discType",Hibernate.STRING);
			codeList = query.setResultTransformer(Transformers.aliasToBean(CustCoupenCode.class)).list();
			return codeList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
		}
	}
	
	
	
	
	
	public OrderProducts getCustomerLast24HoursOrdersDetail(Integer custId,Integer invId) throws Exception {
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		String dateStr = DateFormatUtil.getDDMMYYYYHHMMSSString(cal.getTime());
		
		String queryStr = "select co.cust_id as custId,op.slrpitms_id as spiId,op.slrinv_id as spivInvId,sum(op.qty) as qty "+
									"from rtf_customer_order co "+ 
									"join rtf_order_products op on co.id = op.order_id "+
									"where op.order_status <> 'VOIDED' AND co.status='ACTIVE' AND op.slrinv_id="+invId+" AND co.cust_id="+custId+" and co.cr_date > '"+dateStr+"'"+
									" group by co.cust_id,op.slrpitms_id,op.slrinv_id";
		
		SQLQuery query = null;
		List<OrderProducts> list = null;
		Session querySession = null;
		System.out.println("QUERY  : ");
		System.out.println(queryStr);
		try {
			querySession = sessionFactory.openSession();
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("spiId",Hibernate.INTEGER);
			query.addScalar("spivInvId",Hibernate.INTEGER);
			query.addScalar("qty",Hibernate.INTEGER);
			list = query.setResultTransformer(Transformers.aliasToBean(OrderProducts.class)).list();
			if(list!=null && !list.isEmpty()){
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
		}
	}
	
	
	
}
