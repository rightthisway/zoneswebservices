package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.dao.service.OrderProductSetDAO;
import com.rtf.ecomerce.data.OrderProducts;


public class OrderProductSetDAOImpl extends HibernateDAO<Integer, OrderProducts> implements OrderProductSetDAO{

	@Override
	public List<OrderProducts> getOrderProductByOrderId(Integer orderId) throws Exception {
		return find("FROM OrderProducts WHERE orderId=?",new Object[]{orderId});
	}
	
	
	@Override
	public List<OrderProducts> getOrderProductsToEmailShipTrackNo() throws Exception {
		return find("FROM OrderProducts WHERE status!=? AND isTrackNoUpdated=?",new Object[]{"VOIDED",true});
	}
	
	
	@Override
	public List<OrderProducts> getDeliveredOrderProducts() throws Exception {
		return find("FROM OrderProducts WHERE status=? AND isDelEmailSent=?",new Object[]{"FULFILLED",false});
	}
	
	
	@Override
	public OrderProducts getOrderProductByOrderAndProductId(Integer orderId, Integer prodId) throws Exception {
		return findSingle("FROM OrderProducts WHERE status=? AND orderId=? AND id=?",new Object[]{"FULFILLED",orderId,prodId});
	}

}
