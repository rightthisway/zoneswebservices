package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.data.OrderRefundRequestForm;

public class OrderRefundRequestFormDAO extends HibernateDAO<Integer, OrderRefundRequestForm> implements com.rtf.ecomerce.dao.service.OrderRefundRequestFormDAO {

	public OrderRefundRequestForm getOrderRefundRequestFormByOrderId(Integer orderId) throws Exception {
		return findSingle("FROM OrderRefundRequestForm WHERE orderId=? ",new Object[]{orderId});
	}

	@Override
	public List<OrderRefundRequestForm> getAllOrderRefundRequestToEmail() throws Exception {
		return find("FROM OrderRefundRequestForm WHERE  isEmailed=?",new Object[]{false});
	}
}
