package com.rtf.ecomerce.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.rtf.ecomerce.dao.service.SellerProdItemsVariInventoryDAO;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;

public class SellerProdItemsVariInventoryDAOImpl extends HibernateDAO<Integer, SellerProdItemsVariInventory> implements SellerProdItemsVariInventoryDAO{

	@Override
	public SellerProdItemsVariInventory getInventoryByInventoryId(Integer inventoryId) {
		return findSingle(" from SellerProdItemsVariInventory WHERE spivInvId=?",new Object[]{inventoryId});
	}
	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId) {
		return findSingle("SellerProdItemsVariInventory WHERE vvId1=?",new Object[]{opValId});
	}

	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1, Integer opValId2) {
		return findSingle("SellerProdItemsVariInventory WHERE vvId1=? AND vvId2=?",new Object[]{opValId1,opValId2});
	}

	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1, Integer opValId2,Integer opValId3) {
		return findSingle("SellerProdItemsVariInventory WHERE vvId1=? AND vvId2=? AND AND vvId3=?",new Object[]{opValId1,opValId2,opValId3});
	}

	@Override
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1, Integer opValId2, Integer opValId3,Integer opValId4) {
		Session session = getSession();
		Criteria cri = session.createCriteria(SellerProdItemsVariInventory.class);
		SellerProdItemsVariInventory inv = null;
		try {
			cri.add(Restrictions.eq("vvId1", opValId1));
			//cri.add(Restrictions.eq("status", "ACTIVE"));
			if(opValId2!=null && opValId2 > 0){
				cri.add(Restrictions.eq("vvId2", opValId2));
			}
			if(opValId3!=null && opValId3 > 0){
				cri.add(Restrictions.eq("vvId3", opValId3));
			}
			if(opValId4!=null && opValId4 > 0){
				cri.add(Restrictions.eq("vvId4", opValId4));
			}
			inv = (SellerProdItemsVariInventory) cri.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return inv;
		//return findSingle("SellerProdItemsVariInventory WHERE vvId1=? AND vvId2=? AND AND vvId3=? AND vvId4=?",new Object[]{opValId1,opValId2,opValId3,opValId4});
	}
	
	
	
	@Override
	public List<SellerProdItemsVariInventory> getInventoryByIds(List<Integer> ids) {
		Session session = getSession();
		Criteria cri = session.createCriteria(SellerProdItemsVariInventory.class);
		List<SellerProdItemsVariInventory> list = null;
		try {
			if(ids.isEmpty()){
				cri.add(Restrictions.eq("spivInvId", -1111));
			}else{
				cri.add(Restrictions.in("spivInvId", ids));
			}
			//cri.add(Restrictions.eq("status", "ACTIVE"));
			list = cri.list();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	
	
	public SellerProdItemsVariInventory getAvailableInventoryByProdId(Integer prodId){
		SellerProdItemsVariInventory inv = null;
		try {
			List<SellerProdItemsVariInventory> list = null;
			list = find("FROM SellerProdItemsVariInventory WHERE spiId=? AND avlQty > 0 order by sellPrice",new Object[]{prodId});
			if(!list.isEmpty()){
				inv = list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inv;
	}
	
	
	public List<SellerProdItemsVariInventory> getAllInventoryByProdId(Integer prodId){
		List<SellerProdItemsVariInventory> list = null;
		try {
			list = find("FROM SellerProdItemsVariInventory WHERE spiId=? AND status=?",new Object[]{prodId,"ACTIVE"});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
 
}
