package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.dao.service.SellerProdItemsVariNameDAO;
import com.rtf.ecomerce.data.SellerProdItemsVariName;

public class SellerProdItemsVariNameDAOImpl extends HibernateDAO<Integer, SellerProdItemsVariName> implements SellerProdItemsVariNameDAO{

	@Override
	public List<SellerProdItemsVariName> getAllProductVariants(Integer prodId) {
		return find("FROM SellerProdItemsVariName WHERE spiId=?",new Object[]{prodId});
	}

}
