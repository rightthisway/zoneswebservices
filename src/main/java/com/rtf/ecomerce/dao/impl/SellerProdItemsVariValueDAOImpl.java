package com.rtf.ecomerce.dao.impl;

import java.util.List;

import com.rtf.ecomerce.dao.service.SellerProdItemsVariValueDAO;
import com.rtf.ecomerce.data.SellerProdItemsVariValue;

public class SellerProdItemsVariValueDAOImpl extends HibernateDAO<Integer, SellerProdItemsVariValue> implements SellerProdItemsVariValueDAO{

	public List<SellerProdItemsVariValue> getOptionValues(Integer optionId){
		return find("FROM SellerProdItemsVariValue WHERE slrpiVarId=?",new Object[]{optionId});
	}
	
}
