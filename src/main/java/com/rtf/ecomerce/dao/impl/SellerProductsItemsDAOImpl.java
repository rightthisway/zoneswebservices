package com.rtf.ecomerce.dao.impl;

import java.util.Date;
import java.util.List;

import com.rtf.ecomerce.dao.service.SellerProductsItemsDAO;
import com.rtf.ecomerce.data.SellerProductsItems;

public class SellerProductsItemsDAOImpl extends HibernateDAO<Integer, SellerProductsItems> implements SellerProductsItemsDAO{

	@Override
	public List<SellerProductsItems> getProductsByStatus(String status) {
		return find("FROM SellerProductsItems WHERE status=? order by dispOrder",new Object[]{status});
	}

	@Override
	public SellerProductsItems getProductsById(Integer prodId) {
		return findSingle("FROM SellerProductsItems WHERE spiId=? AND status=?" , new Object[]{prodId,"ACTIVE"});
	}
	
	@Override
	public SellerProductsItems getProductById(Integer prodId) {
		return findSingle("FROM SellerProductsItems WHERE spiId=?" , new Object[]{prodId});
	}
	
	
	@Override
	public List<SellerProductsItems> getExpiredProductByDate(Date today,String status) {
		return find("FROM SellerProductsItems WHERE pExpDate<? AND status=? order by dispOrder",new Object[]{today,status});
	}

}
