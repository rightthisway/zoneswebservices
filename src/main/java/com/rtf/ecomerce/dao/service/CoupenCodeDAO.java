package com.rtf.ecomerce.dao.service;

import java.util.Date;
import java.util.List;

import com.rtf.ecomerce.data.CoupenCode;

public interface CoupenCodeDAO extends RootDAO<Integer, CoupenCode>{

	public List<CoupenCode> getAllProductCodesByIds(List<Integer> ids);
	public CoupenCode getProductCodesByCode(String code);
	public CoupenCode getContestCodesByCode(String code);
	public List<CoupenCode> getDiscountCodeToExpire(Date today);
}
