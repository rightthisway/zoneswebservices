package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CoupenProductMap;

public interface CoupenProductMapDAO extends RootDAO<Integer, CoupenProductMap>{

	List<CoupenProductMap> getAllProductCodeMap(Integer prodId);
}
