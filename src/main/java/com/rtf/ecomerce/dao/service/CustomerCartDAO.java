package com.rtf.ecomerce.dao.service;

import java.util.Date;
import java.util.List;

import com.rtf.ecomerce.data.CustomerCart;

public interface CustomerCartDAO extends RootDAO<Integer, CustomerCart>{
	
	public CustomerCart getCustomerCartByInventoryId(Integer prodId,Integer inventoryId,Integer custId) throws Exception;
	public List<CustomerCart> getAllCustomerCartByCustomerId(Integer customerId) throws Exception;
	public List<CustomerCart> getAllCustomerCartByProductId(Integer prodId) throws Exception;
	public List<CustomerCart> getAllCustomerCartByInvIds(Integer custId, List<Integer> invIds);
	public CustomerCart getCustomerCartByProdIdInventoryId(Integer prodId,Integer inventoryId,Integer custId);
	public List<CustomerCart> getAllCustomerCartToEmail(Date date)throws Exception;
	public Integer removeAllCustomerCartByPoductId(Integer prodId)  throws Exception;
	
}
