package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CustCoupenCode;

public interface CustomerCoupenCodeDAO extends RootDAO<Integer, CustCoupenCode>{
	
	public List<CustCoupenCode> getAllActiveCustomerCoupenCodesByCustId(Integer custId) throws Exception ;
	public CustCoupenCode getActiveCustomerCoupenCodesByCustIdAndCode(Integer custId,String code) throws Exception ;
	public List<CustCoupenCode> getAllActiveCustomerCoupenCodesByCode(String code);
	public CustCoupenCode getActiveCustomerCoupenCodesByCustIdCodeAndCoId(Integer custId,String coId) throws Exception ;
}
