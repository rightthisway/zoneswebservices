package com.rtf.ecomerce.dao.service;

import com.rtf.ecomerce.data.CustomerLoyaltyPointTranx;

public interface CustomerLoyaltyPointTranxDAO extends RootDAO<Integer, CustomerLoyaltyPointTranx>{

	public CustomerLoyaltyPointTranx getCustomerLoyaltyPointTranxByCustIdAndRewardType(Integer customerId,String rewardType);
}
