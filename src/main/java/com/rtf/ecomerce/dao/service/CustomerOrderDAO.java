package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CustomerOrder;

public interface CustomerOrderDAO extends RootDAO<Integer, CustomerOrder>{

	public List<CustomerOrder> getAllCustomerOrdersByCustId(Integer custId) throws Exception;
	public CustomerOrder getACustomerOrderByOrderId(Integer orderId) throws Exception;
	public CustomerOrder getCustomerOrderByOrderIdAndCustomerId(Integer orderId,Integer custId) throws Exception;
	public List<CustomerOrder> getCustomerOrdersToEmail() throws Exception;
}
