package com.rtf.ecomerce.dao.service;

import com.rtf.ecomerce.data.EcommWebServiceTracking;

public interface EcommWebServiceTrackingDAO extends RootDAO<Integer, EcommWebServiceTracking>{

}
