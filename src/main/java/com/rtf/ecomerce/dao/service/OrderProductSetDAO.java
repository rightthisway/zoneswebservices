package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.OrderProducts;

public interface OrderProductSetDAO extends RootDAO<Integer, OrderProducts>{

	public List<OrderProducts> getOrderProductByOrderId(Integer orderId) throws Exception ; 
	public List<OrderProducts> getOrderProductsToEmailShipTrackNo() throws Exception ;
	public List<OrderProducts> getDeliveredOrderProducts() throws Exception;
	public OrderProducts getOrderProductByOrderAndProductId(Integer orderId, Integer prodId) throws Exception;
}
