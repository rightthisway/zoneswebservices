package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.OrderRefundRequestForm;

public interface OrderRefundRequestFormDAO extends RootDAO<Integer, OrderRefundRequestForm>{

	public OrderRefundRequestForm getOrderRefundRequestFormByOrderId(Integer orderId) throws Exception;
	public List<OrderRefundRequestForm> getAllOrderRefundRequestToEmail() throws Exception;
}
