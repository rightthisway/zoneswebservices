package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.CoupenCode;
import com.rtf.ecomerce.data.RtfOrderRefund;

public interface RtfOrderRefundDAO extends RootDAO<Integer, RtfOrderRefund>{

}
