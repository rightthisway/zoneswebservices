package com.rtf.ecomerce.dao.service;

import com.rtf.ecomerce.data.SellerDetails;

public interface SellerDetailsDAO extends RootDAO<Integer, SellerDetails>{

}
