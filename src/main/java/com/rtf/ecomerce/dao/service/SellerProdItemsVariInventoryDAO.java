package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariInventory;

public interface SellerProdItemsVariInventoryDAO extends RootDAO<Integer, SellerProdItemsVariInventory>{

	
	public SellerProdItemsVariInventory getInventoryByInventoryId(Integer inventoryId);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1,Integer opValId2);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1,Integer opValId2,Integer opValId3);
	public SellerProdItemsVariInventory getInventoryByOptionValue(Integer opValId1,Integer opValId2,Integer opValId3,Integer opValId4);
	public List<SellerProdItemsVariInventory> getInventoryByIds(List<Integer> ids);
	public SellerProdItemsVariInventory getAvailableInventoryByProdId(Integer prodId);
	public List<SellerProdItemsVariInventory> getAllInventoryByProdId(Integer prodId);
}
