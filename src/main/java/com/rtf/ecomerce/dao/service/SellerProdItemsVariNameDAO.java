package com.rtf.ecomerce.dao.service;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariName;

public interface SellerProdItemsVariNameDAO extends RootDAO<Integer, SellerProdItemsVariName>{

	
	public List<SellerProdItemsVariName> getAllProductVariants(Integer prodId);
}
