package com.rtf.ecomerce.dao.service;

import java.util.Date;
import java.util.List;

import com.rtf.ecomerce.data.SellerProductsItems;

public interface SellerProductsItemsDAO extends RootDAO<Integer, SellerProductsItems>{
	
	public List<SellerProductsItems> getProductsByStatus(String status);
	public SellerProductsItems getProductsById(Integer prodId);
	public List<SellerProductsItems> getExpiredProductByDate(Date today,String status);
	public SellerProductsItems getProductById(Integer prodId);
}
