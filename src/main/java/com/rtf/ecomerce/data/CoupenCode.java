package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.util.EcommUtil;

@Entity
@Table(name = "rtf_coupon_code_mstr")
public class CoupenCode implements Serializable{
	
	private Integer ccId;	
	private String cpnCode;
	private String cpnName;	
	private String cpnDesc;
	private Double cpnDisc;
	private String status;	
	private Date updDate;
	private String updBy;
	private String discType;
	private String couType;
	private Date expDate;
	
	
	private String  updDateStr ;
	private String expDateStr;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ccID")
	public Integer getCcId() {
		return ccId;
	}
	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}
	
	@Column(name = "cou_code")
	public String getCpnCode() {
		return cpnCode;
	}
	public void setCpnCode(String cpnCode) {
		this.cpnCode = cpnCode;
	}
	
	@Column(name = "cou_name")
	public String getCpnName() {
		return cpnName;
	}
	public void setCpnName(String cpnName) {
		this.cpnName = cpnName;
	}
	
	@Column(name = "cou_desc")
	public String getCpnDesc() {
		return cpnDesc;
	}
	public void setCpnDesc(String cpnDesc) {
		this.cpnDesc = cpnDesc;
	}
	
	@Column(name = "cou_discount")
	public Double getCpnDisc() {
		return cpnDisc;
	}
	public void setCpnDisc(Double cpnDisc) {
		this.cpnDisc = cpnDisc;
	}
	
	@Column(name = "cou_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	@Column(name = "discountype")
	public String getDiscType() {
		return discType;
	}
	public void setDiscType(String discType) {
		this.discType = discType;
	}
	@Column(name = "cou_type")
	public String getCouType() {
		return couType;
	}
	public void setCouType(String couType) {
		this.couType = couType;
	}
	
	@Column(name = "exp_date")
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	
	@Transient
	public String getUpdDateStr() {
		return updDateStr;
	}
	public void setUpdDateStr(String updDateStr) {
		this.updDateStr = updDateStr;
	}
	
	
	@Transient
	public String getExpDateStr() {
		if(expDate!=null){
			expDateStr = EcommUtil.formatDateTpMMddyyyyHHmmAA(expDate);
		}
		return expDateStr;
	}
	public void setExpDateStr(String expDateStr) {
		this.expDateStr = expDateStr;
	}
	
	
	

	
}