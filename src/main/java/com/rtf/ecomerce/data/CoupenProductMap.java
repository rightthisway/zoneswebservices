package com.rtf.ecomerce.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rtf_coupon_product_map")
public class CoupenProductMap implements Serializable{
	
	private Integer cpmId;	
	private Integer ccId;
	private Integer spiId;//seller product item id	
	private String cpnCode;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cpmID")
	public Integer getCpmId() {
		return cpmId;
	}
	public void setCpmId(Integer cpmId) {
		this.cpmId = cpmId;
	}
	@Column(name = "ccID")
	public Integer getCcId() {
		return ccId;
	}
	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "cou_code")
	public String getCpnCode() {
		return cpnCode;
	}
	public void setCpnCode(String cpnCode) {
		this.cpnCode = cpnCode;
	}
	
	
}