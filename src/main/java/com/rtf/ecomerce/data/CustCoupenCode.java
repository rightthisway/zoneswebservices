package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.util.DateFormatUtil;
import com.rtf.ecomerce.util.EcommUtil;

@Entity
@Table(name = "rtf_cust_coupon_code_mstr")
public class CustCoupenCode implements Serializable{
	
	private Integer cccId;	
	private Integer custId;
	private String coId;
	private Double disc;
	private String cpncde;	
	private Integer qNo;
	private Date crDate;
	private String status;
	private Date expDate;
	private String discType;
	
	private String  expDatestr ;
	private String discTxt;
	private String expTxt;
	private String discStr;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cccId")
	public Integer getCccId() {
		return cccId;
	}
	public void setCccId(Integer cccId) {
		this.cccId = cccId;
	}
	
	@Column(name = "custid")
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	
	@Column(name = "conid")
	public String getCoId() {
		return coId;
	}
	public void setCoId(String coId) {
		this.coId = coId;
	}
	
	@Column(name = "cou_discount")
	public Double getDisc() {
		return disc;
	}
	public void setDisc(Double disc) {
		this.disc = disc;
	}
	
	@Column(name = "cou_code")
	public String getCpncde() {
		return cpncde;
	}
	public void setCpncde(String cpncde) {
		this.cpncde = cpncde;
	}
	
	@Column(name = "qsnno")
	public Integer getqNo() {
		return qNo;
	}
	public void setqNo(Integer qNo) {
		this.qNo = qNo;
	}
	
	@Column(name = "credate")
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	
	
	@Column(name = "cpnstatus")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "exp_date")
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	@Column(name = "discountype")
	public String getDiscType() {
		return discType;
	}
	public void setDiscType(String discType) {
		this.discType = discType;
	}
	@Transient
	public String getExpDatestr() {
		if(expDate != null) {
			expDatestr = EcommUtil.formatDateTpMMddyyyyHHmmAA(expDate);
		}
		return expDatestr;
	}
	public void setExpDatestr(String expDatestr) {
		this.expDatestr = expDatestr;
	}
	@Transient
	public String getDiscTxt() {
		if(disc != null) {
			if(discType.equalsIgnoreCase("PERCENTAGE")){
				discTxt = "You have earned "+disc.intValue()+"% discount by playing our game to use against in game discounted products";
			}else{
				discTxt = "You have earned "+disc+"% discount by playing our game to use against in game discounted products";
			}
		}
		return discTxt;
	}
	public void setDiscTxt(String discTxt) {
		this.discTxt = discTxt;
	}
	@Transient
	public String getExpTxt() {
		if(expDate != null) {
			expTxt = "Discount Code will Expiry on "+getExpDatestr(); 
		}
		return expTxt;
	}
	public void setExpTxt(String expTxt) {
		this.expTxt = expTxt;
	}
	
	@Transient
	public String getDiscStr() {
		if(disc!=null){
			try {
				if(discType.equalsIgnoreCase("PERCENTAGE")){
					discStr = "$"+EcommUtil.getRoundedValueString(disc);
				}else{
					discStr = String.valueOf(disc.intValue())+"%";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return discStr;
	}
	public void setDiscStr(String discStr) {
		this.discStr = discStr;
	}
	
}
