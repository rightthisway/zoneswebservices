package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.util.EcommUtil;

@Entity
@Table(name = "rtf_customer_cart")
public class CustomerCart implements Serializable{
	
	private Integer crtId;	
	private Integer custId;
	private Integer sellerId;	
	private Integer spiId;//seller product item id
	private Integer spivInvId;//seller produt item variation inventory id
	private Integer qty;	
	private Double selPrice;
	private String status;
	private Boolean isPlacedOrder;
	private String custCode;
	private String code;
	private Boolean isLoyPntUsed;
	private Date updDate;
	private String updBy;
	private Date lastNotified;
	private Boolean isRwdPointProd;
	private Integer reqRwdPoint;
	
	
	private String selPriceStr;
	private String reqRwdPointStr;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "crtID")
	public Integer getCrtId() {
		return crtId;
	}
	public void setCrtId(Integer crtId) {
		this.crtId = crtId;
	}
	@Column(name = "cust_id")
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	@Column(name = "sler_id")
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "slrinv_id")
	public Integer getSpivInvId() {
		return spivInvId;
	}
	public void setSpivInvId(Integer spivInvId) {
		this.spivInvId = spivInvId;
	}
	@Column(name = "qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	@Column(name = "selprice")
	public Double getSelPrice() {
		return selPrice;
	}
	public void setSelPrice(Double selPrice) {
		this.selPrice = selPrice;
	}
	@Column(name = "cartstatus")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "isplacedorder")
	public Boolean getIsPlacedOrder() {
		return isPlacedOrder;
	}
	public void setIsPlacedOrder(Boolean isPlacedOrder) {
		this.isPlacedOrder = isPlacedOrder;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	@Column(name = "cust_cou_code_apld")
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	@Column(name = "publ_cou_code_apld")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name = "is_with_lty_pnts")
	public Boolean getIsLoyPntUsed() {
		return isLoyPntUsed;
	}
	public void setIsLoyPntUsed(Boolean isLoyPntUsed) {
		this.isLoyPntUsed = isLoyPntUsed;
	}
	
	@Column(name = "last_notified")
	public Date getLastNotified() {
		return lastNotified;
	}
	public void setLastNotified(Date lastNotified) {
		this.lastNotified = lastNotified;
	}
	
	
	@Column(name = "only_rwd_prod")
	public Boolean getIsRwdPointProd() {
		return isRwdPointProd;
	}
	public void setIsRwdPointProd(Boolean isRwdPointProd) {
		this.isRwdPointProd = isRwdPointProd;
	}
	
	@Column(name = "only_req_rwd_pnts_prod")
	public Integer getReqRwdPoint() {
		return reqRwdPoint;
	}
	public void setReqRwdPoint(Integer reqRwdPoint) {
		this.reqRwdPoint = reqRwdPoint;
	}
	
	@Transient
	public String getSelPriceStr() {
		if(selPrice!=null){
			try {
				selPriceStr = EcommUtil.getRoundedValueString(selPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return selPriceStr;
	}
	public void setSelPriceStr(String selPriceStr) {
		this.selPriceStr = selPriceStr;
	}
	
	@Transient
	public String getReqRwdPointStr() {
		if(reqRwdPoint > 0){
			reqRwdPointStr = reqRwdPoint + " REWARD POINTS";
		}
		return reqRwdPointStr;
	}
	public void setReqRwdPointStr(String reqRwdPointStr) {
		this.reqRwdPointStr = reqRwdPointStr;
	}
	
	
	
	
	
}