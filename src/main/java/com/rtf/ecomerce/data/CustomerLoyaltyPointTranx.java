package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rtf_cust_loyalty_points_trans")
public class CustomerLoyaltyPointTranx implements Serializable{

	
	private Integer id;
	private Integer custId;
	private Integer orderId;
	private String tranxType;
	private Integer spentPoints;
	private Integer earnPoints;
	private Integer beforePoints;
	private Integer afterPoints;
	private Date crDate;
	private String crBy;
	private String status;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "lpId")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "cust_id")
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name = "trans_type")
	public String getTranxType() {
		return tranxType;
	}
	public void setTranxType(String tranxType) {
		this.tranxType = tranxType;
	}
	
	@Column(name = "spent_loyalpnts")
	public Integer getSpentPoints() {
		return spentPoints;
	}
	public void setSpentPoints(Integer spentPoints) {
		this.spentPoints = spentPoints;
	}
	
	@Column(name = "earned_loyalpnts")
	public Integer getEarnPoints() {
		return earnPoints;
	}
	public void setEarnPoints(Integer earnPoints) {
		this.earnPoints = earnPoints;
	}
	
	@Column(name = "before_trans_loyalpnts")
	public Integer getBeforePoints() {
		return beforePoints;
	}
	public void setBeforePoints(Integer beforePoints) {
		this.beforePoints = beforePoints;
	}
	
	@Column(name = "after_trans_loyalpnts")
	public Integer getAfterPoints() {
		return afterPoints;
	}
	public void setAfterPoints(Integer afterPoints) {
		this.afterPoints = afterPoints;
	}
	
	@Column(name = "created_date")
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	
	@Column(name = "created_by")
	public String getCrBy() {
		return crBy;
	}
	public void setCrBy(String crBy) {
		this.crBy = crBy;
	}
	
	@Column(name = "trans_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
