package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.util.DateFormatUtil;
import com.rtf.ecomerce.util.EcommUtil;

@Entity
@Table(name = "rtf_customer_order")
public class CustomerOrder implements Serializable{
	
	private Integer id;	
	private Integer custId;
	private Double netTot;	
	private Double disc;
	private Double grosTot;
	private String pPayType;//Primary Payment Type	
	private String sPayType;//Secondary Payment Type
	private Double pPayAmt;//Primary Payment Amount
	private Double sPayAmt;//Secondary Payment Amount
	private String pTrnxId;//Primary Transaction Id
	private String sTrnxId;//Secondary Transaction Id
	private Date crDate;
	private Boolean isEmailed;
	private String orderType;
	private String shAddr1;
	private String shAddr2;
	private String shCity;
	private String shState;
	private String shCountry;
	private String shZip;
	private String shFName;
	private String shLName;
	private String shPhone;
	private String shEmail;
	private String status;
	private Double shCharges;
	private Double tax;
	
	
	private String crDateStr;
	private String crDateTimeStr;
	private String delvStatus;
	private String delvDateStr;
	private String pImgUrl;
	private Double orderSubTotal;
	private Integer noOfItems;
	
	
	private String netTotStr;	
	private String discStr;
	private String grosTotStr;
	private String pPayAmtStr;
	private String sPayAmtStr;
	private String shChargesStr;
	private String taxStr;
	private String orderSubTotalStr;
	List<OrderProducts> ordProducts;
	
	
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "cust_id")
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	@Column(name = "net_total")
	public Double getNetTot() {
		return netTot;
	}
	public void setNetTot(Double netTot) {
		this.netTot = netTot;
	}
	@Column(name = "discount")
	public Double getDisc() {
		return disc;
	}
	public void setDisc(Double disc) {
		this.disc = disc;
	}
	@Column(name = "gross_total")
	public Double getGrosTot() {
		return grosTot;
	}
	public void setGrosTot(Double grosTot) {
		this.grosTot = grosTot;
	}
	@Column(name = "prm_pay_method")
	public String getpPayType() {
		return pPayType;
	}
	public void setpPayType(String pPayType) {
		this.pPayType = pPayType;
	}
	@Column(name = "sec_pay_method")
	public String getsPayType() {
		return sPayType;
	}
	public void setsPayType(String sPayType) {
		this.sPayType = sPayType;
	}
	@Column(name = "prm_pay_amt")
	public Double getpPayAmt() {
		return pPayAmt;
	}
	public void setpPayAmt(Double pPayAmt) {
		this.pPayAmt = pPayAmt;
	}
	@Column(name = "sec_pay_amt")
	public Double getsPayAmt() {
		return sPayAmt;
	}
	public void setsPayAmt(Double sPayAmt) {
		this.sPayAmt = sPayAmt;
	}
	@Column(name = "prm_tranx_id")
	public String getpTrnxId() {
		return pTrnxId;
	}
	public void setpTrnxId(String pTrnxId) {
		this.pTrnxId = pTrnxId;
	}
	@Column(name = "sec_tranx_id")
	public String getsTrnxId() {
		return sTrnxId;
	}
	public void setsTrnxId(String sTrnxId) {
		this.sTrnxId = sTrnxId;
	}
	@Column(name = "cr_date")
	public Date getCrDate() {
		return crDate;
	}
	public void setCrDate(Date crDate) {
		this.crDate = crDate;
	}
	@Column(name = "is_emailed")
	public Boolean getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}
	@Column(name = "order_type")
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	@Column(name = "sh_addr1")
	public String getShAddr1() {
		return shAddr1;
	}
	public void setShAddr1(String shAddr1) {
		this.shAddr1 = shAddr1;
	}
	@Column(name = "sh_addr2")
	public String getShAddr2() {
		return shAddr2;
	}
	public void setShAddr2(String shAddr2) {
		this.shAddr2 = shAddr2;
	}
	@Column(name = "sh_city")
	public String getShCity() {
		return shCity;
	}
	public void setShCity(String shCity) {
		this.shCity = shCity;
	}
	@Column(name = "sh_state")
	public String getShState() {
		return shState;
	}
	public void setShState(String shState) {
		this.shState = shState;
	}
	@Column(name = "sh_country")
	public String getShCountry() {
		return shCountry;
	}
	public void setShCountry(String shCountry) {
		this.shCountry = shCountry;
	}
	@Column(name = "sh_zip")
	public String getShZip() {
		return shZip;
	}
	public void setShZip(String shZip) {
		this.shZip = shZip;
	}
	@Column(name = "sh_fname")
	public String getShFName() {
		return shFName;
	}
	public void setShFName(String shFName) {
		this.shFName = shFName;
	}
	@Column(name = "sh_lname")
	public String getShLName() {
		return shLName;
	}
	public void setShLName(String shLName) {
		this.shLName = shLName;
	}
	@Column(name = "sh_phone")
	public String getShPhone() {
		return shPhone;
	}
	public void setShPhone(String shPhone) {
		this.shPhone = shPhone;
	}
	@Column(name = "sh_email")
	public String getShEmail() {
		return shEmail;
	}
	public void setShEmail(String shEmail) {
		this.shEmail = shEmail;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "shipping_charges")
	public Double getShCharges() {
		if(shCharges==null){
			shCharges = 0.00;
		}
		return shCharges;
	}
	public void setShCharges(Double shCharges) {
		this.shCharges = shCharges;
	}
	
	@Column(name = "tax_chares")
	public Double getTax() {
		if(tax==null){
			tax = 0.00;
		}
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	
	@Transient
	public List<OrderProducts> getOrdProducts() {
		return ordProducts;
	}
	public void setOrdProducts(List<OrderProducts> ordProducts) {
		this.ordProducts = ordProducts;
	}
	@Transient
	public String getCrDateStr() {
		if(crDate != null) {
			crDateStr = EcommUtil.formatDateTpMMddyyyy(crDate);
		}
		return crDateStr;
	}
	public void setCrDateStr(String crDateStr) {
		this.crDateStr = crDateStr;
	}
	
	
	@Transient
	public String getCrDateTimeStr() {
		if(crDate != null) {
			crDateTimeStr = EcommUtil.formatDateTpMMddyyyyHHmmAA(crDate);
		}
		return crDateTimeStr;
	}
	public void setCrDateTimeStr(String crDateTimeStr) {
		this.crDateTimeStr = crDateTimeStr;
	}
	@Transient
	public String getDelvStatus() {
		return delvStatus;
	}
	public void setDelvStatus(String delvStatus) {
		this.delvStatus = delvStatus;
	}
	@Transient
	public String getDelvDateStr() {
		return delvDateStr;
	}
	public void setDelvDateStr(String delvDateStr) {
		this.delvDateStr = delvDateStr;
	}
	
	@Transient
	public String getpImgUrl() {
		return pImgUrl;
	}
	public void setpImgUrl(String pImgUrl) {
		this.pImgUrl = pImgUrl;
	}
	
	@Transient
	public String getNetTotStr() {
		if(netTot!=null){
			try {
				netTotStr = EcommUtil.getRoundedValueString(netTot);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return netTotStr;
	}
	public void setNetTotStr(String netTotStr) {
		this.netTotStr = netTotStr;
	}
	
	@Transient
	public String getDiscStr() {
		if(disc!=null){
			try {
				discStr = EcommUtil.getRoundedValueString(disc);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return discStr;
	}
	public void setDiscStr(String discStr) {
		this.discStr = discStr;
	}
	
	@Transient
	public String getGrosTotStr() {
		if(grosTot!=null){
			try {
				grosTotStr = EcommUtil.getRoundedValueString(grosTot);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return grosTotStr;
	}
	public void setGrosTotStr(String grosTotStr) {
		this.grosTotStr = grosTotStr;
	}
	
	@Transient
	public String getpPayAmtStr() {
		if(pPayAmt!=null){
			try {
				pPayAmtStr = EcommUtil.getRoundedValueString(pPayAmt);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pPayAmtStr;
	}
	public void setpPayAmtStr(String pPayAmtStr) {
		this.pPayAmtStr = pPayAmtStr;
	}
	
	@Transient
	public String getsPayAmtStr() {
		if(sPayAmt!=null){
			try {
				sPayAmtStr = EcommUtil.getRoundedValueString(sPayAmt);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sPayAmtStr;
	}
	public void setsPayAmtStr(String sPayAmtStr) {
		this.sPayAmtStr = sPayAmtStr;
	}
	
	@Transient
	public String getShChargesStr() {
		try {
			shChargesStr = EcommUtil.getRoundedValueString(this.getShCharges());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shChargesStr;
	}
	public void setShChargesStr(String shChargesStr) {
		this.shChargesStr = shChargesStr;
	}
	
	
	@Transient
	public String getTaxStr() {
		try {
			taxStr = EcommUtil.getRoundedValueString(this.getTax());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return taxStr;
	}
	public void setTaxStr(String taxStr) {
		this.taxStr = taxStr;
	}
	
	@Transient
	public Double getOrderSubTotal() {
		orderSubTotal = netTot-(this.getShCharges()+this.getTax());
		return orderSubTotal;
	}
	public void setOrderSubTotal(Double orderSubTotal) {
		this.orderSubTotal = orderSubTotal;
	}
	
	@Transient
	public String getOrderSubTotalStr() {
		try {
			orderSubTotalStr = EcommUtil.getRoundedValueString(orderSubTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderSubTotalStr;
	}
	public void setOrderSubTotalStr(String orderSubTotalStr) {
		this.orderSubTotalStr = orderSubTotalStr;
	}
	
	@Transient
	public Integer getNoOfItems() {
		if(noOfItems == null){
			noOfItems = 0;
		}
		return noOfItems;
	}
	public void setNoOfItems(Integer noOfItems) {
		this.noOfItems = noOfItems;
	}
	
	
	
	
	
	
	
}