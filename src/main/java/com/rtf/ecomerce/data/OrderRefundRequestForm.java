package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_order_refund_request")
public class OrderRefundRequestForm implements Serializable{
	
	private Integer id;	
	private Integer custId;
	private Integer orderId;
	private Date orderDate;
	private String refundRemarks;
	private Date rfndRqstDate; 
	private String status;
	private Boolean isEmailed;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "orf_id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "cust_id")
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	@Column(name = "order_date")
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	@Column(name = "rfnd_rqst_remarks")
	public String getRefundRemarks() {
		return refundRemarks;
	}
	public void setRefundRemarks(String refundRemarks) {
		this.refundRemarks = refundRemarks;
	}
	@Column(name = "rfnd_rqst_date")
	public Date getRfndRqstDate() {
		return rfndRqstDate;
	}
	public void setRfndRqstDate(Date rfndRqstDate) {
		this.rfndRqstDate = rfndRqstDate;
	}
	@Column(name = "rfnd_status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "is_emailed")
	public Boolean getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}
	
	
	
	
	
}