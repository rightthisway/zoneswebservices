package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "sd_prd_gds_category")
public class ProductGdsCategory implements Serializable{

	private Integer pdgsId;	
	private Integer goodsId;
	private Integer catId;
	private String catName;
	private String catDesc;	
	private String gdsCatNameMapg;
	private String gdsCatIdMapg;
	private String allGdsCatIndicator; 
	private String gdsStatus;
	private String catStatus;
	private Date updDate;
	private String updBy;
	
	private String  updDateStr ;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pdgs_id")
	public Integer getPdgsId() {
		return pdgsId;
	}
	public void setPdgsId(Integer pdgsId) {
		this.pdgsId = pdgsId;
	}
	@Column(name = "goods_id")
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	@Column(name = "caty_id")
	public Integer getCatId() {
		return catId;
	}
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	@Column(name = "caty_name")
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	@Column(name = "caty_desc")
	public String getCatDesc() {
		return catDesc;
	}
	public void setCatDesc(String catDesc) {
		this.catDesc = catDesc;
	}
	@Column(name = "gds_caty_name_mapg")
	public String getGdsCatNameMapg() {
		return gdsCatNameMapg;
	}
	public void setGdsCatNameMapg(String gdsCatNameMapg) {
		this.gdsCatNameMapg = gdsCatNameMapg;
	}
	@Column(name = "gds_caty_id_mapg")
	public String getGdsCatIdMapg() {
		return gdsCatIdMapg;
	}
	public void setGdsCatIdMapg(String gdsCatIdMapg) {
		this.gdsCatIdMapg = gdsCatIdMapg;
	}
	@Column(name = "all_gds_caty_indicator")
	public String getAllGdsCatIndicator() {
		return allGdsCatIndicator;
	}
	public void setAllGdsCatIndicator(String allGdsCatIndicator) {
		this.allGdsCatIndicator = allGdsCatIndicator;
	}
	@Column(name = "goods_status")
	public String getGdsStatus() {
		return gdsStatus;
	}
	public void setGdsStatus(String gdsStatus) {
		this.gdsStatus = gdsStatus;
	}
	@Column(name = "caty_status")
	public String getCatStatus() {
		return catStatus;
	}
	public void setCatStatus(String catStatus) {
		this.catStatus = catStatus;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	
	@Transient
	public String getUpdDateStr() {
		return updDateStr;
	}
	public void setUpdDateStr(String updDateStr) {
		this.updDateStr = updDateStr;
	}

	
}