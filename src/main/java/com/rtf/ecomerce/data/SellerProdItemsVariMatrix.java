package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_seller_product_items_varia_matrix")
public class SellerProdItemsVariMatrix implements Serializable{
	
	private Integer spiVarComId;//seller Product Variation comp id
	private Integer setUniqId;
	private Integer spiId;//seller product item id
	private Integer sellerId;
	private String vOptNameValIdCom;//
	private String vOptNameValCom;//
	private String voNameOne;
	private String voValOne;
	private String voNameTwo;
	private String voValTwo;
	private String voNameThree;
	private String voValThree;
	private String voNameFour;
	private String voValFour;
	private Integer varNameIdOne;//seller product item Variation Name Id One
	private Integer varNameIdTwo;//seller product item Variation Name Id Two
	private Integer varNameIdThree;//seller product item Variation Name Id Three
	private Integer varNameIdFour;//seller product item Variation Name Id Four
	private Integer varValIdOne;//seller product item Variation Value Id One
	private Integer varValIdTwo;//seller product item Variation Value Id Two
	private Integer varValIdThree;//seller product item Variation Value Id Three
	private Integer varValIdFour;//seller product item Variation Value Id Four
	private Date updDate; 
	private String updBy;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "slrpivarcom_id")
	public Integer getSpiVarComId() {
		return spiVarComId;
	}
	public void setSpiVarComId(Integer spiVarComId) {
		this.spiVarComId = spiVarComId;
	}
	@Column(name = "set_uniq_id")
	public Integer getSetUniqId() {
		return setUniqId;
	}
	public void setSetUniqId(Integer setUniqId) {
		this.setUniqId = setUniqId;
	}
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "sler_id")
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	@Column(name = "vopt_name_val_id_com")
	public String getvOptNameValIdCom() {
		return vOptNameValIdCom;
	}
	public void setvOptNameValIdCom(String vOptNameValIdCom) {
		this.vOptNameValIdCom = vOptNameValIdCom;
	}
	@Column(name = "vopt_name_val_com")
	public String getvOptNameValCom() {
		return vOptNameValCom;
	}
	public void setvOptNameValCom(String vOptNameValCom) {
		this.vOptNameValCom = vOptNameValCom;
	}
	@Column(name = "vo_name_one")
	public String getVoNameOne() {
		return voNameOne;
	}
	public void setVoNameOne(String voNameOne) {
		this.voNameOne = voNameOne;
	}
	@Column(name = "vo_value_one")
	public String getVoValOne() {
		return voValOne;
	}
	public void setVoValOne(String voValOne) {
		this.voValOne = voValOne;
	}
	@Column(name = "vo_name_two")
	public String getVoNameTwo() {
		return voNameTwo;
	}
	public void setVoNameTwo(String voNameTwo) {
		this.voNameTwo = voNameTwo;
	}
	@Column(name = "vo_value_two")
	public String getVoValTwo() {
		return voValTwo;
	}
	public void setVoValTwo(String voValTwo) {
		this.voValTwo = voValTwo;
	}
	@Column(name = "vo_name_three")
	public String getVoNameThree() {
		return voNameThree;
	}
	public void setVoNameThree(String voNameThree) {
		this.voNameThree = voNameThree;
	}
	@Column(name = "vo_value_three")
	public String getVoValThree() {
		return voValThree;
	}
	public void setVoValThree(String voValThree) {
		this.voValThree = voValThree;
	}
	@Column(name = "vo_name_four")
	public String getVoNameFour() {
		return voNameFour;
	}
	public void setVoNameFour(String voNameFour) {
		this.voNameFour = voNameFour;
	}
	@Column(name = "vo_value_four")
	public String getVoValFour() {
		return voValFour;
	}
	public void setVoValFour(String voValFour) {
		this.voValFour = voValFour;
	}
	@Column(name = "slrpivar_id_one")
	public Integer getVarNameIdOne() {
		return varNameIdOne;
	}
	public void setVarNameIdOne(Integer varNameIdOne) {
		this.varNameIdOne = varNameIdOne;
	}
	@Column(name = "slrpivar_id_two")
	public Integer getVarNameIdTwo() {
		return varNameIdTwo;
	}
	public void setVarNameIdTwo(Integer varNameIdTwo) {
		this.varNameIdTwo = varNameIdTwo;
	}
	@Column(name = "slrpivar_id_three")
	public Integer getVarNameIdThree() {
		return varNameIdThree;
	}
	public void setVarNameIdThree(Integer varNameIdThree) {
		this.varNameIdThree = varNameIdThree;
	}
	@Column(name = "slrpivar_id_four")
	public Integer getVarNameIdFour() {
		return varNameIdFour;
	}
	public void setVarNameIdFour(Integer varNameIdFour) {
		this.varNameIdFour = varNameIdFour;
	}
	@Column(name = "varoval_id_one")
	public Integer getVarValIdOne() {
		return varValIdOne;
	}
	public void setVarValIdOne(Integer varValIdOne) {
		this.varValIdOne = varValIdOne;
	}
	@Column(name = "varoval_id_two")
	public Integer getVarValIdTwo() {
		return varValIdTwo;
	}
	public void setVarValIdTwo(Integer varValIdTwo) {
		this.varValIdTwo = varValIdTwo;
	}
	@Column(name = "varoval_id_three")
	public Integer getVarValIdThree() {
		return varValIdThree;
	}
	public void setVarValIdThree(Integer varValIdThree) {
		this.varValIdThree = varValIdThree;
	}
	@Column(name = "varoval_id_four")
	public Integer getVarValIdFour() {
		return varValIdFour;
	}
	public void setVarValIdFour(Integer varValIdFour) {
		this.varValIdFour = varValIdFour;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
			
}