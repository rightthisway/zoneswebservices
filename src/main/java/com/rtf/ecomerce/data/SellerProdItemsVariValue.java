package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_seller_product_items_vari_opt_value")
public class SellerProdItemsVariValue implements Serializable{
	
	private Integer slrpiVarValId;//seller product item Variation Value Id
	private Integer slrpiVarId;//seller product item Variation Id
	private String varVal;//Variation Option Value
	private String varValImg; //Variation Value Image
	private Date updDate; 
	private String updBy;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "varoval_id")
	public Integer getSlrpiVarValId() {
		return slrpiVarValId;
	}
	public void setSlrpiVarValId(Integer slrpiVarValId) {
		this.slrpiVarValId = slrpiVarValId;
	}
	@Column(name = "slrpivar_id")
	public Integer getSlrpiVarId() {
		return slrpiVarId;
	}
	public void setSlrpiVarId(Integer slrpiVarId) {
		this.slrpiVarId = slrpiVarId;
	}
	@Column(name = "vopt_value")
	public String getVarVal() {
		return varVal;
	}
	public void setVarVal(String varVal) {
		this.varVal = varVal;
	}
	@Column(name = "vopt_value_img")
	public String getVarValImg() {
		return varValImg;
	}
	public void setVarValImg(String varValImg) {
		this.varValImg = varValImg;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
			
}