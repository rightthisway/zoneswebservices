package com.rtf.ecomerce.data;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rtf.ecomerce.util.EcommUtil;

@Entity
@Table(name = "rtf_seller_product_items_mstr")
public class SellerProductsItems implements Serializable{
	
	private Integer spiId;//seller product item id
	private Integer sellerId;
	private Integer dispOrder;
	private String pBrand;//Product Brand
	private String pName;//Product Name
	private String pDesc;//Product Description
	private String pLongDesc;//Product Long Desc
	private Double psMinPrice;//Product sell minimum proce
	private Double pRegMinPrice;//Product 
	private String pImgUrl;//Product image Url
	private String status;
	private Date pExpDate;//Prodct Expiry Date
	private Date updDate;
	private String updBy;
	private String psku;
	private Boolean isVarient;
	private Integer maxCustQty;
	private Boolean dispDiscPerc;
	private Boolean isDispRegPrice;
	private Boolean isSoldOut;
	
	private String prodType;
	private Boolean isRwdPointProd;
	private Integer reqRwdPoint;
	
	
	private String discPerc;
	private Boolean isGameProd;
	private String prodExpDateStr;
	private String gameProdDesc;
	private String gameProdDetailDesc = "This item is available for BIG discounts by answering questions correctly during our LIVE GAME SHOW.";
	private String psMinPriceStr;
	private String pRegMinPriceStr;
	private String proExpStr;
	private String inGameDiscStr;
	private Double inGameDiscPrice;
	private String inGameDiscPriceStr;
	private String reqRwdPointStr;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "slrpitms_id")
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	@Column(name = "sler_id")
	public Integer getSellerId() {
		return sellerId;
	}
	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}
	
	@Column(name = "displ_order")
	public Integer getDispOrder() {
		return dispOrder;
	}
	public void setDispOrder(Integer dispOrder) {
		this.dispOrder = dispOrder;
	}
	@Column(name = "pbrand")
	public String getpBrand() {
		return pBrand;
	}
	public void setpBrand(String pBrand) {
		this.pBrand = pBrand;
	}
	@Column(name = "pname")
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	@Column(name = "pdesc")
	public String getpDesc() {
		return pDesc;
	}
	public void setpDesc(String pDesc) {
		this.pDesc = pDesc;
	}
	@Column(name = "plong_desc")
	public String getpLongDesc() {
		return pLongDesc;
	}
	public void setpLongDesc(String pLongDesc) {
		this.pLongDesc = pLongDesc;
	}
	@Column(name = "psel_min_prc")
	public Double getPsMinPrice() {
		if(psMinPrice!=null){
			try {
				psMinPrice = EcommUtil.getNormalRoundedValue(psMinPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return psMinPrice;
	}
	public void setPsMinPrice(Double psMinPrice) {
		this.psMinPrice = psMinPrice;
	}
	@Column(name = "preg_min_prc")
	public Double getpRegMinPrice() {
		if(pRegMinPrice!=null){
			try {
				pRegMinPrice = EcommUtil.getNormalRoundedValue(pRegMinPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pRegMinPrice;
	}
	public void setpRegMinPrice(Double pRegMinPrice) {
		this.pRegMinPrice = pRegMinPrice;
	}
	@Column(name = "pimg")
	public String getpImgUrl() {
		return pImgUrl;
	}
	public void setpImgUrl(String pImgUrl) {
		this.pImgUrl = pImgUrl;
	}
	@Column(name = "pstatus")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "pexp_date")
	public Date getpExpDate() {
		return pExpDate;
	}
	public void setpExpDate(Date pExpDate) {
		this.pExpDate = pExpDate;
	}
	@Column(name = "lupd_date")
	public Date getUpdDate() {
		return updDate;
	}
	public void setUpdDate(Date updDate) {
		this.updDate = updDate;
	}
	@Column(name = "lupd_by")
	public String getUpdBy() {
		return updBy;
	}
	public void setUpdBy(String updBy) {
		this.updBy = updBy;
	}
	@Column(name = "psku")
	public String getPsku() {
		return psku;
	}
	public void setPsku(String psku) {
		this.psku = psku;
	}
	@Column(name = "max_cust_qty")
	public Integer getMaxCustQty() {
		return maxCustQty;
	}
	public void setMaxCustQty(Integer maxCustQty) {
		this.maxCustQty = maxCustQty;
	}
	@Column(name = "is_variant")
	public Boolean getIsVarient() {
		return isVarient;
	}
	public void setIsVarient(Boolean isVarient) {
		this.isVarient = isVarient;
	}
	
	@Column(name = "is_for_live_game")
	public Boolean getIsGameProd() {
		if(isGameProd == null){
			isGameProd = false;
		}
		return isGameProd;
	}
	public void setIsGameProd(Boolean isGameProd) {
		this.isGameProd = isGameProd;
	}
	
	
	@Column(name = "prod_type")
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	
	@Column(name = "only_rwd_prod")
	public Boolean getIsRwdPointProd() {
		return isRwdPointProd;
	}
	public void setIsRwdPointProd(Boolean isRwdPointProd) {
		this.isRwdPointProd = isRwdPointProd;
	}
	
	@Column(name = "only_req_rwd_pnts_prod")
	public Integer getReqRwdPoint() {
		return reqRwdPoint;
	}
	public void setReqRwdPoint(Integer reqRwdPoint) {
		this.reqRwdPoint = reqRwdPoint;
	}
	
	@Column(name = "disp_perc_off")
	public Boolean getDispDiscPerc() {
		return dispDiscPerc;
	}
	public void setDispDiscPerc(Boolean dispDiscPerc) {
		this.dispDiscPerc = dispDiscPerc;
	}
	
	@Column(name = "disp_reg_price")
	public Boolean getIsDispRegPrice() {
		return isDispRegPrice;
	}
	public void setIsDispRegPrice(Boolean isDispRegPrice) {
		this.isDispRegPrice = isDispRegPrice;
	}
	
	
	@Column(name = "is_sold_out")
	public Boolean getIsSoldOut() {
		return isSoldOut;
	}
	public void setIsSoldOut(Boolean isSoldOut) {
		this.isSoldOut = isSoldOut;
	}
	
	@Transient
	public String getReqRwdPointStr() {
		if(reqRwdPoint > 0){
			reqRwdPointStr = reqRwdPoint + " POINTS";
		}
		return reqRwdPointStr;
	}
	public void setReqRwdPointStr(String reqRwdPointStr) {
		this.reqRwdPointStr = reqRwdPointStr;
	}
	
	@Transient
	public String getDiscPerc() {
		try {
			if(getDispDiscPerc() == null || !getDispDiscPerc()){
				discPerc = "";
				return discPerc;
			}
			if(getIsRwdPointProd()){
				discPerc = "";
				return discPerc;
			}
			if(pRegMinPrice > 0){
				Double diff = pRegMinPrice - psMinPrice;
				Double dec = EcommUtil.getNormalRoundedValue((diff*100)/pRegMinPrice);
				discPerc = dec.intValue()+"% OFF";
			}else{
				discPerc = "";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return discPerc;
	}
	public void setDiscPerc(String discPerc) {
		this.discPerc = discPerc;
	}
	
	
	@Transient
	public String getProdExpDateStr() {
		if(pExpDate!=null){
			prodExpDateStr = EcommUtil.formatDateTpMMddyyyyHHmmAA(pExpDate);
		}
		return prodExpDateStr;
	}
	public void setProdExpDateStr(String prodExpDateStr) {
		this.prodExpDateStr = prodExpDateStr;
	}
	
	@Transient
	public String getGameProdDesc() {
		return gameProdDesc;
	}
	public void setGameProdDesc(String gameProdDesc) {
		this.gameProdDesc = gameProdDesc;
	}
	
	@Transient
	public String getPsMinPriceStr() {
		if(psMinPrice!=null){
			try {
				psMinPriceStr = EcommUtil.getRoundedValueString(psMinPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return psMinPriceStr;
	}
	public void setPsMinPriceStr(String psMinPriceStr) {
		this.psMinPriceStr = psMinPriceStr;
	}
	
	@Transient
	public String getpRegMinPriceStr() {
		if(pRegMinPrice!=null){
			try {
				pRegMinPriceStr = EcommUtil.getRoundedValueString(pRegMinPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pRegMinPriceStr;
	}
	public void setpRegMinPriceStr(String pRegMinPriceStr) {
		this.pRegMinPriceStr = pRegMinPriceStr;
	}
	@Transient
	public String getProExpStr() {
		if(pExpDate!=null){
			Date today = new Date();
			long diff = pExpDate.getTime() - today.getTime();
			long rem = TimeUnit.MILLISECONDS.toHours(diff);
			if(rem>0){
				proExpStr = rem + " hours left to buy";
			}else{
				rem =  TimeUnit.MILLISECONDS.toMinutes(diff);
				if(rem<=1){
					rem = 1;
					proExpStr = rem + " minute left to buy";
				}else{
					proExpStr = rem + " minutes left to buy";
				}
			}
		}
		return proExpStr;
	}
	public void setProExpStr(String proExpStr) {
		this.proExpStr = proExpStr;
	}
	
	@Transient
	public String getInGameDiscStr() {
		return inGameDiscStr;
	}
	public void setInGameDiscStr(String inGameDiscStr) {
		this.inGameDiscStr = inGameDiscStr;
	}
	
	@Transient
	public Double getInGameDiscPrice() {
		return inGameDiscPrice;
	}
	public void setInGameDiscPrice(Double inGameDiscPrice) {
		this.inGameDiscPrice = inGameDiscPrice;
	}
	
	@Transient
	public String getInGameDiscPriceStr() {
		if(inGameDiscPrice!=null){
			try {
				inGameDiscPriceStr = EcommUtil.getRoundedValueString(inGameDiscPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return inGameDiscPriceStr;
	}
	public void setInGameDiscPriceStr(String inGameDiscPriceStr) {
		this.inGameDiscPriceStr = inGameDiscPriceStr;
	}
	
	@Transient
	public String getGameProdDetailDesc() {
		return gameProdDetailDesc;
	}
	public void setGameProdDetailDesc(String gameProdDetailDesc) {
		this.gameProdDetailDesc = gameProdDetailDesc;
	}
	
	
	
}