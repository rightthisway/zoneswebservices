package com.rtf.ecomerce.pojo;

import com.rtf.ecomerce.util.EcommUtil;

public class CartProductPOJO {
	
	private Integer crtId;	
	private Integer custId;
	private Integer spivInvId;
	private Integer spiId;

	private String pName;
	private String pImg;
	private String pVariantDtl;
	private Double pPrice;
	private String pPriceStr;
	private Integer reqPoints;
	private Integer qty;
	private Boolean isPointUsed;
	private Double totalDisc;
	
	
	
	
	public Integer getCrtId() {
		return crtId;
	}
	public void setCrtId(Integer crtId) {
		this.crtId = crtId;
	}
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getSpivInvId() {
		return spivInvId;
	}
	public void setSpivInvId(Integer spivInvId) {
		this.spivInvId = spivInvId;
	}
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getpImg() {
		return pImg;
	}
	public void setpImg(String pImg) {
		this.pImg = pImg;
	}
	public String getpVariantDtl() {
		return pVariantDtl;
	}
	public void setpVariantDtl(String pVariantDtl) {
		this.pVariantDtl = pVariantDtl;
	}
	public Double getpPrice() {
		return pPrice;
	}
	public void setpPrice(Double pPrice) {
		this.pPrice = pPrice;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public Integer getReqPoints() {
		return reqPoints;
	}
	public void setReqPoints(Integer reqPoints) {
		this.reqPoints = reqPoints;
	}
	public String getpPriceStr() {
		if(pPrice !=null){
			try {
				pPriceStr = EcommUtil.getRoundedValueString(pPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pPriceStr;
	}
	public void setpPriceStr(String pPriceStr) {
		this.pPriceStr = pPriceStr;
	}
	public Boolean getIsPointUsed() {
		return isPointUsed;
	}
	public void setIsPointUsed(Boolean isPointUsed) {
		this.isPointUsed = isPointUsed;
	}
	public Double getTotalDisc() {
		return totalDisc;
	}
	public void setTotalDisc(Double totalDisc) {
		this.totalDisc = totalDisc;
	}
}
