package com.rtf.ecomerce.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtf.ecomerce.data.CoupenCode;
import com.rtf.ecomerce.data.CustCoupenCode;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtf.ecomerce.util.EcommUtil;

public class CustomerCartProdDtls implements Serializable{
	
	private Integer crtId;	
	private Integer custId;
	private Integer spivInvId;//seller produt item variation inventory id
	private Integer spiId;//seller product item id
	private String pBrand;//Product Brand
	private String pName;//Product Name
	private Double sellPrice;
	private Double regPrice;
	private Integer reqLPnts;
	private Double priceAftLPnts;
	private Boolean isLoyPntUsed;
	private Boolean isSoldOut;
	private Boolean isVariant;
	
	private String pImg;
	private Integer maxQty;
	private Integer crtQty;
	private Boolean isOutStk;
	private String outStkTxt;
	private String optValTxt;

	@JsonIgnore
	private String optValDtl;
	@JsonIgnore
	private Integer soldQty;
	private Integer avlQty;
	
	@JsonIgnore
	private String pstatus;
	@JsonIgnore
	private Date pExpDate;
	@JsonIgnore
	private Integer custMaxQty;
	private String discPerc;
	
	
	
	private SellerProdItemsVariInventory inv;
	private List<SellerProdItemsVariName> options;
	private String custCodeStr;
	private String loyaltyPointStr;
	private String codeStr;
	private String code;
	private String custCode;
	private String valMsg;
	
	
	
	public Integer getCrtId() {
		return crtId;
	}
	public void setCrtId(Integer crtId) {
		this.crtId = crtId;
	}
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getSpivInvId() {
		return spivInvId;
	}
	public void setSpivInvId(Integer spivInvId) {
		this.spivInvId = spivInvId;
	}
	public Integer getSpiId() {
		return spiId;
	}
	public void setSpiId(Integer spiId) {
		this.spiId = spiId;
	}
	public String getpBrand() {
		return pBrand;
	}
	public void setpBrand(String pBrand) {
		this.pBrand = pBrand;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public Double getSellPrice() {
		if(sellPrice == null){
			sellPrice = 0.0;
		}
		return sellPrice;
	}
	public void setSellPrice(Double sellPrice) {
		this.sellPrice = sellPrice;
	}
	public Double getRegPrice() {
		if(regPrice == null){
			regPrice = 0.00;
		}
		return regPrice;
	}
	public void setRegPrice(Double regPrice) {
		this.regPrice = regPrice;
	}
	public Integer getReqLPnts() {
		return reqLPnts;
	}
	public void setReqLPnts(Integer reqLPnts) {
		this.reqLPnts = reqLPnts;
	}
	public Double getPriceAftLPnts() {
		if(priceAftLPnts == null){
			priceAftLPnts = 0.0;
		}
		return priceAftLPnts;
	}
	public void setPriceAftLPnts(Double priceAftLPnts) {
		this.priceAftLPnts = priceAftLPnts;
	}
	public Integer getAvlQty() {
		if(avlQty == null){
			avlQty = 0;
		}
		return avlQty;
	}
	public void setAvlQty(Integer avlQty) {
		this.avlQty = avlQty;
	}
	public String getpImg() {
		return pImg;
	}
	public void setpImg(String pImg) {
		this.pImg = pImg;
	}
	public String getOptValDtl() {
		return optValDtl;
	}
	public void setOptValDtl(String optValDtl) {
		this.optValDtl = optValDtl;
	}
	public Integer getSoldQty() {
		return soldQty;
	}
	public void setSoldQty(Integer soldQty) {
		this.soldQty = soldQty;
	}
	public Integer getCrtQty() {
		return crtQty;
	}
	public void setCrtQty(Integer crtQty) {
		this.crtQty = crtQty;
	}
	public String getPstatus() {
		return pstatus;
	}
	public void setPstatus(String pstatus) {
		this.pstatus = pstatus;
	}
	public Date getpExpDate() {
		return pExpDate;
	}
	public void setpExpDate(Date pExpDate) {
		this.pExpDate = pExpDate;
	}
	public Integer getCustMaxQty() {
		return custMaxQty;
	}
	public void setCustMaxQty(Integer custMaxQty) {
		this.custMaxQty = custMaxQty;
	}
	public Integer getMaxQty() {
		return maxQty;
	}
	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}
	public Boolean getIsOutStk() {
		return isOutStk;
	}
	public void setIsOutStk(Boolean isOutStk) {
		this.isOutStk = isOutStk;
	}
	public String getOutStkTxt() {
		return outStkTxt;
	}
	public void setOutStkTxt(String outStkTxt) {
		this.outStkTxt = outStkTxt;
	}
	public String getOptValTxt() {
		if(optValDtl != null) {
			optValTxt = optValDtl;
		}
		return optValTxt;
	}
	public void setOptValTxt(String optValTxt) {
		this.optValTxt = optValTxt;
	}
	
	public SellerProdItemsVariInventory getInv() {
		return inv;
	}
	public void setInv(SellerProdItemsVariInventory inv) {
		this.inv = inv;
	}
	public List<SellerProdItemsVariName> getOptions() {
		return options;
	}
	public void setOptions(List<SellerProdItemsVariName> options) {
		this.options = options;
	}
	
	public String getCustCodeStr() {
		return custCodeStr;
	}
	public void setCustCodeStr(String custCodeStr) {
		this.custCodeStr = custCodeStr;
	}
	public String getLoyaltyPointStr() {
		return loyaltyPointStr;
	}
	public void setLoyaltyPointStr(String loyaltyPointStr) {
		this.loyaltyPointStr = loyaltyPointStr;
	}
	public String getCodeStr() {
		return codeStr;
	}
	public void setCodeStr(String codeStr) {
		this.codeStr = codeStr;
	}
	public Boolean getIsLoyPntUsed() {
		return isLoyPntUsed;
	}
	public void setIsLoyPntUsed(Boolean isLoyPntUsed) {
		this.isLoyPntUsed = isLoyPntUsed;
	}
	
	public Boolean getIsSoldOut() {
		return isSoldOut;
	}
	public void setIsSoldOut(Boolean isSoldOut) {
		this.isSoldOut = isSoldOut;
	}
	public Boolean getIsVariant() {
		return isVariant;
	}
	public void setIsVariant(Boolean isVariant) {
		this.isVariant = isVariant;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValMsg() {
		return valMsg;
	}
	public void setValMsg(String valMsg) {
		this.valMsg = valMsg;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	
	public String getDiscPerc() {
		try {
			/*Double diff = this.getRegPrice() - this.getSellPrice();
			Double dec = EcommUtil.getNormalRoundedValue((diff*100)/this.getRegPrice());
			discPerc = dec.intValue()+"% OFF";*/
			discPerc="";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return discPerc;
	}
	public void setDiscPerc(String discPerc) {
		this.discPerc = discPerc;
	}
}