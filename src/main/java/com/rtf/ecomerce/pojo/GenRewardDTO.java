package com.rtf.ecomerce.pojo;

import java.util.List;


public class GenRewardDTO {
	private String clId;
	private String rwdty;	
	private GenRewardDVO dvo;
	private List<GenRewardDVO> lst;
	private String msg;
	private Integer sts;
	private String coId;
	private Integer conrunno;
	
	public String getClId() {
		return clId;
	}
	public void setClId(String clId) {
		this.clId = clId;
	}
	public String getRwdty() {
		return rwdty;
	}
	public void setRwdty(String rwdty) {
		this.rwdty = rwdty;
	}
	public GenRewardDVO getDvo() {
		return dvo;
	}
	public void setDvo(GenRewardDVO dvo) {
		this.dvo = dvo;
	}
	public List<GenRewardDVO> getLst() {
		return lst;
	}
	public void setLst(List<GenRewardDVO> lst) {
		this.lst = lst;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public String getCoId() {
		return coId;
	}
	public void setCoId(String coId) {
		this.coId = coId;
	}
	public Integer getConrunno() {
		return conrunno;
	}
	public void setConrunno(Integer conrunno) {
		this.conrunno = conrunno;
	}
	
	
	

}
