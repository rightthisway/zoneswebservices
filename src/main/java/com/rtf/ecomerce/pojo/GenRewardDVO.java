package com.rtf.ecomerce.pojo;

import java.io.Serializable;

public class GenRewardDVO implements Serializable{
	private String cuId;
	private String rwdtyp;
	private Double rwdval;
	
	private Integer customerId;
	private Integer updLoyltyPoints;
	private Integer custOldLoyaltyPoints;
	private Integer contRwdPoints;
	
	
	public String getCuId() {
		return cuId;
	}
	public void setCuId(String cuId) {
		this.cuId = cuId;
	}
	public String getRwdtyp() {
		return rwdtyp;
	}
	public void setRwdtyp(String rwdtyp) {
		this.rwdtyp = rwdtyp;
	}
	public Double getRwdval() {
		if(rwdval == null) {
			rwdval = 0.0;
		}
		return rwdval;
	}
	public void setRwdval(Double rwdval) {
		this.rwdval = rwdval;
	}
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	
	public Integer getUpdLoyltyPoints() {
		if(updLoyltyPoints == null) {
			updLoyltyPoints = 0;
		}
		return updLoyltyPoints;
	}
	public void setUpdLoyltyPoints(Integer updLoyltyPoints) {
		this.updLoyltyPoints = updLoyltyPoints;
	}
	
	public Integer getCustOldLoyaltyPoints() {
		if(custOldLoyaltyPoints == null ) {
			custOldLoyaltyPoints = 0;
		}
		return custOldLoyaltyPoints;
	}
	public void setCustOldLoyaltyPoints(Integer custOldLoyaltyPoints) {
		this.custOldLoyaltyPoints = custOldLoyaltyPoints;
	}
	
	public Integer getContRwdPoints() {
		if(contRwdPoints == null) {
			contRwdPoints = 0;
		}
		return contRwdPoints;
	}
	public void setContRwdPoints(Integer contRwdPoints) {
		this.contRwdPoints = contRwdPoints;
	}
	@Override
	public String toString() {
		return "GenRewardDVO [cuId=" + cuId + ", rwdtyp=" + rwdtyp + ", rwdval=" + rwdval + "]";
	}

}
