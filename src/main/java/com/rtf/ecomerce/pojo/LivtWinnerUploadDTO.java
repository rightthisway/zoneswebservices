package com.rtf.ecomerce.pojo;

import java.util.Date;
import java.util.List;

public class LivtWinnerUploadDTO {
	
	private String clId;
	private String msg;
	private Integer sts;
	private String coId;
	private Date conStartDat;
	private Integer crNo;
	
	List<String> custList;

	public String getClId() {
		return clId;
	}

	public void setClId(String clId) {
		this.clId = clId;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getSts() {
		return sts;
	}

	public void setSts(Integer sts) {
		this.sts = sts;
	}

	public String getCoId() {
		return coId;
	}

	public void setCoId(String coId) {
		this.coId = coId;
	}

	public List<String> getCustList() {
		return custList;
	}

	public void setCustList(List<String> custList) {
		this.custList = custList;
	}

	public Date getConStartDat() {
		return conStartDat;
	}

	public void setConStartDat(Date conStartDat) {
		this.conStartDat = conStartDat;
	}

	public Integer getCrNo() {
		return crNo;
	}

	public void setCrNo(Integer crNo) {
		this.crNo = crNo;
	}



}
