package com.rtf.ecomerce.resp;

import java.util.List;

import com.rtf.ecomerce.data.SellerProductsItems;
import com.rtf.ecomerce.pojo.CartProductPOJO;
import com.rtf.ecomerce.util.EcommUtil;
import com.zonesws.webservices.utils.Error;

public class CheckoutResp {

	private Integer status;
	private Error error; 
	private String message;
	
	private Double orderTotal;
	private Double ShippingCharge=4.00;
	private Double tax=2.00;
	private Integer noOfItems;
	
	
	private String orderTotalStr;
	private String ShippingChargeStr;
	private String taxStr;
	private String publishableKey;
	private List<CartProductPOJO> prods;
	private SellerProductsItems prod;
	
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Double getShippingCharge() {
		return ShippingCharge;
	}
	public void setShippingCharge(Double shippingCharge) {
		ShippingCharge = shippingCharge;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Integer getNoOfItems() {
		return noOfItems;
	}
	public void setNoOfItems(Integer noOfItems) {
		this.noOfItems = noOfItems;
	}
	public String getOrderTotalStr() {
		if(orderTotal!=null){
			try {
				orderTotalStr = EcommUtil.getRoundedValueString(orderTotal);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orderTotalStr;
	}
	public void setOrderTotalStr(String orderTotalStr) {
		this.orderTotalStr = orderTotalStr;
	}
	public String getShippingChargeStr() {
		if(ShippingCharge!=null){
			try {
				ShippingChargeStr = EcommUtil.getRoundedValueString(ShippingCharge);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ShippingChargeStr;
	}
	public void setShippingChargeStr(String shippingChargeStr) {
		ShippingChargeStr = shippingChargeStr;
	}
	public String getTaxStr() {
		if(tax!=null){
			try {
				taxStr = EcommUtil.getRoundedValueString(tax);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return taxStr;
	}
	public void setTaxStr(String taxStr) {
		this.taxStr = taxStr;
	}
	public String getPublishableKey() {
		return publishableKey;
	}
	public void setPublishableKey(String publishableKey) {
		this.publishableKey = publishableKey;
	}
	public List<CartProductPOJO> getProds() {
		return prods;
	}
	public void setProds(List<CartProductPOJO> prods) {
		this.prods = prods;
	}
	public SellerProductsItems getProd() {
		return prod;
	}
	public void setProd(SellerProductsItems prod) {
		this.prod = prod;
	}
	
	
	
	
	
}
