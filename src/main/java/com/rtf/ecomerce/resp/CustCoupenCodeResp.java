package com.rtf.ecomerce.resp;

import java.util.List;

import com.rtf.ecomerce.data.CustCoupenCode;
import com.zonesws.webservices.utils.Error;

public class CustCoupenCodeResp {

	
	private Integer status;
	private Error error; 
	private String message;
	private List<CustCoupenCode> list;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustCoupenCode> getList() {
		return list;
	}
	public void setList(List<CustCoupenCode> list) {
		this.list = list;
	}
	
}
