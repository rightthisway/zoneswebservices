package com.rtf.ecomerce.resp;

import java.util.List;

import com.rtf.ecomerce.pojo.CustomerCartProdDtls;
import com.rtf.ecomerce.util.EcommUtil;
import com.zonesws.webservices.utils.Error;

public class CustomerCartResp {

	private Integer status;
	private Error error; 
	private String message;
	
	private List<CustomerCartProdDtls> list;
	private CustomerCartProdDtls cart;
	private Double subTotal;
	private String offerHdr;
	private String subTotalStr;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerCartProdDtls> getList() {
		return list;
	}
	public void setList(List<CustomerCartProdDtls> list) {
		this.list = list;
	}
	public CustomerCartProdDtls getCart() {
		return cart;
	}
	public void setCart(CustomerCartProdDtls cart) {
		this.cart = cart;
	}
	public Double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	public String getOfferHdr() {
		return offerHdr;
	}
	public void setOfferHdr(String offerHdr) {
		this.offerHdr = offerHdr;
	}
	
	public String getSubTotalStr() {
		if(subTotal!=null){
			try {
				subTotalStr = EcommUtil.getRoundedValueString(subTotal);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return subTotalStr;
	}
	public void setSubTotalStr(String subTotalStr) {
		this.subTotalStr = subTotalStr;
	}
	
}
