package com.rtf.ecomerce.resp;

import com.zonesws.webservices.utils.Error;

public class GenericResp {

	private Integer status;
	private Error error; 
	private String message;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
