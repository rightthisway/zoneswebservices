package com.rtf.ecomerce.resp;

import java.util.List;

import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;
import com.zonesws.webservices.utils.Error;

public class OrderResp {

	
	private Integer status;
	private Error error; 
	private String message;
	private CustomerOrder order;
	private List<CustomerOrder> orders;
	private List<OrderProducts> prods;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerOrder getOrder() {
		return order;
	}
	public void setOrder(CustomerOrder order) {
		this.order = order;
	}
	public List<CustomerOrder> getOrders() {
		return orders;
	}
	public void setOrders(List<CustomerOrder> orders) {
		this.orders = orders;
	}
	public List<OrderProducts> getProds() {
		return prods;
	}
	public void setProds(List<OrderProducts> prods) {
		this.prods = prods;
	}
	
}
