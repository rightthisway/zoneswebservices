package com.rtf.ecomerce.resp;

import java.util.List;

import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProdItemsVariName;
import com.rtf.ecomerce.data.SellerProductsItems;
import com.zonesws.webservices.utils.Error;

public class ProductResp {

	
	private Integer status;
	private Error error; 
	private String message;
	private List<SellerProductsItems> prods;
	private SellerProductsItems prod;
	private List<SellerProdItemsVariName> options;
	private String mrktText;//="Super saver day! Use discount codes to get discount upto 15%, Also shipping is free for all products.";
	
	
	private String invStatus;
	private SellerProdItemsVariInventory inv;
	private Integer avlQty;
	private List<String> offerList;
	private String offerHdr;
	private String shippCharge;
	private String taxCharge;
	private String maxQtyMsg;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<SellerProductsItems> getProds() {
		return prods;
	}
	public void setProds(List<SellerProductsItems> prods) {
		this.prods = prods;
	}
	public SellerProductsItems getProd() {
		return prod;
	}
	public void setProd(SellerProductsItems prod) {
		this.prod = prod;
	}
	public List<SellerProdItemsVariName> getOptions() {
		return options;
	}
	public void setOptions(List<SellerProdItemsVariName> options) {
		this.options = options;
	}
	public String getInvStatus() {
		return invStatus;
	}
	public void setInvStatus(String invStatus) {
		this.invStatus = invStatus;
	}
	public SellerProdItemsVariInventory getInv() {
		return inv;
	}
	public void setInv(SellerProdItemsVariInventory inv) {
		this.inv = inv;
	}
	public Integer getAvlQty() {
		return avlQty;
	}
	public void setAvlQty(Integer avlQty) {
		this.avlQty = avlQty;
	}
	public List<String> getOfferList() {
		return offerList;
	}
	public void setOfferList(List<String> offerList) {
		this.offerList = offerList;
	}
	public String getOfferHdr() {
		return offerHdr;
	}
	public void setOfferHdr(String offerHdr) {
		this.offerHdr = offerHdr;
	}
	public String getShippCharge() {
		return shippCharge;
	}
	public void setShippCharge(String shippCharge) {
		this.shippCharge = shippCharge;
	}
	public String getTaxCharge() {
		return taxCharge;
	}
	public void setTaxCharge(String taxCharge) {
		this.taxCharge = taxCharge;
	}
	public String getMrktText() {
		return mrktText;
	}
	public void setMrktText(String mrktText) {
		this.mrktText = mrktText;
	}
	public String getMaxQtyMsg() {
		return maxQtyMsg;
	}
	public void setMaxQtyMsg(String maxQtyMsg) {
		this.maxQtyMsg = maxQtyMsg;
	}
}
