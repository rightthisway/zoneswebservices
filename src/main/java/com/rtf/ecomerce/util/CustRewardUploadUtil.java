package com.rtf.ecomerce.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtf.ecomerce.pojo.GenRewardDTO;
import com.rtf.ecomerce.pojo.GenRewardDVO;
import com.rtfquiz.webservices.sqldao.implementation.SQLDaoUtil;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;

public class CustRewardUploadUtil {


	public static int uploadCustRewardstoRtf(GenRewardDTO rewardDto) throws Exception {

		try {
			List<GenRewardDVO> list = rewardDto.getLst();
			List<GenRewardDVO> tempList = new ArrayList<GenRewardDVO>();
	
			List<Customer> customers = DAORegistry.getCustomerDAO().getAllRewardTheFanCustomers();
			//Map<Integer,Customer> custoemrMap = new HashMap<Integer, Customer>();
			Map<String,Customer> custoemrEmailMap = new HashMap<String, Customer>();
			for (Customer customer : customers) {
				//custoemrMap.put(customer.getId(), customer);
				custoemrEmailMap.put(customer.getEmail(), customer);
			}
			Integer processCount = 0,custIDMisisngCnt=0,custObjMissingCnt=0;
			Customer custObj = null;
			int i=0;
			for (GenRewardDVO genRewardDVO : list) {
				
				custObj = custoemrEmailMap.get(genRewardDVO.getCuId());
				if(custObj == null) {
					custObjMissingCnt++;
					continue;
				}
				genRewardDVO.setCustomerId(custObj.getId());
				
				if(genRewardDVO.getCustomerId() == null) {
					custIDMisisngCnt++;
					continue;
				}
				/*custObj = custoemrMap.get(genRewardDVO.getCustomerId());
				if(custObj == null) {
					custObjMissingCnt++;
					continue;
				}*/
				Integer contLPoints = getRewardVal(genRewardDVO.getRwdval());
				Integer custUpdLPoints = custObj.getLoyaltyPoints() + contLPoints;
				genRewardDVO.setUpdLoyltyPoints(custUpdLPoints);
				genRewardDVO.setContRwdPoints(contLPoints);
				genRewardDVO.setCustOldLoyaltyPoints(custObj.getLoyaltyPoints());
				tempList.add(genRewardDVO);
				i++;
				if(i >= 2000) {
					SQLDaoUtil.uploadCustomerLoyaltyPoints(tempList);
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(tempList);
					SQLDaoUtil.uploadCustomerLoyaltyPointsTracking(tempList,rewardDto.getCoId(),rewardDto.getConrunno());
					tempList = new ArrayList<GenRewardDVO>();
					i=0;
				}
				processCount++;
			}
			if(i > 0) {
				SQLDaoUtil.uploadCustomerLoyaltyPoints(tempList);
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(tempList);
				SQLDaoUtil.uploadCustomerLoyaltyPointsTracking(tempList,rewardDto.getCoId(),rewardDto.getConrunno());
				tempList = new ArrayList<GenRewardDVO>();
			}
			System.out.println("Customer Reward Upload DTL Size : "+rewardDto.getCoId()+" :paramsize: "+rewardDto.getLst().size()+" :procedsize: "+processCount+" :custIdMisCnt : "+custIDMisisngCnt+" :custObjMisCnt : "+custObjMissingCnt+" :"+new Date());
			return processCount;
		} catch(Exception e ) {
			e.printStackTrace();
			throw e;
		}
	}
	public static Integer getRewardVal(Double val) {
		if(val != null) {
		 return (int) Math.ceil(val);
		}
		return 0;
	}

	
}
