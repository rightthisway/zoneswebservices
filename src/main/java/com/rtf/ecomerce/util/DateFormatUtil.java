package com.rtf.ecomerce.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {

	
	public static Date getDateWithTwentyFourHourFormat(String datetime){
		Date result =null;
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		try{
			if(datetime != null){
				result = formatDateTime.parse(datetime);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public static String getMMDDYYYYHHMMString(Date date){
		String result =null;
		DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
			if(date != null){
				result = formatDateTime.format(date);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String getMMDDYYYYHHMMSSString(Long timeInMilli){
		String result = null;
		try {
			if(timeInMilli == null){
				return null;
			}
			Date date = new Date(timeInMilli);
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String getMMDDYYYYString(Long timeInMilli){
		String result = null;
		try {
			if(timeInMilli == null){
				return null;
			}
			Date date = new Date(timeInMilli);
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String getCassFormatTimeStamp(Long timeInMilli){
		String result = null;
		try {
			Date date = new Date(timeInMilli);
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			result = formatDateTime.format(date);
			result = result.replaceAll("/","-");
			result += ".000+0000";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String getMMDDYYYYHHMMSSString(Date date){
		String result = null;
		try {
			if(date == null){
				return "";
			}
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String getDDMMYYYYHHMMSSString(Date date){
		String result = null;
		try {
			if(date == null){
				return "";
			}
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public static Integer extractDateElement(String input,String format){
		try {
			if(input==null || input.isEmpty() || format==null || format.isEmpty()){
				return -1;
			}
			if(format.equals("DAY")){
				if(input.contains("/")){
					String arr[] = input.split("/");
					if(arr.length>1){
						return Integer.parseInt(arr[1].isEmpty()?"-1":arr[1]);
					}
				}
				return -1;
			}else if(format.equals("MONTH")){
				if(input.contains("/")){
					String arr[] = input.split("/");
					if(arr.length>0){
						return Integer.parseInt(arr[0].isEmpty()?"-1":arr[0]);
					}
				}else{
					return Integer.parseInt(input);
				}
				return -1;
			}else if(format.equals("YEAR")){
				if(input.contains("/")){
					String arr[] = input.split("/");
					if(arr.length>2){
						return Integer.parseInt(arr[2].isEmpty()?"-1":arr[2]);
					}
				}
				return -1;
			}else if(format.equals("HOUR")){
				String amPm="";
				if(input.contains(":")){
					String arr[];
					if(input.contains(" ")){
						arr = input.split(" ");
						amPm = arr[1];
					}
					arr = input.split(":");
					if(arr.length>0){
						if(amPm.equalsIgnoreCase("pm")){
							return Integer.parseInt(arr[0].isEmpty()?"-1":arr[0])+12;
						}
						return Integer.parseInt(arr[0].isEmpty()?"-1":arr[0]);
					}
				}else{
					return Integer.parseInt(input);
				}
				return -1;
			}else if(format.equals("MINUTE")){
				if(input.contains(":")){
					if(input.contains(" ")){
						input= input.substring(0,input.indexOf(" "));
					}
					String []arr = input.split(":");
					if(arr.length>1){
						return Integer.parseInt(arr[1].isEmpty()?"-1":arr[1]);
					}
				}
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	
}
