package com.rtf.ecomerce.util;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.CoupenCode;
import com.rtf.ecomerce.data.CustCoupenCode;
import com.rtf.ecomerce.data.CustomerCart;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProductsItems;

public class EcommUtil {

	private static DecimalFormat df2 = new DecimalFormat(".##");
	private static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	
	
	public static Double getNormalRoundedValue(Double value) throws Exception {
		if(value==null){
			return 0.00;
		}
		return Double.valueOf(df2.format(value));
	}
	
	public static String getRoundedValueString(Double value) throws Exception {
		Integer result = value.intValue();
		if(value > result){
			return String.format( "%.2f", value);
		}
		return String.format( "%.0f", value);
	}
	
	public static Integer getRoundedValueIntger(Double value) throws Exception {
		value = Math.floor(value);
		return value.intValue();
	}
	
	public static Integer getIntegerValueRoundedUp(Double value) throws Exception {
		Integer result = value.intValue();
		if(value > result){
			result = result + 1;
		}
		return result;
	}
	
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}
	
	public static Double getRoundedUpValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.UP);
		return Double.valueOf(df2.format(value));
	}
	
	
	public static String formatDateTpMMddyyyyHHmmAA(Date date){
		String result = null;
		try {
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm aa");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static String formatDateTpMMddyyyy(Date date){
		String result = null;
		try {
			DateFormat formatDateTime = new SimpleDateFormat("MM/dd/yyyy");
			result = formatDateTime.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	
	public static List<String> getOfferList(String type, Double disc,Double price,String code){
		List<String> offerList = new ArrayList<String>();
		try {
			if(type.equalsIgnoreCase("PERCENTAGE")){
				if(code!=null && !code.isEmpty()){
					offerList.add("Use "+disc.intValue()+"%  discount code ("+code+").");
				}else{
					offerList.add("Game Show Winners will receive a "+disc.intValue()+"% discount");
				}
				
			}else{
				if(code!=null && !code.isEmpty()){
					offerList.add("Use $"+disc+" flat off discount code("+code+").");
				}else{
					offerList.add("Game Show Winners will receive a $"+disc+" flat OFF discount");
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offerList;
	}
	
	
	public static Integer validateDailyProductOrderQtyLimit(Integer custId, Integer invId,Integer qty, Integer maxQty){
		try {
			OrderProducts oProd = EcommDAORegistry.getEcommQueryManagerDAO().getCustomerLast24HoursOrdersDetail(custId, invId);
			if(oProd!=null){
				if(oProd.getQty() >= maxQty){
					return 0;
				}
				
				int remQty = maxQty - oProd.getQty();
				System.out.println("LAST ORDER : "+oProd.getQty()+"   |    "+remQty);
				
				return remQty;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("LAST ORDER : NULL");
		return Integer.MAX_VALUE;	
	}
	
	
	
	
	public static String calculateShippingStatus(Date date){
		String shipping = "Will be shipped b/w ";
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_MONTH, 15);
			shipping = shipping + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
			Integer day = cal.get(Calendar.DATE);
			shipping  = shipping + " "+day;
			shipping  = shipping +(day>1?"th":"st");
			
			shipping  = shipping + " to ";
			cal.add(Calendar.DAY_OF_MONTH, 15);
			day = cal.get(Calendar.DATE);
			shipping = shipping + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
			shipping  = shipping + " "+day;
			shipping  = shipping +(day>1?"th":"st");
			return shipping;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public static Double computeShippingFees(Integer quantity,String countryName,boolean isRewardPointsOrder,boolean isOnlyGiftcardOrder) {
		double shippingFees=0.0;
		if(isOnlyGiftcardOrder) {
			return 0.0;
		}
		if(countryName != null && countryName.equalsIgnoreCase("Canada")) {
			shippingFees = 25.0;
		} else {
			if(isRewardPointsOrder) {
				shippingFees = 3.0;
			}
		}
		return shippingFees;
		
		/*if(quantity > 1) {
			return 0.0;//7.99
		} else {
			return 0.0;//4.99
		}*/
	}
	
	
	
	
	public static Double computeOrderSubTotal(Integer custId){
		Map<Integer, SellerProdItemsVariInventory> invMap = new HashMap<Integer, SellerProdItemsVariInventory>();
		Map<Integer, SellerProductsItems> prodMap = new HashMap<Integer, SellerProductsItems>();
		Double subTotal = 0.00;
		try {
			List<Integer> invIds = new ArrayList<Integer>();
			List<CustomerCart> carts = EcommDAORegistry.getCustomerCartDAO().getAllCustomerCartByCustomerId(custId);
			for(CustomerCart cart: carts){
				invIds.add(cart.getSpivInvId());
				SellerProductsItems prod = EcommDAORegistry.getSellerProductsItemsDAO().get(cart.getSpiId());
				prodMap.put(cart.getSpiId(), prod);
			}
			List<SellerProdItemsVariInventory> inventories = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getInventoryByIds(invIds);
			for(SellerProdItemsVariInventory inv: inventories){
				invMap.put(inv.getSpivInvId(), inv);
			}
			for(CustomerCart cart: carts){
				Double custCodediscount = 0.00;
				Double codeDiscount = 0.00;
				Double rpDisc = 0.0;
				Double finalPrice = 0.00;
				Double sellPrice = 0.00;
				SellerProdItemsVariInventory inv = invMap.get(cart.getSpivInvId());
				SellerProductsItems prod = prodMap.get(cart.getSpiId());
				sellPrice = prod.getPsMinPrice();
				if(inv!=null){
					sellPrice = inv.getSellPrice();
				}
				if(cart.getIsLoyPntUsed() !=null && cart.getIsLoyPntUsed()){
					finalPrice = inv!=null?inv.getPriceAftLPnts():prod.getPsMinPrice();
					rpDisc = (sellPrice - (inv!=null?inv.getPriceAftLPnts():prod.getPsMinPrice()));
				}else if(cart.getIsRwdPointProd()){
					finalPrice = 0.00;
				}else{
					finalPrice = sellPrice;
				}
				if(cart.getCustCode()!=null && !cart.getCustCode().isEmpty()){
					List<CustCoupenCode> custCodes = EcommDAORegistry.getEcommQueryManagerDAO().getCustomerProductCouponCodes(cart.getSpiId(), "CONTEST", custId, cart.getCustCode());
					CustCoupenCode custCode = null;
					if(custCodes!=null && !custCodes.isEmpty()){
						custCode = custCodes.get(0);
					}
					if(custCode!=null && custCode.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						custCodediscount = (sellPrice *custCode.getDisc())/100;
						//finalPrice = EcommUtil.getNormalRoundedValue(finalPrice - custCodediscount);
					}else if(custCode!=null && custCode.getDiscType().equalsIgnoreCase("FLAT")){
						custCodediscount = custCode.getDisc();
						//finalPrice = EcommUtil.getNormalRoundedValue(finalPrice - custCodediscount);
					}
				}
				
				if(cart.getCode()!=null && !cart.getCode().isEmpty()){
					CoupenCode code = EcommDAORegistry.getCoupenCodeDAO().getProductCodesByCode(cart.getCode());
					if(code!=null && code.getDiscType().equalsIgnoreCase("PERCENTAGE")){
						codeDiscount =  (sellPrice *code.getCpnDisc())/100;
						//finalPrice = finalPrice - codeDiscount;
					}else if(code!=null && code.getDiscType().equalsIgnoreCase("FLAT")){
						codeDiscount = code.getCpnDisc();
						//finalPrice = EcommUtil.getNormalRoundedValue(finalPrice - codeDiscount);
					}
				}
				Double custDisc = EcommUtil.getNormalRoundedValue(custCodediscount*cart.getQty());
				Double codeDisc = EcommUtil.getNormalRoundedValue(codeDiscount*cart.getQty());
				rpDisc = rpDisc * cart.getQty();
				Double prodTotal = ((finalPrice * cart.getQty()) - ( custDisc + codeDisc));
				subTotal = subTotal + prodTotal;
				System.out.println("PRODTOTAL : "+prodTotal+"   |   CODEDISC : "+codeDisc+"  |  CUSTDISC : "+custDisc+"  | RPDISC : "+rpDisc+"  |   QTY :  "+cart.getQty()+"   |   SELLPRICE : "+sellPrice);
			}
			subTotal = getNormalRoundedValue(subTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return subTotal;
	}
	
	
	
	
	public static void main(String[] args) throws Exception {
		//System.out.println(calculateShippingStatus(new Date()));
		//System.out.println(getRoundedValueString(100.9));
	}
	
}
