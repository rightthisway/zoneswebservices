package com.rtf.ecomerce.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.common.io.ByteStreams;
import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.CustomerOrder;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtf.ecomerce.data.OrderRefundRequestForm;
import com.rtf.ecomerce.data.SellerDetails;
import com.rtfquiz.webservices.aws.AwsS3Response;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

public class RTFEcommEmailScheduler  extends QuartzJobBean implements StatefulJob {

	
	static MailManager mailManager = null;
	//private static String BCC = "msanghani@rightthisway.com";
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFEcommEmailScheduler.mailManager = mailManager;
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			sendOrderConfirmationEmails();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			sendOrderCancellationEmails();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try { 
			sendOrderDeliveryInfoEmails();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try { 
			//sendOrderDeliveredInfoEmails();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			//sendCartItemReminderEmails();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void sendOrderConfirmationEmails() throws Exception{
		try {
			List<CustomerOrder> orders = EcommDAORegistry.getCustomerProdOrderDAO().getCustomerOrdersToEmail();
			if(orders == null || orders.isEmpty()){
				return;
			}
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("gameShowLink","https://rewardthefan.com/");
			mailMap.put("myOrdersLink", "https://rewardthefan.com/profile");
			for(CustomerOrder order : orders){
				Customer cust = DAORegistry.getCustomerDAO().get(order.getCustId());
				List<OrderProducts> prods = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderId(order.getId());
				if(prods == null || prods.isEmpty()){
					continue;
				}
				Integer qty = 0;
				String prodDetails = "";
				/*Map<String, List<OrderProducts>> sellProdMap = new HashMap<String, List<OrderProducts>>();
				List<OrderProducts> products = null;*/
				for(OrderProducts prod : prods){
					/*SellerDetails seller = EcommDAORegistry.getSellerDetailsDAO().get(prod.getSellerId());
					if(sellProdMap.get(seller.getEmail()) !=null){
						products = sellProdMap.get(seller.getEmail());
					}else{
						products = new ArrayList<OrderProducts>(); 
					}
					products.add(prod);
					sellProdMap.put(seller.getEmail(), products);*/
					
					qty = qty + prod.getQty();
					prodDetails = prodDetails +prod.getpName()+" </br>";
				}
				mailMap.put("prodDetails",prodDetails);
				mailMap.put("qty", qty);
				mailMap.put("custName", cust.getUserId());
				mailMap.put("order", order);
				
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, cust.getEmail(), null, URLUtil.bccEmails, "Reward The Fan : Your order confirmation.",
				    		"mail-product-order-confirmation.html", mailMap, "text/html",null,mailAttachment,null);
					order.setIsEmailed(true);
					EcommDAORegistry.getCustomerProdOrderDAO().update(order);
				
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				/*
				for(Map.Entry<String, List<OrderProducts>> map : sellProdMap.entrySet()){
					prodDetails = "";
					cnt = 0;
					qty = 0;
					String sellEmail = map.getKey();
					List<OrderProducts> sellProds = map.getValue();
					for(OrderProducts sProd : sellProds){
						cnt++;
						prodDetails = prodDetails +cnt+". "+sProd.getpName()+" </br>";
					}
					
					mailMap.put("prodDetails",prodDetails);
					try{
						mailManager.sendMailNow("text/html",URLUtil.fromEmail,sellEmail, null, BCCURLUtil.bccEmails, "Reward The Fan : Your order confirmation.",
					    		"mail-product-order-confirmation.html", mailMap, "text/html",null,mailAttachment,null);
					}catch(Exception e){
						e.printStackTrace();
					}
				}*/
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	
	private void sendOrderCancellationEmails() throws Exception{
		try {
			List<OrderRefundRequestForm> refunds = EcommDAORegistry.getOrderRefundRequestFormDAO().getAllOrderRefundRequestToEmail();
			if(refunds==null || refunds.isEmpty()){
				return;
			}
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("gameShowLink","https://rewardthefan.com/");
			
			for(OrderRefundRequestForm ref : refunds){
				Customer cust  = DAORegistry.getCustomerDAO().get(ref.getCustId());
				mailMap.put("userName",cust.getUserId());
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, cust.getEmail(), null, URLUtil.bccEmails, "Reward The Fan : Your request to return an item.",
				    		"mail-product-order-refund-request.html", mailMap, "text/html",null,mailAttachment,null);
					ref.setIsEmailed(true);
					EcommDAORegistry.getOrderRefundRequestFormDAO().update(ref);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	
	private void sendOrderDeliveryInfoEmails() throws Exception{
		try {
			List<OrderProducts> ordProds = EcommDAORegistry.getOrderProductSetDAO().getOrderProductsToEmailShipTrackNo();
			if(ordProds == null || ordProds.isEmpty()){
				return;
			}
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("gameShowLink","https://rewardthefan.com/");
			
			for(OrderProducts prod : ordProds){
				CustomerOrder order = EcommDAORegistry.getCustomerProdOrderDAO().get(prod.getOrderId());
				Customer cust = DAORegistry.getCustomerDAO().get(order.getCustId());
				if(prod.getShTrackId()== null || prod.getShTrackId().isEmpty()){
					continue;
				}
				if(prod.getShMethod() == null){
					continue;
				}
				String trackUrl = "";
				if(prod.getShMethod().equalsIgnoreCase("FEDEX")){
					trackUrl="https://www.fedex.com/fedextrack/?trknbr=";
				}else if(prod.getShMethod().equalsIgnoreCase("UPS")){
					trackUrl="https://www.ups.com/track?tracknum=";
				}else if(prod.getShMethod().equalsIgnoreCase("USPS")){
					trackUrl="https://tools.usps.com/go/TrackConfirmAction?qtc_tLabels1=";
				}else{
					continue;
				}
				mailMap.put("trackNo",trackUrl+prod.getShTrackId());
				mailMap.put("userName",cust.getUserId());
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, cust.getEmail(), null, URLUtil.bccEmails, "Reward The Fan : Your tracking number.",
				    		"mail-product-order-delivery-info.html", mailMap, "text/html",null,mailAttachment,null);
					prod.setIsTrackNoUpdated(false);
					EcommDAORegistry.getOrderProductSetDAO().update(prod);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	
	
	public void sendOrderDeliveredInfoEmails() throws Exception{
		try {
			List<OrderProducts> ordProds = EcommDAORegistry.getOrderProductSetDAO().getDeliveredOrderProducts();
			
			System.out.println("DELIVERED EMAIL prod Size : "+ordProds.size());
			if(ordProds == null || ordProds.isEmpty()){
				return;
			}
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("gameShowLink","https://rewardthefan.com/");
			
			for(OrderProducts prod : ordProds){
				String msgText  = "";
				CustomerOrder order = EcommDAORegistry.getCustomerProdOrderDAO().get(prod.getOrderId());
				Customer cust = DAORegistry.getCustomerDAO().get(order.getCustId());
				if(prod.getStatus()==null){
					continue;
				}
				if(!prod.getStatus().equalsIgnoreCase("FULFILLED")){
					continue;
				}
				System.out.println("DELIVERED EMAIL  prodId : "+prod.getId()+"  |  Email : "+cust.getEmail());
				mailMap.put("userName",cust.getUserId());
				
				if(order.getpPayType().equals(PaymentMethod.CONTEST_WINNER.toString())){
					msgText = "Thanks for using reward the fan.";
				}else{
					msgText = "Thanks again for purchasing your item on Reward The Fan.";
				}
				mailMap.put("msgText",msgText);
				
				MailAttachment[] custAttachment = new MailAttachment[1];
				
				if(!prod.getProdType().equalsIgnoreCase("PRODUCT") && prod.getDelFileName()!=null && !prod.getDelFileName().isEmpty()){
					AwsS3Response res = AWSFileService.downLoadFileFromS3("rtfassetz", "dpd",prod.getDelFileName(), URLUtil.getProductOrderImagePath());
					if(res == null || res.getStatus()  == 0){
						System.out.println("DELIVERED EMAIL : File not found on S3.");
						continue;
					}
					File file = new File(URLUtil.getProductOrderImagePath()+prod.getDelFileName());
					if(file.exists()){
						String ext = FilenameUtils.getExtension(prod.getDelFileName());
						BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file));
						byte[] bytes = ByteStreams.toByteArray(stream);
						custAttachment[0] = new MailAttachment(bytes, "image/"+ext, file.getName(),URLUtil.getProductOrderImagePath()+prod.getDelFileName());
					}else{
						System.out.println("DELIVERED EMAIL : File not found on server.");
						continue;
					}
				}
				
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForProductDelivered();
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, cust.getEmail(), null, URLUtil.bccEmails, "Reward The Fan : Your item delivered.",
				    		"mail-product-order-delivered.html", mailMap, "text/html",custAttachment,mailAttachment,null);
					prod.setIsDelEmailSent(true);
					prod.setShippingStatus("DELIVERED");
					EcommDAORegistry.getOrderProductSetDAO().update(prod);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	private void sendCartItemReminderEmails() throws Exception{
		try {/*
			Calendar cal = Calendar.getInstance();
			Date today = new Date();
			cal.add(Calendar.MINUTE, -30);
			List<CustomerCart> carts = EcommDAORegistry.getCustomerCartDAO().getAllCustomerCartToEmail(cal.getTime());
			if(carts == null || carts.isEmpty()){
				return;
			}
			
			
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("gameShowLink","https://rewardthefan.com/");
			mailMap.put("cartLink","https://rewardthefan.com/cart");
			
			for(CustomerCart cart : carts){
				Customer cust = DAORegistry.getCustomerDAO().get(cart.getCustId());
				SellerProductsItems product = EcommDAORegistry.getSellerProductsItemsDAO().getProductsById(cart.getSpiId());
				if(product == null || product.getpExpDate().before(today)){
					continue;
				}
						
				mailMap.put("userName",cust.getUserId());
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, cust.getEmail(), null, URLUtil.bccEmails, "Reward The Fan : Your cart is about to expire.",
				    		"mail-product-cart-reminder.html", mailMap, "text/html",null,mailAttachment,null);
					cart.setLastNotified(today);
					EcommDAORegistry.getCustomerCartDAO().update(cart);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
		*/} catch (Exception e) {
			throw e;
		}
	}
	
	
	
	/*public static void main(String[] args) {
		try {
			//testEmail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	public  void testEmail(Customer cust,String template) throws Exception{
		try {
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("gameShowLink","https://rewardthefan.com/");
				String msgText  = "";
				mailMap.put("userName",cust.getUserId());
				msgText = "Thanks again for purchasing your item on Reward The Fan.";
				mailMap.put("msgText",msgText);
				
				MailAttachment[] custAttachment = new MailAttachment[1];
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForProductDelivered();
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, cust.getEmail(), null, URLUtil.bccEmails, "Reward The Fan : Your Order Shipped.",
							template+".html", mailMap, "text/html",custAttachment,mailAttachment,null);
				}catch(Exception e){
					e.printStackTrace();
				}
		} catch (Exception e) {
			throw e;
		}
	}

}
