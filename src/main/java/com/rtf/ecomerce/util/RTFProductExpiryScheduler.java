package com.rtf.ecomerce.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.CoupenCode;
import com.rtf.ecomerce.data.CustCoupenCode;
import com.rtf.ecomerce.data.SellerProdItemsVariInventory;
import com.rtf.ecomerce.data.SellerProductsItems;

public class RTFProductExpiryScheduler extends QuartzJobBean implements StatefulJob {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			processProducts();
		} catch (Exception e) {
			System.out.println("PRODUCT-JOB : ERROR OCCURED");
			e.printStackTrace();
		}
		
		try {
			processCouponCodes();
		} catch (Exception e) {
			System.out.println("DISCCODE-JOB : ERROR OCCURED");
			e.printStackTrace();
		}
		
	}
	
	
	
	private void processProducts()throws Exception{
		try {
			Date today =  new Date();
			List<SellerProdItemsVariInventory> allInvs = new ArrayList<SellerProdItemsVariInventory>();
			List<SellerProductsItems> products = EcommDAORegistry.getSellerProductsItemsDAO().getExpiredProductByDate(today,"ACTIVE");
			if(products == null || products.isEmpty()){
				System.out.println("PRODUCT-JOB : NO PRODUCT FOUND TO EXPIRE.");
				return;
			}
			
			for(SellerProductsItems prod : products){
				List<SellerProdItemsVariInventory> invs = EcommDAORegistry.getSellerProdItemsVariInventoryDAO().getAllInventoryByProdId(prod.getSpiId());
				if(invs!=null && !invs.isEmpty()){
					for(SellerProdItemsVariInventory inv : invs){
						inv.setStatus("EXPIRED");
						inv.setUpdDate(today);
						inv.setUpdBy("SYS");
					}
					allInvs.addAll(invs);
				}
				
				try {
					Integer count = EcommDAORegistry.getCustomerCartDAO().removeAllCustomerCartByPoductId(prod.getSpiId());
					System.out.println("PRODUCT-JOB : CUSTOMER CART REMOVED : "+count);
				} catch (Exception e) {
					e.printStackTrace();
				}
				prod.setStatus("EXPIRED");
				prod.setUpdBy("SYS");
				prod.setUpdDate(today);
			}
			
			
			EcommDAORegistry.getSellerProductsItemsDAO().updateAll(products);
			EcommDAORegistry.getSellerProdItemsVariInventoryDAO().updateAll(allInvs);
			System.out.println("PRODUCT-JOB : NUMBER OF PRODUCT EXPIRED : "+products.size() +"  |  "+allInvs.size());
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	
	private void processCouponCodes()throws Exception{
		try {
			List<CustCoupenCode> allCCode = new ArrayList<CustCoupenCode>();
			Date today =  new Date();
			List<CoupenCode> codes = EcommDAORegistry.getCoupenCodeDAO().getDiscountCodeToExpire(today);
			if(codes == null || codes.isEmpty()){
				System.out.println("DISCCODE-JOB : NO PRODUCT FOUND TO EXPIRE.");
				return;
			}
			
			for(CoupenCode code : codes){
				if(code.getCouType().equalsIgnoreCase("CONTEST")){
					List<CustCoupenCode> custCodes = EcommDAORegistry.getCustomerCoupenCodeDAO().getAllActiveCustomerCoupenCodesByCode(code.getCpnCode());
					for(CustCoupenCode cCode : custCodes){
						cCode.setStatus("EXPIRED");
					}
					if(!custCodes.isEmpty()){
						allCCode.addAll(custCodes);
					}
				}
				
				code.setStatus("EXPIRED");
				code.setUpdBy("SYS");
				code.setExpDate(today);
			}
			
			EcommDAORegistry.getCoupenCodeDAO().updateAll(codes);
			EcommDAORegistry.getCustomerCoupenCodeDAO().updateAll(allCCode);
			System.out.println("DISCCODE-JOB : NUMBER OF DISCOUNT CODE EXPIRED : "+codes.size() +"  |  "+allCCode.size());
			
		} catch (Exception e) {
			throw e;
		}
	}

}
