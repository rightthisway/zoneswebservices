package com.rtf.ecomerce.util;

import com.taxjar.Taxjar;
import com.taxjar.model.rates.RateResponse;


public class TaxJarUtil {

	//public static String TAX_JAR_API_KEY = "1270bb64ba0d5c38fed073d186c4b0ca"; //test
	//public static String TAX_JAR_API_KEY = "8c745adea752acd8fb0bf1a08ca99346"; //prod
	public static Double calculateTaxAmount(String taxJarKey, Double totalAmount,String zipCode) throws Exception {
		return 0.0;
	}
    public static Double calculateTaxAmountOld(String taxJarKey, Double totalAmount,String zipCode) throws Exception {
    	Taxjar client = new Taxjar(taxJarKey);
    	System.out.println("TAX_JAR_API_KEY  : "+taxJarKey);
        try {
            
            Double taxAmout = null;
            Float taxRate= null;
            RateResponse res = client.ratesForLocation(zipCode);
            if(res != null) {
            	taxRate = res.rate.getCombinedRate();
            	System.out.println("ctyR:"+res.rate.getCityRate()+" :statR: "+res.rate.getStateRate()+" :contR:"+res.rate.getCountryRate()
            	+" :combR:"+res.rate.getCombinedRate()+" :stdR:"+res.rate.getStandardRate()+" :reduR:"+res.rate.getReducedRate()+" :supredR:"+res.rate.getSuperReducedRate()
            	+" :add:"+res.rate.getCity()+":"+res.rate.getState()+":"+res.rate.getCountry());
            }
            if(taxRate != null) {
            	taxAmout = EcommUtil.getNormalRoundedValue(totalAmount*taxRate);
            }
            
            return taxAmout;
           
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static void main(String[] args) throws Exception {
    	Double orderTot = 1000.0;
		String zipCode = "111123123123123123";
		
    	//Double tax = calculateTaxAmount(orderTot, zipCode);
    	//System.out.println("final tax : "+tax);
	}
}
