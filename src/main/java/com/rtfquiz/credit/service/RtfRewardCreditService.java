package com.rtfquiz.credit.service;

import java.util.Date;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.utils.CassCustomerUtil;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.rtfquiz.webservices.data.RtfRewardConfig;
import com.rtfquiz.webservices.sqldao.implementation.CustomerRewardHistorySQLDAO;
import com.rtfquiz.webservices.utils.list.RewardObject;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.PollingCustomerRewards;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.jobs.CustomerUtil; 

public class RtfRewardCreditService {
	
	public static RewardObject creditRewards(RtfRewardConfig rtfRewardConfig, Integer customerId, Customer customerObj) {
		
		 Date now = new Date();
		 RewardObject object = new RewardObject();
		 object.setCustomerId(customerId);
		 
		 Integer lives = 0, magicWands =0, stars =0, rtfPoints =0;
		 Double rewardDollars = 0.0;
		
		 if(null != rtfRewardConfig.getNoOfLives() && rtfRewardConfig.getNoOfLives() > 0) {
			customerObj.setQuizCustomerLives(rtfRewardConfig.getNoOfLives());
			DAORegistry.getCustomerDAO().updateQuizCustomerLives(customerObj.getId(),rtfRewardConfig.getNoOfLives());
			CustomerUtil.updatedCustomerUtil(customerObj);
			//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
			try {
				CassCustomerUtil.updateCustomerLives(customerObj);
			}catch(Exception e) {
				e.printStackTrace();
			}
			lives = rtfRewardConfig.getNoOfLives();
		 }
		 
		 if(null != rtfRewardConfig.getNoOfSfStars() && rtfRewardConfig.getNoOfSfStars() > 0) {
			 
			 QuizSuperFanStat superFanStat = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(customerId);
			 if(superFanStat == null) {
				 superFanStat = new QuizSuperFanStat();
				 superFanStat.setCreatedDate(now);
				 superFanStat.setCustomerId(customerId);
				 superFanStat.setNoOfGamePlayed(1);
				 superFanStat.setParticipationChances(0);
				 
				 if(rtfRewardConfig.getSourceType().equals(SourceType.REFERRAL)) {
					 superFanStat.setOtherChances(0);
					 superFanStat.setReferralChances(rtfRewardConfig.getNoOfSfStars());
				 }else {
					 superFanStat.setReferralChances(0);
					 superFanStat.setOtherChances(rtfRewardConfig.getNoOfSfStars());
				 }
				 superFanStat.setStatus("ACTIVE");
				 superFanStat.setTotalNoOfChances(rtfRewardConfig.getNoOfSfStars());
				 superFanStat.setUpdatedDate(now);
			 }else {
				 if(rtfRewardConfig.getSourceType().equals(SourceType.REFERRAL)) {
					 superFanStat.setOtherChances(0);
					 superFanStat.setReferralChances(superFanStat.getReferralChances()+rtfRewardConfig.getNoOfSfStars());
				 }else {
					 superFanStat.setReferralChances(0);
					 superFanStat.setOtherChances(superFanStat.getOtherChances()+rtfRewardConfig.getNoOfSfStars());
				 }
				 superFanStat.setTotalNoOfChances(superFanStat.getTotalNoOfChances()+rtfRewardConfig.getNoOfSfStars());
				 superFanStat.setUpdatedDate(now);
			 }
			 
			 QuizDAORegistry.getQuizSuperFanStatDAO().saveOrUpdate(superFanStat);
			 
			 stars = rtfRewardConfig.getNoOfSfStars();
			 
			 object.setQuizSuperFanStat(superFanStat);;
		 }
		 
		 if(null != rtfRewardConfig.getNoOfMagicWands() && rtfRewardConfig.getNoOfMagicWands() > 0) {
			 
			 PollingCustomerRewards magicWandObj = DAORegistry.getPollingCustomerRewardsDAO().getCustomerMagicWandStat(customerId);
			 
			 if(magicWandObj == null) {
				 magicWandObj = new PollingCustomerRewards();
				 magicWandObj.setCustId(customerId);
				 magicWandObj.setMagicWand(rtfRewardConfig.getNoOfMagicWands());
			 }else {
				 magicWandObj.setMagicWand(magicWandObj.getMagicWand()+rtfRewardConfig.getNoOfMagicWands());
			 }
			 DAORegistry.getPollingCustomerRewardsDAO().saveOrUpdate(magicWandObj);
			 
			 magicWands = rtfRewardConfig.getNoOfMagicWands();
		 }
		 
		 if(null != rtfRewardConfig.getRtfPoints() && rtfRewardConfig.getRtfPoints() > 0) {
			 
			 DAORegistry.getCustomerDAO().updateQuizCustomerRtfPoints(customerId, rtfRewardConfig.getRtfPoints());
			 
			 rtfPoints = rtfRewardConfig.getRtfPoints();
		 }
		 
		 if(null != rtfRewardConfig.getRewardDollar() && rtfRewardConfig.getRewardDollar() > 0) {
			 
			CustomerLoyalty custLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
				
			if(custLoyalty != null) {
				CustomerLoyaltyTracking loyaltyTracking = new CustomerLoyaltyTracking();
				//participantLoyaltyTracking.setContestId(contest.getId());
				loyaltyTracking.setCreatedBy(rtfRewardConfig.getSourceType().toString());
				loyaltyTracking.setCreatedDate(new Date());
				loyaltyTracking.setCustomerId(customerId);
				loyaltyTracking.setRewardPoints(rtfRewardConfig.getRewardDollar());
				loyaltyTracking.setRewardType(rtfRewardConfig.getSourceType().toString());
				
				custLoyalty.setActivePointsAsDouble(custLoyalty.getActivePointsAsDouble() + rtfRewardConfig.getRewardDollar());
				custLoyalty.setDollerConversion(1.00);
				custLoyalty.setLastUpdate(new Date());
				custLoyalty.setLatestEarnedPointsAsDouble(rtfRewardConfig.getRewardDollar());
				custLoyalty.setLatestSpentPointsAsDouble(0.00);
				custLoyalty.setTotalEarnedPoints(custLoyalty.getTotalEarnedPoints()+rtfRewardConfig.getRewardDollar());
				custLoyalty.setNotifiedMessage(rtfRewardConfig.getSourceType().toString()+": "+rtfRewardConfig.getRewardDollar());
				 
				DAORegistry.getCustomerLoyaltyTrackingDAO().save(loyaltyTracking);
				DAORegistry.getCustomerLoyaltyDAO().update(custLoyalty);
				
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customerId,custLoyalty.getActivePointsAsDouble());
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			rewardDollars = rtfRewardConfig.getRewardDollar();
			 
		 }
		 
		 try {
			 CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(customerId, "CR", rtfRewardConfig.getSourceType(), null, lives, magicWands, stars, rtfPoints, rewardDollars, "");
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
		 
		 return object;
		
	}
	
	public static RewardObject creditRewardsWithQty(RtfRewardConfig rtfRewardConfig, Integer customerId, Customer customerObj,Integer quantity) {
		
		 Date now = new Date();
		 RewardObject object = new RewardObject();
		 object.setCustomerId(customerId);
		 
		 Integer lives = 0, magicWands =0, stars =0, rtfPoints =0;
		 Double rewardDollars = 0.0;
		
		 if(null != rtfRewardConfig.getNoOfLives() && rtfRewardConfig.getNoOfLives() > 0) {
			 lives =rtfRewardConfig.getNoOfLives() * quantity;
			customerObj.setQuizCustomerLives(customerObj.getQuizCustomerLives() + lives);
			DAORegistry.getCustomerDAO().updateQuizCustomerLives(customerObj.getId(),lives);
			CustomerUtil.updatedCustomerUtil(customerObj);
			//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
			try {
				CassCustomerUtil.updateCustomerLives(customerObj);
			}catch(Exception e) {
				e.printStackTrace();
			}
		 }
		 
		 if(null != rtfRewardConfig.getNoOfSfStars() && rtfRewardConfig.getNoOfSfStars() > 0) {
			 stars = rtfRewardConfig.getNoOfSfStars() * quantity;
			 QuizSuperFanStat superFanStat = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(customerId);
			 if(superFanStat == null) {
				 superFanStat = new QuizSuperFanStat();
				 superFanStat.setCreatedDate(now);
				 superFanStat.setCustomerId(customerId);
				 superFanStat.setNoOfGamePlayed(1);
				 superFanStat.setParticipationChances(0);
				 
				 if(rtfRewardConfig.getSourceType().equals(SourceType.REFERRAL)) {
					 superFanStat.setOtherChances(0);
					 superFanStat.setReferralChances(stars);
				 }else {
					 superFanStat.setReferralChances(0);
					 superFanStat.setOtherChances(stars);
				 }
				 superFanStat.setStatus("ACTIVE");
				 superFanStat.setTotalNoOfChances(stars);
				 superFanStat.setUpdatedDate(now);
			 }else {
				 if(rtfRewardConfig.getSourceType().equals(SourceType.REFERRAL)) {
					 superFanStat.setOtherChances(0);
					 superFanStat.setReferralChances(superFanStat.getReferralChances()+stars);
				 }else {
					 superFanStat.setReferralChances(0);
					 superFanStat.setOtherChances(superFanStat.getOtherChances()+stars);
				 }
				 superFanStat.setTotalNoOfChances(superFanStat.getTotalNoOfChances()+stars);
				 superFanStat.setUpdatedDate(now);
			 }
			 
			 QuizDAORegistry.getQuizSuperFanStatDAO().saveOrUpdate(superFanStat);
			 
			 object.setQuizSuperFanStat(superFanStat);
		 }
		 
		 if(null != rtfRewardConfig.getNoOfMagicWands() && rtfRewardConfig.getNoOfMagicWands() > 0) {
			 
			 magicWands = rtfRewardConfig.getNoOfMagicWands() * quantity;
			 
			 PollingCustomerRewards magicWandObj = DAORegistry.getPollingCustomerRewardsDAO().getCustomerMagicWandStat(customerId);
			 
			 if(magicWandObj == null) {
				 magicWandObj = new PollingCustomerRewards();
				 magicWandObj.setCustId(customerId);
				 magicWandObj.setMagicWand(stars);
			 }else {
				 magicWandObj.setMagicWand(magicWandObj.getMagicWand()+stars);
			 }
			 DAORegistry.getPollingCustomerRewardsDAO().saveOrUpdate(magicWandObj);
			 
			 
		 }
		 
		 if(null != rtfRewardConfig.getRtfPoints() && rtfRewardConfig.getRtfPoints() > 0) {
			 
			 rtfPoints = rtfRewardConfig.getRtfPoints() * quantity;;
			 DAORegistry.getCustomerDAO().updateQuizCustomerRtfPoints(customerId, rtfPoints);
		 }
		 
		 if(null != rtfRewardConfig.getRewardDollar() && rtfRewardConfig.getRewardDollar() > 0) {
			 rewardDollars = rtfRewardConfig.getRewardDollar()*quantity;
			
			 CustomerLoyalty custLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(custLoyalty != null) {
				CustomerLoyaltyTracking loyaltyTracking = new CustomerLoyaltyTracking();
				//participantLoyaltyTracking.setContestId(contest.getId());
				loyaltyTracking.setCreatedBy(rtfRewardConfig.getSourceType().toString());
				loyaltyTracking.setCreatedDate(new Date());
				loyaltyTracking.setCustomerId(customerId);
				loyaltyTracking.setRewardPoints(rewardDollars);
				loyaltyTracking.setRewardType(rtfRewardConfig.getSourceType().toString());
				
				custLoyalty.setActivePointsAsDouble(custLoyalty.getActivePointsAsDouble() + rewardDollars);
				custLoyalty.setDollerConversion(1.00);
				custLoyalty.setLastUpdate(new Date());
				custLoyalty.setLatestEarnedPointsAsDouble(rewardDollars);
				custLoyalty.setLatestSpentPointsAsDouble(0.00);
				custLoyalty.setTotalEarnedPoints(custLoyalty.getTotalEarnedPoints()+rewardDollars);
				custLoyalty.setNotifiedMessage(rtfRewardConfig.getSourceType().toString()+": "+rewardDollars);
				 
				DAORegistry.getCustomerLoyaltyTrackingDAO().save(loyaltyTracking);
				DAORegistry.getCustomerLoyaltyDAO().update(custLoyalty);
				
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customerId,custLoyalty.getActivePointsAsDouble());
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			 
		 }
		 
		 try {
			 CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(customerId, "CR", rtfRewardConfig.getSourceType(), null, lives, magicWands, stars, rtfPoints, rewardDollars, "");
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
		 
		 return object;
		
	}
	
	
	/*
	 * (Integer customerId, String trxType, SourceType sourceType, Integer
	 * sourceRefId, Integer lives,Integer magicWands, Integer sfStars, Integer
	 * rtfPoints, Double rewardDollar, String description)
	 */
	/*public static Integer allotRandomMediaRewardsToCustomer(Customer customer, RtfRewardConfig rtfRewardConfig) {
		Integer status = 0;

		Integer rwdConfigPts = 0;
		Integer rwdLife = 0;
		Integer rwdStars = 0;
		Integer magicWands = 0;
		Double rwdDollars = 0.00;
		List<String> rwdTypeList = new ArrayList<String>();

		try {
			 
			if (rtfRewardConfig == null) {
				rwdConfigPts = 0;
				throw new NoRewardConfigException();
			} else {
				rwdConfigPts = rtfRewardConfig.getRtfPoints();
				rwdLife = rtfRewardConfig.getNoOfLives();
				magicWands = rtfRewardConfig.getNoOfMagicWands();
				rwdStars = rtfRewardConfig.getNoOfSfStars();
			}

			if (rwdConfigPts != null && rwdConfigPts > 0) {
				rwdTypeList.add(PollingUtil.REWARD_RTFPOINTS);
			} 
			if (rwdLife != null && rwdLife > 0) {
				rwdTypeList.add(PollingUtil.REWARD_LIFE);
			} 
			if (rwdStars != null && rwdStars > 0) {
				rwdTypeList.add(PollingUtil.REWARD_SFSTARS);
			} 
			if (magicWands != null && magicWands > 0) {
				rwdTypeList.add(PollingUtil.REWARD_MAGICWAND);
			}

			if (rwdTypeList.size() == 0) {
				throw new NoRewardConfigException();
			}
			Collections.shuffle(rwdTypeList);
			String rewardTypeSelected = rwdTypeList.get(0);

			if (PollingUtil.REWARD_RTFPOINTS.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currRtfPoints = customer.getRtfPoints();
				if (currRtfPoints == null)
					currRtfPoints = 0;
				Integer cassFinalPts = currRtfPoints + rwdConfigPts;
				status = updateRTFPointsforCustomer(customer.getId(), sqlPts, cassFinalPts);

				try {
					if (status == 1) {
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, 0, 0, 0,
								0, rwdConfigPts, 0.0, " Video Id: " + videoInfo.getmId());
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			if (PollingUtil.REWARD_LIFE.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currLives = customer.getqLive();
				if (currLives == null)
					currLives = 0;
				Integer cassFinalLives = currLives + rwdConfigPts;
				status = updateRTFLivesforCustomer(customer.getId(), sqlPts, cassFinalLives);

				try {
					if (status == 1) {
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, 0,
								rwdConfigPts, 0, 0, 0, 0.0, " Video Id: " + videoInfo.getmId());
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			if (PollingUtil.REWARD_MAGICWAND.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currMw = customer.getMw();
				if (currMw == null)
					currMw = 0;
				Integer cassFinalLives = currMw + rwdConfigPts;
				status = updateRTFMagicWandforCustomer(customer.getId(), sqlPts, cassFinalLives);

				try {
					if (status == 1) {
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, 0, 0,
								rwdConfigPts, 0, 0, 0.0, " Video Id: " + videoInfo.getmId());
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			if (PollingUtil.REWARD_SFSTARS.equals(rewardTypeSelected)) {

				Integer sqlPts = rwdConfigPts;
				Integer currMw = customer.getMw();
				if (currMw == null)
					currMw = 0;
				Integer cassFinalLives = currMw + rwdConfigPts;
				status = updateRTFLivesforCustomer(customer.getId(), sqlPts, cassFinalLives);

				try {
					if (status == 1) {
						SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, 0, 0, 0,
								rwdConfigPts, 0, 0.0, " Video Id: " + videoInfo.getmId());
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		} catch (NoRewardConfigException noRewEx) {
			CassError err = new CassError();
			err.setDesc("Please configure  rewards for " + sourceType.toString());
			videoInfo.setErr(err);
			videoInfo.setSts(0);
			status = 0;
			return 0;
		}

		catch (Exception ex) {
			ex.printStackTrace();
			status = 0;
			return 0;
		} finally {
			try {
				
				 * SQLDaoUtil.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, 0,
				 * 0, 0, 0, rwdConfigPts, 0.0, "TFF Approved uploaded Video Id: " +
				 * videoInfo.getmId());
				 
				} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return status;
	}

	private static Integer updateRTFLivesforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalStat) {
		Integer sts = CustomerRewardCreditService.sqlCreditToCustomerLives(cuId, sqlPts);
		if (sts > 0) {
			sts = CustomerRewardCreditService.cassCreditToCustomerLives(cuId, cassFinalStat);
		}
		return sts;

	}

	private static Integer updateRTFPointsforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalPts) {
		Integer sts = CustomerRewardCreditService.sqlCreditToCustomerRTFPoints(cuId, sqlPts);
		if (sts > 0) {
			sts = CustomerRewardCreditService.cassCreditCustomerRTFPoints(cuId, cassFinalPts);
		}
		return sts;
	}

	private static Integer updateRTFMagicWandforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalPts) {
		Integer sts = CustomerRewardCreditService.creditCustomerRTFMagicWands(cuId, cassFinalPts, sqlPts);
		return sts;
	}

	private static Integer updateRTFSFStarforCustomer(Integer cuId, Integer sqlPts, Integer cassFinalPts) {
		Integer sts = CustomerRewardCreditService.creditCustomerRTFSuperFanStar(cuId, cassFinalPts, sqlPts);
		return sts;
	}

	public static PollingVideoInfo fetchCustomerLikedVideos(Integer cuId, PollingVideoInfo pollingVideoInfo) {

		Map<Integer, MyVideoInfo> videoInfoMap = PollingSQLDaoUtil.fetchCustLikedVideos(cuId);
		if (videoInfoMap == null || videoInfoMap.size() == 0)
			return pollingVideoInfo;
		List<Integer> vIdList = videoInfoMap.keySet().stream().collect(Collectors.toList());
		// id , url , viewcount , 1 , likecount , sponsorlogo
		// fetch liked videos id and url ;
		// fetch liked count , viewed count

		Map<Integer, Integer> vwCntMap = PollingSQLDaoUtil.getVideoViewCount(vIdList);
		Map<Integer, Integer> lkCntMap = PollingSQLDaoUtil.getVideoLikedCount(vIdList, 0);
		for (Integer id : vIdList) {

			if (videoInfoMap.containsKey(id)) {
				((MyVideoInfo) videoInfoMap.get(id)).setVwCnt(vwCntMap.get(id));
				((MyVideoInfo) videoInfoMap.get(id)).setLkCnt(lkCntMap.get(id));
				String vidInfoStr = ((MyVideoInfo) videoInfoMap.get(id)).getRtfTvMediaDets();
				System.out.println("==vidInfoStr==" + vidInfoStr);
				((MyVideoInfo) videoInfoMap.get(id)).setRtfTvMediaDets(vidInfoStr);

			}

		}
		List<MyVideoInfo> vidInfoList = new ArrayList<MyVideoInfo>(videoInfoMap.values());
		pollingVideoInfo.setMyVidInfoList(vidInfoList);
		return pollingVideoInfo;
	}

	public static void main(String a[]) {
		PollingVideoInfo pollingVideoInfo = fetchCustomerLikedVideos(351111, new PollingVideoInfo());
		System.out.println(pollingVideoInfo);
		
	
		
	}*/

}
