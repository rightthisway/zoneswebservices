package com.rtfquiz.jdbc.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.zonesws.webservices.data.WebServiceTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.WebServiceActionType;

public class JDBCConnection {
	
	static Connection quizConnection;
	static Connection rtfConnection;
	
	public static void main(String[] args) throws SQLException {
		/*QuizCustomerContestAnswers obj = new QuizCustomerContestAnswers();
		obj.setAnswer("A");
		obj.setAnswerRewards(0.55);
		obj.setContestId(1001);
		obj.setCumulativeLifeLineUsed(5);
		obj.setCumulativeRewards(0.55);
		obj.setCustomerId(334437);
		obj.setIsCorrectAnswer(true);
		obj.setIsLifeLineUsed(true);
		obj.setQuestionId(150);
		obj.setQuestionSNo(1);*/
		//save(obj);
		
		WebServiceTracking obj = new WebServiceTracking();
		saveTracking(obj);;
	}
	
	public static void saveTracking(WebServiceTracking obj) throws SQLException {
		
		obj.setProductType(ProductType.REWARDTHEFAN);
		obj.setWebXSign("TESTING");
		obj.setPlatForm(ApplicationPlatForm.valueOf("IOS"));
		obj.setSessionId("TESTING");
		obj.setWebConfigId("TESTING");
		obj.setServerIpAddress("192.168.0.1");
		obj.setCustomerIpAddress("192.168.1.1");
		obj.setActionType(WebServiceActionType.QUIZVALIDATEANSWERS);
		obj.setAuthenticatedHit("Yes");
		obj.setContestId("123");
		obj.setCustomerId(12345);
		
		String actionResult = "Success :qNo : 1 :qId: 19 :ans:A :isCont:Y";
		obj.setActionResult(actionResult);
		
		String query = "INSERT INTO web_service_tracking_new(product_type,hit_date,server_ip_address,customer_ip_address,api_name,platform,session_id,"
				+ "authenticated_hit,action_result,customer_id,web_config_id,web_xsign,contest_id) VALUES ('"+ProductType.REWARDTHEFAN.toString()+"',"
				+ "getdate(),'"+obj.getServerIpAddress()+"','"+obj.getCustomerIpAddress()+"',"
				+ "'"+obj.getActionType().toString()+"','"+obj.getPlatForm().toString()+"','"+obj.getSessionId()+"','Yes','"+obj.getActionResult()+"',"
				+ ""+obj.getCustomerId()+",'"+obj.getWebConfigId()+"','"+obj.getWebXSign()+"',"+obj.getContestId()+" )";
		
		//Connection connection = getConnection();
		Connection connection = JDBCConnection.getRtfConnection();  
		
		ResultSet rs= null;
		Statement insertStatement =null;
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(query);
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
		
		 
		
	}
	
	
	
public static void save(QuizCustomerContestAnswers obj) throws SQLException {
		
		int isCrtAns = obj.getIsCorrectAnswer() ? 1 : 0;
		int isLifeApplied = obj.getIsLifeLineUsed() ? 1 : 0;
		
		String query = "INSERT INTO customer_contest_answers (customer_id,contest_id,question_id,question_sl_no,answer,created_datetime,is_correct_answer,"
				+ "is_lifeline_used,updated_datetime,answer_rewards,cumulative_rewards,cumulative_lifeline_used) "
				+ "VALUES ("+obj.getCustomerId()+","+obj.getContestId()+","+obj.getQuestionId()+","+obj.getQuestionSNo()+","
				+ "'"+obj.getAnswer()+"',getdate(),"+isCrtAns+","+isLifeApplied+",getdate()"
				+ ","+obj.getAnswerRewards()+","+obj.getCumulativeRewards()+","+obj.getCumulativeLifeLineUsed()+" )";
		
		//Connection connection = getConnection();
		Connection connection = JDBCConnection.getQuizConnection();  
		
		ResultSet rs= null;
		Statement insertStatement =null;
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			rs = insertStatement.getGeneratedKeys();
			Integer id =null;
			if(rs.next()){
				id = rs.getInt(1);
			}
			connection.commit();
			System.out.println("Generated ID : "+id);
			
			 
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
	}

	
	
 
	
	public static Connection getQuizConnection(){
		
		 Connection connection = null;
		/*if(quizConnection!=null){
			return quizConnection;
		}
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tn1.db.url");
		String username =resourceBundle.getString("tn1.db.user.name");
		String password =resourceBundle.getString("tn1.db.password");*/
		
		/*String url ="jdbc:sqlserver://10.0.1.51:1433;database=RTFQuizMaster_Sanbox;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";
		
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			quizConnection = DriverManager.getConnection(url, username, password);
			return quizConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
		*
		*
		*
		*/
		 
		 Connection rtfquizconnection = null;
		try {		
		
		InitialContext initialContext = new InitialContext();
        Context context = (Context) initialContext.lookup("java:comp/env");
        //The JDBC Data source that we just created
        DataSource ds = (DataSource) context.lookup("rtfquizconpool");
        rtfquizconnection = ds.getConnection();
        
        System.out.println("--  rtfquizconpool   datasource --" + ds   );
		
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		
		
		return 	rtfquizconnection;
		
	}
	
	
	public static Connection getRtfConnection(){
	/*	if(rtfConnection!=null){
			return rtfConnection;
		}
		ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tn1.db.url");
		String username =resourceBundle.getString("tn1.db.user.name");
		String password =resourceBundle.getString("tn1.db.password");
		
		String url ="jdbc:sqlserver://10.0.1.51:1433;database=ZonesApiPlatformV2_Sanbox;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";
		
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtfConnection = DriverManager.getConnection(url, username, password);
			return rtfConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}*/
		
		Connection rtfapiconnection = null;
		
		try {		
			
			InitialContext initialContext = new InitialContext();
	        Context context = (Context) initialContext.lookup("java:comp/env");
	        //The JDBC Data source that we just created
	        DataSource ds = (DataSource) context.lookup("rtfapiconpool");
	        rtfapiconnection = ds.getConnection();
	        
	        System.out.println("-- rtfapiconnection  datasource --" + ds);
			
			}catch(SQLException sqlEx){
				sqlEx.printStackTrace();
				return null;
			}catch(Exception ex){
				ex.printStackTrace();
				return null;
			}
		
		
		
		return rtfapiconnection;
		
		
		
		
		
	}
}
