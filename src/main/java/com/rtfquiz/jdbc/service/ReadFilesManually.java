package com.rtfquiz.jdbc.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ReadFilesManually {
	
	static Connection rtfConnection;
	
	public static void main(String[] args) throws Exception {
	  File file = new File("C:\\Code\\UserIds2.txt");
  	  BufferedReader br = new BufferedReader(new FileReader(file));
  	  String st;
  	  List<String> phoneNumbers = new ArrayList<String>();
  	  while ((st = br.readLine()) != null) {
  	    phoneNumbers.add(st);
  	  }
  	  int i=1;
  	  for (String email : phoneNumbers) {
  		  System.out.println("-----------------------------"+i+":"+email+"-------------------------------------");
  		  try {
  			save(email);
  		  }catch(Exception e) {
  			  e.printStackTrace();
  		  }
  		System.out.println("-----------------------------"+i+":"+email+"-------------------------------------");
  		i++;
	 } 
	}
	 
	
public static void save(String email) throws SQLException {
		String query = "INSERT INTO customer_temp_ulaga(code) VALUES('"+email+"')";
		Connection connection = ReadFilesManually.getRtfConnection();  
		ResultSet rs= null;
		Statement insertStatement =null;
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(query);
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			throw e;
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
	}
	
	public static Connection getRtfConnection(){
		if(rtfConnection!=null){
			return rtfConnection;
		}
		String url ="jdbc:sqlserver://54.175.222.230:1433;database=ZonesApiPlatformV2;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtfConnection = DriverManager.getConnection(url, username, password);
			return rtfConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		 
	}
}
