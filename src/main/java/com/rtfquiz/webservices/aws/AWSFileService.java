package com.rtfquiz.webservices.aws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.Copy;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;

public class AWSFileService {

	public static  String  getAWSBaseURL () {
		return awsHttpURL;
	}
	
	private static String awsHttpURL = "http://d2vtn8rh18hkyv.cloudfront.net/";
	private static String AK = "AKIARJICTKFNN5SWY6VE";
	private static String ASA = "jF1WVZqR7n4wNkPJB3bLIqpg9BgcpxvbDX+JplOp";
	public static String BUCKET_NAME_RTFMEDIA = "rtfservermedia";
	public static String CUSTOMER_DP_DIRECTORY = "dp";
	public static String GF_CARD_BRAND_DIRECTORY = "gfc";
	public static String awsS3Url = "https://rtfservermedia.s3.amazonaws.com/";
	
	public static String BUCKET_NAME_RTFASSESTS = "rtfassetz";
	public static String TICKET_FILES_DIRECTORY = "tixfile";
	public static String GIFTCARD_FILES_DIRECTORY = "gfcfile";
	public static String RTFPROD_FOLDER = "dpd";
	
	public static void main(String[] args) {
		File fileObj = new File("C:\\Tomcat 7.0\\webapps\\SvgMaps\\CUSTOMER_DP\\REWARDTHEFAN_DP_334437.jpg");
		//System.out.println(fileObj.exists());
		//upLoadFileToS3(BUCKET_NAME_RTFMEDIA, "REWARDTHEFAN_DP_334437.jpg", CUSTOMER_DP_DIRECTORY, fileObj);
		/*String ext = FilenameUtils.getExtension("REWARDTHEFAN_DP_334437.jpg");
		System.out.println(ext);*/
		//deleteFilesFromS3(BUCKET_NAME_RTFMEDIA, "222222.jpg", CUSTOMER_DP_DIRECTORY);
		AwsS3Response awsS3Response = 	transferCustomerMediaFileToRTFMedia
		("rtfpublicupload", "generaluploads/334424_1562612116722.mp4", "rtfmedia", "tmp/334424_1562612116722.mp4");
		
		System.out.println("Response is " + awsS3Response.getStatus());
		
	}
	
	public static String getGiftCardBrandImage(Integer index) {
		return "https://rtfservermedia.s3.amazonaws.com/gfc/"+index+".png";
	}
	
	public static String getGiftCardBrandImage(String imageName) {
		return awsS3Url+GF_CARD_BRAND_DIRECTORY+"/"+imageName;
	}
	
	
	public static AwsS3Response transferCustomerMediaFileToRTFMedia
	(String from_bucket, String from_Path, String to_bucket, String to_Path) {		
		
		AwsS3Response awsS3Response = new AwsS3Response();		
		AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);
		AmazonS3 s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
				.build();
		
		TransferManager xfer_mgr = TransferManagerBuilder.standard().withS3Client(s3client).build();
		try {
		    Copy xfer = xfer_mgr.copy(from_bucket, from_Path, to_bucket, to_Path);
		   
		    xfer.waitForCompletion();
		     if (xfer.isDone())  {
		    	 awsS3Response.setStatus(1);
		    	 awsS3Response.setFullFilePath(to_Path);
		    	 awsS3Response.setDesc("File has been Transferred Successfully");
		     }
			
		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while Transferring file");
			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
			xfer_mgr.shutdownNow();
		} catch (AmazonClientException e) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while Transferring file");
			System.out.println("Caught an AmazonClientException while Transferring file " + e.getStackTrace() );
			xfer_mgr.shutdownNow();
		} catch (InterruptedException e) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while Transferring file");
			System.out.println("Caught an InterruptedException while Transferring file " + e.getStackTrace() );
			xfer_mgr.shutdownNow();
		}
		catch (Exception generalEx) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while Transferring file");			
			System.out.println("Caught an General Error while Transferring file " + generalEx.getStackTrace() );
			xfer_mgr.shutdownNow();
		}
		xfer_mgr.shutdownNow();
		return awsS3Response;
	}
	
	
	
	
	
	public static File convertMultiPartToFile(MultipartFile file) throws IOException {
	    File convFile = new File(file.getOriginalFilename());
	    FileOutputStream fos = new FileOutputStream(convFile);
	    fos.write(file.getBytes());
	    fos.close();
	    return convFile;
	}

	public static AwsS3Response upLoadFileToS3(String bucketName , String fileName, String folderName, File fileObj) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();
			String fileNameToUpload = folderName + "/" + fileName;
			s3client.putObject(new PutObjectRequest(bucketName, fileNameToUpload, fileObj));

			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(fileNameToUpload);
			awsS3Response.setDesc("File has been Uploaded Successfully");

		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while uploading file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while uploading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while uploading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}
	 

	public static AwsS3Response downLoadFileFromS3(String bucketName,String folderName, String fileName, String savePath) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			String downLoadFullPath = savePath + fileName;
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, folderName + "/" + fileName));
			FileUtils.copyInputStreamToFile(fileObject.getObjectContent(), new File(downLoadFullPath));

			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(downLoadFullPath);

		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}
	
	
	
	public static AwsS3Response downloadFilesToSharedDrive( String userName , String fileName, Boolean isWinnerUpload) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {
			
			String folderName = "FOLDER NAME";
			
			 
			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			String downLoadFullPath = "RTFCUSTMEDIA_DIRECTORY" + userName+"-"+fileName;
			S3Object fileObject = s3client.getObject(new GetObjectRequest(BUCKET_NAME_RTFMEDIA, folderName + "/" + fileName));
			FileUtils.copyInputStreamToFile(fileObject.getObjectContent(), new File(downLoadFullPath));

			awsS3Response.setStatus(1);
			awsS3Response.setFullFilePath(downLoadFullPath);
			
			
			/*AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			String downLoadFullPath = ShareDriveUtil.sharedDrivePath + fileName;
			
			S3Object fileObject = s3client.getObject(new GetObjectRequest(BUCKET_NAME_PUBLIC_UPLOAD, folderName + "/" + fileName));
			
			SmbFile newFile = ShareDriveUtil.getSmbFile(downLoadFullPath);
			  
			InputStream reader = new BufferedInputStream(fileObject.getObjectContent());
			OutputStream writer = new BufferedOutputStream(newFile.getOutputStream());

			int read = -1;

			while ( ( read = reader.read() ) != -1 ) {
			    writer.write(read);
			}

			writer.flush();
			writer.close();
			reader.close();*/
		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}
	
	public static AwsS3Response deleteFilesFromS3(String bucketName, String fileName, String folderName) {
		AwsS3Response awsS3Response = new AwsS3Response();
		try {

			AWSCredentials credentials = new BasicAWSCredentials(AK, ASA);

			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();

			
			s3client.deleteObject(new DeleteObjectRequest(bucketName, folderName + "/" + fileName));
			
			awsS3Response.setStatus(1);
			
		} catch (AmazonServiceException ase) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while deleting file");

			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		} catch (Exception ex) {
			awsS3Response.setStatus(0);
			awsS3Response.setDesc("Something went wrong while downloading file");
			ex.printStackTrace();
		}
		return awsS3Response;

	}


}