package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.ContestEvent;

public class ContestEventDAO extends HibernateDAO<Integer, ContestEvent> implements com.rtfquiz.webservices.dao.services.ContestEventDAO{

	public List<ContestEvent> getContestEvents(Integer contestId){
		return find("FROM ContestEvent where eventId = ?", new Object[]{contestId});
	}
}
