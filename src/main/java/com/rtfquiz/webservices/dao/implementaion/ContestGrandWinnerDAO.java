package com.rtfquiz.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.rtfquiz.webservices.enums.WinnerType;

public class ContestGrandWinnerDAO extends HibernateDAO<Integer, ContestGrandWinner> implements com.rtfquiz.webservices.dao.services.ContestGrandWinnerDAO{

	public ContestGrandWinner getGrandWinnerByCustomerId(Integer customerId,Integer contestId) {
		return findSingle("FROM ContestGrandWinner WHERE status=? and customerId=? AND contestId=? ",new Object[]{WinnerStatus.ACTIVE,customerId,contestId});
	}
	
	public List<ContestGrandWinner> getRecentContentWinners() {
		return find("FROM ContestGrandWinner WHERE status=? and isNotified=? ",new Object[]{WinnerStatus.ACTIVE,Boolean.FALSE});
	}
	
	public void updateWinnerNotification(Integer id) {
		bulkUpdate("UPDATE ContestGrandWinner SET isNotified=?, notifiedTime=? WHERE id=?",new Object[]{Boolean.TRUE,new Date(),id});
	}
	
	public List<ContestGrandWinner> getAllActiveGrandWinnerContest() {
		return find("FROM ContestGrandWinner WHERE status=? and orderId is null ",new Object[]{WinnerStatus.ACTIVE});
	}
	
	public ContestGrandWinner getOrderNotCreatedGrandWinner(Integer contestId) {
		return findSingle("FROM ContestGrandWinner WHERE status=? and orderId is not null and contestId=? ",new Object[]{WinnerStatus.ORDERED,contestId});
	}
	
	public List<ContestGrandWinner> getAllJackpotWinnersByContestId(Integer contestId) {
		return find("FROM ContestGrandWinner WHERE status=? and contestId=? and winnerType in (?,?)",
				new Object[]{WinnerStatus.ACTIVE,contestId,WinnerType.MINIJACKPOT,WinnerType.MEGAJACKPOT});
	}

	
	public List<ContestGrandWinner> getAllProcessedJackpotWinnersByContestId(Integer contestId) {
		return find("FROM ContestGrandWinner WHERE status=? and contestId=? and winnerType in (?,?)",
				new Object[]{WinnerStatus.PROCESSED,contestId,WinnerType.MINIJACKPOT,WinnerType.MEGAJACKPOT});
	}

} 
