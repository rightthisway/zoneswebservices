package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;

public class ContestReferrerRewardCreditDAO extends HibernateDAO<Integer, ContestReferrerRewardCredit> implements com.rtfquiz.webservices.dao.services.ContestReferrerRewardCreditDAO{
	
	public ContestReferrerRewardCredit getRewardCreditByContestId(Integer contestId) {
		return findSingle("FROM ContestReferrerRewardCredit WHERE contestId=? ",new Object[]{contestId});
	}
	
	public ContestReferrerRewardCredit getAffiliateRewardCreditByContestId(Integer contestId) {
		return findSingle("FROM ContestReferrerRewardCredit WHERE contestId=? and referralProgramType=? ",new Object[]{contestId,"AFFILIATE"});
	}
	
	public ContestReferrerRewardCredit getRewardCreditByContestIdAndType(Integer contestId, String rewardType) {
		return findSingle("FROM ContestReferrerRewardCredit WHERE contestId=? and referralProgramType=? ",new Object[]{contestId,rewardType});
	}
	
	public List<ContestReferrerRewardCredit> getAllContestCredits(Integer contestId) {
		return find("FROM ContestReferrerRewardCredit WHERE contestId=?",new Object[]{contestId});
	}
	

}
