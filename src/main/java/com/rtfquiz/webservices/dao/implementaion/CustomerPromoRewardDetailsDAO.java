package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.CustomerPromoRewardDetails;
import com.rtfquiz.webservices.enums.PromoType;

public class CustomerPromoRewardDetailsDAO extends HibernateDAO<Integer, CustomerPromoRewardDetails> implements com.rtfquiz.webservices.dao.services.CustomerPromoRewardDetailsDAO{

	public CustomerPromoRewardDetails getEarnLifePromoRewardByPromoRefIdandCustomerId(Integer promoRewardId,Integer customerId) {
		return findSingle("FROM CustomerPromoRewardDetails WHERE id=? and customerId=? and promoType=? ",new Object[]{promoRewardId,customerId,PromoType.REWARD_EARNLIFE.toString()});
	}
	
	public CustomerPromoRewardDetails getRewardTixPromoRewardByPromoRefIdandCustomerId(Integer promoRewardId,Integer customerId) {
		return findSingle("FROM CustomerPromoRewardDetails WHERE id=? and customerId=? and promoType=? ",new Object[]{promoRewardId,customerId,PromoType.REWARD_FREE_TIX.toString()});
	}
	
	public List<CustomerPromoRewardDetails> getAllActiveCustomerPromoRewardDetailsByCustomerId(Integer customerId) {
		return find("FROM CustomerPromoRewardDetails WHERE status='ACTIVE' and customerId=? ",new Object[]{customerId});
	}

}
