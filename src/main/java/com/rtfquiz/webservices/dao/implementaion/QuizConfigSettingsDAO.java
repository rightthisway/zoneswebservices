package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizConfigSettings;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizCustomer;

/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class QuizConfigSettingsDAO extends HibernateDAO<Integer, QuizConfigSettings> implements com.rtfquiz.webservices.dao.services.QuizConfigSettingsDAO {
	
	public QuizConfigSettings getConfigSettings() {
		return findSingle("from QuizConfigSettings where id = 1 ", new Object[]{});
	}
}
