package com.rtfquiz.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.rtfquiz.webservices.data.QuizContest;

/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class QuizContestDAO extends HibernateDAO<Integer, QuizContest> implements com.rtfquiz.webservices.dao.services.QuizContestDAO {
	
	public QuizContest getNextQuizContest(Date todayDate,String contestType) {
		return findSingle("from QuizContest where status in ('ACTIVE','STARTED') and contestStartDateTime>? and contestType=? order by contestStartDateTime ", new Object[]{todayDate,contestType});
	}
	public QuizContest getCurrentStartedContest(String contestType) {
		return findSingle("from QuizContest where status ='STARTED' and contestType=? order by contestStartDateTime ", new Object[]{contestType});
	}
	public List<QuizContest> getAllMobileContestsToSendNotification(Date fifteenMinsDate,Date threeMinsDate) {
		return find("from QuizContest where status in ('ACTIVE','STARTED') and contestType='MOBILE' and (contestStartDateTime=? or contestStartDateTime=?) order by contestStartDateTime ", new Object[]{threeMinsDate,fifteenMinsDate});
	}
	public List<QuizContest> getAllMobileContestsToSendNotification(Date threeMinsDate) {
		return find("from QuizContest where status in ('ACTIVE','STARTED') and contestType='MOBILE' and contestStartDateTime=? order by contestStartDateTime ", new Object[]{threeMinsDate});
	}
	
	public List<QuizContest> getAllMobileContestByDate(Date fromDate,Date toDate) {
		return find("from QuizContest where contestType='MOBILE' and contestStartDateTime between ? and ? order by contestStartDateTime ", 
				new Object[]{fromDate,toDate});
	}
	
	public List<QuizContest> getUpCommingQuizContests(Date todayDate,String contestType) {
		Session session = null;
		List<QuizContest> list = null;
		try {
			session = getSession();
			Query query = session.createQuery(" from QuizContest where status in ('ACTIVE','STARTED') and contestStartDateTime>? and contestType=? order by contestStartDateTime ");
			query.setTimestamp(0, todayDate);
			query.setString(1, contestType);
			query.setFirstResult(0);
			query.setMaxResults(1);
			list = query.list();
			session.close();
			
		} catch (Exception e) {
			if(session != null && session.isOpen()) {
				session.close();	
			}
		}
		return list;
	}
	
	public List<QuizContest> getRecentQuizContest() {
		return find("from QuizContest where id > 148 and contestName not like '%test%' and status not in ('DELETED') ",new Object[]{});
		//return find("from QuizContest order by id desc",new Object[]{});
	}
	
	public List<QuizContest> getRecentQuizContest(Date fromDate, Date toDate) {
		return find("from QuizContest where status not in ('DELETED') and contestName not like '%test%' and "
				+ "(contestStartDateTime=? or contestStartDateTime=?) order by contestStartDateTime ", 
				new Object[]{fromDate,toDate});
	}
	
	public List<QuizContest> getUpcomingContest(Date fromDate, Date toDate) {
		return find("from QuizContest where status not in ('DELETED') and "
				+ "(contestStartDateTime=? or contestStartDateTime=?) order by contestStartDateTime ", 
				new Object[]{fromDate,toDate});
	}
	
	public List<QuizContest> getActiveContestByDates(Date fromDate, Date toDate) {
		return find("from QuizContest where status not in ('DELETED') and contestStartDateTime between ? and ? order by contestStartDateTime ", 
				new Object[]{fromDate,toDate});
	}
	
	public List<QuizContest> getAllCompletedMobileContestByDate(Date fromDate,Date toDate) {
		return find("from QuizContest where isCustomerStatsUpdated=? and contestStartDateTime between ? and ? "
				+ " and contestName not like '%test%' order by contestStartDateTime ", 
				new Object[]{Boolean.TRUE,fromDate,toDate});
	}
	
	public List<QuizContest> getAllCompleted2018YearEndContestByDate(Date fromDate,Date toDate, Date offerFromDate, Date offerToDate) {
		return find("from QuizContest where isCustomerStatsUpdated=? and contestName not like '%test%' and contestStartDateTime between ? and ? "
				+ "and contestStartDateTime between ? and ?  order by contestStartDateTime ", 
				new Object[]{Boolean.TRUE,fromDate,toDate,offerFromDate,offerToDate});
	}
	
	
	public QuizContest getCompletedContestByContestID(Integer contestId) {
		return findSingle("from QuizContest where contestName not like '%test%' and isCustomerStatsUpdated = ? and id = ? ", 
				new Object[]{Boolean.TRUE,contestId});
	}
	
}
