package com.rtfquiz.webservices.dao.implementaion;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.rtfquiz.webservices.data.QuizContestParticipants;

public class QuizContestParticipantsDAO extends HibernateDAO<Integer, QuizContestParticipants> implements com.rtfquiz.webservices.dao.services.QuizContestParticipantsDAO{

	public void updateQuizContestParticipantsExistContest(Integer customerId,Integer contestId) {
		bulkUpdate("UPDATE QuizContestParticipants SET status='EXIT', exitDateTime=? WHERE contestId=? and customerId=?",new Object[]{new Date(),contestId,customerId});
	}
	public void updateExistingQuizContestParticipantsByJoinContest(Integer customerId,Integer contestId) {
		bulkUpdate("UPDATE QuizContestParticipants SET status='EXIT' WHERE contestId=? and customerId=?",new Object[]{contestId,customerId});
	}
	public void updateContestParticipantsForContestResetByContestId(Integer contestId) {
		bulkUpdate("UPDATE QuizContestParticipants SET status='EXIT',exitDateTime=? WHERE contestId=?",new Object[]{new Date(),contestId});
	}
	public Boolean isExistingContestant(Integer customerId,Integer contestId) {
		Collection<Integer> list = find("SELECT customerId FROM QuizContestParticipants where contestId=? and customerId=?",new Object[]{contestId,customerId});
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}
}
