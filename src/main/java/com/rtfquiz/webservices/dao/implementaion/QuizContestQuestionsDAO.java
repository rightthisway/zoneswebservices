package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestQuestions;


public class QuizContestQuestionsDAO extends HibernateDAO<Integer, QuizContestQuestions> implements com.rtfquiz.webservices.dao.services.QuizContestQuestionsDAO {
	
	public QuizContestQuestions getQuizContestQuestionById(Integer questionId) {
		return findSingle("from QuizContestQuestions where id = ?", new Object[]{questionId});
	}
	public QuizContestQuestions getQuizContestQuestionByContestIdandQuestionSlNo(Integer contestId,Integer questionSlNo) {
		return findSingle("from QuizContestQuestions where contestId = ? and questionSNo = ?", new Object[]{contestId,questionSlNo});
	}
	
	public List<QuizContestQuestions> getQuizContestQuestionsByContestId(Integer ContestId) {
		return find("from QuizContestQuestions where contestId = ? order by questionSNo", new Object[]{ContestId});
	}
}
