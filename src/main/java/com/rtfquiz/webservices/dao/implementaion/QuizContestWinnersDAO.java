package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomer;

/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class QuizContestWinnersDAO extends HibernateDAO<Integer, QuizContestWinners> implements com.rtfquiz.webservices.dao.services.QuizContestWinnersDAO {
	
	public List<QuizContestWinners> getContestWinnersByContestId(Integer contestId) {
		return find("from QuizContestWinners where contestId = ? ", new Object[]{contestId});
	}
	
	public QuizContestWinners getQuizContestWinnerByCustomerIdAndContestId(Integer customerId,Integer contestId) {
		return findSingle("from QuizContestWinners where customerId=? and contestId = ? order by rewardRank", new Object[]{customerId,contestId});
	}
	
	public List<QuizContestWinners> getQuizContestGrandWinnersByContestId(Integer contestId) {
		return find("from QuizContestWinners where contestId =? and rewardTickets> 0", new Object[]{contestId});
	}
}
