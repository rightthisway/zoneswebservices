package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;


public class QuizCustomerContestAnswersDAO extends HibernateDAO<Integer, QuizCustomerContestAnswers> implements com.rtfquiz.webservices.dao.services.QuizCustomerContestAnswersDAO {
	
	
	public QuizCustomerContestAnswers getCustomerContestAnswerByCustomerIdandQuestionId(Integer customerId,Integer questionId ) {
		return findSingle("from QuizCustomerContestAnswers where customerId = ? and questionId=? ", new Object[]{customerId,questionId});
	}
	
	public QuizCustomerContestAnswers getLifeLineUsedContestAnswerByCustomerIdandContestId(Integer customerId,Integer contestId ) {
		return findSingle("from QuizCustomerContestAnswers where isLifeLineUsed=? and customerId = ? and contestId=? ", new Object[]{Boolean.TRUE,customerId,contestId});
	}
	
	public List<QuizCustomerContestAnswers> getAllCorrectAnsweredDatas(Integer contestId) {
		return find("from QuizCustomerContestAnswers where contestId=? and isCorrectAnswer=?", new Object[]{contestId,Boolean.TRUE});
	}
	public QuizCustomerContestAnswers getLatestContestAnswerByCustomerIdandContestId(Integer customerId,Integer contestId ) {
		return findSingle("from QuizCustomerContestAnswers where  customerId = ? and contestId=? order by questionSNo desc", new Object[]{customerId,contestId});
	}

	public Double getCustomerContestAnswerRewards(Integer customerId,Integer contestId) {
		List<Double> list = find("SELECT sum(answerRewards) FROM QuizCustomerContestAnswers where contestId=? and customerId=?",new Object[]{contestId,customerId});
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return 0.0;
	}
	public Boolean checkCustomerContestLifeLineUsageByCustomerIdandContestId(Integer customerId,Integer contestId) {
		List<Integer> list = find("SELECT customerId FROM QuizCustomerContestAnswers where isLifeLineUsed=? and contestId=? and customerId=? ",new Object[]{Boolean.TRUE,contestId,customerId});
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}
	
	public Integer totalNoOfContestQuestions() {
		List<Long> list = find("SELECT count(id) FROM QuizCustomerContestAnswers ");
		if (list != null && list.size() > 0) {
			return list.get(0).intValue();
		}
		return 0;
	}
	
	public Integer getCorrectAnsweredQuestionByCustomer(Integer customerId) {
		List<Long> list = find("SELECT count(id) FROM QuizCustomerContestAnswers where customerId=? and isCorrectAnswer=?",new Object[]{customerId,Boolean.TRUE});
		if (list != null && list.size() > 0) {
			return list.get(0).intValue();
		}
		return 0;
	}
	
	public Integer getNoOfQuesAnsweredByCustomer(Integer customerId) {
		List<Long> list = find("SELECT count(id) FROM QuizCustomerContestAnswers where customerId=? ",new Object[]{customerId});
		if (list != null && list.size() > 0) {
			return list.get(0).intValue();
		}
		return 0;
	}
	
	public List<QuizCustomerContestAnswers> getFirstQuestionAnsweredCustomers(Integer contestId) {
		return find("from QuizCustomerContestAnswers where contestId=? and questionSNo=?", new Object[]{contestId,1});
	}
}
