package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerContestDetails;


public class QuizCustomerContestDetailsDAO extends HibernateDAO<Integer, QuizCustomerContestDetails> implements com.rtfquiz.webservices.dao.services.QuizCustomerContestDetailsDAO {
	
	
	public QuizCustomerContestDetails getCustomerContestDetailsByCustomerIdandContestId(Integer customerId,Integer contestId ) {
		return findSingle("from QuizCustomerContestDetails where customerId = ? and contestId=? ", new Object[]{customerId,contestId});
	}
	
}
