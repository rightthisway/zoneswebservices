package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomer;

/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class QuizCustomerDAO extends HibernateDAO<Integer, QuizCustomer> implements com.rtfquiz.webservices.dao.services.QuizCustomerDAO {
	public List<QuizCustomer> getCustomer(String email,String password){
		
		return find("from QuizCustomer where email = ? and password =? ", new Object[]{email,password});
		
	} 
	public QuizCustomer getQuizCustomerByEmailId(String emailId) {
		return findSingle("from QuizCustomer where email = ? ", new Object[]{emailId});
	}
	public QuizCustomer getQuizCustomerById(Integer customerId) {
		return findSingle("from QuizCustomer where id= ? ", new Object[]{customerId});
		
	}
	public QuizCustomer getQuizCustomerByEmail(String email) {
		return findSingle("from QuizCustomer where email= ? ", new Object[]{email});
	}
	public QuizCustomer getQuizCustomerByUserId(String userId) {
		return findSingle("from QuizCustomer where userId= ? ", new Object[]{userId});
	}
	public QuizCustomer getQuizCustomerByPhone(String phoneNo) {
		return findSingle("from QuizCustomer where phone = ? ", new Object[]{phoneNo});
	}
}
