package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerFriend;
import com.rtfquiz.webservices.enums.FriendStatus;

public class QuizCustomerFriendDAO extends HibernateDAO<Integer, QuizCustomerFriend> implements com.rtfquiz.webservices.dao.services.QuizCustomerFriendDAO{

	public List<QuizCustomerFriend> getQuizCustomerFriendRecords(Integer customerId,Integer friendCustomerId){
		return find("FROM QuizCustomerFriend WHERE (customerId=? and customerFriendId=?) OR (customerId=? and customerFriendId=?) "
				,new Object[]{customerId,friendCustomerId,friendCustomerId,customerId});
	}
	
	public QuizCustomerFriend getPendingFriendRequest(Integer customerId,Integer friendCustomerId){
		return findSingle("FROM QuizCustomerFriend WHERE customerFriendId=? and customerId=? and status like ? ",new Object[]{customerId,friendCustomerId,FriendStatus.Requested});
	}
	
	public QuizCustomerFriend getFriendRequest(Integer customerId,Integer friendCustomerId){
		return findSingle("FROM QuizCustomerFriend WHERE customerFriendId=? and customerId=? and status like ? ",new Object[]{friendCustomerId,customerId,FriendStatus.Requested});
	}
}
