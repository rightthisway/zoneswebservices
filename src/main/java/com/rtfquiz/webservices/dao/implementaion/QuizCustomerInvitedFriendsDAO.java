package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerFriend;
import com.rtfquiz.webservices.data.QuizCustomerInvitedFriends;
import com.rtfquiz.webservices.enums.FriendStatus;

public class QuizCustomerInvitedFriendsDAO extends HibernateDAO<Integer, QuizCustomerInvitedFriends> implements com.rtfquiz.webservices.dao.services.QuizCustomerInvitedFriendsDAO{

	public List<QuizCustomerInvitedFriends> getAllActiveInvitedFriendsByCustomerId(Integer customerId){
		return find("FROM QuizCustomerInvitedFriends WHERE status='ACTIVE' and customerId=? ",new Object[]{customerId});
	}
	
	public QuizCustomerInvitedFriends getInvitedFriendsByCustomerIdandPhoneNo(Integer customerId,String phone){
		return findSingle("FROM QuizCustomerInvitedFriends WHERE status='ACTIVE' and customerId=? and phone=? ",new Object[]{customerId,phone});
	}
	
}
