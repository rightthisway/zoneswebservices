package com.rtfquiz.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;

/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class QuizCustomerReferralTrackingDAO extends HibernateDAO<Integer, QuizCustomerReferralTracking> implements com.rtfquiz.webservices.dao.services.QuizCustomerReferralTrackingDAO {
	
	public QuizCustomerReferralTracking getReferralTrackingByCustomerId(Integer customerId) {
		return findSingle("from QuizCustomerReferralTracking where customerId = ? ", new Object[]{customerId});
	}
	public QuizCustomerReferralTracking getReferralTrackingByCustomerIdandReferralCode(Integer customerId,String referralCode) {
		return findSingle("from QuizCustomerReferralTracking where customerId = ? and referralCode=?", new Object[]{customerId,referralCode});
	}
	public QuizCustomerReferralTracking getReferralTrackingByReferralCode(String referralCode) {
		return findSingle("from QuizCustomerReferralTracking where referralCode=?", new Object[]{referralCode});
	}
	
	public Integer getTotalNoOfReferrals(Integer customerId) {
		List<Long> list = find("SELECT count(id) FROM QuizCustomerReferralTracking where referralCustomerId=?",new Object[]{customerId});
		if (list != null && list.size() > 0) {
			return list.get(0).intValue();
		}
		return 0;
	}
	
	public List<QuizCustomerReferralTracking> getAllAffiliateReferrals() {
		return find("from QuizCustomerReferralTracking where isAffiliateReferral=? and affiliateReferralStatus=? ",
				new Object[]{Boolean.TRUE,"ACTIVE"});
	}
	
	public List<QuizCustomerReferralTracking> getAllYearEndOfferApplicableReferrals(Date fromDate,Date toDate) {
		return find("from QuizCustomerReferralTracking where yearEndRewardApplied=? and createdDateTime > ?",
				new Object[]{Boolean.FALSE,fromDate});
	}
	
	
	public List<QuizCustomerReferralTracking> getAllSuperFanOfferApplicableReferrals(Date fromDate,Date toDate) {
		return find("from QuizCustomerReferralTracking where referralCustomerId is not null and createdDateTime > ?",
				new Object[]{fromDate});
	}
}
