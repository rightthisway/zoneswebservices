package com.rtfquiz.webservices.dao.implementaion;

import com.rtfquiz.webservices.dao.services.ContestEventDAO;
import com.rtfquiz.webservices.dao.services.ContestParticipantsCassandraDAO;
import com.rtfquiz.webservices.dao.services.QuizContestPasswordAuthDAO;
import com.rtfquiz.webservices.dao.services.ContestReferrerRewardCreditDAO;
import com.rtfquiz.webservices.dao.services.CustomerPromoRewardDetailsDAO;
import com.rtfquiz.webservices.dao.services.QuizConfigSettingsDAO;
import com.rtfquiz.webservices.dao.services.QuizContestDAO;
import com.rtfquiz.webservices.dao.services.QuizContestParticipantsDAO;
import com.rtfquiz.webservices.dao.services.QuizContestQuestionsDAO;
import com.rtfquiz.webservices.dao.services.QuizContestWinnersDAO;
import com.rtfquiz.webservices.dao.services.QuizCustomerContestAnswersDAO;
import com.rtfquiz.webservices.dao.services.QuizCustomerContestDetailsDAO;
import com.rtfquiz.webservices.dao.services.QuizCustomerDAO;
import com.rtfquiz.webservices.dao.services.QuizCustomerReferralTrackingDAO;
import com.rtfquiz.webservices.dao.services.QuizFirebaseCallBackTrackingDAO;
import com.rtfquiz.webservices.dao.services.QuizOTPTrackingDAO;
import com.rtfquiz.webservices.dao.services.QuizSuperFanCustomerLevelDAO;
import com.rtfquiz.webservices.dao.services.QuizSuperFanLevelConfigDAO;
import com.rtfquiz.webservices.dao.services.QuizSuperFanStatDAO;
import com.rtfquiz.webservices.dao.services.QuizSuperFanTrackingDAO;
import com.rtfquiz.webservices.dao.services.RtfCustPhoneReferralsDAO;
import com.rtfquiz.webservices.dao.services.RtfCustProfileAnswersDAO;
import com.rtfquiz.webservices.dao.services.RtfCustProfileQuestionsDAO;
import com.rtfquiz.webservices.dao.services.RtfCustomerRewardRedemptionHistoryDAO;
import com.rtfquiz.webservices.dao.services.RtfPointsConversionSettingDAO;
import com.rtfquiz.webservices.dao.services.RtfRewardConfigDAO;



public class QuizDAORegistry {
	
	private static QuizQueryManagerDAO quizQueryManagerDAO;
	private static RtfRewardConfigDAO rtfRewardConfigDAO;
	private static RtfPointsConversionSettingDAO rtfPointsConversionSettingDAO;
	private static RtfCustomerRewardRedemptionHistoryDAO rtfCustomerRewardRedemptionHistoryDAO;
	private static QuizCustomerDAO quizCustomerDAO;
	private static QuizOTPTrackingDAO quizOTPTrackingDAO;
	private static QuizContestDAO quizContestDAO;
	private static QuizContestQuestionsDAO quizContestQuestionsDAO;
	private static QuizCustomerContestAnswersDAO quizCustomerContestAnswersDAO;
	private static QuizContestWinnersDAO quizContestWinnersDAO;
	private static QuizCustomerReferralTrackingDAO quizCustomerReferralTrackingDAO;
	private static QuizConfigSettingsDAO quizConfigSettingsDAO;
	private static CustomerContestQuestionDAO customerContestQuestionDAO;
	private static ContestEventRequestDAO contestEventRequestDAO;
	private static ContestGrandWinnerDAO contestGrandWinnerDAO;
	private static ContestEventDAO contestEventDAO;
	private static QuizSummaryManagerDAO quizSummaryManagerDAO;
	private static CustomerPromoRewardDetailsDAO customerPromoRewardDetailsDAO;
	private static QuizContestParticipantsDAO quizContestParticipantsDAO;
	private static ContestReferrerRewardCreditDAO contestReferrerRewardCreditDAO;
	private static QuizFirebaseCallBackTrackingDAO quizFirebaseCallBackTrackingDAO;
	private static QuizSuperFanStatDAO quizSuperFanStatDAO;
	private static QuizSuperFanLevelConfigDAO quizSuperFanLevelConfigDAO;
	private static QuizSuperFanCustomerLevelDAO quizSuperFanCustomerLevelDAO;
	private static QuizSuperFanTrackingDAO quizSuperFanTrackingDAO;
	private static QuizCustomerContestDetailsDAO quizCustomerContestDetailsDAO;
	private static ContestParticipantsCassandraDAO contestParticipantsCassandraDAO;
	private static RtfCustProfileAnswersDAO rtfCustProfileAnswersDAO;
	private static RtfCustProfileQuestionsDAO rtfCustProfileQuestionsDAO;
	private static RtfCustPhoneReferralsDAO rtfCustPhoneReferralsDAO;
	private static QuizContestPasswordAuthDAO quizContestPasswordAuthDAO;
	
	public final static QuizCustomerDAO getQuizCustomerDAO() {
		return quizCustomerDAO;
	}
	public final void setQuizCustomerDAO(QuizCustomerDAO quizCustomerDAO) {
		QuizDAORegistry.quizCustomerDAO = quizCustomerDAO;
	}
	
	public final static RtfRewardConfigDAO getRtfRewardConfigDAO() {
		return rtfRewardConfigDAO;
	}
	public final void setRtfRewardConfigDAO(RtfRewardConfigDAO rtfRewardConfigDAO) {
		QuizDAORegistry.rtfRewardConfigDAO = rtfRewardConfigDAO;
	}
	
	public final static RtfPointsConversionSettingDAO getRtfPointsConversionSettingDAO() {
		return rtfPointsConversionSettingDAO;
	}
	public final void setRtfPointsConversionSettingDAO(RtfPointsConversionSettingDAO rtfPointsConversionSettingDAO) {
		QuizDAORegistry.rtfPointsConversionSettingDAO = rtfPointsConversionSettingDAO;
	}
	
	public final static RtfCustomerRewardRedemptionHistoryDAO getRtfCustomerRewardRedemptionHistoryDAO() {
		return rtfCustomerRewardRedemptionHistoryDAO;
	}
	public final void setRtfCustomerRewardRedemptionHistoryDAO(
			RtfCustomerRewardRedemptionHistoryDAO rtfCustomerRewardRedemptionHistoryDAO) {
		QuizDAORegistry.rtfCustomerRewardRedemptionHistoryDAO = rtfCustomerRewardRedemptionHistoryDAO;
	}
	public final static QuizOTPTrackingDAO getQuizOTPTrackingDAO() {
		return quizOTPTrackingDAO;
	}
	public final void setQuizOTPTrackingDAO(QuizOTPTrackingDAO quizOTPTrackingDAO) {
		QuizDAORegistry.quizOTPTrackingDAO = quizOTPTrackingDAO;
	}
	public final static QuizContestDAO getQuizContestDAO() {
		return quizContestDAO;
	}
	public final void setQuizContestDAO(QuizContestDAO quizContestDAO) {
		QuizDAORegistry.quizContestDAO = quizContestDAO;
	}
	public final static QuizContestQuestionsDAO getQuizContestQuestionsDAO() {
		return quizContestQuestionsDAO;
	}
	public final void setQuizContestQuestionsDAO(
			QuizContestQuestionsDAO quizContestQuestionsDAO) {
		QuizDAORegistry.quizContestQuestionsDAO = quizContestQuestionsDAO;
	}
	public final static QuizCustomerContestAnswersDAO getQuizCustomerContestAnswersDAO() {
		return quizCustomerContestAnswersDAO;
	}
	public final void setQuizCustomerContestAnswersDAO(
			QuizCustomerContestAnswersDAO quizCustomerContestAnswersDAO) {
		QuizDAORegistry.quizCustomerContestAnswersDAO = quizCustomerContestAnswersDAO;
	}
	
	public static final QuizCustomerContestDetailsDAO getQuizCustomerContestDetailsDAO() {
		return quizCustomerContestDetailsDAO;
	}
	public final void setQuizCustomerContestDetailsDAO(QuizCustomerContestDetailsDAO quizCustomerContestDetailsDAO) {
		QuizDAORegistry.quizCustomerContestDetailsDAO = quizCustomerContestDetailsDAO;
	}
	public final static QuizQueryManagerDAO getQuizQueryManagerDAO() {
		return quizQueryManagerDAO;
	}
	public final void setQuizQueryManagerDAO(
			QuizQueryManagerDAO quizQueryManagerDAO) {
		QuizDAORegistry.quizQueryManagerDAO = quizQueryManagerDAO;
	}
	public final static QuizContestWinnersDAO getQuizContestWinnersDAO() {
		return quizContestWinnersDAO;
	}
	public final void setQuizContestWinnersDAO(
			QuizContestWinnersDAO quizContestWinnersDAO) {
		QuizDAORegistry.quizContestWinnersDAO = quizContestWinnersDAO;
	}
	public final static QuizCustomerReferralTrackingDAO getQuizCustomerReferralTrackingDAO() {
		return quizCustomerReferralTrackingDAO;
	}
	public final void setQuizCustomerReferralTrackingDAO(
			QuizCustomerReferralTrackingDAO quizCustomerReferralTrackingDAO) {
		QuizDAORegistry.quizCustomerReferralTrackingDAO = quizCustomerReferralTrackingDAO;
	}
	public final static QuizConfigSettingsDAO getQuizConfigSettingsDAO() {
		return quizConfigSettingsDAO;
	}
	public final void setQuizConfigSettingsDAO(
			QuizConfigSettingsDAO quizConfigSettingsDAO) {
		QuizDAORegistry.quizConfigSettingsDAO = quizConfigSettingsDAO;
	}
	public static CustomerContestQuestionDAO getCustomerContestQuestionDAO() {
		return customerContestQuestionDAO;
	}
	public final void setCustomerContestQuestionDAO(
			CustomerContestQuestionDAO customerContestQuestionDAO) {
		QuizDAORegistry.customerContestQuestionDAO = customerContestQuestionDAO;
	}
	public static ContestEventRequestDAO getContestEventRequestDAO() {
		return contestEventRequestDAO;
	}
	public final void setContestEventRequestDAO(
			ContestEventRequestDAO contestEventRequestDAO) {
		QuizDAORegistry.contestEventRequestDAO = contestEventRequestDAO;
	}
	public static ContestGrandWinnerDAO getContestGrandWinnerDAO() {
		return contestGrandWinnerDAO;
	}
	public final void setContestGrandWinnerDAO(
			ContestGrandWinnerDAO contestGrandWinnerDAO) {
		QuizDAORegistry.contestGrandWinnerDAO = contestGrandWinnerDAO;
	}
	public static ContestEventDAO getContestEventDAO() {
		return contestEventDAO;
	}
	public final void setContestEventDAO(ContestEventDAO contestEventDAO) {
		QuizDAORegistry.contestEventDAO = contestEventDAO;
	}
	public static QuizSummaryManagerDAO getQuizSummaryManagerDAO() {
		return quizSummaryManagerDAO;
	}
	public final void setQuizSummaryManagerDAO(
			QuizSummaryManagerDAO quizSummaryManagerDAO) {
		QuizDAORegistry.quizSummaryManagerDAO = quizSummaryManagerDAO;
	}
	public final static CustomerPromoRewardDetailsDAO getCustomerPromoRewardDetailsDAO() {
		return customerPromoRewardDetailsDAO;
	}
	public final void setCustomerPromoRewardDetailsDAO(
			CustomerPromoRewardDetailsDAO customerPromoRewardDetailsDAO) {
		QuizDAORegistry.customerPromoRewardDetailsDAO = customerPromoRewardDetailsDAO;
	}
	public static QuizContestParticipantsDAO getQuizContestParticipantsDAO() {
		return quizContestParticipantsDAO;
	}
	public final void setQuizContestParticipantsDAO(
			QuizContestParticipantsDAO quizContestParticipantsDAO) {
		QuizDAORegistry.quizContestParticipantsDAO = quizContestParticipantsDAO;
	}
	public static ContestReferrerRewardCreditDAO getContestReferrerRewardCreditDAO() {
		return contestReferrerRewardCreditDAO;
	}
	public final void setContestReferrerRewardCreditDAO(ContestReferrerRewardCreditDAO contestReferrerRewardCreditDAO) {
		QuizDAORegistry.contestReferrerRewardCreditDAO = contestReferrerRewardCreditDAO;
	}
	public static QuizFirebaseCallBackTrackingDAO getQuizFirebaseCallBackTrackingDAO() {
		return quizFirebaseCallBackTrackingDAO;
	}
	public final void setQuizFirebaseCallBackTrackingDAO(QuizFirebaseCallBackTrackingDAO quizFirebaseCallBackTrackingDAO) {
		QuizDAORegistry.quizFirebaseCallBackTrackingDAO = quizFirebaseCallBackTrackingDAO;
	}
	public static QuizSuperFanStatDAO getQuizSuperFanStatDAO() {
		return quizSuperFanStatDAO;
	}
	public final void setQuizSuperFanStatDAO(QuizSuperFanStatDAO quizSuperFanStatDAO) {
		QuizDAORegistry.quizSuperFanStatDAO = quizSuperFanStatDAO;
	}
	public static QuizSuperFanLevelConfigDAO getQuizSuperFanLevelConfigDAO() {
		return quizSuperFanLevelConfigDAO;
	}
	public final void setQuizSuperFanLevelConfigDAO(QuizSuperFanLevelConfigDAO quizSuperFanLevelConfigDAO) {
		QuizDAORegistry.quizSuperFanLevelConfigDAO = quizSuperFanLevelConfigDAO;
	}
	
	public static QuizSuperFanCustomerLevelDAO getQuizSuperFanCustomerLevelDAO() {
		return quizSuperFanCustomerLevelDAO;
	}
	public final void setQuizSuperFanCustomerLevelDAO(QuizSuperFanCustomerLevelDAO quizSuperFanCustomerLevelDAO) {
		QuizDAORegistry.quizSuperFanCustomerLevelDAO = quizSuperFanCustomerLevelDAO;
	}
	public static QuizSuperFanTrackingDAO getQuizSuperFanTrackingDAO() {
		return quizSuperFanTrackingDAO;
	}
	public final void setQuizSuperFanTrackingDAO(QuizSuperFanTrackingDAO quizSuperFanTrackingDAO) {
		QuizDAORegistry.quizSuperFanTrackingDAO = quizSuperFanTrackingDAO;
	}
	public static ContestParticipantsCassandraDAO getContestParticipantsCassandraDAO() {
		return contestParticipantsCassandraDAO;
	}
	public final void setContestParticipantsCassandraDAO(ContestParticipantsCassandraDAO contestParticipantsCassandraDAO) {
		QuizDAORegistry.contestParticipantsCassandraDAO = contestParticipantsCassandraDAO;
	}
	public static RtfCustProfileAnswersDAO getRtfCustProfileAnswersDAO() {
		return rtfCustProfileAnswersDAO;
	}
	public final void setRtfCustProfileAnswersDAO(RtfCustProfileAnswersDAO rtfCustProfileAnswersDAO) {
		QuizDAORegistry.rtfCustProfileAnswersDAO = rtfCustProfileAnswersDAO;
	}
	public static RtfCustProfileQuestionsDAO getRtfCustProfileQuestionsDAO() {
		return rtfCustProfileQuestionsDAO;
	}
	public final void setRtfCustProfileQuestionsDAO(RtfCustProfileQuestionsDAO rtfCustProfileQuestionsDAO) {
		QuizDAORegistry.rtfCustProfileQuestionsDAO = rtfCustProfileQuestionsDAO;
	}
	public static RtfCustPhoneReferralsDAO getRtfCustPhoneReferralsDAO() {
		return rtfCustPhoneReferralsDAO;
	}
	public final void setRtfCustPhoneReferralsDAO(RtfCustPhoneReferralsDAO rtfCustPhoneReferralsDAO) {
		QuizDAORegistry.rtfCustPhoneReferralsDAO = rtfCustPhoneReferralsDAO;
	}
	public static QuizContestPasswordAuthDAO getQuizContestPasswordAuthDAO() {
		return quizContestPasswordAuthDAO;
	}
	public final void setQuizContestPasswordAuthDAO(QuizContestPasswordAuthDAO quizContestPasswordAuthDAO) {
		QuizDAORegistry.quizContestPasswordAuthDAO = quizContestPasswordAuthDAO;
	}
	
}
