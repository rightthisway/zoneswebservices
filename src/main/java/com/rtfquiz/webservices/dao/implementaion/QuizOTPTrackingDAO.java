package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizOTPTracking;

/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class QuizOTPTrackingDAO extends HibernateDAO<Integer, QuizOTPTracking> implements com.rtfquiz.webservices.dao.services.QuizOTPTrackingDAO {
	
	public List<QuizOTPTracking> getQuizOTPTrackingByPhoneNo(String phone){
		return find("from QuizOTPTracking where phone = ? ", new Object[]{phone});
	}
	
	public List<QuizOTPTracking> getActiveOTPByPhoneNo(String phone){
		return find("from QuizOTPTracking where phone = ? and status = ? order by createdDate desc", new Object[]{phone,"ACTIVE"});
	}
	
	public List<QuizOTPTracking> getAllActiveOTP(){
		return find("from QuizOTPTracking where status = ? ", new Object[]{"ACTIVE"});
	}
	
	public boolean deleteAllOTPByPhoneNo(String phoneNo){
		try{
			bulkUpdate("delete from QuizOTPTracking where phone = ?", new Object[]{phoneNo});
			return true;
		}catch (Exception e) {
			return false;
		}
	} 
}
