package com.rtfquiz.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;

import com.quiz.cassandra.list.HallOfFameDtls;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.CustomerPromoRewardDetails;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.enums.PromoType;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.rtfquiz.webservices.utils.list.QuizPromoCodeDetail;

public class QuizQueryManagerDAO {
	
	private SessionFactory sessionFactory;
	private static String zonesApiLinkedServer;// = "ZonesApiPlatformV2_Sanbox.dbo";
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public static String getZonesApiLinkedServer() {
		return zonesApiLinkedServer;
	}

	public void setZonesApiLinkedServer(String zonesApiLinkedServer) {
		QuizQueryManagerDAO.zonesApiLinkedServer = zonesApiLinkedServer;
	}



	private static Session querySession = null;
	
	
	public static Session getQuerySession() {
		return querySession;
	}

	public static void setQuerySession(Session querySession) {
		QuizQueryManagerDAO.querySession = querySession;
	}
	
	
	public List<Object[]> getContestAnswersCountByQuestionId(Integer questionId){
		String sql = "select count(*) as answerCount,answer from customer_contest_answers with(nolock)" +
				" where question_id="+questionId +
				" group by answer";
		Query query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			return (List<Object[]>)query.list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public Integer getContestLifeLineUsageCountByQuestionId(Integer questionId){
		String sql = "select count(*) as lifeLineCount from customer_contest_answers with(nolock)" +
				" where question_id="+questionId +" and is_lifeline_used =1"+
				" group by answer";
		Query query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return 0;
	}
	public List<QuizContestWinners> getQuizContestSummaryByDateFromView(Integer summaryListCount,String fromDateStr,String toDateStr,Integer customerId){
		/*String sql = " select " ;
				if(customerId == null) {
					sql = sql + " top "+summaryListCount;
				}
				sql = sql + " sum(reward_tickets) as rewardTickets,sum(reward_points) as rewardPoints," +
				" sum(reward_rank) as rewardRank,customer_id as customerId," +
				" c.user_id as userId,c.cust_image_path as custImagePath" +
				" from contest_winners cw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=cw.customer_id" +
				
				" where created_datetime>='"+fromDateStr+"' and created_datetime<='"+toDateStr+"'";
			
		if(customerId != null) {
			sql = sql + " and customer_id="+customerId;
		}
		sql = sql + " group by customer_id, c.user_id,c.cust_image_path" +
		" order by rewardTickets desc,rewardPoints desc,rewardRank";*/

//Get data from view
		String sql = " select " ;
		if(customerId == null) {
			sql = sql + " top "+summaryListCount;
		}
		sql = sql + " c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" TotalTickets as rewardTickets,TotalPoints as rewardPoints,CusRank as rewardRank" +
				" from contest_winners_rank_for_week_view csw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=csw.customer_id";
		if(customerId != null) {
			sql = sql + " and csw.customer_id="+customerId;
		}
		sql = sql + " where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by CusRank";
//Get data from table
		/*String sql = " select " ;
		if(customerId == null) {
			sql = sql + " top "+summaryListCount;
		}
		sql = sql + " c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" total_tickets as rewardTickets,total_points as rewardPoints,customer_rank as rewardRank" +
				" from contest_summary_rank_for_week csw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=csw.customer_id";
		if(customerId != null) {
			sql = sql + " and csw.customer_id="+customerId;
		}
		sql = sql + " order by customer_rank";*/
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("rewardRank",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			
			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
			System.out.println("This Week cache Query : "+sql+" : "+ new Date());
			return contestWinners;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public List<QuizContestWinners> getQuizContestSummaryByTillDateFromView(Integer summaryListCount){
		/*String sql = " select top "+summaryListCount+" sum(reward_tickets) as rewardTickets,sum(reward_points) as rewardPoints," +
				" sum(reward_rank) as rewardRank,customer_id as customerId," +
				" c.user_id as userId,c.cust_image_path as custImagePath" +
				" from contest_winners cw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=cw.customer_id" +
				" group by customer_id, c.user_id,c.cust_image_path" +
				" order by rewardTickets desc,rewardPoints desc,rewardRank";*/
//Get Data from view
		String sql = " select top "+summaryListCount+" c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" TotalTickets as rewardTickets,TotalPoints as rewardPoints,CusRank as rewardRank" +
				" from contest_winners_rank_till_date_view csw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=csw.customer_id" +
				" where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by CusRank";
//Get Data from table
		/*String sql = " select top "+summaryListCount+" c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" total_tickets as rewardTickets,total_points as rewardPoints,customer_rank as rewardRank" +
				" from contest_summary_rank_till_date csw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=csw.customer_id" +
				" order by rewardRank";*/
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("rewardRank",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);

			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
			System.out.println("Till Date cache Query : "+sql+" : "+ new Date());
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return contestWinners;
	}
	public List<QuizContestWinners> getQuizContestSummaryByTillDate(Integer summaryListCount){
		/*String sql = " select top "+summaryListCount+" sum(reward_tickets) as rewardTickets,sum(reward_points) as rewardPoints," +
				" sum(reward_rank) as rewardRank,customer_id as customerId," +
				" c.user_id as userId,c.cust_image_path as custImagePath" +
				" from contest_winners cw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=cw.customer_id" +
				" group by customer_id, c.user_id,c.cust_image_path" +
				" order by rewardTickets desc,rewardPoints desc,rewardRank";*/
//Get Data from view
		/*String sql = " select top "+summaryListCount+" c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" TotalTickets as rewardTickets,TotalPoints as rewardPoints,CusRank as rewardRank" +
				" from contest_winners_rank_till_date_view csw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=csw.customer_id" +
				" order by CusRank";*/
//Get Data from table
		String sql = " select top "+summaryListCount+" c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" total_tickets as rewardTickets,total_points as rewardPoints,customer_rank as rewardRank" +
				" from contest_summary_rank_till_date csw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=csw.customer_id" +
				" where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by rewardRank";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("rewardRank",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);

			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return contestWinners;
	}
	
	public QuizContestWinners getQuizContestSummaryByTillDateAndCustomerId(Integer customerId){
		/*String sql = " select sum(reward_tickets) as rewardTickets,sum(reward_points) as rewardPoints," +
				" sum(reward_rank) as rewardRank,customer_id as customerId," +
				" c.user_id as userId,c.cust_image_path as custImagePath" +
				" from contest_winners cw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=cw.customer_id" +
				" where customer_id="+customerId +
				" group by customer_id, c.user_id,c.cust_image_path" +
				" order by rewardTickets desc,rewardPoints desc,rewardRank";*/
//get data from view
		/*String sql = " c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" TotalTickets as rewardTickets,TotalPoints as rewardPoints,CusRank as rewardRank" +
				" from contest_winners_rank_till_date_view csw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=csw.customer_id" +
				" where csw.customer_id="+customerId +
				" order by CusRank";*/
//get Data from table		
		String sql = " select c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
		"  total_tickets as rewardTickets,total_points as rewardPoints,customer_rank as rewardRank" +
		" from contest_summary_rank_till_date csw with(nolock)" +
		" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=csw.customer_id" +
		" where csw.customer_id="+customerId +
		" order by customer_rank";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("rewardRank",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			
			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
			if(contestWinners != null && contestWinners.size() > 0) {
				return contestWinners.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}

	public List<QuizContestWinners> getQuizContestSummaryByDate(Integer summaryListCount,String fromDateStr,String toDateStr,Integer customerId){
		/*String sql = " select " ;
				if(customerId == null) {
					sql = sql + " top "+summaryListCount;
				}
				sql = sql + " sum(reward_tickets) as rewardTickets,sum(reward_points) as rewardPoints," +
				" sum(reward_rank) as rewardRank,customer_id as customerId," +
				" c.user_id as userId,c.cust_image_path as custImagePath" +
				" from contest_winners cw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=cw.customer_id" +
				
				" where created_datetime>='"+fromDateStr+"' and created_datetime<='"+toDateStr+"'";
			
		if(customerId != null) {
			sql = sql + " and customer_id="+customerId;
		}
		sql = sql + " group by customer_id, c.user_id,c.cust_image_path" +
		" order by rewardTickets desc,rewardPoints desc,rewardRank";*/

//Get data from view
		/*String sql = " select " ;
		if(customerId == null) {
			sql = sql + " top "+summaryListCount;
		}
		sql = sql + " c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" TotalTickets as rewardTickets,TotalPoints as rewardPoints,CusRank as rewardRank" +
				" from contest_winners_rank_for_week_view csw" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=csw.customer_id";
		if(customerId != null) {
			sql = sql + " and csw.customer_id="+customerId;
		}
		sql = sql + " order by CusRank";*/
//Get data from table
		String sql = " select " ;
		if(customerId == null) {
			sql = sql + " top "+summaryListCount;
		}
		sql = sql + " c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" total_tickets as rewardTickets,total_points as rewardPoints,customer_rank as rewardRank" +
				" from contest_summary_rank_for_week csw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=csw.customer_id";
		if(customerId != null) {
			sql = sql + " and csw.customer_id="+customerId;
		}
		sql = sql + " where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by customer_rank";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("rewardRank",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			
			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
			return contestWinners;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public List<QuizContestWinners> getContestWinnersByContestId(Integer summaryListCount,Integer contestId,Integer customerId){
		String sql = " select " ;
				if(customerId == null) {
					sql = sql + " top "+summaryListCount;
				}
				sql = sql + " sum(reward_tickets) as rewardTickets,sum(reward_points) as rewardPoints," +
				" sum(reward_rank) as rewardRank,customer_id as customerId,contest_id as contestId," +
				" c.user_id as userId,c.cust_image_path as custImagePath" +
				" from contest_winners cw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=cw.customer_id" +
				" where contest_id ="+contestId;
		if(customerId != null) {
			sql = sql + " and customer_id="+customerId;
		}
		sql = sql + " group by customer_id,contest_id, c.user_id,c.cust_image_path" +
		" order by rewardTickets desc,rewardPoints desc,rewardRank";
		
		/*String sql = " select " ;
		if(customerId == null) {
			sql = sql + " top "+summaryListCount;
		}
		sql = sql + " rewardTickets,rewardPoints,customerId,userId,custImagePath," +//contestId+" as contestId,"+
				" RANK() OVER ( ORDER BY rewardTickets desc, rewardPoints DESC) as rewardRank" +
				" FROM (" +
				" select sum(tickets) as rewardTickets, sum(points) as rewardPoints," +
				" custWin.customer_id as customerId,c.user_id as userId,c.cust_image_path as custImagePath" +
				" from (" +
				" select customer_id, sum(iif(reward_tickets is null, 0, reward_tickets)) as tickets," +
				" sum(iif(reward_points is null, 0, reward_points)) as points" +
				" from contest_winners" +
				" where contest_id="+contestId+
				" group by customer_id" +
				" UNION" +
				" select customer_id, 0 as tickets, sum(iif(answer_rewards is null, 0, answer_rewards)) as points" +
				" from customer_contest_answers" +
				" where contest_id=" +contestId+
				" group by customer_id" +
				" ) custWin" +
				" inner join "+zonesApiLinkedServer+".customer c on c.id=custWin.customer_id" +
				" group by custWin.customer_id,c.user_id,c.cust_image_path" +
				" ) cw";*/

		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("rewardRank",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			//query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			
			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
			return contestWinners;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public List<QuizContestWinners> getContestWinnersByContestIdOrderByUserId(Integer summaryListCount,Integer contestId,Integer customerId){
		String sql = " select " ;
				if(customerId == null) {
					sql = sql + " top "+summaryListCount;
				}
				sql = sql + " sum(reward_tickets) as rewardTickets,sum(reward_points) as rewardPoints," +
				" sum(reward_rank) as rewardRank,customer_id as customerId,contest_id as contestId," +
				" c.user_id as userId,c.cust_image_path as custImagePath" +
				" from contest_winners cw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=cw.customer_id" +
				" where contest_id ="+contestId;
		if(customerId != null) {
			sql = sql + " and customer_id="+customerId;
		}
		sql = sql + " group by customer_id,contest_id, c.user_id,c.cust_image_path" +
		" order by userId asc";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("rewardRank",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			//query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			
			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
			return contestWinners;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	/*public String getcustomerFrientStatus(Integer customerId,Integer friendCustoemrId) {
		String sql ="select status from customer_friends where (customer_id="+customerId+" and friend_customer_id="+friendCustoemrId+")" +
				" or (customer_id="+friendCustoemrId+" and friend_customer_id="+customerId+")" ;
		Query query = null;
		Broker broker = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<String> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	}*/
	/*public Integer getAllPendingFriendsRequestCountByCustomerId(Integer customerId) throws Exception {
		String sql = "select count(*) as friendRequestCount from customer_friends where status='REQUEST' and friend_customer_id="+customerId;
		Query query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
		//return null;
	}*/
	public List<QuizContestWinners> getCustomerContestRewardsForUpdate(Integer contestId){
		String sql =" select customerId,sum(rewardPoints) as rewardPoints from (" +
				" select customer_id as customerId,sum(reward_points) as rewardPoints from contest_winners with(nolock)" +
				" where contest_id=" +contestId+
				" group by customer_id" +
				" union" +
				" select customer_id as customerId,sum(answer_rewards) as rewardPoints from customer_contest_answers with(nolock)" +
				" where contest_id=" +contestId +
				" and answer_rewards is not null and answer_rewards > 0" +
				" group by customer_id" +
				" ) as g" +
				" group by customerId";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizContestWinners> contestWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rewardPoints",Hibernate.DOUBLE);
			query.addScalar("customerId",Hibernate.INTEGER);
			
			contestWinners = query.setResultTransformer(Transformers.aliasToBean(QuizContestWinners.class)).list();
			return contestWinners;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public List<QuizCustomerContestAnswers> getAllCustomerContestAnswersForCacheRefresh(Integer contestId){
		String sql ="select co.id as id,co.customer_id as customerId,co.contest_id as contestId,co.question_id as questionId,co.question_sl_no as questionSNo," +
				" co.answer as answer,co.is_correct_answer as isCorrectAnswer,co.is_lifeline_used as isLifeLineUsed,co.answer_rewards as answerRewards," +
				" co.created_datetime as createdDateTime,co.updated_datetime as updatedDateTime   from customer_contest_answers co" +
				" inner join (select max(id) as id,customer_id from customer_contest_answers where contest_id="+contestId+" group by customer_id) as cc on cc.customer_id=co.customer_id and cc.id=co.id" +
				" where contest_id="+contestId;//and question_sl_no=12
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizCustomerContestAnswers> customerAnswers = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("questionId",Hibernate.INTEGER);
			query.addScalar("questionSNo",Hibernate.INTEGER);
			query.addScalar("answer",Hibernate.STRING);
			query.addScalar("isCorrectAnswer",Hibernate.BOOLEAN);
			query.addScalar("isLifeLineUsed",Hibernate.BOOLEAN);
			query.addScalar("answerRewards",Hibernate.DOUBLE);
			query.addScalar("createdDateTime",Hibernate.TIMESTAMP);
			query.addScalar("updatedDateTime",Hibernate.TIMESTAMP);
			
			customerAnswers = query.setResultTransformer(Transformers.aliasToBean(QuizCustomerContestAnswers.class)).list();
			return customerAnswers;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public List<QuizCustomerContestAnswers> getAllCustomersContestAnswerRewardsForCacheRefresh(Integer contestId){
		String sql ="select customer_id as customerId,sum(answer_rewards) as answerRewards from customer_contest_answers with(nolock)" +
				" where contest_id="+contestId+ //and question_sl_no=12
				" and answer_rewards is not null and answer_rewards > 0" +
				" group by customer_id";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizCustomerContestAnswers> customerAnswers = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("answerRewards",Hibernate.DOUBLE);
			
			customerAnswers = query.setResultTransformer(Transformers.aliasToBean(QuizCustomerContestAnswers.class)).list();
			return customerAnswers;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Integer> getAllCustomersLifeLineUsageForCacheRefresh(Integer contestId){
		String sql ="select customer_id as customerId from customer_contest_answers with(nolock)" +
				" where contest_id=" +contestId+
				" and is_lifeline_used=1" +
				" group by customer_id";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		//List<QuizCustomerContestAnswers> customerAnswers = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			//query.addScalar("customerId",Hibernate.INTEGER);
			
			List<Integer> list = query.list();
			//customerAnswers = query.setResultTransformer(Transformers.aliasToBean(QuizCustomerContestAnswers.class)).list();
			return list;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public List<ContestGrandWinner> getAllActiveGrandWinnersByCustomerId(Integer customerId){
		String sql =" select cg.id as id,cg.customer_id as customerId, c.id as contestId,"
				+ "CASE WHEN c.extended_name is not null THEN c.extended_name WHEN "
				+ "c.extended_name is null THEN c.contest_name END as contestName,cg.reward_tickets as rewardTickets,cg.expiry_date as expiryDate" +
				" from contest_grand_winners cg with(nolock) " +
				" inner join contest c with(nolock) on c.id=cg.contest_id  " +
				" where cg.customer_id="+customerId+" and cg.status='ACTIVE' and cg.reward_tickets > 0 and cg.expiry_date>getdate()" +
				" order by cg.created_date";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<ContestGrandWinner> contestGrandWinners = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("contestId",Hibernate.INTEGER);
			query.addScalar("contestName",Hibernate.STRING);
			query.addScalar("rewardTickets",Hibernate.INTEGER);
			query.addScalar("expiryDate",Hibernate.TIMESTAMP);
			
			contestGrandWinners = query.setResultTransformer(Transformers.aliasToBean(ContestGrandWinner.class)).list();
			return contestGrandWinners;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public Integer getAllValidContestPromoCodesCount(){
		
		String sql =" select count(*) as promocodeCount" +
				" from contest with(nolock) " +
				" where status in ('EXPIRED','STARTED') and promo_expiry_date>getdate() and promotional_code is not null" +
				" ";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizPromoCodeDetail> promoList = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public Double getContestWinnerRewardPoints(Integer contestId){
		
		String sql =" select distinct reward_points  from contest_winners with(nolock) where contest_id=" +contestId;
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizPromoCodeDetail> promoList = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object> list = query.list();
			if(list != null && list.size() > 0) {
				return Double.valueOf(list.get(0).toString());
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public List<QuizPromoCodeDetail> getAllValidContestPromoCodes(Integer customerId){
		String sql =" select promotional_code as promoCode,discount_percentage as discountPercentage,promo_ref_id as promoRefId,promo_ref_name as promoRefName," +
				" promo_ref_type as promoRefType,promo_expiry_date as promoExpiryDate" +
				" from contest " +
				" where status in ('EXPIRED','STARTED') and promo_expiry_date>getdate() and promotional_code is not null" +
				" and id in (select contest_id from customer_contest_answers where customer_id="+customerId+")" +
				" order by promo_expiry_date";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<QuizPromoCodeDetail> promoList = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("promoCode",Hibernate.STRING);
			query.addScalar("discountPercentage",Hibernate.DOUBLE);
			query.addScalar("promoRefId",Hibernate.INTEGER);
			query.addScalar("promoRefName",Hibernate.STRING);
			query.addScalar("promoRefType",Hibernate.STRING);
			query.addScalar("promoExpiryDate",Hibernate.TIMESTAMP);
			
			promoList = query.setResultTransformer(Transformers.aliasToBean(QuizPromoCodeDetail.class)).list();
			return promoList;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public List<CustomerPromoRewardDetails> getAllValidContestPromoCodesForPromoPage(Integer customerId){
		String sql =" select id as id,"+customerId+" as customerId,contest_name as promoName,promotional_code as promoCode,discount_percentage as discountPercentage,promo_ref_id as promoRefId,promo_ref_name as promoRefName," +
				" promo_ref_type as promoRefType,promo_expiry_date as promoExpiryDate" +
				" ,'"+PromoType.CONTEST_PROMO+"' as promoType,0 as earnLifeCount,0 as freeTixCount" +
				" from contest with(nolock) " +
				" where status in ('EXPIRED','STARTED') and promo_expiry_date>getdate() and promotional_code is not null" +
				" and id in (select distinct contest_id from contest_participants with(nolock) where customer_id="+customerId+")" +
				//" and id in (select contest_id from  "+zonesApiLinkedServer+".web_service_tracking_new where  api_name ='QUIZJOINCONTEST' and contest_id is not null and customer_id="+customerId+")" +
				" union " +
				" select id as id,customer_id  as customerId,promo_name as promoName, promotional_code as promoCode,discount_percentage as discountPercentage,"
				+ "promo_ref_id as promoRefId,promo_ref_name as promoRefName," +
				" promo_ref_type as promoRefType,promo_expiry_date as promoExpiryDate," +
				" promo_type as promoType,earn_life_count as earnLifeCount,free_tix_count as freeTixCount" +
				" from customer_promo_reward_details with(nolock) " +
				" where status in ('ACTIVE') and promo_expiry_date>getdate() and customer_id="+customerId+
				//" order by promo_expiry_date" +
				" " +
				" order by promo_expiry_date";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<CustomerPromoRewardDetails> promoList = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("promoName",Hibernate.STRING);
			query.addScalar("promoCode",Hibernate.STRING);
			query.addScalar("discountPercentage",Hibernate.DOUBLE);
			query.addScalar("promoRefId",Hibernate.INTEGER);
			query.addScalar("promoRefName",Hibernate.STRING);
			query.addScalar("promoRefType",Hibernate.STRING);
			query.addScalar("promoExpiryDate",Hibernate.TIMESTAMP);
			query.addScalar("promoType",Hibernate.STRING);
			query.addScalar("earnLifeCount",Hibernate.INTEGER);
			query.addScalar("freeTixCount",Hibernate.INTEGER);
			
			promoList = query.setResultTransformer(Transformers.aliasToBean(CustomerPromoRewardDetails.class)).list();
			return promoList;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	public Integer updateContestSummaryTillDateDataTable() throws Exception {
		
		String sql = "exec SP_QMAP02a_load_contest_summary_rank_till_date";
		Query query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.executeUpdate();
			Integer count = 0;
			return count;
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
			//throw e;
		}
		return null;
	}
	public Integer updateContestSummaryThisWeekDataTable() throws Exception {
		
		String sql = "exec SP_QMAP01a_load_contest_summary_rank_for_week";
		Query query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.executeUpdate();
			Integer count = 0;
			return count;
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
			//throw e;
		}
		return null;
	}
	
	public Integer updateAllCustomersContestDataByContestId(Integer contestId) throws Exception {
		String sql = "update c set c.total_contest_played=total_contest_played+1," +
				" contest_high_score= case when c.contest_high_score<cca.highScore then cca.highScore else c.contest_high_score end," +
				" reward_life_cont_count=reward_life_cont_count+1," +
				" last_updated_timestamp=getdate()" +
				" from "+zonesApiLinkedServer+".customer c" +
				" inner join (select max(case when is_correct_answer=1 then question_sl_no else 0 end) as highScore," +
				" customer_id as customerId from customer_contest_answers WITH(NOLOCK) " +
				" where contest_id="+contestId +
				" group by customer_id)  as cca on cca.customerid=c.id";
		Query query = null;
		Session session = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			Integer count = query.executeUpdate();
			session.close();
			return count;
			
		} catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
			throw e;
		}
		//return null;
	}
	public Integer updateAllCustomersContestWinsDataByContestId(Integer contestId) throws Exception {
		String sql = "update c set c.total_contest_wins=total_contest_wins+1,last_updated_timestamp=getdate()" +
				" from "+zonesApiLinkedServer+".customer c " +
				" inner join contest_winners cw on cw.customer_id=c.id" +
				" where cw.contest_id="+contestId;
		Query query = null;
		Session session = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			Integer count = query.executeUpdate();
			session.close();
			return count;
			
		} catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
			throw e;
		}
		//return null;
	}
	public Integer getCustomerCountByContestId(Integer contestId) {
		String sql =" select count(distinct customer_id) as customerCount " + 
				" from contest_participants cp WITH(NOLOCK)  " + 
				"where cp.contest_id="+contestId+" and cp.status='active' ";
		
		SQLQuery query = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return 0;
	}
	public List<CustomerSearchDetails> getCustomerCountDetailsByContestIdandPageNo(Integer contestId,Integer pageNo,Integer maxRows){
		String sql =" select distinct c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath,min(cp.join_datetime) as joinDate " + 
				" from contest_participants cp WITH(NOLOCK)  " + 
				" inner join "+zonesApiLinkedServer+".customer c WITH(NOLOCK) on c.id=cp.customer_id " + 
				" where cp.status='active' and cp.contest_id= "+contestId + 
				" group by c.id,c.user_id,cust_image_path " + 
				" order by joinDate " + 
				" OFFSET ("+pageNo+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY  OPTION (RECOMPILE)";
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<CustomerSearchDetails> customerList = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			query.addScalar("joinDate",Hibernate.TIMESTAMP);
			
			customerList = query.setResultTransformer(Transformers.aliasToBean(CustomerSearchDetails.class)).list();
			return customerList;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public Integer getTotalNoOfContestQuestions() {
		String sql ="select count(cq.id) as quesCount from contest c with(nolock) inner join contest_questions cq with(nolock) on cq.contest_id=c.id " + 
				"where c.contest_name not like '%test%' and status not in ('DELETED') and c.id > 95";
		
		SQLQuery query = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return 0;
	}
	
	public List<HallOfFameDtls> getHallOfFameByDateFromView(Integer hallOfFameCount){

		//Get data from view
		String sql = " select top "+hallOfFameCount + " c.id as cuId,c.user_id as uId,c.cust_image_path as imgP," +
				" TotalTickets as rTix,TotalPoints as rPoints,CusRank as rRank" +
				" from contest_winners_rank_for_week_view csw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=csw.customer_id" +
		" where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by CusRank";

		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<HallOfFameDtls> hallOfFameList = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rTix",Hibernate.INTEGER);
			query.addScalar("rPoints",Hibernate.DOUBLE);
			query.addScalar("rRank",Hibernate.INTEGER);
			query.addScalar("cuId",Hibernate.INTEGER);
			query.addScalar("uId",Hibernate.STRING);
			query.addScalar("imgP",Hibernate.STRING);
			
			hallOfFameList = query.setResultTransformer(Transformers.aliasToBean(HallOfFameDtls.class)).list();
			System.out.println("This Week cache Query : "+sql+" : "+ new Date());
			return hallOfFameList;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public List<HallOfFameDtls> getHallOfFameByTillDateFromView(Integer summaryListCount){
//Get Data from view
		String sql = " select top "+summaryListCount+" c.id as cuId,c.user_id as uId,c.cust_image_path as imgP," +
				" TotalTickets as rTix,TotalPoints as rPoints,CusRank as rRank" +
				" from contest_winners_rank_till_date_view csw with(nolock)" +
				" inner join "+zonesApiLinkedServer+".customer c with(nolock) on c.id=csw.customer_id" +
				" where c.product_type='REWARDTHEFAN' and c.is_test_account=0 order by CusRank";
//Get Data from table
		
		SQLQuery query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		List<HallOfFameDtls> hallOfFameList = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			query.addScalar("rTix",Hibernate.INTEGER);
			query.addScalar("rPoints",Hibernate.DOUBLE);
			query.addScalar("rRank",Hibernate.INTEGER);
			query.addScalar("cuId",Hibernate.INTEGER);
			query.addScalar("uId",Hibernate.STRING);
			query.addScalar("imgP",Hibernate.STRING);

			hallOfFameList = query.setResultTransformer(Transformers.aliasToBean(HallOfFameDtls.class)).list();
			System.out.println("Till Date cache Query : "+sql+" : "+ new Date());
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return hallOfFameList;
	}
	
	
	public List<Integer> getQuizParticipantsByContestIdOld(Integer contestId){
		String sql ="select distinct customer_id from contest_participants with(nolock) where contest_id = "+contestId;
		SQLQuery query = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			return list;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Integer> getContestIssueNotificationIds(){
		String sql ="select distinct customer_id from RTFQuizMaster.dbo.customer_contest_answers where contest_id=517 and question_sl_no =8 and answer='B' ";
		SQLQuery query = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			return list;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	public List<Integer> getContestQuestionIssueCustomerIds(){
		String sql ="select distinct customer_id from RTFQuizMaster.dbo.customer_contest_answers where  contest_id=540 and question_sl_no=1 "
				+ "and answer in ('A','B') ";
		SQLQuery query = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			return list;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	public Integer getContestCreditProcessedCustomerCount(Integer contestId){
		String sql ="select count(*) as totCredited from quiz_super_fan_tracking where contest_id = "+contestId+" "
				+ "and this_contest_participant_stars <> 0";
		SQLQuery query = null;
		try {
			querySession = QuizQueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QuizQueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return 0;
	}
	
	
	
}
