package com.rtfquiz.webservices.dao.implementaion;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;

import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.utils.list.QuizAnswerCountDetails;
import com.rtfquiz.webservices.utils.list.QuizPromoCodeDetail;
import com.zonesws.webservices.dao.implementaion.EventDAO;
import com.zonesws.webservices.data.Affiliate;
import com.zonesws.webservices.data.AffiliateCashRewardHistory;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Broker;
import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.data.CountryCity;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.EventArtist;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.data.PaypalApi;
import com.zonesws.webservices.data.PreOrderPageAudit;
import com.zonesws.webservices.data.RTFCategorySearch;
import com.zonesws.webservices.data.Venue;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.tmat.dao.implementaion.QueryManagerDAO;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.RealTicketSectionDetails;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZoneEvent;
import com.zonesws.webservices.utils.list.ArtistState;
import com.zonesws.webservices.utils.list.Item;
import com.zonesws.webservices.utils.list.NearestStates;
import com.zonesws.webservices.utils.list.StripeCredentials;
import com.zonesws.webservices.utils.list.TeamCardImageDetails;

public class QuizSummaryManagerDAO {
	
	private SessionFactory sessionFactory;
	private static String zonesApiLinkedServer;// = "ZonesApiPlatformV2_Sanbox.dbo";
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public static String getZonesApiLinkedServer() {
		return zonesApiLinkedServer;
	}

	public void setZonesApiLinkedServer(String zonesApiLinkedServer) {
		QuizSummaryManagerDAO.zonesApiLinkedServer = zonesApiLinkedServer;
	}

	public int updateCustomerPromoCodeAndContestOrderStats() throws Exception {
		Session session = null;
		int count = 0;		
		try {
			session = getSessionFactory().openSession();
			
			String query = "update c set c.is_promo_code=1 from  "+zonesApiLinkedServer+".customer c" +
					" where c.product_Type='rewardthefan' and c.is_promo_code=0 " +
					" and c.id in (select distinct customer_id from contest_participants ca" +
					" inner join contest cc on cc.id=ca.contest_id and promo_expiry_date>getdate()" +
					" union" +
					" select distinct customer_id from customer_promo_reward_details ca where" +
					" status='ACTIVE' and promo_expiry_date>getdate())";
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			
			query = "update c set c.is_promo_code=0 from  "+zonesApiLinkedServer+".customer c" +
					" where c.product_Type='rewardthefan' and c.is_promo_code=1" +
					" and c.id not in (select distinct customer_id from contest_participants ca" +
					" inner join contest cc on cc.id=ca.contest_id and promo_expiry_date>getdate()" +
					" union" +
					" select distinct customer_id from customer_promo_reward_details ca where" +
					" status='ACTIVE' and promo_expiry_date>getdate())";
			sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
			query = "update c set c.is_contest_order=1 from  "+zonesApiLinkedServer+".customer c" +
					" where product_Type='rewardthefan' and c.is_contest_order=0" +
					" and c.id in (select distinct customer_id from contest_grand_winners where status='ACTIVE'and expiry_date>getdate())";
			sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
			query = "update c set c.is_contest_order=0 from "+zonesApiLinkedServer+".customer c" +
					" where product_Type='rewardthefan' and c.is_contest_order=1" +
					" and c.id not in (select distinct customer_id from contest_grand_winners where status='ACTIVE'and expiry_date>getdate() )";
			sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
			session.close();
			
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}
	
	public int updateCustomersRewardEarnLifeStats(Integer rewadLifeThreshold) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			session = getSessionFactory().openSession();
			
			String query = "INSERT INTO customer_promo_reward_details(customer_id,promo_name,promo_type,promotional_code,earn_life_count," +
					" free_tix_count,discount_percentage,promo_ref_id,promo_ref_type,promo_ref_name,promo_expiry_date,promo_offer_id," +
					" status,created_date,created_by,updated_date,updated_by,apply_date)" +
					" select c.id,'FREE EARN LIFE','REWARD_EARNLIFE',null,1,0,0,null,null,null,dateadd(MM,12,getdate()),null,'ACTIVE',getdate(),'AUTO',null,null,null" +
					" from  ZonesApiPlatformV2_Sanbox.dbo.customer c where product_type='REWARDTHEFAN' and" +
					" reward_life_cont_count>"+rewadLifeThreshold;
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			
			query = "update c set c.reward_life_cont_count=reward_life_cont_count-"+rewadLifeThreshold+",last_updated_timestamp=getdate()" +
					" from  ZonesApiPlatformV2_Sanbox.dbo.customer c where product_type='REWARDTHEFAN' and" +
					" reward_life_cont_count>"+rewadLifeThreshold;
			
			sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
			session.close();
			
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}
	
	public Integer updateContestSummaryTillDateDataTable() throws Exception {
		
		String sql = "exec SP_QMAP02a_load_contest_summary_rank_till_date";
		Session session = null;
		Connection connection = null;
		try {
			/*session = getSessionFactory().openSession();
			connection = session.connection();
			connection.setAutoCommit(false);
			PreparedStatement st = connection.prepareStatement("{call SP_QMAP02a_load_contest_summary_rank_till_date}");
			st.execute();
			connection.commit();
			return 0;*/
			
			connection = getSessionFactory().openSession().connection();  
			CallableStatement cs = connection.prepareCall( "{ call SP_QMAP02a_load_contest_summary_rank_till_date }");
			cs.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
			//throw e;
		} finally {
			if(connection != null) {
				connection.close();
			}
			if(session != null) {
				session.close();
			}
		}
		return 0;
	}
	public Integer updateContestSummaryThisWeekDataTable() throws Exception {
		
		String sql = "exec SP_QMAP01a_load_contest_summary_rank_for_week";
		Session session = null;
		Connection connection = null;
		try {
			/*session = getSessionFactory().openSession();
			connection = session.connection();
			connection.setAutoCommit(false);
			PreparedStatement st = connection.prepareStatement("{call SP_QMAP01a_load_contest_summary_rank_for_week}");
			st.execute();
			connection.commit();*/
			//return 0;
			
			connection = getSessionFactory().openSession().connection();  
			CallableStatement cs = connection.prepareCall( "{ call SP_QMAP01a_load_contest_summary_rank_for_week }");
			cs.execute();

			
		} catch (Exception e) {
			e.printStackTrace();
			//throw e;
		} finally {
			if(connection != null) {
				connection.close();
			}
			if(session != null) {
				session.close();
			}
		}
		return 0;
	}
	
	public Integer updateContestSummaryThisWeekDataTableNew() {
		Session session = null;
	try {
		session = getSessionFactory().openSession();
		/*eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}*/
		
		//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
		String queryStr = "exec SP_QMAP01a_load_contest_summary_rank_for_week";
		
		Query query = session.createSQLQuery(queryStr);
		//System.out.println(query);
		Integer count = query.executeUpdate();
		System.out.println("This Week Count : "+count);
		
		}catch (Exception e) {
			/*if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}*/
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return 0;
	
}
	public Integer updateContestSummaryTillDateDataTableNew() {
		Session session = null;
	try {
		session = getSessionFactory().openSession();
		/*eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}*/
		
		//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
		String queryStr = "exec SP_QMAP02a_load_contest_summary_rank_till_date";
		
		Query query = session.createSQLQuery(queryStr);
		//System.out.println(query);
		Integer count = query.executeUpdate();
		System.out.println("Till Date Count : "+count);
		
		}catch (Exception e) {
			/*if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}*/
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return 0;
	
}

	
}
