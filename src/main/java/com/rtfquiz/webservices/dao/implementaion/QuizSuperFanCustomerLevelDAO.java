package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizSuperFanCustomerLevel;

/**
 * class having db related methods for QuizSuperFanCustomerLevel
 * @author Ulaganathan
 *
 */
public class QuizSuperFanCustomerLevelDAO extends HibernateDAO<Integer, QuizSuperFanCustomerLevel> implements com.rtfquiz.webservices.dao.services.QuizSuperFanCustomerLevelDAO {
	
	public List<QuizSuperFanCustomerLevel> getAllActiveLevels() {
		return find("from QuizSuperFanCustomerLevel where status =? order by levelNo ASC", new Object[]{"ACTIVE"});
	}
	 	
}
