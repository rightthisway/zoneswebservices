package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizSuperFanLevelConfig;

/**
 * class having db related methods for QuizSuperFanLevelConfig
 * @author Ulaganathan
 *
 */
public class QuizSuperFanLevelConfigDAO extends HibernateDAO<Integer, QuizSuperFanLevelConfig> implements com.rtfquiz.webservices.dao.services.QuizSuperFanLevelConfigDAO {
	
	public List<QuizSuperFanLevelConfig> getAllActiveLevels() {
		return find("from QuizSuperFanLevelConfig where status =? order by levelNo", new Object[]{"ACTIVE"});
	}
	 	
}
