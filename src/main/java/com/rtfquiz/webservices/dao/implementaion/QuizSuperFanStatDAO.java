package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.QuizSuperFanStat;

/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class QuizSuperFanStatDAO extends HibernateDAO<Integer, QuizSuperFanStat> implements com.rtfquiz.webservices.dao.services.QuizSuperFanStatDAO {
	
	public QuizSuperFanStat getQuizSuperFanStat(Integer customerId) {
		return findSingle("from QuizSuperFanStat where customerId=?", new Object[]{customerId});
	}
	
	public List<QuizSuperFanStat> getSuperFanWinnersByChances(Integer noOfChances) {
		return find("from QuizSuperFanStat where totalNoOfChances > ?", new Object[]{noOfChances});
	}
	
	public List<QuizSuperFanStat> getAllSuperFanCustomerData() {
		return find("from QuizSuperFanStat where totalNoOfChances > 0");
	}
	 
}
