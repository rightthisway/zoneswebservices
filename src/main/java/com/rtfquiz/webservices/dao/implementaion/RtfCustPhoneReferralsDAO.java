package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.RtfCustPhoneReferrals;
import com.rtfquiz.webservices.data.RtfCustProfileAnswers;
import com.rtfquiz.webservices.data.RtfCustProfileQuestions;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;


public class RtfCustPhoneReferralsDAO extends HibernateDAO<Integer, RtfCustPhoneReferrals> implements com.rtfquiz.webservices.dao.services.RtfCustPhoneReferralsDAO {
	
	
	public List<RtfCustPhoneReferrals> getAllActiveRtfCustPhoneReferrals(Integer customerId){
		return find("from RtfCustPhoneReferrals where cuId=? ", new Object[]{customerId});
	}
	
	public List<String> getAllReferredPhoneNosByCustomerId(Integer customerId) {
		Session session = getSession();
		List<String> phoneNos=null;
	try {
		String queryStr = "select phone from rtf_cust_phone_referrals where cust_id="+customerId;
		
		SQLQuery query = session.createSQLQuery(queryStr);
		
		phoneNos = (List<String>)query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return phoneNos;
	
	}
}
