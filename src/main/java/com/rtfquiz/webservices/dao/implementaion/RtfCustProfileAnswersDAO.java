package com.rtfquiz.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.RtfCustProfileAnswers;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.list.CustProfileAnswerCountDtls;


public class RtfCustProfileAnswersDAO extends HibernateDAO<Integer, RtfCustProfileAnswers> implements com.rtfquiz.webservices.dao.services.RtfCustProfileAnswersDAO {
	
	
	public List<RtfCustProfileAnswers> getRtfCustProfileAnswersByCustoemrId(Integer customerId) {
		return find("from RtfCustProfileAnswers where cuId = ?", new Object[]{customerId});
	}
	public List<RtfCustProfileAnswers> getAllActiveRtfCustProfileAnswersByCustoemrIdAndProfileType(Integer customerId,String profileType) {
		return find("from RtfCustProfileAnswers where status='ACTIVE' and cuId = ?", new Object[]{customerId});
	}
	public RtfCustProfileAnswers getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(Integer customerId,Integer questionId) {
		return findSingle("from RtfCustProfileAnswers where status='ACTIVE' and cuId = ? and qId=?", new Object[]{customerId,questionId});
	}
	
	public List<RtfCustProfileAnswers> getRtfCustProfileQuestionAnswersByCustomerIdAndProfileType(Integer customerId,String profileType) {
		Session session = getSession();
		List<RtfCustProfileAnswers> rtfCustProfileAsn = null;
	try {
		String queryStr = "select q.id as qId,q.qsl_no as qSlNo,q.question as question,q.question_type as qType,q.points as points," + 
				" q.profile_type as pType,a.answer as answer,a.id as id,q.points as qPoints" + 
				" from rtf_cust_profile_questions q" + 
				" left join rtf_cust_profile_answers a on a.question_id=q.id and a.customer_id=" + customerId+
				" where q.status='active' "+// and q.profile_type='"+profileType+"'"+ 
				" order by q.qsl_no";
		
		SQLQuery query = session.createSQLQuery(queryStr);
		query.addScalar("qId",Hibernate.INTEGER);
		query.addScalar("qSlNo",Hibernate.INTEGER);
		query.addScalar("question",Hibernate.STRING);
		query.addScalar("qType",Hibernate.STRING);
		query.addScalar("points",Hibernate.INTEGER);
		query.addScalar("answer",Hibernate.STRING);
		query.addScalar("pType",Hibernate.STRING);
		query.addScalar("id",Hibernate.INTEGER);
		query.addScalar("qPoints",Hibernate.INTEGER);
		
		rtfCustProfileAsn = query.setResultTransformer(Transformers.aliasToBean(RtfCustProfileAnswers.class)).list();
		
		return rtfCustProfileAsn;
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return null;
	
	}
	public List<RtfCustProfileAnswers> getRtfCustProfileQuestions(String profileType) {
		Session session = getSession();
		List<RtfCustProfileAnswers> rtfCustProfileAsn = null;
	try {
		String queryStr = "select id as qid,qsl_no as qslNo,question as question,question_type as qType,points as points,status as status," + 
				" profile_type as pType,q.points as qPoints from rtf_cust_profile_questions " + 
				" where status='active' and profile_type='"+profileType+"'" + 
				" order by qsl_no ";
		
		SQLQuery query = session.createSQLQuery(queryStr);
		query.addScalar("qid",Hibernate.INTEGER);
		query.addScalar("qslNo",Hibernate.INTEGER);
		query.addScalar("question",Hibernate.STRING);
		query.addScalar("question_type",Hibernate.STRING);
		query.addScalar("points",Hibernate.INTEGER);
		query.addScalar("status",Hibernate.STRING);
		query.addScalar("pType",Hibernate.STRING);
		query.addScalar("qPoints",Hibernate.INTEGER);

		rtfCustProfileAsn = query.setResultTransformer(Transformers.aliasToBean(RtfCustProfileAnswers.class)).list();
		
		return rtfCustProfileAsn;
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return null;
	
	}
	public CustProfileAnswerCountDtls getRtfCustoemrProfileQuestinsAndAnswersCount(Integer customerId) {
		Session session = getSession();
		Integer percentage = 0;
	try {
		String queryStr = " select sum(IIF(a.id is null, 0, 1)) as ansCount,count(q.id) as questCount" + 
				" from rtf_cust_profile_questions q " + 
				" left join rtf_cust_profile_answers a on a.question_id=q.id and a.customer_id="+customerId+""+
				" where q.status='active' ";
		
		SQLQuery query = session.createSQLQuery(queryStr);
		query.addScalar("ansCount",Hibernate.INTEGER);
		query.addScalar("questCount",Hibernate.INTEGER);
		
		List<CustProfileAnswerCountDtls> result =  query.setResultTransformer(Transformers.aliasToBean(CustProfileAnswerCountDtls.class)).list();
		if(result != null && result.size() > 0) {
			return result.get(0);
		}
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return null;
	
	}
	public Integer getRtfCustProfilePercentage(Integer customerId) {
		Session session = getSession();
		Integer percentage = 0;
	try {
		String queryStr = "declare @quests int = 0,@ans int =0;" + 
				" set @quests = (select count(*) as cnt from rtf_cust_profile_questions with(nolock) where status='active');" + 
				" set @ans = (select count(*) as cnt from rtf_cust_profile_answers where status='active' and customer_id="+customerId+");" + 
				" select round(@ans * 100/@quests,0) as perc; ";
		
		SQLQuery query = session.createSQLQuery(queryStr);
		
		List<Integer> result = (List<Integer>)query.list();
		for (Integer object : result) {
			percentage = object;
		}
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return percentage;
	
	}
}
