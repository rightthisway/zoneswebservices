package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.RtfCustProfileAnswers;
import com.rtfquiz.webservices.data.RtfCustProfileQuestions;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;


public class RtfCustProfileQuestionsDAO extends HibernateDAO<Integer, RtfCustProfileQuestions> implements com.rtfquiz.webservices.dao.services.RtfCustProfileQuestionsDAO {
	
	
	public List<RtfCustProfileQuestions> getAllActiveRtfCustProfileQuestions(){
		return find("from RtfCustProfileQuestions where status='ACTIVE' ");
	}
	public List<RtfCustProfileQuestions> getAllActiveRtfCustProfileQuestionsByProfileType(String profileType){
		return find("from RtfCustProfileQuestions where status='ACTIVE' and pType=?", new Object[]{profileType});
	}
}
