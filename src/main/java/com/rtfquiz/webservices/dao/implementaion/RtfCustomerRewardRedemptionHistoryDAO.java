package com.rtfquiz.webservices.dao.implementaion;

import com.rtfquiz.webservices.data.RtfCustomerRewardRedemptionHistory;

public class RtfCustomerRewardRedemptionHistoryDAO extends HibernateDAO<Integer, RtfCustomerRewardRedemptionHistory> implements 
com.rtfquiz.webservices.dao.services.RtfCustomerRewardRedemptionHistoryDAO{

	public RtfCustomerRewardRedemptionHistory getActiveReferralRewardSetting() {
		return findSingle("FROM RtfCustomerRewardRedemptionHistory WHERE status=? ",new Object[]{"ACTIVE"});
	}
	 
}
