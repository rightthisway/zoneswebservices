package com.rtfquiz.webservices.dao.implementaion;

import java.util.List;

import com.rtfquiz.webservices.data.RtfPointsConversionSetting;

public class RtfPointsConversionSettingDAO extends HibernateDAO<Integer, RtfPointsConversionSetting> implements com.rtfquiz.webservices.dao.services.RtfPointsConversionSettingDAO{

	public List<RtfPointsConversionSetting> getActiveConversionSetting() {
		return find("FROM RtfPointsConversionSetting WHERE status=? ",new Object[]{true});
	}
	 
}
