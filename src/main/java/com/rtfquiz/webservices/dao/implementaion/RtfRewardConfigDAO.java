package com.rtfquiz.webservices.dao.implementaion;

import com.rtfquiz.webservices.data.RtfRewardConfig;
import com.zonesws.webservices.enums.SourceType;

public class RtfRewardConfigDAO extends HibernateDAO<Integer, RtfRewardConfig> implements com.rtfquiz.webservices.dao.services.RtfRewardConfigDAO{

	public RtfRewardConfig getActiveReferralRewardSetting() {
		return findSingle("FROM RtfRewardConfig WHERE status=? ",new Object[]{true});
	}
	public RtfRewardConfig getActiveReferralRewardSettingBySourceType(SourceType sourceType) {
		return findSingle("FROM RtfRewardConfig WHERE status=? and sourceType=? ",new Object[]{Boolean.TRUE,sourceType});
	}
	 
}
