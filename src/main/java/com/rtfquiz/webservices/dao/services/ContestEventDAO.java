package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.ContestEvent;

public interface ContestEventDAO extends RootDAO<Integer, ContestEvent> {
	
	public List<ContestEvent> getContestEvents(Integer contestId);

}
