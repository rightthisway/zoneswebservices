package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.ContestEventRequest;

public interface ContestEventRequestDAO extends RootDAO<Integer, ContestEventRequest> {

}
