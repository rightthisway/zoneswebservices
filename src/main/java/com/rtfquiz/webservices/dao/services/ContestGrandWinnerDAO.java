package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.ContestGrandWinner;

public interface ContestGrandWinnerDAO extends RootDAO<Integer, ContestGrandWinner>{

	public ContestGrandWinner getGrandWinnerByCustomerId(Integer customerId,Integer contestId);
	public List<ContestGrandWinner> getRecentContentWinners();
	public void updateWinnerNotification(Integer id);
	public List<ContestGrandWinner> getAllActiveGrandWinnerContest();
	public ContestGrandWinner getOrderNotCreatedGrandWinner(Integer contestId);
	public List<ContestGrandWinner> getAllJackpotWinnersByContestId(Integer contestId);
	public List<ContestGrandWinner> getAllProcessedJackpotWinnersByContestId(Integer contestId);
}
