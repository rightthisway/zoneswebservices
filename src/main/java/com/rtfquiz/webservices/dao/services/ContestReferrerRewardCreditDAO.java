package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;

public interface ContestReferrerRewardCreditDAO extends RootDAO<Integer, ContestReferrerRewardCredit>{

	public ContestReferrerRewardCredit getRewardCreditByContestId(Integer contestId);
	public ContestReferrerRewardCredit getAffiliateRewardCreditByContestId(Integer contestId);
	public ContestReferrerRewardCredit getRewardCreditByContestIdAndType(Integer contestId, String rewardType);
	public List<ContestReferrerRewardCredit> getAllContestCredits(Integer contestId);
}
