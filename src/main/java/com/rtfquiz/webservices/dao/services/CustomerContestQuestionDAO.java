package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.CustomerContestQuestion;

public interface CustomerContestQuestionDAO extends RootDAO<Integer, CustomerContestQuestion>{

}
