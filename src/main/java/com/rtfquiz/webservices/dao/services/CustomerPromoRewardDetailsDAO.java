package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.CustomerPromoRewardDetails;

public interface CustomerPromoRewardDetailsDAO extends RootDAO<Integer, CustomerPromoRewardDetails>{

	public CustomerPromoRewardDetails getEarnLifePromoRewardByPromoRefIdandCustomerId(Integer promoRewardId,Integer customerId);
	public CustomerPromoRewardDetails getRewardTixPromoRewardByPromoRefIdandCustomerId(Integer promoRewardId,Integer customerId);
	public List<CustomerPromoRewardDetails> getAllActiveCustomerPromoRewardDetailsByCustomerId(Integer customerId);
}
