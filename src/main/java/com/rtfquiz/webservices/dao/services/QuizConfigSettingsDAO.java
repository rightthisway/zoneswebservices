package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.QuizConfigSettings;

/**
 * interface having db related methods for Customer
 * @author Tamil
 *
 */
public interface QuizConfigSettingsDAO  extends RootDAO<Integer, QuizConfigSettings>{
	
	public QuizConfigSettings getConfigSettings();
}



