package com.rtfquiz.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.rtfquiz.webservices.data.QuizContest;

/**
 * interface having db related methods for Customer
 * @author Tamil
 *
 */
public interface QuizContestDAO  extends RootDAO<Integer, QuizContest>{
	
	public QuizContest getNextQuizContest(Date todayDate,String contestType);
	public List<QuizContest> getUpCommingQuizContests(Date todayDate,String contestType);
	public List<QuizContest> getAllMobileContestsToSendNotification(Date fifteenMinsDate,Date threeMinsDate);
	public List<QuizContest> getAllMobileContestsToSendNotification(Date threeMinsDate);
	public QuizContest getCurrentStartedContest(String contestType);
	public List<QuizContest> getAllMobileContestByDate(Date fromDate,Date toDate);
	public List<QuizContest> getRecentQuizContest();
	public List<QuizContest> getRecentQuizContest(Date fromDate, Date toDate) ;
	public List<QuizContest> getUpcomingContest(Date fromDate, Date toDate);
	public List<QuizContest> getAllCompletedMobileContestByDate(Date fromDate,Date toDate);
	public List<QuizContest> getActiveContestByDates(Date fromDate, Date toDate);
	public List<QuizContest> getAllCompleted2018YearEndContestByDate(Date fromDate,Date toDate, Date offerFromDate, Date offerToDate);
	public QuizContest getCompletedContestByContestID(Integer contestId);
}



