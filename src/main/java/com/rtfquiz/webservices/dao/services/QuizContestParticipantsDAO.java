package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.QuizContestParticipants;

public interface QuizContestParticipantsDAO extends RootDAO<Integer, QuizContestParticipants>{
	public void updateQuizContestParticipantsExistContest(Integer customerId,Integer contestId);
	public void updateExistingQuizContestParticipantsByJoinContest(Integer customerId,Integer contestId);
	public Boolean isExistingContestant(Integer customerId,Integer contestId);
	public void updateContestParticipantsForContestResetByContestId(Integer contestId);

}
