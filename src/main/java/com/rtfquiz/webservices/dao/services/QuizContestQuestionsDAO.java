package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestQuestions;


public interface QuizContestQuestionsDAO  extends RootDAO<Integer, QuizContestQuestions>{
	
	public QuizContestQuestions getQuizContestQuestionById(Integer questionId);
	
	public QuizContestQuestions getQuizContestQuestionByContestIdandQuestionSlNo(Integer contestId,Integer questionSlNo);
	
	public List<QuizContestQuestions> getQuizContestQuestionsByContestId(Integer ContestId);
}



