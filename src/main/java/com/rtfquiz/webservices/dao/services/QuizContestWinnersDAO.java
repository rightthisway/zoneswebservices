package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;

/**
 * interface having db related methods for QuizContestWinners
 * @author Tamil
 *
 */
public interface QuizContestWinnersDAO  extends RootDAO<Integer, QuizContestWinners>{
	
	public List<QuizContestWinners> getContestWinnersByContestId(Integer contestId);
	public QuizContestWinners getQuizContestWinnerByCustomerIdAndContestId(Integer customerId,Integer contestId);
	public List<QuizContestWinners> getQuizContestGrandWinnersByContestId(Integer contestId);
}



