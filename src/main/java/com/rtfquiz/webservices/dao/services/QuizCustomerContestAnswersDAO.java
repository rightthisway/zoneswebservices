package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;


public interface QuizCustomerContestAnswersDAO  extends RootDAO<Integer, QuizCustomerContestAnswers>{
	
	public QuizCustomerContestAnswers getCustomerContestAnswerByCustomerIdandQuestionId(Integer customerId,Integer questionId );
	public QuizCustomerContestAnswers getLifeLineUsedContestAnswerByCustomerIdandContestId(Integer customerId,Integer contestId );
	public List<QuizCustomerContestAnswers> getAllCorrectAnsweredDatas(Integer contestId);
	public QuizCustomerContestAnswers getLatestContestAnswerByCustomerIdandContestId(Integer customerId,Integer contestId );
	public Double getCustomerContestAnswerRewards(Integer customerId,Integer contestId) ;
	public Boolean checkCustomerContestLifeLineUsageByCustomerIdandContestId(Integer customerId,Integer contestId);
	public Integer totalNoOfContestQuestions();
	public Integer getCorrectAnsweredQuestionByCustomer(Integer customerId);
	public Integer getNoOfQuesAnsweredByCustomer(Integer customerId);
	public List<QuizCustomerContestAnswers> getFirstQuestionAnsweredCustomers(Integer contestId);
}



