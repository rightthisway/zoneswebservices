package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerContestDetails;


public interface QuizCustomerContestDetailsDAO  extends RootDAO<Integer, QuizCustomerContestDetails>{
	
	public QuizCustomerContestDetails getCustomerContestDetailsByCustomerIdandContestId(Integer customerId,Integer contestId );
	
}



