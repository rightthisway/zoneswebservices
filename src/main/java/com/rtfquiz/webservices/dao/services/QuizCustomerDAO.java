package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomer;

/**
 * interface having db related methods for Customer
 * @author Tamil
 *
 */
public interface QuizCustomerDAO  extends RootDAO<Integer, QuizCustomer>{
	/**
	 * method to get Customer by email and password
	 * @param email, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 */
	public QuizCustomer getQuizCustomerByEmailId(String email);
	/**
	 * method to get QuizCustomer by email and password
	 * @param emailId, email address of a QuizCustomer
	 * @param password , password of a QuizCustomer
	 * @return QuizCustomer
	 */
	public QuizCustomer getQuizCustomerById(Integer customerId);
	
	/**
	 *  method to get QuizCustomer by QuizCustomer Id and Product Type
	 * @param QuizCustomerId,  QuizCustomer Id
	 * @param productType,  Product Type
	 * @return QuizCustomer
	 */
	public QuizCustomer getQuizCustomerByEmail(String email);
	
	public QuizCustomer getQuizCustomerByUserId(String userId);
	
	public QuizCustomer getQuizCustomerByPhone(String phoneNo);
	
}



