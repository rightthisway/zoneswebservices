package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerFriend;

public interface QuizCustomerFriendDAO extends RootDAO<Integer, QuizCustomerFriend>{

	public List<QuizCustomerFriend> getQuizCustomerFriendRecords(Integer customerId,Integer friendCustomerId);
	public QuizCustomerFriend getPendingFriendRequest(Integer customerId,Integer friendCustomerId);
}
