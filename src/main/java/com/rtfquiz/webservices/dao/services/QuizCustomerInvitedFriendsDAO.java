package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerInvitedFriends;

public interface QuizCustomerInvitedFriendsDAO extends RootDAO<Integer, QuizCustomerInvitedFriends>{

	public List<QuizCustomerInvitedFriends> getAllActiveInvitedFriendsByCustomerId(Integer customerId);
	public QuizCustomerInvitedFriends getInvitedFriendsByCustomerIdandPhoneNo(Integer customerId,String phone);
}
