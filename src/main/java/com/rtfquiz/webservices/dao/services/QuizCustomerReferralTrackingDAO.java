package com.rtfquiz.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;

/**
 * interface having db related methods for Customer
 * @author Tamil
 *
 */
public interface QuizCustomerReferralTrackingDAO  extends RootDAO<Integer, QuizCustomerReferralTracking>{
	
	public QuizCustomerReferralTracking getReferralTrackingByCustomerId(Integer customerId);
	public QuizCustomerReferralTracking getReferralTrackingByCustomerIdandReferralCode(Integer customerId,String referralCode);
	public QuizCustomerReferralTracking getReferralTrackingByReferralCode(String referralCode);
	public Integer getTotalNoOfReferrals(Integer customerId);
	public List<QuizCustomerReferralTracking> getAllAffiliateReferrals();
	public List<QuizCustomerReferralTracking> getAllYearEndOfferApplicableReferrals(Date fromDate,Date toDate);
	public List<QuizCustomerReferralTracking> getAllSuperFanOfferApplicableReferrals(Date fromDate,Date toDate);
}



