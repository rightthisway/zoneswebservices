package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.QuizFirebaseCallBackTracking;

public interface QuizFirebaseCallBackTrackingDAO extends RootDAO<Integer, QuizFirebaseCallBackTracking>{

}
