package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizOTPTracking;

/**
 * interface having db related methods for Customer
 * @author Tamil
 *
 */
public interface QuizOTPTrackingDAO  extends RootDAO<Integer, QuizOTPTracking>{
	
	public List<QuizOTPTracking> getQuizOTPTrackingByPhoneNo(String phone);
	public List<QuizOTPTracking> getActiveOTPByPhoneNo(String phone);
	public List<QuizOTPTracking> getAllActiveOTP();
	public boolean deleteAllOTPByPhoneNo(String phoneNo);
}



