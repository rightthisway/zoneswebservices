package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizSuperFanCustomerLevel;

/**
 * interface having db related methods for QuizSuperFanCustomerLevel
 * @author Ulaganathan
 *
 */
public interface QuizSuperFanCustomerLevelDAO  extends RootDAO<Integer, QuizSuperFanCustomerLevel>{
	
	public List<QuizSuperFanCustomerLevel> getAllActiveLevels();
}



