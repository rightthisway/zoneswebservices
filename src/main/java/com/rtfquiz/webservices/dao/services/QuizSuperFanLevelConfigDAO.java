package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizSuperFanLevelConfig;

/**
 * interface having db related methods for QuizSuperFanLevelConfig
 * @author Ulaganathan
 *
 */
public interface QuizSuperFanLevelConfigDAO  extends RootDAO<Integer, QuizSuperFanLevelConfig>{
	
	public List<QuizSuperFanLevelConfig> getAllActiveLevels();
}



