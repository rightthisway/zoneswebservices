package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.QuizSuperFanStat;

/**
 * interface having db related methods for QuizSuperFanStat
 * @author Tamil
 *
 */
public interface QuizSuperFanStatDAO  extends RootDAO<Integer, QuizSuperFanStat>{
	
	public QuizSuperFanStat getQuizSuperFanStat(Integer customerId);
	public List<QuizSuperFanStat> getSuperFanWinnersByChances(Integer noOfChances);
	public List<QuizSuperFanStat> getAllSuperFanCustomerData();
}



