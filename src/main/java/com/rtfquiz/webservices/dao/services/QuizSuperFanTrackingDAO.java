package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.QuizSuperFanTracking;

/**
 * interface having db related methods for QuizSuperFanTracking
 * @author Tamil
 *
 */
public interface QuizSuperFanTrackingDAO  extends RootDAO<Integer, QuizSuperFanTracking>{
	
	 
}



