package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.RtfCustPhoneReferrals;


public interface RtfCustPhoneReferralsDAO  extends RootDAO<Integer, RtfCustPhoneReferrals>{
	
	public List<RtfCustPhoneReferrals> getAllActiveRtfCustPhoneReferrals(Integer customerId);
	public List<String> getAllReferredPhoneNosByCustomerId(Integer customerId);
}



