package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.RtfCustProfileAnswers;
import com.zonesws.webservices.utils.list.CustProfileAnswerCountDtls;


public interface RtfCustProfileAnswersDAO  extends RootDAO<Integer, RtfCustProfileAnswers>{
	
	
	public List<RtfCustProfileAnswers> getRtfCustProfileAnswersByCustoemrId(Integer customerId);
	public List<RtfCustProfileAnswers> getAllActiveRtfCustProfileAnswersByCustoemrIdAndProfileType(Integer customerId,String profileType);
	public List<RtfCustProfileAnswers> getRtfCustProfileQuestionAnswersByCustomerIdAndProfileType(Integer customerId,String profileType);
	public RtfCustProfileAnswers getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(Integer customerId,Integer questionId);
	public List<RtfCustProfileAnswers> getRtfCustProfileQuestions(String profileType);
	public Integer getRtfCustProfilePercentage(Integer customerId);
	public CustProfileAnswerCountDtls getRtfCustoemrProfileQuestinsAndAnswersCount(Integer customerId);
}



