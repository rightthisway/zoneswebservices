package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.RtfCustProfileQuestions;


public interface RtfCustProfileQuestionsDAO  extends RootDAO<Integer, RtfCustProfileQuestions>{
	
	
	public List<RtfCustProfileQuestions> getAllActiveRtfCustProfileQuestions();
	public List<RtfCustProfileQuestions> getAllActiveRtfCustProfileQuestionsByProfileType(String profileType);
}



