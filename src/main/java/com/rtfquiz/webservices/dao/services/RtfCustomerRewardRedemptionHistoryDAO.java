package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.RtfCustomerRewardRedemptionHistory;

public interface RtfCustomerRewardRedemptionHistoryDAO extends RootDAO<Integer, RtfCustomerRewardRedemptionHistory>{

	public RtfCustomerRewardRedemptionHistory getActiveReferralRewardSetting(); 
}
