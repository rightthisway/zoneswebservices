package com.rtfquiz.webservices.dao.services;

import java.util.List;

import com.rtfquiz.webservices.data.RtfPointsConversionSetting;

public interface RtfPointsConversionSettingDAO extends RootDAO<Integer, RtfPointsConversionSetting>{

	public List<RtfPointsConversionSetting> getActiveConversionSetting(); 
}
