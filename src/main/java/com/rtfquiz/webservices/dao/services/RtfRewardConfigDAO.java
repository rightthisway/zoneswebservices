package com.rtfquiz.webservices.dao.services;

import com.rtfquiz.webservices.data.RtfRewardConfig;
import com.zonesws.webservices.enums.SourceType;

public interface RtfRewardConfigDAO extends RootDAO<Integer, RtfRewardConfig>{

	public RtfRewardConfig getActiveReferralRewardSetting();
	public RtfRewardConfig getActiveReferralRewardSettingBySourceType(SourceType sourceType);
}
