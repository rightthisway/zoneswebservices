package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.enums.MiniJackpotType;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.rtfquiz.webservices.enums.WinnerType;

@Entity
@Table(name = "contest_grand_winners")
public class ContestGrandWinner implements Serializable{

	private Integer id;
	private Integer contestId;
	private Integer customerId;
	
	@JsonIgnore
	private Integer eventId;
	@JsonIgnore
	private Integer orderId;
	private Integer rewardTickets;
	@JsonIgnore
	private WinnerStatus status;
	@JsonIgnore
	private Date createdDate;
	@JsonIgnore
	private Date expiryDate;
	@JsonIgnore
	private Boolean isNotified;
	@JsonIgnore
	private Date notifiedTime;
	
	private String expiryDateStr;
	
	private String contestName;
	
	@JsonIgnore
	private Integer questionId;
	
	@JsonIgnore
	private WinnerType winnerType;
	
	private MiniJackpotType jackpotCreditType;
	
	private String notes;
	
	private String userId;
	
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name = "reward_tickets")
	public Integer getRewardTickets() {
		return rewardTickets;
	}
	public void setRewardTickets(Integer rewardTickets) {
		this.rewardTickets = rewardTickets;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	public WinnerStatus getStatus() {
		return status;
	}
	public void setStatus(WinnerStatus status) {
		this.status = status;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "expiry_date")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@Transient
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	@Transient
	public String getExpiryDateStr() {
		if(null != expiryDate ){
			expiryDateStr =  dateTimeFormat1.format(expiryDate);
		}
		return expiryDateStr;
	}
	public void setExpiryDateStr(String expiryDateStr) {
		this.expiryDateStr = expiryDateStr;
	}
	
	@Column(name = "is_notified")
	public Boolean getIsNotified() {
		return isNotified;
	}
	public void setIsNotified(Boolean isNotified) {
		this.isNotified = isNotified;
	}
	
	@Column(name = "notified_date")
	public Date getNotifiedTime() {
		return notifiedTime;
	}
	public void setNotifiedTime(Date notifiedTime) {
		this.notifiedTime = notifiedTime;
	}
	
	@Column(name = "question_id")
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "winner_type")
	public WinnerType getWinnerType() {
		return winnerType;
	}
	public void setWinnerType(WinnerType winnerType) {
		this.winnerType = winnerType;
	}
	
	@Column(name = "notes")
	public String getNotes() {
		if(null == notes) {
			notes ="";
		}
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "mini_jackpot_type")
	public MiniJackpotType getJackpotCreditType() {
		return jackpotCreditType;
	}
	public void setJackpotCreditType(MiniJackpotType jackpotCreditType) {
		this.jackpotCreditType = jackpotCreditType;
	}
	
	@Transient
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
