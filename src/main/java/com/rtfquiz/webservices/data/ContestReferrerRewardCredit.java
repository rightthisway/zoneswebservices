package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contest_referrer_reward_credits")
public class ContestReferrerRewardCredit implements Serializable{

	private Integer id;
	private Integer contestId;
	private Double rewardCreditConv;
	private String status;
	private Date createdDate;
	private Date updatedDate;
	private Boolean isNotified;
	private Date notifiedTime;
	private String referralProgramType;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "is_notified")
	public Boolean getIsNotified() {
		return isNotified;
	}
	public void setIsNotified(Boolean isNotified) {
		this.isNotified = isNotified;
	}
	
	@Column(name = "notified_date")
	public Date getNotifiedTime() {
		return notifiedTime;
	}
	public void setNotifiedTime(Date notifiedTime) {
		this.notifiedTime = notifiedTime;
	}
	
	@Column(name = "reward_credit_conv")
	public Double getRewardCreditConv() {
		return rewardCreditConv;
	}
	public void setRewardCreditConv(Double rewardCreditConv) {
		this.rewardCreditConv = rewardCreditConv;
	}
	
	@Column(name = "updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "referral_program_type")
	public String getReferralProgramType() {
		return referralProgramType;
	}
	public void setReferralProgramType(String referralProgramType) {
		this.referralProgramType = referralProgramType;
	}
	 
	
	
	
}
