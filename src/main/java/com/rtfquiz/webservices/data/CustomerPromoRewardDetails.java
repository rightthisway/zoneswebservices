package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.enums.PromoType;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.zonesws.webservices.utils.TicketUtil;

@Entity
@Table(name = "customer_promo_reward_details")
public class CustomerPromoRewardDetails implements Serializable{

	private Integer id;
	private Integer customerId;
	@JsonIgnore
	private Integer promoRewardId;
	private String promoName;
	private String promoType;
	private String promoCode;
	private Integer promoRefId;
	private String promoRefType;
	private String promoRefName;
	private Double discountPercentage;
	private String promoText;
	
	private String discountPercStr;
	private String prmoExpiryDateStr;
	private String btnText;
	
	@JsonIgnore
	private Date promoExpiryDate;
	@JsonIgnore
	private String status;
	@JsonIgnore
	private Date createdDate;
	@JsonIgnore
	private String createdBy;
	@JsonIgnore
	private Date updatedDate;
	@JsonIgnore
	private String updatedBy;
	@JsonIgnore
	private Date applyDate;
	
	private Integer freeTixCount;
	private Integer earnLifeCount;
	
	@JsonIgnore
	private Integer promoOfferId;
	
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy");
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name = "promo_reward_id")
	public Integer getPromoRewardId() {
		return promoRewardId;
	}
	public void setPromoRewardId(Integer promoRewardId) {
		this.promoRewardId = promoRewardId;
	}
	
	@Column(name = "promo_name")
	public String getPromoName() {
		return promoName;
	}
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}
	
	//@Enumerated(EnumType.STRING)
	@Column(name = "promo_type")
	public String getPromoType() {
		return promoType;
	}
	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}
	
	@Column(name = "promotional_code")
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	@Column(name = "promo_ref_id")
	public Integer getPromoRefId() {
		return promoRefId;
	}
	public void setPromoRefId(Integer promoRefId) {
		this.promoRefId = promoRefId;
	}
	
	@Column(name = "promo_ref_type")
	public String getPromoRefType() {
		return promoRefType;
	}
	public void setPromoRefType(String promoRefType) {
		this.promoRefType = promoRefType;
	}
	
	@Column(name = "promo_ref_name")
	public String getPromoRefName() {
		return promoRefName;
	}
	public void setPromoRefName(String promoRefName) {
		this.promoRefName = promoRefName;
	}
	
	@Column(name = "discount_percentage")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	
	@Transient
	public String getPrmoExpiryDateStr() {
		if(promoExpiryDate != null) {
			this.prmoExpiryDateStr = dateTimeFormat1.format(promoExpiryDate);
		}
		return prmoExpiryDateStr;
	}
	public void setPrmoExpiryDateStr(String prmoExpiryDateStr) {
		this.prmoExpiryDateStr = prmoExpiryDateStr;
	}
	
	@Column(name = "promo_expiry_date")
	public Date getPromoExpiryDate() {
		return promoExpiryDate;
	}
	public void setPromoExpiryDate(Date promoExpiryDate) {
		this.promoExpiryDate = promoExpiryDate;
	}
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name = "apply_date")
	public Date getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	
	@Column(name = "free_tix_count")
	public Integer getFreeTixCount() {
		if(freeTixCount == null) {
			freeTixCount = 0;
		}
		return freeTixCount;
	}
	public void setFreeTixCount(Integer freeTixCount) {
		this.freeTixCount = freeTixCount;
	}
	
	@Column(name = "earn_life_count")
	public Integer getEarnLifeCount() {
		if(earnLifeCount == null) {
			earnLifeCount = 0;
		}
		return earnLifeCount;
	}
	public void setEarnLifeCount(Integer earnLifeCount) {
		this.earnLifeCount = earnLifeCount;
	}
	
	@Column(name = "promo_offer_id")
	public Integer getPromoOfferId() {
		return promoOfferId;
	}
	public void setPromoOfferId(Integer promoOfferId) {
		this.promoOfferId = promoOfferId;
	}
	
	@Transient
	public String getDiscountPercStr() {
		if(discountPercentage != null) {
			discountPercStr = TicketUtil.percentageDF.format(discountPercentage);
		} else {
			discountPercStr="0";
		}
		return discountPercStr;
	}
	public void setDiscountPercStr(String discountPercStr) {
		this.discountPercStr = discountPercStr;
	}
	
	@Transient
	public String getPromoText() {
		if(promoType!= null) {
			if(promoType.equals(PromoType.CONTEST_PROMO.toString()) || 
					promoType.equals(PromoType.REWARD_PROMO.toString())) {
				if(promoName != null) {
					promoText = promoName;					
				} else {
					promoText = "";
				}
			} else if(promoType.equals(PromoType.REWARD_EARNLIFE.toString())) {
					promoText = getEarnLifeCount()+" FREE LIFE";					
			} else if(promoType.equals(PromoType.REWARD_FREE_TIX.toString())) {
				promoText = getFreeTixCount()+" FREE TICKETS";
			}
		} else {
			promoText = "";
		}
		return promoText;
	}
	public void setPromoText(String promoText) {
		this.promoText = promoText;
	}
	
	@Transient
	public String getBtnText() {
		if(promoType!= null) {
			if(promoType.equals(PromoType.CONTEST_PROMO.toString())) {
				btnText = "BUY TICKETS";
			} else if(promoType.equals(PromoType.REWARD_PROMO.toString())) {
				btnText = "BUY TICKETS";
			} else if(promoType.equals(PromoType.REWARD_EARNLIFE.toString())) {
				btnText = "ADD LIFE";
			} else if(promoType.equals(PromoType.REWARD_FREE_TIX.toString())) {
				btnText = "REDEEM";
			}
		} else {
			btnText = "";
		}
		return btnText;
	}
	public void setBtnText(String btnText) {
		this.btnText = btnText;
	}
	
	
	
}
