package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CustomerType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.SignupType;
import com.zonesws.webservices.utils.PasswordUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizConfigSettings")
@Entity
@Table(name="config_settings")
public class QuizConfigSettings  implements Serializable{
	
	private Integer id;
	private String videoSourceUrl;
	private String appSyncUrl;
	private String appSyncToken;
	private String jwPlayerLicenceKeyAndroid;
	private String jwPlayerLicenceKeyIOS;
	private String startingCountdownVideoUrl;
	
	private String partnerId;
	private String sourceId;
	private String entryId;
	private String liveStreamUrl;
	private String uiConfId;
	private String notificationSevenMins;
	private String notificationTwentyMins;
	private Integer rewardEarnLifeThreshold;
	
	@JsonIgnore
	private Date updatedDateTime;
	@JsonIgnore
	private String updatedBy;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="video_source_url")
	public String getVideoSourceUrl() {
		return videoSourceUrl;
	}
	public void setVideoSourceUrl(String videoSourceUrl) {
		this.videoSourceUrl = videoSourceUrl;
	}
	
	@Column(name="app_sync_url")
	public String getAppSyncUrl() {
		return appSyncUrl;
	}
	public void setAppSyncUrl(String appSyncUrl) {
		this.appSyncUrl = appSyncUrl;
	}
	
	@Column(name="app_sync_token")
	public String getAppSyncToken() {
		return appSyncToken;
	}
	public void setAppSyncToken(String appSyncToken) {
		this.appSyncToken = appSyncToken;
	}
	
	@Column(name="jw_player_licence_key_android")
	public String getJwPlayerLicenceKeyAndroid() {
		return jwPlayerLicenceKeyAndroid;
	}
	public void setJwPlayerLicenceKeyAndroid(String jwPlayerLicenceKeyAndroid) {
		this.jwPlayerLicenceKeyAndroid = jwPlayerLicenceKeyAndroid;
	}
	
	@Column(name="jw_player_licence_key_ios")
	public String getJwPlayerLicenceKeyIOS() {
		return jwPlayerLicenceKeyIOS;
	}
	public void setJwPlayerLicenceKeyIOS(String jwPlayerLicenceKeyIOS) {
		this.jwPlayerLicenceKeyIOS = jwPlayerLicenceKeyIOS;
	}
	
	@Column(name="updated_datetime")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(name="starting_countdown_video_url")
	public String getStartingCountdownVideoUrl() {
		return startingCountdownVideoUrl;
	}
	public void setStartingCountdownVideoUrl(String startingCountdownVideoUrl) {
		this.startingCountdownVideoUrl = startingCountdownVideoUrl;
	}
	
	@Column(name="partner_id")
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	
	@Column(name="source_id")
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
	@Column(name="entry_id")
	public String getEntryId() {
		return entryId;
	}
	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}
	
	@Column(name="live_stream_url")
	public String getLiveStreamUrl() {
		return liveStreamUrl;
	}
	public void setLiveStreamUrl(String liveStreamUrl) {
		this.liveStreamUrl = liveStreamUrl;
	}
	
	@Column(name="uiconf_id")
	public String getUiConfId() {
		return uiConfId;
	}
	public void setUiConfId(String uiConfId) {
		this.uiConfId = uiConfId;
	}
	
	@Column(name="notifiction_7")
	public String getNotificationSevenMins() {
		return notificationSevenMins;
	}
	public void setNotificationSevenMins(String notificationSevenMins) {
		this.notificationSevenMins = notificationSevenMins;
	}
	
	@Column(name="notifiction_20")
	public String getNotificationTwentyMins() {
		return notificationTwentyMins;
	}
	public void setNotificationTwentyMins(String notificationTwentyMins) {
		this.notificationTwentyMins = notificationTwentyMins;
	}
	
	@Column(name="lives_threshold")
	public Integer getRewardEarnLifeThreshold() {
		return rewardEarnLifeThreshold;
	}
	public void setRewardEarnLifeThreshold(Integer rewardEarnLifeThreshold) {
		this.rewardEarnLifeThreshold = rewardEarnLifeThreshold;
	}
	
	
	
	
	
}
