package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.enums.ContestJackpotType;
import com.rtfquiz.webservices.enums.ContestProcessStatus;
import com.rtfquiz.webservices.enums.ContestQuestRewardType;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents QuizContest entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizContest")
@Entity
@Table(name="contest")
public class QuizContest  implements Serializable{
	
	static SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");
	static SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");
	
	private Integer id;
	private String contestName;
	
	@JsonIgnore
	private Date contestStartDateTime;
	
	@JsonIgnore
	private String status;
	
	@JsonIgnore
	private Integer participantsCount;
	@JsonIgnore
	private Integer winnersCount;
	@JsonIgnore
	private Integer ticketWinnersCount;
	@JsonIgnore
	private Integer pointWinnersCount;
	
	@JsonIgnore
	private Date createdDateTime;
	@JsonIgnore
	private String createdBy;
	@JsonIgnore
	private Date updatedDateTime;
	@JsonIgnore
	private String updatedBy;
	
	@JsonIgnore
	private String contestType;
	
	private Integer maxFreeTicketWinners;
	
	private Integer freeTicketsPerWinner;
	
	@JsonIgnore
	private Double pointsPerWinner;
	
	private Integer noOfQuestions;
	private Double totalRewards;
	
	@JsonIgnore
	private Double rewardsPerQuestion;
	
	@JsonIgnore
	private ContestProcessStatus processStatus;
	
	@JsonIgnore
	private Boolean isCustomerStatsUpdated;
	
	private String contestStartDate;
	private String contestStartTime;
	
	@JsonIgnore
	private String promoCode;
	@JsonIgnore
	private Integer promoRefId;
	@JsonIgnore
	private String promoRefType;
	@JsonIgnore
	private String promoRefName;
	@JsonIgnore
	private Double discountPercentage;
	@JsonIgnore
	private Date promoExpiryDate;
	@JsonIgnore
	private Double singleTicketPrice;
	@JsonIgnore
	private String zone;
	
	@JsonIgnore
	private String lastAction;
	@JsonIgnore
	private Integer lastQuestionNo;
	
	@JsonIgnore
	private String extendedName;
	
	@JsonIgnore
	private Double referralRewards;
	
	@JsonIgnore
	private Integer participantStar;
	
	@JsonIgnore
	private Integer referralStar;
	
	@JsonIgnore
	private Integer participantLives;
	
	@JsonIgnore
	private Double participantRewards;
	
	@JsonIgnore
	private ContestJackpotType contestJackpotType;
	
	@JsonIgnore
	private Integer giftCardValueId;
	
	@JsonIgnore
	private Integer giftCardPerWinner;
	
	@JsonIgnore
	private ContestQuestRewardType questionRewardType;
	
	@JsonIgnore
	private String contestCategory;
	@JsonIgnore
	private String contestMode;
	
	@JsonIgnore
	private String contestPwd;
	
	@JsonIgnore
	private Integer referralRtfPoints;
	
	@JsonIgnore
	private Integer participantRtfPoints;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_name")
	public String getContestName() {
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	
	@Column(name="contest_start_datetime")
	public Date getContestStartDateTime() {
		return contestStartDateTime;
	}
	public void setContestStartDateTime(Date contestStartDateTime) {
		this.contestStartDateTime = contestStartDateTime;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="participants_count")
	public Integer getParticipantsCount() {
		return participantsCount;
	}
	public void setParticipantsCount(Integer participantsCount) {
		this.participantsCount = participantsCount;
	}
	
	@Column(name="winners_count")
	public Integer getWinnersCount() {
		return winnersCount;
	}
	public void setWinnersCount(Integer winnersCount) {
		this.winnersCount = winnersCount;
	}
	
	@Column(name="ticket_winners_count")
	public Integer getTicketWinnersCount() {
		return ticketWinnersCount;
	}
	public void setTicketWinnersCount(Integer ticketWinnersCount) {
		this.ticketWinnersCount = ticketWinnersCount;
	}
	
	@Column(name="point_winners_count")
	public Integer getPointWinnersCount() {
		return pointWinnersCount;
	}
	public void setPointWinnersCount(Integer pointWinnersCount) {
		this.pointWinnersCount = pointWinnersCount;
	}
	
	@Column(name="created_datetime")
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="updated_datetime")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name="eligible_free_ticket_winners")
	public Integer getMaxFreeTicketWinners() {
		return maxFreeTicketWinners;
	}
	public void setMaxFreeTicketWinners(Integer maxFreeTicketWinners) {
		this.maxFreeTicketWinners = maxFreeTicketWinners;
	}
	
	@Column(name="free_tickets_per_winner")
	public Integer getFreeTicketsPerWinner() {
		return freeTicketsPerWinner;
	}
	public void setFreeTicketsPerWinner(Integer freeTicketsPerWinner) {
		this.freeTicketsPerWinner = freeTicketsPerWinner;
	}
	
	@Column(name="points_per_winner")
	public Double getPointsPerWinner() {
		return pointsPerWinner;
	}
	public void setPointsPerWinner(Double pointsPerWinner) {
		this.pointsPerWinner = pointsPerWinner;
	}
	
	@Column(name="no_of_questions")
	public Integer getNoOfQuestions() {
		return noOfQuestions;
	}
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}
	
	@Column(name="contest_type")
	public String getContestType() {
		return contestType;
	}
	public void setContestType(String contestType) {
		this.contestType = contestType;
	}
	
	@Column(name="total_rewards")
	public Double getTotalRewards() {
		return totalRewards;
	}
	public void setTotalRewards(Double totalRewards) {
		this.totalRewards = totalRewards;
	}
	
	@Column(name="rewards_per_question")
	public Double getRewardsPerQuestion() {
		return rewardsPerQuestion;
	}
	public void setRewardsPerQuestion(Double rewardsPerQuestion) {
		this.rewardsPerQuestion = rewardsPerQuestion;
	}
	
	@Column(name="is_customer_stats_updated")
	public Boolean getIsCustomerStatsUpdated() {
		return isCustomerStatsUpdated;
	}
	public void setIsCustomerStatsUpdated(Boolean isCustomerStatsUpdated) {
		this.isCustomerStatsUpdated = isCustomerStatsUpdated;
	}
	
	@Column(name="promotional_code")
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	@Column(name="promo_ref_id")
	public Integer getPromoRefId() {
		return promoRefId;
	}
	public void setPromoRefId(Integer promoRefId) {
		this.promoRefId = promoRefId;
	}
	@Column(name="promo_ref_type")
	public String getPromoRefType() {
		return promoRefType;
	}
	public void setPromoRefType(String promoRefType) {
		this.promoRefType = promoRefType;
	}
	
	@Column(name="promo_ref_name")
	public String getPromoRefName() {
		return promoRefName;
	}
	public void setPromoRefName(String promoRefName) {
		this.promoRefName = promoRefName;
	}
	@Column(name="discount_percentage")
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	@Column(name="promo_expiry_date")
	public Date getPromoExpiryDate() {
		return promoExpiryDate;
	}
	public void setPromoExpiryDate(Date promoExpiryDate) {
		this.promoExpiryDate = promoExpiryDate;
	}
	
	@Transient
	public ContestProcessStatus getProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(ContestProcessStatus processStatus) {
		this.processStatus = processStatus;
	}
	
	@Transient
	public String getContestStartDate() {
		if(contestStartDate != null) {
			return contestStartDate;
		}
		if(getContestStartDateTime() != null) {
			contestStartDate = dt.format(getContestStartDateTime());
		}
		return contestStartDate;
	}
	public void setContestStartDate(String contestStartDate) {
		this.contestStartDate = contestStartDate;
	}
	
	@Transient
	public String getContestStartTime() {
		if(contestStartTime != null) {
			return contestStartTime;
		}
		if(getContestStartDateTime() != null) {
			
			try {
				Date today  = dt.parse(dt.format(new Date()));
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, 1);
				Date tomorrow = dt.parse(dt.format(new Date(cal.getTimeInMillis())));
				Date contestDate = dt.parse(dt.format(getContestStartDateTime()));
				if(contestDate.compareTo(today) == 0) {
					contestStartTime = "Today "+timeFt.format(getContestStartDateTime());
					return contestStartTime;
				} else if(contestDate.compareTo(tomorrow) == 0) {
					contestStartTime = "Tomorrow "+timeFt.format(getContestStartDateTime());
					return contestStartTime;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			contestStartTime = dt.format(getContestStartDateTime())+" "+timeFt.format(getContestStartDateTime());
		}
		return contestStartTime;
	}
	public void setContestStartTimeStr(String contestStartTime) {
		this.contestStartTime = contestStartTime;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="single_tix_price")
	public Double getSingleTicketPrice() {
		return singleTicketPrice;
	}
	public void setSingleTicketPrice(Double singleTicketPrice) {
		this.singleTicketPrice = singleTicketPrice;
	}
	@Column(name="last_action")
	public String getLastAction() {
		return lastAction;
	}
	public void setLastAction(String lastAction) {
		this.lastAction = lastAction;
	}
	@Column(name="last_question_no")
	public Integer getLastQuestionNo() {
		return lastQuestionNo;
	}
	public void setLastQuestionNo(Integer lastQuestionNo) {
		this.lastQuestionNo = lastQuestionNo;
	}
	
	@Column(name="extended_name")
	public String getExtendedName() {
		return extendedName;
	}
	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}
	
	@Column(name="referral_rewards")
	public Double getReferralRewards() {
		if(null == referralRewards) {
			referralRewards = 0.00;
		}
		return referralRewards;
	}
	public void setReferralRewards(Double referralRewards) {
		this.referralRewards = referralRewards;
	}
	
	@Column(name="participant_star")
	public Integer getParticipantStar() {
		if(null == participantStar) {
			participantStar =0;
		}
		return participantStar;
	}
	public void setParticipantStar(Integer participantStar) {
		this.participantStar = participantStar;
	}
	
	@Column(name="referral_star")
	public Integer getReferralStar() {
		if(null == referralStar) {
			referralStar =0;
		}
		return referralStar;
	}
	public void setReferralStar(Integer referralStar) {
		this.referralStar = referralStar;
	}
	
	@Column(name="participant_lives")
	public Integer getParticipantLives() {
		if(null == participantLives) {
			participantLives =0;
		}
		return participantLives;
	}
	public void setParticipantLives(Integer participantLives) {
		this.participantLives = participantLives;
	}
	
	@Column(name="participant_rewards")
	public Double getParticipantRewards() {
		if(null == participantRewards) {
			participantRewards =0.0;
		}
		return participantRewards;
	}
	public void setParticipantRewards(Double participantRewards) {
		this.participantRewards = participantRewards;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="contest_jackpot_type")
	public ContestJackpotType getContestJackpotType() {
		if(null == contestJackpotType) {
			contestJackpotType = ContestJackpotType.NORMAL;
		}
		return contestJackpotType;
	}
	public void setContestJackpotType(ContestJackpotType contestJackpotType) {
		this.contestJackpotType = contestJackpotType;
	}
	
	@Column(name="gift_card_value_id")
	public Integer getGiftCardValueId() {
		return giftCardValueId;
	}
	public void setGiftCardValueId(Integer giftCardValueId) {
		this.giftCardValueId = giftCardValueId;
	}
	
	@Column(name="gift_card_per_winner")
	public Integer getGiftCardPerWinner() {
		return giftCardPerWinner;
	}
	public void setGiftCardPerWinner(Integer giftCardPerWinner) {
		this.giftCardPerWinner = giftCardPerWinner;
	}
	@Enumerated(EnumType.STRING)
	@Column(name="question_reward_type")
	public ContestQuestRewardType getQuestionRewardType() {
		return questionRewardType;
	}
	public void setQuestionRewardType(ContestQuestRewardType questionRewardType) {
		this.questionRewardType = questionRewardType;
	}
	
	@Column(name="contest_category")
	public String getContestCategory() {
		return contestCategory;
	}
	public void setContestCategory(String contestCategory) {
		this.contestCategory = contestCategory;
	}
	@Column(name="contest_mode")
	public String getContestMode() {
		return contestMode;
	}
	public void setContestMode(String contestMode) {
		this.contestMode = contestMode;
	}
	@Column(name="contest_password")
	public String getContestPwd() {
		return contestPwd;
	}
	public void setContestPwd(String contestPwd) {
		this.contestPwd = contestPwd;
	}
	
	@Column(name="referral_rtf_points")
	public Integer getReferralRtfPoints() {
		if(referralRtfPoints == null) {
			referralRtfPoints = 0;
		}
		return referralRtfPoints;
	}
	public void setReferralRtfPoints(Integer referralRtfPoints) {
		this.referralRtfPoints = referralRtfPoints;
	}
	
	@Column(name="participants_rtf_points")
	public Integer getParticipantRtfPoints() {
		if(participantRtfPoints == null) {
			participantRtfPoints = 0;
		}
		return participantRtfPoints;
	}
	public void setParticipantRtfPoints(Integer participantRtfPoints) {
		this.participantRtfPoints = participantRtfPoints;
	}
	
	
}
