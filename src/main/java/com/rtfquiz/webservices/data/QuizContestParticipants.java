package com.rtfquiz.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("QuizContestParticipants")
@Entity
@Table(name="contest_participants")
public class QuizContestParticipants implements Serializable{

	
	private Integer id;
	private Integer contestId;
	private Integer customerId;
	private String platform;
	private String ipAddress;
	private Date joinDateTime;
	private Date exitDateTime;
	private String status;
	
	private String joinDateTimeStr;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Column(name="platform")
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Column(name="join_datetime")
	public Date getJoinDateTime() {
		return joinDateTime;
	}

	public void setJoinDateTime(Date joinDateTime) {
		this.joinDateTime = joinDateTime;
	}

	@Column(name="exit_datetime")
	public Date getExitDateTime() {
		return exitDateTime;
	}

	public void setExitDateTime(Date exitDateTime) {
		this.exitDateTime = exitDateTime;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
