package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.enums.MiniJackpotType;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizContestQuestions")
@Entity
@Table(name="contest_questions")
public class QuizContestQuestions  implements Serializable{
	
	private Integer id;
	private Integer contestId;
	private Integer questionSNo;
	private String question;
	private String optionA;
	private String optionB;
	private String optionC;
	//private String optionD;
	private Double questionRewards;
	
	@JsonIgnore
	private Date createdDateTime;
	@JsonIgnore
	private String createdBy;
	@JsonIgnore
	private Date updatedDateTime;
	@JsonIgnore
	private String updatedBy;

	@JsonIgnore
	private String answer;
	
	private Boolean isAnswerCountComputed = false;
	
	@JsonIgnore
	private MiniJackpotType jackpotCreditType;
	
	@JsonIgnore
	private Integer jNoOfWinners;
	
	@JsonIgnore
	private Double jQtyPerWinner;
	
	@JsonIgnore
	private Integer jGiftCardId;
	 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="question_sl_no")
	public Integer getQuestionSNo() {
		return questionSNo;
	}
	public void setQuestionSNo(Integer questionSNo) {
		this.questionSNo = questionSNo;
	}
	@Column(name="question")
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	@Column(name="option_a")
	public String getOptionA() {
		return optionA;
	}
	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}
	@Column(name="option_b")
	public String getOptionB() {
		return optionB;
	}
	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}
	
	@Column(name="option_c")
	public String getOptionC() {
		return optionC;
	}
	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}
	
	/*@Column(name="option_d")
	public String getOptionD() {
		return optionD;
	}
	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}*/
	
	@Column(name="created_datetime")
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="updated_datetime")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="answer")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	@Column(name="question_reward")
	public Double getQuestionRewards() {
		return questionRewards;
	}
	public void setQuestionRewards(Double questionRewards) {
		this.questionRewards = questionRewards;
	}
	
	@Transient
	public Boolean getIsAnswerCountComputed() {
		if(isAnswerCountComputed == null) {
			isAnswerCountComputed = false;
		}
		return isAnswerCountComputed;
	}
	public void setIsAnswerCountComputed(Boolean isAnswerCountComputed) {
		this.isAnswerCountComputed = isAnswerCountComputed;
	}
	 
	@Column(name="mini_jackpot_type")
	@Enumerated(EnumType.ORDINAL)
	public MiniJackpotType getJackpotCreditType() {
		return jackpotCreditType;
	}
	public void setJackpotCreditType(MiniJackpotType jackpotCreditType) {
		this.jackpotCreditType = jackpotCreditType;
	}
	
	@Column(name="no_of_winner")
	public Integer getjNoOfWinners() {
		return jNoOfWinners;
	}
	public void setjNoOfWinners(Integer jNoOfWinners) {
		this.jNoOfWinners = jNoOfWinners;
	}
	
	@Column(name="qty_per_winner")
	public Double getjQtyPerWinner() {
		return jQtyPerWinner;
	}
	public void setjQtyPerWinner(Double jQtyPerWinner) {
		this.jQtyPerWinner = jQtyPerWinner;
	}
	
	@Column(name="jackpot_gift_card_id")
	public Integer getjGiftCardId() {
		return jGiftCardId;
	}
	public void setjGiftCardId(Integer jGiftCardId) {
		this.jGiftCardId = jGiftCardId;
	}
	
	
	
}
