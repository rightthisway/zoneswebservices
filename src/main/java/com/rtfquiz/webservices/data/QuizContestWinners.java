package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.enums.RewardType;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizContestWinners")
@Entity
@Table(name="contest_winners")
public class QuizContestWinners  implements Serializable{
	
	private Integer id;
	private Integer contestId;
	private Integer customerId;
	private Integer rewardTickets;
	private Double rewardPoints;
	private Integer rewardRank;
	
	private String customerName;
	private String userId;
	private String customerLastName;
	private String profilePicWV;
	private String rewardPointsStr;
	@JsonIgnore
	private RewardType rewardType;
	@JsonIgnore
	private Date createdDateTime;

	@JsonIgnore
	private String custImagePath;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="reward_type")
	@Enumerated(EnumType.ORDINAL)
	public RewardType getRewardType() {
		return rewardType;
	}
	public void setRewardType(RewardType rewardType) {
		this.rewardType = rewardType;
	}
	
	@Column(name="reward_tickets")
	public Integer getRewardTickets() {
		return rewardTickets;
	}
	public void setRewardTickets(Integer rewardTickets) {
		this.rewardTickets = rewardTickets;
	}
	
	
	/*public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}*/
	@Column(name="reward_points")
	public Double getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(Double rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	@Column(name="reward_rank")
	public Integer getRewardRank() {
		return rewardRank;
	}
	
	public void setRewardRank(Integer rewardRank) {
		this.rewardRank = rewardRank;
	}
	
	@Column(name="created_datetime")
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Transient
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Transient
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Transient
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	
	@Transient
	public String getProfilePicWV() {
		if(profilePicWV == null) {
			if(custImagePath != null) {
				profilePicWV = URLUtil.profilePicWebURByImageName(custImagePath);
			} 
		}
		return profilePicWV;
	}
	public void setProfilePicWV(String profilePicWV) {
		this.profilePicWV = profilePicWV;
	}
	@Transient
	public String getCustImagePath() {
		return custImagePath;
	}
	public void setCustImagePath(String custImagePath) {
		this.custImagePath = custImagePath;
	}
	
	@Transient
	public String getRewardPointsStr() {
		if(rewardPoints != null) {
			try {
				rewardPointsStr = TicketUtil.getRoundedValueString(rewardPoints);
			} catch (Exception e) {
				rewardPointsStr="0.00";
				e.printStackTrace();
			}
		} else {
			rewardPointsStr="0.00";
		}
		return rewardPointsStr;
	}
	public void setRewardPointsStr(String rewardPointsStr) {
		this.rewardPointsStr = rewardPointsStr;
	}
	

}
