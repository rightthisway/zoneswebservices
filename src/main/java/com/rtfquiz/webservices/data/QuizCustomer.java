package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CustomerType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.SignupType;
import com.zonesws.webservices.utils.PasswordUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizCustomer")
@Entity
@Table(name="customer")
public class QuizCustomer  implements Serializable{
	
	private Integer id;
	private String customerName;
	private String email;
	private String phone;
	private String extension;
	private String shareCode;
	private Integer customerLives;
	private Integer noOfTicketsWon;
	private CustomerType customerType;
	private String custImagePath;
	private String lastName;
	private Date lastUpdated;
	
	private String userId;
	//private String referralCode;
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return email
	 */
	@Column(name="email")
	public String getEmail() {
		if(null == email || email.isEmpty()){
			email="";
		}
		return email;
	}
	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * 
	 * @return phone
	 */
	@Column(name="phone")
	public String getPhone() {
		if(null == phone ){
			phone = "";
		}
		return phone;
	}
	/**
	 * 
	 * @param phone , Phone no to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name="customer_name")
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	//@Column(name="ext")
	@Transient
	public String getExtension() {
		if(null == extension ){
			extension = "";
		}
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	//@Enumerated(EnumType.STRING)
	//@Column(name="customer_type")
	@Transient
	public CustomerType getCustomerType() {
		return customerType;
	}
	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}
	
	@Column(name = "cust_profile_image_path")
	public String getCustImagePath() {
		return custImagePath;
	}

	public void setCustImagePath(String custImagePath) {
		this.custImagePath = custImagePath;
	}
	
	//@Column(name="last_name")
	@Transient
	public String getLastName() {
		if(null == lastName){
			lastName="";
		}
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name = "share_code")
	public String getShareCode() {
		return shareCode;
	}

	public void setShareCode(String shareCode) {
		this.shareCode = shareCode;
	}

	@Column(name = "ticket_won")
	public Integer getNoOfTicketsWon() {
		return noOfTicketsWon;
	}

	public void setNoOfTicketsWon(Integer noOfTicketsWon) {
		this.noOfTicketsWon = noOfTicketsWon;
	}
	@Column(name = "customer_lives")
	public Integer getCustomerLives() {
		return customerLives;
	}

	public void setCustomerLives(Integer customerLives) {
		this.customerLives = customerLives;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
}
