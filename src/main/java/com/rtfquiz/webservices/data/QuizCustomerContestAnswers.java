package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CustomerType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.SignupType;
import com.zonesws.webservices.utils.PasswordUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizCustomerAnswers")
@Entity
@Table(name="customer_contest_answers")
public class QuizCustomerContestAnswers  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer contestId;
	private Integer questionId;
	private Integer questionSNo;
	private String answer;
	private Boolean isCorrectAnswer;
	private Boolean isLifeLineUsed;
	private Double answerRewards;
	
	private Date fbCallbackTime;
	private Date answerTime;
	private Integer retryCount;
	private Boolean isAutoCreated;
	private Integer rtfPoints;
	
	@JsonIgnore
	private Double cumulativeRewards;
	
	@JsonIgnore
	private Integer cumulativeLifeLineUsed;
	
	@JsonIgnore
	private Date createdDateTime;
	@JsonIgnore
	private Date updatedDateTime;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="question_sl_no")
	public Integer getQuestionSNo() {
		return questionSNo;
	}
	public void setQuestionSNo(Integer questionSNo) {
		this.questionSNo = questionSNo;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="question_id")
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	
	@Column(name="answer")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	@Column(name="is_correct_answer")
	public Boolean getIsCorrectAnswer() {
		return isCorrectAnswer;
	}
	public void setIsCorrectAnswer(Boolean isCorrectAnswer) {
		this.isCorrectAnswer = isCorrectAnswer;
	}
	
	@Column(name="created_datetime")
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Column(name="is_lifeline_used")
	public Boolean getIsLifeLineUsed() {
		return isLifeLineUsed;
	}
	public void setIsLifeLineUsed(Boolean isLifeLineUsed) {
		this.isLifeLineUsed = isLifeLineUsed;
	}
	
	@Column(name="updated_datetime")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Column(name="answer_rewards")
	public Double getAnswerRewards() {
		if(answerRewards == null) {
			answerRewards = 0.0;
		}
		return answerRewards;
	}
	public void setAnswerRewards(Double answerRewards) {
		this.answerRewards = answerRewards;
	}
	
	@Column(name="cumulative_rewards")
	public Double getCumulativeRewards() {
		return cumulativeRewards;
	}
	public void setCumulativeRewards(Double cumulativeRewards) {
		this.cumulativeRewards = cumulativeRewards;
	}
	
	@Column(name="cumulative_lifeline_used")
	public Integer getCumulativeLifeLineUsed() {
		return cumulativeLifeLineUsed;
	}
	public void setCumulativeLifeLineUsed(Integer cumulativeLifeLineUsed) {
		this.cumulativeLifeLineUsed = cumulativeLifeLineUsed;
	}
	
	@Column(name="fb_callback_time")
	public Date getFbCallbackTime() {
		return fbCallbackTime;
	}
	public void setFbCallbackTime(Date fbCallbackTime) {
		this.fbCallbackTime = fbCallbackTime;
	}
	
	@Column(name="ans_time")
	public Date getAnswerTime() {
		return answerTime;
	}
	public void setAnswerTime(Date answerTime) {
		this.answerTime = answerTime;
	}
	
	@Column(name="rt_count")
	public Integer getRetryCount() {
		if(retryCount == null) {
			retryCount = 0;
		}
		return retryCount;
	}
	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}
	
	@Column(name="auto_created")
	public Boolean getIsAutoCreated() {
		if(isAutoCreated==null) {
			isAutoCreated = false;
		}
		return isAutoCreated;
	}
	public void setIsAutoCreated(Boolean isAutoCreated) {
		this.isAutoCreated = isAutoCreated;
	}
	
	@Column(name="rtf_points")
	public Integer getRtfPoints() {
		if(rtfPoints == null) {
			rtfPoints=0;
		}
		return rtfPoints;
	}
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}	
	
}
