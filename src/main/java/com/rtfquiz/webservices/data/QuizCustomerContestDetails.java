package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CustomerType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.SignupType;
import com.zonesws.webservices.utils.PasswordUtil;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizCustomerContestDetails")
@Entity
@Table(name="customer_contest_details")
public class QuizCustomerContestDetails  implements Serializable{
	
	private Integer id;
	private Integer cuId;
	private Integer coId;
	private Integer lqNo;
	private Boolean isLqCrt;
	private Boolean isLqLife;
	private Double cuRwds;
	private Integer cuLife;
	private Date createdDate;
	private Integer rtfPoints;
	private Double sflRwds;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="customer_id")
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	
	@Column(name="contest_id")
	public Integer getCoId() {
		return coId;
	}
	public void setCoId(Integer coId) {
		this.coId = coId;
	}
	@Column(name="last_quest_no")
	public Integer getLqNo() {
		if(lqNo == null) {
			lqNo = 0;
		}
		return lqNo;
	}
	public void setLqNo(Integer lqNo) {
		this.lqNo = lqNo;
	}
	@Column(name="is_last_quest_crt")
	public Boolean getIsLqCrt() {
		return isLqCrt;
	}
	public void setIsLqCrt(Boolean isLqCrt) {
		this.isLqCrt = isLqCrt;
	}
	@Column(name="is_last_quest_life")
	public Boolean getIsLqLife() {
		return isLqLife;
	}
	public void setIsLqLife(Boolean isLqLife) {
		this.isLqLife = isLqLife;
	}
	
	@Column(name="cumulative_rwds")
	public Double getCuRwds() {
		if(cuRwds == null ) {
			cuRwds = 0.0;
		}
		return cuRwds;
	}
	public void setCuRwds(Double cuRwds) {
		this.cuRwds = cuRwds;
	}
	
	@Column(name="cumulative_life")
	public Integer getCuLife() {
		if(cuLife == null) {
			cuLife =0;
		}
		return cuLife;
	}
	public void setCuLife(Integer cuLife) {
		this.cuLife = cuLife;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="rtf_points")
	public Integer getRtfPoints() {
		if(rtfPoints == null) {
			rtfPoints = 0;
		}
		return rtfPoints;
	}
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}
	
	@Column(name="sfl_rwds")
	public Double getSflRwds() {
		if(sflRwds == null) {
			sflRwds=0.0;
		}
		return sflRwds;
	}
	public void setSflRwds(Double sflRwds) {
		this.sflRwds = sflRwds;
	}

}
