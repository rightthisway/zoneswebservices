package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.rtfquiz.webservices.enums.FriendStatus;
import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("QuizCustomerFriend")
@Entity
@Table(name="customer_friends")
public class QuizCustomerFriend implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer customerFriendId;
	private Date createdDate;
	private Date updatedDate;
	private FriendStatus status;
	private Boolean isSender;
	private Integer unfriendBy;
	
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="friend_customer_id")
	public Integer getCustomerFriendId() {
		return customerFriendId;
	}
	public void setCustomerFriendId(Integer customerFriendId) {
		this.customerFriendId = customerFriendId;
	}
	
	@Column(name="create_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Enumerated(value=EnumType.STRING)
	@Column(name="status")
	public FriendStatus getStatus() {
		return status;
	}
	public void setStatus(FriendStatus status) {
		this.status = status;
	}
	
	@Column(name="is_sender")
	public Boolean getIsSender() {
		return isSender;
	}
	public void setIsSender(Boolean isSender) {
		this.isSender = isSender;
	}
	
	@Column(name="unfriend_by")
	public Integer getUnfriendBy() {
		return unfriendBy;
	}
	public void setUnfriendBy(Integer unfriendBy) {
		this.unfriendBy = unfriendBy;
	}
	
	
	
}
