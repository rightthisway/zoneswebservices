package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizCustomerReferral")
@Entity
@Table(name="customer_referral_tracking")
public class QuizCustomerReferralTracking  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private String referralCode;
	private Integer referralCustomerId;
	private Integer rtfPromotionalId;
	@JsonIgnore
	private Date createdDateTime;
	@JsonIgnore
	private Boolean livesToTireOneCustomer;
	
	@JsonIgnore
	private Boolean livesToPrimaryCustomer;
	
	@JsonIgnore
	private Boolean isAffiliateReferral;
	
	@JsonIgnore
	private String affiliateReferralStatus;
	
	@JsonIgnore
	private Date updatedDateTime;
	
	@JsonIgnore
	private Date contestDate;
	
	@JsonIgnore
	private Boolean yearEndRewardApplied;
	
	@JsonIgnore
	private Boolean isPurchased;
	@JsonIgnore
	private Boolean isGamePlayed;
	 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="referral_code")
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	
	@Column(name="referral_customer_id")
	public Integer getReferralCustomerId() {
		return referralCustomerId;
	}
	public void setReferralCustomerId(Integer referralCustomerId) {
		this.referralCustomerId = referralCustomerId;
	}
	
	@Column(name="created_datetime")
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Column(name="rtf_promotional_id")
	public Integer getRtfPromotionalId() {
		return rtfPromotionalId;
	}
	public void setRtfPromotionalId(Integer rtfPromotionalId) {
		this.rtfPromotionalId = rtfPromotionalId;
	}
	
	@Column(name = "lives_to_tireone_customer")
	public Boolean getLivesToTireOneCustomer() {
		if(null == livesToTireOneCustomer) {
			livesToTireOneCustomer = false;
		}
		return livesToTireOneCustomer;
	}
	public void setLivesToTireOneCustomer(Boolean livesToTireOneCustomer) {
		this.livesToTireOneCustomer = livesToTireOneCustomer;
	}
	
	@Column(name = "lives_to_primary_customer")
	public Boolean getLivesToPrimaryCustomer() {
		return livesToPrimaryCustomer;
	}
	public void setLivesToPrimaryCustomer(Boolean livesToPrimaryCustomer) {
		this.livesToPrimaryCustomer = livesToPrimaryCustomer;
	}
	
	@Column(name = "is_affiliate_referral")
	public Boolean getIsAffiliateReferral() {
		return isAffiliateReferral;
	}
	public void setIsAffiliateReferral(Boolean isAffiliateReferral) {
		this.isAffiliateReferral = isAffiliateReferral;
	}
	
	@Column(name = "affiliate_referral_status")
	public String getAffiliateReferralStatus() {
		return affiliateReferralStatus;
	}
	public void setAffiliateReferralStatus(String affiliateReferralStatus) {
		this.affiliateReferralStatus = affiliateReferralStatus;
	}
	
	@Column(name = "updated_date")
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}
	
	@Column(name = "contest_date")
	public Date getContestDate() {
		return contestDate;
	}
	public void setContestDate(Date contestDate) {
		this.contestDate = contestDate;
	}
	
	@Column(name = "year_end_referral_reward_applied")
	public Boolean getYearEndRewardApplied() {
		return yearEndRewardApplied;
	}
	public void setYearEndRewardApplied(Boolean yearEndRewardApplied) {
		this.yearEndRewardApplied = yearEndRewardApplied;
	}
	
	@Column(name = "is_purchased")
	public Boolean getIsPurchased() {
		return isPurchased;
	}
	public void setIsPurchased(Boolean isPurchased) {
		this.isPurchased = isPurchased;
	}
	
	@Column(name = "is_played")
	public Boolean getIsGamePlayed() {
		return isGamePlayed;
	}
	public void setIsGamePlayed(Boolean isGamePlayed) {
		this.isGamePlayed = isGamePlayed;
	}
	
	
	
	
}
