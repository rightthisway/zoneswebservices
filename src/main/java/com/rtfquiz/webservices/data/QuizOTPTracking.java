package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Tamil
 *
 */
@XStreamAlias("QuizOTPTracking")
@Entity
@Table(name="cust_phone_otp_verification")
public class QuizOTPTracking  implements Serializable{
	
	private Integer id;
	private String ctyPhCode;
	private String phone;
	private String otp;
	private String status;
	private Date createdDate;
	private Date verifiedDate;
	private String messageId;
	
	private String responseOtp;
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="phone")
	public String getPhone() {
		return phone;
	}

	

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name="otp")
	public String getOtp() {
		if(otp == null) {
			otp = "";
		}
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	@Column(name="created_datetime")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name="verified_datetime")
	public Date getVerifiedDate() {
		return verifiedDate;
	}

	public void setVerifiedDate(Date verifiedDate) {
		this.verifiedDate = verifiedDate;
	}

	@Column(name="message_id")
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@Transient
	public String getResponseOtp() {
		if(responseOtp == null) {
			responseOtp = "";
		}
		return responseOtp;
	}

	public void setResponseOtp(String responseOtp) {
		this.responseOtp = responseOtp;
	}

	@Column(name="country_phone_code")
	public String getCtyPhCode() {
		return ctyPhCode;
	}

	public void setCtyPhCode(String ctyPhCode) {
		this.ctyPhCode = ctyPhCode;
	}
	
}
