package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * represents QuizSuperFanCustomerLevel entity
 * @author Ulaganathan
 *
 */
 
@Entity
@Table(name="quiz_super_fan_contest_cust_levels")
public class QuizSuperFanCustomerLevel  implements Serializable{
	
	@JsonIgnore
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	 
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="contest_id")
	private Integer contestId;
	
	@Column(name="level_id")
	private Integer levelId;
	
	@Column(name="sf_star")
	private Integer sfStar;
	
	@Column(name="question_sl_no")
	private Integer questionSlNo;
	
	@JsonIgnore
	@Column(name="created_date")
	private Date createdDate = new Date();
	
	@JsonIgnore
	@Column(name="updated_date")
	private Date updatedDate;
	
	@JsonIgnore
	@Column(name="status")
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public Integer getLevelId() {
		return levelId;
	}

	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}

	public Integer getSfStar() {
		return sfStar;
	}

	public void setSfStar(Integer sfStar) {
		this.sfStar = sfStar;
	}

	public Integer getQuestionSlNo() {
		return questionSlNo;
	}

	public void setQuestionSlNo(Integer questionSlNo) {
		this.questionSlNo = questionSlNo;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	 

	 
}
