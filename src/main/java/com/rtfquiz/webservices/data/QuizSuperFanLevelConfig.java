package com.rtfquiz.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * represents QuizSuperFanLevelConfig entity
 * @author Ulaganathan
 *
 */
 
@Entity
@Table(name="quiz_super_fan_level_config")
public class QuizSuperFanLevelConfig  implements Serializable{
	
	@JsonIgnore
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	 
	@Column(name="sf_star_from")
	private Integer starFrom;
	
	@Column(name="sf_star_to")
	private Integer starTo;
	
	@Column(name="level_no")
	private Integer levelNo;
	
	@Column(name="level_title")
	private String levelTitle;
	
	@JsonIgnore
	@Column(name="status")
	private String status;
	
	@Transient
	private Boolean showUserLevel;
	
	@Transient
	private Boolean releaseLock;
	
	@Transient
	private Integer custSfStars;
	
	@Transient
	private String levelRange;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStarFrom() {
		return starFrom;
	}

	public void setStarFrom(Integer starFrom) {
		this.starFrom = starFrom;
	}

	public Integer getStarTo() {
		return starTo;
	}

	public void setStarTo(Integer starTo) {
		this.starTo = starTo;
	}

	public Integer getLevelNo() {
		return levelNo;
	}

	public void setLevelNo(Integer levelNo) {
		this.levelNo = levelNo;
	}

	public String getLevelTitle() {
		return levelTitle;
	}

	public void setLevelTitle(String levelTitle) {
		this.levelTitle = levelTitle;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCustSfStars() {
		if(null == custSfStars) {
			custSfStars =0;
		}
		return custSfStars;
	}

	public void setCustSfStars(Integer custSfStars) {
		this.custSfStars = custSfStars;
	}

	public String getLevelRange() {
		levelRange = starFrom+" - "+starTo;
		return levelRange;
	}

	public void setLevelRange(String levelRange) {
		this.levelRange = levelRange;
	}

	public Boolean getShowUserLevel() {
		if(null == showUserLevel) {
			showUserLevel = false;
		}
		return showUserLevel;
	}

	public void setShowUserLevel(Boolean showUserLevel) {
		this.showUserLevel = showUserLevel;
	}

	public Boolean getReleaseLock() {
		if(null == releaseLock) {
			releaseLock = false;
		}
		return releaseLock;
	}

	public void setReleaseLock(Boolean releaseLock) {
		this.releaseLock = releaseLock;
	}
	 
}
