package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * represents QuizSuperFanStat entity
 * @author Tamil
 *
 */
 
@Entity
@Table(name="quiz_super_fan_stat")
public class QuizSuperFanStat  implements Serializable{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	 
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="game_played")
	private Integer noOfGamePlayed;
	
	@Column(name="total_no_of_chances")
	private Integer totalNoOfChances;
	
	@Column(name="referral_chances")
	private Integer referralChances;
	
	@Column(name="participant_chances")
	private Integer participationChances;
	
	@Column(name="other_chances")
	private Integer otherChances;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="updated_date")
	private Date updatedDate;
	
	@Column(name="status")
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getNoOfGamePlayed() {
		if(null == noOfGamePlayed) {
			noOfGamePlayed = 0;
		}
		return noOfGamePlayed;
	}

	public void setNoOfGamePlayed(Integer noOfGamePlayed) {
		this.noOfGamePlayed = noOfGamePlayed;
	}

	public Integer getTotalNoOfChances() {
		if(null == totalNoOfChances) {
			totalNoOfChances = 0;
		}
		return totalNoOfChances;
	}

	public void setTotalNoOfChances(Integer totalNoOfChances) {
		this.totalNoOfChances = totalNoOfChances;
	}

	public Integer getReferralChances() {
		if(null == referralChances) {
			referralChances = 0;
		}
		return referralChances;
	}

	public void setReferralChances(Integer referralChances) {
		this.referralChances = referralChances;
	}

	public Integer getParticipationChances() {
		if(null == participationChances) {
			participationChances = 0;
		}
		return participationChances;
	}

	public void setParticipationChances(Integer participationChances) {
		this.participationChances = participationChances;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getOtherChances() {
		return otherChances;
	}

	public void setOtherChances(Integer otherChances) {
		this.otherChances = otherChances;
	}
	
}
