package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * represents QuizSuperFanTracking entity
 * @author Tamil
 *
 */
 
@Entity
@Table(name="quiz_super_fan_tracking")
public class QuizSuperFanTracking  implements Serializable{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	 
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="contest_id")
	private Integer contestId;
	
	@Column(name="old_sf_stars")
	private Integer oldSuperFanStars; 
	
	@Column(name="new_sf_stars")
	private Integer newSuperFanStars;
	
	@Column(name="this_contest_participant_stars")
	private Integer thisContestParticipantStars;
	
	@Column(name="this_contest_referral_stars")
	private Integer thisContestReferralStars;
	
	@Column(name="old_participant_stars")
	private Integer oldParticipantStars;
	
	@Column(name="old_referral_stars")
	private Integer oldReferralStars;
	
	@Column(name="notes")
	private String notes;
	
	@Column(name="created_date")
	private Date createdDate;

	public final Integer getId() {
		return id;
	}

	public final void setId(Integer id) {
		this.id = id;
	}

	public final Integer getCustomerId() {
		return customerId;
	}

	public final void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public final Integer getContestId() {
		return contestId;
	}

	public final void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public final Integer getOldSuperFanStars() {
		return oldSuperFanStars;
	}

	public final void setOldSuperFanStars(Integer oldSuperFanStars) {
		this.oldSuperFanStars = oldSuperFanStars;
	}

	public final Integer getNewSuperFanStars() {
		return newSuperFanStars;
	}

	public final void setNewSuperFanStars(Integer newSuperFanStars) {
		this.newSuperFanStars = newSuperFanStars;
	}

	public final Integer getThisContestParticipantStars() {
		return thisContestParticipantStars;
	}

	public final void setThisContestParticipantStars(Integer thisContestParticipantStars) {
		this.thisContestParticipantStars = thisContestParticipantStars;
	}

	public final Integer getThisContestReferralStars() {
		return thisContestReferralStars;
	}

	public final void setThisContestReferralStars(Integer thisContestReferralStars) {
		this.thisContestReferralStars = thisContestReferralStars;
	}

	public final Integer getOldParticipantStars() {
		return oldParticipantStars;
	}

	public final void setOldParticipantStars(Integer oldParticipantStars) {
		this.oldParticipantStars = oldParticipantStars;
	}

	public final Integer getOldReferralStars() {
		return oldReferralStars;
	}

	public final void setOldReferralStars(Integer oldReferralStars) {
		this.oldReferralStars = oldReferralStars;
	}

	public final String getNotes() {
		return notes;
	}

	public final void setNotes(String notes) {
		this.notes = notes;
	}

	public final Date getCreatedDate() {
		return createdDate;
	}

	public final void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	} 
	
}
