package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "rtf_cust_profile_answers")
public class RtfCustProfileAnswers implements Serializable{

	@JsonIgnore
	private Integer id;
	
	private Integer qId;
	
	@JsonIgnore
	private Integer cuId;
	private String answer;

	@JsonIgnore
	private Integer points;
	@JsonIgnore
	private String status;
	@JsonIgnore
	private Date created;
	@JsonIgnore
	private Date updated;
	
	private String question;
	private Integer qSlNo;
	private String qType;
	@JsonIgnore
	private String pType;
	private Boolean edit = true;
	private Integer qPoints;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "question_id")
	public Integer getqId() {
		return qId;
	}
	public void setqId(Integer qId) {
		this.qId = qId;
	}
	
	@Column(name = "customer_id")
	public Integer getCuId() {
		return cuId;
	}
	public void setCuId(Integer cuId) {
		this.cuId = cuId;
	}
	
	@Column(name = "answer")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	@Column(name = "points")
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "created_date")
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	@Column(name = "updated_date")
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	@Transient
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	@Transient
	public Integer getqSlNo() {
		return qSlNo;
	}
	public void setqSlNo(Integer qSlNo) {
		this.qSlNo = qSlNo;
	}
	
	@Transient
	public String getqType() {
		return qType;
	}
	public void setqType(String qType) {
		this.qType = qType;
	}
	
	@Transient
	public String getpType() {
		return pType;
	}
	public void setpType(String pType) {
		this.pType = pType;
	}
	
	@Transient
	public Boolean getEdit() {
		if(answer != null && !answer.isEmpty()) {
			edit = false;
		} else {
			edit = true;
		}
		return edit;
	}
	public void setEdit(Boolean edit) {
		this.edit = edit;
	}
	
	@Transient
	public Integer getqPoints() {
		if(qPoints == null) {
			qPoints=0;
		}
		return qPoints;
	}
	public void setqPoints(Integer qPoints) {
		this.qPoints = qPoints;
	}
	
}
