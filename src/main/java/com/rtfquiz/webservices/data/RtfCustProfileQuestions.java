package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rtf_cust_profile_questions")
public class RtfCustProfileQuestions implements Serializable{

	private Integer id;
	private Integer qSlNo;
	private String question;
	private String qType;
	private Integer points;
	private String status;
	private String pType;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "qsl_no")
	public Integer getqSlNo() {
		return qSlNo;
	}
	public void setqSlNo(Integer qSlNo) {
		this.qSlNo = qSlNo;
	}
	
	@Column(name = "question")
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	@Column(name = "question_type")
	public String getqType() {
		return qType;
	}
	public void setqType(String qType) {
		this.qType = qType;
	}
	
	@Column(name = "points")
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "profile_type")
	public String getpType() {
		return pType;
	}
	public void setpType(String pType) {
		this.pType = pType;
	}


}
