package com.rtfquiz.webservices.data;

import java.util.Date;

 
public class RtfCustomerRewardHistory{
	
	private Integer id;
	private Integer customerId;
	private String transactionType;
	private String sourceType;
	private Integer sourceRefId;
	private Integer lives;
	private Integer magicWands;
	private Integer superFanStars;
	private Integer rtfPoints;
	private Double rewardDollars;
	private String description;
	private Date createdDate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public Integer getSourceRefId() {
		return sourceRefId;
	}
	public void setSourceRefId(Integer sourceRefId) {
		this.sourceRefId = sourceRefId;
	}
	public Integer getLives() {
		return lives;
	}
	public void setLives(Integer lives) {
		this.lives = lives;
	}
	public Integer getMagicWands() {
		return magicWands;
	}
	public void setMagicWands(Integer magicWands) {
		this.magicWands = magicWands;
	}
	public Integer getSuperFanStars() {
		return superFanStars;
	}
	public void setSuperFanStars(Integer superFanStars) {
		this.superFanStars = superFanStars;
	}
	public Integer getRtfPoints() {
		return rtfPoints;
	}
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}
	public Double getRewardDollars() {
		return rewardDollars;
	}
	public void setRewardDollars(Double rewardDollars) {
		this.rewardDollars = rewardDollars;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	} 
}
