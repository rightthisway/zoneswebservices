package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.PointRedemptionType;


@XStreamAlias("RtfCustomerPointsConversionHistory")
@Entity
@Table(name="rtf_customer_reward_redemption_history")
public class RtfCustomerRewardRedemptionHistory implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "customer_id")
	private Integer customerId;
	
	@Column(name = "rtf_points_debited")
	private Integer rtfPointsDebited;

	@Column(name = "no_of_lives")
	private Integer noOfLives;
	
	@Column(name = "no_of_magic_wand")
	private Integer noOfMagicWands;
	
	@Column(name = "reward_dollars")
	private Double rewardDollar;
	
	@Enumerated(EnumType.STRING)
	@Column(name="redemption_type")
	private PointRedemptionType pointRedemptionType;
	
	@Column(name = "order_id")
	private Integer orderId;

	@Column(name = "created_date")
	private Date createdDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getRtfPointsDebited() {
		return rtfPointsDebited;
	}

	public void setRtfPointsDebited(Integer rtfPointsDebited) {
		this.rtfPointsDebited = rtfPointsDebited;
	}

	public Integer getNoOfLives() {
		return noOfLives;
	}

	public void setNoOfLives(Integer noOfLives) {
		this.noOfLives = noOfLives;
	}

	public Integer getNoOfMagicWands() {
		return noOfMagicWands;
	}

	public void setNoOfMagicWands(Integer noOfMagicWands) {
		this.noOfMagicWands = noOfMagicWands;
	}

	public Double getRewardDollar() {
		return rewardDollar;
	}

	public void setRewardDollar(Double rewardDollar) {
		this.rewardDollar = rewardDollar;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public PointRedemptionType getPointRedemptionType() {
		return pointRedemptionType;
	}

	public void setPointRedemptionType(PointRedemptionType pointRedemptionType) {
		this.pointRedemptionType = pointRedemptionType;
	}
	
}
