package com.rtfquiz.webservices.data;

import java.util.List;

import com.rtfquiz.webservices.aws.AWSFileService;
import com.zonesws.webservices.data.GiftCardBrandDTO;
import com.zonesws.webservices.data.RtfGiftCard;
import com.zonesws.webservices.utils.Error;

public class RtfGiftCardRespose {

	private Error err;
	private Integer sts;
	private List<RtfGiftCard> cards;
	private GiftCardBrandDTO brandDTO;
	private List<GiftCardBrandDTO> gfBrands;
	private String comingSoonImg;
	
	public Error getErr() {
		return err;
	}
	public void setErr(Error err) {
		this.err = err;
	}
	public Integer getSts() {
		return sts;
	}
	public void setSts(Integer sts) {
		this.sts = sts;
	}
	public List<RtfGiftCard> getCards() {
		return cards;
	}
	public void setCards(List<RtfGiftCard> cards) {
		this.cards = cards;
	}
	public List<GiftCardBrandDTO> getGfBrands() {
		return gfBrands;
	}
	public void setGfBrands(List<GiftCardBrandDTO> gfBrands) {
		this.gfBrands = gfBrands;
	}
	public GiftCardBrandDTO getBrandDTO() {
		return brandDTO;
	}
	public void setBrandDTO(GiftCardBrandDTO brandDTO) {
		this.brandDTO = brandDTO;
	}
	public String getComingSoonImg() {
		comingSoonImg = AWSFileService.getGiftCardBrandImage("comingsoon.png");
		return comingSoonImg;
	}
	public void setComingSoonImg(String comingSoonImg) {
		this.comingSoonImg = comingSoonImg;
	}
	
	
}
