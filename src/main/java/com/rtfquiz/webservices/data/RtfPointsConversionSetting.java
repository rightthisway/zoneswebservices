package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.PowerUpType;

 
@XStreamAlias("RtfPointsConversionSetting")
@Entity
@Table(name="rtf_points_conversion_setting")
public class RtfPointsConversionSetting implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@JsonIgnore
	@Enumerated(EnumType.STRING)
	@Column(name="power_up_type")
	private PowerUpType powerUpType;
	
	@Column(name = "qty")
	private Integer qty;

	@Column(name = "rtf_points")
	private Integer rtfPoints;

	@JsonIgnore
	@Column(name = "status")
	private Boolean status;

	@JsonIgnore
	@Column(name = "created_date")
	private Date createdDate;

	@JsonIgnore
	@Column(name = "created_by")
	private String createdBy;

	@JsonIgnore
	@Column(name = "updated_date")
	private Date updatedDate;

	@JsonIgnore
	@Column(name = "update_by")
	private String updatedBy;
	
	@Transient
	private String qtyKey;
	
	@Transient
	private String popupText;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PowerUpType getPowerUpType() {
		return powerUpType;
	}

	public void setPowerUpType(PowerUpType powerUpType) {
		this.powerUpType = powerUpType;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Integer getRtfPoints() {
		return rtfPoints;
	}

	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getQtyKey() {
		if(null != qty) {
			qtyKey = String.valueOf(qty);
		}
		return qtyKey;
	}

	public void setQtyKey(String qtyKey) {
		this.qtyKey = qtyKey;
	}

	public String getPopupText() {
		if(null != powerUpType) {
			switch (powerUpType) {
				case Lives:
					popupText = "You can convert your reward points to lives.";
					break;
					
				case MagicWands:
					popupText = "You can convert your reward points to erasers.";
					break;
					
				case RewardDollars:
					popupText = "You can convert your reward points to reward$.";
					break;
					
				default:
					break;
			}
		}
		return popupText;
	}

	public void setPopupText(String popupText) {
		this.popupText = popupText;
	}
	
}
