package com.rtfquiz.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.SourceType;

 
@XStreamAlias("RtfRewardConfig")
@Entity
@Table(name="rtf_reward_config")
public class RtfRewardConfig implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "reward_title")
	private String rewardTitle;
	
	@Column(name = "no_of_lives")
	private Integer noOfLives;
	
	@Column(name = "no_of_magic_wand")
	private Integer noOfMagicWands;
	
	@Column(name = "no_of_sf_stars")
	private Integer noOfSfStars;

	@Column(name = "rtf_points")
	private Integer rtfPoints;

	@Column(name = "reward_dollars")
	private Double rewardDollar;

	@Column(name = "status")
	private Boolean status;
	
	@Enumerated(EnumType.STRING)
	@Column(name="action_type")
	private SourceType sourceType;

	@Column(name = "active_date")
	private Date activeDate;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "update_by")
	private String updatedBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRewardTitle() {
		return rewardTitle;
	}

	public void setRewardTitle(String rewardTitle) {
		this.rewardTitle = rewardTitle;
	}

	public Integer getNoOfLives() {
		return noOfLives;
	}

	public void setNoOfLives(Integer noOfLives) {
		this.noOfLives = noOfLives;
	}

	public Integer getNoOfMagicWands() {
		return noOfMagicWands;
	}

	public void setNoOfMagicWands(Integer noOfMagicWands) {
		this.noOfMagicWands = noOfMagicWands;
	}

	public Integer getNoOfSfStars() {
		return noOfSfStars;
	}

	public void setNoOfSfStars(Integer noOfSfStars) {
		this.noOfSfStars = noOfSfStars;
	}

	public Integer getRtfPoints() {
		return rtfPoints;
	}

	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}

	public Double getRewardDollar() {
		return rewardDollar;
	}

	public void setRewardDollar(Double rewardDollar) {
		this.rewardDollar = rewardDollar;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
