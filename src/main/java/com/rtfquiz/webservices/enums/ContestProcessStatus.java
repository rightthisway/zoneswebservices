package com.rtfquiz.webservices.enums;

public enum ContestProcessStatus {
	ACTIVE,STARTED,WINNERCOMPUTED,GRANDWINNERCOMPUTED,ENDCONTEST;
}
