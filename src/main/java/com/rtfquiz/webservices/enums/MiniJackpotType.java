package com.rtfquiz.webservices.enums;

public enum MiniJackpotType {

	NONE,GIFTCARD,TICKET,LIFELINE,REWARD,SUPERFANSTAR,RTFPOINTS
}
