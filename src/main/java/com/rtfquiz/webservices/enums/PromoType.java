package com.rtfquiz.webservices.enums;

public enum PromoType {
	CONTEST_PROMO,
	REWARD_PROMO,
	REWARD_EARNLIFE,
	REWARD_FREE_TIX;
}
