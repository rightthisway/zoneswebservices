package com.rtfquiz.webservices.enums;

public enum WinnerStatus {

	ACTIVE,ORDERED,EXPIRED,PROCESSED;
}
