package com.rtfquiz.webservices.jobs;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizOTPTracking;

public class QuizExpiredOTPCleaner extends QuartzJobBean implements StatefulJob{
	
	protected void cleanLockedTickets(){
		
		List<QuizOTPTracking> otpTrackings = QuizDAORegistry.getQuizOTPTrackingDAO().getAllActiveOTP();
		
		for (QuizOTPTracking quizOTPTracking : otpTrackings) {
			
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		cleanLockedTickets();
	}
}
