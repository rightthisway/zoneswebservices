package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.utils.Connections;


public class ContestGrandWinnerSQLDAO {
	static Connections connections  = new Connections();

public boolean saveAllContestGrandWinners(List<ContestGrandWinner> contestGrandWinnersList) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO contest_grand_winners (customer_id,contest_id,reward_tickets,created_date,expiry_date,status,winner_type,question_id,"
			+ " mini_jackpot_type,event_id, order_id,is_notified,notified_date,notes) " +
			" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(ContestGrandWinner grandWinner : contestGrandWinnersList){	
			preparedStatement.setInt(1,grandWinner.getCustomerId());
			preparedStatement.setInt(2,grandWinner.getContestId());
			preparedStatement.setDouble(3,grandWinner.getRewardTickets());
			if(grandWinner.getCreatedDate() != null) {
				preparedStatement.setTimestamp(4,new Timestamp(grandWinner.getCreatedDate().getTime()));
			} else {
				preparedStatement.setString(4,null);
			}
			if(grandWinner.getExpiryDate() != null) {
				preparedStatement.setTimestamp(5,new Timestamp(grandWinner.getExpiryDate().getTime()));
			} else {
				preparedStatement.setString(5,null);
			}
			if(grandWinner.getStatus() != null) {
				preparedStatement.setString(6,grandWinner.getStatus().toString());
			} else {
				preparedStatement.setString(6,null);
			}
			if(grandWinner.getWinnerType() != null) {
				preparedStatement.setString(7,grandWinner.getWinnerType().toString());
			} else {
				preparedStatement.setString(7,null);
			}
			if(grandWinner.getQuestionId() != null) {
				preparedStatement.setInt(8,grandWinner.getQuestionId());
			} else {
				preparedStatement.setString(8,null);
			}
			if(grandWinner.getJackpotCreditType() != null) {
				preparedStatement.setString(9,grandWinner.getJackpotCreditType().toString());
			} else {
				preparedStatement.setString(9,null);
			}
			if(grandWinner.getEventId() != null) {
				preparedStatement.setInt(10,grandWinner.getEventId());
			} else {
				preparedStatement.setString(10,null);
			}
			if(grandWinner.getOrderId() != null) {
				preparedStatement.setInt(11,grandWinner.getOrderId());
			} else {
				preparedStatement.setString(11,null);
			}
			preparedStatement.setBoolean(12,grandWinner.getIsNotified());
			if(grandWinner.getNotifiedTime() != null) {
				preparedStatement.setTimestamp(13,new Timestamp(grandWinner.getNotifiedTime().getTime()));
			} else {
				preparedStatement.setString(13,null);
			}
			preparedStatement.setString(14,grandWinner.getNotes());
			
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}
}
}
