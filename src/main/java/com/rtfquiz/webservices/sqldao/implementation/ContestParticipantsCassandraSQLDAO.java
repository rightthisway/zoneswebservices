package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import com.rtfquiz.webservices.data.ContestParticipantsCassandra;
import com.rtfquiz.webservices.utils.Connections;


public class ContestParticipantsCassandraSQLDAO {
	static Connections connections  = new Connections();
	
public static boolean saveAllContestParticipantsCassandra(List<ContestParticipantsCassandra> contParticipantsCassList) throws Exception {
		
		Connection connection = connections.getRtfQuizMasterConnection();  
		connection.setAutoCommit(false);
		
		String query = "INSERT INTO contest_participants_cassandra(contest_id,customer_id,ip_address,platform,join_datetime,exit_datetime,status) " +
				"VALUES (?,?,?,?,?,?,?)";
		  PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			for(ContestParticipantsCassandra contWinner : contParticipantsCassList){	
				preparedStatement.setInt(1,contWinner.getContestId());
				preparedStatement.setInt(2,contWinner.getCustomerId());
				preparedStatement.setString(3,contWinner.getIpAddress());
				preparedStatement.setString(4,contWinner.getPlatform());
				if(contWinner.getJoinDateTime() != null) {
					preparedStatement.setTimestamp(5,new Timestamp(contWinner.getJoinDateTime().getTime()));
				} else {
					preparedStatement.setString(5,null);
				}
				if(contWinner.getExitDateTime() != null) {
					preparedStatement.setTimestamp(6,new Timestamp(contWinner.getExitDateTime().getTime()));
				} else {
					preparedStatement.setString(6,null);
				}
				preparedStatement.setString(7,contWinner.getStatus());
				
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			
			if(preparedStatement!=null){
				preparedStatement.close();
			}
		}
	}
}
