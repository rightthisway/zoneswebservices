package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.utils.Connections;


public class ContestWinnersSQLDAO {
	static Connections connections  = new Connections();

public static boolean saveAllContestWinners(List<QuizContestWinners> contestWinnersList) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO contest_winners(contest_id,customer_id,reward_type,created_datetime,"
			+ " reward_tickets,reward_points,reward_rank) " +
			"VALUES (?,?,?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(QuizContestWinners contWinner : contestWinnersList){	
			preparedStatement.setInt(1,contWinner.getContestId());
			preparedStatement.setInt(2,contWinner.getCustomerId());
			if(contWinner.getRewardType() != null) {
				preparedStatement.setInt(3,contWinner.getRewardType().ordinal());
			} else {
				preparedStatement.setString(3,null);
			}
			if(contWinner.getCreatedDateTime() != null) {
				preparedStatement.setTimestamp(4,new Timestamp(contWinner.getCreatedDateTime().getTime()));
			} else {
				preparedStatement.setString(4,null);
			}
			
			preparedStatement.setInt(5,contWinner.getRewardTickets());
			preparedStatement.setDouble(6,contWinner.getRewardPoints());
			preparedStatement.setInt(7,contWinner.getRewardRank());
			
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}
}	
}
