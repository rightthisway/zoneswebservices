package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.utils.Connections;


public class CustomerContestAnswersSQLDAO {
	static Connections connections  = new Connections();
	
public static boolean saveAllCustomerContestAnswers(List<QuizCustomerContestAnswers> custAnsList) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO customer_contest_answers (customer_id,contest_id,question_id,question_sl_no,answer,created_datetime,"
			+ " is_correct_answer,is_lifeline_used,updated_datetime,answer_rewards,cumulative_rewards,cumulative_lifeline_used,rt_count,"
			+ " ans_time,fb_callback_time,auto_created,rtf_points ) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(QuizCustomerContestAnswers custAns : custAnsList){	
			preparedStatement.setInt(1,custAns.getCustomerId());
			preparedStatement.setInt(2,custAns.getContestId());
			preparedStatement.setInt(3,custAns.getQuestionId());
			preparedStatement.setInt(4,custAns.getQuestionSNo());
			preparedStatement.setString(5,custAns.getAnswer());
			if(custAns.getCreatedDateTime() != null) {
				preparedStatement.setTimestamp(6,new Timestamp(custAns.getCreatedDateTime().getTime()));	
			} else {
				preparedStatement.setString(6,null);
			}
			preparedStatement.setBoolean(7,custAns.getIsCorrectAnswer());
			preparedStatement.setBoolean(8,custAns.getIsLifeLineUsed());
			if(custAns.getUpdatedDateTime() != null) {
				preparedStatement.setTimestamp(9,new Timestamp(custAns.getUpdatedDateTime().getTime()));	
			} else {
				preparedStatement.setString(9,null);
			}
			preparedStatement.setDouble(10,custAns.getAnswerRewards());
			preparedStatement.setDouble(11,custAns.getCumulativeRewards());
			preparedStatement.setInt(12,custAns.getCumulativeLifeLineUsed());
			preparedStatement.setInt(13,custAns.getRetryCount());
			
			if(custAns.getAnswerTime() != null) {
				preparedStatement.setTimestamp(14,new Timestamp(custAns.getAnswerTime().getTime()));	
			} else {
				preparedStatement.setString(14,null);
			}
			if(custAns.getFbCallbackTime() != null) {
				preparedStatement.setTimestamp(15,new Timestamp(custAns.getFbCallbackTime().getTime()));	
			} else {
				preparedStatement.setString(15,null);
			}
			preparedStatement.setBoolean(16,custAns.getIsAutoCreated());
			preparedStatement.setInt(17,custAns.getRtfPoints());
			
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}

public static boolean saveAllCustomerQuestionAnswers(List<QuizCustomerContestAnswers> contestAnswersList) throws Exception {
	
	Connection connection = connections.getRtfZonesPlatformConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO [dbo].[customer_contest_answers] ([customer_id] ,[contest_id] ,[question_id] ,[question_sl_no] ,[answer] ,[created_datetime],"
			+ "[is_correct_answer] ,[is_lifeline_used] ,[updated_datetime] ,[answer_rewards] ,[cumulative_rewards] ,[cumulative_lifeline_used],"
			+ "[rt_count] ,[ans_time] ,[fb_callback_time] ,[auto_created]) VALUES (? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(QuizCustomerContestAnswers object : contestAnswersList){
			
			preparedStatement.setInt(1,object.getCustomerId());
			preparedStatement.setInt(2,object.getContestId());
			preparedStatement.setInt(3,object.getQuestionId());
			preparedStatement.setInt(4,object.getQuestionSNo());
			preparedStatement.setString(5,object.getAnswer());
			preparedStatement.setTimestamp(6,new Timestamp(object.getCreatedDateTime().getTime()));
			preparedStatement.setBoolean(7,object.getIsCorrectAnswer());
			preparedStatement.setBoolean(8,object.getIsLifeLineUsed());
			preparedStatement.setTimestamp(9,new Timestamp(object.getUpdatedDateTime().getTime()));
			preparedStatement.setDouble(10,object.getAnswerRewards());
			preparedStatement.setDouble(11,object.getCumulativeRewards());
			preparedStatement.setInt(11,object.getCumulativeLifeLineUsed());
			preparedStatement.setInt(12,object.getRetryCount());
			preparedStatement.setTimestamp(13,new Timestamp(object.getAnswerTime().getTime()));
			preparedStatement.setTimestamp(14,new Timestamp(object.getFbCallbackTime().getTime()));
			preparedStatement.setBoolean(15,object.getIsAutoCreated());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}
}
