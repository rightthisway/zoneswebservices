package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomerContestDetails;
import com.rtfquiz.webservices.utils.Connections;


public class CustomerContestDetailsSQLDAO {
	static Connections connections  = new Connections();
	
public static boolean saveAllCustContestDetails(List<QuizCustomerContestDetails> custContestDetailsList) throws Exception {
		
		Connection connection = connections.getRtfQuizMasterConnection();  
		connection.setAutoCommit(false);
		
		String query = "INSERT INTO customer_contest_details(customer_id,contest_id,last_quest_no,is_last_quest_crt,is_last_quest_life,"
				+ " cumulative_rwds,cumulative_life,created_date,rtf_points) " +
				  " VALUES (?,?,?,?,?,?,?,?,?)";
		  PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			for(QuizCustomerContestDetails contWinner : custContestDetailsList){	
				preparedStatement.setInt(1,contWinner.getCuId());
				preparedStatement.setInt(2,contWinner.getCoId());
				preparedStatement.setInt(3,contWinner.getLqNo());
				preparedStatement.setBoolean(4,contWinner.getIsLqCrt());
				preparedStatement.setBoolean(5,contWinner.getIsLqLife());
				preparedStatement.setDouble(6,contWinner.getCuRwds());
				preparedStatement.setInt(7,contWinner.getCuLife());
				if(contWinner.getCreatedDate() != null) {
					preparedStatement.setTimestamp(8,new Timestamp(contWinner.getCreatedDate().getTime()));
				} else {
					preparedStatement.setString(8,null);
				}
				preparedStatement.setInt(9, contWinner.getRtfPoints());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			
			if(preparedStatement!=null){
				preparedStatement.close();
			}
		}
	}
}
