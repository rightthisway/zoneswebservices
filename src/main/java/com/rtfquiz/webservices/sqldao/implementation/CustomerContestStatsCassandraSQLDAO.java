package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import com.rtfquiz.webservices.utils.Connections;
import com.zonesws.webservices.data.CustomerCassandra;


public class CustomerContestStatsCassandraSQLDAO {
	static Connections connections  = new Connections();


public static boolean saveAllContestStats(List<CustomerCassandra> custCassandraList) throws Exception {
	
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO customer_contest_stats_from_cassandra (customer_id,contest_id,lives_before_contest,lives_used,"
			+ "lives_after_contest,created_datetime,contest_ques_rewards,contest_winner_rewards,cumulative_contest_rewards,que_rtf_points,rtf_points_bf_contest ) " +
			"VALUES (?,?,?,?,?,getdate(),?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(CustomerCassandra obj : custCassandraList){	
			preparedStatement.setInt(1,obj.getCustomerId());
			preparedStatement.setInt(2,obj.getContestId());
			preparedStatement.setInt(3,obj.getLivesBeforeContest());
			preparedStatement.setInt(4,obj.getLivesUsed());
			preparedStatement.setInt(5,obj.getLivesAfterContest()); 
			preparedStatement.setDouble(6,obj.getContestQusReward());
			preparedStatement.setDouble(7,obj.getContestWinnerReward());
			preparedStatement.setDouble(8,obj.getCumulativeContestRewards()); 
			preparedStatement.setInt(9,obj.getQuestRtfPoints());
			preparedStatement.setInt(10,obj.getRtfPointsBeforeContest());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}


}
