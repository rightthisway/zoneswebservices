package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import com.rtfquiz.webservices.utils.Connections;
import com.zonesws.webservices.data.Customer;


public class CustomerLoyaltyRewardInfoSQLDAO {
	static Connections connections  = new Connections();
public static boolean updateCustomerRewards(List<Customer> custList) throws Exception {
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	String query = "update cust_loyalty_reward_info set active_reward_points=active_reward_points+?, latest_earn_points=?,total_earn_points=total_earn_points+?, "
			+ "latest_spent_points=0,updated_time=getdate() where cust_id=?";
	PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(Customer obj : custList){	
			preparedStatement.setDouble(1,obj.getThisContestRewards());
			preparedStatement.setDouble(2,obj.getThisContestRewards());
			preparedStatement.setDouble(3,obj.getThisContestRewards()); 
			preparedStatement.setInt(4,obj.getId()); 
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}
}
