package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import com.rtfquiz.webservices.utils.Connections;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;


public class CustomerLoyaltyTrackingSQLDAO {
	static Connections connections  = new Connections();
public static boolean saveAllLoyaltyTracking(List<CustomerLoyaltyTracking> loyaltyTrackingList) throws Exception {
	
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	
	
	String query = "INSERT INTO cust_loyalty_reward_info_tracking (customer_id, contest_id, reward_points, created_by, created_date, reward_type,rtf_points ) " +
			"VALUES (?,?,?,'AUTO',getdate(),'CONTEST',?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(CustomerLoyaltyTracking obj : loyaltyTrackingList){	
			preparedStatement.setInt(1,obj.getCustomerId());
			preparedStatement.setInt(2,obj.getContestId());
			preparedStatement.setDouble(3,obj.getRewardPoints());
			preparedStatement.setInt(4,obj.getRtfPoints());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}
}
