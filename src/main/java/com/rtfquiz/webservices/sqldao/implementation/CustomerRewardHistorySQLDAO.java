package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.rtfquiz.webservices.utils.Connections;
import com.zonesws.webservices.enums.SourceType;


public class CustomerRewardHistorySQLDAO {
	static Connections connections  = new Connections();

public static boolean saveCustomerRewardHistory(Integer customerId, String trxType, SourceType sourceType, Integer sourceRefId,Integer lives,
		Integer magicWands, Integer sfStars, Integer rtfPoints, Double rewardDollar, String description) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	String query = "INSERT INTO rtf_cust_reward_history(customer_id,transaction_type,source_type,ref_id,lives,magic_wands,"
			+ "sf_stars,rtf_points,reward_dollars,created_date,description) " +
			"VALUES (?,?,?,?,?,?,?,?,?,getdate(),?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		preparedStatement.setInt(1,customerId);
		preparedStatement.setString(2,trxType);
		preparedStatement.setString(3,sourceType.toString());
		preparedStatement.setInt(4,null != sourceRefId?sourceRefId:-1);
		preparedStatement.setInt(5,null != lives?lives:0);
		preparedStatement.setInt(6,null != magicWands?magicWands:0);
		preparedStatement.setInt(7,null != sfStars?sfStars:0);
		preparedStatement.setInt(8,null != rtfPoints?rtfPoints:0);
		preparedStatement.setDouble(9,null != rewardDollar?rewardDollar:0.0);
		preparedStatement.setString(10,null != description?description:"");
		preparedStatement.executeUpdate();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}
}

}
