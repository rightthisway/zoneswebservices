package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtfquiz.webservices.utils.Connections;
import com.zonesws.webservices.data.Customer;


public class CustomerSQLDAO {
	
	static Connections connections  = new Connections();

public static Customer getCustomerData(Customer object) {
	String sql = "select c.id,c.quiz_cust_lives as cLife, cr.active_reward_points as cReward from customer c inner join cust_loyalty_reward_info cr "
			+ "on cr.cust_id = c.id where c.id=" + object.getId();
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next()) {
			object = new Customer();
			object.setId(rs.getInt("id"));
			object.setQuizCustomerLives(rs.getInt("cLife"));;
			object.setRewardDollar(rs.getDouble("cReward"));
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
	}

	return object;

}

public static Map<Integer, Customer> getAllOtpVerifiedCustomerData() {
	String sql = "select c.id,c.quiz_cust_lives as cLife,c.rtf_points as rtfPoints, cr.active_reward_points as cReward from customer c inner join cust_loyalty_reward_info cr "
			+ "on cr.cust_id = c.id and c.is_otp_verified=1";
	Map<Integer, Customer> custMap = new HashMap<Integer, Customer>();
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		Customer object = null;
		while (rs.next()) {
			object = new Customer();
			object.setId(rs.getInt("id"));
			object.setQuizCustomerLives(rs.getInt("cLife"));;
			object.setRewardDollar(rs.getDouble("cReward"));
			object.setRtfPoints(rs.getInt("rtfPoints"));
			custMap.put(object.getId(), object);
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return custMap;

}
public static boolean updateCustomerLives(List<Customer> customerLivesList) throws Exception {
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	String query = "update customer set quiz_cust_lives=? where id=?";
	PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(Customer obj : customerLivesList){	
			preparedStatement.setInt(1,obj.getQuizCustomerLives());
			preparedStatement.setInt(2,obj.getId()); 
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}


public static boolean updateCustomerLivesAndRtfPoints(List<Customer> customerLivesList) throws Exception {
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	String query = "update customer set quiz_cust_lives=?,rtf_points=? where id=?";
	PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(Customer obj : customerLivesList){	
			preparedStatement.setInt(1,obj.getQuizCustomerLives());
			preparedStatement.setInt(2,obj.getRtfPoints()); 
			preparedStatement.setInt(3,obj.getId());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}
}
