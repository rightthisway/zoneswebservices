package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import com.rtfquiz.webservices.data.ContestParticipantsCassandra;
import com.rtfquiz.webservices.data.QuizContestPasswordAuth;
import com.rtfquiz.webservices.utils.Connections;


public class QuizContestPasswordAuthSQLDAO {
	static Connections connections  = new Connections();
	
public static boolean saveAllQuizContestPasswordAuth(List<QuizContestPasswordAuth> quizcontPwdAuthList) throws Exception {
		
		Connection connection = connections.getRtfQuizMasterConnection();  
		connection.setAutoCommit(false);
		
		String query = "INSERT INTO contest_password_auth (co_id,cu_id, password,is_auth, created_date, updated_date) "
				+ " VALUES (?, ?, ?, ?, ?,?)";
		  PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			for(QuizContestPasswordAuth quizContPwdAuth : quizcontPwdAuthList){	
				preparedStatement.setInt(1,quizContPwdAuth.getContestId());
				preparedStatement.setInt(2,quizContPwdAuth.getCustomerId());
				preparedStatement.setString(3,quizContPwdAuth.getPassword());
				preparedStatement.setBoolean(4,quizContPwdAuth.getIsAuth());
				if(quizContPwdAuth.getCreatedDate() != null) {
					preparedStatement.setTimestamp(5,new Timestamp(quizContPwdAuth.getCreatedDate().getTime()));
				} else {
					preparedStatement.setString(5,null);
				}
				if(quizContPwdAuth.getUpdatedDate() != null) {
					preparedStatement.setTimestamp(6,new Timestamp(quizContPwdAuth.getUpdatedDate().getTime()));
				} else {
					preparedStatement.setString(6,null);
				}
				
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			
			if(preparedStatement!=null){
				preparedStatement.close();
			}
		}
	}
}
