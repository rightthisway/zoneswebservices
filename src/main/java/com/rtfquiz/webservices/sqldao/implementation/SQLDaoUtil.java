package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rtf.ecomerce.pojo.GenRewardDVO;
import com.rtf.ecomerce.pojo.LivtWinnerUploadDTO;
import com.rtfquiz.webservices.utils.Connections;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.list.MyReward;
import com.zonesws.webservices.utils.list.ReferralDTO;


public class SQLDaoUtil {
	static Connections connections  = new Connections();

public static boolean updateContestSummaryTillDateDataTableSql() throws Exception {
	 
	 CallableStatement cs = null;
	 try{
		 Connection connection = connections.getRtfQuizMasterConnection();
	     cs = connection.prepareCall("{call SP_QMAP02a_load_contest_summary_rank_till_date()}");
       return cs.execute();
       
	 }catch(Exception e){
		 e.printStackTrace();
		 //return false;
		 throw e;
	 }
}
public static boolean updateContestSummaryThisWeekDataTableSql() throws Exception {
	 
	 CallableStatement cs = null;
	 try{
		 Connection connection = connections.getRtfQuizMasterConnection();
	     cs = connection.prepareCall("{call SP_QMAP01a_load_contest_summary_rank_for_week()}");
       return cs.execute();
       
	 }catch(Exception e){
		 e.printStackTrace();
		 //return false;
		 throw e;
	 }
}



public static List<ReferralDTO> getAllCustomerReferrals(Integer customerId) {
	
	
	String sql = "select c.id,c.user_id as userId, c.phone as phone,rt.created_datetime,c.signup_date from "
			+ ""+URLUtil.quizApiLinkedServer+".customer_referral_tracking rt with(nolock) "
			+ " inner join customer c with(nolock) on c.id = rt.customer_id and c.is_otp_verified=1 "
			+ "where rt.referral_customer_id=" +customerId+" order by rt.created_datetime desc";
	//System.out.println(sql);
	
	List<ReferralDTO> list = new ArrayList<ReferralDTO>();
	ReferralDTO dto = null; 
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next()) {
			dto = new ReferralDTO();
			dto.setCustomerId(rs.getInt("id"));
			dto.setPhoneNo(rs.getString("phone"));
			dto.setReferralDate(rs.getString("created_datetime"));
			dto.setUserId(rs.getString("userId"));
			dto.setSignUpDate(rs.getString("signup_date"));
			list.add(dto);
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
	}

	return list;

}

public static MyReward getCustomerRewards(Integer customerId) {
	String sql = "select c.id,c.quiz_cust_lives as cLife, c.rtf_points as rtfPoints,c.magic_wands as magicwand, sf.total_no_of_chances as sfStars, "
			+ "cr.active_reward_points as cReward from customer c with(nolock) left join cust_loyalty_reward_info cr with(nolock)  on cr.cust_id = c.id "
			+ " left join "+URLUtil.quizApiLinkedServer+".quiz_super_fan_stat sf with(nolock) "
			+ "on sf.customer_id =c.id where c.id=" +customerId;
	MyReward myReward = null; 
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next()) {
			myReward = new MyReward();
			myReward.setCid(customerId);
			myReward.setArp(rs.getInt("rtfPoints"));
			myReward.setAl(rs.getInt("cLife"));
			myReward.setAmw(rs.getInt("magicwand"));
			myReward.setArd(rs.getDouble("cReward"));
			myReward.setAs(rs.getInt("sfStars"));
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
	}

	return myReward;

}
public static boolean uploadContestWinnerData(LivtWinnerUploadDTO winnerUploadObj){
	Connection conn =  null;
	PreparedStatement pstmt = null;
	
	try {
		int batchSize = 10000;
		
		String sqlQuery = "insert into rtf_mig_saas_rtf_contest_winner(custid,conid,con_start_date,con_running_no) values(?,?,?,?)";
		conn = connections.getRtfEcommConnection();
		conn.setAutoCommit(false);
		pstmt = conn.prepareStatement(sqlQuery);
		
		List<String> custList = winnerUploadObj.getCustList();
		int count =0;
		for (String custId : custList) {
			
			pstmt.setString(1, custId);
			pstmt.setString(2, winnerUploadObj.getCoId());
			if(winnerUploadObj.getConStartDat() != null) {
				pstmt.setTimestamp(3, new Timestamp(winnerUploadObj.getConStartDat().getTime()));	
			} else {
				pstmt.setTimestamp(3, new Timestamp(new Date().getTime()));	
			}
			pstmt.setInt(4, winnerUploadObj.getCrNo());
			pstmt.addBatch();
			count++;
			if (count == batchSize) {
				pstmt.executeBatch();
				conn.commit();
				pstmt.clearBatch();
				System.out.println("[WINNER UPLOAD]" + count);
				count = 0;
			}
		}
		if (count > 0) {
			System.out.println("[WINNER UPLOAD]" + count);
			pstmt.executeBatch();
			conn.commit();
		}
		return true;
	}  catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			pstmt.close();
			
			if(conn != null && !conn.isClosed()) {
				conn.setAutoCommit(true);
				conn.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	return false;
}
public static boolean uploadCustomerLoyaltyPoints(List<GenRewardDVO> list){
	Connection conn =  null;
	PreparedStatement pstmt = null;
	
	try {
		String sqlQuery = "update customer set rtf_cur_val_bal =rtf_cur_val_bal+? where id=?";
		conn = connections.getRtfZonesPlatformConnection();
		conn.setAutoCommit(false);
		pstmt = conn.prepareStatement(sqlQuery);
		
		for (GenRewardDVO rewrdObj : list) {
			
			pstmt.setInt(1, rewrdObj.getRwdval().intValue());
			pstmt.setInt(2, rewrdObj.getCustomerId());
			pstmt.addBatch();
		}
		pstmt.executeBatch();
		conn.commit();
		return true;
	}  catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			pstmt.close();
			
			if(conn != null && !conn.isClosed()) {
				conn.setAutoCommit(true);
				conn.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	return false;
}
public static boolean uploadCustomerLoyaltyPointsTracking(List<GenRewardDVO> list,String conId,Integer conRunNo) {
	Connection conn =  null;
	PreparedStatement pstmt = null;
	
	try {
		String sqlQuery = "insert into rtf_cust_loyalty_points_trans(cust_id,earned_loyalpnts,"
				+ "before_trans_loyalpnts,after_trans_loyalpnts,con_id,con_running_no,spent_loyalpnts,trans_type,trans_status,created_date,created_by,order_id)"
				+ " values(?,?,?,?,?,?,0,'CONT_REWARDS','ACTIVE',getdate(),'AUTO',-1)";
		conn = connections.getRtfEcommConnection();
		conn.setAutoCommit(false);
		pstmt = conn.prepareStatement(sqlQuery);
		
		for (GenRewardDVO rewrdObj : list) {
			
			pstmt.setInt(1, rewrdObj.getCustomerId());
			pstmt.setInt(2, rewrdObj.getContRwdPoints());
			pstmt.setInt(3, rewrdObj.getCustOldLoyaltyPoints());
			pstmt.setInt(4, rewrdObj.getUpdLoyltyPoints());
			pstmt.setString(5, conId);
			pstmt.setInt(6, conRunNo);
			pstmt.addBatch();
		}
		pstmt.executeBatch();
		conn.commit();
		return true;
	}  catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			pstmt.close();
			
			if(conn != null && !conn.isClosed()) {
				conn.setAutoCommit(true);
				conn.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	return false;
}

public static boolean updateLivtHallOfFameData() throws Exception {
	 
	 CallableStatement cs = null;
	 Connection connection =null;
	 try{
		 connection = connections.getRtfEcommConnection();
	     cs = connection.prepareCall("{call spGenRTFHallOfFameData()}");
      return cs.execute();
      
	 }catch(Exception e){
		 e.printStackTrace();
		 //return false;
		 throw e;
	 } finally {
			try {
				if(connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
}
 


}
