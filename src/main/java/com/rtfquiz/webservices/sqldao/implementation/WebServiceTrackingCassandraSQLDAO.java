package com.rtfquiz.webservices.sqldao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.List;

import com.rtfquiz.webservices.utils.Connections;
import com.zonesws.webservices.data.WebServiceTrackingCassandra;


public class WebServiceTrackingCassandraSQLDAO {
	static Connections connections  = new Connections();

public static boolean saveAllRtfTracking(List<WebServiceTrackingCassandra> rtftrackingList) throws Exception {
		
		Connection connection = connections.getRtfZonesPlatformConnection();  
		connection.setAutoCommit(false);
		
		String query = "INSERT INTO web_service_tracking_cassandra (api_name,created_datetime,start_date,end_date,action_result,contest_id,cust_id,"
				+ " cust_ip_addr,description,device_info,device_type,platform,session_id,app_ver,fb_callback_time,device_api_start_time,"
				+ " res_status,question_no,rt_count,ans_count,node_id ) " +
				"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		  PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			for(WebServiceTrackingCassandra rtfTracking : rtftrackingList){	
				preparedStatement.setString(1,rtfTracking.getApiName());
				if(rtfTracking.getCreatedDateTime() != null) {
					preparedStatement.setTimestamp(2,new Timestamp(rtfTracking.getCreatedDateTime().getTime()));	
				} else {
					preparedStatement.setString(2,null);
				}
				if(rtfTracking.getStartDate() != null) {
					preparedStatement.setTimestamp(3,new Timestamp(rtfTracking.getStartDate().getTime()));	
				} else {
					preparedStatement.setString(3,null);
				}
				if(rtfTracking.getEndDate() != null) {
					preparedStatement.setTimestamp(4,new Timestamp(rtfTracking.getEndDate().getTime()));	
				} else {
					preparedStatement.setString(4,null);
				}
				
				preparedStatement.setString(5,rtfTracking.getActionResult());
				preparedStatement.setInt(6,rtfTracking.getContestId());
				preparedStatement.setInt(7,rtfTracking.getCustId());
				preparedStatement.setString(8,rtfTracking.getCustIPAddr());
				preparedStatement.setString(9,rtfTracking.getDescription());
				preparedStatement.setString(10,rtfTracking.getDeviceInfo());
				preparedStatement.setString(11,rtfTracking.getDeviceType());
				preparedStatement.setString(12,rtfTracking.getPlatForm());
				preparedStatement.setString(13,rtfTracking.getSessionId());
				preparedStatement.setString(14,rtfTracking.getAppVersion());
				
				if(rtfTracking.getFbCallbackTime() != null) {
					preparedStatement.setTimestamp(15,new Timestamp(rtfTracking.getFbCallbackTime().getTime()));	
				} else {
					preparedStatement.setString(15,null);
				}
				if(rtfTracking.getDeviceAPIStartTime() != null) {
					preparedStatement.setTimestamp(16,new Timestamp(rtfTracking.getDeviceAPIStartTime().getTime()));	
				} else {
					preparedStatement.setString(16,null);
				}
				
				preparedStatement.setInt(17,rtfTracking.getResponseStatus());
				preparedStatement.setString(18,rtfTracking.getQuestionNo());
				preparedStatement.setInt(19,rtfTracking.getRetryCount());
				preparedStatement.setInt(20,rtfTracking.getAnsCount());
				preparedStatement.setInt(21,rtfTracking.getNodeId());
				
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			
			if(preparedStatement!=null){
				preparedStatement.close();
			}
		}	
	}
}
