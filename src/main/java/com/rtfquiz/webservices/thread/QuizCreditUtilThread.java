package com.rtfquiz.webservices.thread;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.utils.QuizAffiliateReferralCredit;
import com.rtfquiz.webservices.utils.QuizCreditUtil;
import com.rtfquiz.webservices.utils.QuizReferrerRewardCredit;

public class QuizCreditUtilThread implements Runnable{
	
	public static Map<Integer, Boolean> contestMap = new HashMap<Integer, Boolean>();
	private Integer contestId;
	private QuizContest contest;
	
	public QuizCreditUtilThread(){
		
	}
	 
	public QuizCreditUtilThread(QuizContest contest,Integer contestId){
		this.contest = contest;
		this.contestId = contestId;
	}
	
	public static Boolean getProcessedContestMap(Integer contestId) {
		return contestMap.get(contestId);
	}
	
	public static void refereshMap() {
		contestMap = new HashMap<Integer, Boolean>();
	}
	
	@Override
	public void run() {
		try {
			contestMap.put(contestId, true);
			
			Date now = new Date();
			
			List<ContestReferrerRewardCredit> contestCreditList = QuizDAORegistry.getContestReferrerRewardCreditDAO().getAllContestCredits(contestId);
			
			boolean runCreditUtil = true, runReferralCreditUtil = true, runAffiliateReferralCreditUtil = true;
			
			for (ContestReferrerRewardCredit obj : contestCreditList) {
				if(obj.getReferralProgramType().equals("CONTEST_CREDITS")) {
					runCreditUtil = false;
				}else if(obj.getReferralProgramType().equals("CUSTOMER_REFERRAL")) {
					runReferralCreditUtil = false;
				}else if(obj.getReferralProgramType().equals("AFFILIATE")) {
					runAffiliateReferralCreditUtil = false;
				}
			}
			
			String jobsName = "";
			
			if(runCreditUtil) {
				jobsName = "Super Fan Stars,";
			}
			
			if(runReferralCreditUtil) {
				jobsName = jobsName + "Referral Rewards,";
			}
			
			if(runAffiliateReferralCreditUtil) {
				jobsName = jobsName+ "Affiliate Rewards";
			}
			
			/*try {
				TwilioSMSServices.notifyContestCreditJobStatus(message);
			}catch(Exception e) {
				e.printStackTrace();
			}*/
			
			if(runCreditUtil) {
				try {
					QuizCreditUtil.start(contestId);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			if(runReferralCreditUtil) {
				try {
					QuizReferrerRewardCredit.init(contestId);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			if(runAffiliateReferralCreditUtil) {
				try {
					QuizAffiliateReferralCredit.init(contestId);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			try {
				String subject = "Contest Credit("+jobsName+") Process Done for Contest ID: "+contest.getId();
				String message = "Contest Credit("+jobsName+") Process started at "+now+" and successfully completed at "+new Date()+": "
						+ " Participant Stars: "+contest.getParticipantStar()+", Participant Lives: "+contest.getParticipantLives()+", "
						+ "Participant Rewards: "+contest.getParticipantRewards()+" and Special Referral Rewards: "+contest.getReferralRewards()+
						" are credited to the participants.";
				QuizCreditUtil.sendEmail(subject,  message, contest);
				/*TwilioSMSServices.notifyContestCreditJobStatus(message);*/
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
