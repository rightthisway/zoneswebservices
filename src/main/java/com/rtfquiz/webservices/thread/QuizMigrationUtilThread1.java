package com.rtfquiz.webservices.thread;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtfquiz.webservices.utils.QuizMigrationUtil;
import com.rtftrivia.ws.controller.QuizController;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;

public class QuizMigrationUtilThread1 implements Runnable{
	
	public static Map<Integer, Boolean> contestMap = new HashMap<Integer, Boolean>();
	private List<CustomerLoyaltyTracking> loyaltyTrackingList;
	private Integer contestId;
	
	public QuizMigrationUtilThread1(){
		
	}
	 
	public QuizMigrationUtilThread1(List<CustomerLoyaltyTracking> loyaltyTrackingList,Integer contestId){
		this.loyaltyTrackingList = loyaltyTrackingList;
		this.contestId = contestId;
	}
	
	public static Boolean getProcessedContestMap(Integer contestId) {
		return contestMap.get(contestId);
	}
	
	public static void refereshMap() {
		contestMap = new HashMap<Integer, Boolean>();
	}
	
	@Override
	public void run() {
		try {
			contestMap.put(contestId, true);
			System.out.println("ContestMigration- {QuizMigrationUtilThread1} Insert Loyalty Tracking Table Data. Process started at "+new Date());
			try {
				QuizMigrationUtil.processLoyaltyTrackingTableData(loyaltyTrackingList);
			} catch(Exception e) {
				e.printStackTrace();
			}
			System.out.println("ContestMigration- {QuizMigrationUtilThread1} Insert Loyalty Tracking Table Data. Process ended at "+new Date());
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
