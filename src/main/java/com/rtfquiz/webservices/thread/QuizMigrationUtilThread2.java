package com.rtfquiz.webservices.thread;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtfquiz.webservices.utils.QuizMigrationUtil;
import com.zonesws.webservices.data.CustomerCassandra;

public class QuizMigrationUtilThread2 implements Runnable{
	
	public static Map<Integer, Boolean> contestMap = new HashMap<Integer, Boolean>();
	private List<CustomerCassandra> custCassandraList;
	private Integer contestId;
	
	public QuizMigrationUtilThread2(){
		
	}
	 
	public QuizMigrationUtilThread2(List<CustomerCassandra> custCassandraList,Integer contestId){
		this.custCassandraList = custCassandraList;
		this.contestId = contestId;
	}
	
	public static Boolean getProcessedContestMap(Integer contestId) {
		return contestMap.get(contestId);
	}
	
	public static void refereshMap() {
		contestMap = new HashMap<Integer, Boolean>();
	}
	
	@Override
	public void run() {
		try {
			contestMap.put(contestId, true);
			System.out.println("ContestMigration- {QuizMigrationUtilThread2} Insert Customer Cassandra Table Data. Process started at "+new Date());
			try {
				QuizMigrationUtil.processCustomerCassandraTableData(custCassandraList);
			} catch(Exception e) {
				e.printStackTrace();
			}
			System.out.println("ContestMigration- {QuizMigrationUtilThread2} Insert Customer Cassandra Table Data. Process ended at "+new Date());
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
