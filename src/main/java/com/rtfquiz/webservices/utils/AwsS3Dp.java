package com.rtfquiz.webservices.utils;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.jobs.CustomerUtil;

/**
 * AwsS3Dp
 * @author KUlaganathan
 *
 */
public class AwsS3Dp  {
	 
	 
	public static void uploadPictures(List<Customer> customers) throws Exception{
		
		String fileBasePath = "/rtw/apache-tomcat-7.0.92/webapps/SvgMaps/CUSTOMER_DP/";
		
		//String fileBasePath = "C:\\Tomcat 7.0\\webapps\\SvgMaps\\CUSTOMER_DP\\";
		  
		for (Customer customerObj : customers) {
			try {
				Boolean isProcessed = DAORegistry.getQueryManagerDAO().isCustomerDpMovedToAWS(customerObj.getId());
				if(null != isProcessed && isProcessed) {
					continue;
				}
				File fileObj = new File(fileBasePath+customerObj.getCustImagePath());
				String ext = FilenameUtils.getExtension(customerObj.getCustImagePath());
				String newFileName = customerObj.getId()+"."+ext;
				try {
					AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTFMEDIA, newFileName, AWSFileService.CUSTOMER_DP_DIRECTORY, fileObj);
					customerObj.setCustImagePath(newFileName);
					//DAORegistry.getCustomerDAO().updateCustImagePath(newFileName, customerObj.getId());
					//CassandraDAORegistry.getCassCustomerDAO().updateProfilePicName(customerObj.getId(), newFileName);
					DAORegistry.getQueryManagerDAO().saveProcessedCustomers(customerObj.getId());
				}catch(Exception e) {
					e.printStackTrace();
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		} 
	}
	  
	 
}