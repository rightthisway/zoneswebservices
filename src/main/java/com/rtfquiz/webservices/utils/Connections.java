package com.rtfquiz.webservices.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zonesws.webservices.utils.URLUtil;

public class Connections {
	private static Logger logger = LoggerFactory.getLogger(Connections.class);
	
	private	Connection rtfZonesPlatformConnection;
	private	Connection rtfQuizMasterConnection;
	private	Connection rtfEcommConnection;
	 
	
	public  Connection getRtfZonesPlatformConnection() throws Exception{
		try {
			if(rtfZonesPlatformConnection!=null && !rtfZonesPlatformConnection.isClosed()){
				return rtfZonesPlatformConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("zoneTicketsConnection Exception...."+new Date());
			logger.error("zoneTicketsConnection Exception...."+new Date());
		}
		//System.out.println("TN1 New Conection....");
		logger.info("zoneTicketsConnection new Conection....");
		
		
		/*
		String url ="jdbc:sqlserver://54.209.112.185:1433;database=ZonesApiPlatformV2_Sanbox;sendStringParametersAsUnicode=false";//
		String username ="tayo";
		String password ="po01ro02ro30";*/
		

		String url = URLUtil.rtfzonesJdbcUrl;
		String username = URLUtil.rtfzonesUsername;
		String password = URLUtil.rtfzonesPassword;
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtfZonesPlatformConnection = DriverManager.getConnection(url, username, password);
			return rtfZonesPlatformConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	
	public  Connection getRtfQuizMasterConnection() throws Exception{
		try {
			if(rtfEcommConnection!=null && !rtfEcommConnection.isClosed()){
				return rtfEcommConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("rtfEcommConnection Exception...."+new Date());
			logger.error("rtfEcommConnection Exception...."+new Date());
		}
		//System.out.println("TN1 New Conection....");
		logger.info("rtfEcommConnection new Conection....");
		
		/*String url ="jdbc:sqlserver://54.209.112.185:1433;database=RTFQuizMaster_Sanbox;sendStringParametersAsUnicode=false";//
		String username ="tayo";
		String password ="po01ro02ro30";*/
		
		String url = URLUtil.rtfQuizJdbcUrl;
		String username = URLUtil.rtfQuizUsername;
		String password = URLUtil.rtfQuizPassword;
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtfQuizMasterConnection = DriverManager.getConnection(url, username, password);
			return rtfQuizMasterConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	public  Connection getRtfEcommConnection() throws Exception{
		try {
			/*if(rtfEcommConnection!=null && !rtfEcommConnection.isClosed()){
				return rtfEcommConnection;
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("rtfEcommConnection Exception...."+new Date());
			logger.error("rtfEcommConnection Exception...."+new Date());
		}
		//System.out.println("TN1 New Conection....");
		logger.info("rtfEcommConnection new Conection....");
		
		/*String url ="jdbc:sqlserver://54.209.112.185:1433;database=RTFQuizMaster_Sanbox;sendStringParametersAsUnicode=false";//
		String username ="tayo";
		String password ="po01ro02ro30";*/
		
		String url = URLUtil.rtfEcommJdbcUrl;
		String username = URLUtil.rtfEcommUsername;
		String password = URLUtil.rtfEcommPassword;
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtfEcommConnection = DriverManager.getConnection(url, username, password);
			return rtfEcommConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			//return null;
			throw sqlEx;
		}catch(Exception ex){
			ex.printStackTrace();
			//return null;
			throw ex;
		}
	}
	public static ResultSet getResultSet(Statement statement,String sql){
		try{
			return statement.executeQuery(sql);
		}catch(SQLException sqlEx){
			return null;
		}
	}
	

	public static CallableStatement getCallableStatement(Connection connnection,String sql){
		try{
			return connnection.prepareCall(sql);
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}
		
	}

}
