package com.rtfquiz.webservices.utils;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;


@Service
@Scope("singleton")
public class HazelcastServiceUtil 
{
	
	private static HazelcastInstance hz = null;
	
	
	public static HazelcastInstance getHazelCastInstance() {
		
		System.out.println("Called getHazelCastInstance*****");
		if(hz == null) {
			
			System.out.println("Instance is NULL *****");
			hz = Hazelcast.newHazelcastInstance();
		}
		System.out.println(" Instance is NOT NULL *****");
		return hz;
	}

	public static void setHz(HazelcastInstance hz) {
		HazelcastServiceUtil.hz = hz;
	}

	
	

}