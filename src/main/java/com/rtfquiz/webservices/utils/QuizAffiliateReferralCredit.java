package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.data.QuizAffiliateRewardHistory;
import com.zonesws.webservices.data.QuizAffiliateSetting;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * QuizAffiliateReferralSystem
 * @author KUlaganathan
 *
 */
public class QuizAffiliateReferralCredit extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	private static Logger logger = LoggerFactory.getLogger(QuizAffiliateReferralCredit.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	static boolean isRunning = false;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizAffiliateReferralCredit.mailManager = mailManager;
	}


	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	 
	public static void init(Integer contestId) throws Exception{
		
		System.out.println("Initiating the email scheduler process for quiz affiliate referral system....");
		try {
			
		
		System.out.println("QARS: Job Started:  Contest ID: "+contestId+" : "+ new Date());
		//System.out.println("QARS: Job Started:  Contest ID: "+contestId);
		
		QuizContest contest= QuizDAORegistry.getQuizContestDAO().getCompletedContestByContestID(contestId);
		Date start = new Date();
		if(null != contest) {
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			
			List<QuizAffiliateSetting> affiliateList = DAORegistry.getQuizAffiliateSettingDAO().getAllActiveAffiliates();
			
			if(null == affiliateList || affiliateList.isEmpty()) {
				System.out.println("QARS: inside affiliate list is empty :  Contest ID: "+contestId+" : "+ new Date());
				return;
			}
			
			Map<Integer, QuizAffiliateSetting> affiliateMap = new HashMap<Integer, QuizAffiliateSetting>();
			
			for (QuizAffiliateSetting obj : affiliateList) {
				
				if(obj.getCustomerId().equals(598155) || obj.getReferralCode().equalsIgnoreCase("HarryGBestMC")) {
					continue;
				}
				
				affiliateMap.put(obj.getCustomerId(),obj);
			}
			
			List<QuizCustomerReferralTracking> referralList = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getAllAffiliateReferrals();
			
			Map<Integer, QuizCustomerReferralTracking> tireOneUserPrimaryUserMap = new HashMap<Integer, QuizCustomerReferralTracking>();
			
			for (QuizCustomerReferralTracking tracking : referralList) {
				
				QuizAffiliateSetting affiliateSetting = affiliateMap.get(tracking.getReferralCustomerId());
				
				if(null != affiliateSetting) {
					tireOneUserPrimaryUserMap.put(tracking.getCustomerId(), tracking);
				}
			}
			
			if(null == tireOneUserPrimaryUserMap || tireOneUserPrimaryUserMap.isEmpty() || tireOneUserPrimaryUserMap.size() <= 0) {
				System.out.println("QARS: inside tireoneuser primary map is empty :  Contest ID: "+contestId+" : "+ new Date());
				return;
			}
			
			System.out.println("QARS: tireOneUserPrimaryUserMap: "+tireOneUserPrimaryUserMap.size());
			//System.out.println("QARS: tireOneUserPrimaryUserMap: "+tireOneUserPrimaryUserMap.size());
			
			try {
				 
				ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getAffiliateRewardCreditByContestId(contest.getId());
				
				if(null != credit) {
					System.out.println("QARS: inside acontest referralreward credit is empty :  Contest ID: "+contestId+" : "+ new Date());
					 return ;
				}else {
					credit = new ContestReferrerRewardCredit();
					credit.setContestId(contest.getId());
					credit.setCreatedDate(start);
					credit.setIsNotified(true);
					credit.setNotifiedTime(new Date());
					credit.setRewardCreditConv(0.00);
					credit.setReferralProgramType("AFFILIATE");
					credit.setStatus("STARTED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
				}
				
				List<QuizCustomerContestAnswers> answers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getFirstQuestionAnsweredCustomers(contest.getId());
				
				Map<Integer, Boolean> customerAnsweredMap = new HashMap<Integer, Boolean>();
				for (QuizCustomerContestAnswers contestAnswer : answers) {
					customerAnsweredMap.put(contestAnswer.getCustomerId(), true);
				}
				
				boolean isProecced = false;
				
				for (Integer tireOneCutId : tireOneUserPrimaryUserMap.keySet()) {
					
					QuizCustomerReferralTracking tracking = tireOneUserPrimaryUserMap.get(tireOneCutId);
					
					if(null == tracking) {
						continue;
					}
					
					QuizAffiliateSetting quizAffiliateSetting = affiliateMap.get(tracking.getReferralCustomerId());
					
					if(null == quizAffiliateSetting || !quizAffiliateSetting.getStatus().equalsIgnoreCase("ACTIVE")) {
						continue;
					}
					
					if(Util.isValidAffiliateReferral(start, quizAffiliateSetting)) {
						
					}else {
						continue;
					}
					
					Boolean answeredFirstQuestion = customerAnsweredMap.get(tireOneCutId);
					
					if(null == answeredFirstQuestion) {
						continue;
					}
					
					Customer customer = CustomerUtil.getCustomerById(tireOneCutId);
					
					if(customer.getSignupDate().before(quizAffiliateSetting.getEffectiveFromDate())) {
						continue;
					}
					
					Long noOfDays = Util.getDifferenceDays(customer.getSignupDate(), start);
					
					if(noOfDays > loyaltySettings.getQuizAffiliateReferralCreditMaxDays()) {
						tracking.setAffiliateReferralStatus("EXPIRED");
						tracking.setUpdatedDateTime(new Date());
						QuizDAORegistry.getQuizCustomerReferralTrackingDAO().update(tracking);
						continue;
					}
					
					QuizAffiliateRewardHistory affiliateRewardHistory = null;
					 
					if(quizAffiliateSetting.getRewardType().equals("REWARD_DOLLAR")) {
						CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(quizAffiliateSetting.getCustomerId());
						
						if(null == customerLoyalty) {
							continue;
						}
						CustomerLoyaltyHistory history = new CustomerLoyaltyHistory();
						
						history.setCreateDate(new Date());
						history.setCustomerId(quizAffiliateSetting.getCustomerId());
						history.setDollarConversionRate(loyaltySettings.getDollerConversion());
						history.setEmailDescription("");
						history.setIsRewardsEmailSent(true);
						history.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
						history.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
						history.setOrderId(null);
						history.setOrderTotal(null);
						history.setPointsEarnedAsDouble(quizAffiliateSetting.getRewardValue());
						history.setRewardConversionRate(loyaltySettings.getRewardConversion());
						history.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(quizAffiliateSetting.getRewardValue(),loyaltySettings.getDollerConversion()));
						history.setRewardSpentAmount(0.00);
						history.setRewardStatus(RewardStatus.ACTIVE);
						history.setOrderType(OrderType.CONTEST_AFFILIATE_REFERRAL);
						history.setUpdatedDate(new Date());
						history.setTireOneCustEarnPoints(0.00);
						history.setAnsQuestion(0);
						history.setTireOneCustId(tireOneCutId);
						history.setContestId(contest.getId());
						history.setOrderId(-1);
						history.setOrderTotal(0.00);
						
						Double balRewardPoints = customerLoyalty.getActivePointsAsDouble() + quizAffiliateSetting.getRewardValue() - 0;
						Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
						history.setBalanceRewardAmount(balRewardAmount);
						history.setBalanceRewardPointsAsDouble(balRewardPoints);
						 
						Double totalReferrelDollars = null != customerLoyalty.getTotalAffiliateReferralDollars()?customerLoyalty.getTotalAffiliateReferralDollars():0;
						
						customerLoyalty.setTotalAffiliateReferralDollars(totalReferrelDollars + quizAffiliateSetting.getRewardValue());
						customerLoyalty.setLastAffiliateReferralDollars(quizAffiliateSetting.getRewardValue());
						customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble() + quizAffiliateSetting.getRewardValue());
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLatestEarnedPointsAsDouble(quizAffiliateSetting.getRewardValue());
						customerLoyalty.setLatestSpentPointsAsDouble(0.00);
						customerLoyalty.setTotalEarnedPointsAsDouble(customerLoyalty.getTotalEarnedPointsAsDouble()+quizAffiliateSetting.getRewardValue());
						
						affiliateRewardHistory =new QuizAffiliateRewardHistory();
						affiliateRewardHistory.setContestId(contest.getId());
						affiliateRewardHistory.setAffiliateCustomerId(quizAffiliateSetting.getCustomerId());
						affiliateRewardHistory.setCashReward(quizAffiliateSetting.getRewardValue());
						affiliateRewardHistory.setCreatedDate(new Date());
						affiliateRewardHistory.setLivesToAffiliateCustomer(quizAffiliateSetting.getNoOfLivesToAffiliateCustomer());
						affiliateRewardHistory.setLivesToTireOneCustomer(quizAffiliateSetting.getNoOfLivesToTireOneCustomer());
						affiliateRewardHistory.setRewardDollar(0.00);
						affiliateRewardHistory.setRewardType(quizAffiliateSetting.getRewardType());
						affiliateRewardHistory.setStatus("ACTIVE");
						affiliateRewardHistory.setTireoneCustomerId(tireOneCutId);
						affiliateRewardHistory.setUpdatedDate(new Date());
						
						quizAffiliateSetting.setTotalCreditedRewardDollar(quizAffiliateSetting.getTotalCreditedRewardDollar()+quizAffiliateSetting.getRewardValue());
						DAORegistry.getCustomerLoyaltyHistoryDAO().save(history);
						
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
						
						try {
							CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customerLoyalty.getCustomerId(),customerLoyalty.getActivePointsAsDouble());
						}catch(Exception e) {
							e.printStackTrace();
						}
						
					}else{
						affiliateRewardHistory =new QuizAffiliateRewardHistory();
						affiliateRewardHistory.setContestId(contest.getId());
						affiliateRewardHistory.setAffiliateCustomerId(quizAffiliateSetting.getCustomerId());
						affiliateRewardHistory.setCashReward(quizAffiliateSetting.getRewardValue());
						affiliateRewardHistory.setCreatedDate(new Date());
						affiliateRewardHistory.setLivesToAffiliateCustomer(quizAffiliateSetting.getNoOfLivesToAffiliateCustomer());
						affiliateRewardHistory.setLivesToTireOneCustomer(quizAffiliateSetting.getNoOfLivesToTireOneCustomer());
						affiliateRewardHistory.setRewardDollar(0.00);
						affiliateRewardHistory.setRewardType(quizAffiliateSetting.getRewardType());
						affiliateRewardHistory.setStatus("ACTIVE");
						affiliateRewardHistory.setTireoneCustomerId(tireOneCutId);
						affiliateRewardHistory.setUpdatedDate(new Date());
						
						quizAffiliateSetting.setActiveCashReward(quizAffiliateSetting.getActiveCashReward()+quizAffiliateSetting.getRewardValue());
						quizAffiliateSetting.setTotalCashReward(quizAffiliateSetting.getTotalCashReward()+quizAffiliateSetting.getRewardValue());
					}
					
					if(null != affiliateRewardHistory) {
						isProecced = true;
						
						tracking.setAffiliateReferralStatus("CREDITED");
						tracking.setUpdatedDateTime(new Date());
						tracking.setContestDate(contest.getContestStartDateTime());
						
						DAORegistry.getQuizAffiliateSettingDAO().update(quizAffiliateSetting);
						DAORegistry.getQuizAffiliateRewardHistoryDAO().save(affiliateRewardHistory);
						QuizDAORegistry.getQuizCustomerReferralTrackingDAO().update(tracking);
					}
				}
				credit.setStatus("COMPLETED");
				credit.setUpdatedDate(new Date());
				QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
			}catch(Exception e) {
				System.out.println("QARS: inside catch exception 2 : "+contestId+" : "+new Date());
				e.printStackTrace();
			}
		} else {
			System.out.println("QARS: inside else Contest Not Yet completed  : "+contestId+" : "+new Date());
		}
		System.out.println("QARS: Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		System.out.println("QARS: Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date());
	}catch(Exception e){
		System.out.println("QARS: inside catch exception 3 : "+contestId+" : "+new Date());
		e.printStackTrace();
		isRunning = false;
	}
		isRunning = false;
	}
	
	public static void creditRewardDollarToTireOneCustomer(Integer tireOneCustomerId, Integer affiliateCustId,Double tireOneCustRewards) {
		
		try {
			LoyaltySettings loyaltySettings =  DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			if(null == loyaltySettings) {
				return;
			}
			
			CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(tireOneCustomerId);
			if(null == customerLoyalty) {
				return;
			}
			
			Double totalReward = TicketUtil.getRoundedUpValue(tireOneCustRewards);
			
			CustomerLoyaltyHistory history = new CustomerLoyaltyHistory();
			
			history.setCreateDate(new Date());
			history.setCustomerId(tireOneCustomerId);
			history.setDollarConversionRate(loyaltySettings.getDollerConversion());
			history.setEmailDescription("'Affiliate referral credited from "+affiliateCustId);
			history.setIsRewardsEmailSent(true);
			history.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
			history.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
			history.setOrderId(null);
			history.setOrderTotal(null);
			history.setPointsEarnedAsDouble(totalReward);
			history.setRewardConversionRate(loyaltySettings.getRewardConversion());
			history.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(totalReward,loyaltySettings.getDollerConversion()));
			history.setPointsSpentAsDouble(0.00);
			history.setRewardSpentAmount(0.00);
			history.setRewardStatus(RewardStatus.ACTIVE);
			history.setOrderType(OrderType.AFFILIATE_REFERRAL);
			history.setUpdatedDate(new Date());
			history.setTireOneCustEarnPoints(totalReward);
			history.setAnsQuestion(0);
			history.setTireOneCustId(affiliateCustId);
			history.setContestId(-1);
			history.setOrderId(-1);
			history.setOrderTotal(0.00);
			
			Double balRewardPoints = customerLoyalty.getActivePointsAsDouble() + totalReward - 0;
			Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
			history.setBalanceRewardAmount(balRewardAmount);
			history.setBalanceRewardPointsAsDouble(balRewardPoints);
			 
			Double totalReferrelDollars = null != customerLoyalty.getTotalReferralDollars()?customerLoyalty.getTotalReferralDollars():0;
			
			customerLoyalty.setTotalReferralDollars(totalReferrelDollars + totalReward);
			customerLoyalty.setLastReferralDollars(totalReward);
			customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble() + totalReward);
			customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
			customerLoyalty.setLastUpdate(new Date());
			customerLoyalty.setLatestEarnedPointsAsDouble(totalReward);
			customerLoyalty.setLatestSpentPointsAsDouble(0.00);
			customerLoyalty.setTotalEarnedPointsAsDouble(customerLoyalty.getTotalEarnedPointsAsDouble()+totalReward);
			
			DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
			DAORegistry.getCustomerLoyaltyHistoryDAO().save(history);
			
			try {
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(tireOneCustomerId,customerLoyalty.getActivePointsAsDouble());
			}catch(Exception e) {
				e.printStackTrace();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				//init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}