package com.rtfquiz.webservices.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizConfigSettings;

/**
 * QuizConfigSettingsUtil
 * @author KUlaganathan
 *
 */
public class QuizConfigSettingsUtil{
	private static Logger logger = LoggerFactory.getLogger(QuizConfigSettingsUtil.class);
	public static QuizConfigSettings quizConfigSettingsObj = null;
	
	public static QuizConfigSettings getConfigSettings(){
		if(null != quizConfigSettingsObj) {
			return quizConfigSettingsObj;
		}else {
			quizConfigSettingsObj = QuizDAORegistry.getQuizConfigSettingsDAO().getConfigSettings();
			return quizConfigSettingsObj;
		} 
	}
	
	public static void forceUpdateConfigSettings(){
		try {
			QuizConfigSettings quizConfigSettingsTemp = QuizDAORegistry.getQuizConfigSettingsDAO().getConfigSettings();
			if(null != quizConfigSettingsTemp) {
				quizConfigSettingsObj = quizConfigSettingsTemp;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	 
	
}