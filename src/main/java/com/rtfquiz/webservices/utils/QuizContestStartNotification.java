package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizConfigSettings;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.utils.list.CustomerNotificationDetails;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerNotificationDeliveryTracking;
import com.zonesws.webservices.data.Property;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.notifications.OneSignalNotificationUtil;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.APNSNotificationServicePushTemp;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Order Status Notification
 * @author KUlaganathan
 *
 */
public class QuizContestStartNotification extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	private static Logger logger = LoggerFactory.getLogger(QuizContestStartNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizContestStartNotification.mailManager = mailManager;
	}


	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	/*public void setHallOfFameMaxUserCount() {
		
		Integer maxCountPerPage = 100; 
		
		Property property = DAORegistry.getPropertyDAO().get("zws.normal.search.event.threshold");
		
		if(null != property) {
			maxCountPerPage = Integer.valueOf(property.getValue());
		}
		
		
		
	}*/
	

	public void init() throws Exception{
		
		logger.info("Initiating the email scheduler process for customer orders....");
		
		/* if events are within 5 days then do not send notification
		 * if event is 5 days away then send notification on next of order date
		 * if notification date is 30days pas then check same above condition and notifications
		 * consider unfilled orders only 
		saying You tickets are on the way and will be ready before DAY OF THE EVENT
		 */
		Integer fifteenMinsTime = 3;
		Integer thrreMinstime = 60;
		Calendar fifteenMinscal = Calendar.getInstance();
		fifteenMinscal.set(Calendar.SECOND, 0);
		fifteenMinscal.set(Calendar.MILLISECOND, 0);
		fifteenMinscal.add(Calendar.MINUTE, fifteenMinsTime);
		Date fifteenMinsDate = new Date(fifteenMinscal.getTimeInMillis());
		
		Calendar threeeMinsCal = Calendar.getInstance();
		threeeMinsCal.set(Calendar.SECOND, 0);
		threeeMinsCal.set(Calendar.MILLISECOND, 0);
		threeeMinsCal.add(Calendar.MINUTE, thrreMinstime);
		Date threeMinsDate = new Date(threeeMinsCal.getTimeInMillis());
		
		List<QuizContest> contestList = QuizDAORegistry.getQuizContestDAO().getAllMobileContestsToSendNotification(fifteenMinsDate/*, threeMinsDate*/);
		QuizConfigSettings quizConfigSettings = QuizDAORegistry.getQuizConfigSettingsDAO().getConfigSettings();
		
		
		Integer count=0,androidProcessed=0,iosProcessed=0;
		if(contestList != null && !contestList.isEmpty()) {
			System.out.println("QUIZ NOTIFICATION insode If : "+contestList.size());
			logger.info("QUIZ NOTIFICATION insode If : "+contestList.size());
			Date start = new Date();
			List<CustomerNotificationDetails> custNotificationList = DAORegistry.getQueryManagerDAO().getAllOtpVerifiedCustomersNotificationDeviceDetails();
			if(custNotificationList == null) {
				System.out.println("QUIZ NOTIFICATION Error No Customers : "+contestList.size()+" : "+new Date());
				logger.info("QUIZ NOTIFICATION Error No Customers : "+contestList.size());
				return ;
			}
			count = custNotificationList.size();
			
			for (QuizContest contest : contestList) {
			
				System.out.println("QUIZ NOTIFICATION Time : "+contest.getContestStartDateTime()+" : "+contest.getContestName());
				logger.info("QUIZ NOTIFICATION Time : "+contest.getContestStartDateTime()+" : "+contest.getContestName());
				
				String notificationMsg = "Reward The Fan is LIVE in less than FIVE";
				if(contest.getContestStartDateTime().compareTo(threeMinsDate) == 0) {
					//notificationMsg = contest.getContestName()+" Contest is going to start in Next "+thrreMinstime+" mins.";
					//notificationMsg = "Reward The Fan is LIVE in less than FIVE";
					if(quizConfigSettings != null && quizConfigSettings.getNotificationSevenMins() != null) {
						notificationMsg = quizConfigSettings.getNotificationSevenMins();
						
					}
				} else {
					//notificationMsg = contest.getContestName()+" Contest is going to start in Next "+fifteenMinsTime+" mins.";
					//notificationMsg = "Put your hands up, Reward The Fan Is giving away tickets in less than 15 minutes!";
					if(quizConfigSettings != null && quizConfigSettings.getNotificationTwentyMins() != null) {
						notificationMsg = quizConfigSettings.getNotificationTwentyMins();
					}
				}
				
				//sendBulkNotifications(notificationMsg, contest, false);
				
				sendQuizContestNotifications(custNotificationList, notificationMsg, contest, false);
				
				/*for (CustomerNotificationDetails notifiDetails : custNotificationList) {
					
					try {
						String jsonString="";
						if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.ANDROID.toString())) {
							NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
							notificationJsonObject.setNotificationType(NotificationType.QUIZ_CONTEST_START);
							notificationJsonObject.setMessage(notificationMsg);
							//notificationJsonObject.setOrderId(customerOrder.getId());
							Gson gson = new Gson();	
							jsonString = gson.toJson(notificationJsonObject);
							gcmNotificationService.sendMessage(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), jsonString);
							androidProcessed++;
						} else if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.IOS.toString())) {
							Map<String, String> customFields = new HashMap<String, String>();
							customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_CONTEST_START));
							//customFields.put("orderId", String.valueOf(customerOrder.getId()));
							apnsNotificationService.sendNotification(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), notificationMsg,customFields);
							iosProcessed++;
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("QUIZ NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId()+" : "+new Date());
						logger.info("QUIZ NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId());
					}
						
					//}
					
				}*/
			}
			System.out.println("QUIZ NOTIFICATION AUTO Completed : "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime())+" : "+new Date());
			logger.info("QUIZ NOTIFICATION AUTO Completed : "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime()));
		}
		
	}
	
public static void sendBulkNotifications(String notificationMsg,QuizContest contest,Boolean isManualTrigger) throws Exception {
		
		Date start = new Date();
		try {
		
			OneSignalNotificationUtil.sendBulkNotification(notificationMsg);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("QUIZ BULK NOTIFICATION Error While sending Notification to  : "+new Date());
			logger.info("QUIZ BULK NOTIFICATION Error While sending Notification to  : ");
			
		}
					
		Integer contestId=0;
		if(contest != null) {
			contestId=contest.getId();
		}
		System.out.println("QUIZ BULK NOTIFICATION Completed cId :"+contestId+" :isman: "+isManualTrigger+":tm: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		logger.info("QUIZ BULK NOTIFICATION Completed cId :"+contestId+" :isman: "+isManualTrigger+":tm: "+(new Date().getTime()-start.getTime()));
	}
	public static void sendQuizContestNotifications(List<CustomerNotificationDetails> custNotificationList,String notificationMsg,QuizContest contest,Boolean isManualTrigger) throws Exception {
		
		Integer androidProcessed=0,iosProcessed=0;
		Integer count = 0;
		Date start = new Date();
		if(custNotificationList != null) {
			 count = custNotificationList.size();
			 List<CustomerNotificationDeliveryTracking> iosTrackingList = new ArrayList<CustomerNotificationDeliveryTracking>();
			 List<CustomerNotificationDeliveryTracking> androidTrackingList = new ArrayList<CustomerNotificationDeliveryTracking>();
			 
			for (CustomerNotificationDetails notifiDetails : custNotificationList) {
				CustomerNotificationDeliveryTracking tracking = new CustomerNotificationDeliveryTracking(notifiDetails);
				tracking.setNotificationMsg(notificationMsg);
				if(contest != null) {
					tracking.setContestId(contest.getId());
				}
				
				try {
					String jsonString="";
					if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.ANDROID.toString())) {
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.QUIZ_CONTEST_START);
						notificationJsonObject.setMessage(notificationMsg);
						//notificationJsonObject.setOrderId(customerOrder.getId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						String respnseMSg = gcmNotificationService.sendFCMMessage(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), jsonString);
						if(respnseMSg.equals("1")) {
							tracking.setStatus("SUCCESS");
							tracking.setSenderType("FCM");
						} else {
							if(respnseMSg.equalsIgnoreCase("MismatchSenderId")) {
								respnseMSg = gcmNotificationService.sendMessageOne(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), jsonString);
								if(respnseMSg.equals("1")) {
									tracking.setStatus("SUCCESS");	
								} else {
									tracking.setStatus("FAILED");
									tracking.setErrorMsg(respnseMSg);									
								}
								tracking.setSenderType("GCM");
							} else {
								tracking.setSenderType("FCM");
								tracking.setStatus("FAILED");
								tracking.setErrorMsg(respnseMSg);
							}
						}
						androidProcessed++;
						
						tracking.setCreatedDate(new Date());
						tracking.setLastUpdated(new Date());
						
						//tracking.setMessage("SUCCESS");
						androidTrackingList.add(tracking);
						
						
					} else if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.IOS.toString())) {
						Map<String, String> customFields = new HashMap<String, String>();
						customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_CONTEST_START));
						//customFields.put("orderId", String.valueOf(customerOrder.getId()));
						apnsNotificationService.sendNotification(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), notificationMsg,customFields);
						//String respnseMSg = APNSNotificationServicePushTemp.sendNotification(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), notificationMsg,customFields);
						//if(respnseMSg.equals("1")) {
							tracking.setStatus("SUCCESS");	
						//} else {
						///	tracking.setStatus("FAILED");
						//	tracking.setErrorMsg(respnseMSg);
						//}
						iosProcessed++;
						
						tracking.setCreatedDate(new Date());
						tracking.setLastUpdated(new Date());
						tracking.setStatus("SUCCESS");
						//tracking.setMessage("SUCCESS");
						iosTrackingList.add(tracking);
						
						
						
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("QUIZ NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId()+" : "+new Date());
					logger.info("QUIZ NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId());
					
					tracking.setCreatedDate(new Date());
					tracking.setLastUpdated(new Date());
					tracking.setStatus("EXCEPTION");
					try {
						tracking.setErrorMsg(""+e.getMessage());
					} catch (Exception ee) {}
					
					if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.ANDROID.toString())) {
						androidTrackingList.add(tracking);
					} else {
						iosTrackingList.add(tracking);	
					}
					
				}
					
				//}
			}
			Map<String,Date> iosInactiveDeviceMap = apnsNotificationService.getAllInActiveDevices();
			if(iosInactiveDeviceMap != null && !iosInactiveDeviceMap.isEmpty()) {
				System.out.println("Invalid Token Count : "+iosInactiveDeviceMap.size());
				for (CustomerNotificationDeliveryTracking tracking : iosTrackingList) {
					Date lastRun = iosInactiveDeviceMap.remove(tracking.getNotificationRegId().toLowerCase());
					if(lastRun != null) {
						tracking.setStatus("INVALIDTOKEN");
						tracking.setLastSentDate(lastRun);
						System.out.println("Invalid Token  : "+tracking.getNotificationRegId());
					}
				}
			}
			
			DAORegistry.getCustomerNotificationDeliveryTrackingDAO().saveAll(androidTrackingList);
			DAORegistry.getCustomerNotificationDeliveryTrackingDAO().saveAll(iosTrackingList);
		}
		Integer contestId=0;
		if(contest != null) {
			contestId=contest.getId();
		}
		System.out.println("QUIZ NOTIFICATION Completed cId :"+contestId+" :isman: "+isManualTrigger+" :tot: "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		logger.info("QUIZ NOTIFICATION Completed cId :"+contestId+" :isman: "+isManualTrigger+" :tot: "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime()));
	}
	public void initOne() throws Exception{
		
		logger.info("Initiating the email scheduler process for customer orders....");
		
		/* if events are within 5 days then do not send notification
		 * if event is 5 days away then send notification on next of order date
		 * if notification date is 30days pas then check same above condition and notifications
		 * consider unfilled orders only 
		saying You tickets are on the way and will be ready before DAY OF THE EVENT
		 */
		Integer fifteenMinsTime = 15;
		Integer thrreMinstime = 3;
		Calendar fifteenMinscal = Calendar.getInstance();
		fifteenMinscal.set(Calendar.SECOND, 0);
		fifteenMinscal.set(Calendar.MILLISECOND, 0);
		fifteenMinscal.add(Calendar.MINUTE, fifteenMinsTime);
		Date fifteenMinsDate = new Date(fifteenMinscal.getTimeInMillis());
		
		Calendar threeeMinsCal = Calendar.getInstance();
		threeeMinsCal.set(Calendar.SECOND, 0);
		threeeMinsCal.set(Calendar.MILLISECOND, 0);
		threeeMinsCal.add(Calendar.MINUTE, thrreMinstime);
		Date threeMinsDate = new Date(threeeMinsCal.getTimeInMillis());
		

		List<QuizContest> contestList = QuizDAORegistry.getQuizContestDAO().getAllMobileContestsToSendNotification(fifteenMinsDate, threeMinsDate);
		List<Customer> customers = DAORegistry.getCustomerDAO().getAllOtpVerifiedCustomers();
		
		if(contestList != null) {
			System.out.println("QUIZ NOTIFICATION insode If : "+contestList.size());
			logger.info("QUIZ NOTIFICATION insode If : "+contestList.size());
		}
		for (QuizContest contest : contestList) {
			
			System.out.println("QUIZ NOTIFICATION Time : "+contest.getContestStartDateTime()+" : "+contest.getContestName());
			logger.info("QUIZ NOTIFICATION Time : "+contest.getContestStartDateTime()+" : "+contest.getContestName());
			
			String notificationMsg = "";
			if(contest.getContestStartDateTime().compareTo(threeMinsDate) == 0) {
				//notificationMsg = contest.getContestName()+" Contest is going to start in Next "+thrreMinstime+" mins.";
				notificationMsg = "Reward The Fan is LIVE in less than FIVE";
				
			} else {
				//notificationMsg = contest.getContestName()+" Contest is going to start in Next "+fifteenMinsTime+" mins.";
				notificationMsg = "Put your hands up, Reward The Fan Is giving away tickets in less than 15 minutes!";
			}
			
			//for (Integer customerOrderId : customerOrderIDs) {
			for (Customer customer : customers) {
				
					List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(customer.getId());
					if(deviceList !=null && !deviceList.isEmpty()){
	
						String jsonString="";
						for (CustomerDeviceDetails device : deviceList) {

							switch (device.getApplicationPlatForm()) {
							
							case ANDROID:
								NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
								notificationJsonObject.setNotificationType(NotificationType.QUIZ_CONTEST_START);
								notificationJsonObject.setMessage(notificationMsg);
								//notificationJsonObject.setOrderId(customerOrder.getId());
								Gson gson = new Gson();	
								jsonString = gson.toJson(notificationJsonObject);
								gcmNotificationService.sendMessage(NotificationType.QUIZ_CONTEST_START, device.getNotificationRegId(), jsonString);
								break;
								
							case IOS:
							    Map<String, String> customFields = new HashMap<String, String>();
							    customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_CONTEST_START));
							    //customFields.put("orderId", String.valueOf(customerOrder.getId()));
								apnsNotificationService.sendNotification(NotificationType.QUIZ_CONTEST_START, device.getNotificationRegId(), notificationMsg,customFields);
								break;
								
			
								default:
									break;
							}
						}
					}
					
				//}
				
			}
		}
		
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			/*if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				init();
				System.out.println("Inside Start Cont Not If : "+ new Date());
			} else {
				System.out.println("Inside Start Cont Not Else : "+ new Date());
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Integer fifteenMinsTime = 3;
		Integer thrreMinstime = 60;
		Calendar fifteenMinscal = Calendar.getInstance();
		fifteenMinscal.set(Calendar.SECOND, 0);
		fifteenMinscal.set(Calendar.MILLISECOND, 0);
		fifteenMinscal.add(Calendar.MINUTE, fifteenMinsTime);
		Date fifteenMinsDate = new Date(fifteenMinscal.getTimeInMillis());
		
		Calendar threeeMinsCal = Calendar.getInstance();
		threeeMinsCal.set(Calendar.SECOND, 0);
		threeeMinsCal.set(Calendar.MILLISECOND, 0);
		threeeMinsCal.add(Calendar.MINUTE, thrreMinstime);
		Date threeMinsDate = new Date(threeeMinsCal.getTimeInMillis());
		
		System.out.println("60: "+threeMinsDate);
		System.out.println("3: "+fifteenMinsDate);
	}
}