package com.rtfquiz.webservices.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.enums.RewardType;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.rtfquiz.webservices.utils.list.CustomerFriendDetails;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.rtfquiz.webservices.utils.list.QuizCustomerCountDetailInfo;
import com.rtfquiz.webservices.utils.list.QuizJoinContestInfo;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.utils.URLUtil;

/**
 * 
 * @author Tamil
 *
 */
public class QuizContestUtilTemp {

	/*static Map<Integer,QuizContest> contestMap = new ConcurrentHashMap<Integer, QuizContest>();*/
	//DB
	/*static Map<Integer,QuizContestQuestions> contestQuestionsMap = new ConcurrentHashMap<Integer, QuizContestQuestions>();*/
	/*static Map<Integer,List<Integer>> customersCountMap = new ConcurrentHashMap<Integer, List<Integer>>();*/
	/*static Map<Integer,Set<Integer>> contestVisitorsCountMap = new ConcurrentHashMap<Integer, Set<Integer>>();*/
	//DB
	/*static  Map<Integer,Map<Integer,QuizCustomerContestAnswers>> customerContestAnsMap = new ConcurrentHashMap<Integer, Map<Integer,QuizCustomerContestAnswers>>();*/
	/*static  Map<Integer,Map<Integer,Double>> customerContestAnswerRewardsMap = new ConcurrentHashMap<Integer, Map<Integer,Double>>();*/
	//static  Map<Integer,Map<Integer,Map<String,Integer>>> questionOptionsCountMap = new ConcurrentHashMap<Integer, Map<Integer,Map<String,Integer>>>();
	/*static  Map<Integer,Map<Integer,Boolean>> contestLifeLineUsageMap = new ConcurrentHashMap<Integer, Map<Integer,Boolean>>();*/
	/*static Map<Integer,Map<Integer, QuizContestWinners>> contestWinnersMap = new ConcurrentHashMap<Integer, Map<Integer, QuizContestWinners>>();*/
	
	/*static List<QuizContestWinners> thisWeekSummaryList = new ArrayList<QuizContestWinners>();*/
	/*static List<QuizContestWinners> tillDateSummaryList = new ArrayList<QuizContestWinners>();*/
	/*static Map<Integer,List<QuizContestWinners>> contestSummaryMap = new ConcurrentHashMap<Integer, List<QuizContestWinners>>();*/
	
	private static Logger log = LoggerFactory.getLogger(QuizContestUtil.class);
	
	static Integer contestSummaryDataSize = 100;
	static Integer contestWinnersSummaryDataSize = 200;

	public static QuizContest getQuizContestByContestId(Integer contestId) {
		//DB
		/*QuizContest contest = contestMap.get(contestId);
		if(contest == null) {
			contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			if(contest != null) {
				contestMap.put(contestId, contest);
			}
		}
		return contest;*/
		return QuizDAORegistry.getQuizContestDAO().get(contestId);
	}
	public static Boolean updateQuizContestMap(QuizContest contest) {
		
		//DB
		/*contestMap.put(contest.getId(), contest);*/
		
		return true;
	}
	public static Boolean updateQuizContest(QuizContest contest) {
		
		//QuizDAORegistry.getQuizContestDAO().update(contest);
		
		//DB
		/*contestMap.put(contest.getId(), contest);*/
		
		return true;
	}
	public static Boolean removeQuizContestMap(QuizContest contest) {
		//DB
		/*contestMap.remove(contest.getId());*/
		
		return true;
	}
	public static void clearCacheDataByContestId(Integer contestId) throws Exception {

		//DB
		/*contestMap.remove(contestId);*/
		/*customersCountMap.remove(contestId);*/
		//DB
		/*customerContestAnsMap.remove(contestId);*/
		/*customerContestAnswerRewardsMap.remove(contestId);*/
		//questionOptionsCountMap.remove(contestId);
		/*contestLifeLineUsageMap.remove(contestId);*/
		/*contestWinnersMap.remove(contestId);*/
		/*contestQuestionsMap.remove(contestId);*/
		/*contestVisitorsCountMap.remove(contestId);*/
		/*contestSummaryMap.remove(contestId);*/
		
	}
	
	public static void refreshCacheDataByContestId(Integer contestId) throws Exception {
		
		//DB
		/*QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
		contestMap.put(contest.getId(), contest);*/
		
		//DB
		/*List<QuizCustomerContestAnswers> answersList = QuizDAORegistry.getQuizQueryManagerDAO().getAllCustomerContestAnswersForCacheRefresh(contestId);
		Map<Integer,QuizCustomerContestAnswers> customerAnswersMap = new HashMap<Integer, QuizCustomerContestAnswers>();
		for (QuizCustomerContestAnswers customerAnswer : answersList) {
			customerAnswersMap.put(customerAnswer.getCustomerId(), customerAnswer);
		}
		customerContestAnsMap.put(contestId,customerAnswersMap);*/
		
		//DB
		/*List<QuizCustomerContestAnswers> answerRewardsList = QuizDAORegistry.getQuizQueryManagerDAO().getAllCustomersContestAnswerRewardsForCacheRefresh(contestId);
		Map<Integer,Double> customerRewardsMap = new HashMap<Integer, Double>();
		for (QuizCustomerContestAnswers customerAnswer : answerRewardsList) {
			customerRewardsMap.put(customerAnswer.getCustomerId(), customerAnswer.getAnswerRewards());
		}
		customerContestAnswerRewardsMap.put(contestId, customerRewardsMap);*/
		
		//DB
		/*List<Integer> lifelineUsedCustoemrs = QuizDAORegistry.getQuizQueryManagerDAO().getAllCustomersLifeLineUsageForCacheRefresh(contestId);
		Map<Integer,Boolean> lifeLineUsageMap = new HashMap<Integer, Boolean>();
		for (Integer customerId : lifelineUsedCustoemrs) {
			lifeLineUsageMap.put(customerId, Boolean.TRUE);
		}
		contestLifeLineUsageMap.put(contestId, lifeLineUsageMap);*/
		
		//DB
		/*List<QuizContestWinners> winners = QuizDAORegistry.getQuizContestWinnersDAO().getContestWinnersByContestId(contestId);
		Map<Integer,QuizContestWinners> winnersMap = new HashMap<Integer, QuizContestWinners>();
		for (QuizContestWinners contestWinner : winners) {
			winnersMap.put(contestWinner.getCustomerId(), contestWinner);
		}
		contestWinnersMap.put(contestId, winnersMap);*/
		
		//DB
		/*if(contest.getLastQuestionNo() != null ) {
			QuizContestQuestions question = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionByContestIdandQuestionSlNo(contestId, contest.getLastQuestionNo());
			if(question != null && !contest.getLastAction().equalsIgnoreCase("QUESTION")) {
				question.setIsAnswerCountComputed(true);
			}
			contestQuestionsMap.put(contestId, question);
		}*/
		
		//DB
		/*List<Integer> visitorsList = DAORegistry.getQueryManagerDAO().getAllContestVisitorsForCacheRefresh(contestId);
		Set<Integer> visitors = new HashSet<Integer>(visitorsList);
		contestVisitorsCountMap.put(contestId, visitors);*/
		
		//DB
		/*List<Integer> customerCountList = DAORegistry.getQueryManagerDAO().getAllContestCustomerCountDetailsForCacheRefresh(contestId);
		customersCountMap.put(contestId, customerCountList);*/
		
		//customersCountMap.remove(contestId);
			//customerContestAnsMap.remove(contestId);
			//customerContestAnswerRewardsMap.remove(contestId);
		//questionOptionsCountMap.remove(contestId);
			//contestLifeLineUsageMap.remove(contestId);
			//contestWinnersMap.remove(contestId);
			//contestQuestionsMap.remove(contestId);
		//contestVisitorsCountMap.remove(contestId);
		
	}
	
	public static QuizJoinContestInfo updateQuizJoinContestCustomersCount(QuizJoinContestInfo quizJoinContestInfo,Integer contestId,Integer customerId) throws Exception {
		try {
		Integer count=0;
		boolean isExistingContestant=false;
		//DB
		/*List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		if(contestCount.contains(customerId)) {
			//isExistingContestant = true;
		} else {
			contestCount.add(customerId);
		}
		count = contestCount.size();
		customersCountMap.put(contestId, contestCount);*/
		
//to capture visitors count		
		//DB
		/*Set<Integer> visitosList = contestVisitorsCountMap.get(contestId);
		if(visitosList == null) {
			visitosList = new HashSet<Integer>();
		}
		if(!visitosList.add(customerId)) {
			isExistingContestant = true;
		}
		contestVisitorsCountMap.put(contestId,visitosList);*/
//end		
		isExistingContestant = QuizDAORegistry.getQuizContestParticipantsDAO().isExistingContestant(customerId, contestId);
		count =  QuizDAORegistry.getQuizQueryManagerDAO().getCustomerCountByContestId(contestId);
		
		quizJoinContestInfo.setTotalUsersCount(count);
		quizJoinContestInfo.setIsExistingContestant(isExistingContestant);
		
		QuizCustomerContestAnswers customerAnswer = getCustomerAnswers(customerId, contestId);
		if(customerAnswer != null) {
			quizJoinContestInfo.setLastAnsweredQuestionNo(customerAnswer.getQuestionSNo());
			quizJoinContestInfo.setIsLastAnswerCorrect(customerAnswer.getIsCorrectAnswer());
		} else {
			quizJoinContestInfo.setLastAnsweredQuestionNo(0);
			quizJoinContestInfo.setIsLastAnswerCorrect(Boolean.FALSE);
		}
		quizJoinContestInfo.setIsContestLifeLineUsed(QuizContestUtilTemp.checkCustomerContestLifeLineUsage(customerId, contestId));
		
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return quizJoinContestInfo;
	}
	public static QuizJoinContestInfo updateQuizExitCustomersCount(QuizJoinContestInfo quizJoinContestInfo,Integer contestId,Integer customerId) throws Exception {
		Integer count=0;
		boolean isExistingContestant=false;
		//DB
		/*List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount == null) {
			contestCount = new ArrayList<Integer>();
		}
		if(contestCount.remove(customerId)) {
			isExistingContestant = true;
		}
		count = contestCount.size();
		customersCountMap.put(contestId, contestCount);*/
		
		//count =  QuizDAORegistry.getQuizQueryManagerDAO().getCustomerCountByContestId(contestId);
		quizJoinContestInfo.setTotalUsersCount(count);
		//quizJoinContestInfo.setIsExistingContestant(isExistingContestant);
		
		/*QuizCustomerContestAnswers customerAnswer = getCustomerAnswers(customerId, contestId);
		if(customerAnswer != null) {
			quizJoinContestInfo.setLastAnsweredQuestionNo(customerAnswer.getQuestionSNo());
			quizJoinContestInfo.setIsLastAnswerCorrect(customerAnswer.getIsCorrectAnswer());
		} else {
			quizJoinContestInfo.setLastAnsweredQuestionNo(0);
			quizJoinContestInfo.setIsLastAnswerCorrect(Boolean.FALSE);
		}
		quizJoinContestInfo.setIsContestLifeLineUsed(QuizContestUtil.checkCustomerContestLifeLineUsage(customerId, contestId));*/
		
		return quizJoinContestInfo;
	}
	public static Integer getContestCustomersCount(Integer contestId) throws Exception {
		//DB
		/*Integer count=0;
		List<Integer> contestCount = customersCountMap.get(contestId);
		if(contestCount != null) {
			count = contestCount.size();
		}
		
		return count;*/
		
		Integer count = QuizDAORegistry.getQuizQueryManagerDAO().getCustomerCountByContestId(contestId);
		return count;
		
	}
	public static Boolean isExistingContestant(Integer contestId,Integer customerId) throws Exception {
		//DB
		/*boolean isExistingcontestant = false;
		Set<Integer> visitorsList = contestVisitorsCountMap.get(contestId);
		if(visitorsList != null && visitorsList.contains(customerId)) {
			isExistingcontestant = true; 
		}*/
		boolean isExistingcontestant = QuizDAORegistry.getQuizContestParticipantsDAO().isExistingContestant(customerId, contestId);
		
		return isExistingcontestant;
	}
	public static QuizCustomerContestAnswers getCustomerAnswers(Integer customerId,Integer contestId) throws Exception {
		//DB
		/*Map<Integer,QuizCustomerContestAnswers> customerAnswersMap = customerContestAnsMap.get(contestId);
		if(customerAnswersMap == null) {
			return null;
		}
		return customerAnswersMap.get(customerId);*/
		
		return QuizDAORegistry.getQuizCustomerContestAnswersDAO().getLatestContestAnswerByCustomerIdandContestId(customerId, contestId);
		
	}
	//DB
	/*public static Boolean updateContestQuestions(QuizContestQuestions contestQuestion) throws Exception {

		contestQuestionsMap.put(contestQuestion.getContestId(), contestQuestion);
		
		return true;
	}*/
	public static QuizContestQuestions getContestCurrentQuestions(Integer contestId) throws Exception {
		//DB
		//QuizContestQuestions questions = contestQuestionsMap.get(contestId);
		//return questions;
		QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
		if(contest.getLastQuestionNo() != null ) {
			QuizContestQuestions question = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionByContestIdandQuestionSlNo(contestId, contest.getLastQuestionNo());
			if(question != null && !contest.getLastAction().equalsIgnoreCase("QUESTION")) {
				question.setIsAnswerCountComputed(true);
			}
			return question;
		} 
		return null;
	}
	
	//DB
	/*public static Boolean updateCustomerAnswerMap(QuizCustomerContestAnswers customerAnswers) throws Exception {
	
		Map<Integer,QuizCustomerContestAnswers> customerAnswersMap = customerContestAnsMap.get(customerAnswers.getContestId());
		if(customerAnswersMap == null) {
			customerAnswersMap = new ConcurrentHashMap<Integer, QuizCustomerContestAnswers>();
		}
		customerAnswersMap.put(customerAnswers.getCustomerId(), customerAnswers);
		customerContestAnsMap.put(customerAnswers.getContestId(),customerAnswersMap);
		
		return true;
	}*/
	public static Boolean updateCustomerAnswers(QuizCustomerContestAnswers customerAnswers) throws Exception {
		boolean flag=true;
		//DB
		/*Map<Integer,QuizCustomerContestAnswers> customerAnswersMap = customerContestAnsMap.get(customerAnswers.getContestId());
		if(customerAnswersMap == null) {
			customerAnswersMap = new ConcurrentHashMap<Integer, QuizCustomerContestAnswers>();
		}*/
		
		
		/*QuizCustomerContestAnswers custoemrAnsMapObj = customerAnswersMap.get(customerAnswers.getCustomerId());
		if(custoemrAnsMapObj != null) {
			if(custoemrAnsMapObj.getQuestionSNo() >= customerAnswers.getQuestionSNo()) {
				return false;
			}
		} else if(customerAnswers.getQuestionSNo() > 1) {
			return false;
		}*/
		
		//DB
		/*customerAnswersMap.put(customerAnswers.getCustomerId(), customerAnswers);
		customerContestAnsMap.put(customerAnswers.getContestId(),customerAnswersMap);*/
		
		//DB
		//capture customer contest answer rewards
		/*if(customerAnswers.getAnswerRewards() != null && customerAnswers.getAnswerRewards() > 0) {
			Map<Integer,Double> customerRewardsMap = customerContestAnswerRewardsMap.get(customerAnswers.getContestId());
			if(customerRewardsMap == null) {
				customerRewardsMap = new ConcurrentHashMap<Integer, Double>();
			}
			Double rewards = customerRewardsMap.get(customerAnswers.getCustomerId());
			if(rewards == null) {
				rewards = 0.0;
			}
			rewards = rewards + customerAnswers.getAnswerRewards();
			rewards = TicketUtil.getNormalRoundedValue(rewards);
			customerRewardsMap.put(customerAnswers.getCustomerId(), rewards);
			customerContestAnswerRewardsMap.put(customerAnswers.getContestId(),customerRewardsMap);
		}*/
		
		
		//Capture each questions correct and wrong answers count
		/*Map<Integer,Map<String,Integer>> contestCountMap = questionOptionsCountMap.get(customerAnswers.getContestId());
		if(contestCountMap == null) {
			contestCountMap = new ConcurrentHashMap<Integer, Map<String,Integer>>();
		}
		Map<String,Integer> questionCountMap = contestCountMap.get(customerAnswers.getQuestionSNo());
		if(questionCountMap == null) {
			questionCountMap = new ConcurrentHashMap<String, Integer>();
		}
		Integer count = questionCountMap.get(customerAnswers.getAnswer());
		if(count == null) {
			count=0;
		}
		count++;
		questionCountMap.put(customerAnswers.getAnswer(), count);
		contestCountMap.put(customerAnswers.getQuestionSNo(), questionCountMap);
		questionOptionsCountMap.put(customerAnswers.getContestId(), contestCountMap);*/
		
		return flag;
	}
	public static Double getCustomerContestAnswerRewards(Integer contestId,Integer customerId) throws Exception {
		/*Map<Integer,Double> customerRewardsMap = customerContestAnswerRewardsMap.get(contestId);
		if(customerRewardsMap == null) {
			customerRewardsMap = new ConcurrentHashMap<Integer, Double>();
		}
		Double rewards = customerRewardsMap.get(customerId);
		if(rewards == null) {
			rewards = 0.0;
		}
		return rewards;*/
		double rewards = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getCustomerContestAnswerRewards(customerId, contestId);
		return rewards;
	}
	public static QuizCustomerCountDetailInfo getCustomerCountDetailsInfo(Integer contestId,QuizCustomerCountDetailInfo customerCountDetail,Integer pageNo,Integer maxRows) throws Exception {
		
		Integer totalCount=0;
		Boolean hasMoreCustomers = false;
		List<CustomerSearchDetails> customersList = null;
		
		//DB
		/*List<Integer> customerIdList = null;
		List<Integer> contestCountList = customersCountMap.get(contestId);
		if(contestCountList != null) {
			totalCount = contestCountList.size();
			Integer fromIndex = maxRows*(pageNo - 1);
			Integer toIndex = maxRows*pageNo;
			if(totalCount > fromIndex) {
				if(totalCount <= toIndex) {
					toIndex = totalCount;
				} else {
					hasMoreCustomers = true;
				}
				customerIdList = contestCountList.subList(fromIndex, toIndex);
			}
			if(customerIdList != null) {
				CustomerSearchDetails customerDtl = null;
				customersList = new ArrayList<CustomerSearchDetails>();
				for (Integer customerId : customerIdList) {
					Customer customer = CustomerUtil.getCustomerById(customerId);
					if(customer != null) {
						customerDtl = new CustomerSearchDetails(customer);
						customersList.add(customerDtl);
					}
				}
			}
		}*/
		customersList = QuizDAORegistry.getQuizQueryManagerDAO().getCustomerCountDetailsByContestIdandPageNo(contestId, pageNo, maxRows);
		if(customersList != null ) { 
			if(customersList.size() >= maxRows) {
				hasMoreCustomers = true;
			}
			totalCount = customersList.size();
		}
		
		customerCountDetail.setTotalCount(totalCount);
		customerCountDetail.setHasMoreCustomers(hasMoreCustomers);
		customerCountDetail.setSearchResults(customersList);
		
		return customerCountDetail;
	}
	public static Boolean updateCustomerContestDetails(Integer contestId) throws Exception {

		QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
		if(!contest.getIsCustomerStatsUpdated()) {
			Date fromDate = new Date();
			Date start = new Date();
			Date startOne = new Date();
			QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestDataByContestId(contestId);
			log.info("Time to Update Cust COntest Data : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
			
			startOne = new Date();
			QuizDAORegistry.getQuizQueryManagerDAO().updateAllCustomersContestWinsDataByContestId(contestId);
			log.info("Time to Update Cust COntest Wins Data : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
			
			startOne = new Date();
			//QuizDAORegistry.getQuizSummaryManagerDAO().updateCustomerPromoCodeAndContestORderStats();
			QuizCustomerPromocodeandContestOrderStatsScheduler.processCustomerPromoCodeAndContestOrderStats();
			log.info("Time to Update Cust PRomo and co stats Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
			
			startOne = new Date();
			updateContestRewardsToCustomer(contestId);
			log.info("Time to Update CONT REWARDS to Customer : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
			
			startOne = new Date();
			CustomerUtil.updateCustomerUtilForContestCustomers(contestId, fromDate);
			log.info("Time to Update CUST UTIL for STATS : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
			
			try {
				startOne = new Date();
			//QuizContestUtil.forceSummaryRefreshTable();
			//QuizContestUtil.refreshSummaryDataTable();
			//QuizContestUtil.refreshSummaryDataCache();
			QuizContestUtilTemp.forceManualefreshSummaryDataTable();
			log.info("Time to Refresh Summary Table : "+(new Date().getTime()-startOne.getTime())+" : contestId : "+contestId+" : "+new Date());
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			log.info("Time to Update ALL CONTEST STATS : "+(new Date().getTime()-start.getTime())+" : contestId : "+contestId+" : "+new Date());
			
			contest.setIsCustomerStatsUpdated(true);
			QuizDAORegistry.getQuizContestDAO().update(contest);
			return true;
		}
		return false;
	}
	public static Boolean updateContestRewardsToCustomer(Integer contestId) throws Exception {
		
		List<QuizContestWinners> customerRewardsList = QuizDAORegistry.getQuizQueryManagerDAO().getCustomerContestRewardsForUpdate(contestId);
		if(customerRewardsList != null) {
			for (QuizContestWinners quizContestWinners : customerRewardsList) {
				CustomerLoyaltyTracking tracking = new CustomerLoyaltyTracking();
				tracking.setCustomerId(quizContestWinners.getCustomerId());
				tracking.setRewardPoints(quizContestWinners.getRewardPoints());
				tracking.setRewardType("CONTEST");
				tracking.setContestId(contestId);
				tracking.setCreatedDate(new Date());
				tracking.setCreatedBy("AUTO");
				System.out.println("Contest Reward Credit : "+quizContestWinners.getCustomerId()+" :coI: "+quizContestWinners.getContestId()+" : "+tracking.getContestId()+" : "+new Date());
				
				DAORegistry.getCustomerLoyaltyTrackingDAO().save(tracking);
				DAORegistry.getCustomerLoyaltyDAO().updateCustomerLoyaltyByCustomerForManualCredits(quizContestWinners.getRewardPoints(), quizContestWinners.getCustomerId());
			}
		}
		
		return true;
	}
	
	/*public static QuizCustomerCountDetailInfo getCustomerCountDetailsInfoOld(Integer contestId,Integer questionNo,QuizCustomerCountDetailInfo customerCountDetail) throws Exception {
		
		Integer optionACount = 0,optionBCount=0,optionCCount=0;
		Integer firstQuestCounts = 0,playingCount=0,eliminatedCount=0;
		Map<Integer,Map<String,Integer>> contestCountMap = questionOptionsCountMap.get(contestId);
		if(contestCountMap != null) {
			Map<String,Integer> questioncountMap = contestCountMap.get(1);
			for (Integer optionCount : questioncountMap.values()) {
				firstQuestCounts += optionCount;
			}
			if(firstQuestCounts > 0 && questionNo > 1) {
				Integer previousQuestNo = questionNo - 1;
				QuizContestQuestions contestQuestions = QuizDAORegistry.getQuizContestQuestionsDAO().getQuizContestQuestionByContestIdandQuestionSlNo(contestId, previousQuestNo);
				questioncountMap = contestCountMap.get(previousQuestNo);
				if(questioncountMap != null) {
					Integer tempCount  = questioncountMap.get(contestQuestions.getAnswer());
					if(tempCount != null) {
						playingCount = tempCount;
					}
				}
				eliminatedCount = firstQuestCounts - playingCount;
			} else {
				playingCount = getContestCustomersCount(contestId);
				eliminatedCount = 0;
				
			}
		} else {
			playingCount = getContestCustomersCount(contestId);
			eliminatedCount = 0;
		}
		customerCountDetail.setPlayingCount(playingCount);
		customerCountDetail.setEliminatedCount(eliminatedCount);
		
		return customerCountDetail;
	}*/
	
	//DB
	/*public static Boolean updateCustomerLifeLineUsage(QuizCustomerContestAnswers customerAnswers) throws Exception {
		boolean flag=true;
		Map<Integer,Boolean> customerLifeLineMap = contestLifeLineUsageMap.get(customerAnswers.getContestId());
		if(customerLifeLineMap == null) {
			customerLifeLineMap = new ConcurrentHashMap<Integer, Boolean>();
		}
		customerLifeLineMap.put(customerAnswers.getCustomerId(), customerAnswers.getIsLifeLineUsed());
		contestLifeLineUsageMap.put(customerAnswers.getContestId(),customerLifeLineMap);
		
		return flag;
	}*/
	public static Boolean checkCustomerContestLifeLineUsage(Integer customerId,Integer contestId) throws Exception {
		/*boolean flag=true;
		Map<Integer,Boolean> customerLifeLineMap = contestLifeLineUsageMap.get(contestId);
		if(customerLifeLineMap == null || customerLifeLineMap.get(customerId) == null) {
			return false;
		}
		return flag;*/
		Boolean flag = QuizDAORegistry.getQuizCustomerContestAnswersDAO().checkCustomerContestLifeLineUsageByCustomerIdandContestId(customerId, contestId);
		return flag;
	}
	
	public static Boolean updateContestWinners(Integer contestId,Integer customerId) {
		
		/*QuizContest contest = getQuizContestByContestId(contestId);
		if(contest == null || contest.getProcessStatus() != null) {
			return false;
		}*/
		QuizContestWinners contestWinner = new QuizContestWinners();
		contestWinner.setCustomerId(customerId);
		contestWinner.setContestId(contestId);
		contestWinner.setCreatedDateTime(new Date());
		
		contestWinner.setRewardTickets(0);
		contestWinner.setRewardType(RewardType.POINTS);
		contestWinner.setRewardPoints(0.0);
		contestWinner.setRewardRank(0);
		
		
		QuizDAORegistry.getQuizContestWinnersDAO().save(contestWinner);

		//DAORegistry.getCustomerDAO().update(customer);
		//CustomerUtil.updatedCustomerUtil(customer);
		
		//DB
		/*Map<Integer,QuizContestWinners> contestwinnersMap = contestWinnersMap.get(contestId);
		if(contestwinnersMap == null) {
			contestwinnersMap = new ConcurrentHashMap<Integer,QuizContestWinners>();
		}
		contestwinnersMap.put(customerId,contestWinner);
		contestWinnersMap.put(contestId, contestwinnersMap);*/
		
		//contestMap.put(contestId, contest);
		
		return true;
	}
	public static Double updateContestWinnersRewadPoints(Integer contestId,Integer customerId) {
		
		Double rewardsPerWinner = 0.0;
		List<QuizContestWinners> winners = QuizDAORegistry.getQuizContestWinnersDAO().getContestWinnersByContestId(contestId);
		Map<Integer,QuizContestWinners> contestwinnersMap = new HashMap<Integer, QuizContestWinners>();
		for (QuizContestWinners contestWinner : winners) {
			contestwinnersMap.put(contestWinner.getCustomerId(), contestWinner);
		}
		//DB
		/*Map<Integer,QuizContestWinners> contestwinnersMap = contestWinnersMap.get(contestId);*/
		if(contestwinnersMap != null && contestwinnersMap.size() > 0) {
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			List<QuizContestWinners> winnersList = new ArrayList<QuizContestWinners>(contestwinnersMap.values()); 
			Integer winnersCount = contestwinnersMap.size();
			Double contestTotalRewards = contest.getTotalRewards();
			
			rewardsPerWinner = Math.floor((contestTotalRewards/winnersCount)*100)/100;
			for (QuizContestWinners quizContestWinners : winnersList) {
				quizContestWinners.setRewardPoints(rewardsPerWinner);
			}
			QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(winnersList);
			contest.setPointsPerWinner(rewardsPerWinner);
			
			QuizDAORegistry.getQuizContestDAO().update(contest);
		}
		
		return rewardsPerWinner;
	}
	public static Double getContestWinnerRewardPoints(Integer contestId) {
		Double rewardPoints =0.0;
		try {
			rewardPoints = QuizDAORegistry.getQuizQueryManagerDAO().getContestWinnerRewardPoints(contestId);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return rewardPoints;
	}

	public static List<QuizContestWinners> computeContestGrandWinners(Integer contestId) {
		
		List<QuizContestWinners> grandWinnersList = new ArrayList<QuizContestWinners>();
		Date start = new Date();
		long process =0,postPros=0,dbUpdate=0,finalPros=0;
		//DB
		/*Map<Integer,QuizContestWinners> contestwinnersMap = contestWinnersMap.get(contestId);*/
		List<QuizContestWinners> winners = QuizDAORegistry.getQuizContestWinnersDAO().getContestWinnersByContestId(contestId);
		Map<Integer,QuizContestWinners> contestwinnersMap = new HashMap<Integer, QuizContestWinners>();
		for (QuizContestWinners contestWinner : winners) {
			contestwinnersMap.put(contestWinner.getCustomerId(), contestWinner);
		}
		
		if(contestwinnersMap != null && contestwinnersMap.size() > 0) {
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			List<QuizContestWinners> winnersList = new ArrayList<QuizContestWinners>(contestwinnersMap.values()); 
			
			Integer winnersCount = winnersList.size();
			Integer grandwinnersCount = contest.getMaxFreeTicketWinners();
			
			if(winnersCount >= grandwinnersCount) {
				
				String userId = ",";
				for (QuizContestWinners winner : winnersList) {
					userId = userId+","+winner.getCustomerId();
				}
				
				System.out.println("QUIZWINNER RANDOM: WINNER USERIDS->"+userId);
				
				
				Set<Integer> indexList = new HashSet<Integer>();
				for(int i=0;i<grandwinnersCount;i++) {
					Boolean flag = true;
					while(flag) {
						Random random = new Random();
						
						int index = random.nextInt(winnersCount);
						
						System.out.println(winnersCount+"===QUIZWINNER RANDOM->"+index);
						
						//int index = random.nextInt((winnersCount - 1) + 1) + 1;
						if(indexList.add(index)) {
							
							QuizContestWinners quizContestWinners = winnersList.get(index);
							
							System.out.println("QUIZWINNER RANDOM: FINAL WINNER USERID->"+quizContestWinners.getCustomerId());
							grandWinnersList.add(quizContestWinners);
							flag = false;
						}
					}
				}
			} else {
				grandWinnersList.addAll(winnersList);
			}
			process = (new Date().getTime()-start.getTime());
			Integer expairyDays = 1;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, expairyDays);
			Date expiryDate = new Date(cal.getTimeInMillis());
			
			List<ContestGrandWinner> grandWinners = new ArrayList<ContestGrandWinner>();
			for (QuizContestWinners quizContestWinners : grandWinnersList) {
				quizContestWinners.setRewardTickets(contest.getFreeTicketsPerWinner());
				
				ContestGrandWinner grandWinner = new ContestGrandWinner();
				grandWinner.setCustomerId(quizContestWinners.getCustomerId());
				grandWinner.setContestId(quizContestWinners.getContestId());
				grandWinner.setRewardTickets(contest.getFreeTicketsPerWinner());
				grandWinner.setStatus(WinnerStatus.ACTIVE);
				grandWinner.setCreatedDate(new Date());
				grandWinner.setExpiryDate(expiryDate);
				
				grandWinners.add(grandWinner);
			}
			postPros = new Date().getTime()-(start.getTime()+process);
			QuizDAORegistry.getQuizContestWinnersDAO().saveOrUpdateAll(grandWinnersList);
			QuizDAORegistry.getContestGrandWinnerDAO().saveAll(grandWinners);

			dbUpdate = new Date().getTime()-(start.getTime()+process+postPros);
			//contest.setTicketWinnersCount(grandWinnersList.size());
			//QuizDAORegistry.getQuizContestDAO().update(contest);
		}
		
		int rank =1;
		for (QuizContestWinners contestWinner : grandWinnersList) {
			Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
			if(customer != null) {
				contestWinner.setUserId(customer.getUserId());
				contestWinner.setCustomerName(customer.getCustomerName());
				contestWinner.setCustomerLastName(customer.getLastName());
				contestWinner.setCustomerId(customer.getId());
				
				if(customer.getCustImagePath() != null) {
					contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
				} else {
					contestWinner.setProfilePicWV(null);
				}
			}
			contestWinner.setRewardRank(0);
			rank++;
		}
		finalPros = new Date().getTime()-(start.getTime()+process+postPros+dbUpdate);
		System.out.println("Grand Winner Compute : "+contestId+" :process: "+process+" :postPros: "+postPros+" :dbUpdate: "+dbUpdate+" :final: "+finalPros+" : tot : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return grandWinnersList;
	}
	public static List<QuizContestWinners> getContestGrandWinners(Integer contestId) {
		
		List<QuizContestWinners> list =   QuizDAORegistry.getQuizContestWinnersDAO().getQuizContestGrandWinnersByContestId(contestId);
		for (QuizContestWinners contestWinner : list) {
			Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
			contestWinner.setUserId(customer.getUserId());
			//contestWinner.setCustomerName(customer.getCustomerName());
			//contestWinner.setCustomerLastName(customer.getLastName());
			contestWinner.setCustomerId(customer.getId());
			
			if(customer.getCustImagePath() != null) {
				contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
			} else {
				contestWinner.setProfilePicWV(null);
			}
		}
		return list;
	}
	/*public static synchronized Boolean updateContestWinnersOld(Integer contestId,Integer customerId) {
		QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
		Customer customer = CustomerUtil.getCustomerById(customerId);
		
		QuizContestWinners contestWinner = new QuizContestWinners();
		contestWinner.setCustomerId(customerId);
		contestWinner.setContestId(contest.getId());
		contestWinner.setCreatedDateTime(new Date());

		if(contest.getMaxFreeTicketWinners()> contest.getTicketWinnersCount()) {
			contestWinner.setRewardTickets(contest.getFreeTicketsPerWinner());
			contestWinner.setRewardType(RewardType.TICKETS);
			contestWinner.setRewardPoints(0.0);
			contest.setTicketWinnersCount(contest.getTicketWinnersCount() + 1);
			customer.setQuizNoOfTicketsWon(customer.getQuizNoOfTicketsWon() + contest.getFreeTicketsPerWinner());
		} else {
			contestWinner.setRewardPoints(contest.getPointsPerWinner().doubleValue());
			contestWinner.setRewardType(RewardType.POINTS);
			contestWinner.setRewardTickets(0);
			contest.setPointWinnersCount(contest.getPointWinnersCount() + 1);
			customer.setQuizNoOfPointsWon(customer.getQuizNoOfPointsWon() + contest.getPointsPerWinner());
		}
		contest.setWinnersCount(contest.getWinnersCount()+1);
		contestWinner.setRewardRank(contest.getWinnersCount());
		
		
		QuizDAORegistry.getQuizContestDAO().update(contest);
		QuizDAORegistry.getQuizContestWinnersDAO().save(contestWinner);

		DAORegistry.getCustomerDAO().update(customer);
		CustomerUtil.updatedCustomerUtil(customer);
		
		Set<Integer> contestwinners = contestWinnersMap.get(contestId);
		if(contestwinners == null) {
			contestwinners = new HashSet<Integer>();
		}
		contestwinners.add(customerId);
		contestWinnersMap.put(contestId, contestwinners);
		
		contestMap.put(contestId, contest);
		
		return true;
	}*/
	public static List<QuizContestWinners> getQuizContestWinnersByContestId(Integer contestId){
		try {
			List<QuizContestWinners> contestWinners = QuizDAORegistry.getQuizQueryManagerDAO().getContestWinnersByContestIdOrderByUserId(contestSummaryDataSize,contestId,null);
			if(contestWinners != null) {
				int rank =1;
				//for (QuizContestWinners contestWinner : contestWinners) {
					/*Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
					if(customer != null) {
						contestWinner.setUserId(customer.getUserId());
						contestWinner.setCustomerName(customer.getCustomerName());
						contestWinner.setCustomerLastName(customer.getLastName());
						contestWinner.setCustomerId(customer.getId());
						
						if(customer.getCustImagePath() != null) {
							contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
						} else {
							contestWinner.setProfilePicWV(null);
						}
					}*/
					//contestWinner.setRewardRank(rank);
					//rank++;
				//}
			}
			return contestWinners;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static QuizContestWinners getQuizContestWinnerByCustomerIdAndContestId(Integer customerId,Integer contestId){
		try {
			QuizContestWinners contestWinner = null;
			List<QuizContestWinners> contestWinners = QuizDAORegistry.getQuizQueryManagerDAO().getContestWinnersByContestId(contestSummaryDataSize,contestId,customerId);
			if(contestWinners != null && contestWinners.size() > 0) {
				contestWinner = contestWinners.get(0);
			}
			if(contestWinner == null) {
				contestWinner = new QuizContestWinners();
				contestWinner.setRewardPoints(0.0);
				contestWinner.setRewardTickets(0);
				contestWinner.setRewardRank(0);
				contestWinner.setCustomerId(customerId);
				contestWinner.setContestId(contestId);
				
				Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
				if(customer != null) {
					contestWinner.setUserId(customer.getUserId());
					//contestWinner.setCustomerName(customer.getCustomerName());
					//contestWinner.setCustomerLastName(customer.getLastName());
					
					if(customer.getCustImagePath() != null) {
						contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
					} else {
						contestWinner.setProfilePicWV(null);
					}
				}
			}
			return contestWinner;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<QuizContestWinners> getQuizContestWinnersByTillDate(){
		try {
			//List<QuizContestWinners> contestWinners = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDate(contestSummaryDataSize);
			List<QuizContestWinners> contestWinners = getSummaryForTillDateData();
			if(contestWinners != null) {
				int rank = 1;
				//for (QuizContestWinners contestWinner : contestWinners) {
					/*Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
					if(customer != null) {
						contestWinner.setUserId(customer.getUserId());
						contestWinner.setCustomerName(customer.getCustomerName());
						contestWinner.setCustomerLastName(customer.getLastName());
						contestWinner.setCustomerId(customer.getId());
						
						if(customer.getCustImagePath() != null) {
							contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
						} else {
							contestWinner.setProfilePicWV(null);
						}
					}*/
					//contestWinner.setRewardRank(rank);
					//rank++;
				//}
			}
			return contestWinners;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static QuizContestWinners getQuizContestWinnersByTillDateAndCustomerId(Integer customerId){
		try {
			QuizContestWinners contestWinner = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDateAndCustomerId(customerId);
			if(contestWinner == null) {
				contestWinner = new QuizContestWinners();
				contestWinner.setRewardPoints(0.0);
				contestWinner.setRewardTickets(0);
				contestWinner.setRewardRank(0);
				contestWinner.setCustomerId(customerId);
				
				Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
				if(customer != null) {
					contestWinner.setUserId(customer.getUserId());
					//contestWinner.setCustomerName(customer.getCustomerName());
					//contestWinner.setCustomerLastName(customer.getLastName());
					
					if(customer.getCustImagePath() != null) {
						contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
					} else {
						contestWinner.setProfilePicWV(null);
					}
				}
			}
			return contestWinner;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<QuizContestWinners> getQuizContestWinnersByThisWeek(String fromDateStr,String toDateStr){
		try {
			//List<QuizContestWinners> contestWinners = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize,fromDateStr, toDateStr, null);
			List<QuizContestWinners> contestWinners = getSummaryForThisWeekData();
			if(contestWinners != null) {
				int rank = 1;
				//for (QuizContestWinners contestWinner : contestWinners) {
					/*Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
					if(customer != null) {
						contestWinner.setUserId(customer.getUserId());
						contestWinner.setCustomerName(customer.getCustomerName());
						contestWinner.setCustomerLastName(customer.getLastName());
						contestWinner.setCustomerId(customer.getId());
						
						if(customer.getCustImagePath() != null) {
							contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
						} else {
							contestWinner.setProfilePicWV(null);
						}
					}*/
					//contestWinner.setRewardRank(rank);
					//rank++;
				//}
			}
			return contestWinners;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static QuizContestWinners getQuizContestWinnersByThisWeekAndCustomerId(String fromDateStr,String toDateStr,Integer customerId){
		try {
			QuizContestWinners contestWinner = null;
			List<QuizContestWinners> contestWinners = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize,fromDateStr, toDateStr, customerId);
			if(contestWinners != null && contestWinners.size() > 0) {
				contestWinner = contestWinners.get(0);
			}
			if(contestWinner == null) {
				contestWinner = new QuizContestWinners();
				contestWinner.setRewardPoints(0.0);
				contestWinner.setRewardTickets(0);
				contestWinner.setRewardRank(0);
				contestWinner.setCustomerId(customerId);
				
				Customer customer = CustomerUtil.getCustomerById(contestWinner.getCustomerId());
				if(customer != null) {
					contestWinner.setUserId(customer.getUserId());
					//contestWinner.setCustomerName(customer.getCustomerName());
					//contestWinner.setCustomerLastName(customer.getLastName());
						
					if(customer.getCustImagePath() != null) {
						contestWinner.setProfilePicWV(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),ApplicationPlatForm.ANDROID));
					} else {
						contestWinner.setProfilePicWV(null);
					}
				}
			}
				
			
			return contestWinner;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<CustomerFriendDetails> getCustomerFriendsList(Integer customerId) throws Exception {
		
		return DAORegistry.getQueryManagerDAO().getCustomerFriends(customerId);
		
	}
	public static boolean refreshSummaryDataCache()  {
		try {
			Date start = new Date();
			
			refreshSummaryTillDateData();
			refreshSummaryForThisWeekData();
			System.out.println("REFRESH SUMMARY CACHE : "+(new Date().getTime()-start.getTime())+" : "+ new Date());
			
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean refreshSummaryDataTable()  {
		try {
			//Integer tillDateCount = QuizDAORegistry.getQuizQueryManagerDAO().updateContestSummaryTillDateDataTable();
			//Integer thisWeek = QuizDAORegistry.getQuizQueryManagerDAO().updateContestSummaryThisWeekDataTable();
			Date start = new Date();
			Integer tillDateCount = QuizDAORegistry.getQuizSummaryManagerDAO().updateContestSummaryTillDateDataTable();
			Integer thisWeek = QuizDAORegistry.getQuizSummaryManagerDAO().updateContestSummaryThisWeekDataTable();
			
			System.out.println("tilldateCount : "+tillDateCount+" :thisWeekCount :  "+thisWeek+" : "+(new Date().getTime()-start.getTime())+" : "+ new Date());
			
			//refreshSummaryTillDateData();
			//refreshSummaryForThisWeekData();
			//System.out.println("tilldateCount : "+tillDateCount+" :thisWeekCount :  "+thisWeek+" : "+ new Date());
			
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean forceManualefreshSummaryDataTable()  {
		try {
			//Integer tillDateCount = QuizDAORegistry.getQuizQueryManagerDAO().updateContestSummaryTillDateDataTable();
			//Integer thisWeek = QuizDAORegistry.getQuizQueryManagerDAO().updateContestSummaryThisWeekDataTable();
			Date start = new Date();
			Integer tillDateCount = QuizDAORegistry.getQuizSummaryManagerDAO().updateContestSummaryTillDateDataTable();
			Integer thisWeek = QuizDAORegistry.getQuizSummaryManagerDAO().updateContestSummaryThisWeekDataTable();
			
			System.out.println("tilldateCount : "+tillDateCount+" :thisWeekCount :  "+thisWeek+" : "+(new Date().getTime()-start.getTime())+" : "+ new Date());
			
			refreshSummaryTillDateDataFromTable();
			refreshSummaryForThisWeekDataFromTable();
			System.out.println("tilldateCount : "+tillDateCount+" :thisWeekCount :  "+thisWeek+" : "+ new Date());
			
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static List<QuizContestWinners> getSummaryForThisWeekData()  {
		try {
			//DB
			/*if(thisWeekSummaryList == null || thisWeekSummaryList.isEmpty()) {
				refreshSummaryForThisWeekData();
			}
			//return  new ArrayList<QuizContestWinners>(thisWeekSummaryList);
			return thisWeekSummaryList;*/
			
			//return QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDateFromView(contestSummaryDataSize, null, null, null);
			return QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize, null, null, null);
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static List<QuizContestWinners> getSummaryForTillDateData()  {
		try {
			//DB
			/*if(tillDateSummaryList == null || tillDateSummaryList.isEmpty()) {
				refreshSummaryTillDateData();
			}
			//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
			return tillDateSummaryList;
			*/
			//return QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDateFromView(contestSummaryDataSize);
			return QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDate(contestSummaryDataSize);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<QuizContestWinners> getContestSummaryData(Integer contestId)  {
		try {
			//DB
			/*List<QuizContestWinners> contestSummaryList = contestSummaryMap.get(contestId);
			if(contestSummaryList == null || contestSummaryList.isEmpty()) {
				refreshContestSummaryData(contestId);
				
				contestSummaryList = contestSummaryMap.get(contestId);
			}
			//return  new ArrayList<QuizContestWinners>(tillDateSummaryList);
			return contestSummaryList;*/
			
			return QuizDAORegistry.getQuizQueryManagerDAO().getContestWinnersByContestIdOrderByUserId(contestSummaryDataSize,contestId,null);
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean refreshContestSummaryData(Integer contestId)  {
		try {
			//DB
			//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize, null, null, null);
			/*List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getContestWinnersByContestIdOrderByUserId(contestSummaryDataSize,contestId,null);
			if(winnersList != null) {
				contestSummaryMap.put(contestId,winnersList);
			}*/
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean refreshSummaryForThisWeekData()  {
		try {
			//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize, null, null, null);
			
			//DB
			/*List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDateFromView(contestSummaryDataSize, null, null, null);
			if(winnersList != null) {
				thisWeekSummaryList = new ArrayList<QuizContestWinners>(winnersList);
			}*/
			
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static boolean refreshSummaryTillDateData()  {
		try {
			//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDate(contestSummaryDataSize);
			//DB
			/*List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDateFromView(contestSummaryDataSize);
			if(winnersList != null) {
				tillDateSummaryList = new ArrayList<QuizContestWinners>(winnersList);
			}*/
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean refreshSummaryForThisWeekDataFromTable()  {
		try {
			//DB
			/*List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDate(contestSummaryDataSize, null, null, null);
			//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByDateFromView(contestSummaryDataSize, null, null, null);
			if(winnersList != null) {
				thisWeekSummaryList = new ArrayList<QuizContestWinners>(winnersList);
			}*/
			
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static boolean refreshSummaryTillDateDataFromTable()  {
		try {
			//DB
			/*List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDate(contestSummaryDataSize);
			//List<QuizContestWinners> winnersList = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDateFromView(contestSummaryDataSize);
			if(winnersList != null) {
				tillDateSummaryList = new ArrayList<QuizContestWinners>(winnersList);
			}*/
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void updateCustomerProfileImages() throws Exception { 
		List<Integer> testCustomerIDs = DAORegistry.getQueryManagerDAO().getAllTestCustomerIds();
		if(testCustomerIDs != null) {
			int count=0;
			System.out.println("Test Customer IDs : "+testCustomerIDs.size());
			for (Integer customerId : testCustomerIDs) {
				Customer customer = CustomerUtil.getCustomerById(customerId);
				if(customer != null ) {
					count++;
					String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
					customer.setCustImagePath(fileName);
					DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
					CustomerUtil.updatedCustomerUtil(customer);
				}
			}
			System.out.println("Test Account Created : "+testCustomerIDs.size()+" : created : "+count);
		}
	}
	public static boolean forceSummaryRefreshTable() {
		forceSummaryDataRefresh = true;
		return true;
	}
	
	private static boolean forceSummaryDataRefresh = true;
	private Timer timer = new Timer();
	public void afterPropertiesSet() throws Exception {
		Calendar cal = Calendar.getInstance();
		timer.scheduleAtFixedRate(new SummaryDataRefresh(), cal.getTime(), 30L * 1000L);
	}
	
	public class SummaryDataRefresh extends TimerTask {
		public synchronized void run() {
			if(forceSummaryDataRefresh) {
				try {
					Date start = new Date();
					QuizContestUtilTemp.refreshSummaryDataTable();
					
					System.out.println("QUIZ Summary Refresh : "+(new Date().getTime()-start.getTime())+" : time : "+new Date());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				forceSummaryDataRefresh = false;
			}
		}
	}
	
	public static String computeDaysHoursMinutes(Date startDate,Date endDate) {
		String text = "";
		
		long secs = (endDate.getTime() - startDate.getTime()) / 1000;
		long days = secs / 86400;
		secs = secs % 86400;
		long hours = secs / 3600;    
		secs = secs % 3600;
		long mins = secs / 60;
		secs = secs % 60;
		
		if(days < 9) {
			text = "0"+days;
		}
		text = (days<10?"0"+days:days)+"d" +":" + (hours<10?"0"+hours:hours)+"h"+":" + (mins<10?"0"+mins:mins)+"m";
		
		return text;
	}
	public static String computehoursBetweenDates(Date startDate,Date endDate) {
		String text = "";
		
		long secs = (endDate.getTime() - startDate.getTime()) / 1000;
		long hours = secs / 3600;    
		
		
		if(hours < 10) {
			text = "0"+hours;
		} else {
			text = ""+hours;
		}
		return text;
	}
	
	public static void mainOLD(String[] args) {
		
		 
		   
		Set<Integer> indexList = new HashSet<Integer>();
		Integer grandwinnersCount = 5;
		Integer winnersCount = 10;
		int count =0;
		for(int i=0;i<grandwinnersCount;i++) {
			Boolean flag = false;
			while(!flag) {
				/*Random random = new Random();
				int val = random.nextInt(winnersCount);
				if(indexList.add(val)) {
					flag = true;
				}
				count++;*/
				Scanner scr = new Scanner(System.in);
				String fName = scr.nextLine();
				String fName1 = fName.trim().replaceAll("'","").replaceAll("-","").replaceAll("_","").replaceAll("_","").replaceAll("\\.","").replaceAll(" ","");
				String fName2 = fName.replaceAll("\\W", "").replaceAll("-", "");
				String fName3 = fName.replaceAll("[^a-zA-Z0-9]*", "");
				boolean flaga = false;
				if(fName1.equals(fName2)) {
					flaga = true;
				}
				System.out.println("fname : "+fName +" : "+fName1+" : "+fName2+" : "+fName3+" : "+flaga);
			}
		}
		for (Integer integer : indexList) {
			System.out.println("List : "+integer);
		}
		System.out.println("Count : "+count);
	}
	
	public static void mainfdfd(String[] args) {
		Set<Integer> indexList = new HashSet<Integer>();
		for(int i=0;i<1;i++) {
		Boolean flag = true;
			while(flag) {
				Random random = new Random();
				int index = random.nextInt(15)+1;
				if(indexList.add(index)) {
					System.out.println("index----->"+index);
					flag = false;
				}
			}
		}
	}
	
	public static void main(String[] args) {

		Date startDate = new Date();
		Date endDate = new Date();
		endDate.setDate(endDate.getDate()+1);
		String hours = computehoursBetweenDates(startDate, endDate);
		System.out.println("Start : "+startDate+" : "+endDate+" :hours: "+hours);

	}

	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
}
