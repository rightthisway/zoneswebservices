package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.QuizContest;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Order Status Notification
 * @author KUlaganathan
 *
 */
public class QuizContestWinnerNotification extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	private static Logger logger = LoggerFactory.getLogger(QuizContestWinnerNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizContestWinnerNotification.mailManager = mailManager;
	}
	public void init() throws Exception{
		
		logger.info("Initiating the email & push notification scheduler process for contest winners....");
		
		List<ContestGrandWinner> grandWinnerList = QuizDAORegistry.getContestGrandWinnerDAO().getRecentContentWinners();
		
		Integer count=0,androidProcessed=0,iosProcessed=0;
		
		if(grandWinnerList != null && !grandWinnerList.isEmpty()) {
			System.out.println("QUIZ GRANDWINNER NOTIFICATION insode If : "+grandWinnerList.size());
			logger.info("QUIZ GRANDWINNER NOTIFICATION insode If : "+grandWinnerList.size());
			Date start = new Date();
			
			for (ContestGrandWinner winner : grandWinnerList) {
			
				System.out.println("QUIZ WINNER NOTIFICATION CustID : "+winner.getCustomerId());
				logger.info("QUIZ WINNER NOTIFICATION CustID : "+winner.getCustomerId());
				
				List<CustomerDeviceDetails> custNotificationList = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveDeviceDetailsByCustomerId(winner.getCustomerId());
				
				String notificationMsg = "Congratulations, You have won 4tickets";
				
				for (CustomerDeviceDetails notifiDetails : custNotificationList) {
					
					try {
						String jsonString="";
						if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.ANDROID.toString())) {
							NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
							notificationJsonObject.setNotificationType(NotificationType.QUIZ_GRAND_WINNER);
							notificationJsonObject.setMessage(notificationMsg);
							Gson gson = new Gson();	
							jsonString = gson.toJson(notificationJsonObject);
							gcmNotificationService.sendMessage(NotificationType.QUIZ_GRAND_WINNER, notifiDetails.getNotificationRegId(), jsonString);
							androidProcessed++;
						} else if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.IOS.toString())) {
							Map<String, String> customFields = new HashMap<String, String>();
							customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_GRAND_WINNER));
							//customFields.put("orderId", String.valueOf(customerOrder.getId()));
							apnsNotificationService.sendNotification(NotificationType.QUIZ_GRAND_WINNER, notifiDetails.getNotificationRegId(), notificationMsg,customFields);
							iosProcessed++;
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("QUIZ WINNER NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId()+" : "+new Date());
						logger.info("QUIZ WINNER NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId());
					} 
				}
				
				Customer customer = CustomerUtil.getCustomerById(winner.getCustomerId());
				
				QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(winner.getContestId());
				
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("contestName", contest.getContestName());
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForContestEmail();
				try{
					String subject = "Reward The Fan Trivia- You are the grand winner! ";
					mailManager.sendMailNow("text/html","livetrivia@rewardthefan.com", customer.getEmail(), 
							null,URLUtil.bccEmails, subject,
							"mail-send-contest-grand-winner.html", mailMap, "text/html", null,mailAttachment,null);
				}catch(Exception e){
					e.printStackTrace();
				}
				winner.setIsNotified(true);
				winner.setNotifiedTime(new Date());
				try{
					QuizDAORegistry.getContestGrandWinnerDAO().updateWinnerNotification(winner.getId());
				}catch (Exception e) {
					e.printStackTrace();
					System.out.println("QUIZ WINNER NOTIFICATION Error While updating Notified Time  : "+winner.getId()+" : "+new Date());
					logger.info("QUIZ WINNER NOTIFICATION Error While updating Notified Time  : "+winner.getId()+" : "+new Date());
				} 
				
				
			}
			System.out.println("QUIZ WINNER NOTIFICATION Completed : "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime())+" : "+new Date());
			logger.info("QUIZ WINNER NOTIFICATION Completed : "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime()));
		}
		
	}
	 
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				//init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
		notificationJsonObject.setNotificationType(NotificationType.QUIZ_GRAND_WINNER);
		notificationJsonObject.setMessage("Congratulations, You have won 4tickets");
		Gson gson = new Gson();	
		String jsonString = gson.toJson(notificationJsonObject);
		System.out.println(jsonString);
	}
	 
}