package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.rtfquiz.webservices.data.QuizSuperFanTracking;
import com.rtfquiz.webservices.sqldao.implementation.CustomerRewardHistorySQLDAO;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.ContestMigrationStats;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.QuizAffiliateRewardHistory;
import com.zonesws.webservices.data.QuizAffiliateSetting;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * QuizCreditUtil - Credit Reward Dollars, Super Fan Stars, Life lines to the customers.
 * @author KUlaganathan
 *
 */
public class QuizCreditUtil{
	
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
	
	private static Logger logger = LoggerFactory.getLogger(QuizCreditUtil.class);
	static MailManager mailManager = null;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizCreditUtil.mailManager = mailManager;
	}

	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public static void start(Integer contestId) throws Exception{
		
		System.out.println("Initiating the scheduler process for computing super fan stats....");
		try {
			String referralFromDateStr = "2018-08-01 00.03.15";
			Date referralFromDate = dateTimeFormat.parse(referralFromDateStr);
			Date now = new Date();
			System.out.println("QUIZ CONTEST CREDITS Job Started: CONTEST ID: "+contestId);
			//System.out.println("QUIZ CONTEST CREDITS Job Started: CONTEST ID: "+contestId);
			
			QuizContest contest= QuizDAORegistry.getQuizContestDAO().getCompletedContestByContestID(contestId);
			
			if(null != contest) {
				Integer totalParticipants = 0;
				Date start = new Date();
				List<QuizCustomerReferralTracking> referralList = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().
						getAllSuperFanOfferApplicableReferrals(referralFromDate,now);
				
				Map<Integer, QuizCustomerReferralTracking> tireOneUserPrimaryUserMap = new HashMap<Integer, QuizCustomerReferralTracking>();
				
				Map<Integer, QuizCustomerReferralTracking> affiliateReferralMap = new HashMap<Integer, QuizCustomerReferralTracking>();
				
				for (QuizCustomerReferralTracking tracking : referralList) {
					tireOneUserPrimaryUserMap.put(tracking.getCustomerId(), tracking);
					
					if(null != tracking.getIsAffiliateReferral() && tracking.getIsAffiliateReferral()) {
						affiliateReferralMap.put(tracking.getCustomerId(), tracking);
					}
				}
				
				//Get HarryGBestMC Affiliate Details
				QuizAffiliateSetting affiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getAffiliate(598155);
				
				 
				System.out.println("QUIZ CONTEST CREDITS: Total No of Referrals : "+tireOneUserPrimaryUserMap.size());
				
				Collection<QuizSuperFanStat> superFanStatsList = QuizDAORegistry.getQuizSuperFanStatDAO().getAll();
				
				Map<Integer, QuizSuperFanStat> superFanStatMap = new HashMap<Integer, QuizSuperFanStat>();
				
				for (QuizSuperFanStat obj : superFanStatsList) {
					superFanStatMap.put(obj.getCustomerId(), obj);
				}
				String resMsg = "";
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String currentDateStr = df.format(new Date());
				String specialReferralFromStr = currentDateStr+" 00.01.00",specialReferralToStr = currentDateStr+" 23.59.59";
				Date specialReferralFromDate = dateTimeFormat.parse(specialReferralFromStr);
				Date specialReferralToDate = dateTimeFormat.parse(specialReferralToStr);
					
				try {
					
					resMsg ="QUIZ CONTEST CREDITS Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Started: "+new Date();
					System.out.println(resMsg);
					//System.out.println(resMsg);
					
					List<ContestMigrationStats> contestMigrationStatList = DAORegistry.getContestMigrationStatsDAO().getAllStatsByContestId(contest.getId());
					if(null == contestMigrationStatList || contestMigrationStatList.isEmpty() || contestMigrationStatList.size() < 2) {
						resMsg ="QUIZ CONTEST CREDITS Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
						System.out.println(resMsg);
						//System.out.println(resMsg);
						return;
					}
					
					boolean isCompleted = true;
					for (ContestMigrationStats obj : contestMigrationStatList) {
						if(obj.getStatus().equals("RUNNING")) {
							isCompleted = false;
							break;
						}
					}
					
					if(!isCompleted) {
						resMsg ="QUIZ CONTEST CREDITS Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
						System.out.println(resMsg);
						//System.out.println(resMsg);
						return;
					}
					
					ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getRewardCreditByContestIdAndType(contest.getId(),"CONTEST_CREDITS");
					
					if(null != credit) {
						resMsg ="QUIZ CONTEST CREDITS Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: Already processed contest.";
						System.out.println(resMsg);
						//System.out.println(resMsg);
						return;
					}else {
						credit = new ContestReferrerRewardCredit();
						credit.setContestId(contest.getId());
						credit.setCreatedDate(start);
						credit.setIsNotified(true);
						credit.setNotifiedTime(new Date());
						credit.setRewardCreditConv(0.00);
						credit.setReferralProgramType("CONTEST_CREDITS");
						credit.setStatus("STARTED");
						credit.setUpdatedDate(new Date());
						QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
					}
					
					List<Integer> participantsList = DAORegistry.getQueryManagerDAO().getQuizParticipantsByContestId(contest.getId());
					
					Map<Integer, Boolean> participantMap = new HashMap<Integer, Boolean>();
					for (Integer participant : participantsList) {
						participantMap.put(participant, true);
					}
					
					if(participantMap.isEmpty() || participantMap.size() <= 0) {
						resMsg ="QUIZ CONTEST CREDITS participants is empty Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: No Participant found.";
						System.out.println(resMsg);
						//System.out.println(resMsg);
						return;
					}
					
					totalParticipants = participantMap.size();
					
					System.out.println("QUIZ CONTEST CREDITS: No Of Contest Participants : "+participantMap.size());
					System.out.println(resMsg);
					
					Map<Integer, QuizSuperFanTracking> superFanTrackingMap = new HashMap<Integer, QuizSuperFanTracking>();
					
					Integer referralChances = contest.getReferralStar(), participantChances = contest.getParticipantStar();
					
					Integer participantLives = contest.getParticipantLives();
					Double participantRewards = contest.getParticipantRewards();
					Integer participantRtfPoints = contest.getParticipantRtfPoints();
					
					Double specialReferralReward = contest.getReferralRewards();
					Integer specialReferralRtfPoints = contest.getReferralRtfPoints();
					
					for(Integer participantCustId: participantMap.keySet()) {
						
						try {
							
							QuizCustomerReferralTracking tracking = tireOneUserPrimaryUserMap.get(participantCustId);
							
							boolean isReferral = false;
							
							Integer primaryCustId = null;
							if(null != tracking) {
								isReferral = true; 
								primaryCustId= tracking.getReferralCustomerId();
							}
							
							Integer oldStars = 0, newStars =0, oldParticipantStars=0,oldReferralStars=0;
							 
							if(isReferral) {
								
								QuizSuperFanStat primaryCustStat = superFanStatMap.get(primaryCustId);
								
								if(null == primaryCustStat) {
									primaryCustStat = new QuizSuperFanStat();
									primaryCustStat.setCreatedDate(start);
									primaryCustStat.setCustomerId(primaryCustId);
									primaryCustStat.setNoOfGamePlayed(0);
									primaryCustStat.setParticipationChances(0);
									primaryCustStat.setReferralChances(referralChances);
									primaryCustStat.setStatus("ACTIVE");
									primaryCustStat.setTotalNoOfChances(referralChances);
									primaryCustStat.setUpdatedDate(start);
									
									oldStars = 0;
									newStars = oldStars + referralChances;
									oldParticipantStars= 0;
									oldReferralStars = 0;
								}else {
									
									oldStars = primaryCustStat.getTotalNoOfChances();
									newStars = oldStars + referralChances;
									oldParticipantStars= primaryCustStat.getParticipationChances();
									oldReferralStars = primaryCustStat.getReferralChances();
									
									primaryCustStat.setReferralChances(primaryCustStat.getReferralChances() + referralChances);
									primaryCustStat.setTotalNoOfChances(primaryCustStat.getTotalNoOfChances() + referralChances);
									primaryCustStat.setUpdatedDate(start);
									primaryCustStat.setStatus("ACTIVE");
								}
								try {
									CassandraDAORegistry.getCassCustomerDAO().updateCustomerSuperFanTotalChances(primaryCustId, primaryCustStat.getTotalNoOfChances());
								}catch(Exception e) {
									e.printStackTrace();
								}
								QuizDAORegistry.getQuizSuperFanStatDAO().saveOrUpdate(primaryCustStat);
								superFanStatMap.put(primaryCustId, primaryCustStat);
								
								try {
									QuizSuperFanTracking quizSuperFanTracking = superFanTrackingMap.get(primaryCustId);
									if(null == quizSuperFanTracking) {
										quizSuperFanTracking = new QuizSuperFanTracking();
										quizSuperFanTracking.setThisContestParticipantStars(0);
									}else {
										//quizSuperFanTracking.setThisContestParticipantStars(0);
									}
									quizSuperFanTracking.setContestId(contest.getId());
									quizSuperFanTracking.setCreatedDate(start);
									quizSuperFanTracking.setCustomerId(primaryCustId);
									quizSuperFanTracking.setOldSuperFanStars(oldStars);
									quizSuperFanTracking.setNewSuperFanStars(newStars);
									quizSuperFanTracking.setNotes("");
									quizSuperFanTracking.setOldParticipantStars(oldParticipantStars);
									quizSuperFanTracking.setOldReferralStars(oldReferralStars);
									quizSuperFanTracking.setThisContestReferralStars(referralChances);
									QuizDAORegistry.getQuizSuperFanTrackingDAO().saveOrUpdate(quizSuperFanTracking);
									superFanTrackingMap.put(primaryCustId, quizSuperFanTracking);
								}catch(Exception e) {
									e.printStackTrace();
								}
								
							}
							
							if(isReferral && specialReferralReward > 0 ) {
								Boolean isPrimaryCustomerPlayed = participantMap.get(primaryCustId);
								try {
							 	Boolean validSpecialCredit2 = Util.isDateBetweenTwoDate(tracking.getCreatedDateTime(), specialReferralFromDate, specialReferralToDate);
							 
							 	if(validSpecialCredit2 && isPrimaryCustomerPlayed) {
						 			Customer customer = CustomerUtil.getCustomerById(participantCustId);
								 	Boolean validSpecialCredit1 = Util.isDateBetweenTwoDate(customer.getSignupDate(), specialReferralFromDate, specialReferralToDate);
								 	
								 	if(validSpecialCredit1) {
								 		
							 		Double rewardCredit = specialReferralReward;
								 	CustomerLoyalty tireOneCustLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(participantCustId);
								 	
								 	if(null != tireOneCustLoyalty) {
								 		CustomerLoyaltyTracking tireOneCustTracking = new CustomerLoyaltyTracking();
									 	tireOneCustTracking.setContestId(contest.getId());
									 	tireOneCustTracking.setCreatedBy("2019-SPECIAL-REFERRAL-CREDIT-"+contest.getId());
									 	tireOneCustTracking.setCreatedDate(new Date());
									 	tireOneCustTracking.setCustomerId(participantCustId);
									 	tireOneCustTracking.setRewardPoints(rewardCredit);
									 	tireOneCustTracking.setRewardType("CONTEST");
										
										tireOneCustLoyalty.setActivePointsAsDouble(tireOneCustLoyalty.getActivePointsAsDouble() + rewardCredit);
										tireOneCustLoyalty.setDollerConversion(1.00);
										tireOneCustLoyalty.setLastUpdate(new Date());
										tireOneCustLoyalty.setLatestEarnedPointsAsDouble(rewardCredit);
										tireOneCustLoyalty.setLatestSpentPointsAsDouble(0.00);
										tireOneCustLoyalty.setTotalEarnedPoints(tireOneCustLoyalty.getTotalEarnedPoints()+rewardCredit);
										tireOneCustLoyalty.setNotifiedMessage("2019-SPECIAL-REFERRAL-CREDIT: "+rewardCredit);
										 
										DAORegistry.getCustomerLoyaltyTrackingDAO().save(tireOneCustTracking);
										DAORegistry.getCustomerLoyaltyDAO().update(tireOneCustLoyalty);
										
										try {
											CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(participantCustId,tireOneCustLoyalty.getActivePointsAsDouble());
										}catch(Exception e) {
											e.printStackTrace();
										}
								 	}
									
									CustomerLoyalty primaryCustLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(primaryCustId);
									
									if(primaryCustLoyalty != null) {
										CustomerLoyaltyTracking primaryCustTracking = new CustomerLoyaltyTracking();
										primaryCustTracking.setContestId(contest.getId());
										primaryCustTracking.setCreatedBy("2019-SPECIAL-REFERRAL-CREDIT-"+contest.getId()+"-FROM: "+participantCustId);
										primaryCustTracking.setCreatedDate(new Date());
										primaryCustTracking.setCustomerId(primaryCustId);
										primaryCustTracking.setRewardPoints(rewardCredit);
										primaryCustTracking.setRewardType("CONTEST");
										
										primaryCustLoyalty.setActivePointsAsDouble(primaryCustLoyalty.getActivePointsAsDouble() + rewardCredit);
										primaryCustLoyalty.setDollerConversion(1.00);
										primaryCustLoyalty.setLastUpdate(new Date());
										primaryCustLoyalty.setLatestEarnedPointsAsDouble(rewardCredit);
										primaryCustLoyalty.setLatestSpentPointsAsDouble(0.00);
										primaryCustLoyalty.setTotalEarnedPoints(primaryCustLoyalty.getTotalEarnedPoints()+rewardCredit);
										primaryCustLoyalty.setNotifiedMessage("2019-SPECIAL-REFERRAL-CREDIT: "+rewardCredit);
										 
										DAORegistry.getCustomerLoyaltyTrackingDAO().save(primaryCustTracking);
										DAORegistry.getCustomerLoyaltyDAO().update(primaryCustLoyalty);
										
										try {
											CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(primaryCustId,primaryCustLoyalty.getActivePointsAsDouble());
										}catch(Exception e) {
											e.printStackTrace();
										}
									}
								 	}
							 	
							 	}
							 		
							 	}catch(Exception e) {
							 		e.printStackTrace();
							 	}
							}
							
							if(isReferral && specialReferralRtfPoints > 0 ) {
								Boolean isPrimaryCustomerPlayed = participantMap.get(primaryCustId);
								try {
							 	Boolean validSpecialCredit2 = Util.isDateBetweenTwoDate(tracking.getCreatedDateTime(), specialReferralFromDate, specialReferralToDate);
							 
							 	if(validSpecialCredit2 && isPrimaryCustomerPlayed) {
						 			Customer customer = CustomerUtil.getCustomerById(participantCustId);
								 	Boolean validSpecialCredit1 = Util.isDateBetweenTwoDate(customer.getSignupDate(), specialReferralFromDate, specialReferralToDate);
								 	
								 	if(validSpecialCredit1) {
								 		try {
								 			customer.setRtfPoints(customer.getRtfPoints() + specialReferralRtfPoints);
											//DAORegistry.getCustomerDAO().update(participant);
											DAORegistry.getCustomerDAO().updateQuizCustomerRtfPoints(participantCustId, specialReferralRtfPoints);
											
											CassandraDAORegistry.getCassCustomerDAO().updateCustomerRtfPointsByCustomerId(participantCustId, customer.getRtfPoints());
											
											CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(participantCustId, "CR", SourceType.CONT_SPL_REFERRAL_TIREONE, contestId, 0, 0, 0, specialReferralRtfPoints, 0.0, "");
											
										}catch(Exception e) {
											e.printStackTrace();
										}
										
										try {
											Customer primaryCustomer = CustomerUtil.getCustomerById(primaryCustId);
											
											primaryCustomer.setRtfPoints(primaryCustomer.getRtfPoints() + specialReferralRtfPoints);
											//DAORegistry.getCustomerDAO().update(participant);
											DAORegistry.getCustomerDAO().updateQuizCustomerRtfPoints(primaryCustId, specialReferralRtfPoints);
											
											CassandraDAORegistry.getCassCustomerDAO().updateCustomerRtfPointsByCustomerId(primaryCustId, primaryCustomer.getRtfPoints());
											
											CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(primaryCustId, "CR", SourceType.CONT_SPL_REFERRAL_PRIMARY, contestId, 0, 0, 0, specialReferralRtfPoints, 0.0, "Referral For:"+participantCustId);
											
										}catch(Exception e) {
											e.printStackTrace();
										}
									
								 	}
							 	
							 	}
							 		
							 	}catch(Exception e) {
							 		e.printStackTrace();
							 	}
							}
							QuizSuperFanStat participantCustStat = superFanStatMap.get(participantCustId);
							if(null == participantCustStat) {
								participantCustStat = new QuizSuperFanStat();
								participantCustStat.setCreatedDate(start);
								participantCustStat.setCustomerId(participantCustId);
								participantCustStat.setNoOfGamePlayed(1);
								participantCustStat.setParticipationChances(participantChances);
								participantCustStat.setReferralChances(0);
								participantCustStat.setStatus("ACTIVE");
								participantCustStat.setTotalNoOfChances(participantChances);
								participantCustStat.setUpdatedDate(start);
								
								oldStars = 0;
								newStars = oldStars + participantChances;
								oldParticipantStars= 0;
								oldReferralStars = 0;
								
							}else {
								
								oldStars = participantCustStat.getTotalNoOfChances();
								newStars = oldStars + participantChances;
								oldParticipantStars= participantCustStat.getParticipationChances();
								oldReferralStars = participantCustStat.getReferralChances();
								
								participantCustStat.setNoOfGamePlayed(participantCustStat.getNoOfGamePlayed() + 1);
								participantCustStat.setParticipationChances(participantCustStat.getParticipationChances() + participantChances);
								participantCustStat.setTotalNoOfChances(participantCustStat.getTotalNoOfChances() + participantChances);
								participantCustStat.setStatus("ACTIVE");
								participantCustStat.setUpdatedDate(start);
							}
							try {
								CassandraDAORegistry.getCassCustomerDAO().updateCustomerSuperFanTotalChances(participantCustId, participantCustStat.getTotalNoOfChances());
							}catch(Exception e) {
								e.printStackTrace();
							}
							QuizDAORegistry.getQuizSuperFanStatDAO().saveOrUpdate(participantCustStat);
							
							superFanStatMap.put(participantCustId, participantCustStat);
							
							try {
								QuizSuperFanTracking quizSuperFanTracking = superFanTrackingMap.get(participantCustId);
								if(null == quizSuperFanTracking) {
									quizSuperFanTracking = new QuizSuperFanTracking();
									quizSuperFanTracking.setThisContestReferralStars(0);
								}else {
									 
								}
								quizSuperFanTracking.setContestId(contest.getId());
								quizSuperFanTracking.setCreatedDate(start);
								quizSuperFanTracking.setCustomerId(participantCustId);
								quizSuperFanTracking.setOldSuperFanStars(oldStars);
								quizSuperFanTracking.setNewSuperFanStars(newStars);
								quizSuperFanTracking.setNotes("");
								quizSuperFanTracking.setOldParticipantStars(oldParticipantStars);
								quizSuperFanTracking.setOldReferralStars(oldReferralStars);
								quizSuperFanTracking.setThisContestParticipantStars(participantChances);
								QuizDAORegistry.getQuizSuperFanTrackingDAO().saveOrUpdate(quizSuperFanTracking);
								superFanTrackingMap.put(participantCustId, quizSuperFanTracking);
							}catch(Exception e) {
								e.printStackTrace();
							}
							
							if(participantLives != null && participantLives > 0) {
								try {
									Customer participant = DAORegistry.getCustomerDAO().get(participantCustId);
									participant.setQuizCustomerLives(participant.getQuizCustomerLives() + participantLives);
									DAORegistry.getCustomerDAO().update(participant);;
									CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(participantCustId, participant.getQuizCustomerLives());
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							
							if(participantRewards != null && participantRewards > 0) {
								try {
									
									CustomerLoyalty participantLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(participantCustId);
									
									if(participantLoyalty != null) {
										CustomerLoyaltyTracking participantLoyaltyTracking = new CustomerLoyaltyTracking();
										participantLoyaltyTracking.setContestId(contest.getId());
										participantLoyaltyTracking.setCreatedBy("PARCIPANT-REWARD-CREDIT-"+contest.getId()+"");
										participantLoyaltyTracking.setCreatedDate(new Date());
										participantLoyaltyTracking.setCustomerId(participantCustId);
										participantLoyaltyTracking.setRewardPoints(participantRewards);
										participantLoyaltyTracking.setRewardType("CONTEST");
										
										participantLoyalty.setActivePointsAsDouble(participantLoyalty.getActivePointsAsDouble() + participantRewards);
										participantLoyalty.setDollerConversion(1.00);
										participantLoyalty.setLastUpdate(new Date());
										participantLoyalty.setLatestEarnedPointsAsDouble(participantRewards);
										participantLoyalty.setLatestSpentPointsAsDouble(0.00);
										participantLoyalty.setTotalEarnedPoints(participantLoyalty.getTotalEarnedPoints()+participantRewards);
										participantLoyalty.setNotifiedMessage("PARCIPANT-REWARD-CREDIT: "+participantRewards);
										 
										DAORegistry.getCustomerLoyaltyTrackingDAO().save(participantLoyaltyTracking);
										DAORegistry.getCustomerLoyaltyDAO().update(participantLoyalty);
										
										try {
											CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(participantCustId,participantLoyalty.getActivePointsAsDouble());
										}catch(Exception e) {
											e.printStackTrace();
										}
									}
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							
							if(participantRtfPoints != null && participantRtfPoints > 0) {
								try {
									
									Customer participant = DAORegistry.getCustomerDAO().get(participantCustId);
									participant.setRtfPoints(participant.getRtfPoints() + participantRtfPoints);
									//DAORegistry.getCustomerDAO().update(participant);
									DAORegistry.getCustomerDAO().updateQuizCustomerRtfPoints(participantCustId, participantRtfPoints);
									
									CassandraDAORegistry.getCassCustomerDAO().updateCustomerRtfPointsByCustomerId(participantCustId, participant.getRtfPoints());
									
									CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(participantCustId, "CR", SourceType.CONTEST_PARTICIPANT, contestId, 0, 0, 0, participantRtfPoints, 0.0, "");
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							
							/* HARRYGBESTMC Affiliate SF & Cash Credit Added on - 05/19/2019 - By Ulaganathan*/
							
							//Valid till June-17-2019
							 
							QuizCustomerReferralTracking affReferralObj = affiliateReferralMap.get(participantCustId);
							
							if(null != affReferralObj && affiliateSetting != null) {
								
								if(affReferralObj.getReferralCustomerId().equals(affiliateSetting.getCustomerId()) && !affReferralObj.getAffiliateReferralStatus().equals("CREDITED")) {
									 
									if(null != affiliateSetting.getRewardValue() && affiliateSetting.getRewardValue() >0) {
										QuizAffiliateRewardHistory affiliateRewardHistory =new QuizAffiliateRewardHistory();
										affiliateRewardHistory.setContestId(contest.getId());
										affiliateRewardHistory.setAffiliateCustomerId(affiliateSetting.getCustomerId());
										affiliateRewardHistory.setCashReward(affiliateSetting.getRewardValue());
										affiliateRewardHistory.setCreatedDate(new Date());
										affiliateRewardHistory.setLivesToAffiliateCustomer(0);
										affiliateRewardHistory.setLivesToTireOneCustomer(0);
										affiliateRewardHistory.setRewardDollar(0.00);
										affiliateRewardHistory.setRewardType(affiliateSetting.getRewardType());
										affiliateRewardHistory.setStatus("ACTIVE");
										affiliateRewardHistory.setTireoneCustomerId(participantCustId);
										affiliateRewardHistory.setUpdatedDate(new Date());
										
										affiliateSetting.setActiveCashReward(affiliateSetting.getActiveCashReward()+affiliateSetting.getRewardValue());
										affiliateSetting.setTotalCashReward(affiliateSetting.getTotalCashReward()+affiliateSetting.getRewardValue());
										 
										tracking.setAffiliateReferralStatus("CREDITED");
										tracking.setUpdatedDateTime(new Date());
										tracking.setContestDate(contest.getContestStartDateTime());
										
										DAORegistry.getQuizAffiliateSettingDAO().update(affiliateSetting);
										DAORegistry.getQuizAffiliateRewardHistoryDAO().save(affiliateRewardHistory);
										QuizDAORegistry.getQuizCustomerReferralTrackingDAO().update(tracking);
										 
									}
									
									if(null != affiliateSetting.getTireOneCustSuperFanStars() && affiliateSetting.getTireOneCustSuperFanStars() > 0) {
										
										Integer affSFStars = affiliateSetting.getTireOneCustSuperFanStars();
										
										participantCustStat = superFanStatMap.get(participantCustId);
										
										if(null == participantCustStat) {
											participantCustStat = new QuizSuperFanStat();
											participantCustStat.setCreatedDate(start);
											participantCustStat.setCustomerId(participantCustId);
											participantCustStat.setNoOfGamePlayed(1);
											participantCustStat.setParticipationChances(affSFStars);
											participantCustStat.setReferralChances(0);
											participantCustStat.setStatus("ACTIVE");
											participantCustStat.setTotalNoOfChances(affSFStars);
											participantCustStat.setUpdatedDate(start);
											
											oldStars = 0;
											newStars = oldStars + affSFStars;
											oldParticipantStars= 0;
											oldReferralStars = 0;
											
										}else {
											
											oldStars = participantCustStat.getTotalNoOfChances();
											newStars = oldStars + affSFStars;
											oldParticipantStars= participantCustStat.getParticipationChances();
											oldReferralStars = participantCustStat.getReferralChances();
											
											participantCustStat.setNoOfGamePlayed(participantCustStat.getNoOfGamePlayed() + 1);
											participantCustStat.setParticipationChances(participantCustStat.getParticipationChances() + affSFStars);
											participantCustStat.setTotalNoOfChances(participantCustStat.getTotalNoOfChances() + affSFStars);
											participantCustStat.setStatus("ACTIVE");
											participantCustStat.setUpdatedDate(start);
										}
										try {
											CassandraDAORegistry.getCassCustomerDAO().updateCustomerSuperFanTotalChances(participantCustId, participantCustStat.getTotalNoOfChances());
										}catch(Exception e) {
											e.printStackTrace();
										}
										QuizDAORegistry.getQuizSuperFanStatDAO().saveOrUpdate(participantCustStat);
									}
								}
								
							}
							
							/* HARRYGBESTMC Affiliate SF & Cash Credit Added on - 05/19/2019 - By Ulaganathan*/
							
						}catch(Exception e) {
							e.printStackTrace();
						}
						
					}
					credit.setStatus("COMPLETED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
					
					resMsg = "QUIZ CONTEST CREDITS Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Process Completed: "+new Date();
					System.out.println(resMsg);
					
				}catch(Exception e) {
					System.out.println("QUIZ CONTEST CREDITS inside catch exception 2  : "+contestId+" : "+new Date());
					e.printStackTrace();
				}
				resMsg = "QUIZ CONTEST CREDITS Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date();
				System.out.println(resMsg);
			} else {
				System.out.println("QUIZ CONTEST CREDITS inside else Contest Not Yet completed  : "+contestId+" : "+new Date());
			}
		}catch(Exception e){
			System.out.println("QUIZ CONTEST CREDITS inside catch exception 3  : "+contestId+" : "+new Date());
			e.printStackTrace();
		}
	}
	
	public static void sendEmail(String subject, String message,  QuizContest contest ) {
		try{
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("contestId",contest.getId());
			mailMap.put("contestName",contest.getContestName());
			mailMap.put("contestDate",contest.getContestStartDateTime());
			mailMap.put("message",message);
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			mailManager.sendMailNow("text/html",URLUtil.fromEmail, "AODev@rightthisway.com", 
				null,null, subject,
				"mail-notify-contest-credit-process-status.html", mailMap, "text/html", null,mailAttachment,null);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
}