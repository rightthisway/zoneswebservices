package com.rtfquiz.webservices.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerNotificationDeliveryTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;

/**
 * Order Status Notification
 * @author KUlaganathan
 *
 */
public class QuizCustomerLifeLineEarnedNotificationUtil {
	
	private static Logger logger = LoggerFactory.getLogger(QuizCustomerLifeLineEarnedNotificationUtil.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}

	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public static void main(String[] args) {
		
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, -10);
		long days = getDifferenceDays(calendar.getTime(), new Date());
		System.out.println("From Date :"+calendar.getTime());
		System.out.println("To Date :"+new Date());
		System.out.println(days);
	}
	

	public static void sendLifeLinedEarnedNotification(Customer referralCustomer,Customer customer) throws Exception {
		
		try {
			logger.debug("Quiz Lifeline Earned Notification starts....");
		
			String notificationMsg = "Congratulation. "+customer.getUserId()+" used your referral code. You have earned a life!";
			
			List<CustomerDeviceDetails> deviceList = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveDeviceDetailsByCustomerId(referralCustomer.getId());
			if(deviceList !=null && !deviceList.isEmpty()){

				String jsonString="";
				for (CustomerDeviceDetails device : deviceList) {

					switch (device.getApplicationPlatForm()) {
					
					case ANDROID:
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.QUIZ_LIFELINE_EARNED);
						notificationJsonObject.setMessage(notificationMsg);
						notificationJsonObject.setCustomerId(referralCustomer.getId());
						//notificationJsonObject.setOrderId(customerOrder.getId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						gcmNotificationService.sendMessage(NotificationType.QUIZ_LIFELINE_EARNED, device.getNotificationRegId(), jsonString);
						break;
						
					case IOS:
					    Map<String, String> customFields = new HashMap<String, String>();
					    customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_LIFELINE_EARNED));
					    //customFields.put("orderId", String.valueOf(customerOrder.getId()));
					    customFields.put("customerId", String.valueOf(referralCustomer.getId()));
						apnsNotificationService.sendNotification(NotificationType.QUIZ_LIFELINE_EARNED, device.getNotificationRegId(), notificationMsg,customFields);
						break;
						
	
						default:
							break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error While sending Lifeline Earned Notification : "+referralCustomer.getId());
			// TODO: handle exception
		}
	}
	
	public static void tireOneCustomerEarnedLifeline(Customer customer) throws Exception {
		
		try {
			logger.debug("Quiz Lifeline Earned Notification starts....");
		
			String notificationMsg = "Congratulation. You have earned a life!";
			
			List<CustomerDeviceDetails> deviceList = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveDeviceDetailsByCustomerId(customer.getId());
			if(deviceList !=null && !deviceList.isEmpty()){

				String jsonString="";
				for (CustomerDeviceDetails device : deviceList) {

					switch (device.getApplicationPlatForm()) {
					
					case ANDROID:
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.QUIZ_LIFELINE_EARNED);
						notificationJsonObject.setMessage(notificationMsg);
						notificationJsonObject.setCustomerId(customer.getId());
						//notificationJsonObject.setOrderId(customerOrder.getId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						gcmNotificationService.sendMessage(NotificationType.QUIZ_LIFELINE_EARNED, device.getNotificationRegId(), jsonString);
						break;
						
					case IOS:
					    Map<String, String> customFields = new HashMap<String, String>();
					    customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_LIFELINE_EARNED));
					    //customFields.put("orderId", String.valueOf(customerOrder.getId()));
					    customFields.put("customerId", String.valueOf(customer.getId()));
						apnsNotificationService.sendNotification(NotificationType.QUIZ_LIFELINE_EARNED, device.getNotificationRegId(), notificationMsg,customFields);
						break;
						
	
						default:
							break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error While sending Lifeline Earned Notification : "+customer.getId());
			// TODO: handle exception
		}
	}
	
	
public static void sendNotification(Customer customer, String notificationMsg) throws Exception {
		
		try {
			logger.debug("Quiz Lifeline Earned Notification starts....");
		
			
			List<CustomerDeviceDetails> deviceList = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveDeviceDetailsByCustomerId(customer.getId());
			if(deviceList !=null && !deviceList.isEmpty()){

				String jsonString="";
				for (CustomerDeviceDetails device : deviceList) {

					switch (device.getApplicationPlatForm()) {
					
					case ANDROID:
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.QUIZ_LIFELINE_EARNED);
						notificationJsonObject.setMessage(notificationMsg);
						notificationJsonObject.setCustomerId(customer.getId());
						//notificationJsonObject.setOrderId(customerOrder.getId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						gcmNotificationService.sendMessage(NotificationType.QUIZ_LIFELINE_EARNED, device.getNotificationRegId(), jsonString);
						break;
						
					case IOS:
					    Map<String, String> customFields = new HashMap<String, String>();
					    customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_LIFELINE_EARNED));
					    //customFields.put("orderId", String.valueOf(customerOrder.getId()));
					    customFields.put("customerId", String.valueOf(customer.getId()));
						apnsNotificationService.sendNotification(NotificationType.QUIZ_LIFELINE_EARNED, device.getNotificationRegId(), notificationMsg,customFields);
						break;
						
	
						default:
							break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error While sending Lifeline Earned Notification : "+customer.getId());
			// TODO: handle exception
		}
	}
	
	
public static void sendTestNotificationFromLinuxMachine(List<CustomerDeviceDetails> custNotificationList,String notificationMsg) throws Exception {
		
		Integer androidProcessed=0,iosProcessed=0;
		Integer count = 0;
		Date start = new Date();
		if(custNotificationList != null) {
			 count = custNotificationList.size();
			 List<CustomerNotificationDeliveryTracking> iosTrackingList = new ArrayList<CustomerNotificationDeliveryTracking>();
			 List<CustomerNotificationDeliveryTracking> androidTrackingList = new ArrayList<CustomerNotificationDeliveryTracking>();
			 
			for (CustomerDeviceDetails notifiDetails : custNotificationList) {
				CustomerNotificationDeliveryTracking tracking = new CustomerNotificationDeliveryTracking(notifiDetails);
				tracking.setNotificationMsg(notificationMsg);
				
				try {
					String jsonString="";
					if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.ANDROID)) {
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.QUIZ_CONTEST_START);
						notificationJsonObject.setMessage(notificationMsg);
						//notificationJsonObject.setOrderId(customerOrder.getId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						String respnseMSg = gcmNotificationService.sendFCMMessage(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), jsonString);
						if(respnseMSg.equals("1")) {
							tracking.setStatus("SUCCESS");
							tracking.setSenderType("FCM");
						} else {
							if(respnseMSg.equalsIgnoreCase("MismatchSenderId")) {
								respnseMSg = gcmNotificationService.sendMessageOne(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), jsonString);
								if(respnseMSg.equals("1")) {
									tracking.setStatus("SUCCESS");	
								} else {
									tracking.setStatus("FAILED");
									tracking.setErrorMsg(respnseMSg);									
								}
								tracking.setSenderType("GCM");
							} else {
								tracking.setSenderType("FCM");
								tracking.setStatus("FAILED");
								tracking.setErrorMsg(respnseMSg);
							}
						}
						androidProcessed++;
						
						tracking.setCreatedDate(new Date());
						tracking.setLastUpdated(new Date());
						
						//tracking.setMessage("SUCCESS");
						androidTrackingList.add(tracking);
						
						
					} else if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.IOS)) {
						Map<String, String> customFields = new HashMap<String, String>();
						customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_CONTEST_START));
						//customFields.put("orderId", String.valueOf(customerOrder.getId()));
						apnsNotificationService.sendNotification(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), notificationMsg,customFields);
						//String respnseMSg = APNSNotificationServicePushTemp.sendNotification(NotificationType.QUIZ_CONTEST_START, notifiDetails.getNotificationRegId(), notificationMsg,customFields);
						//if(respnseMSg.equals("1")) {
							tracking.setStatus("SUCCESS");	
						//} else {
						///	tracking.setStatus("FAILED");
						//	tracking.setErrorMsg(respnseMSg);
						//}
						iosProcessed++;
						
						tracking.setCreatedDate(new Date());
						tracking.setLastUpdated(new Date());
						tracking.setStatus("SUCCESS");
						//tracking.setMessage("SUCCESS");
						iosTrackingList.add(tracking);
						
						
						
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("QUIZ NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId()+" : "+new Date());
					logger.info("QUIZ NOTIFICATION Error While sending Notification to  : "+notifiDetails.getCustomerId());
					
					tracking.setCreatedDate(new Date());
					tracking.setLastUpdated(new Date());
					tracking.setStatus("EXCEPTION");
					try {
						tracking.setErrorMsg(""+e.getMessage());
					} catch (Exception ee) {}
					
					if(notifiDetails.getApplicationPlatForm().equals(ApplicationPlatForm.ANDROID.toString())) {
						androidTrackingList.add(tracking);
					} else {
						iosTrackingList.add(tracking);	
					}
					
				}
					
				//}
			}
			Map<String,Date> iosInactiveDeviceMap = apnsNotificationService.getAllInActiveDevices();
			if(iosInactiveDeviceMap != null && !iosInactiveDeviceMap.isEmpty()) {
				System.out.println("Invalid Token Count : "+iosInactiveDeviceMap.size());
				for (CustomerNotificationDeliveryTracking tracking : iosTrackingList) {
					Date lastRun = iosInactiveDeviceMap.remove(tracking.getNotificationRegId().toLowerCase());
					if(lastRun != null) {
						tracking.setStatus("INVALIDTOKEN");
						tracking.setLastSentDate(lastRun);
						System.out.println("Invalid Token  : "+tracking.getNotificationRegId());
					}
				}
			}
			
			DAORegistry.getCustomerNotificationDeliveryTrackingDAO().saveAll(androidTrackingList);
			DAORegistry.getCustomerNotificationDeliveryTrackingDAO().saveAll(iosTrackingList);
		}
		System.out.println("QUIZ NOTIFICATION Completed tot: "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		logger.info("QUIZ NOTIFICATION Completed tot: "+count+" :and: "+androidProcessed+" :ios: "+iosProcessed+":tm: "+(new Date().getTime()-start.getTime()));
	}
	
	
	
}