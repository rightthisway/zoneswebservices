package com.rtfquiz.webservices.utils;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * 
 * @author Tamil
 *
 */
public class QuizCustomerPromocodeandContestOrderStatsScheduler extends QuartzJobBean implements StatefulJob{
	
	
	private static Logger logger = LoggerFactory.getLogger(QuizCustomerPromocodeandContestOrderStatsScheduler.class);
	static MailManager mailManager = null;
	static Boolean isRunning = false;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizCustomerPromocodeandContestOrderStatsScheduler.mailManager = mailManager;
	}

	public static void processCustomerPromoCodeAndContestOrderStats() throws Exception{
		
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			if(!isRunning) {
				isRunning = true;
				try {
				
					logger.info("Initiating the Promocode and contestorder stats scheduler....");
					Date start = new Date();
					QuizDAORegistry.getQuizSummaryManagerDAO().updateCustomerPromoCodeAndContestOrderStats();
			
					logger.info("QUIZ CUST PROMO CO STATS UPDATE : "+(new Date().getTime()-start.getTime())+" : "+ new Date());
				} catch (Exception e) {
					logger.info("QUIZ CUST PROMO CO STATS UPDATE Error : ");
					e.printStackTrace();
				}
				isRunning = false;
			} else {
				logger.info("QUIZ CUST PROMO CO STATS UPDATE IS Running : "+ new Date());
			}
		}
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			//init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
		notificationJsonObject.setNotificationType(NotificationType.QUIZ_CONTEST_START);
		notificationJsonObject.setMessage("Put your hands up, Reward The Fan Is giving away tickets in less than 15 minutes!");
		//notificationJsonObject.setOrderId(customerOrder.getId());
		Gson gson = new Gson();	
		String jsonString = gson.toJson(notificationJsonObject);
		System.out.println(jsonString);
	}
}