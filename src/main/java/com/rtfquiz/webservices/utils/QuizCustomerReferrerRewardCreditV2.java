package com.rtfquiz.webservices.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.ContestMigrationStats;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * QuizCustomerReferrerRewardCreditV2
 * @author KUlaganathan
 *
 */
public class QuizCustomerReferrerRewardCreditV2 extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
	
	private static Logger logger = LoggerFactory.getLogger(QuizCustomerReferrerRewardCreditV2.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizCustomerReferrerRewardCreditV2.mailManager = mailManager;
	}


	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public static void main(String[] args) throws ParseException {
		
		SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
		
		String offerFromDateStr = "2018-11-18 20.30.00",offerToDateStr = "2018-12-31 23.58.59";
		
		Date fromDate = dateTimeFormat1.parse(offerFromDateStr);
		Date toDate = dateTimeFormat1.parse(offerToDateStr);
		
		/*Calendar fromCal = new GregorianCalendar();
		
		fromCal.set*/
		
		System.out.println(fromDate+"-----------"+toDate);
	}

	public void init() throws Exception{
		
		logger.info("Initiating the email scheduler process for customer orders....");
		try {
		 
		Calendar fromDate = Calendar.getInstance();
		fromDate.add(Calendar.MINUTE, -60);
		
		Calendar toDate = Calendar.getInstance();
		
		String offerFromDateStr = "2018-11-18 20.30.00",offerToDateStr = "2018-12-31 23.58.59";
		
		Date offerFromDate = dateTimeFormat.parse(offerFromDateStr);
		Date offerToDate = dateTimeFormat.parse(offerToDateStr);
		 
		
		System.out.println("CRRN-V2: Job Started:  From Date: "+fromDate.getTime()+", To Date: "+toDate.getTime());
		logger.info("CRRN-V2: Job Started:  From Date: "+fromDate.getTime()+", To Date: "+toDate.getTime());
		
		List<QuizContest> contestList = QuizDAORegistry.getQuizContestDAO().getAllCompleted2018YearEndContestByDate(fromDate.getTime(), toDate.getTime(),offerFromDate,offerToDate);
		
		if(contestList != null && !contestList.isEmpty()) {
			
			Integer contestSize = contestList.size();
			
			System.out.println("CRRN-V2: Total Contest : "+contestSize);
			logger.info("CRRN-V2: Total Contest : "+contestSize);
			
			Date start = new Date();
			
			LoyaltySettings loyaltySettings =  DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			
			if(null == loyaltySettings) {
				return;
			}
			
			Double primaryUserEarnPerc = loyaltySettings.getContestCustReferralRewardPerc(); 
			
			List<QuizCustomerReferralTracking> referralList = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().
					getAllYearEndOfferApplicableReferrals(offerFromDate,offerToDate);
			
			Map<Integer, QuizCustomerReferralTracking> tireOneUserPrimaryUserMap = new HashMap<Integer, QuizCustomerReferralTracking>();
			
			for (QuizCustomerReferralTracking tracking : referralList) {
				tireOneUserPrimaryUserMap.put(tracking.getCustomerId(), tracking);
			}
			
			String resMsg = "";
			
			for (QuizContest contest : contestList) {
				
				try {
					
					resMsg ="CRRN-V2: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", ReferralReward: "+primaryUserEarnPerc+", Process Started: "+new Date();
					logger.info(resMsg);
					
					
					List<ContestMigrationStats> contestMigrationStatList = DAORegistry.getContestMigrationStatsDAO().getAllStatsByContestId(contest.getId());
					
					if(null == contestMigrationStatList || contestMigrationStatList.isEmpty()) {
						resMsg ="CRRN-V2: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", ReferralReward: "+primaryUserEarnPerc+", Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
						logger.info(resMsg);
						continue;
					}
					
					boolean isCompleted = false;
					for (ContestMigrationStats obj : contestMigrationStatList) {
						if(obj.getStatus().equals("COMPLETED")) {
							isCompleted = true;
							break;
						}
					}
					
					if(!isCompleted) {
						resMsg ="CRRN-V2: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", ReferralReward: "+primaryUserEarnPerc+", Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
						logger.info(resMsg);
						continue;
					}
					
					
					ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getRewardCreditByContestIdAndType(contest.getId(),"2018_YEAREND_REWARDS");
					
					if(null != credit) {
						resMsg ="CRRN-V2: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", ReferralReward: "+primaryUserEarnPerc+", Process Skipped: "+new Date()+", Reason: Already processed contest.";
						logger.info(resMsg);
						continue;
					}
					
					List<QuizCustomerContestAnswers> answers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getFirstQuestionAnsweredCustomers(contest.getId());
					
					Map<Integer, Boolean> customerAnsweredMap = new HashMap<Integer, Boolean>();
					for (QuizCustomerContestAnswers contestAnswer : answers) {
						customerAnsweredMap.put(contestAnswer.getCustomerId(), true);
					}
					
					for(Integer participantCustId: customerAnsweredMap.keySet()) {
						
						Double participantPoints = 1.00, referralPoints = 0.00;
						
						QuizCustomerReferralTracking tracking = tireOneUserPrimaryUserMap.get(participantCustId);
						
						boolean isReferral = false;
						
						Integer primaryCustId = null;
						
						if(null != tracking) {
							isReferral = true; 
							referralPoints = 3.00;
							primaryCustId= tracking.getReferralCustomerId();
						}
						
						participantPoints = TicketUtil.getRoundedUpValue(participantPoints);
						
						logger.info("CRRN-V2: participantCustId : "+participantCustId+", PrimaryCustomerId: "+primaryCustId+", Participant Reward: "+participantPoints+", Primary User Reward: "+referralPoints);
						
						CustomerLoyalty participantLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(participantCustId);
						
						if(null == participantLoyalty) {
							continue;
						}
						
						if(isReferral) {
							
							referralPoints = TicketUtil.getRoundedUpValue(referralPoints);
							
							CustomerLoyalty primaryCustLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(primaryCustId);
							
							if(null != primaryCustLoyalty) {
								CustomerLoyaltyHistory history = new CustomerLoyaltyHistory();
								history.setCreateDate(new Date());
								history.setCustomerId(primaryCustId);
								history.setDollarConversionRate(loyaltySettings.getDollerConversion());
								history.setEmailDescription("");
								history.setIsRewardsEmailSent(true);
								history.setInitialRewardAmount(primaryCustLoyalty.getActiveRewardDollers());
								history.setInitialRewardPointsAsDouble(primaryCustLoyalty.getActivePointsAsDouble());
								history.setPointsEarnedAsDouble(referralPoints);
								history.setRewardConversionRate(loyaltySettings.getRewardConversion());
								history.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(referralPoints,loyaltySettings.getDollerConversion()));
								history.setRewardSpentAmount(0.00);
								history.setRewardStatus(RewardStatus.ACTIVE);
								history.setOrderType(OrderType.CONTEST_REFERRAL);
								history.setEmailDescription("2018-YEAREND-REWARD-REFERRAL-CREDITS: "+referralPoints);
								history.setUpdatedDate(new Date());
								history.setTireOneCustEarnPoints(0.00);
								history.setAnsQuestion(0);
								history.setTireOneCustId(participantCustId);
								history.setContestId(contest.getId()); 
								history.setOrderId(-1);
								history.setOrderTotal(0.00);
								
								Double balRewardPoints1 = primaryCustLoyalty.getActivePointsAsDouble() + referralPoints - 0;
								Double balRewardAmount1 = RewardConversionUtil.getRewardDoller(balRewardPoints1,loyaltySettings.getDollerConversion());
								history.setBalanceRewardAmount(balRewardAmount1);
								history.setBalanceRewardPointsAsDouble(balRewardPoints1);
								
								Double totalReferrelDollars = null != primaryCustLoyalty.getTotalReferralDollars()?primaryCustLoyalty.getTotalReferralDollars():0;
								
								primaryCustLoyalty.setTotalReferralDollars(totalReferrelDollars + referralPoints);
								primaryCustLoyalty.setLastReferralDollars(referralPoints);
								primaryCustLoyalty.setActivePointsAsDouble(primaryCustLoyalty.getActivePointsAsDouble() + referralPoints);
								primaryCustLoyalty.setTotalEarnedPointsAsDouble(primaryCustLoyalty.getTotalEarnedPointsAsDouble() + referralPoints);
								primaryCustLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
								primaryCustLoyalty.setLastUpdate(new Date());
								primaryCustLoyalty.setLatestEarnedPointsAsDouble(referralPoints);
								primaryCustLoyalty.setLatestSpentPointsAsDouble(0.00); 
								primaryCustLoyalty.setNotifiedMessage("2018-YEAREND-REFERRAL-CREDITS: "+referralPoints);
								
								DAORegistry.getCustomerLoyaltyHistoryDAO().save(history);
								DAORegistry.getCustomerLoyaltyDAO().update(primaryCustLoyalty);
								
								try {
									CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(primaryCustId,primaryCustLoyalty.getActivePointsAsDouble());
								}catch(Exception e) {
									e.printStackTrace();
								}
								
								CustomerLoyaltyTracking tireOneCustBonus = new CustomerLoyaltyTracking();
								tireOneCustBonus.setContestId(contest.getId());
								tireOneCustBonus.setCreatedBy("2018-YEAREND-REWARD-REFERRAL-CREDITS");
								tireOneCustBonus.setCreatedDate(new Date());
								tireOneCustBonus.setCustomerId(participantCustId);
								tireOneCustBonus.setRewardPoints(referralPoints);
								tireOneCustBonus.setRewardType("CONTEST");
								DAORegistry.getCustomerLoyaltyTrackingDAO().save(tireOneCustBonus);
								
								tracking.setYearEndRewardApplied(true);
								tracking.setUpdatedDateTime(new Date());
								QuizDAORegistry.getQuizCustomerReferralTrackingDAO().update(tracking);
								
							}
						}
						
						CustomerLoyaltyTracking rewardTrackingObj = new CustomerLoyaltyTracking();
						rewardTrackingObj.setContestId(contest.getId());
						rewardTrackingObj.setCreatedBy("2018-YEAREND-PARTICIPANT-CREDIT");
						rewardTrackingObj.setCreatedDate(new Date());
						rewardTrackingObj.setCustomerId(participantCustId);
						rewardTrackingObj.setRewardPoints(participantPoints);
						rewardTrackingObj.setRewardType("CONTEST");
						
						Double newRewards = participantPoints + referralPoints;
						
						newRewards = TicketUtil.getRoundedUpValue(newRewards);
						
						participantLoyalty.setActivePointsAsDouble(participantLoyalty.getActivePointsAsDouble() + newRewards);
						participantLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
						participantLoyalty.setLastUpdate(new Date());
						participantLoyalty.setLatestEarnedPointsAsDouble(newRewards);
						participantLoyalty.setLatestSpentPointsAsDouble(0.00);
						participantLoyalty.setTotalEarnedPoints(participantLoyalty.getTotalEarnedPoints()+newRewards);
						participantLoyalty.setNotifiedMessage("2018-YEAREND-PARTICIPANT-CREDIT: "+newRewards);
						
						 
						DAORegistry.getCustomerLoyaltyTrackingDAO().save(rewardTrackingObj);
						DAORegistry.getCustomerLoyaltyDAO().update(participantLoyalty);
						
						try {
							CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(participantCustId,participantLoyalty.getActivePointsAsDouble());
						}catch(Exception e) {
							e.printStackTrace();
						}
						
						/*try {
							Customer customer = CustomerUtil.getCustomerById(participantCustId);
							Integer customerLives = customer.getQuizCustomerLives();
							if(customerLives == null) {
								customerLives=0;
							}
							
							customerLives = customerLives + 5;
							 
							customer.setQuizCustomerLives(customerLives);
							DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
							CustomerUtil.updatedCustomerUtil(customer);
							
							//QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
							
						}catch(Exception e) {
							e.printStackTrace();
						}*/
					}
					
					credit = new ContestReferrerRewardCredit();
					credit.setContestId(contest.getId());
					credit.setCreatedDate(start);
					credit.setIsNotified(true);
					credit.setNotifiedTime(new Date());
					credit.setRewardCreditConv(primaryUserEarnPerc);
					credit.setReferralProgramType("2018_YEAREND_REWARDS");
					credit.setStatus("COMPLETED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
					
					resMsg = "CRRN-V2: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", ReferralReward: "+primaryUserEarnPerc+", Process Completed: "+new Date();
					logger.info(resMsg);
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			resMsg = "CRRN-V2: Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date();
			logger.info(resMsg);
		}
	}catch(Exception e){
		e.printStackTrace();
	}}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				//December 31st is the last day for this program
				
				//init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}