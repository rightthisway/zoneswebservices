package com.rtfquiz.webservices.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizContest;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * QuizGrandWinnerTicketsUtil
 * @author KUlaganathan
 *
 */
public class QuizGrandWinnerTicketsUpcomingUtil extends QuartzJobBean implements StatefulJob{
	
	private static Logger logger = LoggerFactory.getLogger(QuizGrandWinnerTicketsUpcomingUtil.class);
	static MailManager mailManager = null;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizGrandWinnerTicketsUpcomingUtil.mailManager = mailManager;
	}
	
	public static void main(String[] args) {
		
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		toDate.add(Calendar.HOUR, 36);
		
		System.out.println("Contest From: "+fromDate.getTime());
		System.out.println("Condition TO: "+toDate.getTime());
		 
	}
 
	
	public void init() throws Exception{
		
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		toDate.add(Calendar.HOUR, 12);
		
		System.out.println("GRAND WINNER TICKETS JOB:- Begins: "+new Date());
		Collection<QuizContest> contestList = QuizDAORegistry.getQuizContestDAO().getActiveContestByDates(fromDate.getTime(),toDate.getTime());
		for (QuizContest contest : contestList) {
			try {
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", PROC BEGIN TIME: "+new Date());
				Boolean isDone = DAORegistry.getEventDAO().callContestGrandWinnerProcedure(contest.getId(),0);
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", PROC END TIME: "+new Date());
			}catch(Exception e) {
				e.printStackTrace();
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", EXCEPTION: "+new Date());
			}
		}
		System.out.println("GRAND WINNER TICKETS JOB Ends: "+new Date());
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}