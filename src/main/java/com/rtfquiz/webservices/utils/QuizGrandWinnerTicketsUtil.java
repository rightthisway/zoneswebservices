package com.rtfquiz.webservices.utils;

import java.util.Collection;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.QuizContest;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * QuizGrandWinnerTicketsUtil
 * @author KUlaganathan
 *
 */
public class QuizGrandWinnerTicketsUtil extends QuartzJobBean implements StatefulJob{
	
	private static Logger logger = LoggerFactory.getLogger(QuizGrandWinnerTicketsUtil.class);
	static MailManager mailManager = null;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizGrandWinnerTicketsUtil.mailManager = mailManager;
	}

	/*public void initOLd() throws Exception{
		System.out.println("GRAND WINNER TICKETS JOB Begins: "+new Date());
		List<ContestGrandWinner> grandWinners = QuizDAORegistry.getContestGrandWinnerDAO().getAllActiveGrandWinnerContest();
		for (ContestGrandWinner contestGrandWinner : grandWinners) {
			try {
				QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestGrandWinner.getContestId());
				System.out.println("CONTESTID: "+contestGrandWinner.getContestId()+", PROC BEGIN TIME: "+new Date());
				Boolean isDone = DAORegistry.getEventDAO().callContestGrandWinnerProcedure(contest.getId(),0);
				System.out.println("CONTESTID: "+contestGrandWinner.getContestId()+", PROC END TIME: "+new Date());
			}catch(Exception e) {
				e.printStackTrace();
				System.out.println("CONTESTID: "+contestGrandWinner.getContestId()+", EXCEPTION: "+new Date());
			}
		}
		System.out.println("GRAND WINNER TICKETS JOB Ends: "+new Date());
	}*/
	
	public void init() throws Exception{
		System.out.println("GRAND WINNER TICKETS JOB:- Begins: "+new Date());
		Collection<QuizContest> contestList = QuizDAORegistry.getQuizContestDAO().getRecentQuizContest();
		for (QuizContest contest : contestList) {
			ContestGrandWinner winner = QuizDAORegistry.getContestGrandWinnerDAO().getOrderNotCreatedGrandWinner(contest.getId());
			if(winner != null) {
				try {
					Integer eventCount = DAORegistry.getEventDAO().getContestEventsCount(contest.getId());
					if(eventCount <= 0) {
						continue;	
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					DAORegistry.getEventDAO().callContestGrandWinnerProcedure(contest.getId(),1);
				}catch(Exception e) {
					e.printStackTrace();
					System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", EXCEPTION: "+new Date());
				}
				continue;
			}
			
			if(contest.getContestName().toLowerCase().contains("test") || contest.getStatus().equals("DELETED")) {
				continue;
			}
			try {
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", PROC BEGIN TIME: "+new Date());
				Boolean isDone = DAORegistry.getEventDAO().callContestGrandWinnerProcedure(contest.getId(),0);
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", PROC END TIME: "+new Date());
			}catch(Exception e) {
				e.printStackTrace();
				System.out.println("GRAND WINNER TICKETS JOB:- CONTESTID: "+contest.getId()+", EXCEPTION: "+new Date());
			}
		}
		System.out.println("GRAND WINNER TICKETS JOB Ends: "+new Date());
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				//init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}