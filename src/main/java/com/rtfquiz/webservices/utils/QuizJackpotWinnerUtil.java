package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.enums.WinnerStatus;

/**
 * QuizJackpotWinnerUtil
 * @author KUlaganathan
 *
 */
public class QuizJackpotWinnerUtil {
	
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
	private static Logger logger = LoggerFactory.getLogger(QuizJackpotWinnerUtil.class);
	public static boolean isRunning = false;
	 
	
	public static void creditJackpotWinnerBenefits(QuizContest contest) throws Exception{
		logger.info("Initiating the email scheduler process for computing super fan stats....");
		try {
			Date start = new Date();
			String resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Started: "+new Date();	
			try {
				logger.info(resMsg);
				System.out.println(resMsg);
				ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getRewardCreditByContestIdAndType(contest.getId(),"JACKPOT_WINNERS_CREDIT");
				if(null != credit) {
					resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: Already processed contest.";
					logger.info(resMsg);
					System.out.println(resMsg);
					return;
				}else {
					credit = new ContestReferrerRewardCredit();
					credit.setContestId(contest.getId());
					credit.setCreatedDate(start);
					credit.setIsNotified(true);
					credit.setNotifiedTime(new Date());
					credit.setRewardCreditConv(0.00);
					credit.setReferralProgramType("JACKPOT_WINNERS_CREDIT");
					credit.setStatus("STARTED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
				}
				
				List<ContestGrandWinner> contestGrandWinnerList = QuizDAORegistry.getContestGrandWinnerDAO().getAllJackpotWinnersByContestId(contest.getId());
				
				if(null == contestGrandWinnerList || contestGrandWinnerList.isEmpty()) {
					resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: No Grand Winner Found.";
					logger.info(resMsg);
					System.out.println(resMsg);
					credit.setStatus("COMPLETED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
					return;
				}
				
				String notes = "";
				for (ContestGrandWinner winner : contestGrandWinnerList) {
					boolean updateWinnerObj = true; 
					try {
						switch (winner.getJackpotCreditType()) {
							case GIFTCARD:
								notes = QuizMiniJackpotWinnerCredit.createJackpotGiftCardOrder(winner);
								break;	
							case TICKET:
								updateWinnerObj = false;
								break;		
								
							case SUPERFANSTAR:
								notes = QuizMiniJackpotWinnerCredit.addSuperFanStarsToJackpotWinner(winner);
								break;	
								
							case LIFELINE:
								notes = QuizMiniJackpotWinnerCredit.addLifeLineToJackpotWinner(winner);
								break;
								
							case REWARD:
								notes = QuizMiniJackpotWinnerCredit.addRewardsToJackpotWinner(winner);
								break;
								
							case RTFPOINTS:
								notes = QuizMiniJackpotWinnerCredit.addRtfPointsToJackpotWinner(winner);
								break;
								
							case NONE:
								updateWinnerObj = false;
								break;
		
							default:
								updateWinnerObj = false;
								break;
						}
						
						if(updateWinnerObj) {
							System.out.println(notes);
							winner.setNotes(notes);
							winner.setNotifiedTime(start);
							winner.setStatus(WinnerStatus.PROCESSED);
							winner.setIsNotified(true);
							QuizDAORegistry.getContestGrandWinnerDAO().update(winner);
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				credit.setStatus("COMPLETED");
				credit.setUpdatedDate(new Date());
				QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
				resMsg = "QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Process Completed: "+new Date();
				logger.info(resMsg);
				System.out.println(resMsg);
				return ;
			}catch(Exception e) { 
				e.printStackTrace();
			}
			resMsg = "QUIZ JACKPOT WINNER Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date();
			logger.info(resMsg);
			System.out.println(resMsg);
		}catch(Exception e){
			e.printStackTrace();
		}
		return;
	}
	
	 
	
}