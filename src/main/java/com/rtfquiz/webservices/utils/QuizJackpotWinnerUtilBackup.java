package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.list.JackpotCreditResponse;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.ContestMigrationStats;

/**
 * QuizJackpotWinnerUtil
 * @author KUlaganathan
 *
 */
public class QuizJackpotWinnerUtilBackup {
	
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
	private static Logger logger = LoggerFactory.getLogger(QuizJackpotWinnerUtilBackup.class);
	public static boolean isRunning = false;
	 
	
	public JackpotCreditResponse init(QuizContest contest,JackpotCreditResponse responseObj) throws Exception{
		logger.info("Initiating the email scheduler process for computing super fan stats....");
		try {
			if(isRunning) {
				responseObj.setMsg("Fail: Jackpot Credit Job is already running.!");
				responseObj.setSts(0);
				return responseObj;
			}
			isRunning = true;
			 
			Calendar fromDate = Calendar.getInstance();
			fromDate.add(Calendar.MINUTE, -60);
			Date start = new Date();
			String resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Started: "+new Date();	
			try {
				logger.info(resMsg);
				List<ContestMigrationStats> contestMigrationStatList = DAORegistry.getContestMigrationStatsDAO().getAllStatsByContestId(contest.getId());
				if(null == contestMigrationStatList || contestMigrationStatList.isEmpty() || contestMigrationStatList.size() < 2) {
					resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
					logger.info(resMsg);
					isRunning = false;
					responseObj.setMsg("Fail: Migration not completed yet for this contest");
					responseObj.setSts(0);
					return responseObj;
				}
				
				boolean isCompleted = true;
				for (ContestMigrationStats obj : contestMigrationStatList) {
					if(obj.getStatus().equals("RUNNING")) {
						isCompleted = false;
						continue;
					}
				}
				
				if(!isCompleted) {
					resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
					logger.info(resMsg);
					isRunning = false;
					responseObj.setMsg("Fail: Migration not completed yet for this contest");
					responseObj.setSts(0);
					return responseObj;
				}
				
				ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getRewardCreditByContestIdAndType(contest.getId(),"JACKPOT_WINNERS_CREDIT");
				
				if(null != credit) {
					resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: Already processed contest.";
					logger.info(resMsg);
					isRunning = false;
					responseObj.setMsg("Fail: Jackpot Winners Credit was done already.");
					responseObj.setSts(0);
					return responseObj;
				}else {
					credit = new ContestReferrerRewardCredit();
					credit.setContestId(contest.getId());
					credit.setCreatedDate(start);
					credit.setIsNotified(true);
					credit.setNotifiedTime(new Date());
					credit.setRewardCreditConv(0.00);
					credit.setReferralProgramType("JACKPOT_WINNERS_CREDIT");
					credit.setStatus("STARTED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
				}
				
				List<ContestGrandWinner> contestGrandWinnerList = QuizDAORegistry.getContestGrandWinnerDAO().getAllJackpotWinnersByContestId(contest.getId());
				
				if(null == contestGrandWinnerList || contestGrandWinnerList.isEmpty()) {
					resMsg ="QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: No Grand Winner Found.";
					logger.info(resMsg);
					credit.setStatus("COMPLETED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
					isRunning = false;
					responseObj.setMsg("Fail: There is no Jackpot Winners..");
					responseObj.setSts(0);
					return responseObj;
				}
				
				String notes = "";
				for (ContestGrandWinner winner : contestGrandWinnerList) {
					boolean updateWinnerObj = true; 
					try {
						switch (winner.getJackpotCreditType()) {
							case GIFTCARD:
								notes = QuizMiniJackpotWinnerCredit.createJackpotGiftCardOrder(winner);
								break;	
							case TICKET:
								//User have to choose tickets from mobile app.
								updateWinnerObj = false;
								break;		
								
							case SUPERFANSTAR:
								notes = QuizMiniJackpotWinnerCredit.addSuperFanStarsToJackpotWinner(winner);
								break;	
								
							case LIFELINE:
								notes = QuizMiniJackpotWinnerCredit.addLifeLineToJackpotWinner(winner);
								break;
								
							case REWARD:
								notes = QuizMiniJackpotWinnerCredit.addRewardsToJackpotWinner(winner);
								break;
								
							case NONE:
								updateWinnerObj = false;
								break;
		
							default:
								updateWinnerObj = false;
								break;
						}
						
						if(updateWinnerObj) {
							System.out.println(notes);
							winner.setNotes(notes);
							winner.setNotifiedTime(start);
							winner.setStatus(WinnerStatus.PROCESSED);
							winner.setIsNotified(true);
							QuizDAORegistry.getContestGrandWinnerDAO().update(winner);
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				credit.setStatus("COMPLETED");
				credit.setUpdatedDate(new Date());
				QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
				resMsg = "QUIZ JACKPOT WINNER Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Process Completed: "+new Date();
				logger.info(resMsg);
				isRunning = false;
				responseObj.setMsg("Success: Jackpot Credits are successfully given");
				responseObj.setSts(1);
				responseObj.setjWinList(contestGrandWinnerList);
				return responseObj;
			}catch(Exception e) { 
				e.printStackTrace();
			}
			resMsg = "QUIZ JACKPOT WINNER Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date();
			logger.info(resMsg);
		}catch(Exception e){
			e.printStackTrace();
			isRunning = false;
			responseObj.setMsg("Fail: Exception occured while crediting jackpot credits.!");
			responseObj.setSts(0);
		}
		isRunning = false;
		return responseObj;
	}
	
	 
	
}