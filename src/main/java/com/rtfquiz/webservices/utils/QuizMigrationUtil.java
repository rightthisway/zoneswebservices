package com.rtfquiz.webservices.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerContestDetails;
import com.rtfquiz.webservices.enums.ContestQuestRewardType;
import com.rtfquiz.webservices.sqldao.implementation.CustomerContestAnswersSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.CustomerContestDetailsSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.CustomerContestStatsCassandraSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.CustomerLoyaltyRewardInfoSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.CustomerLoyaltyTrackingSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.CustomerSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.SQLDaoUtil;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCassandra;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;

/**
 * QuizCreditUtil - Credit Reward Dollars, Super Fan Stars, Life lines to the customers.
 * @author KUlaganathan
 *
 */
public class QuizMigrationUtil{
	
	private static Logger logger = LoggerFactory.getLogger(QuizMigrationUtil.class);
	public static int maxBatchIndex = 1000;
	
	public static void processCustContestAnswerstoSql(Integer contestId) throws Exception {
		try {
			Date createDate = new Date();
			int processCount = 0;
			processCount = 0;
			QuizCustomerContestAnswers customerAnswers = null;
			List<QuizCustomerContestAnswers> toBeSavedCustAnsList = new ArrayList<QuizCustomerContestAnswers>();
			Boolean isQuestPoints = false;
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			if(contest.getQuestionRewardType() != null && contest.getQuestionRewardType().equals(ContestQuestRewardType.POINTS)) {
				isQuestPoints = true;
			}
			List<CustContAnswers> cassCustAnsList = CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContId(contestId);
			for (CustContAnswers obj : cassCustAnsList) {
				processCount++;
				try {
					customerAnswers = new QuizCustomerContestAnswers();
					customerAnswers.setCustomerId(obj.getCuId());
					customerAnswers.setContestId(contestId);
					customerAnswers.setQuestionId(obj.getqId());
					customerAnswers.setQuestionSNo(obj.getqNo());
					if(null != obj.getAns()) {
						customerAnswers.setAnswer(obj.getAns());
					}
					customerAnswers.setIsCorrectAnswer(obj.getIsCrt());
					customerAnswers.setCreatedDateTime(new Date(obj.getCrDate()));
					if(null != obj.getUpDate()) {
						customerAnswers.setUpdatedDateTime(new Date(obj.getUpDate()));
					}
					customerAnswers.setIsLifeLineUsed(obj.getIsLife());
					customerAnswers.setCumulativeRewards(obj.getCuRwds());
					customerAnswers.setCumulativeLifeLineUsed(obj.getCuLife());
					
					if(isQuestPoints) {
						customerAnswers.setRtfPoints(obj.getaRwds().intValue());
					} else {
						customerAnswers.setAnswerRewards(null != obj.getaRwds()?obj.getaRwds():null);
					}
					try {
						customerAnswers.setRetryCount(obj.getRetryCount());
						customerAnswers.setIsAutoCreated(obj.getIsAutoCreated());
						if(null != obj.getFbCallbackTime() && !obj.getFbCallbackTime().isEmpty()) {
							customerAnswers.setFbCallbackTime(new Date(Long.valueOf(obj.getFbCallbackTime())));
						}
						if(null != obj.getAnswerTime() && !obj.getAnswerTime().isEmpty()) {
							customerAnswers.setAnswerTime(new Date(Long.valueOf(obj.getAnswerTime())));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					toBeSavedCustAnsList.add(customerAnswers);
					
					if(processCount % maxBatchIndex == 0) {
						CustomerContestAnswersSQLDAO.saveAllCustomerContestAnswers(toBeSavedCustAnsList);
						toBeSavedCustAnsList = new ArrayList<>();
					}
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(null != toBeSavedCustAnsList && !toBeSavedCustAnsList.isEmpty()) {
				try {
					CustomerContestAnswersSQLDAO.saveAllCustomerContestAnswers(toBeSavedCustAnsList);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
 
	
	public static void processCustomerCassandraTableData(List<CustomerCassandra> custCassandraList) {
		int processCount = 0;
		List<CustomerCassandra> tempList = new ArrayList<>();
		for (CustomerCassandra object : custCassandraList) {
			try {
				tempList.add(object);
				processCount++;
				if(processCount % maxBatchIndex == 0) {
					CustomerContestStatsCassandraSQLDAO.saveAllContestStats(tempList);
					tempList = new ArrayList<>();
					processCount =0;
				}	
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(null != tempList && !tempList.isEmpty() && tempList.size() > 0) {
			try {
				CustomerContestStatsCassandraSQLDAO.saveAllContestStats(tempList); 
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}

	}
	
	public static void processLoyaltyTrackingTableData(List<CustomerLoyaltyTracking> custLoyaltyTrackingList) {
		int processCount = 0;
		List<CustomerLoyaltyTracking> tempList = new ArrayList<>();
		for (CustomerLoyaltyTracking object : custLoyaltyTrackingList) {
			try {
				tempList.add(object);
				processCount++;
				if(processCount % maxBatchIndex == 0) {
					CustomerLoyaltyTrackingSQLDAO.saveAllLoyaltyTracking(tempList); 
					tempList = new ArrayList<>();
					processCount =0;
				}	
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if(null != tempList && !tempList.isEmpty() && tempList.size() > 0) {
			try {
				CustomerLoyaltyTrackingSQLDAO.saveAllLoyaltyTracking(tempList); 
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
	}
 
	
	public static void updateContestCustomerDatainBatch(List<Customer> custList) {
		/*try {
			System.out.println("ContestMigration- Batch Update CustomerCassandraTable(insert) Started at "+new Date());
			SQLDaoUtil.saveAllContestStats(custCassandraList);
			System.out.println("ContestMigration- Batch Update CustomerCassandraTable(insert) Successfully Ends at "+new Date());
		}catch(Exception e) {
			System.out.println("ContestMigration- Batch Update CustomerCassandraTable(insert) Exception Occurred at "+new Date());
			e.printStackTrace();
		}*/
		
		/*try {
			System.out.println("ContestMigration- Batch Update CustomerLoyaltyTracking(insert) Started at "+new Date());
			SQLDaoUtil.saveAllLoyaltyTracking(custLoyaltyTrackingList);
			System.out.println("ContestMigration- Batch Update CustomerLoyaltyTracking(insert) Successfully Ends at "+new Date());
		}catch(Exception e) {
			System.out.println("ContestMigration- Batch Update CustomerLoyaltyTracking(insert) Exception Occurred at "+new Date());
			e.printStackTrace();
		}*/
		System.out.println("ContestMigration- Batch Update Starts at "+new Date());
		try {
			//System.out.println("ContestMigration- Batch Update Customer(Update) Started at "+new Date());
			//SQLDaoUtil.updateCustomerLives(custList);
			CustomerSQLDAO.updateCustomerLivesAndRtfPoints(custList);		
			//System.out.println("ContestMigration- Batch Update Customer(Update) Successfully Ends at "+new Date());
		}catch(Exception e) {
			System.out.println("ContestMigration- Batch Update Customer(Update) Exception Occurred at "+new Date());
			e.printStackTrace();
		}
		
		try {
			//System.out.println("ContestMigration- Batch Update CustomerLoyalty(Update) Started at "+new Date());
			CustomerLoyaltyRewardInfoSQLDAO.updateCustomerRewards(custList);
			//System.out.println("ContestMigration- Batch Update CustomerLoyalty(Update) Successfully Ends at "+new Date());
		}catch(Exception e) {
			System.out.println("ContestMigration- Batch Update CustomerLoyalty(Update) Exception Occurred at "+new Date());
			e.printStackTrace();
		}
		
		//System.out.println("ContestMigration- Batch Update CustomerCassandraObject(Get & Update) Started at "+new Date());
		for (Customer custObj : custList) {
			try {
				//custObj = SQLDaoUtil.getCustomerData(custObj);
				//CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(custObj.getId(), custObj.getQuizCustomerLives(), custObj.getRewardDollar());
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLivesAndPoints(custObj.getId(), custObj.getQuizCustomerLives(), custObj.getRewardDollar(),custObj.getRtfPoints());
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		//System.out.println("ContestMigration- Batch Update CustomerCassandraObject(Get & Update) Completed at "+new Date());
		
		/*try {
			System.out.println("ContestMigration- Batch Update CustomerCassandraObject(Update) Started at "+new Date());
			CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(custList);
			System.out.println("ContestMigration- Batch Update CustomerCassandraObject(Update) Successfully Ends at "+new Date());
		}catch(Exception e) {
			System.out.println("ContestMigration- Batch Update CustomerCassandraObject(Update) Exception Occurred at "+new Date());
			e.printStackTrace();
		}*/
		System.out.println("ContestMigration- Batch Update Ends at "+new Date());
	}
	
	public static void migrateCustContestDetailsToSQL(Integer contestId) throws Exception {
		try {
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			List<CustContDtls> custContDetailsList = CassandraDAORegistry.getCustContDtlsDAO().getAllCustContDtls();
			QuizCustomerContestDetails quizCustContDetails = null; 
			List<QuizCustomerContestDetails> tempList = new ArrayList<QuizCustomerContestDetails>();
			int processCount = 0;
			Boolean isQuestPoints = false;
			if(contest.getQuestionRewardType() != null && contest.getQuestionRewardType().equals(ContestQuestRewardType.POINTS)) {
				isQuestPoints = true;
			}
			for (CustContDtls custContDtls : custContDetailsList) {
				try {
					quizCustContDetails = new QuizCustomerContestDetails();
					quizCustContDetails.setCuId(custContDtls.getCuId());
					quizCustContDetails.setCoId(custContDtls.getCoId());
					quizCustContDetails.setLqNo(custContDtls.getLqNo());
					quizCustContDetails.setIsLqCrt(custContDtls.getIsLqCrt());
					quizCustContDetails.setIsLqLife(custContDtls.getIsLqLife());
					quizCustContDetails.setCuLife(custContDtls.getCuLife());
					quizCustContDetails.setCreatedDate(new Date());
					if(isQuestPoints) {
						quizCustContDetails.setRtfPoints(custContDtls.getCuRwds().intValue());
					} else {
						quizCustContDetails.setCuRwds(custContDtls.getCuRwds());
					}
					
					tempList.add(quizCustContDetails);
					processCount++;
					//QuizDAORegistry.getQuizCustomerContestDetailsDAO().save(quizCustContDetails);
					
					if(processCount % maxBatchIndex == 0) {
						CustomerContestDetailsSQLDAO.saveAllCustContestDetails(tempList);
						tempList = new ArrayList<QuizCustomerContestDetails>();
						processCount = 0;
					}
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(processCount > 0) {
				CustomerContestDetailsSQLDAO.saveAllCustContestDetails(tempList);
				tempList = new ArrayList<QuizCustomerContestDetails>();
				processCount = 0;
			}
			
			//System.out.println("Started Truncate RTF API TRACKING Start: "+new Date());
			//CassandraDAORegistry.getCustContDtlsDAO().truncate();
			//System.out.println("Started Truncate RTF API TRACKING END: "+new Date());
			
			int size = 0;
			if(custContDetailsList != null) {
				size = custContDetailsList.size();
			}
			System.out.println("Cust Contest Dtls Migrated  : "+size+" : "+ new Date());
			
		} catch(Exception e) {
			System.out.println("Error in Cust Contest Dtls Migration  : "+ new Date());
			e.printStackTrace();
		}
	}
	
}