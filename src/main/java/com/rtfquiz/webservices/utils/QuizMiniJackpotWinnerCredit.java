package com.rtfquiz.webservices.utils;

import java.util.Date;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.rtfquiz.webservices.data.QuizSuperFanTracking;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.RtfGiftCard;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.data.RtfGiftCardQuantity;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.utils.TicketUtil;

/**
 * QuizMiniJackpotWinnerCredit
 * @author KUlaganathan
 *
 */
 
public class QuizMiniJackpotWinnerCredit {
	
	 public static String createJackpotGiftCardOrder(ContestGrandWinner winner) {
		 String response = "";
		 try {
		 	ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(ApplicationPlatForm.ANDROID.toString());
			Integer customerId = winner.getCustomerId();
			QuizContestQuestions question = QuizDAORegistry.getQuizContestQuestionsDAO().get(winner.getQuestionId());
			Integer qty = question.getjQtyPerWinner().intValue();
			Integer gcId = question.getjGiftCardId();
			Customer customer = CustomerUtil.getCustomerById(customerId);;
			
			RtfGiftCardQuantity giftCardQty = DAORegistry.getRtfGiftCardQuantityDAO().get(gcId);
			
			RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(giftCardQty.getCardId());
			
			
			//LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			CustomerLoyalty customerRewards = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(customerRewards == null){
				customerRewards = new CustomerLoyalty();
				customerRewards.setActivePoints("0.00");
				customerRewards.setActivePointsAsDouble(0.00);
				customerRewards.setActiveRewardDollers(0.00);
				customerRewards.setContestRewardDollars("0.00");
				customerRewards.setCurRewardPointBalance("0.00");
				customerRewards.setCustomerId(customerId);
				customerRewards.setDollerConversion(1.00);
				customerRewards.setLastAffiliateReferralDollars(0.00);
				customerRewards.setLastReferralDollars(0.00);
				customerRewards.setLastUpdate(new Date());
				customerRewards.setLatestEarnedPoints("0.00");
				customerRewards.setLatestEarnedPointsAsDouble(0.00);
				customerRewards.setLatestSpentPointsAsDouble(0.00);
				customerRewards.setPendingPoints("0.00");
				customerRewards.setPendingPointsAsDouble(0.00);
				customerRewards.setRevertedPoints("0.00");
				customerRewards.setRevertedPointsAsDouble(0.00);
				customerRewards.setShowAffiliateReward(false);
				customerRewards.setShowReferralReward(true);
				customerRewards.setTotalAffiliateReferralDollars(0.00);
				customerRewards.setTotalAffiliateReferralDollarsStr("0.00");
				customerRewards.setTotalEarnedPoints("0.00");
				customerRewards.setTotalEarnedPointsAsDouble(0.00);
				customerRewards.setTotalReferralDollars(0.00);
				customerRewards.setTotalReferralDollarsStr("0.00");
				customerRewards.setTotalSpentPoints("0.00");
				customerRewards.setTotalSpentPointsAsDouble(0.00);
				customerRewards.setVoidedPoints("0.00");
				customerRewards.setVoidedPointsAsDouble(0.00);
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerRewards);
			}
			
			Double requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardDollarConvRate());
			
			RtfGiftCardOrder order  = new RtfGiftCardOrder();
			order.setShippingAddressId(-1);
			order.setCardDescription(card.getDescription());
			order.setCardId(giftCardQty.getId());
			order.setCardTitle(card.getTitle());
			order.setCreatedBy(customer.getUserId());
			order.setCreatedDate(new Date());
			order.setCustomerId(customerId);
			order.setIsEmailSent(false);
			order.setIsOrderFulfilled(false);
			order.setOrderType(OrderType.CONTEST);
			order.setOriginalCost(requiredAmount);
			order.setQuantity(qty);
			order.setRedeemedRewards(requiredAmount);
			order.setStatus("PENDING");
			
			
			Integer freeQty = giftCardQty.getFreeQty()-qty;
			Integer usedQty = giftCardQty.getUsedQty()+qty;
			
			giftCardQty.setFreeQty(freeQty);
			giftCardQty.setUsedQty(usedQty);
			
			DAORegistry.getRtfGiftCardOrderDAO().save(order);
			DAORegistry.getRtfGiftCardQuantityDAO().update(giftCardQty);
			
			response = "Gift Card Order Created Successfully. Order No: "+order.getId();
			
		 }catch(Exception e) {
			 e.printStackTrace();
			 response = "Alert: Exception occured while creating gift card order. Please check immediately.";
		 }
		return response;
	 }
	 
	 public static String addLifeLineToJackpotWinner(ContestGrandWinner winner) {
		 String response = "";
		 try {
			 QuizContestQuestions question = QuizDAORegistry.getQuizContestQuestionsDAO().get(winner.getQuestionId());
			 Customer customer = DAORegistry.getCustomerDAO().get(winner.getCustomerId());
			 int oldLifeLine = customer.getQuizCustomerLives();
			 customer.setQuizCustomerLives(customer.getQuizCustomerLives() + question.getjQtyPerWinner().intValue());
			 DAORegistry.getCustomerDAO().update(customer);
			 CustomerUtil.updatedCustomerUtil(customer);
			 CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(winner.getCustomerId(), customer.getQuizCustomerLives());
			 response = "Lifeline added successfully.  Old Lifeline: "+oldLifeLine+", Winner Lifeline: "+question.getjQtyPerWinner().intValue()+", New Lifeline: "+customer.getQuizCustomerLives();
		 }catch(Exception e) {
			 e.printStackTrace();
			 response = "Alert: Exception occured while adding lifeline to customer. Please check immediately.";
		 }
		 return response;
	 }
	 public static String addRtfPointsToJackpotWinner(ContestGrandWinner winner) {
		 String response = "";
		 try {
			 QuizContestQuestions question = QuizDAORegistry.getQuizContestQuestionsDAO().get(winner.getQuestionId());
			 Customer customer = DAORegistry.getCustomerDAO().get(winner.getCustomerId());
			 int OldrtfPoints = customer.getRtfPoints();
			 int jackpotPoints =0;
			 if(question.getjQtyPerWinner() != null) {
				 jackpotPoints = question.getjQtyPerWinner().intValue();
			 }
			 customer.setRtfPoints(OldrtfPoints + jackpotPoints);
			 
			 CustomerLoyaltyTracking participantLoyaltyTracking = new CustomerLoyaltyTracking();
			 participantLoyaltyTracking.setContestId(winner.getContestId());
			 participantLoyaltyTracking.setCreatedBy("JACKPOT-WINNER-RTFPOINTS-CID:"+winner.getContestId()+"-QID:"+question.getId());
			 participantLoyaltyTracking.setCreatedDate(new Date());
			 participantLoyaltyTracking.setCustomerId(winner.getCustomerId());
			 participantLoyaltyTracking.setRtfPoints(jackpotPoints);
			 participantLoyaltyTracking.setRewardType("CONTEST");
			
			 DAORegistry.getCustomerDAO().update(customer);
			 CustomerUtil.updatedCustomerUtil(customer);
			 CassandraDAORegistry.getCassCustomerDAO().updateCustomerRtfPointsByCustomerId(winner.getCustomerId(), customer.getRtfPoints());
			 DAORegistry.getCustomerLoyaltyTrackingDAO().save(participantLoyaltyTracking);
			 
			 response = "RtfPoints added successfully.  Old RtfPoint: "+OldrtfPoints+", Winner RtfPoint: "+jackpotPoints+", New RtfPoint: "+customer.getRtfPoints();
		 }catch(Exception e) {
			 e.printStackTrace();
			 response = "Alert: Exception occured while adding RtfPoint to customer. Please check immediately.";
		 }
		 return response;
	 }
	 public static String addRewardsToJackpotWinner(ContestGrandWinner winner) {
		 String response = "";
		 try {
			 QuizContestQuestions question = QuizDAORegistry.getQuizContestQuestionsDAO().get(winner.getQuestionId());
			 Double toBeCreditedReward = question.getjQtyPerWinner();
			 CustomerLoyalty custLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(winner.getCustomerId());
			 if(custLoyalty != null) {
				Double oldRewards = custLoyalty.getActivePointsAsDouble();
				CustomerLoyaltyTracking participantLoyaltyTracking = new CustomerLoyaltyTracking();
				participantLoyaltyTracking.setContestId(winner.getContestId());
				participantLoyaltyTracking.setCreatedBy("JACKPOT-WINNER-REWARD-CID:"+winner.getContestId()+"-QID:"+question.getId());
				participantLoyaltyTracking.setCreatedDate(new Date());
				participantLoyaltyTracking.setCustomerId(winner.getCustomerId());
				participantLoyaltyTracking.setRewardPoints(toBeCreditedReward);
				participantLoyaltyTracking.setRewardType("CONTEST");
				
				custLoyalty.setActivePointsAsDouble(custLoyalty.getActivePointsAsDouble() + toBeCreditedReward);
				custLoyalty.setDollerConversion(1.00);
				custLoyalty.setLastUpdate(new Date());
				custLoyalty.setLatestEarnedPointsAsDouble(toBeCreditedReward);
				custLoyalty.setLatestSpentPointsAsDouble(0.00);
				custLoyalty.setTotalEarnedPoints(custLoyalty.getTotalEarnedPoints()+toBeCreditedReward);
				custLoyalty.setNotifiedMessage("JACKPOT-WINNER-REWARD: "+toBeCreditedReward);
				 
				DAORegistry.getCustomerLoyaltyTrackingDAO().save(participantLoyaltyTracking);
				DAORegistry.getCustomerLoyaltyDAO().update(custLoyalty);
				
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(winner.getCustomerId(),custLoyalty.getActivePointsAsDouble());
				}catch(Exception e) {
					e.printStackTrace();
				}
				response = "Rewards added successfully.  Old Rewards: "+oldRewards+", Winner Rewards: "+toBeCreditedReward+", New Rewards: "+custLoyalty.getActivePointsAsDouble();
			 }else {
				response = "Alert: Rewards not added. Reason: No Record Found in CustomerLoyalty Table for this customer.";
			 }
			 
		 }catch(Exception e) {
			 e.printStackTrace();
			 response = "Alert: Exception occured while adding rewards to customer. Please check immediately.";
		 }
		 return response;
	 }
	 
	 public static String addSuperFanStarsToJackpotWinner(ContestGrandWinner winner) {
		 String response = "";
		 try {
			 QuizContestQuestions question = QuizDAORegistry.getQuizContestQuestionsDAO().get(winner.getQuestionId());
			 
			 Date start = new Date();
			 Integer toBeCreditedStars = question.getjQtyPerWinner().intValue();
			 
			 QuizSuperFanStat superFanObj = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(winner.getCustomerId());
			 
			 Integer oldStars = 0,newStars=0,oldParticipantStars=0,oldReferralStars=0;
			 
			if(null == superFanObj) {
				superFanObj = new QuizSuperFanStat();
				superFanObj.setCreatedDate(start);
				superFanObj.setCustomerId(winner.getCustomerId());
				superFanObj.setNoOfGamePlayed(1);
				superFanObj.setParticipationChances(toBeCreditedStars);
				superFanObj.setReferralChances(0);
				superFanObj.setStatus("ACTIVE");
				superFanObj.setTotalNoOfChances(toBeCreditedStars);
				superFanObj.setUpdatedDate(start);
				
				oldStars = 0;
				newStars = oldStars + toBeCreditedStars;
				oldParticipantStars= 0;
				oldReferralStars = 0;
				
			}else {
				
				oldStars = superFanObj.getTotalNoOfChances();
				newStars = oldStars + toBeCreditedStars;
				oldParticipantStars= superFanObj.getParticipationChances();
				oldReferralStars = superFanObj.getReferralChances();
				
				superFanObj.setNoOfGamePlayed(superFanObj.getNoOfGamePlayed() + 1);
				superFanObj.setParticipationChances(superFanObj.getParticipationChances() + toBeCreditedStars);
				superFanObj.setTotalNoOfChances(superFanObj.getTotalNoOfChances() + toBeCreditedStars);
				superFanObj.setStatus("ACTIVE");
				superFanObj.setUpdatedDate(start);
			}
			try {
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerSuperFanTotalChances(winner.getCustomerId(), superFanObj.getTotalNoOfChances());
			}catch(Exception e) {
				e.printStackTrace();
			}
			QuizDAORegistry.getQuizSuperFanStatDAO().saveOrUpdate(superFanObj);
			
			try {
				QuizSuperFanTracking quizSuperFanTracking = new QuizSuperFanTracking();
				quizSuperFanTracking.setThisContestReferralStars(0);
				quizSuperFanTracking.setContestId(winner.getContestId());
				quizSuperFanTracking.setCreatedDate(start);
				quizSuperFanTracking.setCustomerId(winner.getCustomerId());
				quizSuperFanTracking.setOldSuperFanStars(oldStars);
				quizSuperFanTracking.setNewSuperFanStars(newStars);
				quizSuperFanTracking.setNotes("");
				quizSuperFanTracking.setOldParticipantStars(oldParticipantStars);
				quizSuperFanTracking.setOldReferralStars(oldReferralStars);
				quizSuperFanTracking.setThisContestParticipantStars(toBeCreditedStars);
				QuizDAORegistry.getQuizSuperFanTrackingDAO().saveOrUpdate(quizSuperFanTracking);
			}catch(Exception e) {
				e.printStackTrace();
			}
			response = "Super Fan Stars added successfully.  Old Stars: "+oldStars+", Winner Stars: "+toBeCreditedStars+", New Stars: "+newStars;
		 }catch(Exception e) {
			e.printStackTrace();
			response = "Alert: Exception occured while adding super fan stars to customer. Please check immediately.";
		 }
		 return response;
	 }
	 
	 
	 public static String createGiftCardOrder(QuizContest contest,Integer customerId) {
		 String response = "";
		 try {
			Integer qty = contest.getGiftCardPerWinner();
			Integer gcId = contest.getGiftCardValueId();
			Customer customer = CustomerUtil.getCustomerById(customerId);;
			RtfGiftCardQuantity giftCardQty = DAORegistry.getRtfGiftCardQuantityDAO().get(gcId);
			RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(giftCardQty.getCardId());
			CustomerLoyalty customerRewards = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			if(customerRewards == null){
				customerRewards = new CustomerLoyalty();
				customerRewards.setActivePoints("0.00");
				customerRewards.setActivePointsAsDouble(0.00);
				customerRewards.setActiveRewardDollers(0.00);
				customerRewards.setContestRewardDollars("0.00");
				customerRewards.setCurRewardPointBalance("0.00");
				customerRewards.setCustomerId(customerId);
				customerRewards.setDollerConversion(1.00);
				customerRewards.setLastAffiliateReferralDollars(0.00);
				customerRewards.setLastReferralDollars(0.00);
				customerRewards.setLastUpdate(new Date());
				customerRewards.setLatestEarnedPoints("0.00");
				customerRewards.setLatestEarnedPointsAsDouble(0.00);
				customerRewards.setLatestSpentPointsAsDouble(0.00);
				customerRewards.setPendingPoints("0.00");
				customerRewards.setPendingPointsAsDouble(0.00);
				customerRewards.setRevertedPoints("0.00");
				customerRewards.setRevertedPointsAsDouble(0.00);
				customerRewards.setShowAffiliateReward(false);
				customerRewards.setShowReferralReward(true);
				customerRewards.setTotalAffiliateReferralDollars(0.00);
				customerRewards.setTotalAffiliateReferralDollarsStr("0.00");
				customerRewards.setTotalEarnedPoints("0.00");
				customerRewards.setTotalEarnedPointsAsDouble(0.00);
				customerRewards.setTotalReferralDollars(0.00);
				customerRewards.setTotalReferralDollarsStr("0.00");
				customerRewards.setTotalSpentPoints("0.00");
				customerRewards.setTotalSpentPointsAsDouble(0.00);
				customerRewards.setVoidedPoints("0.00");
				customerRewards.setVoidedPointsAsDouble(0.00);
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerRewards);
			}
			
			Double requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardDollarConvRate());
			
			RtfGiftCardOrder order  = new RtfGiftCardOrder();
			order.setShippingAddressId(-1);
			order.setCardDescription(card.getDescription());
			order.setCardId(giftCardQty.getId());
			order.setCardTitle(card.getTitle());
			order.setCreatedBy(customer.getUserId());
			order.setCreatedDate(new Date());
			order.setCustomerId(customerId);
			order.setIsEmailSent(false);
			order.setIsOrderFulfilled(false);
			order.setOrderType(OrderType.CONTEST);
			order.setOriginalCost(requiredAmount);
			order.setQuantity(qty);
			order.setRedeemedRewards(requiredAmount);
			order.setStatus("PENDING");
			
			Integer freeQty = giftCardQty.getFreeQty()-qty;
			Integer usedQty = giftCardQty.getUsedQty()+qty;
			
			giftCardQty.setFreeQty(freeQty);
			giftCardQty.setUsedQty(usedQty);
			
			DAORegistry.getRtfGiftCardOrderDAO().save(order);
			DAORegistry.getRtfGiftCardQuantityDAO().update(giftCardQty);
			
			response = "Gift Card Order Created Successfully. Order No: "+order.getId();
			
		 }catch(Exception e) {
			 e.printStackTrace();
			 response = "Alert: Exception occured while creating gift card order. Please check immediately.";
		 }
		 System.out.println(response);
		return response;
	 }
	
}