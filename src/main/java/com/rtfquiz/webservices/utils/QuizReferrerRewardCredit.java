package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.rtfquiz.webservices.sqldao.implementation.CustomerRewardHistorySQLDAO;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.ContestMigrationStats;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * QuizCustomerReferrerRewardCredit
 * @author KUlaganathan
 *
 */
public class QuizReferrerRewardCredit extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	private static Logger logger = LoggerFactory.getLogger(QuizReferrerRewardCredit.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	public static boolean isRunning = false;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		QuizReferrerRewardCredit.mailManager = mailManager;
	}


	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public static void init(Integer contestId) throws Exception{
		
		System.out.println("Initiating the custoemr Referral Reward Credit Job...."+ new Date());
		try {
			//If points rtf enabled as reward type then change to True
			Boolean isPointsEnabled= Boolean.TRUE;
		 
		Calendar fromDate = Calendar.getInstance();
		 
		fromDate.add(Calendar.MINUTE, -60);
		
		Calendar toDate = Calendar.getInstance();
		
		System.out.println("CRRN: Job Started:  From Date: "+fromDate.getTime()+", To Date: "+toDate.getTime());
		System.out.println("CRRN: Job Started:  From Date: "+fromDate.getTime()+", To Date: "+toDate.getTime());
		
		QuizContest contest= QuizDAORegistry.getQuizContestDAO().getCompletedContestByContestID(contestId);
		
		if(null != contest) {
			
			Date start = new Date();
			
			LoyaltySettings loyaltySettings =  DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			
			if(null == loyaltySettings) {
				System.out.println("CRRN: inside loyalty settings is empty : "+new Date());
				return;
			}
			
			Double primaryUserEarnPerc =loyaltySettings.getContestCustReferralRewardPerc();
			
			Boolean isFixed = loyaltySettings.getReferralRewardCreditType().equals("Fixed")?true:false;
			
			Collection<QuizCustomerReferralTracking> referralList = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getAll();
			
			Map<Integer, Integer> tireOneUserPrimaryUserMap = new HashMap<Integer, Integer>();
			
			for (QuizCustomerReferralTracking tracking : referralList) {
				tireOneUserPrimaryUserMap.put(tracking.getCustomerId(), tracking.getReferralCustomerId());
			}
			
			String resMsg = "";
			
				
			try {
				resMsg ="CRRN: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Fixed Credit Type: "+isFixed+", ReferralReward: "+primaryUserEarnPerc+", Process Started: "+new Date();
				System.out.println(resMsg);
				
				List<ContestMigrationStats> contestMigrationStatList = DAORegistry.getContestMigrationStatsDAO().getAllStatsByContestId(contest.getId());
				
				if(null == contestMigrationStatList || contestMigrationStatList.isEmpty()) {
					resMsg ="CRRN: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", ReferralReward: "+primaryUserEarnPerc+", Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
					System.out.println(resMsg);
					return;
				}
				
				boolean isCompleted = false;
				int count=0;
				for (ContestMigrationStats obj : contestMigrationStatList) {
					if(obj.getStatus().equals("COMPLETED")) {
						count++;
						//isCompleted = true;
						//break;
					}
				}
				if(count == 2) {
					isCompleted = true;
				}
				
				if(!isCompleted) {
					resMsg ="CRRN: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", ReferralReward: "+primaryUserEarnPerc+", Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress.";
					System.out.println(resMsg);
					return;
				}
				if(null == contestMigrationStatList || contestMigrationStatList.isEmpty() || contestMigrationStatList.size() < 2) {
					resMsg ="CRRN: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+",  Process Skipped: "+new Date()+", Reason: Migration is not done yet.Its in Progress 2 data not in completed status.";
					System.out.println(resMsg);
					//System.out.println(resMsg);
					return;
				}
				
				ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getRewardCreditByContestIdAndType(contest.getId(),"CUSTOMER_REFERRAL");
				
				if(null != credit) {
					resMsg = "CRRN: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Fixed Credit Type: "+isFixed+", ReferralReward: "+primaryUserEarnPerc+", Process Skipped: "+new Date()+", Reason: Already processed contest.";
					System.out.println(resMsg);
					return;
				}else {
					credit = new ContestReferrerRewardCredit();
					credit.setContestId(contest.getId());
					credit.setCreatedDate(start);
					credit.setIsNotified(true);
					credit.setNotifiedTime(new Date());
					credit.setRewardCreditConv(primaryUserEarnPerc);
					credit.setReferralProgramType("CUSTOMER_REFERRAL");
					credit.setStatus("STARTED");
					credit.setUpdatedDate(new Date());
					QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
				}
				List<QuizCustomerContestAnswers> answers = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getAllCorrectAnsweredDatas(contest.getId());
				if(answers == null || answers.isEmpty()) {
					resMsg = "CRRN: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Fixed Credit Type: "+isFixed+", ReferralReward: "+primaryUserEarnPerc+", Process Skipped: "+new Date()+", Reason: No answers data found.";
					System.out.println(resMsg);
					QuizDAORegistry.getContestReferrerRewardCreditDAO().delete(credit);
					return;
				} else {
					System.out.println("CRRN Referral reward Credits Contest Id : "+contest.getId()+" : date : "+new Date()+" :size : "+answers.size());
				}
				Map<Integer, Map<Integer, QuizCustomerContestAnswers>> customerAnsweredMap = new HashMap<Integer, Map<Integer, QuizCustomerContestAnswers>>();
				
				for (QuizCustomerContestAnswers contestAnswer : answers) {
					
					if(null == contestAnswer.getAnswerRewards()) {
						continue;
					}
					
					Map<Integer, QuizCustomerContestAnswers> questionResultMap = customerAnsweredMap.get(contestAnswer.getCustomerId());
					if(null == questionResultMap || questionResultMap.isEmpty()) {
						questionResultMap = new HashMap<Integer, QuizCustomerContestAnswers>();
					} 
					questionResultMap.put(contestAnswer.getQuestionSNo(), contestAnswer);
					customerAnsweredMap.put(contestAnswer.getCustomerId(), questionResultMap);
				}
				
				boolean isProecced = false;
				
				for (Integer tireOneCutId : tireOneUserPrimaryUserMap.keySet()) {
					
					Integer primaryCustId = tireOneUserPrimaryUserMap.get(tireOneCutId);
					
					if(null == primaryCustId) {
						continue;
					}
					
					Map<Integer, QuizCustomerContestAnswers> questionResultMap = customerAnsweredMap.get(tireOneCutId);
					
					if(null == questionResultMap || questionResultMap.isEmpty()) {
						continue;
					}
					
					Double totalReward = 0.00;
					Integer totalAnsQuestion = 0;
					for (QuizCustomerContestAnswers obj : questionResultMap.values()) {
						
						totalAnsQuestion++;
						
						/*if(obj.getQuestionSNo().equals(contest.getNoOfQuestions()) || obj.getAnswerRewards() <= 0){
							continue;
						}*/
						if(obj.getQuestionSNo().equals(contest.getNoOfQuestions()) || obj.getRtfPoints() <= 0){
							continue;
						}
						if(isFixed) {
							totalReward = totalReward + primaryUserEarnPerc;
						}else {
							Double quesReward = obj.getAnswerRewards();
							totalReward = totalReward + quesReward;
						}
					}
					
					Double earnReward = 0.00;
					if(isFixed) {
						earnReward = totalReward;
					}else {
						earnReward = totalReward * (primaryUserEarnPerc/100);
					}
					
					earnReward = TicketUtil.getRoundedUpValue(earnReward);
					
					resMsg = "CRRN: TireOneCustomerId : "+tireOneCutId+", PrimaryCustomerId: "+primaryCustId+", Reward Dollars: "+earnReward;
					System.out.println(resMsg);
					
					if(isPointsEnabled) {
						
						try {
							SourceType sourceType=SourceType.CONTEST_REFERRAL;
							Integer totPoints = earnReward.intValue();
							String desc = "RefFor:"+tireOneCutId;
							
							Customer customer = DAORegistry.getCustomerDAO().get(primaryCustId);
							Integer custTotPoints = customer.getRtfPoints()+totPoints;
							customer.setRtfPoints(custTotPoints);
							
							DAORegistry.getCustomerDAO().updateQuizCustomerRtfPoints(primaryCustId, totPoints);
							CassandraDAORegistry.getCassCustomerDAO().updateCustomerRtfPointsByCustomerId(primaryCustId, custTotPoints);
							
							 CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, contest.getId(), 0, 0, 0, totPoints, 0.0, desc);
						 }catch(Exception e) {
							 e.printStackTrace();
						 }
						
					} else {
						CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(primaryCustId);
						
						if(null == customerLoyalty) {
							continue;
						}
						
						CustomerLoyaltyHistory history = new CustomerLoyaltyHistory();
						
						history.setCreateDate(new Date());
						history.setCustomerId(primaryCustId);
						history.setDollarConversionRate(loyaltySettings.getDollerConversion());
						history.setEmailDescription("");
						history.setIsRewardsEmailSent(true);
						history.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
						history.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
						history.setOrderId(null);
						history.setOrderTotal(null);
						history.setPointsEarnedAsDouble(totalReward);
						history.setRewardConversionRate(loyaltySettings.getRewardConversion());
						history.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(totalReward,loyaltySettings.getDollerConversion()));
						history.setRewardSpentAmount(0.00);
						history.setRewardStatus(RewardStatus.ACTIVE);
						history.setOrderType(OrderType.CONTEST_REFERRAL);
						history.setUpdatedDate(new Date());
						history.setTireOneCustEarnPoints(totalReward);
						history.setAnsQuestion(totalAnsQuestion);
						history.setTireOneCustId(tireOneCutId);
						history.setContestId(contest.getId());
						history.setOrderId(-1);
						history.setOrderTotal(0.00);
						
						Double balRewardPoints = customerLoyalty.getActivePointsAsDouble() + totalReward - 0;
						Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
						history.setBalanceRewardAmount(balRewardAmount);
						history.setBalanceRewardPointsAsDouble(balRewardPoints);
						 
						Double totalReferrelDollars = null != customerLoyalty.getTotalReferralDollars()?customerLoyalty.getTotalReferralDollars():0;
						
						customerLoyalty.setTotalReferralDollars(totalReferrelDollars + totalReward);
						customerLoyalty.setLastReferralDollars(totalReward);
						customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble() + totalReward);
						customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLatestEarnedPointsAsDouble(totalReward);
						customerLoyalty.setLatestSpentPointsAsDouble(0.00);
						customerLoyalty.setTotalEarnedPointsAsDouble(customerLoyalty.getTotalEarnedPointsAsDouble()+totalReward);
						
						isProecced = true;
						
						DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
						DAORegistry.getCustomerLoyaltyHistoryDAO().save(history);
						
						try {
							CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(primaryCustId,customerLoyalty.getActivePointsAsDouble());
						}catch(Exception e) {
							e.printStackTrace();
							System.out.println("CRRN: inside catch exception update casscustomer : "+new Date());
						}
					}
				}
				
				credit.setStatus("COMPLETED");
				credit.setUpdatedDate(new Date());
				QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
				resMsg = "CRRN: Contest Name : "+contest.getContestName()+", Date: "+contest.getContestStartDate()+", Contest Id: "+contest.getId()+", Fixed Credit Type: "+isFixed+", ReferralReward: "+primaryUserEarnPerc+", Process Completed: "+new Date();
				System.out.println(resMsg);
				
				
			}catch(Exception e) {
				System.out.println("CRRN: inside catch exception 2 : "+new Date());
				e.printStackTrace();
			}
			resMsg = "CRRN: Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date();
			System.out.println(resMsg);
		} else {
			System.out.println("CRRN: inside else Contest Not Yet completed  : "+contestId+" : "+new Date());
		}
	}catch(Exception e){
		System.out.println("CRRN: inside catch exception 3 : "+new Date());
		e.printStackTrace();
	}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				//init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}