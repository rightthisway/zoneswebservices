package com.rtfquiz.webservices.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfquiz.webservices.utils.list.BotDetail;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.utils.URLUtil;

/**
 * RTFBotsUtil
 * @author KUlaganathan
 *
 */
public class RTFBotsUtil extends QuartzJobBean implements StatefulJob  {
	private static Logger logger = LoggerFactory.getLogger(RTFBotsUtil.class);
	public static List<Integer> botCustomerIds = new ArrayList<Integer>();
	
	private static boolean isMigrated = false;
	
	public static List<Integer> getAllBots(){
		if(null != botCustomerIds && !botCustomerIds.isEmpty()) {
			return botCustomerIds;
		}else {
			try {
				init();
			}catch(Exception e) {
				e.printStackTrace();
			}
			return botCustomerIds;
		} 
	}
	
	public static List<Integer> getBotsForGrandWinnerComputation(){
		return botCustomerIds; 
	}
	
	public static void init() throws Exception{
		logger.info("BOTS Util :- Begins: "+new Date());
		List<Integer> botIds = DAORegistry.getQueryManagerDAO().getAllBotsCustomerIds();
		if(null != botIds && !botIds.isEmpty()) {
			botCustomerIds.clear();
			botCustomerIds =  new ArrayList<Integer>(botIds);
		}
		
		isMigrated = true;
		logger.info("BOTS Util :- Ends: "+new Date());
	}
	
	public static Map<String, Object> getAllBotsDetails() throws Exception{
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Set<String> referralCodeList = new HashSet<String>();
		List<BotDetail> botList = new ArrayList<BotDetail>();
		
		List<Object[]> objectList = DAORegistry.getQueryManagerDAO().getAllBotsInformation();
		
		BotDetail botDetail = null;
		String refCode = null;
		for (Object[] object : objectList) {
			try {
				botDetail = new BotDetail();
				botDetail.setCustomerId((Integer)object[0]);
				botDetail.setEmail((String)object[1]);
				botDetail.setUserId((String)object[2]);
				botDetail.setPhoneNo((String)object[3]);
				botDetail.setSignupDate((String)object[4]);
				
				Boolean otpVerified = (Boolean)object[5];
				
				botDetail.setOtpVerified(otpVerified?"YES":"NO");
				botDetail.setSignUpPlatform((String)object[6]);
				botDetail.setSignUpDeviceId((String)object[7]);
				
				if(null == object[8]) {
					refCode = "";
				}else {
					refCode = (String)object[8];
					referralCodeList.add(refCode);
				}
				botDetail.setReferrerCode(refCode);
				botDetail.setAllLoggedInDevices((String)object[9]);
				botDetail.setAllIPAddress((String)object[10]);
				botList.add(botDetail);
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		resultMap.put("BotList", botList);
		resultMap.put("ReferralCodeList", referralCodeList);
		return resultMap;
	}
 
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		try {
			if(isMigrated) {
				return;
			}
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}