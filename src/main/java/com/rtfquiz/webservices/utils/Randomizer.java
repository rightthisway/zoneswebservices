package com.rtfquiz.webservices.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Randomizer {
	
	private static final String FILENAME = "C:\\Code\\Jewish.txt";

	public static void main(String[] args) throws InterruptedException {
		
		List<String> names = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				//System.out.println(sCurrentLine);
				names.add(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		/*int i =1 ;
		for (String name : names) {
			System.out.println(i+"==========="+name);
			i++;
		}*/
		
		runRandomizer(names);

	}
	
	public static void runRandomizer(List<String> contestWinnerList) throws InterruptedException {
		
		System.out.println("Winner Names: ");
		System.out.println("----------------");
		System.out.println(" ");
		System.out.println(contestWinnerList);
		Collections.sort(contestWinnerList);
		System.out.println(" ");
		//System.out.println("SORTED IN ASCENDING ORDER:");
		//System.out.println(contestWinnerList);
		System.out.println("------------------------Entering into lottery--------------------------");
		System.out.println(" ");
		for (String email : contestWinnerList) {
			System.out.println(email);
		}
		System.out.println(" ");
		System.out.println("---------------------Declaring Grand Winner-----------------------------");
		//Thread.sleep(10000);
		Integer maxGrandWinner = 1;
		Integer totalWimmers = contestWinnerList.size();
		Set<Integer> indexList = new HashSet<Integer>();
		for(int i=0;i<maxGrandWinner;i++) {
			Boolean flag = true;
			while(flag) {
				Random random = new Random();
				int index = random.nextInt(totalWimmers);
				//System.out.println("TOTAL WINNERS: "+totalWimmers+", QUIZWINNER RANDOMIZER INDEX: "+index);
				if(indexList.add(index)) {
					String winnerEmail = contestWinnerList.get(index);
					System.out.println(" ");
					System.out.println("GRAND WINNER IS  : "+winnerEmail);
					flag = false;
				}
			}
		}
	}
	
	public static void mainOLD(String[] args) throws InterruptedException {
		
		List<String> contestWinnerList = new ArrayList<String>();
		
		contestWinnerList.add("collinzucker@me.com");
		contestWinnerList.add("eitankastner@aol.com");
		contestWinnerList.add("c.dunleavy23@gmail.com");
		contestWinnerList.add("rachyo2000@yahoo.com");
		contestWinnerList.add("ronniekastner@gmail.com");
		contestWinnerList.add("mollypawelski@gmail.com");
		
		System.out.println("Winner Emails: ");
		System.out.println("----------------");
		System.out.println(" ");
		System.out.println(contestWinnerList);
		Collections.sort(contestWinnerList);
		System.out.println(" ");
		//System.out.println("SORTED IN ASCENDING ORDER:");
		//System.out.println(contestWinnerList);
		System.out.println("------------------------Entering into lottery--------------------------");
		System.out.println(" ");
		for (String email : contestWinnerList) {
			System.out.println(email);
		}
		System.out.println(" ");
		System.out.println("---------------------Declaring Grand Winner-----------------------------");
		Thread.sleep(10000);
		Integer maxGrandWinner = 1;
		Integer totalWimmers = contestWinnerList.size();
		Set<Integer> indexList = new HashSet<Integer>();
		for(int i=0;i<maxGrandWinner;i++) {
			Boolean flag = true;
			while(flag) {
				Random random = new Random();
				int index = random.nextInt(totalWimmers);
				//System.out.println("TOTAL WINNERS: "+totalWimmers+", QUIZWINNER RANDOMIZER INDEX: "+index);
				if(indexList.add(index)) {
					String winnerEmail = contestWinnerList.get(index);
					System.out.println(" ");
					System.out.println("GRAND WINNER IS  "+winnerEmail);
					flag = false;
				}
			}
		}
	}

}
