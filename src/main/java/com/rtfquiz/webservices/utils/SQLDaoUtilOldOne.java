package com.rtfquiz.webservices.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.ContestParticipantsCassandra;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerContestDetails;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCassandra;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.WebServiceTrackingCassandra;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.list.MyReward;
import com.zonesws.webservices.utils.list.ReferralDTO;


public class SQLDaoUtilOldOne {
	static Connections connections  = new Connections();
	
public static boolean saveAllRtfTracking(List<WebServiceTrackingCassandra> rtftrackingList) throws Exception {
		
		Connection connection = connections.getRtfZonesPlatformConnection();  
		connection.setAutoCommit(false);
		
		String query = "INSERT INTO web_service_tracking_cassandra (api_name,created_datetime,start_date,end_date,action_result,contest_id,cust_id,"
				+ " cust_ip_addr,description,device_info,device_type,platform,session_id,app_ver,fb_callback_time,device_api_start_time,"
				+ " res_status,question_no,rt_count,ans_count,node_id ) " +
				"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		  PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			for(WebServiceTrackingCassandra rtfTracking : rtftrackingList){	
				preparedStatement.setString(1,rtfTracking.getApiName());
				if(rtfTracking.getCreatedDateTime() != null) {
					preparedStatement.setTimestamp(2,new Timestamp(rtfTracking.getCreatedDateTime().getTime()));	
				} else {
					preparedStatement.setString(2,null);
				}
				if(rtfTracking.getStartDate() != null) {
					preparedStatement.setTimestamp(3,new Timestamp(rtfTracking.getStartDate().getTime()));	
				} else {
					preparedStatement.setString(3,null);
				}
				if(rtfTracking.getEndDate() != null) {
					preparedStatement.setTimestamp(4,new Timestamp(rtfTracking.getEndDate().getTime()));	
				} else {
					preparedStatement.setString(4,null);
				}
				
				preparedStatement.setString(5,rtfTracking.getActionResult());
				preparedStatement.setInt(6,rtfTracking.getContestId());
				preparedStatement.setInt(7,rtfTracking.getCustId());
				preparedStatement.setString(8,rtfTracking.getCustIPAddr());
				preparedStatement.setString(9,rtfTracking.getDescription());
				preparedStatement.setString(10,rtfTracking.getDeviceInfo());
				preparedStatement.setString(11,rtfTracking.getDeviceType());
				preparedStatement.setString(12,rtfTracking.getPlatForm());
				preparedStatement.setString(13,rtfTracking.getSessionId());
				preparedStatement.setString(14,rtfTracking.getAppVersion());
				
				if(rtfTracking.getFbCallbackTime() != null) {
					preparedStatement.setTimestamp(15,new Timestamp(rtfTracking.getFbCallbackTime().getTime()));	
				} else {
					preparedStatement.setString(15,null);
				}
				if(rtfTracking.getDeviceAPIStartTime() != null) {
					preparedStatement.setTimestamp(16,new Timestamp(rtfTracking.getDeviceAPIStartTime().getTime()));	
				} else {
					preparedStatement.setString(16,null);
				}
				preparedStatement.setInt(17,rtfTracking.getResponseStatus());
				preparedStatement.setString(18,rtfTracking.getQuestionNo());
				preparedStatement.setInt(19,rtfTracking.getRetryCount());
				preparedStatement.setInt(20,rtfTracking.getAnsCount());
				preparedStatement.setInt(21,rtfTracking.getNodeId());
				
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			
			if(preparedStatement!=null){
				preparedStatement.close();
			}
		}	
	}
	public static boolean saveAllCustContestDetails(List<QuizCustomerContestDetails> custContestDetailsList) throws Exception {
		
		Connection connection = connections.getRtfQuizMasterConnection();  
		connection.setAutoCommit(false);
		
		String query = "INSERT INTO customer_contest_details(customer_id,contest_id,last_quest_no,is_last_quest_crt,is_last_quest_life,"
				+ " cumulative_rwds,cumulative_life,created_date,rtf_points) " +
				  " VALUES (?,?,?,?,?,?,?,?,?)";
		  PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			for(QuizCustomerContestDetails contWinner : custContestDetailsList){	
				preparedStatement.setInt(1,contWinner.getCuId());
				preparedStatement.setInt(2,contWinner.getCoId());
				preparedStatement.setInt(3,contWinner.getLqNo());
				preparedStatement.setBoolean(4,contWinner.getIsLqCrt());
				preparedStatement.setBoolean(5,contWinner.getIsLqLife());
				preparedStatement.setDouble(6,contWinner.getCuRwds());
				preparedStatement.setInt(7,contWinner.getCuLife());
				if(contWinner.getCreatedDate() != null) {
					preparedStatement.setTimestamp(8,new Timestamp(contWinner.getCreatedDate().getTime()));
				} else {
					preparedStatement.setString(8,null);
				}
				preparedStatement.setInt(9, contWinner.getRtfPoints());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			
			if(preparedStatement!=null){
				preparedStatement.close();
			}
		}
	}
	public static boolean saveAllContestParticipantsCassandra(List<ContestParticipantsCassandra> contParticipantsCassList) throws Exception {
		
		Connection connection = connections.getRtfQuizMasterConnection();  
		connection.setAutoCommit(false);
		
		String query = "INSERT INTO contest_participants_cassandra(contest_id,customer_id,ip_address,platform,join_datetime,exit_datetime,status) " +
				"VALUES (?,?,?,?,?,?,?)";
		  PreparedStatement preparedStatement = connection.prepareStatement(query);
		try {
			for(ContestParticipantsCassandra contWinner : contParticipantsCassList){	
				preparedStatement.setInt(1,contWinner.getContestId());
				preparedStatement.setInt(2,contWinner.getCustomerId());
				preparedStatement.setString(3,contWinner.getIpAddress());
				preparedStatement.setString(4,contWinner.getPlatform());
				if(contWinner.getJoinDateTime() != null) {
					preparedStatement.setTimestamp(5,new Timestamp(contWinner.getJoinDateTime().getTime()));
				} else {
					preparedStatement.setString(5,null);
				}
				if(contWinner.getExitDateTime() != null) {
					preparedStatement.setTimestamp(6,new Timestamp(contWinner.getExitDateTime().getTime()));
				} else {
					preparedStatement.setString(6,null);
				}
				preparedStatement.setString(7,contWinner.getStatus());
				
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			connection.rollback();
			//return false;
			throw e;
		}finally{
			
			if(preparedStatement!=null){
				preparedStatement.close();
			}
		}
	}
public static boolean saveAllContestWinners(List<QuizContestWinners> contestWinnersList) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO contest_winners(contest_id,customer_id,reward_type,created_datetime,"
			+ " reward_tickets,reward_points,reward_rank) " +
			"VALUES (?,?,?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(QuizContestWinners contWinner : contestWinnersList){	
			preparedStatement.setInt(1,contWinner.getContestId());
			preparedStatement.setInt(2,contWinner.getCustomerId());
			if(contWinner.getRewardType() != null) {
				preparedStatement.setInt(3,contWinner.getRewardType().ordinal());
			} else {
				preparedStatement.setString(3,null);
			}
			if(contWinner.getCreatedDateTime() != null) {
				preparedStatement.setTimestamp(4,new Timestamp(contWinner.getCreatedDateTime().getTime()));
			} else {
				preparedStatement.setString(4,null);
			}
			
			preparedStatement.setInt(5,contWinner.getRewardTickets());
			preparedStatement.setDouble(6,contWinner.getRewardPoints());
			preparedStatement.setInt(7,contWinner.getRewardRank());
			
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}
}
public boolean saveAllContestGrandWinners(List<ContestGrandWinner> contestGrandWinnersList) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO contest_grand_winners (customer_id,contest_id,reward_tickets,created_date,expiry_date,status,winner_type,question_id,"
			+ " mini_jackpot_type,event_id, order_id,is_notified,notified_date,notes) " +
			" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(ContestGrandWinner grandWinner : contestGrandWinnersList){	
			preparedStatement.setInt(1,grandWinner.getCustomerId());
			preparedStatement.setInt(2,grandWinner.getContestId());
			preparedStatement.setDouble(3,grandWinner.getRewardTickets());
			if(grandWinner.getCreatedDate() != null) {
				preparedStatement.setTimestamp(4,new Timestamp(grandWinner.getCreatedDate().getTime()));
			} else {
				preparedStatement.setString(4,null);
			}
			if(grandWinner.getExpiryDate() != null) {
				preparedStatement.setTimestamp(5,new Timestamp(grandWinner.getExpiryDate().getTime()));
			} else {
				preparedStatement.setString(5,null);
			}
			if(grandWinner.getStatus() != null) {
				preparedStatement.setString(6,grandWinner.getStatus().toString());
			} else {
				preparedStatement.setString(6,null);
			}
			if(grandWinner.getWinnerType() != null) {
				preparedStatement.setString(7,grandWinner.getWinnerType().toString());
			} else {
				preparedStatement.setString(7,null);
			}
			if(grandWinner.getQuestionId() != null) {
				preparedStatement.setInt(8,grandWinner.getQuestionId());
			} else {
				preparedStatement.setString(8,null);
			}
			if(grandWinner.getJackpotCreditType() != null) {
				preparedStatement.setString(9,grandWinner.getJackpotCreditType().toString());
			} else {
				preparedStatement.setString(9,null);
			}
			if(grandWinner.getEventId() != null) {
				preparedStatement.setInt(10,grandWinner.getEventId());
			} else {
				preparedStatement.setString(10,null);
			}
			if(grandWinner.getOrderId() != null) {
				preparedStatement.setInt(11,grandWinner.getOrderId());
			} else {
				preparedStatement.setString(11,null);
			}
			preparedStatement.setBoolean(12,grandWinner.getIsNotified());
			if(grandWinner.getNotifiedTime() != null) {
				preparedStatement.setTimestamp(13,new Timestamp(grandWinner.getNotifiedTime().getTime()));
			} else {
				preparedStatement.setString(13,null);
			}
			preparedStatement.setString(14,grandWinner.getNotes());
			
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}
}
public static boolean saveAllCustomerContestAnswers(List<QuizCustomerContestAnswers> custAnsList) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO customer_contest_answers (customer_id,contest_id,question_id,question_sl_no,answer,created_datetime,"
			+ " is_correct_answer,is_lifeline_used,updated_datetime,answer_rewards,cumulative_rewards,cumulative_lifeline_used,rt_count,"
			+ " ans_time,fb_callback_time,auto_created,rtf_points ) " +
			"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(QuizCustomerContestAnswers custAns : custAnsList){	
			preparedStatement.setInt(1,custAns.getCustomerId());
			preparedStatement.setInt(2,custAns.getContestId());
			preparedStatement.setInt(3,custAns.getQuestionId());
			preparedStatement.setInt(4,custAns.getQuestionSNo());
			preparedStatement.setString(5,custAns.getAnswer());
			if(custAns.getCreatedDateTime() != null) {
				preparedStatement.setTimestamp(6,new Timestamp(custAns.getCreatedDateTime().getTime()));	
			} else {
				preparedStatement.setString(6,null);
			}
			preparedStatement.setBoolean(7,custAns.getIsCorrectAnswer());
			preparedStatement.setBoolean(8,custAns.getIsLifeLineUsed());
			if(custAns.getUpdatedDateTime() != null) {
				preparedStatement.setTimestamp(9,new Timestamp(custAns.getUpdatedDateTime().getTime()));	
			} else {
				preparedStatement.setString(9,null);
			}
			preparedStatement.setDouble(10,custAns.getAnswerRewards());
			preparedStatement.setDouble(11,custAns.getCumulativeRewards());
			preparedStatement.setInt(12,custAns.getCumulativeLifeLineUsed());
			preparedStatement.setInt(13,custAns.getRetryCount());
			
			if(custAns.getAnswerTime() != null) {
				preparedStatement.setTimestamp(14,new Timestamp(custAns.getAnswerTime().getTime()));	
			} else {
				preparedStatement.setString(14,null);
			}
			if(custAns.getFbCallbackTime() != null) {
				preparedStatement.setTimestamp(15,new Timestamp(custAns.getFbCallbackTime().getTime()));	
			} else {
				preparedStatement.setString(15,null);
			}
			preparedStatement.setBoolean(16,custAns.getIsAutoCreated());
			preparedStatement.setInt(17,custAns.getRtfPoints());
			
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}

public static boolean saveAllContestStats(List<CustomerCassandra> custCassandraList) throws Exception {
	
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO customer_contest_stats_from_cassandra (customer_id,contest_id,lives_before_contest,lives_used,"
			+ "lives_after_contest,created_datetime,contest_ques_rewards,contest_winner_rewards,cumulative_contest_rewards,que_rtf_points,rtf_points_bf_contest ) " +
			"VALUES (?,?,?,?,?,getdate(),?,?,?,?,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(CustomerCassandra obj : custCassandraList){	
			preparedStatement.setInt(1,obj.getCustomerId());
			preparedStatement.setInt(2,obj.getContestId());
			preparedStatement.setInt(3,obj.getLivesBeforeContest());
			preparedStatement.setInt(4,obj.getLivesUsed());
			preparedStatement.setInt(5,obj.getLivesAfterContest()); 
			preparedStatement.setDouble(6,obj.getContestQusReward());
			preparedStatement.setDouble(7,obj.getContestWinnerReward());
			preparedStatement.setDouble(8,obj.getCumulativeContestRewards()); 
			preparedStatement.setInt(9,obj.getQuestRtfPoints());
			preparedStatement.setInt(10,obj.getRtfPointsBeforeContest());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}


public static boolean saveAllLoyaltyTracking(List<CustomerLoyaltyTracking> loyaltyTrackingList) throws Exception {
	
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	
	
	String query = "INSERT INTO cust_loyalty_reward_info_tracking (customer_id, contest_id, reward_points, created_by, created_date, reward_type,rtf_points ) " +
			"VALUES (?,?,?,'AUTO',getdate(),'CONTEST',?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(CustomerLoyaltyTracking obj : loyaltyTrackingList){	
			preparedStatement.setInt(1,obj.getCustomerId());
			preparedStatement.setInt(2,obj.getContestId());
			preparedStatement.setDouble(3,obj.getRewardPoints());
			preparedStatement.setInt(4,obj.getRtfPoints());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}

public static boolean updateCustomerLives(List<Customer> customerLivesList) throws Exception {
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	String query = "update customer set quiz_cust_lives=? where id=?";
	PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(Customer obj : customerLivesList){	
			preparedStatement.setInt(1,obj.getQuizCustomerLives());
			preparedStatement.setInt(2,obj.getId()); 
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}

public static boolean updateCustomerLivesAndRtfPoints(List<Customer> customerLivesList) throws Exception {
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	String query = "update customer set quiz_cust_lives=?,rtf_points=? where id=?";
	PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(Customer obj : customerLivesList){	
			preparedStatement.setInt(1,obj.getQuizCustomerLives());
			preparedStatement.setInt(2,obj.getRtfPoints()); 
			preparedStatement.setInt(3,obj.getId());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}


public static boolean updateCustomerRewards(List<Customer> custList) throws Exception {
	Connection connection = connections.getRtfZonesPlatformConnection();
	connection.setAutoCommit(false);
	String query = "update cust_loyalty_reward_info set active_reward_points=active_reward_points+?, latest_earn_points=?,total_earn_points=total_earn_points+?, "
			+ "latest_spent_points=0,updated_time=getdate() where cust_id=?";
	PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(Customer obj : custList){	
			preparedStatement.setDouble(1,obj.getThisContestRewards());
			preparedStatement.setDouble(2,obj.getThisContestRewards());
			preparedStatement.setDouble(3,obj.getThisContestRewards()); 
			preparedStatement.setInt(4,obj.getId()); 
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}

public static boolean saveAllCustomerQuestionAnswers(List<QuizCustomerContestAnswers> contestAnswersList) throws Exception {
	
	Connection connection = connections.getRtfZonesPlatformConnection();  
	connection.setAutoCommit(false);
	
	String query = "INSERT INTO [dbo].[customer_contest_answers] ([customer_id] ,[contest_id] ,[question_id] ,[question_sl_no] ,[answer] ,[created_datetime],"
			+ "[is_correct_answer] ,[is_lifeline_used] ,[updated_datetime] ,[answer_rewards] ,[cumulative_rewards] ,[cumulative_lifeline_used],"
			+ "[rt_count] ,[ans_time] ,[fb_callback_time] ,[auto_created]) VALUES (? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		for(QuizCustomerContestAnswers object : contestAnswersList){
			
			preparedStatement.setInt(1,object.getCustomerId());
			preparedStatement.setInt(2,object.getContestId());
			preparedStatement.setInt(3,object.getQuestionId());
			preparedStatement.setInt(4,object.getQuestionSNo());
			preparedStatement.setString(5,object.getAnswer());
			preparedStatement.setTimestamp(6,new Timestamp(object.getCreatedDateTime().getTime()));
			preparedStatement.setBoolean(7,object.getIsCorrectAnswer());
			preparedStatement.setBoolean(8,object.getIsLifeLineUsed());
			preparedStatement.setTimestamp(9,new Timestamp(object.getUpdatedDateTime().getTime()));
			preparedStatement.setDouble(10,object.getAnswerRewards());
			preparedStatement.setDouble(11,object.getCumulativeRewards());
			preparedStatement.setInt(11,object.getCumulativeLifeLineUsed());
			preparedStatement.setInt(12,object.getRetryCount());
			preparedStatement.setTimestamp(13,new Timestamp(object.getAnswerTime().getTime()));
			preparedStatement.setTimestamp(14,new Timestamp(object.getFbCallbackTime().getTime()));
			preparedStatement.setBoolean(15,object.getIsAutoCreated());
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		connection.commit();
		
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}	
}



public static Customer getCustomerData(Customer object) {
	String sql = "select c.id,c.quiz_cust_lives as cLife, cr.active_reward_points as cReward from customer c inner join cust_loyalty_reward_info cr "
			+ "on cr.cust_id = c.id where c.id=" + object.getId();
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next()) {
			object = new Customer();
			object.setId(rs.getInt("id"));
			object.setQuizCustomerLives(rs.getInt("cLife"));;
			object.setRewardDollar(rs.getDouble("cReward"));
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
	}

	return object;

}
public static boolean updateContestSummaryTillDateDataTableSql() throws Exception {
	 
	 CallableStatement cs = null;
	 try{
		 Connection connection = connections.getRtfQuizMasterConnection();
	     cs = connection.prepareCall("{call SP_QMAP02a_load_contest_summary_rank_till_date()}");
       return cs.execute();
       
	 }catch(Exception e){
		 e.printStackTrace();
		 //return false;
		 throw e;
	 }
}
public static boolean updateContestSummaryThisWeekDataTableSql() throws Exception {
	 
	 CallableStatement cs = null;
	 try{
		 Connection connection = connections.getRtfQuizMasterConnection();
	     cs = connection.prepareCall("{call SP_QMAP01a_load_contest_summary_rank_for_week()}");
       return cs.execute();
       
	 }catch(Exception e){
		 e.printStackTrace();
		 //return false;
		 throw e;
	 }
}
public static Map<Integer, Customer> getAllOtpVerifiedCustomerData() {
	String sql = "select c.id,c.quiz_cust_lives as cLife, cr.active_reward_points as cReward from customer c inner join cust_loyalty_reward_info cr "
			+ "on cr.cust_id = c.id and c.is_otp_verified=1";
	Map<Integer, Customer> custMap = new HashMap<Integer, Customer>();
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		Customer object = null;
		while (rs.next()) {
			object = new Customer();
			object.setId(rs.getInt("id"));
			object.setQuizCustomerLives(rs.getInt("cLife"));;
			object.setRewardDollar(rs.getDouble("cReward"));
			custMap.put(object.getId(), object);
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return custMap;

}


public static boolean saveCustomerRewardHistory(Integer customerId, String trxType, SourceType sourceType, Integer sourceRefId,Integer lives,
		Integer magicWands, Integer sfStars, Integer rtfPoints, Double rewardDollar, String description) throws Exception {
	
	Connection connection = connections.getRtfQuizMasterConnection();  
	connection.setAutoCommit(false);
	String query = "INSERT INTO rtf_cust_reward_history(customer_id,transaction_type,source_type,ref_id,lives,magic_wands,"
			+ "sf_stars,rtf_points,reward_dollars,created_date,description) " +
			"VALUES (?,?,?,?,?,?,?,?,?,getdate(),?)";
	  PreparedStatement preparedStatement = connection.prepareStatement(query);
	try {
		preparedStatement.setInt(1,customerId);
		preparedStatement.setString(2,trxType);
		preparedStatement.setString(3,sourceType.toString());
		preparedStatement.setInt(4,null != sourceRefId?sourceRefId:-1);
		preparedStatement.setInt(5,null != lives?lives:0);
		preparedStatement.setInt(6,null != magicWands?magicWands:0);
		preparedStatement.setInt(7,null != sfStars?sfStars:0);
		preparedStatement.setInt(8,null != rtfPoints?rtfPoints:0);
		preparedStatement.setDouble(9,null != rewardDollar?rewardDollar:0.0);
		preparedStatement.setString(10,null != description?description:"");
		preparedStatement.executeUpdate();
		connection.commit();
		return true;
	} catch (Exception e) {
		e.printStackTrace();
		connection.rollback();
		//return false;
		throw e;
	}finally{
		
		if(preparedStatement!=null){
			preparedStatement.close();
		}
	}
}


public static List<ReferralDTO> getAllCustomerReferrals(Integer customerId) {
	
	
	String sql = "select c.id,c.user_id as userId, c.phone as phone,rt.created_datetime,c.signup_date from "
			+ ""+URLUtil.quizApiLinkedServer+".customer_referral_tracking rt with(nolock) "
			+ " inner join customer c with(nolock) on c.id = rt.customer_id and c.is_otp_verified=1 "
			+ "where rt.referral_customer_id=" +customerId+" order by rt.created_datetime desc";
	//System.out.println(sql);
	
	List<ReferralDTO> list = new ArrayList<ReferralDTO>();
	ReferralDTO dto = null; 
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next()) {
			dto = new ReferralDTO();
			dto.setCustomerId(rs.getInt("id"));
			dto.setPhoneNo(rs.getString("phone"));
			dto.setReferralDate(rs.getString("created_datetime"));
			dto.setUserId(rs.getString("userId"));
			dto.setSignUpDate(rs.getString("signup_date"));
			list.add(dto);
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
	}

	return list;

}

public static MyReward getCustomerRewards(Integer customerId) {
	String sql = "select c.id,c.quiz_cust_lives as cLife, c.rtf_points as rtfPoints,mw.magicwand, sf.total_no_of_chances as sfStars, "
			+ "cr.active_reward_points as cReward from customer c with(nolock) left join cust_loyalty_reward_info cr with(nolock)  on cr.cust_id = c.id "
			+ "left join polling_customer_rewards mw with(nolock) on mw.cust_id = c.id left join "
			+ ""+URLUtil.quizApiLinkedServer+".quiz_super_fan_stat sf with(nolock) "
			+ "on sf.customer_id =c.id where c.id=" +customerId;
	MyReward myReward = null; 
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next()) {
			myReward = new MyReward();
			myReward.setCid(customerId);
			myReward.setArp(rs.getInt("rtfPoints"));
			myReward.setAl(rs.getInt("cLife"));
			myReward.setAmw(rs.getInt("magicwand"));
			myReward.setArd(rs.getDouble("cReward"));
			myReward.setAs(rs.getInt("sfStars"));
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
	}

	return myReward;

}

public static MyReward test(Integer customerId) {
	String sql = "select c.id,c.quiz_cust_lives as cLife, c.rtf_points as rtfPoints,mw.magicwand, sf.total_no_of_chances as sfStars, "
			+ "cr.active_reward_points as cReward from customer c with(nolock) left join cust_loyalty_reward_info cr with(nolock)  on cr.cust_id = c.id "
			+ "left join polling_customer_rewards mw with(nolock) on mw.cust_id = c.id left join "
			+ "RTFQuizMaster_Sanbox.dbo.quiz_super_fan_stat sf with(nolock) "
			+ "on sf.customer_id =c.id where c.id=" +customerId;
	MyReward myReward = null; 
	try {
		Connection conn = connections.getRtfZonesPlatformConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next()) {
			myReward = new MyReward();
			myReward.setCid(customerId);
			myReward.setArp(rs.getInt("rtfPoints"));
			myReward.setAl(rs.getInt("cLife"));
			myReward.setAmw(rs.getInt("magicwand"));
			myReward.setArd(rs.getDouble("cReward"));
			myReward.setAs(rs.getInt("sfStars"));
		}

		rs.close();
		stmt.close();
		// conn.close();
	} catch (SQLException se) {
		se.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
	}

	return myReward;

}

public static void main(String[] args) throws Exception {
	MyReward obj = test(431596);
	System.out.println(obj.getArd());
}

}
