package com.rtfquiz.webservices.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * TestCronJobRunning
 * @author KUlaganathan
 *
 */
 
public class TestCronJobRunning extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	private static Logger logger = LoggerFactory.getLogger(TestCronJobRunning.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	static boolean isRunning = false;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		TestCronJobRunning.mailManager = mailManager;
	}


	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	

	public void init() throws Exception{
		
		logger.info("Initiating the email scheduler process for testing crosn job running....");
		try {
			
		if(isRunning) {
			return;
		}
		isRunning = true;
		 
		Calendar fromDate = Calendar.getInstance();
		fromDate.add(Calendar.MINUTE, -60);
		
		Calendar toDate = Calendar.getInstance();
		
		Date start = new Date();
		 
		
		System.out.println("TestCronJobRunning: Job Started:  From Date: "+fromDate.getTime()+", To Date: "+toDate.getTime());
		logger.info("TestCronJobRunning: Job Started:  From Date: "+fromDate.getTime()+", To Date: "+toDate.getTime());
		
		Thread.sleep(1000);
		
		ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getRewardCreditByContestIdAndType(250000,"TESTCRONJOBRUNNING");
		
		if(null != credit) {
			return;
		} 
		credit = new ContestReferrerRewardCredit();
		credit.setContestId(250000);
		credit.setCreatedDate(new Date());
		credit.setIsNotified(true);
		credit.setNotifiedTime(new Date());
		credit.setRewardCreditConv(0.00);
		credit.setReferralProgramType("TESTCRONJOBRUNNING");
		credit.setStatus("COMPLETED");
		credit.setUpdatedDate(new Date());
		QuizDAORegistry.getContestReferrerRewardCreditDAO().saveOrUpdate(credit);
		 
		System.out.println("CRRN: Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		logger.info("CRRN: Job Completed: "+(new Date().getTime()-start.getTime())+" : "+new Date());
	}catch(Exception e){
		e.printStackTrace();
		isRunning = false;
	}
		isRunning = false;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}