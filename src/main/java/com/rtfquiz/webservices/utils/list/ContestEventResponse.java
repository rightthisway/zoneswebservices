package com.rtfquiz.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.list.NormalSearchResult;

@XStreamAlias("ContestEventResponse")
public class ContestEventResponse {
	
	private Integer status;
	private Error error; 
	private Integer contestId;
	private NormalSearchResult normalSearchResult;
	private String contestName;
	private Integer quantity;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public NormalSearchResult getNormalSearchResult() {
		return normalSearchResult;
	}
	public void setNormalSearchResult(NormalSearchResult normalSearchResult) {
		this.normalSearchResult = normalSearchResult;
	}
	public String getContestName() {
		if(contestName == null) {
			contestName = "";
		}
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public Integer getQuantity() {
		if(quantity == null) {
			quantity = 0;
		}
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
}
