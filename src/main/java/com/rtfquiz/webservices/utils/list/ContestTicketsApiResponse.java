package com.rtfquiz.webservices.utils.list;

import com.rtfquiz.webservices.data.QuizContest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("ContestTicketsApiResponse")
public class ContestTicketsApiResponse {
	
	private Integer status;
	private Error error; 
	private String message;
	private Integer contestId;
	private QuizContest quizContest;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public QuizContest getQuizContest() {
		return quizContest;
	}
	public void setQuizContest(QuizContest quizContest) {
		this.quizContest = quizContest;
	}
	
}
