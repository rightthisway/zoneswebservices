package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.RtfCustProfileAnswers;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustProfileBasicInfo")
public class CustProfileBasicInfo {
	private String fName;
	private String lName;
	private String addr;
	private String dob;
	private String gender;
	
	private String userId;
	private String email;
	private String phone;
	
	private Integer fNamePts;
	private Integer lNamePts;
	private Integer addrPts;
	private Integer dobPts;
	private Integer genderPts;
	
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Integer getfNamePts() {
		if(fNamePts == null) {
			fNamePts = 0;
		}
		return fNamePts;
	}
	public void setfNamePts(Integer fNamePts) {
		this.fNamePts = fNamePts;
	}
	public Integer getlNamePts() {
		if(lNamePts == null) {
			lNamePts = 0;
		}
		return lNamePts;
	}
	public void setlNamePts(Integer lNamePts) {
		this.lNamePts = lNamePts;
	}
	public Integer getAddrPts() {
		if(addrPts == null) {
			addrPts = 0;
		}
		return addrPts;
	}
	public void setAddrPts(Integer addrPts) {
		this.addrPts = addrPts;
	}
	public Integer getDobPts() {
		if(dobPts == null) {
			dobPts = 0;
		}
		return dobPts;
	}
	public void setDobPts(Integer dobPts) {
		this.dobPts = dobPts;
	}
	public Integer getGenderPts() {
		if(genderPts == null) {
			genderPts = 0;
		}
		return genderPts;
	}
	public void setGenderPts(Integer genderPts) {
		this.genderPts = genderPts;
	}
	
	
}
