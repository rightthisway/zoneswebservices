package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.RtfCustProfileAnswers;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.list.CustomerDetails;

@XStreamAlias("CustProfileDtls")
public class CustProfileDtls {
	private Integer status;
	private Error error; 
	private String message;
	private CustProfileBasicInfo basicInfo;
	private List<RtfCustProfileAnswers> answerList;
	private Integer percentage;
	private String pointsTxt;
	private String headerText;
	
	CustomerDetails customerDetails;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<RtfCustProfileAnswers> getAnswerList() {
		return answerList;
	}
	public void setAnswerList(List<RtfCustProfileAnswers> answerList) {
		this.answerList = answerList;
	}
	public Integer getPercentage() {
		if(percentage == null) {
			percentage = 0;
		}
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
	public CustProfileBasicInfo getBasicInfo() {
		return basicInfo;
	}
	public void setBasicInfo(CustProfileBasicInfo basicInfo) {
		this.basicInfo = basicInfo;
	}
	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}
	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}
	public String getPointsTxt() {
		return pointsTxt;
	}
	public void setPointsTxt(String pointsTxt) {
		this.pointsTxt = pointsTxt;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getHeaderText() {
		return headerText;
	}
	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}
}
