package com.rtfquiz.webservices.utils.list;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.utils.URLUtil;
@XStreamAlias("CustomerFriendDetails")
public class CustomerFriendDetails implements Serializable{
		
	
		private Integer customerId;
		private String userId;
		private String status;
		//private Boolean isSender;
		
		@JsonIgnore
		private String custImagePath;
		
		private String profilePicWV;
		
		
		public Integer getCustomerId() {
			if(customerId == null) {
				customerId = 0;
			}
			return customerId;
		}
		public void setCustomerId(Integer customerId) {
			this.customerId = customerId;
		}
		public String getUserId() {
			if(userId == null) {
				userId = "";
			}
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getCustImagePath() {
			return custImagePath;
		}
		public void setCustImagePath(String custImagePath) {
			this.custImagePath = custImagePath;
		}
		public String getProfilePicWV() {
			if(profilePicWV == null) {
				if(getCustImagePath() != null) {
					this.profilePicWV = URLUtil.profilePicWebURByImageName(getCustImagePath(),ApplicationPlatForm.ANDROID);
				} else {
					this.profilePicWV = "";
				}
			}
			return profilePicWV;
		}
		public void setProfilePicWV(String profilePicWV) {
			this.profilePicWV = profilePicWV;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		
	}