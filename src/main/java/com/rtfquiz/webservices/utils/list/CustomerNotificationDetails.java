package com.rtfquiz.webservices.utils.list;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * Represents Customer App Device Details
 * @author Ulaganathan
 *
 */
public class CustomerNotificationDetails  implements Serializable{
	
	private Integer customerId;
	private String userId;
	private String email;
	private String	applicationPlatForm;
	private String deviceId;
	private String notificationRegId;
	
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getApplicationPlatForm() {
		return applicationPlatForm;
	}
	public void setApplicationPlatForm(String applicationPlatForm) {
		this.applicationPlatForm = applicationPlatForm;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getNotificationRegId() {
		return notificationRegId;
	}
	public void setNotificationRegId(String notificationRegId) {
		this.notificationRegId = notificationRegId;
	}
	
}
