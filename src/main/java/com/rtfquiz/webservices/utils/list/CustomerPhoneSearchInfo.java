package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerPhoneSearchInfo")
public class CustomerPhoneSearchInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	private List<CustomerSearchDetails> customerResult;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerSearchDetails> getCustomerResult() {
		return customerResult;
	}
	public void setCustomerResult(List<CustomerSearchDetails> customerResult) {
		this.customerResult = customerResult;
	}
	
}
