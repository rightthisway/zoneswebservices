package com.rtfquiz.webservices.utils.list;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.utils.URLUtil;
@XStreamAlias("CustomerDetails")
public class CustomerSearchDetails implements Serializable{
		
	
		private Integer customerId;
		private String customerName;
		private String lastName;
		private String userId;
		private String phone;
		private String fStatus;

		@JsonIgnore
		private String custImagePath;
		
		private String profilePicWV;
		
		@JsonIgnore
		private Date joinDate;
		
		public CustomerSearchDetails() {
			
		}
		public CustomerSearchDetails(Customer customer) {
			this.customerId = customer.getId();
			this.userId = customer.getUserId();
			this.custImagePath = customer.getCustImagePath();
		}
		
		public Integer getCustomerId() {
			if(customerId == null) {
				customerId = 0;
			}
			return customerId;
		}
		public void setCustomerId(Integer customerId) {
			this.customerId = customerId;
		}
		public String getCustomerName() {
			if(customerName == null) {
				customerName = "";
			}
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		
		public String getLastName() {
			if(lastName == null) {
				lastName = "";
			}
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getUserId() {
			if(userId == null) {
				userId = "";
			}
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getCustImagePath() {
			return custImagePath;
		}
		public void setCustImagePath(String custImagePath) {
			this.custImagePath = custImagePath;
		}
		public String getProfilePicWV() {
			this.profilePicWV = "";
			/*if(profilePicWV == null) {
				if(getCustImagePath() != null) {
					this.profilePicWV = URLUtil.profilePicWebURByImageName(getCustImagePath(),ApplicationPlatForm.ANDROID);
				} else {
					this.profilePicWV = "";
				}
			}*/
			return profilePicWV;
		}
		public void setProfilePicWV(String profilePicWV) {
			this.profilePicWV = profilePicWV;
		}
		public String getPhone() {
			if(phone == null) {
				phone = "";
			}
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getfStatus() {
			if(fStatus == null) {
				fStatus = "";
			}
			return fStatus;
		}
		public void setfStatus(String fStatus) {
			this.fStatus = fStatus;
		}
		public Date getJoinDate() {
			return joinDate;
		}
		public void setJoinDate(Date joinDate) {
			this.joinDate = joinDate;
		}
		
	
		
	}