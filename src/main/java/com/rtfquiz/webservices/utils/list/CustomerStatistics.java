package com.rtfquiz.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CustomerStatistics")
public class CustomerStatistics {
	
	private Integer customerId;
	private String userId;
	private String fullName;
	private String email;
	private String phoneNo;
	private String signupDate;
	private String signUpDeviceId;
	private String referrerCode;
	private Integer quizCustomerLives;
	/*private Integer quizNoOfTicketsWon;
	private Double quizNoOfPointsWon;*/
	private Double contestGameRewards;
	private Double referralRewards;
	private Double purchaseRewards;
	private Double pendingPurchase;
	private Double currentActiveRewards;
	private Double spendRewards;
	private String isOtpVerified;
	private Integer totalContestPlayed;
	private Integer totalContestWins;
	private String isBot;
	private Integer totalNoOfQuestions;
	private Integer correctAnsQuestions;
	private Integer noOfQuestionAnswered;
	private Integer noOfCustomerReferred;
	
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getSignupDate() {
		return signupDate;
	}
	public void setSignupDate(String signupDate) {
		this.signupDate = signupDate;
	}
	public String getSignUpDeviceId() {
		return signUpDeviceId;
	}
	public void setSignUpDeviceId(String signUpDeviceId) {
		this.signUpDeviceId = signUpDeviceId;
	}
	public String getReferrerCode() {
		return referrerCode;
	}
	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}
	public Integer getQuizCustomerLives() {
		return quizCustomerLives;
	}
	public void setQuizCustomerLives(Integer quizCustomerLives) {
		this.quizCustomerLives = quizCustomerLives;
	}
	/*public Integer getQuizNoOfTicketsWon() {
		return quizNoOfTicketsWon;
	}
	public void setQuizNoOfTicketsWon(Integer quizNoOfTicketsWon) {
		this.quizNoOfTicketsWon = quizNoOfTicketsWon;
	}
	public Double getQuizNoOfPointsWon() {
		return quizNoOfPointsWon;
	}
	public void setQuizNoOfPointsWon(Double quizNoOfPointsWon) {
		this.quizNoOfPointsWon = quizNoOfPointsWon;
	}*/
	public Double getContestGameRewards() {
		return contestGameRewards;
	}
	public void setContestGameRewards(Double contestGameRewards) {
		this.contestGameRewards = contestGameRewards;
	}
	public Double getReferralRewards() {
		return referralRewards;
	}
	public void setReferralRewards(Double referralRewards) {
		this.referralRewards = referralRewards;
	}
	public Double getPurchaseRewards() {
		return purchaseRewards;
	}
	public void setPurchaseRewards(Double purchaseRewards) {
		this.purchaseRewards = purchaseRewards;
	}
	public Double getPendingPurchase() {
		return pendingPurchase;
	}
	public void setPendingPurchase(Double pendingPurchase) {
		this.pendingPurchase = pendingPurchase;
	}
	public Double getSpendRewards() {
		return spendRewards;
	}
	public void setSpendRewards(Double spendRewards) {
		this.spendRewards = spendRewards;
	}
	public String getIsOtpVerified() {
		return isOtpVerified;
	}
	public void setIsOtpVerified(String isOtpVerified) {
		this.isOtpVerified = isOtpVerified;
	}
	public Integer getTotalContestPlayed() {
		return totalContestPlayed;
	}
	public void setTotalContestPlayed(Integer totalContestPlayed) {
		this.totalContestPlayed = totalContestPlayed;
	}
	public Integer getTotalContestWins() {
		return totalContestWins;
	}
	public void setTotalContestWins(Integer totalContestWins) {
		this.totalContestWins = totalContestWins;
	}
	public String getIsBot() {
		return isBot;
	}
	public void setIsBot(String isBot) {
		this.isBot = isBot;
	}
	public Integer getNoOfQuestionAnswered() {
		return noOfQuestionAnswered;
	}
	public void setNoOfQuestionAnswered(Integer noOfQuestionAnswered) {
		this.noOfQuestionAnswered = noOfQuestionAnswered;
	}
	public Integer getNoOfCustomerReferred() {
		return noOfCustomerReferred;
	}
	public void setNoOfCustomerReferred(Integer noOfCustomerReferred) {
		this.noOfCustomerReferred = noOfCustomerReferred;
	}
	public Integer getTotalNoOfQuestions() {
		return totalNoOfQuestions;
	}
	public void setTotalNoOfQuestions(Integer totalNoOfQuestions) {
		this.totalNoOfQuestions = totalNoOfQuestions;
	}
	public Integer getCorrectAnsQuestions() {
		return correctAnsQuestions;
	}
	public void setCorrectAnsQuestions(Integer correctAnsQuestions) {
		this.correctAnsQuestions = correctAnsQuestions;
	}
	public Double getCurrentActiveRewards() {
		return currentActiveRewards;
	}
	public void setCurrentActiveRewards(Double currentActiveRewards) {
		this.currentActiveRewards = currentActiveRewards;
	}
	 
}
