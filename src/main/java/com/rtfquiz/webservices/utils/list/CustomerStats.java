package com.rtfquiz.webservices.utils.list;

import java.util.List;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerStats")
public class CustomerStats {
	
	private Integer status;
	private Error error; 
	private String message; 
	private List<CustomerStatistics> customerStatistics;
	private List<BotDetail> botList;
	private Set<String> referrlCodeList;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerStatistics> getCustomerStatistics() {
		return customerStatistics;
	}
	public void setCustomerStatistics(List<CustomerStatistics> customerStatistics) {
		this.customerStatistics = customerStatistics;
	}
	public List<BotDetail> getBotList() {
		return botList;
	}
	public void setBotList(List<BotDetail> botList) {
		this.botList = botList;
	}
	public Set<String> getReferrlCodeList() {
		return referrlCodeList;
	}
	public void setReferrlCodeList(Set<String> referrlCodeList) {
		this.referrlCodeList = referrlCodeList;
	}
}
