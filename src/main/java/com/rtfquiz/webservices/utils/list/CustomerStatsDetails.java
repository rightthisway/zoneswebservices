package com.rtfquiz.webservices.utils.list;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.utils.URLUtil;
@XStreamAlias("CustomerDetails")
public class CustomerStatsDetails implements Serializable{
		
	
		private Integer customerId;
		private String customerName;
		private String lastName;
		private String userId;
		private Integer totalContestPlayed=0;
		private Integer totalContestWins=0;
		private Integer contestHighScore=0;
		private Double totalRewardPoints=0.0;
		private String friendStatus;
		private Boolean isSender = false;
		
		@JsonIgnore
		private String custImagePath;
		
		private String profilePicWV;
		
		public CustomerStatsDetails() {
			
		}
		public CustomerStatsDetails(Customer customer) {
			this.customerId = customer.getId();
			this.userId = customer.getUserId();
			this.custImagePath = customer.getCustImagePath();
			this.totalContestPlayed = customer.getTotalContestPlayed();
			this.totalContestWins = customer.getTotalContestWins();
			this.contestHighScore = customer.getContestHighScore();
		}
		
		public Integer getCustomerId() {
			if(customerId == null) {
				customerId = 0;
			}
			return customerId;
		}
		public void setCustomerId(Integer customerId) {
			this.customerId = customerId;
		}
		public String getCustomerName() {
			if(customerName == null) {
				customerName = "";
			}
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		
		public String getLastName() {
			if(lastName == null) {
				lastName = "";
			}
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getUserId() {
			if(userId == null) {
				userId = "";
			}
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getCustImagePath() {
			return custImagePath;
		}
		public void setCustImagePath(String custImagePath) {
			this.custImagePath = custImagePath;
		}
		public String getProfilePicWV() {
			if(profilePicWV == null) {
				if(getCustImagePath() != null && !getCustImagePath().equals("")) {
					this.profilePicWV = URLUtil.profilePicWebURByImageName(getCustImagePath(),ApplicationPlatForm.ANDROID);
				} else {
					this.profilePicWV = "";
				}
			}
			return profilePicWV;
		}
		public void setProfilePicWV(String profilePicWV) {
			this.profilePicWV = profilePicWV;
		}
		public Integer getTotalContestPlayed() {
			if(totalContestPlayed == null) {
				totalContestPlayed = 0;
			}
			return totalContestPlayed;
		}
		public void setTotalContestPlayed(Integer totalContestPlayed) {
			this.totalContestPlayed = totalContestPlayed;
		}
		public Integer getTotalContestWins() {
			if(totalContestWins == null) {
				totalContestWins = 0;
			}
			return totalContestWins;
		}
		public void setTotalContestWins(Integer totalContestWins) {
			this.totalContestWins = totalContestWins;
		}
		public Integer getContestHighScore() {
			if(contestHighScore == null) {
				contestHighScore = 0;
			}
			return contestHighScore;
		}
		public void setContestHighScore(Integer contestHighScore) {
			this.contestHighScore = contestHighScore;
		}
		public Double getTotalRewardPoints() {
			if(totalRewardPoints == null) {
				totalRewardPoints = 0.0;
			}
			return totalRewardPoints;
		}
		public void setTotalRewardPoints(Double totalRewardPoints) {
			this.totalRewardPoints = totalRewardPoints;
		}
		public String getFriendStatus() {
			if(friendStatus == null) {
				friendStatus = "";
			}
			return friendStatus;
		}
		public void setFriendStatus(String friendStatus) {
			this.friendStatus = friendStatus;
		}
		public Boolean getIsSender() {
			if(isSender == null) {
				isSender = false;
			}
			return isSender;
		}
		public void setIsSender(Boolean isSender) {
			this.isSender = isSender;
		}
		
	
		
	}