package com.rtfquiz.webservices.utils.list;

import java.util.Date;

import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizAnswerCountDetails")
public class QuizAnswerCountDetails {
	
	private Integer status;
	private Error error; 
	private String message;
	private String correctAnswer;
	private Integer optionACount=0;
	private Integer optionBCount=0;
	private Integer optionCCount=0;
	//private Integer optionDCount=0;
	private Double questionRewards=0.0;
	
	private Integer lifeLineUsedCount=0;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCorrectAnswer() {
		if(correctAnswer == null) {
			correctAnswer = "";
		}
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	public Integer getOptionACount() {
		if(optionACount == null) {
			optionACount =0;
		}
		return optionACount;
	}
	public void setOptionACount(Integer optionACount) {
		this.optionACount = optionACount;
	}
	public Integer getOptionBCount() {
		if(optionBCount == null) {
			optionBCount =0;
		}
		return optionBCount;
	}
	public void setOptionBCount(Integer optionBCount) {
		this.optionBCount = optionBCount;
	}
	public Integer getOptionCCount() {
		if(optionCCount == null) {
			optionCCount =0;
		}
		return optionCCount;
	}
	public void setOptionCCount(Integer optionCCount) {
		this.optionCCount = optionCCount;
	}
	/*public Integer getOptionDCount() {
		return optionDCount;
	}
	public void setOptionDCount(Integer optionDCount) {
		this.optionDCount = optionDCount;
	}*/
	public Double getQuestionRewards() {
		if(questionRewards == null) {
			questionRewards =0.0;
		}
		return questionRewards;
	}
	public void setQuestionRewards(Double questionRewards) {
		this.questionRewards = questionRewards;
	}
	public Integer getLifeLineUsedCount() {
		if(lifeLineUsedCount == null) {
			lifeLineUsedCount =0;
		}
		return lifeLineUsedCount;
	}
	public void setLifeLineUsedCount(Integer lifeLineUsedCount) {
		this.lifeLineUsedCount = lifeLineUsedCount;
	}
	
	
	
	
}
