package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizContestSummary")
public class QuizContestSummaryInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	private Integer contestWinnersCount;
	List<QuizContestWinners> quizContestWinners;
	QuizContestWinners quizContestWinner;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<QuizContestWinners> getQuizContestWinners() {
		return quizContestWinners;
	}
	public void setQuizContestWinners(List<QuizContestWinners> quizContestWinners) {
		this.quizContestWinners = quizContestWinners;
	}
	public Integer getContestWinnersCount() {
		if(contestWinnersCount == null) {
			contestWinnersCount = 0;
		}
		return contestWinnersCount;
	}
	public void setContestWinnersCount(Integer contestWinnersCount) {
		this.contestWinnersCount = contestWinnersCount;
	}
	public QuizContestWinners getQuizContestWinner() {
		return quizContestWinner;
	}
	public void setQuizContestWinner(QuizContestWinners quizContestWinner) {
		this.quizContestWinner = quizContestWinner;
	}
	
	
	
	
}
