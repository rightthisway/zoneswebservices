package com.rtfquiz.webservices.utils.list;

import java.util.Date;

import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizContestWinnerRewards")
public class QuizContestWinnerRewards {
	
	private Integer status;
	private Error error; 
	private String message;
	private Double rewardsPerWinner;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Double getRewardsPerWinner() {
		if(rewardsPerWinner == null) {
			rewardsPerWinner = 0.0;
		}
		return rewardsPerWinner;
	}
	public void setRewardsPerWinner(Double rewardsPerWinner) {
		this.rewardsPerWinner = rewardsPerWinner;
	}
	
	
	
	
	
}
