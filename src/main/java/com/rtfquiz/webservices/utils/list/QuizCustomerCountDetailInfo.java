package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizCustomerCountDetails")
public class QuizCustomerCountDetailInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	private Integer totalCount = 0;
	//private Integer eliminatedCount=0;
	//private Integer playingCount = 0;
	private Boolean hasMoreCustomers = false;
	List<CustomerSearchDetails> searchResults;
	private Integer maxPerPage = 0;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	/*public Integer getEliminatedCount() {
		return eliminatedCount;
	}
	public void setEliminatedCount(Integer eliminatedCount) {
		this.eliminatedCount = eliminatedCount;
	}
	public Integer getPlayingCount() {
		return playingCount;
	}
	public void setPlayingCount(Integer playingCount) {
		this.playingCount = playingCount;
	}*/
	public Boolean getHasMoreCustomers() {
		if(hasMoreCustomers == null) {
			hasMoreCustomers = false;
		}
		return hasMoreCustomers;
	}
	public void setHasMoreCustomers(Boolean hasMoreCustomers) {
		this.hasMoreCustomers = hasMoreCustomers;
	}
	public List<CustomerSearchDetails> getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(List<CustomerSearchDetails> searchResults) {
		this.searchResults = searchResults;
	}
	public Integer getTotalCount() {
		if(totalCount == null) {
			totalCount = 0;
		}
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getMaxPerPage() {
		if(null == maxPerPage) {
			maxPerPage = 0;
		}
		return maxPerPage;
	}
	public void setMaxPerPage(Integer maxPerPage) {
		this.maxPerPage = maxPerPage;
	}
	
	
	
	
}
