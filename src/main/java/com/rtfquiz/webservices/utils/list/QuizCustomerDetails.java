package com.rtfquiz.webservices.utils.list;

import com.rtfquiz.webservices.data.QuizCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizCustomerInfo")
public class QuizCustomerDetails {
	private Integer status;
	private Error error; 
	private QuizCustomer quizCustomer;
	private String customerProfilePicWebView;
	private String message;
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getCustomerProfilePicWebView() {
		if(customerProfilePicWebView == null) {
			customerProfilePicWebView = "";
		}
		return customerProfilePicWebView;
	}
	public void setCustomerProfilePicWebView(String customerProfilePicWebView) {
		this.customerProfilePicWebView = customerProfilePicWebView;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public QuizCustomer getQuizCustomer() {
		return quizCustomer;
	}
	public void setQuizCustomer(QuizCustomer quizCustomer) {
		this.quizCustomer = quizCustomer;
	}
	
	
	
	
}
