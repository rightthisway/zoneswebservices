package com.rtfquiz.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizCustomerLifeLineUsage")
public class QuizCustomerLifeLineUsage {
	
	private Integer status;
	private Error error; 
	private String message;
	private Boolean isLifeLineUsed= false;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getIsLifeLineUsed() {
		if(isLifeLineUsed == null) {
			isLifeLineUsed = false;
		}
		return isLifeLineUsed;
	}
	public void setIsLifeLineUsed(Boolean isLifeLineUsed) {
		this.isLifeLineUsed = isLifeLineUsed;
	}
	@Override
	public String toString() {
		return "QuizCustomerLifeLineUsage [status=" + status + ", error=" + error + ", message=" + message
				+ ", isLifeLineUsed=" + isLifeLineUsed + "]";
	}
	
	
}
