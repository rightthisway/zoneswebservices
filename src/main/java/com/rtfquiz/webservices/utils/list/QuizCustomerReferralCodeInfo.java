package com.rtfquiz.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizReferralCodeInfo")
public class QuizCustomerReferralCodeInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	private Integer quizCustomerLives;
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getQuizCustomerLives() {
		if(quizCustomerLives == null) {
			quizCustomerLives = 0;
		}
		return quizCustomerLives;
	}
	public void setQuizCustomerLives(Integer quizCustomerLives) {
		this.quizCustomerLives = quizCustomerLives;
	}
	
	
	
	
}
