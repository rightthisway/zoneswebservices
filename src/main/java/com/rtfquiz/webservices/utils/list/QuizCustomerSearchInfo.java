package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerSearchInfo")
public class QuizCustomerSearchInfo {
	private Integer status;
	private Error error; 
	private String message;
	private List<CustomerSearchDetails> customerSearchDetails;
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerSearchDetails> getCustomerSearchDetails() {
		return customerSearchDetails;
	}
	public void setCustomerSearchDetails(
			List<CustomerSearchDetails> customerSearchDetails) {
		this.customerSearchDetails = customerSearchDetails;
	}
	
	
	
	
	
}
