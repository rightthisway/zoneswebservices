package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizCustomerStatsInfo")
public class QuizCustomerStatsInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	private CustomerStatsDetails customerStatsDetails;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerStatsDetails getCustomerStatsDetails() {
		return customerStatsDetails;
	}
	public void setCustomerStatsDetails(CustomerStatsDetails customerStatsDetails) {
		this.customerStatsDetails = customerStatsDetails;
	}
	
}
