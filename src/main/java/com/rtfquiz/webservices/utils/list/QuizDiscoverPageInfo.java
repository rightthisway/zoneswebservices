package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizDiscoverPageInfo")
public class QuizDiscoverPageInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	private List<CustomerFriendDetails> customerFriends;
	private List<CustomerFriendDetails> requestsList;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerFriendDetails> getCustomerFriends() {
		return customerFriends;
	}
	public void setCustomerFriends(List<CustomerFriendDetails> customerFriends) {
		this.customerFriends = customerFriends;
	}
	public List<CustomerFriendDetails> getRequestsList() {
		return requestsList;
	}
	public void setRequestsList(List<CustomerFriendDetails> requestsList) {
		this.requestsList = requestsList;
	}
	
}
