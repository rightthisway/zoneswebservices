package com.rtfquiz.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TextUtil;

@XStreamAlias("QuizJoinContestInfo")
public class QuizJoinContestInfo {
	
	private Integer status;
	private Error error; 
	private Integer totalUsersCount=0;
	private String message;
	private Boolean isExistingContestant=false;
	private Integer lastAnsweredQuestionNo;
	private Boolean isLastAnswerCorrect;
	private Boolean isContestLifeLineUsed=false;
	private Boolean isOtpVerified = false;
	
	private Boolean epShowPromoCode;
	private String epPromoCode;
	private String epDiscText;
	private Integer epPromoRefId;
	private String epPromoRefType;
	private String epPromoRefName;
	private Double contestAnswerRewards;
	private Integer noOfQuestions;
	private Integer contestId;
	
	//Flag to enable or disable firebase callback tracking form APP for each questions
	private Boolean isFirebaseTracking = false;
	
	private String contestTicketsConfirmDialog;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getTotalUsersCount() {
		if(totalUsersCount == null) {
			totalUsersCount  = 0;
		}
		return totalUsersCount;
	}
	public void setTotalUsersCount(Integer totalUsersCount) {
		this.totalUsersCount = totalUsersCount;
	}
	public Boolean getIsExistingContestant() {
		if(isExistingContestant == null) {
			isExistingContestant  = false;
		}
		return isExistingContestant;
	}
	public void setIsExistingContestant(Boolean isExistingContestant) {
		this.isExistingContestant = isExistingContestant;
	}
	public Integer getLastAnsweredQuestionNo() {
		if(lastAnsweredQuestionNo == null) {
			lastAnsweredQuestionNo  = 0;
		}
		return lastAnsweredQuestionNo;
	}
	public void setLastAnsweredQuestionNo(Integer lastAnsweredQuestionNo) {
		this.lastAnsweredQuestionNo = lastAnsweredQuestionNo;
	}
	public Boolean getIsLastAnswerCorrect() {
		if(isLastAnswerCorrect == null) {
			isLastAnswerCorrect = false;
		}
		return isLastAnswerCorrect;
	}
	public void setIsLastAnswerCorrect(Boolean isLastAnswerCorrect) {
		this.isLastAnswerCorrect = isLastAnswerCorrect;
	}
	public Boolean getIsContestLifeLineUsed() {
		if(isContestLifeLineUsed == null) {
			isContestLifeLineUsed = false;
		}
		return isContestLifeLineUsed;
	}
	public void setIsContestLifeLineUsed(Boolean isContestLifeLineUsed) {
		this.isContestLifeLineUsed = isContestLifeLineUsed;
	}
	public Boolean getIsOtpVerified() {
		if(isOtpVerified == null) {
			isOtpVerified = false;
		}
		return isOtpVerified;
	}
	public void setIsOtpVerified(Boolean isOtpVerified) {
		this.isOtpVerified = isOtpVerified;
	}
	public String getEpPromoCode() {
		if(epPromoCode == null) {
			epPromoCode = "";
		}
		return epPromoCode;
	}
	public void setEpPromoCode(String epPromoCode) {
		this.epPromoCode = epPromoCode;
	}
	public Boolean getEpShowPromoCode() {
		if(epShowPromoCode == null) {
			epShowPromoCode = false;
		}
		return epShowPromoCode;
	}
	public void setEpShowPromoCode(Boolean epShowPromoCode) {
		this.epShowPromoCode = epShowPromoCode;
	}
	public String getEpDiscText() {
		if(epDiscText == null) {
			epDiscText = "";
		}
		return epDiscText;
	}
	public void setEpDiscText(String epDiscText) {
		this.epDiscText = epDiscText;
	}
	
	public Integer getEpPromoRefId() {
		return epPromoRefId;
	}
	public void setEpPromoRefId(Integer epPromoRefId) {
		this.epPromoRefId = epPromoRefId;
	}
	public String getEpPromoRefType() {
		if(epPromoRefType == null) {
			epPromoRefType = "";
		}
		return epPromoRefType;
	}
	public void setEpPromoRefType(String epPromoRefType) {
		this.epPromoRefType = epPromoRefType;
	}
	public Double getContestAnswerRewards() {
		if(contestAnswerRewards == null) {
			contestAnswerRewards = 0.0;
		}
		return contestAnswerRewards;
	}
	public void setContestAnswerRewards(Double contestAnswerRewards) {
		this.contestAnswerRewards = contestAnswerRewards;
	}
	
	public Integer getNoOfQuestions() {
		if(noOfQuestions == null) {
			noOfQuestions = 0;
		}
		return noOfQuestions;
	}
	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public String getEpPromoRefName() {
		if(epPromoRefName == null) {
			epPromoRefName = "";
		}
		return epPromoRefName;
	}
	public void setEpPromoRefName(String epPromoRefName) {
		this.epPromoRefName = epPromoRefName;
	}
	
	
	public Boolean getIsFirebaseTracking() {
		return isFirebaseTracking;
	}
	public void setIsFirebaseTrackingdf(Boolean isFirebaseTracking) {
		this.isFirebaseTracking = isFirebaseTracking;
	}
	
	public String getContestTicketsConfirmDialog() {
		contestTicketsConfirmDialog = TextUtil.getContestTicketsConfirmDialog("");
		return contestTicketsConfirmDialog;
	}
	public void setContestTicketsConfirmDialog(String contestTicketsConfirmDialog) {
		this.contestTicketsConfirmDialog = contestTicketsConfirmDialog;
	}
	@Override
	public String toString() {
		return "QuizJoinContestInfo [status=" + status + ", error=" + error + ", tUCt=" + totalUsersCount
				+ ", message=" + message + ", isExCont=" + isExistingContestant
				+ ", lAQNo=" + lastAnsweredQuestionNo + ", isLACor=" + isLastAnswerCorrect
				+ ", isCLifeUsd=" + isContestLifeLineUsed + ", isOtpVerified=" + isOtpVerified
				+ ", epSPCode=" + epShowPromoCode + ", epPCode=" + epPromoCode + ", epDText=" + epDiscText
				+ ", epPRefId=" + epPromoRefId + ", epPRefType=" + epPromoRefType + ", epPRefName="
				+ epPromoRefName + ", cARew=" + contestAnswerRewards + ", noOfQ=" + noOfQuestions
				+ ", cId=" + contestId + "]";
	}
	
}
