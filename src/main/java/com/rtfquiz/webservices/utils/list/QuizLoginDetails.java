package com.rtfquiz.webservices.utils.list;

import com.rtfquiz.webservices.data.QuizCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizLoginInfo")
public class QuizLoginDetails {
	private Integer status;
	private Error error; 
	private QuizCustomer quizCustomer;
	private Boolean showUserInfoPopup;
	private String message;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public QuizCustomer getQuizCustomer() {
		return quizCustomer;
	}
	public void setQuizCustomer(QuizCustomer quizCustomer) {
		this.quizCustomer = quizCustomer;
	}
	public Boolean getShowUserInfoPopup() {
		if(showUserInfoPopup == null) {
			showUserInfoPopup = false;
		}
		return showUserInfoPopup;
	}
	public void setShowUserInfoPopup(Boolean showUserInfoPopup) {
		this.showUserInfoPopup = showUserInfoPopup;
	}
	
	
	
	
}
