package com.rtfquiz.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizOTPInfo")
public class QuizOTPDetails {
	private Integer status;
	private Error error; 
	private String message;
	private String otp;
	private Integer otpTrackingId;
	private Integer expiryTimeInSeconds;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getOtpTrackingId() {
		if(otpTrackingId == null) {
			otpTrackingId = 0;
		}
		return otpTrackingId;
	}
	public void setOtpTrackingId(Integer otpTrackingId) {
		this.otpTrackingId = otpTrackingId;
	}
	
	public String getOtp() {
		if(otp == null) {
			otp = "";
		}
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Integer getExpiryTimeInSeconds() {
		if(expiryTimeInSeconds == null) {
			expiryTimeInSeconds = 0;
		}
		return expiryTimeInSeconds;
	}
	public void setExpiryTimeInSeconds(Integer expiryTimeInSeconds) {
		this.expiryTimeInSeconds = expiryTimeInSeconds;
	}
	
}
