package com.rtfquiz.webservices.utils.list;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizPromoCodeDetail")
public class QuizPromoCodeDetail {
	
	private String promoCode;
	private Integer promoRefId;
	private String promoRefType;
	private String promoRefName;
	private Double discountPercentage;
	private Date promoExpiryDate;
	private String promoText;
	
	
	public String getPromoCode() {
		if(promoCode == null) {
			promoCode = "";
		}
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public Integer getPromoRefId() {
		return promoRefId;
	}
	public void setPromoRefId(Integer promoRefId) {
		this.promoRefId = promoRefId;
	}
	public String getPromoRefName() {
		if(promoRefName == null) {
			promoRefName = "";
		}
		return promoRefName;
	}
	public void setPromoRefName(String promoRefName) {
		this.promoRefName = promoRefName;
	}
	public Double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Date getPromoExpiryDate() {
		return promoExpiryDate;
	}
	public void setPromoExpiryDate(Date promoExpiryDate) {
		this.promoExpiryDate = promoExpiryDate;
	}
	public String getPromoText() {
		if(promoText == null) {
			promoText = "";
		}
		return promoText;
	}
	public void setPromoText(String promoText) {
		this.promoText = promoText;
	}
	public String getPromoRefType() {
		if(promoRefType == null) {
			promoRefType = "";
		}
		return promoRefType;
	}
	public void setPromoRefType(String promoRefType) {
		this.promoRefType = promoRefType;
	}
	
}
