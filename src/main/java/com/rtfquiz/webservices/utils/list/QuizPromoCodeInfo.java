package com.rtfquiz.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.CustomerPromoRewardDetails;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizPromoCodeInfo")
public class QuizPromoCodeInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	
	//List<QuizPromoCodeDetail> promoCodeList;
	List<CustomerPromoRewardDetails> promoCodeList;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<CustomerPromoRewardDetails> getPromoCodeList() {
		return promoCodeList;
	}
	public void setPromoCodeList(List<CustomerPromoRewardDetails> promoCodeList) {
		this.promoCodeList = promoCodeList;
	}
	
	
}
