package com.rtfquiz.webservices.utils.list;

import java.util.Date;

import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizValidateAnswerInfo")
public class QuizValidateAnswerInfo {
	
	private Integer status;
	private Error error; 
	private String message;
	private Boolean isCorrectAnswer= false;
	private String correctAnswer;
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getIsCorrectAnswer() {
		if(isCorrectAnswer == null) {
			isCorrectAnswer = false;
		}
		return isCorrectAnswer;
	}
	public void setIsCorrectAnswer(Boolean isCorrectAnswer) {
		this.isCorrectAnswer = isCorrectAnswer;
	}
	public String getCorrectAnswer() {
		if(correctAnswer == null) {
			correctAnswer = "";
		}
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	@Override
	public String toString() {
		return "QuizValidateAnswerInfo [status=" + status + ", error=" + error + ", message=" + message
				+ ", isCorrectAnswer=" + isCorrectAnswer + ", correctAnswer=" + correctAnswer + "]";
	}
	
	
	
	
}
