package com.rtfquiz.webservices.utils.list;

import java.util.Date;

import com.rtfquiz.webservices.data.QuizCustomer;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("QuizVideoSourceUrlInfo")
public class QuizVideoSourceUrlInfo {
	
	private Integer status;
	private Error error; 
	private String videoSourceUrl;
	private String message;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getVideoSourceUrl() {
		return videoSourceUrl;
	}
	public void setVideoSourceUrl(String videoSourceUrl) {
		this.videoSourceUrl = videoSourceUrl;
	}
	
}
