package com.rtfquiz.webservices.utils.list;

import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.PollingCustomerRewards;

@XStreamAlias("RewardObject")
public class RewardObject {
	
	private Integer customerId;
	private Customer customerObj; 
	private CustomerLoyalty customerLoyalty; 
	private CustomerLoyaltyTracking loyaltyTracking; 
	private QuizSuperFanStat quizSuperFanStat; 
	private PollingCustomerRewards pollingCustomerRewards; 
	private String message;
	private Integer givenLives;
	private Integer givenMagicWands;
	private Integer givenStars;
	private Integer givenRewardPoints;
	private Double givenRewardDollars;
	
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Customer getCustomerObj() {
		return customerObj;
	}
	public void setCustomerObj(Customer customerObj) {
		this.customerObj = customerObj;
	}
	public CustomerLoyalty getCustomerLoyalty() {
		return customerLoyalty;
	}
	public void setCustomerLoyalty(CustomerLoyalty customerLoyalty) {
		this.customerLoyalty = customerLoyalty;
	}
	 
	public CustomerLoyaltyTracking getLoyaltyTracking() {
		return loyaltyTracking;
	}
	public void setLoyaltyTracking(CustomerLoyaltyTracking loyaltyTracking) {
		this.loyaltyTracking = loyaltyTracking;
	}
	public QuizSuperFanStat getQuizSuperFanStat() {
		return quizSuperFanStat;
	}
	public void setQuizSuperFanStat(QuizSuperFanStat quizSuperFanStat) {
		this.quizSuperFanStat = quizSuperFanStat;
	}
	public PollingCustomerRewards getPollingCustomerRewards() {
		return pollingCustomerRewards;
	}
	public void setPollingCustomerRewards(PollingCustomerRewards pollingCustomerRewards) {
		this.pollingCustomerRewards = pollingCustomerRewards;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getGivenLives() {
		if(null == givenLives) {
			givenLives=0;
		}
		return givenLives;
	}
	public void setGivenLives(Integer givenLives) {
		this.givenLives = givenLives;
	}
	public Integer getGivenMagicWands() {
		if(null == givenMagicWands) {
			givenMagicWands=0;
		}
		return givenMagicWands;
	}
	public void setGivenMagicWands(Integer givenMagicWands) {
		this.givenMagicWands = givenMagicWands;
	}
	public Integer getGivenStars() {
		if(null == givenStars) {
			givenStars=0;
		}
		return givenStars;
	}
	public void setGivenStars(Integer givenStars) {
		this.givenStars = givenStars;
	}
	public Integer getGivenRewardPoints() {
		if(null == givenRewardPoints) {
			givenRewardPoints=0;
		}
		return givenRewardPoints;
	}
	public void setGivenRewardPoints(Integer givenRewardPoints) {
		this.givenRewardPoints = givenRewardPoints;
	}
	public Double getGivenRewardDollars() {
		if(null == givenRewardDollars) {
			givenRewardDollars=0.00;
		}
		return givenRewardDollars;
	}
	public void setGivenRewardDollars(Double givenRewardDollars) {
		this.givenRewardDollars = givenRewardDollars;
	}
	
}
