package com.rtftrivia.ws.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CommonRespInfo;
import com.quiz.cassandra.utils.CassCustomerUtil;
import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.CountryPhoneCode;
import com.rtf.ecomerce.data.CustomerLoyaltyPointTranx;
import com.rtfquiz.credit.service.RtfRewardCreditService;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.rtfquiz.webservices.data.QuizOTPTracking;
import com.rtfquiz.webservices.data.RtfCustPhoneReferrals;
import com.rtfquiz.webservices.data.RtfCustProfileAnswers;
import com.rtfquiz.webservices.data.RtfCustProfileQuestions;
import com.rtfquiz.webservices.data.RtfRewardConfig;
import com.rtfquiz.webservices.enums.ProfileQuestionType;
import com.rtfquiz.webservices.enums.QuizMessageTextType;
import com.rtfquiz.webservices.sqldao.implementation.CustomerRewardHistorySQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.SQLDaoUtil;
import com.rtfquiz.webservices.utils.AwsS3Dp;
import com.rtfquiz.webservices.utils.QuizAffiliateReferralCredit;
import com.rtfquiz.webservices.utils.QuizCustomerLifeLineEarnedNotificationUtil;
import com.rtfquiz.webservices.utils.list.CustPhoneReferralInfo;
import com.rtfquiz.webservices.utils.list.CustProfileBasicInfo;
import com.rtfquiz.webservices.utils.list.CustProfileDtls;
import com.rtfquiz.webservices.utils.list.QuizCustomerReferralCodeInfo;
import com.rtfquiz.webservices.utils.list.QuizMessageTextsInfo;
import com.rtfquiz.webservices.utils.list.QuizOTPDetails;
import com.rtfquiz.webservices.utils.list.QuizSettings;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Country;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerLoginHistory;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.Property;
import com.zonesws.webservices.data.QuizAffiliateRewardHistory;
import com.zonesws.webservices.data.QuizAffiliateSetting;
import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;
import com.zonesws.webservices.data.ResetPassword;
import com.zonesws.webservices.data.ResetPasswordRequest;
import com.zonesws.webservices.data.ResetPasswordToken;
import com.zonesws.webservices.data.RtfPromotionalEarnLifeOffers;
import com.zonesws.webservices.data.State;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.enums.AddressType;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CustomerType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.PromotionalType;
import com.zonesws.webservices.enums.RewardBreakUpType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.enums.SignupType;
import com.zonesws.webservices.enums.SourceType;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.filter.ReferralCodeGenerator;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.ShareDriveUtil;
import com.zonesws.webservices.jobs.UserIdCreationScheduler;
import com.zonesws.webservices.sms.TwilioSMSServices;
import com.zonesws.webservices.utils.CityUtil;
import com.zonesws.webservices.utils.CountryUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.FileUtil;
import com.zonesws.webservices.utils.ImageCompressUtil;
import com.zonesws.webservices.utils.PasswordUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.CustProfileAnswerCountDtls;
import com.zonesws.webservices.utils.list.CustomerAddress;
import com.zonesws.webservices.utils.list.CustomerDetails;
import com.zonesws.webservices.utils.list.CustomerLoyaltyDetails;
import com.zonesws.webservices.utils.list.CustomerProfilePic;
import com.zonesws.webservices.utils.list.CustomerRewards;
import com.zonesws.webservices.utils.list.ReferralDTO;
import com.zonesws.webservices.utils.list.ReferralRewardDetails;
import com.zonesws.webservices.utils.list.ReffererCodeResponse;
import com.zonesws.webservices.utils.list.RewardDetails;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

import jcifs.smb.SmbFile;

@Controller
@RequestMapping({"/GetOTP","/ValidatePhoneNoLogin","/CustomerLoginByPhoneNo","/UpdateCustomerReferralCode","/UpdateCustomerReferralCodeV1",
	"/GetCustomerProfilePic","/UpdateCustomerSignUpDetails","/CustomerLogin","/CustomerRegistrationNew","/CustomerLoginNew",
	"/GetCustomerLoyaltyRewards","/GetCustomerInfo","/AddCustomerAddress","/RemoveCustomerAddress","/GetReffererCode",
	"/GetCustomerRewardsBreakup","/UpdateCustomerPhoneNumber","/Customer/AffiliateReward", "/Customer/ReferralReward/ContestLevel" ,
	"/Customer/ReferralReward","/CustomerLogout","/ChangePassword","/ResetPassword","/ResetPasswordRequest",
	"/ValidateOTP","/GetQuizMessageTexts","/ContestQuestionIssueEmail","/CustomerSignUpWithoutName","/UploadProfilePicture","/UpdateCustPhoneReferrals","/GetCustPhoneReferrals",
	"/UploadDefaultProfilePicture","/DeleteDefaultProfilePicture","/SampleFileUploadToS3","/GetCustProfileInfo","/UpdateCustProfileQuest",
	"/GetCustProfilePercentage","/Customer/GetMyReferrals","/BlockCustomerComment","/SixRewardTheFanSignUp","/UpdateCustomerUnBlock",
	"/CustomerLoginByEmailPass","/UpdateCustomerSignUpDetailsApp","/UpdateCustomerSignUpDetailsWeb","/GetCountryPhoneCodes"})
public class AuthenticationController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(AuthenticationController.class);
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static SimpleDateFormat dateFormat =  new SimpleDateFormat("MM/dd/yyyy");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	public static Error authorizationValidation(HttpServletRequest request) throws Exception {
		Error error = new Error();
		HttpSession session = request.getSession();
		String ip =(String)session.getAttribute("ip");
		String configIdString = request.getParameter("configId");
		Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
		
		if(!isAuthrozed){
			error.setDescription("You are not authorized.");
			return error;
		}
		
		if(configIdString!=null && !configIdString.isEmpty()){
			/*try {
				if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
					error.setDescription("You are not authorized.");
					//quizOTPDetails.setError(error);
					//quizOTPDetails.setStatus(0);
					//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
					return error;
				
				}
			} catch (Exception e) {
				error.setDescription("You are not authorized.");
				//quizOTPDetails.setError(error);
				//quizOTPDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
				return error;
			}*/
		}else{
			error.setDescription("You are not authorized.");
			//quizOTPDetails.setError(error);
			//quizOTPDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
			return error;
		}
		return null;
	}
	 
	
	@RequestMapping(value="/ValidatePhoneNoLogin",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails validatePhoneNoLogin(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,authError.getDescription());
				return customerDetails;
			}
			
			//String loginPassword = request.getParameter("password");
			//String loginEmail = request.getParameter("userName");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId=request.getParameter("notificationRegId");
			//String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");
			String ctyPhCode = request.getParameter("ctyPhCode");
			String verificationCode = request.getParameter("verificationCode");
			String otpTrackingIdStr = request.getParameter("otpTrackingId");
			
			resMsg = request.getParameter("phone")+":vc:"+request.getParameter("verificationCode")+":ti:"+request.getParameter("otpTrackingId");
		 
			
			ProductType productType = ProductType.REWARDTHEFAN;
			try{
				productType = ProductType.valueOf(productTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid product type");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,"Please send valid product type");
				return customerDetails;
			}
			
			 
			ApplicationPlatForm applicationPlatForm=null;
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,"Please send valid application platform");
				return customerDetails;
			}
			
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			/*if(TextUtil.isEmptyOrNull(loginEmail)){
				error.setDescription("Please enter your Email or userId");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter your Email or userId");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(loginPassword)){
				error.setDescription("Please enter an Email and Password to continue");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Please enter an Email and Password to continue");
				return customerDetails;
			}*/
			if(TextUtil.isEmptyOrNull(phone)){
				resMsg = "Please enter a phone number:" + resMsg;
				error.setDescription("Please enter a phone number");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			phone = phone.trim();
			//String tempPhone = phone;
			//tempPhone =  QuizSettings.validatePhoneNo(tempPhone);
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				resMsg = "Please enter valid 10 digit phone number:" + resMsg;
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			
			
			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				 ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			
			if(TextUtil.isEmptyOrNull(verificationCode)){
				resMsg = "Please enter your OTP:" + resMsg;
				error.setDescription("Please enter your OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			
			List<QuizOTPTracking> allActiveOtpList = QuizDAORegistry.getQuizOTPTrackingDAO().getActiveOTPByPhoneNo(phone);
			
			if(null == allActiveOtpList || allActiveOtpList.isEmpty()) {
				error.setDescription("You have entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"You have entered wrong OTP");
				return customerDetails;
			}
			
			QuizOTPTracking otpTracking = null;
			for (QuizOTPTracking quizOTPTracking : allActiveOtpList) {
				if(quizOTPTracking.getOtp().equals(verificationCode)){
					otpTracking = quizOTPTracking;
				}
			}
			
			if(otpTracking == null){
				resMsg = "You entered wrong OTP:" + resMsg;
				error.setDescription("You have entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			}
			
			Date sentDate = otpTracking.getCreatedDate();
			Date curDate = new Date();
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sentDate);
			calendar.add(Calendar.SECOND, QuizSettings.otpLiveSeconds);
			
			if(calendar.getTime().before(curDate)){
				resMsg = "OTP Expired. Please Click Resend:" + resMsg;
				error.setDescription("OTP Expired. Please Click Resend");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
				return customerDetails;
			} 
			
			otpTracking.setStatus("VALIDATED");
			otpTracking.setVerifiedDate(new Date());
			QuizDAORegistry.getQuizOTPTrackingDAO().update(otpTracking);
			
			//Get customer by email or user id
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserNameByProductType(loginEmail,productType);
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmailByProductType(loginEmail,productType);
			
				//if(null == customer){
				//error.setDescription("The email or user id you entered is incorrect. Please try again. ");
				//customerDetails.setError(error);
				//customerDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"The email or user id you entered is incorrect. Please try again.");
				//return customerDetails;
				//}
			//boolean showOtpPopup = true;
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmail(loginEmail);
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByPhoneAndProductType(phone, productType);
			
			//executed only when customer is already registered with us from RTW and RTW2 database
			if(customer == null || (customer.getProductType().equals(ProductType.RTW) || 
					customer.getProductType().equals(ProductType.RTW2) || customer.getProductType().equals(ProductType.TIXCITY))){

				/*if(loginPassword.trim().length() < 8 || loginPassword.trim().length() > 20){//tempPhone.length() < 10
					error.setDescription("Password Length must be in between 8-20.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERLOGIN,"Password Length must be in between 8-20.");
					return customerDetails;
				}*/
				//loginPassword = loginPassword.replace(" +", "");
				//String password = PasswordUtil.generateEncrptedPassword(loginPassword);
				CustomerLoyalty customerLoyalty = null;
				
				if(customer == null) {
					customer = new Customer();
					
					//11-21-2018 3:22 PM - Give 1 life to all new registration. Given by Mitul - Done By Ulaganathan
					customer.setQuizCustomerLives(1);
					
					//Add 0 on customer registration 
					customer.setRtfPoints(0);
					customer.setMagicWands(0);
					customer.setSfStars(0);
				} else {
					//customer.setIsEmailed(true);
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					
					try{
						//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
						DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(customer.getId());
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				customer.setBrokerId(1001);
				customer.setIsClient(true);
				customer.setProductType(productType);
				customer.setSignupType(SignupType.REWARDTHEFAN);
				customer.setCustomerName("");
				customer.setLastName("");
				//customer.setReferralCode(null!=referralCode?referralCode:"");
				//customer.setEmail(loginEmail);
				//customer.setEncryptPassword(loginPassword);
				//customer.setUserName(loginEmail);
				customer.setSignupDate(new Date());
				customer.setCustomerType(CustomerType.WEB_CUSTOMER);
				customer.setApplicationPlatForm(applicationPlatForm);
				customer.setDeviceId(null != deviceId?deviceId:"");
				customer.setNotificationRegId(null!=notificationRegId?notificationRegId:"");
				customer.setSocialAccountId("");
				customer.setUserId(null);
				customer.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
				customer.setPhone(phone);
				customer.setFirstTimeLogin("Yes");
				customer.setIsOtpVerified(Boolean.TRUE);

				//Tamil : for website phone login to create userid by phone 
				if(!applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
					String userId = UserIdCreationScheduler.generateCustomerUserId(customer.getCustomerName(), customer.getLastName(), customer.getPhone());
					customer.setUserId(userId);
				}
				
				//customer.setReferrerCode(loginEmail);
				customer.setLastUpdatedDate(new Date());
				//Save Customer
				DAORegistry.getCustomerDAO().saveOrUpdate(customer);
				
				if(customer.getCustImagePath() == null) {
					String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
					customer.setCustImagePath(fileName);
					DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
					//CustomerUtil.updatedCustomerUtil(customer);
				}
				
				/*Add new Customer to Customer Util - Begins*/
				try{
					CustomerUtil.updatedCustomerUtil(customer);
				}catch(Exception e){
					e.printStackTrace();
				}
				/*Add new Customer to Customer Util - Ends*/
				
				if(customerLoyalty == null) {
					customerLoyalty = new CustomerLoyalty();
					customerLoyalty.setCustomerId(customer.getId());
					customerLoyalty.setActivePointsAsDouble(0.00);
					customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
					customerLoyalty.setLatestSpentPointsAsDouble(0.00);
					customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
					customerLoyalty.setTotalSpentPointsAsDouble(0.00);
					customerLoyalty.setLastUpdate(new Date());
					customerLoyalty.setLastNotifiedTime(new Date());
					customerLoyalty.setPendingPointsAsDouble(0.00);
					customerLoyalty.setTotalReferralDollars(0.00);
					customerLoyalty.setLastReferralDollars(0.00);
					customerLoyalty.setTotalAffiliateReferralDollars(0.00);
					customerLoyalty.setLastAffiliateReferralDollars(0.00);
					//Save Customer Loyalty Information
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
				}
				
				/*CustomerLoginHistory loginHistory = new CustomerLoginHistory();
				loginHistory.setCustomerId(customer.getId());
				loginHistory.setApplicationPlatForm(applicationPlatForm);
				loginHistory.setLastLoginTime(new Date());
				loginHistory.setLoginType(SignupType.REWARDTHEFAN);
				loginHistory.setDeviceId(deviceId);
				loginHistory.setNotificationRegId(notificationRegId);
				loginHistory.setLoginIp(loginIp);
				loginHistory.setSocialAccessToken(null);	
				//Save Customer Login History
				DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);*/
				
				/*if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
						applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
					
					CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
					
					if(null == customerDeviceDetails){
						customerDeviceDetails = new CustomerDeviceDetails();
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
						customerDeviceDetails.setCreatedDate(new Date());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setStatus("ACTIVE");
						customerDeviceDetails.setDeviceId(deviceId);
						customerDeviceDetails.setNotificationRegId(notificationRegId);
						customerDeviceDetails.setLoginIp(loginIp);
					}else{
						customerDeviceDetails.setCustomerId(customer.getId());
						customerDeviceDetails.setLastUpdated(new Date());
						customerDeviceDetails.setNotificationRegId(notificationRegId);
					}
					//Save Customer Device Details
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				
					//Add Customer Latest Device Details into CustomerDeviceUtils -  Begins
					try{
						CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
					}catch(Exception e){
						e.printStackTrace();
					}
					//Add Customer Latest Device Details into CustomerDeviceUtils -  Ends
				}*/
				/*String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+loginEmail.split("@")[0]);
				boolean isEmailSent = true;
				String toEmail = customer.getEmail();
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("promotionalCode", custPromoCode);
				mailMap.put("userName",customer.getEmail());
				
				//inline(image in mail body) image add code
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
				
				try{
					//if(productType.equals(productType.REWARDTHEFAN)) {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
								"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
					//} else {
					//	isEmailSent = false;
					//	toEmail = "";
					//}
				}catch(Exception e){
					e.printStackTrace();
					isEmailSent = false;
					toEmail = "";
				}*/
				
				/*Calendar calendar = new GregorianCalendar();
				calendar.add(Calendar.YEAR, 1);
				RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
				custPromoOffer.setCreatedDate(new Date());
				custPromoOffer.setCustomerId(customer.getId());
				custPromoOffer.setDiscount(25.00);
				custPromoOffer.setStartDate(new Date());
				custPromoOffer.setEndDate(calendar.getTime());
				custPromoOffer.setFlatOfferOrderThreshold(1.00);
				custPromoOffer.setIsFlatDiscount(false);
				custPromoOffer.setMaxOrders(1);
				custPromoOffer.setModifiedDate(new Date());
				custPromoOffer.setPromoCode(custPromoCode);
				custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
				custPromoOffer.setNoOfOrders(0);
				custPromoOffer.setStatus("ENABLED");
				custPromoOffer.setIsEmailSent(isEmailSent);
				custPromoOffer.setToEmail(toEmail);*/
				//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
			} else {
				 
			}
			if(customer.getIsOtpVerified() == null || !customer.getIsOtpVerified()) {
				customer.setIsOtpVerified(Boolean.TRUE);
				DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			}
			
			CustomerLoginHistory loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(SignupType.REWARDTHEFAN);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			//loginHistory.setSocialAccessToken(fbAccessToken);			
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
				}
				DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(customer.getUserId() == null || customer.getUserId().equals("") ||
					customer.getEmail() == null || customer.getEmail().equals("")) {
				customerDetails.setShowUserIdPopup(true);
			}
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			customerDetails.setCustomerId(customer.getId());
			customerDetails.setMessage("Quiz Customer Logged in successfully.");
			customerDetails.setStatus(1);
			
			resMsg = "Logged in successfully:" + resMsg;
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
			
		} catch (Exception e) {
			resMsg = "Oops Somthing Went Wrong. Please Login Again!:" + resMsg;
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,resMsg);
			return customerDetails;
		}
		log.info("QUIZ VALID PHONE LOGIN : "+resMsg+":"+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return customerDetails;
	}
	
	@RequestMapping(value = "/UpdateCustomerReferralCode", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerReferralCodeInfo updateCustomerReferralCode(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerReferralCodeInfo quizReferalInfo =new QuizCustomerReferralCodeInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizReferalInfo.setError(authError);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,authError.getDescription());
				return quizReferalInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(referralcodeStr)){
				error.setDescription("Referral Code is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Referral Code is mandatory");
				return quizReferalInfo;
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Customer Id is mandatory");
				return quizReferalInfo;
			}
			
			Boolean isRtfReferalCode = false;
			RtfPromotionalEarnLifeOffers rtfReferral = null;
			Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
			if(referralCustomer == null) {
				
				rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getActiveRtfPromotionEarnLifeOfferByPromoCode(referralcodeStr);
				Date today = new Date();
				if(rtfReferral != null && rtfReferral.getStartDate().compareTo(today)<=0 && rtfReferral.getEndDate().compareTo(today)>=0 ) {
					isRtfReferalCode = true;
				} else {
					error.setDescription("Referral Code is Invalid");
					quizReferalInfo.setError(error);
					quizReferalInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Referral Code is Invalid");
					return quizReferalInfo;
				}
			}
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Customer Id is Invalid");
				return quizReferalInfo;
			}
			if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
				error.setDescription("You can't use your own referral code.");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"You can't use your own referral code.");
				return quizReferalInfo;
			}
			
			QuizCustomerReferralTracking existReferralTracking =  QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(Integer.parseInt(customerIdStr));
			if(existReferralTracking != null) {
				error.setDescription("You have already used referral code");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"You have already Used Referral Code");
				return quizReferalInfo;
			}
			/*QuizCustomerReferralTracking existReferralTracking =  QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerIdandReferralCode(Integer.parseInt(customerIdStr),referralcodeStr);;
			 
			if(existReferralTracking != null) {
				error.setDescription("You have already used this referral code");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"You have already Used Referral Code");
				return quizReferalInfo;
			}*/ 
			
			String mesg = "Referal Code Updated and Customer Lives Added Successfully.";
			
			if(isRtfReferalCode) {
				QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
				referraltracking.setCustomerId(customer.getId());
				referraltracking.setReferralCode(referralcodeStr);
				referraltracking.setUpdatedDateTime(new Date());
				referraltracking.setIsAffiliateReferral(false);
				referraltracking.setRtfPromotionalId(rtfReferral.getId());
				referraltracking.setCreatedDateTime(new Date());
				
				Integer customerLives = customer.getQuizCustomerLives();
				if(customerLives == null) {
					customerLives=0;
				}
				customerLives = customerLives+rtfReferral.getMaxLifePerCustomer();
				customer.setQuizCustomerLives(customerLives);	
				
				CustomerUtil.updatedCustomerUtil(customer);
				DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
				
				/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
				try {
					CassCustomerUtil.updateCustomerLives(customer);
				}catch(Exception e) {
					e.printStackTrace();
				}
				/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
				
				mesg = "You have earned "+rtfReferral.getMaxLifePerCustomer()+" lifes";
				
				QuizCustomerLifeLineEarnedNotificationUtil.sendNotification(customer,mesg );
				QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
			} else {

				QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
				Integer tempPrimaryCustPoints=0,tempTireOneCustPoints=0;
				Integer tempPrimaryCustLives=0,tempTireOneCustLives=0;
				
				Integer customerLives = referralCustomer.getQuizCustomerLives();
				if(customerLives == null) {
					customerLives=0;
				}
				
				Integer tireOneCustLives = customer.getQuizCustomerLives();
				if(tireOneCustLives == null) {
					tireOneCustLives=0;
				}
				Integer customerPoints = referralCustomer.getRtfPoints();
				if(customerPoints == null) {
					customerPoints=0;
				}
				
				Integer tireOneCustPoints = customer.getRtfPoints();
				if(tireOneCustPoints == null) {
					tireOneCustPoints=0;
				}
				
				boolean showPrimaryCustNotification = true,showTireOneCustNotification = true;;
				
				QuizAffiliateSetting quizAffiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getActiveAffiliateByCustomerId(referralCustomer.getId());
				
				if(null != quizAffiliateSetting) {
					
					referraltracking.setIsAffiliateReferral(true);
					referraltracking.setAffiliateReferralStatus("ACTIVE");
					
					if(null != quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() && quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() > 0) {
						referraltracking.setLivesToPrimaryCustomer(true);
						tempPrimaryCustLives = tempPrimaryCustLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
						customerLives = customerLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
					}else {
						referraltracking.setLivesToPrimaryCustomer(false);
						customerLives = customerLives + 0;
						showPrimaryCustNotification = false;
					}
					
					if(null != quizAffiliateSetting.getNoOfLivesToTireOneCustomer() && quizAffiliateSetting.getNoOfLivesToTireOneCustomer() > 0) {
						referraltracking.setLivesToTireOneCustomer(true);
						tempTireOneCustLives = tempTireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
						tireOneCustLives = tireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
					}else {
						referraltracking.setLivesToTireOneCustomer(false);
						tireOneCustLives = tireOneCustLives + 0;
						showTireOneCustNotification = false;
					}
					if(null != quizAffiliateSetting.getTireOneCustRewards() && quizAffiliateSetting.getTireOneCustRewards() > 0) {
						QuizAffiliateReferralCredit.creditRewardDollarToTireOneCustomer(customer.getId(), quizAffiliateSetting.getCustomerId(), quizAffiliateSetting.getTireOneCustRewards());
						
						mesg = "Referral Code Updated and You have earned "+quizAffiliateSetting.getTireOneCustRewards().intValue()+"$ rewards";
					}
					
				}else {
					
					RtfRewardConfig rewardObj = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PRIMARYREFERRAL);
					if(rewardObj != null) {
						if(rewardObj.getNoOfLives() != null) {
							tempPrimaryCustLives = rewardObj.getNoOfLives();
							
							tempTireOneCustLives = rewardObj.getNoOfLives();	
						}
						if(rewardObj.getRtfPoints() != null) {
							tempPrimaryCustPoints = rewardObj.getRtfPoints();
							
							tempTireOneCustPoints = rewardObj.getRtfPoints();
						}
						
						customerLives = customerLives + tempPrimaryCustLives;
						customerPoints = customerPoints + tempPrimaryCustPoints;
						
						tireOneCustLives = tireOneCustLives + tempTireOneCustLives;
						tireOneCustPoints = tireOneCustPoints + tempTireOneCustPoints;
					
						referraltracking.setLivesToPrimaryCustomer(true);
						referraltracking.setLivesToTireOneCustomer(true);
					}
					//RSour
					referraltracking.setIsAffiliateReferral(false); 
					
				}
				referraltracking.setYearEndRewardApplied(false);
				referraltracking.setCustomerId(customer.getId());
				referraltracking.setReferralCode(referralcodeStr);
				referraltracking.setReferralCustomerId(referralCustomer.getId());
				referraltracking.setCreatedDateTime(new Date());
				referraltracking.setUpdatedDateTime(new Date());
				QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
				
				if(showPrimaryCustNotification) {
					referralCustomer.setQuizCustomerLives(customerLives);
					referralCustomer.setRtfPoints(customerPoints);
					
					//DAORegistry.getCustomerDAO().updateQuizCustomerLives(referralCustomer.getId(),customerLives);
					DAORegistry.getCustomerDAO().updateQuizCustomerLivesAndRtfPoints(referralCustomer.getId(),customerLives,customerPoints);
					CustomerUtil.updatedCustomerUtil(referralCustomer);
					
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
					try {
						CassandraDAORegistry.getCassCustomerDAO().updateCustomerLivesAndRtfPoints(referralCustomer.getId(), customerLives, customerPoints);
						//CassCustomerUtil.updateCustomerLives(referralCustomer);
					}catch(Exception e) {
						e.printStackTrace();
					}
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
					try {
						CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(referralCustomer.getId(), "CR", SourceType.PRIMARYREFERRAL, referraltracking.getId(), tempPrimaryCustLives, 0, 0, tempPrimaryCustPoints, 0.0, "");
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					try {
						String notificationMsg = "Congratulation. "+customer.getUserId()+" used your referral code. You have earned";
						if(tempPrimaryCustLives > 0) {
							notificationMsg = notificationMsg + " "+tempPrimaryCustLives+" life.";
						}
						if(tempPrimaryCustPoints > 0) {
							notificationMsg = notificationMsg + " "+tempPrimaryCustLives+" points.";
						}
						
						QuizCustomerLifeLineEarnedNotificationUtil.sendNotification(referralCustomer, notificationMsg);
						//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				
				if(showTireOneCustNotification) {
					customer.setQuizCustomerLives(tireOneCustLives);
					customer.setRtfPoints(tireOneCustPoints);
					
					//DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),tireOneCustLives);
					DAORegistry.getCustomerDAO().updateQuizCustomerLivesAndRtfPoints(customer.getId(),tireOneCustLives,tireOneCustPoints);
					CustomerUtil.updatedCustomerUtil(customer);
					
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
					try {
						CassandraDAORegistry.getCassCustomerDAO().updateCustomerLivesAndRtfPoints(customer.getId(), tireOneCustLives, tireOneCustPoints);
						//CassCustomerUtil.updateCustomerLives(customer);
					}catch(Exception e) {
						e.printStackTrace();
					}
					/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
					try {
						CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(customer.getId(), "CR", SourceType.TIREONEREFERRAL, referraltracking.getId(), tempTireOneCustLives, 0, 0, tempTireOneCustPoints, 0.0, "");
					}catch(Exception e) {
						e.printStackTrace();
					}
					//QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
					try {
						String notificationMsg = "Congratulation. You have earned ";
						if(tempTireOneCustLives > 0) {
							notificationMsg = notificationMsg + " "+tempTireOneCustLives+" life.";
						}
						if(tempTireOneCustPoints > 0) {
							notificationMsg = notificationMsg + " "+tempTireOneCustPoints+" points.";
						}
						
						QuizCustomerLifeLineEarnedNotificationUtil.sendNotification(customer, notificationMsg);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			quizReferalInfo.setQuizCustomerLives(customer.getQuizCustomerLives());
			quizReferalInfo.setStatus(1);
			quizReferalInfo.setMessage(mesg);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Referral Code.");
			quizReferalInfo.setError(error);
			quizReferalInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Error occured while Updating Referral Code.");
			return quizReferalInfo;
		}
		log.info("QUIZ UPDT REFCODE : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizReferalInfo;
	}
	
	
	
	
	@RequestMapping(value = "/UpdateCustomerReferralCodeV1", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerReferralCodeInfo updateCustomerReferralCodeV1(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerReferralCodeInfo quizReferalInfo =new QuizCustomerReferralCodeInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizReferalInfo.setError(authError);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,authError.getDescription());
				return quizReferalInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(referralcodeStr)){
				error.setDescription("Referral Code is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Referral Code is mandatory");
				return quizReferalInfo;
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Customer Id is mandatory");
				return quizReferalInfo;
			}
			
			Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
			if(referralCustomer == null) {
				error.setDescription("Referral Code is Invalid");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Referral Code is Invalid");
				return quizReferalInfo;
			}
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Customer Id is Invalid");
				return quizReferalInfo;
			}
			if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
				error.setDescription("You can't use your own referral code.");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"You can't use your own referral code.");
				return quizReferalInfo;
			}
			
			QuizCustomerReferralTracking existReferralTracking =  QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(Integer.parseInt(customerIdStr));
			if(existReferralTracking != null) {
				error.setDescription("You have already used referral code");
				quizReferalInfo.setError(error);
				quizReferalInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"You have already Used Referral Code");
				return quizReferalInfo;
			}
			QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
			Integer tempPrimaryCustPoints=0,tempTireOneCustPoints=0;
			
			Integer customerPoints = referralCustomer.getLoyaltyPoints();
			if(customerPoints == null) {
				customerPoints=0;
			}
			
			Integer tireOneCustPoints = customer.getLoyaltyPoints();
			if(tireOneCustPoints == null) {
				tireOneCustPoints=0;
			}
			
			boolean showPrimaryCustNotification = true,showTireOneCustNotification = true;;
			
			RtfRewardConfig rewardObj = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PRIMARYREFERRAL);
			if(rewardObj != null) {
				if(rewardObj.getRtfPoints() != null) {
					tempPrimaryCustPoints = rewardObj.getRtfPoints();
					tempTireOneCustPoints = rewardObj.getRtfPoints();
				}
				
				customerPoints = customerPoints + tempPrimaryCustPoints;
				tireOneCustPoints = tireOneCustPoints + tempTireOneCustPoints;
			
				referraltracking.setLivesToPrimaryCustomer(false);
				referraltracking.setLivesToTireOneCustomer(false);
			}
			//RSour
			referraltracking.setIsAffiliateReferral(false); 
			referraltracking.setYearEndRewardApplied(false);
			referraltracking.setCustomerId(customer.getId());
			referraltracking.setReferralCode(referralcodeStr);
			referraltracking.setReferralCustomerId(referralCustomer.getId());
			referraltracking.setCreatedDateTime(new Date());
			referraltracking.setUpdatedDateTime(new Date());
			referraltracking.setIsGamePlayed(false);
			referraltracking.setIsPurchased(false);
			QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
			String mesg = "Referal Code Applied Successfully.";
			if(showPrimaryCustNotification) {/*
				referralCustomer.setRtfPoints(customerPoints);
				
				//DAORegistry.getCustomerDAO().updateQuizCustomerLives(referralCustomer.getId(),customerLives);
				DAORegistry.getCustomerDAO().updateQuizCustomerLivesAndRtfPoints(referralCustomer.getId(),customerLives,customerPoints);
				CustomerUtil.updatedCustomerUtil(referralCustomer);
				
				 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerLivesAndRtfPoints(referralCustomer.getId(), customerLives, customerPoints);
					//CassCustomerUtil.updateCustomerLives(referralCustomer);
				}catch(Exception e) {
					e.printStackTrace();
				}
				 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends
				try {
					CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(referralCustomer.getId(), "CR", SourceType.PRIMARYREFERRAL, referraltracking.getId(), tempPrimaryCustLives, 0, 0, tempPrimaryCustPoints, 0.0, "");
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				try {
					String notificationMsg = "Congratulation. "+customer.getUserId()+" used your referral code. You have earned";
					if(tempPrimaryCustLives > 0) {
						notificationMsg = notificationMsg + " "+tempPrimaryCustLives+" life.";
					}
					if(tempPrimaryCustPoints > 0) {
						notificationMsg = notificationMsg + " "+tempPrimaryCustLives+" points.";
					}
					
					QuizCustomerLifeLineEarnedNotificationUtil.sendNotification(referralCustomer, notificationMsg);
					//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
				} catch(Exception e) {
					e.printStackTrace();
				}
			*/}
			
			if(showTireOneCustNotification) {/*
				customer.setQuizCustomerLives(tireOneCustLives);
				customer.setRtfPoints(tireOneCustPoints);
				
				//DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),tireOneCustLives);
				DAORegistry.getCustomerDAO().updateQuizCustomerLivesAndRtfPoints(customer.getId(),tireOneCustLives,tireOneCustPoints);
				CustomerUtil.updatedCustomerUtil(customer);
				
				 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerLivesAndRtfPoints(customer.getId(), tireOneCustLives, tireOneCustPoints);
					//CassCustomerUtil.updateCustomerLives(customer);
				}catch(Exception e) {
					e.printStackTrace();
				}
				 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends
				try {
					CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(customer.getId(), "CR", SourceType.TIREONEREFERRAL, referraltracking.getId(), tempTireOneCustLives, 0, 0, tempTireOneCustPoints, 0.0, "");
				}catch(Exception e) {
					e.printStackTrace();
				}
				//QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
				try {
					String notificationMsg = "Congratulation. You have earned ";
					if(tempTireOneCustLives > 0) {
						notificationMsg = notificationMsg + " "+tempTireOneCustLives+" life.";
					}
					if(tempTireOneCustPoints > 0) {
						notificationMsg = notificationMsg + " "+tempTireOneCustPoints+" points.";
					}
					
					QuizCustomerLifeLineEarnedNotificationUtil.sendNotification(customer, notificationMsg);
				} catch(Exception e) {
					e.printStackTrace();
				}
			*/}
			quizReferalInfo.setQuizCustomerLives(customer.getQuizCustomerLives());
			quizReferalInfo.setStatus(1);
			quizReferalInfo.setMessage(mesg);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Referral Code.");
			quizReferalInfo.setError(error);
			quizReferalInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUPDATEREFERALCODE,"Error occured while Updating Referral Code.");
			return quizReferalInfo;
		}
		log.info("QUIZ UPDT REFCODE : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizReferalInfo;
	}
	
	
	
	
	

	@RequestMapping(value = "/GetCustomerProfilePic", method=RequestMethod.POST)
	public @ResponsePayload CustomerProfilePic getCustomerProfilePic(@RequestParam (value="customerProfilePic", required=false) MultipartFile uploadedFile,
			HttpServletRequest request, HttpServletResponse response, Model model){
		CustomerProfilePic customerProfilePic = new CustomerProfilePic();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			String customerIdStr = request.getParameter("customerId");
			String productTypeStr = request.getParameter("productType");
			String action = request.getParameter("actionType");
			String profilePicPrefix = URLUtil.DP_PRFEIX_CODE;
			
			String platForm = request.getParameter("platForm");
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer = CustomerUtil.getCustomerById(customerId);
				
				if(customer == null){
					error.setDescription("Customer is not recognized");
					customerProfilePic.setError(error);
					customerProfilePic.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Customer is not recognized");
					return customerProfilePic;
				}
				
			}catch(Exception e){
				error.setDescription("Customer is not recognized");
				customerProfilePic.setError(error);
				customerProfilePic.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Customer is not recognized");
				return customerProfilePic;
			}
			
			if(action != null && action.equals("saveProfilePic")){
				if(customer != null ){
					
					if(null == uploadedFile || uploadedFile.equals(null)){
						error.setDescription("Customer profile picture is mandatory");
						customerProfilePic.setError(error);
						customerProfilePic.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Customer profile picture is mandatory");
						return customerProfilePic;
					}
					
					
					customerId = customer.getId();
					String fileExt = uploadedFile.getOriginalFilename();
					String ext = FilenameUtils.getExtension(fileExt);
					
					if(!FileUtil.validateFileExtension(fileExt)){
						error.setDescription("Please upload valid image. Allowed image types are PNG,JPG,JPEG,GIF,TIF");
						customerProfilePic.setError(error);
						customerProfilePic.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Please upload valid image file. Allowed Image types are PNG,JPG,JPEG,GIF,TIF");
						return customerProfilePic;
					}
					if(URLUtil.isSharedDriveOn) {
						String uploadDir = URLUtil.profilePicUploadDirectory(profilePicPrefix, customerId, ext);
						SmbFile newFile = ShareDriveUtil.getSmbFile(uploadDir);
						String fileName = newFile.getName(); 
						Customer cust = CustomerUtil.getCustomerById(customerId);
						String custImagePath = cust.getCustImagePath();
						if(custImagePath != null){
							String deleteDir = URLUtil.getProfilePicFile(custImagePath); 							
							SmbFile deleteFile = ShareDriveUtil.getSmbFile(deleteDir); 
							boolean isDeleted = false;
							try {
								deleteFile.delete();
								isDeleted = true;
							}catch(Exception e) {
								isDeleted = false;
								e.printStackTrace();
							}
							
							if(isDeleted == true){
								DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customerId);
								try {
									ImageCompressUtil.compressImage(fileName, ext, URLUtil.profilePicBaseDirectory(), uploadedFile.getInputStream());
									//ImageUtil.compressImage(fileName, ext, URLUtil.profilePicBaseDirectory(), URLUtil.profilePicBaseDirectory());
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
						}else{
							DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customerId);
							try {
								ImageCompressUtil.compressImage(fileName, ext, URLUtil.profilePicBaseDirectory(), uploadedFile.getInputStream());
								//ImageUtil.compressImage(fileName, ext, URLUtil.profilePicBaseDirectory(), URLUtil.profilePicBaseDirectory());
							}catch(Exception e) {
								e.printStackTrace();
							}
						}
						cust.setCustImagePath(fileName);
						customerProfilePic.setStatus(1);
						customerProfilePic.setCustomerId(Integer.parseInt(customerIdStr));
						customerProfilePic.setCustomerPicWebView(URLUtil.profilePicWebURByImageName(fileName,applicationPlatForm));
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
					}else {
						
						File fileObj = AWSFileService.convertMultiPartToFile(uploadedFile);
						
						String fileName = customerId+"."+ext;
						 
						Customer cust = CustomerUtil.getCustomerById(customerId);
						String custImagePath = cust.getCustImagePath(); 
						
						if(custImagePath != null){
							
							if(!custImagePath.contains("default")) {
								try {
									AWSFileService.deleteFilesFromS3(AWSFileService.BUCKET_NAME_RTFMEDIA, custImagePath, AWSFileService.CUSTOMER_DP_DIRECTORY);
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							
							try {
								
								AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTFMEDIA, fileName, AWSFileService.CUSTOMER_DP_DIRECTORY, fileObj);
								
								DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customerId);
								CassandraDAORegistry.getCassCustomerDAO().updateProfilePicName(customerId, fileName);
								cust.setCustImagePath(fileName);
								CustomerUtil.updatedCustomerUtil(cust);
							}catch(Exception e) {
								e.printStackTrace();
							}
							 
						}else{
							
							try {
								
								AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTFMEDIA, fileName, AWSFileService.CUSTOMER_DP_DIRECTORY, fileObj);
								
								
								DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customerId);
								CassandraDAORegistry.getCassCustomerDAO().updateProfilePicName(customerId, fileName);
								cust.setCustImagePath(fileName);
								CustomerUtil.updatedCustomerUtil(cust);
							}catch(Exception e) {
								e.printStackTrace();
							} 
						}
						
						cust.setCustImagePath(fileName);
						customerProfilePic.setStatus(1);
						customerProfilePic.setCustomerId(Integer.parseInt(customerIdStr));
						customerProfilePic.setCustomerPicWebView(URLUtil.profilePicWebURByImageName(fileName,applicationPlatForm));
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success: Image Updated: "+fileName);
					}
				}
			}else if(action != null && action.equals("getProfilePic")){
				boolean customerPic = false;
				if(customer != null ){
					customerId = customer.getId();
					String fileName = customer.getCustImagePath();
					if(fileName != null){
						customerProfilePic.setStatus(1);
						customerProfilePic.setCustomerId(Integer.parseInt(customerIdStr));
						customerProfilePic.setCustomerPicWebView(URLUtil.profilePicWebURByImageName(fileName,applicationPlatForm));
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success: Get Image");
					}else{
						customerProfilePic.setStatus(1);
						customerProfilePic.setCustomerId(Integer.parseInt(customerIdStr));
						customerProfilePic.setDescription("Customer profile picture not exists");
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success: Get Image");
					}
				}
			} else if(action != null && action.equals("deleteProfilePic")){
				if(customer != null ){
					
					customerId = customer.getId();
					Customer cust = CustomerUtil.getCustomerById(customerId);
					String custImagePath = cust.getCustImagePath();
					
					boolean flag = false;
					if(custImagePath != null){
						
						if(URLUtil.isSharedDriveOn) {
							String deleteDir = URLUtil.getProfilePicFile(custImagePath);
							SmbFile deleteFile = ShareDriveUtil.getSmbFile(deleteDir);
							boolean isDeleted = false;
							try {
								deleteFile.delete();
								isDeleted = true;
							}catch(Exception e) {
								isDeleted = false;
							}
							if(isDeleted == true){
								String fileName = URLUtil.getCustomerDefaultprofilePicName(customerId);
								cust.setCustImagePath(fileName);
								CustomerUtil.updatedCustomerUtil(cust);
								DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customerId);
								
								try {
									CassandraDAORegistry.getCassCustomerDAO().updateProfilePicName(customerId, fileName);
								}catch(Exception e) {
									e.printStackTrace();
								}
								
								flag = true;
							}
						}else {
							
							if(!custImagePath.contains("default")) {
								try {
									AWSFileService.deleteFilesFromS3(AWSFileService.BUCKET_NAME_RTFMEDIA, custImagePath, AWSFileService.CUSTOMER_DP_DIRECTORY);
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							String fileName = URLUtil.getCustomerDefaultprofilePicName(customerId);
							cust.setCustImagePath(fileName);
							try {
								CustomerUtil.updatedCustomerUtil(cust);
								DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customerId);
								CassandraDAORegistry.getCassCustomerDAO().updateProfilePicName(customerId, fileName);
							}catch(Exception e) {
								e.printStackTrace();
							}
							flag = true;
						}
					} 
					if(!flag) {
						error.setDescription("Customer Profile Picture not exists or could not delete.");
						customerProfilePic.setError(error);
						customerProfilePic.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Customer Profile Picture not exists or could not delete.");
						return customerProfilePic;
					}
					
					//String ext = FilenameUtils.getExtension(cust.getCustImagePath());
					if(cust.getCustImagePath() != null) {
						customerProfilePic.setCustomerPicWebView(URLUtil.profilePicWebURByImageName(cust.getCustImagePath(),applicationPlatForm));
					}
					 customerProfilePic.setStatus(1);
					 customerProfilePic.setCustomerId(Integer.parseInt(customerIdStr));
					 TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Customer Profile Picture Deleted Successfully.");
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while writing the customer customer profile pic.");
			customerProfilePic.setError(error);
			customerProfilePic.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Error occured while writing the customer customer profile pic");
			return customerProfilePic;
		}
		return customerProfilePic;
	}
	

	@RequestMapping(value="/UpdateCustomerSignUpDetails",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails updateCustomerSignUpDetails(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,authError.getDescription());
				return customerDetails;
			}
			
			String loginPassword = request.getParameter("password");
			String loginEmail = request.getParameter("email");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm"); 
			String userId = request.getParameter("userId");
			String customerId = request.getParameter("customerId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid product type");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(loginEmail)){
				error.setDescription("Please enter Valid Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter Valid Email");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(loginPassword)){
				error.setDescription("Please enter Valid Password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter Valid Password");
				return customerDetails;
			}
			if(loginPassword.trim().length() < 8 || loginPassword.trim().length() > 20){//tempPhone.length() < 10
				error.setDescription("Password Length must be in between 8-20.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Password Length must be in between 8-20.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer Id is mandatory");
				return customerDetails;
			}
			Customer customer =CustomerUtil.getCustomerById(Integer.parseInt(customerId));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer Id is Invalid");
				return customerDetails;
			}
			Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByEmailAndProductTypeExceptThisCustomer(loginEmail,productType,customer.getId());
			if(tempCustomer != null) {
				error.setDescription("This EmailId is already signed up. Please choose a different EmailId.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"This EmailId is already signed up. Please choose a different EmailId.");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(userId)){
				error.setDescription("Please enter valid user id.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter valid user id.");
				return customerDetails;
			}
			userId = userId.trim();
			if(userId.length()< 5 || userId.length() > 12) {
				error.setDescription("Please select user id within 5 to 12 letters or numbers.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select user id within 5 to 12 letters or numbers.");
				return customerDetails;
			}
			if(!userId.matches("[a-zA-Z0-9]*")) {
				error.setDescription("Please select user id with letters and/or numbers only.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select user id with letters and/or numbers only.");
				return customerDetails;
			}
			tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductTypeExceptThisCustomer(userId, productType, customer.getId());
			RtfPromotionalEarnLifeOffers rtfReferralTemp = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
			if(tempCustomer != null || rtfReferralTemp != null) {
				error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Whoops! This userId is already signed up. Please choose a different UserId.");
				return customerDetails;
			}
			
			if(!TextUtil.isEmptyOrNull(referralcodeStr)){
			
				Boolean isRtfReferalCode = false;
				RtfPromotionalEarnLifeOffers rtfReferral = null;
				Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
				if(referralCustomer == null) {
					
					rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getActiveRtfPromotionEarnLifeOfferByPromoCode(referralcodeStr);
					Date today = new Date();
					if(rtfReferral != null && rtfReferral.getStartDate().compareTo(today)<=0 && rtfReferral.getEndDate().compareTo(today)>=0 ) {
						isRtfReferalCode = true;
					} else {
						error.setDescription("Referral Code is Invalid");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Referral Code is Invalid");
						return customerDetails;
					}
				}
				
				if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("You can't use your own referral code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You can't use your own referral code.");
					return customerDetails;
				}
				if(userId != null && userId.equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("User Id and Referal code can't be the same.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"User Id and Referal code can't be the same.");
					return customerDetails;
				}
			
				QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
				if(existReferralTracking != null) {
					error.setDescription("You have already Used Referral Code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You have already Used Referral Code.");
					return customerDetails;
				}
				
				if(isRtfReferalCode) {
					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setReferralCode(referralcodeStr);
					referraltracking.setRtfPromotionalId(rtfReferral.getId());
					referraltracking.setCreatedDateTime(new Date());
					referraltracking.setUpdatedDateTime(new Date());
					referraltracking.setIsAffiliateReferral(false);
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					
					Integer customerLives = customer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					customerLives=customerLives+rtfReferral.getMaxLifePerCustomer();
					customer.setQuizCustomerLives(customerLives);	
					DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
					CustomerUtil.updatedCustomerUtil(customer);
				} else {
					
					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					Integer tempPrimaryCustPoints=0,tempTireOneCustPoints=0;
					Integer tempPrimaryCustLives=0,tempTireOneCustLives=0;
					
					Integer customerLives = referralCustomer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					
					Integer tireOneCustLives = customer.getQuizCustomerLives();
					if(tireOneCustLives == null) {
						tireOneCustLives=0;
					}
					Integer customerPoints = referralCustomer.getRtfPoints();
					if(customerPoints == null) {
						customerPoints=0;
					}
					
					Integer tireOneCustPoints = customer.getRtfPoints();
					if(tireOneCustPoints == null) {
						tireOneCustPoints=0;
					}
					
					boolean showPrimaryCustNotification = true,showTireOneCustNotification = true;;
					
					QuizAffiliateSetting quizAffiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getActiveAffiliateByCustomerId(referralCustomer.getId());
					
					if(null != quizAffiliateSetting) {
						
						referraltracking.setIsAffiliateReferral(true);
						referraltracking.setAffiliateReferralStatus("ACTIVE");
						
						if(null != quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() && quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() > 0) {
							referraltracking.setLivesToPrimaryCustomer(true);
							tempPrimaryCustLives = tempPrimaryCustLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
							customerLives = customerLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
						}else {
							referraltracking.setLivesToPrimaryCustomer(false);
							customerLives = customerLives + 0;
							showPrimaryCustNotification = false;
						}
						
						if(null != quizAffiliateSetting.getNoOfLivesToTireOneCustomer() && quizAffiliateSetting.getNoOfLivesToTireOneCustomer() > 0) {
							referraltracking.setLivesToTireOneCustomer(true);
							tempTireOneCustLives = tempTireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
							tireOneCustLives = tireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
						}else {
							referraltracking.setLivesToTireOneCustomer(false);
							tireOneCustLives = tireOneCustLives + 0;
							showTireOneCustNotification = false;
						}
						
						if(null != quizAffiliateSetting.getTireOneCustRewards() && quizAffiliateSetting.getTireOneCustRewards() > 0) {
							QuizAffiliateReferralCredit.creditRewardDollarToTireOneCustomer(customer.getId(), quizAffiliateSetting.getCustomerId(), quizAffiliateSetting.getTireOneCustRewards());
						}
						
					}else {
						
						RtfRewardConfig rewardObj = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PRIMARYREFERRAL);
						if(rewardObj != null) {
							if(rewardObj.getNoOfLives() != null) {
								tempPrimaryCustLives = rewardObj.getNoOfLives();
								
								tempTireOneCustLives = rewardObj.getNoOfLives();	
							}
							if(rewardObj.getRtfPoints() != null) {
								tempPrimaryCustPoints = rewardObj.getRtfPoints();
								
								tempTireOneCustPoints = rewardObj.getRtfPoints();
							}
							
							customerLives = customerLives + tempPrimaryCustLives;
							customerPoints = customerPoints + tempPrimaryCustPoints;
							
							tireOneCustLives = tireOneCustLives + tempTireOneCustLives;
							tireOneCustPoints = tireOneCustPoints + tempTireOneCustPoints;
						
							referraltracking.setLivesToPrimaryCustomer(true);
							referraltracking.setLivesToTireOneCustomer(true);
						}
						//RSour
						//isCustReferral = true;
						referraltracking.setIsAffiliateReferral(false); 
						
					}
					
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setReferralCode(referralcodeStr);
					referraltracking.setYearEndRewardApplied(false);
					referraltracking.setReferralCustomerId(referralCustomer.getId());
					referraltracking.setCreatedDateTime(new Date());
					referraltracking.setUpdatedDateTime(new Date());
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					 
					
					if(showPrimaryCustNotification) {
						referralCustomer.setQuizCustomerLives(customerLives);
						referralCustomer.setRtfPoints(customerPoints);
						
						//DAORegistry.getCustomerDAO().updateQuizCustomerLives(referralCustomer.getId(),customerLives);
						DAORegistry.getCustomerDAO().updateQuizCustomerLivesAndRtfPoints(referralCustomer.getId(),customerLives,customerPoints);
						CustomerUtil.updatedCustomerUtil(referralCustomer);
						
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
						try {
							CassandraDAORegistry.getCassCustomerDAO().updateCustomerLivesAndRtfPoints(referralCustomer.getId(), customerLives, customerPoints);
							//CassCustomerUtil.updateCustomerLives(referralCustomer);
						}catch(Exception e) {
							e.printStackTrace();
						}
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
						
						try {
							CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(referralCustomer.getId(), "CR", SourceType.PRIMARYREFERRAL, referraltracking.getId(), tempPrimaryCustLives, 0, 0, tempPrimaryCustPoints, 0.0, "");
						}catch(Exception e) {
							e.printStackTrace();
						}
						try {
							String notificationMsg = "Congratulation. "+customer.getUserId()+" used your referral code. You have earned";
							if(tempPrimaryCustLives > 0) {
								notificationMsg = notificationMsg + " "+tempPrimaryCustLives+" life.";
							}
							if(tempPrimaryCustPoints > 0) {
								notificationMsg = notificationMsg + " "+tempPrimaryCustLives+" points.";
							}
							
							QuizCustomerLifeLineEarnedNotificationUtil.sendNotification(referralCustomer, notificationMsg);
							//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
					
					if(showTireOneCustNotification) {
						customer.setQuizCustomerLives(tireOneCustLives);
						customer.setRtfPoints(tireOneCustPoints);
						
						//DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),tireOneCustLives);
						DAORegistry.getCustomerDAO().updateQuizCustomerLivesAndRtfPoints(customer.getId(),tireOneCustLives,tireOneCustPoints);
						CustomerUtil.updatedCustomerUtil(customer);
						
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Begin*/
						try {
							CassandraDAORegistry.getCassCustomerDAO().updateCustomerLivesAndRtfPoints(customer.getId(), tireOneCustLives, tireOneCustPoints);
							//CassCustomerUtil.updateCustomerLives(customer);
						}catch(Exception e) {
							e.printStackTrace();
						}
						/* 12/07/2018: Update Cassandra Customer Quiz Lives - By Ulaganathan :: Ends*/
						try {
							CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(customer.getId(), "CR", SourceType.TIREONEREFERRAL, referraltracking.getId(), tempTireOneCustLives, 0, 0, tempTireOneCustPoints, 0.0, "");
						}catch(Exception e) {
							e.printStackTrace();
						}
						//QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
						try {
							String notificationMsg = "Congratulation. You have earned ";
							if(tempTireOneCustLives > 0) {
								notificationMsg = notificationMsg + " "+tempTireOneCustLives+" life.";
							}
							if(tempTireOneCustPoints > 0) {
								notificationMsg = notificationMsg + " "+tempTireOneCustPoints+" points.";
							}
							
							QuizCustomerLifeLineEarnedNotificationUtil.sendNotification(customer, notificationMsg);
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			customer.setEncryptPassword(loginPassword);
			customer.setEmail(loginEmail);
			customer.setUserName(loginEmail);
			customer.setReferrerCode(loginEmail);
			customer.setUserId(userId);
			customer.setIsEmailed(false);
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			CustomerUtil.updatedCustomerUtil(customer);
				
			customerDetails.setCustomerId(customer.getId());
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer); 
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Success");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Quiz User Id Setup details.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Error occured while Fetching Quiz User Id Setup details.");
			return customerDetails;
		}
		finally {
			log.info("QUIZ UPD CUST PHONE LOGIN : "+request.getParameter("customerId")+" : uId: "+request.getParameter("userId")+" : email: "+request.getParameter("email")+" : refCo: "+request.getParameter("referralCode")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());			
		}
		
		return customerDetails;
	}
	
	@RequestMapping(value="/CustomerLogin",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerLogin(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
				return customerDetails;
			}
			
			String loginPassword = request.getParameter("password");
			String loginEmail = request.getParameter("userName");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId=request.getParameter("notificationRegId");
			//String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Notification RegistrationID is mandatory");
				return customerDetails;
			}	
			}
			
			if(TextUtil.isEmptyOrNull(loginEmail)){
				error.setDescription("Please enter your Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Please enter your Email");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(loginPassword)){
				error.setDescription("Please enter an Email and Password to continue");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Please enter an Email and Password to continue");
				return customerDetails;
			}
			CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			//Get customer by email or user id
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserNameByProductType(loginEmail,productType);
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmailByProductType(loginEmail,productType);
			
			if(null == customer){
				error.setDescription("The email or user id you entered is incorrect. Please try again. ");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"The email or user id you entered is incorrect. Please try again.");
				return customerDetails;
			}
			String dbPassword = customer.getPassword().replace(" +", "");
			loginPassword = loginPassword.replace(" +", "");
			String password = PasswordUtil.generateEncrptedPassword(loginPassword);
			
			if(!password.equals(dbPassword)){
				error.setDescription("The password you entered is incorrect.Click Forgot Password to retrieve login information");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Click Forgot Email or Password to retrieve login information");
				return customerDetails;
			}
			
			//Get CustomerAddress by customer id and add it to the response
			//Start			
			List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
			List<UserAddress> billingAddress = new ArrayList<UserAddress>();
			List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
			
			if(userAddress != null){
				for(UserAddress address : userAddress){
					if(address.getAddressType() == AddressType.BILLING_ADDRESS)
						billingAddress.add(address);
					else
						shippingAddress.add(address);					
				}
			}						
			//End
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			CustomerLoginHistory loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(SignupType.REWARDTHEFAN);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			//loginHistory.setSocialAccessToken(fbAccessToken);			
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}
				
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			/*boolean customerPic = false;
			String profilePicPrefix = URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			if(null !=shippingAddress && shippingAddress.size() >2){
				Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			}
			customerDetails.getCustomer().setShippingAddress(shippingAddress);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Oops Somthing Went Wrong. Please Login Again!");
			return customerDetails;
		}
	}
	
	@RequestMapping(value="/CustomerRegistrationNew",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerRegistration(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String signUpTypeStr = request.getParameter("signUpType");
			String name = request.getParameter("name");
			String lastName =request.getParameter("lastName");
			//String referralCode = request.getParameter("referralCode");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId = request.getParameter("notificationRegId");
			String socialAccountId = request.getParameter("socialAccountId");
			String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			String customerPromoAlert = request.getParameter("customerPromoAlert");
			String phone = request.getParameter("phone");
			String ctyPhCode = request.getParameter("ctyPhCode");
			String userId = request.getParameter("userId");
			

			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				 ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(signUpTypeStr)){
				error.setDescription("SignUp Type is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"SignUp Type is mandatory");
				return customerDetails;
			}
			SignupType signupType=null;
			if(!TextUtil.isEmptyOrNull(signUpTypeStr)){
				try{
					signupType = SignupType.valueOf(signUpTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid signup type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid signup type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			if(signupType.equals(SignupType.FACEBOOK) || signupType.equals(SignupType.GOOGLE)){
				error.setDescription("We no longer support login/Signup via FaceBook and Google.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"We no longer support login/Signup via FaceBook and Google.");
				return customerDetails;
			}
			//if(signupType.equals(SignupType.FACEBOOK) || signupType.equals(SignupType.GOOGLE)){
				
			//}else{
				
				if(TextUtil.isEmptyOrNull(userId)){
					error.setDescription("Please enter valid user id.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter valid user id.");
					return customerDetails;
					//userId = "";
				} else {
					userId = userId.trim();
					if(userId.length()< 5 || userId.length() > 12) {
						error.setDescription("Please select user id within 5 to 12 letters or numbers.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please select user id within 5 to 12 letters or numbers.");
						return customerDetails;
					}
					if(!userId.matches("[a-zA-Z0-9]*")) {
						error.setDescription("Please select user id with letters and/or numbers only.");
						customerDetails.setError(error);
						
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please select user id with letters and/or numbers only.");
						return customerDetails;
					}
					Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
					RtfPromotionalEarnLifeOffers rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
					if(tempCustomer != null || rtfReferral != null) {
							error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
							customerDetails.setError(error);
							customerDetails.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Whoops! This userId is already signed up. Please choose a different UserId.");
							return customerDetails;
					}
				}
			//}		
			
			/*if(TextUtil.isEmptyOrNull(lastName)){
				error.setDescription("Customer LastName is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Customer LastName is mandatory");
				return customerDetails;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(name)){
				error.setDescription("Please enter your First Name");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your First Name");
				return customerDetails;
			}*/
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please enter your Phone No");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Phone no");
				return customerDetails;
			}
			phone =  QuizSettings.validatePhoneNo(phone);
			if(phone.length() < 10){
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			
			if(!TextUtil.isEmptyOrNull(customerPromoAlert) && customerPromoAlert.equals("YES")){
				customerDetails.setShowPromoAert(true);
				customerDetails.setPromoAlertMessage(TextUtil.getCustomerPromoAlertText());
			}
			
			String finalPassword = password;
			CustomerLoyalty customerLoyalty = null;
			Customer validateCustomer = null;
			CustomerLoginHistory loginHistory = null;
			CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			if(signupType.equals(SignupType.FACEBOOK)){
				
				if(TextUtil.isEmptyOrNull(socialAccountId)){
					error.setDescription(signupType+" Account Id is Mandatory");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,signupType+" Account Id is Mandatory");
					return customerDetails;
				}
				
				validateCustomer = DAORegistry.getCustomerDAO().getCustomerBySocialAcctIdByProductType(socialAccountId,productType);
				
				boolean errorFalg = false;
				if(null == validateCustomer){
					errorFalg = true;
					if(!TextUtil.isEmptyOrNull(email)){
						validateCustomer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
						if(null == validateCustomer){
							errorFalg = true;
						}else{
							errorFalg = false;
						}
					}
				}
				
				if(errorFalg){
					
					if(TextUtil.isEmptyOrNull(fbAccessToken)){
						error.setDescription("FaceBook Access Token is Mandatory");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"FaceBook Access Token is Mandatory");
						return customerDetails;
					}
					
					if(TextUtil.isEmptyOrNull(email)){
						error.setDescription("Please enter your Email");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Email");
						return customerDetails;
					}
					
				}else{
					
					if(!TextUtil.isEmptyOrNull(email) && !validateCustomer.getEmail().equals(email) &&
							null != validateCustomer.getSecondaryEmail() && !validateCustomer.getSecondaryEmail().equals(email)){
						validateCustomer.setSecondaryEmail(email);
						DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
					}
					
					//Get CustomerAddress by customer id and add it to the response
					//Start
					
					List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(validateCustomer.getId());
					List<UserAddress> billingAddress = new ArrayList<UserAddress>();
					List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
					
					if(userAddress != null){
						for(UserAddress address : userAddress){
							if(address.getAddressType() == AddressType.BILLING_ADDRESS)
								billingAddress.add(address);
							else
								shippingAddress.add(address);					
						}
					}								
					//End
					
					List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(validateCustomer.getId());
					
					if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
						validateCustomer.setFirstTimeLogin("No");
					}else{
						validateCustomer.setFirstTimeLogin("Yes");
					}
					loginHistory = new CustomerLoginHistory();
					loginHistory.setCustomerId(validateCustomer.getId());
					loginHistory.setApplicationPlatForm(applicationPlatForm);
					loginHistory.setLastLoginTime(new Date());
					loginHistory.setLoginType(signupType);
					loginHistory.setDeviceId(deviceId);
					loginHistory.setNotificationRegId(notificationRegId);
					loginHistory.setLoginIp(loginIp);
					loginHistory.setSocialAccessToken(fbAccessToken);			
					DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
					
					//executed only when customer is already registered with us from RTW and RTW2 database
					if(validateCustomer.getProductType().equals(ProductType.RTW) || validateCustomer.getProductType().equals(ProductType.RTW2) 
							|| validateCustomer.getProductType().equals(ProductType.TIXCITY)){
						validateCustomer.setProductType(ProductType.REWARDTHEFAN);
						validateCustomer.setSignupType(SignupType.REWARDTHEFAN);
						validateCustomer.setSignupDate(new Date());
						validateCustomer.setIsEmailed(true);
						validateCustomer.setApplicationPlatForm(applicationPlatForm);
						validateCustomer.setCustomerName(name);
						validateCustomer.setLastName(null!=lastName?lastName:"");
						/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
						String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
						Long referrerNumber =  Long.valueOf(property.getValue());
						referrerNumber++;  
						validateCustomer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
						property.setValue(String.valueOf(referrerNumber));
						DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
						validateCustomer.setReferrerCode(validateCustomer.getEmail());
						finalPassword = ReferralCodeGenerator.generatePassword(name, "RTF");
						validateCustomer.setEncryptPassword(finalPassword);
						
						validateCustomer.setUserId(userId);
						if(phone != null && !phone.isEmpty()) {
							validateCustomer.setPhone(phone);
							validateCustomer.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);;
						}
						DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
						
						if(validateCustomer.getCustImagePath() == null) {
							String fileName = URLUtil.getCustomerDefaultprofilePicName(validateCustomer.getId());
							validateCustomer.setCustImagePath(fileName);
							DAORegistry.getCustomerDAO().updateCustImagePath(fileName, validateCustomer.getId());
							//CustomerUtil.updatedCustomerUtil(customer);
						}
						/*Add new Customer to Customer Util - Begins*/
						try{
							CustomerUtil.updatedCustomerUtil(validateCustomer);
						}catch(Exception e){
							e.printStackTrace();
						}
						/*Add new Customer to Customer Util - Ends*/
						
						try{
							//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
							DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(validateCustomer.getId());
						}catch(Exception e){
							e.printStackTrace();
						}
						
						
					}
					
					if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
							applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
						
						if(null == customerDeviceDetails){
							
							customerDeviceDetails = new CustomerDeviceDetails();
							customerDeviceDetails.setCustomerId(validateCustomer.getId());
							customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
							customerDeviceDetails.setCreatedDate(new Date());
							customerDeviceDetails.setLastUpdated(new Date());
							customerDeviceDetails.setStatus("ACTIVE");
							customerDeviceDetails.setDeviceId(deviceId);
							customerDeviceDetails.setNotificationRegId(notificationRegId);
							customerDeviceDetails.setLoginIp(loginIp);
							DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
						}else{
							customerDeviceDetails.setCustomerId(validateCustomer.getId());
							customerDeviceDetails.setLastUpdated(new Date());
							customerDeviceDetails.setNotificationRegId(notificationRegId);
							DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
						}
						
						try{
							CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
						}catch(Exception e){
							e.printStackTrace();
						}
						
					}
					
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					if(null != customerLoyalty){
						
					}else{
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setCustomerId(validateCustomer.getId());
						customerLoyalty.setActivePointsAsDouble(0.00);
						customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
						customerLoyalty.setLatestSpentPointsAsDouble(0.00);
						customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
						customerLoyalty.setTotalSpentPointsAsDouble(0.00);
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLastNotifiedTime(new Date());
						customerLoyalty.setPendingPointsAsDouble(0.00);
						customerLoyalty.setTotalAffiliateReferralDollars(0.00);
						customerLoyalty.setLastAffiliateReferralDollars(0.00);
						
						//Save Customer Loyalty Information
						DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
					}
					
					customerDetails.setCustomerLoyalty(customerLoyalty);
					customerDetails.setCustomer(validateCustomer);
					customerDetails.getCustomer().setBillingAddress(billingAddress);
					if(null !=shippingAddress && shippingAddress.size() >2){
						Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
					}
					customerDetails.getCustomer().setShippingAddress(shippingAddress);
					customerDetails.setStatus(1);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
					return customerDetails;
					
				}
				
				//Password we are generating for facebook and google singup.
				/*Calendar calendar = new GregorianCalendar();
				finalPassword = name.replaceAll("\\s+", "")+calendar.get(Calendar.YEAR);*/
				
			}else if(signupType.equals(SignupType.GOOGLE)){
				
				if(TextUtil.isEmptyOrNull(email)){
					error.setDescription("Please enter your Email");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Email");
					return customerDetails;
				}
				
				if(TextUtil.isEmptyOrNull(socialAccountId)){
					error.setDescription(signupType+" Account Id is Mandatory");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,signupType+" Account Id is Mandatory");
					return customerDetails;
				}
				
				//for considering rtw and rtw2 customer for registration
				//validateCustomer = DAORegistry.getCustomerDAO().getCustomerByEmailByProductType(email,productType);
				validateCustomer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
				if(validateCustomer != null){
					
					/*if(!TextUtil.isEmptyOrNull(email) && !validateCustomer.getEmail().equals(email) &&
							null != validateCustomer.getSecondaryEmail() && !validateCustomer.getSecondaryEmail().equals(email)){
						validateCustomer.setSecondaryEmail(email);
						DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
					}*/
					
					List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(validateCustomer.getId());
					
					if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
						validateCustomer.setFirstTimeLogin("No");
					}else{
						validateCustomer.setFirstTimeLogin("Yes");
					}
					loginHistory = new CustomerLoginHistory();
					loginHistory.setCustomerId(validateCustomer.getId());
					loginHistory.setApplicationPlatForm(applicationPlatForm);
					loginHistory.setLastLoginTime(new Date());
					loginHistory.setLoginType(signupType);
					loginHistory.setDeviceId(deviceId);
					loginHistory.setNotificationRegId(notificationRegId);
					loginHistory.setLoginIp(loginIp);
					loginHistory.setSocialAccessToken(fbAccessToken);			
					DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
					
					
					//Get CustomerAddress by customer id and add it to the response
					//Start
					
					List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(validateCustomer.getId());
					List<UserAddress> billingAddress = new ArrayList<UserAddress>();
					List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
					
					if(userAddress != null){
						for(UserAddress address : userAddress){
							if(address.getAddressType() == AddressType.BILLING_ADDRESS)
								billingAddress.add(address);
							else
								shippingAddress.add(address);					
						}
					}								
					
					//executed only when customer is already registered with us from RTW and RTW2 database
					if(validateCustomer.getProductType().equals(ProductType.RTW) || validateCustomer.getProductType().equals(ProductType.RTW2)
							|| validateCustomer.getProductType().equals(ProductType.TIXCITY)){
						validateCustomer.setProductType(ProductType.REWARDTHEFAN);
						validateCustomer.setSignupType(SignupType.REWARDTHEFAN);
						validateCustomer.setSignupDate(new Date());
						validateCustomer.setIsEmailed(true);
						validateCustomer.setApplicationPlatForm(applicationPlatForm);
						validateCustomer.setCustomerName(name);
						validateCustomer.setLastName(null!=lastName?lastName:"");
						/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
						String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
						Long referrerNumber =  Long.valueOf(property.getValue());
						referrerNumber++;  
						validateCustomer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
						property.setValue(String.valueOf(referrerNumber));
						DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
						validateCustomer.setReferrerCode(validateCustomer.getEmail());
						finalPassword = ReferralCodeGenerator.generatePassword(name, "RTF");
						validateCustomer.setEncryptPassword(finalPassword);
						
						validateCustomer.setUserId(userId);
						if(phone != null && !phone.isEmpty()) {
							validateCustomer.setPhone(phone);
							validateCustomer.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
						}
						
						DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
						
						if(validateCustomer.getCustImagePath() == null) {
							String fileName = URLUtil.getCustomerDefaultprofilePicName(validateCustomer.getId());
							validateCustomer.setCustImagePath(fileName);
							DAORegistry.getCustomerDAO().updateCustImagePath(fileName, validateCustomer.getId());
							//CustomerUtil.updatedCustomerUtil(customer);
						}
						/*Add new Customer to Customer Util - Begins*/
						try{
							CustomerUtil.updatedCustomerUtil(validateCustomer);
						}catch(Exception e){
							e.printStackTrace();
						}
						/*Add new Customer to Customer Util - Ends*/
						
						try{
							//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
							DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(validateCustomer.getId());
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					
					if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
							applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
						
						if(null == customerDeviceDetails){
							customerDeviceDetails = new CustomerDeviceDetails();
							customerDeviceDetails.setCustomerId(validateCustomer.getId());
							customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
							customerDeviceDetails.setCreatedDate(new Date());
							customerDeviceDetails.setLastUpdated(new Date());
							customerDeviceDetails.setStatus("ACTIVE");
							customerDeviceDetails.setDeviceId(deviceId);
							customerDeviceDetails.setNotificationRegId(notificationRegId);
							customerDeviceDetails.setLoginIp(loginIp);
							DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
						}else{
							customerDeviceDetails.setCustomerId(validateCustomer.getId());
							customerDeviceDetails.setLastUpdated(new Date());
							customerDeviceDetails.setNotificationRegId(notificationRegId);
							DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
						}
						
						try{
							CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
						}catch(Exception e){
							e.printStackTrace();
						}
						
					}
					
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					if(null != customerLoyalty){
						
					}else{
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setCustomerId(validateCustomer.getId());
						customerLoyalty.setActivePointsAsDouble(0.00);
						customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
						customerLoyalty.setLatestSpentPointsAsDouble(0.00);
						customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
						customerLoyalty.setTotalSpentPointsAsDouble(0.00);
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLastNotifiedTime(new Date());
						customerLoyalty.setPendingPointsAsDouble(0.00);
						customerLoyalty.setTotalAffiliateReferralDollars(0.00);
						customerLoyalty.setLastAffiliateReferralDollars(0.00);
						//Save Customer Loyalty Information
						DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
					}
					
					customerDetails.setCustomerLoyalty(customerLoyalty);
					customerDetails.setCustomer(validateCustomer);
					customerDetails.getCustomer().setBillingAddress(billingAddress);
					if(null !=shippingAddress && shippingAddress.size() >2){
						Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
					}
					customerDetails.getCustomer().setShippingAddress(shippingAddress);
					
					customerDetails.setStatus(1);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
					return customerDetails;
				}
				//Password we are generating for facebook and google singup.
				/*Calendar calendar = new GregorianCalendar();
				finalPassword = name.replaceAll("\\s+", "")+calendar.get(Calendar.YEAR);*/
				
			}else{
				if(TextUtil.isEmptyOrNull(email)){
					error.setDescription("Please enter your Email");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Email");
					return customerDetails;
				}
				
				if(TextUtil.isEmptyOrNull(password)){
					error.setDescription("Please choose a Password");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please choose a Password");
					return customerDetails;
				}
				finalPassword = password;
				
				validateCustomer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
				
				//executed only when customer is already registered with us from RTW and RTW2 database
				if(validateCustomer != null && (validateCustomer.getProductType().equals(ProductType.RTW) || 
						validateCustomer.getProductType().equals(ProductType.RTW2) || validateCustomer.getProductType().equals(ProductType.TIXCITY))){
					validateCustomer.setProductType(ProductType.REWARDTHEFAN);
					validateCustomer.setSignupType(SignupType.REWARDTHEFAN);
					validateCustomer.setSignupDate(new Date());
					validateCustomer.setIsEmailed(true);
					validateCustomer.setApplicationPlatForm(applicationPlatForm);
					validateCustomer.setCustomerName(null!=name?name:"");
					validateCustomer.setLastName(null!=lastName?lastName:"");
					/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
					String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
					Long referrerNumber =  Long.valueOf(property.getValue());
					referrerNumber++;  
					validateCustomer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
					property.setValue(String.valueOf(referrerNumber));
					DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
					validateCustomer.setReferrerCode(validateCustomer.getEmail());
					validateCustomer.setEncryptPassword(finalPassword);
					validateCustomer.setBrokerId(1001);
					validateCustomer.setIsClient(true);
					
					validateCustomer.setUserId(userId);
					if(phone != null && !phone.isEmpty()) {
						validateCustomer.setPhone(phone);
						validateCustomer.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
					}
					DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
					
					if(validateCustomer.getCustImagePath() == null) {
						String fileName = URLUtil.getCustomerDefaultprofilePicName(validateCustomer.getId());
						validateCustomer.setCustImagePath(fileName);
						DAORegistry.getCustomerDAO().updateCustImagePath(fileName, validateCustomer.getId());
						//CustomerUtil.updatedCustomerUtil(customer);
					}
					/*Add new Customer to Customer Util - Begins*/
					try{
						CustomerUtil.updatedCustomerUtil(validateCustomer);
					}catch(Exception e){
						e.printStackTrace();
					}
					/*Add new Customer to Customer Util - Ends*/
					try{
						//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
						DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(validateCustomer.getId());
					}catch(Exception e){
						e.printStackTrace();
					}
					
					
					String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(validateCustomer.getId()+""+email.split("@")[0]);
					
					Calendar calendar = new GregorianCalendar();
					calendar.add(Calendar.YEAR, 1);
					
					RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
					custPromoOffer.setCreatedDate(new Date());
					custPromoOffer.setCustomerId(validateCustomer.getId());
					custPromoOffer.setDiscount(25.00);
					custPromoOffer.setStartDate(new Date());
					custPromoOffer.setEndDate(calendar.getTime());
					custPromoOffer.setFlatOfferOrderThreshold(1.00);
					custPromoOffer.setIsFlatDiscount(false);
					custPromoOffer.setMaxOrders(1);
					custPromoOffer.setModifiedDate(new Date());
					custPromoOffer.setPromoCode(custPromoCode);
					custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
					custPromoOffer.setNoOfOrders(0);
					custPromoOffer.setStatus("ENABLED");
					
					boolean isEmailSent = true;
					String toEmail = validateCustomer.getEmail();
					try{
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("promotionalCode", custPromoCode);
						mailMap.put("userName",validateCustomer.getEmail());
						/*String userNameHtml = "<span style='color: #F06931;'> "+validateCustomer.getEmail()+" ";
						if(null != validateCustomer.getUserId() && !validateCustomer.getUserId().isEmpty()){
							userNameHtml += "</span> or <span style='color: #F06931;'> "+validateCustomer.getUserId()+" </span>";
						}else{
							userNameHtml += "</span>";
						}
						mailMap.put("userNameHtml",userNameHtml);*/
						
						//inline(image in mail body) image add code
						MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, validateCustomer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
						
					}catch(Exception e1){
						e1.printStackTrace();
						isEmailSent = false;
						toEmail = "";
					}
					custPromoOffer.setIsEmailSent(isEmailSent);
					custPromoOffer.setToEmail(toEmail);
					//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
					
					
					//List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(validateCustomer.getId());
					List<UserAddress> billingAddress = new ArrayList<UserAddress>();
					List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
					
					//if(userAddress != null){
					//	for(UserAddress address : userAddress){
					//		if(address.getAddressType() == AddressType.BILLING_ADDRESS)
					//			billingAddress.add(address);
					//		else
					//			shippingAddress.add(address);					
					//	}
					//}	
					
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					
					if(null != customerLoyalty){
						
					}else{
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setCustomerId(validateCustomer.getId());
						customerLoyalty.setActivePointsAsDouble(0.00);
						customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
						customerLoyalty.setLatestSpentPointsAsDouble(0.00);
						customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
						customerLoyalty.setTotalSpentPointsAsDouble(0.00);
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLastNotifiedTime(new Date());
						customerLoyalty.setPendingPointsAsDouble(0.00);
						customerLoyalty.setTotalAffiliateReferralDollars(0.00);
						customerLoyalty.setLastAffiliateReferralDollars(0.00);
						//Save Customer Loyalty Information
						DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
					}
					
					try {
						CassCustomerUtil.addCustomer(validateCustomer, customerLoyalty);
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					//customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					customerDetails.setCustomerLoyalty(customerLoyalty);
					customerDetails.setCustomer(validateCustomer);
					customerDetails.getCustomer().setBillingAddress(billingAddress);
					if(null !=shippingAddress && shippingAddress.size() >2){
						Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
					}
					customerDetails.getCustomer().setShippingAddress(shippingAddress);
					
					customerDetails.setStatus(1);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
					return customerDetails;
				}else{
					if(null != validateCustomer){
						error.setDescription("Whoops! This email is already signed up. Please choose a different Email.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Whoops! This email is already signed up. Please choose a different Email.");
						return customerDetails;
					}
				}
			}
			
			Customer customer = new Customer();
			
			//11-21-2018 3:22 PM - Give 1 life to all new registration. Given by Mitul - Done By Ulaganathan
			customer.setQuizCustomerLives(1);
			
			customer.setBrokerId(1001);
			customer.setIsClient(true);
			customer.setProductType(productType);
			customer.setSignupType(signupType);
			customer.setCustomerName(null!=name?name:"");
			customer.setLastName(null!=lastName?lastName:"");
			//customer.setReferralCode(null!=referralCode?referralCode:"");
			customer.setEmail(email);
			customer.setEncryptPassword(finalPassword);
			customer.setUserName(email);
			customer.setSignupDate(new Date());
			customer.setCustomerType(CustomerType.WEB_CUSTOMER);
			customer.setApplicationPlatForm(applicationPlatForm);
			customer.setDeviceId(null != deviceId?deviceId:"");
			customer.setNotificationRegId(null!=notificationRegId?notificationRegId:"");
			customer.setSocialAccountId(null != socialAccountId?socialAccountId:"");
			
			customer.setUserId(userId);
			customer.setPhone((null != phone && !phone.equals(""))?phone:null);
			customer.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
			
			/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
			String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
			Long referrerNumber =  Long.valueOf(property.getValue());
			referrerNumber++;  
			customer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
			property.setValue(String.valueOf(referrerNumber));
			DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
			customer.setReferrerCode(email);
			//Save Customer
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			
			//property.setValue(String.valueOf(referrerNumber));
			
			//Save Customer Referral Code Auto Number
			//DAORegistry.getPropertyDAO().saveOrUpdate(property);
			
			if(customer.getCustImagePath() == null) {
				String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
				customer.setCustImagePath(fileName);
				DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
				//CustomerUtil.updatedCustomerUtil(customer);
			}
			/*Add new Customer to Customer Util - Begins*/
			try{
				CustomerUtil.updatedCustomerUtil(customer);
			}catch(Exception e){
				e.printStackTrace();
			}
			/*Add new Customer to Customer Util - Ends*/
			
			
			customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			if(null != customerLoyalty){
				
			}else{
				customerLoyalty = new CustomerLoyalty();
				customerLoyalty.setCustomerId(customer.getId());
				customerLoyalty.setActivePointsAsDouble(0.00);
				customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
				customerLoyalty.setLatestSpentPointsAsDouble(0.00);
				customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
				customerLoyalty.setTotalSpentPointsAsDouble(0.00);
				customerLoyalty.setLastUpdate(new Date());
				customerLoyalty.setLastNotifiedTime(new Date());
				customerLoyalty.setPendingPointsAsDouble(0.00);
				customerLoyalty.setTotalAffiliateReferralDollars(0.00);
				customerLoyalty.setLastAffiliateReferralDollars(0.00);
				//Save Customer Loyalty Information
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			}
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(signupType);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			loginHistory.setSocialAccessToken(fbAccessToken);	
			
			//Save Customer Login History
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
				}
				//Save Customer Device Details
				DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
			
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Begins*/
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Ends*/
			}
			
			String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+email.split("@")[0]);
			
			Calendar calendar = new GregorianCalendar();
			calendar.add(Calendar.YEAR, 1);
			
			RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
			custPromoOffer.setCreatedDate(new Date());
			custPromoOffer.setCustomerId(customer.getId());
			custPromoOffer.setDiscount(25.00);
			custPromoOffer.setStartDate(new Date());
			custPromoOffer.setEndDate(calendar.getTime());
			custPromoOffer.setFlatOfferOrderThreshold(1.00);
			custPromoOffer.setIsFlatDiscount(false);
			custPromoOffer.setMaxOrders(1);
			custPromoOffer.setModifiedDate(new Date());
			custPromoOffer.setPromoCode(custPromoCode);
			custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
			custPromoOffer.setNoOfOrders(0);
			custPromoOffer.setStatus("ENABLED");
			
			boolean isEmailSent = true;
			String toEmail = customer.getEmail();
			
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("promotionalCode", custPromoCode);
			mailMap.put("userName",customer.getEmail());
			
			/*String userNameHtml = "<span style='color: #F06931;'> "+customer.getEmail()+" ";
			if(null != customer.getUserId() && !customer.getUserId().isEmpty()){
				userNameHtml += "</span> or <span style='color: #F06931;'> "+customer.getUserId()+" </span>";
			}else{
				userNameHtml += "</span>";
			}
			mailMap.put("userNameHtml",userNameHtml);*/
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			
			try{
				switch (productType) {
					case REWARDTHEFAN:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
						break;
					
					case ZONETICKETS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case MINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case LASTROWMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case VIPMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.fromEmail, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
	
					default:
						break;
				}
			
			}catch(Exception e){
				e.printStackTrace();
				isEmailSent = false;
				toEmail = "";
			}
			
			custPromoOffer.setIsEmailSent(isEmailSent);
			custPromoOffer.setToEmail(toEmail);
			//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
			
			/*boolean customerPic = false;
			String profilePicPrefix =URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while creating customer.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Error occured while creating customer");
			return customerDetails;
		}
	}
	
	@RequestMapping(value="/GetCustomerLoyaltyRewards",method=RequestMethod.POST)
	public @ResponsePayload CustomerLoyaltyDetails getLoyaltyReward(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerLoyaltyDetails customerLoyaltyDetails =new CustomerLoyaltyDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerLoyaltyDetails.setError(error);
						customerLoyaltyDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"You are not authorized");
						return customerLoyaltyDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"You are not authorized");
					return customerLoyaltyDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String customerId= request.getParameter("customerId");
						
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"Product Type is mandatory");
				return customerLoyaltyDetails;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"Please send valid product type");
					return customerLoyaltyDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customet ID  is mandatory");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"Customet ID is mandatory");
				return customerLoyaltyDetails;
			}
			Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(Integer.parseInt(customerId));
			if(null != customerLoyalty){
				Double totalContestReward = 0.00;
				Integer custId = Integer.parseInt(customerId);
				try{
					totalContestReward = DAORegistry.getQueryManagerDAO().getContestRewardDollarsByCustomerId(custId);
					customerLoyalty.setContestRewardDollarsDouble(totalContestReward);
				}catch(Exception e){
					System.out.println("ERROR In REWARD FETCH: CustomerId:"+customerId);
					e.printStackTrace();
				}
				
				QuizAffiliateSetting affiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getAffiliate(custId);
				if(null != affiliateSetting) {
					Double total = affiliateSetting.getTotalCashReward() + affiliateSetting.getTotalCreditedRewardDollar();
					customerLoyalty.setTotalAffiliateReferralDollars(total);
					customerLoyalty.setShowAffiliateReward(true);
				}
			}
			customerLoyaltyDetails.setCustomer(customer);
			customerLoyalty.setShowReferralReward(true);
			customerLoyaltyDetails.setCustomerLoyalty(customerLoyalty);
			//customerLoyaltyDetails.setConversionMeassage("LoyaltyConversionMessage 1 point = 1$");
			customerLoyaltyDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"Success");
			return customerLoyaltyDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting customer details.");
			customerLoyaltyDetails.setError(error);
			customerLoyaltyDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOYALTYREWARD,"Error occured while gettting customer details");
			return customerLoyaltyDetails;
		}
	}
	
	@RequestMapping(value = "/GetCustomerInfo", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails getCustomerInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
				return customerDetails;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Customer Id is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Product Type is mandatory");
				return customerDetails;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(, ProductType.REWARDTHEFAN);
			if(customer == null){
				error.setDescription("Customer not valid");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Customer not valid");
				return customerDetails;
			}
			
			/*if(customer.getUserId() != null) {
				QuizCustomerReferralTracking referalTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByReferralCode(customer.getUserId());
				if(referalTracking != null) {
					customerDetails.setIsUserIdEditable(false);
				}
			}*/
			customerDetails.setIsUserIdEditable(false);
			
			CustomerLoyalty customerLoyalty = null;
			List<UserAddress> billingAddress = new ArrayList<UserAddress>();
			List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
			Integer latestShippingId = null;
			
			if(customer != null){
				customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
				
				//Get CustomerAddress by customer id and add it to the response
				//Start			
				List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
							
				if(userAddress != null){
					
					
					Date lastestTime = null;
					
					
					for(UserAddress address : userAddress){
						if(address.getAddressType() == AddressType.BILLING_ADDRESS){
							billingAddress.add(address);
						}else{
							shippingAddress.add(address);
							if(lastestTime == null){
								lastestTime = address.getLastUpdated();
								latestShippingId = address.getId();
							}else{
								Date curObjTime = address.getLastUpdated();
								if(curObjTime.after(lastestTime)){
									lastestTime = address.getLastUpdated();
									latestShippingId = address.getId();
								}
							}
							
						}
						}
					}
										
				//End
				
				if(null !=shippingAddress && shippingAddress.size() >2){
					Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
				}
				
				}
			customerDetails.setStatus(1);
			customerDetails.setCustomer(customer);
		    customerDetails.setRevisedId(latestShippingId);
		    if(null != customer.getCustImagePath() ) {
		    	customerDetails.setCustomerPicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath()));
		    }
		    //System.out.println("Revised Id------>"+latestShippingId);
			customer.setBillingAddress(null != billingAddress && !billingAddress.isEmpty()?billingAddress:null);
			
			if(null !=shippingAddress && shippingAddress.size() >2){
				Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			}
			customer.setShippingAddress(null != shippingAddress && !shippingAddress.isEmpty()?shippingAddress:null);
			customerDetails.setCustomerLoyalty(customerLoyalty);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Success");
			}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Information.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Error occured while Fetching Customer Information");
			return customerDetails;
		}
		
		return customerDetails;
	
	}
	
	@RequestMapping(value="/AddCustomerAddress",method=RequestMethod.POST)
	public @ResponsePayload CustomerAddress addCustomerAddress(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerAddress customerAddress =new CustomerAddress();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You are not authorized");
				return customerAddress;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerAddress.setError(error);
						customerAddress.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You are not authorized");
						return customerAddress;
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You are not authorized");
					return customerAddress;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You are not authorized");
				return customerAddress;
			} */
			
			String customerIdStr = request.getParameter("customerId");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String addressTypeStr = request.getParameter("addressType");
			String addressLine1 = request.getParameter("addressLine1");
			String addressLine2 = request.getParameter("addressLine2");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String postalCode = request.getParameter("postalCode");
			String country = request.getParameter("country");
			String ctyPhCode = request.getParameter("ctyPhCode");
			String phone1 = request.getParameter("phone1");
			String phone2 = request.getParameter("phone2");
			String phone3 = request.getParameter("phone3");
			String email = request.getParameter("email");
			String platForm = request.getParameter("platForm");
			String productTypeStr = request.getParameter("productType");
			String action = request.getParameter("action");// Save or Update
			String addressIdStr = request.getParameter("addressId");
			

			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				 ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Product Type is mandatory");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(action)){
				error.setDescription("Action is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Action is mandatory");
				return customerAddress;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please send valid product type");
					return customerAddress;
				}
			}
					
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Customer Id is mandatory");
				return customerAddress;
			}
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer = DAORegistry.getCustomerDAO().getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Customer is not recognized");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Customer is not recognized");
					return customerAddress;
				}
			}catch(Exception e){
				error.setDescription("Customer is not recognized");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Customer is not recognized");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(addressTypeStr)){
				error.setDescription("Address Type is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Address Type is mandatory");
				return customerAddress;
			}
			
			
			AddressType addressType = null;
			
			try{
				addressType = AddressType.valueOf(addressTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid Address type");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please send valid Address type");
				return customerAddress;
			}
			
			
			if(TextUtil.isEmptyOrNull(firstName)){
				error.setDescription("Please enter your First Name");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please enter your First Name");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(lastName)){
				error.setDescription("Please enter your Last Name");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please enter your Last Name");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(addressLine1)){
				error.setDescription("Please enter your Address");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please enter your Address");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(city)){
				error.setDescription("Please enter City");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please enter City");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(postalCode)){
				error.setDescription("Please enter Postal Code");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please enter Postal Code");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(state)){
				error.setDescription("Please choose a State");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please choose a State");
				return customerAddress;
			}
			if(TextUtil.isEmptyOrNull(country)){
				error.setDescription("Please choose a Country");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please choose a Country");
				return customerAddress;
			}
			
			Integer stateId = null, countryId = null;
			Country countryObj = null;
			State stateObj =  null;
			try{
				stateId = Integer.parseInt(state.trim());
				stateObj =  DAORegistry.getStateDAO().getStateById(stateId);
				if(null == stateObj){
					error.setDescription("You must choose a valid State");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You must choose a valid State");
					return customerAddress;
				}
			}catch(Exception e){
				error.setDescription("You must choose a valid State");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You must choose a valid State");
				return customerAddress;
			}
			
			try{
				countryId = Integer.parseInt(country.trim());
				countryObj = DAORegistry.getCountryDAO().get(countryId);
				if(null == countryObj){
					error.setDescription("Please choose valid country");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please choose valid country");
					return customerAddress;
				}
			}catch(Exception e){
				error.setDescription("You must choose a valid Country");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You must choose a valid Country");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(phone1)){
				error.setDescription("Please enter a phone number");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please enter a phone number");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(email)){
				error.setDescription("Please enter an email");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Please enter an email");
				return customerAddress;
			}
			
			Integer addressId=null;					
			UserAddress userAddress = null;
			if(!TextUtil.isEmptyOrNull(addressIdStr)){
				try{
					addressId = Integer.parseInt(addressIdStr.trim());
					userAddress = DAORegistry.getUserAddressDAO().get(addressId);
				}catch(Exception e){
					error.setDescription("User Address Id not Recognized");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"User Address Id not Recognized");
					return customerAddress;
				}
			}
			
				
			int shippingAddressCount = 0;
			List<UserAddress> userAddressList = DAORegistry.getUserAddressDAO().getUserAddressInfo(customerId,addressType);
			shippingAddressCount = DAORegistry.getUserAddressDAO().getShippingAddressCount(customerId);

			switch (addressType) {
				case BILLING_ADDRESS:
					UserAddress billingAddress = (null != userAddressList && !userAddressList.isEmpty())?userAddressList.get(0):null;
					
					String msg = "Billing Address successfully updated!";
					if(null == billingAddress){
						billingAddress=  new UserAddress();
						billingAddress.setStatus(Status.ACTIVE);
						billingAddress.setCustomerId(Integer.valueOf(customerIdStr));
						msg = "Billing Address successfully added!";
					}
					billingAddress.setFirstName(firstName);
					billingAddress.setLastName(lastName);
					billingAddress.setAddressLine1(addressLine1);
					billingAddress.setAddressLine2(addressLine2);
					billingAddress.setAddressType(AddressType.BILLING_ADDRESS);
					billingAddress.setCity(city);;
					billingAddress.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
					billingAddress.setPhone1(phone1);
					billingAddress.setPhone2(phone2);
					billingAddress.setPhone3(phone3);
					billingAddress.setEmail(email != null && !email.isEmpty()?email:null);
					billingAddress.setZipCode(postalCode);
					billingAddress.setCountry(countryObj);
					billingAddress.setState(stateObj);
					billingAddress.setLastUpdated(new Date());
					DAORegistry.getUserAddressDAO().saveOrUpdate(billingAddress);
					
					/* Update Customer First Name & Last Name While Customer Added Billing Address 
					 * if customer do not have first & Last Name - Added By Ulaganathan on 5/15/2018 11:20AM*/
					if(null == customer.getCustomerName() || customer.getCustomerName().isEmpty()){
						try{
							customer.setCustomerName(firstName);
							customer.setLastName(lastName);
							DAORegistry.getCustomerDAO().update(customer);
							CustomerUtil.updatedCustomerUtil(customer);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					/* End - Added By Ulaganathan on 5/15/2018 11:20AM*/
					
					List<UserAddress> shippingAddressList = DAORegistry.getUserAddressDAO().getUserAddressInfo(customerId,AddressType.SHIPPING_ADDRESS);
					List<UserAddress> billingAddressList = new ArrayList<UserAddress>();
					
					if(null !=shippingAddressList && shippingAddressList.size() >2){
						Collections.sort(shippingAddressList,CityUtil.sortShippingAddressByUpdatedTime);
					}
					
					billingAddressList.add(billingAddress);
						
					/*try{
						if(phone1 != null && !phone1.isEmpty()){
							customer.setPhone(phone1);
							DAORegistry.getCustomerDAO().saveOrUpdate(customer);
							CustomerUtil.updatedCustomerUtil(customer);
						}
					}catch(Exception e){
						e.printStackTrace();
					}*/
					
					customer.setBillingAddress(billingAddressList);
					customer.setShippingAddress(shippingAddressList);
					
					customerAddress.setRevisedAddressType(addressType);
					customerAddress.setRevisedId(billingAddress.getId());
					customerAddress.setCustomer(customer);
					customerAddress.setStatus(1);
					customerAddress.setMessage(msg);
					return customerAddress;
					
				case SHIPPING_ADDRESS:
					billingAddressList = DAORegistry.getUserAddressDAO().getUserAddressInfo(customerId,AddressType.BILLING_ADDRESS);
					UserAddress shippAddress = null;
					 if(action.equals("Save")){
						 if(shippingAddressCount<5) {
							shippAddress = new UserAddress();
							shippAddress.setStatus(Status.ACTIVE);
							shippAddress.setCustomerId(Integer.valueOf(customerIdStr));
							shippAddress.setFirstName(firstName);
							shippAddress.setLastName(lastName);
							shippAddress.setAddressLine1(addressLine1);
							shippAddress.setAddressLine2(addressLine2);
							shippAddress.setAddressType(AddressType.SHIPPING_ADDRESS);
							shippAddress.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
							shippAddress.setCity(city);
							shippAddress.setPhone1(phone1);
							shippAddress.setPhone2(phone2);
							shippAddress.setPhone3(phone3);
							shippAddress.setEmail(email != null && !email.isEmpty()?email:null);
							shippAddress.setZipCode(postalCode);
							shippAddress.setCountry(countryObj);
							shippAddress.setState(stateObj);
							shippAddress.setLastUpdated(new Date());
							DAORegistry.getUserAddressDAO().saveOrUpdate(shippAddress);
							userAddressList.add(shippAddress);
							
							
							if(null !=userAddressList && userAddressList.size() >2){
								Collections.sort(userAddressList,CityUtil.sortShippingAddressByUpdatedTime);
							}
							customer.setShippingAddress(userAddressList);
							customer.setBillingAddress(billingAddressList);
							
							customerAddress.setRevisedAddressType(addressType);
							customerAddress.setRevisedId(shippAddress.getId());
							customerAddress.setCustomer(customer);
							customerAddress.setStatus(1);
							customerAddress.setMessage("Shipping Address successfully added!");
							TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Shipping Address successfully added!");
							return customerAddress;
						 } else {
							 error.setDescription("You have added maximum number of shipping addresses. Please edit or delete any one of the existings address");
							 customerAddress.setError(error);
							 customerAddress.setStatus(0);
							 TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"You have added maximum number of shipping addresses. Please edit or delete any one of the existings address");
							 return customerAddress;
						 }
					}else if(action.equals("Update")){
						
						if(null == userAddress){
							error.setDescription("Shipping Address Id not Recognized");
							customerAddress.setError(error);
							customerAddress.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Shipping Address Id not Recognized");
							return customerAddress;
						}
						
						for (UserAddress shipAddress : userAddressList) {
							
							if(shipAddress.getId().equals(userAddress.getId()) || 
									shipAddress.getId()== userAddress.getId() ){
								shipAddress.setFirstName(firstName);
								shipAddress.setLastName(lastName);
								shipAddress.setAddressLine1(addressLine1);
								shipAddress.setAddressLine2(addressLine2);
								shipAddress.setCity(city);
								shipAddress.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
								shipAddress.setPhone1(phone1);
								shipAddress.setPhone2(phone2);
								shipAddress.setPhone3(phone3);
								shipAddress.setEmail(email != null && !email.isEmpty()?email:null);
								shipAddress.setZipCode(postalCode);
								shipAddress.setCountry(countryObj);
								shipAddress.setState(stateObj);
								shipAddress.setLastUpdated(new Date());
								DAORegistry.getUserAddressDAO().saveOrUpdate(shipAddress);
								customerAddress.setRevisedAddressType(addressType);
								customerAddress.setRevisedId(shipAddress.getId());
								break;
							}
						}
						if(null !=userAddressList && userAddressList.size() >2){
							Collections.sort(userAddressList,CityUtil.sortShippingAddressByUpdatedTime);
						}
						customer.setShippingAddress(userAddressList);
						customer.setBillingAddress(billingAddressList);
						customerAddress.setCustomer(customer);
						customerAddress.setStatus(1);
						customerAddress.setMessage("Shipping Address successfully updated!");
						TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Shipping Address successfully updated!");
						return customerAddress;
					}
					break;
	
				default:
					break;
			}
			customerAddress.setStatus(0);
			customerAddress.setMessage("Please send valid parameters");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Success");
			return customerAddress;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Adding customer Address.");
			customerAddress.setError(error);
			customerAddress.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.ADDCUSTOMERADDRESS,"Error occured while Adding customer Address");
			return customerAddress;
		}
	}
	
	@RequestMapping(value="/RemoveCustomerAddress",method=RequestMethod.POST)
	public @ResponsePayload CustomerAddress removeCustomerAddress(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerAddress customerAddress =new CustomerAddress();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"You are not authorized");
				return customerAddress;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerAddress.setError(error);
						customerAddress.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"You are not authorized");
						return customerAddress;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"You are not authorized");
					return customerAddress;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"You are not authorized");
				return customerAddress;
			} 
			String productTypeStr = request.getParameter("productType");
			String customerIdStr = request.getParameter("customerId");
			String platForm = request.getParameter("platForm");
			String addressIdStr = request.getParameter("addressId");
			String addressTypeStr = request.getParameter("addressType");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Product Type is mandatory");
				return customerAddress;
			}
			
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Please send valid product type");
					return customerAddress;
				}
			}
			
			
					
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Customer Id is mandatory");
				return customerAddress;
			}
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);//DAORegistry.getCustomerDAO().get(customerId);
				if(null == customer){
					error.setDescription("Customer is not recognized");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Customer is not recognized");
					return customerAddress;
				}
			}catch(Exception e){
				error.setDescription("Customer is not recognized");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Customer is not recognized");
				return customerAddress;
			}
			
			if(TextUtil.isEmptyOrNull(addressTypeStr)){
				error.setDescription("Address Type is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Address Type is mandatory");
				return customerAddress;
			}
			
			
			AddressType addressType = null;
			try{
				addressType = AddressType.valueOf(addressTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid Address type");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Please send valid Address type");
				return customerAddress;
			}
			
			

			if(TextUtil.isEmptyOrNull(addressIdStr)){
				error.setDescription("Address Id is mandatory.");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Address Id is mandatory");
				return customerAddress;
			}
			
			Integer addressId=null;					
			UserAddress userAddress = null;
			try{
				addressId = Integer.parseInt(addressIdStr.trim());
				userAddress = DAORegistry.getUserAddressDAO().getUserAddressByIdandAddressType(customerId,addressId, addressType);
				if(null == userAddress){
					error.setDescription("User Address is not valid");
					customerAddress.setError(error);
					customerAddress.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"User Address is not valid");
					return customerAddress;
				}
			}catch(Exception e){
				error.setDescription("User Address Id not Recognized");
				customerAddress.setError(error);
				customerAddress.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"User Address Id not Recognized");
				return customerAddress;
			}
			userAddress.setStatus(Status.DELETED);
			DAORegistry.getUserAddressDAO().saveOrUpdate(userAddress);
			customerAddress.setStatus(1);
			customerAddress.setMessage("Shipping Address successfully removed!");
			return customerAddress;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while removing customer address.");
			customerAddress.setError(error);
			customerAddress.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.REMOVECUSTOMERADDRESS,"Error occured while removing customer address");
			return customerAddress;
		}
	}
	
	@RequestMapping(value = "/GetReffererCode",method=RequestMethod.POST)
	public @ResponsePayload ReffererCodeResponse getReffererCode(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ReffererCodeResponse reffererCodeResponse =new ReffererCodeResponse();
		Error error = new Error();
		try { 
			
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						reffererCodeResponse.setError(error);
						reffererCodeResponse.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREFFERERCODE,"You are not authorized");
						return reffererCodeResponse;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					reffererCodeResponse.setError(error);
					reffererCodeResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREFFERERCODE,"You are not authorized");
					return reffererCodeResponse;
				}
			}else{
				error.setDescription("You are not authorized.");
				reffererCodeResponse.setError(error);
				reffererCodeResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREFFERERCODE,"You are not authorized");
				return reffererCodeResponse;
			}

			String productTypeStr = request.getParameter("productType");			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					reffererCodeResponse.setError(error);
					reffererCodeResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREFFERERCODE,"Please send valid product type");
					return reffererCodeResponse;
				}
			}
			
			String platFormStr = request.getParameter("platForm");
			if(TextUtil.isEmptyOrNull(platFormStr)){
				reffererCodeResponse.setStatus(0);
				return reffererCodeResponse;
			}
			ApplicationPlatForm platForm = null;
			if(!TextUtil.isEmptyOrNull(platFormStr)){
				try{
					platForm = ApplicationPlatForm.valueOf(platFormStr);
				}catch(Exception e){
					reffererCodeResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETFANTASYEVENTTEAMS,"Please send valid platform");
					return reffererCodeResponse;
				}
			}
			
			String customerId = request.getParameter("customerId");
			String fontSizeStr = TicketUtil.getFontSize(request, platForm);
			String learnMoreText = "<center><font "+fontSizeStr+" color=#ffffff >Win 4 FREE tickets to see HAMILTON in any city!<br/><br/>" +
					"For every friend you refer who spends $250 or more on any event, we reward you with a chance for 4 FREE tickets to see HAMILTON in any city!<br/><br/>" +
					"Register Today and refer your friends to Reward The Fan.<br/></font></center>";
			reffererCodeResponse.setLearnMoreText(learnMoreText);
			
			if(TextUtil.isEmptyOrNull(customerId)){
				String text = "<center><font "+fontSizeStr+" color=#f06812 >Registered users get a <b>unique referral code</b>" +
				"<br>" +
				"to refer others and have a chance to enter the Hamilton Lottery</font></center>";
				reffererCodeResponse.setText(text);
				reffererCodeResponse.setText1("<center><font "+fontSizeStr+" color=#000000 >Start referring by clicking share button below</font></center>");
				reffererCodeResponse.setStatus(1);
				return reffererCodeResponse;
			}
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerId)); //DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
			
			if(customer == null){
				/*String text = "<center><font "+fontSizeStr+" color=#FFFFFF >Send your friends a discount code" +
				"<br>" +
				"AND" +
				"<br>" +		
				"Earn reward points on their first purchase</font></center>";*/
				String text = "<center><font "+fontSizeStr+" color=#f06812 >Registered users get a <b>unique referral code</b>" +
				"<br>" +
				"to refer others and have a chance to enter the Hamilton Lottery</font></center>";
				reffererCodeResponse.setText(text);
				reffererCodeResponse.setText1("<center><font "+fontSizeStr+" color=#000000 >Start referring by clicking share button below</font></center>");
				reffererCodeResponse.setStatus(1);
				return reffererCodeResponse;
			}
			
			reffererCodeResponse.setCustomerId(customer.getId());
			reffererCodeResponse.setStatus(1);
			reffererCodeResponse.setReferrerCode(customer.getReferrerCode());
			
			/* Share Link Validation for Share Icon - Added By Ulaganathan*/
			if(null != customer){
				reffererCodeResponse.setEligibleToShareRefCode(customer.getEligibleToShareRefCode());
				reffererCodeResponse.setShareErrorMessage(TextUtil.getCustomerShareLinkValidationMessage(fontSizeStr));
			}
			/* Share Link Validation for Share Icon - Added By Ulaganathan*/
			
			/*String text = "<center><font "+fontSizeStr+" color=#FFFFFF >Send your friends a discount code" +
			"<br>" +
			"AND" +
			"<br>" +		
			"Earn reward points on their first purchase</font></center>";*/
			String text = "<center><font "+fontSizeStr+" color=#f06812 >Registered users get a <b>unique referral code</b>" +
			"<br>" +
			"to refer others and have a chance to enter the Hamilton Lottery</font></center>";
			reffererCodeResponse.setText(text);
			reffererCodeResponse.setText1("<center><font "+fontSizeStr+" color=#000000 >Start referring by clicking share button below</font></center>");
			if(platForm.equals(ApplicationPlatForm.DESKTOP_SITE)){
				TextUtil.computeDesktopMessages(reffererCodeResponse, customer.getReferrerCode(),"HM");
			}else{
				TextUtil.computeMobileAppMessages(reffererCodeResponse, customer.getReferrerCode(), platForm,"HM");
			}

		}catch (Exception e) {
			error.setDescription(e.getMessage());
			reffererCodeResponse.setError(error);
			reffererCodeResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETREFFERERCODE,"Exception Occurred");
			return reffererCodeResponse;
		}
		return reffererCodeResponse;
	} 
	
	@RequestMapping(value = "/UpdateCustomerPhoneNumber", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails updateCustomerPhone(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"You are not authorized");
				return customerDetails;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String productTypeStr = request.getParameter("productType");
			//String phoneNumber = request.getParameter("phoneNumber");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			//String userId = request.getParameter("userId");
			
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Customer Id is mandatory");
				return customerDetails;
			}
			
			/*if(TextUtil.isEmptyOrNull(phoneNumber)){
				error.setDescription("Please enter your Phone Number");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Please enter your Phone Number");
				return customerDetails;
			}*/
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId =  Integer.parseInt(customerIdStr.trim());
				customer = DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
				if(null == customer){
					error.setDescription("Customer is not recognized");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Customer is not recognized");
					return customerDetails;
				}
			}catch(Exception e){
				error.setDescription("Customer is not recognized");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Customer is not recognized");
				return customerDetails;
			}
			
			/*if(TextUtil.isEmptyOrNull(userId)){
				 
			}else {
				if(customer.getUserId() != null && !customer.getUserId().equalsIgnoreCase(userId)) {
					QuizCustomerReferralTracking tracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByReferralCode(customer.getUserId());
					if(tracking != null) {
						error.setDescription("User Id can't be editable. someone used it as referal code.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"User Id can't be editable. someone used it as referal code.");
						return customerDetails;
					}
					error.setDescription("User Id cannot be updated to preserve your reward dollars. Please contact customer support.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,
							"User Id cannot be updated to preserve your reward dollars. Please contact customer support.");
					return customerDetails;
				}
			}*/
			
			/*userId = userId.trim();
			if(userId.length()< 5 || userId.length() > 12) {
				error.setDescription("Please select user id within 5 to 12 letters or numbers.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Please select user id within 5 to 12 letters or numbers.");
				return customerDetails;
			}
			if(!userId.matches("[a-zA-Z0-9]*")) {
				error.setDescription("Please select user id with letters and/or numbers only.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Please select user id with letters and/or numbers only.");
				return customerDetails;
			} 
			
			Customer customerObj = DAORegistry.getCustomerDAO().getCustomerByUserIdExceptThisCustomer(userId,customerId);
			RtfPromotionalEarnLifeOffers rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
			if(customerObj != null || rtfReferral != null) {
				error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Whoops! This userId is already signed up. Please choose a different UserId.");
				return customerDetails;
			}
			customer.setUserId(userId);
			*/
			
			/*if(customer.getPhone() == null || !customer.getPhone().equals(phoneNumber)) {
				customer.setIsOtpVerified(Boolean.FALSE);
			}
			customer.setPhone(phoneNumber);*/
			
			/* Customer Can Update Their First Name, Last Name & User Name - Added By Ulaganathan on 5/15/2018 11:20AM*/
			if(!TextUtil.isEmptyOrNull(firstName)){
				customer.setCustomerName(firstName);
			}

			if(!TextUtil.isEmptyOrNull(lastName)){
				customer.setLastName(lastName);		
			}
			
			/*End - Added By Ulaganathan on 5/15/2018 11:20AM*/
			
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			try{
				CustomerUtil.updatedCustomerUtil(customer);
			}catch(Exception e){
				
			}
			
			CustomerLoyalty customerLoyalty = null;
			List<UserAddress> billingAddress = new ArrayList<UserAddress>();
			List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
			
			if(customer != null){
				customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
				
				//Get CustomerAddress by customer id and add it to the response
				//Start			
				List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
							
				if(userAddress != null){
					for(UserAddress address : userAddress){
						if(address.getAddressType() == AddressType.BILLING_ADDRESS)
							billingAddress.add(address);
						else
							shippingAddress.add(address);					
					}
				}						
				//End
			}
			
			customerDetails.setMessage("Personal informations successfully updated!");
			customerDetails.setStatus(1);
			customerDetails.setCustomer(customer);
			customer.setBillingAddress(billingAddress);
			if(null !=shippingAddress && shippingAddress.size() >2){
				Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			}
			customer.setShippingAddress(shippingAddress);
			customerDetails.setCustomerLoyalty(customerLoyalty);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while updating customer phone number.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERPHONENUMBER,"Error occured while updating customer phone number");
			return customerDetails;
		}
		return customerDetails;
	}
	
	@RequestMapping(value="/GetCustomerRewardsBreakup",method=RequestMethod.POST)
	public @ResponsePayload CustomerRewards getCustomerRewardsBreakup(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerRewards customerLoyaltyDetails =new CustomerRewards();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerLoyaltyDetails.setError(error);
						customerLoyaltyDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"You are not authorized");
						return customerLoyaltyDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"You are not authorized");
					return customerLoyaltyDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String breakUpType = request.getParameter("rewardBreakUpType");
			String customerIdStr= request.getParameter("customerId");
						
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Product Type is mandatory");
				return customerLoyaltyDetails;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Please send valid product type");
					return customerLoyaltyDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(breakUpType)){
				error.setDescription("Reward BreakUp Type is mandatory.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Reward BreakUp Type is mandatory");
				return customerLoyaltyDetails;
			}
			RewardBreakUpType rewardBreakUpType = null;
			if(!TextUtil.isEmptyOrNull(breakUpType)){
				try{
					rewardBreakUpType = RewardBreakUpType.valueOf(breakUpType);
				}catch(Exception e){
					error.setDescription("Please send valid reward break up type");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Please send valid reward break up type");
					return customerLoyaltyDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customet ID  is mandatory");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Customet ID is mandatory");
				return customerLoyaltyDetails;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Please send valid Customer Id");
					return customerLoyaltyDetails;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Please send valid Customer Id");
				return customerLoyaltyDetails;
			}
			
			List<RewardDetails> rewardList = new ArrayList<RewardDetails>();
			
			switch (rewardBreakUpType) {
			
				case PENDING_POINTS:
					rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.PENDING, customerId,rewardBreakUpType);
					
					break;
				case EARNED_POINTS:
					rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.ACTIVE, customerId,rewardBreakUpType);
					break;
					
				case REDEEMED_POINTS:
					rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRedeemedRewardsByCustomerId(customerId);
					break;
					
				case VOIDED_POINTS:
					rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.PENDING, customerId,rewardBreakUpType);
					break;
					
				case REVERTED_POINTS:
					rewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerRewardsByRewardStatus(RewardStatus.PENDING, customerId,rewardBreakUpType);
					break;
	
				default:
					break;
			}
			
			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			Calendar calendar = new GregorianCalendar();
			String activeDateStr = null;
			for (RewardDetails rewardDetails : rewardList) {
				
				if(null != rewardDetails.getEventDate() ){
					activeDateStr =  rewardDetails.getEventDate();
					if( rewardDetails.getEventTime().equals("TBD")){
						activeDateStr = activeDateStr+" 12:00 AM";
					}else{
						activeDateStr = activeDateStr+" "+ rewardDetails.getEventTime();
					}
				}
				
				Date activateDate = null;
				try{
					activateDate = dateTimeFormat.parse(activeDateStr);
				}catch (Exception e) {
					// TODO: handle exception
				}
				if(null != activateDate){
					calendar.setTime(activateDate);
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					rewardDetails.setActiveDate(dateTimeFormat.format(calendar.getTime()));
				}else{
					rewardDetails.setActiveDate(activeDateStr);
						
				}
			}
			
			customerLoyaltyDetails.setRewardList(rewardList);
			customerLoyaltyDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Success");
			return customerLoyaltyDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting GETREWARDSBREAKUP.");
			customerLoyaltyDetails.setError(error);
			customerLoyaltyDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"Error occured while gettting GETREWARDSBREAKUP");
			return customerLoyaltyDetails;
		}
	}
	
	@RequestMapping(value="/Customer/GetMyReferrals",method=RequestMethod.POST)
	public @ResponsePayload CustomerRewards getCustomerReferrals(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerRewards customerLoyaltyDetails =new CustomerRewards();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerLoyaltyDetails.setError(error);
						customerLoyaltyDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"You are not authorized");
						return customerLoyaltyDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"You are not authorized");
					return customerLoyaltyDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String customerIdStr= request.getParameter("customerId");
						
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"Product Type is mandatory");
				return customerLoyaltyDetails;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"Please send valid product type");
					return customerLoyaltyDetails;
				}
			}
			
			 
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customet ID  is mandatory");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"Customet ID is mandatory");
				return customerLoyaltyDetails;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"Please send valid Customer Id");
					return customerLoyaltyDetails;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"Please send valid Customer Id");
				return customerLoyaltyDetails;
			}
			List<ReferralDTO> referrals = SQLDaoUtil.getAllCustomerReferrals(customerId);
			if(null == referrals || referrals.isEmpty() || referrals.size() <= 0) {
				customerLoyaltyDetails.setMessage("No referrals found.");
			}
			customerLoyaltyDetails.setReferrals(referrals);
			customerLoyaltyDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"Success");
			return customerLoyaltyDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Server Error. Error Occured While Fetching Contest Referral Rewards");
			customerLoyaltyDetails.setError(error);
			customerLoyaltyDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYREFERRALS,"Server Error. Error Occured While Fetching Contest Referral Rewards");
			return customerLoyaltyDetails;
		}
	}
	
	
	@RequestMapping(value="/Customer/ReferralReward",method=RequestMethod.POST)
	public @ResponsePayload CustomerRewards getCustomerReferralRewards(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerRewards customerLoyaltyDetails =new CustomerRewards();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerLoyaltyDetails.setError(error);
						customerLoyaltyDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"You are not authorized");
						return customerLoyaltyDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETREWARDSBREAKUP,"You are not authorized");
					return customerLoyaltyDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String customerIdStr= request.getParameter("customerId");
						
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Product Type is mandatory");
				return customerLoyaltyDetails;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Please send valid product type");
					return customerLoyaltyDetails;
				}
			}
			
			 
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customet ID  is mandatory");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Customet ID is mandatory");
				return customerLoyaltyDetails;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Please send valid Customer Id");
					return customerLoyaltyDetails;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Please send valid Customer Id");
				return customerLoyaltyDetails;
			}
			
			List<CustomerLoyaltyHistory> referralRewards = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerContestReferralRewards(customerId);
			
			Map<Integer, ReferralRewardDetails> referralRewardMap = new HashMap<Integer, ReferralRewardDetails>();
			String lastName =null,custFullName=null;
			for (CustomerLoyaltyHistory obj : referralRewards) {
				
				ReferralRewardDetails details = referralRewardMap.get(obj.getTireOneCustId());
				
				if(null != details) {
					details.setRewardsAsDouble(details.getRewardsAsDouble()+obj.getTireOneCustEarnPoints());
				}else {
					
					Customer tireOneCustomer = CustomerUtil.getCustomerById(obj.getTireOneCustId());
					
					details = new ReferralRewardDetails();
					details.setPrimaryCustId(customerId);
					details.setTireOneCustId(obj.getTireOneCustId());
					lastName = null !=  tireOneCustomer.getLastName() &&  !tireOneCustomer.getLastName().isEmpty()?" "+ tireOneCustomer.getLastName():"";
					custFullName = tireOneCustomer.getCustomerName()+lastName;
					details.setCustomerName(custFullName);
					details.setPhoneNo(tireOneCustomer.getPhone());
					details.setRewardsAsDouble(obj.getTireOneCustEarnPoints());
					details.setUserName(tireOneCustomer.getUserId());
					details.setEmailId(tireOneCustomer.getEmail());
					tireOneCustomer.getUserName();
				}
				referralRewardMap.put(obj.getTireOneCustId(), details);
			}
			
			if(null != referralRewardMap && !referralRewardMap.isEmpty()) {
				List<ReferralRewardDetails> list = new ArrayList<>(referralRewardMap.values());
				try {
					//sort by age
					Collections.sort(list, new Comparator<ReferralRewardDetails>() {
						@Override
						public int compare(ReferralRewardDetails o1, ReferralRewardDetails o2) {
							return o2.getRewardsAsDouble().compareTo(o1.getRewardsAsDouble());
						}
					});
				}catch(Exception e) {
					e.printStackTrace();
				}
				customerLoyaltyDetails.setReferralRewardsList(list);
				customerLoyaltyDetails.setStatus(1);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Success");
				return customerLoyaltyDetails;
			}
			
			customerLoyaltyDetails.setReferralRewardsList(referralRewardMap.values());
			customerLoyaltyDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Success");
			return customerLoyaltyDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Server Error. Error Occured While Fetching Contest Referral Rewards");
			customerLoyaltyDetails.setError(error);
			customerLoyaltyDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDBREAK,"Server Error. Error Occured While Fetching Contest Referral Rewards");
			return customerLoyaltyDetails;
		}
	}
	
	
	@RequestMapping(value="/Customer/ReferralReward/ContestLevel",method=RequestMethod.POST)
	public @ResponsePayload CustomerRewards getCustomerReferralRewardsByContestLevel(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerRewards customerLoyaltyDetails =new CustomerRewards();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerLoyaltyDetails.setError(error);
						customerLoyaltyDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"You are not authorized");
						return customerLoyaltyDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"You are not authorized");
					return customerLoyaltyDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String customerIdStr = request.getParameter("tireOneCustomerId");
			String primaryCustIdStr = request.getParameter("customerId");
			String platFormStr = request.getParameter("platForm");
						
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"Product Type is mandatory");
				return customerLoyaltyDetails;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"Please send valid product type");
					return customerLoyaltyDetails;
				}
			}
			
			 
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customet ID  is mandatory");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"Customet ID is mandatory");
				return customerLoyaltyDetails;
			}
			
			Integer tireOneCustId=null;
			Integer primaryCustId=null;
			Customer tireOneCustomer = null;
			try{
				tireOneCustId = Integer.parseInt(customerIdStr.trim());
				tireOneCustomer =  CustomerUtil.getCustomerById(tireOneCustId);
				primaryCustId = Integer.parseInt(primaryCustIdStr.trim());
				if(null == tireOneCustomer){
					error.setDescription("Please send valid Customer Id");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"Please send valid Customer Id");
					return customerLoyaltyDetails;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"Please send valid Customer Id");
				return customerLoyaltyDetails;
			}
			
			List<CustomerLoyaltyHistory> referralRewards = DAORegistry.getCustomerLoyaltyHistoryDAO().getReferralRewardsByPRimaryAndTireOneCustomerIds(primaryCustId,tireOneCustId);
			
			
			List<ReferralRewardDetails> referralRewardDetailList = new ArrayList<ReferralRewardDetails>();
			String lastName =null,custFullName=null;
			
			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			for (CustomerLoyaltyHistory obj : referralRewards) {
				
				QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(obj.getContestId());
				if(contest == null) {
					continue;
				}
				ReferralRewardDetails details = new ReferralRewardDetails();
				details.setPrimaryCustId(primaryCustId);
				details.setTireOneCustId(primaryCustId);
				lastName = null !=  tireOneCustomer.getLastName() &&  !tireOneCustomer.getLastName().isEmpty()?" "+ tireOneCustomer.getLastName():"";
				custFullName = tireOneCustomer.getCustomerName()+lastName;
				details.setCustomerName(custFullName);
				details.setPhoneNo(tireOneCustomer.getPhone());
				details.setRewardsAsDouble(obj.getTireOneCustEarnPoints());
				details.setUserName(tireOneCustomer.getUserId());
				details.setEmailId(tireOneCustomer.getEmail());
				details.setNoOfAnsweredQuestion(obj.getAnsQuestion());
				details.setContestDateTime(contest.getContestStartDateTime());
				details.setContestDate(dateTimeFormat.format(contest.getContestStartDateTime()));
				details.setContestName(contest.getContestName());
				referralRewardDetailList.add(details);
			}
			
			if(null != referralRewardDetailList && !referralRewardDetailList.isEmpty()) {
				try {
					//sort by age
					Collections.sort(referralRewardDetailList, new Comparator<ReferralRewardDetails>() {
						@Override
						public int compare(ReferralRewardDetails o1, ReferralRewardDetails o2) {
							return o2.getContestDateTime().compareTo(o1.getContestDateTime());
						}
					});
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			 
			customerLoyaltyDetails.setReferralRewardsList(referralRewardDetailList);
			customerLoyaltyDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"Success");
			return customerLoyaltyDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Server Error. Error Occured While Fetching Contest Referral Rewards");
			customerLoyaltyDetails.setError(error);
			customerLoyaltyDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.REFERRALREWARDCONTESTBREAK,"Server Error. Error Occured While Fetching Contest Referral Rewards");
			return customerLoyaltyDetails;
		}
	}
	
	@RequestMapping(value="/Customer/AffiliateReward",method=RequestMethod.POST)
	public @ResponsePayload CustomerRewards getCustomerAffiliateRewards(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerRewards customerLoyaltyDetails =new CustomerRewards();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerLoyaltyDetails.setError(error);
						customerLoyaltyDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"You are not authorized");
						return customerLoyaltyDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"You are not authorized");
					return customerLoyaltyDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"You are not authorized");
				return customerLoyaltyDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String customerIdStr= request.getParameter("customerId");
						
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"Product Type is mandatory");
				return customerLoyaltyDetails;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"Please send valid product type");
					return customerLoyaltyDetails;
				}
			}
			
			 
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customet ID  is mandatory");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"Customet ID is mandatory");
				return customerLoyaltyDetails;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					customerLoyaltyDetails.setError(error);
					customerLoyaltyDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"Please send valid Customer Id");
					return customerLoyaltyDetails;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerLoyaltyDetails.setError(error);
				customerLoyaltyDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"Please send valid Customer Id");
				return customerLoyaltyDetails;
			}
			
			List<QuizAffiliateRewardHistory> referralRewards = DAORegistry.getQuizAffiliateRewardHistoryDAO().getAllRewardsBreak(customerId);
			
			List<ReferralRewardDetails> list = new ArrayList<ReferralRewardDetails>();
			ReferralRewardDetails details = null;
			String lastName =null,custFullName=null;
			for (QuizAffiliateRewardHistory obj : referralRewards) {
					
					Customer tireOneCustomer = CustomerUtil.getCustomerById(obj.getTireoneCustomerId());
					
					details = new ReferralRewardDetails();
					details.setPrimaryCustId(customerId);
					details.setTireOneCustId(obj.getTireoneCustomerId());
					lastName = null !=  tireOneCustomer.getLastName() &&  !tireOneCustomer.getLastName().isEmpty()?" "+ tireOneCustomer.getLastName():"";
					custFullName = tireOneCustomer.getCustomerName()+lastName;
					details.setCustomerName(custFullName);
					details.setPhoneNo(tireOneCustomer.getPhone());
					if(obj.getRewardType().equals("CASH_REWARD")) {
						details.setRewardType("CASH REWARD");
						details.setRewardsAsDouble(obj.getCashReward());
					}else {
						details.setRewardType("REWARD DOLLAR");
						details.setRewardsAsDouble(obj.getRewardDollar());
					}
					details.setUserName(tireOneCustomer.getUserId());
					details.setEmailId(tireOneCustomer.getEmail());
					list.add(details);
			}
			customerLoyaltyDetails.setReferralRewardsList(list); 
			customerLoyaltyDetails.setStatus(1);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"Success");
			return customerLoyaltyDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Server Error. Error Occured While Fetching Contest Affiliate Referral Rewards");
			customerLoyaltyDetails.setError(error);
			customerLoyaltyDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.AFFILIATEREWARDBREAK,"Server Error. Error Occured While Fetching Contest Affiliate Referral Rewards");
			return customerLoyaltyDetails;
		}
	}
	
	
	@RequestMapping(value="/CustomerLogout",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerLogout(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"You are not authorized");
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String customerIdStr = request.getParameter("customerId");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			//String notificationRegId=request.getParameter("notificationRegId");
			String loginIp = request.getParameter("loginIp");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Notification RegistrationID is mandatory");
				return customerDetails;
			}	*/
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is Madantory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Customer Id is Madantory");
				return customerDetails;
			}
			
			
			try{
				//DAORegistry.getCustomerDeviceDetailsDAO().updateCustomerDeviceDetails(Integer.parseInt(customerIdStr.trim()), deviceId);
			}catch(Exception e){
				e.printStackTrace();
			}
			customerDetails.setMessage("Logged Out Successfully");
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while CUSTOMERLOGOUT.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGOUT,"Error occured while CUSTOMERLOGOUT");
			return customerDetails;
		}
	}
	
	
	@RequestMapping(value="/ResetPasswordRequest",method=RequestMethod.POST)
	public @ResponsePayload ResetPasswordRequest resetPasswordRequest(HttpServletRequest request, Model model,HttpServletResponse response){
		ResetPasswordRequest customerDetails =new ResetPasswordRequest();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTATECOUNTRY,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"You are not authorized");
				return customerDetails;
			}
			String email = request.getParameter("email");
			String productTypeStr = request.getParameter("productType");
			
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"Product Type is mandatory");
				return customerDetails;
			}
			
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please choose valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"Please choose valid product type");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(email)){
				error.setDescription("Please enter your Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"Please enter your Email");
				return customerDetails;
			}
			
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmailByProductType(email, productType);
			
			if(null == customer){
				error.setDescription("We do not have an account for the Email you entered. Please enter a valid Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"We do not have an account for the Email you entered. Please enter a valid Email");
				return customerDetails;
			}
			
			ResetPasswordToken passwordToken = new ResetPasswordToken();
			passwordToken.setCustomerId(customer.getId());
			passwordToken.setEmail(customer.getEmail());
			
			Property property = DAORegistry.getPropertyDAO().get("customer.resetpassword.auto.number");
			Long autoNumber =  Long.valueOf(property.getValue());
			autoNumber++;
			
			//Generating encrypted reset token
			String resetToken = PasswordUtil.generateEncrptedPassword(customer.getId()+""+autoNumber+""+customer.getPassword());
			passwordToken.setResetToken(resetToken);
			passwordToken.setStatus("ACTIVE");
			passwordToken.setCreateDate(new Date());
			passwordToken.setLastUpdated(new Date());
			DAORegistry.getResetPasswordTokenDAO().saveOrUpdate(passwordToken);
			
			property.setValue(String.valueOf(autoNumber));
			DAORegistry.getPropertyDAO().saveOrUpdate(property);
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("customer", customer);
			mailMap.put("resetToken", resetToken);
			mailMap.put("resetPasswordLink", URLUtil.getResetPasswordLink()+"?resetToken="+resetToken);
			mailMap.put("rewardPointsLink", URLUtil.getRewardPoints());
			
			
			
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			try{
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
						null, URLUtil.bccEmails, "Reward The Fan : Reset Password Instruction.",
			    		"mail-rewardfan-reset-password.html", mailMap, "text/html",null,mailAttachment,null);
			}catch(Exception e){
				error.setDescription("We do not have an account for the Email you entered. Please enter a valid Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"We do not have an account for the Email you entered. Please enter a valid Email");
				return customerDetails;
			}
			
			customerDetails.setEmail(email);
			customerDetails.setMessage("RewardTheFan Password Reset instructions were sent to your email. Please check your email");
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while reseting password Request.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORDREQUEST,"Error occured while reseting password Request");
			return customerDetails;
		}
	}
	
	
	@RequestMapping(value="/ResetPassword",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails resetPassword(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"You are not authorized");
				return customerDetails;
			}
			
			String resetToken = request.getParameter("resetToken");
			String newPassword = request.getParameter("newPassword");
			String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Product Type is Mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(resetToken)){
				error.setDescription("Please enter your Temporary Password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Please enter your Temporary Password");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(newPassword)){
				error.setDescription("Please enter a New Password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Please enter a New Password");
				return customerDetails;
			}
			
			//Commented By Ulaga
			ResetPasswordToken passwordToken = DAORegistry.getResetPasswordTokenDAO().getResetPasswordTokenByToken(resetToken);
			
			if(null == passwordToken){
				error.setDescription("Temporary Password is invalid. Please enter a valid Temporary Password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Temporary Password is invalid. Please enter a valid Temporary Password");
				return customerDetails;
			}
			
			if(passwordToken.getStatus().equals("EXPIRED")){
				error.setDescription("Temporary Password has expired. Please request a new reset password link");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Temporary Password has expired. Please request a new reset password link");
				return customerDetails;
			}
			
			if(newPassword.trim().length() < 8 || newPassword.trim().length() > 20){//tempPhone.length() < 10
				error.setDescription("Password Length must be in between 8-20.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Password Length must be in between 8-20.");
				return customerDetails;
			}
			
			
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByIdByProduct(passwordToken.getCustomerId(), productType);
			if(null == customer){
				error.setDescription("We did not find an existing ID and/or password. Please create a new account");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"We did not find an existing ID and/or password. Please create a new account");
				return customerDetails;
			}
			passwordToken.setLastUpdated(new Date());
			passwordToken.setStatus("EXPIRED");
			customer.setEncryptPassword(newPassword);
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			DAORegistry.getResetPasswordTokenDAO().saveOrUpdate(passwordToken);
			
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("customer", customer);
			mailMap.put("newPassword", newPassword);
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			
			try{
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
						null,URLUtil.bccEmails, "Your Password was successfully reset",
			    		"mail-zonetickets-reset-password-success.html", mailMap, "text/html", null,mailAttachment,null);
			
			}catch(Exception e){
				e.printStackTrace();
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			if(null != customerLoyalty){
				customerDetails.setCustomerLoyalty(customerLoyalty);
			}
			
			ResetPassword resetPasswrod =new ResetPassword(); 
			resetPasswrod.setMessage("Your Password was successfully reset!");
			resetPasswrod.setEmail(customer.getEmail());
			customerDetails.setResetPassword(resetPasswrod);
			customerDetails.setCustomer(customer);
			
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while reseting password.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.RESETPASSWORD,"Error occured while reseting password");
			return customerDetails;
		}
	}
	
	@RequestMapping(value="/ChangePassword",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails changePassword(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"You are not authorized");
				return customerDetails;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String productTypeStr = request.getParameter("productType");
			String newPassword = request.getParameter("newPassword");
			String oldPassword = request.getParameter("oldPassword");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"Product Type is Mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Seems that your session got expired.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"Seems that your session got expired.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(newPassword)){
				error.setDescription("Please enter a New Password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"Please enter a New Password");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(oldPassword)){
				error.setDescription("Please enter old password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"Please enter old password");
				return customerDetails;
			}
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer= CustomerUtil.getCustomerById(customerId);
				if(customer==null ){
					error.setDescription("Seems that your session got expired.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.CHANGEPASSWORD,"Seems that your session got expired.");
					return customerDetails;
				}
			}catch(Exception e){
				error.setDescription("Seems that your session got expired.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.CHANGEPASSWORD,"Seems that your session got expired.");
				return customerDetails;
			}
			
			String encryptedText = PasswordUtil.generateEncrptedPassword(oldPassword.trim());
			
			if(!encryptedText.equals(customer.getPassword().trim())){
				error.setDescription("Old password you have entered is wrong. Please enter valid old password.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.CHANGEPASSWORD,"Old password is wrong. Please enter valid old password.");
				return customerDetails;
			}
			customer.setEncryptPassword(newPassword);
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("customer", customer);
			mailMap.put("newPassword", newPassword);
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			try{
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
						null,URLUtil.bccEmails, "Your Password was successfully changed",
			    		"mail-zonetickets-reset-password-success.html", mailMap, "text/html", null,mailAttachment,null);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			if(null != customerLoyalty){
				customerDetails.setCustomerLoyalty(customerLoyalty);
			}
			
			ResetPassword resetPasswrod =new ResetPassword(); 
			resetPasswrod.setMessage("Your Password was successfully changed!");
			resetPasswrod.setEmail(customer.getEmail());
			customerDetails.setResetPassword(resetPasswrod);
			customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while changing password.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CHANGEPASSWORD,"Error occured while changing password");
			return customerDetails;
		}
	}
	
	
	@RequestMapping(value = "/CustomerLoginByPhoneNo", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerLoginByPhoneNo(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,authError.getDescription());
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			//String deviceId = request.getParameter("deviceId");
			//String notificationRegId=request.getParameter("notificationRegId");
			//String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");
			String ctyPhCode = request.getParameter("ctyPhCode");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid product type");
					return customerDetails;
				}
			}
			
			
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please enter a phone number");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter a phone number");
				return customerDetails;
			}
			phone = phone.trim();
			//String tempPhone = phone;
			//tempPhone =  QuizSettings.validatePhoneNo(tempPhone);
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			

			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				 ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			
			/*if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				showOtpPopup = true;
			}
			customerDetails.setShowOtpPopup(showOtpPopup);*/
			
			QuizOTPTracking otpTracking = generateOTPTracking(phone,ctyPhCode, error);
			if(otpTracking == null) {
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,error.getDescription());
				
				log.info("QUIZ LOGIN 00 : "+request.getParameter("customerId")+" : uName: "+request.getParameter("userName")+" : phone: "+request.getParameter("phone")+" : shotpPop: "+customerDetails.getShowOtpPopup()+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
				
				return customerDetails;
			}
			customerDetails.setLoginOTP(otpTracking.getResponseOtp());
			customerDetails.setOtpExpiryTimeInSeconds(QuizSettings.otpLiveSeconds);
			customerDetails.setOtpTrackingId(otpTracking.getId());
			
			customerDetails.setStatus(1);
			customerDetails.setMessage("Login OTP sent successfully.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Login OTP sent successfully");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Oops Somthing Went Wrong. Please Login Again!");
			return customerDetails;
		}
		log.info("QUIZ PHONE NOLOGIN : "+" : phone: "+request.getParameter("phone")+" : shotpPop: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return customerDetails;
	}
	
	
	
	
	@RequestMapping(value = "/CustomerLoginByEmailPass", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerLoginByEmailPass(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,authError.getDescription());
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			//String deviceId = request.getParameter("deviceId");
			//String notificationRegId=request.getParameter("notificationRegId");
			//String loginIp = request.getParameter("loginIp");
			String email = request.getParameter("email");
			String pass = request.getParameter("pass");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(email)){
				error.setDescription("Please enter a valid email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter a valid email");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(pass)){
				error.setDescription("Please enter a valid password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter a valid password");
				return customerDetails;
			}
			if(pass.trim().length() < 8 || pass.trim().length() > 20){//tempPhone.length() < 10
				error.setDescription("Password Length must be in between 8-20.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Password Length must be in between 8-20.");
				return customerDetails;
			}
			email = email.trim();
			//pass = pass.trim();
			//String tempPhone = phone;
			//tempPhone =  QuizSettings.validatePhoneNo(tempPhone);
			/*if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}*/
			
			/*if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				showOtpPopup = true;
			}
			customerDetails.setShowOtpPopup(showOtpPopup);*/
			
			/*QuizOTPTracking otpTracking = generateOTPTracking(phone, error);
			if(otpTracking == null) {
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,error.getDescription());
				
				log.info("QUIZ LOGIN 00 : "+request.getParameter("customerId")+" : uName: "+request.getParameter("userName")+" : phone: "+request.getParameter("phone")+" : shotpPop: "+customerDetails.getShowOtpPopup()+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
				
				return customerDetails;
			}*/
			
			pass = PasswordUtil.generateEncrptedPassword(pass);
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmailIdAndPassword(email, pass);
			if(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE)){
				if(customer == null){
					error.setDescription("Invalid credentials, Please enter valid email and password combination to login.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Invalid credential, please pass valid credential to login.");
					return customerDetails;
				}
			}else{
				List<CountryPhoneCode> codes =  DAORegistry.getQueryManagerDAO().getAllActivePhoneCodes();
				customerDetails.setPhoneCodes(codes);
			}
			if(customer == null){
				customer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
				if(customer != null){
					error.setDescription("Invalid credentials, Please enter valid email and password combination to login.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Invalid credential, please pass valid credential to login.");
					return customerDetails;
				}
				customer = new Customer();
				customer.setEmail(email);
				customer.setPassword(pass);
				customer.setProductType(productType);
				customer.setApplicationPlatForm(applicationPlatForm);
				customer.setSignupDate(new Date());
				customer.setBrokerId(1001);
				customer.setContestHighScore(0);
				customer.setCustomerType(CustomerType.PHONE_CUSTOMER);
				customer.setIsBlocked(false);
				customer.setIsClient(false);
				customer.setIsEmailed(false);
				customer.setIsOtpVerified(false);
				customer.setLoyaltyPoints(0);
				customer.setMagicWands(0);
				customer.setQuizCustomerLives(0);
				customer.setReferrerCode(email);
				customer.setUserName(email);
				customer.setRewardDollar(0.00);
				customer.setRtfPoints(0);
				customer.setSfQuesLevel(0);
				customer.setSfStars(0);
				customer.setSignupType(SignupType.REWARDTHEFAN);
				DAORegistry.getCustomerDAO().saveOrUpdate(customer);
				
				
				String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
				customer.setCustImagePath(fileName);
				try {
					DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
					CassandraDAORegistry.getCassCustomerDAO().updateProfilePicName(customer.getId(), fileName);
				}catch(Exception e) {
					e.printStackTrace();
				}
				CustomerUtil.updatedCustomerUtil(customer);
			}
			
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			//customerDetails.setMessage("Login OTP sent successfully.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Login OTP sent successfully");
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Oops Somthing Went Wrong. Please Login Again!");
			return customerDetails;
		}
		log.info("QUIZ PHONE NOLOGIN : "+" : phone: "+request.getParameter("phone")+" : shotpPop: "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return customerDetails;
	}
	
	
	
	
	@RequestMapping(value = "/GetCountryPhoneCodes", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails getCountryPhoneCodes(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,authError.getDescription());
				return customerDetails;
			}
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			List<CountryPhoneCode> codes =  DAORegistry.getQueryManagerDAO().getAllActivePhoneCodes();
			customerDetails.setPhoneCodes(codes);
			customerDetails.setStatus(1);
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Oops Somthing Went Wrong. Please Again!");
			return customerDetails;
		}
		return customerDetails;
	}
	
	
	
	
	
	@RequestMapping(value="/UpdateCustomerSignUpDetailsApp",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails updateCustomerSignUpDetailsApp(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,authError.getDescription());
				return customerDetails;
			}
			
			String countryCode = request.getParameter("countryCode");
			String phone = request.getParameter("phone");
			String ageGroup = request.getParameter("ageGroup");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm"); 
			String userId = request.getParameter("userId");
			String customerId = request.getParameter("customerId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid product type");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Mobile number is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Mobile number is mandatory");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(countryCode)){
				error.setDescription("Country phone code is mendatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Country phone code is mendatory");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(ageGroup)){
				error.setDescription("Age group is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Age group is mandatory");
				return customerDetails;
			}
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer Id is mandatory");
				return customerDetails;
			}
			Customer customer =CustomerUtil.getCustomerById(Integer.parseInt(customerId));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer Id is Invalid");
				return customerDetails;
			}
			/*Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByEmailAndProductTypeExceptThisCustomer(customer.getEmail(),productType,customer.getId());
			if(tempCustomer != null) {
				error.setDescription("This EmailId is already signed up. Please choose a different EmailId.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"This EmailId is already signed up. Please choose a different EmailId.");
				return customerDetails;
			}*/
			
			if(TextUtil.isEmptyOrNull(userId)){
				error.setDescription("Please enter valid screen name.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter valid user id.");
				return customerDetails;
			}
			userId = userId.trim();
			if(userId.length()< 5 || userId.length() > 12) {
				error.setDescription("Please select screen name within 5 to 12 letters or numbers.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select screen name within 5 to 12 letters or numbers.");
				return customerDetails;
			}
			if(!userId.matches("[a-zA-Z0-9]*")) {
				error.setDescription("Please select screen name with letters and/or numbers only.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select user id with letters and/or numbers only.");
				return customerDetails;
			}
			Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductTypeExceptThisCustomer(userId, productType, customer.getId());
			//RtfPromotionalEarnLifeOffers rtfReferralTemp = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
			if(tempCustomer != null) {
				error.setDescription("Whoops! This screen name is already signed up. Please choose a different Screen name.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Whoops! This userId is already signed up. Please choose a different UserId.");
				return customerDetails;
			}
			
			customer.setUserId(userId);
			customer.setCtyPhCode(countryCode);
			customer.setAgeGroup(ageGroup);
			customer.setPhone(phone);
			customer.setIsEmailed(false);
			customer.setLoyaltyPoints(1000);
			customer.setIsOtpVerified(true);
			
			try {
				CustomerLoyaltyPointTranx loyaltyPointTrax = new CustomerLoyaltyPointTranx();
				loyaltyPointTrax.setCrBy(userId);
				loyaltyPointTrax.setCrDate(new Date());
				loyaltyPointTrax.setCustId(customer.getId());
				loyaltyPointTrax.setEarnPoints(1000);
				loyaltyPointTrax.setSpentPoints(0);
				loyaltyPointTrax.setBeforePoints(0);
				loyaltyPointTrax.setAfterPoints(1000);
				loyaltyPointTrax.setStatus("ACTIVE");
				loyaltyPointTrax.setOrderId(-1);
				loyaltyPointTrax.setTranxType("REGISTRATION");
				EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			CustomerUtil.updatedCustomerUtil(customer);
			customerDetails.setCustomerId(customer.getId());
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			
			
			if(!TextUtil.isEmptyOrNull(referralcodeStr)){
				Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
				if(referralCustomer == null) {
					error.setDescription("Referral Code is Invalid");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Referral Code is Invalid");
					return customerDetails;
				}
				
				if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("You can't use your own referral code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You can't use your own referral code.");
					return customerDetails;
				}
				if(userId != null && userId.equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("screen name and Referal code can't be the same.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"User Id and Referal code can't be the same.");
					return customerDetails;
				}
			
				QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
				if(existReferralTracking != null) {
					error.setDescription("You have already Used Referral Code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You have already Used Referral Code.");
					return customerDetails;
				}
				
				QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
				Integer tempPrimaryCustPoints=0,tempTireOneCustPoints=0;

				Integer customerPoints = referralCustomer.getLoyaltyPoints();
				if(customerPoints == null) {
					customerPoints=0;
				}

				Integer tireOneCustPoints = customer.getLoyaltyPoints();
				if(tireOneCustPoints == null) {
					tireOneCustPoints=0;
				}

				RtfRewardConfig rewardObj = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PRIMARYREFERRAL);
				if(rewardObj != null) {
					if(rewardObj.getRtfPoints() != null) {
						tempPrimaryCustPoints = rewardObj.getRtfPoints();
						tempTireOneCustPoints = rewardObj.getRtfPoints();
					}
					
					customerPoints = customerPoints + tempPrimaryCustPoints;
					tireOneCustPoints = tireOneCustPoints + tempTireOneCustPoints;

					referraltracking.setLivesToPrimaryCustomer(false);
					referraltracking.setLivesToTireOneCustomer(false);
				}
				referraltracking.setIsAffiliateReferral(false); 
				referraltracking.setYearEndRewardApplied(false);
				referraltracking.setCustomerId(customer.getId());
				referraltracking.setReferralCode(referralcodeStr);
				referraltracking.setReferralCustomerId(referralCustomer.getId());
				referraltracking.setCreatedDateTime(new Date());
				referraltracking.setUpdatedDateTime(new Date());
				referraltracking.setIsGamePlayed(false);
				referraltracking.setIsPurchased(false);
				QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
				String mesg = "Referal Code Applied Successfully.";
			}
			
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer); 
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Success");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Quiz User Id Setup details.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Error occured while Fetching Quiz User Id Setup details.");
			return customerDetails;
		}
		finally {
			log.info("QUIZ UPD CUST PHONE LOGIN : "+request.getParameter("customerId")+" : uId: "+request.getParameter("userId")+" : email: "+request.getParameter("email")+" : refCo: "+request.getParameter("referralCode")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());			
		}
		
		return customerDetails;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value="/UpdateCustomerSignUpDetailsWeb",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails updateCustomerSignUpDetailsWeb(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,authError.getDescription());
				return customerDetails;
			}
			
			String countryCode = request.getParameter("countryCode");
			String phone = request.getParameter("phone");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String ageGroup = request.getParameter("ageGroup");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm"); 
			String userId = request.getParameter("userId");
			String referralcodeStr = request.getParameter("referralCode");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid product type");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Mobile number is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Mobile number is mandatory");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(countryCode)){
				error.setDescription("Country phone code is mendatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Country phone code is mendatory");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(ageGroup)){
				error.setDescription("Age group is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Age group is mandatory");
				return customerDetails;
			}
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(email)){//tempPhone.length() < 10
				error.setDescription("Please enter valid email.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter valid email.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(password)){
				error.setDescription("Please enter a valid password");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGINBYPHONENO,"Please enter a valid password");
				return customerDetails;
			}
			if(password.trim().length() < 8 || password.trim().length() > 20){//tempPhone.length() < 10
				error.setDescription("Password Length must be in between 8-20.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Password Length must be in between 8-20.");
				return customerDetails;
			}
			Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
			if(tempCustomer != null) {
				error.setDescription("This EmailId is already signed up. Please choose a different EmailId.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"This EmailId is already signed up. Please choose a different EmailId.");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(userId)){
				error.setDescription("Please enter valid screen name.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please enter valid user id.");
				return customerDetails;
			}
			userId = userId.trim();
			if(userId.length()< 5 || userId.length() > 12) {
				error.setDescription("Please select screen name within 5 to 12 letters or numbers.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select screen name within 5 to 12 letters or numbers.");
				return customerDetails;
			}
			if(!userId.matches("[a-zA-Z0-9]*")) {
				error.setDescription("Please select screen name with letters and/or numbers only.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Please select user id with letters and/or numbers only.");
				return customerDetails;
			}
			tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
			//RtfPromotionalEarnLifeOffers rtfReferralTemp = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
			if(tempCustomer != null) {
				error.setDescription("Whoops! This screen name is already signed up. Please choose a different screen name.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Whoops! This userId is already signed up. Please choose a different UserId.");
				return customerDetails;
			}
			
			
			password = PasswordUtil.generateEncrptedPassword(password);
			Customer customer = new Customer();
			customer.setEmail(email);
			customer.setPassword(password);
			customer.setProductType(productType);
			customer.setApplicationPlatForm(applicationPlatForm);
			customer.setPhone(phone);
			customer.setSignupDate(new Date());
			customer.setBrokerId(1001);
			customer.setContestHighScore(0);
			customer.setCustomerType(CustomerType.WEB_CUSTOMER);
			customer.setIsBlocked(false);
			customer.setIsClient(false);
			customer.setIsEmailed(false);
			customer.setIsOtpVerified(false);
			customer.setLoyaltyPoints(0);
			customer.setMagicWands(0);
			customer.setQuizCustomerLives(0);
			customer.setReferrerCode(email);
			customer.setUserName(email);
			customer.setRewardDollar(0.00);
			customer.setRtfPoints(0);
			customer.setSfQuesLevel(0);
			customer.setSfStars(0);
			customer.setSignupType(SignupType.REWARDTHEFAN);
			customer.setUserId(userId);
			customer.setCtyPhCode(countryCode);
			customer.setAgeGroup(ageGroup);
			customer.setIsEmailed(false);
			customer.setLoyaltyPoints(1000);
			customer.setIsOtpVerified(true);
			DAORegistry.getCustomerDAO().save(customer);
			
			try {
				CustomerLoyaltyPointTranx loyaltyPointTrax = new CustomerLoyaltyPointTranx();
				loyaltyPointTrax.setCrBy(userId);
				loyaltyPointTrax.setCrDate(new Date());
				loyaltyPointTrax.setCustId(customer.getId());
				loyaltyPointTrax.setEarnPoints(1000);
				loyaltyPointTrax.setSpentPoints(0);
				loyaltyPointTrax.setBeforePoints(0);
				loyaltyPointTrax.setAfterPoints(1000);
				loyaltyPointTrax.setStatus("ACTIVE");
				loyaltyPointTrax.setOrderId(-1);
				loyaltyPointTrax.setTranxType("REGISTRATION");
				EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
			
			String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
			customer.setCustImagePath(fileName);
			try {
				DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
				CassandraDAORegistry.getCassCustomerDAO().updateProfilePicName(customer.getId(), fileName);
			}catch(Exception e) {
				e.printStackTrace();
			}

			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			
			CustomerUtil.updatedCustomerUtil(customer);
			
			if(!TextUtil.isEmptyOrNull(referralcodeStr)){
				Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralcodeStr,ProductType.REWARDTHEFAN);
				if(referralCustomer == null) {
					error.setDescription("Referral Code is Invalid");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Referral Code is Invalid");
					return customerDetails;
				}
				
				if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("You can't use your own referral code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You can't use your own referral code.");
					return customerDetails;
				}
				if(userId != null && userId.equalsIgnoreCase(referralcodeStr)) {
					error.setDescription("screen name and Referal code can't be the same.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"User Id and Referal code can't be the same.");
					return customerDetails;
				}
			
				QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
				if(existReferralTracking != null) {
					error.setDescription("You have already Used Referral Code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You have already Used Referral Code.");
					return customerDetails;
				}
				
				QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
				Integer tempPrimaryCustPoints=0,tempTireOneCustPoints=0;

				Integer customerPoints = referralCustomer.getLoyaltyPoints();
				if(customerPoints == null) {
					customerPoints=0;
				}

				Integer tireOneCustPoints = customer.getLoyaltyPoints();
				if(tireOneCustPoints == null) {
					tireOneCustPoints=0;
				}

				RtfRewardConfig rewardObj = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PRIMARYREFERRAL);
				if(rewardObj != null) {
					if(rewardObj.getRtfPoints() != null) {
						tempPrimaryCustPoints = rewardObj.getRtfPoints();
						tempTireOneCustPoints = rewardObj.getRtfPoints();
					}
					
					customerPoints = customerPoints + tempPrimaryCustPoints;
					tireOneCustPoints = tireOneCustPoints + tempTireOneCustPoints;

					referraltracking.setLivesToPrimaryCustomer(false);
					referraltracking.setLivesToTireOneCustomer(false);
				}
				referraltracking.setIsAffiliateReferral(false); 
				referraltracking.setYearEndRewardApplied(false);
				referraltracking.setCustomerId(customer.getId());
				referraltracking.setReferralCode(referralcodeStr);
				referraltracking.setReferralCustomerId(referralCustomer.getId());
				referraltracking.setCreatedDateTime(new Date());
				referraltracking.setUpdatedDateTime(new Date());
				referraltracking.setIsGamePlayed(false);
				referraltracking.setIsPurchased(false);
				QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
			}
				
			customerDetails.setCustomerId(customer.getId());
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer); 
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Success");
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Quiz User Id Setup details.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Error occured while Fetching Quiz User Id Setup details.");
			return customerDetails;
		}
		finally {
			log.info("QUIZ UPD CUST PHONE LOGIN : "+request.getParameter("customerId")+" : uId: "+request.getParameter("userId")+" : email: "+request.getParameter("email")+" : refCo: "+request.getParameter("referralCode")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());			
		}
		
		return customerDetails;
	}
	
	
	
	
	
	
	
	public QuizOTPTracking generateOTPTracking(String phone, String ctyPhCode,Error error) throws Exception {
		
		String otp = getOneTimePassword();
		Integer minute = QuizSettings.otpLiveSeconds / 60;
		
		QuizOTPTracking otpTracking = new QuizOTPTracking();
		String messageId = null;
		if(URLUtil.isProductionEnvironment) {
			messageId = TwilioSMSServices.sendOTP(phone, otp,minute);	
		} else {
			 messageId = phone+"-"+otp;//TwilioSMSServices.sendOTP(phone, otp,minute);
			 otpTracking.setResponseOtp(otp);
		}
		
		if(messageId == null){
			error.setDescription("Please enter valid phone number to verify OTP.");
			//customerDetails.setError(error);
			//customerDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Phone no is mandatory");
			return null;
		}
		
		otpTracking.setPhone(phone);
		otpTracking.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
		otpTracking.setOtp(otp);
		otpTracking.setCreatedDate(new Date());
		otpTracking.setStatus("ACTIVE");
		otpTracking.setMessageId(messageId);
		QuizDAORegistry.getQuizOTPTrackingDAO().save(otpTracking);
		
		return otpTracking;
	}
	
	public static String getOneTimePassword() throws Exception {
	Random random = new Random();
	return ""+random.nextInt(9)+""+random.nextInt(9)+""+random.nextInt(9)+""+random.nextInt(9);
	}
	
	
	@RequestMapping(value = "/GetOTP", method=RequestMethod.POST)
	public @ResponsePayload QuizOTPDetails generateOTP(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizOTPDetails quizOTPDetails =new QuizOTPDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizOTPDetails.setError(authError);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,authError.getDescription());
				return quizOTPDetails;
			}*/
			
			String phone = request.getParameter("phone");
			String ctyPhCode = request.getParameter("ctyPhCode");
			String resendOTP = request.getParameter("resendOTP");
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Phone no is mandatory");
				quizOTPDetails.setError(error);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Phone no is mandatory");
				return quizOTPDetails;
			}
			
			phone = phone.trim();
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){
				error.setDescription("Please enter valid 10 digit phone number.");
				quizOTPDetails.setError(error);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Please enter valid 10 digit phone number.");
				return quizOTPDetails;
			}
			

			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				 ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			
			
			//phone =  QuizSettings.validatePhoneNo(phone);
			
			if(!TextUtil.isEmptyOrNull(resendOTP) && resendOTP.equalsIgnoreCase("true")){
				QuizDAORegistry.getQuizOTPTrackingDAO().deleteAllOTPByPhoneNo(phone);
			}
			
			QuizOTPTracking otpTracking = generateOTPTracking(phone, ctyPhCode, error);
			if(otpTracking == null) {
				quizOTPDetails.setError(error);
				quizOTPDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,error.getDescription());
				
				log.info("QUIZ GET OTP 00 : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
				return quizOTPDetails;
			}
			
			quizOTPDetails.setOtp(otpTracking.getResponseOtp());
			quizOTPDetails.setExpiryTimeInSeconds(QuizSettings.otpLiveSeconds);
			quizOTPDetails.setStatus(1);
			quizOTPDetails.setOtpTrackingId(otpTracking.getId());
			quizOTPDetails.setMessage("OTP Generated and Sent Successfully.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending OTP.");
			quizOTPDetails.setError(error);
			quizOTPDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETOTP,"Error occured while sending OTP.");
			return quizOTPDetails;
		}
		log.info("QUIZ GET OTP : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizOTPDetails;
	
	}
	
	@RequestMapping(value = "/ValidateOTP", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails validateLoginOTP(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				customerDetails.setError(authError);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,authError.getDescription());
				return customerDetails;
			}*/
			
			String productTypeStr = request.getParameter("productType");
			//String signUpTypeStr = request.getParameter("signUpType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId = request.getParameter("notificationRegId");
			String loginIp = request.getParameter("loginIp");
			String phone = request.getParameter("phone");
			String ctyPhCode = request.getParameter("ctyPhCode");

			String customerId = request.getParameter("customerId");
			String verificationCode = request.getParameter("verificationCode");
			String otpTrackingIdStr = request.getParameter("otpTrackingId");
			
			ApplicationPlatForm applicationPlatForm=null;
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please send valid application platform");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please send phone no");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please send phone no");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			phone = phone.trim();
			if(phone.matches(TextUtil.phoneRegEx) || phone.length() < 10){//tempPhone.length() < 10
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(verificationCode)){
				error.setDescription("Please enter your OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please enter your OTP");
				return customerDetails;
			}
			
			Integer otpTrackingId = null;
			if(!TextUtil.isEmptyOrNull(otpTrackingIdStr)){
				try {
					otpTrackingId = Integer.parseInt(otpTrackingIdStr.trim());
				}catch(Exception e) {
					error.setDescription("Please enter valid OTP");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Please enter valid OTP");
					return customerDetails;
				}
			}
			
			List<QuizOTPTracking> allActiveOtpList = QuizDAORegistry.getQuizOTPTrackingDAO().getActiveOTPByPhoneNo(phone);
			
			if(null == allActiveOtpList || allActiveOtpList.isEmpty()) {
				error.setDescription("You have entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"You have entered wrong OTP");
				return customerDetails;
			}
			
			QuizOTPTracking otpTracking = null;
			for (QuizOTPTracking quizOTPTracking : allActiveOtpList) {
				if(quizOTPTracking.getOtp().equals(verificationCode)){
					otpTracking = quizOTPTracking;
				}
			}
			
			if(otpTracking == null){
				error.setDescription("You entered wrong OTP");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEPHONENOLOGIN,"You have entered wrong OTP");
				return customerDetails;
			}
			
			Customer customer = DAORegistry.getCustomerDAO().get(Integer.parseInt(customerId));
			if(null == customer){
				error.setDescription("The customer id you sent is incorrect. Please try again. ");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"The customer id you sent is incorrect. Please try again. ");
				return customerDetails;
			}
			
			Date sentDate = otpTracking.getCreatedDate();
			Date curDate = new Date();
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sentDate);
			calendar.add(Calendar.SECOND, QuizSettings.otpLiveSeconds);
			
			if(calendar.getTime().before(curDate)){
				error.setDescription("OTP Expired. Please Click Resend");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"You entered wrong OTP");
				return customerDetails;
			} 
			
			otpTracking.setStatus("VALIDATED");
			otpTracking.setVerifiedDate(new Date());
			QuizDAORegistry.getQuizOTPTrackingDAO().update(otpTracking);
			
			Customer duplicatePhoneCustomer = DAORegistry.getCustomerDAO().getOtpVerifiedCustomerByPoneAndProductTypeExceptThisCustomer(phone, ProductType.REWARDTHEFAN, customer.getId());
			if(duplicatePhoneCustomer != null) {
				customerDetails.setIsOtherCustPhoneNo(true);
				customerDetails.setOtherCustEmail(duplicatePhoneCustomer.getEmail());
				customerDetails.setStatus(1);
				String message = "This Phone No Already Mapped with Account "+duplicatePhoneCustomer.getEmail()+". Do you want to reaplce it with this new Account?";
				customerDetails.setMessage(message);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Phone No Exist with different Email.");
				return customerDetails;
			}
			
			if(customer.getPhone() == null || !customer.getPhone().equals(phone.trim()) || !customer.getIsOtpVerified()) {
				customer.setIsOtpVerified(true);
				customer.setCtyPhCode(ctyPhCode);
				customer.setPhone(phone);
				CustomerUtil.updatedCustomerUtil(customer);
				DAORegistry.getCustomerDAO().update(customer);
			}
			//QuizCustomer quizCustomer = QuizDAORegistry.getQuizCustomerDAO().getQuizCustomerByPhone(otpTracking.getPhone());
			//if(null != quizCustomer){
			//	customerDetails.setQuizCustomer(quizCustomer);
			//	customerDetails.setShowUserInfoPopup(false);
			//}else{
			//	customerDetails.setShowUserInfoPopup(true);
			//}
			
			//CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			//Get CustomerAddress by customer id and add it to the response
			//Start			
			/*List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
			List<UserAddress> billingAddress = new ArrayList<UserAddress>();
			List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
			
			if(userAddress != null){
				for(UserAddress address : userAddress){
					if(address.getAddressType() == AddressType.BILLING_ADDRESS)
						billingAddress.add(address);
					else
						shippingAddress.add(address);					
				}
			}*/					
			//End
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			CustomerLoginHistory loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(SignupType.REWARDTHEFAN);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			//loginHistory.setSocialAccessToken(fbAccessToken);			
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			/*if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}
				
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
			}*/
			
			if(customer.getUserId() == null || customer.getUserId().equals("")) {
				customerDetails.setShowUserIdPopup(true);
			} 
			/*boolean customerPic = false;
			String profilePicPrefix = URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = AdminController.readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Begin*/
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			/* 12/07/2018: Update Cassandra Customer Table - By Ulaganathan :: Ends*/
			
			//customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			//if(null !=shippingAddress && shippingAddress.size() >2){
			//	Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			//}
			//customerDetails.getCustomer().setShippingAddress(shippingAddress);
			
			customerDetails.setStatus(1);
			customerDetails.setMessage("OTP successfully verified");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Oops, Somthing is up while validating OTP");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZVALIDATEOTP,"Oops, Somthing is up while validating OTP");
			return customerDetails;
		}
		log.info("QUIZ VALIDATE OTP : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return customerDetails;
	
	}
	
	
	@RequestMapping(value = "/GetQuizMessageTexts", method=RequestMethod.POST)
	public @ResponsePayload QuizMessageTextsInfo getQuizMessageTexts(HttpServletRequest request,HttpServletResponse response){
		QuizMessageTextsInfo messageTextsInfo =new QuizMessageTextsInfo();
		Error error = new Error();
		Date start = new Date();
		try {

			String customerIdStr = request.getParameter("customerId");
			String type = request.getParameter("type");
			String platForm = request.getParameter("platForm");
			
			if(TextUtil.isEmptyOrNull(type)){
				error.setDescription("Type is Mendatory");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Process Type is Mendatory");
				return messageTextsInfo;
			}
			QuizMessageTextType messageType = null;
			try {
				messageType = QuizMessageTextType.valueOf(type);
			}catch (Exception e) {
				error.setDescription("Type is Invalid");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Type is Invalid");
				return messageTextsInfo;
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Customer Id is mandatory");
				return messageTextsInfo;
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Application Platform is mandatory");
				return messageTextsInfo;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					messageTextsInfo.setError(error);
					messageTextsInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Please send valid application platform");
					return messageTextsInfo;
				}
			}
			Integer customerId=null;
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				error.setDescription("Invalid customer Id");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Invalid customer Id");
				return messageTextsInfo;
			}
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Customer Id is Invalid");
				return messageTextsInfo;
			}
			String messageText = "";
			if(messageType.equals(QuizMessageTextType.INVITEFRIEND)) {
				
				/*messageText = "Have you played the Reward The Fan trivia game yet? If you haven�t heard of it, it�s a new trivia "
						+ "game which rewards correctly answered questions with reward dollars and the chance to win big from a "
						+ "cash winner's pot! Plus you're entered into a raffle for sports, concerts and theater tickets! \n"
						+ "Download the app and use my code '"+customer.getUserId()+"' so we'll both get a free life! \n";*/

				/*messageText = "Download Reward The Fan to win Free Tickets to see your favorite live events JUST for answering trivia questions!\n"
						+ " Use my code '"+customer.getUserId()+"' and we'll both get a free life! \n";*/
				
				messageText = "Download Reward The Fan - the ONLY Live Shopping Game Show where you earn BIGGER and BIGGER discounts for every question you get right - YOU are in total control of scoring your own deal!";

				/*messageText = "I'm playing Reward The Fan's Live Trivia Game Show and you should too! \n"
						+ " Use my code '"+customer.getUserId()+"' to sign up and play! \n";*/
				if(applicationPlatForm.equals(ApplicationPlatForm.IOS) || applicationPlatForm.equals(ApplicationPlatForm.ANDROID)) {
					messageText = messageText + "onelink.to/5xwz4g";
				} else {
					messageText = messageText + " https://rewardthefan.com";
				}
			} else if(messageType.equals(QuizMessageTextType.SHAREMYRANK)){
				//messageText="Hi, I am "+customer.getUserId()+", My rank is #0.I have won "+customer.getQuizNoOfTicketsWon()+" Tix " +
					//	"and $"+TicketUtil.getRoundedValueString(customer.getQuizNoOfPointsWon());
				QuizContestWinners winner = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDateAndCustomerId(customerId);
				Integer rank = 0,tixCount=0;
				Double points=0.0;
				if(winner != null) {
					rank = winner.getRewardRank();
					points = winner.getRewardPoints();
					tixCount = winner.getRewardTickets();
				}
				messageText="I just earned $"+TicketUtil.getRoundedValueString(points)+" Reward Points, " +
						" and won "+tixCount+" Tix by playing Reward The Fan! I'm ranked #"+rank+" !";//
			}
			messageTextsInfo.setMsgText(messageText);
			messageTextsInfo.setStatus(1);
			messageTextsInfo.setMessage("");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while getting Quiz Message Texts.");
			messageTextsInfo.setError(error);
			messageTextsInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Error occured while getting Quiz Message Texts.");
			return messageTextsInfo;
		}
		log.info("QUIZ MSG TEXT : "+request.getParameter("customerId")+" : coId: "+request.getParameter("type")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return messageTextsInfo;
		
	}
	
	@RequestMapping(value="/ContestQuestionIssueEmail",method=RequestMethod.GET)
	public @ResponsePayload CustomerDetails sendTriviaWelcomeEmail(HttpServletRequest request, Model model,
			HttpServletResponse response) throws Exception{
		CustomerDetails customerDetails =new CustomerDetails();
		try {
			 
			Map<String,Object> mailMap = new HashMap<String,Object>();
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
			
			
			 File file = new File("C:\\Code\\cusomerid.txt");
		  	  BufferedReader br = new BufferedReader(new FileReader(file));
		  	  String st;
		  	  
		  	  List<String> phoneNumbers = new ArrayList<String>();
		  	  while ((st = br.readLine()) != null) {
		  	    phoneNumbers.add(st);
		  	  }
		  	  
		  	  List<Integer> failedIds = new ArrayList<Integer>();
		  	List<Integer> noEmailList = new ArrayList<Integer>();
			int i =1,total = phoneNumbers.size();
			for (String custIdStr : phoneNumbers) {
				
				Integer custId = Integer.parseInt(custIdStr.trim());
				
				Customer customer = DAORegistry.getCustomerDAO().get(custId);

				if(customer == null || customer.getEmail() == null) {
					noEmailList.add(custId);
					continue;
				}
				
				if(i%10 == 0) {
					System.out.println("Inside Sleep");
					//Thread.sleep(1000);
				}
				
				
				//URLUtil.bccEmails
				String bcc = "tselvan@rightthisway.com";
				try {
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,bcc, "RTFantasy September 5th 2019", "email-contest-issue-notification.html", mailMap, "text/html", null,mailAttachment,null);
				}catch(Exception e){
					e.printStackTrace();
					failedIds.add(custId);
					Thread.sleep(5000);
				}
				i++;
				
				System.out.println("Total: "+total+", Running: "+i+", CustomerID : "+custId+", Email: "+customer.getEmail());
				
			}
			System.out.println("Failed Count : "+failedIds.size());
			for (Integer cid : failedIds) {
				System.out.println(cid);
				
			}
			System.out.println("NoEmail Count : "+failedIds.size());
			for (Integer cid : noEmailList) {
				System.out.println(cid);
				
			}
			
			
		
			customerDetails.setMessage("Email triggered. Please check your email box");
			customerDetails.setStatus(1);
			} catch (Exception e) {
				customerDetails.setMessage("Error: "+e.getLocalizedMessage());
				customerDetails.setStatus(1);
				e.printStackTrace();
			}
			return customerDetails;
	}
	 
	

	@RequestMapping(value="/CustomerSignUpWithoutName",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerRegistrationNew(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//						configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String signUpTypeStr = request.getParameter("signUpType");
			//String name = request.getParameter("name");
			//String lastName =request.getParameter("lastName");
			String referralCodeStr = request.getParameter("referralCode");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId = request.getParameter("notificationRegId");
			String socialAccountId = request.getParameter("socialAccountId");
			String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			String customerPromoAlert = request.getParameter("customerPromoAlert");
			String phone = request.getParameter("phone");
			String ctyPhCode = request.getParameter("ctyPhCode");
			String userId = request.getParameter("userId");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(signUpTypeStr)){
				error.setDescription("SignUp Type is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"SignUp Type is mandatory");
				return customerDetails;
			}
			SignupType signupType=null;
			if(!TextUtil.isEmptyOrNull(signUpTypeStr)){
				try{
					signupType = SignupType.valueOf(signUpTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid signup type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid signup type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			if(signupType.equals(SignupType.FACEBOOK) || signupType.equals(SignupType.GOOGLE)){
				error.setDescription("We no longer support login/Signup via FaceBook and Google.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"We no longer support login/Signup via FaceBook and Google.");
				return customerDetails;
			}
			//if(signupType.equals(SignupType.FACEBOOK) || signupType.equals(SignupType.GOOGLE)){
				
			//}else{
				
				if(TextUtil.isEmptyOrNull(userId)){
					error.setDescription("Please enter valid user id.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter valid user id.");
					return customerDetails;
					//userId = "";
				} else {
					userId = userId.trim();
					if(userId.length()< 5 || userId.length() > 12) {
						error.setDescription("Please select user id within 5 to 12 letters or numbers.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please select user id within 5 to 12 letters or numbers.");
						return customerDetails;
					}
					if(!userId.matches("[a-zA-Z0-9]*")) {
						error.setDescription("Please select user id with letters and/or numbers only.");
						customerDetails.setError(error);
						
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please select user id with letters and/or numbers only.");
						return customerDetails;
					}
					Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
					RtfPromotionalEarnLifeOffers rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
					if(tempCustomer != null || rtfReferral != null) {
							error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
							customerDetails.setError(error);
							customerDetails.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Whoops! This userId is already signed up. Please choose a different UserId.");
							return customerDetails;
					}
				}
			//}		
			
			/*if(TextUtil.isEmptyOrNull(lastName)){
				error.setDescription("Customer LastName is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Customer LastName is mandatory");
				return customerDetails;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(name)){
				error.setDescription("Please enter your First Name");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your First Name");
				return customerDetails;
			}*/
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please enter your Phone No");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Phone no");
				return customerDetails;
			}
			phone =  QuizSettings.validatePhoneNo(phone);
			if(phone.length() < 10){
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			
			if(!TextUtil.isEmptyOrNull(customerPromoAlert) && customerPromoAlert.equals("YES")){
				customerDetails.setShowPromoAert(true);
				customerDetails.setPromoAlertMessage(TextUtil.getCustomerPromoAlertText());
			}
			
			String finalPassword = password;
			CustomerLoyalty customerLoyalty = null;
			Customer validateCustomer = null;
			CustomerLoginHistory loginHistory = null;
			CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			if(signupType.equals(SignupType.FACEBOOK)){}
			else if(signupType.equals(SignupType.GOOGLE)){}
			else{
				if(TextUtil.isEmptyOrNull(email)){
					error.setDescription("Please enter your Email");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Email");
					return customerDetails;
				}
				
				if(TextUtil.isEmptyOrNull(password)){
					error.setDescription("Please choose a Password");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please choose a Password");
					return customerDetails;
				}
				finalPassword = password;
				
				validateCustomer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
				
				//executed only when customer is already registered with us from RTW and RTW2 database
				if(validateCustomer != null && (validateCustomer.getProductType().equals(ProductType.RTW) || 
						validateCustomer.getProductType().equals(ProductType.RTW2) || validateCustomer.getProductType().equals(ProductType.TIXCITY))){
					validateCustomer.setProductType(ProductType.REWARDTHEFAN);
					validateCustomer.setSignupType(SignupType.REWARDTHEFAN);
					validateCustomer.setSignupDate(new Date());
					validateCustomer.setIsEmailed(true);
					validateCustomer.setApplicationPlatForm(applicationPlatForm);
					validateCustomer.setCustomerName("");
					validateCustomer.setLastName("");
					/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
					String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
					Long referrerNumber =  Long.valueOf(property.getValue());
					referrerNumber++;  
					validateCustomer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
					property.setValue(String.valueOf(referrerNumber));
					DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
					validateCustomer.setReferrerCode(validateCustomer.getEmail());
					validateCustomer.setEncryptPassword(finalPassword);
					validateCustomer.setBrokerId(1001);
					validateCustomer.setIsClient(true);
					
					validateCustomer.setUserId(userId);
					if(phone != null && !phone.isEmpty()) {
						validateCustomer.setPhone(phone);
						validateCustomer.setCtyPhCode(ctyPhCode);
					}
					DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
					
					if(validateCustomer.getCustImagePath() == null) {
						String fileName = URLUtil.getCustomerDefaultprofilePicName(validateCustomer.getId());
						validateCustomer.setCustImagePath(fileName);
						DAORegistry.getCustomerDAO().updateCustImagePath(fileName, validateCustomer.getId());
						//CustomerUtil.updatedCustomerUtil(customer);
					}
					/*Add new Customer to Customer Util - Begins*/
					try{
						CustomerUtil.updatedCustomerUtil(validateCustomer);
					}catch(Exception e){
						e.printStackTrace();
					}
					/*Add new Customer to Customer Util - Ends*/
					try{
						//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
						DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(validateCustomer.getId());
					}catch(Exception e){
						e.printStackTrace();
					}
					
					
					String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(validateCustomer.getId()+""+email.split("@")[0]);
					
					Calendar calendar = new GregorianCalendar();
					calendar.add(Calendar.YEAR, 1);
					
					RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
					custPromoOffer.setCreatedDate(new Date());
					custPromoOffer.setCustomerId(validateCustomer.getId());
					custPromoOffer.setDiscount(25.00);
					custPromoOffer.setStartDate(new Date());
					custPromoOffer.setEndDate(calendar.getTime());
					custPromoOffer.setFlatOfferOrderThreshold(1.00);
					custPromoOffer.setIsFlatDiscount(false);
					custPromoOffer.setMaxOrders(1);
					custPromoOffer.setModifiedDate(new Date());
					custPromoOffer.setPromoCode(custPromoCode);
					custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
					custPromoOffer.setNoOfOrders(0);
					custPromoOffer.setStatus("ENABLED");
					
					boolean isEmailSent = true;
					String toEmail = validateCustomer.getEmail();
					try{
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("promotionalCode", custPromoCode);
						mailMap.put("userName",validateCustomer.getEmail());
						/*String userNameHtml = "<span style='color: #F06931;'> "+validateCustomer.getEmail()+" ";
						if(null != validateCustomer.getUserId() && !validateCustomer.getUserId().isEmpty()){
							userNameHtml += "</span> or <span style='color: #F06931;'> "+validateCustomer.getUserId()+" </span>";
						}else{
							userNameHtml += "</span>";
						}
						mailMap.put("userNameHtml",userNameHtml);*/
						
						//inline(image in mail body) image add code
						MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, validateCustomer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
						
					}catch(Exception e1){
						e1.printStackTrace();
						isEmailSent = false;
						toEmail = "";
					}
					custPromoOffer.setIsEmailSent(isEmailSent);
					custPromoOffer.setToEmail(toEmail);
					//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
					
					
					//List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(validateCustomer.getId());
					List<UserAddress> billingAddress = new ArrayList<UserAddress>();
					List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
					
					//if(userAddress != null){
					//	for(UserAddress address : userAddress){
					//		if(address.getAddressType() == AddressType.BILLING_ADDRESS)
					//			billingAddress.add(address);
					//		else
					//			shippingAddress.add(address);					
					//	}
					//}	
					
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					
					if(null != customerLoyalty){
						
					}else{
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setCustomerId(validateCustomer.getId());
						customerLoyalty.setActivePointsAsDouble(0.00);
						customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
						customerLoyalty.setLatestSpentPointsAsDouble(0.00);
						customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
						customerLoyalty.setTotalSpentPointsAsDouble(0.00);
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLastNotifiedTime(new Date());
						customerLoyalty.setPendingPointsAsDouble(0.00);
						
						//Save Customer Loyalty Information
						DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
					}
					
					
					//customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					customerDetails.setCustomerLoyalty(customerLoyalty);
					customerDetails.setCustomer(validateCustomer);
					customerDetails.getCustomer().setBillingAddress(billingAddress);
					if(null !=shippingAddress && shippingAddress.size() >2){
						Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
					}
					customerDetails.getCustomer().setShippingAddress(shippingAddress);
					
					customerDetails.setStatus(1);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
					return customerDetails;
				}else{
					if(null != validateCustomer){
						error.setDescription("Whoops! This email is already signed up. Please choose a different Email.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Whoops! This email is already signed up. Please choose a different Email.");
						return customerDetails;
					}
				}
			}
			
			Customer customer = new Customer();
			
			//11-21-2018 3:22 PM - Give 1 life to all new registration. Given by Mitul - Done By Ulaganathan
			customer.setQuizCustomerLives(1);
			
			customer.setBrokerId(1001);
			customer.setIsClient(true);
			customer.setProductType(productType);
			customer.setSignupType(signupType);
			customer.setCustomerName(userId);
			customer.setLastName("");
			//customer.setReferralCode(null!=referralCode?referralCode:"");
			customer.setEmail(email);
			customer.setEncryptPassword(finalPassword);
			customer.setUserName(email);
			customer.setSignupDate(new Date());
			customer.setCustomerType(CustomerType.WEB_CUSTOMER);
			customer.setApplicationPlatForm(applicationPlatForm);
			customer.setDeviceId(null != deviceId?deviceId:"");
			customer.setNotificationRegId(null!=notificationRegId?notificationRegId:"");
			customer.setSocialAccountId(null != socialAccountId?socialAccountId:"");
			
			customer.setUserId(userId);
			customer.setPhone((null != phone && !phone.equals(""))?phone:null);
			customer.setCtyPhCode(ctyPhCode);;
			/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
			String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
			Long referrerNumber =  Long.valueOf(property.getValue());
			referrerNumber++;  
			customer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
			property.setValue(String.valueOf(referrerNumber));
			DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
			customer.setReferrerCode(email);
			//Save Customer
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			
			//property.setValue(String.valueOf(referrerNumber));
			
			//Save Customer Referral Code Auto Number
			//DAORegistry.getPropertyDAO().saveOrUpdate(property);
			
			if(customer.getCustImagePath() == null) {
				String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
				customer.setCustImagePath(fileName);
				DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
				//CustomerUtil.updatedCustomerUtil(customer);
			}
			/*Add new Customer to Customer Util - Begins*/
			try{
				CustomerUtil.updatedCustomerUtil(customer);
			}catch(Exception e){
				e.printStackTrace();
			}
			/*Add new Customer to Customer Util - Ends*/
			
			
			customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			if(null != customerLoyalty){
				
			}else{
				customerLoyalty = new CustomerLoyalty();
				customerLoyalty.setCustomerId(customer.getId());
				customerLoyalty.setActivePointsAsDouble(0.00);
				customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
				customerLoyalty.setLatestSpentPointsAsDouble(0.00);
				customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
				customerLoyalty.setTotalSpentPointsAsDouble(0.00);
				customerLoyalty.setLastUpdate(new Date());
				customerLoyalty.setLastNotifiedTime(new Date());
				customerLoyalty.setPendingPointsAsDouble(0.00);
				customerLoyalty.setTotalAffiliateReferralDollars(0.00);
				customerLoyalty.setLastAffiliateReferralDollars(0.00);
				//Save Customer Loyalty Information
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			}
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(signupType);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			loginHistory.setSocialAccessToken(fbAccessToken);	
			
			//Save Customer Login History
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
				}
				//Save Customer Device Details
				DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
			
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Begins*/
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Ends*/
			}
			
			if(!TextUtil.isEmptyOrNull(referralCodeStr)){
				//error.setDescription("Referral Code is mandatory");
				//customerDetails.setError(error);
				//customerDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Referral Code is mandatory");
				//return customerDetails;
			//} else {
			
				Boolean isRtfReferalCode = false;
				RtfPromotionalEarnLifeOffers rtfReferral = null;
				Customer referralCustomer = DAORegistry.getCustomerDAO().getCustomerByUserIdAndProductType(referralCodeStr,ProductType.REWARDTHEFAN);
				if(referralCustomer == null) {
					
					rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getActiveRtfPromotionEarnLifeOfferByPromoCode(referralCodeStr);
					Date today = new Date();
					if(rtfReferral != null && rtfReferral.getStartDate().compareTo(today)<=0 && rtfReferral.getEndDate().compareTo(today)>=0 ) {
						isRtfReferalCode = true;
					} else {
						error.setDescription("Referral Code is Invalid");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Referral Code is Invalid");
						return customerDetails;
					}
				}
				
				if(customer.getUserId() != null && customer.getUserId().equalsIgnoreCase(referralCodeStr)) {
					error.setDescription("You can't use your own referral code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"You can't use your own referral code.");
					return customerDetails;
				}
				if(userId != null && userId.equalsIgnoreCase(referralCodeStr)) {
					error.setDescription("User Id and Referal code can't be the same.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"User Id and Referal code can't be the same.");
					return customerDetails;
				}
			
				QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
				if(existReferralTracking != null) {
					error.setDescription("Customer already Used Referral Code");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOMERSIGNUPDETAILS,"Customer already Used Referral Code");
					return customerDetails;
				}
				/*QuizCustomerReferralTracking existReferralTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerIdandReferralCode(customer.getId(), referralcodeStr);
				if(existReferralTracking != null) {
					error.setDescription("Customer already updated with This Referral Code");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZUSERIDSETUP,"Customer already updated with This Referral Code");
					return customerDetails;
				}*/
				
				if(isRtfReferalCode) {
					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setReferralCode(referralCodeStr);
					//referraltracking.setReferralCustomerId(referralCustomer.getId());
					referraltracking.setRtfPromotionalId(rtfReferral.getId());
					referraltracking.setUpdatedDateTime(new Date());
					referraltracking.setIsAffiliateReferral(false);
					referraltracking.setCreatedDateTime(new Date());
					referraltracking.setIsGamePlayed(false);
					referraltracking.setIsPurchased(false);
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					
					Integer customerLives = customer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					customerLives=customerLives+rtfReferral.getMaxLifePerCustomer();
					customer.setQuizCustomerLives(customerLives);	
					
					
					DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),customerLives);
					CustomerUtil.updatedCustomerUtil(customer);
					
					//QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(customer);
				} else {

					QuizCustomerReferralTracking referraltracking = new QuizCustomerReferralTracking();
					
					Integer customerLives = referralCustomer.getQuizCustomerLives();
					if(customerLives == null) {
						customerLives=0;
					}
					
					Integer tireOneCustLives = customer.getQuizCustomerLives();
					if(tireOneCustLives == null) {
						tireOneCustLives=0;
					}
					
					boolean showPrimaryCustNotification = true,showTireOneCustNotification = true;;
					
					QuizAffiliateSetting quizAffiliateSetting = DAORegistry.getQuizAffiliateSettingDAO().getActiveAffiliateByCustomerId(referralCustomer.getId());
					
					if(null != quizAffiliateSetting) {
						
						referraltracking.setIsAffiliateReferral(true);
						referraltracking.setAffiliateReferralStatus("ACTIVE");
						
						if(null != quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() && quizAffiliateSetting.getNoOfLivesToAffiliateCustomer() > 0) {
							referraltracking.setLivesToPrimaryCustomer(true);
							customerLives = customerLives + quizAffiliateSetting.getNoOfLivesToAffiliateCustomer();
						}else {
							referraltracking.setLivesToPrimaryCustomer(false);
							customerLives = customerLives + 0;
							showPrimaryCustNotification = false;
						}
						
						if(null != quizAffiliateSetting.getNoOfLivesToTireOneCustomer() && quizAffiliateSetting.getNoOfLivesToTireOneCustomer() > 0) {
							referraltracking.setLivesToTireOneCustomer(true);
							tireOneCustLives = tireOneCustLives + quizAffiliateSetting.getNoOfLivesToTireOneCustomer();
						}else {
							referraltracking.setLivesToTireOneCustomer(false);
							tireOneCustLives = tireOneCustLives + 0;
							showTireOneCustNotification = false;
						}
						
					}else {
						referraltracking.setIsAffiliateReferral(false); 
						referraltracking.setLivesToPrimaryCustomer(true);
						referraltracking.setLivesToTireOneCustomer(true);
						
						customerLives++;
						tireOneCustLives++;
					}
					
					referraltracking.setCustomerId(customer.getId());
					referraltracking.setYearEndRewardApplied(false);
					referraltracking.setReferralCode(referralCodeStr);
					referraltracking.setReferralCustomerId(referralCustomer.getId());
					referraltracking.setCreatedDateTime(new Date());
					referraltracking.setUpdatedDateTime(new Date());
					referraltracking.setIsGamePlayed(false);
					referraltracking.setIsPurchased(false);
					QuizDAORegistry.getQuizCustomerReferralTrackingDAO().save(referraltracking);
					 
					
					if(showPrimaryCustNotification) {
						referralCustomer.setQuizCustomerLives(customerLives);
						DAORegistry.getCustomerDAO().updateQuizCustomerLives(referralCustomer.getId(),customerLives);
						CustomerUtil.updatedCustomerUtil(referralCustomer);
						
						QuizCustomerLifeLineEarnedNotificationUtil.sendLifeLinedEarnedNotification(referralCustomer,customer);
					}
					
					if(showTireOneCustNotification) {
						customer.setQuizCustomerLives(tireOneCustLives);	
						DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),tireOneCustLives);
						CustomerUtil.updatedCustomerUtil(customer);
						QuizCustomerLifeLineEarnedNotificationUtil.tireOneCustomerEarnedLifeline(customer);
					}
				}
			}
			
			String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+email.split("@")[0]);
			
			Calendar calendar = new GregorianCalendar();
			calendar.add(Calendar.YEAR, 1);
			
			RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
			custPromoOffer.setCreatedDate(new Date());
			custPromoOffer.setCustomerId(customer.getId());
			custPromoOffer.setDiscount(25.00);
			custPromoOffer.setStartDate(new Date());
			custPromoOffer.setEndDate(calendar.getTime());
			custPromoOffer.setFlatOfferOrderThreshold(1.00);
			custPromoOffer.setIsFlatDiscount(false);
			custPromoOffer.setMaxOrders(1);
			custPromoOffer.setModifiedDate(new Date());
			custPromoOffer.setPromoCode(custPromoCode);
			custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
			custPromoOffer.setNoOfOrders(0);
			custPromoOffer.setStatus("ENABLED");
			
			boolean isEmailSent = true;
			String toEmail = customer.getEmail();
			
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("promotionalCode", custPromoCode);
			mailMap.put("userName",customer.getEmail());
			
			/*String userNameHtml = "<span style='color: #F06931;'> "+customer.getEmail()+" ";
			if(null != customer.getUserId() && !customer.getUserId().isEmpty()){
				userNameHtml += "</span> or <span style='color: #F06931;'> "+customer.getUserId()+" </span>";
			}else{
				userNameHtml += "</span>";
			}
			mailMap.put("userNameHtml",userNameHtml);*/
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			
			try{
				switch (productType) {
					case REWARDTHEFAN:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
						break;
					
					case ZONETICKETS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case MINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case LASTROWMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case VIPMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.fromEmail, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
	
					default:
						break;
				}
			
			}catch(Exception e){
				e.printStackTrace();
				isEmailSent = false;
				toEmail = "";
			}
			
			custPromoOffer.setIsEmailSent(isEmailSent);
			custPromoOffer.setToEmail(toEmail);
			//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
			
			/*boolean customerPic = false;
			String profilePicPrefix =URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while creating customer.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Error occured while creating customer");
				return customerDetails;
			}
		}	 
	
	@RequestMapping(value="/CustomerLoginNew",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails customerLoginNew(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"You are not authorized");
				return customerDetails;
			}
			
			String loginPassword = request.getParameter("password");
			String loginEmail = request.getParameter("userName");
			String productTypeStr = request.getParameter("productType");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId=request.getParameter("notificationRegId");
			//String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid application platform");
					return customerDetails;
				}
			}
			
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			if(TextUtil.isEmptyOrNull(loginEmail)){
				error.setDescription("Please enter your Email");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Please enter your Email");
				return customerDetails;
			}
			if(TextUtil.isEmptyOrNull(loginPassword)){
				error.setDescription("Please enter an Email and Password to continue");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Please enter an Email and Password to continue");
				return customerDetails;
			}
			CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			//Get customer by email or user id
			Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserNameByProductType(loginEmail,productType);
			//Customer customer = DAORegistry.getCustomerDAO().getCustomerByEmailByProductType(loginEmail,productType);
			
			if(null == customer){
				error.setDescription("The email or user id you entered is incorrect. Please try again. ");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"The email or user id you entered is incorrect. Please try again.");
				return customerDetails;
			}
			String dbPassword = customer.getPassword().replace(" +", "");
			loginPassword = loginPassword.replace(" +", "");
			String password = PasswordUtil.generateEncrptedPassword(loginPassword);
			
			if(!password.equals(dbPassword)){
				error.setDescription("The password you entered is incorrect.Click Forgot Password to retrieve login information");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Click Forgot Email or Password to retrieve login information");
				return customerDetails;
			}
			
			//Get CustomerAddress by customer id and add it to the response
			//Start			
			List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
			List<UserAddress> billingAddress = new ArrayList<UserAddress>();
			List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
			
			if(userAddress != null){
				for(UserAddress address : userAddress){
					if(address.getAddressType() == AddressType.BILLING_ADDRESS)
						billingAddress.add(address);
					else
						shippingAddress.add(address);					
				}
			}						
			//End
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			CustomerLoginHistory loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(SignupType.REWARDTHEFAN);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			//loginHistory.setSocialAccessToken(fbAccessToken);			
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
				}
				
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			/*boolean customerPic = false;
			String profilePicPrefix = URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			
			if(customer.getUserId() == null || customer.getUserId().isEmpty()) {
				try {
					String userId = UserIdCreationScheduler.generateCustomerUserId(customer.getCustomerName(), customer.getLastName(), customer.getEmail());
					if(userId != null && !userId.isEmpty()) {
						customer.setUserId(userId.toLowerCase());
						DAORegistry.getCustomerDAO().saveOrUpdate(customer);
						CustomerUtil.updatedCustomerUtil(customer);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			customerDetails.getCustomer().setBillingAddress(billingAddress);
			
			if(null !=shippingAddress && shippingAddress.size() >2){
				Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
			}
			customerDetails.getCustomer().setShippingAddress(shippingAddress);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Oops Somthing Went Wrong. Please Login Again!");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERLOGIN,"Oops Somthing Went Wrong. Please Login Again!");
			return customerDetails;
		}
	}
	
	
	@RequestMapping(value = "/UploadProfilePicture", method=RequestMethod.GET)
	public @ResponsePayload CustomerProfilePic uploadProfilePicToAwsS23(HttpServletRequest request, HttpServletResponse response, Model model){
		CustomerProfilePic customerProfilePic = new CustomerProfilePic();
		List<Customer> customers = DAORegistry.getCustomerDAO().getAllOtpVerifiedCustomers1();
		try {
			AwsS3Dp.uploadPictures(customers);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return customerProfilePic;
	}
	
	
	@RequestMapping(value = "/UploadDefaultProfilePicture", method=RequestMethod.GET)
	public @ResponsePayload CustomerProfilePic uploadDefaultProfilePicToAwsS23(HttpServletRequest request, HttpServletResponse response, Model model){
		CustomerProfilePic customerProfilePic = new CustomerProfilePic();
		String fileName = "C:\\Tomcat 7.0\\webapps\\SvgMaps\\RTF_DEFAULT_DP\\";
		for (int i=0;i<20;i++) {
			String filePath = fileName+"DEFAULT_DP_"+i+".jpg";
			File fileObj = new File(filePath);
			String ext ="jpg";
			AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTFMEDIA, "default_"+i+"."+ext, AWSFileService.CUSTOMER_DP_DIRECTORY, fileObj);
		} 
		return customerProfilePic;
	}
	
	
	@RequestMapping(value = "/DeleteDefaultProfilePicture", method=RequestMethod.GET)
	public @ResponsePayload CustomerProfilePic deleteDefaultProfilePicFromAwsS23(HttpServletRequest request, HttpServletResponse response, Model model){
		CustomerProfilePic customerProfilePic = new CustomerProfilePic();
		for (int i=0;i<20;i++) {
			AWSFileService.deleteFilesFromS3(AWSFileService.BUCKET_NAME_RTFMEDIA, "default_"+i+".jpg", AWSFileService.CUSTOMER_DP_DIRECTORY);
		} 
		return customerProfilePic;
	}
	
	
	@RequestMapping(value = "/SampleFileUploadToS3", method=RequestMethod.POST)
	public @ResponsePayload CustomerProfilePic sampleFileUploadToAwsS3(@RequestParam (value="customerProfilePic", required=false) MultipartFile uploadedFile,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		CustomerProfilePic customerProfilePic = new CustomerProfilePic();
		try {
			
			String customerId = request.getParameter("cId");
			String fileExt = uploadedFile.getOriginalFilename();
			String ext = FilenameUtils.getExtension(fileExt);
			System.out.println(fileExt+"-----------"+ext);
			File fileObj = AWSFileService.convertMultiPartToFile(uploadedFile);
			String fileName = customerId+"."+ext;
			AWSFileService.upLoadFileToS3(AWSFileService.BUCKET_NAME_RTFMEDIA, fileName, AWSFileService.CUSTOMER_DP_DIRECTORY, fileObj);	
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return customerProfilePic;
	}
	
	@RequestMapping(value = "/GetCustProfileInfo", method=RequestMethod.POST)
	public @ResponsePayload CustProfileDtls getCustProfileInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustProfileDtls custProfileDtls =new CustProfileDtls();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}*/
			
			String customerIdStr = request.getParameter("cuId");
			String profileTypeStr = request.getParameter("pType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"Customer Id is mandatory");
				return custProfileDtls;
			}
			
			if(TextUtil.isEmptyOrNull(profileTypeStr)){
				error.setDescription("Profile Type is mandatory.");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"Profile Type is mandatory");
				return custProfileDtls;
			}
			if(!profileTypeStr.equals("BASIC") && !profileTypeStr.equals("FANDOM")) {
				error.setDescription("Profile Type is Invalid.");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"Profile Type is Invalid");
				return custProfileDtls;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
			//Customer customer =  DAORegistry.getCustomerDAO().get(Integer.parseInt(customerIdStr.trim()));
			if(customer == null){
				error.setDescription("Customer not valid");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"Customer not valid");
				return custProfileDtls;
			}
			int totalquestions = 0,totalanswers=0,percentage = 0;
			List<RtfCustProfileAnswers> custProfAnsList = new ArrayList<RtfCustProfileAnswers>();
			
			if(profileTypeStr.equals("BASIC")) {
				CustProfileBasicInfo basicInfo = new CustProfileBasicInfo();
				basicInfo.setUserId(customer.getUserId());
				basicInfo.setEmail(customer.getEmail());
				basicInfo.setPhone(customer.getPhone());
				basicInfo.setfName(customer.getCustomerName());
				basicInfo.setlName(customer.getLastName());
				
				List<RtfCustProfileQuestions> questionsList = QuizDAORegistry.getRtfCustProfileQuestionsDAO().getAllActiveRtfCustProfileQuestionsByProfileType(profileTypeStr.toUpperCase());
				if(questionsList != null) {
					for (RtfCustProfileQuestions question : questionsList) {
						if(question.getqType().equals(ProfileQuestionType.FNAME.toString())) {
							basicInfo.setfNamePts(question.getPoints());
						} else if(question.getqType().equals(ProfileQuestionType.LNAME.toString())) {
							basicInfo.setlNamePts(question.getPoints());
						} else if(question.getqType().equals(ProfileQuestionType.ADDRESS.toString())) {
							basicInfo.setAddrPts(question.getPoints());
						} else if(question.getqType().equals(ProfileQuestionType.DOB.toString())) {
							basicInfo.setDobPts(question.getPoints());
						} else if(question.getqType().equals(ProfileQuestionType.GENDER.toString())) {
							basicInfo.setGenderPts(question.getPoints());
						}
					}
				}
				
				List<RtfCustProfileAnswers> answerList = QuizDAORegistry.getRtfCustProfileAnswersDAO().getRtfCustProfileQuestionAnswersByCustomerIdAndProfileType(customer.getId(), profileTypeStr);
				if(answerList != null && !answerList.isEmpty()) {
					for (RtfCustProfileAnswers answer : answerList) {
						totalquestions++;
						if(answer.getAnswer() != null) {
							totalanswers++;
						}
						if(answer.getpType()!= null && answer.getpType().equals("BASIC") 
								&& answer.getqType() != null) {
							/*if(answer.getqType().equals(ProfileQuestionType.FNAME.toString())) {
								basicInfo.setfName(answer.getAnswer());
							} else if(answer.getqType().equals(ProfileQuestionType.LNAME.toString())) {
								basicInfo.setlName(answer.getAnswer());
							} else*/
							if(answer.getqType().equals(ProfileQuestionType.ADDRESS.toString())) {
								basicInfo.setAddr(answer.getAnswer());
							} else if(answer.getqType().equals(ProfileQuestionType.DOB.toString())) {
								basicInfo.setDob(answer.getAnswer());
							} else if(answer.getqType().equals(ProfileQuestionType.GENDER.toString())) {
								basicInfo.setGender(answer.getAnswer());
							}
						}
					}
					percentage = Math.round((totalanswers*100)/totalquestions);
					
					custProfileDtls.setPercentage(percentage);
					custProfileDtls.setBasicInfo(basicInfo);
				}
				RtfRewardConfig rewardConfig = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PROFILE_BASIC);
				if(rewardConfig != null && rewardConfig.getRtfPoints() != null && 
						rewardConfig.getRtfPoints() > 0 ) {
					custProfileDtls.setPointsTxt("BASIC INFO : "+rewardConfig.getRtfPoints()+" POINTS");	
				} else {
					custProfileDtls.setPointsTxt("BASIC INFO ");
				}
				
			} else if(profileTypeStr.equals("FANDOM")) {
				List<RtfCustProfileAnswers> notAnsList = new ArrayList<RtfCustProfileAnswers>();
				List<RtfCustProfileAnswers>answeredList = new ArrayList<RtfCustProfileAnswers>();
				List<RtfCustProfileAnswers> answerList = QuizDAORegistry.getRtfCustProfileAnswersDAO().getRtfCustProfileQuestionAnswersByCustomerIdAndProfileType(customer.getId(), profileTypeStr);
				if(answerList != null) {
					for (RtfCustProfileAnswers answer : answerList) {
						totalquestions++;
						if(answer.getAnswer() != null) {
							totalanswers++;
						}
						if(answer.getpType()!= null && answer.getpType().equals(profileTypeStr)) {
							//custProfAnsList.add(answer);
							if(answer.getAnswer() != null) {
								answeredList.add(answer);
							} else {
								notAnsList.add(answer);
							}
						}
					}
					custProfAnsList.addAll(notAnsList);
					custProfAnsList.addAll(answeredList);
					
					percentage = Math.round((totalanswers*100)/totalquestions);
					
					custProfileDtls.setPercentage(percentage);
					custProfileDtls.setAnswerList(custProfAnsList);
				}
				RtfRewardConfig rewardConfig = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PROFILE_FANDOM);
				if(rewardConfig != null && rewardConfig.getRtfPoints() != null && 
						rewardConfig.getRtfPoints() > 0 ) {
					custProfileDtls.setPointsTxt("FANDOM INFO : "+rewardConfig.getRtfPoints()+" POINTS");	
				} else {
					custProfileDtls.setPointsTxt("FANDOM INFO ");
				}
			}
			
			custProfileDtls.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"Success");
			}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Information.");
			custProfileDtls.setError(error);
			custProfileDtls.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"Error occured while Fetching Customer Information");
			return custProfileDtls;
		}
		
		return custProfileDtls;
	
	}
	 
	@RequestMapping(value = "/UpdateCustProfileQuest", method=RequestMethod.POST)
	public @ResponsePayload CustProfileDtls updateCustProfileQuest(HttpServletRequest request,HttpServletResponse response,
			Model model){
		
		CustProfileDtls custProfileDtls =new CustProfileDtls();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}*/
			
			String customerIdStr = request.getParameter("cuId");
			String profileTypeStr = request.getParameter("pType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Customer Id is mandatory");
				return custProfileDtls;
			}
			
			if(TextUtil.isEmptyOrNull(profileTypeStr)){
				error.setDescription("Profile Type is mandatory.");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Profile Type is mandatory");
				return custProfileDtls;
			}
			if(!profileTypeStr.equals("BASIC") && !profileTypeStr.equals("FANDOM")) {
				error.setDescription("Profile Type is Invalid.");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Profile Type is Invalid");
				return custProfileDtls;
			}
			Customer customer = DAORegistry.getCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr.trim()));//CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
			if(customer == null){
				error.setDescription("Customer not valid");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Customer not valid");
				return custProfileDtls;
			}
			
			boolean isCustUpdate = false;
			boolean isNewAnswer=false;
			Integer totPoints = 0;
			SourceType sourceType = null;
			List<RtfCustProfileAnswers> answersList = new ArrayList<RtfCustProfileAnswers>();
			
			if(profileTypeStr.equals("FANDOM")) {
				
				String qidsStr = request.getParameter("qids");
				if(TextUtil.isEmptyOrNull(qidsStr)){
					error.setDescription("Question id is mandatory.");
					custProfileDtls.setError(error);
					custProfileDtls.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Question id is mandatory.");
					return custProfileDtls;
				}
				
				/*List<RtfCustProfileAnswers> ansListDb = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndProfileType(customer.getId(), profileTypeStr);
				Map<Integer,RtfCustProfileAnswers> ansMapDb = new HashMap<Integer,RtfCustProfileAnswers>();
				if(ansListDb != null) {
					for (RtfCustProfileAnswers rtfCustProfileAnswers : ansListDb) {
						ansMapDb.put(rtfCustProfileAnswers.getqId(), rtfCustProfileAnswers);
					}
				}*/
				List<RtfCustProfileQuestions> questionsList = QuizDAORegistry.getRtfCustProfileQuestionsDAO().getAllActiveRtfCustProfileQuestions();
				Map<Integer,RtfCustProfileQuestions> questionsMap = new HashMap<Integer,RtfCustProfileQuestions>();
				if(questionsList != null) {
					for (RtfCustProfileQuestions profileQuestions : questionsList) {
						questionsMap.put(profileQuestions.getId(), profileQuestions);
					}
				}
				
				String[] qidsArr = qidsStr.split(",");
				for (String qidStr : qidsArr) {
					try {
						Integer qid = Integer.parseInt(qidStr);
						
						String ans = request.getParameter("ans_"+qid);
						if(TextUtil.isEmptyOrNull(ans)){
							error.setDescription("Question id is mandatory.");
							custProfileDtls.setError(error);
							custProfileDtls.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Question id is mandatory.");
							return custProfileDtls;
						}
						if(ans.length()> 250) {
							error.setDescription("Answer should be within 250 letters.");
							custProfileDtls.setError(error);
							custProfileDtls.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Answer should be within 250 letters.");
							return custProfileDtls;
						}
						RtfCustProfileQuestions profileQuest = questionsMap.get(qid);
						if(profileQuest != null) {
							//RtfCustProfileAnswers custAns = ansMapDb.get(qid);
							RtfCustProfileAnswers custAns = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(customer.getId(), qid);
							if(custAns == null) {
								custAns = new RtfCustProfileAnswers();
								custAns.setAnswer(ans);
								custAns.setqId(qid);
								custAns.setCuId(customer.getId());
								custAns.setPoints(profileQuest.getPoints());
								custAns.setStatus("ACTIVE");
								custAns.setCreated(new Date());
								totPoints += profileQuest.getPoints();
								isNewAnswer = true;
							} else {
								custAns.setAnswer(ans);
								custAns.setUpdated(new Date());
							}
							QuizDAORegistry.getRtfCustProfileAnswersDAO().saveOrUpdate(custAns);
							//answersList.add(custAns);
						} else {
							error.setDescription("Question Id is Invalid.");
							custProfileDtls.setError(error);
							custProfileDtls.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Question Id is Invalid.");
							return custProfileDtls;
						}
						
					} catch(Exception e) {
						e.printStackTrace();
						error.setDescription("Question id is Invalid.");
						custProfileDtls.setError(error);
						custProfileDtls.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Question id is Invalid.");
						return custProfileDtls;
					}
				}
				sourceType = SourceType.PROFILE_FANDOM;
				
			} else if(profileTypeStr.equals("BASIC")) {
				
				/*List<RtfCustProfileAnswers> ansListDb = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndProfileType(customer.getId(), profileTypeStr);
				Map<Integer,RtfCustProfileAnswers> ansMapDb = new HashMap<Integer,RtfCustProfileAnswers>();
				if(ansListDb != null) {
					for (RtfCustProfileAnswers rtfCustProfileAnswers : ansListDb) {
						ansMapDb.put(rtfCustProfileAnswers.getqId(), rtfCustProfileAnswers);
					}
				}*/
				List<RtfCustProfileQuestions> questionsList = QuizDAORegistry.getRtfCustProfileQuestionsDAO().getAllActiveRtfCustProfileQuestionsByProfileType(profileTypeStr.toUpperCase());
				Map<String,RtfCustProfileQuestions> questionsMap = new HashMap<String,RtfCustProfileQuestions>();
				if(questionsList != null) {
					for (RtfCustProfileQuestions profileQuestions : questionsList) {
						questionsMap.put(profileQuestions.getqType(), profileQuestions);
					}
				}
				
				RtfCustProfileQuestions question = null;
				RtfCustProfileAnswers custAns = null;
				String fname = request.getParameter("fname");
				if(!TextUtil.isEmptyOrNull(fname)){
					if(fname.length()> 250) {
						error.setDescription("First Name should be within 250 letters.");
						custProfileDtls.setError(error);
						custProfileDtls.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"First Name should be within 250 letters.");
						return custProfileDtls;
					}
					
					question = questionsMap.get(ProfileQuestionType.FNAME.toString());
					if(question != null) {
						//custAns = ansMapDb.get(question.getId());
						custAns = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(customer.getId(), question.getId());
						if(custAns == null) {
							custAns = new RtfCustProfileAnswers();
							custAns.setAnswer(fname);
							custAns.setqId(question.getId());
							custAns.setCuId(customer.getId());
							custAns.setPoints(question.getPoints());
							custAns.setStatus("ACTIVE");
							custAns.setCreated(new Date());
							totPoints += question.getPoints();
							isNewAnswer = true;
							
						} else {
							custAns.setAnswer(fname);
							custAns.setUpdated(new Date());
						}
						//answersList.add(custAns);
						QuizDAORegistry.getRtfCustProfileAnswersDAO().saveOrUpdate(custAns);
						customer.setCustomerName(fname);
						isCustUpdate = true;
					}
				}
				String lname = request.getParameter("lname");
				if(!TextUtil.isEmptyOrNull(lname)){
					if(lname.length()> 250) {
						error.setDescription("Last Name should be within 250 letters.");
						custProfileDtls.setError(error);
						custProfileDtls.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Last Name should be within 250 letters.");
						return custProfileDtls;
					}
					question = questionsMap.get(ProfileQuestionType.LNAME.toString());
					if(question != null) {
						//custAns = ansMapDb.get(question.getId());
						custAns = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(customer.getId(), question.getId());
						if(custAns == null) {
							custAns = new RtfCustProfileAnswers();
							custAns.setAnswer(lname);
							custAns.setqId(question.getId());
							custAns.setCuId(customer.getId());
							custAns.setPoints(question.getPoints());
							custAns.setStatus("ACTIVE");
							custAns.setCreated(new Date());
							totPoints += question.getPoints();
							isNewAnswer = true;
							
						} else {
							custAns.setAnswer(lname);
							custAns.setUpdated(new Date());
						}
						QuizDAORegistry.getRtfCustProfileAnswersDAO().saveOrUpdate(custAns);
						//answersList.add(custAns);
						customer.setLastName(lname);
						isCustUpdate = true;
					}
				}
				String address = request.getParameter("address");
				if(!TextUtil.isEmptyOrNull(address)){
					if(address.length()> 250) {
						error.setDescription("Address should be within 250 letters.");
						custProfileDtls.setError(error);
						custProfileDtls.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Address should be within 250 letters.");
						return custProfileDtls;
					}
					question = questionsMap.get(ProfileQuestionType.ADDRESS.toString());
					if(question != null) {
						//custAns = ansMapDb.get(question.getId());
						custAns = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(customer.getId(), question.getId());
						if(custAns == null) {
							custAns = new RtfCustProfileAnswers();
							custAns.setAnswer(address);
							custAns.setqId(question.getId());
							custAns.setCuId(customer.getId());
							custAns.setPoints(question.getPoints());
							custAns.setStatus("ACTIVE");
							custAns.setCreated(new Date());
							totPoints += question.getPoints();
							isNewAnswer = true;
							
						} else {
							custAns.setAnswer(address);
							custAns.setUpdated(new Date());
						}
						QuizDAORegistry.getRtfCustProfileAnswersDAO().saveOrUpdate(custAns);
						//answersList.add(custAns);
						//customer.setLastName(lname);
					}
				}
				String dob = request.getParameter("dob");
				if(!TextUtil.isEmptyOrNull(dob)){
					Date dateofBirth = null;
					try {
						dateofBirth = dateFormat.parse(dob);
					} catch(Exception e) {
						error.setDescription("DOB is invalid.");
						custProfileDtls.setError(error);
						custProfileDtls.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"DOB is invalid.");
						return custProfileDtls;
					}
					question = questionsMap.get(ProfileQuestionType.DOB.toString());
					if(question != null) {
						//custAns = ansMapDb.get(question.getId());
						custAns = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(customer.getId(), question.getId());
						if(custAns == null) {
							custAns = new RtfCustProfileAnswers();
							custAns.setAnswer(dob);
							custAns.setqId(question.getId());
							custAns.setCuId(customer.getId());
							custAns.setPoints(question.getPoints());
							custAns.setStatus("ACTIVE");
							custAns.setCreated(new Date());
							totPoints += question.getPoints();
							isNewAnswer = true;
							
						} else {
							custAns.setAnswer(dob);
							custAns.setUpdated(new Date());
						}
						QuizDAORegistry.getRtfCustProfileAnswersDAO().saveOrUpdate(custAns);
						//answersList.add(custAns);
						//customer.setLastName(lname);
					}
				}
				String gender = request.getParameter("gender");
				if(!TextUtil.isEmptyOrNull(gender)) {
					if(!gender.equals("MALE") && !gender.equals("FEMALE") && !gender.equals("TRANSGENDER") && !gender.equals("OTHER")) {
						error.setDescription("Gender is not Valid");
						custProfileDtls.setError(error);
						custProfileDtls.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Gender is not Valid");
						return custProfileDtls;
					}
					question = questionsMap.get(ProfileQuestionType.GENDER.toString());
					if(question != null) {
						//custAns = ansMapDb.get(question.getId());
						custAns = QuizDAORegistry.getRtfCustProfileAnswersDAO().getAllActiveRtfCustProfileAnswersByCustoemrIdAndQuestionId(customer.getId(), question.getId());
						if(custAns == null) {
							custAns = new RtfCustProfileAnswers();
							custAns.setAnswer(gender);
							custAns.setqId(question.getId());
							custAns.setCuId(customer.getId());
							custAns.setPoints(question.getPoints());
							custAns.setStatus("ACTIVE");
							custAns.setCreated(new Date());
							totPoints += question.getPoints();
							isNewAnswer = true;
							
						} else {
							custAns.setAnswer(gender);
							custAns.setUpdated(new Date());
						}
						QuizDAORegistry.getRtfCustProfileAnswersDAO().saveOrUpdate(custAns);
						//answersList.add(custAns);
						//customer.setLastName(lname);
					}
				}
				sourceType = SourceType.PROFILE_BASIC;
			}
			if(isCustUpdate) {
				DAORegistry.getCustomerDAO().update(customer);
				CustomerUtil.removeCustomerUtil(customer.getId());
			}
			QuizDAORegistry.getRtfCustProfileAnswersDAO().saveOrUpdateAll(answersList);
			/*if(totPoints > 0) {
				//CustomerLoyaltyTracking rewardPointsTracking = new CustomerLoyaltyTracking();
				//rewardPointsTracking.setCustomerId(customer.getId());
				//rewardPointsTracking.setRewardType("PROFILEQUEST");
				//rewardPointsTracking.setCreatedDate(new Date());
				//rewardPointsTracking.setCreatedBy("AUTO");
				//rewardPointsTracking.setRtfPoints(totPoints);
				
				DAORegistry.getCustomerDAO().updateQuizCustomerRtfPoints(customer.getId(), totPoints);
				//DAORegistry.getCustomerLoyaltyTrackingDAO().save(rewardPointsTracking);
				
				customer = DAORegistry.getCustomerDAO().get(customer.getId());
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRtfPointsByCustomerId(customer.getId(), customer.getRtfPoints());
				
				 try {
					 CustomerRewardHistorySQLDAO.saveCustomerRewardHistory(customer.getId(), "CR", sourceType, null, 0, 0, 0, totPoints, 0.0, "");
				 }catch(Exception e) {
					 e.printStackTrace();
				 }
				
				CustomerUtil.removeCustomerUtil(customer.getId());
			}*/
			if(isNewAnswer) {
				boolean isCreditEligilbe = false;
				CustomerLoyaltyPointTranx loyaltyPointTraxDb = EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().getCustomerLoyaltyPointTranxByCustIdAndRewardType(customer.getId(), "PROFILEQUEST");
				if(loyaltyPointTraxDb == null) {
					System.out.println("inside trans ");
					CustProfileAnswerCountDtls obj = QuizDAORegistry.getRtfCustProfileAnswersDAO().getRtfCustoemrProfileQuestinsAndAnswersCount(customer.getId());
					if(obj != null) {
						
						if(obj.getAnsCount() >0 && obj.getQuestCount() >0 && 
								obj.getAnsCount().equals(obj.getQuestCount())) {
							isCreditEligilbe = true;
						}
						System.out.println("inside trans obj "+obj.getAnsCount()+":"+ obj.getQuestCount()+":"+isCreditEligilbe+":"+customer.getId());
					}
				} else {
					System.out.println("inside trans else");
				}
				
				if(isCreditEligilbe) {
					try {
						Integer earnLoyaltyPoints = 1000;
						DAORegistry.getCustomerDAO().updateCustomerLoyaltyPoints(customer.getId(), earnLoyaltyPoints);
						customer = DAORegistry.getCustomerDAO().get(customer.getId());
						CassandraDAORegistry.getCassCustomerDAO().updateCustomerLoyaltyPoints(customer.getId(), customer.getLoyaltyPoints());
						
						
						CustomerLoyaltyPointTranx loyaltyPointTrax = new CustomerLoyaltyPointTranx();
						loyaltyPointTrax.setCrBy(customer.getUserId());
						loyaltyPointTrax.setCrDate(new Date());
						loyaltyPointTrax.setCustId(customer.getId());
						loyaltyPointTrax.setEarnPoints(earnLoyaltyPoints);
						loyaltyPointTrax.setSpentPoints(0);
						loyaltyPointTrax.setBeforePoints(0);
						loyaltyPointTrax.setAfterPoints(customer.getLoyaltyPoints());
						loyaltyPointTrax.setStatus("ACTIVE");
						loyaltyPointTrax.setOrderId(-1);
						loyaltyPointTrax.setTranxType("PROFILEQUEST");
						EcommDAORegistry.getCustomerLoyaltyPointTranxDAO().save(loyaltyPointTrax);
					} catch (Exception e) {
						e.printStackTrace();
					}
					CustomerUtil.removeCustomerUtil(customer.getId());
				}
			}
			if(profileTypeStr.equals("BASIC")) {
				CustomerDetails customerDetails = new CustomerDetails();
				CustomerLoyalty customerLoyalty = null;
				List<UserAddress> billingAddress = new ArrayList<UserAddress>();
				List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
				
				customer = DAORegistry.getCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr));
				if(customer != null){
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					
					//Get CustomerAddress by customer id and add it to the response
					//Start			
					List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(customer.getId());
								
					if(userAddress != null){
						for(UserAddress address : userAddress){
							if(address.getAddressType() == AddressType.BILLING_ADDRESS)
								billingAddress.add(address);
							else
								shippingAddress.add(address);					
						}
					}						
					//End
				}
				
				customerDetails.setMessage("");
				customerDetails.setStatus(1);
				customerDetails.setCustomer(customer);
				customer.setBillingAddress(billingAddress);
				if(null !=shippingAddress && shippingAddress.size() >2){
					Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
				}
				customer.setShippingAddress(shippingAddress);
				customerDetails.setCustomerLoyalty(customerLoyalty);
				
				custProfileDtls.setCustomerDetails(customerDetails);
				custProfileDtls.setMessage("Basic Info Updated Successfully.");
			} else if (profileTypeStr.equals("FANDOM")) {
				custProfileDtls.setMessage("Fandom Info Updated Successfully.");
			}
			Integer percentage = QuizDAORegistry.getRtfCustProfileAnswersDAO().getRtfCustProfilePercentage(customer.getId());
			custProfileDtls.setPercentage(percentage);
			
			custProfileDtls.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Success");
			}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Customer Profile Quest.");
			custProfileDtls.setError(error);
			custProfileDtls.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPROFILEQUEST,"Error occured while Updating Customer Profile Quest.");
			return custProfileDtls;
		}
		
		return custProfileDtls;
	
	}
	
	@RequestMapping(value = "/GetCustProfilePercentage", method=RequestMethod.POST)
	public @ResponsePayload CustProfileDtls getCustProfilePercentage(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustProfileDtls custProfileDtls =new CustProfileDtls();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}*/
			
			String customerIdStr = request.getParameter("cuId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEPERC,"Customer Id is mandatory");
				return custProfileDtls;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(, ProductType.REWARDTHEFAN);
			if(customer == null){
				error.setDescription("Customer not valid");
				custProfileDtls.setError(error);
				custProfileDtls.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEPERC,"Customer not valid");
				return custProfileDtls;
			}
			
			Integer percentage = QuizDAORegistry.getRtfCustProfileAnswersDAO().getRtfCustProfilePercentage(customer.getId());
			custProfileDtls.setPercentage(percentage);
			custProfileDtls.setHeaderText("Register as a LOYAL FAN and earn an additional 1000 points.");
			custProfileDtls.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEPERC,"Success");
			}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Information.");
			custProfileDtls.setError(error);
			custProfileDtls.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEPERC,"Error occured while Fetching Customer Information");
			return custProfileDtls;
		}
		
		return custProfileDtls;
	
	}
	@RequestMapping(value = "/UpdateCustPhoneReferrals", method=RequestMethod.POST)
	public @ResponsePayload CustPhoneReferralInfo updateCustPhoneReferrals(HttpServletRequest request,HttpServletResponse response,
			Model model){
		
		CustPhoneReferralInfo custPhoneRefInfo =new CustPhoneReferralInfo();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"You are not authorized");
				return customerDetails;
			}*/
			
			String customerIdStr = request.getParameter("cuId");
			String phoneStr = request.getParameter("phones");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				custPhoneRefInfo.setError(error);
				custPhoneRefInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"Customer Id is mandatory");
				return custPhoneRefInfo;
			}
			
			if(TextUtil.isEmptyOrNull(phoneStr)){
				error.setDescription("Phone number is mandatory.");
				custPhoneRefInfo.setError(error);
				custPhoneRefInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"Phone number is mandatory.");
				return custPhoneRefInfo;
			}
			Customer customer = DAORegistry.getCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr.trim()));//CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
			if(customer == null){
				error.setDescription("Customer not valid");
				custPhoneRefInfo.setError(error);
				custPhoneRefInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"Customer not valid");
				return custPhoneRefInfo;
			}
			Integer rtfPoints=0;
			String rewardType="";
			Double rewardValue = 0.0;
			RtfRewardConfig rewardConfig = QuizDAORegistry.getRtfRewardConfigDAO().getActiveReferralRewardSettingBySourceType(SourceType.PHONEREFERRAL);
			if(rewardConfig != null) {
				if(rewardConfig.getNoOfLives() != null && rewardConfig.getNoOfLives() > 0) {
					rewardValue = rewardConfig.getNoOfLives().doubleValue();
					rewardType = "LIFELINE";
				} else if(rewardConfig.getRtfPoints() != null && rewardConfig.getRtfPoints() > 0) {
					rewardValue = rewardConfig.getRtfPoints().doubleValue();
					rewardType = "POINTS";
				} else if(rewardConfig.getNoOfSfStars() != null && rewardConfig.getNoOfSfStars() > 0) {
					rewardValue = rewardConfig.getNoOfSfStars().doubleValue();
					rewardType = "SUPERFANSTAR";
				} else if(rewardConfig.getNoOfMagicWands() != null && rewardConfig.getNoOfMagicWands() > 0) {
					rewardValue = rewardConfig.getNoOfMagicWands().doubleValue();
					rewardType = "MAGICWAND";
				} else if(rewardConfig.getRewardDollar() != null && rewardConfig.getRewardDollar() > 0) {
					rewardValue = rewardConfig.getRewardDollar();
					rewardType = "REWARD";
				}
			}
			Integer quantity = 0;
			Date now = new Date();
			List<RtfCustPhoneReferrals> phoneReferrals = new ArrayList<RtfCustPhoneReferrals>();
			List<String> referredPhones = QuizDAORegistry.getRtfCustPhoneReferralsDAO().getAllReferredPhoneNosByCustomerId(customer.getId());
			
			RtfCustPhoneReferrals phoneReferral = null;
			String[] phoneArr = phoneStr.split(",");
			for (String phone : phoneArr) {
				if(referredPhones == null || !referredPhones.contains(phone)) {
					phoneReferral = new RtfCustPhoneReferrals();
					phoneReferral.setCuId(customer.getId());
					phoneReferral.setPhone(phone);
					phoneReferral.setCreated(now);
					phoneReferral.setRewardType(rewardType);
					phoneReferral.setRewardValue(rewardValue);
					
					phoneReferrals.add(phoneReferral);
					quantity++;
				}
			}
			QuizDAORegistry.getRtfCustPhoneReferralsDAO().saveAll(phoneReferrals);
			
			if(rewardConfig != null && quantity > 0) {
				RtfRewardCreditService.creditRewardsWithQty(rewardConfig, customer.getId(), customer, quantity);	
			}
			
				
			custPhoneRefInfo.setMessage("Phone Referrals Updated Successfully.");
			custPhoneRefInfo.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"Success");
			}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Updating Customer Phone Referrals.");
			custPhoneRefInfo.setError(error);
			custPhoneRefInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTPHONEREFERRALS,"Error occured while Updating Customer Phone Referrals.");
			return custPhoneRefInfo;
		}
		
		return custPhoneRefInfo;
	
	}
	
	@RequestMapping(value = "/GetCustPhoneReferrals", method=RequestMethod.POST)
	public @ResponsePayload CustPhoneReferralInfo getCustPhoneReferrals(HttpServletRequest request,HttpServletResponse response,
			Model model){
		
		CustPhoneReferralInfo custPhoneRefInfo =new CustPhoneReferralInfo();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"You are not authorized");
				return customerDetails;
			}*/
			
			String customerIdStr = request.getParameter("cuId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				custPhoneRefInfo.setError(error);
				custPhoneRefInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"Customer Id is mandatory");
				return custPhoneRefInfo;
			}
			Customer customer = DAORegistry.getCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr.trim()));//CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
			if(customer == null){
				error.setDescription("Customer not valid");
				custPhoneRefInfo.setError(error);
				custPhoneRefInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"Customer not valid");
				return custPhoneRefInfo;
			}
			
			List<String> referredPhones = QuizDAORegistry.getRtfCustPhoneReferralsDAO().getAllReferredPhoneNosByCustomerId(customer.getId());
			custPhoneRefInfo.setPhones(referredPhones);
			
			//custPhoneRefInfo.setMessage("Phone Referrals Updated Successfully.");
			custPhoneRefInfo.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"Success");
			}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Getting cust phone referrals");
			custPhoneRefInfo.setError(error);
			custPhoneRefInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPHONEREFERRALS,"Error occured while Getting cust phone referrals.");
			return custPhoneRefInfo;
		}
		
		return custPhoneRefInfo;
	
	}
	@RequestMapping(value = "/BlockCustomerComment", method=RequestMethod.POST)
	public @ResponsePayload CommonRespInfo blockCustomerComment(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CommonRespInfo custProfileDtls =new CommonRespInfo();
		CassError error = new CassError();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTPROFILEINFO,"You are not authorized");
				return customerDetails;
			}*/
			
			String customerIdStr = request.getParameter("cuId");
			String blockSource = request.getParameter("source");
			String sourceIdStr = request.getParameter("sourceId");
			String userName = request.getParameter("userName");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"Customer Id is mandatory");
				return custProfileDtls;
			}
			if(TextUtil.isEmptyOrNull(blockSource)){
				error.setDesc("Block Source is mandatory");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"Block Source is mandatory");
				return custProfileDtls;
			}
			if(TextUtil.isEmptyOrNull(sourceIdStr)){
				error.setDesc("Source Id is mandatory");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"Source Id is mandatory");
				return custProfileDtls;
			}
			if(TextUtil.isEmptyOrNull(userName)){
				error.setDesc("User Name is mandatory");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"User Name is mandatory");
				return custProfileDtls;
			}
			
			Customer customer = DAORegistry.getCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null){
				error.setDesc("Customer not valid");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"Customer not valid");
				return custProfileDtls;
			}
			CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customer.getId());
			if(cassCustomer == null) {
				error.setDesc("Customer not Exist in Cassandra.");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"Customer not Exist in Cassandra.");
				return custProfileDtls;
			}
			if((customer.getIsBlocked() != null && customer.getIsBlocked()) && cassCustomer.getIsBlocked() != null && cassCustomer.getIsBlocked()) {
				error.setDesc("Customer Already Blocked");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"Customer Already Blocked");
				return custProfileDtls;
			}
			
			CassandraDAORegistry.getCassCustomerDAO().updateCutomerBlockedStatus(Boolean.TRUE, customer.getId());
			DAORegistry.getCustomerDAO().updateCustBlockedStatus(Boolean.TRUE, customer.getId());
			
			custProfileDtls.setSts(1);
			custProfileDtls.setMsg("Customer Blocked Successfully.");
			String actionRes = "Success:cuId"+customerIdStr+":sour:"+blockSource+":sId:"+sourceIdStr+":usr:"+userName;
			TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,actionRes);
			}catch(Exception e){
			e.printStackTrace();
			error.setDesc("Error occured while Blocking Customer.");
			custProfileDtls.setErr(error);
			custProfileDtls.setSts(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.BLOCKCUSTOMERCOMMENT,"Error occured while Blocking Customer.");
			return custProfileDtls;
		}
		
		return custProfileDtls;
	
	}
	@RequestMapping(value = "/UpdateCustomerUnBlock", method=RequestMethod.POST)
	public @ResponsePayload CommonRespInfo updateCustomerUnBlock(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CommonRespInfo custProfileDtls =new CommonRespInfo();
		CassError error = new CassError();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"You are not authorized");
				return customerDetails;
			}*/
			
			String customerIdStr = request.getParameter("cuId");
			String userName = request.getParameter("userName");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDesc("Customer Id is mandatory");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"Customer Id is mandatory");
				return custProfileDtls;
			}
			if(TextUtil.isEmptyOrNull(userName)){
				error.setDesc("User Name is mandatory");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"User Name is mandatory");
				return custProfileDtls;
			}
			
			Customer customer = DAORegistry.getCustomerDAO().getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null){
				error.setDesc("Customer not valid");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"Customer not valid");
				return custProfileDtls;
			}
			CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customer.getId());
			if(cassCustomer == null) {
				error.setDesc("Customer not Exist in Cassandra.");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"Customer not Exist in Cassandra.");
				return custProfileDtls;
			}
			if((customer.getIsBlocked() != null && !customer.getIsBlocked())) {
				error.setDesc("Customer Already UnBlocked");
				custProfileDtls.setErr(error);
				custProfileDtls.setSts(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"Customer Already UnBlocked");
				return custProfileDtls;
			}
			
			CassandraDAORegistry.getCassCustomerDAO().updateCutomerBlockedStatus(Boolean.FALSE, customer.getId());
			DAORegistry.getCustomerDAO().updateCustBlockedStatus(Boolean.FALSE, customer.getId());
			
			custProfileDtls.setSts(1);
			custProfileDtls.setMsg("Customer Blocked Successfully.");
			String actionRes = "Success:cuId"+customerIdStr+":usr:"+userName;
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,actionRes);
			}catch(Exception e){
			e.printStackTrace();
			error.setDesc("Error occured while Blocking Customer.");
			custProfileDtls.setErr(error);
			custProfileDtls.setSts(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATECUSTOEMRUNBLOCK,"Error occured while Blocking Customer.");
			return custProfileDtls;
		}
		
		return custProfileDtls;
	
	}
	
	@RequestMapping(value="/SixRewardTheFanSignUp",method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails sixRewardTheFanSignUp(HttpServletRequest request, Model model,HttpServletResponse response){
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
				return customerDetails;
			}
			
			String productTypeStr = request.getParameter("productType");
			String signUpTypeStr = request.getParameter("signUpType");
			String name = request.getParameter("name");
			String lastName =request.getParameter("lastName");
			//String referralCode = request.getParameter("referralCode");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String platForm = request.getParameter("platForm");
			String deviceId = request.getParameter("deviceId");
			String notificationRegId = request.getParameter("notificationRegId");
			String socialAccountId = request.getParameter("socialAccountId");
			String fbAccessToken = request.getParameter("fbAccessToken");
			String loginIp = request.getParameter("loginIp");
			String customerPromoAlert = request.getParameter("customerPromoAlert");
			String phone = request.getParameter("phone");
			String ctyPhCode = request.getParameter("ctyPhCode");
			String userId = request.getParameter("userId");
			

			if(TextUtil.isEmptyOrNull(ctyPhCode)){
				 ctyPhCode = CountryUtil.defaultCountryPhoneCode;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Product Type is mandatory");
				return customerDetails;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid product type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(signUpTypeStr)){
				error.setDescription("SignUp Type is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"SignUp Type is mandatory");
				return customerDetails;
			}
			SignupType signupType=null;
			if(!TextUtil.isEmptyOrNull(signUpTypeStr)){
				try{
					signupType = SignupType.valueOf(signUpTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid signup type");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid signup type");
					return customerDetails;
				}
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Application Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please send valid application platform");
					return customerDetails;
				}
			}
			if(platForm.contains("ANDROID")||platForm.contains("IOS")){
				/*if(TextUtil.isEmptyOrNull(notificationRegId)){
				error.setDescription("Notification RegistrationID is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Notification RegistrationID is mandatory");
				return customerDetails;
			}*/	
				loginIp = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
			}
			
			if(signupType.equals(SignupType.FACEBOOK) || signupType.equals(SignupType.GOOGLE)){
				error.setDescription("We no longer support login/Signup via FaceBook and Google.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"We no longer support login/Signup via FaceBook and Google.");
				return customerDetails;
			}
			//if(signupType.equals(SignupType.FACEBOOK) || signupType.equals(SignupType.GOOGLE)){
				
			//}else{
				
				if(TextUtil.isEmptyOrNull(userId)){
					error.setDescription("Please enter valid user id.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter valid user id.");
					return customerDetails;
					//userId = "";
				} else {
					userId = userId.trim();
					if(userId.length()< 5 || userId.length() > 12) {
						error.setDescription("Please select user id within 5 to 12 letters or numbers.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please select user id within 5 to 12 letters or numbers.");
						return customerDetails;
					}
					if(!userId.matches("[a-zA-Z0-9]*")) {
						error.setDescription("Please select user id with letters and/or numbers only.");
						customerDetails.setError(error);
						
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please select user id with letters and/or numbers only.");
						return customerDetails;
					}
					Customer tempCustomer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
					RtfPromotionalEarnLifeOffers rtfReferral = DAORegistry.getRtfPromotionalEarnLifeOffersDAO().getRtfPromotionEarnLifeOfferByPromoCode(userId);
					if(tempCustomer != null || rtfReferral != null) {
							error.setDescription("Whoops! This userId is already signed up. Please choose a different UserId.");
							customerDetails.setError(error);
							customerDetails.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Whoops! This userId is already signed up. Please choose a different UserId.");
							return customerDetails;
					}
				}
			//}		
			
			/*if(TextUtil.isEmptyOrNull(lastName)){
				error.setDescription("Customer LastName is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Customer LastName is mandatory");
				return customerDetails;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(name)){
				error.setDescription("Please enter your First Name");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your First Name");
				return customerDetails;
			}*/
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Please enter your Phone No");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Phone no");
				return customerDetails;
			}
			phone =  QuizSettings.validatePhoneNo(phone);
			if(phone.length() < 10){
				error.setDescription("Please enter valid 10 digit phone number.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter valid 10 digit phone number.");
				return customerDetails;
			}
			
			if(!TextUtil.isEmptyOrNull(customerPromoAlert) && customerPromoAlert.equals("YES")){
				customerDetails.setShowPromoAert(true);
				customerDetails.setPromoAlertMessage(TextUtil.getCustomerPromoAlertText());
			}
			
			String finalPassword = password;
			CustomerLoyalty customerLoyalty = null;
			Customer validateCustomer = null;
			CustomerLoginHistory loginHistory = null;
			CustomerDeviceDetails customerDeviceDetails = DAORegistry.getCustomerDeviceDetailsDAO().getActiveDeviceDetailsByDeviceId(deviceId);
			
			if(signupType.equals(SignupType.FACEBOOK)){ 
				
			}else{
				if(TextUtil.isEmptyOrNull(email)){
					error.setDescription("Please enter your Email");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please enter your Email");
					return customerDetails;
				}
				
				if(TextUtil.isEmptyOrNull(password)){
					error.setDescription("Please choose a Password");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Please choose a Password");
					return customerDetails;
				}
				finalPassword = password;
				
				validateCustomer = DAORegistry.getCustomerDAO().getCustomerByEmail(email);
				
				//executed only when customer is already registered with us from RTW and RTW2 database
				if(validateCustomer != null && (validateCustomer.getProductType().equals(ProductType.RTW) || 
						validateCustomer.getProductType().equals(ProductType.RTW2) || validateCustomer.getProductType().equals(ProductType.TIXCITY))){
					validateCustomer.setProductType(ProductType.REWARDTHEFAN);
					validateCustomer.setSignupType(SignupType.REWARDTHEFAN);
					validateCustomer.setSignupDate(new Date());
					validateCustomer.setIsEmailed(true);
					validateCustomer.setApplicationPlatForm(applicationPlatForm);
					validateCustomer.setCustomerName(null!=name?name:"");
					validateCustomer.setLastName(null!=lastName?lastName:"");
					/*Property property = DAORegistry.getPropertyDAO().get("rtf.customer.referrer.code");
					String preFix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
					Long referrerNumber =  Long.valueOf(property.getValue());
					referrerNumber++;  
					validateCustomer.setReferrerCode(ReferralCodeGenerator.generateCustomerReferalCode(referrerNumber,preFix));
					property.setValue(String.valueOf(referrerNumber));
					DAORegistry.getPropertyDAO().saveOrUpdate(property);*/
					validateCustomer.setReferrerCode(validateCustomer.getEmail());
					validateCustomer.setEncryptPassword(finalPassword);
					validateCustomer.setBrokerId(1001);
					validateCustomer.setIsClient(true);
					validateCustomer.setIsOtpVerified(true);
					validateCustomer.setUserId(userId);
					if(phone != null && !phone.isEmpty()) {
						validateCustomer.setPhone(phone);
						validateCustomer.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
					}
					DAORegistry.getCustomerDAO().saveOrUpdate(validateCustomer);
					
					if(validateCustomer.getCustImagePath() == null) {
						String fileName = URLUtil.getCustomerDefaultprofilePicName(validateCustomer.getId());
						validateCustomer.setCustImagePath(fileName);
						DAORegistry.getCustomerDAO().updateCustImagePath(fileName, validateCustomer.getId());
						//CustomerUtil.updatedCustomerUtil(customer);
					}
					/*Add new Customer to Customer Util - Begins*/
					try{
						CustomerUtil.updatedCustomerUtil(validateCustomer);
					}catch(Exception e){
						e.printStackTrace();
					}
					/*Add new Customer to Customer Util - Ends*/
					try{
						//To Delete RTW, RTW2, Tixcity Customer Address - Added By Ulaganathan
						DAORegistry.getUserAddressDAO().deleteCustomerExistingAddresses(validateCustomer.getId());
					}catch(Exception e){
						e.printStackTrace();
					}
					
					
					String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(validateCustomer.getId()+""+email.split("@")[0]);
					
					Calendar calendar = new GregorianCalendar();
					calendar.add(Calendar.YEAR, 1);
					
					RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
					custPromoOffer.setCreatedDate(new Date());
					custPromoOffer.setCustomerId(validateCustomer.getId());
					custPromoOffer.setDiscount(25.00);
					custPromoOffer.setStartDate(new Date());
					custPromoOffer.setEndDate(calendar.getTime());
					custPromoOffer.setFlatOfferOrderThreshold(1.00);
					custPromoOffer.setIsFlatDiscount(false);
					custPromoOffer.setMaxOrders(1);
					custPromoOffer.setModifiedDate(new Date());
					custPromoOffer.setPromoCode(custPromoCode);
					custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
					custPromoOffer.setNoOfOrders(0);
					custPromoOffer.setStatus("ENABLED");
					
					boolean isEmailSent = true;
					String toEmail = validateCustomer.getEmail();
					try{
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("promotionalCode", custPromoCode);
						mailMap.put("userName",validateCustomer.getEmail());
						/*String userNameHtml = "<span style='color: #F06931;'> "+validateCustomer.getEmail()+" ";
						if(null != validateCustomer.getUserId() && !validateCustomer.getUserId().isEmpty()){
							userNameHtml += "</span> or <span style='color: #F06931;'> "+validateCustomer.getUserId()+" </span>";
						}else{
							userNameHtml += "</span>";
						}
						mailMap.put("userNameHtml",userNameHtml);*/
						
						//inline(image in mail body) image add code
						MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, validateCustomer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
						
					}catch(Exception e1){
						e1.printStackTrace();
						isEmailSent = false;
						toEmail = "";
					}
					custPromoOffer.setIsEmailSent(isEmailSent);
					custPromoOffer.setToEmail(toEmail);
					//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
					
					
					//List<UserAddress> userAddress = DAORegistry.getUserAddressDAO().getUserAddressByCustomerId(validateCustomer.getId());
					List<UserAddress> billingAddress = new ArrayList<UserAddress>();
					List<UserAddress> shippingAddress = new ArrayList<UserAddress>();
					
					//if(userAddress != null){
					//	for(UserAddress address : userAddress){
					//		if(address.getAddressType() == AddressType.BILLING_ADDRESS)
					//			billingAddress.add(address);
					//		else
					//			shippingAddress.add(address);					
					//	}
					//}	
					
					customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					
					if(null != customerLoyalty){
						
					}else{
						customerLoyalty = new CustomerLoyalty();
						customerLoyalty.setCustomerId(validateCustomer.getId());
						customerLoyalty.setActivePointsAsDouble(0.00);
						customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
						customerLoyalty.setLatestSpentPointsAsDouble(0.00);
						customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
						customerLoyalty.setTotalSpentPointsAsDouble(0.00);
						customerLoyalty.setLastUpdate(new Date());
						customerLoyalty.setLastNotifiedTime(new Date());
						customerLoyalty.setPendingPointsAsDouble(0.00);
						customerLoyalty.setTotalAffiliateReferralDollars(0.00);
						customerLoyalty.setLastAffiliateReferralDollars(0.00);
						//Save Customer Loyalty Information
						DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
					}
					
					
					//customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(validateCustomer.getId());
					customerDetails.setCustomerLoyalty(customerLoyalty);
					customerDetails.setCustomer(validateCustomer);
					customerDetails.getCustomer().setBillingAddress(billingAddress);
					if(null !=shippingAddress && shippingAddress.size() >2){
						Collections.sort(shippingAddress,CityUtil.sortShippingAddressByUpdatedTime);
					}
					customerDetails.getCustomer().setShippingAddress(shippingAddress);
					
					try {
						CassCustomerUtil.addCustomer(validateCustomer, customerLoyalty);
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					customerDetails.setStatus(1);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
					return customerDetails;
				}else{
					if(null != validateCustomer){
						error.setDescription("Whoops! This email is already signed up. Please choose a different Email.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Whoops! This email is already signed up. Please choose a different Email.");
						return customerDetails;
					}
				}
			}
			
			Customer customer = new Customer();
			
			//11-21-2018 3:22 PM - Give 1 life to all new registration. Given by Mitul - Done By Ulaganathan
			customer.setQuizCustomerLives(1);
			
			customer.setBrokerId(1001);
			customer.setIsClient(true);
			customer.setProductType(productType);
			customer.setSignupType(signupType);
			customer.setCustomerName(null!=name?name:"");
			customer.setLastName(null!=lastName?lastName:"");
			//customer.setReferralCode(null!=referralCode?referralCode:"");
			customer.setEmail(email);
			customer.setEncryptPassword(finalPassword);
			customer.setUserName(email);
			customer.setSignupDate(new Date());
			customer.setCustomerType(CustomerType.WEB_CUSTOMER);
			customer.setApplicationPlatForm(applicationPlatForm);
			customer.setDeviceId(null != deviceId?deviceId:"");
			customer.setNotificationRegId(null!=notificationRegId?notificationRegId:"");
			customer.setSocialAccountId(null != socialAccountId?socialAccountId:"");
			
			customer.setUserId(userId);
			customer.setPhone((null != phone && !phone.equals(""))?phone:null);
			customer.setCtyPhCode(null != ctyPhCode && !ctyPhCode.isEmpty()?ctyPhCode:CountryUtil.defaultCountryPhoneCode);
			customer.setIsOtpVerified(true);
			 
			customer.setReferrerCode(email);
			//Save Customer
			DAORegistry.getCustomerDAO().saveOrUpdate(customer);
			
			//property.setValue(String.valueOf(referrerNumber));
			
			//Save Customer Referral Code Auto Number
			//DAORegistry.getPropertyDAO().saveOrUpdate(property);
			
			if(customer.getCustImagePath() == null) {
				String fileName = URLUtil.getCustomerDefaultprofilePicName(customer.getId());
				customer.setCustImagePath(fileName);
				DAORegistry.getCustomerDAO().updateCustImagePath(fileName, customer.getId());
				//CustomerUtil.updatedCustomerUtil(customer);
			}
			/*Add new Customer to Customer Util - Begins*/
			try{
				CustomerUtil.updatedCustomerUtil(customer);
			}catch(Exception e){
				e.printStackTrace();
			}
			/*Add new Customer to Customer Util - Ends*/
			
			
			customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
			
			if(null != customerLoyalty){
				
			}else{
				customerLoyalty = new CustomerLoyalty();
				customerLoyalty.setCustomerId(customer.getId());
				customerLoyalty.setActivePointsAsDouble(0.00);
				customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
				customerLoyalty.setLatestSpentPointsAsDouble(0.00);
				customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
				customerLoyalty.setTotalSpentPointsAsDouble(0.00);
				customerLoyalty.setLastUpdate(new Date());
				customerLoyalty.setLastNotifiedTime(new Date());
				customerLoyalty.setPendingPointsAsDouble(0.00);
				customerLoyalty.setTotalAffiliateReferralDollars(0.00);
				customerLoyalty.setLastAffiliateReferralDollars(0.00);
				//Save Customer Loyalty Information
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			}
			
			List<CustomerLoginHistory> loginHistories = DAORegistry.getCustomerLoginHistoryDAO().getAllLoginHistoryByCustomerId(customer.getId());
			if(null != loginHistories && !loginHistories.isEmpty() && loginHistories.size() >= 1){
				customer.setFirstTimeLogin("No");
			}else{
				customer.setFirstTimeLogin("Yes");
			}
			loginHistory = new CustomerLoginHistory();
			loginHistory.setCustomerId(customer.getId());
			loginHistory.setApplicationPlatForm(applicationPlatForm);
			loginHistory.setLastLoginTime(new Date());
			loginHistory.setLoginType(signupType);
			loginHistory.setDeviceId(deviceId);
			loginHistory.setNotificationRegId(notificationRegId);
			loginHistory.setLoginIp(loginIp);
			loginHistory.setSocialAccessToken(fbAccessToken);	
			
			//Save Customer Login History
			DAORegistry.getCustomerLoginHistoryDAO().saveOrUpdate(loginHistory);
			
			if((applicationPlatForm.equals(ApplicationPlatForm.IOS) || 
					applicationPlatForm.equals(ApplicationPlatForm.ANDROID))){
				
				if(null == customerDeviceDetails){
					customerDeviceDetails = new CustomerDeviceDetails();
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setApplicationPlatForm(applicationPlatForm);
					customerDeviceDetails.setCreatedDate(new Date());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setStatus("ACTIVE");
					customerDeviceDetails.setDeviceId(deviceId);
					customerDeviceDetails.setNotificationRegId(notificationRegId);
					customerDeviceDetails.setLoginIp(loginIp);
				}else{
					customerDeviceDetails.setCustomerId(customer.getId());
					customerDeviceDetails.setLastUpdated(new Date());
					customerDeviceDetails.setNotificationRegId(notificationRegId);
				}
				//Save Customer Device Details
				DAORegistry.getCustomerDeviceDetailsDAO().saveOrUpdate(customerDeviceDetails);
			
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Begins*/
				try{
					CustomerDeviceDetailsUtils.updatedCustomerDeviceDetailsToUtil(customerDeviceDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				/*Add Customer Latest Device Details into CustomerDeviceUtils -  Ends*/
			}
			
			String custPromoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+email.split("@")[0]);
			
			Calendar calendar = new GregorianCalendar();
			calendar.add(Calendar.YEAR, 1);
			
			RTFCustomerPromotionalOffer custPromoOffer = new RTFCustomerPromotionalOffer();
			custPromoOffer.setCreatedDate(new Date());
			custPromoOffer.setCustomerId(customer.getId());
			custPromoOffer.setDiscount(25.00);
			custPromoOffer.setStartDate(new Date());
			custPromoOffer.setEndDate(calendar.getTime());
			custPromoOffer.setFlatOfferOrderThreshold(1.00);
			custPromoOffer.setIsFlatDiscount(false);
			custPromoOffer.setMaxOrders(1);
			custPromoOffer.setModifiedDate(new Date());
			custPromoOffer.setPromoCode(custPromoCode);
			custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
			custPromoOffer.setNoOfOrders(0);
			custPromoOffer.setStatus("ENABLED");
			
			boolean isEmailSent = true;
			String toEmail = customer.getEmail();
			
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("promotionalCode", custPromoCode);
			mailMap.put("userName",customer.getEmail());
			
			/*String userNameHtml = "<span style='color: #F06931;'> "+customer.getEmail()+" ";
			if(null != customer.getUserId() && !customer.getUserId().isEmpty()){
				userNameHtml += "</span> or <span style='color: #F06931;'> "+customer.getUserId()+" </span>";
			}else{
				userNameHtml += "</span>";
			}
			mailMap.put("userNameHtml",userNameHtml);*/
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
			
			try{
				switch (productType) {
					case REWARDTHEFAN:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
					    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,null);
						break;
					
					case ZONETICKETS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case MINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case LASTROWMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.bccEmails, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
						
					case VIPMINICATS:
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null, URLUtil.fromEmail, "You have successfully registered with ZoneTickets.com.",
					    		"mail-zonetickets-customer-registration.txt", mailMap, "text/html", null);
						break;
	
					default:
						break;
				}
			
			}catch(Exception e){
				e.printStackTrace();
				isEmailSent = false;
				toEmail = "";
			}
			
			custPromoOffer.setIsEmailSent(isEmailSent);
			custPromoOffer.setToEmail(toEmail);
			//DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
			
			/*boolean customerPic = false;
			String profilePicPrefix =URLUtil.DP_PRFEIX_CODE;
			if(customer != null ){
				Map<String, Boolean> map = readCustomerPic(customer.getId());
				String getExt = null;
				for(Map.Entry<String, Boolean> entry: map.entrySet()){
					getExt = entry.getKey();
					customerPic = entry.getValue();
				}
				if(customerPic){
					customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURL(profilePicPrefix, customer.getId(), getExt,applicationPlatForm));
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERPROFILEPIC,"Success");
				}else{
					customerDetails.setCustomerProfilePicWebView(null);
				}
			}*/
			
			if(customer.getCustImagePath() != null) {
				customerDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
			} else {
				customerDetails.setCustomerProfilePicWebView(null);
			}
			
			try {
				CassCustomerUtil.addCustomer(customer, customerLoyalty);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			customerDetails.setCustomerLoyalty(customerLoyalty);
			customerDetails.setCustomer(customer);
			customerDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Success");
			return customerDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while creating customer.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"Error occured while creating customer");
			return customerDetails;
		}
	}
	
}	