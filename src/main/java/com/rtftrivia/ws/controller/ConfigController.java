package com.rtftrivia.ws.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.LoadZipCodes;
import com.zonesws.webservices.jobs.TeamCardImageUtil;
import com.zonesws.webservices.jobs.WebServiceUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.EventArtistUtil;
import com.zonesws.webservices.utils.RTFHomeCardsCacheUtil;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.EventResult;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/LoadValues"})
public class ConfigController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(ConfigController.class);
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	 
	@RequestMapping(value="/LoadValues",method=RequestMethod.GET)
	public @ResponsePayload EventResult loadValues(HttpServletRequest request, Model model,HttpServletResponse response){
		EventResult eventList =new EventResult();
		Error error = new Error();
		
		 
		try{
			EventArtistUtil.init();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		 
		try{
			WebServiceUtil.init();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("WebServiceUtil End : "+new Date());
		
		try{
			CustomerUtil.init();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("CustomerUtil End : "+new Date());
		
		System.out.println("TeamCardImageUtil Begin : "+new Date());
		try{
			TeamCardImageUtil.init();	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("TeamCardImageUtil End : "+new Date());
		
		
		System.out.println("================================================");
		
		System.out.println("ApiConfigUtil Begin : "+new Date());
		ApiConfigUtil.init();
		System.out.println("ApiConfigUtil End : "+new Date());
		
		
		System.out.println("================================================");
		  
		LoadZipCodes.init();
		 
		System.out.println("UpcomingEventUtil End : "+new Date());
		RTFHomeCardsCacheUtil homeCardsCacheUtil = new RTFHomeCardsCacheUtil();
		homeCardsCacheUtil.init();
		
		error.setDescription("Done");
		eventList.setError(error);
		eventList.setStatus(0);
		return eventList;
	}
}	