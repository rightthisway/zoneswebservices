package com.rtftrivia.ws.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerFriend;
import com.rtfquiz.webservices.data.QuizCustomerInvitedFriends;
import com.rtfquiz.webservices.enums.FriendStatus;
import com.rtfquiz.webservices.enums.QuizMessageTextType;
import com.rtfquiz.webservices.utils.QuizContestUtil;
import com.rtfquiz.webservices.utils.list.CustomerFriendDetails;
import com.rtfquiz.webservices.utils.list.CustomerPhoneSearchInfo;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.rtfquiz.webservices.utils.list.CustomerStatsDetails;
import com.rtfquiz.webservices.utils.list.QuizContestFriendRequestDetails;
import com.rtfquiz.webservices.utils.list.QuizCustomerSearchInfo;
import com.rtfquiz.webservices.utils.list.QuizCustomerStatsInfo;
import com.rtfquiz.webservices.utils.list.QuizDiscoverPageInfo;
import com.rtfquiz.webservices.utils.list.QuizInviteFriendsInfo;
import com.rtfquiz.webservices.utils.list.QuizMessageTextsInfo;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/discover/GetDiscoverPageData","/discover/GetCustomerPhoneSearch","/discover/QuizCustomersAutoSearch",
	"/discover/GetCustomerStatsDetails","/discover/SendFriendRequest","/discover/AcceptRejectFriendRequest"})
public class DiscoverFriendsController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(DiscoverFriendsController.class);
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	private static Integer apiHitCount = 0;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	 
	@RequestMapping(value = "/discover/GetDiscoverPageData", method=RequestMethod.POST)
	public @ResponsePayload QuizDiscoverPageInfo getDiscoverPageData(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizDiscoverPageInfo quizDiscoverPageInfo = new QuizDiscoverPageInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizDiscoverPageInfo.setError(authError);
				quizDiscoverPageInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETDISCOVERPAGEDATA,authError.getDescription());
				return quizDiscoverPageInfo;
			}
			
			String customerIdStr = request.getParameter("customerId");
			//String contestIdStr = request.getParameter("contestId");
			//String friendCustomerIdStr = request.getParameter("friendCustomerId");
			//String questionNoStr = request.getParameter("questionNo");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizDiscoverPageInfo.setError(error);
				quizDiscoverPageInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETDISCOVERPAGEDATA,"Customer Id is mandatory");
				return quizDiscoverPageInfo;
			}
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Question No is mandatory");
				quizDiscoverPageInfo.setError(error);
				quizDiscoverPageInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETDISCOVERPAGEDATA,"Question No is mandatory");
				return quizDiscoverPageInfo;
			}*/
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizDiscoverPageInfo.setError(error);
				quizDiscoverPageInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETDISCOVERPAGEDATA,"Customer Id is Invalid");
				return quizDiscoverPageInfo;
			}
			
			List<CustomerFriendDetails> customerFriends = QuizContestUtil.getCustomerFriendsList(customer.getId());
			List<CustomerFriendDetails> friendsList = new ArrayList<CustomerFriendDetails>();
			List<CustomerFriendDetails> requestedList = new ArrayList<CustomerFriendDetails>();
			if(customerFriends != null) {
				for (CustomerFriendDetails customerFriendDetails : customerFriends) {
					if(customerFriendDetails.getStatus().equals("Friend")) {
						friendsList.add(customerFriendDetails);
					} else {
						requestedList.add(customerFriendDetails);
					}
				}				
			}
			quizDiscoverPageInfo.setRequestsList(requestedList);
			quizDiscoverPageInfo.setCustomerFriends(friendsList);
			
			quizDiscoverPageInfo.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETDISCOVERPAGEDATA,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Discover Page Details.");
			quizDiscoverPageInfo.setError(error);
			quizDiscoverPageInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETDISCOVERPAGEDATA,"Error occured while Fetching Discover Page Details.");
			return quizDiscoverPageInfo;
		}
		log.info("QUIZ DISC PAGE : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizDiscoverPageInfo;
	
	}
	
	@RequestMapping(value = "/discover/GetCustomerPhoneSearch", method=RequestMethod.POST)
	public @ResponsePayload CustomerPhoneSearchInfo GetCustomerPhoneSearch(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerPhoneSearchInfo customerPhoneSearch = new CustomerPhoneSearchInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				customerPhoneSearch.setError(authError);
				customerPhoneSearch.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERPHONESEARCH,authError.getDescription());
				return customerPhoneSearch;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String phone = request.getParameter("phone");
			//String contestIdStr = request.getParameter("contestId");
			//String friendCustomerIdStr = request.getParameter("friendCustomerId");
			//String questionNoStr = request.getParameter("questionNo");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				customerPhoneSearch.setError(error);
				customerPhoneSearch.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERPHONESEARCH,"Customer Id is mandatory");
				return customerPhoneSearch;
			}
			
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Phone No is mandatory");
				customerPhoneSearch.setError(error);
				customerPhoneSearch.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERPHONESEARCH,"Phone No is mandatory");
				return customerPhoneSearch;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				customerPhoneSearch.setError(error);
				customerPhoneSearch.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERPHONESEARCH,"Customer Id is Invalid");
				return customerPhoneSearch;
			}
			
			String phoneStr="";
			phone = phone.replaceAll("'", "");
			String[] phoneArr=phone.split(",");
			for (int i = 0; i < phoneArr.length; i++) {
				phoneStr = phoneStr + ",'" + phoneArr[i]+"'";
			}
			if(phoneStr.length() > 0) {
				phoneStr = phoneStr.substring(1);
			}
			
			List<CustomerSearchDetails> custoemrSearchResult = DAORegistry.getQueryManagerDAO().getDisCoverPageCustomersByPhoneSearch(customer.getId(), phoneStr);
			customerPhoneSearch.setCustomerResult(custoemrSearchResult);
			
			customerPhoneSearch.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERPHONESEARCH,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Phone Search Details.");
			customerPhoneSearch.setError(error);
			customerPhoneSearch.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERPHONESEARCH,"Error occured while Fetching Customer Phone Search Details.");
			return customerPhoneSearch;
		}
		log.info("QUIZ PHONE SEARCH : "+request.getParameter("customerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return customerPhoneSearch;
	
	}
	
	
	
	@RequestMapping(value = "/discover/QuizCustInviteFriends", method=RequestMethod.POST)
	public @ResponsePayload QuizInviteFriendsInfo quizCustInviteFriends(HttpServletRequest request,HttpServletResponse response){
		QuizInviteFriendsInfo inviteFriendInfo =new QuizInviteFriendsInfo();
		Error error = new Error();
		Date start = new Date();
		try {

			String customerIdStr = request.getParameter("customerId");
			String phone = request.getParameter("phone");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				inviteFriendInfo.setError(error);
				inviteFriendInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTINVITEFRIENDS,"Customer Id is mandatory");
				return inviteFriendInfo;
			}
			if(TextUtil.isEmptyOrNull(phone)){
				error.setDescription("Phone No is Mendatory");
				inviteFriendInfo.setError(error);
				inviteFriendInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTINVITEFRIENDS,"Phone No is Mendatory");
				return inviteFriendInfo;
			}
			Integer customerId=null;
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				error.setDescription("Invalid customer Id");
				inviteFriendInfo.setError(error);
				inviteFriendInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTINVITEFRIENDS,"Invalid customer Id");
				return inviteFriendInfo;
			}
			
			QuizCustomerInvitedFriends invitedFriend = DAORegistry.getQuizCustomerInvitedFriendsDAO().getInvitedFriendsByCustomerIdandPhoneNo(customerId, phone);
			if(invitedFriend != null) {
				error.setDescription("Phone No already invited.");
				inviteFriendInfo.setError(error);
				inviteFriendInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTINVITEFRIENDS,"Phone No already invited.");
				return inviteFriendInfo;
			}
			
			invitedFriend = new QuizCustomerInvitedFriends();
			invitedFriend.setCustomerId(customerId);
			invitedFriend.setPhone(phone);
			invitedFriend.setCreatedDate(new Date());
			invitedFriend.setUpdatedDate(new Date());
			invitedFriend.setStatus("ACTIVE");
			DAORegistry.getQuizCustomerInvitedFriendsDAO().save(invitedFriend);
			
			inviteFriendInfo.setStatus(1);
			inviteFriendInfo.setPhone(phone);
			inviteFriendInfo.setMessage("Friend Invited Successfully.");
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Invite friends.");
			inviteFriendInfo.setError(error);
			inviteFriendInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTINVITEFRIENDS,"Error occured while Invite friends..");
			return inviteFriendInfo;
		}
		log.info("QUIZ INVITE FRND : "+request.getParameter("customerId")+" : coId: "+request.getParameter("phone")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return inviteFriendInfo;
	}
	@RequestMapping(value = "/discover/GetQuizMessageTexts", method=RequestMethod.POST)
	public @ResponsePayload QuizMessageTextsInfo getQuizMessageTexts(HttpServletRequest request,HttpServletResponse response){
		QuizMessageTextsInfo messageTextsInfo =new QuizMessageTextsInfo();
		Error error = new Error();
		Date start = new Date();
		try {

			String customerIdStr = request.getParameter("customerId");
			String type = request.getParameter("type");
			String platForm = request.getParameter("platForm");
			
			if(TextUtil.isEmptyOrNull(type)){
				error.setDescription("Type is Mendatory");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Process Type is Mendatory");
				return messageTextsInfo;
			}
			QuizMessageTextType messageType = null;
			try {
				messageType = QuizMessageTextType.valueOf(type);
			}catch (Exception e) {
				error.setDescription("Type is Invalid");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Type is Invalid");
				return messageTextsInfo;
			}
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Customer Id is mandatory");
				return messageTextsInfo;
			}
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Application Platform is mandatory");
				return messageTextsInfo;
			}
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					messageTextsInfo.setError(error);
					messageTextsInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Please send valid application platform");
					return messageTextsInfo;
				}
			}
			Integer customerId=null;
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				error.setDescription("Invalid customer Id");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Invalid customer Id");
				return messageTextsInfo;
			}
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				messageTextsInfo.setError(error);
				messageTextsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Customer Id is Invalid");
				return messageTextsInfo;
			}
			String messageText = "";
			if(messageType.equals(QuizMessageTextType.INVITEFRIEND)) {
				
				messageText = "Have you played the Reward The Fan trivia game yet? If you haven�t heard of it, it�s a new trivia "
						+ "game which rewards correctly answered questions with reward dollars and the chance to win big from a "
						+ "cash winner's pot! Plus you're entered into a raffle for sports, concerts and theater tickets! \n"
						+ "Download the app and use my code '"+customer.getUserId()+"' so we'll both get a free life! \n";

				/*messageText = "I'm playing Reward The Fan's Live Trivia Game Show and you should too! \n"
						+ " Use my code '"+customer.getUserId()+"' to sign up and play! \n";*/
				if(applicationPlatForm.equals(ApplicationPlatForm.IOS) || applicationPlatForm.equals(ApplicationPlatForm.ANDROID)) {
					messageText = messageText + "onelink.to/5xwz4g";
				} else {
					messageText = messageText + " https://rewardthefan.com";
				}
			} else if(messageType.equals(QuizMessageTextType.SHAREMYRANK)){
				//messageText="Hi, I am "+customer.getUserId()+", My rank is #0.I have won "+customer.getQuizNoOfTicketsWon()+" Tix " +
					//	"and $"+TicketUtil.getRoundedValueString(customer.getQuizNoOfPointsWon());
				QuizContestWinners winner = QuizDAORegistry.getQuizQueryManagerDAO().getQuizContestSummaryByTillDateAndCustomerId(customerId);
				Integer rank = 0,tixCount=0;
				Double points=0.0;
				if(winner != null) {
					rank = winner.getRewardRank();
					points = winner.getRewardPoints();
					tixCount = winner.getRewardTickets();
				}
				messageText="I just earned $"+TicketUtil.getRoundedValueString(points)+" Reward Dollars, " +
						" and won "+tixCount+" Tix by playing Reward The Fan! I'm ranked #"+rank+" !";//
			}
			messageTextsInfo.setMsgText(messageText);
			messageTextsInfo.setStatus(1);
			messageTextsInfo.setMessage("");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while getting Quiz Message Texts.");
			messageTextsInfo.setError(error);
			messageTextsInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETQUIZMESSAGETEXT,"Error occured while getting Quiz Message Texts.");
			return messageTextsInfo;
		}
		log.info("QUIZ MSG TEXT : "+request.getParameter("customerId")+" : coId: "+request.getParameter("type")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return messageTextsInfo;
		
	}	
	
	
	@RequestMapping(value = "/discover/QuizCustomersAutoSearch", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerSearchInfo getQuizCustomersAutosearch(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerSearchInfo customerSearchInfo =new QuizCustomerSearchInfo();
		Error error = new Error();
		Date start =new Date();
		try {

			String customerId = request.getParameter("customerId");
			String searchKey = request.getParameter("searchKey");
			
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customer Id is mandatory.");
				customerSearchInfo.setError(error);
				customerSearchInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERAUTOSEARCH,"Customer Id is mandatory");
				return customerSearchInfo;
			}
			if(TextUtil.isEmptyOrNull(searchKey)){
				error.setDescription("Please enter a search keyword");
				customerSearchInfo.setError(error);
				customerSearchInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERAUTOSEARCH,"Please enter a search keyword");
				return customerSearchInfo;
			}
			if(searchKey != null && !searchKey.equals("")) {
				searchKey = searchKey.replaceAll("\\W+", "%");
			}
			
			List<CustomerSearchDetails> searchResult = DAORegistry.getCustomerDAO().getCustomerAutosearchForQuiz(searchKey,Integer.parseInt(customerId));
			//long autoSearchDBTime = new Date().getTime()-(startTime);
			//log.info("AutoSearchDB "+" : "+searchKey+" : Time : "+autoSearchDBTime);
			
			customerSearchInfo.setCustomerSearchDetails(searchResult);
			customerSearchInfo.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCUSTOMERAUTOSEARCH,"Success");
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Quiz Customer Auto search :");
			customerSearchInfo.setError(error);
			customerSearchInfo.setStatus(0);
			return customerSearchInfo;
		}
		log.info("QUIZ CUST AUTO: "+request.getParameter("customerId")+" :coId: "+request.getParameter("searchKey")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return customerSearchInfo;
		
	}
	
	
    
	@RequestMapping(value = "/discover/GetCustomerStatsDetails", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerStatsInfo getCustomerStatsDetails(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerStatsInfo quizCustomerStatsInfo = new QuizCustomerStatsInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizCustomerStatsInfo.setError(authError);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,authError.getDescription());
				return quizCustomerStatsInfo;
			}*/
			
			String customerIdStr = request.getParameter("customerId");
			String friendCustomerIdStr = request.getParameter("friendCustomerId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Customer Id is mandatory");
				return quizCustomerStatsInfo;
			}
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Question No is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Question No is mandatory");
				return quizCustomerStatsInfo;
			}*/
			if(TextUtil.isEmptyOrNull(friendCustomerIdStr)){
				error.setDescription("Friend Customer Id is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETCUSTOMERINFO,"Friend Customer Id is mandatory");
				return quizCustomerStatsInfo;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Customer Id is Invalid");
				return quizCustomerStatsInfo;
			}
			
			Customer friendCustomer = CustomerUtil.getCustomerById(Integer.parseInt(friendCustomerIdStr));
			if(friendCustomer == null) {
				error.setDescription("Friend Customer Id is Invalid");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Friend Customer Id is Invalid");
				return quizCustomerStatsInfo;
			}
			String friendStatus ="Add Friend";// DAORegistry.getQueryManagerDAO().getcustomerFrientStatus(customer.getId(), friendCustomer.getId());
			Boolean isSender = false;
			
			if(customer.getId().equals(friendCustomer.getId())) {
				friendStatus = FriendStatus.SameCustoemr.toString();
			} else {
	 			List<QuizCustomerFriend> custoemrFriends = DAORegistry.getQuizCustomerFriendDAO().getQuizCustomerFriendRecords(customer.getId(), friendCustomer.getId());
				if(custoemrFriends != null) {
					for (QuizCustomerFriend customerFriend : custoemrFriends) {
						if(customerFriend.getStatus().equals(FriendStatus.Requested) ||
								customerFriend.getStatus().equals(FriendStatus.Friend)) {
							
							friendStatus = customerFriend.getStatus().toString();
							
							if(customerFriend.getCustomerId().equals(customer.getId())) {
								isSender = customerFriend.getIsSender();	
							} else {
								isSender = !customerFriend.getIsSender();
							}
						}
					}
				}
			}
			if(!isSender && friendStatus.equals(FriendStatus.Requested.toString())) {
				friendStatus = FriendStatus.Accept.toString();
			}
			CustomerStatsDetails custoemrStats = new CustomerStatsDetails(friendCustomer);
			custoemrStats.setFriendStatus(friendStatus);
			custoemrStats.setIsSender(isSender);
			quizCustomerStatsInfo.setCustomerStatsDetails(custoemrStats);
			
			quizCustomerStatsInfo.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Stats Details.");
			quizCustomerStatsInfo.setError(error);
			quizCustomerStatsInfo.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Error occured while Fetching Customer Stats Details.");
			return quizCustomerStatsInfo;
		}
		log.info("QUIZ CUT STATS DTLS : "+request.getParameter("customerId")+" :fCId: "+request.getParameter("friendCustomerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizCustomerStatsInfo;
	
	}
	
	@RequestMapping(value = "/discover/SendFriendRequest", method=RequestMethod.POST)
	public @ResponsePayload QuizContestFriendRequestDetails sendFriendRequest(HttpServletRequest request,HttpServletResponse response){
		QuizContestFriendRequestDetails friendRequestDetail =new QuizContestFriendRequestDetails();
		Error error = new Error();
		Date start = new Date();
		try {

			String customerIdStr = request.getParameter("customerId");
			String friendCustomerId = request.getParameter("friendCustomerId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Customer Id is mandatory");
				return friendRequestDetail;
			}
			if(TextUtil.isEmptyOrNull(friendCustomerId)){
				error.setDescription("Friend Customer Id is Mendatory");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Friend Customer Id is Mendatory");
				return friendRequestDetail;
			}
			Integer customerId=null;
			Integer customerFriendId = null;
			try {
				customerId = Integer.parseInt(customerIdStr);
				customerFriendId = Integer.parseInt(friendCustomerId);
			} catch (Exception e) {
				error.setDescription("Invalid value for customerId or friendCustomerId");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Invalid value for customerId or friendCustomerId");
				return friendRequestDetail;
			}
			List<QuizCustomerFriend> friends = DAORegistry.getQuizCustomerFriendDAO().getQuizCustomerFriendRecords(customerId,customerFriendId);
			String msg=null;
			Boolean isValid = true;
			if(!friends.isEmpty()){
				for(QuizCustomerFriend f : friends){
					if(f.getStatus().equals(FriendStatus.Requested)){
						msg = "Friend request is already sent to selected user.";
						isValid = false;
						break;
					}else if(f.getStatus().equals(FriendStatus.Friend)){
						msg = "Selected user is already your friend";
						isValid = false;
						break;
					}/*else if(f.getStatus().equals(FriendStatus.BLOCKED)){
						msg = "Selected user is already your friend";
					}*/
				}
				if(!isValid){
					error.setDescription(msg);
					friendRequestDetail.setError(error);
					friendRequestDetail.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,msg);
					return friendRequestDetail;
				}
			}
			
			
			QuizCustomerFriend friend = new QuizCustomerFriend();
			friend.setCustomerId(customerId);
			friend.setCustomerFriendId(customerFriendId);
			friend.setCreatedDate(new Date());
			friend.setUpdatedDate(new Date());
			friend.setStatus(FriendStatus.Requested);
			friend.setIsSender(true);
			DAORegistry.getQuizCustomerFriendDAO().save(friend);
			friendRequestDetail.setStatus(1);
			friendRequestDetail.setFriendStatus(FriendStatus.Requested.toString());
			friendRequestDetail.setMessage("Friend request is sent.");
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending friend request.");
			friendRequestDetail.setError(error);
			friendRequestDetail.setStatus(0);
			return friendRequestDetail;
		}
		log.info("QUIZ SEND F-REQ: "+request.getParameter("customerId")+" :fcId: "+request.getParameter("friendCustomerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return friendRequestDetail;
	}
	
	
	@RequestMapping(value = "/discover/AcceptRejectFriendRequest", method=RequestMethod.POST)
	public @ResponsePayload QuizContestFriendRequestDetails acceptRejectFriendRequest(HttpServletRequest request,HttpServletResponse response){
		QuizContestFriendRequestDetails friendRequestDetail =new QuizContestFriendRequestDetails();
		Error error = new Error();
		try {

			String customerIdStr = request.getParameter("customerId");
			String friendCustomerId = request.getParameter("friendCustomerId");
			String statusStr = request.getParameter("status");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Customer Id is mandatory");
				return friendRequestDetail;
			}
			if(TextUtil.isEmptyOrNull(friendCustomerId)){
				error.setDescription("Friend Customer Id is Mendatory");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Friend Customer Id is Mendatory");
				return friendRequestDetail;
			}
			if(TextUtil.isEmptyOrNull(statusStr)){
				error.setDescription("Friend Request accept/reject status mendatory");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Friend Request accept/reject status mendatory");
				return friendRequestDetail;
			}
			
			Integer customerId=null;
			Integer customerFriendId = null;
			FriendStatus status = null;
			try {
				customerId = Integer.parseInt(customerIdStr);
				customerFriendId = Integer.parseInt(friendCustomerId);
			}catch (Exception e) {
				error.setDescription("Invalid value for customerId or friendCustomerId");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Invalid value for customerId or friendCustomerId");
				return friendRequestDetail;
			}
			try {
				status  = FriendStatus.valueOf(statusStr);
			} catch (Exception e) {
				error.setDescription("Invalid friend request status");
				friendRequestDetail.setError(error);
				friendRequestDetail.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Invalid friend request status");
				return friendRequestDetail;
			}
			
			
			if(status.equals(FriendStatus.Friend)){
				QuizCustomerFriend friendRequest = DAORegistry.getQuizCustomerFriendDAO().getPendingFriendRequest(customerId,customerFriendId);
				if(friendRequest == null){
					error.setDescription("Friend Request is not found");
					friendRequestDetail.setError(error);
					friendRequestDetail.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Friend Request is not found");
					return friendRequestDetail;
				}
				QuizCustomerFriend friend = new QuizCustomerFriend();
				friend.setCustomerId(friendRequest.getCustomerFriendId());
				friend.setCustomerFriendId(friendRequest.getCustomerId());
				friend.setCreatedDate(new Date());
				friend.setUpdatedDate(new Date());
				friend.setStatus(FriendStatus.Friend);
				friend.setIsSender(false);
				DAORegistry.getQuizCustomerFriendDAO().save(friend);
				
				friendRequest.setUpdatedDate(new Date());
				friendRequest.setStatus(status);
				DAORegistry.getQuizCustomerFriendDAO().update(friendRequest);
				friendRequestDetail.setStatus(1);
				friendRequestDetail.setFriendStatus(FriendStatus.Friend.toString());
				friendRequestDetail.setMessage("You are now friends.");
				return friendRequestDetail;
			}else if(status.equals(FriendStatus.UnFriend)){
				List<QuizCustomerFriend> friends  = DAORegistry.getQuizCustomerFriendDAO().getQuizCustomerFriendRecords(customerId, customerFriendId);
				if(!friends.isEmpty()){
					for(QuizCustomerFriend ff : friends){
						if(ff.getStatus().equals(FriendStatus.Friend)){
							ff.setStatus(FriendStatus.UnFriend);
							ff.setUnfriendBy(customerId);
						}
					}
					DAORegistry.getQuizCustomerFriendDAO().updateAll(friends);
					friendRequestDetail.setMessage("Your unfriend request processed susccessfully");
					friendRequestDetail.setFriendStatus("Add Friend");
					friendRequestDetail.setStatus(1);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"No Friend records found with given customerId and friendCustomerId");
					return friendRequestDetail;
				}else{
					error.setDescription("No Friend records found with given customerId and friendCustomerId");
					friendRequestDetail.setError(error);
					friendRequestDetail.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"No Friend records found with given customerId and friendCustomerId");
					return friendRequestDetail;
				}
			}else if(status.equals(FriendStatus.Rejected)){
				QuizCustomerFriend friendRequest = DAORegistry.getQuizCustomerFriendDAO().getPendingFriendRequest(customerId,customerFriendId);
				if(friendRequest == null){
					error.setDescription("Friend Request is not found");
					friendRequestDetail.setError(error);
					friendRequestDetail.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Friend Request is not found");
					return friendRequestDetail;
				}
				friendRequest.setUpdatedDate(new Date());
				friendRequest.setStatus(status);
				DAORegistry.getQuizCustomerFriendDAO().update(friendRequest);
				friendRequestDetail.setStatus(1);
				friendRequestDetail.setFriendStatus("Add Friend");
				friendRequestDetail.setMessage("Friend request is rejected.");
				return friendRequestDetail;
			}else if(status.equals(FriendStatus.Deleted)){
				QuizCustomerFriend friendRequest = DAORegistry.getQuizCustomerFriendDAO().getFriendRequest(customerId,customerFriendId);
				if(friendRequest == null){
					error.setDescription("Friend Request is not found");
					friendRequestDetail.setError(error);
					friendRequestDetail.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSENDFRIENDREQUEST,"Friend Request is not found");
					return friendRequestDetail;
				}
				friendRequest.setStatus(FriendStatus.Deleted);
				friendRequest.setUpdatedDate(new Date());
				friendRequest.setUnfriendBy(customerId);
				DAORegistry.getQuizCustomerFriendDAO().update(friendRequest);
				friendRequestDetail.setStatus(1);
				friendRequestDetail.setFriendStatus("Add Friend");
				friendRequestDetail.setMessage("Friend request is withdrawn.");
				return friendRequestDetail;
			}
			error.setDescription("Please send valid friend status");
			friendRequestDetail.setError(error);
			friendRequestDetail.setStatus(0);
			return friendRequestDetail;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while accepting/rejecting friend request.");
			friendRequestDetail.setError(error);
			friendRequestDetail.setStatus(0);
			return friendRequestDetail;
		}
	}
     
	
	public static Error authorizationValidation(HttpServletRequest request) throws Exception {
		Error error = new Error();
		HttpSession session = request.getSession();
		String ip =(String)session.getAttribute("ip");
		String configIdString = request.getParameter("configId");
		Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
		
		if(!isAuthrozed){
			error.setDescription("You are not authorized.");
			//quizOTPDetails.setError(error);
			//quizOTPDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
			return error;
		}
		
		if(configIdString!=null && !configIdString.isEmpty()){
			/*try {
				if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
					error.setDescription("You are not authorized.");
					//quizOTPDetails.setError(error);
					//quizOTPDetails.setStatus(0);
					//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
					return error;
				
				}
			} catch (Exception e) {
				error.setDescription("You are not authorized.");
				//quizOTPDetails.setError(error);
				//quizOTPDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
				return error;
			}*/
		}else{
			error.setDescription("You are not authorized.");
			//quizOTPDetails.setError(error);
			//quizOTPDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
			return error;
		}
		return null;
	}
	
}	