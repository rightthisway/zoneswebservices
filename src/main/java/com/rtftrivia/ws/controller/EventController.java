package com.rtftrivia.ws.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Cards;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.data.ParentCategory;
import com.zonesws.webservices.data.RTFCategorySearch;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.enums.CardType;
import com.zonesws.webservices.enums.HomeCardType;
import com.zonesws.webservices.enums.ParentType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.TeamCardImageUtil;
import com.zonesws.webservices.utils.CategorySearchMechanism;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.EventArtistUtil;
import com.zonesws.webservices.utils.PaginationUtil;
import com.zonesws.webservices.utils.RTFCategorySearchUtil;
import com.zonesws.webservices.utils.RTFGetCacheDetails;
import com.zonesws.webservices.utils.RTFHomeCardConstruction;
import com.zonesws.webservices.utils.SearchConstructionUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.ZipCodeUtil;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.ArtistResult;
import com.zonesws.webservices.utils.list.AutoPageResult;
import com.zonesws.webservices.utils.list.AutoSearchResult;
import com.zonesws.webservices.utils.list.Card;
import com.zonesws.webservices.utils.list.EventCards;
import com.zonesws.webservices.utils.list.EventResult;
import com.zonesws.webservices.utils.list.NormalSearchResult;
import com.zonesws.webservices.utils.list.RTFCacheObj;
import com.zonesws.webservices.utils.list.VenueResult;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/GetHomeCards","/GetHomeCardsMobile","/NormalizedSearch"})
public class EventController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(EventController.class);
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	 

	@RequestMapping(value="/GetHomeCards",method=RequestMethod.POST)
	public @ResponsePayload EventCards getHomeCards(HttpServletRequest request, Model model,HttpServletResponse response){
		EventCards eventList =new EventCards();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
				return eventList;
			}
//			Integer configId= null;
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
						return eventList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
					return eventList;
				}
			}else{
				error.setDescription("You are not authorized.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
				return eventList;
			}
			String productTypeStr = request.getParameter("productType");
			String cardTypeStr = request.getParameter("cardType");
			String ipAddress = (null != request.getParameter("clientIPAddress"))?request.getParameter("clientIPAddress"):ip;
			String deviceId = request.getParameter("deviceId");
			String state = request.getParameter("state");
			String startDateStr = request.getParameter("startDate");
			String endDateStr = request.getParameter("endDate");
			String customerIdStr = request.getParameter("customerId");
			String pageNumberStr = request.getParameter("pageNumber");
			String platForm = request.getParameter("platForm");
			String defaultLocSearchFlag = request.getParameter("defaultLocationSearch");
			
			System.out.println("PARAMETER VALIDATION - BEGINS: "+new Date());
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Product Type is mandatory");
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Please send valid product type");
					return eventList;
				}
			}
			
			
			ApplicationPlatForm applicationPlatForm=ApplicationPlatForm.IOS;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid Application Platform");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Please send valid Application Platform");
					return eventList;
				}
			}
			
			
			if(TextUtil.isEmptyOrNull(cardTypeStr)){
				error.setDescription("Home Card Type is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Home Card Type is mandatory");
			}
			
			HomeCardType homeCardType=null;
			if(!TextUtil.isEmptyOrNull(cardTypeStr)){
				try{
					homeCardType = HomeCardType.valueOf(cardTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid Home Card type");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Please send valid Home Card type");
					return eventList;
				}
			}
			
			
			if(TextUtil.isEmptyOrNull(pageNumberStr)){
				error.setDescription("Page Number is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Page Number Address mandatory");
				return eventList;
			}
			
			if(deviceId ==null ){
				error.setDescription("Customer Device Id is Mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Customer Device Id is Mandatory");
				return eventList;
			}
			
			Integer pageNumber = null;
			try{
				pageNumber = Integer.parseInt(pageNumberStr.trim());
			}catch(Exception e){
				error.setDescription("Page Number should be integer");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Page Number should be integer");
				return eventList;
			}
			
			
			
			Date eventStartDate = null,eventEndDate=null;
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Boolean dateFilterApplied = false,locationFilterApplied=false;
			
			if((null != startDateStr && (startDateStr.equals("00/00/0000") || startDateStr == "00/00/0000")) &&
					(null != endDateStr && (endDateStr.equals("00/00/0000") || endDateStr == "00/00/0000"))){

			}else{
				if(!TextUtil.isEmptyOrNull(startDateStr)){
					try{
						eventStartDate = df.parse(startDateStr.trim());
						dateFilterApplied = true;
					}catch(Exception e){
						error.setDescription("Valid Start Date Format is MM/dd/yyyy");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Valid Start Date Format is MM/dd/yyyy");
						return eventList;
					}
				}
				
				if(!TextUtil.isEmptyOrNull(endDateStr)){
					try{
						eventEndDate = df.parse(endDateStr.trim());
						dateFilterApplied = true;
					}catch(Exception e){
						error.setDescription("Valid End Date Format is MM/dd/yyyy");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Valid End Date Format is MM/dd/yyyy");
						return eventList;
					}
				}
				
				
			}
			
			if(!TextUtil.isEmptyOrNull(state)){
				locationFilterApplied=true;
			}
			
			if(applicationPlatForm.equals(ApplicationPlatForm.IOS) || applicationPlatForm.equals(ApplicationPlatForm.ANDROID)){
				if(!locationFilterApplied){
					/*state="NY";
					locationFilterApplied=true;*/
				}else{
					//Add Code if state is outside USA and CA
				}
			}else if(!locationFilterApplied ){
				try{
					state = ZipCodeUtil.getStateByIP(ipAddress);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(null != state && !state.isEmpty()){
					locationFilterApplied=true;
				}
			}
				
			/*List<Event> upcomingEvents=UpcomingEventUtil.getNext5UpcomingEvents();
			Set<Integer> skipEventIds = new HashSet<Integer>();
			for (Event event : upcomingEvents) {
				skipEventIds.add(event.getEventId());
			}*/
				
				/*End*/
			int maxRows = PaginationUtil.eventCardsMaxRows;
			System.out.println("HOMECARDS LOGICAL EXCUTION - BEGINS: "+new Date());
			
			boolean showMoreButton = true;
			String alertMessage = "";			
			RTFCacheObj rtfCacheObj  = RTFGetCacheDetails.getHomeCards(pageNumber, maxRows, locationFilterApplied, 
					state, dateFilterApplied, eventStartDate, eventEndDate);
			
			
			if(null != rtfCacheObj && null != rtfCacheObj.getCacheOff() && !rtfCacheObj.getCacheOff()){
				
				if(null == rtfCacheObj.getEventList() || rtfCacheObj.getEventList().isEmpty()){
					System.out.println("HOMECARDS LOGICAL EXCUTION - ENDS: "+new Date());
			        eventList.setCards(null);
					eventList.setStatus(1);
					eventList.setShowMoreButton(false);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Success");
					return eventList;
				}
				
				List<Card> cards = RTFHomeCardConstruction.constructCards(rtfCacheObj.getEventList(),customerIdStr, 
						pageNumber, maxRows,  applicationPlatForm, configIdString,null);
				
				if(null != customerIdStr && !customerIdStr.isEmpty()){
					
					Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
					
					/* Share Link Validation for Share Icon - Added By Ulaganathan*/
					String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
					if(null != customer){
						eventList.setEligibleToShareRefCode(customer.getEligibleToShareRefCode());
						eventList.setShareErrorMessage(TextUtil.getCustomerShareLinkValidationMessage(fontSizeStr));
					}
					/* Share Link Validation for Share Icon - Added By Ulaganathan*/
				}
				
				
				
				if(null != cards && !cards.isEmpty()){
					System.out.println("HOMECARDS LOGICAL EXCUTION - ENDS: "+new Date());
			        eventList.setCards(cards);
					eventList.setStatus(1);
					eventList.setShowMoreButton(rtfCacheObj.getHomeCardShowMore());
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Success");
					return eventList;
				}
			}
			
			List<Event> events = DAORegistry.getEventDAO().getAllEventsByHomeCardType(homeCardType, eventStartDate, eventEndDate,state,
					pageNumber,maxRows,null);
			
			Customer customer = null;
			
			if(null != customerIdStr && !customerIdStr.isEmpty()){
				customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
			}
			
			/* Share Link Validation for Share Icon - Added By Ulaganathan*/
			String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			if(null != customer){
				//eventList.setEligibleToShareRefCode(customer.getEligibleToShareRefCode());
				//eventList.setShareErrorMessage(TextUtil.getCustomerShareLinkValidationMessage(fontSizeStr));
			}
			/* Share Link Validation for Share Icon - Added By Ulaganathan*/
			
			Map<Integer, List<Event>> artistEventmap = new HashMap<Integer, List<Event>>();
			
			Long daysDifference = 0l;
			Date curDate = new Date(),eventDate = null;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			if(null == events || events.size() < maxRows){
				showMoreButton = false;
			}
			
			for (Event event : events) {
				
				try{
					if(event.getEventDateTime().contains("TBD")){
						eventDate = dateFormat.parse(event.getEventDateTime().replaceAll("TBD", "12:00 AM")); 
					}else{
						eventDate = dateFormat.parse(event.getEventDateTime());
					}
				}catch(Exception e){
					eventDate = new Date();
				}
				daysDifference = DateUtil.getDifferenceMintues(curDate, eventDate);
				event.setDayDifference(daysDifference);
				
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}
				event.setEventImageUrl(applicationPlatForm);
				URLUtil.getShareLinkURL(event, customer,applicationPlatForm);*/
				
				List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
				if(null == artistIds || artistIds.isEmpty() || artistIds.size() > 1){
					continue;
				}
				event.setArtistIds(artistIds);
				
				//FavoriteUtil.markEventAsFavorite(event, favouriteEventMap, favouriteArtistMap, unFavoritedEventIds);
				
				List<Event> list = artistEventmap.get(event.getArtistId());
				
				if(null != list && !list.isEmpty()){
					artistEventmap.get(artistIds.get(0)).add(event);
				}else{
					list = new ArrayList<Event>();
					list.add(event);
					artistEventmap.put(artistIds.get(0), list);
				}
			}
			
			Card card = null;
			List<Card> cards = new ArrayList<Card>();
			
			pageNumber = pageNumber-1;
			
			int i =1 , cardTypeCount =(maxRows * pageNumber)+1, totalCount=1;
			
			/*List<Cards> cardsList = DAORegistry.getCardsDAO().getAllCardsByProductId(1);
			Map<Integer, Cards> cardsPositionMap = new HashMap<Integer, Cards>();
			for (Cards ticTrackerCard : cardsList) {
				cardsPositionMap.put(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE)?ticTrackerCard.getDesktopPosition():ticTrackerCard.getMobilePosition(), ticTrackerCard);
			}*/
			
			Map<String, List<Event>> map = new HashMap<String, List<Event>>();
						
			for (Integer artistId : artistEventmap.keySet()) {
				List<Event> artisEvents = artistEventmap.get(artistId);
				Collections.sort(artisEvents, TicketUtil.eventDaysComparatorForEventDateAsc);
				String key = artisEvents.get(0).getDayDifference()+"_"+artistId;
				map.put(key, artisEvents);
			}
			
			List<String> keyList = new ArrayList<String>(map.keySet());
			Collections.sort(keyList, TicketUtil.comparatorCardsAscByEventDate);
			
			for (String key : keyList) {
				
				Integer artistId =Integer.parseInt(key.split("_")[1]);
				
                Cards ticTrackerCard = null;//cardsPositionMap.get(cardTypeCount);
                card=  new Card();
                if(null != ticTrackerCard){
                     
                      /*try{
                    	  card.setCardType(ticTrackerCard.getCardType());
                    	  switch (ticTrackerCard.getCardType()) {
                          
	                          case PROMOTION:
	                                
	                                 
	                                card.setArtistId(ticTrackerCard.getArtistId());
	                                card.setCardName(ticTrackerCard.getImageText());
							        card.setCardImage(MapUtil.getCardImage(ticTrackerCard.getImageFileUrl(),applicationPlatForm));
	                                card.setImageText(ticTrackerCard.getImageText());
	                                card.setIsFavoriteArtist(false);
	                                
	                                List<Event> eventDetails = DAORegistry.getEventDAO().getEventsByArtistId(ticTrackerCard.getArtistId(), 1, ticTrackerCard.getNoOfEvents());
	                                
	                                if(eventDetails == null || eventDetails.isEmpty()){
	                                	continue;
	                                }
	                                card.setEvents(eventDetails);
	                                
	                                break;
	                                
	                          case MAINPROMOTION:
							      card.setCardImage(MapUtil.getMainPromotionCardImage(ticTrackerCard.getImageFileUrl(),ticTrackerCard.getMobileImageFileUrl(),applicationPlatForm));
							      card.setCardImageForMobile(MapUtil.getMainPromotionCardImage(ticTrackerCard.getImageFileUrl(),ticTrackerCard.getMobileImageFileUrl(),ApplicationPlatForm.ANDROID));
	                              card.setImageText(ticTrackerCard.getImageText());
	                              
	                              if(ticTrackerCard.getPromoCardType().equals("ARTIST") ){
	                            	  card.setArtistId(ticTrackerCard.getArtistId());
		                              card.setCardName(ticTrackerCard.getArtist().getName());
	                              }else if(ticTrackerCard.getPromoCardType().equals("VENUE") ){
	                            	  card.setArtistId(ticTrackerCard.getVenueId());
		                              card.setCardName(ticTrackerCard.getVenue().getBuilding());
	                              }else if(ticTrackerCard.getPromoCardType().equals("GRAND") ){
	                            	  card.setArtistId(ticTrackerCard.getGrandChildCatId());
		                              card.setCardName(ticTrackerCard.getGrandChildCategory().getName());
	                              }else if(ticTrackerCard.getPromoCardType().equals("CHILD") ){
	                            	  card.setArtistId(ticTrackerCard.getChildCatId());
		                              card.setCardName(ticTrackerCard.getChildCategory().getName());
	                              }else if(ticTrackerCard.getPromoCardType().equals("PARENT") ){
	                            	  card.setArtistId(ticTrackerCard.getParentCatId());
		                              card.setCardName(ticTrackerCard.getParentCategory().getName());
	                              } 
	                              card.setPromoCardType(ticTrackerCard.getPromoCardType());
	                              break;
	                          case UPCOMMINGEVENT:
	                          	events=UpcomingEventUtil.getNext5UpcomingEvents();
	                          	for (Event event : events) {
	          						 event = MapUtil.testSvgDetailsByEvent(event);
	          						 if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
	          							continue;
	          						 }
		            					  event.setEventImageUrl(applicationPlatForm);
		            					  URLUtil.getShareLinkURL(event, customer,applicationPlatForm);
		                                  card.setCardImage("");
		                                  card.setCardType(CardType.UPCOMMINGEVENT);
		                                  card.setCardName("Upcoming Events");
		                                  try{
		                                	  card.setEvents(UpcomingEventUtil.getNext5UpcomingEvents());
		                                  }catch(Exception e){
		                                	  e.printStackTrace();
		                                  }
		                                  
		                                  if(null == card.getEvents() || card.getEvents().isEmpty()){
		                                	continue;  
		                                  }
		                                  
		                                  List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
			                  			  if(null == artistIds || artistIds.isEmpty()){
			                  				continue;
			                  			  }
			                  			  event.setArtistIds(artistIds);
	                          	}
	                                break;
	                                
	                          case YESNO:
	                                card.setCardImage(MapUtil.getCardImage(ticTrackerCard.getImageFileUrl(),applicationPlatForm));
	                                card.setImageText(ticTrackerCard.getImageText());
	                                card.setYesUrl("");
	                                card.setNoUrl("");
	                                break;
	
	                          default:
	                                break;
                    	  }
                    	  
                      }catch(Exception e){
                    	  e.printStackTrace();
                    	  continue;
                      }*/
                      
                }else{
                      
                      List<Event> finalEvents = new ArrayList<Event>();
                      List<Event> artisEvents = map.get(key);
                      
                      
                      Event event = artisEvents.get(0);
                      
                      card.setArtistId(artistId);
                      //card.setCardImage(MapUtil.getCardImage(event));
                      //Get the team card image for artist
                      try {
                    	  card.setCardImage(TeamCardImageUtil.getPrarentCategoryImage(event.getParentCategoryId()));  
                      }catch(Exception e) {
                    	  card.setCardImage(TeamCardImageUtil.getTeamCardImageByArtist(artistId,applicationPlatForm));
                      }
                      
                      card.setCardType(CardType.TEAM_CARD);
                      card.setIsFavoriteArtist(false);
                      card.setCardName(event.getArtistName());
                      card.setArtistId(event.getArtistId());
                      finalEvents = new ArrayList<Event>();
                      finalEvents.add(event);
                      card.setEvents(finalEvents);
                      
                }
                
                cardTypeCount++;
                cards.add(card);
                i++;
                
                if(totalCount > 25){
                      break;
                }
                totalCount++;
			}
			
			System.out.println("HOMECARDS LOGICAL EXCUTION - ENDS: "+new Date());
			eventList.setAlertMessage("");
	        eventList.setCards(cards);
			eventList.setStatus(1);
			eventList.setShowMoreButton(showMoreButton);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Success");
			return eventList;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Oops something went wrong. Please try search again.");
			eventList.setError(error);
			eventList.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Oops something went wrong. Please try search again.");
			return eventList;
		}
	}
	
	
	@RequestMapping(value="/GetHomeCardsMobile",method=RequestMethod.POST)
	public @ResponsePayload EventCards getHomeCardsMobile(HttpServletRequest request, Model model,HttpServletResponse response){
		EventCards eventList =new EventCards();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
				return eventList;
			}
//			Integer configId= null;
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
						return eventList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
					return eventList;
				}
			}else{
				error.setDescription("You are not authorized.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"You are not authorized");
				return eventList;
			}
			String productTypeStr = request.getParameter("productType");
			String cardTypeStr = request.getParameter("cardType");
			String ipAddress = (null != request.getParameter("clientIPAddress"))?request.getParameter("clientIPAddress"):ip;
			String deviceId = request.getParameter("deviceId");
			String state = request.getParameter("state");
			String startDateStr = request.getParameter("startDate");
			String endDateStr = request.getParameter("endDate");
			String customerIdStr = request.getParameter("customerId");
			String pageNumberStr = request.getParameter("pageNumber");
			String platForm = request.getParameter("platForm");
			String defaultLocSearchFlag = request.getParameter("defaultLocationSearch");
			
			System.out.println("PARAMETER VALIDATION - BEGINS: "+new Date());
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Product Type is mandatory");
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Please send valid product type");
					return eventList;
				}
			}
			
			
			ApplicationPlatForm applicationPlatForm=ApplicationPlatForm.IOS;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					error.setDescription("Please send valid Application Platform");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Please send valid Application Platform");
					return eventList;
				}
			}
			
			
			if(TextUtil.isEmptyOrNull(cardTypeStr)){
				error.setDescription("Home Card Type is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Home Card Type is mandatory");
			}
			
			HomeCardType homeCardType=null;
			if(!TextUtil.isEmptyOrNull(cardTypeStr)){
				try{
					homeCardType = HomeCardType.valueOf(cardTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid Home Card type");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Please send valid Home Card type");
					return eventList;
				}
			}
			
			
			if(TextUtil.isEmptyOrNull(pageNumberStr)){
				error.setDescription("Page Number is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Page Number Address mandatory");
				return eventList;
			}
			
			if(deviceId ==null ){
				error.setDescription("Customer Device Id is Mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Customer Device Id is Mandatory");
				return eventList;
			}
			
			Integer pageNumber = null;
			try{
				pageNumber = Integer.parseInt(pageNumberStr.trim());
			}catch(Exception e){
				error.setDescription("Page Number should be integer");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Page Number should be integer");
				return eventList;
			}
			
			
			
			Date eventStartDate = null,eventEndDate=null;
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Boolean dateFilterApplied = false,locationFilterApplied=false;
			
			if((null != startDateStr && (startDateStr.equals("00/00/0000") || startDateStr == "00/00/0000")) &&
					(null != endDateStr && (endDateStr.equals("00/00/0000") || endDateStr == "00/00/0000"))){

			}else{
				if(!TextUtil.isEmptyOrNull(startDateStr)){
					try{
						eventStartDate = df.parse(startDateStr.trim());
						dateFilterApplied = true;
					}catch(Exception e){
						error.setDescription("Valid Start Date Format is MM/dd/yyyy");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Valid Start Date Format is MM/dd/yyyy");
						return eventList;
					}
				}
				
				if(!TextUtil.isEmptyOrNull(endDateStr)){
					try{
						eventEndDate = df.parse(endDateStr.trim());
						dateFilterApplied = true;
					}catch(Exception e){
						error.setDescription("Valid End Date Format is MM/dd/yyyy");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Valid End Date Format is MM/dd/yyyy");
						return eventList;
					}
				}
				
				
			}
			
			if(!TextUtil.isEmptyOrNull(state)){
				locationFilterApplied=true;
			}
			
			 
			Set<Integer> skipEventIds = new HashSet<Integer>();
				/*End*/
			int maxRows = PaginationUtil.eventCardsMaxRows;
			
			
			boolean showMoreButton = true;
			String alertMessage = "";	
			
			System.out.println("HOMECARDS_MOBILE LOGICAL EXCUTION - BEGINS: "+new Date());
			System.out.println("HOMECARDS_MOBILE: Fetch Event List From Cache - BEGINS: "+new Date());
			RTFCacheObj rtfCacheObj  = RTFGetCacheDetails.getHomeCards(pageNumber, maxRows, locationFilterApplied, 
					state, dateFilterApplied, eventStartDate, eventEndDate);
			
			System.out.println("HOMECARDS_MOBILE: Fetch Event List From Cache - Ends: "+new Date());
			if(null != rtfCacheObj && null != rtfCacheObj.getCacheOff() && !rtfCacheObj.getCacheOff()){
				
				if(null == rtfCacheObj.getEventList() || rtfCacheObj.getEventList().isEmpty()){
					System.out.println("HOMECARDS_MOBILE LOGICAL EXCUTION - ENDS: "+new Date());
			        eventList.setCards(null);
					eventList.setStatus(1);
					eventList.setShowMoreButton(false);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Success");
					return eventList;
				}
				
				System.out.println("HOMECARDS_MOBILE: Construct Home Cards - Begins: "+new Date());
				List<Card> cards = RTFHomeCardConstruction.constructCardsMobile(rtfCacheObj.getEventList(),customerIdStr, 
						pageNumber, maxRows,  applicationPlatForm, configIdString,skipEventIds);
				System.out.println("HOMECARDS_MOBILE: Construct Home Cards - Ends: "+new Date());
				
				if(null != cards && !cards.isEmpty()){
					System.out.println("HOMECARDS_MOBILE LOGICAL EXCUTION - ENDS: "+new Date());
			        eventList.setCards(cards);
					eventList.setStatus(1);
					eventList.setShowMoreButton(rtfCacheObj.getHomeCardShowMore());
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Success");
					return eventList;
				}
			}
			
			System.out.println("HOMECARDS_MOBILE: Manual DB Fetch Begins "+new Date());
			List<Event> events = DAORegistry.getEventDAO().getAllEventsByHomeCardType(homeCardType, eventStartDate, eventEndDate,state,
					pageNumber,maxRows,skipEventIds);
			
			System.out.println("HOMECARDS_MOBILE: Manual DB Fetch Ends "+new Date());
			
			Customer customer = null;
			
			if(null != customerIdStr && !customerIdStr.isEmpty()){
				customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
			}
			
			Map<Integer, List<Event>> artistEventmap = new HashMap<Integer, List<Event>>();
			
			Long daysDifference = 0l;
			Date curDate = new Date(),eventDate = null;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			if(null == events || events.size() < maxRows){
				showMoreButton = false;
			}
			
			System.out.println("HOMECARDS_MOBILE: Grouping all events - Begins "+new Date());
			
			for (Event event : events) {
				
				 
				
				try{
					if(event.getEventDateTime().contains("TBD")){
						eventDate = dateFormat.parse(event.getEventDateTime().replaceAll("TBD", "12:00 AM")); 
					}else{
						eventDate = dateFormat.parse(event.getEventDateTime());
					}
				}catch(Exception e){
					eventDate = new Date();
				}
				daysDifference = DateUtil.getDifferenceMintues(curDate, eventDate);
				event.setDayDifference(daysDifference);
				
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}
				event.setEventImageUrl(applicationPlatForm);
				URLUtil.getShareLinkURL(event, customer,applicationPlatForm);*/
				
				List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
				if(null == artistIds || artistIds.isEmpty() || artistIds.size() > 1){
					continue;
				}
				event.setArtistIds(artistIds);
				
				List<Event> list = artistEventmap.get(event.getArtistId());
				
				if(null != list && !list.isEmpty()){
					artistEventmap.get(artistIds.get(0)).add(event);
				}else{
					list = new ArrayList<Event>();
					list.add(event);
					artistEventmap.put(artistIds.get(0), list);
				}
			}
			
			System.out.println("HOMECARDS_MOBILE: Grouping all events - Ends "+new Date());
			
			Card card = null;
			List<Card> cards = new ArrayList<Card>();
			
			pageNumber = pageNumber-1;
			
			int i =1 , cardTypeCount =(maxRows * pageNumber)+1, totalCount=1;
			
			/*List<Cards> cardsList = DAORegistry.getCardsDAO().getAllCardsByProductId(1);
			Map<Integer, Cards> cardsPositionMap = new HashMap<Integer, Cards>();
			for (Cards ticTrackerCard : cardsList) {
				cardsPositionMap.put(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE)?ticTrackerCard.getDesktopPosition():ticTrackerCard.getMobilePosition(), ticTrackerCard);
			}*/
			
			Map<String, List<Event>> map = new HashMap<String, List<Event>>();
						
			for (Integer artistId : artistEventmap.keySet()) {
				List<Event> artisEvents = artistEventmap.get(artistId);
				Collections.sort(artisEvents, TicketUtil.eventDaysComparatorForEventDateAsc);
				String key = artisEvents.get(0).getDayDifference()+"_"+artistId;
				map.put(key, artisEvents);
			}
			
			List<String> keyList = new ArrayList<String>(map.keySet());
			Collections.sort(keyList, TicketUtil.comparatorCardsAscByEventDate);
			
			System.out.println("HOMECARDS_MOBILE: Cards grouping logic - Begins "+new Date());
			for (String key : keyList) {
				
			  Integer artistId =Integer.parseInt(key.split("_")[1]);
              card=  new Card();
              Boolean isFavouriteArtist = false;
              List<Event> finalEvents = new ArrayList<Event>();
              List<Event> artisEvents = map.get(key);
              Event event = artisEvents.get(0);
              card.setArtistId(artistId); 
              //Get the team card image for artist
              card.setCardImage(TeamCardImageUtil.getPrarentCategoryImage(event.getParentCategoryId()));
              card.setCardImage("");
              card.setCardType(CardType.TEAM_CARD);
              card.setIsFavoriteArtist(isFavouriteArtist);
              card.setCardName(event.getArtistName());
              card.setArtistId(event.getArtistId());
              finalEvents = new ArrayList<Event>();
              finalEvents.add(event);
              card.setEvents(finalEvents);
                
              cardTypeCount++;
              cards.add(card);
              i++;
            
              if(totalCount > 25){
                  break;
              }
              totalCount++;
			}
			
			System.out.println("HOMECARDS_MOBILE: Cards grouping logic - Ends "+new Date());
			
			System.out.println("HOMECARDS_MOBILE LOGICAL EXCUTION - ENDS: "+new Date());
			eventList.setAlertMessage("");
	        eventList.setCards(cards);
			eventList.setStatus(1);
			eventList.setShowMoreButton(showMoreButton);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Success");
			return eventList;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Oops something went wrong. Please try search again.");
			eventList.setError(error);
			eventList.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETHOMECARDS,"Oops something went wrong. Please try search again.");
			return eventList;
		}
	}
	
	

	@RequestMapping(value="/NormalizedSearch",method=RequestMethod.POST)
	public @ResponsePayload EventResult search(HttpServletRequest request, Model model,HttpServletResponse response){
		EventResult eventList =new EventResult();
		Error error = new Error();
		long startTime = new Date().getTime();
		//System.out.println("Search Start : "+new Date());
		Date stDate = new Date();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"You are not authorized");
				return eventList;
			}
//			Integer configId= null;
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"You are not authorized");
						return eventList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"You are not authorized");
					return eventList;
				}
			}else{
				error.setDescription("You are not authorized.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"You are not authorized");
				return eventList;
			}
			String productTypeStr = request.getParameter("productType");
			String ipAddress = (null != request.getParameter("clientIPAddress") && 
					!request.getParameter("clientIPAddress").isEmpty())?request.getParameter("clientIPAddress"):ip;
			String deviceId = request.getParameter("deviceId");
			String searchType = request.getParameter("searchType");
			String searchKey = request.getParameter("searchKey");
			String platForm = request.getParameter("platForm");
			
			if(TextUtil.isEmptyOrNull(searchType)){
				error.setDescription("Search Type is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Search Type is mandatory");
				return eventList;
			}
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Product Type is mandatory");
				return eventList;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Please send valid product type");
					return eventList;
				}
			}
			
			
			if(TextUtil.isEmptyOrNull(ipAddress)){
				error.setDescription("Customer IP Address is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Customer IP Address mandatory");
				return eventList;
			}
			
			if(TextUtil.isEmptyOrNull(deviceId)){
				error.setDescription("Customer Device Id is Mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Customer Device Id is Mandatory");
				return eventList;
			}
			
			if(searchType.equals("AUTOSEARCH")){
				
				if(TextUtil.isEmptyOrNull(searchKey)){
					error.setDescription("Please enter a search keyword");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Please enter a search keyword");
					return eventList;
				}
				if(searchKey != null && !searchKey.equals("")) {
					searchKey = searchKey.replaceAll("\\W+", "%");
				}
				
				AutoSearchResult autoSearchResult = getAutosearchResults(searchKey);
				long autoSearchDBTime = new Date().getTime()-(startTime);
				//System.out.println("AutoSearchDB "+" : "+searchKey+" : Time : "+autoSearchDBTime);
				
				eventList.setAutoSearchResult(autoSearchResult);
				eventList.setNormalSearchResult(null);
				eventList.setStatus(1);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Success");
				return eventList;
			}
			
			String artistIdStr = request.getParameter("artistId");
			String venueIdStr = request.getParameter("venueId");
			String rtfCatSearchIdStr = request.getParameter("rtfCatSearchId");
			String grandChildCategoryIdStr = request.getParameter("grandchildId");
			String childIdStr = request.getParameter("childId");
			String parentCatIdStr = request.getParameter("parentCatId");
			String customerIdStr = request.getParameter("customerId");
			String startDateStr = request.getParameter("startDate");
			String endDateStr = request.getParameter("endDate");
			String pageNumberStr = request.getParameter("pageNumber");
			String artistPageNumberStr = request.getParameter("artistPageNumber");
			String venuePageNumberStr = request.getParameter("venuePageNumber");
			String tabType = request.getParameter("tabType");
			
			String locationOption = request.getParameter("locationOption"); //Enabled,Disabled,AllLocation
			//String latitude = request.getParameter("latitude");
			//String longitude = request.getParameter("longitude");
			String state = request.getParameter("state");
			//String city = request.getParameter("city");
			String getAll = request.getParameter("getAll");
			String isOtherGridStr = request.getParameter("isOtherGrid");
			String aRefValueStr = request.getParameter("aRefValue");
			String aRefTypeStr = request.getParameter("aRefType");
						
			if(TextUtil.isEmptyOrNull(isOtherGridStr)){
				isOtherGridStr =  "false";
			}
			
			if(TextUtil.isEmptyOrNull(locationOption)){
				error.setDescription("Location Option is mandatory.");
				eventList.setError(error);
				eventList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Location Option is mandatory");
				return eventList;
			}
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			Integer pageNumber = null,artistPageNumber=null,venuePageNumber=null;
			if(searchType.equals("NORMALSEARCH")) {
				if(TextUtil.isEmptyOrNull(tabType)){
					error.setDescription("Tab Type is mandatory.");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Tab Type is mandatory");
					return eventList;
				}
				if(tabType.equals("ALL") || tabType.equals("EVENTS")) {
					if(TextUtil.isEmptyOrNull(pageNumberStr)){
						error.setDescription("Page Number is mandatory.");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Page Number is mandatory");
						return eventList;
					}
					if(pageNumberStr !=null && !pageNumberStr.isEmpty()){
						try{
							pageNumber = Integer.parseInt(pageNumberStr.trim());
						}catch(Exception e){
							error.setDescription("Page Number should be Integer");
							eventList.setError(error);
							eventList.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Page Number should be Integer");
							return eventList;
						}
					}
				}
				
			} else {
				
				tabType="NOTAB";
				
				if(TextUtil.isEmptyOrNull(pageNumberStr)){
					error.setDescription("Page Number is mandatory.");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Page Number is mandatory");
					return eventList;
				}	
			}
			if(pageNumber == null) {
				pageNumber = 1;
			}
			if(artistPageNumber == null) {
				artistPageNumber = 1;
			}
			if(venuePageNumber == null) {
				venuePageNumber = 1;
			}
			
			if(TextUtil.isEmptyOrNull(searchKey)){
				
				boolean isSearchKeyRequired = true;
				if(!TextUtil.isEmptyOrNull(parentCatIdStr)){
					isSearchKeyRequired = false;
				}else if(!TextUtil.isEmptyOrNull(childIdStr)){
					isSearchKeyRequired = false;
				}else if(!TextUtil.isEmptyOrNull(grandChildCategoryIdStr)){
					isSearchKeyRequired = false;
				}else if(!TextUtil.isEmptyOrNull(rtfCatSearchIdStr)){
					isSearchKeyRequired = false;
				}else if(!TextUtil.isEmptyOrNull(artistIdStr)){
					isSearchKeyRequired = false;
				}else if(!TextUtil.isEmptyOrNull(venueIdStr)){
					isSearchKeyRequired = false;
				}
				
				if(isSearchKeyRequired){
					error.setDescription("Please enter a search keyword");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Please enter a search keyword");
					return eventList;
				}
			}else{
				boolean keyIsNotParent = true;
				
				if(null != parentCatIdStr && !parentCatIdStr.isEmpty() && parentCatIdStr.equals("0")) {
					searchKey ="";
				}
				String tempKey = searchKey.toUpperCase().trim();
				
				for (ParentType parentType : ParentType.values()) {
					
					if(!keyIsNotParent){
						break;
					}
					
					switch (parentType) {
					
						case SPORT:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="1";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.SPORTS);
							}
							break;
							
						case SPORTS:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="1";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.SPORTS);
							}
							break;
							
						case CONCERT:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="2";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.CONCERTS);
							}
							break;
							
						case CONCERTS:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="2";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.CONCERTS);
							}
							break;
							
						case THEATER:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="3";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.THEATRE);
							}
							break;

							
						case THEATERS:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="3";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.THEATRE);
							}
							break;
							
						case THEATRE:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="3";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.THEATRE);
							}
							break;

							
						case THEATRES:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="3";
								keyIsNotParent = false;
								searchKey = String.valueOf(ParentType.THEATRE);
							}
							break;

							
						case FAMILY:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="4";
								keyIsNotParent = false;
								searchKey = "OTHER";
							}
							break;
							
						case FAMILYS:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="4";
								keyIsNotParent = false;
								searchKey = "OTHER";
							}
							break;
							
						case FAMILIES:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="4";
								keyIsNotParent = false;
								searchKey = "OTHER";
							}
							break;
							
						case CHILD:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="4";
								keyIsNotParent = false;
								searchKey = "OTHER";
							}
							break;
							
						case CHILDREN:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="4";
								keyIsNotParent = false;
								searchKey = "OTHER";
							}
							break;
							
						case CHILDRENS:
							if(tempKey.equalsIgnoreCase(parentType.toString())){
								parentCatIdStr="4";
								keyIsNotParent = false;
								searchKey = "OTHER";
							}
							break;
	
						default:
							break;
					}
				}
				if(keyIsNotParent){
					if(!TextUtil.isEmptyOrNull(rtfCatSearchIdStr)){
						RTFCategorySearch searchObj = RTFCategorySearchUtil.getRTFCategoryById(Integer.parseInt(rtfCatSearchIdStr.trim()));
						if(null != searchObj){
							grandChildCategoryIdStr = searchObj.getGrandChildId().toString();
							childIdStr = searchObj.getChildId().toString();
						}
					}else{
						
						
						RTFCategorySearch searchObj = RTFCategorySearchUtil.getRTFCategoryBySearchKey(searchKey);
						if(null != searchObj){
							rtfCatSearchIdStr = searchObj.getId().toString();
							grandChildCategoryIdStr = searchObj.getGrandChildId().toString();
							childIdStr = searchObj.getChildId().toString();
						}
					}
				}
			}
			if(searchKey != null && !searchKey.equals("")) {
				searchKey = searchKey.replaceAll("\\W+", "%");
			}
			
			Integer artistId = null;
			if(artistIdStr !=null && !artistIdStr.isEmpty()){
				try{
					artistId = Integer.parseInt(artistIdStr.trim());
				}catch(Exception e){
					error.setDescription("Artist Id should be Integer");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Artist Id should be Integer");
					return eventList;
				}
			}
			String aRefValue = null;
			ArtistReferenceType aRefType = null;
			
			if((aRefValueStr !=null && !aRefValueStr.isEmpty() && !aRefValueStr.equals("\"\"") && !aRefValueStr.toLowerCase().equals("null") && !aRefValueStr.toLowerCase().equals("\"null\"")) ||
					(aRefTypeStr != null && !aRefTypeStr.isEmpty() && !aRefTypeStr.equals("\"\"") && !aRefTypeStr.toLowerCase().equals("null") && !aRefTypeStr.toLowerCase().equals("\"null\""))){
				try {
					aRefType = ArtistReferenceType.valueOf(aRefTypeStr);
					
					if(aRefType == null) {
						error.setDescription("Artist Ref Type is not valid");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Artist Ref Type is not valid");
						return eventList;
					}
					try{
						aRefValue = aRefValueStr;
						if(aRefType.equals(ArtistReferenceType.PARENT_ID) || aRefType.equals(ArtistReferenceType.CHILD_ID) ||
								aRefType.equals(ArtistReferenceType.GRANDCHILD_ID) || aRefType.equals(ArtistReferenceType.RTFCATS_ID)) {
							Integer.parseInt(aRefValue);
						}
					}catch(Exception e){
						error.setDescription("Artist Ref Value is not valid for given ref Type");
						eventList.setError(error);
						eventList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Artist Ref Value is not valid for given ref Type");
						return eventList;
					}
				} catch(Exception e){
					System.out.println("arefTy : "+aRefTypeStr+" : arefVal : "+aRefValueStr+" ser : "+searchKey);
					error.setDescription("Artist Ref Type or Ref Value is not valid");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Artist Ref Type or Ref Value is not valid");
					return eventList;
				}
			}
			
			Integer venueId = null;
			if(venueIdStr !=null && !venueIdStr.isEmpty()){
				try{
					venueId = Integer.parseInt(venueIdStr.trim());
				}catch(Exception e){
					error.setDescription("Venue Id should be Integer");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Venue Id should be Integer");
					return eventList;
				}
			}
			
			Boolean showAllEvents = false;
			Integer parentId = null;
			ParentCategory parentCategoryObj = null;
			if(parentCatIdStr !=null && !parentCatIdStr.isEmpty()){
				try{
					parentId = Integer.parseInt(parentCatIdStr.trim());
					
					if(parentId > 0) {
						parentCategoryObj =  DAORegistry.getParentCategoryDAO().get(parentId);
						if(null == parentCategoryObj){
							error.setDescription("Parent Catgeory is not valid");
							eventList.setError(error);
							eventList.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Parent Catgeory is not valid");
							return eventList;
						}
					}else {
						showAllEvents = true;
					}
					
				}catch(Exception e){
					error.setDescription("Parent Catgeory Id should be Integer");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Parent Catgeory Id should be Integer");
					return eventList;
				}
			}
			
			Integer grandChildId = null;
			if(grandChildCategoryIdStr !=null && !grandChildCategoryIdStr.isEmpty()){
				try{
					grandChildId = Integer.parseInt(grandChildCategoryIdStr.trim());
				}catch(Exception e){
					error.setDescription("Grand Child Id should be Integer");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Grand Child Id should be Integer");
					return eventList;
				}
			}
			Integer childId = null;
			if(childIdStr !=null && !childIdStr.isEmpty()){
				try{
					childId = Integer.parseInt(childIdStr.trim());
				}catch(Exception e){
					error.setDescription("Child Id should be Integer");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Child Id should be Integer");
					return eventList;
				}
			}
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Date eventStartDate = null, eventEndDate=null;
			Boolean isDateSearch = false;
			if(startDateStr !=null && !startDateStr.isEmpty()){
				try{
					eventStartDate = df.parse(startDateStr.trim());
				}catch(Exception e){
					error.setDescription("Valid Start Date Format is MM/dd/yyyy");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Valid Start Date Format is MM/dd/yyyy");
					return eventList;
				}
				isDateSearch = true;
			}
			
			if(endDateStr !=null && !endDateStr.isEmpty()){
				try{
					eventEndDate = df.parse(endDateStr.trim());
				}catch(Exception e){
					error.setDescription("Valid End Date Format is MM/dd/yyyy");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Valid End Date Format is MM/dd/yyyy");
					return eventList;
				}
				isDateSearch = true;
			}
			
			
			Integer rtfCatSearchId = null;
			if(!TextUtil.isEmptyOrNull(rtfCatSearchIdStr)){
				try{
					rtfCatSearchId = Integer.parseInt(rtfCatSearchIdStr.trim());
				}catch(Exception e){
					error.setDescription("RTF Category Search Id should be Integer");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"RTF Category Search Id should be Integer");
					return eventList;
				}
			}
			
			List<String> zipCodes = new ArrayList<String>();
			String nearestZipCodes = "",alertMessage="";
			boolean zipCodeFalg = false;
			
			if(locationOption.equals("Enabled")){
				if(TextUtil.isEmptyOrNull(state)){
					error.setDescription("State is mandatory.");
					eventList.setError(error);
					eventList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"State is mandatory");
					return eventList;
				}
				zipCodeFalg = true;
			}
			long validateTime = new Date().getTime()-startTime;
			//System.out.println("Validation Time : "+validateTime+" : "+searchKey);
			
			Customer customer = null;
			
			if(null != customerIdStr && !customerIdStr.isEmpty()){
				customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
			}
			String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			
			int maxRow = 0;
			if(getAll != null && !getAll.isEmpty()) {
				maxRow = PaginationUtil.eventMaxRows;
			} else {
				maxRow = searchType.equals("AUTOSEARCH")?PaginationUtil.eventMaxRows:PaginationUtil.normalSearchEventMaxRows;
			}
			
			long preProcssTime = new Date().getTime()-(startTime+validateTime);
			//System.out.println("PreProcess Time : "+preProcssTime+" : "+searchKey);
			
			if(!searchType.equals("AUTOSEARCH")){
				maxRow =25;
			}
			if(nearestZipCodes != null && nearestZipCodes.isEmpty()) {
				nearestZipCodes = null;
			}
			HashMap<String, Object> allObj = new HashMap<String, Object>();
			
			boolean isLocationFilterApplied = false,dateFilterApplied = false;
			
			if(null != state && !state.isEmpty()){
				isLocationFilterApplied = true;
			}
			
			if(null != startDateStr && !startDateStr.isEmpty() || null != endDateStr && !endDateStr.isEmpty() ){
				dateFilterApplied = true;
			}
			
			Boolean isOtherGrid = Boolean.valueOf(isOtherGridStr);
			
			 
			allObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(showAllEvents,searchKey, pageNumber, maxRow,startDateStr,
					endDateStr,artistId,grandChildId,venueId,childId,searchType,artistPageNumber,venuePageNumber,
					tabType,parentId,null,state,aRefValue,aRefType);
			
			if(tabType.equals("EVENTS") && pageNumber.equals(1) && (isLocationFilterApplied || dateFilterApplied)){
				ArrayList<Integer> eventIds = allObj.get("eventIds") != null ? (ArrayList<Integer>) (Object) allObj.get("eventIds") : new ArrayList<Integer>();
				if(eventIds.size() <= 20){
					int otherEventsMaxRow = 25;//PaginationUtil.otherEventGridMaxRows;
					HashMap<String, Object> otherLocObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(showAllEvents,searchKey, pageNumber, 
							otherEventsMaxRow,null,null,artistId,grandChildId,venueId,childId,searchType,
							artistPageNumber,venuePageNumber,"EVENTS",parentId,null,null,null,null);
					ArrayList<Event> otherEventList = otherLocObj.get("event") != null ? (ArrayList<Event>) (Object) otherLocObj.get("event") : new ArrayList<Event>();
					allObj = SearchConstructionUtil.getOtherLocationEvents(allObj,eventIds, otherEventList, searchKey, isLocationFilterApplied, state, pageNumber,otherEventsMaxRow);
					allObj.put("showOtherEventGrid", true);
				}
			}
			
			List<AutoPageResult> autoPageResult = allObj.get("autoPageResultList") != null ? (ArrayList<AutoPageResult>) (Object) allObj.get("autoPageResultList") : null;
			
			if(searchType.equals("AUTOSEARCH")){
				long autoSearchDBTime = new Date().getTime()-(startTime+validateTime+preProcssTime);
				//System.out.println("AutoSearchDB "+" : "+searchKey+" : Time : "+autoSearchDBTime);
				AutoSearchResult autoSearchResult = new AutoSearchResult();
				boolean showSeeAll = false;
				maxRow = PaginationUtil.autoSearchMaxRows;
				int i =1 ;
				List<AutoPageResult> autoPageResultNew = new ArrayList<AutoPageResult>();
				if(autoPageResult.size() > 0){
					for (AutoPageResult result : autoPageResult) {
						if(!CategorySearchMechanism.skipThisArtist(result.getId(), result.getName(),searchKey,null,null)){
							continue;
						}
						autoPageResultNew.add(result);
						i++;
					}
					if(i > maxRow){
						showSeeAll = true;
					}
				}
				autoSearchResult.setAutoPageResults(autoPageResultNew);
				autoSearchResult.setShowAutoSeeAll(showSeeAll);
				eventList.setAutoSearchResult(autoSearchResult);
				eventList.setNormalSearchResult(null);
				eventList.setStatus(1);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Success");
				return eventList;
			}
			
			Boolean showOtherEventGrid = allObj.get("showOtherEventGrid") != null ? (Boolean) allObj.get("showOtherEventGrid") : false;
			Boolean showMoreEvents = allObj.get("showMoreEvents") != null ? (Boolean) allObj.get("showMoreEvents") : false;
			Boolean showMoreOtherEvents = allObj.get("showMoreOtherEvents") != null ? (Boolean) allObj.get("showMoreOtherEvents") : false;
			
			String artistRefType = allObj.get("aRefType") != null ? (String) allObj.get("aRefType") : "";
			String artistRefValue = allObj.get("aRefValue") != null ? (String) allObj.get("aRefValue") : "";
			
			ArrayList<Event> events = allObj.get("event") != null ? (ArrayList<Event>) (Object) allObj.get("event") : new ArrayList<Event>();
			ArrayList<Event> otherEvents = allObj.get("OtherEvents") != null ? (ArrayList<Event>) (Object) allObj.get("OtherEvents") : new ArrayList<Event>();
			
			
			int eventsCount = 0;
			if(events != null && events.size() > 0) {
				eventsCount = events.size();
			}
			if(maxRow <= eventsCount) {
				showMoreEvents = true;
			} else {
				showMoreEvents = false;
			}
			int artistCount = 0, venueCount = 0;
			
			long eventSearchDBTime = new Date().getTime()-(startTime+validateTime+preProcssTime);
			//System.out.println("eventSearchDB Time : "+eventSearchDBTime+" : "+searchKey);
			
			long artistSearchDBTime = new Date().getTime()-(startTime+validateTime+preProcssTime+eventSearchDBTime);
			
			List<GrandChildCategory> rtfCatSearchList = null;
			Boolean isSportsChild = false;

			if(null != parentCategoryObj  && parentCategoryObj.getName().equals("SPORTS")) {
				isSportsChild = true;
			}
			
			long grandChildSearchDBTime = new Date().getTime()-(startTime+validateTime+preProcssTime+eventSearchDBTime+artistSearchDBTime);
			//System.out.println("GrandChildSearchDBTime Time : "+grandChildSearchDBTime+" : "+searchKey);
			
			int totalEventCount = 0, totalArtistCount=0,totalVenueCount=0;
			 
			ArtistResult artistResult = null;
			VenueResult venueResult = null;
			AutoSearchResult autoSearchResult = new AutoSearchResult();
			NormalSearchResult normalSearchResult = new NormalSearchResult();
			 
			List<Event> normalSearchEvents = new ArrayList<Event>();
			List<ArtistResult> normalSearchArtists = new ArrayList<ArtistResult>();
			Date std = new Date();
			int maxEventCount = 0,maxArtistCount=0,maxVenuecount = 0,i=0;
			for (Event event : events) {
				
				totalEventCount = event.getTotalEvents();
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}*/
				normalSearchEvents.add(event);
				maxEventCount++;
				i++;
			}
			
			//System.out.println("event POST Process Time :"+searchKey+" : time : " +(new Date().getTime()-std.getTime()));
			std = new Date();
			
			List<Event> allOtherEvents = new ArrayList<Event>();
			
			for (Event event : otherEvents) {
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}*/
				 
				allOtherEvents.add(event);
			}
			//System.out.println("Other event POST Process Time :"+searchKey+" : time : "+(new Date().getTime()-std.getTime()));
			std = new Date();
			 
			//System.out.println("Artist POST Process Time :"+searchKey+" : time : "+(new Date().getTime()-std.getTime()));
			std = new Date();
			
			 
			if(events != null && events.size()>0) {
				totalEventCount = events.get(0).getTotalEvents();
			}
			 
			//System.out.println("Venue POST Process Time :"+searchKey+" : time : "+(new Date().getTime()-std.getTime()));
			std = new Date();
			normalSearchResult.setSportsChildOrTeam(isSportsChild);
			normalSearchResult.setArtistResults(normalSearchArtists);
			normalSearchResult.setEvents(normalSearchEvents);
			normalSearchResult.setOtherEvents(allOtherEvents);
			normalSearchResult.setShowOtherEvents(showOtherEventGrid);
			normalSearchResult.setOtherEventShowMore(showMoreOtherEvents);
			normalSearchResult.setEventShowMore(showMoreEvents);
			
			if(artistRefType == null || artistRefValue== null ||
					artistRefType.equals("") || artistRefValue.equals("")) {
				artistRefType = "";
				artistRefValue = "";
			}
			normalSearchResult.setaRefType(artistRefType);
			normalSearchResult.setaRefValue(artistRefValue);
				
			//System.out.println("NML SEARCH END : "+searchKey+" : "+ (new Date().getTime()-stDate.getTime())+" : "+new Date());
			//System.out.println("GENERALISEDSEARCH LOGICAL EXCUTION - ENDS: "+new Date());
			eventList.setAutoSearchResult(autoSearchResult);
			eventList.setNormalSearchResult(normalSearchResult);
			eventList.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Success");
			//long postProcTime = new Date().getTime()-(startTime+validateTime+preProcssTime+eventSearchDBTime+artistSearchDBTime+grandChildSearchDBTime);
			//System.out.println("FinalPostProceTime Time 2 : "+postProcTime+" : "+searchKey);
			//System.out.println("Search End : "+new Date());
			return eventList;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getLocalizedMessage());
			error.setDescription("Error occured while getting events.");
			eventList.setError(error);
			eventList.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Error occured while getting events");
			return eventList;
		}
	}
	
	public AutoSearchResult getAutosearchResults(String searchKey) {
		
		HashMap<String, Object> allObj = new HashMap<String, Object>();
		allObj = DAORegistry.getEventDAO().getAutoSearchResultNew(searchKey, 1, PaginationUtil.eventMaxRows);
		
		List<AutoPageResult> autoPageResult = allObj.get("autoPageResultList") != null ? (ArrayList<AutoPageResult>) (Object) allObj.get("autoPageResultList") : null;
	
		AutoSearchResult autoSearchResult = new AutoSearchResult();
		boolean showSeeAll = false;
		int maxRow = PaginationUtil.autoSearchMaxRows;
		int i =1 ;
		List<AutoPageResult> autoPageResultNew = new ArrayList<AutoPageResult>();
		if(autoPageResult.size() > 0) {
			for (AutoPageResult result : autoPageResult) {
				if(!CategorySearchMechanism.skipThisArtist(result.getId(), result.getName(),searchKey,null,null)){
					continue;
				}
				autoPageResultNew.add(result);
				i++;
			}
			if(i > maxRow){
				showSeeAll = true;
			}
		}
		autoSearchResult.setAutoPageResults(autoPageResultNew);
		autoSearchResult.setShowAutoSeeAll(showSeeAll);
		return autoSearchResult;
	}
	
}	