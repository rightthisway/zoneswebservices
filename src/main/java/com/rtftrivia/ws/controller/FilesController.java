package com.rtftrivia.ws.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.google.common.io.ByteStreams;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerTicketDownloads;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.enums.FilesType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.filter.SecurityUtil;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.ShareDriveUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.list.Downloaddetails;
import com.zonesws.webservices.utils.list.GenericResponseDTO;
import com.zonesws.webservices.utils.list.TicketDownloadNotification;
import com.zonesws.webservices.utils.mail.MailAttachment;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;


@Controller
@RequestMapping({"/GetFile","/DownloadOrderTickets","/FileUploadToServer","/GetProfileImageFile" , "/GenericFileUpload" , "/UploadGiftCards","/UploadVideo"})

public class FilesController {
	private final Logger log = LoggerFactory.getLogger(FilesController.class);
	public static String svgMapBasePath = "GetFile";
	public static String profileImagePath = "GetProfileImageFile";
	
	@RequestMapping(value="/GetFile")
	public void getFile(HttpServletRequest request,Model model,HttpServletResponse response, ModelMap map){
		try {
			
			String fileType = request.getParameter("ft");
			String fileName = request.getParameter("fn");
			
			FilesType filesType = FilesType.valueOf(fileType);
			
			String folderPath = "";
			
			switch (filesType) {
			
				case eventmap:
					folderPath = URLUtil.getSVGMapPath();
					break;
					
				case ordermap:
					folderPath = URLUtil.getStoredSVGMapPath();
					break;
					
				case regularcard:
					folderPath = URLUtil.getCardsImagePath();
					break;
					
				case explorecard:
					folderPath = URLUtil.getExploreCardsImagePath();
					break;
					
				case regularimages:
					folderPath = URLUtil.getRegularImagePath();
					break;
					
				case profilepic:
					folderPath = URLUtil.getCustomerProfileImagePathOld();
					break;
					
				case paymenticon:
					folderPath = URLUtil.getPaymentIconImagePath();
					break;
					
				case promoofferimages:
					folderPath = URLUtil.getPromotionalDialogImagePath();
					break;
	
				default:
					break;
			}
			System.out.println("=================================SMB- Begins===================================");
			System.out.println("SMB: FilesType : "+filesType);
			String url = folderPath+fileName;
			System.out.println("SMB: URL : "+url);
			SmbFile svgTxtFile = ShareDriveUtil.getSmbFile(url);
			System.out.println("SMB: FILE : "+svgTxtFile);
			OutputStream out = null;
			SmbFileInputStream in = null;
			try {
				response.reset();
	        	response.setContentType(svgTxtFile.getContentType());
	        	response.setContentLength((int)svgTxtFile.length());
			    out = response.getOutputStream();
			    in = new SmbFileInputStream(svgTxtFile);
			    
			    byte[] b = new byte[8192];
		        int n, tot = 0;
			    while(( n = in.read( b )) > 0 ) {
		            out.write( b, 0, n );
		        }
			    
			    System.out.println("SMB: SUCCESS->"+new Date());
			    System.out.println("=================================SMB- Ends===================================");
			    
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				if(in != null) {
					in.close();
				}
				if(out != null) {
					out.close();
				} 
			}
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("SMB: FAILURE->"+new Date());
			System.out.println("=================================SMB- Ends-  Exception===================================");
		}
	}
	
	
	@RequestMapping(value="/GetProfileImageFile")
	public void getProfileImageFile(HttpServletRequest request,Model model,HttpServletResponse response, ModelMap map){
		try {
			
			String fileType = request.getParameter("ft");
			String fileName = request.getParameter("fn");
			FilesType filesType = FilesType.valueOf(fileType);
			String folderPath = URLUtil.getCustomerProfileImagePathOld();
			 
			System.out.println("=================================SMB PROFILE PICS- Begins===================================");
			System.out.println("SMB: FilesType : "+filesType);
			String url = folderPath+fileName;
			System.out.println("SMB PROFILE PICS: URL : "+url);
			SmbFile svgTxtFile = ShareDriveUtil.getSmbFile(url);
			System.out.println("SMB PROFILE PICS: FILE : "+svgTxtFile);
			OutputStream out = null;
			SmbFileInputStream in = null;
			try {
				response.reset();
	        	response.setContentType(svgTxtFile.getContentType());
	        	response.setContentLength((int)svgTxtFile.length());
			    out = response.getOutputStream();
			    in = new SmbFileInputStream(svgTxtFile);
			    
			    byte[] b = new byte[8192];
		        int n, tot = 0;
			    while(( n = in.read( b )) > 0 ) {
		            out.write( b, 0, n );
		        }
			    
			    System.out.println("SMB: SUCCESS->"+new Date());
			    System.out.println("=================================SMB PROFILE PICS- Ends===================================");
			    
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				if(in != null) {
					in.close();
				}
				if(out != null) {
					out.close();
				} 
			}
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("SMB: FAILURE->"+new Date());
			System.out.println("=================================SMB PROFILE PICS- Ends-  Exception===================================");
		}
	}
	

	
	 @RequestMapping(value = "/DownloadOrderTickets")
	    public String download(HttpServletRequest request,Model model,HttpServletResponse response, String file)  {
	    	Downloaddetails downloadDetails = new Downloaddetails();	
	    	Error error = new Error();
			 try {
				HttpSession session = request.getSession();
				String ip =(String)session.getAttribute("ip");
				String configIdString = request.getParameter("cId");
				String platform = request.getParameter("platForm");
				String encryptedStr = request.getQueryString();
				String clientIPAddress = request.getParameter("cTrack");
				
				encryptedStr = encryptedStr.replace("identity=","");
				String keyString = ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey();
				String customerId = "";
				String inoiceNoStr = "",ticketAttachedIdStr="";	
				String decodedText = SecurityUtil.doDecryption(encryptedStr, keyString);
				String origText[] = decodedText.split("_");
				configIdString = origText[0];
				
				if(configIdString!=null && !configIdString.isEmpty()){
					try {
//						configId=Integer.parseInt(configIdString);
						if(DAORegistry.getIpConfigDAO().getIpConfigByIpAndConfigId(configIdString,ip)==null){
							error.setDescription("You are not authorized.");
							downloadDetails.setError(error);
							downloadDetails.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
							
						
						}
					} catch (Exception e) {
						error.setDescription("You are not authorized.");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
						
					}
				}else{
					error.setDescription("You are not authorized.");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
					
				}
				
				if(encryptedStr != null && !encryptedStr.isEmpty()){
					decodedText = SecurityUtil.doDecryption(encryptedStr, keyString);
					String origText1[] = decodedText.split("_");
					customerId = origText1[1];
					inoiceNoStr = origText1[2];
					ticketAttachedIdStr = origText1[3];
				}

				Integer invoiceId = null;
				Invoice invoice = null;
				try{
					invoiceId=Integer.parseInt(inoiceNoStr.trim());
					invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
				}catch(Exception e){
					error.setDescription("OrderNumber is Mandatory");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"OrderNumber is Mandatory");
					//return downloadDetails;
				}
				Integer ticketAttachedId = null;
				InvoiceTicketAttachment attachment = null;
				try{
					ticketAttachedId=Integer.parseInt(ticketAttachedIdStr.trim());
					
					attachment = DAORegistry.getInvoiceTicketAttachmentDAO().get(ticketAttachedId);
					
					if(attachment == null){
						error.setDescription("Ticket Attachment Id is not valid");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket Attachment Id is not valid");
					}
					
				}catch(Exception e){
					error.setDescription("Ticket Attachment Id is Mandatory");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket Attachment Id is Mandatory");
					//return downloadDetails;
				}
				if(TextUtil.isEmptyOrNull(customerId)){
					error.setDescription("Customet ID  is mandatory");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Customet ID is mandatory");
					//return downloadDetails;
				}
				
				if(URLUtil.isSharedDriveOn) {
					SmbFileInputStream in = null;
					OutputStream out = null;
					try {
						String url = URLUtil.findETicketDownloadURLFromDirectory(attachment.getFilePath());
						SmbFile svgTxtFile = ShareDriveUtil.getSmbFile(url);
						
						response.reset();
						
						response.setContentType("application/octet-stream");
			            response.setContentLength((int) (svgTxtFile.length()));
			            response.setHeader("Content-Disposition", "attachment; filename=\"" + svgTxtFile.getName() + "\"");
			            
						//response.setContentType(svgTxtFile.getContentType());
			        	//response.setContentLength((int)svgTxtFile.length());
					    
			            out = response.getOutputStream();
					    in = new SmbFileInputStream(svgTxtFile);
					    
					    byte[] b = new byte[8192];
				        int n, tot = 0;
					    while(( n = in.read( b )) > 0 ) {
				            out.write( b, 0, n );
				        }
					    try{	
			            	 CustomerTicketDownloads ticketDownload = new CustomerTicketDownloads();
			                 ticketDownload.setCustomerId(Integer.parseInt(customerId));
			                 ticketDownload.setInvoiceId(invoiceId);
			                 ticketDownload.setOrderId(null != invoice?invoice.getId():null);
			                 ticketDownload.setDownloadDateTime(new Date());
			                 ticketDownload.setTicketName(attachment.getFilePath());
			                 ticketDownload.setTicketType(attachment.getType().toString());
			                 ticketDownload.setPlatform(platform);
			                 ticketDownload.setIpAddress(null != clientIPAddress && !clientIPAddress.isEmpty()?clientIPAddress:ip);
			                 DAORegistry.getCustomerTicketDownloadsDAO().save(ticketDownload);
			            }catch(Exception e){
			            	e.printStackTrace();
			            }
					    
					}catch(Exception e) {
						e.printStackTrace();
			            log.error(e.getMessage(), e);
			            error.setDescription("Ticket not found for this order");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket not found for this order");
					}finally {
						if(in != null) {
							in.close();
						}
						
						if(out != null) {
							out.close();
						} 
					}
				}else {
					File ticketFile = new File (URLUtil.findETicketDownloadURLFromDirectory(attachment.getFilePath()));
			        log.trace("Write response...");
			        InputStream fileInputStream = null;
			        OutputStream output = null;
			        try{
			        	fileInputStream = new FileInputStream(ticketFile);
			        	output = response.getOutputStream(); 

			            response.reset();

			            response.setContentType("application/octet-stream");
			            response.setContentLength((int) (ticketFile.length()));
			            response.setHeader("Content-Disposition", "attachment; filename=\"" + ticketFile.getName() + "\"");

			            IOUtils.copyLarge(fileInputStream, output);
			            output.flush();
			            try{	
			            	 CustomerTicketDownloads ticketDownload = new CustomerTicketDownloads();
			                 ticketDownload.setCustomerId(Integer.parseInt(customerId));
			                 ticketDownload.setInvoiceId(invoiceId);
			                 ticketDownload.setOrderId(null != invoice?invoice.getId():null);
			                 ticketDownload.setDownloadDateTime(new Date());
			                 ticketDownload.setTicketName(attachment.getFilePath());
			                 ticketDownload.setTicketType(attachment.getType().toString());
			                 ticketDownload.setPlatform(platform);
			                 ticketDownload.setIpAddress(null != clientIPAddress && !clientIPAddress.isEmpty()?clientIPAddress:ip);
			                 DAORegistry.getCustomerTicketDownloadsDAO().save(ticketDownload);
			            }catch(Exception e){
			            	e.printStackTrace();
			            }
			        } catch (IOException e) {
			        	e.printStackTrace();
			            log.error(e.getMessage(), e);
			            error.setDescription("Ticket not found for this order");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket not found for this order");
						//return downloadDetails;
			        }finally{
			        	if(null != output){
			        		output.close();
			        	}
			        	if(null != fileInputStream){
			        		fileInputStream.close();
			        	}
			        }
				}
			
			
	    } catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while downloading.");
			downloadDetails.setError(error);
			downloadDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Error occured while gettting customer details");
		}

	    return null;
	    }
	 
	 
	 	@RequestMapping(value="/FileUploadToServer")
		public @ResponsePayload TicketDownloadNotification sendMail(
				HttpServletRequest request,HttpServletResponse response){
			Error error = new Error();
			TicketDownloadNotification invoiceRefundResponse = new TicketDownloadNotification();
			try {
				
				
				Boolean attachTickets = true;
				MailAttachment[] ticketAttachments =null;
				if(attachTickets){ 
					
					List<MultipartFile> files = new ArrayList<MultipartFile>();
					
					MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					files.add(multipartRequest.getFile("file_1"));
					 
					Map<String, List<String>> filesMap = new HashMap<String, List<String>>();
					
					int ticketAttachementSize = 0;
					
					if(files != null && files.size() > 0) {
						
						for (MultipartFile multipartFile : files) {
							
							String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
							String filePath = Util.getTicketDownloadFilePath(multipartFile.getOriginalFilename());
							BufferedOutputStream stream = null;
							try {
								SmbFile file = ShareDriveUtil.getSmbFile(filePath);
								stream = new BufferedOutputStream(file.getOutputStream());
								FileCopyUtils.copy(multipartFile.getInputStream(), stream);
								
								List<String> filePaths= filesMap.get(multipartFile.getContentType());
								if(null != filePaths && !filePaths.isEmpty()){
									filePaths.add(filePath);
								}else{
									filePaths = new ArrayList<String>();
									filePaths.add(filePath);
								}
								filesMap.put(multipartFile.getContentType(), filePaths);
								ticketAttachementSize++;
								
							}catch(Exception e) {
								e.printStackTrace();
							}finally {
								if(null != stream) {
									stream.close();
								}
							}
						}
					}
					
					 if(filesMap!= null && !filesMap.isEmpty()){
						 ticketAttachments = new MailAttachment[ticketAttachementSize];
						 int i=0;
						 for (String mimeType : filesMap.keySet()) {
							List<String> filePaths= filesMap.get(mimeType);
							for (String filePath : filePaths) {
								SmbFile file = ShareDriveUtil.getSmbFile(filePath);
								BufferedInputStream stream = new BufferedInputStream(file.getInputStream());
								byte[] bytes = ByteStreams.toByteArray(stream);
								String fileName = Util.getFileNameFromPath(filePath);
								String ext = FilenameUtils.getExtension(fileName);
								ticketAttachments[i] = new MailAttachment(bytes,mimeType,"Tickets"+(i+1)+"."+ext,filePath);
								i++;
							} 
						 }
					 }
				}
				
				//RTFOrderDownloadNotification.testNotification(ticketAttachments);
				invoiceRefundResponse.setMessage("Success");
				invoiceRefundResponse.setStatus(1);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Success");
				return invoiceRefundResponse;
			} catch (Exception e) {
				System.out.println("Ticket Upload is failed");
				e.printStackTrace();
				invoiceRefundResponse.setMessage("Fail");
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Ticket Upload is failed");
				return invoiceRefundResponse;
			}
		}
	 	
	 	
	 	 
	 	 
		 @RequestMapping(value = "/UploadGiftCards", headers=("content-type=multipart/*"), method = RequestMethod.POST)
		 public @ResponsePayload TicketDownloadNotification uploadGiftCards(@RequestParam("file") MultipartFile inputFile , @RequestParam("uploadPath") String uploadPath ) {	
		  
	 	  HttpHeaders headers = new HttpHeaders();		  
		  TicketDownloadNotification response  = new TicketDownloadNotification();
		  
		  if (inputFile.isEmpty()) {
			  response.setMessage("File is Empty or Corrupted");
			  response.setStatus(0);
			  return response;
		  }	
		  String originalFilename = "";
		  String destPath = "";
		 
		  try {			
			
			destPath = URLUtil.getGiftCardAbsoluteAssetPath(uploadPath); 
		    originalFilename = inputFile.getOriginalFilename(); 
		    System.out.println(" File Name uploaded is " +    originalFilename    + "destPath " + destPath);	 
		    File destinationFile = new File(destPath +  File.separator + originalFilename);	    
		    System.out.println( " [ CARD FILE PATH ] " + destinationFile.getAbsolutePath());	    
		    inputFile.transferTo(destinationFile);
		    response.setMessage("File is Uploaded Successfully");
			response.setStatus(1);
		   } catch (Exception e) { 
			   
			   response.setMessage("Failed Uploading Filee " + originalFilename );
			   response.setStatus(0);
			  	  
		   }
		   return response;
		 }	
	 	  	 	 
	 	 
	/* 	 

	 		public static void main(String[] args) {
	 			
	 			// the file we want to upload
	 			File inFile = new File("C:/REWARDTHEFAN/GiftCards/TMP/Chrysanthemum.jpg");
	 			FileInputStream fis = null;
	 			
	 			try {
	 				
	 				fis = new FileInputStream(inFile);
	 				DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());	 				
	 				// RTF API URL [ ?uploadPath=GCMD ] for Gift Card  or [ ?uploadPath=GCMD ] for Gift Card Order.
	 				HttpPost httppost = new HttpPost("http://127.0.0.1:7779/UploadGiftCards?");
	 				
	 				@SuppressWarnings("deprecation")
					MultipartEntity entity = new MultipartEntity();
	 				// set the file input stream and file name as arguments
	 				entity.addPart("file", new InputStreamBody(fis, inFile.getName()));
	 				httppost.setEntity(entity);	 				
	 				HttpResponse response = httpclient.execute(httppost);	 				
	 				int statusCode = response.getStatusLine().getStatusCode();
	 				HttpEntity responseEntity = response.getEntity();
	 				String responseString = EntityUtils.toString(responseEntity, "UTF-8");	 				
	 				System.out.println("[" + statusCode + "] " + responseString);
	 				
	 				
	 				
	 			} catch (ClientProtocolException e) {
	 				System.err.println("Unable to make connection");
	 				e.printStackTrace();
	 			} catch (IOException e) {
	 				System.err.println("Unable to read file");
	 				e.printStackTrace();
	 			} finally {
	 				try {
	 					if (fis != null) fis.close();
	 				} catch (IOException e) {}
	 			}
	 		}*/
		 
		 
		 
		 @RequestMapping(value = "/UploadVideo", headers=("content-type=multipart/*"), method = RequestMethod.POST)
		 public @ResponsePayload GenericResponseDTO UploadVideo(@RequestParam("file") MultipartFile inputFile , @RequestParam("cuId") String cuId ) {	
		  
	 	  HttpHeaders headers = new HttpHeaders();		  
	 	 GenericResponseDTO response  = new GenericResponseDTO();
		  
		  if (inputFile.isEmpty()) {
			  response.setMessage("File is Empty or Corrupted");
			  response.setStatus(0);
			  return response;
		  }	
		 
		  if(TextUtil.isEmptyOrNull(cuId)) {
			  response.setMessage("Please Send Valid Customer Id");
			  response.setStatus(0);
			  return response;
		  }
		  Customer customer = null;
		  try {
		   customer = CustomerUtil.getCustomerById(Integer.valueOf(cuId));
		  }catch(Exception ex) {
			  response.setMessage("Please Send Valid Customer Id");
			  response.setStatus(0);
			  return response;
		  }
		  
		  if(customer == null) {
			  response.setMessage("Customer Not Found.Please Send Valid Customer Id");
			  response.setStatus(0);
			  return response;
		  }
		  
		  String originalFilename = "";
		  String destPath = "";
		 
		  try {			
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); 
			String uploadDate = dateFormat.format(date);
			uploadDate = uploadDate.replace("/", "-");
			destPath = URLUtil.getVideoUploadAbsolutePath(); 
		    originalFilename = inputFile.getOriginalFilename(); 
		    String newfileName = cuId + "_"  + uploadDate;
 		    System.out.println(" File Name uploaded is " +    newfileName    + "destPath " + destPath);	 
		    File destinationFile = new File(destPath +  File.separator + newfileName);	    
		    System.out.println( " [ Video  FILE PATH ] " + destinationFile.getAbsolutePath());	    
		    inputFile.transferTo(destinationFile);
		    response.setMessage(originalFilename + " File is Uploaded Successfully & new File name is " + destPath +  File.separator + newfileName );
			response.setStatus(1);
		   } catch (Exception e) { 
			   
			   e.printStackTrace();
			   
			   response.setMessage("Failed Uploading Filee " + originalFilename );
			   response.setStatus(0);
			  	  
		   }
		   return response;
		 }	
	 	  	 	 

		 
	 		 
	 	
    
}