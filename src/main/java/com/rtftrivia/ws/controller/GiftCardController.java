package com.rtftrivia.ws.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.RtfCustomerRewardRedemptionHistory;
import com.rtfquiz.webservices.data.RtfGiftCardRespose;
import com.rtfquiz.webservices.enums.RedeemType;
import com.rtfquiz.webservices.utils.RTFBotsUtil;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.GiftCardBrand;
import com.zonesws.webservices.data.GiftCardBrandDTO;
import com.zonesws.webservices.data.GiftCardFileAttachment;
import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.data.RtfGiftCard;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.data.RtfGiftCardQuantity;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PointRedemptionType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.enums.TransactionType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.utils.CountryUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.GiftCardUtil;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.CustomerOrderResponse;
import com.zonesws.webservices.utils.list.RtfOrderConfirmation;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/v1/GetGiftCardBrands","/v1/GetGiftCardsByBrand","/v1/CreateGiftCardOrder","/v1/ViewGiftCardOrder","/v1/ListMyGiftCards"})
public class GiftCardController {
	
	private MailManager mailManager;
	private ZonesProperty properties;
	public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	@RequestMapping(value="/v1/GetGiftCardBrands",method=RequestMethod.POST)
	public @ResponsePayload RtfGiftCardRespose getGiftCardBrands(HttpServletRequest request,HttpServletResponse response){
		RtfGiftCardRespose cards =new RtfGiftCardRespose();
		Error error = new Error();
		String pageNoStr = request.getParameter("pNo");
		String customerIdStr = request.getParameter("cId");
		String platForm = request.getParameter("pfm"); 
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see tickets.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Please login to see Gift Cards");
				return cards;
			}
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Customer is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Customer is not recognized");
				return cards;
			}
			List<GiftCardBrand> list = DAORegistry.getGiftCardBrandDAO().getAllActiveGiftCardBrands();
			List<GiftCardBrandDTO> responseList = new ArrayList<GiftCardBrandDTO>();
			GiftCardBrandDTO object = new GiftCardBrandDTO();
			for (GiftCardBrand giftCardBrand : list) {
				List<RtfGiftCard> giftCardQtyList =DAORegistry.getQueryManagerDAO().getAvailableGiftCardsByBrand(giftCardBrand.getId()); 
				String imageUrl = giftCardBrand.getImageUrl();
				Boolean noactiveQtyflag = false;
				if(null == giftCardQtyList || giftCardQtyList.isEmpty() || giftCardQtyList.size() <= 0) {
					if(!giftCardBrand.getDisplayOutOfStack()) {
						continue;
					}
					noactiveQtyflag = true;
					imageUrl = giftCardBrand.getOutOfStackImageUrl();
				}
				object = new GiftCardBrandDTO();
				object.setbId(giftCardBrand.getId());
				object.setbName(giftCardBrand.getName());
				object.setbDesc(giftCardBrand.getDescription());
				object.setImgUrl(AWSFileService.getGiftCardBrandImage(imageUrl));
				object.setNacards(noactiveQtyflag);
				responseList.add(object);
			}
			cards.setSts(1);
			cards.setGfBrands(responseList);
			return cards;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something is not right. You do not have any gift cards right now.!");
			cards.setErr(error);
			cards.setSts(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Error Occured While Getting Gift Card Brands");
			return cards;
		}
	}
	
	

	@RequestMapping(value="/v1/GetGiftCardsByBrand",method=RequestMethod.POST)
	public @ResponsePayload RtfGiftCardRespose GetGiftCardsByBrand(HttpServletRequest request,HttpServletResponse response){
		RtfGiftCardRespose cards =new RtfGiftCardRespose();
		Error error = new Error();
		String pageNoStr = request.getParameter("pNo");
		String customerIdStr = request.getParameter("cId");
		String giftCardBrandIdStr = request.getParameter("gfcbId");
		String platForm = request.getParameter("pfm"); 
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see tickets.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Please login to see Gift Cards");
				return cards;
			}
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Customer is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Customer is not recognized");
				return cards;
			}
			
			Integer giftCardBrandId = null;
			GiftCardBrand giftCardBrand = null;
			try {
				giftCardBrandId=Integer.parseInt(giftCardBrandIdStr);
				giftCardBrand = DAORegistry.getGiftCardBrandDAO().get(giftCardBrandId);
				if(giftCardBrand==null){
					error.setDescription("Gift Card Brand is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Gift Card Brand is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Gift Card Brand is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Gift Card Brand is not recognized");
				return cards;
			}
			
			List<RtfGiftCard> list =DAORegistry.getQueryManagerDAO().getAvailableGiftCardsByBrand(giftCardBrandId); 
			for(RtfGiftCard card : list){
				List<RtfGiftCardQuantity> qList = new ArrayList<RtfGiftCardQuantity>();
				for(int i=1;i<=card.getMaxThresholdQty();i++){
					RtfGiftCardQuantity quantity = new RtfGiftCardQuantity();
					quantity.setTotalAmount(card.getAmount()*i);
					if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){ 
						quantity.setReqRewardDollars(card.getRewardDollarConvRate() * (card.getAmount()*i));
						quantity.setReqRewardPoints(card.getRewardPointConvRate() * (card.getAmount()*i));
						Double lp = card.getLoyaltyPointsConvRate() * (card.getAmount()*i);
						quantity.setReqLoyaltyPoints(lp.intValue());
					}else{ 
						quantity.setReqRewardDollars(card.getRewardDollarConvRate() * (card.getAmount()*i));
						quantity.setReqRewardPoints(card.getRewardPointConvRate() * (card.getAmount()*i));
						Double lp = card.getLoyaltyPointsConvRate() * (card.getAmount()*i);
						quantity.setReqLoyaltyPoints(lp.intValue());
					}
					quantity.setRedeemPopupMsg("");
					quantity.setPopupMsgRDollars("Total reward dollars spend : $"+quantity.getReqRewardDollarString());
					quantity.setPopupMsgRPoints("Total reward points spend : "+quantity.getReqRewardPointString());
					quantity.setPopupMsgLoyaltyPoints("Total loyalty points spend : "+quantity.getReqLoyaltyPoints());
					quantity.setQty(i);
					qList.add(quantity);
				}
				if(card.getMaxThresholdQty() >= 1) {
					if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){ 
						card.setPopupMsgRDollars(("Total reward dollars spend : $"+TicketUtil.getRoundedValueString(card.getRewardDollarConvRate() * (card.getAmount()))));
						card.setPopupMsgRPoints(("Total reward points spend : "+TicketUtil.getRoundedValueString(card.getRewardPointConvRate() * (card.getAmount()))));
						Double lp = card.getLoyaltyPointsConvRate() * (card.getAmount());
						card.setPopupMsgLoyaltyPoints("Total loyalty points spend : "+lp);
					}else{ 
						card.setPopupMsgRDollars(("Total reward dollars spend : $"+TicketUtil.getRoundedValueString(card.getRewardDollarConvRate() * (card.getAmount()))));
						card.setPopupMsgRPoints("Total reward points spend : "+TicketUtil.getRoundedValueString(card.getRewardPointConvRate() * (card.getAmount())));
						Double lp = card.getLoyaltyPointsConvRate() * (card.getAmount());
						card.setPopupMsgLoyaltyPoints("Total loyalty points spend : "+lp);
					}
				}
				card.setImageUrl(AWSFileService.getGiftCardBrandImage(giftCardBrand.getImageUrl()));
				card.setQuantity(qList);
			}
			 
			cards.setSts(1);
			cards.setCards(list);
			return cards;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while getting gift cards.");
			cards.setErr(error);
			cards.setSts(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Error occured while getting gift cards");
			return cards;
		}
	}
	
	@RequestMapping(value="/v1/CreateGiftCardOrder",method=RequestMethod.POST)
	public synchronized @ResponsePayload RtfOrderConfirmation createGiftCardOrder(HttpServletRequest request,HttpServletResponse response){
		RtfOrderConfirmation orderConfirmation = new RtfOrderConfirmation();
		Error error = new Error();
		String gcIdStr = request.getParameter("gcId");
		String customerIdStr = request.getParameter("cId");
		String qtyStr = request.getParameter("qty");
		String redeemTypeStr = request.getParameter("redeemType"); //rdollars, rpoints
		String platFormStr = request.getParameter("pfm");
		String shippingIdStr = request.getParameter("sId");
		
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to create order.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Please login to create order");
				return orderConfirmation;
			}
			if(null == gcIdStr || gcIdStr.isEmpty()) {
				error.setDescription("Gift Card is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Gift Card is not recognized");
				return orderConfirmation;
			}
			if(null == qtyStr || qtyStr.isEmpty()) {
				error.setDescription("Please select valid Quantity.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Please select valid Quantity");
				return orderConfirmation;
			}
			
			if(null == redeemTypeStr || redeemTypeStr.isEmpty()) {
				error.setDescription("Please select valid redeemption type.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Please select valid redeemption type");
				return orderConfirmation;
			}
			if(null == shippingIdStr || shippingIdStr.isEmpty()) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			Integer shippingId = Integer.parseInt(shippingIdStr);
			UserAddress shipping = DAORegistry.getUserAddressDAO().get(shippingId);
			
			if(null == shipping) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			Integer customerId = null;
			Integer qty = null;
			Integer gcvId = null;
			Customer customer = null;
			RtfGiftCard card = null;
			RtfGiftCardQuantity giftCardQty = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				 
				customer = DAORegistry.getCustomerDAO().get(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Customer is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Customer is not recognized");
				return orderConfirmation;
			}
			try {
				qty=Integer.parseInt(qtyStr);
			} catch (Exception e) {
				error.setDescription("Please select valid Quantity.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Please select valid Quantity");
				return orderConfirmation;
			}
			try {
				gcvId=Integer.parseInt(gcIdStr);
				giftCardQty = DAORegistry.getRtfGiftCardQuantityDAO().get(gcvId);
				card = DAORegistry.getRtfGiftCardDAO().get(giftCardQty.getCardId());
				if(card == null || !card.getStatus() || giftCardQty == null){
					error.setDescription("Gift card id is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Gift card id is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Gift card id is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Gift card id is not recognized");
				return orderConfirmation;
			}
			
			if(qty > giftCardQty.getMaxThresholdQty()){
				error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"User trying to purchase more than the threshold qty.");
				return orderConfirmation;
			}
			
			RedeemType redeemType = RedeemType.rdollars;
			try {
				redeemType = RedeemType.valueOf(redeemTypeStr);
			}catch(Exception e) {
				error.setDescription("Please select valid redeemption type.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Please select valid redeemption type.");
				return orderConfirmation;
			}
			
			if(GiftCardUtil.enableNextOrder) {
				
				/*Date lastPurcahseDate = DAORegistry.getQueryManagerDAO().getRecentOrderDate(customerId);
				
				if(null != lastPurcahseDate) {
					
					Boolean blockPurchase = GiftCardUtil.validatePurchase(lastPurcahseDate);
					
					if(null != blockPurchase && blockPurchase){
						error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"User already purcahsed maximum therhold qty..");
						return orderConfirmation;
					}
				}*/
				
				String curDate = dateFormat.format(new Date());
				String fromDateStr = curDate+" 00:01:00.000",endDateStr = curDate+" 23:59:59.000";;
				Boolean isCustomerCreatedSingleOrder = DAORegistry.getQueryManagerDAO().isCustomerCreatedSingleOrder(customerId,fromDateStr,endDateStr);
				
				if(null != isCustomerCreatedSingleOrder && isCustomerCreatedSingleOrder) {
					error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"User already purcahsed maximum therhold qty..");
					return orderConfirmation;
				}
				
			}else {
				Integer qtyPurchasedByThisCust = DAORegistry.getQueryManagerDAO().getTotalPurchasedQtyByCustomerAndGiftCard(customerId, gcvId);
				
				System.out.println("GIFTCARD ORDER:  CARD ID: "+giftCardQty.getCardId()+", GCVIDDB: "+giftCardQty.getId()+", Gift Card Value ID: "+gcvId+", CustomerId: "+customerId+", QTY: "+qty+", THreshold QTY: "+giftCardQty.getMaxThresholdQty()+", QTY PUrcahsed: "+qtyPurchasedByThisCust);
				
				if(null != qtyPurchasedByThisCust && qtyPurchasedByThisCust >= giftCardQty.getMaxThresholdQty()){
					error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"User already purcahsed maximum therhold qty..");
					return orderConfirmation;
				}
			}
			
			
			if(qty > giftCardQty.getFreeQty()){
				error.setDescription("We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				return orderConfirmation;
			}
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			CustomerLoyalty customerRewards = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			
			if(customerRewards == null){
				customerRewards = new CustomerLoyalty();
				customerRewards.setActivePoints("0.00");
				customerRewards.setActivePointsAsDouble(0.00);
				customerRewards.setActiveRewardDollers(0.00);
				customerRewards.setContestRewardDollars("0.00");
				customerRewards.setCurRewardPointBalance("0.00");
				customerRewards.setCustomerId(customerId);
				customerRewards.setDollerConversion(1.00);
				customerRewards.setLastAffiliateReferralDollars(0.00);
				customerRewards.setLastReferralDollars(0.00);
				customerRewards.setLastUpdate(new Date());
				customerRewards.setLatestEarnedPoints("0.00");
				customerRewards.setLatestEarnedPointsAsDouble(0.00);
				customerRewards.setLatestSpentPointsAsDouble(0.00);
				customerRewards.setPendingPoints("0.00");
				customerRewards.setPendingPointsAsDouble(0.00);
				customerRewards.setRevertedPoints("0.00");
				customerRewards.setRevertedPointsAsDouble(0.00);
				customerRewards.setShowAffiliateReward(false);
				customerRewards.setShowReferralReward(true);
				customerRewards.setTotalAffiliateReferralDollars(0.00);
				customerRewards.setTotalAffiliateReferralDollarsStr("0.00");
				customerRewards.setTotalEarnedPoints("0.00");
				customerRewards.setTotalEarnedPointsAsDouble(0.00);
				customerRewards.setTotalReferralDollars(0.00);
				customerRewards.setTotalReferralDollarsStr("0.00");
				customerRewards.setTotalSpentPoints("0.00");
				customerRewards.setTotalSpentPointsAsDouble(0.00);
				customerRewards.setVoidedPoints("0.00");
				customerRewards.setVoidedPointsAsDouble(0.00);
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerRewards);
			}
			
			boolean isRewardDollars = false;
			
			Double activeRewards = 0.0, requiredAmount = 0.00, oldActiveRewards=0.00;
			
			if(redeemType.equals(RedeemType.rdollars)) {
				oldActiveRewards =customerRewards.getActivePointsAsDouble();
				if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
					requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardDollarConvRate());
				}else{
					requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardDollarConvRate());
				}
				activeRewards = TicketUtil.getRoundedUpValue(customerRewards.getActivePointsAsDouble());
				if(requiredAmount > activeRewards) {
					error.setDescription("You do not have enough reward dollars to redeem these gift card.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"You do not have enough reward dollars to redeem these gift card.");
					return orderConfirmation;
				}
				
				isRewardDollars = true;
			}else {
				
				activeRewards = customer.getRtfPoints().doubleValue();
				
				if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
					requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardPointConvRate());
				}else{
					requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardPointConvRate());
				}
				
				if(requiredAmount > activeRewards) {
					error.setDescription("You do not have enough reward points to redeem these gift card.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"You do not have enough reward points to redeem these gift card.");
					return orderConfirmation;
				}
			}
			
			List<Integer> botCustIds = RTFBotsUtil.getAllBots();
			Boolean isCustomerCreatedTwoOrder = DAORegistry.getQueryManagerDAO().isCustomerCreatedTwoOrder(customerId);
			if(botCustIds.contains(customerId) || isCustomerCreatedTwoOrder){
				error.setDescription("Sorry, another Fan has already redeemed this gift card.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Sorry, another Fan has already redeemed this gift card.");
				return orderConfirmation;
			}
			
			
			RtfGiftCardOrder order  = new RtfGiftCardOrder();
			order.setCardDescription(card.getDescription());
			order.setCardId(gcvId);
			order.setCardTitle(card.getTitle());
			order.setCreatedBy(customer.getUserId());
			order.setCreatedDate(new Date());
			order.setCustomerId(customerId);
			order.setIsEmailSent(false);
			order.setIsOrderFulfilled(false);
			order.setIsOrderFilledMailSent(false);
			order.setOrderType(OrderType.REGULAR);
			order.setOriginalCost(requiredAmount);
			order.setQuantity(qty);
			order.setRedeemedRewards(requiredAmount);
			order.setStatus("OUTSTANDING");
			order.setShippingAddressId(shippingId);
			order.setRedeemType(redeemType);
			
			RtfGiftCardQuantity validationObj = DAORegistry.getRtfGiftCardQuantityDAO().get(gcvId);
			if(validationObj.getFreeQty() == 0 || qty > validationObj.getFreeQty()){
				error.setDescription("We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				return orderConfirmation;
			}
			
			Integer freeQty = validationObj.getFreeQty()-qty;
			Integer usedQty = validationObj.getUsedQty()+qty;
			
			validationObj.setFreeQty(freeQty);
			validationObj.setUsedQty(usedQty);
			
			DAORegistry.getRtfGiftCardQuantityDAO().update(validationObj);
			DAORegistry.getRtfGiftCardOrderDAO().save(order);
			
			if(isRewardDollars) {
				customerRewards.setLastUpdate(new Date());
				customerRewards.setActivePointsAsDouble(customerRewards.getActivePointsAsDouble()-requiredAmount);
				customerRewards.setTotalSpentPointsAsDouble(customerRewards.getTotalSpentPointsAsDouble()+requiredAmount);
				customerRewards.setLatestSpentPointsAsDouble(requiredAmount);
				customerRewards.setLastUpdate(new Date());
			}
			
			
			try {
				UserAddress shippingAddr = DAORegistry.getUserAddressDAO().get(shippingId);
				CustomerOrderDetail orderDetail = new CustomerOrderDetail();
				orderDetail.setShippingAddress1(shippingAddr.getAddressLine1());
				orderDetail.setShippingAddress2(shippingAddr.getAddressLine2());
				orderDetail.setShippingCountryId(shippingAddr.getCountry().getId());
				orderDetail.setShippingStateId(shippingAddr.getState().getId());
				orderDetail.setShippingCity(shippingAddr.getCity());
				orderDetail.setShippingCountry(shippingAddr.getCountryName());
				orderDetail.setShippingEmail(shippingAddr.getEmail());
				orderDetail.setShippingFirstName(shippingAddr.getFirstName());
				orderDetail.setShippingLastName(shippingAddr.getLastName());
				orderDetail.setCtyPhCode(null != shippingAddr.getCtyPhCode() && !shippingAddr.getCtyPhCode().isEmpty()?shippingAddr.getCtyPhCode():CountryUtil.defaultCountryPhoneCode);
				orderDetail.setShippingPhone1(shippingAddr.getPhone1());
				orderDetail.setShippingPhone2(shippingAddr.getPhone2());
				orderDetail.setShippingState(shippingAddr.getStateName());
				orderDetail.setShippingZipCode(shippingAddr.getZipCode());
				orderDetail.setOrderId(order.getId());
				orderDetail.setOrderType("GIFTCARD");
				DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(isRewardDollars) {
				CustomerLoyaltyHistory offerRewardHistory = new CustomerLoyaltyHistory();
				offerRewardHistory.setCustomerId(customerId);
				offerRewardHistory.setOrderId(order.getId());
				offerRewardHistory.setRewardConversionRate(loyaltySettings.getRewardConversion());
				offerRewardHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
				offerRewardHistory.setPointsSpentAsDouble(requiredAmount);
				offerRewardHistory.setRewardSpentAmount(0.00);
				offerRewardHistory.setPointsEarnedAsDouble(0.00);
				offerRewardHistory.setRewardEarnAmount(0.00);
				offerRewardHistory.setInitialRewardPointsAsDouble(oldActiveRewards);
				offerRewardHistory.setInitialRewardAmount(customerRewards.getActiveRewardDollers());
				offerRewardHistory.setOrderType(OrderType.GIFTCARDORDER);
				offerRewardHistory.setCreateDate(new Date());
				offerRewardHistory.setUpdatedDate(new Date());
				offerRewardHistory.setOrderTotal(requiredAmount);
				offerRewardHistory.setOrderId(order.getId());
				offerRewardHistory.setRewardStatus(RewardStatus.ACTIVE);
				
				Double balanceRewardPointsNew = oldActiveRewards + requiredAmount - 0;
				Double balanceRewardAmountNew = RewardConversionUtil.getRewardDoller(balanceRewardPointsNew,loyaltySettings.getDollerConversion());
				
				offerRewardHistory.setBalanceRewardPointsAsDouble(balanceRewardPointsNew);
				offerRewardHistory.setBalanceRewardAmount(balanceRewardAmountNew);
				
				try {
					//NEED TO DISCUSS
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customerId, customerRewards.getActivePointsAsDouble());
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				DAORegistry.getCustomerLoyaltyDAO().update(customerRewards);
				DAORegistry.getCustomerLoyaltyHistoryDAO().save(offerRewardHistory);
			}else {
				customer.setRtfPoints(customer.getRtfPoints() - requiredAmount.intValue());
				DAORegistry.getCustomerDAO().updatePointsRedemption(customerId,  requiredAmount.intValue());
				
				RtfCustomerRewardRedemptionHistory history = new RtfCustomerRewardRedemptionHistory();
				history.setCreatedDate(new Date());
				history.setCustomerId(customerId);
				history.setRtfPointsDebited(requiredAmount.intValue());
				history.setPointRedemptionType(PointRedemptionType.GCORDER);
				history.setOrderId(order.getId());
				QuizDAORegistry.getRtfCustomerRewardRedemptionHistoryDAO().save(history);
			}
			 
			orderConfirmation.setoId(order.getId());
			
			/*if(null != platForm &&  platForm.equals(ApplicationPlatForm.ANDROID)) {
				orderConfirmation.setStatus(2);
				error.setDescription("Gift Card Order Created Successfully.");
				orderConfirmation.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDER,"Success");
				return orderConfirmation;
			}else {
				
			}*/
			orderConfirmation.setStatus(1);
			orderConfirmation.setMsg("Gift Card Order Created Successfully");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDER,"Success");
			return orderConfirmation;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error Occured while creating gift card order.");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDORDER,"Error Occured while creating gift card order.");
			return orderConfirmation;
		}
	}
	
	
	@RequestMapping(value="/v1/UpdateGiftCardOrder",method=RequestMethod.POST)
	public @ResponsePayload RtfOrderConfirmation updateGiftCardOrder(HttpServletRequest request,HttpServletResponse response){
		RtfOrderConfirmation orderConfirmation = new RtfOrderConfirmation();
		Error error = new Error();
		String oIdStr = request.getParameter("oId");
		String customerIdStr = request.getParameter("cId");
		String platFormStr = request.getParameter("pfm");
		String shippingIdStr = request.getParameter("sId");
		
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to create order.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Please login to create order");
				return orderConfirmation;
			}
			if(null == oIdStr || oIdStr.isEmpty()) {
				error.setDescription("Gift Card is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Gift Card is not recognized");
				return orderConfirmation;
			}
			 
			if(null == shippingIdStr || shippingIdStr.isEmpty()) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			Integer shippingId = Integer.parseInt(shippingIdStr);
			UserAddress shipping = DAORegistry.getUserAddressDAO().get(shippingId);
			
			if(null == shipping) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			Integer customerId = null;
			Integer qty = null;
			Integer oId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Customer is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Customer is not recognized");
				return orderConfirmation;
			}
			 
			
			RtfGiftCardOrder order = null;
			try {
				oId=Integer.parseInt(oIdStr);
				order = DAORegistry.getRtfGiftCardOrderDAO().get(oId);
				 
				if(order == null){
					error.setDescription("Gift card order is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Gift card order is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Gift card order is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Gift card order is not recognized");
				return orderConfirmation;
			}
			order.setShippingAddressId(shippingId);
			
			
			try {
				UserAddress shippingAddr = DAORegistry.getUserAddressDAO().get(shippingId);
				CustomerOrderDetail orderDetail = new CustomerOrderDetail();
				orderDetail.setShippingAddress1(shippingAddr.getAddressLine1());
				orderDetail.setShippingAddress2(shippingAddr.getAddressLine2());
				orderDetail.setShippingCountryId(shippingAddr.getCountry().getId());
				orderDetail.setShippingStateId(shippingAddr.getState().getId());
				orderDetail.setShippingCity(shippingAddr.getCity());
				orderDetail.setShippingCountry(shippingAddr.getCountryName());
				orderDetail.setShippingEmail(shippingAddr.getEmail());
				orderDetail.setShippingFirstName(shippingAddr.getFirstName());
				orderDetail.setShippingLastName(shippingAddr.getLastName());
				orderDetail.setShippingPhone1(shippingAddr.getPhone1());
				orderDetail.setShippingPhone2(shippingAddr.getPhone2());
				orderDetail.setShippingState(shippingAddr.getStateName());
				orderDetail.setShippingZipCode(shippingAddr.getZipCode());
				orderDetail.setOrderId(order.getId());
				orderDetail.setOrderType("GIFTCARD");
				DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			order.setStatus("OUTSTANDING");
			DAORegistry.getRtfGiftCardOrderDAO().update(order);
			orderConfirmation.setoId(order.getId());
			orderConfirmation.setStatus(1);
			orderConfirmation.setMsg("Order Updated Successfully");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATEGIFTCARDORDER,"Success");
			return orderConfirmation;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error Occured while updating gift card order.");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Error Occured while updating gift card order.");
			return orderConfirmation;
		}
	}
	
	
	
	
	@RequestMapping(value="/v1/ViewGiftCardOrder",method=RequestMethod.POST)
	public @ResponsePayload CustomerOrderResponse viewGiftCardOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerOrderResponse resObj = new CustomerOrderResponse();
		Error error = new Error();
		String orderIdStr = request.getParameter("oId");
		String customerIdStr = request.getParameter("cId");
		String platFormStr = request.getParameter("pfm"); 
		String msg = "";
		try {
			 
			String ip =((HttpServletRequest)request).getHeader("X-Forwarded-For");
			String configIdString = request.getParameter("configId");
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				msg ="Please login to see your gift card order.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,msg);
				return resObj;
			}
			
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = DAORegistry.getCustomerDAO().get(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Customer is not recognized");
					return resObj;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Customer is not recognized");
				return resObj;
			}
			
			Integer orderId = Integer.parseInt(orderIdStr);
			
			CustomerOrder order = new CustomerOrder();
			RtfGiftCardOrder giftCardOrder = DAORegistry.getRtfGiftCardOrderDAO().get(orderId);
			
			if(giftCardOrder == null) {
				error.setDescription("Invalid Order.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Invalid Order");
				return resObj;
			}
			
			if(!giftCardOrder.getCustomerId().equals(customerId)) {
				error.setDescription("You do not have the access to view this order..");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"You do not have access to view this order..");
				return resObj;
			}
			//CustomerLoyaltyHistory latDownloadRewardHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getLateDownloadCustomerLoyaltyHistoryByGiftCardOrderId(orderId);
			//CustomerLoyalty custLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			UserAddress shippingAddr = DAORegistry.getUserAddressDAO().get(giftCardOrder.getShippingAddressId());
			
			RtfGiftCardQuantity cardValueObj = DAORegistry.getRtfGiftCardQuantityDAO().get(giftCardOrder.getCardId());
			
			RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(cardValueObj.getCardId());
			if(null != card.getGfcBrandId()) {
				GiftCardBrand giftCardBrand = DAORegistry.getGiftCardBrandDAO().get(card.getGfcBrandId());
				card.setImageUrl(AWSFileService.getGiftCardBrandImage(giftCardBrand.getImageUrl()));
			}
			
			order.setId(giftCardOrder.getId());
			order.setCreateDateTemp(giftCardOrder.getCreatedDate());
			order.setQty(giftCardOrder.getQuantity());
			order.setOrderTotalAsDouble(giftCardOrder.getRedeemedRewards());
			order.setOriginalOrderTotal(giftCardOrder.getOriginalCost());
			
			String redeemText = order.getOrderTotal();
			if(null == giftCardOrder.getRedeemType() || giftCardOrder.getRedeemType().equals(RedeemType.rdollars)) {
				redeemText = redeemText+" Reward Dollars";
			}else {
				redeemText = redeemText+" Reward Points";
			}
			order.setRedeemText(redeemText);
			order.setTotalServiceFees(TicketUtil.getRoundedValueString(0.00));
			order.setSingleTixServiceFees(TicketUtil.getRoundedValueString(0.00));
			//order.setCustomerLoyaltyHistory(latDownloadRewardHistory);
			order.setCustomer(customer);
			order.setIsShippingRequired(true);
			order.setEventName(giftCardOrder.getCardTitle());
			order.setDesktopShareLinkUrl(card.getImageUrl());
			order.setDescription(card.getDescription());
			order.setGfCardValue(TicketUtil.getRoundedValueString(cardValueObj.getAmount()));
			 
			/*CustomerOrderDetail orderDetail = new CustomerOrderDetail();
			orderDetail.setShippingAddress1(shippingAddr.getAddressLine1());
			orderDetail.setShippingAddress2(shippingAddr.getAddressLine2());
			orderDetail.setShippingCity(shippingAddr.getCity());
			orderDetail.setShippingCountry(shippingAddr.getCountryName());
			//orderDetail.setShippingCountryId(shippingAddr.getCountry().getId());
			orderDetail.setShippingEmail(shippingAddr.getEmail());
			orderDetail.setShippingFirstName(shippingAddr.getFirstName());
			orderDetail.setShippingLastName(shippingAddr.getLastName());
			orderDetail.setShippingPhone1(shippingAddr.getPhone1());
			orderDetail.setShippingPhone2(shippingAddr.getPhone2());
			orderDetail.setShippingState(shippingAddr.getStateName());
			orderDetail.setShippingZipCode(shippingAddr.getZipCode());
			orderDetail.setOrderId(order.getId());*/
			
			CustomerOrderDetail orderDetail = DAORegistry.getCustomerOrderDetailDAO().getGiftCardCustomerOrderDetailByOrderId(giftCardOrder.getId());
			order.setCustomerOrderDetail(orderDetail);
			
			String fontSizeStr = TicketUtil.getFontSize(request, platForm);
			order.setViewOrderType(String.valueOf(giftCardOrder.getOrderType()));
			
			String orderDeliveryNote= null;
			if(giftCardOrder.getStatus().equalsIgnoreCase("PENDING")){
				order.setButtonOption("GETSHIPPING");
				order.setTicketDownloadUrl(null);
				order.setShowInfoButton(Boolean.TRUE);
				order.setOrderDeliveryNote("Update Shipping");
				order.setDeliveryInfo(null);
			} else if(giftCardOrder.getStatus().equalsIgnoreCase("VOIDED")){
				orderDeliveryNote = "Order Cancelled";
				order.setButtonOption("OC");
				order.setOrderDeliveryNote(orderDeliveryNote);
				order.setDeliveryInfo(null);
			}else if(giftCardOrder.getIsOrderFulfilled()){
				
				List<GiftCardFileAttachment> fileAttachments = DAORegistry.getGiftCardFileAttachmentDAO().getAllAttachemntByOrderId(giftCardOrder.getId());
				
				List<InvoiceTicketAttachment> tickets = new ArrayList<>();
				InvoiceTicketAttachment attachment = null;
				for (GiftCardFileAttachment attachmentObj : fileAttachments) {
					attachment = new InvoiceTicketAttachment();
					attachment.setDownloadUrl(URLUtil.getGiftCardDownloadUrl(request,attachmentObj.getId(), giftCardOrder.getId(), configIdString,customerId,platForm,ip));
					attachment.setType(attachmentObj.getType()); 
					attachment.setPosition(attachmentObj.getPosition());
					attachment.setExtension(URLUtil.getFileExtension(attachmentObj.getFilePath()));
					attachment.setFileName(URLUtil.getGiftCardFileName(attachmentObj.getFilePath()));
					attachment.setTrxType(TransactionType.GFC);
					tickets.add(attachment);
				}
				
				orderDeliveryNote = "Download Gift Cards";
				order.setOrderDeliveryNote(orderDeliveryNote);
				order.setButtonOption("DL");
				order.setInvoiceTicketAttachment(tickets);
				order.setShippingMethod("E-Ticket");
				order.setDeliveryInfo(null);
			}else if(giftCardOrder.getStatus().equalsIgnoreCase("OUTSTANDING")){
				order.setButtonOption("PROGRESS");
				order.setTicketDownloadUrl(null);
				order.setShowInfoButton(Boolean.TRUE);
				order.setOrderDeliveryNote("INFO");
				TicketUtil.getGiftCardDeliveryInFo(order,fontSizeStr);
				TicketUtil.getGiftCardProgressPopupMsg(order,fontSizeStr);
				order.setShippingMethod("E-Ticket");
				order.setDeliveryInfo(null);
			}
			
			String convRateString = "";
			try {
				if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
					convRateString = "Conversion Rate : "+(TicketUtil.getRoundedValue(card.getRewardDollarConvRate())*100)+"%";
				}else{
					convRateString = "Conversion Rate : "+TicketUtil.getRoundedValueString(card.getRewardDollarConvRate());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			order.setConvRateString(convRateString);
			
			//order.setAllCustomerRelatedObjects();
			
			
			List<CustomerOrder> customerOrderList = new ArrayList<CustomerOrder>();
			customerOrderList.add(order);
			//resObj.setCustRewards(custLoyalty.getActivePoints());
			resObj.setCustomerOrders(customerOrderList);
			resObj.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDERVIEW,"Success");
			return resObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Seems there was some issue while fecthing your order information.");
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Seems there was some issue while fecthing your order information.");
			return resObj;
		}
	}
	
	
	@RequestMapping(value="/v1/ListMyGiftCards",method=RequestMethod.POST)
	public @ResponsePayload CustomerOrderResponse listMyGiftCards(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerOrderResponse resObj = new CustomerOrderResponse();
		Error error = new Error();
		String oStatus = request.getParameter("oStatus"); //ACTIVE, PAST
		String customerIdStr = request.getParameter("cId");
		String platFormStr = request.getParameter("pfm"); 
		String msg = "";
		try {
			
			String ip =((HttpServletRequest)request).getHeader("X-Forwarded-For");
			String configIdString = request.getParameter("configId");
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				msg ="Please login to see your gift card orders.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,msg);
				return resObj;
			}
			
			if(null == oStatus || oStatus.isEmpty()) {
				msg ="Order Status is mandatory.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,msg);
				return resObj;
			}
			
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = DAORegistry.getCustomerDAO().get(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,"Customer is not recognized");
					return resObj;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,"Customer is not recognized");
				return resObj;
			}
			List<RtfGiftCardOrder> list = DAORegistry.getRtfGiftCardOrderDAO().getAllOrdersByCustomerId(customerId);
			if(list == null) {
				resObj.setCustomerOrders(new ArrayList<>());
				resObj.setStatus(1);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.LISTGIFTCARDS,"Success");
				return resObj;
			}
			
			CustomerOrder order = null;
			List<CustomerOrder> customerOrderList = new ArrayList<CustomerOrder>();
			for (RtfGiftCardOrder giftCardOrder : list) {
				
				order = new CustomerOrder();
				
				RtfGiftCardQuantity rtfGiftCardQuantity = DAORegistry.getRtfGiftCardQuantityDAO().get(giftCardOrder.getCardId());
				
				RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(rtfGiftCardQuantity.getCardId());
				
				if(null != card.getGfcBrandId()) {
					GiftCardBrand giftCardBrand = DAORegistry.getGiftCardBrandDAO().get(card.getGfcBrandId());
					card.setImageUrl(AWSFileService.getGiftCardBrandImage(giftCardBrand.getImageUrl()));
				}
				
				order.setId(giftCardOrder.getId());
				order.setCreateDateTemp(giftCardOrder.getCreatedDate());
				order.setQty(giftCardOrder.getQuantity());
				order.setOrderTotalAsDouble(giftCardOrder.getRedeemedRewards());
				order.setEventName(giftCardOrder.getCardTitle());
				order.setDesktopShareLinkUrl(card.getImageUrl());
				order.setDescription(card.getDescription());
				order.setGfCardValue(TicketUtil.getRoundedValueString(rtfGiftCardQuantity.getAmount()));
				
				String redeemText = order.getOrderTotal();
				if(null == giftCardOrder.getRedeemType() || giftCardOrder.getRedeemType().equals(RedeemType.rdollars)) {
					redeemText = redeemText+" Reward Dollars";
				}else if(giftCardOrder.getRedeemType().equals(RedeemType.loyaltypoints)){
					redeemText = redeemText+" Loyalty Points";
				}else {
					redeemText = redeemText+" Reward Points";
				}
				order.setRedeemText(redeemText);
				
				String fontSizeStr = TicketUtil.getFontSize(request, platForm);
				order.setViewOrderType(String.valueOf(giftCardOrder.getOrderType()));
				
				String orderDeliveryNote= null;
				
				if(giftCardOrder.getStatus().equalsIgnoreCase("PENDING")){
					order.setButtonOption("GETSHIPPING");
					order.setTicketDownloadUrl(null);
					order.setShowInfoButton(Boolean.TRUE);
					order.setOrderDeliveryNote("Update Shipping");
					order.setDeliveryInfo(null);
				} else if(giftCardOrder.getStatus().equalsIgnoreCase("VOIDED")){
					orderDeliveryNote = "Order Cancelled";
					order.setButtonOption("OC");
					order.setOrderDeliveryNote(orderDeliveryNote);
					order.setDeliveryInfo(null);
				}else if(giftCardOrder.getIsOrderFulfilled()){
					
					List<GiftCardFileAttachment> fileAttachments = DAORegistry.getGiftCardFileAttachmentDAO().getAllAttachemntByOrderId(giftCardOrder.getId());
					List<InvoiceTicketAttachment> tickets = new ArrayList<>();
					InvoiceTicketAttachment attachment = null;
					for (GiftCardFileAttachment attachmentObj : fileAttachments) {
						attachment = new InvoiceTicketAttachment();
						attachment.setDownloadUrl(URLUtil.getGiftCardDownloadUrl(request,attachmentObj.getId(), giftCardOrder.getId(), configIdString,customerId,platForm,ip));
						attachment.setType(attachmentObj.getType()); 
						attachment.setPosition(attachmentObj.getPosition());
						attachment.setExtension(URLUtil.getFileExtension(attachmentObj.getFilePath()));
						attachment.setFileName(URLUtil.getGiftCardFileName(attachmentObj.getFilePath()));
						attachment.setTrxType(TransactionType.GFC);
						tickets.add(attachment);
					}
					
					orderDeliveryNote = "Download Gift Cards";
					order.setOrderDeliveryNote(orderDeliveryNote);
					order.setButtonOption("DL");
					order.setInvoiceTicketAttachment(tickets);
					order.setShippingMethod("E-Ticket");
					order.setDeliveryInfo(null);
					
				}else if(giftCardOrder.getStatus().equalsIgnoreCase("OUTSTANDING")){
					order.setButtonOption("PROGRESS");
					order.setTicketDownloadUrl(null);
					order.setShowInfoButton(Boolean.TRUE);
					order.setOrderDeliveryNote("INFO");
					TicketUtil.getGiftCardDeliveryInFo(order,fontSizeStr);
					TicketUtil.getGiftCardProgressPopupMsg(order,fontSizeStr);
					order.setShippingMethod("E-Ticket");
					order.setDeliveryInfo(null);
				}
				String convRateString = "";
				try {
					if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
						convRateString = "Conversion Rate : "+(TicketUtil.getRoundedValue(card.getRewardDollarConvRate())*100)+"%";
					}else{
						convRateString = "Conversion Rate : "+TicketUtil.getRoundedValueString(card.getRewardDollarConvRate());
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				order.setConvRateString(convRateString);
				customerOrderList.add(order);
				
			}
			resObj.setCustomerOrders(customerOrderList);
			resObj.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.LISTGIFTCARDS,"Success");
			return resObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Seems there was some issue while fecthing your order information.");
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,"Seems there was some issue while fecthing your order information.");
			return resObj;
		}
	}
	
		 
}