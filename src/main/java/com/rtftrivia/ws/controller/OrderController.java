package com.rtftrivia.ws.controller;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.AddressInfo;
import com.zonesws.webservices.data.Affiliate;
import com.zonesws.webservices.data.AffiliateCashReward;
import com.zonesws.webservices.data.AffiliateCashRewardHistory;
import com.zonesws.webservices.data.AffiliatePromoCodeTracking;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.AutoCatsLockedTickets;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.CheckOutOfferAppliedAudit;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerFantasyOrder;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.CustomerOrderRequest;
import com.zonesws.webservices.data.CustomerSuperFan;
import com.zonesws.webservices.data.CustomerWallet;
import com.zonesws.webservices.data.CustomerWalletHistory;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.data.OrderPaymentBreakup;
import com.zonesws.webservices.data.OrderTicketGroup;
import com.zonesws.webservices.data.PaypalTracking;
import com.zonesws.webservices.data.PaypalTransactionDetail;
import com.zonesws.webservices.data.PreOrderPageAudit;
import com.zonesws.webservices.data.Property;
import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;
import com.zonesws.webservices.data.RTFOrderTracking;
import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.data.ReferredCodeTracking;
import com.zonesws.webservices.data.State;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.data.ZoneRGBColor;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.InvoiceStatus;
import com.zonesws.webservices.enums.LoyalFanType;
import com.zonesws.webservices.enums.OrderFetchType;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PartialPaymentMethod;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.PromotionalType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.enums.SecondaryOrderType;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.filter.SecurityUtil;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.ShareDriveUtil;
import com.zonesws.webservices.notifications.RTFOrderPlacedNotification;
import com.zonesws.webservices.util.service.CreditCardCheckout;
import com.zonesws.webservices.util.service.PayPalCheckout;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.FavoriteUtil;
import com.zonesws.webservices.utils.MapUtil;
import com.zonesws.webservices.utils.PaginationUtil;
import com.zonesws.webservices.utils.RTFAffiliateBrokerUtil;
import com.zonesws.webservices.utils.RTFPromotionalOfferUtil;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.CancelOrderResponse;
import com.zonesws.webservices.utils.list.CustomerOrderResponse;
import com.zonesws.webservices.utils.list.CustomerOrdersResponse;
import com.zonesws.webservices.utils.list.OrderConfirmation;
import com.zonesws.webservices.utils.list.OrderPaymentResponse;
import com.zonesws.webservices.utils.list.OrderTracking;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

import jcifs.smb.SmbFile;

@Controller
@RequestMapping({"/InitiateOrder","/ConfirmOrder","/InitiateTicTrackerOrder","/ConfirmTicTrackerOrder","/InitiateTicTrackerLongOrder",
	"/TrackingOrder","/ConfirmTicTrackerLongOrder","/ViewRTFOrder","/InitiateTicTrackerLongOrderNew","/ConfirmTicTrackerLongOrder",
	"/OrderPaymentCapture","/GetOrder","/ListOrders","/CancelOrder"})
public class OrderController implements ServletContextAware {
	
	@Autowired
	ServletContext context; 
	
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	public static DecimalFormat df = new DecimalFormat("#.##");
	private CreditCardCheckout creditCardCheckout;
	private PayPalCheckout payPalCheckout;
	private MailManager mailManager;
	private ZonesProperty properties;
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	public CreditCardCheckout getCreditCardCheckout() {
		return creditCardCheckout;
	}
	public void setCreditCardCheckout(CreditCardCheckout creditCardCheckout) {
		this.creditCardCheckout = creditCardCheckout;
	}
	
	public PayPalCheckout getPayPalCheckout() {
		return payPalCheckout;
	}
	public void setPayPalCheckout(PayPalCheckout payPalCheckout) {
		this.payPalCheckout = payPalCheckout;
	}
	
	public MailManager getMailManager() {
		
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	@RequestMapping(value = "/InitiateOrder",method = RequestMethod.POST)
	public @ResponsePayload OrderConfirmation initiateOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderConfirmation orderConfirmation = new OrderConfirmation();
		Error error = new Error();
		boolean noPayment = true;
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(noPayment) {
				error.setDescription("You can only redeem tickets using reward dollars");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You can only redeem tickets using reward dollars");
				return orderConfirmation;
			}
			
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}
			

			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String eventIdStr = request.getParameter("eventId");
			String categoryTicketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String orderTotalStr = request.getParameter("orderTotal");
			String isFanPriceStr = request.getParameter("isFanPrice");
			
			String billingAddressIdStr = request.getParameter("billingAddressId");
			String shippingAddressIdStr = request.getParameter("shippingAddressId");
			String shippingSameAsBilling = request.getParameter("shippingSameAsBilling");
			String loyaltySpentStr = request.getParameter("rewardPoints");
			String appPlatformStr = request.getParameter("platForm");
			String referralCode = request.getParameter("referralCode");
			String refCodeIdentity = request.getParameter("eventRefIdentity"); 
			String refCId = request.getParameter("cId"); 
			String bFirstName = request.getParameter("bFirstName");
			String bLastName = request.getParameter("bLastName");
			String bAddress1 = request.getParameter("bAddress1");
			String bAddress2 = request.getParameter("bAddress2");
			String bCity = request.getParameter("bCity");
			String bState = request.getParameter("bState");
			String bCountry = request.getParameter("bCountry");
			String bZipCode = request.getParameter("bZipCode");
			String bPhoneNumber = request.getParameter("bPhoneNumber");
			String bEmail = request.getParameter("bEmail");
			String sFirstName = request.getParameter("sFirstName");
			String sLastName = request.getParameter("sLastName");
			String sAddress1 = request.getParameter("sAddress1");
			String sAddress2 = request.getParameter("sAddress2");
			String sCity = request.getParameter("sCity");
			String sState = request.getParameter("sState");
			String sCountry = request.getParameter("sCountry");
			String sZipCode = request.getParameter("sZipCode");
			String sPhoneNumber = request.getParameter("sPhoneNumber");
			String sEmail = request.getParameter("sEmail");
			String onDayBeforeTheEventStr = request.getParameter("onDayBeforeTheEvent");
			
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String primaryTransactionId = request.getParameter("primaryTransactionId");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String secondaryTransactionId = request.getParameter("secondaryTransactionId");
			
			boolean onDayBeforeTheEvent = false;
			
			if(!TextUtil.isEmptyOrNull(onDayBeforeTheEventStr)){
				onDayBeforeTheEvent = Boolean.valueOf(onDayBeforeTheEventStr);
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Product Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Application Platform is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			System.out.println("CREATEORDER : CustomerId: "+customerIdStr+", EventId: "+eventIdStr+", CATTGID: "+categoryTicketGroupIdStr);
			
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Please send valid Event Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Event Id");
				return orderConfirmation;
			}
			if(TextUtil.isEmptyOrNull(categoryTicketGroupIdStr)){
				error.setDescription("Category Ticket group is mandatory..");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Category Ticket group is mandatory");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(isFanPriceStr)){
				isFanPriceStr = "false";
			}
			
			Boolean isFanPrice = null;
			try{
				isFanPrice = Boolean.valueOf(isFanPriceStr);
			}catch(Exception e){
				error.setDescription("Fan Price Flag should be true or false");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Fan Price Flag should be true or false");
				return orderConfirmation;
			}
			
			Integer customerId = null;
			
			try{
				customerId = Integer.parseInt(customerIdStr);
				
			}catch(Exception e){
				error.setDescription("Event id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Event id should be integer");
				return orderConfirmation;
			}
			
			Integer eventId = null;
			try{
				eventId = Integer.parseInt(eventIdStr);
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			Integer categoryTicketGroupId = null;
			try{
				categoryTicketGroupId = Integer.parseInt(categoryTicketGroupIdStr);
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(sessionId)){
				error.setDescription("Session Id is invalid.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Session Id is invalid.");
				return orderConfirmation;
			}
			
			CustomerOrderRequest orderRequest = new CustomerOrderRequest();
			
			Customer customer = DAORegistry.getCustomerDAO().getCustomerById(customerId);
			Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, productType);
			
			AutoCatsLockedTickets autoCatsLockedTickets = null;
			CategoryTicketGroup categoryTicketGroup =  DAORegistry.getCategoryTicketGroupDAO().getBroadCastedCategoryTicketsbyId(categoryTicketGroupId);
			 
			if(applicationPlatForm.toString().equals("TICK_TRACKER")){
				autoCatsLockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketBySessionCategoryTicketGroupId(sessionId, categoryTicketGroupId);
			}else{
				autoCatsLockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketBySessionAndCategoryTicketGroupId(sessionId, productType, Integer.parseInt(categoryTicketGroupIdStr));
			}
			
			if(customer == null){
				error.setDescription("Please choose valid customer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please choose valid customer");
				return orderConfirmation;
			}
			
			if(event == null){
				error.setDescription("Event not recognized");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Event not recognized");
				return orderConfirmation;
			}
			
			if(categoryTicketGroup == null){
				error.setButtonValue("Find Other Tickets to this Event");
				error.setOverRideButton(true);
				error.setDescription("The ticket or rate you chose is no longer available. " +
				"If you've just bought a similar ticket, its possible you bought the last ticket available");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Ticket that you are looking for is already been sold out...");
				return orderConfirmation;
			}
			
			//Re-Compute Ticket Price by Platform
			categoryTicketGroup.computeTicketPrice(applicationPlatForm);
			
			
			//paymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,PARTIAL_REWARDS,FULL_REWARDS
			if(TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				error.setDescription("Primary Payment method is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary Payment method is mandatory");
				return orderConfirmation;
			}
			
			PaymentMethod primaryPaymentMethod=null;
			if(!TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				try{
					primaryPaymentMethod = PaymentMethod.valueOf(primaryPaymentMethodStr);
				}catch(Exception e){
					error.setDescription("Please send valid Primary Payment Method");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Primary Payment Method");
					return orderConfirmation;
				}
			}
			
			Integer shippingAddressId=null,billingAddressId=null;
			
			Double orderTotal = null;
			try{
				orderTotal = Double.valueOf(orderTotalStr.trim());
				orderTotal = Double.valueOf(df.format(orderTotal));
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Order Total is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Order Total is Mandatory");
				return orderConfirmation;
			}
			
			CustomerSuperFan customerLoyalFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
			
			Double originalOrderTotal = null,serviceFees = 0.00,serverOrderTotal=0.00,originalTicketPrice = categoryTicketGroup.getOriginalPrice();
			
			serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getOriginalPrice(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
			originalOrderTotal =  ((categoryTicketGroup.getOriginalPrice() * categoryTicketGroup.getQuantity()) + serviceFees);
			originalOrderTotal =  TicketUtil.getRoundedValue(originalOrderTotal);
			
			//Setting up Ticket Original Price
			orderRequest.setTicketPrice(originalTicketPrice);
			
			Boolean isMobileDiscount = false;
			if(applicationPlatForm.equals(ApplicationPlatForm.IOS) || applicationPlatForm.equals(ApplicationPlatForm.ANDROID)){
				//isMobileDiscount = true;
			}
			
			RTFPromotionalOfferTracking promoCodeTracking = null;
			boolean rtfPromotionalCode = false, isCustReferalDiscountCode = false, isAffilateReferral = false;
			Double ticketPrice = categoryTicketGroup.getOriginalPrice();
			if((null != referralCode && !referralCode.isEmpty()) || isMobileDiscount){
				
				RTFCustomerPromotionalOffer custPromoOffer = DAORegistry.getRtfCustomerPromotionalOfferDAO().getPromoOfferCustomerId(customer.getId());
				Boolean isValidRTFPromoCode = FavoriteUtil.isValidCustomerPromotionalCode(custPromoOffer, referralCode.trim());
				
				promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
						sessionId, categoryTicketGroupId, eventId);
				
				if(isValidRTFPromoCode){
					
					/*promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
							sessionId, categoryTicketGroupId, eventId);*/
					
					isValidRTFPromoCode = true;
				}else if(RTFPromotionalOfferUtil.validateRTFPromoCode(referralCode.trim())){
					
					/*promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
							sessionId, categoryTicketGroupId, eventId);*/
					isValidRTFPromoCode = true;
					
				}else {
					
					if(RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim())){
						
						/*promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
								sessionId, categoryTicketGroupId, eventId);*/
						
						if(null != promoCodeTracking && promoCodeTracking.getOfferType().equals(PromotionalType.AFFILIATE_PROMO_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							isValidRTFPromoCode = true;
							isAffilateReferral = true;
						}else{
							promoCodeTracking = null;
						}
					}else{
						
						/*promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
								sessionId, categoryTicketGroupId, eventId);*/
						
						if(null != promoCodeTracking && promoCodeTracking.getOfferType().equals(PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							isValidRTFPromoCode = true;
							isCustReferalDiscountCode = true;
						}else{
							promoCodeTracking = null;
						}
					}
				}
				
				if(isMobileDiscount){
					isValidRTFPromoCode = true;
					if(promoCodeTracking == null){
						isValidRTFPromoCode = false;
					}
				}
				
				/*if(promoCodeTracking == null && !isValidRTFPromoCode  && isMobileDiscount){
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
							sessionId, categoryTicketGroupId, eventId);
					isValidRTFPromoCode = true;
				}*/
				
				if(isValidRTFPromoCode){
					Double discAmt = null; 
					Double discConv = promoCodeTracking.getAdditionalDiscountConv() + (isMobileDiscount?promoCodeTracking.getMobileAppDiscConv():0.00);
					if(promoCodeTracking.getIsFlatDiscount()){
						discAmt = TicketUtil.getSingleTicketFlatDiscount(categoryTicketGroup.getQuantity(), discConv);
						categoryTicketGroup.setIsFlatDiscount(true);
						categoryTicketGroup.setFlatDiscount(discAmt);
					}else{
						discAmt = TicketUtil.getDiscountedPrice(ticketPrice, discConv);
						categoryTicketGroup.setIsFlatDiscount(false);
						categoryTicketGroup.setFlatDiscount(discAmt);
					}
					if(isFanPrice != null && isFanPrice){
						if(!promoCodeTracking.getIsFlatDiscount()){
							ticketPrice = ticketPrice - discAmt;
							categoryTicketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
						}
					}else{
						ticketPrice = ticketPrice - discAmt;
						categoryTicketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					}
					rtfPromotionalCode = true;
				}else{
					
				}
			}
			
			boolean isCheckoutOfferApplied = false;
			CheckOutOfferAppliedAudit audit = null;
			
			if(!rtfPromotionalCode){
				audit = DAORegistry.getCheckOutOfferAppliedAuditDAO().getCheckoutOffer(applicationPlatForm, customer.getId(), 
						sessionId, categoryTicketGroupId, isFanPrice);
				
				if(null != audit ){
					Double discAmt = null; 
					Double discConv = audit.getDiscount() ;
					if(audit.getIsFlatOff()){
						discAmt = TicketUtil.getSingleTicketFlatDiscount(categoryTicketGroup.getQuantity(), discConv);
						categoryTicketGroup.setIsFlatDiscount(true);
						categoryTicketGroup.setFlatDiscount(discAmt);
					}else{
						discConv = discConv / 100;;
						discAmt = TicketUtil.getDiscountedPrice(ticketPrice, discConv);
						categoryTicketGroup.setIsFlatDiscount(false);
						categoryTicketGroup.setFlatDiscount(discAmt);
					}
					if(isFanPrice != null && isFanPrice){
						if(!audit.getIsFlatOff()){
							ticketPrice = ticketPrice - discAmt;
							categoryTicketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
						}
					}else{
						ticketPrice = ticketPrice - discAmt;
						categoryTicketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					}
					isCheckoutOfferApplied = true;
				}
			}
			/*Discount Applied On Price for On Day Before Ticket download options - Begins*/
			if(onDayBeforeTheEvent){
				Double discAmt = null; 
				Double discConv = PaginationUtil.onDayBeforeEventTicketDownloadRewardOffer;
				discAmt = TicketUtil.getDiscountedPrice(ticketPrice, discConv);
				ticketPrice = ticketPrice - discAmt;
				categoryTicketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
			}
			/*Discount Applied On Price for On Day Before Ticket download options - Ends*/
			
			serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getOriginalPrice(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
			
			serverOrderTotal = (TicketUtil.getRoundedValue((categoryTicketGroup.getOriginalPrice() * categoryTicketGroup.getQuantity())) + serviceFees);
			
			System.out.println(orderTotalStr);
			System.out.println(serverOrderTotal);
			
			Double discountAmount = 0.00;
			OrderType orderType = OrderType.REGULAR;
			Double soldPrice = categoryTicketGroup.getOriginalPrice();
			Double primaryPayAmt = 0.00,secondaryPayAmt=0.00;
			
			CustomerSuperFan changeLoyalFan = null;
			
			if(isFanPrice){
				if(null != customerLoyalFan){
					
					Boolean isLoyalFanEvent = FavoriteUtil.markArtistAsLoyalFan(customerLoyalFan, event);
					
					if(!isLoyalFanEvent){
						if(!customerLoyalFan.getTicketPurchased()){
							soldPrice = categoryTicketGroup.getLoyalFanPriceDouble();
							serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getLoyalFanPriceDouble(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
							serverOrderTotal =  ((categoryTicketGroup.getLoyalFanPriceDouble() * categoryTicketGroup.getQuantity()) + serviceFees);
							orderType = OrderType.LOYALFAN;
							customerLoyalFan.setEndDate(new Date());
							customerLoyalFan.setStatus(Status.DELETED);
							customerLoyalFan.setUpdatedDate(new Date());
							
							changeLoyalFan = new CustomerSuperFan();
							if(event.getParentCategoryId().equals(1)){
								List<Artist> artists = DAORegistry.getQueryManagerDAO().getArtistByEvent(event.getEventId());
								Artist artist = artists.get(0);
								changeLoyalFan.setArtistOrTeamId(artist.getId());
								changeLoyalFan.setLoyalFanName(artist.getName());
								changeLoyalFan.setLoyalFanType(LoyalFanType.SPORTS);
							}else{
								changeLoyalFan.setArtistOrTeamId(-1);
								
								String loyalFanName = event.getState();
								List<State> states = DAORegistry.getStateDAO().getAllStateBySearchKey(event.getState());
								if(null != states && !states.isEmpty()){
									loyalFanName = states.get(0).getName();
								}
								changeLoyalFan.setLoyalFanName(loyalFanName);
								changeLoyalFan.setState(event.getState());
								changeLoyalFan.setCountry(event.getCountry());
								//changeLoyalFan.setZipCode(null != event.getPinCode() && event.getPinCode().isEmpty()?event.getPinCode():null);
								LoyalFanType loyalFanType = event.getParentCategoryId().equals(2)?LoyalFanType.CONCERTS:LoyalFanType.THEATER;
								changeLoyalFan.setLoyalFanType(loyalFanType);
							}
							changeLoyalFan.setCreatedDate(new Date());;
							changeLoyalFan.setUpdatedDate(new Date());
							changeLoyalFan.setCustomerId(customerId);
							changeLoyalFan.setStartDate(new Date());
							changeLoyalFan.setStatus(Status.ACTIVE);
							changeLoyalFan.setTicketPurchased(true);
						}else{
							error.setDescription("You are currently not eligible to purchase this ticket at a discounted price. You will be redirected back to ticket listings.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							orderConfirmation.setRedirectTicketListing(true);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are currently not eligible to purchase this ticket at a discounted price");
							return orderConfirmation;
						}
					}else {
						soldPrice = categoryTicketGroup.getLoyalFanPriceDouble();
						customerLoyalFan.setTicketPurchased(true);
						customerLoyalFan.setUpdatedDate(new Date());
						serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getLoyalFanPriceDouble(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
						serverOrderTotal =  ((categoryTicketGroup.getLoyalFanPriceDouble() * categoryTicketGroup.getQuantity()) + serviceFees);
						orderType = OrderType.LOYALFAN;
					}
				}else{
					
					soldPrice = categoryTicketGroup.getLoyalFanPriceDouble();
					
					serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getLoyalFanPriceDouble(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
					serverOrderTotal =  ((categoryTicketGroup.getLoyalFanPriceDouble() * categoryTicketGroup.getQuantity()) + serviceFees);
					
					orderType = OrderType.LOYALFAN;
					changeLoyalFan = new CustomerSuperFan();
					
					if(event.getParentCategoryId().equals(1)){
						List<Artist> artists = DAORegistry.getQueryManagerDAO().getArtistByEvent(event.getEventId());
						Artist artist = artists.get(0);
						changeLoyalFan.setArtistOrTeamId(artist.getId());
						changeLoyalFan.setLoyalFanName(artist.getName());
						changeLoyalFan.setLoyalFanType(LoyalFanType.SPORTS);
					}else{
						changeLoyalFan.setArtistOrTeamId(-1);
						String loyalFanName = event.getState();
						List<State> states = DAORegistry.getStateDAO().getAllStateBySearchKey(event.getState());
						if(null != states && !states.isEmpty()){
							loyalFanName = states.get(0).getName();
						}
						changeLoyalFan.setLoyalFanName(loyalFanName);
						changeLoyalFan.setState(event.getState());
						changeLoyalFan.setCountry(event.getCountry());
						//changeLoyalFan.setZipCode(null != event.getPinCode() && event.getPinCode().isEmpty()?event.getPinCode():null);
						LoyalFanType loyalFanType = event.getParentCategoryId().equals(2)?LoyalFanType.CONCERTS:LoyalFanType.THEATER;
						changeLoyalFan.setLoyalFanType(loyalFanType);
					}
					changeLoyalFan.setCreatedDate(new Date());;
					changeLoyalFan.setUpdatedDate(new Date());
					changeLoyalFan.setCustomerId(customerId);
					changeLoyalFan.setStartDate(new Date());
					changeLoyalFan.setStatus(Status.ACTIVE);
					changeLoyalFan.setTicketPurchased(true);
				}
			}
			
			serverOrderTotal =  TicketUtil.getRoundedValue(serverOrderTotal);
			discountAmount = originalOrderTotal - serverOrderTotal;
			
			Boolean isOrderTotalMismatch = false;
			if(serverOrderTotal > orderTotal || serverOrderTotal < orderTotal){
				/*error.setDescription("Order Total is Mismatching");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Order Total is Mismatching");
				return orderConfirmation;*/
				isOrderTotalMismatch =  true;
				
				if(serverOrderTotal < orderTotal){
					serverOrderTotal = orderTotal;
				}
			}
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double rewardEarnConv = 0.00;
			
			/*if(RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim())){
				rewardEarnConv = loyaltySettings.getAffiliatePromoRewardConv();
			}else */
				
			if(isCustReferalDiscountCode){
				rewardEarnConv = promoCodeTracking.getCustomerEarnRewardConv();
			}else{
				rewardEarnConv = loyaltySettings.getRewardConversion();
			}
			
			primaryPayAmt = serverOrderTotal;
			
			CustomerLoyaltyHistory customerLoyaltyHistory = new CustomerLoyaltyHistory();
			CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			
			Double activeRewardAmount = RewardConversionUtil.getRewardDoller(customerLoyalty.getActivePointsAsDouble(),loyaltySettings.getDollerConversion());
			Double requiredPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getDollerConversion());
			Double orderRewardEarnAmount = RewardConversionUtil.getRewardDoller(requiredPoints, loyaltySettings.getDollerConversion());
			
			Double orderTotalForRewards = serverOrderTotal;
			try{
				if(null != loyaltySpentStr && !loyaltySpentStr.isEmpty()){
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(Double.valueOf(loyaltySpentStr.trim()),loyaltySettings.getDollerConversion());
					orderTotalForRewards = TicketUtil.getRoundedValue(orderTotalForRewards - enteredRewardAmount);
					
					if(orderTotalForRewards < 0){
						orderTotalForRewards = 0.00;
					}
				}				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
			
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setOrderTotal(orderTotal);
			customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
			customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setPointsSpentAsDouble(0.00);
			customerLoyaltyHistory.setRewardSpentAmount(0.00);
			customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
			customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
			customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
			customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
			Double balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - 0;
			Double balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
			customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
			
			PartialPaymentMethod secondaryPaymentMethod=null;
			
			AddressInfo addressInfo = new AddressInfo();
			
			Boolean isSaveCard = false;
			
			switch (primaryPaymentMethod) {
			
				case FULL_REWARDS:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					UserAddress shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.FULL_REWARDS);
					addressInfo.setShippingAddress(shippingAddress);
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("Please enter active reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter active reward dollars");
						return orderConfirmation;
					}	
					
					Double loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("Reward dollars amount must be an integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars amount must be an integer");
						return orderConfirmation;
					}
					
					
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(requiredPoints < loyaltySpent){
						Double diffOrderReward = loyaltySpent - requiredPoints;
						error.setDescription("Your have entered an amount more than the required reward dollars. Please subtract "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered an amount more than the required reward dollars. Please subtract "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						return orderConfirmation;
					}
					
					if(requiredPoints > loyaltySpent){
						Double diffOrderReward = requiredPoints - loyaltySpent;
						error.setDescription("Your have entered an amount less than the required reward dollars. Please add "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than required reward dollars, reduce "+TicketUtil.getRoundedValueString(diffOrderReward)+" more reward dollars");
						return orderConfirmation;
					}
					
					if((customerLoyalty.getActivePointsAsDouble() < loyaltySpent) ||
							(activeRewardAmount < enteredRewardAmount)){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(activeRewardAmount < enteredRewardAmount){
						error.setDescription("Please enter a valid reward dollars amount. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
					
					break;
				case PARTIAL_REWARDS:
					
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("reward dollars are Mandatory.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"reward dollars are Mandatory");
						return orderConfirmation;
					}	
					
					loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("reward dollars should be Integer.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"reward dollars should be Integer");
						return orderConfirmation;
					}
					

					//Secondary PaymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY
					if(TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						error.setDescription("Please enter a valid Secondary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
						return orderConfirmation;
					}
					
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(activeRewardAmount < loyaltySpent){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(requiredPoints < loyaltySpent){
						error.setDescription("Your have entered more than the required reward dollars. Please select Full Rewards as Primary Payment Method."); 
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than the required reward dollars. Please select Full Rewards as Primary Payment Method.");
						return orderConfirmation;
					}
					
					if(requiredPoints == loyaltySpent){
						error.setDescription("Your have entered the required reward dollars. Please select Full Rewards as Primary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered the required reward dollars. Please select  Full Rewards as Primary Payment Method");
						return orderConfirmation;
					}
					
					
					
					
					Double tempOrderTotal = serverOrderTotal - enteredRewardAmount ;
					primaryPayAmt = enteredRewardAmount;
					secondaryPayAmt = tempOrderTotal;
					
					if(secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						
						if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						try{
							billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
						}catch(Exception e){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
							shippingAddressId =  billingAddressId;
						}else{
							if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
								error.setDescription("Please enter a valid Shipping Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
								return orderConfirmation;
							}
							
							try{
								shippingAddressId = Integer.parseInt(shippingAddressIdStr);
							}catch(Exception e){
								error.setDescription("Shipping Address should be integer");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
								return orderConfirmation;
							}
						}
						
						UserAddress billingAddress = null;
						if(null != billingAddressId){
							billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
						}
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						if(billingAddress == null){
							error.setDescription("Billing Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
							return orderConfirmation;
						}
						addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(billingAddress);
						
						secondaryTransactionId = "PARTIAL_CREDITCARD_TRANSACTION";
						
					}else if (secondaryPaymentMethod.equals(PartialPaymentMethod.PAYPAL)){
						
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
						
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						addressInfo.setPaymentMethod(PaymentMethod.PAYPAL);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(null);
						
						secondaryTransactionId = "PARTIAL_PAYPAL_TRANSACTION";
						
						/*paypalTransactionDetail = new PaypalTransactionDetail();
						paypalTransactionDetail.setPaymentId(secondaryTransactionId);
						try{
							paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
						}catch(Exception e){
							e.printStackTrace();
						}*/
						
					}else{
						addressInfo.setValues(bFirstName, bLastName,bAddress1, bAddress2, bCity, bState, bCountry, bZipCode,bPhoneNumber,bEmail,
								sFirstName,sLastName, sAddress1,sAddress2, sCity, sState, sCountry, sZipCode,sPhoneNumber,sEmail);
						addressInfo.setPaymentMethod(PaymentMethod.valueOf(String.valueOf(secondaryPaymentMethod)));
						addressInfo.setShippingAddress(null);
						addressInfo.setBillingAddress(null);
						
						secondaryTransactionId = "PARTIAL_"+secondaryPaymentMethodStr+"_TRANSACTION";
						
					}
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
 					
					break;
					
				case CREDITCARD:
					
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					UserAddress billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					primaryTransactionId = "CREDITCARD_TRANSACTION";
					
					break;
					
				case PAYPAL:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setPaymentMethod(PaymentMethod.PAYPAL);
					/*addressInfo.setValues(bFirstName, bLastName,bAddress1, bAddress2, bCity, bState, bCountry, bZipCode,bPhoneNumber,bEmail,
							sFirstName,sLastName, sAddress1,sAddress2, sCity, sState, sCountry, sZipCode,sPhoneNumber,sEmail);
					
					addressInfo.setShippingAddress(null);
					addressInfo.setBillingAddress(null);*/

					
					primaryTransactionId = "PAYPAL_TRANSACTION";
					
					/*paypalTransactionDetail = new PaypalTransactionDetail();
					paypalTransactionDetail.setPaymentId(primaryTransactionId);
					try{
						paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
					}catch(Exception e){
						e.printStackTrace();
					}*/
					
					break;
					
				case GOOGLEPAY:
					
					addressInfo.setValues(bFirstName, bLastName,bAddress1, bAddress2, bCity, bState, bCountry, bZipCode,bPhoneNumber,bEmail,
							sFirstName,sLastName, sAddress1,sAddress2, sCity, sState, sCountry, sZipCode,sPhoneNumber,sEmail);
					addressInfo.setPaymentMethod(PaymentMethod.GOOGLEPAY);
					addressInfo.setShippingAddress(null);
					addressInfo.setBillingAddress(null);
					
					primaryTransactionId = "GOOGLEPAY_TRANSACTION";
					
					
					break;
					
				case IPAY:
					
					addressInfo.setValues(bFirstName, bLastName,bAddress1, bAddress2, bCity, bState, bCountry, bZipCode,bPhoneNumber,bEmail,
							sFirstName,sLastName, sAddress1,sAddress2, sCity, sState, sCountry, sZipCode,sPhoneNumber,sEmail);
					addressInfo.setPaymentMethod(PaymentMethod.IPAY);
					addressInfo.setShippingAddress(null);
					addressInfo.setBillingAddress(null);
					
					primaryTransactionId = "IPAY_TRANSACTION";
					
					break;
	
				default:
					break;
			}
			
			
			CustomerOrderDetail customerOrderDetail = new CustomerOrderDetail();
			
			if(addressInfo.getPaymentMethod().equals(PaymentMethod.CREDITCARD)){
				
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getBillingAddress().getAddressLine1());
				customerOrderDetail.setBillingAddress2(addressInfo.getBillingAddress().getAddressLine2());
				customerOrderDetail.setBillingCity(addressInfo.getBillingAddress().getCity());
				customerOrderDetail.setBillingCountry(addressInfo.getBillingAddress().getCountry().getCountryName());
				customerOrderDetail.setBillingFirstName(addressInfo.getBillingAddress().getFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getBillingAddress().getLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getBillingAddress().getPhone1());
				customerOrderDetail.setBillingPhone2(addressInfo.getBillingAddress().getPhone2());
				customerOrderDetail.setBillingState(addressInfo.getBillingAddress().getState().getName());
				/*customerOrderDetail.setBillingCountryId(addressInfo.getBillingAddress().getCountry().getId());
				customerOrderDetail.setBillingStateId(addressInfo.getBillingAddress().getState().getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getBillingAddress().getZipCode());
				customerOrderDetail.setBillingEmail(null != addressInfo.getBillingAddress().getEmail()?addressInfo.getBillingAddress().getEmail():customer.getEmail());
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			} else if (addressInfo.getPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else if (addressInfo.getPaymentMethod().equals(PaymentMethod.PAYPAL)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else{
				/*Country billingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getbCountry());
				State billingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(billingCountry.getId(), addressInfo.getbState());*/
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getbAddress1());
				customerOrderDetail.setBillingAddress2(addressInfo.getbAddress2());
				customerOrderDetail.setBillingCity(addressInfo.getbCity());
				customerOrderDetail.setBillingCountry(addressInfo.getbCountry());
				customerOrderDetail.setBillingFirstName(addressInfo.getbFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getbLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getbPhoneNumber());
				customerOrderDetail.setBillingState(addressInfo.getbState());
				/*customerOrderDetail.setBillingCountryId(billingCountry.getId());
				customerOrderDetail.setBillingStateId(billingState.getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getbZipCode());
				customerOrderDetail.setBillingEmail(null!=addressInfo.getbEmail()?addressInfo.getbEmail():customer.getEmail());
				
				/*Country shippingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getsCountry());
				State shippingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(shippingCountry.getId(), addressInfo.getsState());*/
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getsAddress1());
				customerOrderDetail.setShippingAddress2(addressInfo.getsAddress2());
				customerOrderDetail.setShippingCity(addressInfo.getsCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getsFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getsLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getsPhoneNumber());
				customerOrderDetail.setShippingCountry(addressInfo.getsCountry());
				customerOrderDetail.setShippingState(addressInfo.getsState());
				/*customerOrderDetail.setShippingCountryId(shippingCountry.getId());
				customerOrderDetail.setShippingStateId(shippingState.getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getsZipCode());
				customerOrderDetail.setShippingEmail(null!=addressInfo.getsEmail()?addressInfo.getsEmail():customer.getEmail());
			}
			
			CustomerLoyaltyHistory refererCustLoyaltyHistory = null;
			CustomerLoyalty refererCustomerLoyalty = null;
			ReferredCodeTracking referredCodeTracking = null;
			Boolean validReferralCode = false,affiliatePromoCode = false;
			
			AffiliateCashReward affiliateCashReward = null;
			AffiliateCashRewardHistory affiliateCashRewardHistory = null;
			
			//Customer referral code functionality
			
			//Customer custReferal = null;
			if(null != referralCode && !referralCode.isEmpty() && (!rtfPromotionalCode || isCustReferalDiscountCode || isAffilateReferral)){
				
				if(!isCustReferalDiscountCode && isAffilateReferral){
					
					Affiliate affiliate = RTFAffiliateBrokerUtil.getAffiliateObjByPromoCode(referralCode.trim());
					
					affiliatePromoCode =  true;
					
					affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(affiliate.getUserId());
					Double pendingCash = RewardConversionUtil.getAffiliateCashCredit(orderTotalForRewards, affiliate.getAffiliateCashReward());
					
					if(null == affiliateCashReward){
						affiliateCashReward= new AffiliateCashReward();
						affiliateCashReward.setUserId(affiliate.getUserId());
						affiliateCashReward.setActiveCash(0.00);
						affiliateCashReward.setPendingCash(pendingCash);
						affiliateCashReward.setLastCreditedCash(0.00);
						affiliateCashReward.setTotalCreditedCash(0.00);
						affiliateCashReward.setLastDebitedCash(0.00);
						affiliateCashReward.setTotalDebitedCash(0.00);
						affiliateCashReward.setLastVoidCash(0.00);
						affiliateCashReward.setTotalVoidedCash(0.00);
					}else{
						affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash()+pendingCash);
						//affiliateCashReward.setLastCreditedCash(0.00);
						//affiliateCashReward.setTotalCreditedCash(0.00);
						//affiliateCashReward.setLastDebitedCash(0.00);
						//affiliateCashReward.setTotalDebitedCash(0.00);
						//affiliateCashReward.setLastVoidCash(0.00);
						//affiliateCashReward.setTotalVoidedCash(0.00);
					}
					affiliateCashRewardHistory = new AffiliateCashRewardHistory();
					affiliateCashRewardHistory.setPromoCode(referralCode.trim());
					affiliateCashRewardHistory.setCustomerId(customerId);
					affiliateCashRewardHistory.setUserId(affiliate.getUserId());
					affiliateCashRewardHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					affiliateCashRewardHistory.setCreditConv(affiliate.getAffiliateCashReward());
					affiliateCashRewardHistory.setCreditedCash(pendingCash);
					affiliateCashRewardHistory.setCreateDate(new Date());
					affiliateCashRewardHistory.setUpdatedDate(new Date());
					affiliateCashRewardHistory.setIsRewardsEmailSent(Boolean.FALSE);
					affiliateCashRewardHistory.setOrderTotal(orderTotalForRewards);
				}else{
					
					Customer custReferrer = DAORegistry.getCustomerDAO().getCustomerByIdByProduct(promoCodeTracking.getReferredBy(), productType);
					if(custReferrer.getReferrerCode() == null){
					}else{
					}
						
					refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferrer.getId());
					
					//Double earningRewardPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, rewardEarnConv);
					
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, promoCodeTracking.getReferrarEarnRewardConv());
					
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferrer.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(promoCodeTracking.getReferrarEarnRewardConv());
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderType(orderType);
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferrer.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferrer.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("ReferralCode");
					
					validReferralCode = true;
					
				}
			}else if((null != refCodeIdentity && !refCodeIdentity.isEmpty()) 
					&& (refCId != null && !refCId.isEmpty())){
				
				Customer custReferal = new Customer();
				custReferal = SecurityUtil.validateReferalIdentity(refCId, refCodeIdentity, eventId, customerId, referredCodeTracking,custReferal);
				if(custReferal.getEventShareValidated()){
					try{
						refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferal.getId());	
					}catch(Exception e){
						e.printStackTrace();
					}
					
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
					
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					
					//refererCustomerLoyalty.setActivePoints(refererCustomerLoyalty.getActivePoints()-(null != refererCustLoyaltyHistory.getPointsSpent()?refererCustLoyaltyHistory.getPointsSpent():0));
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					//refererCustomerLoyalty.setActivePoints(refererCustLoyaltyHistory.getBalanceRewardPoints());
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					//refererCustomerLoyalty.setLatestEarnedPointsAsDouble(refererCustLoyaltyHistory.getPointsEarnedAsDouble());
					//refererCustomerLoyalty.setLatestSpentPointsAsDouble(refererCustLoyaltyHistory.getPointsSpentAsDouble());
					//refererCustomerLoyalty.setTotalEarnedPoints(refererCustomerLoyalty.getTotalEarnedPoints()+refererCustLoyaltyHistory.getPointsEarned());
					//refererCustomerLoyalty.setTotalSpentPointsAsDouble(refererCustomerLoyalty.getTotalSpentPointsAsDouble()+refererCustLoyaltyHistory.getPointsSpentAsDouble());
					
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderId(custReferal.getId());
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferal.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferal.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("EventShareLinkReferral");
					
					validReferralCode = true;
				}
			}
			
			Property property = null;
			if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
				Long primaryTrxId =  Long.valueOf(property.getValue());
				primaryTrxId++;
				primaryTransactionId = PaginationUtil.fullRewardPaymentPrefix+primaryTrxId;
				property.setValue(String.valueOf(primaryTrxId));
			}else if(primaryPaymentMethod.equals(PaymentMethod.PARTIAL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				Long partialPrimaryTrxId =  Long.valueOf(property.getValue());
				partialPrimaryTrxId++;
				primaryTransactionId = PaginationUtil.partialRewardPaymentPrefix+partialPrimaryTrxId;
				property.setValue(String.valueOf(partialPrimaryTrxId));
			}
			
			CustomerOrder customerOrder = new CustomerOrder();
			Customer customerDetail = new Customer();
			customerDetail.setId(customerId);
			customerOrder.setCategoryTicketGroupId(categoryTicketGroup.getId());
			customerOrder.setOrderType(orderType);
			customerOrder.setCustomer(customerDetail);
			customerOrder.setEventDateTemp(event.getEventDate());
			customerOrder.setEventId(event.getEventId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventTime(null != event.getEventTime()?event.getEventTime():null);
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setVenueName(event.getVenueName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueState(event.getState());
			customerOrder.setVenueCountry(event.getCountry());
			customerOrder.setProductType(productType);
			customerOrder.setShippingMethod(categoryTicketGroup.getShippingMethod());
			customerOrder.setTaxesAsDouble(serviceFees);
			customerOrder.setBrokerId(categoryTicketGroup.getBrokerId());
			customerOrder.setBrokerServicePerc(TicketUtil.getBrokerServiceFeePerc(categoryTicketGroup.getBrokerId()));
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(event.getEventDate());
			calendar.add(Calendar.DAY_OF_MONTH, -5);
			
			Double finalTicketPrice = serverOrderTotal / categoryTicketGroup.getQuantity();
			
			customerOrder.setShippingDateTemp(calendar.getTime());
			customerOrder.setStatus(OrderStatus.PAYMENT_PENDING);
			customerOrder.setQty(categoryTicketGroup.getQuantity());			
			customerOrder.setSection(categoryTicketGroup.getSection());
			customerOrder.setSectionDescription(categoryTicketGroup.getSectionDescription());
			customerOrder.setLastUpdatedDateTemp(new Date());
			customerOrder.setCreateDateTemp(new Date());			
			customerOrder.setTicketPriceAsDouble(finalTicketPrice);
			customerOrder.setTicketSoldPriceAsDouble(soldPrice);
			customerOrder.setOriginalOrderTotal(originalOrderTotal);
			customerOrder.setDiscountAmount(discountAmount);
			customerOrder.setOrderTotalAsDouble(serverOrderTotal);
			customerOrder.setPrimaryPayAmtAsDouble(primaryPayAmt);
			customerOrder.setSecondaryPayAmtAsDouble(secondaryPayAmt);
			customerOrder.setPrimaryPaymentMethod(primaryPaymentMethod);
			customerOrder.setPrimaryTransactionId(primaryTransactionId);
			customerOrder.setSecondaryPaymentMethod(null != secondaryPaymentMethod ? secondaryPaymentMethod:PartialPaymentMethod.NULL);
			customerOrder.setSecondaryTransactionId(null != secondaryTransactionId ? secondaryTransactionId:"");
			customerOrder.setAppPlatForm(applicationPlatForm);
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setIsInvoiceSent(false);
			customerOrder.setIsLongSale(false);
			
			DAORegistry.getCustomerOrderDAO().saveOrUpdate(customerOrder);
		    
			if(null != property){
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
			}
			
			/*Invoice invoice = null;
			if(customerOrder != null){
				invoice = new Invoice();
				invoice.setCustomerOrderId(customerOrder.getId());
				invoice.setInvoiceTotal(serverOrderTotal);
				invoice.setTicketCount(categoryTicketGroup.getQuantity());
				invoice.setCustomerId(customerId);
				invoice.setExtPONo(null);
				invoice.setStatus(InvoiceStatus.Outstanding);
				invoice.setInvoiceType("INVOICE");
				invoice.setCreatedDate(new Date());
				invoice.setCreatedBy(customer.getCustomerName());
				invoice.setIsSent(false);
				invoice.setSentDate(null);
				invoice.setRealTixMap("NO");
				invoice.setBrokerId(categoryTicketGroup.getBrokerId());
				//Persisting the customer order details in invoice
				DAORegistry.getInvoiceDAO().save(invoice);
			}*/
			
			customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()-(null != customerLoyaltyHistory.getPointsSpentAsDouble()?customerLoyaltyHistory.getPointsSpentAsDouble():0));
			customerLoyalty.setPendingPointsAsDouble((null != customerLoyalty.getPendingPointsAsDouble()?customerLoyalty.getPendingPointsAsDouble():0)+customerLoyaltyHistory.getPointsEarnedAsDouble());
			customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
			customerLoyalty.setLastUpdate(new Date());
			//customerLoyalty.setLatestSpentPointsAsDouble(customerLoyaltyHistory.getPointsSpentAsDouble());
			//customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble()+customerLoyaltyHistory.getPointsSpentAsDouble());
			customerLoyaltyHistory.setOrderType(orderType);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setUpdatedDate(new Date());
			customerLoyaltyHistory.setOrderTotal(serverOrderTotal);
			customerLoyaltyHistory.setOrderId(customerOrder.getId());
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
			
			DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(customerLoyaltyHistory);
			
			if(onDayBeforeTheEvent){
				
				/*Double ticketDownloadRewardOffer = PaginationUtil.onDayBeforeEventTicketDownloadRewardOffer;
				Double ticketDownloadRewardDollars = RewardConversionUtil.getRewardPoints(serverOrderTotal, ticketDownloadRewardOffer);
				Double orderEarnAmount = RewardConversionUtil.getRewardDoller(ticketDownloadRewardDollars, loyaltySettings.getDollerConversion());
				
				customerLoyalty.setPendingPointsAsDouble((null != customerLoyalty.getPendingPointsAsDouble()?customerLoyalty.getPendingPointsAsDouble():0)+customerLoyaltyHistory.getPointsEarnedAsDouble());
				customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
				customerLoyalty.setLastUpdate(new Date());

				CustomerLoyaltyHistory offerRewardHistory = new CustomerLoyaltyHistory();
				offerRewardHistory.setCustomerId(customerId);
				offerRewardHistory.setRewardConversionRate(rewardEarnConv);
				offerRewardHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
				offerRewardHistory.setPointsSpentAsDouble(0.00);
				offerRewardHistory.setRewardSpentAmount(0.00);
				offerRewardHistory.setPointsEarnedAsDouble(ticketDownloadRewardDollars);
				offerRewardHistory.setRewardEarnAmount(orderEarnAmount);
				offerRewardHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
				offerRewardHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
				offerRewardHistory.setOrderType(OrderType.TICKETONDAYBEFOREEVENT);
				offerRewardHistory.setCreateDate(new Date());
				offerRewardHistory.setUpdatedDate(new Date());
				offerRewardHistory.setOrderTotal(serverOrderTotal);
				offerRewardHistory.setOrderId(customerOrder.getId());
				offerRewardHistory.setRewardStatus(RewardStatus.ORDERPENDING);
				
				Double balanceRewardPointsNew = customerLoyalty.getActivePointsAsDouble() + ticketDownloadRewardDollars - 0;
				Double balanceRewardAmountNew = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
				
				offerRewardHistory.setBalanceRewardPointsAsDouble(balanceRewardPointsNew);
				offerRewardHistory.setBalanceRewardAmount(balanceRewardAmountNew);
				
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
				DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(offerRewardHistory);
				
				orderRequest.setOrderOfferRewardHistoryId(offerRewardHistory.getId());*/
				orderRequest.setTicketOnDayBeforeTheEvent(true);
			}else{
				orderRequest.setTicketOnDayBeforeTheEvent(false);
			}
			
			
			/* Add All Pending Values to Order Request - Begins  */
			orderRequest.setSessionId(sessionId);
			orderRequest.setOrderId(customerOrder.getId());
			orderRequest.setOrderLoyaltyHistoryId(customerLoyaltyHistory.getId());
			orderRequest.setOrderLoyaltyInfoId(customerLoyalty.getId());
			orderRequest.setOrderTotal(serverOrderTotal);
			orderRequest.setOrderType(orderType);
			orderRequest.setOriginalOrderTotal(originalOrderTotal);
			orderRequest.setAppPlatForm(customerOrder.getAppPlatForm());
			orderRequest.setCategoryTicketGroupId(categoryTicketGroupId);
			orderRequest.setCustomerId(customerId);
			orderRequest.setEventId(eventId);
			orderRequest.setInvoiceId(-1);
			orderRequest.setIsLongSale(false);
			orderRequest.setIsLoyalFan(isFanPrice);
			orderRequest.setPrimaryPayAmt(primaryPayAmt);
			orderRequest.setPrimaryPaymentMethod(primaryPaymentMethod);
			orderRequest.setPrimaryTransactionId(primaryTransactionId);
			orderRequest.setSecondaryPayAmt(secondaryPayAmt);
			orderRequest.setSecondaryPaymentMethod(secondaryPaymentMethod);
			orderRequest.setSecondaryTransactionId(secondaryTransactionId);
			orderRequest.setQuantity(customerOrder.getQty());
			orderRequest.setSoldPrice(soldPrice);
			orderRequest.setThirdPayAmt(customerOrder.getThirdPayAmtAsDouble());
			orderRequest.setThirdPaymentMethod(customerOrder.getThirdPaymentMethod());
			orderRequest.setThirdTransactionId(customerOrder.getThirdTransactionId());
			orderRequest.setTicketGroupId(null);
			
			String customerIpAddress = "";
			if(customerOrder.getAppPlatForm().equals(ApplicationPlatForm.IOS) || 
					customerOrder.getAppPlatForm().equals(ApplicationPlatForm.ANDROID)){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			orderRequest.setIpAddress(customerIpAddress);
			orderRequest.setStatus(OrderStatus.PAYMENT_PENDING);
			orderRequest.setCreatedTime(new Date());
			orderRequest.setUpdatedTime(new Date());
			/* Add All Pending Values to Order Request - Ends  */
			
			if(isCheckoutOfferApplied){
				promoCodeTracking = new RTFPromotionalOfferTracking();
				promoCodeTracking.setCustomerId(customer.getId());
				promoCodeTracking.setPromotionalOfferId(audit.getId());
				promoCodeTracking.setTicketGroupId(audit.getCategoryTicketGroupId());
				promoCodeTracking.setIsLongTicket(false);
				promoCodeTracking.setEventId(eventId);
				promoCodeTracking.setPlatForm(applicationPlatForm);
				promoCodeTracking.setSessionId(sessionId);
				promoCodeTracking.setStatus("ORDERPENDING");
				promoCodeTracking.setOfferType(PromotionalType.FIRST_CHECKOUT_OFFER);
				promoCodeTracking.setOrderId(null);
				promoCodeTracking.setPromoCode(referralCode);
				promoCodeTracking.setIsFlatDiscount(audit.getIsFlatOff());
				Double discConv = audit.getDiscount() / 100;;
				promoCodeTracking.setAdditionalDiscountConv(discConv);
				promoCodeTracking.setAdditionalDiscountAmt(0.00);
				promoCodeTracking.setServiceFees(serviceFees);
				promoCodeTracking.setTotalPrice(orderTotal);
				promoCodeTracking.setTicketPrice(orderRequest.getTicketPrice());
				promoCodeTracking.setDiscountedPrice(customerOrder.getTicketSoldPriceAsDouble());
				promoCodeTracking.setCreatedDate(new Date());
				promoCodeTracking.setUpdatedDate(new Date());
				promoCodeTracking.setOrderId(customerOrder.getId());
				DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
				
				//Add RTF Promo Tracking ID Into Order Request
				orderRequest.setRtfPromoTrackingId(promoCodeTracking.getId());
				orderRequest.setRtfCheckoutOfferId(audit.getId());
				
				audit.setStatus("ORDERPENDING");
				DAORegistry.getCheckOutOfferAppliedAuditDAO().update(audit);
				
			}else if(rtfPromotionalCode){
				try{
					promoCodeTracking.setOrderId(customerOrder.getId());
					promoCodeTracking.setStatus("ORDERPENDING");
					promoCodeTracking.setUpdatedDate(new Date());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
					
					//DAORegistry.getRtfPromotionalOfferDAO().updatePromotionaOfferOrderCount(promoCodeTracking.getPromotionalOfferId());
					
					//Add RTF Promo Tracking ID Into Order Request
					orderRequest.setRtfPromoTrackingId(promoCodeTracking.getId());
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(affiliatePromoCode){
				
				affiliateCashReward.setLastUpdate(new Date());
				DAORegistry.getAffiliateCashRewardDAO().saveOrUpdate(affiliateCashReward);
				affiliateCashRewardHistory.setOrderId(customerOrder.getId());
				DAORegistry.getAffiliateCashRewardHistoryDAO().saveOrUpdate(affiliateCashRewardHistory);
				try{
					AffiliatePromoCodeTracking affliCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(applicationPlatForm, 
								affiliateCashReward.getUserId(), customer.getId(), sessionId, categoryTicketGroupId, eventId, referralCode,false);
					if(null != affliCodeTracking){
						affliCodeTracking.setUpdatedDate(new Date());
						affliCodeTracking.setStatus("ORDERPENDING");
						affliCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
						affliCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
						affliCodeTracking.setUpdatedDate(new Date());
						affliCodeTracking.setOrderId(customerOrder.getId());
						affliCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
						affliCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
					}else{
						affliCodeTracking = new AffiliatePromoCodeTracking();
						affliCodeTracking.setTicketGroupId(categoryTicketGroupId);
						affliCodeTracking.setEventId(eventId);
						affliCodeTracking.setIpAddress(ip);
						affliCodeTracking.setPlatForm(applicationPlatForm);
						affliCodeTracking.setSessionId(sessionId);
						affliCodeTracking.setStatus("ORDERPENDING");
						affliCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
						affliCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
						affliCodeTracking.setCreatedDate(new Date());
						affliCodeTracking.setUpdatedDate(new Date());
						affliCodeTracking.setCustomerId(customerId);
						affliCodeTracking.setUserId(affiliateCashReward.getUserId());
						affliCodeTracking.setOrderId(customerOrder.getId());
						affliCodeTracking.setPromoCode(referralCode);
						affliCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
						affliCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
					}
					DAORegistry.getAffiliatePromoCodeTrackingDAO().saveOrUpdate(affliCodeTracking);
					
					//Add Affiliate Reward ID's to Order Request.
					orderRequest.setAffCashRewardHistoryId(affiliateCashRewardHistory.getId());
					orderRequest.setAffCashRewardId(affiliateCashReward.getId());
					orderRequest.setAffPromoTrackingId(affliCodeTracking.getId());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(validReferralCode || isCustReferalDiscountCode){
				
				refererCustLoyaltyHistory.setOrderId(customerOrder.getId());
				
				if(null != refererCustLoyaltyHistory){ //Safer side to avoid null pointer exception
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(refererCustomerLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(refererCustLoyaltyHistory);
					DAORegistry.getReferredCodeTrackingDAO().saveOrUpdate(referredCodeTracking);
					
					//Add Referral Reward ID's to Order Request.
					orderRequest.setRefCustLoyaltyHistoryId(refererCustLoyaltyHistory.getId());
					orderRequest.setRefCustLoyaltyInfoId(refererCustomerLoyalty.getId());
					orderRequest.setRefTrackingId(referredCodeTracking.getId());
					referralCode = referredCodeTracking.getReferredCode();
				}
			}
			
			if(isFanPrice ){
				
				   try{
					   
					Date startdate = null;
					   
					if(null != customerLoyalFan){
						startdate = customerLoyalFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(customerLoyalFan);
						
						//Add Old Loyal Fan ID to Order Request.
						orderRequest.setOldLoyalFanId(customerLoyalFan.getId());
					}
					if(null != changeLoyalFan){
						startdate = changeLoyalFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(changeLoyalFan);
						
						//Add New Loyal Fan ID to Order Request.
						orderRequest.setNewLoyalFanId(changeLoyalFan.getId());
						
						/*calendar = new GregorianCalendar();
						calendar.setTime(startdate);
						calendar.add(Calendar.YEAR,1);
						startdate = calendar.getTime();
						String formattedendDate = dateTimeFormat.format(startdate);
						
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("customer", customer);  
						mailMap.put("changeLoyalFan", changeLoyalFan); 
						mailMap.put("startdate", formattedendDate);
						MailAttachment[] mailAttachment = new MailAttachment[1];
						String filePath = URLUtil.getLogoImage();
						mailAttachment[0] = new MailAttachment(null,"image/png","logo.png");
					
						if(null != changeLoyalFan && changeLoyalFan.getTicketPurchased()==true){
							try{
								mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
										null, "AODev@rightthisway.com", 
										"Reward The Fan: LOYAL FAN ",
										"mail-loyal-fan.html", mailMap, "text/html", null,mailAttachment,filePath);
							}catch(Exception e) {
								e.printStackTrace();
							}
						}*/
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
               
			customerOrderDetail.setOrderId(customerOrder.getId());
			try{
				if(null != customerOrderDetail.getBillingPhone1() && !customerOrderDetail.getBillingPhone1().isEmpty()){
					customer.setPhone(customerOrderDetail.getBillingPhone1());
					DAORegistry.getCustomerDAO().saveOrUpdate(customer);
					CustomerUtil.updatedCustomerUtil(customer);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			DAORegistry.getCustomerOrderDetailDAO().save(customerOrderDetail);
			
			/*if(null != paypalTransactionDetail && null != paypalTransactionDetail.getPaymentId()  
					&& !paypalTransactionDetail.getPaymentId().isEmpty()){
				paypalTransactionDetail.setOrderId(customerOrder.getId());
				DAORegistry.getPaypalTransactionDetailDAO().saveOrUpdate(paypalTransactionDetail);
				
				PaypalTracking paypalTracking = DAORegistry.getPaypalTrackingDAO().getTrackingByCustomerIdByTicketId(customerId, 
						eventId, categoryTicketGroupId, sessionId, applicationPlatForm);
				
				if(null != paypalTracking){
					paypalTracking.setLastUpdated(new Date());
					paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
					paypalTracking.setTransactionId(paypalTransactionDetail.getId());
					paypalTracking.setStatus("COMPLETED");
				}else{
					paypalTracking = new PaypalTracking();
					paypalTracking.setCatTicketGroupId(categoryTicketGroupId);
					paypalTracking.setCreatedDate(new Date());
					paypalTracking.setCustomerId(customerId);
					paypalTracking.setEventId(eventId);
					paypalTracking.setLastUpdated(new Date());
					paypalTracking.setOrderType(orderType);
					paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
					paypalTracking.setPlatform(applicationPlatForm);
					paypalTracking.setQty(categoryTicketGroup.getQuantity());
					paypalTracking.setStatus("COMPLETED");
					paypalTracking.setTransactionId(paypalTransactionDetail.getId());
					paypalTracking.setOrderTotal(serverOrderTotal);
					paypalTracking.setZone(categoryTicketGroup.getSection());
				}
				
				DAORegistry.getPaypalTrackingDAO().saveOrUpdate(paypalTracking);
			}*/

			DAORegistry.getCategoryTicketGroupDAO().updateCategoryTicketGroupAsLocked(categoryTicketGroupId, -1, 
					soldPrice, categoryTicketGroup.getQuantity());
			
			/*try{
				DAORegistry.getCategoryTicketDAO().updateCategoryTicket(categoryTicketGroupId, invoice.getId(), soldPrice);
			}catch(Exception e){
				e.printStackTrace();
			}*/
			
			
			
			if(null != autoCatsLockedTickets ){
				
				orderRequest.setLockId(autoCatsLockedTickets.getId());
				
				//remove the locked ticket information after it has been purchased by customer
				//DAORegistry.getAutoCatsLockedTicketDAO().delete(autoCatsLockedTickets);
			}
			
			/*try{
				//To Send Order Placed notification 
				RTFOrderPlacedNotification notification = new RTFOrderPlacedNotification();
				notification.sendRewardNotifications(customerOrder);
			}catch(Exception e){
				e.printStackTrace();
			}*/
			
			DAORegistry.getCustomerOrderRequestDAO().saveOrUpdate(orderRequest);
			
			try{
				MapUtil.copySVGMapandText(event.getVenueId(), event.getVenueCategoryName(),customerOrder.getId());
			}catch(Exception e){
				e.printStackTrace();
			}
			orderConfirmation.setOrder(customerOrder);
			orderConfirmation.setOrderId(customerOrder.getId());
			orderConfirmation.setMessage("Your Order was successfully created!");
			orderConfirmation.setStatus(1);	
			
			if(isOrderTotalMismatch){
				try{
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("OrderNo",orderRequest.getOrderId());
					mailMap.put("OrderRequestNo",orderRequest.getOrderId());
					mailMap.put("customer",customer);
					mailMap.put("paymentMethod",String.valueOf(orderRequest.getPrimaryPaymentMethod()));
					mailMap.put("serverOrderTotal", serverOrderTotal);
					mailMap.put("originalOrderTotal", originalOrderTotal);
					mailMap.put("orderTotal", orderTotal);
					String subject = "Order Total Was Mismatching. Please Check This Order.!";
					mailMap.put("message",subject);
					MailAttachment[] mailAttachment = new MailAttachment[1];
					String filePath = null;
					mailAttachment[0] = new MailAttachment(null,"image/png","logo.png",URLUtil.getLogoImage());
					mailManager.sendMailNow("text/html","sales@rewardthefan.com", "AODev@rightthisway.com", 
							null,"AODev@rightthisway.com", "DEV TEAM : "+subject,"mail-order-total-mismatch-orders.html", 
							mailMap, "text/html", null,mailAttachment,filePath);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			try{
				TrackingUtils.orderTracking(request, customerOrder,sessionId,referralCode,isFanPrice);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}catch (Exception e) {
			//later on add money refund procedure here e.g(credit,debit,paypal and loyalty)
			e.printStackTrace();
			error.setDescription("Opps somthing went wrong. Order was not created.");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Opps somthing went wrong. Order was not created.");
			return orderConfirmation;
		}		
		return orderConfirmation;
	}
	
	
	

	@RequestMapping(value = "/ConfirmOrder",method = RequestMethod.POST)
	public @ResponsePayload OrderConfirmation confirmOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderConfirmation orderConfirmation = new OrderConfirmation();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
				return orderConfirmation;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String appPlatformStr = request.getParameter("platForm");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String orderIdStr = request.getParameter("orderId");
			String isPaymentSuccessStr = request.getParameter("isPaymentSuccess");
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String primaryTransactionId = request.getParameter("primaryTransactionId");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String secondaryTransactionId = request.getParameter("secondaryTransactionId");
			String thirdPaymentMethodStr = request.getParameter("thirdPaymentMethod");
			String thirdTransactionId = request.getParameter("thirdTransactionId");
			String ipAddress = request.getParameter("clientIPAddress");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Product Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Application Platform is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
				return orderConfirmation;
			}
			
			Integer orderId = null;
			CustomerOrder customerOrder = null;
			CustomerOrderRequest orderRequest = null;
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
				customerOrder = DAORegistry.getCustomerOrderDAO().getPaymentPendingOrderById(orderId);
				if(null == customerOrder ){
					error.setDescription("Order No is not valid");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
					return orderConfirmation;
				}
				orderRequest = DAORegistry.getCustomerOrderRequestDAO().getOrderRequestByOrderId(orderId);
				if(null == orderRequest ){
					error.setDescription("Oops Something Went Wrong. Please Contact Website Administrator.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Oops Something Went Wrong. Please Contact Website Administrator.");
					return orderConfirmation;
				}
				
			}catch(Exception e){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
				return orderConfirmation;
			}

			if(TextUtil.isEmptyOrNull(isPaymentSuccessStr)){
				error.setDescription("Payment Success Flag is invalid.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Payment Success Flag is invalid.");
				return orderConfirmation;
			}
			
			Boolean isPaymentSuccess = Boolean.valueOf(isPaymentSuccessStr);
			
			//paymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,PARTIAL_REWARDS,FULL_REWARDS
			if(TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				error.setDescription("Primary Payment method is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Payment method is mandatory");
				return orderConfirmation;
			}
			
			PaymentMethod primaryPaymentMethod=null;
			if(!TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				try{
					primaryPaymentMethod = PaymentMethod.valueOf(primaryPaymentMethodStr);
				}catch(Exception e){
					error.setDescription("Please send valid Primary Payment Method");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid Primary Payment Method");
					return orderConfirmation;
				}
			}
			
			PartialPaymentMethod secondaryPaymentMethod = null;
			PaypalTransactionDetail paypalTransactionDetail = null;
			
			if(!isPaymentSuccess){
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getOrderLoyaltyInfoId());
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderLoyaltyHistoryId());
				
				if(null != orderRequest.getAffCashRewardId()){
					
					AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().get(orderRequest.getAffCashRewardId());
					AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().get(orderRequest.getAffCashRewardHistoryId());
					
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().get(orderRequest.getAffPromoTrackingId());
					
					affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash() - affiliateCashRewardHistory.getCreditedCash());
					affiliateCashReward.setLastUpdate(new Date());
					
					DAORegistry.getAffiliateCashRewardDAO().update(affiliateCashReward);
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().delete(affiliatePromoCodeTracking);
					DAORegistry.getAffiliateCashRewardHistoryDAO().delete(affiliateCashRewardHistory);
				}

				if(null != orderRequest.getRtfPromoTrackingId()){
					RTFPromotionalOfferTracking offerTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().get(orderRequest.getRtfPromoTrackingId());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().delete(offerTracking);
				}
				
				if(null != orderRequest.getRefCustLoyaltyHistoryId()){
					
					CustomerLoyalty refLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getRefCustLoyaltyInfoId());
					CustomerLoyaltyHistory refLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getRefCustLoyaltyHistoryId());
					ReferredCodeTracking referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().get(orderRequest.getRefTrackingId());
					
					refLoyalty.setPendingPointsAsDouble(refLoyalty.getPendingPointsAsDouble() - refLoyaltyHistory.getPointsEarnedAsDouble());
					refLoyalty.setLastUpdate(new Date());
					
					DAORegistry.getCustomerLoyaltyDAO().update(refLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().delete(refLoyaltyHistory);
					DAORegistry.getReferredCodeTrackingDAO().delete(referredCodeTracking);
				}
				
				try{
					DAORegistry.getCategoryTicketGroupDAO().revertSoldTicketToActive(customerOrder.getCategoryTicketGroupId());
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(null != orderRequest.getLockId() ){
					//remove the locked ticket information after it has been purchased by customer
					DAORegistry.getAutoCatsLockedTicketDAO().deleteByLockId(orderRequest.getLockId());
				}
				
				customerLoyalty.setPendingPointsAsDouble(customerLoyalty.getPendingPointsAsDouble() - customerLoyaltyHistory.getPointsEarnedAsDouble());
				if(null != customerOrder.getPrimaryPaymentMethod() && 
						(customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS) 
								|| customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
					customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()+ customerLoyaltyHistory.getPointsSpentAsDouble());
					customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble() - customerLoyaltyHistory.getPointsSpentAsDouble());
				}
				customerLoyalty.setLastUpdate(new Date());
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				DAORegistry.getCustomerLoyaltyHistoryDAO().delete(customerLoyaltyHistory);
				
				
				/*if(null != orderRequest.getOrderOfferRewardHistoryId()){
					
					CustomerLoyaltyHistory history = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderOfferRewardHistoryId());
					
					customerLoyalty.setPendingPointsAsDouble(customerLoyalty.getPendingPointsAsDouble() - history.getPointsEarnedAsDouble());
					customerLoyalty.setLastUpdate(new Date());
					
					DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().delete(history);
				}*/
				
				
				orderRequest.setStatus(OrderStatus.PAYMENT_FAILED);
				orderRequest.setUpdatedTime(new Date());
				
				DAORegistry.getCustomerOrderDAO().delete(customerOrder);
				DAORegistry.getCustomerOrderRequestDAO().update(orderRequest);
				
				try{
					PreOrderPageAudit pageAudit = DAORegistry.getPreOrderPageAuditDAO().getAudit(applicationPlatForm, 
							orderRequest.getCustomerId(), orderRequest.getSessionId(), orderRequest.getCategoryTicketGroupId(), orderRequest.getEventId());
					if(pageAudit != null){
						pageAudit.setStatus("PAYMENTFAILED");
						pageAudit.setUpdatedDate(new Date());
						String reason = "Order Not Confirmed. Customer initiated the payment and the transaction was not completed. " +
								"Customer Order Request No is "+orderRequest.getOrderId()+". Please contact Dev Team for furthur Details." ;
						pageAudit.setReason(reason);
						DAORegistry.getPreOrderPageAuditDAO().saveOrUpdate(pageAudit);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				orderConfirmation.setMessage("Order not confirmed due to invalid transaction.");
				orderConfirmation.setStatus(1);	
				return orderConfirmation;
			}else{
				
				List<OrderPaymentBreakup> orderPaymentBreakupList = new ArrayList<OrderPaymentBreakup>();
				OrderPaymentBreakup paymentBreakUp = null;
				
				if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS) || primaryPaymentMethod.equals(PaymentMethod.PARTIAL_REWARDS)){
					primaryTransactionId = customerOrder.getPrimaryTransactionId();
				}
				
				/*Ulaganathan : Order Payment breakups - Begins*/
				paymentBreakUp = new OrderPaymentBreakup();
				paymentBreakUp.setOrderId(orderId);
				paymentBreakUp.setPaymentAmount(customerOrder.getPrimaryPayAmtAsDouble());
				paymentBreakUp.setPaymentMethod(primaryPaymentMethod);
				paymentBreakUp.setTransactionId(primaryTransactionId);
				paymentBreakUp.setDoneBy("Customer");
				paymentBreakUp.setPaymentDate(new Date());
				paymentBreakUp.setOrderSequence(1);
				orderPaymentBreakupList.add(paymentBreakUp);
				/*Ulaganathan : Order Payment breakups - Ends*/
				
				if(primaryPaymentMethod.equals(PaymentMethod.CREDITCARD) ||
						primaryPaymentMethod.equals(PaymentMethod.GOOGLEPAY) ||
						primaryPaymentMethod.equals(PaymentMethod.IPAY) || 
						primaryPaymentMethod.equals(PaymentMethod.CREDITCARD) ){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.PAYPAL)){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
					paypalTransactionDetail = new PaypalTransactionDetail();
					paypalTransactionDetail.setPaymentId(primaryTransactionId);
					try{
						paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
					}catch(Exception e){
						e.printStackTrace();
					}
				 
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
					//Nothing to be written over here. Transaction ID Will be generated While Creating Dummy Order
					
				}else{
					
					//Secondary PaymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY
					if(TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						error.setDescription("Please enter a valid Secondary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please enter a valid Secondary Payment Method");
						return orderConfirmation;
					}
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					
					if(secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD) ||
							secondaryPaymentMethod.equals(PartialPaymentMethod.GOOGLEPAY) ||
							secondaryPaymentMethod.equals(PartialPaymentMethod.IPAY) || 
							secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD) ){
						
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
					}else if(secondaryPaymentMethod.equals(PartialPaymentMethod.PAYPAL)){
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						
						paypalTransactionDetail = new PaypalTransactionDetail();
						paypalTransactionDetail.setPaymentId(secondaryTransactionId);
						try{
							paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					
					/*Ulaganathan : Order Payment breakups - Begins*/
					paymentBreakUp = new OrderPaymentBreakup();
					paymentBreakUp.setOrderId(orderId);
					paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
					paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
					paymentBreakUp.setTransactionId(secondaryTransactionId);
					paymentBreakUp.setDoneBy("Customer");
					paymentBreakUp.setOrderSequence(2);
					paymentBreakUp.setPaymentDate(new Date());
					orderPaymentBreakupList.add(paymentBreakUp);
					/*Ulaganathan : Order Payment breakups - Ends*/
				}
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getOrderLoyaltyInfoId());
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderLoyaltyHistoryId());
				
				boolean isPromoApplied = false;
				boolean showDiscountPriceArea = false;
				String promoCode = "";
				
				if(null != orderRequest.getAffCashRewardId()){
					AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().get(orderRequest.getAffCashRewardId());
					AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().get(orderRequest.getAffCashRewardHistoryId());
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().get(orderRequest.getAffPromoTrackingId());
					
					affiliateCashRewardHistory.setRewardStatus(RewardStatus.PENDING);
					affiliateCashRewardHistory.setUpdatedDate(new Date());
					
					affiliatePromoCodeTracking.setStatus("COMPLETED");
					affiliatePromoCodeTracking.setUpdatedDate(new Date());
					
					showDiscountPriceArea = false;
					isPromoApplied = true;
					promoCode = affiliatePromoCodeTracking.getPromoCode();
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().update(affiliatePromoCodeTracking);
					DAORegistry.getAffiliateCashRewardHistoryDAO().update(affiliateCashRewardHistory);
				}
				
				if(null != orderRequest.getRtfPromoTrackingId()){
					RTFPromotionalOfferTracking offerTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().get(orderRequest.getRtfPromoTrackingId());
					offerTracking.setStatus("COMPLETED");
					offerTracking.setUpdatedDate(new Date());
					
					showDiscountPriceArea = true;
					isPromoApplied = true;
					promoCode = offerTracking.getPromoCode();
					
					if(null != offerTracking.getOfferType() && offerTracking.getOfferType().equals(PromotionalType.CUSTOMER_PROMO)){
						DAORegistry.getRtfCustomerPromotionalOfferDAO().updatePromotionaOfferOrderCount(offerTracking.getPromotionalOfferId());
					}else if(offerTracking.getOfferType().equals(PromotionalType.NORMAL_PROMO)){
						DAORegistry.getRtfPromotionalOfferHdrDAO().updatePromotionaOfferOrderCount(offerTracking.getPromotionalOfferId());
					}else if (offerTracking.getOfferType().equals(PromotionalType.FIRST_CHECKOUT_OFFER)){
						CheckOutOfferAppliedAudit audit = DAORegistry.getCheckOutOfferAppliedAuditDAO().get(orderRequest.getRtfCheckoutOfferId());
						if(audit != null){
							audit.setStatus("COMPLETED");
							audit.setOrderId(orderId);
							audit.setUpdatedDate(new Date());
							DAORegistry.getCheckOutOfferAppliedAuditDAO().update(audit);
						}
						promoCode = "CHECKOUT OFFER";
					}
					DAORegistry.getRtfPromotionalOfferTrackingDAO().update(offerTracking);
				}
				
				if(null != orderRequest.getRefCustLoyaltyHistoryId()){
					CustomerLoyalty refLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getRefCustLoyaltyInfoId());
					CustomerLoyaltyHistory refLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getRefCustLoyaltyHistoryId());
					ReferredCodeTracking referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().get(orderRequest.getRefTrackingId());
					
					refLoyaltyHistory.setUpdatedDate(new Date());
					refLoyaltyHistory.setRewardStatus(RewardStatus.PENDING);
					DAORegistry.getCustomerLoyaltyHistoryDAO().update(refLoyaltyHistory);
				}
				
				
				/*if(null != orderRequest.getOrderOfferRewardHistoryId()){
					CustomerLoyaltyHistory history = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderOfferRewardHistoryId());
					history.setUpdatedDate(new Date());
					history.setRewardStatus(RewardStatus.PENDING);
					DAORegistry.getCustomerLoyaltyHistoryDAO().update(history);
				}*/
				
				
				Invoice invoice = null;
				if(customerOrder != null){
					invoice = new Invoice();
					invoice.setCustomerOrderId(customerOrder.getId());
					invoice.setInvoiceTotal(customerOrder.getOrderTotalAsDouble());
					invoice.setTicketCount(customerOrder.getQty());
					invoice.setCustomerId(orderRequest.getCustomerId());
					invoice.setExtPONo(null);
					invoice.setStatus(InvoiceStatus.Outstanding);
					invoice.setInvoiceType("INVOICE");
					invoice.setCreatedDate(new Date());
					invoice.setCreatedBy(customerOrder.getCustomer().getCustomerName());
					invoice.setIsSent(false);
					invoice.setSentDate(null);
					invoice.setRealTixMap("NO");
					invoice.setBrokerId(customerOrder.getBrokerId());
					//Persisting the customer order details in invoice
					DAORegistry.getInvoiceDAO().save(invoice);
				}
				
				orderRequest.setInvoiceId(invoice.getId());
				
				try{
					DAORegistry.getCategoryTicketDAO().updateCategoryTicket(customerOrder.getCategoryTicketGroupId(), 
							invoice.getId(), orderRequest.getSoldPrice());
					DAORegistry.getCategoryTicketGroupDAO().updateTicketAsSold(customerOrder.getCategoryTicketGroupId(), invoice.getId() );
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(null != orderRequest.getLockId() ){
					//remove the locked ticket information after it has been purchased by customer
					DAORegistry.getAutoCatsLockedTicketDAO().deleteByLockId(orderRequest.getLockId());
				}
				
				
				if(null != paypalTransactionDetail && null != paypalTransactionDetail.getPaymentId()  
						&& !paypalTransactionDetail.getPaymentId().isEmpty()){
					paypalTransactionDetail.setOrderId(customerOrder.getId());
					DAORegistry.getPaypalTransactionDetailDAO().saveOrUpdate(paypalTransactionDetail);
					
					PaypalTracking paypalTracking = DAORegistry.getPaypalTrackingDAO().getTrackingByCustomerIdByTicketId(orderRequest.getCustomerId(), 
							customerOrder.getEventId(), customerOrder.getCategoryTicketGroupId(), sessionId, applicationPlatForm);
					
					if(null != paypalTracking){
						paypalTracking.setLastUpdated(new Date());
						paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
						paypalTracking.setTransactionId(paypalTransactionDetail.getId());
						paypalTracking.setStatus("COMPLETED");
					}else{
						paypalTracking = new PaypalTracking();
						paypalTracking.setCatTicketGroupId(customerOrder.getCategoryTicketGroupId());
						paypalTracking.setCreatedDate(new Date());
						paypalTracking.setCustomerId(orderRequest.getCustomerId());
						paypalTracking.setEventId(customerOrder.getEventId());
						paypalTracking.setLastUpdated(new Date());
						paypalTracking.setOrderType(customerOrder.getOrderType());
						paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
						paypalTracking.setPlatform(applicationPlatForm);
						paypalTracking.setQty(customerOrder.getQty());
						paypalTracking.setStatus("COMPLETED");
						paypalTracking.setTransactionId(paypalTransactionDetail.getId());
						paypalTracking.setOrderTotal(customerOrder.getOrderTotalAsDouble());
						paypalTracking.setZone(customerOrder.getSection());
					}
					DAORegistry.getPaypalTrackingDAO().saveOrUpdate(paypalTracking);
				}
				
				customerLoyaltyHistory.setRewardStatus(RewardStatus.PENDING);
				customerLoyaltyHistory.setUpdatedDate(new Date());
				DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
				
				if(null !=customerLoyaltyHistory.getPointsSpentAsDouble() && 
						customerLoyaltyHistory.getPointsSpentAsDouble() > 0){
					customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble() + customerLoyaltyHistory.getPointsSpentAsDouble());
					customerLoyalty.setLatestSpentPointsAsDouble(customerLoyaltyHistory.getPointsSpentAsDouble());
					DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				}
				
				orderRequest.setStatus(OrderStatus.ACTIVE);
				orderRequest.setUpdatedTime(new Date());
				
				if(orderRequest.getTicketOnDayBeforeTheEvent()){
					showDiscountPriceArea = true;
				}
				
				customerOrder.setTicketOnDayBeforeTheEvent(orderRequest.getTicketOnDayBeforeTheEvent());
				customerOrder.setIsInvoiceSent(false);
				customerOrder.setStatus(OrderStatus.ACTIVE);
				customerOrder.setLastUpdatedDateTemp(new Date());
				
				customerOrder.setIsPromoCodeApplied(isPromoApplied);
				customerOrder.setShowDiscPriceArea(showDiscountPriceArea);
				customerOrder.setPromotionalCode(promoCode);
				customerOrder.setNormalTixPrice(orderRequest.getTicketPrice());
				customerOrder.setDiscountedTixPrice(orderRequest.getSoldPrice());
				
				DAORegistry.getCustomerOrderDAO().update(customerOrder);
				DAORegistry.getCustomerOrderRequestDAO().update(orderRequest);
				
				//Save Order payment Breakups - Done By Ulaganathan
				DAORegistry.getOrderPaymentBreakupDAO().saveAll(orderPaymentBreakupList);
				
				try{
					if(null == customerOrder.getCustomer().getEligibleToShareRefCode() || !customerOrder.getCustomer().getEligibleToShareRefCode()){
						DAORegistry.getCustomerDAO().updateCustomerForTicketPurchase(orderRequest.getCustomerId());
					}
					
					PreOrderPageAudit pageAudit = DAORegistry.getPreOrderPageAuditDAO().getAudit(applicationPlatForm, 
							orderRequest.getCustomerId(), orderRequest.getSessionId(), orderRequest.getCategoryTicketGroupId(), orderRequest.getEventId());
					
					if(pageAudit != null){
						pageAudit.setStatus("COMPLETED");
						pageAudit.setUpdatedDate(new Date());
						pageAudit.setReason("Order Created Successfully and Order No is :"+orderRequest.getOrderId());
						DAORegistry.getPreOrderPageAuditDAO().saveOrUpdate(pageAudit);
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(orderRequest.getIsLoyalFan()){
					CustomerSuperFan loyalFan =  DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(orderRequest.getCustomerId());
					
					try{
						if(null != loyalFan && (null == loyalFan.getIsEmailSent() || !loyalFan.getIsEmailSent())){
							Date startdate = loyalFan.getStartDate();
							Calendar calendar = new GregorianCalendar();
							calendar.setTime(startdate);
							calendar.add(Calendar.YEAR,1);
							startdate = calendar.getTime();
							String formattedendDate = dateTimeFormat.format(startdate);
							
							Customer customer = customerOrder.getCustomer();
							Map<String,Object> mailMap = new HashMap<String,Object>();
							mailMap.put("customer", customerOrder.getCustomer());  
							
							String message = TextUtil.getSportsLoyalFanEmailBody(loyalFan.getLoyalFanName());
							
							if(!loyalFan.getLoyalFanType().equals(LoyalFanType.SPORTS)){
								message = TextUtil.getNonSportsLoyalFanEmailBody(loyalFan.getLoyalFanName(), String.valueOf(loyalFan.getLoyalFanType()),loyalFan.getCountry());
							}
							mailMap.put("message", message); 
							mailMap.put("startdate", formattedendDate);
							
							MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
					        mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(),  null,URLUtil.bccEmails, 
					          "Reward The Fan: LOYAL FAN ",
					          "mail-loyal-fan.html", mailMap, "text/html", null,mailAttachment,null);
					        loyalFan.setIsEmailSent(true);
					        
					        DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(loyalFan);
						}
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}
				
				try{
					//To Send Order Placed notification 
					RTFOrderPlacedNotification notification = new RTFOrderPlacedNotification();
					//notification.sendRewardNotifications(customerOrder);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			orderConfirmation.setOrderId(customerOrder.getId());
			orderConfirmation.setMessage("Your Order was successfully created!");
			orderConfirmation.setStatus(1);	
			return orderConfirmation;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Opps somthing went wrong. Order was not Confirmend. Please Contact Administrator");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Opps somthing went wrong. Order was not Confirmend. Please Contact Administrator");
			return orderConfirmation;
		}
			
	}

	
	@RequestMapping(value = "/InitiateTicTrackerOrder",method = RequestMethod.POST)
	public @ResponsePayload OrderConfirmation initiateTicTrakerOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderConfirmation orderConfirmation = new OrderConfirmation();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}
			

			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String eventIdStr = request.getParameter("eventId");
			String categoryTicketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String orderTotalStr = request.getParameter("orderTotal");
			String isFanPriceStr = request.getParameter("isFanPrice");
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String primaryTransactionId = request.getParameter("primaryTransactionId");			
			String primaryPaymentAmountStrs = request.getParameter("primaryPaymentAmount");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String secondaryTransactionId = request.getParameter("secondaryTransactionId");
			String secondaryPaymentAmountStrs = request.getParameter("secondaryPaymentAmount");
			String thirdPaymentMethodStr = request.getParameter("thirdPaymentMethod");
			String isLongSaleStr = request.getParameter("isLongSale");
			String serviceFeesStr = request.getParameter("serviceFees");
			
			String billingAddressIdStr = request.getParameter("billingAddressId");
			String shippingAddressIdStr = request.getParameter("shippingAddressId");
			String shippingSameAsBilling = request.getParameter("shippingSameAsBilling");
			String loyaltySpentStr = request.getParameter("rewardPoints");
			String appPlatformStr = request.getParameter("platForm");
			String referralCode = request.getParameter("referralCode");
			String refCodeIdentity = request.getParameter("eventRefIdentity"); 
			String refCId = request.getParameter("cId"); 
			String bFirstName = request.getParameter("bFirstName");
			String bLastName = request.getParameter("bLastName");
			String bAddress1 = request.getParameter("bAddress1");
			String bAddress2 = request.getParameter("bAddress2");
			String bCity = request.getParameter("bCity");
			String bState = request.getParameter("bState");
			String bCountry = request.getParameter("bCountry");
			String bZipCode = request.getParameter("bZipCode");
			String bPhoneNumber = request.getParameter("bPhoneNumber");
			String bEmail = request.getParameter("bEmail");
			String sFirstName = request.getParameter("sFirstName");
			String sLastName = request.getParameter("sLastName");
			String sAddress1 = request.getParameter("sAddress1");
			String sAddress2 = request.getParameter("sAddress2");
			String sCity = request.getParameter("sCity");
			String sState = request.getParameter("sState");
			String sCountry = request.getParameter("sCountry");
			String sZipCode = request.getParameter("sZipCode");
			String sPhoneNumber = request.getParameter("sPhoneNumber");
			String sEmail = request.getParameter("sEmail");
			String orderTypeStr = request.getParameter("orderType");
			String onDayBeforeTheEventStr = request.getParameter("onDayBeforeTheEvent");
			
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Product Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Application Platform is Mandatory");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(orderTypeStr)){
				error.setDescription("Order Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Order Type is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			OrderType orderType = null;
			try{
				orderType = OrderType.valueOf(orderTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid Order Type");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Order Type");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Please send valid Event Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Event Id");
				return orderConfirmation;
			}
			if(TextUtil.isEmptyOrNull(categoryTicketGroupIdStr)){
				error.setDescription("Category Ticket group is mandatory..");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Category Ticket group is mandatory");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(isFanPriceStr)){
				/*error.setDescription("Fan Price Flag is mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Fan Price Flag is mandatory");
				return orderConfirmation;*/
				
				isFanPriceStr = "false";
			}
			
			if(TextUtil.isEmptyOrNull(isLongSaleStr)){
				isLongSaleStr = "false";
			}
			
			Boolean isLongSale = null;
			try{
				isLongSale = Boolean.valueOf(isLongSaleStr);
			}catch(Exception e){
				error.setDescription("Long Sale Flag should be true or false");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Long Sale Flag should be true or false");
				return orderConfirmation;
			}
			
			Boolean isFanPrice = null;
			try{
				isFanPrice = Boolean.valueOf(isFanPriceStr);
			}catch(Exception e){
				error.setDescription("Fan Price Flag should be true or false");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Fan Price Flag should be true or false");
				return orderConfirmation;
			}
			
			Integer customerId = null;
			
			try{
				customerId = Integer.parseInt(customerIdStr);
				
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			Integer eventId = null;
			try{
				eventId = Integer.parseInt(eventIdStr);
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			Integer categoryTicketGroupId = null;
			try{
				categoryTicketGroupId = Integer.parseInt(categoryTicketGroupIdStr);
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(sessionId)){
				error.setDescription("Session Id is invalid.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Session Id is invalid.");
				return orderConfirmation;
			}
			
			CustomerOrderRequest orderRequest = new CustomerOrderRequest();
			Customer customer = CustomerUtil.getCustomerById(customerId);//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
			Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, productType);
			if(event==null){
				event = DAORegistry.getEventDAO().getEventById(eventId, productType);
			}
			
			CategoryTicketGroup categoryTicketGroup = null;
			categoryTicketGroup =  DAORegistry.getCategoryTicketGroupDAO().getBroadCastedCategoryTicketsbyId(categoryTicketGroupId);
			
			if(customer == null){
				error.setDescription("Please choose valid customer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please choose valid customer");
				return orderConfirmation;
			}
			
			if(event == null){
				error.setDescription("Event not recognized");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Event not recognized");
				return orderConfirmation;
			}
			
			if(categoryTicketGroup == null){
				error.setButtonValue("Find Other Tickets to this Event");
				error.setOverRideButton(true);
				error.setDescription("The ticket or rate you chose is no longer available. " +
				"If you've just bought a similar ticket, its possible you bought the last ticket available");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Ticket that you are looking for is already been sold out...");
				return orderConfirmation;
			}
			
			
			//paymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,PARTIAL_REWARDS,FULL_REWARDS
			if(TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				error.setDescription("Primary Payment method is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary Payment method is mandatory");
				return orderConfirmation;
			}
			
			PaymentMethod primaryPaymentMethod=null;
			if(!TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				try{
					primaryPaymentMethod = PaymentMethod.valueOf(primaryPaymentMethodStr);
				}catch(Exception e){
					error.setDescription("Please send valid Primary Payment Method");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Primary Payment Method");
					return orderConfirmation;
				}
			}
			
			Integer shippingAddressId=null,billingAddressId=null;
			
			Double orderTotal = null;
			try{
				orderTotal = Double.valueOf(orderTotalStr.trim());
				orderTotal = Double.valueOf(df.format(orderTotal));
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Order Total is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Order Total is Mandatory");
				return orderConfirmation;
			}
			
			boolean onDayBeforeTheEvent = false;
			
			if(!TextUtil.isEmptyOrNull(onDayBeforeTheEventStr)){
				onDayBeforeTheEvent = Boolean.valueOf(onDayBeforeTheEventStr);
			}
			
			CustomerSuperFan customerLoyalFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
			
			Double originalOrderTotal = null,serviceFees = 0.00,serverOrderTotal=0.00,originalTicketPrice = categoryTicketGroup.getOriginalPrice();
			
			serverOrderTotal = orderTotal;
			
			serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getOriginalPrice(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
			originalOrderTotal =  ((categoryTicketGroup.getOriginalPrice() * categoryTicketGroup.getQuantity()) + serviceFees);
			originalOrderTotal =  TicketUtil.getRoundedValue(originalOrderTotal);
			
			
			//Setting up Ticket Original Price
			orderRequest.setTicketPrice(originalTicketPrice);
			
			RTFPromotionalOfferTracking promoCodeTracking = null;
			boolean rtfPromotionalCode = false, isCustReferalDiscountCode = false,isAffilateReferral = false;
			Double ticketPrice = categoryTicketGroup.getOriginalPrice();
			if(null != referralCode && !referralCode.isEmpty()){
				RTFCustomerPromotionalOffer custPromoOffer = DAORegistry.getRtfCustomerPromotionalOfferDAO().getPromoOfferCustomerId(customer.getId());
				Boolean isValidRTFPromoCode = FavoriteUtil.isValidCustomerPromotionalCode(custPromoOffer, referralCode.trim());
				if(isValidRTFPromoCode){
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
							sessionId, categoryTicketGroupId, eventId);
					rtfPromotionalCode = true;
				}else if(RTFPromotionalOfferUtil.validateRTFPromoCode(referralCode.trim())){
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
							sessionId, categoryTicketGroupId, eventId);
					rtfPromotionalCode = true;
				}else {
					
					if(RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim())){
						promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
								sessionId, categoryTicketGroupId, eventId);
						if(null != promoCodeTracking && promoCodeTracking.getOfferType().equals(PromotionalType.AFFILIATE_PROMO_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							isValidRTFPromoCode = true;
							isAffilateReferral = true;
						}else{
							promoCodeTracking = null;
						}
					}else{
						promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
								sessionId, categoryTicketGroupId, eventId);
						
						if(promoCodeTracking.getOfferType().equals(PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							isValidRTFPromoCode = true;
							isCustReferalDiscountCode = true;
						}else{
							promoCodeTracking = null;
						}
					}
				}
			}
			//serverOrderTotal = ((categoryTicketGroup.getOriginalPrice() * categoryTicketGroup.getQuantity()) + serviceFees);
			
			//serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getOriginalPrice(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
			
			System.out.println(orderTotalStr);
			System.out.println(serverOrderTotal);
			
			Double discountAmount = 0.00;
			//OrderType orderType = OrderType.REGULAR;
			Double soldPrice = categoryTicketGroup.getSoldPrice();
			Double primaryPayAmt = 0.00,secondaryPayAmt=0.00,thirdPayAmount=0.00;
			CustomerSuperFan changeLoyalFan = null;
			
			if(isFanPrice){
				if(null != customerLoyalFan){
					
					Boolean isLoyalFanEvent = FavoriteUtil.markArtistAsLoyalFan(customerLoyalFan, event);
					
					if(!isLoyalFanEvent){
						if(!customerLoyalFan.getTicketPurchased()){
							//serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getLoyalFanPriceDouble(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
							//serverOrderTotal =  ((categoryTicketGroup.getLoyalFanPriceDouble() * categoryTicketGroup.getQuantity()) + serviceFees);
							orderType = OrderType.LOYALFAN;
							customerLoyalFan.setEndDate(new Date());
							customerLoyalFan.setStatus(Status.DELETED);
							customerLoyalFan.setUpdatedDate(new Date());
							
							changeLoyalFan = new CustomerSuperFan();
							
							if(event.getParentCategoryId().equals(1)){
								List<Artist> artists = DAORegistry.getQueryManagerDAO().getArtistByEvent(event.getEventId());
								Artist artist = artists.get(0);
								changeLoyalFan.setArtistOrTeamId(artist.getId());
								changeLoyalFan.setLoyalFanName(artist.getName());
								changeLoyalFan.setLoyalFanType(LoyalFanType.SPORTS);
							}else{
								changeLoyalFan.setArtistOrTeamId(-1);
								String loyalFanName = event.getState();
								List<State> states = DAORegistry.getStateDAO().getAllStateBySearchKey(event.getState());
								if(null != states && !states.isEmpty()){
									loyalFanName = states.get(0).getName();
								}
								changeLoyalFan.setLoyalFanName(loyalFanName);
								changeLoyalFan.setState(event.getState());
								changeLoyalFan.setCountry(event.getCountry());
								//changeLoyalFan.setZipCode(null != event.getPinCode() && event.getPinCode().isEmpty()?event.getPinCode():null);
								LoyalFanType loyalFanType = event.getParentCategoryId().equals(2)?LoyalFanType.CONCERTS:LoyalFanType.THEATER;
								changeLoyalFan.setLoyalFanType(loyalFanType);
							}
							changeLoyalFan.setCreatedDate(new Date());
							changeLoyalFan.setUpdatedDate(new Date());
							changeLoyalFan.setCustomerId(customerId);
							changeLoyalFan.setStartDate(new Date());
							changeLoyalFan.setStatus(Status.ACTIVE);
							changeLoyalFan.setTicketPurchased(true);
						}else{
							error.setDescription("You are currently not eligible to purchase this ticket at a discounted price. You will be redirected back to ticket listings.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							orderConfirmation.setRedirectTicketListing(true);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are currently not eligible to purchase this ticket at a discounted price");
							return orderConfirmation;
						}
					}else {
						customerLoyalFan.setTicketPurchased(true);
						customerLoyalFan.setUpdatedDate(new Date());
						orderType = OrderType.LOYALFAN;
					}
				}else{
					
					orderType = OrderType.LOYALFAN;
					changeLoyalFan = new CustomerSuperFan();
					
					if(event.getParentCategoryId().equals(1)){
						List<Artist> artists = DAORegistry.getQueryManagerDAO().getArtistByEvent(event.getEventId());
						Artist artist = artists.get(0);
						changeLoyalFan.setArtistOrTeamId(artist.getId());
						changeLoyalFan.setLoyalFanName(artist.getName());
						changeLoyalFan.setLoyalFanType(LoyalFanType.SPORTS);
					}else {
						changeLoyalFan.setArtistOrTeamId(-1);
						String loyalFanName = event.getState();
						List<State> states = DAORegistry.getStateDAO().getAllStateBySearchKey(event.getState());
						if(null != states && !states.isEmpty()){
							loyalFanName = states.get(0).getName();
						}
						changeLoyalFan.setLoyalFanName(loyalFanName);
						changeLoyalFan.setState(event.getState());
						changeLoyalFan.setCountry(event.getCountry());
						//changeLoyalFan.setZipCode(null != event.getPinCode() && event.getPinCode().isEmpty()?event.getPinCode():null);
						LoyalFanType loyalFanType = event.getParentCategoryId().equals(2)?LoyalFanType.CONCERTS:LoyalFanType.THEATER;
						changeLoyalFan.setLoyalFanType(loyalFanType);
					}
					changeLoyalFan.setCreatedDate(new Date());
					changeLoyalFan.setUpdatedDate(new Date());
					changeLoyalFan.setCustomerId(customerId);
					changeLoyalFan.setStartDate(new Date());
					changeLoyalFan.setStatus(Status.ACTIVE);
					changeLoyalFan.setTicketPurchased(true);
				}
			}
			serverOrderTotal =  TicketUtil.getRoundedValue(serverOrderTotal);
			discountAmount = originalOrderTotal - orderTotal;
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double rewardEarnConv = 0.00;
			
			/*if(RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim())){
				rewardEarnConv = loyaltySettings.getAffiliatePromoRewardConv();
			}else */
				
			if(isCustReferalDiscountCode){
				rewardEarnConv = promoCodeTracking.getCustomerEarnRewardConv();
			}else{
				rewardEarnConv = loyaltySettings.getRewardConversion();
			}
			
			primaryPayAmt = serverOrderTotal;
			
			CustomerLoyaltyHistory customerLoyaltyHistory = new CustomerLoyaltyHistory();
			CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			PaypalTransactionDetail paypalTransactionDetail = null;
			
			Double activeRewardAmount = RewardConversionUtil.getRewardDoller(customerLoyalty.getActivePointsAsDouble(),loyaltySettings.getDollerConversion());
			Double requiredPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getDollerConversion());
			Double orderRewardEarnAmount = RewardConversionUtil.getRewardDoller(requiredPoints, loyaltySettings.getDollerConversion());
			
			
			Double orderTotalForRewards = serverOrderTotal;
			try{
				if(null != loyaltySpentStr && !loyaltySpentStr.isEmpty()){
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(Double.valueOf(loyaltySpentStr.trim()),loyaltySettings.getDollerConversion());
					orderTotalForRewards = TicketUtil.getRoundedValue(orderTotalForRewards - enteredRewardAmount);
					
					if(orderTotalForRewards < 0){
						orderTotalForRewards = 0.00;
					}
				}				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
			if(isAffilateReferral){
				Affiliate affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(referralCode.trim());
				if(affiliateObj!= null && !affiliateObj.getIsEarnRewardPoints()){
					earningRewardPoints = 0.00;
				}
			}
			if(primaryPaymentMethod.equals(PaymentMethod.ACCOUNT_RECIVABLE)){
				earningRewardPoints = 0.00;
			}
			
			
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setOrderTotal(orderTotal);
			customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
			customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setPointsSpentAsDouble(0.00);
			customerLoyaltyHistory.setRewardSpentAmount(0.00);
			customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
			customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
			customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
			customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
			Double balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - 0;
			Double balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
			customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
			
			CustomerWallet customerWallet = null;
			CustomerWalletHistory customerWalletHistory = null;
			PartialPaymentMethod secondaryPaymentMethod=null;
			PartialPaymentMethod thirdPaymentMethod=null;
			String thirdPaymentTrxId = null;
			AddressInfo addressInfo = new AddressInfo();
			Boolean isSaveCard = false;
			switch (primaryPaymentMethod) {
			
				case FULL_REWARDS:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					UserAddress shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.FULL_REWARDS);
					addressInfo.setShippingAddress(shippingAddress);
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("Please enter active reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter active reward dollars");
						return orderConfirmation;
					}	
					
					Double loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("Reward dollars amount must be an integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars amount must be an integer");
						return orderConfirmation;
					}
					
					
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(requiredPoints < loyaltySpent){
						Double diffOrderReward = loyaltySpent - requiredPoints;
						error.setDescription("Your have entered an amount more than the required reward dollars. Please subtract "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered an amount more than the required reward dollars.  Please subtract  "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						return orderConfirmation;
					}
					
					if(requiredPoints > loyaltySpent){
						Double diffOrderReward = requiredPoints - loyaltySpent;
						error.setDescription("Your have entered an amount less than the required reward dollars. Please add "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than required reward dollars , reduce "+TicketUtil.getRoundedValueString(diffOrderReward)+" more reward dollars");
						return orderConfirmation;
					}
					
					if((customerLoyalty.getActivePointsAsDouble() < loyaltySpent) ||
							(activeRewardAmount < enteredRewardAmount)){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(activeRewardAmount < enteredRewardAmount){
						error.setDescription("Please enter a valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
					
					break;
					
				case PARTIAL_REWARDS:
					
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("Reward dollars are Mandatory.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars are Mandatory");
						return orderConfirmation;
					}	
					
					loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("Reward dollars should be Integer.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars should be Integer");
						return orderConfirmation;
					}
					

					//Secondary PaymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY
					if(TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						error.setDescription("Please enter a valid Secondary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
						return orderConfirmation;
					}
					
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(activeRewardAmount < loyaltySpent){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(requiredPoints < loyaltySpent){
						error.setDescription("Your have entered more than the required reward dollars. Please select Full Rewards as Primary Payment Method."); 
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than the required reward dollars. Please select  Full Rewards as Primary Payment Method.");
						return orderConfirmation;
					}
					
					if(requiredPoints == loyaltySpent){
						error.setDescription("Your have entered the required reward dollars. Please select Full Rewards as Primary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered the required reward dollars amount. Please select  Full Rewards as Primary Payment Method");
						return orderConfirmation;
					}
					
					Double tempOrderTotal = serverOrderTotal - enteredRewardAmount ;
					primaryPayAmt = enteredRewardAmount;
					
					if(secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						try{
							billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
						}catch(Exception e){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
							shippingAddressId =  billingAddressId;
						}else{
							if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
								error.setDescription("Please enter a valid Shipping Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
								return orderConfirmation;
							}
							
							try{
								shippingAddressId = Integer.parseInt(shippingAddressIdStr);
							}catch(Exception e){
								error.setDescription("Shipping Address should be integer");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
								return orderConfirmation;
							}
						}
						
						UserAddress billingAddress = null;
						if(null != billingAddressId){
							billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
						}
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						if(billingAddress == null){
							error.setDescription("Billing Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
							return orderConfirmation;
						}
						addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(billingAddress);
						
						secondaryTransactionId = "PARTIAL_CREDITCARD_TRANSACTION";
						
					}else if (secondaryPaymentMethod.equals(PartialPaymentMethod.CUSTOMER_WALLET)){
						
						String walletTransactionIdStr = request.getParameter("walletTransactionId");
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						secondaryTransactionId = "PARTIAL_CUSTOMER_WALLET_TRANSACTION";
						
						if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						try{
							billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
						}catch(Exception e){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
							shippingAddressId =  billingAddressId;
						}else{
							if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
								error.setDescription("Please enter a valid Shipping Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
								return orderConfirmation;
							}
							
							try{
								shippingAddressId = Integer.parseInt(shippingAddressIdStr);
							}catch(Exception e){
								error.setDescription("Shipping Address should be integer");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
								return orderConfirmation;
							}
						}
						
						UserAddress billingAddress = null;
						if(null != billingAddressId){
							billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
						}
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						if(billingAddress == null){
							error.setDescription("Billing Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
							return orderConfirmation;
						}
						addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(billingAddress);
						
						if(!TextUtil.isEmptyOrNull(thirdPaymentMethodStr)){
							try{
								thirdPaymentMethod = PartialPaymentMethod.valueOf(thirdPaymentMethodStr);
							}catch(Exception e){
								error.setDescription("Please enter a valid Third Payment Method");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Third Payment Method");
								return orderConfirmation;
							}
						}
						
						if(null != thirdPaymentMethod && thirdPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
							
							String thirdPaymentAmountStrs = request.getParameter("thirdPaymentAmount");
							String thirdTransactionId = request.getParameter("thirdTransactionId");
							
							if(TextUtil.isEmptyOrNull(thirdPaymentAmountStrs)){
								error.setDescription("Third payment amount is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Third payment amount is not valid");
								return orderConfirmation;
							}
							
							try{
								thirdPayAmount = Double.valueOf(thirdPaymentAmountStrs.trim());
							}catch(Exception e){
								error.setDescription("Third payment amount is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Third payment amount is not valid");
								return orderConfirmation;
							}
							
							if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
								error.setDescription("Please enter a valid Billing Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
								return orderConfirmation;
							}
							
							try{
								billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
							}catch(Exception e){
								error.setDescription("Please enter a valid Billing Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
								return orderConfirmation;
							}
							
							if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
								shippingAddressId =  billingAddressId;
							}else{
								if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
									error.setDescription("Please enter a valid Shipping Address");
									orderConfirmation.setError(error);
									orderConfirmation.setStatus(0);
									TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
									return orderConfirmation;
								}
								
								try{
									shippingAddressId = Integer.parseInt(shippingAddressIdStr);
								}catch(Exception e){
									error.setDescription("Shipping Address should be integer");
									orderConfirmation.setError(error);
									orderConfirmation.setStatus(0);
									TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
									return orderConfirmation;
								}
							}
							
							billingAddress = null;
							if(null != billingAddressId){
								billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
							}
							shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
							
							if(shippingAddress == null){
								error.setDescription("Shipping Address is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
								return orderConfirmation;
							}
							
							if(billingAddress == null){
								error.setDescription("Billing Address is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
								return orderConfirmation;
							}
							addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
							addressInfo.setShippingAddress(shippingAddress);
							addressInfo.setBillingAddress(billingAddress);
							thirdPaymentTrxId = "PARTIAL_THIRD_CREDITCARD_TRXID";
						}
					}
					
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
								
					
					break;
					
				case CREDITCARD:
					
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					UserAddress billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					primaryTransactionId = "CREDITCARD_TRANSACTION";
	
					break;
					
				case PAYPAL:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setPaymentMethod(PaymentMethod.PAYPAL);
					/*addressInfo.setValues(bFirstName, bLastName,bAddress1, bAddress2, bCity, bState, bCountry, bZipCode,bPhoneNumber,bEmail,
							sFirstName,sLastName, sAddress1,sAddress2, sCity, sState, sCountry, sZipCode,sPhoneNumber,sEmail);
					
					addressInfo.setShippingAddress(null);
					addressInfo.setBillingAddress(null);*/
					
					primaryTransactionId = "PAYPAL_TRANSACTION";
					
					break;
					
				case GOOGLEPAY:
					
					addressInfo.setValues(bFirstName, bLastName,bAddress1, bAddress2, bCity, bState, bCountry, bZipCode,bPhoneNumber,bEmail,
							sFirstName,sLastName, sAddress1,sAddress2, sCity, sState, sCountry, sZipCode,sPhoneNumber,sEmail);
					addressInfo.setPaymentMethod(PaymentMethod.GOOGLEPAY);
					addressInfo.setShippingAddress(null);
					addressInfo.setBillingAddress(null);
					
					primaryTransactionId = "GOOGLEPAY_TRANSACTION";
					
					break;
					
				case IPAY:
					
					addressInfo.setValues(bFirstName, bLastName,bAddress1, bAddress2, bCity, bState, bCountry, bZipCode,bPhoneNumber,bEmail,
							sFirstName,sLastName, sAddress1,sAddress2, sCity, sState, sCountry, sZipCode,sPhoneNumber,sEmail);
					addressInfo.setPaymentMethod(PaymentMethod.IPAY);
					addressInfo.setShippingAddress(null);
					addressInfo.setBillingAddress(null);
					
					primaryTransactionId = "GOOGLEPAY_TRANSACTION";
					
					break;
					
				case CUSTOMER_WALLET:
					
					if(TextUtil.isEmptyOrNull(primaryPaymentAmountStrs)){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					try{
						primaryPayAmt = Double.valueOf(primaryPaymentAmountStrs.trim());
					}catch(Exception e){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					String walletTransactionIdStr = request.getParameter("walletTransactionId");
					
					primaryTransactionId = "CUSTOMER_WALLET";
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					if(null != secondaryPaymentMethod && secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						/*if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}*/
						
						secondaryTransactionId = "WALLET_CREDITCARD_TRANSACTION";
						
					}
					
					break;
					
				case ACCOUNT_RECIVABLE:
					
					if(TextUtil.isEmptyOrNull(primaryPaymentAmountStrs)){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					try{
						primaryPayAmt = Double.valueOf(primaryPaymentAmountStrs.trim());
					}catch(Exception e){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					primaryTransactionId = "ACCOUNT_RECIVABLE";
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					break;

	
				default:
					break;
			}
			
			
			CustomerOrderDetail customerOrderDetail = new CustomerOrderDetail();
			
			if(addressInfo.getPaymentMethod().equals(PaymentMethod.CREDITCARD)){
				
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getBillingAddress().getAddressLine1());
				customerOrderDetail.setBillingAddress2(addressInfo.getBillingAddress().getAddressLine2());
				customerOrderDetail.setBillingCity(addressInfo.getBillingAddress().getCity());
				customerOrderDetail.setBillingCountry(addressInfo.getBillingAddress().getCountry().getCountryName());
				customerOrderDetail.setBillingFirstName(addressInfo.getBillingAddress().getFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getBillingAddress().getLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getBillingAddress().getPhone1());
				customerOrderDetail.setBillingPhone2(addressInfo.getBillingAddress().getPhone2());
				customerOrderDetail.setBillingState(addressInfo.getBillingAddress().getState().getName());
				/*customerOrderDetail.setBillingCountryId(addressInfo.getBillingAddress().getCountry().getId());
				customerOrderDetail.setBillingStateId(addressInfo.getBillingAddress().getState().getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getBillingAddress().getZipCode());
				customerOrderDetail.setBillingEmail(null != addressInfo.getBillingAddress().getEmail()?addressInfo.getBillingAddress().getEmail():customer.getEmail());
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			} else if (addressInfo.getPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else if (addressInfo.getPaymentMethod().equals(PaymentMethod.PAYPAL)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else{
				/*Country billingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getbCountry());
				State billingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(billingCountry.getId(), addressInfo.getbState());*/
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getbAddress1());
				customerOrderDetail.setBillingAddress2(addressInfo.getbAddress2());
				customerOrderDetail.setBillingCity(addressInfo.getbCity());
				customerOrderDetail.setBillingCountry(addressInfo.getbCountry());
				customerOrderDetail.setBillingFirstName(addressInfo.getbFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getbLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getbPhoneNumber());
				customerOrderDetail.setBillingState(addressInfo.getbState());
				/*customerOrderDetail.setBillingCountryId(billingCountry.getId());
				customerOrderDetail.setBillingStateId(billingState.getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getbZipCode());
				customerOrderDetail.setBillingEmail(null!=addressInfo.getbEmail()?addressInfo.getbEmail():customer.getEmail());
				
				/*Country shippingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getsCountry());
				State shippingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(shippingCountry.getId(), addressInfo.getsState());*/
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getsAddress1());
				customerOrderDetail.setShippingAddress2(addressInfo.getsAddress2());
				customerOrderDetail.setShippingCity(addressInfo.getsCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getsFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getsLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getsPhoneNumber());
				customerOrderDetail.setShippingCountry(addressInfo.getsCountry());
				customerOrderDetail.setShippingState(addressInfo.getsState());
				/*customerOrderDetail.setShippingCountryId(shippingCountry.getId());
				customerOrderDetail.setShippingStateId(shippingState.getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getsZipCode());
				customerOrderDetail.setShippingEmail(null!=addressInfo.getsEmail()?addressInfo.getsEmail():customer.getEmail());
				
			}
			
			
			CustomerLoyaltyHistory refererCustLoyaltyHistory = null;
			CustomerLoyalty refererCustomerLoyalty = null;
			ReferredCodeTracking referredCodeTracking = null;
			Boolean validReferralCode = false,affiliatePromoCode = false;
			
			AffiliateCashReward affiliateCashReward = null;
			AffiliateCashRewardHistory affiliateCashRewardHistory = null;
			
			//Customer custReferal = null;
			if(null != referralCode && !referralCode.isEmpty() && (!rtfPromotionalCode || isCustReferalDiscountCode || isAffilateReferral)){
				
				Affiliate affiliate = RTFAffiliateBrokerUtil.getAffiliateObjByPromoCode(referralCode.trim());
				
				if(null != affiliate && !isCustReferalDiscountCode){
					affiliatePromoCode =  true;
					affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(affiliate.getUserId());
					Double pendingCash = RewardConversionUtil.getAffiliateCashCredit(orderTotalForRewards, affiliate.getPhoneOrderCashReward());
					
					if(null == affiliateCashReward){
						affiliateCashReward= new AffiliateCashReward();
						affiliateCashReward.setUserId(affiliate.getUserId());
						affiliateCashReward.setActiveCash(0.00);
						affiliateCashReward.setPendingCash(pendingCash);
						affiliateCashReward.setLastCreditedCash(0.00);
						affiliateCashReward.setTotalCreditedCash(0.00);
						affiliateCashReward.setLastDebitedCash(0.00);
						affiliateCashReward.setTotalDebitedCash(0.00);
						affiliateCashReward.setLastVoidCash(0.00);
						affiliateCashReward.setTotalVoidedCash(0.00);
					}else{
						affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash()+pendingCash);
					}
					affiliateCashRewardHistory = new AffiliateCashRewardHistory();
					affiliateCashRewardHistory.setPromoCode(referralCode.trim());
					affiliateCashRewardHistory.setCustomerId(customerId);
					affiliateCashRewardHistory.setUserId(affiliate.getUserId());
					affiliateCashRewardHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					affiliateCashRewardHistory.setCreditConv(affiliate.getPhoneOrderCashReward());
					affiliateCashRewardHistory.setCreditedCash(pendingCash);
					affiliateCashRewardHistory.setCreateDate(new Date());
					affiliateCashRewardHistory.setUpdatedDate(new Date());
					affiliateCashRewardHistory.setIsRewardsEmailSent(Boolean.FALSE);
					affiliateCashRewardHistory.setOrderTotal(orderTotalForRewards);
					
					rtfPromotionalCode = true;
				}else{
					Customer custReferrer = DAORegistry.getCustomerDAO().getCustomerByIdByProduct(promoCodeTracking.getReferredBy(), productType);
					if(custReferrer.getReferrerCode() == null){
					}else{
					}
					refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferrer.getId());
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, promoCodeTracking.getReferrarEarnRewardConv());
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferrer.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(promoCodeTracking.getReferrarEarnRewardConv());
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderType(orderType);
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferrer.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferrer.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("ReferralCode");
					
					validReferralCode = true;
					rtfPromotionalCode = true;
					
				}
				
			}else if((null != refCodeIdentity && !refCodeIdentity.isEmpty()) 
					&& (refCId != null && !refCId.isEmpty())){
				
				Customer custReferal = new Customer();
				custReferal = SecurityUtil.validateReferalIdentity(refCId, refCodeIdentity, eventId, customerId, referredCodeTracking,custReferal);
				if(custReferal.getEventShareValidated()){
					try{
						refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferal.getId());	
					}catch(Exception e){
						e.printStackTrace();
					}
					
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
					
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderId(custReferal.getId());
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferal.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferal.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("EventShareLinkReferral");
					
					validReferralCode = true;
					
				}
				
				
			}
			
			Property property = null;
			if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
				Long primaryTrxId =  Long.valueOf(property.getValue());
				primaryTrxId++;
				primaryTransactionId = PaginationUtil.fullRewardPaymentPrefix+primaryTrxId;
				property.setValue(String.valueOf(primaryTrxId));
			}else if(primaryPaymentMethod.equals(PaymentMethod.PARTIAL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				Long partialPrimaryTrxId =  Long.valueOf(property.getValue());
				partialPrimaryTrxId++;
				primaryTransactionId = PaginationUtil.partialRewardPaymentPrefix+partialPrimaryTrxId;
				property.setValue(String.valueOf(partialPrimaryTrxId));
			}
			
			CustomerOrder customerOrder = new CustomerOrder();
			Customer customerDetail = new Customer();
			customerDetail.setId(customerId);
			customerOrder.setCategoryTicketGroupId(categoryTicketGroup.getId());
			customerOrder.setOrderType(orderType);
			customerOrder.setCustomer(customerDetail);
			customerOrder.setEventDateTemp(event.getEventDate());
			customerOrder.setEventId(event.getEventId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventTime(null != event.getEventTime()?event.getEventTime():null);
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setVenueName(event.getVenueName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueState(event.getState());
			customerOrder.setVenueCountry(event.getCountry());
			customerOrder.setProductType(productType);
			customerOrder.setShippingMethod(categoryTicketGroup.getShippingMethod());
			customerOrder.setTaxesAsDouble(null != serviceFeesStr && !serviceFeesStr.isEmpty()?Double.valueOf(serviceFeesStr.trim()):serviceFees);
			//customerOrder.setTaxesAsDouble(serviceFees);
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(event.getEventDate());
			calendar.add(Calendar.DAY_OF_MONTH, -5);
			
			Double finalTicketPrice = serverOrderTotal / categoryTicketGroup.getQuantity();
			
			customerOrder.setShippingDateTemp(calendar.getTime());
			customerOrder.setStatus(OrderStatus.PAYMENT_PENDING);
			customerOrder.setQty(categoryTicketGroup.getQuantity());			
			customerOrder.setSection(categoryTicketGroup.getSection());
			customerOrder.setSectionDescription(categoryTicketGroup.getSectionDescription());
			customerOrder.setLastUpdatedDateTemp(new Date());
			customerOrder.setCreateDateTemp(new Date());			
			customerOrder.setTicketPriceAsDouble(finalTicketPrice);
			customerOrder.setTicketSoldPriceAsDouble(soldPrice);
			customerOrder.setOriginalOrderTotal(originalOrderTotal);
			customerOrder.setDiscountAmount(discountAmount);
			customerOrder.setOrderTotalAsDouble(serverOrderTotal);
			customerOrder.setPrimaryPayAmtAsDouble(primaryPayAmt);
			customerOrder.setSecondaryPayAmtAsDouble(secondaryPayAmt);
			customerOrder.setPrimaryPaymentMethod(primaryPaymentMethod);
			customerOrder.setPrimaryTransactionId(primaryTransactionId);
			customerOrder.setSecondaryPaymentMethod(null != secondaryPaymentMethod ? secondaryPaymentMethod:PartialPaymentMethod.NULL);
			customerOrder.setSecondaryTransactionId(null != secondaryTransactionId ? secondaryTransactionId:"");
			customerOrder.setThirdPaymentMethod(null != thirdPaymentMethod ? thirdPaymentMethod:PartialPaymentMethod.NULL);
			customerOrder.setThirdTransactionId(null != thirdPaymentTrxId ? thirdPaymentTrxId:"");
			customerOrder.setThirdPayAmtAsDouble(thirdPayAmount);
			customerOrder.setAppPlatForm(applicationPlatForm);
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setIsInvoiceSent(false);
			customerOrder.setBrokerId(categoryTicketGroup.getBrokerId());
			customerOrder.setBrokerServicePerc(TicketUtil.getBrokerServiceFeePerc(categoryTicketGroup.getBrokerId()));
			
			customerOrder.setIsLongSale(isLongSale);
			if(isLongSale){
				customerOrder.setActualSeat(categoryTicketGroup.getActualSeat());
				customerOrder.setActualSection(categoryTicketGroup.getLongSection());
				customerOrder.setActualRow(categoryTicketGroup.getRow());
			}
			
			
			DAORegistry.getCustomerOrderDAO().saveOrUpdate(customerOrder);
		    
			if(null != property){
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
			}
			
			/*if(customerWallet != null){
				customerWallet.setPendingDebit(0.00);
				customerWallet.setLastUpdated(new Date());
				customerWalletHistory.setOrderId(customerOrder.getId());
				customerWalletHistory.setTransactionStatus("APPROVED");
				customerWalletHistory.setUpdatedOn(new Date());
				DAORegistry.getCustomerWalletDAO().saveOrUpdate(customerWallet);
				DAORegistry.getCustomerWalletHistoryDAO().saveOrUpdate(customerWalletHistory);
			}*/
			
			/*Invoice invoice = null;
			if(customerOrder != null){
				invoice = new Invoice();
				invoice.setCustomerOrderId(customerOrder.getId());
				invoice.setInvoiceTotal(serverOrderTotal);
				invoice.setTicketCount(categoryTicketGroup.getQuantity());
				invoice.setCustomerId(customerId);
				invoice.setExtPONo(null);
				invoice.setStatus(InvoiceStatus.Outstanding);
				invoice.setInvoiceType("INVOICE");
				invoice.setCreatedDate(new Date());
				invoice.setCreatedBy(customer.getCustomerName());
				invoice.setIsSent(false);
				invoice.setSentDate(null);
				invoice.setRealTixMap("NO");
				invoice.setBrokerId(categoryTicketGroup.getBrokerId());
				//Persisting the customer order details in invoice
				DAORegistry.getInvoiceDAO().save(invoice);
				
				if(invoice.getId() != null){
					DAORegistry.getCategoryTicketGroupDAO().updateCategoryTicket(categoryTicketGroup.getId(), invoice.getId());
				}
			}*/
			
			
			customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()-(null != customerLoyaltyHistory.getPointsSpentAsDouble()?customerLoyaltyHistory.getPointsSpentAsDouble():0));
			customerLoyalty.setPendingPointsAsDouble((null != customerLoyalty.getPendingPointsAsDouble()?customerLoyalty.getPendingPointsAsDouble():0)+customerLoyaltyHistory.getPointsEarnedAsDouble());
			customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
			customerLoyalty.setLastUpdate(new Date());
			customerLoyaltyHistory.setOrderType(orderType);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setUpdatedDate(new Date());
			customerLoyaltyHistory.setOrderTotal(serverOrderTotal);
			customerLoyaltyHistory.setOrderId(customerOrder.getId());
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
			
			DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(customerLoyaltyHistory);
			
			
			if(onDayBeforeTheEvent){
				
				Double ticketDownloadRewardOffer = PaginationUtil.onDayBeforeEventTicketDownloadRewardOffer;
				Double ticketDownloadRewardDollars = RewardConversionUtil.getRewardPoints(serverOrderTotal, ticketDownloadRewardOffer);
				Double orderEarnAmount = RewardConversionUtil.getRewardDoller(ticketDownloadRewardDollars, loyaltySettings.getDollerConversion());
				
				customerLoyalty.setPendingPointsAsDouble((null != customerLoyalty.getPendingPointsAsDouble()?customerLoyalty.getPendingPointsAsDouble():0)+customerLoyaltyHistory.getPointsEarnedAsDouble());
				customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
				customerLoyalty.setLastUpdate(new Date());

				CustomerLoyaltyHistory offerRewardHistory = new CustomerLoyaltyHistory();
				offerRewardHistory.setCustomerId(customerId);
				offerRewardHistory.setRewardConversionRate(rewardEarnConv);
				offerRewardHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
				offerRewardHistory.setPointsSpentAsDouble(0.00);
				offerRewardHistory.setRewardSpentAmount(0.00);
				offerRewardHistory.setPointsEarnedAsDouble(ticketDownloadRewardDollars);
				offerRewardHistory.setRewardEarnAmount(orderEarnAmount);
				offerRewardHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
				offerRewardHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
				offerRewardHistory.setOrderType(OrderType.TICKETONDAYBEFOREEVENT);
				offerRewardHistory.setCreateDate(new Date());
				offerRewardHistory.setUpdatedDate(new Date());
				offerRewardHistory.setOrderTotal(serverOrderTotal);
				offerRewardHistory.setOrderId(customerOrder.getId());
				offerRewardHistory.setRewardStatus(RewardStatus.ORDERPENDING);
				
				Double balanceRewardPointsNew = customerLoyalty.getActivePointsAsDouble() + ticketDownloadRewardDollars - 0;
				Double balanceRewardAmountNew = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
				
				offerRewardHistory.setBalanceRewardPointsAsDouble(balanceRewardPointsNew);
				offerRewardHistory.setBalanceRewardAmount(balanceRewardAmountNew);
				
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
				DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(offerRewardHistory);
				
				orderRequest.setOrderOfferRewardHistoryId(offerRewardHistory.getId());
				orderRequest.setTicketOnDayBeforeTheEvent(true);
			}else{
				orderRequest.setTicketOnDayBeforeTheEvent(false);
			}
			
			/* Add All Pending Values to Order Request - Begins  */
			orderRequest.setSessionId(sessionId);
			orderRequest.setOrderId(customerOrder.getId());
			orderRequest.setOrderLoyaltyHistoryId(customerLoyaltyHistory.getId());
			orderRequest.setOrderLoyaltyInfoId(customerLoyalty.getId());
			orderRequest.setOrderTotal(serverOrderTotal);
			orderRequest.setOrderType(orderType);
			orderRequest.setOriginalOrderTotal(originalOrderTotal);
			orderRequest.setAppPlatForm(customerOrder.getAppPlatForm());
			orderRequest.setCategoryTicketGroupId(categoryTicketGroupId);
			orderRequest.setCustomerId(customerId);
			orderRequest.setEventId(eventId);
			orderRequest.setInvoiceId(-1);
			orderRequest.setIsLongSale(false);
			orderRequest.setIsLoyalFan(isFanPrice);
			orderRequest.setPrimaryPayAmt(primaryPayAmt);
			orderRequest.setPrimaryPaymentMethod(primaryPaymentMethod);
			orderRequest.setPrimaryTransactionId(primaryTransactionId);
			orderRequest.setSecondaryPayAmt(secondaryPayAmt);
			orderRequest.setSecondaryPaymentMethod(secondaryPaymentMethod);
			orderRequest.setSecondaryTransactionId(secondaryTransactionId);
			orderRequest.setQuantity(customerOrder.getQty());
			orderRequest.setSoldPrice(soldPrice);
			orderRequest.setThirdPayAmt(customerOrder.getThirdPayAmtAsDouble());
			orderRequest.setThirdPaymentMethod(customerOrder.getThirdPaymentMethod());
			orderRequest.setThirdTransactionId(customerOrder.getThirdTransactionId());
			orderRequest.setTicketGroupId(null);
			
			String customerIpAddress = "";
			if(customerOrder.getAppPlatForm().equals(ApplicationPlatForm.IOS) || 
					customerOrder.getAppPlatForm().equals(ApplicationPlatForm.ANDROID)){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			orderRequest.setIpAddress(customerIpAddress);
			orderRequest.setStatus(OrderStatus.PAYMENT_PENDING);
			orderRequest.setCreatedTime(new Date());
			orderRequest.setUpdatedTime(new Date());
			/* Add All Pending Values to Order Request - Ends  */
			
			
			if(rtfPromotionalCode){
				try{
					promoCodeTracking.setOrderId(customerOrder.getId());
					promoCodeTracking.setStatus("ORDERPENDING");
					promoCodeTracking.setUpdatedDate(new Date());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
					
					//Add RTF Promo Tracking ID Into Order Request
					orderRequest.setRtfPromoTrackingId(promoCodeTracking.getId());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(affiliatePromoCode){
				affiliateCashReward.setLastUpdate(new Date());
				DAORegistry.getAffiliateCashRewardDAO().saveOrUpdate(affiliateCashReward);
				
				affiliateCashRewardHistory.setOrderId(customerOrder.getId());
				DAORegistry.getAffiliateCashRewardHistoryDAO().saveOrUpdate(affiliateCashRewardHistory);
				
				try{
					
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = null;
					if(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE) || 
							applicationPlatForm.equals(ApplicationPlatForm.TICK_TRACKER)){
						
						affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(applicationPlatForm, 
								affiliateCashReward.getUserId(), customer.getId(), sessionId, categoryTicketGroupId, eventId, referralCode,false);
						
						if(null != affiliatePromoCodeTracking){
							affiliatePromoCodeTracking.setUpdatedDate(new Date());
							affiliatePromoCodeTracking.setStatus("ORDERPENDING");
							affiliatePromoCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
							affiliatePromoCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
							affiliatePromoCodeTracking.setUpdatedDate(new Date());
							affiliatePromoCodeTracking.setOrderId(customerOrder.getId());
							affiliatePromoCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
							affiliatePromoCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
						}
					}
					
					if(null == affiliatePromoCodeTracking){
						affiliatePromoCodeTracking = new AffiliatePromoCodeTracking();
						affiliatePromoCodeTracking.setTicketGroupId(categoryTicketGroupId);
						affiliatePromoCodeTracking.setEventId(eventId);
						affiliatePromoCodeTracking.setIpAddress(ip);
						affiliatePromoCodeTracking.setPlatForm(applicationPlatForm);
						affiliatePromoCodeTracking.setSessionId(sessionId);
						affiliatePromoCodeTracking.setStatus("ORDERPENDING");
						affiliatePromoCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
						affiliatePromoCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
						affiliatePromoCodeTracking.setCreatedDate(new Date());
						affiliatePromoCodeTracking.setUpdatedDate(new Date());
						affiliatePromoCodeTracking.setCustomerId(customerId);
						affiliatePromoCodeTracking.setUserId(affiliateCashReward.getUserId());
						affiliatePromoCodeTracking.setOrderId(customerOrder.getId());
						affiliatePromoCodeTracking.setPromoCode(referralCode);
						affiliatePromoCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
						affiliatePromoCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
					}
					DAORegistry.getAffiliatePromoCodeTrackingDAO().saveOrUpdate(affiliatePromoCodeTracking);
					
					//Add Affiliate Reward ID's to Order Request.
					orderRequest.setAffCashRewardHistoryId(affiliateCashRewardHistory.getId());
					orderRequest.setAffCashRewardId(affiliateCashReward.getId());
					orderRequest.setAffPromoTrackingId(affiliatePromoCodeTracking.getId());
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(validReferralCode || isCustReferalDiscountCode){
				
				refererCustLoyaltyHistory.setOrderId(customerOrder.getId());
				
				if(null != refererCustLoyaltyHistory){ //Safer side to avoid null pointer exception
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(refererCustomerLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(refererCustLoyaltyHistory);
					
					DAORegistry.getReferredCodeTrackingDAO().saveOrUpdate(referredCodeTracking);
					
					//Add Referral Reward ID's to Order Request.
					orderRequest.setRefCustLoyaltyHistoryId(refererCustLoyaltyHistory.getId());
					orderRequest.setRefCustLoyaltyInfoId(refererCustomerLoyalty.getId());
					orderRequest.setRefTrackingId(referredCodeTracking.getId());
					referralCode = referredCodeTracking.getReferredCode();
				}
			}
			
			
			if(isFanPrice ){
				   try{
					Date startdate = null;
					if(null != customerLoyalFan){
						startdate = customerLoyalFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(customerLoyalFan);
						
						//Add Old Loyal Fan ID to Order Request.
						orderRequest.setOldLoyalFanId(customerLoyalFan.getId());
						
					}
					if(null != changeLoyalFan){
						startdate = changeLoyalFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(changeLoyalFan);
						
						//Add New Loyal Fan ID to Order Request.
						orderRequest.setNewLoyalFanId(changeLoyalFan.getId());
						
						/*calendar = new GregorianCalendar();
						calendar.setTime(startdate);
						calendar.add(Calendar.YEAR,1);
						startdate = calendar.getTime();
						String formattedendDate = dateTimeFormat.format(startdate);
						
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("customer", customer);  
						mailMap.put("changeLoyalFan", changeLoyalFan); 
						mailMap.put("startdate", formattedendDate);
						MailAttachment[] mailAttachment = new MailAttachment[1];
						String filePath = URLUtil.getLogoImage();
						mailAttachment[0] = new MailAttachment(null,"image/png","logo.png");
					
						if(null != changeLoyalFan && changeLoyalFan.getTicketPurchased()==true){
							try{
								mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
										null, "AODev@rightthisway.com", 
										"Reward The Fan: LOYAL FAN ",
										"mail-loyal-fan.html", mailMap, "text/html", null,mailAttachment,filePath);
							}catch(Exception e) {
								e.printStackTrace();
							}
						}*/
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
               
			customerOrderDetail.setOrderId(customerOrder.getId());
			DAORegistry.getCustomerOrderDetailDAO().save(customerOrderDetail);
			
			/*if(null != paypalTransactionDetail && null != paypalTransactionDetail.getPaymentId()  
					&& !paypalTransactionDetail.getPaymentId().isEmpty()){
				paypalTransactionDetail.setOrderId(customerOrder.getId());
				DAORegistry.getPaypalTransactionDetailDAO().saveOrUpdate(paypalTransactionDetail);
				
				PaypalTracking paypalTracking = DAORegistry.getPaypalTrackingDAO().getTrackingByCustomerIdByTicketId(customerId, 
						eventId, categoryTicketGroupId, sessionId, applicationPlatForm);
				
				if(null != paypalTracking){
					paypalTracking.setLastUpdated(new Date());
					paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
					paypalTracking.setTransactionId(paypalTransactionDetail.getId());
					paypalTracking.setStatus("COMPLETED");
				}else{
					paypalTracking = new PaypalTracking();
					paypalTracking.setCatTicketGroupId(categoryTicketGroupId);
					paypalTracking.setCreatedDate(new Date());
					paypalTracking.setCustomerId(customerId);
					paypalTracking.setEventId(eventId);
					paypalTracking.setLastUpdated(new Date());
					paypalTracking.setOrderType(orderType);
					paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
					paypalTracking.setPlatform(applicationPlatForm);
					paypalTracking.setQty(categoryTicketGroup.getQuantity());
					paypalTracking.setStatus("COMPLETED");
					paypalTracking.setTransactionId(paypalTransactionDetail.getId());
					paypalTracking.setOrderTotal(serverOrderTotal);
					paypalTracking.setZone(categoryTicketGroup.getSection());
				}
				
				DAORegistry.getPaypalTrackingDAO().saveOrUpdate(paypalTracking);
			}*/
			
			/*categoryTicketGroup.setInvoiceId(invoice.getId());
			categoryTicketGroup.setStatus(TicketStatus.SOLD);
			categoryTicketGroup.setSoldPrice(soldPrice);
			categoryTicketGroup.setSoldQuantity(categoryTicketGroup.getQuantity());
			categoryTicketGroup.setLastUpdatedDate(new Date());*/
			
			DAORegistry.getCategoryTicketGroupDAO().updateCategoryTicketGroupAsLocked(categoryTicketGroupId, -1, 
					soldPrice, categoryTicketGroup.getQuantity());
			
			DAORegistry.getCustomerOrderRequestDAO().saveOrUpdate(orderRequest);
			
			try{
				MapUtil.copySVGMapandText(event.getVenueId(), event.getVenueCategoryName(),customerOrder.getId());
			}catch(Exception e){
				e.printStackTrace();
			}
               
			orderConfirmation.setOrderId(customerOrder.getId());
			orderConfirmation.setOrder(customerOrder);
			orderConfirmation.setMessage("Your Order was successfully created!");
			orderConfirmation.setStatus(1);	
			TrackingUtils.orderTracking(request, customerOrder, sessionId, referralCode,isFanPrice);
		}catch (Exception e) {
			//later on add money refund procedure here e.g(credit,debit,paypal and loyalty)
			e.printStackTrace();
			error.setDescription("Oops something went wrong while creating order !");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Oops something went wrong while creating order !");
			return orderConfirmation;
		}		
		return orderConfirmation;
	}
	
	
	
	@RequestMapping(value = "/ConfirmTicTrackerOrder",method = RequestMethod.POST)
	public @ResponsePayload OrderConfirmation confirmTickTrackerOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderConfirmation orderConfirmation = new OrderConfirmation();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
				return orderConfirmation;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String appPlatformStr = request.getParameter("platForm");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String orderIdStr = request.getParameter("orderId");
			String isPaymentSuccessStr = request.getParameter("isPaymentSuccess");
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String primaryTransactionId = request.getParameter("primaryTransactionId");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String secondaryTransactionId = request.getParameter("secondaryTransactionId");
			String thirdPaymentMethodStr = request.getParameter("thirdPaymentMethod");
			String thirdTransactionId = request.getParameter("thirdTransactionId");
			String walletTransactionIdStr = request.getParameter("walletTransactionId");
			
			String ipAddress = request.getParameter("clientIPAddress");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Product Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Application Platform is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
				return orderConfirmation;
			}
			
			Integer orderId = null;
			CustomerOrder customerOrder = null;
			CustomerOrderRequest orderRequest = null;
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
				customerOrder = DAORegistry.getCustomerOrderDAO().getPaymentPendingOrderById(orderId);
				if(null == customerOrder ){
					error.setDescription("Order No is not valid");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
					return orderConfirmation;
				}
				orderRequest = DAORegistry.getCustomerOrderRequestDAO().getOrderRequestByOrderId(orderId);
				if(null == orderRequest ){
					error.setDescription("Oops Something Went Wrong. Please Contact Website Administrator.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Oops Something Went Wrong. Please Contact Website Administrator.");
					return orderConfirmation;
				}
				
			}catch(Exception e){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
				return orderConfirmation;
			}

			if(TextUtil.isEmptyOrNull(isPaymentSuccessStr)){
				error.setDescription("Payment Success Flag is invalid.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Payment Success Flag is invalid.");
				return orderConfirmation;
			}
			
			Boolean isPaymentSuccess = Boolean.valueOf(isPaymentSuccessStr);
			
			//paymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,PARTIAL_REWARDS,FULL_REWARDS
			if(TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				error.setDescription("Primary Payment method is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Payment method is mandatory");
				return orderConfirmation;
			}
			
			PaymentMethod primaryPaymentMethod=null;
			if(!TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				try{
					primaryPaymentMethod = PaymentMethod.valueOf(primaryPaymentMethodStr);
				}catch(Exception e){
					error.setDescription("Please send valid Primary Payment Method");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid Primary Payment Method");
					return orderConfirmation;
				}
			}
			
			PartialPaymentMethod secondaryPaymentMethod = null;
			PartialPaymentMethod thirdPaymentMethod=null;
			PaypalTransactionDetail paypalTransactionDetail = null;
			
			CustomerWalletHistory customerWalletHistory = null;
			CustomerWallet customerWallet = null;

			
			if(!isPaymentSuccess){
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getOrderLoyaltyInfoId());
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderLoyaltyHistoryId());
				
				if(null != orderRequest.getAffCashRewardId()){
					
					AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().get(orderRequest.getAffCashRewardId());
					AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().get(orderRequest.getAffCashRewardHistoryId());
					
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().get(orderRequest.getAffPromoTrackingId());
					
					affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash() - affiliateCashRewardHistory.getCreditedCash());
					affiliateCashReward.setLastUpdate(new Date());
					
					DAORegistry.getAffiliateCashRewardDAO().update(affiliateCashReward);
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().delete(affiliatePromoCodeTracking);
					DAORegistry.getAffiliateCashRewardHistoryDAO().delete(affiliateCashRewardHistory);
				}

				if(null != orderRequest.getRtfPromoTrackingId()){
					RTFPromotionalOfferTracking offerTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().get(orderRequest.getRtfPromoTrackingId());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().delete(offerTracking);
				}
				
				if(null != orderRequest.getRefCustLoyaltyHistoryId()){
					
					CustomerLoyalty refLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getRefCustLoyaltyInfoId());
					CustomerLoyaltyHistory refLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getRefCustLoyaltyHistoryId());
					ReferredCodeTracking referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().get(orderRequest.getRefTrackingId());
					
					refLoyalty.setPendingPointsAsDouble(refLoyalty.getPendingPointsAsDouble() - refLoyaltyHistory.getPointsEarnedAsDouble());
					refLoyalty.setLastUpdate(new Date());
					
					DAORegistry.getCustomerLoyaltyDAO().update(refLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().delete(refLoyaltyHistory);
					DAORegistry.getReferredCodeTrackingDAO().delete(referredCodeTracking);
				}
				
				try{
					DAORegistry.getCategoryTicketGroupDAO().revertSoldTicketToActive(customerOrder.getCategoryTicketGroupId());
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(null != orderRequest.getLockId() ){
					//remove the locked ticket information after it has been purchased by customer
					DAORegistry.getAutoCatsLockedTicketDAO().deleteByLockId(orderRequest.getLockId());
				}
				
				customerLoyalty.setPendingPointsAsDouble(customerLoyalty.getPendingPointsAsDouble() - customerLoyaltyHistory.getPointsEarnedAsDouble());
				if(null != customerOrder.getPrimaryPaymentMethod() && 
						(customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS) 
								|| customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
					customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()+ customerLoyaltyHistory.getPointsSpentAsDouble());
					customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble() - customerLoyaltyHistory.getPointsSpentAsDouble());
				}
				customerLoyalty.setLastUpdate(new Date());
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				DAORegistry.getCustomerLoyaltyHistoryDAO().delete(customerLoyaltyHistory);
				
				if(null != orderRequest.getOrderOfferRewardHistoryId()){
					
					CustomerLoyaltyHistory history = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderOfferRewardHistoryId());
					
					customerLoyalty.setPendingPointsAsDouble(customerLoyalty.getPendingPointsAsDouble() - history.getPointsEarnedAsDouble());
					customerLoyalty.setLastUpdate(new Date());
					
					DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().delete(history);
				}
	
				orderRequest.setStatus(OrderStatus.PAYMENT_FAILED);
				orderRequest.setUpdatedTime(new Date());
				
				customerWalletHistory = DAORegistry.getCustomerWalletHistoryDAO().getPendingDebitTransactionByOrderId(customerOrder.getId());
				
				if(null != customerWalletHistory){
					customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(orderRequest.getCustomerId()); 
					customerWallet.setActiveCredit(customerWallet.getActiveCredit()+customerWalletHistory.getTransactionAmount());
					customerWallet.setTotalUsedCredit(customerWallet.getTotalUsedCredit()-customerWalletHistory.getTransactionAmount());
					customerWallet.setLastUpdated(new Date());
					DAORegistry.getCustomerWalletDAO().update(customerWallet);
					DAORegistry.getCustomerWalletHistoryDAO().delete(customerWalletHistory);
				}
				
				DAORegistry.getCustomerOrderDAO().delete(customerOrder);
				DAORegistry.getCustomerOrderRequestDAO().update(orderRequest);
				
				orderConfirmation.setMessage("Order not confirmed due to invalid transaction.");
				orderConfirmation.setStatus(1);	
				return orderConfirmation;
			}else{
				
				List<OrderPaymentBreakup> orderPaymentBreakupList = new ArrayList<OrderPaymentBreakup>();
				OrderPaymentBreakup paymentBreakUp = null;
				
				if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS) || primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
					primaryTransactionId = customerOrder.getPrimaryTransactionId();
				}
				
				/*Ulaganathan : Order Payment breakups - Begins*/
				paymentBreakUp = new OrderPaymentBreakup();
				paymentBreakUp.setOrderId(orderId);
				paymentBreakUp.setPaymentAmount(customerOrder.getPrimaryPayAmtAsDouble());
				paymentBreakUp.setPaymentMethod(primaryPaymentMethod);
				paymentBreakUp.setTransactionId(primaryTransactionId);
				paymentBreakUp.setDoneBy("Customer");
				paymentBreakUp.setPaymentDate(new Date());
				paymentBreakUp.setOrderSequence(1);
				orderPaymentBreakupList.add(paymentBreakUp);
				/*Ulaganathan : Order Payment breakups - Ends*/
				
				
				if(primaryPaymentMethod.equals(PaymentMethod.CREDITCARD) ||
						primaryPaymentMethod.equals(PaymentMethod.GOOGLEPAY) ||
						primaryPaymentMethod.equals(PaymentMethod.IPAY) || 
						primaryPaymentMethod.equals(PaymentMethod.CREDITCARD) ){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.ACCOUNT_RECIVABLE)){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					Property property = DAORegistry.getPropertyDAO().get("rtf.acct.receivable.auto.payid");
					Long primaryTrxId =  Long.valueOf(property.getValue());
					primaryTrxId++;
					primaryTransactionId = PaginationUtil.acctReceivablePaymentPrefix+primaryTrxId;
					property.setValue(String.valueOf(primaryTrxId));
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.PAYPAL)){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
					paypalTransactionDetail = new PaypalTransactionDetail();
					paypalTransactionDetail.setPaymentId(primaryTransactionId);
					try{
						paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.CUSTOMER_WALLET)){

					if(TextUtil.isEmptyOrNull(walletTransactionIdStr)){
						error.setDescription("Wallet Transaction Id is mandatory.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Wallet Transaction Id is mandatory.");
						return orderConfirmation;
					}

					customerWalletHistory = DAORegistry.getCustomerWalletHistoryDAO().getPendingDebitTransactionById(Integer.parseInt(walletTransactionIdStr.trim()));
					customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(orderRequest.getCustomerId());

					if(null == customerWalletHistory){
						error.setDescription("Your wallet transaction is not valid.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Your wallet transaction is not valid.");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(customerWalletHistory.getTransactionId());
					orderRequest.setPrimaryTransactionId( customerWalletHistory.getTransactionId());
					
					orderPaymentBreakupList.get(0).setTransactionId(customerWalletHistory.getTransactionId());
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					if(null != secondaryPaymentMethod && secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(secondaryTransactionId);
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
					}
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
					//Nothing to be written over here. Transaction ID Will be generated While Creating Dummy Order
					
					 
				}else{
					
					
					//Secondary PaymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY
					if(TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						error.setDescription("Please enter a valid Secondary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please enter a valid Secondary Payment Method");
						return orderConfirmation;
					}
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					
					if(secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD) ||
							secondaryPaymentMethod.equals(PartialPaymentMethod.GOOGLEPAY) ||
							secondaryPaymentMethod.equals(PartialPaymentMethod.IPAY) || 
							secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD) ){
						
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(secondaryTransactionId);
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
						
					}else if(secondaryPaymentMethod.equals(PartialPaymentMethod.PAYPAL)){
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						
						paypalTransactionDetail = new PaypalTransactionDetail();
						paypalTransactionDetail.setPaymentId(secondaryTransactionId);
						try{
							paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
						}catch(Exception e){
							e.printStackTrace();
						}
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(secondaryTransactionId);
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
						
					}else if (secondaryPaymentMethod.equals(PartialPaymentMethod.CUSTOMER_WALLET)){
						
						if(TextUtil.isEmptyOrNull(walletTransactionIdStr)){
							error.setDescription("Wallet Transaction Id is mandatory.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Wallet Transaction Id is mandatory.");
							return orderConfirmation;
						}

						customerWalletHistory = DAORegistry.getCustomerWalletHistoryDAO().getPendingDebitTransactionById(Integer.parseInt(walletTransactionIdStr.trim()));
						customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(orderRequest.getCustomerId());

						if(null == customerWalletHistory){
							error.setDescription("Your wallet transaction is not valid.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Your wallet transaction is not valid.");
							return orderConfirmation;
						}
						//customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						//orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						customerOrder.setSecondaryTransactionId(customerWalletHistory.getTransactionId());
						orderRequest.setSecondaryTransactionId( customerWalletHistory.getTransactionId());
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(customerWalletHistory.getTransactionId());
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
						
						if(!TextUtil.isEmptyOrNull(thirdPaymentMethodStr)){
							try{
								thirdPaymentMethod = PartialPaymentMethod.valueOf(thirdPaymentMethodStr);
							}catch(Exception e){
								error.setDescription("Please enter a valid Third Payment Method");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Third Payment Method");
								return orderConfirmation;
							}
						}
						
						if(null != thirdPaymentMethod && thirdPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
							customerOrder.setThirdTransactionId(thirdTransactionId);
							orderRequest.setThirdTransactionId(thirdTransactionId);
							
							/*Ulaganathan : Order Payment breakups - Begins*/
							paymentBreakUp = new OrderPaymentBreakup();
							paymentBreakUp.setOrderId(orderId);
							paymentBreakUp.setPaymentAmount(customerOrder.getThirdPayAmtAsDouble());
							paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(thirdPaymentMethodStr));
							paymentBreakUp.setTransactionId(thirdTransactionId);
							paymentBreakUp.setDoneBy("Customer");
							paymentBreakUp.setPaymentDate(new Date());
							paymentBreakUp.setOrderSequence(3);
							orderPaymentBreakupList.add(paymentBreakUp);
							/*Ulaganathan : Order Payment breakups - Ends*/
						}
							
					}
				}
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getOrderLoyaltyInfoId());
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderLoyaltyHistoryId());
				boolean isPromoApplied = false;
				boolean showDiscountPriceArea = false;
				String promoCode = "";
				if(null != orderRequest.getAffCashRewardId()){
					AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().get(orderRequest.getAffCashRewardId());
					AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().get(orderRequest.getAffCashRewardHistoryId());
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().get(orderRequest.getAffPromoTrackingId());
					
					affiliateCashRewardHistory.setRewardStatus(RewardStatus.PENDING);
					affiliateCashRewardHistory.setUpdatedDate(new Date());
					
					affiliatePromoCodeTracking.setStatus("COMPLETED");
					affiliatePromoCodeTracking.setUpdatedDate(new Date());
					
					showDiscountPriceArea = false;
					isPromoApplied = true;
					promoCode = affiliatePromoCodeTracking.getPromoCode();
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().update(affiliatePromoCodeTracking);
					DAORegistry.getAffiliateCashRewardHistoryDAO().update(affiliateCashRewardHistory);
				}
				
				if(null != orderRequest.getRtfPromoTrackingId()){
					RTFPromotionalOfferTracking offerTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().get(orderRequest.getRtfPromoTrackingId());
					offerTracking.setStatus("COMPLETED");
					offerTracking.setUpdatedDate(new Date());
					if(null != offerTracking.getOfferType() && offerTracking.getOfferType().equals(PromotionalType.CUSTOMER_PROMO)){
						DAORegistry.getRtfCustomerPromotionalOfferDAO().updatePromotionaOfferOrderCount(offerTracking.getPromotionalOfferId());
					}else if(offerTracking.getOfferType().equals(PromotionalType.NORMAL_PROMO)){
						DAORegistry.getRtfPromotionalOfferHdrDAO().updatePromotionaOfferOrderCount(offerTracking.getPromotionalOfferId());
					}
					
					showDiscountPriceArea = true;
					isPromoApplied = true;
					promoCode = offerTracking.getPromoCode();
					
					DAORegistry.getRtfPromotionalOfferTrackingDAO().update(offerTracking);
				}
				
				if(null != orderRequest.getRefCustLoyaltyHistoryId()){
					
					CustomerLoyalty refLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getRefCustLoyaltyInfoId());
					CustomerLoyaltyHistory refLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getRefCustLoyaltyHistoryId());
					ReferredCodeTracking referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().get(orderRequest.getRefTrackingId());
					
					refLoyaltyHistory.setUpdatedDate(new Date());
					refLoyaltyHistory.setRewardStatus(RewardStatus.PENDING);
					DAORegistry.getCustomerLoyaltyHistoryDAO().update(refLoyaltyHistory);
				}
				
				if(null != orderRequest.getOrderOfferRewardHistoryId()){
					CustomerLoyaltyHistory history = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderOfferRewardHistoryId());
					history.setUpdatedDate(new Date());
					history.setRewardStatus(RewardStatus.PENDING);
					DAORegistry.getCustomerLoyaltyHistoryDAO().update(history);
				}
				
				Invoice invoice = null;
				if(customerOrder != null){
					invoice = new Invoice();
					invoice.setCustomerOrderId(customerOrder.getId());
					invoice.setInvoiceTotal(customerOrder.getOrderTotalAsDouble());
					invoice.setTicketCount(customerOrder.getQty());
					invoice.setCustomerId(orderRequest.getCustomerId());
					invoice.setExtPONo(null);
					invoice.setStatus(InvoiceStatus.Outstanding);
					invoice.setInvoiceType("INVOICE");
					invoice.setCreatedDate(new Date());
					invoice.setCreatedBy(customerOrder.getCustomer().getCustomerName());
					invoice.setIsSent(false);
					invoice.setSentDate(null);
					invoice.setRealTixMap("NO");
					invoice.setBrokerId(customerOrder.getBrokerId());
					//Persisting the customer order details in invoice
					DAORegistry.getInvoiceDAO().save(invoice);
				}
				
				orderRequest.setInvoiceId(invoice.getId());
				
				try{
					DAORegistry.getCategoryTicketDAO().updateCategoryTicket(customerOrder.getCategoryTicketGroupId(), 
							invoice.getId(), orderRequest.getSoldPrice());
					DAORegistry.getCategoryTicketGroupDAO().updateTicketAsSold(customerOrder.getCategoryTicketGroupId(), invoice.getId() );
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(null != orderRequest.getLockId() ){
					//remove the locked ticket information after it has been purchased by customer
					DAORegistry.getAutoCatsLockedTicketDAO().deleteByLockId(orderRequest.getLockId());
				}
				
				
				if(null != paypalTransactionDetail && null != paypalTransactionDetail.getPaymentId()  
						&& !paypalTransactionDetail.getPaymentId().isEmpty()){
					paypalTransactionDetail.setOrderId(customerOrder.getId());
					DAORegistry.getPaypalTransactionDetailDAO().saveOrUpdate(paypalTransactionDetail);
					
					PaypalTracking paypalTracking = DAORegistry.getPaypalTrackingDAO().getTrackingByCustomerIdByTicketId(orderRequest.getCustomerId(), 
							customerOrder.getEventId(), customerOrder.getCategoryTicketGroupId(), sessionId, applicationPlatForm);
					
					if(null != paypalTracking){
						paypalTracking.setLastUpdated(new Date());
						paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
						paypalTracking.setTransactionId(paypalTransactionDetail.getId());
						paypalTracking.setStatus("COMPLETED");
					}else{
						paypalTracking = new PaypalTracking();
						paypalTracking.setCatTicketGroupId(customerOrder.getCategoryTicketGroupId());
						paypalTracking.setCreatedDate(new Date());
						paypalTracking.setCustomerId(orderRequest.getCustomerId());
						paypalTracking.setEventId(customerOrder.getEventId());
						paypalTracking.setLastUpdated(new Date());
						paypalTracking.setOrderType(customerOrder.getOrderType());
						paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
						paypalTracking.setPlatform(applicationPlatForm);
						paypalTracking.setQty(customerOrder.getQty());
						paypalTracking.setStatus("COMPLETED");
						paypalTracking.setTransactionId(paypalTransactionDetail.getId());
						paypalTracking.setOrderTotal(customerOrder.getOrderTotalAsDouble());
						paypalTracking.setZone(customerOrder.getSection());
					}
					DAORegistry.getPaypalTrackingDAO().saveOrUpdate(paypalTracking);
				}
				
				customerLoyaltyHistory.setRewardStatus(RewardStatus.PENDING);
				customerLoyaltyHistory.setUpdatedDate(new Date());
				DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
				
				if(null !=customerLoyaltyHistory.getPointsSpentAsDouble() && 
						customerLoyaltyHistory.getPointsSpentAsDouble() > 0){
					customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble() + customerLoyaltyHistory.getPointsSpentAsDouble());
					customerLoyalty.setLatestSpentPointsAsDouble(customerLoyaltyHistory.getPointsSpentAsDouble());
					DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				}
				
				
				if(customerWallet != null){
					customerWallet.setPendingDebit(0.00);
					customerWallet.setLastUpdated(new Date());
					customerWalletHistory.setOrderId(customerOrder.getId());
					customerWalletHistory.setTransactionStatus("APPROVED");
					customerWalletHistory.setUpdatedOn(new Date());
					DAORegistry.getCustomerWalletDAO().saveOrUpdate(customerWallet);
					DAORegistry.getCustomerWalletHistoryDAO().saveOrUpdate(customerWalletHistory);
				}
				
				orderRequest.setStatus(OrderStatus.ACTIVE);
				orderRequest.setUpdatedTime(new Date());
				
				customerOrder.setTicketOnDayBeforeTheEvent(orderRequest.getTicketOnDayBeforeTheEvent());
				customerOrder.setIsInvoiceSent(false);
				customerOrder.setStatus(OrderStatus.ACTIVE);
				customerOrder.setLastUpdatedDateTemp(new Date());
				
				customerOrder.setIsPromoCodeApplied(isPromoApplied);
				customerOrder.setShowDiscPriceArea(showDiscountPriceArea);
				customerOrder.setPromotionalCode(promoCode);
				customerOrder.setNormalTixPrice(orderRequest.getTicketPrice());
				customerOrder.setDiscountedTixPrice(orderRequest.getSoldPrice());
				
				DAORegistry.getCustomerOrderDAO().update(customerOrder);
				DAORegistry.getCustomerOrderRequestDAO().update(orderRequest);
				
				//Save Order payment Breakups - Done By Ulaganathan
				DAORegistry.getOrderPaymentBreakupDAO().saveAll(orderPaymentBreakupList);
				
				try{
					if(null == customerOrder.getCustomer().getEligibleToShareRefCode() || !customerOrder.getCustomer().getEligibleToShareRefCode()){
						DAORegistry.getCustomerDAO().updateCustomerForTicketPurchase(orderRequest.getCustomerId());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				if(orderRequest.getIsLoyalFan()){
					CustomerSuperFan loyalFan =  DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(orderRequest.getCustomerId());
					
					try{
						if(null != loyalFan && (null == loyalFan.getIsEmailSent() || !loyalFan.getIsEmailSent())){
							Date startdate = loyalFan.getStartDate();
							Calendar calendar = new GregorianCalendar();
							calendar.setTime(startdate);
							calendar.add(Calendar.YEAR,1);
							startdate = calendar.getTime();
							String formattedendDate = dateTimeFormat.format(startdate);
							
							Customer customer = customerOrder.getCustomer();
							Map<String,Object> mailMap = new HashMap<String,Object>();
							mailMap.put("customer", customerOrder.getCustomer());  
							
							String message = TextUtil.getSportsLoyalFanEmailBody(loyalFan.getLoyalFanName());
							
							if(!loyalFan.getLoyalFanType().equals(LoyalFanType.SPORTS)){
								message = TextUtil.getNonSportsLoyalFanEmailBody(loyalFan.getLoyalFanName(), String.valueOf(loyalFan.getLoyalFanType()),loyalFan.getCountry());
							}
							mailMap.put("message", message); 
							mailMap.put("startdate", formattedendDate);
							
							MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
					        mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(),  null,URLUtil.bccEmails, 
					          "Reward The Fan: LOYAL FAN ",
					          "mail-loyal-fan.html", mailMap, "text/html", null,mailAttachment,null);
					        loyalFan.setIsEmailSent(true);
					        
					        DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(loyalFan);
						}
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}
				
			}
			orderConfirmation.setOrderId(customerOrder.getId());
			orderConfirmation.setMessage("Your Order was successfully created!");
			orderConfirmation.setStatus(1);	
			return orderConfirmation;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Opps somthing went wrong. Order was not Confirmend. Please Contact Administrator");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Opps somthing went wrong. Order was not Confirmend. Please Contact Administrator");
			return orderConfirmation;
		}
			
	}
	
	
	
	@RequestMapping(value = "/InitiateTicTrackerLongOrder",method = RequestMethod.POST)
	public @ResponsePayload OrderConfirmation tickTrackerLongTixCreateOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderConfirmation orderConfirmation = new OrderConfirmation();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}

			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String eventIdStr = request.getParameter("eventId");
			String categoryTicketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String realTicketGroupIdStr = request.getParameter("realTicketGroupId");
			String orderTotalStr = request.getParameter("orderTotal");
			String isFanPriceStr = request.getParameter("isFanPrice");
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String primaryTransactionId = request.getParameter("primaryTransactionId");			
			String primaryPaymentAmountStrs = request.getParameter("primaryPaymentAmount");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String secondaryTransactionId = request.getParameter("secondaryTransactionId");
			String secondaryPaymentAmountStrs = request.getParameter("secondaryPaymentAmount");
			String thirdPaymentMethodStr = request.getParameter("thirdPaymentMethod");
			String serviceFeesStr = request.getParameter("serviceFees");
			
			String billingAddressIdStr = request.getParameter("billingAddressId");
			String shippingAddressIdStr = request.getParameter("shippingAddressId");
			String shippingSameAsBilling = request.getParameter("shippingSameAsBilling");
			String loyaltySpentStr = request.getParameter("rewardPoints");
			String appPlatformStr = request.getParameter("platForm");
			String referralCode = request.getParameter("referralCode");
			String refCodeIdentity = request.getParameter("eventRefIdentity"); 
			String refCId = request.getParameter("cId"); 
			String bFirstName = request.getParameter("bFirstName");
			String bLastName = request.getParameter("bLastName");
			String bAddress1 = request.getParameter("bAddress1");
			String bAddress2 = request.getParameter("bAddress2");
			String bCity = request.getParameter("bCity");
			String bState = request.getParameter("bState");
			String bCountry = request.getParameter("bCountry");
			String bZipCode = request.getParameter("bZipCode");
			String bPhoneNumber = request.getParameter("bPhoneNumber");
			String bEmail = request.getParameter("bEmail");
			String sFirstName = request.getParameter("sFirstName");
			String sLastName = request.getParameter("sLastName");
			String sAddress1 = request.getParameter("sAddress1");
			String sAddress2 = request.getParameter("sAddress2");
			String sCity = request.getParameter("sCity");
			String sState = request.getParameter("sState");
			String sCountry = request.getParameter("sCountry");
			String sZipCode = request.getParameter("sZipCode");
			String sPhoneNumber = request.getParameter("sPhoneNumber");
			String sEmail = request.getParameter("sEmail");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Product Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Application Platform is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Please send valid Event Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Event Id");
				return orderConfirmation;
			}
			if(TextUtil.isEmptyOrNull(categoryTicketGroupIdStr)){
				error.setDescription("Category Ticket group is mandatory..");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Category Ticket group is mandatory");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(realTicketGroupIdStr)){
				error.setDescription("Real Ticket group is mandatory..");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Real Ticket group is mandatory");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(isFanPriceStr)){
				isFanPriceStr = "false";
			}
			
			Boolean isLongSale = true;
			Boolean isFanPrice = null;
			try{
				isFanPrice = Boolean.valueOf(isFanPriceStr);
			}catch(Exception e){
				error.setDescription("Fan Price Flag should be true or false");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Fan Price Flag should be true or false");
				return orderConfirmation;
			}
			
			Integer customerId = null;
			
			try{
				customerId = Integer.parseInt(customerIdStr);
				
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			Integer eventId = null;
			try{
				eventId = Integer.parseInt(eventIdStr);
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			Integer realTicketGroupId = Integer.parseInt(realTicketGroupIdStr.trim());
			Integer categoryTicketGroupId = null;
			try{
				categoryTicketGroupId = Integer.parseInt(categoryTicketGroupIdStr);
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(sessionId)){
				error.setDescription("Session Id is invalid.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Session Id is invalid.");
				return orderConfirmation;
			}
			
			CustomerOrderRequest orderRequest = new CustomerOrderRequest();
			Customer customer = CustomerUtil.getCustomerById(customerId);//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
			Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, productType);
			if(event==null){
				event = DAORegistry.getEventDAO().getEventById(eventId, productType);
			}
			
			CategoryTicketGroup categoryTicketGroup = null;
			categoryTicketGroup =  DAORegistry.getCategoryTicketGroupDAO().getBroadCastedCategoryTicketsbyId(categoryTicketGroupId);
			
			if(customer == null){
				error.setDescription("Please choose valid customer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please choose valid customer");
				return orderConfirmation;
			}
			
			if(event == null){
				error.setDescription("Event not recognized");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Event not recognized");
				return orderConfirmation;
			}
			
			if(categoryTicketGroup == null){
				error.setButtonValue("Find Other Tickets to this Event");
				error.setOverRideButton(true);
				error.setDescription("The ticket or rate you chose is no longer available. " +
				"If you've just bought a similar ticket, its possible you bought the last ticket available");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Ticket that you are looking for is already been sold out...");
				return orderConfirmation;
			}
			
			
			//paymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,PARTIAL_REWARDS,FULL_REWARDS
			if(TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				error.setDescription("Primary Payment method is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary Payment method is mandatory");
				return orderConfirmation;
			}
			
			PaymentMethod primaryPaymentMethod=null;
			if(!TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				try{
					primaryPaymentMethod = PaymentMethod.valueOf(primaryPaymentMethodStr);
				}catch(Exception e){
					error.setDescription("Please send valid Primary Payment Method");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Primary Payment Method");
					return orderConfirmation;
				}
			}
			
			Integer shippingAddressId=null,billingAddressId=null;
			
			Double orderTotal = null;
			try{
				orderTotal = Double.valueOf(orderTotalStr.trim());
				orderTotal = Double.valueOf(df.format(orderTotal));
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Order Total is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Order Total is Mandatory");
				return orderConfirmation;
			}
			
			CustomerSuperFan customerSuperFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
			
			Double originalOrderTotal = null,serviceFees = 0.00,serverOrderTotal=0.00,originalTicketPrice = categoryTicketGroup.getOriginalPrice();
			
			serverOrderTotal = orderTotal;
			
			serviceFees = TicketUtil.getBrokerServiceFees(categoryTicketGroup.getBrokerId(), categoryTicketGroup.getOriginalPrice(), categoryTicketGroup.getQuantity(), categoryTicketGroup.getOriginalTaxPerc());
			originalOrderTotal =  ((categoryTicketGroup.getOriginalPrice() * categoryTicketGroup.getQuantity()) + serviceFees);
			originalOrderTotal =  TicketUtil.getRoundedValue(originalOrderTotal);
			
			//Setting up Ticket Original Price
			orderRequest.setTicketPrice(originalTicketPrice);
			
			RTFPromotionalOfferTracking promoCodeTracking = null;
			boolean rtfPromotionalCode = false,isCustReferalDiscountCode = false,isAffilateReferral = false;
			if(null != referralCode && !referralCode.isEmpty()){
				if(RTFPromotionalOfferUtil.validateRTFPromoCode(referralCode.trim())){
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
							sessionId, realTicketGroupId, eventId);
					rtfPromotionalCode = true;
				}else {
					
					if(RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim())){
						promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
								sessionId, categoryTicketGroupId, eventId);
						if(null != promoCodeTracking && promoCodeTracking.getOfferType().equals(PromotionalType.AFFILIATE_PROMO_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							rtfPromotionalCode = true;
							isAffilateReferral = true;
						}else{
							promoCodeTracking = null;
						}
					}else{
						promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
								sessionId, categoryTicketGroupId, eventId);
						
						if(promoCodeTracking.getOfferType().equals(PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							rtfPromotionalCode = true;
							isCustReferalDiscountCode = true;
						}else{
							promoCodeTracking = null;
						}
					}
					
				}
			}
			
			Double discountAmount = 0.00;
			OrderType orderType = OrderType.REGULAR;
			Double soldPrice = categoryTicketGroup.getSoldPrice();
			Double primaryPayAmt = 0.00,secondaryPayAmt=0.00,thirdPayAmount=0.00;
			CustomerSuperFan changeLoyalFan = null;
			
			if(isFanPrice){
				if(null != customerSuperFan){
					
					Boolean isLoyalFanEvent = FavoriteUtil.markArtistAsLoyalFan(customerSuperFan, event);
					
					if(!isLoyalFanEvent){
						if(!customerSuperFan.getTicketPurchased()){
							orderType = OrderType.LOYALFAN;
							customerSuperFan.setEndDate(new Date());
							customerSuperFan.setStatus(Status.DELETED);
							changeLoyalFan = new CustomerSuperFan();
							/*changeLoyalFan.setArtistId(event.getArtistId());
							changeLoyalFan.setArtistName(event.getArtistName());
							changeLoyalFan.setCategoryName(event.getParentCategoryName());*/
							changeLoyalFan.setCustomerId(customerId);
							changeLoyalFan.setStartDate(new Date());
							changeLoyalFan.setStatus(Status.ACTIVE);
							changeLoyalFan.setTicketPurchased(true);
						}else{
							error.setDescription("You are currently not eligible to purchase this ticket at a discounted price. You will be redirected back to ticket listings.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							orderConfirmation.setRedirectTicketListing(true);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are currently not eligible to purchase this ticket at a discounted price");
							return orderConfirmation;
						}
					}else {
						customerSuperFan.setTicketPurchased(true);
						orderType = OrderType.LOYALFAN;
					}
				}else{
					orderType = OrderType.LOYALFAN;
					changeLoyalFan = new CustomerSuperFan();
					/*changeLoyalFan.setArtistId(event.getArtistId());
					changeLoyalFan.setArtistName(event.getArtistName());
					changeLoyalFan.setCategoryName(event.getParentCategoryName());*/
					changeLoyalFan.setCustomerId(customerId);
					changeLoyalFan.setStartDate(new Date());
					changeLoyalFan.setStatus(Status.ACTIVE);
					changeLoyalFan.setTicketPurchased(true);
				}
			}
			serverOrderTotal =  TicketUtil.getRoundedValue(serverOrderTotal);
			discountAmount = originalOrderTotal - serverOrderTotal;
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double rewardEarnConv = 0.00;
			
			/*if(RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim())){
				rewardEarnConv = loyaltySettings.getAffiliatePromoRewardConv();
			}else*/ 
			
			if(isCustReferalDiscountCode){
				rewardEarnConv = promoCodeTracking.getCustomerEarnRewardConv();
			}else{
				rewardEarnConv = loyaltySettings.getRewardConversion();
			}
			
			primaryPayAmt = serverOrderTotal;
			
			CustomerLoyaltyHistory customerLoyaltyHistory = new CustomerLoyaltyHistory();
			CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			PaypalTransactionDetail paypalTransactionDetail = null;
			
			Double activeRewardAmount = RewardConversionUtil.getRewardDoller(customerLoyalty.getActivePointsAsDouble(),loyaltySettings.getDollerConversion());
			Double requiredPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getDollerConversion());
			Double orderRewardEarnAmount = RewardConversionUtil.getRewardDoller(requiredPoints, loyaltySettings.getDollerConversion());
			
			Double orderTotalForRewards = serverOrderTotal;
			try{
				if(null != loyaltySpentStr && !loyaltySpentStr.isEmpty()){
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(Double.valueOf(loyaltySpentStr.trim()),loyaltySettings.getDollerConversion());
					orderTotalForRewards = TicketUtil.getRoundedValue(orderTotalForRewards - enteredRewardAmount);
					
					if(orderTotalForRewards < 0){
						orderTotalForRewards = 0.00;
					}
				}				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
			if(isAffilateReferral){
				Affiliate affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(referralCode.trim());
				if(affiliateObj!= null && !affiliateObj.getIsEarnRewardPoints()){
					earningRewardPoints = 0.00;
				}
			}
			if(primaryPaymentMethod.equals(PaymentMethod.ACCOUNT_RECIVABLE)){
				earningRewardPoints = 0.00;
			}
			
			
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setOrderTotal(orderTotal);
			customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
			customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setPointsSpentAsDouble(0.00);
			customerLoyaltyHistory.setRewardSpentAmount(0.00);
			customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
			customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
			customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
			customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
			Double balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - 0;
			Double balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
			customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
			
			CustomerWallet customerWallet = null;
			CustomerWalletHistory customerWalletHistory = null;
			PartialPaymentMethod secondaryPaymentMethod=null;
			PartialPaymentMethod thirdPaymentMethod=null;
			String thirdPaymentTrxId = null;
			AddressInfo addressInfo = new AddressInfo();
			Boolean isSaveCard = false;
			switch (primaryPaymentMethod) {
			
				case FULL_REWARDS:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					UserAddress shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.FULL_REWARDS);
					addressInfo.setShippingAddress(shippingAddress);
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("Please enter active reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter active reward dollars");
						return orderConfirmation;
					}	
					
					Double loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("Reward dollars amount must be an integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars amount must be an integer");
						return orderConfirmation;
					}
					
					
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(requiredPoints < loyaltySpent){
						Double diffOrderReward = loyaltySpent - requiredPoints;
						error.setDescription("Your have entered an amount more than the required reward dollars. Please subtract "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered an amount more than the required reward dollars. Please subtract "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						return orderConfirmation;
					}
					
					if(requiredPoints > loyaltySpent){
						Double diffOrderReward = requiredPoints - loyaltySpent;
						error.setDescription("Your have entered an amount less than the required reward dollars. Please add "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than required reward dollars , reduce "+TicketUtil.getRoundedValueString(diffOrderReward)+" more reward dollars");
						return orderConfirmation;
					}
					
					if((customerLoyalty.getActivePointsAsDouble() < loyaltySpent) ||
							(activeRewardAmount < enteredRewardAmount)){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(activeRewardAmount < enteredRewardAmount){
						error.setDescription("Please enter a valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid reward dollars amount. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
					
					break;
					
				case PARTIAL_REWARDS:
					
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("Reward dollars are Mandatory.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars are Mandatory");
						return orderConfirmation;
					}	
					
					loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("Reward dollars should be Integer.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars should be Integer");
						return orderConfirmation;
					}
					

					//Secondary PaymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY
					if(TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						error.setDescription("Please enter a valid Secondary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
						return orderConfirmation;
					}
					
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(activeRewardAmount < loyaltySpent){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(requiredPoints < loyaltySpent){
						error.setDescription("Your have entered more than the required reward dollars. Please select  Full Rewards as Primary Payment Method."); 
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than the required reward dollars. Please select  Full Rewards as Primary Payment Method.");
						return orderConfirmation;
					}
					
					if(requiredPoints == loyaltySpent){
						error.setDescription("Your have entered the required reward dollars. Please select Full Rewards as Primary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered the required reward dollars. Please select  Full Rewards as Primary Payment Method");
						return orderConfirmation;
					}
					
					Double tempOrderTotal = serverOrderTotal - enteredRewardAmount ;
					primaryPayAmt = enteredRewardAmount;
					
					if(secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						try{
							billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
						}catch(Exception e){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
							shippingAddressId =  billingAddressId;
						}else{
							if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
								error.setDescription("Please enter a valid Shipping Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
								return orderConfirmation;
							}
							
							try{
								shippingAddressId = Integer.parseInt(shippingAddressIdStr);
							}catch(Exception e){
								error.setDescription("Shipping Address should be integer");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
								return orderConfirmation;
							}
						}
						
						UserAddress billingAddress = null;
						if(null != billingAddressId){
							billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
						}
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						if(billingAddress == null){
							error.setDescription("Billing Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
							return orderConfirmation;
						}
						addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(billingAddress);
						
						secondaryTransactionId = "PARTIAL_CREDITCARD_TRANSACTION";
						
					}else if (secondaryPaymentMethod.equals(PartialPaymentMethod.CUSTOMER_WALLET)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						secondaryTransactionId = "PARTIAL_CUSTOMER_WALLET_TRANSACTION";
						
						if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						try{
							billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
						}catch(Exception e){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
							shippingAddressId =  billingAddressId;
						}else{
							if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
								error.setDescription("Please enter a valid Shipping Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
								return orderConfirmation;
							}
							
							try{
								shippingAddressId = Integer.parseInt(shippingAddressIdStr);
							}catch(Exception e){
								error.setDescription("Shipping Address should be integer");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
								return orderConfirmation;
							}
						}
						
						UserAddress billingAddress = null;
						if(null != billingAddressId){
							billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
						}
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						if(billingAddress == null){
							error.setDescription("Billing Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
							return orderConfirmation;
						}
						addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(billingAddress);
						
						if(!TextUtil.isEmptyOrNull(thirdPaymentMethodStr)){
							try{
								thirdPaymentMethod = PartialPaymentMethod.valueOf(thirdPaymentMethodStr);
							}catch(Exception e){
								error.setDescription("Please enter a valid Third Payment Method");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Third Payment Method");
								return orderConfirmation;
							}
						}
						
						if(null != thirdPaymentMethod && thirdPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
							
							String thirdPaymentAmountStrs = request.getParameter("thirdPaymentAmount");
							String thirdTransactionId = request.getParameter("thirdTransactionId");
							
							if(TextUtil.isEmptyOrNull(thirdPaymentAmountStrs)){
								error.setDescription("Third payment amount is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Third payment amount is not valid");
								return orderConfirmation;
							}
							
							try{
								thirdPayAmount = Double.valueOf(thirdPaymentAmountStrs.trim());
							}catch(Exception e){
								error.setDescription("Third payment amount is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Third payment amount is not valid");
								return orderConfirmation;
							}
							
							if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
								error.setDescription("Please enter a valid Billing Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
								return orderConfirmation;
							}
							
							try{
								billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
							}catch(Exception e){
								error.setDescription("Please enter a valid Billing Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
								return orderConfirmation;
							}
							
							if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
								shippingAddressId =  billingAddressId;
							}else{
								if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
									error.setDescription("Please enter a valid Shipping Address");
									orderConfirmation.setError(error);
									orderConfirmation.setStatus(0);
									TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
									return orderConfirmation;
								}
								
								try{
									shippingAddressId = Integer.parseInt(shippingAddressIdStr);
								}catch(Exception e){
									error.setDescription("Shipping Address should be integer");
									orderConfirmation.setError(error);
									orderConfirmation.setStatus(0);
									TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
									return orderConfirmation;
								}
							}
							
							billingAddress = null;
							if(null != billingAddressId){
								billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
							}
							shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
							
							if(shippingAddress == null){
								error.setDescription("Shipping Address is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
								return orderConfirmation;
							}
							
							if(billingAddress == null){
								error.setDescription("Billing Address is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
								return orderConfirmation;
							}
							addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
							addressInfo.setShippingAddress(shippingAddress);
							addressInfo.setBillingAddress(billingAddress);
							
							thirdPaymentTrxId = "PARTIAL_THIRD_CREDITCARD_TRXID";
							
						}
					}
					
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
								
					
					break;
					
				case CREDITCARD:
					
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					UserAddress billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					primaryTransactionId = "CREDITCARD_TRANSACTION";
	
					break;
					
				case PAYPAL:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setPaymentMethod(PaymentMethod.PAYPAL);
					primaryTransactionId = "PAYPAL_TRANSACTION";
					
					break;
					
				case GOOGLEPAY:
					
					break;
					
				case IPAY:
					
					break;
					
				case CUSTOMER_WALLET:
					
					if(TextUtil.isEmptyOrNull(primaryPaymentAmountStrs)){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					try{
						primaryPayAmt = Double.valueOf(primaryPaymentAmountStrs.trim());
					}catch(Exception e){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					primaryTransactionId = "CUSTOMER_WALLET";
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					if(null != secondaryPaymentMethod && secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						secondaryTransactionId = "WALLET_CREDITCARD_TRANSACTION";
					}
					
					break;
					
				case ACCOUNT_RECIVABLE:
					
					if(TextUtil.isEmptyOrNull(primaryPaymentAmountStrs)){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					try{
						primaryPayAmt = Double.valueOf(primaryPaymentAmountStrs.trim());
					}catch(Exception e){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					primaryTransactionId = "ACCOUNT_RECIVABLE";
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					break;

	
				default:
					break;
			}
			
			
			CustomerOrderDetail customerOrderDetail = new CustomerOrderDetail();
			
			if(addressInfo.getPaymentMethod().equals(PaymentMethod.CREDITCARD)){
				
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getBillingAddress().getAddressLine1());
				customerOrderDetail.setBillingAddress2(addressInfo.getBillingAddress().getAddressLine2());
				customerOrderDetail.setBillingCity(addressInfo.getBillingAddress().getCity());
				customerOrderDetail.setBillingCountry(addressInfo.getBillingAddress().getCountry().getCountryName());
				customerOrderDetail.setBillingFirstName(addressInfo.getBillingAddress().getFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getBillingAddress().getLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getBillingAddress().getPhone1());
				customerOrderDetail.setBillingPhone2(addressInfo.getBillingAddress().getPhone2());
				customerOrderDetail.setBillingState(addressInfo.getBillingAddress().getState().getName());
				/*customerOrderDetail.setBillingCountryId(addressInfo.getBillingAddress().getCountry().getId());
				customerOrderDetail.setBillingStateId(addressInfo.getBillingAddress().getState().getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getBillingAddress().getZipCode());
				customerOrderDetail.setBillingEmail(null != addressInfo.getBillingAddress().getEmail()?addressInfo.getBillingAddress().getEmail():customer.getEmail());
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			} else if (addressInfo.getPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else if (addressInfo.getPaymentMethod().equals(PaymentMethod.PAYPAL)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else{
				/*Country billingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getbCountry());
				State billingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(billingCountry.getId(), addressInfo.getbState());*/
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getbAddress1());
				customerOrderDetail.setBillingAddress2(addressInfo.getbAddress2());
				customerOrderDetail.setBillingCity(addressInfo.getbCity());
				customerOrderDetail.setBillingCountry(addressInfo.getbCountry());
				customerOrderDetail.setBillingFirstName(addressInfo.getbFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getbLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getbPhoneNumber());
				customerOrderDetail.setBillingState(addressInfo.getbState());
				/*customerOrderDetail.setBillingCountryId(billingCountry.getId());
				customerOrderDetail.setBillingStateId(billingState.getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getbZipCode());
				customerOrderDetail.setBillingEmail(null!=addressInfo.getbEmail()?addressInfo.getbEmail():customer.getEmail());
				
				/*Country shippingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getsCountry());
				State shippingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(shippingCountry.getId(), addressInfo.getsState());*/
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getsAddress1());
				customerOrderDetail.setShippingAddress2(addressInfo.getsAddress2());
				customerOrderDetail.setShippingCity(addressInfo.getsCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getsFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getsLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getsPhoneNumber());
				customerOrderDetail.setShippingCountry(addressInfo.getsCountry());
				customerOrderDetail.setShippingState(addressInfo.getsState());
				/*customerOrderDetail.setShippingCountryId(shippingCountry.getId());
				customerOrderDetail.setShippingStateId(shippingState.getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getsZipCode());
				customerOrderDetail.setShippingEmail(null!=addressInfo.getsEmail()?addressInfo.getsEmail():customer.getEmail());
				
			}
			
			
			CustomerLoyaltyHistory refererCustLoyaltyHistory = null;
			CustomerLoyalty refererCustomerLoyalty = null;
			ReferredCodeTracking referredCodeTracking = null;
			Boolean validReferralCode = false,affiliatePromoCode = false;
			
			AffiliateCashReward affiliateCashReward = null;
			AffiliateCashRewardHistory affiliateCashRewardHistory = null;
			
			//Customer custReferal = null;
			if(null != referralCode && !referralCode.isEmpty() && (!rtfPromotionalCode || isCustReferalDiscountCode || isAffilateReferral)){
				
				Affiliate affiliate = RTFAffiliateBrokerUtil.getAffiliateObjByPromoCode(referralCode.trim());
				
				if(null != affiliate && !isCustReferalDiscountCode){
					
					affiliatePromoCode =  true;
					
					affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(affiliate.getUserId());
					Double pendingCash = RewardConversionUtil.getAffiliateCashCredit(orderTotalForRewards, affiliate.getPhoneOrderCashReward());
					
					if(null == affiliateCashReward){
						affiliateCashReward= new AffiliateCashReward();
						affiliateCashReward.setUserId(affiliate.getUserId());
						affiliateCashReward.setActiveCash(0.00);
						affiliateCashReward.setPendingCash(pendingCash);
						affiliateCashReward.setLastCreditedCash(0.00);
						affiliateCashReward.setTotalCreditedCash(0.00);
						affiliateCashReward.setLastDebitedCash(0.00);
						affiliateCashReward.setTotalDebitedCash(0.00);
						affiliateCashReward.setLastVoidCash(0.00);
						affiliateCashReward.setTotalVoidedCash(0.00);
					}else{
						affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash()+pendingCash);
					}
					affiliateCashRewardHistory = new AffiliateCashRewardHistory();
					affiliateCashRewardHistory.setPromoCode(referralCode.trim());
					affiliateCashRewardHistory.setCustomerId(customerId);
					affiliateCashRewardHistory.setUserId(affiliate.getUserId());
					affiliateCashRewardHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					affiliateCashRewardHistory.setCreditConv( affiliate.getPhoneOrderCashReward());
					affiliateCashRewardHistory.setCreditedCash(pendingCash);
					affiliateCashRewardHistory.setCreateDate(new Date());
					affiliateCashRewardHistory.setUpdatedDate(new Date());
					affiliateCashRewardHistory.setIsRewardsEmailSent(Boolean.FALSE);
					affiliateCashRewardHistory.setOrderTotal(orderTotalForRewards);
				}else{
					
					Customer custReferrer = DAORegistry.getCustomerDAO().getCustomerByIdByProduct(promoCodeTracking.getReferredBy(), productType);
					if(custReferrer.getReferrerCode() == null){
					}else{
					}
						
					refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferrer.getId());
					
					//Double earningRewardPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, rewardEarnConv);
					
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, promoCodeTracking.getReferrarEarnRewardConv());
					
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferrer.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(promoCodeTracking.getReferrarEarnRewardConv());
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderType(orderType);
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferrer.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferrer.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("ReferralCode");
					
					validReferralCode = true;
					
				}
				
			}else if((null != refCodeIdentity && !refCodeIdentity.isEmpty()) 
					&& (refCId != null && !refCId.isEmpty())){
				
				Customer custReferal = new Customer();
				custReferal = SecurityUtil.validateReferalIdentity(refCId, refCodeIdentity, eventId, customerId, referredCodeTracking,custReferal);
				if(custReferal.getEventShareValidated()){
					try{
						refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferal.getId());	
					}catch(Exception e){
						e.printStackTrace();
					}
					
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
					if(isAffilateReferral){
						Affiliate affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(referralCode.trim());
						if(affiliateObj!= null && !affiliateObj.getIsEarnRewardPoints()){
							pointsEarned = 0.00;
						}
					}
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderId(custReferal.getId());
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferal.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferal.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("EventShareLinkReferral");
					validReferralCode = true;
				}
			}
			
			Property property = null;
			if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
				Long primaryTrxId =  Long.valueOf(property.getValue());
				primaryTrxId++;
				primaryTransactionId = PaginationUtil.fullRewardPaymentPrefix+primaryTrxId;
				property.setValue(String.valueOf(primaryTrxId));
			}else if(primaryPaymentMethod.equals(PaymentMethod.PARTIAL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				Long partialPrimaryTrxId =  Long.valueOf(property.getValue());
				partialPrimaryTrxId++;
				primaryTransactionId = PaginationUtil.partialRewardPaymentPrefix+partialPrimaryTrxId;
				property.setValue(String.valueOf(partialPrimaryTrxId));
			}
			
			CustomerOrder customerOrder = new CustomerOrder();
			Customer customerDetail = new Customer();
			customerDetail.setId(customerId);
			customerOrder.setCategoryTicketGroupId(categoryTicketGroup.getId());
			customerOrder.setOrderType(orderType);
			customerOrder.setCustomer(customerDetail);
			customerOrder.setEventDateTemp(event.getEventDate());
			customerOrder.setEventId(event.getEventId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventTime(null != event.getEventTime()?event.getEventTime():null);
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setVenueName(event.getVenueName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueState(event.getState());
			customerOrder.setVenueCountry(event.getCountry());
			customerOrder.setProductType(productType);
			customerOrder.setShippingMethod(categoryTicketGroup.getShippingMethod());
			customerOrder.setTaxesAsDouble(null != serviceFeesStr && !serviceFeesStr.isEmpty()?Double.valueOf(serviceFeesStr.trim()):serviceFees);
			//customerOrder.setTaxesAsDouble(serviceFees);
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(event.getEventDate());
			calendar.add(Calendar.DAY_OF_MONTH, -5);
			
			Double finalTicketPrice = serverOrderTotal / categoryTicketGroup.getQuantity();
			
			customerOrder.setShippingDateTemp(calendar.getTime());
			customerOrder.setStatus(OrderStatus.PAYMENT_PENDING);
			customerOrder.setQty(categoryTicketGroup.getQuantity());			
			customerOrder.setSection(categoryTicketGroup.getSection());
			customerOrder.setSectionDescription(categoryTicketGroup.getSectionDescription());
			customerOrder.setLastUpdatedDateTemp(new Date());
			customerOrder.setCreateDateTemp(new Date());			
			customerOrder.setTicketPriceAsDouble(finalTicketPrice);
			customerOrder.setTicketSoldPriceAsDouble(soldPrice);
			customerOrder.setOriginalOrderTotal(originalOrderTotal);
			customerOrder.setDiscountAmount(discountAmount);
			customerOrder.setOrderTotalAsDouble(serverOrderTotal);
			customerOrder.setPrimaryPayAmtAsDouble(primaryPayAmt);
			customerOrder.setSecondaryPayAmtAsDouble(secondaryPayAmt);
			customerOrder.setPrimaryPaymentMethod(primaryPaymentMethod);
			customerOrder.setPrimaryTransactionId(primaryTransactionId);
			customerOrder.setSecondaryPaymentMethod(null != secondaryPaymentMethod ? secondaryPaymentMethod:PartialPaymentMethod.NULL);
			customerOrder.setSecondaryTransactionId(null != secondaryTransactionId ? secondaryTransactionId:"");
			customerOrder.setThirdPaymentMethod(null != thirdPaymentMethod ? thirdPaymentMethod:PartialPaymentMethod.NULL);
			customerOrder.setThirdTransactionId(null != thirdPaymentTrxId ? thirdPaymentTrxId:"");
			customerOrder.setThirdPayAmtAsDouble(thirdPayAmount);
			customerOrder.setAppPlatForm(applicationPlatForm);
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setIsInvoiceSent(false);
			customerOrder.setBrokerId(categoryTicketGroup.getBrokerId());
			customerOrder.setBrokerServicePerc(TicketUtil.getBrokerServiceFeePerc(categoryTicketGroup.getBrokerId()));
			
			customerOrder.setIsLongSale(isLongSale);
			if(isLongSale){
				customerOrder.setActualSeat(categoryTicketGroup.getActualSeat());
				customerOrder.setActualSection(categoryTicketGroup.getLongSection());
				customerOrder.setActualRow(categoryTicketGroup.getRow());
			}
			
			
			DAORegistry.getCustomerOrderDAO().saveOrUpdate(customerOrder);
		    
			if(null != property){
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
			}
			
			
			customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()-(null != customerLoyaltyHistory.getPointsSpentAsDouble()?customerLoyaltyHistory.getPointsSpentAsDouble():0));
			customerLoyalty.setPendingPointsAsDouble((null != customerLoyalty.getPendingPointsAsDouble()?customerLoyalty.getPendingPointsAsDouble():0)+customerLoyaltyHistory.getPointsEarnedAsDouble());
			customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
			customerLoyalty.setLastUpdate(new Date());
			customerLoyaltyHistory.setOrderType(orderType);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setUpdatedDate(new Date());
			customerLoyaltyHistory.setOrderTotal(serverOrderTotal);
			customerLoyaltyHistory.setOrderId(customerOrder.getId());
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
			
			DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(customerLoyaltyHistory);
			
			/* Add All Pending Values to Order Request - Begins  */
			orderRequest.setSessionId(sessionId);
			orderRequest.setOrderId(customerOrder.getId());
			orderRequest.setOrderLoyaltyHistoryId(customerLoyaltyHistory.getId());
			orderRequest.setOrderLoyaltyInfoId(customerLoyalty.getId());
			orderRequest.setOrderTotal(serverOrderTotal);
			orderRequest.setOrderType(orderType);
			orderRequest.setOriginalOrderTotal(originalOrderTotal);
			orderRequest.setAppPlatForm(customerOrder.getAppPlatForm());
			orderRequest.setCategoryTicketGroupId(categoryTicketGroupId);
			orderRequest.setCustomerId(customerId);
			orderRequest.setEventId(eventId);
			orderRequest.setInvoiceId(-1);
			orderRequest.setIsLongSale(true);
			orderRequest.setIsLoyalFan(isFanPrice);
			orderRequest.setPrimaryPayAmt(primaryPayAmt);
			orderRequest.setPrimaryPaymentMethod(primaryPaymentMethod);
			orderRequest.setPrimaryTransactionId(primaryTransactionId);
			orderRequest.setSecondaryPayAmt(secondaryPayAmt);
			orderRequest.setSecondaryPaymentMethod(secondaryPaymentMethod);
			orderRequest.setSecondaryTransactionId(secondaryTransactionId);
			orderRequest.setQuantity(customerOrder.getQty());
			orderRequest.setSoldPrice(soldPrice);
			orderRequest.setThirdPayAmt(customerOrder.getThirdPayAmtAsDouble());
			orderRequest.setThirdPaymentMethod(customerOrder.getThirdPaymentMethod());
			orderRequest.setThirdTransactionId(customerOrder.getThirdTransactionId());
			orderRequest.setTicketGroupId(null);
			
			String customerIpAddress = "";
			if(customerOrder.getAppPlatForm().equals(ApplicationPlatForm.IOS) || 
					customerOrder.getAppPlatForm().equals(ApplicationPlatForm.ANDROID)){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			orderRequest.setIpAddress(customerIpAddress);
			orderRequest.setStatus(OrderStatus.PAYMENT_PENDING);
			orderRequest.setCreatedTime(new Date());
			orderRequest.setUpdatedTime(new Date());
			/* Add All Pending Values to Order Request - Ends  */
			
			if(rtfPromotionalCode){
				try{
					promoCodeTracking.setOrderId(customerOrder.getId());
					promoCodeTracking.setStatus("ORDERPENDING");
					promoCodeTracking.setUpdatedDate(new Date());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
					
					//Add RTF Promo Tracking ID Into Order Request
					orderRequest.setRtfPromoTrackingId(promoCodeTracking.getId());
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(affiliatePromoCode){
				affiliateCashReward.setLastUpdate(new Date());
				DAORegistry.getAffiliateCashRewardDAO().saveOrUpdate(affiliateCashReward);
				
				affiliateCashRewardHistory.setOrderId(customerOrder.getId());
				DAORegistry.getAffiliateCashRewardHistoryDAO().saveOrUpdate(affiliateCashRewardHistory);
				
				try{
					
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = null;
					if(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE) || 
							applicationPlatForm.equals(ApplicationPlatForm.TICK_TRACKER)){
						
						affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(applicationPlatForm, 
								affiliateCashReward.getUserId(), customer.getId(), sessionId, 
								realTicketGroupId, eventId, referralCode,true);
						
						if(null != affiliatePromoCodeTracking){
							affiliatePromoCodeTracking.setUpdatedDate(new Date());
							affiliatePromoCodeTracking.setStatus("ORDERPENDING");
							affiliatePromoCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
							affiliatePromoCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
							affiliatePromoCodeTracking.setUpdatedDate(new Date());
							affiliatePromoCodeTracking.setOrderId(customerOrder.getId());
							affiliatePromoCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
							affiliatePromoCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
						}
					}
					
					if(null == affiliatePromoCodeTracking){
						affiliatePromoCodeTracking = new AffiliatePromoCodeTracking();
						affiliatePromoCodeTracking.setIsLongTicket(true);
						affiliatePromoCodeTracking.setTicketGroupId(realTicketGroupId);
						affiliatePromoCodeTracking.setEventId(eventId);
						affiliatePromoCodeTracking.setIpAddress(ip);
						affiliatePromoCodeTracking.setPlatForm(applicationPlatForm);
						affiliatePromoCodeTracking.setSessionId(sessionId);
						affiliatePromoCodeTracking.setStatus("ORDERPENDING");
						affiliatePromoCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
						affiliatePromoCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
						affiliatePromoCodeTracking.setCreatedDate(new Date());
						affiliatePromoCodeTracking.setUpdatedDate(new Date());
						affiliatePromoCodeTracking.setCustomerId(customerId);
						affiliatePromoCodeTracking.setUserId(affiliateCashReward.getUserId());
						affiliatePromoCodeTracking.setOrderId(customerOrder.getId());
						affiliatePromoCodeTracking.setPromoCode(referralCode);
						affiliatePromoCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
						affiliatePromoCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
					}
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().saveOrUpdate(affiliatePromoCodeTracking);
					
					//Add Affiliate Reward ID's to Order Request.
					orderRequest.setAffCashRewardHistoryId(affiliateCashRewardHistory.getId());
					orderRequest.setAffCashRewardId(affiliateCashReward.getId());
					orderRequest.setAffPromoTrackingId(affiliatePromoCodeTracking.getId());
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
			if(validReferralCode || isCustReferalDiscountCode){
				
				refererCustLoyaltyHistory.setOrderId(customerOrder.getId());
				
				if(null != refererCustLoyaltyHistory){ //Safer side to avoid null pointer exception
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(refererCustomerLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(refererCustLoyaltyHistory);
					DAORegistry.getReferredCodeTrackingDAO().saveOrUpdate(referredCodeTracking);
					
					//Add Referral Reward ID's to Order Request.
					orderRequest.setRefCustLoyaltyHistoryId(refererCustLoyaltyHistory.getId());
					orderRequest.setRefCustLoyaltyInfoId(refererCustomerLoyalty.getId());
					orderRequest.setRefTrackingId(referredCodeTracking.getId());
					referralCode = referredCodeTracking.getReferredCode();
				}
			}
			
			
			if(isFanPrice ){
				   try{
					Date startdate = null;
					if(null != customerSuperFan){
						startdate = customerSuperFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(customerSuperFan);
						
						//Add Old Loyal Fan ID to Order Request.
						orderRequest.setOldLoyalFanId(customerSuperFan.getId());
					}
					if(null != changeLoyalFan){
						startdate = changeLoyalFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(changeLoyalFan);
						
						//Add New Loyal Fan ID to Order Request.
						orderRequest.setNewLoyalFanId(changeLoyalFan.getId());
						
						/*calendar = new GregorianCalendar();
						calendar.setTime(startdate);
						calendar.add(Calendar.YEAR,1);
						startdate = calendar.getTime();
						String formattedendDate = dateTimeFormat.format(startdate);
						
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("customer", customer);  
						mailMap.put("changeLoyalFan", changeLoyalFan); 
						mailMap.put("startdate", formattedendDate);
						MailAttachment[] mailAttachment = new MailAttachment[1];
						String filePath = URLUtil.getLogoImage();
						mailAttachment[0] = new MailAttachment(null,"image/png","logo.png");
					
						if(null != changeLoyalFan && changeLoyalFan.getTicketPurchased()==true){
							try{
								mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
										null, "AODev@rightthisway.com", 
										"Reward The Fan: LOYAL FAN ",
										"mail-loyal-fan.html", mailMap, "text/html", null,mailAttachment,filePath);
							}catch(Exception e) {
								e.printStackTrace();
							}
						}*/
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
               
			customerOrderDetail.setOrderId(customerOrder.getId());
			DAORegistry.getCustomerOrderDetailDAO().save(customerOrderDetail);
			
			DAORegistry.getCategoryTicketGroupDAO().updateCategoryTicketGroupAsLocked(categoryTicketGroupId, -1, 
					soldPrice, categoryTicketGroup.getQuantity());
			
			DAORegistry.getCustomerOrderRequestDAO().saveOrUpdate(orderRequest);
			
			try{
				MapUtil.copySVGMapandText(event.getVenueId(), event.getVenueCategoryName(),customerOrder.getId());
			}catch(Exception e){
				e.printStackTrace();
			}
               
			orderConfirmation.setOrderId(customerOrder.getId());
			orderConfirmation.setMessage("Your Order has been initiated");
			orderConfirmation.setStatus(1);	
			TrackingUtils.orderTracking(request, customerOrder, sessionId, referralCode,isFanPrice);
		}catch (Exception e) {
			//later on add money refund procedure here e.g(credit,debit,paypal and loyalty)
			e.printStackTrace();
			error.setDescription("Oops something went wrong while initiating the order");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Oops something went wrong while initiating the order");
			return orderConfirmation;
		}		
		return orderConfirmation;
	}
	
	
	@RequestMapping(value = "/TrackingOrder",method = RequestMethod.POST)
	public @ResponsePayload OrderTracking trackingOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderTracking orderTracking = new OrderTracking();
		try{
			String productTypeStr = request.getParameter("productType");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String eventIdStr = request.getParameter("eventId");
			String categoryTicketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String orderTotalStr = request.getParameter("orderTotal");
			String isFanPriceStr = request.getParameter("isFanPrice");
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String billingAddressIdStr = request.getParameter("billingAddressId");
			String shippingAddressIdStr = request.getParameter("shippingAddressId");
			String loyaltySpentStr = request.getParameter("rewardPoints");
			String appPlatformStr = request.getParameter("platForm");
			String referralCode = request.getParameter("referralCode");
			String refCodeIdentity = request.getParameter("eventRefIdentity"); 
			String trackingId = request.getParameter("trackingId"); 
			String orderNo = request.getParameter("orderId"); 
			String message = request.getParameter("message");
			String clientIPAddress = request.getParameter("clientIPAddress");
			
			RTFOrderTracking rtfOrderTracking = null;
			if(!TextUtil.isEmptyOrNull(trackingId)){
				rtfOrderTracking = DAORegistry.getRtfOrderTrackingDAO().get(Integer.parseInt(trackingId.trim()));
				rtfOrderTracking.setOrderId(orderNo);
				rtfOrderTracking.setMessage(message);
				rtfOrderTracking.setUpdatedDate(new Date());
			}else{
				rtfOrderTracking = new RTFOrderTracking();
				rtfOrderTracking.setBillingAddressId(billingAddressIdStr);
				rtfOrderTracking.setCategoryTicketGroupId(categoryTicketGroupIdStr);
				rtfOrderTracking.setCeateDate(new Date());
				rtfOrderTracking.setCustomerId(customerIdStr);
				rtfOrderTracking.setEventId(eventIdStr);
				rtfOrderTracking.setEventRefIdentity(refCodeIdentity);
				rtfOrderTracking.setIpAddress(clientIPAddress);
				rtfOrderTracking.setIsFanPrice(isFanPriceStr);
				rtfOrderTracking.setMessage(message);
				rtfOrderTracking.setOrderId(orderNo);
				rtfOrderTracking.setOrderTotal(orderTotalStr);
				rtfOrderTracking.setPlatForm(appPlatformStr);
				rtfOrderTracking.setPrimaryPaymentMethod(primaryPaymentMethodStr);
				rtfOrderTracking.setProductType(productTypeStr);
				rtfOrderTracking.setReferralCode(referralCode);
				rtfOrderTracking.setRewardPoints(loyaltySpentStr);
				rtfOrderTracking.setSecondaryPaymentMethod(secondaryPaymentMethodStr);
				rtfOrderTracking.setSessionId(sessionId);
				rtfOrderTracking.setShippingAddressId(shippingAddressIdStr);
				rtfOrderTracking.setUpdatedDate(new Date());
			}
			DAORegistry.getRtfOrderTrackingDAO().saveOrUpdate(rtfOrderTracking);
			orderTracking.setTrackingId(rtfOrderTracking.getId());
			orderTracking.setMessage("Order Information Tracked Successfully");
			orderTracking.setStatus(1);
		}catch(Exception e){
			orderTracking.setMessage("Order Information Tracked Successfully");
			orderTracking.setStatus(0);
			e.printStackTrace();
		}
		return orderTracking;
	}
	

	@RequestMapping(value = "/InitiateTicTrackerLongOrderNew",method = RequestMethod.POST)
	public @ResponsePayload OrderConfirmation tickTrackerLongTixCreateOrderNew(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderConfirmation orderConfirmation = new OrderConfirmation();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}

			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");
			String ticketGroupRefIds = request.getParameter("ticketGroupRefIds");
			String eventIdStr = request.getParameter("eventId");
			String orderTotalStr = request.getParameter("orderTotal"); 
			String isFanPriceStr = request.getParameter("isFanPrice");
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String primaryTransactionId = request.getParameter("primaryTransactionId");			
			String primaryPaymentAmountStrs = request.getParameter("primaryPaymentAmount");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String secondaryTransactionId = request.getParameter("secondaryTransactionId");
			String secondaryPaymentAmountStrs = request.getParameter("secondaryPaymentAmount");
			String thirdPaymentMethodStr = request.getParameter("thirdPaymentMethod");
			String serviceFeesStr = request.getParameter("serviceFees");
			
			String billingAddressIdStr = request.getParameter("billingAddressId");
			String shippingAddressIdStr = request.getParameter("shippingAddressId");
			String shippingSameAsBilling = request.getParameter("shippingSameAsBilling");
			String loyaltySpentStr = request.getParameter("rewardPoints");
			String appPlatformStr = request.getParameter("platForm");
			String referralCode = request.getParameter("referralCode");
			String promoTrackingIdStr = request.getParameter("promoTrackingId");
			String affPromoTrackingIdStr = request.getParameter("affPromoTrackingId");
			String refCodeIdentity = request.getParameter("eventRefIdentity"); 
			String refCId = request.getParameter("cId"); 
			String bFirstName = request.getParameter("bFirstName");
			String bLastName = request.getParameter("bLastName");
			String bAddress1 = request.getParameter("bAddress1");
			String bAddress2 = request.getParameter("bAddress2");
			String bCity = request.getParameter("bCity");
			String bState = request.getParameter("bState");
			String bCountry = request.getParameter("bCountry");
			String bZipCode = request.getParameter("bZipCode");
			String bPhoneNumber = request.getParameter("bPhoneNumber");
			String bEmail = request.getParameter("bEmail");
			String sFirstName = request.getParameter("sFirstName");
			String sLastName = request.getParameter("sLastName");
			String sAddress1 = request.getParameter("sAddress1");
			String sAddress2 = request.getParameter("sAddress2");
			String sCity = request.getParameter("sCity");
			String sState = request.getParameter("sState");
			String sCountry = request.getParameter("sCountry");
			String sZipCode = request.getParameter("sZipCode");
			String sPhoneNumber = request.getParameter("sPhoneNumber");
			String sEmail = request.getParameter("sEmail");
			String orderTypeStr = request.getParameter("orderType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Product Type is Mandatory");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(orderTypeStr)){
				error.setDescription("Order Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Order Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			OrderType orderType = null;
			try{
				orderType = OrderType.valueOf(orderTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid order type");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid order type");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Application Platform is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Please send valid Event Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Event Id");
				return orderConfirmation;
			}
			if(TextUtil.isEmptyOrNull(ticketGroupRefIds)){
				error.setDescription("Ticket Group Ids are mandatory..");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Ticket Group Ids are mandatory");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(isFanPriceStr)){
				isFanPriceStr = "false";
			}
			
			Boolean isLongSale = true;
			Boolean isFanPrice = null;
			try{
				isFanPrice = Boolean.valueOf(isFanPriceStr);
			}catch(Exception e){
				error.setDescription("Fan Price Flag should be true or false");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Fan Price Flag should be true or false");
				return orderConfirmation;
			}
			
			Integer customerId = null;
			try{
				customerId = Integer.parseInt(customerIdStr);
				
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			Integer eventId = null;
			try{
				eventId = Integer.parseInt(eventIdStr);
			}catch(Exception e){
				error.setDescription("Customer id should be integer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Customer id should be integer");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(sessionId)){
				error.setDescription("Session Id is invalid.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Session Id is invalid.");
				return orderConfirmation;
			}
			
			CustomerOrderRequest orderRequest = new CustomerOrderRequest();
			Customer customer = CustomerUtil.getCustomerById(customerId);//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
			Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, productType);
			if(event==null){
				event = DAORegistry.getEventDAO().getEventById(eventId, productType);
			}
			
			if(customer == null){
				error.setDescription("Please choose valid customer");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please choose valid customer");
				return orderConfirmation;
			}
			
			if(event == null){
				error.setDescription("Event not recognized");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Event not recognized");
				return orderConfirmation;
			}
			
			/* Get All Ticket Group Information From Request - Begins */
			String[] ticketGroupIdsArry = ticketGroupRefIds.split(",");
			Integer quantity = 0;
			Double priceTotal = 0.00;
			
			Map<Integer, OrderTicketGroup> ticketGroupMap = new HashMap<Integer, OrderTicketGroup>();
			for (String ticketGroupIdRefIdStr : ticketGroupIdsArry) {
				Integer ticketGroupRefId = Integer.parseInt(ticketGroupIdRefIdStr.trim());
				OrderTicketGroup orderTicketGroup = DAORegistry.getOrderTicketGroupDAO().get(ticketGroupRefId);
				
				if(null == orderTicketGroup){
					error.setDescription("Something Wrong With Ticket Group Ref Ids. Please Contact Administrator.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Something Wrong With Ticket Group Ref Ids.");
					return orderConfirmation;
				}
				quantity = quantity + orderTicketGroup.getQuantity();
				ticketGroupMap.put(ticketGroupRefId, orderTicketGroup);
				priceTotal = priceTotal + TicketUtil.getRoundedValue(orderTicketGroup.getActualPrice() * orderTicketGroup.getQuantity());
			}
			/* Get All Ticket Group Information From Request - Ends */
			
			//paymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,PARTIAL_REWARDS,FULL_REWARDS
			if(TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				error.setDescription("Primary Payment method is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary Payment method is mandatory");
				return orderConfirmation;
			}
			
			PaymentMethod primaryPaymentMethod=null;
			if(!TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				try{
					primaryPaymentMethod = PaymentMethod.valueOf(primaryPaymentMethodStr);
				}catch(Exception e){
					error.setDescription("Please send valid Primary Payment Method");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please send valid Primary Payment Method");
					return orderConfirmation;
				}
			}
			
			Integer shippingAddressId=null,billingAddressId=null;
			
			Double orderTotal = null;
			try{
				orderTotal = Double.valueOf(orderTotalStr.trim());
				orderTotal = Double.valueOf(df.format(orderTotal));
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Order Total is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Order Total is Mandatory");
				return orderConfirmation;
			}
			
			CustomerSuperFan customerSuperFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
			
			Double originalOrderTotal = null,serviceFees = 0.00,serverOrderTotal=0.00;
			
			serverOrderTotal = orderTotal;			
			serviceFees = Double.valueOf(serviceFeesStr.trim());
			
			originalOrderTotal =  TicketUtil.getRoundedValue(priceTotal + serviceFees);
			
			//Setting up Ticket Original Price
			orderRequest.setTicketPrice(0.00);
			
			RTFPromotionalOfferTracking promoCodeTracking = null;
			boolean rtfPromotionalCode = false,isCustReferalDiscountCode = false,isAffilateReferral = false;
			if(null != referralCode && !referralCode.isEmpty()){
				promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTrackingByTrackingId(applicationPlatForm, customer.getId(), 
						sessionId, Integer.parseInt(promoTrackingIdStr.trim()));
				if(RTFPromotionalOfferUtil.validateRTFPromoCode(referralCode.trim())){
					rtfPromotionalCode = true;
				}else {
					if(RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim())){
						if(null != promoCodeTracking && promoCodeTracking.getOfferType().equals(PromotionalType.AFFILIATE_PROMO_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							rtfPromotionalCode = true;
							isAffilateReferral = true;
						}else{
							promoCodeTracking = null;
						}
					}else{
						if(promoCodeTracking.getOfferType().equals(PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE) 
								&& referralCode.toUpperCase().equals(promoCodeTracking.getPromoCode().toUpperCase())){
							rtfPromotionalCode = true;
							isCustReferalDiscountCode = true;
						}else{
							promoCodeTracking = null;
						}
					}
				}
			}
			
			Double discountAmount = 0.00;
			//OrderType orderType = OrderType.REGULAR;
			Double soldPrice = 0.00;
			Double primaryPayAmt = 0.00,secondaryPayAmt=0.00,thirdPayAmount=0.00;
			CustomerSuperFan changeLoyalFan = null;
			
			if(isFanPrice){
				if(null != customerSuperFan){
					
					Boolean isLoyalFanEvent = FavoriteUtil.markArtistAsLoyalFan(customerSuperFan, event);
					
					if(!isLoyalFanEvent){
						if(!customerSuperFan.getTicketPurchased()){
							orderType = OrderType.LOYALFAN;
							customerSuperFan.setEndDate(new Date());
							customerSuperFan.setStatus(Status.DELETED);
							changeLoyalFan = new CustomerSuperFan();
							changeLoyalFan.setCustomerId(customerId);
							changeLoyalFan.setStartDate(new Date());
							changeLoyalFan.setStatus(Status.ACTIVE);
							changeLoyalFan.setTicketPurchased(true);
						}else{
							error.setDescription("You are currently not eligible to purchase this ticket at a discounted price. You will be redirected back to ticket listings.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							orderConfirmation.setRedirectTicketListing(true);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"You are currently not eligible to purchase this ticket at a discounted price");
							return orderConfirmation;
						}
					}else {
						customerSuperFan.setTicketPurchased(true);
						orderType = OrderType.LOYALFAN;
					}
				}else{
					orderType = OrderType.LOYALFAN;
					changeLoyalFan = new CustomerSuperFan();
					changeLoyalFan.setCustomerId(customerId);
					changeLoyalFan.setStartDate(new Date());
					changeLoyalFan.setStatus(Status.ACTIVE);
					changeLoyalFan.setTicketPurchased(true);
				}
			}
			serverOrderTotal =  TicketUtil.getRoundedValue(serverOrderTotal);
			discountAmount = originalOrderTotal - serverOrderTotal;
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double rewardEarnConv = 0.00;
			
			if(isCustReferalDiscountCode){
				rewardEarnConv = promoCodeTracking.getCustomerEarnRewardConv();
			}else{
				rewardEarnConv = loyaltySettings.getRewardConversion();
			}
			
			primaryPayAmt = serverOrderTotal;
			
			CustomerLoyaltyHistory customerLoyaltyHistory = new CustomerLoyaltyHistory();
			CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			PaypalTransactionDetail paypalTransactionDetail = null;
			
			Double activeRewardAmount = RewardConversionUtil.getRewardDoller(customerLoyalty.getActivePointsAsDouble(),loyaltySettings.getDollerConversion());
			Double requiredPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getDollerConversion());
			Double orderRewardEarnAmount = RewardConversionUtil.getRewardDoller(requiredPoints, loyaltySettings.getDollerConversion());
			
			Double orderTotalForRewards = serverOrderTotal;
			try{
				if(null != loyaltySpentStr && !loyaltySpentStr.isEmpty()){
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(Double.valueOf(loyaltySpentStr.trim()),loyaltySettings.getDollerConversion());
					orderTotalForRewards = TicketUtil.getRoundedValue(orderTotalForRewards - enteredRewardAmount);
					if(orderTotalForRewards < 0){
						orderTotalForRewards = 0.00;
					}
				}				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
			if(isAffilateReferral){
				Affiliate affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(referralCode.trim());
				if(affiliateObj!= null && !affiliateObj.getIsEarnRewardPoints()){
					earningRewardPoints = 0.00;
				}
			}
			if(primaryPaymentMethod.equals(PaymentMethod.ACCOUNT_RECIVABLE)){
				earningRewardPoints = 0.00;
			}
			
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setOrderTotal(orderTotal);
			customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
			customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setPointsSpentAsDouble(0.00);
			customerLoyaltyHistory.setRewardSpentAmount(0.00);
			customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
			customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
			customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
			customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
			Double balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - 0;
			Double balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
			customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
			customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
			
			CustomerWallet customerWallet = null;
			CustomerWalletHistory customerWalletHistory = null;
			PartialPaymentMethod secondaryPaymentMethod=null;
			PartialPaymentMethod thirdPaymentMethod=null;
			String thirdPaymentTrxId = null;
			AddressInfo addressInfo = new AddressInfo();
			Boolean isSaveCard = false;
			switch (primaryPaymentMethod) {
			
				case FULL_REWARDS:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					UserAddress shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.FULL_REWARDS);
					addressInfo.setShippingAddress(shippingAddress);
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("Please enter active reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter active reward dollars");
						return orderConfirmation;
					}	
					
					Double loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("Reward dollars amount must be an integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Reward dollars amount must be an integer");
						return orderConfirmation;
					}
					
					
					Double enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(requiredPoints < loyaltySpent){
						Double diffOrderReward = loyaltySpent - requiredPoints;
						error.setDescription("Your have entered an amount more than the required reward dollars. Please subtract "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered an amount more than the required reward dollars.  Please subtract  "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						return orderConfirmation;
					}
					
					if(requiredPoints > loyaltySpent){
						Double diffOrderReward = requiredPoints - loyaltySpent;
						error.setDescription("Your have entered an amount less than the required reward dollars. Please add "+TicketUtil.getRoundedValueString(diffOrderReward)+" reward dollars");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than required reward dollars , reduce "+TicketUtil.getRoundedValueString(diffOrderReward)+" more reward dollars");
						return orderConfirmation;
					}
					
					if((customerLoyalty.getActivePointsAsDouble() < loyaltySpent) ||
							(activeRewardAmount < enteredRewardAmount)){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(activeRewardAmount < enteredRewardAmount){
						error.setDescription("Please enter a valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid reward dollars amount. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
					
					break;
					
				case PARTIAL_REWARDS:
					
					
					if(TextUtil.isEmptyOrNull(loyaltySpentStr)){
						error.setDescription("reward dollars are Mandatory.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"reward dollars are Mandatory");
						return orderConfirmation;
					}	
					
					loyaltySpent = null;
					try{
						loyaltySpent = Double.valueOf(loyaltySpentStr.trim());
					}catch(Exception e){
						error.setDescription("Reward dollars should be Integer.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"reward dollars should be Integer");
						return orderConfirmation;
					}
					

					//Secondary PaymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY
					if(TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						error.setDescription("Please enter a valid Secondary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
						return orderConfirmation;
					}
					
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					enteredRewardAmount = RewardConversionUtil.getRewardDoller(loyaltySpent,loyaltySettings.getDollerConversion());
					
					if(activeRewardAmount < loyaltySpent){
						error.setDescription("Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter valid reward dollars. Your Active Rewards are "+customerLoyalty.getActivePoints());
						return orderConfirmation;
					}
					
					if(requiredPoints < loyaltySpent){
						error.setDescription("Your have entered more than the required reward dollars. Please select Full Rewards as Primary Payment Method."); 
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered more than the required reward dollars. Please select  Full Rewards as Primary Payment Method.");
						return orderConfirmation;
					}
					
					if(requiredPoints == loyaltySpent){
						error.setDescription("Your have entered the required reward dollars. Please select Full Rewards as Primary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Your have entered the required reward dollars amount. Please select  Full Rewards as Primary Payment Method");
						return orderConfirmation;
					}
					
					Double tempOrderTotal = serverOrderTotal - enteredRewardAmount ;
					primaryPayAmt = enteredRewardAmount;
					
					if(secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						try{
							billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
						}catch(Exception e){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
							shippingAddressId =  billingAddressId;
						}else{
							if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
								error.setDescription("Please enter a valid Shipping Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
								return orderConfirmation;
							}
							
							try{
								shippingAddressId = Integer.parseInt(shippingAddressIdStr);
							}catch(Exception e){
								error.setDescription("Shipping Address should be integer");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
								return orderConfirmation;
							}
						}
						
						UserAddress billingAddress = null;
						if(null != billingAddressId){
							billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
						}
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						if(billingAddress == null){
							error.setDescription("Billing Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
							return orderConfirmation;
						}
						addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(billingAddress);
						
						secondaryTransactionId = "PARTIAL_CREDITCARD_TRANSACTION";
						
					}else if (secondaryPaymentMethod.equals(PartialPaymentMethod.CUSTOMER_WALLET)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						secondaryTransactionId = "PARTIAL_CUSTOMER_WALLET_TRANSACTION";
						
						if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						try{
							billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
						}catch(Exception e){
							error.setDescription("Please enter a valid Billing Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
							return orderConfirmation;
						}
						
						if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
							shippingAddressId =  billingAddressId;
						}else{
							if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
								error.setDescription("Please enter a valid Shipping Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
								return orderConfirmation;
							}
							
							try{
								shippingAddressId = Integer.parseInt(shippingAddressIdStr);
							}catch(Exception e){
								error.setDescription("Shipping Address should be integer");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
								return orderConfirmation;
							}
						}
						
						UserAddress billingAddress = null;
						if(null != billingAddressId){
							billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
						}
						shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
						
						if(shippingAddress == null){
							error.setDescription("Shipping Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
							return orderConfirmation;
						}
						
						if(billingAddress == null){
							error.setDescription("Billing Address is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
							return orderConfirmation;
						}
						addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
						addressInfo.setShippingAddress(shippingAddress);
						addressInfo.setBillingAddress(billingAddress);
						
						if(!TextUtil.isEmptyOrNull(thirdPaymentMethodStr)){
							try{
								thirdPaymentMethod = PartialPaymentMethod.valueOf(thirdPaymentMethodStr);
							}catch(Exception e){
								error.setDescription("Please enter a valid Third Payment Method");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Third Payment Method");
								return orderConfirmation;
							}
						}
						
						if(null != thirdPaymentMethod && thirdPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
							
							String thirdPaymentAmountStrs = request.getParameter("thirdPaymentAmount");
							String thirdTransactionId = request.getParameter("thirdTransactionId");
							
							if(TextUtil.isEmptyOrNull(thirdPaymentAmountStrs)){
								error.setDescription("Third payment amount is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Third payment amount is not valid");
								return orderConfirmation;
							}
							
							try{
								thirdPayAmount = Double.valueOf(thirdPaymentAmountStrs.trim());
							}catch(Exception e){
								error.setDescription("Third payment amount is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Third payment amount is not valid");
								return orderConfirmation;
							}
							
							if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
								error.setDescription("Please enter a valid Billing Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
								return orderConfirmation;
							}
							
							try{
								billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
							}catch(Exception e){
								error.setDescription("Please enter a valid Billing Address");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
								return orderConfirmation;
							}
							
							if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
								shippingAddressId =  billingAddressId;
							}else{
								if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
									error.setDescription("Please enter a valid Shipping Address");
									orderConfirmation.setError(error);
									orderConfirmation.setStatus(0);
									TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
									return orderConfirmation;
								}
								
								try{
									shippingAddressId = Integer.parseInt(shippingAddressIdStr);
								}catch(Exception e){
									error.setDescription("Shipping Address should be integer");
									orderConfirmation.setError(error);
									orderConfirmation.setStatus(0);
									TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
									return orderConfirmation;
								}
							}
							
							billingAddress = null;
							if(null != billingAddressId){
								billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
							}
							shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
							
							if(shippingAddress == null){
								error.setDescription("Shipping Address is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
								return orderConfirmation;
							}
							
							if(billingAddress == null){
								error.setDescription("Billing Address is not valid");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
								return orderConfirmation;
							}
							addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
							addressInfo.setShippingAddress(shippingAddress);
							addressInfo.setBillingAddress(billingAddress);
							
							thirdPaymentTrxId = "PARTIAL_THIRD_CREDITCARD_TRXID";
							
						}
					}
					
					
					customerLoyaltyHistory.setCustomerId(customerId);
					customerLoyaltyHistory.setCreateDate(new Date());
					customerLoyaltyHistory.setOrderTotal(orderTotal);
					customerLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					customerLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setPointsSpentAsDouble(loyaltySpent);
					customerLoyaltyHistory.setRewardSpentAmount(enteredRewardAmount);
					customerLoyaltyHistory.setPointsEarnedAsDouble(earningRewardPoints);
					customerLoyaltyHistory.setRewardEarnAmount(orderRewardEarnAmount);
					customerLoyaltyHistory.setInitialRewardPointsAsDouble(customerLoyalty.getActivePointsAsDouble());
					customerLoyaltyHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
					balanceRewardPoints = customerLoyalty.getActivePointsAsDouble() + earningRewardPoints - loyaltySpent;
					balanceRewardAmount = RewardConversionUtil.getRewardDoller(balanceRewardPoints,loyaltySettings.getDollerConversion());
					customerLoyaltyHistory.setBalanceRewardPointsAsDouble(balanceRewardPoints);
					customerLoyaltyHistory.setBalanceRewardAmount(balanceRewardAmount);
								
					
					break;
					
				case CREDITCARD:
					
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					UserAddress billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					primaryTransactionId = "CREDITCARD_TRANSACTION";
	
					break;
					
				case PAYPAL:
					
					if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
						error.setDescription("Please enter a valid Shipping Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
						return orderConfirmation;
					}
					
					try{
						shippingAddressId = Integer.parseInt(shippingAddressIdStr);
					}catch(Exception e){
						error.setDescription("Shipping Address should be integer");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
						return orderConfirmation;
					}
					
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setPaymentMethod(PaymentMethod.PAYPAL);
					primaryTransactionId = "PAYPAL_TRANSACTION";
					
					break;
					
				case GOOGLEPAY:
					
					break;
					
				case IPAY:
					
					break;
					
				case CUSTOMER_WALLET:
					
					if(TextUtil.isEmptyOrNull(primaryPaymentAmountStrs)){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					try{
						primaryPayAmt = Double.valueOf(primaryPaymentAmountStrs.trim());
					}catch(Exception e){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					primaryTransactionId = "CUSTOMER_WALLET";
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					if(null != secondaryPaymentMethod && secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryPaymentAmountStrs)){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						
						try{
							secondaryPayAmt = Double.valueOf(secondaryPaymentAmountStrs.trim());
						}catch(Exception e){
							error.setDescription("Secondary payment amount is not valid");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Secondary payment amount is not valid");
							return orderConfirmation;
						}
						secondaryTransactionId = "WALLET_CREDITCARD_TRANSACTION";
					}
					
					break;
					
				case ACCOUNT_RECIVABLE:
					
					if(TextUtil.isEmptyOrNull(primaryPaymentAmountStrs)){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					try{
						primaryPayAmt = Double.valueOf(primaryPaymentAmountStrs.trim());
					}catch(Exception e){
						error.setDescription("Primary payment amount is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Primary payment amount is not valid");
						return orderConfirmation;
					}
					
					primaryTransactionId = "ACCOUNT_RECIVABLE";
					
					if(TextUtil.isEmptyOrNull(billingAddressIdStr)){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					try{
						billingAddressId =  Integer.parseInt(billingAddressIdStr.trim());
					}catch(Exception e){
						error.setDescription("Please enter a valid Billing Address");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Billing Address");
						return orderConfirmation;
					}
					
					if(!TextUtil.isEmptyOrNull(shippingSameAsBilling) && shippingSameAsBilling.equals("Yes")){
						shippingAddressId =  billingAddressId;
					}else{
						if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
							error.setDescription("Please enter a valid Shipping Address");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Shipping Address");
							return orderConfirmation;
						}
						
						try{
							shippingAddressId = Integer.parseInt(shippingAddressIdStr);
						}catch(Exception e){
							error.setDescription("Shipping Address should be integer");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address should be integer");
							return orderConfirmation;
						}
					}
					
					billingAddress= null;
					if(null != billingAddressId){
						billingAddress = DAORegistry.getUserAddressDAO().get(billingAddressId);
					}
					shippingAddress = DAORegistry.getUserAddressDAO().get(shippingAddressId);
					
					if(shippingAddress == null){
						error.setDescription("Shipping Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Shipping Address is not valid");
						return orderConfirmation;
					}
					
					if(billingAddress == null){
						error.setDescription("Billing Address is not valid");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Billing Address is not valid");
						return orderConfirmation;
					}
					
					addressInfo.setPaymentMethod(PaymentMethod.CREDITCARD);
					addressInfo.setShippingAddress(shippingAddress);
					addressInfo.setBillingAddress(billingAddress);
					
					break;

	
				default:
					break;
			}
			
			
			CustomerOrderDetail customerOrderDetail = new CustomerOrderDetail();
			
			if(addressInfo.getPaymentMethod().equals(PaymentMethod.CREDITCARD)){
				
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getBillingAddress().getAddressLine1());
				customerOrderDetail.setBillingAddress2(addressInfo.getBillingAddress().getAddressLine2());
				customerOrderDetail.setBillingCity(addressInfo.getBillingAddress().getCity());
				customerOrderDetail.setBillingCountry(addressInfo.getBillingAddress().getCountry().getCountryName());
				customerOrderDetail.setBillingFirstName(addressInfo.getBillingAddress().getFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getBillingAddress().getLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getBillingAddress().getPhone1());
				customerOrderDetail.setBillingPhone2(addressInfo.getBillingAddress().getPhone2());
				customerOrderDetail.setBillingState(addressInfo.getBillingAddress().getState().getName());
				/*customerOrderDetail.setBillingCountryId(addressInfo.getBillingAddress().getCountry().getId());
				customerOrderDetail.setBillingStateId(addressInfo.getBillingAddress().getState().getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getBillingAddress().getZipCode());
				customerOrderDetail.setBillingEmail(null != addressInfo.getBillingAddress().getEmail()?addressInfo.getBillingAddress().getEmail():customer.getEmail());
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			} else if (addressInfo.getPaymentMethod().equals(PaymentMethod.FULL_REWARDS)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else if (addressInfo.getPaymentMethod().equals(PaymentMethod.PAYPAL)){
				
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getShippingAddress().getAddressLine1());
				customerOrderDetail.setShippingAddress2(addressInfo.getShippingAddress().getAddressLine2());
				customerOrderDetail.setShippingCity(addressInfo.getShippingAddress().getCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getShippingAddress().getFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getShippingAddress().getLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getShippingAddress().getPhone1());
				customerOrderDetail.setShippingPhone2(addressInfo.getShippingAddress().getPhone2());
				customerOrderDetail.setShippingCountry(addressInfo.getShippingAddress().getCountry().getCountryName());
				customerOrderDetail.setShippingState(addressInfo.getShippingAddress().getState().getName());
				/*customerOrderDetail.setShippingCountryId(addressInfo.getShippingAddress().getCountry().getId());
				customerOrderDetail.setShippingStateId(addressInfo.getShippingAddress().getState().getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getShippingAddress().getZipCode());
				customerOrderDetail.setShippingEmail(null != addressInfo.getShippingAddress().getEmail()?addressInfo.getShippingAddress().getEmail():customer.getEmail());
			
			}else{
				/*Country billingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getbCountry());
				State billingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(billingCountry.getId(), addressInfo.getbState());*/
				//Getting Billing Information from AddressInfo 
				customerOrderDetail.setBillingAddress1(addressInfo.getbAddress1());
				customerOrderDetail.setBillingAddress2(addressInfo.getbAddress2());
				customerOrderDetail.setBillingCity(addressInfo.getbCity());
				customerOrderDetail.setBillingCountry(addressInfo.getbCountry());
				customerOrderDetail.setBillingFirstName(addressInfo.getbFirstName());
				customerOrderDetail.setBillingLastName(addressInfo.getbLastName());
				customerOrderDetail.setBillingPhone1(addressInfo.getbPhoneNumber());
				customerOrderDetail.setBillingState(addressInfo.getbState());
				/*customerOrderDetail.setBillingCountryId(billingCountry.getId());
				customerOrderDetail.setBillingStateId(billingState.getId());*/
				customerOrderDetail.setBillingZipCode(addressInfo.getbZipCode());
				customerOrderDetail.setBillingEmail(null!=addressInfo.getbEmail()?addressInfo.getbEmail():customer.getEmail());
				
				/*Country shippingCountry = DAORegistry.getCountryDAO().getCountryByCountryName(addressInfo.getsCountry());
				State shippingState = DAORegistry.getStateDAO().getStateByCountryIdAndStateName(shippingCountry.getId(), addressInfo.getsState());*/
				//Getting Shipping Information from AddressInfo 
				customerOrderDetail.setShippingAddress1(addressInfo.getsAddress1());
				customerOrderDetail.setShippingAddress2(addressInfo.getsAddress2());
				customerOrderDetail.setShippingCity(addressInfo.getsCity());
				customerOrderDetail.setShippingFirstName(addressInfo.getsFirstName());
				customerOrderDetail.setShippingLastName(addressInfo.getsLastName());
				customerOrderDetail.setShippingPhone1(addressInfo.getsPhoneNumber());
				customerOrderDetail.setShippingCountry(addressInfo.getsCountry());
				customerOrderDetail.setShippingState(addressInfo.getsState());
				/*customerOrderDetail.setShippingCountryId(shippingCountry.getId());
				customerOrderDetail.setShippingStateId(shippingState.getId());*/
				customerOrderDetail.setShippingZipCode(addressInfo.getsZipCode());
				customerOrderDetail.setShippingEmail(null!=addressInfo.getsEmail()?addressInfo.getsEmail():customer.getEmail());
				
			}
			
			
			CustomerLoyaltyHistory refererCustLoyaltyHistory = null;
			CustomerLoyalty refererCustomerLoyalty = null;
			ReferredCodeTracking referredCodeTracking = null;
			Boolean validReferralCode = false,affiliatePromoCode = false;
			
			AffiliateCashReward affiliateCashReward = null;
			AffiliateCashRewardHistory affiliateCashRewardHistory = null;
			
			//Customer custReferal = null;
			if(null != referralCode && !referralCode.isEmpty() && (!rtfPromotionalCode || isCustReferalDiscountCode || isAffilateReferral)){
				
				Affiliate affiliate = RTFAffiliateBrokerUtil.getAffiliateObjByPromoCode(referralCode.trim());
				
				if(null != affiliate && !isCustReferalDiscountCode){
					
					affiliatePromoCode =  true;
					
					affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().getAffiliateByUserId(affiliate.getUserId());
					Double pendingCash = RewardConversionUtil.getAffiliateCashCredit(orderTotalForRewards, affiliate.getPhoneOrderCashReward());
					
					if(null == affiliateCashReward){
						affiliateCashReward= new AffiliateCashReward();
						affiliateCashReward.setUserId(affiliate.getUserId());
						affiliateCashReward.setActiveCash(0.00);
						affiliateCashReward.setPendingCash(pendingCash);
						affiliateCashReward.setLastCreditedCash(0.00);
						affiliateCashReward.setTotalCreditedCash(0.00);
						affiliateCashReward.setLastDebitedCash(0.00);
						affiliateCashReward.setTotalDebitedCash(0.00);
						affiliateCashReward.setLastVoidCash(0.00);
						affiliateCashReward.setTotalVoidedCash(0.00);
					}else{
						affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash()+pendingCash);
					}
					affiliateCashRewardHistory = new AffiliateCashRewardHistory();
					affiliateCashRewardHistory.setPromoCode(referralCode.trim());
					affiliateCashRewardHistory.setCustomerId(customerId);
					affiliateCashRewardHistory.setUserId(affiliate.getUserId());
					affiliateCashRewardHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					affiliateCashRewardHistory.setCreditConv( affiliate.getPhoneOrderCashReward());
					affiliateCashRewardHistory.setCreditedCash(pendingCash);
					affiliateCashRewardHistory.setCreateDate(new Date());
					affiliateCashRewardHistory.setUpdatedDate(new Date());
					affiliateCashRewardHistory.setIsRewardsEmailSent(Boolean.FALSE);
					affiliateCashRewardHistory.setOrderTotal(orderTotalForRewards);
				}else{
					
					Customer custReferrer = DAORegistry.getCustomerDAO().getCustomerByIdByProduct(promoCodeTracking.getReferredBy(), productType);
					if(custReferrer.getReferrerCode() == null){
					}else{
					}
						
					refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferrer.getId());
					
					//Double earningRewardPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, rewardEarnConv);
					
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, promoCodeTracking.getReferrarEarnRewardConv());
					if(isAffilateReferral){
						Affiliate affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(referralCode.trim());
						if(affiliateObj!= null && !affiliateObj.getIsEarnRewardPoints()){
							pointsEarned = 0.00;
						}
					}
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferrer.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(promoCodeTracking.getReferrarEarnRewardConv());
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderType(orderType);
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferrer.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferrer.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("ReferralCode");
					
					validReferralCode = true;
					
				}
				
			}else if((null != refCodeIdentity && !refCodeIdentity.isEmpty()) 
					&& (refCId != null && !refCId.isEmpty())){
				
				Customer custReferal = new Customer();
				custReferal = SecurityUtil.validateReferalIdentity(refCId, refCodeIdentity, eventId, customerId, referredCodeTracking,custReferal);
				if(custReferal.getEventShareValidated()){
					try{
						refererCustomerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custReferal.getId());	
					}catch(Exception e){
						e.printStackTrace();
					}
					
					Double pointsEarned = RewardConversionUtil.getRewardPoints(orderTotalForRewards, rewardEarnConv);
					if(isAffilateReferral){
						Affiliate affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(referralCode.trim());
						if(affiliateObj!= null && !affiliateObj.getIsEarnRewardPoints()){
							pointsEarned = 0.00;
						}
					}
					refererCustLoyaltyHistory = new CustomerLoyaltyHistory();
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					refererCustLoyaltyHistory.setRewardConversionRate(rewardEarnConv);
					refererCustLoyaltyHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setPointsSpentAsDouble(0.00);
					refererCustLoyaltyHistory.setRewardSpentAmount(0.00);
					refererCustLoyaltyHistory.setPointsEarnedAsDouble(pointsEarned);
					refererCustLoyaltyHistory.setRewardEarnAmount(RewardConversionUtil.getRewardDoller(pointsEarned,loyaltySettings.getDollerConversion()));
					refererCustLoyaltyHistory.setInitialRewardPointsAsDouble(refererCustomerLoyalty.getActivePointsAsDouble());
					refererCustLoyaltyHistory.setInitialRewardAmount(refererCustomerLoyalty.getActiveRewardDollers());
					Double balRewardPoints = refererCustomerLoyalty.getActivePointsAsDouble() + pointsEarned - 0;
					Double balRewardAmount = RewardConversionUtil.getRewardDoller(balRewardPoints,loyaltySettings.getDollerConversion());
					refererCustLoyaltyHistory.setBalanceRewardPointsAsDouble(balRewardPoints);
					refererCustLoyaltyHistory.setBalanceRewardAmount(balRewardAmount);
					
					refererCustomerLoyalty.setPendingPointsAsDouble((null != refererCustomerLoyalty.getPendingPointsAsDouble()?refererCustomerLoyalty.getPendingPointsAsDouble():0)+pointsEarned);
					refererCustomerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
					refererCustomerLoyalty.setLastUpdate(new Date());
					
					refererCustLoyaltyHistory.setOrderType(OrderType.REFERRER);
					refererCustLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
					refererCustLoyaltyHistory.setUpdatedDate(new Date());
					refererCustLoyaltyHistory.setCreateDate(new Date());
					refererCustLoyaltyHistory.setOrderTotal(serverOrderTotal);
					refererCustLoyaltyHistory.setOrderId(custReferal.getId());
					refererCustLoyaltyHistory.setCustomerId(custReferal.getId());
					
					referredCodeTracking = new ReferredCodeTracking();
					referredCodeTracking.setReferredBy(custReferal.getId());
					referredCodeTracking.setReferredTo(customer.getId());
					referredCodeTracking.setReferredCode(custReferal.getReferrerCode());
					referredCodeTracking.setReferredDate(new Date());
					referredCodeTracking.setReferrenceType("EventShareLinkReferral");
					validReferralCode = true;
				}
			}
			
			Property property = null;
			if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
				Long primaryTrxId =  Long.valueOf(property.getValue());
				primaryTrxId++;
				primaryTransactionId = PaginationUtil.fullRewardPaymentPrefix+primaryTrxId;
				property.setValue(String.valueOf(primaryTrxId));
			}else if(primaryPaymentMethod.equals(PaymentMethod.PARTIAL_REWARDS)){
				property = DAORegistry.getPropertyDAO().get("rtf.partialrewards.auto.primarytrxid");
				Long partialPrimaryTrxId =  Long.valueOf(property.getValue());
				partialPrimaryTrxId++;
				primaryTransactionId = PaginationUtil.partialRewardPaymentPrefix+partialPrimaryTrxId;
				property.setValue(String.valueOf(partialPrimaryTrxId));
			}
			
			CustomerOrder customerOrder = new CustomerOrder();
			Customer customerDetail = new Customer();
			customerDetail.setId(customerId);
			customerOrder.setCategoryTicketGroupId(-1);
			customerOrder.setOrderType(orderType);
			customerOrder.setCustomer(customerDetail);
			customerOrder.setEventDateTemp(event.getEventDate());
			customerOrder.setEventId(event.getEventId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventTime(null != event.getEventTime()?event.getEventTime():null);
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setVenueName(event.getVenueName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueState(event.getState());
			customerOrder.setVenueCountry(event.getCountry());
			customerOrder.setProductType(productType);
			customerOrder.setShippingMethod("ETICKETS");
			customerOrder.setTaxesAsDouble(null != serviceFeesStr && !serviceFeesStr.isEmpty()?Double.valueOf(serviceFeesStr.trim()):serviceFees);
			//customerOrder.setTaxesAsDouble(serviceFees);
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(event.getEventDate());
			calendar.add(Calendar.DAY_OF_MONTH, -5);
			
			Double finalTicketPrice = serverOrderTotal / quantity;
			
			customerOrder.setShippingDateTemp(calendar.getTime());
			customerOrder.setStatus(OrderStatus.PAYMENT_PENDING);
			customerOrder.setIsLongSale(isLongSale);
			customerOrder.setQty(quantity);			
			customerOrder.setSection("LONG-SALE");
			customerOrder.setSectionDescription("LONG-SALE");
			customerOrder.setLastUpdatedDateTemp(new Date());
			customerOrder.setCreateDateTemp(new Date());			
			customerOrder.setTicketPriceAsDouble(finalTicketPrice);
			customerOrder.setTicketSoldPriceAsDouble(soldPrice);
			customerOrder.setOriginalOrderTotal(originalOrderTotal);
			customerOrder.setDiscountAmount(discountAmount);
			customerOrder.setOrderTotalAsDouble(serverOrderTotal);
			customerOrder.setPrimaryPayAmtAsDouble(primaryPayAmt);
			customerOrder.setSecondaryPayAmtAsDouble(secondaryPayAmt);
			customerOrder.setPrimaryPaymentMethod(primaryPaymentMethod);
			customerOrder.setPrimaryTransactionId(primaryTransactionId);
			customerOrder.setSecondaryPaymentMethod(null != secondaryPaymentMethod ? secondaryPaymentMethod:PartialPaymentMethod.NULL);
			customerOrder.setSecondaryTransactionId(null != secondaryTransactionId ? secondaryTransactionId:"");
			customerOrder.setThirdPaymentMethod(null != thirdPaymentMethod ? thirdPaymentMethod:PartialPaymentMethod.NULL);
			customerOrder.setThirdTransactionId(null != thirdPaymentTrxId ? thirdPaymentTrxId:"");
			customerOrder.setThirdPayAmtAsDouble(thirdPayAmount);
			customerOrder.setAppPlatForm(applicationPlatForm);
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setIsInvoiceSent(false);
			customerOrder.setBrokerId(1001);
			customerOrder.setBrokerServicePerc(TicketUtil.getBrokerServiceFeePerc(1001));
			
			/*if(isLongSale){
				customerOrder.setActualSeat(categoryTicketGroup.getActualSeat());
				customerOrder.setActualSection(categoryTicketGroup.getLongSection());
				customerOrder.setActualRow(categoryTicketGroup.getRow());
			}*/
			
			
			DAORegistry.getCustomerOrderDAO().saveOrUpdate(customerOrder);
		    
			if(null != property){
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
			}
			
			
			customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()-(null != customerLoyaltyHistory.getPointsSpentAsDouble()?customerLoyaltyHistory.getPointsSpentAsDouble():0));
			customerLoyalty.setPendingPointsAsDouble((null != customerLoyalty.getPendingPointsAsDouble()?customerLoyalty.getPendingPointsAsDouble():0)+customerLoyaltyHistory.getPointsEarnedAsDouble());
			customerLoyalty.setDollerConversion(loyaltySettings.getDollerConversion());
			customerLoyalty.setLastUpdate(new Date());
			customerLoyaltyHistory.setOrderType(orderType);
			customerLoyaltyHistory.setCreateDate(new Date());
			customerLoyaltyHistory.setUpdatedDate(new Date());
			customerLoyaltyHistory.setOrderTotal(serverOrderTotal);
			customerLoyaltyHistory.setOrderId(customerOrder.getId());
			customerLoyaltyHistory.setCustomerId(customerId);
			customerLoyaltyHistory.setRewardStatus(RewardStatus.ORDERPENDING);
			
			DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
			DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(customerLoyaltyHistory);
			
			/* Add All Pending Values to Order Request - Begins  */
			orderRequest.setSessionId(sessionId);
			orderRequest.setOrderId(customerOrder.getId());
			orderRequest.setOrderLoyaltyHistoryId(customerLoyaltyHistory.getId());
			orderRequest.setOrderLoyaltyInfoId(customerLoyalty.getId());
			orderRequest.setOrderTotal(serverOrderTotal);
			orderRequest.setOrderType(orderType);
			orderRequest.setOriginalOrderTotal(originalOrderTotal);
			orderRequest.setAppPlatForm(customerOrder.getAppPlatForm());
			orderRequest.setCategoryTicketGroupId(-1);
			orderRequest.setCustomerId(customerId);
			orderRequest.setEventId(eventId);
			orderRequest.setInvoiceId(-1);
			orderRequest.setIsLongSale(true);
			orderRequest.setIsLoyalFan(isFanPrice);
			orderRequest.setPrimaryPayAmt(primaryPayAmt);
			orderRequest.setPrimaryPaymentMethod(primaryPaymentMethod);
			orderRequest.setPrimaryTransactionId(primaryTransactionId);
			orderRequest.setSecondaryPayAmt(secondaryPayAmt);
			orderRequest.setSecondaryPaymentMethod(secondaryPaymentMethod);
			orderRequest.setSecondaryTransactionId(secondaryTransactionId);
			orderRequest.setQuantity(customerOrder.getQty());
			orderRequest.setSoldPrice(soldPrice);
			orderRequest.setThirdPayAmt(customerOrder.getThirdPayAmtAsDouble());
			orderRequest.setThirdPaymentMethod(customerOrder.getThirdPaymentMethod());
			orderRequest.setThirdTransactionId(customerOrder.getThirdTransactionId());
			orderRequest.setTicketGroupId(null);
			
			String customerIpAddress = "";
			if(customerOrder.getAppPlatForm().equals(ApplicationPlatForm.IOS) || 
					customerOrder.getAppPlatForm().equals(ApplicationPlatForm.ANDROID)){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			orderRequest.setIpAddress(customerIpAddress);
			orderRequest.setStatus(OrderStatus.PAYMENT_PENDING);
			orderRequest.setCreatedTime(new Date());
			orderRequest.setUpdatedTime(new Date());
			/* Add All Pending Values to Order Request - Ends  */
			
			if(rtfPromotionalCode){
				try{
					promoCodeTracking.setOrderId(customerOrder.getId());
					promoCodeTracking.setStatus("ORDERPENDING");
					promoCodeTracking.setUpdatedDate(new Date());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
					
					//Add RTF Promo Tracking ID Into Order Request
					orderRequest.setRtfPromoTrackingId(promoCodeTracking.getId());
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(affiliatePromoCode){
				affiliateCashReward.setLastUpdate(new Date());
				DAORegistry.getAffiliateCashRewardDAO().saveOrUpdate(affiliateCashReward);
				
				affiliateCashRewardHistory.setOrderId(customerOrder.getId());
				DAORegistry.getAffiliateCashRewardHistoryDAO().saveOrUpdate(affiliateCashRewardHistory);
				
				try{
					
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = null;
					if(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE) || 
							applicationPlatForm.equals(ApplicationPlatForm.TICK_TRACKER)){
						
						affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTrackingByTrackingId(applicationPlatForm, 
								affiliateCashReward.getUserId(), customer.getId(), sessionId,  Integer.parseInt(affPromoTrackingIdStr.trim()));
						
						if(null != affiliatePromoCodeTracking){
							affiliatePromoCodeTracking.setUpdatedDate(new Date());
							affiliatePromoCodeTracking.setStatus("ORDERPENDING");
							affiliatePromoCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
							affiliatePromoCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
							affiliatePromoCodeTracking.setUpdatedDate(new Date());
							affiliatePromoCodeTracking.setOrderId(customerOrder.getId());
							affiliatePromoCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
							affiliatePromoCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
						}
					}
					
					if(null == affiliatePromoCodeTracking){
						affiliatePromoCodeTracking = new AffiliatePromoCodeTracking();
						affiliatePromoCodeTracking.setIsLongTicket(true);
						affiliatePromoCodeTracking.setTicketGroupId(-1);
						affiliatePromoCodeTracking.setEventId(eventId);
						affiliatePromoCodeTracking.setIpAddress(ip);
						affiliatePromoCodeTracking.setPlatForm(applicationPlatForm);
						affiliatePromoCodeTracking.setSessionId(sessionId);
						affiliatePromoCodeTracking.setStatus("ORDERPENDING");
						affiliatePromoCodeTracking.setCustRewardConv(loyaltySettings.getRewardConversion());
						affiliatePromoCodeTracking.setCustRewardPoints(customerLoyaltyHistory.getPointsEarnedAsDouble());
						affiliatePromoCodeTracking.setCreatedDate(new Date());
						affiliatePromoCodeTracking.setUpdatedDate(new Date());
						affiliatePromoCodeTracking.setCustomerId(customerId);
						affiliatePromoCodeTracking.setUserId(affiliateCashReward.getUserId());
						affiliatePromoCodeTracking.setOrderId(customerOrder.getId());
						affiliatePromoCodeTracking.setPromoCode(referralCode);
						affiliatePromoCodeTracking.setCreditConv(affiliateCashRewardHistory.getCreditConv());
						affiliatePromoCodeTracking.setCreditedCash(affiliateCashRewardHistory.getCreditedCash());
					}
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().saveOrUpdate(affiliatePromoCodeTracking);
					
					//Add Affiliate Reward ID's to Order Request.
					orderRequest.setAffCashRewardHistoryId(affiliateCashRewardHistory.getId());
					orderRequest.setAffCashRewardId(affiliateCashReward.getId());
					orderRequest.setAffPromoTrackingId(affiliatePromoCodeTracking.getId());
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(validReferralCode || isCustReferalDiscountCode){
				
				refererCustLoyaltyHistory.setOrderId(customerOrder.getId());
				
				if(null != refererCustLoyaltyHistory){ //Safer side to avoid null pointer exception
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(refererCustomerLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(refererCustLoyaltyHistory);
					DAORegistry.getReferredCodeTrackingDAO().saveOrUpdate(referredCodeTracking);
					
					//Add Referral Reward ID's to Order Request.
					orderRequest.setRefCustLoyaltyHistoryId(refererCustLoyaltyHistory.getId());
					orderRequest.setRefCustLoyaltyInfoId(refererCustomerLoyalty.getId());
					orderRequest.setRefTrackingId(referredCodeTracking.getId());
					referralCode = referredCodeTracking.getReferredCode();
				}
			}
			
			if(isFanPrice ){
				   try{
					Date startdate = null;
					if(null != customerSuperFan){
						startdate = customerSuperFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(customerSuperFan);
						
						//Add Old Loyal Fan ID to Order Request.
						orderRequest.setOldLoyalFanId(customerSuperFan.getId());
					}
					if(null != changeLoyalFan){
						startdate = changeLoyalFan.getStartDate();
						DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(changeLoyalFan);
						
						//Add New Loyal Fan ID to Order Request.
						orderRequest.setNewLoyalFanId(changeLoyalFan.getId());
						
						/*calendar = new GregorianCalendar();
						calendar.setTime(startdate);
						calendar.add(Calendar.YEAR,1);
						startdate = calendar.getTime();
						String formattedendDate = dateTimeFormat.format(startdate);
						
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("customer", customer);  
						mailMap.put("changeLoyalFan", changeLoyalFan); 
						mailMap.put("startdate", formattedendDate);
						MailAttachment[] mailAttachment = new MailAttachment[1];
						String filePath = URLUtil.getLogoImage();
						mailAttachment[0] = new MailAttachment(null,"image/png","logo.png");
					
						if(null != changeLoyalFan && changeLoyalFan.getTicketPurchased()==true){
							try{
								mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
										null, "AODev@rightthisway.com", 
										"Reward The Fan: LOYAL FAN ",
										"mail-loyal-fan.html", mailMap, "text/html", null,mailAttachment,filePath);
							}catch(Exception e) {
								e.printStackTrace();
							}
						}*/
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
               
			customerOrderDetail.setOrderId(customerOrder.getId());
			DAORegistry.getCustomerOrderDetailDAO().save(customerOrderDetail);
			
			DAORegistry.getCustomerOrderRequestDAO().saveOrUpdate(orderRequest);
			
			
			/*Update Order Id into Order Ticket Group Details - Begins*/
			Collection<OrderTicketGroup> orderTicketGroups = ticketGroupMap.values();
			for (OrderTicketGroup orderTicketGroup : orderTicketGroups) {
				orderTicketGroup.setOrderId(customerOrder.getId());
			}
			DAORegistry.getOrderTicketGroupDAO().updateAll(orderTicketGroups);
			/*Update Order Id into Order Ticket Group Details - Ends*/
			
			
			try{
				MapUtil.copySVGMapandText(event.getVenueId(), event.getVenueCategoryName(),customerOrder.getId());
			}catch(Exception e){
				e.printStackTrace();
			}
               
			orderConfirmation.setOrderId(customerOrder.getId());
			orderConfirmation.setMessage("Your Order has been initiated");
			orderConfirmation.setStatus(1);	
			TrackingUtils.orderTracking(request, customerOrder, sessionId, referralCode,isFanPrice);
		}catch (Exception e) {
			//later on add money refund procedure here e.g(credit,debit,paypal and loyalty)
			e.printStackTrace();
			error.setDescription("Oops something went wrong while initiating the order");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Oops something went wrong while initiating the order");
			return orderConfirmation;
		}		
		return orderConfirmation;
	}
	@RequestMapping(value = "/ConfirmTicTrackerLongOrder",method = RequestMethod.POST)
	public @ResponsePayload OrderConfirmation confirmTickTrackerLongOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderConfirmation orderConfirmation = new OrderConfirmation();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
				return orderConfirmation;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String appPlatformStr = request.getParameter("platForm");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String orderIdStr = request.getParameter("orderId");
			String isPaymentSuccessStr = request.getParameter("isPaymentSuccess");
			String primaryPaymentMethodStr = request.getParameter("primaryPaymentMethod");
			String primaryTransactionId = request.getParameter("primaryTransactionId");
			String secondaryPaymentMethodStr = request.getParameter("secondaryPaymentMethod");
			String secondaryTransactionId = request.getParameter("secondaryTransactionId");
			String thirdPaymentMethodStr = request.getParameter("thirdPaymentMethod");
			String thirdTransactionId = request.getParameter("thirdTransactionId");
			String walletTransactionIdStr = request.getParameter("walletTransactionId");
			
			String ipAddress = request.getParameter("clientIPAddress");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Product Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Application Platform is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
				return orderConfirmation;
			}
			
			Integer orderId = null;
			CustomerOrder customerOrder = null;
			CustomerOrderRequest orderRequest = null;
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
				customerOrder = DAORegistry.getCustomerOrderDAO().getPaymentPendingOrderById(orderId);
				if(null == customerOrder ){
					error.setDescription("Order No is not valid");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
					return orderConfirmation;
				}
				orderRequest = DAORegistry.getCustomerOrderRequestDAO().getOrderRequestByOrderId(orderId);
				if(null == orderRequest ){
					error.setDescription("Oops Something Went Wrong. Please Contact Website Administrator.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Oops Something Went Wrong. Please Contact Website Administrator.");
					return orderConfirmation;
				}
				
			}catch(Exception e){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Order No is not valid");
				return orderConfirmation;
			}

			if(TextUtil.isEmptyOrNull(isPaymentSuccessStr)){
				error.setDescription("Payment Success Flag is invalid.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Payment Success Flag is invalid.");
				return orderConfirmation;
			}
			
			Boolean isPaymentSuccess = Boolean.valueOf(isPaymentSuccessStr);
			
			//paymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,PARTIAL_REWARDS,FULL_REWARDS
			if(TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				error.setDescription("Primary Payment method is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Payment method is mandatory");
				return orderConfirmation;
			}
			
			PaymentMethod primaryPaymentMethod=null;
			if(!TextUtil.isEmptyOrNull(primaryPaymentMethodStr)){
				try{
					primaryPaymentMethod = PaymentMethod.valueOf(primaryPaymentMethodStr);
				}catch(Exception e){
					error.setDescription("Please send valid Primary Payment Method");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please send valid Primary Payment Method");
					return orderConfirmation;
				}
			}
			
			PartialPaymentMethod secondaryPaymentMethod = null;
			PartialPaymentMethod thirdPaymentMethod=null;
			PaypalTransactionDetail paypalTransactionDetail = null;
			
			CustomerWalletHistory customerWalletHistory = null;
			CustomerWallet customerWallet = null;

			
			if(!isPaymentSuccess){
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getOrderLoyaltyInfoId());
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderLoyaltyHistoryId());
				
				if(null != orderRequest.getAffCashRewardId()){
					
					AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().get(orderRequest.getAffCashRewardId());
					AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().get(orderRequest.getAffCashRewardHistoryId());
					
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().get(orderRequest.getAffPromoTrackingId());
					
					affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash() - affiliateCashRewardHistory.getCreditedCash());
					affiliateCashReward.setLastUpdate(new Date());
					
					DAORegistry.getAffiliateCashRewardDAO().update(affiliateCashReward);
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().delete(affiliatePromoCodeTracking);
					DAORegistry.getAffiliateCashRewardHistoryDAO().delete(affiliateCashRewardHistory);
				}

				if(null != orderRequest.getRtfPromoTrackingId()){
					RTFPromotionalOfferTracking offerTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().get(orderRequest.getRtfPromoTrackingId());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().delete(offerTracking);
				}
				
				if(null != orderRequest.getRefCustLoyaltyHistoryId()){
					
					CustomerLoyalty refLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getRefCustLoyaltyInfoId());
					CustomerLoyaltyHistory refLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getRefCustLoyaltyHistoryId());
					ReferredCodeTracking referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().get(orderRequest.getRefTrackingId());
					
					refLoyalty.setPendingPointsAsDouble(refLoyalty.getPendingPointsAsDouble() - refLoyaltyHistory.getPointsEarnedAsDouble());
					refLoyalty.setLastUpdate(new Date());
					
					DAORegistry.getCustomerLoyaltyDAO().update(refLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().delete(refLoyaltyHistory);
					DAORegistry.getReferredCodeTrackingDAO().delete(referredCodeTracking);
				}
				
				try{
					DAORegistry.getCategoryTicketGroupDAO().revertSoldTicketToActive(customerOrder.getCategoryTicketGroupId());
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(null != orderRequest.getLockId() ){
					//remove the locked ticket information after it has been purchased by customer
					DAORegistry.getAutoCatsLockedTicketDAO().deleteByLockId(orderRequest.getLockId());
				}
				
				customerLoyalty.setPendingPointsAsDouble(customerLoyalty.getPendingPointsAsDouble() - customerLoyaltyHistory.getPointsEarnedAsDouble());
				if(null != customerOrder.getPrimaryPaymentMethod() && 
						(customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS) 
								|| customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
					customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()+ customerLoyaltyHistory.getPointsSpentAsDouble());
					customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble() - customerLoyaltyHistory.getPointsSpentAsDouble());
				}
				customerLoyalty.setLastUpdate(new Date());
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				DAORegistry.getCustomerLoyaltyHistoryDAO().delete(customerLoyaltyHistory);
				
				orderRequest.setStatus(OrderStatus.PAYMENT_FAILED);
				orderRequest.setUpdatedTime(new Date());
				
				customerWalletHistory = DAORegistry.getCustomerWalletHistoryDAO().getPendingDebitTransactionByOrderId(customerOrder.getId());
				
				if(null != customerWalletHistory){
					customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(orderRequest.getCustomerId()); 
					customerWallet.setActiveCredit(customerWallet.getActiveCredit()+customerWalletHistory.getTransactionAmount());
					customerWallet.setTotalUsedCredit(customerWallet.getTotalUsedCredit()-customerWalletHistory.getTransactionAmount());
					customerWallet.setLastUpdated(new Date());
					DAORegistry.getCustomerWalletDAO().update(customerWallet);
					DAORegistry.getCustomerWalletHistoryDAO().delete(customerWalletHistory);
				}
				
				DAORegistry.getCustomerOrderDAO().delete(customerOrder);
				DAORegistry.getCustomerOrderRequestDAO().update(orderRequest);
				
				
				DAORegistry.getOrderTicketGroupDAO().updateOrderTicketGroupDetails(orderRequest.getOrderId());
				
				
				orderConfirmation.setMessage("Order not confirmed due to invalid transaction.");
				orderConfirmation.setStatus(1);	
				return orderConfirmation;
			}else{
				
				List<OrderPaymentBreakup> orderPaymentBreakupList = new ArrayList<OrderPaymentBreakup>();
				OrderPaymentBreakup paymentBreakUp = null;
				
				if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS) || primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
					primaryTransactionId = customerOrder.getPrimaryTransactionId();
				}
				
				/*Ulaganathan : Order Payment breakups - Begins*/
				paymentBreakUp = new OrderPaymentBreakup();
				paymentBreakUp.setOrderId(orderId);
				paymentBreakUp.setPaymentAmount(customerOrder.getPrimaryPayAmtAsDouble());
				paymentBreakUp.setPaymentMethod(primaryPaymentMethod);
				paymentBreakUp.setTransactionId(primaryTransactionId);
				paymentBreakUp.setDoneBy("Customer");
				paymentBreakUp.setPaymentDate(new Date());
				paymentBreakUp.setOrderSequence(1);
				orderPaymentBreakupList.add(paymentBreakUp);
				/*Ulaganathan : Order Payment breakups - Ends*/
				
				
				if(primaryPaymentMethod.equals(PaymentMethod.CREDITCARD) ||
						primaryPaymentMethod.equals(PaymentMethod.GOOGLEPAY) ||
						primaryPaymentMethod.equals(PaymentMethod.IPAY) || 
						primaryPaymentMethod.equals(PaymentMethod.CREDITCARD) ){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.ACCOUNT_RECIVABLE)){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					Property property = DAORegistry.getPropertyDAO().get("rtf.acct.receivable.auto.payid");
					Long primaryTrxId =  Long.valueOf(property.getValue());
					primaryTrxId++;
					primaryTransactionId = PaginationUtil.acctReceivablePaymentPrefix+primaryTrxId;
					property.setValue(String.valueOf(primaryTrxId));
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.PAYPAL)){
					
					if(TextUtil.isEmptyOrNull(primaryTransactionId)){
						error.setDescription("Primary Transaction Id is mandatory");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Primary Transaction Id is mandatory");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(primaryTransactionId);
					orderRequest.setPrimaryTransactionId(primaryTransactionId);
					
					paypalTransactionDetail = new PaypalTransactionDetail();
					paypalTransactionDetail.setPaymentId(primaryTransactionId);
					try{
						paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.CUSTOMER_WALLET)){

					if(TextUtil.isEmptyOrNull(walletTransactionIdStr)){
						error.setDescription("Wallet Transaction Id is mandatory.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Wallet Transaction Id is mandatory.");
						return orderConfirmation;
					}

					customerWalletHistory = DAORegistry.getCustomerWalletHistoryDAO().getPendingDebitTransactionById(Integer.parseInt(walletTransactionIdStr.trim()));
					customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(orderRequest.getCustomerId());

					if(null == customerWalletHistory){
						error.setDescription("Your wallet transaction is not valid.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Your wallet transaction is not valid.");
						return orderConfirmation;
					}
					
					customerOrder.setPrimaryTransactionId(customerWalletHistory.getTransactionId());
					orderRequest.setPrimaryTransactionId( customerWalletHistory.getTransactionId());
					
					orderPaymentBreakupList.get(0).setTransactionId(customerWalletHistory.getTransactionId());
					
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					if(null != secondaryPaymentMethod && secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(secondaryTransactionId);
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
					}
					
				}else if(primaryPaymentMethod.equals(PaymentMethod.FULL_REWARDS)){
					//Nothing to be written over here. Transaction ID Will be generated While Creating Dummy Order
					
					/*Ulaganathan : Order Payment breakups - Begins*/
					paymentBreakUp = new OrderPaymentBreakup();
					paymentBreakUp.setOrderId(orderId);
					paymentBreakUp.setPaymentAmount(customerOrder.getPrimaryPayAmtAsDouble());
					paymentBreakUp.setPaymentMethod(primaryPaymentMethod);
					paymentBreakUp.setTransactionId(customerOrder.getPrimaryTransactionId());
					paymentBreakUp.setDoneBy("Customer");
					paymentBreakUp.setPaymentDate(new Date());
					paymentBreakUp.setOrderSequence(1);
					orderPaymentBreakupList.add(paymentBreakUp);
					/*Ulaganathan : Order Payment breakups - Ends*/
				}else{
					
					paymentBreakUp.setTransactionId(customerOrder.getPrimaryTransactionId());
					
					//Secondary PaymentMethod can be CREDITCARD,PAYPAL,IPAY,GOOGLEPAY
					if(TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						error.setDescription("Please enter a valid Secondary Payment Method");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please enter a valid Secondary Payment Method");
						return orderConfirmation;
					}
					if(!TextUtil.isEmptyOrNull(secondaryPaymentMethodStr)){
						try{
							secondaryPaymentMethod = PartialPaymentMethod.valueOf(secondaryPaymentMethodStr);
						}catch(Exception e){
							error.setDescription("Please enter a valid Secondary Payment Method");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Please enter a valid Secondary Payment Method");
							return orderConfirmation;
						}
					}
					
					
					if(secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD) ||
							secondaryPaymentMethod.equals(PartialPaymentMethod.GOOGLEPAY) ||
							secondaryPaymentMethod.equals(PartialPaymentMethod.IPAY) || 
							secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD) ){
						
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(secondaryTransactionId);
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
						
					}else if(secondaryPaymentMethod.equals(PartialPaymentMethod.PAYPAL)){
						
						if(TextUtil.isEmptyOrNull(secondaryTransactionId)){
							error.setDescription("Secondary Transaction Id is Mandatory");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Secondary Transaction Id is Mandatory");
							return orderConfirmation;
						}
						
						customerOrder.setSecondaryTransactionId(secondaryTransactionId);
						orderRequest.setSecondaryTransactionId(secondaryTransactionId);
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(secondaryTransactionId);
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
						
						
						paypalTransactionDetail = new PaypalTransactionDetail();
						paypalTransactionDetail.setPaymentId(secondaryTransactionId);
						try{
							paypalTransactionDetail = payPalCheckout.getPaymentInformation(paypalTransactionDetail);	
						}catch(Exception e){
							e.printStackTrace();
						}
					}else if (secondaryPaymentMethod.equals(PartialPaymentMethod.CUSTOMER_WALLET)){
						if(TextUtil.isEmptyOrNull(walletTransactionIdStr)){
							error.setDescription("Wallet Transaction Id is mandatory.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Wallet Transaction Id is mandatory.");
							return orderConfirmation;
						}

						customerWalletHistory = DAORegistry.getCustomerWalletHistoryDAO().getPendingDebitTransactionById(Integer.parseInt(walletTransactionIdStr.trim()));
						customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(orderRequest.getCustomerId());

						if(null == customerWalletHistory){
							error.setDescription("Your wallet transaction is not valid.");
							orderConfirmation.setError(error);
							orderConfirmation.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Your wallet transaction is not valid.");
							return orderConfirmation;
						}
						customerOrder.setSecondaryTransactionId(customerWalletHistory.getTransactionId());
						orderRequest.setSecondaryTransactionId( customerWalletHistory.getTransactionId());
						
						/*Ulaganathan : Order Payment breakups - Begins*/
						paymentBreakUp = new OrderPaymentBreakup();
						paymentBreakUp.setOrderId(orderId);
						paymentBreakUp.setPaymentAmount(customerOrder.getSecondaryPayAmtAsDouble());
						paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(secondaryPaymentMethodStr));
						paymentBreakUp.setTransactionId(customerWalletHistory.getTransactionId());
						paymentBreakUp.setDoneBy("Customer");
						paymentBreakUp.setPaymentDate(new Date());
						paymentBreakUp.setOrderSequence(2);
						orderPaymentBreakupList.add(paymentBreakUp);
						/*Ulaganathan : Order Payment breakups - Ends*/
						
						if(!TextUtil.isEmptyOrNull(thirdPaymentMethodStr)){
							try{
								thirdPaymentMethod = PartialPaymentMethod.valueOf(thirdPaymentMethodStr);
							}catch(Exception e){
								error.setDescription("Please enter a valid Third Payment Method");
								orderConfirmation.setError(error);
								orderConfirmation.setStatus(0);
								TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"Please enter a valid Third Payment Method");
								return orderConfirmation;
							}
						}
						
						if(null != thirdPaymentMethod && thirdPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)){
							customerOrder.setThirdTransactionId(thirdTransactionId);
							orderRequest.setThirdTransactionId(thirdTransactionId);
							
							/*Ulaganathan : Order Payment breakups - Begins*/
							paymentBreakUp = new OrderPaymentBreakup();
							paymentBreakUp.setOrderId(orderId);
							paymentBreakUp.setPaymentAmount(customerOrder.getThirdPayAmtAsDouble());
							paymentBreakUp.setPaymentMethod(PaymentMethod.valueOf(thirdPaymentMethodStr));
							paymentBreakUp.setTransactionId(thirdTransactionId);
							paymentBreakUp.setDoneBy("Customer");
							paymentBreakUp.setPaymentDate(new Date());
							paymentBreakUp.setOrderSequence(3);
							orderPaymentBreakupList.add(paymentBreakUp);
							/*Ulaganathan : Order Payment breakups - Ends*/
						}
							
					}
				}
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getOrderLoyaltyInfoId());
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderLoyaltyHistoryId());
				boolean isPromoApplied = false;
				boolean showDiscountPriceArea = false;
				String promoCode = "";
				if(null != orderRequest.getAffCashRewardId()){
					AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().get(orderRequest.getAffCashRewardId());
					AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().get(orderRequest.getAffCashRewardHistoryId());
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().get(orderRequest.getAffPromoTrackingId());
					
					affiliateCashRewardHistory.setRewardStatus(RewardStatus.PENDING);
					affiliateCashRewardHistory.setUpdatedDate(new Date());
					
					affiliatePromoCodeTracking.setStatus("COMPLETED");
					affiliatePromoCodeTracking.setUpdatedDate(new Date());
					
					showDiscountPriceArea = false;
					isPromoApplied = true;
					promoCode = affiliatePromoCodeTracking.getPromoCode();
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().update(affiliatePromoCodeTracking);
					DAORegistry.getAffiliateCashRewardHistoryDAO().update(affiliateCashRewardHistory);
				}
				
				if(null != orderRequest.getRtfPromoTrackingId()){
					RTFPromotionalOfferTracking offerTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().get(orderRequest.getRtfPromoTrackingId());
					offerTracking.setStatus("COMPLETED");
					offerTracking.setUpdatedDate(new Date());
					if(null != offerTracking.getOfferType() && offerTracking.getOfferType().equals(PromotionalType.CUSTOMER_PROMO)){
						DAORegistry.getRtfCustomerPromotionalOfferDAO().updatePromotionaOfferOrderCount(offerTracking.getPromotionalOfferId());
					}else if(offerTracking.getOfferType().equals(PromotionalType.NORMAL_PROMO)){
						DAORegistry.getRtfPromotionalOfferHdrDAO().updatePromotionaOfferOrderCount(offerTracking.getPromotionalOfferId());
					}
					
					showDiscountPriceArea = true;
					isPromoApplied = true;
					promoCode = offerTracking.getPromoCode();
					
					DAORegistry.getRtfPromotionalOfferTrackingDAO().update(offerTracking);
				}
				
				if(null != orderRequest.getRefCustLoyaltyHistoryId()){
					
					CustomerLoyalty refLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getRefCustLoyaltyInfoId());
					CustomerLoyaltyHistory refLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getRefCustLoyaltyHistoryId());
					ReferredCodeTracking referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().get(orderRequest.getRefTrackingId());
					
					refLoyaltyHistory.setUpdatedDate(new Date());
					refLoyaltyHistory.setRewardStatus(RewardStatus.PENDING);
					DAORegistry.getCustomerLoyaltyHistoryDAO().update(refLoyaltyHistory);
				}
				
				
				Invoice invoice = null;
				if(customerOrder != null){
					invoice = new Invoice();
					invoice.setCustomerOrderId(customerOrder.getId());
					invoice.setInvoiceTotal(customerOrder.getOrderTotalAsDouble());
					invoice.setTicketCount(customerOrder.getQty());
					invoice.setCustomerId(orderRequest.getCustomerId());
					invoice.setExtPONo(null);
					invoice.setStatus(InvoiceStatus.Outstanding);
					invoice.setInvoiceType("INVOICE");
					invoice.setCreatedDate(new Date());
					invoice.setCreatedBy(customerOrder.getCustomer().getCustomerName());
					invoice.setIsSent(false);
					invoice.setSentDate(null);
					invoice.setRealTixMap("NO");
					invoice.setBrokerId(customerOrder.getBrokerId());
					//Persisting the customer order details in invoice
					DAORegistry.getInvoiceDAO().save(invoice);
				}
				
				orderRequest.setInvoiceId(invoice.getId());
				
				/*try{
					DAORegistry.getCategoryTicketDAO().updateCategoryTicket(customerOrder.getCategoryTicketGroupId(), 
							invoice.getId(), orderRequest.getSoldPrice());
					DAORegistry.getCategoryTicketGroupDAO().updateTicketAsSold(customerOrder.getCategoryTicketGroupId(), invoice.getId() );
				}catch(Exception e){
					e.printStackTrace();
				}*/
				
				/*if(null != orderRequest.getLockId() ){
					//remove the locked ticket information after it has been purchased by customer
					DAORegistry.getAutoCatsLockedTicketDAO().deleteByLockId(orderRequest.getLockId());
				}*/
				
				
				if(null != paypalTransactionDetail && null != paypalTransactionDetail.getPaymentId()  
						&& !paypalTransactionDetail.getPaymentId().isEmpty()){
					paypalTransactionDetail.setOrderId(customerOrder.getId());
					DAORegistry.getPaypalTransactionDetailDAO().saveOrUpdate(paypalTransactionDetail);
					
					PaypalTracking paypalTracking = DAORegistry.getPaypalTrackingDAO().getTrackingByCustomerIdByTicketId(orderRequest.getCustomerId(), 
							customerOrder.getEventId(), customerOrder.getCategoryTicketGroupId(), sessionId, applicationPlatForm);
					
					if(null != paypalTracking){
						paypalTracking.setLastUpdated(new Date());
						paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
						paypalTracking.setTransactionId(paypalTransactionDetail.getId());
						paypalTracking.setStatus("COMPLETED");
					}else{
						paypalTracking = new PaypalTracking();
						paypalTracking.setCatTicketGroupId(customerOrder.getCategoryTicketGroupId());
						paypalTracking.setCreatedDate(new Date());
						paypalTracking.setCustomerId(orderRequest.getCustomerId());
						paypalTracking.setEventId(customerOrder.getEventId());
						paypalTracking.setLastUpdated(new Date());
						paypalTracking.setOrderType(customerOrder.getOrderType());
						paypalTracking.setPaypalTransactionId(paypalTransactionDetail.getPaymentId());
						paypalTracking.setPlatform(applicationPlatForm);
						paypalTracking.setQty(customerOrder.getQty());
						paypalTracking.setStatus("COMPLETED");
						paypalTracking.setTransactionId(paypalTransactionDetail.getId());
						paypalTracking.setOrderTotal(customerOrder.getOrderTotalAsDouble());
						paypalTracking.setZone(customerOrder.getSection());
					}
					DAORegistry.getPaypalTrackingDAO().saveOrUpdate(paypalTracking);
				}
				
				customerLoyaltyHistory.setRewardStatus(RewardStatus.PENDING);
				customerLoyaltyHistory.setUpdatedDate(new Date());
				DAORegistry.getCustomerLoyaltyHistoryDAO().update(customerLoyaltyHistory);
				
				if(null !=customerLoyaltyHistory.getPointsSpentAsDouble() && 
						customerLoyaltyHistory.getPointsSpentAsDouble() > 0){
					customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble() + customerLoyaltyHistory.getPointsSpentAsDouble());
					customerLoyalty.setLatestSpentPointsAsDouble(customerLoyaltyHistory.getPointsSpentAsDouble());
					DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				}
				
				
				if(customerWallet != null){
					customerWallet.setPendingDebit(0.00);
					customerWallet.setLastUpdated(new Date());
					customerWalletHistory.setOrderId(customerOrder.getId());
					customerWalletHistory.setTransactionStatus("APPROVED");
					customerWalletHistory.setUpdatedOn(new Date());
					DAORegistry.getCustomerWalletDAO().saveOrUpdate(customerWallet);
					DAORegistry.getCustomerWalletHistoryDAO().saveOrUpdate(customerWalletHistory);
				}
				
				orderRequest.setStatus(OrderStatus.ACTIVE);
				orderRequest.setUpdatedTime(new Date());
				
				customerOrder.setIsInvoiceSent(false);
				customerOrder.setStatus(OrderStatus.ACTIVE);
				customerOrder.setLastUpdatedDateTemp(new Date());
				
				customerOrder.setIsPromoCodeApplied(isPromoApplied);
				customerOrder.setShowDiscPriceArea(showDiscountPriceArea);
				customerOrder.setPromotionalCode(promoCode);
				customerOrder.setNormalTixPrice(orderRequest.getTicketPrice());
				customerOrder.setDiscountedTixPrice(orderRequest.getSoldPrice());
				
				DAORegistry.getCustomerOrderDAO().update(customerOrder);
				DAORegistry.getCustomerOrderRequestDAO().update(orderRequest);
				
				try{
					if(null == customerOrder.getCustomer().getEligibleToShareRefCode() || !customerOrder.getCustomer().getEligibleToShareRefCode()){
						DAORegistry.getCustomerDAO().updateCustomerForTicketPurchase(orderRequest.getCustomerId());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				if(orderRequest.getIsLoyalFan()){
					CustomerSuperFan loyalFan =  DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(orderRequest.getCustomerId());
					
					try{
						if(null != loyalFan && (null == loyalFan.getIsEmailSent() || !loyalFan.getIsEmailSent())){
							Date startdate = loyalFan.getStartDate();
							Calendar calendar = new GregorianCalendar();
							calendar.setTime(startdate);
							calendar.add(Calendar.YEAR,1);
							startdate = calendar.getTime();
							String formattedendDate = dateTimeFormat.format(startdate);
							
							Customer customer = customerOrder.getCustomer();
							Map<String,Object> mailMap = new HashMap<String,Object>();
							mailMap.put("customer", customerOrder.getCustomer());  
							
							String message = TextUtil.getSportsLoyalFanEmailBody(loyalFan.getLoyalFanName());
							
							if(!loyalFan.getLoyalFanType().equals(LoyalFanType.SPORTS)){
								message = TextUtil.getNonSportsLoyalFanEmailBody(loyalFan.getLoyalFanName(), String.valueOf(loyalFan.getLoyalFanType()),loyalFan.getCountry());
							}
							mailMap.put("message", message); 
							mailMap.put("startdate", formattedendDate);
							
							MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
					        mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(),  null,URLUtil.bccEmails, 
					          "Reward The Fan: LOYAL FAN ",
					          "mail-loyal-fan.html", mailMap, "text/html", null,mailAttachment,null);
					        loyalFan.setIsEmailSent(true);
					        
					        DAORegistry.getCustomerSuperFanDAO().saveOrUpdate(loyalFan);
						}
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}
				
			}
			orderConfirmation.setOrderId(customerOrder.getId());
			orderConfirmation.setMessage("Your Order was successfully created!");
			orderConfirmation.setStatus(1);	
			return orderConfirmation;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Opps somthing went wrong. Order was not Confirmend. Please Contact Administrator");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CONFIRMORDER,"Opps somthing went wrong. Order was not Confirmend. Please Contact Administrator");
			return orderConfirmation;
		}
	}
	
	@RequestMapping(value = "/ViewRTFOrder",method = RequestMethod.POST)
	public @ResponsePayload CustomerOrderResponse viewRTFOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerOrderResponse customerOrderResponse = new CustomerOrderResponse();
		Error error = new Error();
		
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
				return customerOrderResponse;
			}
//			Integer configId= null;
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerOrderResponse.setError(error);
						customerOrderResponse.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
						return customerOrderResponse;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
					return customerOrderResponse;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
				return customerOrderResponse;
			}
			String productTypeStr = request.getParameter("productType");
			String orderIdStr = request.getParameter("orderNo");
			String customerIdStr = request.getParameter("customerId");
			String platForm = request.getParameter("platForm");
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Product Type is mandatory");
				return customerOrderResponse;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Please send valid product type");
					return customerOrderResponse;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is Mandatory");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Order No is Mandatory");
				return customerOrderResponse;
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is Mandatory");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Customer Id is Mandatory");
				return customerOrderResponse;
			}
			
			Integer customerId=null,orderId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Please send valid Customer Id");
					return customerOrderResponse;
				}
				
				/* Share Link Validation for Share Icon - Added By Ulaganathan*/
				customerOrderResponse.setEligibleToShareRefCode(true);
				customerOrderResponse.setShareErrorMessage("");
				
				try{
					CustomerUtil.updatedCustomerUtil(customer);
				}catch(Exception e){
					e.printStackTrace();
				}
				/* Share Link Validation for Share Icon - Added By Ulaganathan*/
				
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Please send valid Customer Id");
				return customerOrderResponse;
			}
			
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
			}catch(Exception e){
				error.setDescription("The Order Number you entered is invalid. Please try again");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"The Order Number you entered is invalid. Please try again");
				return customerOrderResponse;
			}
			
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByOrderId(orderId);
			if(customerOrder == null){
				error.setDescription("The Order Number you entered is invalid. Please try again");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"The Order Number you entered is invalid. Please try again");
				return customerOrderResponse;
			}
			
			Boolean showLongInfo= false;
			boolean isLongSale = false;
			List<OrderTicketGroup> orderTicketGroups = null;
			if(null != customerOrder.getIsLongSale() && customerOrder.getIsLongSale()){
				orderTicketGroups = DAORegistry.getOrderTicketGroupDAO().getAllTicketGroupsByOrderId(orderId);
				showLongInfo= true;
				isLongSale = true;
			}else if(null == customerOrder.getIsLongSale() || !customerOrder.getIsLongSale() ){
				orderTicketGroups = DAORegistry.getOrderTicketGroupDAO().getAllTicketGroupsByOrderId(orderId);
				if(null != orderTicketGroups && !orderTicketGroups.isEmpty()){
					showLongInfo= true;
				}
			}
			customerOrder.setIsOrderFullFilled(showLongInfo);
			
			 //Adding the code to show the svgMapWebViewUrl in response
			 String svgZone = customerOrder.getSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
			 
			if(showLongInfo){
				customerOrder.setSectionDescription("<font><b>------</b></font>");
				customerOrder.setIsOrderFullFilled(true);
				String zone = "", section="",row="",seat="";
				Double ticketPrice = 0.00;
				int i =0;
				for (OrderTicketGroup orderTicketGroup : orderTicketGroups) {
					if(i == 0 ){
						zone = orderTicketGroup.getZone();
						section= orderTicketGroup.getSection();
						row= orderTicketGroup.getRow();
						seat= orderTicketGroup.getSeat();
					}else{
						zone = zone +","+orderTicketGroup.getZone();
						section= section +","+orderTicketGroup.getSection();
						row= row +","+orderTicketGroup.getRow();
						seat= seat +","+orderTicketGroup.getSeat();
					}
					ticketPrice = ticketPrice + TicketUtil.getRoundedValue(orderTicketGroup.getSoldPrice() * orderTicketGroup.getQuantity());
					if(isLongSale){
						if(null != orderTicketGroup.getZone() && !orderTicketGroup.getZone().equals("--")){
							svgZone =  svgZone +","+orderTicketGroup.getZone().toUpperCase().replace("ZONE", "").replaceAll(" +","");
						}
					}
					
					i++;
				}
				customerOrder.setZone(zone);
				customerOrder.setRealSection(section);
				customerOrder.setRealRow(row);
				customerOrder.setRealSeat(seat);
				if(null != customerOrder.getIsLongSale() && customerOrder.getIsLongSale()){
					customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(ticketPrice));
				}else{
					customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(customerOrder.getTicketSoldPriceAsDouble() * customerOrder.getQty()));
				}
			} else{
				customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(customerOrder.getTicketSoldPriceAsDouble() * customerOrder.getQty()));
			}
			Double totalTax = customerOrder.getTaxesAsDouble();
			Double singleTixFees = TicketUtil.getRoundedValue(totalTax / customerOrder.getQty());
			customerOrder.setTotalServiceFees(TicketUtil.getRoundedValueString(totalTax));
			customerOrder.setSingleTixServiceFees(TicketUtil.getRoundedValueString(singleTixFees));
			
			Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByCustomerOrder(customerOrder.getId(), customerId);
			String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			customerOrder.setViewOrderType(String.valueOf(customerOrder.getOrderType()));
			
			if(null != customerOrder.getSecondaryOrdertype() && customerOrder.getSecondaryOrdertype().equals(SecondaryOrderType.CROWNJEWEL)){
				customerOrder.setShowCJEOrder(true);
				customerOrder.setCjeOrderInfo("Fantasy Order No : "+customerOrder.getSecondaryOrderId());
				customerOrder.setCjeOrderNo(customerOrder.getSecondaryOrderId());
				customerOrder.setViewOrderType(String.valueOf(customerOrder.getSecondaryOrdertype()));
			}
			String orderDeliveryNote= null;
			String isRealTixMap = null;
			if(null == invoice || invoice.getStatus().equals(InvoiceStatus.Voided)){
				orderDeliveryNote = "Order Cancelled";
				customerOrder.setButtonOption("OC");
				customerOrder.setOrderDeliveryNote(orderDeliveryNote);
				customerOrder.setDeliveryInfo(null);
				
			}else{
				
				isRealTixMap = invoice.getRealTixMap();
				if(isRealTixMap.equals("Yes") || isRealTixMap.equals("YES")){
					customerOrder.setShowInfoButton(Boolean.FALSE);
					if(invoice.getShippingMethodId()!=null &&!invoice.getShippingMethodId().equals(3)){
						orderDeliveryNote = "Download Tickets";
						customerOrder.setButtonOption("DL");
				
						List<InvoiceTicketAttachment> invoiceTicketAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
						
						List<InvoiceTicketAttachment> finalTicketAttachments = null;
						
						if(invoiceTicketAttachmentList!=null){
							
							finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
							
							for (InvoiceTicketAttachment tixAttachemnt : invoiceTicketAttachmentList) {
								try {
									if(URLUtil.isSharedDriveOn) {
										SmbFile smbFile = ShareDriveUtil.getSmbFile(URLUtil.findETicketDownloadURLFromDirectory(tixAttachemnt.getFilePath()));
										if(null != smbFile && smbFile.exists()){
											tixAttachemnt.setDownloadUrl(URLUtil.getETicketDownloadUrl(request, customerOrder, configIdString,
													customer.getId(),invoice,tixAttachemnt.getId(),applicationPlatForm,ip));
											finalTicketAttachments.add(tixAttachemnt);
										}
									}else {
										File file = new File (URLUtil.findETicketDownloadURLFromDirectory(tixAttachemnt.getFilePath()));
										if(null != file && file.exists()){
											tixAttachemnt.setDownloadUrl(URLUtil.getETicketDownloadUrl(request, customerOrder, configIdString,
													customer.getId(),invoice,tixAttachemnt.getId(),applicationPlatForm,ip));
											finalTicketAttachments.add(tixAttachemnt);
										}
									}
									
									tixAttachemnt.setExtension(URLUtil.getFileExtension(tixAttachemnt.getFilePath()));
									tixAttachemnt.setFileName(URLUtil.getFileNameForTriviaTickets(tixAttachemnt.getFilePath()));
									
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							
							try{
								Collections.sort(finalTicketAttachments, TicketUtil.ticketAttachmentsByPosition);
							}catch(Exception e){
								e.printStackTrace();
							}
						}else{
							finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
						}
						
						
						if(null != finalTicketAttachments && !finalTicketAttachments.isEmpty() && finalTicketAttachments.size() >0){
							orderDeliveryNote = "Download Tickets";
							customerOrder.setOrderDeliveryNote(orderDeliveryNote);
							customerOrder.setButtonOption("DL");
							customerOrder.setInvoiceTicketAttachment(finalTicketAttachments);
							customerOrder.setShippingMethod("E-Ticket");
							customerOrder.setDeliveryInfo(null);
						}else{
							
							Date eventDate = customerOrder.getEventDateTemp();
							Date curDate =  new Date();
							
							long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
							
							if(noOfDays < 0){
								orderDeliveryNote = "No Ticket Attached";
								customerOrder.setButtonOption("OC");
								//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
								//order.setShippingMethod("E-Ticket");
								customerOrder.setOrderDeliveryNote(orderDeliveryNote);
								customerOrder.setDeliveryInfo(null);
							}else{
								customerOrder.setButtonOption("NOBUTTON");
								customerOrder.setTicketDownloadUrl(null);
								customerOrder.setShowInfoButton(Boolean.TRUE);
								TicketUtil.getTicketDeliveryInFo(customerOrder,fontSizeStr);
								TicketUtil.getUnfilledTicketOrderDeliveryNote(customerOrder,fontSizeStr);
								customerOrder.setShippingMethod("E-Ticket");
								customerOrder.setDeliveryInfo(null);
							}
							
						}
						
						/*customerOrder.setInvoiceTicketAttachment(invoiceTicketAttachmentList);
						//customerOrder.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, customerOrder, configIdString,customer.getId(),invoice,invoiceTicketAttachment));
						customerOrder.setDeliveryInfo("");
						customerOrder.setShippingMethod("E-Ticket");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);*/
					}else if(invoice.getShippingMethodId()!=null && invoice.getShippingMethodId().equals(3)){
						String fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
						orderDeliveryNote = "FedEx : "+fedExTrackingNo;
						customerOrder.setButtonOption("FEDEX");
						customerOrder.setTicketDownloadUrl(URLUtil.getFedExTrackingURL(invoice));
						customerOrder.setDeliveryInfo("");
						customerOrder.setShippingMethod("FedEx");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);
					}
				}else{
					
					Date eventDate = customerOrder.getEventDateTemp();
					Date curDate =  new Date();
					long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
					Date tmpDate  = dateFormat.parse("01/01/3000");
					Boolean isNullEventFlag = false;
					if(eventDate.compareTo(tmpDate) == 0) {
						isNullEventFlag = true;
						customerOrder.setEventDate(null);
						customerOrder.setEventDateTemp(null);
						customerOrder.setEventTime(null);
					}
					
					if(noOfDays < 0 && !isNullEventFlag){
						orderDeliveryNote = "Order Cancelled";
						customerOrder.setButtonOption("OC");
						//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
						//order.setShippingMethod("E-Ticket");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);
						customerOrder.setDeliveryInfo(null);
					}else{
						customerOrder.setButtonOption("NOBUTTON");
						customerOrder.setTicketDownloadUrl(null);
						customerOrder.setShowInfoButton(Boolean.TRUE);
						TicketUtil.getTicketDeliveryInFo(customerOrder,fontSizeStr);
						TicketUtil.getUnfilledTicketOrderDeliveryNote(customerOrder,fontSizeStr);
					}
					
				}
			}
			
			
			 Collection<ZoneRGBColor> zoneRgbColors =DAORegistry.getZoneRGBColorDAO().getAll();
			 Map<String, ZoneRGBColor> rgbColorMap = new HashMap<String, ZoneRGBColor>();
			 for (ZoneRGBColor zoneRGBColor : zoneRgbColors) {
				if(ProductType.PRESALEZONETICKETS.equals(productType)){
					rgbColorMap.put(zoneRGBColor.getZone().replaceAll("_","").toUpperCase(), zoneRGBColor);
				}else{
					rgbColorMap.put(zoneRGBColor.getZone().replaceAll(" +","").toUpperCase(), zoneRGBColor);
				}
			  }
			 
			ZoneRGBColor zoneRGBColor = rgbColorMap.get(svgZone);
			if(null != zoneRGBColor){
				customerOrder.setColorCode(zoneRGBColor.getColor());
			}else{
				customerOrder.setColorCode("#F4ED6D");
			}
			
			String svgWebViewUrl = "";
			
			if(null != customerOrder.getOrderType() && customerOrder.getOrderType().equals(OrderType.CONTEST)){
				if(customerOrder.getSection().equals("TBD")){
					
					List<String> zoneList =  DAORegistry.getQueryManagerDAO().getAllZonesByVenueCategoryId(customerOrder.getVenueCategory());
					if(null != zoneList && !zoneList.isEmpty()){
						for (String zone : zoneList) {
							svgZone = svgZone +","+ zone.toUpperCase().replace("ZONE", "").replaceAll(" +","");
						}
						svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,null,true);
					}else{
						svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,null,false);
					}
				}else{
					svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,null,false);
				}
				
			}else{
				svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,customerOrder.getId(),false);
			}
			
			/*if(applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				customerOrder.setIsIOS(true);
			}else {
				customerOrder.setIsIOS(false);
			}*/
			
			customerOrder.setSvgKey(svgZone.toLowerCase());
			 
			MapUtil.getOrderSvgTextDetails(customerOrder, applicationPlatForm);
			 
			//it will set customer loyalty,customer detail and customer card info.
			customerOrder.setAllCustomerRelatedObjects();
			customerOrder.setRewardPoints("<font color=#012e4f>You have just earned <b>"+customerOrder.getCustomerLoyaltyHistory().getPointsEarned()+" reward dollars</b> from this purchase.</font>");
			
			List<CustomerOrder> customerOrderList = new ArrayList<CustomerOrder>();
			customerOrderList.add(customerOrder);
			customerOrderResponse.setCustomerOrders(customerOrderList);
			customerOrderResponse.setStatus(1);
			customerOrderResponse.setSvgWebViewUrl(svgWebViewUrl);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Success");
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Creating Customer Order.");
			customerOrderResponse.setError(error);
			customerOrderResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Error occured while Creating Customer Order");
			return customerOrderResponse;
		}
		return customerOrderResponse;
	}
	
	@RequestMapping(value = "/OrderPaymentCapture",method = RequestMethod.POST)
	public @ResponsePayload OrderPaymentResponse orderPayment(HttpServletRequest request,HttpServletResponse response,Model model){
		OrderPaymentResponse orderConfirmation = new OrderPaymentResponse();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"You are not authorized");
				return orderConfirmation;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"You are not authorized");
						return orderConfirmation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"You are not authorized");
					return orderConfirmation;
				}
			}else{
				error.setDescription("You are not authorized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"You are not authorized");
				return orderConfirmation;
			}
			String productTypeStr = request.getParameter("productType");
			String appPlatformStr = request.getParameter("platForm");
			String sessionId = request.getParameter("sessionId");
			String customerIdStr = request.getParameter("customerId");	
			String orderIdStr = request.getParameter("orderId");
			String paymentMethodStr = request.getParameter("paymentMethod");
			String transactionId = request.getParameter("transactionId");
			String transactionAmountStr = request.getParameter("transactionAmount");
			String tfUserName = request.getParameter("tfUserName");
			String ipAddress = request.getParameter("clientIPAddress");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please send valid Customer Id");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Please send valid Customer Id");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Product Type is Mandatory");
				return orderConfirmation;
			}
						
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Please send valid product type");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(appPlatformStr)){
				error.setDescription("Application Platform is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Application Platform is Mandatory");
				return orderConfirmation;
			}
						
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(appPlatformStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(appPlatformStr);
				}catch(Exception e){
					error.setDescription("Please send valid application platform");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Please send valid application platform");
					return orderConfirmation;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Order No is not valid");
				return orderConfirmation;
			}
			
			Integer orderId = null;
			CustomerOrder customerOrder = null;
			CustomerOrderRequest orderRequest = null;
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
				customerOrder = DAORegistry.getCustomerOrderDAO().get(orderId);
				if(null == customerOrder ){
					error.setDescription("Order No is not valid");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Order No is not valid");
					return orderConfirmation;
				}
			}catch(Exception e){
				error.setDescription("Order No is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Order No is not valid");
				return orderConfirmation;
			}
			
			if(TextUtil.isEmptyOrNull(paymentMethodStr)){
				error.setDescription("Payment method is Mandatory");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Payment method is Mandatory");
				return orderConfirmation;
			}
						
			PaymentMethod paymentMethod=null;
			try{
				paymentMethod = PaymentMethod.valueOf(paymentMethodStr);
			}catch(Exception e){
				error.setDescription("Please send valid payment method");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Please send valid payment method");
				return orderConfirmation;
			}
			if(TextUtil.isEmptyOrNull(transactionId)){
				error.setDescription("Transaction ID is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Transaction ID is not valid");
				return orderConfirmation;
			}
			if(TextUtil.isEmptyOrNull(transactionAmountStr)){
				error.setDescription("Transaction Amount is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Transaction Amount is not valid");
				return orderConfirmation;
			}
			Double trxAmount = 0.00;
			try{
				trxAmount = Double.valueOf(transactionAmountStr.trim());
			}catch(Exception e){
				error.setDescription("Transaction Amount is not valid");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Transaction Amount is not valid");
				return orderConfirmation;
			}
			
			List<OrderPaymentBreakup> paymentBreakupList = DAORegistry.getOrderPaymentBreakupDAO().getPaymentBreaksByOrderId(orderId);
			Integer lastSeq = 1;
			for (OrderPaymentBreakup obj : paymentBreakupList) {
				if(obj.getOrderSequence() > lastSeq){
					lastSeq = obj.getOrderSequence();
				}
			}
			
			OrderPaymentBreakup paymentBreakUp = new OrderPaymentBreakup();
			paymentBreakUp.setOrderId(orderId);
			paymentBreakUp.setPaymentAmount(customerOrder.getPrimaryPayAmtAsDouble());
			paymentBreakUp.setPaymentMethod(paymentMethod);
			paymentBreakUp.setTransactionId(transactionId);
			paymentBreakUp.setDoneBy(tfUserName);
			paymentBreakUp.setPaymentDate(new Date());
			paymentBreakUp.setOrderSequence(lastSeq + 1);
			DAORegistry.getOrderPaymentBreakupDAO().save(paymentBreakUp);
			
			paymentBreakupList.add(paymentBreakUp);
			
			orderConfirmation.setStatus(1);
			orderConfirmation.setMessage("Payment captured successfully!");
			orderConfirmation.setOrderId(orderId);
			orderConfirmation.setOrderPaymentBreakupList(paymentBreakupList);
			return orderConfirmation;
		}catch(Exception e){
			error.setDescription("Something went wrong while capturing payment!");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.ORDERPAYMENT,"Something went wrong while capturing payment!");
			return orderConfirmation;
		}
	}
	
	
	@RequestMapping(value = "/ListOrders",method = RequestMethod.POST)
	public @ResponsePayload CustomerOrdersResponse getCustomerOrders(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerOrdersResponse customerOrderResponse = new CustomerOrdersResponse();
		Error error = new Error();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			/*if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"You are not authorized");
				return customerOrderResponse;
			}
			
//			Integer configId= null;
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerOrderResponse.setError(error);
						customerOrderResponse.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"You are not authorized");
						return customerOrderResponse;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"You are not authorized");
					return customerOrderResponse;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"You are not authorized");
				return customerOrderResponse;
			}*/
			
			String customerIdStr = request.getParameter("customerId");
			String productTypeStr = request.getParameter("productType");
			String orderTypeStr = request.getParameter("orderType");
			String orderFetchTypeStr = request.getParameter("orderFetchType");
			String platForm = request.getParameter("platForm");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Product Type is mandatory");
				return customerOrderResponse;
			}
			
			ProductType productType=null;
			try{
				productType = ProductType.valueOf(productTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid product type");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Please send valid product type");
				return customerOrderResponse;
			}
			
			if(TextUtil.isEmptyOrNull(orderFetchTypeStr)){
				error.setDescription("Order Fetching Type is mandatory");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Order Fetching Type is mandatory");
				return customerOrderResponse;
			}
			
			OrderFetchType orderFetchType=null;
			try{
				orderFetchType = OrderFetchType.valueOf(orderFetchTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid order fetch type");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Please send valid order fetch type");
				return customerOrderResponse;
			}
			
			if(TextUtil.isEmptyOrNull(orderTypeStr)){
				error.setDescription("Order Type is mandatory.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Order Type is mandatory");
				return customerOrderResponse;
			}
			
			OrderType orderType=null;
			try{
				orderType = OrderType.valueOf(orderTypeStr);
			}catch(Exception e){
				error.setDescription("Please send valid order type");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Please send valid order type");
				return customerOrderResponse;
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Please login to view your ticket orders");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Customer Id is Mandatory");
				return customerOrderResponse;
			}
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer = CustomerUtil.getCustomerById(customerId); //DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
				if(null == customer){
					error.setDescription("Please login to view your ticket orders");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Please send valid Customer Id");
					return customerOrderResponse;
				}
			}catch(Exception e){
				error.setDescription("Please login to view your ticket orders");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Please send valid Customer Id");
				return customerOrderResponse;
			}
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			
			Integer includeDays = 0;
			String orderDeliveryNote = "";
			String fedExTrackingNo = "";
			if(orderFetchType.equals(OrderFetchType.ALL)){
				
				List<CustomerOrder> activeOrders = DAORegistry.getCustomerOrderDAO().getOrdersByCustomerIdandOrderType(customerId,OrderFetchType.ActiveOrders,includeDays,orderType);
				List<CustomerOrder> pastOrders = DAORegistry.getCustomerOrderDAO().getOrdersByCustomerIdandOrderType(customerId,OrderFetchType.PastOrders,includeDays,orderType);
				
				if(null != activeOrders && !activeOrders.isEmpty()){
					List<InvoiceTicketAttachment> finalTicketAttachments = null; 
					for (CustomerOrder order : activeOrders) {
						Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByCustomerOrder(order.getId(), customerId);
						
						String isRealTixMap = null;
						if(null == invoice || invoice.getStatus().equals(InvoiceStatus.Voided)){
							
							orderDeliveryNote = "Order Cancelled";
							order.setButtonOption("OC");
							//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
							//order.setShippingMethod("E-Ticket");
							order.setOrderDeliveryNote(orderDeliveryNote);
							order.setDeliveryInfo(null);
							
						}else{
							 isRealTixMap = invoice.getRealTixMap();
							if(isRealTixMap.equals("Yes") || isRealTixMap.equals("YES")){
								order.setShowInfoButton(Boolean.FALSE);
								if(invoice.getShippingMethodId()!=null&&!invoice.getShippingMethodId().equals(3)){
									
									List<InvoiceTicketAttachment> invoiceTicketAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
									if(invoiceTicketAttachmentList!=null){
										finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
										for (InvoiceTicketAttachment tixAttachemnt : invoiceTicketAttachmentList) {
											try {
												File file = new File (URLUtil.findETicketDownloadURLFromDirectory(tixAttachemnt.getFilePath()));
												if(null != file && file.exists()){
													tixAttachemnt.setDownloadUrl(URLUtil.getETicketDownloadUrl(request, order, configIdString,
															customer.getId(),invoice,tixAttachemnt.getId(),applicationPlatForm,ip));
													finalTicketAttachments.add(tixAttachemnt);
												}
												tixAttachemnt.setExtension(URLUtil.getFileExtension(tixAttachemnt.getFilePath()));
												tixAttachemnt.setFileName(URLUtil.getFileNameForTriviaTickets(tixAttachemnt.getFilePath()));
												
											}catch(Exception e) {
												e.printStackTrace();
											}
										}
										try{
											Collections.sort(invoiceTicketAttachmentList, TicketUtil.ticketAttachmentsByPosition);
										}catch(Exception e){
											e.printStackTrace();
										}
									}else{
										finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
									}
									
									if(null != finalTicketAttachments && !finalTicketAttachments.isEmpty() && finalTicketAttachments.size()>0){
										orderDeliveryNote = "Download Tickets";
										order.setOrderDeliveryNote(orderDeliveryNote);
										order.setButtonOption("DL");
										order.setInvoiceTicketAttachment(finalTicketAttachments);
										order.setShippingMethod("E-Ticket");
										order.setDeliveryInfo(null);
									}else{
										order.setButtonOption("PROGRESS");
										order.setTicketDownloadUrl(null);
										order.setShowInfoButton(Boolean.TRUE);
										order.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(order));
										TicketUtil.getTicketDeliveryInFo(order,fontSizeStr);
										TicketUtil.getProgressPopupMsg(order,fontSizeStr);
										order.setShippingMethod("E-Ticket");
										order.setDeliveryInfo(null);
									}
								}else if(invoice.getShippingMethodId()!=null&& invoice.getShippingMethodId().equals(3)){
									fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
									orderDeliveryNote = "FedEx: "+fedExTrackingNo;
									order.setButtonOption("FEDEX");
									order.setTicketDownloadUrl(URLUtil.getFedExTrackingURL(invoice));
									order.setDeliveryInfo(null);
									order.setShippingMethod("FedEx");
									order.setOrderDeliveryNote(orderDeliveryNote);
								}
							}else{
								
								Date eventDate = order.getEventDateTemp();
								Date curDate =  new Date();
								long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
								Date tmpDate  = dateFormat.parse("01/01/3000");
								Boolean isNullEventFlag = false;
								if(eventDate.compareTo(tmpDate) == 0) {
									isNullEventFlag = true;
									order.setEventDate(null);
									order.setEventDateTemp(null);
									order.setEventTime(null);
								}
								
								
								if(noOfDays < 0 && !isNullEventFlag){
									orderDeliveryNote = "Order Cancelled";
									order.setButtonOption("OC");
									order.setOrderDeliveryNote(orderDeliveryNote);
									order.setDeliveryInfo(null);
								}else{
									order.setButtonOption("PROGRESS");
									order.setTicketDownloadUrl(null);
									order.setShowInfoButton(Boolean.TRUE);
									order.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(order));
									TicketUtil.getTicketDeliveryInFo(order,fontSizeStr);
									TicketUtil.getProgressPopupMsg(order,fontSizeStr);
									order.setShippingMethod("E-Ticket");
									order.setDeliveryInfo(null);
								}
								
							}
						}
					}
				}
				if(null != pastOrders && !pastOrders.isEmpty()){
					
					List<InvoiceTicketAttachment> finalTicketAttachments = null; 

					for (CustomerOrder order : pastOrders) {
						Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByCustomerOrder(order.getId(), customerId);
						
						String isRealTixMap = null;
						
						if(null == invoice || invoice.getStatus().equals(InvoiceStatus.Voided)){
							
							orderDeliveryNote = "Order Cancelled";
							order.setButtonOption("OC");
							//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
							//order.setShippingMethod("E-Ticket");
							order.setOrderDeliveryNote(orderDeliveryNote);
							order.setDeliveryInfo(null);
							
						}else{
							 isRealTixMap = invoice.getRealTixMap();
							 if(isRealTixMap.equals("Yes") || isRealTixMap.equals("YES")){
								order.setShowInfoButton(Boolean.FALSE);
								if(invoice.getShippingMethodId()!=null && !invoice.getShippingMethodId().equals(3)){
									orderDeliveryNote = "Download Tickets";
									order.setButtonOption("DL");
									
									List<InvoiceTicketAttachment> invoiceTicketAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
									
									
									
									if(invoiceTicketAttachmentList!=null){
										finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
										for (InvoiceTicketAttachment tixAttachemnt : invoiceTicketAttachmentList) {
											try {
												File file = new File (URLUtil.findETicketDownloadURLFromDirectory(tixAttachemnt.getFilePath()));
												if(null != file && file.exists()){
													tixAttachemnt.setDownloadUrl(URLUtil.getETicketDownloadUrl(request, order, configIdString,
															customer.getId(),invoice,tixAttachemnt.getId(),applicationPlatForm,ip));
													finalTicketAttachments.add(tixAttachemnt);
												}
												tixAttachemnt.setExtension(URLUtil.getFileExtension(tixAttachemnt.getFilePath()));
												tixAttachemnt.setFileName(URLUtil.getFileNameForTriviaTickets(tixAttachemnt.getFilePath()));
												
											}catch(Exception e) {
												e.printStackTrace();
											}
										}
										try{
											Collections.sort(invoiceTicketAttachmentList, TicketUtil.ticketAttachmentsByPosition);
										}catch(Exception e){
											e.printStackTrace();
										}
									}else{
										finalTicketAttachments =  new ArrayList<InvoiceTicketAttachment>();
									}
									
									
									if(null != finalTicketAttachments && !finalTicketAttachments.isEmpty() && finalTicketAttachments.size() >0){
										orderDeliveryNote = "Download Tickets";
										order.setOrderDeliveryNote(orderDeliveryNote);
										order.setButtonOption("DL");
										order.setInvoiceTicketAttachment(finalTicketAttachments);
										order.setShippingMethod("E-Ticket");
										order.setDeliveryInfo(null);
									}else{
										
										Date eventDate = order.getEventDateTemp();
										Date curDate =  new Date();
										long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
										
										if(noOfDays < 0){
											orderDeliveryNote = "No Ticket Attached";
											order.setButtonOption("OC");
											order.setOrderDeliveryNote(orderDeliveryNote);
											order.setDeliveryInfo(null);
										}else{
											order.setButtonOption("PROGRESS");
											order.setTicketDownloadUrl(null);
											order.setShowInfoButton(Boolean.TRUE);
											order.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(order));
											TicketUtil.getTicketDeliveryInFo(order,fontSizeStr);
											TicketUtil.getProgressPopupMsg(order,fontSizeStr);
											order.setShippingMethod("E-Ticket");
											order.setDeliveryInfo(null);
										}
									}
									/*order.setInvoiceTicketAttachment(invoiceTicketAttachmentList);
									order.setDeliveryInfo(null);
									order.setShippingMethod("E-Ticket");
									order.setOrderDeliveryNote(orderDeliveryNote);*/
								}else if(invoice.getShippingMethodId()!=null && invoice.getShippingMethodId().equals(3)){
									fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
									orderDeliveryNote = "FedEx: "+fedExTrackingNo;
									order.setButtonOption("FEDEX");
									order.setTicketDownloadUrl(URLUtil.getFedExTrackingURL(invoice));
									order.setDeliveryInfo(null);
									order.setShippingMethod("FedEx");
									order.setOrderDeliveryNote(orderDeliveryNote);
								}
							}else{
								
								orderDeliveryNote = "Order Cancelled";
								order.setButtonOption("OC");
								order.setOrderDeliveryNote(orderDeliveryNote);
								order.setDeliveryInfo(null);
								
							}
						}
					}
				}

				customerOrderResponse.setCustomerOrders(activeOrders);
				customerOrderResponse.setPastOrders(pastOrders);
//comment for Production				
				List<ContestGrandWinner> grandWiners = QuizDAORegistry.getQuizQueryManagerDAO().getAllActiveGrandWinnersByCustomerId(customerId);
				customerOrderResponse.setContestGrandWinners(grandWiners);
				
			}else{
				
				List<CustomerOrder> customerOrders = DAORegistry.getCustomerOrderDAO().getOrdersByCustomerIdandOrderType(customerId,orderFetchType,includeDays,orderType);
				
				List<InvoiceTicketAttachment> finalTicketAttachments = null;
				
				if(null != customerOrders && !customerOrders.isEmpty()){
					
					for (CustomerOrder order : customerOrders) {
						Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByCustomerOrder(order.getId(), customerId);
						
						String isRealTixMap = null;
						
						if(null ==invoice || invoice.getStatus().equals(InvoiceStatus.Voided)){
							
							orderDeliveryNote = "Order Cancelled";
							order.setButtonOption("OC");
							//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
							//order.setShippingMethod("E-Ticket");
							order.setOrderDeliveryNote(orderDeliveryNote);
							order.setDeliveryInfo(null);
							
						}else{
							 isRealTixMap = invoice.getRealTixMap();
							 if(isRealTixMap.equals("Yes") || isRealTixMap.equals("YES")){
								order.setShowInfoButton(Boolean.FALSE);
								if(invoice.getShippingMethodId()!=null&&!invoice.getShippingMethodId().equals(3)){
									
									List<InvoiceTicketAttachment> invoiceTicketAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
									
									if(invoiceTicketAttachmentList!=null){
										finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
										for (InvoiceTicketAttachment tixAttachemnt : invoiceTicketAttachmentList) {
											try {
												File file = new File (URLUtil.findETicketDownloadURLFromDirectory(tixAttachemnt.getFilePath()));
												if(null != file && file.exists()){
													tixAttachemnt.setDownloadUrl(URLUtil.getETicketDownloadUrl(request, order, configIdString,
															customer.getId(),invoice,tixAttachemnt.getId(),applicationPlatForm,ip));
													finalTicketAttachments.add(tixAttachemnt);
												}
												tixAttachemnt.setExtension(URLUtil.getFileExtension(tixAttachemnt.getFilePath()));
												tixAttachemnt.setFileName(URLUtil.getFileNameForTriviaTickets(tixAttachemnt.getFilePath()));
												
											}catch(Exception e) {
												e.printStackTrace();
											}
										}
										try{
											Collections.sort(invoiceTicketAttachmentList, TicketUtil.ticketAttachmentsByPosition);
										}catch(Exception e){
											e.printStackTrace();
										}
									}else{
										finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
									}
									
									if(null != finalTicketAttachments && !finalTicketAttachments.isEmpty() && finalTicketAttachments.size() > 0 ){
										orderDeliveryNote = "Download Tickets";
										order.setOrderDeliveryNote(orderDeliveryNote);
										order.setButtonOption("DL");
										order.setInvoiceTicketAttachment(finalTicketAttachments);
										order.setShippingMethod("E-Ticket");
										order.setDeliveryInfo(null);
									}else{
										Date eventDate = order.getEventDateTemp();
										Date curDate =  new Date();
										long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
										
										if(noOfDays < 0){
											orderDeliveryNote = "No Ticket Attached";
											order.setButtonOption("OC");
											order.setOrderDeliveryNote(orderDeliveryNote);
											order.setDeliveryInfo(null);
										}else{
											order.setButtonOption("PROGRESS");
											order.setTicketDownloadUrl(null);
											order.setShowInfoButton(Boolean.TRUE);
											order.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(order));
											TicketUtil.getTicketDeliveryInFo(order,fontSizeStr);
											TicketUtil.getProgressPopupMsg(order,fontSizeStr);
											order.setShippingMethod("E-Ticket");
											order.setDeliveryInfo(null);
										}
									}
									
									
									/*order.setInvoiceTicketAttachment(invoiceTicketAttachmentList);
									order.setDeliveryInfo(null);
									order.setShippingMethod("E-Ticket");
									order.setOrderDeliveryNote(orderDeliveryNote);*/
								}else if(invoice.getShippingMethodId()!=null && invoice.getShippingMethodId().equals(3)){
									fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
									orderDeliveryNote = "FedEx: "+fedExTrackingNo;
									order.setButtonOption("FEDEX");
									order.setTicketDownloadUrl(URLUtil.getFedExTrackingURL(invoice));
									order.setDeliveryInfo(null);
									order.setShippingMethod("FedEx");
									order.setOrderDeliveryNote(orderDeliveryNote);
								}
							}else{
								
								if(orderFetchType.equals(OrderFetchType.PastOrders)){
									orderDeliveryNote = "Order Cancelled";
									order.setButtonOption("OC");
									//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
									//order.setShippingMethod("E-Ticket");
									order.setOrderDeliveryNote(orderDeliveryNote);
									order.setDeliveryInfo(null);
								}else{
									
									Date eventDate = order.getEventDateTemp();
									Date curDate =  new Date();
									long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
									Date tmpDate  = dateFormat.parse("01/01/3000");
									Boolean isNullEventFlag = false;
									if(eventDate.compareTo(tmpDate) == 0) {
										isNullEventFlag = true;
										order.setEventDateTemp(null);
										order.setEventDate(null);
										order.setEventTime(null);
									}
									
									if(noOfDays < 0 && !isNullEventFlag){
									
										orderDeliveryNote = "Order Cancelled";
										order.setButtonOption("OC");
										order.setOrderDeliveryNote(orderDeliveryNote);
										order.setDeliveryInfo(null);
									}else{
										order.setButtonOption("PROGRESS");
										order.setTicketDownloadUrl(null);
										order.setShowInfoButton(Boolean.TRUE);
										order.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(order));
										TicketUtil.getTicketDeliveryInFo(order,fontSizeStr);
										TicketUtil.getProgressPopupMsg(order,fontSizeStr);
										order.setShippingMethod("E-Ticket");
										order.setDeliveryInfo(null);
									}
								}
							}
						}
					}
				}
				customerOrderResponse.setCustomerOrders(customerOrders);
				customerOrderResponse.setPastOrders(null);
				//comment for Production								
				List<ContestGrandWinner> grandWiners = QuizDAORegistry.getQuizQueryManagerDAO().getAllActiveGrandWinnersByCustomerId(customerId);
				customerOrderResponse.setContestGrandWinners(grandWiners);
			}
			customerOrderResponse.setContestTicketsConfirmDialog(TextUtil.getContestTicketsConfirmDialog(fontSizeStr));
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Success");
			customerOrderResponse.setStatus(1);
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while getting Customer Orders.");
			customerOrderResponse.setError(error);
			customerOrderResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETORDERS,"Error occured while getting Customer Orders");
			return customerOrderResponse;
		}
		return customerOrderResponse;
	}
	
	
	@RequestMapping(value = "/GetOrder",method = RequestMethod.POST)
	public @ResponsePayload CustomerOrderResponse getOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerOrderResponse customerOrderResponse = new CustomerOrderResponse();
		Error error = new Error();
		
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
				return customerOrderResponse;
			}
//			Integer configId= null;
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerOrderResponse.setError(error);
						customerOrderResponse.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
						return customerOrderResponse;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
					return customerOrderResponse;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"You are not authorized");
				return customerOrderResponse;
			}
			String productTypeStr = request.getParameter("productType");
			String orderIdStr = request.getParameter("orderNo");
			String customerIdStr = request.getParameter("customerId");
			String platForm = request.getParameter("platForm");
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Product Type is mandatory");
				return customerOrderResponse;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Please send valid product type");
					return customerOrderResponse;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is Mandatory");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Order No is Mandatory");
				return customerOrderResponse;
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is Mandatory");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Customer Id is Mandatory");
				return customerOrderResponse;
			}
			
			Integer customerId=null,orderId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					customerOrderResponse.setError(error);
					customerOrderResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Please send valid Customer Id");
					return customerOrderResponse;
				}
				
				/* Share Link Validation for Share Icon - Added By Ulaganathan*/
				customerOrderResponse.setEligibleToShareRefCode(true);
				customerOrderResponse.setShareErrorMessage("");
				
				try{
					CustomerUtil.updatedCustomerUtil(customer);
				}catch(Exception e){
					e.printStackTrace();
				}
				/* Share Link Validation for Share Icon - Added By Ulaganathan*/
				
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Please send valid Customer Id");
				return customerOrderResponse;
			}
			
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
			}catch(Exception e){
				error.setDescription("The Order Number you entered is invalid. Please try again");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"The Order Number you entered is invalid. Please try again");
				return customerOrderResponse;
			}
			
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getCustomerOrderByOrderId(orderId);
			if(customerOrder == null){
				error.setDescription("The Order Number you entered is invalid. Please try again");
				customerOrderResponse.setError(error);
				customerOrderResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"The Order Number you entered is invalid. Please try again");
				return customerOrderResponse;
			}
			
			Boolean showLongInfo= false;
			boolean isLongSale = false;
			List<OrderTicketGroup> orderTicketGroups = null;
			if(null != customerOrder.getIsLongSale() && customerOrder.getIsLongSale()){
				orderTicketGroups = DAORegistry.getOrderTicketGroupDAO().getAllTicketGroupsByOrderId(orderId);
				showLongInfo= true;
				isLongSale = true;
			}else if(null == customerOrder.getIsLongSale() || !customerOrder.getIsLongSale() ){
				orderTicketGroups = DAORegistry.getOrderTicketGroupDAO().getAllTicketGroupsByOrderId(orderId);
				if(null != orderTicketGroups && !orderTicketGroups.isEmpty()){
					showLongInfo= true;
				}
			}
			customerOrder.setIsOrderFullFilled(showLongInfo);
			
			 //Adding the code to show the svgMapWebViewUrl in response
			 String svgZone = customerOrder.getSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
			 
			if(showLongInfo){
				customerOrder.setSectionDescription("<font><b>------</b></font>");
				customerOrder.setIsOrderFullFilled(true);
				String zone = "", section="",row="",seat="";
				Double ticketPrice = 0.00;
				int i =0;
				for (OrderTicketGroup orderTicketGroup : orderTicketGroups) {
					if(i == 0 ){
						zone = orderTicketGroup.getZone();
						section= orderTicketGroup.getSection();
						row= orderTicketGroup.getRow();
						seat= orderTicketGroup.getSeat();
					}else{
						zone = zone +","+orderTicketGroup.getZone();
						section= section +","+orderTicketGroup.getSection();
						row= row +","+orderTicketGroup.getRow();
						seat= seat +","+orderTicketGroup.getSeat();
					}
					ticketPrice = ticketPrice + TicketUtil.getRoundedValue(orderTicketGroup.getSoldPrice() * orderTicketGroup.getQuantity());
					if(isLongSale){
						if(null != orderTicketGroup.getZone() && !orderTicketGroup.getZone().equals("--")){
							svgZone =  svgZone +","+orderTicketGroup.getZone().toUpperCase().replace("ZONE", "").replaceAll(" +","");
						}
					}
					
					i++;
				}
				customerOrder.setZone(zone);
				customerOrder.setRealSection(section);
				customerOrder.setRealRow(row);
				customerOrder.setRealSeat(seat);
				if(null != customerOrder.getIsLongSale() && customerOrder.getIsLongSale()){
					customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(ticketPrice));
				}else{
					customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(customerOrder.getTicketSoldPriceAsDouble() * customerOrder.getQty()));
				}
			} else{
				customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(customerOrder.getTicketSoldPriceAsDouble() * customerOrder.getQty()));
			}
			Double totalTax = customerOrder.getTaxesAsDouble();
			Double singleTixFees = TicketUtil.getRoundedValue(totalTax / customerOrder.getQty());
			customerOrder.setTotalServiceFees(TicketUtil.getRoundedValueString(totalTax));
			customerOrder.setSingleTixServiceFees(TicketUtil.getRoundedValueString(singleTixFees));
			
			Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByCustomerOrder(customerOrder.getId(), customerId);
			String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			customerOrder.setViewOrderType(String.valueOf(customerOrder.getOrderType()));
			
			if(null != customerOrder.getSecondaryOrdertype() && customerOrder.getSecondaryOrdertype().equals(SecondaryOrderType.CROWNJEWEL)){
				customerOrder.setShowCJEOrder(true);
				customerOrder.setCjeOrderInfo("Fantasy Order No : "+customerOrder.getSecondaryOrderId());
				customerOrder.setCjeOrderNo(customerOrder.getSecondaryOrderId());
				customerOrder.setViewOrderType(String.valueOf(customerOrder.getSecondaryOrdertype()));
			}
			String orderDeliveryNote= null;
			String isRealTixMap = null;
			if(null == invoice || invoice.getStatus().equals(InvoiceStatus.Voided)){
				orderDeliveryNote = "Order Cancelled";
				customerOrder.setButtonOption("OC");
				customerOrder.setOrderDeliveryNote(orderDeliveryNote);
				customerOrder.setDeliveryInfo(null);
				
			}else{
				
				isRealTixMap = invoice.getRealTixMap();
				if(isRealTixMap.equals("Yes") || isRealTixMap.equals("YES")){
					customerOrder.setShowInfoButton(Boolean.FALSE);
					if(invoice.getShippingMethodId()!=null &&!invoice.getShippingMethodId().equals(3)){
						orderDeliveryNote = "Download Tickets";
						customerOrder.setButtonOption("DL");
				
						List<InvoiceTicketAttachment> invoiceTicketAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
						
						List<InvoiceTicketAttachment> finalTicketAttachments = null;
						
						if(invoiceTicketAttachmentList!=null){
							
							finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
							
							for (InvoiceTicketAttachment tixAttachemnt : invoiceTicketAttachmentList) {
								try {
									File file = new File (URLUtil.findETicketDownloadURLFromDirectory(tixAttachemnt.getFilePath()));
									if(null != file && file.exists()){
										tixAttachemnt.setDownloadUrl(URLUtil.getETicketDownloadUrl(request, customerOrder, configIdString,
												customer.getId(),invoice,tixAttachemnt.getId(),applicationPlatForm,ip));
										finalTicketAttachments.add(tixAttachemnt);
									}
									
									tixAttachemnt.setExtension(URLUtil.getFileExtension(tixAttachemnt.getFilePath()));
									tixAttachemnt.setFileName(URLUtil.getFileNameForTriviaTickets(tixAttachemnt.getFilePath()));
									
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							
							try{
								Collections.sort(finalTicketAttachments, TicketUtil.ticketAttachmentsByPosition);
							}catch(Exception e){
								e.printStackTrace();
							}
						}else{
							finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
						}
						
						
						if(null != finalTicketAttachments && !finalTicketAttachments.isEmpty() && finalTicketAttachments.size() >0){
							orderDeliveryNote = "Download Tickets";
							customerOrder.setOrderDeliveryNote(orderDeliveryNote);
							customerOrder.setButtonOption("DL");
							customerOrder.setInvoiceTicketAttachment(finalTicketAttachments);
							customerOrder.setShippingMethod("E-Ticket");
							customerOrder.setDeliveryInfo(null);
						}else{
							
							Date eventDate = customerOrder.getEventDateTemp();
							Date curDate =  new Date();
							
							long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
							
							if(noOfDays < 0){
								orderDeliveryNote = "No Ticket Attached";
								customerOrder.setButtonOption("OC");
								//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
								//order.setShippingMethod("E-Ticket");
								customerOrder.setOrderDeliveryNote(orderDeliveryNote);
								customerOrder.setDeliveryInfo(null);
							}else{
								customerOrder.setButtonOption("PROGRESS");
								customerOrder.setTicketDownloadUrl(null);
								customerOrder.setShowInfoButton(Boolean.TRUE);
								customerOrder.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(customerOrder));
								TicketUtil.getTicketDeliveryInFo(customerOrder,fontSizeStr);
								TicketUtil.getProgressPopupMsg(customerOrder,fontSizeStr);
								customerOrder.setShippingMethod("E-Ticket");
								customerOrder.setDeliveryInfo(null);
							}
							
						}
						
						/*customerOrder.setInvoiceTicketAttachment(invoiceTicketAttachmentList);
						//customerOrder.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, customerOrder, configIdString,customer.getId(),invoice,invoiceTicketAttachment));
						customerOrder.setDeliveryInfo("");
						customerOrder.setShippingMethod("E-Ticket");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);*/
					}else if(invoice.getShippingMethodId()!=null && invoice.getShippingMethodId().equals(3)){
						String fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
						orderDeliveryNote = "FedEx : "+fedExTrackingNo;
						customerOrder.setButtonOption("FEDEX");
						customerOrder.setTicketDownloadUrl(URLUtil.getFedExTrackingURL(invoice));
						customerOrder.setDeliveryInfo("");
						customerOrder.setShippingMethod("FedEx");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);
					}
				}else{
					
					Date eventDate = customerOrder.getEventDateTemp();
					Date curDate =  new Date();
					long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
					Date tmpDate  = dateFormat.parse("01/01/3000");
					Boolean isNullEventFlag = false;
					if(eventDate.compareTo(tmpDate) == 0) {
						isNullEventFlag = true;
						customerOrder.setEventDate(null);
						customerOrder.setEventDateTemp(null);
						customerOrder.setEventTime(null);
					}
					
					if(noOfDays < 0 && !isNullEventFlag){
						orderDeliveryNote = "Order Cancelled";
						customerOrder.setButtonOption("OC");
						//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
						//order.setShippingMethod("E-Ticket");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);
						customerOrder.setDeliveryInfo(null);
					}else{
						customerOrder.setButtonOption("PROGRESS");
						customerOrder.setTicketDownloadUrl(null);
						customerOrder.setShowInfoButton(Boolean.TRUE);
						customerOrder.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(customerOrder));
						TicketUtil.getTicketDeliveryInFo(customerOrder,fontSizeStr);
						TicketUtil.getProgressPopupMsg(customerOrder,fontSizeStr);
						customerOrder.setShippingMethod("E-Ticket");
						customerOrder.setDeliveryInfo(null);
					}
					
				}
			}
			
			
			 Collection<ZoneRGBColor> zoneRgbColors =DAORegistry.getZoneRGBColorDAO().getAll();
			 Map<String, ZoneRGBColor> rgbColorMap = new HashMap<String, ZoneRGBColor>();
			 for (ZoneRGBColor zoneRGBColor : zoneRgbColors) {
				if(ProductType.PRESALEZONETICKETS.equals(productType)){
					rgbColorMap.put(zoneRGBColor.getZone().replaceAll("_","").toUpperCase(), zoneRGBColor);
				}else{
					rgbColorMap.put(zoneRGBColor.getZone().replaceAll(" +","").toUpperCase(), zoneRGBColor);
				}
			  }
			 
			ZoneRGBColor zoneRGBColor = rgbColorMap.get(svgZone);
			if(null != zoneRGBColor){
				customerOrder.setColorCode(zoneRGBColor.getColor());
			}else{
				customerOrder.setColorCode("#F4ED6D");
			}
			
			String svgWebViewUrl = "";
			
			if(null != customerOrder.getOrderType() && customerOrder.getOrderType().equals(OrderType.CONTEST)){
				if(customerOrder.getSection().equals("TBD")){
					
					List<String> zoneList =  DAORegistry.getQueryManagerDAO().getAllZonesByVenueCategoryId(customerOrder.getVenueCategory());
					if(null != zoneList && !zoneList.isEmpty()){
						for (String zone : zoneList) {
							svgZone = svgZone +","+ zone.toUpperCase().replace("ZONE", "").replaceAll(" +","");
						}
						svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,null,true);
					}else{
						svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,null,false);
					}
				}else{
					svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,null,false);
				}
				
			}else{
				svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,customerOrder.getId(),false);
			}
			
			/*if(applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				customerOrder.setIsIOS(true);
			}else {
				customerOrder.setIsIOS(false);
			}*/
			
			customerOrder.setSvgKey(svgZone.toLowerCase());
			 
			MapUtil.getOrderSvgTextDetails(customerOrder, applicationPlatForm);
			 
			//it will set customer loyalty,customer detail and customer card info.
			customerOrder.setAllCustomerRelatedObjects();
			customerOrder.setRewardPoints("<font color=#012e4f>You have just earned <b>"+customerOrder.getCustomerLoyaltyHistory().getPointsEarned()+" reward dollars</b> from this purchase.</font>");
			
			List<CustomerOrder> customerOrderList = new ArrayList<CustomerOrder>();
			customerOrderList.add(customerOrder);
			customerOrderResponse.setCustomerOrders(customerOrderList);
			customerOrderResponse.setStatus(1);
			customerOrderResponse.setSvgWebViewUrl(svgWebViewUrl);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Success");
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while getting order.");
			customerOrderResponse.setError(error);
			customerOrderResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VIEWORDER,"Error occured while getting order Order");
			return customerOrderResponse;
		}
		return customerOrderResponse;
	}
	
	@RequestMapping(value = "/CancelOrder",method = RequestMethod.POST)
	public @ResponsePayload CancelOrderResponse cancelOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		Error error = new Error();
		CancelOrderResponse cancelResponse = new CancelOrderResponse();
		try {
			String productTypeStr = request.getParameter("productType");
			String orderIdStr = request.getParameter("orderNo");
			String customerIdStr = request.getParameter("customerId");
			String platForm = request.getParameter("platForm");
			String orderTypeStr = request.getParameter("orderType");
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				cancelResponse.setError(error);
				cancelResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Product Type is mandatory");
				return cancelResponse;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					cancelResponse.setError(error);
					cancelResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Please send valid product type");
					return cancelResponse;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderTypeStr)){
				error.setDescription("Order Type is mandatory.");
				cancelResponse.setError(error);
				cancelResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Product Type is mandatory");
				return cancelResponse;
			}
			
			OrderType orderType=null;
			if(!TextUtil.isEmptyOrNull(orderTypeStr)){
				try{
					orderType = OrderType.valueOf(orderTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid order type");
					cancelResponse.setError(error);
					cancelResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Please send valid order type");
					return cancelResponse;
				}
			}
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is Mandatory");
				cancelResponse.setError(error);
				cancelResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Order No is Mandatory");
				return cancelResponse;
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is Mandatory");
				cancelResponse.setError(error);
				cancelResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Customer Id is Mandatory");
				return cancelResponse;
			}
			
			Integer customerId=null,orderId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					cancelResponse.setError(error);
					cancelResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Please send valid Customer Id");
					return cancelResponse;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				cancelResponse.setError(error);
				cancelResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Please send valid Customer Id");
				return cancelResponse;
			}
			
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
			}catch(Exception e){
				error.setDescription("The Order Number you entered is invalid. Please try again");
				cancelResponse.setError(error);
				cancelResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"The Order Number you entered is invalid. Please try again");
				return cancelResponse;
			}
			if(orderType.equals(OrderType.REGULAR)){
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(orderId);
				if(customerOrder == null){
					error.setDescription("The Order Number you entered is invalid. Please try again");
					cancelResponse.setError(error);
					cancelResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"The Order Number you entered is invalid. Please try again");
					return cancelResponse;
				}
				String zones = customerOrder.getSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
				String svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(),zones,customerOrder.getId(),false);
				CustomerOrderDetail orderDetails = DAORegistry.getCustomerOrderDetailDAO().getCustomerOrderDetailByOrderId(customerOrder.getId());
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("customer", customer);  
				mailMap.put("customerOrder", customerOrder);
				mailMap.put("venueMap", svgWebViewUrl);
				MailAttachment[] mailAttachment = Util.getOrderConfirmationEmailAttachmentImages();
				String email=null;
				if(orderDetails.getShippingEmail()!=null && !orderDetails.getShippingEmail().isEmpty()){
					email = orderDetails.getShippingEmail();
				}else if(orderDetails.getBillingEmail()!=null && !orderDetails.getBillingEmail().isEmpty()){
					email = orderDetails.getBillingEmail();
				}else{
					email =customer.getEmail();
				}
				//email = "msanghani@rightthisway.com";
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail,email, 
							null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
							"Your order has been cancelled", "mail-cancel-customer-order.html", mailMap, "text/html", null,mailAttachment,null);
					
					DAORegistry.getCustomerOrderDAO().updateOrderCancelEmailSent(customerOrder.getId(),email);
					
				}catch(Exception e) {
					error.setDescription("Error Occured while sending mail to customer.");
					cancelResponse.setError(error);
					cancelResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Error Occured while sending mail to customer.");
					return cancelResponse;
				}
			}else if(orderType.equals(OrderType.FANTASYTICKETS)){
				CustomerFantasyOrder futureOrder = DAORegistry.getCustomerFantasyOrderDAO().get(orderId);
				if(futureOrder == null){
					error.setDescription("The Order Number you entered is invalid fantasy order. Please try again");
					cancelResponse.setError(error);
					cancelResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"The Order Number you entered is invalid fantasy order. Please try again");
					return cancelResponse;
				}
				
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("firstName", customer.getCustomerName());
				mailMap.put("lastName", customer.getLastName());
				mailMap.put("teamName", futureOrder.getTeamName());
				mailMap.put("leagueName", futureOrder.getFantasyEventName());
				mailMap.put("email", customer.getEmail());
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
				
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.bccEmails, 
							"Fantasy Sports Ticket Order Cancelled",
							"mail-fantasy-ticket-team-exclude.html", mailMap, "text/html", null,mailAttachment,null);
				}catch(Exception e) {
					error.setDescription("Error Occured while sending mail to customer.");
					cancelResponse.setError(error);
					cancelResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Error Occured while sending mail to customer.");
					return cancelResponse;
				}
			}
			cancelResponse.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Success");
			return cancelResponse;
		} catch (Exception e) {
			error.setDescription("Something went wrong");
			cancelResponse.setError(error);
			cancelResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Something went wrong. Please try again");
			return cancelResponse;
		}
	}
	
	
}
