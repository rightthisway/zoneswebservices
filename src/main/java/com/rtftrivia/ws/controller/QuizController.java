
package com.rtftrivia.ws.controller;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassContestGrandWinner;
import com.quiz.cassandra.data.CassContestWinners;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.data.CassFbCallBackTracking;
import com.quiz.cassandra.data.CassRtfApiTracking;
import com.quiz.cassandra.data.CassSuperFanWinner;
import com.quiz.cassandra.data.ContestParticipants;
import com.quiz.cassandra.data.ContestPasswordAuth;
import com.quiz.cassandra.data.CustContAnswers;
import com.quiz.cassandra.data.CustContDtls;
import com.quiz.cassandra.data.CustContestStat;
import com.quiz.cassandra.data.SuperFanContCustLevels;
import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.CommonRespInfo;
import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.utils.CassContestUtil;
import com.quiz.cassandra.utils.CassCustomerUpload;
import com.quiz.cassandra.utils.TrackingUtil;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.ContestParticipantsCassandra;
import com.rtfquiz.webservices.data.CustomerContestQuestion;
import com.rtfquiz.webservices.data.QuizConfigSettings;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.data.QuizContestPasswordAuth;
import com.rtfquiz.webservices.data.QuizContestQuestions;
import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.data.QuizCustomerContestAnswers;
import com.rtfquiz.webservices.data.QuizCustomerFriend;
import com.rtfquiz.webservices.data.QuizCustomerReferralTracking;
import com.rtfquiz.webservices.data.QuizFirebaseCallBackTracking;
import com.rtfquiz.webservices.data.QuizSuperFanCustomerLevel;
import com.rtfquiz.webservices.data.QuizSuperFanLevelConfig;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.rtfquiz.webservices.enums.ContestJackpotType;
import com.rtfquiz.webservices.enums.ContestQuestRewardType;
import com.rtfquiz.webservices.enums.FriendStatus;
import com.rtfquiz.webservices.enums.MiniJackpotType;
import com.rtfquiz.webservices.enums.RewardType;
import com.rtfquiz.webservices.enums.WinnerStatus;
import com.rtfquiz.webservices.enums.WinnerType;
import com.rtfquiz.webservices.sqldao.implementation.ContestParticipantsCassandraSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.ContestWinnersSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.CustomerSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.QuizContestPasswordAuthSQLDAO;
import com.rtfquiz.webservices.sqldao.implementation.SQLDaoUtil;
import com.rtfquiz.webservices.sqldao.implementation.WebServiceTrackingCassandraSQLDAO;
import com.rtfquiz.webservices.thread.QuizCreditUtilThread;
import com.rtfquiz.webservices.thread.QuizMigrationUtilThread1;
import com.rtfquiz.webservices.thread.QuizMigrationUtilThread2;
import com.rtfquiz.webservices.utils.QuizConfigSettingsUtil;
import com.rtfquiz.webservices.utils.QuizContestStartNotification;
import com.rtfquiz.webservices.utils.QuizContestUtil;
import com.rtfquiz.webservices.utils.QuizJackpotWinnerUtil;
import com.rtfquiz.webservices.utils.QuizMigrationUtil;
import com.rtfquiz.webservices.utils.QuizMiniJackpotWinnerCredit;
import com.rtfquiz.webservices.utils.RTFBotsUtil;
import com.rtfquiz.webservices.utils.list.ContestEventResponse;
import com.rtfquiz.webservices.utils.list.CustomerNotificationDetails;
import com.rtfquiz.webservices.utils.list.CustomerStatistics;
import com.rtfquiz.webservices.utils.list.CustomerStats;
import com.rtfquiz.webservices.utils.list.CustomerStatsDetails;
import com.rtfquiz.webservices.utils.list.QuizContestInfo;
import com.rtfquiz.webservices.utils.list.QuizContestSummaryInfo;
import com.rtfquiz.webservices.utils.list.QuizCustomerStatsInfo;
import com.rtfquiz.webservices.utils.list.QuizGenericResponse;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CategoryTicket;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.ContestMigrationStats;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCassandra;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.OrderPaymentBreakup;
import com.zonesws.webservices.data.Property;
import com.zonesws.webservices.data.RtfConfigContestClusterNodes;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.data.WebServiceTrackingCassandra;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.InvoiceStatus;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.sms.TwilioSMSServices;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.PaginationUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.NormalSearchResult;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/GetCustomerStatsDetails","/SubmitTrivia","/CreateWinnerOrder","/GetContestEvents",
	"/RefreshNextContestList","/RefreshContestCacheData","/ForceMigrationToSqlFromCass","/UpdateCustContStats",
	"/MigrateApiTrackingToSql","/TruncateCassandraContestData","/MigrateContestDataFromCassandraToSQL",
	"/MigrateCustomerDataFromSQLToCassandra", "/SendSuperFanWinner","/AddSuperFanWinners","/GetCustomerStatsByUserId",
	"/GetQuizContestInfo","/AddTestAccountAsSuperFanWinners","/CreditSuperFanQuestionsLevels", "/ResetSuperFanLevelsData"})
public class QuizController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(QuizController.class);
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private static SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private static DecimalFormat percentageDF = new DecimalFormat("#.##");
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	public static Error authorizationValidation(HttpServletRequest request) throws Exception {
		Error error = new Error();
		HttpSession session = request.getSession();
		String ip =(String)session.getAttribute("ip");
		String configIdString = request.getParameter("configId");
		Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
		
		if(!isAuthrozed){
			error.setDescription("You are not authorized.");
			//quizOTPDetails.setError(error);
			//quizOTPDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
			return error;
		}
		
		if(configIdString!=null && !configIdString.isEmpty()){
			/*try {
				if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
					error.setDescription("You are not authorized.");
					//quizOTPDetails.setError(error);
					//quizOTPDetails.setStatus(0);
					//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
					return error;
				
				}
			} catch (Exception e) {
				error.setDescription("You are not authorized.");
				//quizOTPDetails.setError(error);
				//quizOTPDetails.setStatus(0);
				//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
				return error;
			}*/
		}else{
			error.setDescription("You are not authorized.");
			//quizOTPDetails.setError(error);
			//quizOTPDetails.setStatus(0);
			//TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"You are not authorized");
			return error;
		}
		return null;
	}
	
	
	@RequestMapping(value = "/MigrateCustomerDataFromSQLToCassandra")
	public @ResponsePayload DashboardInfo migrateCustomerDataFromSQLToCassandra(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		try {
			Integer processedCut = CassCustomerUpload.startUpload();
			dashboardInfo.setSts(1);
			resMsg = "Customer Data's are migrated from sql server to cassandra. Total Transfered Customers :"+processedCut;
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating customer data's from sql to cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while migrating customer data's from sql to cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("MIGRATING CUSTOMER DATA FROM SQL TO CASSANDRA :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	}
	
	
	@RequestMapping(value = "/MigrateContestDataFromCassandraToSQL")
	public @ResponsePayload DashboardInfo migrateContestDataFromCassandraToSQL(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "", contestIdStr = request.getParameter("contestId");
		try {
			Integer contestId = Integer.parseInt(contestIdStr);
			migrateToSqlFromCassandra(contestId);
			
			dashboardInfo.setSts(1);
			resMsg = "Contest Data's are migrated from cassandra to sql server for contest id :"+contestIdStr;
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating contest data from cassandra to sql server";
			e.printStackTrace();
			error.setDesc("Error occured while migrating contest data from cassandra to sql server");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("MIGRATING CONTEST DATA FROM CASSANDRA TO SQL  :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	}
	
	@RequestMapping(value = "/TruncateCassandraContestData")
	public @ResponsePayload DashboardInfo refreshCassandraDatabaseForContest(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		try {
			String contestIdStr = request.getParameter("contestId");
			truncateCassandraContestData(contestIdStr);
			
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg("Cassandra Contest data deleted successfully.!");
		}catch(Exception e){
			e.printStackTrace();
			error.setDesc("Error occured while deleting contest data");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} 
		return dashboardInfo;
	}
	
	public void truncateCassandraContestData(String contestIdStr) {
		try {
			System.out.println("Truncate Cassandra Table Started: "+new Date());
			CassandraDAORegistry.getContestParticipantsDAO().truncate();
			CassandraDAORegistry.getCustContAnswersDAO().truncate();
			CassandraDAORegistry.getCassContestGrandWinnerDAO().truncate();
			CassandraDAORegistry.getCassContestWinnersDAO().truncate();
			CassandraDAORegistry.getJackpotWinnerDAO().truncate();
			System.out.println("Truncate Cassandra Table Ended: "+new Date());
		}catch(Exception e){
			System.out.println("Truncate Cassandra Table Exception occured: ");
			e.printStackTrace();
			return;
		}
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		
		/*MigrateApiTrackingUtil trackingUtil = new MigrateApiTrackingUtil();
		Thread t = new Thread(trackingUtil);
		t.start();*/
		
	}
	
	public void migrateToSqlFromCassandraOld(Integer contestId) {
		
		Date start = new Date();
		
		ContestMigrationStats migrationStatObj = new ContestMigrationStats();
		migrationStatObj.setContestId(contestId);
		migrationStatObj.setStartDate(start);
		migrationStatObj.setStatus("RUNNING");
		DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
		
		/*QuizContestParticipants participant = null;*/
		QuizCustomerContestAnswers customerAnswers = null;
		QuizContestWinners contestWinner = null;
		ContestGrandWinner contestGrandWinner = null;
		
		/*List<QuizContestParticipants> toBeSavedParticipants = new ArrayList<QuizContestParticipants>();*/
		List<QuizCustomerContestAnswers> toBeSavedCustAnsList = new ArrayList<QuizCustomerContestAnswers>();
		List<QuizContestWinners> toBeSavedContWinnerList = new ArrayList<QuizContestWinners>();
		List<ContestGrandWinner> toBeSavedGrandWinnerList = new ArrayList<ContestGrandWinner>();
		
		/*List<ContestParticipants> cassContPrticipantsList = CassandraDAORegistry.getContestParticipantsDAO().getAllParticipantsByContestId(contestId);*/
		List<CustContAnswers> cassCustAnsList = CassandraDAORegistry.getCustContAnswersDAO().getCustContAnswersByContId(contestId);
		List<CassContestWinners> cassContWinnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		List<CassContestGrandWinner> grandWinnerList = CassandraDAORegistry.getCassContestGrandWinnerDAO().getContestGrandWinnersByContestId(contestId);
		
		
		
		for (CassContestGrandWinner obj : grandWinnerList) {
			try {
				contestGrandWinner = new ContestGrandWinner();
				contestGrandWinner.setContestId(obj.getCoId());
				contestGrandWinner.setCreatedDate(new Date(obj.getCrDated()));
				contestGrandWinner.setCustomerId(obj.getCuId());
				contestGrandWinner.setExpiryDate(new Date(obj.getExDate()));
				contestGrandWinner.setStatus(WinnerStatus.ACTIVE);
				contestGrandWinner.setRewardTickets(obj.getrTix());
				contestGrandWinner.setWinnerType(WinnerType.REGULAR);
				contestGrandWinner.setNotes("");
				toBeSavedGrandWinnerList.add(contestGrandWinner);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		 
		  QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
		
		  List<CassContestGrandWinner> jackpotWinneList = CassandraDAORegistry.getJackpotWinnerDAO().getMegaJackpotWinnersByContestId(contestId);
		  
		  if(null != jackpotWinneList && !jackpotWinneList.isEmpty()) {
			WinnerType winnerType = (null != contest.getContestJackpotType() && contest.getContestJackpotType().equals(ContestJackpotType.MEGA))?WinnerType.MEGAJACKPOT:WinnerType.MINIJACKPOT;
			
			ContestJackpotType jackpotType = contest.getContestJackpotType();
			
			Integer expairyDays = 1;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, expairyDays);
					
			for (CassContestGrandWinner obj : jackpotWinneList) {
				try {
					contestGrandWinner = new ContestGrandWinner();
					contestGrandWinner.setContestId(obj.getCoId());
					contestGrandWinner.setCreatedDate(new Date(obj.getCrDated()));
					contestGrandWinner.setCustomerId(obj.getCuId());
					contestGrandWinner.setExpiryDate(cal.getTime());
					contestGrandWinner.setStatus(WinnerStatus.ACTIVE);
					contestGrandWinner.setQuestionId(obj.getqId());
					contestGrandWinner.setWinnerType(winnerType);
					contestGrandWinner.setNotes(obj.getJackpotSt());
					QuizContestQuestions contestQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().get(obj.getqId());
					
					contestGrandWinner.setJackpotCreditType(contestQuestion.getJackpotCreditType());
					if(contestQuestion.getJackpotCreditType().equals(MiniJackpotType.TICKET)) {
						contestGrandWinner.setRewardTickets(contestQuestion.getjQtyPerWinner().intValue());
					}
					 
					toBeSavedGrandWinnerList.add(contestGrandWinner);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		} 
		
		
		/*for (ContestParticipants obj : cassContPrticipantsList) {
			try {
				participant = new QuizContestParticipants();
				participant.setContestId(contestId);
				participant.setCustomerId(obj.getCuId());
				participant.setPlatform(obj.getPfm());
				participant.setIpAddress(obj.getIpAdd());
				participant.setJoinDateTime(new Date(obj.getJnDate()));
				if(null != obj.getExDate()) {
					participant.setExitDateTime(new Date(obj.getExDate()));
				} 
				participant.setStatus(obj.getStatus());
				toBeSavedParticipants.add(participant);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}*/
		
		Map<Integer, CustContestStat> contestCustAnsMap = new HashMap<Integer, CustContestStat>(); 
		
		for (CustContAnswers obj : cassCustAnsList) {
			try {
				customerAnswers = new QuizCustomerContestAnswers();
				customerAnswers.setCustomerId(obj.getCuId());
				customerAnswers.setContestId(contestId);
				customerAnswers.setQuestionId(obj.getqId());
				customerAnswers.setQuestionSNo(obj.getqNo());
				if(null != obj.getAns()) {
					customerAnswers.setAnswer(obj.getAns());
				}
				customerAnswers.setIsCorrectAnswer(obj.getIsCrt());
				customerAnswers.setCreatedDateTime(new Date(obj.getCrDate()));
				if(null != obj.getUpDate()) {
					customerAnswers.setUpdatedDateTime(new Date(obj.getUpDate()));
				}
				customerAnswers.setIsLifeLineUsed(obj.getIsLife());
				customerAnswers.setAnswerRewards(null != obj.getaRwds()?obj.getaRwds():null);
				customerAnswers.setCumulativeRewards(obj.getCuRwds());
				customerAnswers.setCumulativeLifeLineUsed(obj.getCuLife());
				
				try {
					customerAnswers.setRetryCount(obj.getRetryCount());
					customerAnswers.setIsAutoCreated(obj.getIsAutoCreated());
					if(null != obj.getFbCallbackTime() && !obj.getFbCallbackTime().isEmpty()) {
						customerAnswers.setFbCallbackTime(new Date(Long.valueOf(obj.getFbCallbackTime())));
					}
					if(null != obj.getAnswerTime() && !obj.getAnswerTime().isEmpty()) {
						customerAnswers.setAnswerTime(new Date(Long.valueOf(obj.getAnswerTime())));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				toBeSavedCustAnsList.add(customerAnswers);
				
				Integer livesUsed = 0;
				
				if(null != obj.getIsLife() && obj.getIsLife()) {
					livesUsed = 1;
				}
				Double ansRewards = 0.00;
				if(null != obj.getaRwds()) {
					ansRewards = obj.getaRwds();
				}
				
				CustContestStat custContestStat = contestCustAnsMap.get(obj.getCuId());
				
				if(null == custContestStat) {
					custContestStat = new CustContestStat();
					custContestStat.setCumulativeAnsRewards(ansRewards);
					custContestStat.setLivesUsed(livesUsed);
				}else {
					custContestStat.setCumulativeAnsRewards(custContestStat.getCumulativeAnsRewards()+ansRewards);
					custContestStat.setLivesUsed(custContestStat.getLivesUsed() + livesUsed);
				}
				
				contestCustAnsMap.put(obj.getCuId(), custContestStat);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		Map<Integer, QuizContestWinners> contestWinnerMap = new HashMap<Integer, QuizContestWinners>(); 
		
		for (CassContestWinners obj : cassContWinnersList) {
			try {
				contestWinner = new QuizContestWinners();
				contestWinner.setCustomerId(obj.getCuId());
				contestWinner.setContestId(contestId);
				contestWinner.setCreatedDateTime(new Date(obj.getCrDated()));
				contestWinner.setRewardTickets(obj.getrTix());
				contestWinner.setRewardType(RewardType.POINTS);
				contestWinner.setRewardPoints(obj.getrPoints());
				contestWinner.setRewardRank(0);
				toBeSavedContWinnerList.add(contestWinner);
				contestWinnerMap.put(obj.getCuId(), contestWinner);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		/*if(null != toBeSavedParticipants && !toBeSavedParticipants.isEmpty()) {
			QuizDAORegistry.getQuizContestParticipantsDAO().saveAll(toBeSavedParticipants);
		} */
		
		if(null != toBeSavedCustAnsList && !toBeSavedCustAnsList.isEmpty()) {
			QuizDAORegistry.getQuizCustomerContestAnswersDAO().saveAll(toBeSavedCustAnsList);
		}
		
		if(null != toBeSavedContWinnerList && !toBeSavedContWinnerList.isEmpty()) {
			QuizDAORegistry.getQuizContestWinnersDAO().saveAll(toBeSavedContWinnerList);
		}
		
		if(null != toBeSavedGrandWinnerList && !toBeSavedGrandWinnerList.isEmpty()) {
			QuizDAORegistry.getContestGrandWinnerDAO().saveAll(toBeSavedGrandWinnerList);
		}
		
		
		Integer livesBeforeContest = null, livesUsed =null, livesAfterContest = null;
		Double contestQuesRewards=null,contestWinnerRewards=null;
		
		CustomerCassandra customerCassandra = null;
		CustomerLoyaltyTracking loyaltyRewardTracking = null;
		
		Date createDate = new Date();
		
		for(Integer custId : contestCustAnsMap.keySet()) {
			try {
				livesBeforeContest = 0;
				livesUsed =0;
				livesAfterContest = 0;
				
				contestQuesRewards=0.00;
				contestWinnerRewards=0.00;
				
				Customer customer = CustomerUtil.getCustomerById(custId);
				CustContestStat contestStatObj = contestCustAnsMap.get(custId);
				QuizContestWinners contestWinnerObj = contestWinnerMap.get(custId);
				if(null != contestWinnerObj) {
					contestWinnerRewards = contestWinnerObj.getRewardPoints();
				}
				contestQuesRewards = contestStatObj.getCumulativeAnsRewards();
				
				livesBeforeContest = customer.getQuizCustomerLives();
				livesUsed = contestStatObj.getLivesUsed();
				livesAfterContest = livesBeforeContest - livesUsed;
				/*
				System.out.println("CASSANDRA MIGRATION :- CustomerId: "+custId+", QueRewards : "+contestQuesRewards+", WinnerRewards: "+contestWinnerRewards
						+", LivesBeforeGame: "+livesBeforeContest+", LivesUsed: "+livesUsed+", LivesAfterGame: "+livesAfterContest);*/
				
				
				customerCassandra = new CustomerCassandra(custId, contestId, createDate);
				customerCassandra.setContestQusReward(contestQuesRewards);
				customerCassandra.setContestWinnerReward(contestWinnerRewards);
				customerCassandra.setCumulativeContestRewards(contestQuesRewards + contestWinnerRewards);
				customerCassandra.setLivesAfterContest(livesAfterContest);
				customerCassandra.setLivesBeforeContest(livesBeforeContest);
				customerCassandra.setLivesUsed(livesUsed);
				
				DAORegistry.getCustomerCassandraDAO().save(customerCassandra);
				
				customer.setQuizCustomerLives(livesAfterContest);
				
				DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),livesAfterContest);
				CustomerUtil.updatedCustomerUtil(customer);
				
				loyaltyRewardTracking = new CustomerLoyaltyTracking();
				loyaltyRewardTracking.setContestId(contestId);
				loyaltyRewardTracking.setCreatedBy("AUTO");
				loyaltyRewardTracking.setCreatedDate(createDate);
				loyaltyRewardTracking.setCustomerId(custId);
				loyaltyRewardTracking.setRewardPoints(customerCassandra.getCumulativeContestRewards());
				loyaltyRewardTracking.setRewardType("CONTEST");
				
				DAORegistry.getCustomerLoyaltyTrackingDAO().saveOrUpdate(loyaltyRewardTracking);
				
				Double contestRewards = customerCassandra.getCumulativeContestRewards();
				
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(custId);
				
				if(null == customerLoyalty) {
					continue;	
				}
				
				//System.out.println("Active Rewards Before Game: "+customerLoyalty.getActivePointsAsDouble());
				
				customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble() + contestRewards);
				customerLoyalty.setContestRewardDollarsDouble(customerLoyalty.getContestRewardDollarsDouble() + contestRewards);
				customerLoyalty.setLastUpdate(new Date());
				customerLoyalty.setLatestEarnedPointsAsDouble(contestRewards);
				customerLoyalty.setLatestSpentPointsAsDouble(0.00);
				customerLoyalty.setTotalEarnedPointsAsDouble(customerLoyalty.getTotalEarnedPointsAsDouble()+contestRewards);
				
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				
				try {
					CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndLives(customer.getId(), customer.getQuizCustomerLives(), customerLoyalty.getActivePointsAsDouble());
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				//System.out.println("Active Rewards After Game: "+customerLoyalty.getActivePointsAsDouble());
				
				//System.out.println("------------------------------------------------------------------------------------------------------");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		migrationStatObj.setEndDate(new Date());
		migrationStatObj.setStatus("COMPLETED");
		DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
		
		try {
			System.out.println("Contest participants Migration Starts : "+ new Date());
			CassContestUtil.updateCustomerPostContestDetailsinSQL(contestId);
			System.out.println("Contest participants Migration Ends : "+ new Date());
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		/* Grand Winner Gift Card Order Creation Code Start Here- By Ulaganathan*/
		try {
			if(null != toBeSavedGrandWinnerList && !toBeSavedGrandWinnerList.isEmpty() && null != contest.getGiftCardValueId() && contest.getGiftCardValueId() > 0 
					&& null != contest.getGiftCardPerWinner() && contest.getGiftCardPerWinner() > 0 ) {
				
				for (ContestGrandWinner grandWinner : toBeSavedGrandWinnerList) {
					try {
						if(null != grandWinner.getWinnerType() && grandWinner.getWinnerType().equals(WinnerType.REGULAR)) {
							QuizMiniJackpotWinnerCredit.createGiftCardOrder(contest, grandWinner.getCustomerId());
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
				} 
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		/* Grand Winner Gift Card Order Creation Code End Here- By Ulaganathan*/
		
		/* MINI & MEGA Jackpot Winner Credits Code Start Here- By Ulaganathan*/
		try {
			QuizJackpotWinnerUtil.creditJackpotWinnerBenefits(contest);
		}catch(Exception e) {
			e.printStackTrace();
		}
		/* MINI & MEGA Jackpot Winner Credits Code End Here- By Ulaganathan*/
		
	}
	
	public static void migrateContestParticipantsToSQL() throws Exception {
		try {
			List<ContestParticipants> contestParticipantsList = CassandraDAORegistry.getContestParticipantsDAO().getAllParticipants();
			ContestParticipantsCassandra participant = null; 
			List<ContestParticipantsCassandra> tempList = new ArrayList<ContestParticipantsCassandra>();
			int processCount =0;
			int sqlBatchThreshhold = 1000;
			
			for (ContestParticipants obj : contestParticipantsList) {
				try {
					
					participant = new ContestParticipantsCassandra();
					participant.setContestId(obj.getCoId());
					participant.setCustomerId(obj.getCuId());
					participant.setPlatform(obj.getPfm());
					participant.setIpAddress(obj.getIpAdd());
					if(obj.getJnDate() != null) {
						participant.setJoinDateTime(new Date(obj.getJnDate()));	
					}
					if(null != obj.getExDate()) {
						participant.setExitDateTime(new Date(obj.getExDate()));
					} 
					participant.setStatus(obj.getStatus());
	
					tempList.add(participant);
					processCount++;
					//QuizDAORegistry.getContestParticipantsCassandraDAO().save(participant);
					
					if(processCount % sqlBatchThreshhold == 0) {
						
						ContestParticipantsCassandraSQLDAO.saveAllContestParticipantsCassandra(tempList);
						tempList = new ArrayList<ContestParticipantsCassandra>();
						processCount = 0;
						
					}
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(processCount > 0) {
				ContestParticipantsCassandraSQLDAO.saveAllContestParticipantsCassandra(tempList);
				tempList = new ArrayList<ContestParticipantsCassandra>();
				processCount = 0;
			}
			int size = 0;
			if(contestParticipantsList != null) {
				size = contestParticipantsList.size();
			}
			System.out.println("Contest Participants Caas Migrated  : "+size+" : "+ new Date());
		} catch(Exception e) {
			System.out.println("Error in Contest Participants Caas Migration  : "+ new Date());
			e.printStackTrace();
		}
		
		//System.out.println("Started Truncate RTF API TRACKING Start: "+new Date());
		//CassandraDAORegistry.getContestParticipantsDAO().truncate();
		//System.out.println("Started Truncate RTF API TRACKING END: "+new Date());
	}
	
	public static void migrateContestPasswordAuthToSQL() throws Exception {
		try {
			List<ContestPasswordAuth> contPasswordAuthList = CassandraDAORegistry.getContestPasswordAuthDAO().getAll();
			QuizContestPasswordAuth quizContPwdAuth = null; 
			List<QuizContestPasswordAuth> tempList = new ArrayList<QuizContestPasswordAuth>();
			int processCount =0;
			int sqlBatchThreshhold = 1000;
			
			for (ContestPasswordAuth obj : contPasswordAuthList) {
				try {
					
					quizContPwdAuth = new QuizContestPasswordAuth();
					quizContPwdAuth.setContestId(obj.getCoId());
					quizContPwdAuth.setCustomerId(obj.getCuId());
					quizContPwdAuth.setPassword(obj.getPwd());
					quizContPwdAuth.setIsAuth(obj.getIsAuth());
					if(obj.getCrDate() != null) {
						quizContPwdAuth.setCreatedDate(new Date(obj.getCrDate()));	
					}
					if(null != obj.getUpDate()) {
						quizContPwdAuth.setUpdatedDate(new Date(obj.getUpDate()));
					} 
	
					tempList.add(quizContPwdAuth);
					processCount++;
					//QuizDAORegistry.getContestParticipantsCassandraDAO().save(participant);
					
					if(processCount % sqlBatchThreshhold == 0) {
						
						QuizContestPasswordAuthSQLDAO.saveAllQuizContestPasswordAuth(tempList);
						tempList = new ArrayList<QuizContestPasswordAuth>();
						processCount = 0;
						
					}
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(processCount > 0) {
				QuizContestPasswordAuthSQLDAO.saveAllQuizContestPasswordAuth(tempList);
				tempList = new ArrayList<QuizContestPasswordAuth>();
				processCount = 0;
			}
			int size = 0;
			if(contPasswordAuthList != null) {
				size = contPasswordAuthList.size();
			}
			System.out.println("Contest password auth Caas Migrated  : "+size+" : "+ new Date());
		} catch(Exception e) {
			System.out.println("Error in Contest password auth Caas Migrate  : "+ new Date());
			e.printStackTrace();
		}
		
		//System.out.println("Started Truncate RTF API TRACKING Start: "+new Date());
		//CassandraDAORegistry.getContestParticipantsDAO().truncate();
		//System.out.println("Started Truncate RTF API TRACKING END: "+new Date());
	}
	
public static String updatePostMigrationStatusonServlet(Integer contestId) throws Exception {
		
		Map<String, String> map = URLUtil.getParameterMap();
		map.put("coId", ""+contestId);
		
		/*String data = URLUtil.getObject(map, URLUtil.apiServerBaseUrl+"rtfcontest/PostMigrationStatsUpdate.json");
		//Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonCustomDateSerializer()).create();
		//JsonObject jsonObject = gson.fromJson(data, JsonObject.class);
		//CommonRespInfo commonRespInfo = gson.fromJson(((JsonObject)jsonObject.get("commonRespInfo")), CommonRespInfo.class);
		
		ObjectMapper objectMapper = new ObjectMapper();
		CommonRespInfo commonRespInfo = objectMapper.readValue(data, CommonRespInfo.class);
		
		if(commonRespInfo.getSts() != 1){
			if(commonRespInfo.getErr() != null) {
				return commonRespInfo.getErr().getDesc();		
			}
		}*/
		String resMsg = "";
		CommonRespInfo commonRespInfo = null;
		List<RtfConfigContestClusterNodes> clusterNodeList = URLUtil.rtfContestClusterNodesList;
		/*if(URLUtil.isProductionEnvironment) {
			clusterNodeList.add(new RtfConfigContestClusterNodes(0,"http://10.0.0.34:8081/rtfcontest/",1));
			clusterNodeList.add(new RtfConfigContestClusterNodes(0,"http://10.0.0.34:8082/rtfcontest/",1));
		}*/
		for (RtfConfigContestClusterNodes rtfContestNodes : clusterNodeList) {
			try {
				String data = URLUtil.getObject(map, rtfContestNodes.getUrl()+"PostMigrationStatsUpdate.json");
				ObjectMapper objectMapper = new ObjectMapper();
				commonRespInfo = objectMapper.readValue(data, CommonRespInfo.class);
				if(commonRespInfo.getSts() != 1){
					if(commonRespInfo.getErr() != null) {
						String msg = rtfContestNodes.getUrl()+":"+commonRespInfo.getErr().getDesc(); 
						resMsg = resMsg + msg + ",";
						System.out.println("POST MIG ERROR : "+msg +" : "+data);
					}
				}
			} catch (Exception e) {
				e. printStackTrace();
			}
		}
		
		if(resMsg.equals("")) {
			resMsg = null;
		}
		
		return null;
	}
	
	
	@RequestMapping(value = "/MigrateApiTrackingToSql")
	public @ResponsePayload DashboardInfo migrateApiTrackingToSql(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "";
		try {
			migrateTrackingDetailsToSQL();
			dashboardInfo.setSts(1);
			resMsg = "Contest Data's are migrated from cassandra to sql server for contest id :";
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating contest data from cassandra to sql server";
			e.printStackTrace();
			error.setDesc("Error occured while migrating contest data from cassandra to sql server");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("MIGRATING CONTEST DATA FROM CASSANDRA TO SQL  :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	}
	
	public void migrateTrackingDetailsToSQL() throws Exception {
		
		int sqlBatchThreshhold = 1000;
		/*Contest PArticipants Migration to SQL - starts */
		try {
			migrateContestParticipantsToSQL();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*Contest PArticipants Migration to SQL - Ends */
		
		/*Contest Password Auth Migration to SQL - starts */
		try {
			migrateContestPasswordAuthToSQL();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*Contest Password Auth Migration to SQL - Ends */
		
		try {
			List<CassRtfApiTracking> trackingList = CassandraDAORegistry.getCassRtfApiTrackingDAO().getAll();
			WebServiceTrackingCassandra obj = null; 
			List<WebServiceTrackingCassandra> tempList = new ArrayList<WebServiceTrackingCassandra>();
			int processCount = 0;
			
			for (CassRtfApiTracking cassObj : trackingList) {
				try {
					obj = new WebServiceTrackingCassandra();
					obj.setActionResult(cassObj.getActionResult());
					obj.setApiName(cassObj.getApiName());
					obj.setContestId(null != cassObj.getContestId()?cassObj.getContestId():null);
					obj.setCustId(null != cassObj.getCustId()?cassObj.getCustId():null);
					obj.setCustIPAddr(cassObj.getCustIPAddr());
					obj.setDescription(cassObj.getDescription());
					obj.setDeviceInfo(cassObj.getDeviceInfo());
					obj.setDeviceType(cassObj.getDeviceType());
					obj.setPlatForm(cassObj.getPlatForm());
					obj.setSessionId(cassObj.getSessionId());
					obj.setAppVersion(null != cassObj.getAppVerion()?cassObj.getAppVerion():"");
					obj.setResponseStatus(null != cassObj.getResponseStatus()?cassObj.getResponseStatus():null);
					obj.setQuestionNo(null != cassObj.getQuestionNo()?cassObj.getQuestionNo():"");
					obj.setRetryCount(cassObj.getRetryCount());
					obj.setAnsCount(cassObj.getAnsCount());
					obj.setNodeId(cassObj.getNodeId());
					try {
						if(null != cassObj.getStartDateStr() && !cassObj.getStartDateStr().isEmpty()) {
							obj.setStartDate(df.parse(cassObj.getStartDateStr()));
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					try {
						if(null != cassObj.getEndDateStr() && !cassObj.getEndDateStr().isEmpty()) {
							obj.setEndDate(df.parse(cassObj.getEndDateStr()));
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					try {
						if(null != cassObj.getFbCallbackTime() && !cassObj.getFbCallbackTime().isEmpty()) {
							Long fbCallBackTime = Long.valueOf(cassObj.getFbCallbackTime());
							obj.setFbCallbackTime(new Date(fbCallBackTime));
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					try {
						if(null != cassObj.getDeviceAPIStartTime() && !cassObj.getDeviceAPIStartTime().isEmpty()) {
							Long deviceAPIStartTime = Long.valueOf(cassObj.getDeviceAPIStartTime());
							obj.setDeviceAPIStartTime(new Date(deviceAPIStartTime));
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
					
					tempList.add(obj);
					processCount++;
					//DAORegistry.getWebServiceTrackingCassandraDAO().save(obj);
					
					if(processCount % sqlBatchThreshhold == 0) {
						WebServiceTrackingCassandraSQLDAO.saveAllRtfTracking(tempList);
						tempList = new ArrayList<WebServiceTrackingCassandra>();
						processCount = 0;
					}
					
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			if(processCount > 0) {
				WebServiceTrackingCassandraSQLDAO.saveAllRtfTracking(tempList);
				tempList = new ArrayList<WebServiceTrackingCassandra>();
				processCount = 0;
			}
			
			//System.out.println("Started Truncate RTF API TRACKING Start: "+new Date());
			CassandraDAORegistry.getCassRtfApiTrackingDAO().truncate();
			//System.out.println("Started Truncate RTF API TRACKING END: "+new Date());
			
			int size = 0;
			if(trackingList != null) {
				size = trackingList.size();
			}
			System.out.println("RTF Tracking List Migrated Migrated  : "+size+" : "+ new Date());
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error in RTF Tracking Migration : "+ new Date());
		}
			
		try {
			migrateFbCallBackTrackingDetailsToSQL();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void migrateFbCallBackTrackingDetailsToSQL() {
		List<CassFbCallBackTracking> trackingList = CassandraDAORegistry.getCassFbCallBackTrackingDAO().getAllFbCallBackTrackings();
		QuizFirebaseCallBackTracking firebaseTracking = null; 
		for (CassFbCallBackTracking cassObj : trackingList) {
			try {
				firebaseTracking = new QuizFirebaseCallBackTracking();
				firebaseTracking.setCustomerId(cassObj.getCuId());
				firebaseTracking.setContestId(cassObj.getCoId());
				firebaseTracking.setQuestionNo(cassObj.getqNo());
				firebaseTracking.setIpAddress(cassObj.getIpAddr());
				firebaseTracking.setDeviceModel(cassObj.getdModel());
				firebaseTracking.setOsVersion(cassObj.getOsVer());
				firebaseTracking.setPlatform(cassObj.getPlatform());
				firebaseTracking.setCallBackType(cassObj.getCbType());
				firebaseTracking.setMessage(cassObj.getMessage());
				
				try {
					if(null != cassObj.getCreateDate() && cassObj.getCreateDate() > 0) {
						firebaseTracking.setCreateDate(new Date(cassObj.getCreateDate()));
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				QuizDAORegistry.getQuizFirebaseCallBackTrackingDAO().save(firebaseTracking);
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		//System.out.println("Started Truncate RTF API TRACKING Start: "+new Date());
		CassandraDAORegistry.getCassFbCallBackTrackingDAO().truncate();
		//System.out.println("Started Truncate RTF API TRACKING END: "+new Date());
	}
	 
	
	@RequestMapping(value = "/ForceMigrationToSqlFromCass")
	public @ResponsePayload DashboardInfo forceMigrationToSqlFromCass(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		String resMsg = "", contestIdStr =request.getParameter("contestId");
		String isEndedSuccessfullyStr = request.getParameter("isEndedSuccessfully");
		try {
			CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
			Integer contestId = Integer.parseInt(contestIdStr.trim());
			if(null == isEndedSuccessfullyStr || isEndedSuccessfullyStr.isEmpty()) {
				isEndedSuccessfullyStr = "false";
			}
			Boolean contestEndSuccessfuly = Boolean.valueOf(isEndedSuccessfullyStr);
			
			ContestMigrationStats migrationStatObj = null;
			
			if(contestEndSuccessfuly) {
				migrationStatObj = new ContestMigrationStats();
				migrationStatObj.setContestId(contestId);
				migrationStatObj.setStartDate(start);
				migrationStatObj.setStatus("RUNNING");
				migrationStatObj.setMigrationType("RTFTRACKINGDATA");
				DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
			}
			
			/*try {
				
				QuizContestParticipants participant = null;
				
				List<QuizContestParticipants> toBeSavedParticipants = new ArrayList<QuizContestParticipants>();
				List<ContestParticipants> cassContPrticipantsList = CassandraDAORegistry.getContestParticipantsDAO().getAllParticipantsByContestId(contestId);
				if(null != cassContPrticipantsList && !cassContPrticipantsList.isEmpty() && cassContPrticipantsList.size() > 0) {
					Map<Integer, Boolean> participantIdMap =  new HashMap<Integer, Boolean>();
					for (ContestParticipants obj : cassContPrticipantsList) {
						try {
							participant = new QuizContestParticipants();
							participant.setContestId(contestId);
							participant.setCustomerId(obj.getCuId());
							participant.setPlatform(obj.getPfm());
							participant.setIpAddress(obj.getIpAdd());
							participant.setJoinDateTime(new Date(obj.getJnDate()));
							if(null != obj.getExDate() && obj.getExDate() > 0) {
								participant.setExitDateTime(new Date(obj.getExDate()));
							} 
							participant.setStatus(obj.getStatus());
							toBeSavedParticipants.add(participant);
							
							participantIdMap.put(obj.getCuId(), true);
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					if(null != toBeSavedParticipants && !toBeSavedParticipants.isEmpty()) {
						//System.out.println("TobeSaved Participants: "+toBeSavedParticipants.size());
						QuizDAORegistry.getQuizContestParticipantsDAO().saveAll(toBeSavedParticipants);
						//System.out.println("Started Truncate Contest Participants Start: "+new Date());
						
						//System.out.println("Started Truncate Contest Participants End: "+new Date());
						if(null != participantIdMap && !participantIdMap.isEmpty() && participantIdMap.size() > 0) {
							for (Integer custId : participantIdMap.keySet()) {
								try {
									Customer customer = CustomerUtil.getCustomerById(custId);
									if(null != customer) {
										Integer lives = customer.getQuizCustomerLives() + 1;
										try {
											DAORegistry.getCustomerDAO().updateQuizCustomerLives(customer.getId(),lives);
											CustomerUtil.updatedCustomerUtil(customer);
										}catch(Exception e) {
											e.printStackTrace();
										}
										CassandraDAORegistry.getCassCustomerDAO().updateCustomerLives(custId, lives);
									}
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
						}
					} 
				}
			}catch(Exception e) {
				e.printStackTrace();
			}*/
			
			migrateTrackingDetailsToSQL();
			 
			if(contestEndSuccessfuly) {
				
				migrationStatObj.setEndDate(new Date());
				migrationStatObj.setStatus("COMPLETED");
				DAORegistry.getContestMigrationStatsDAO().update(migrationStatObj);
				
				try {
					
					Boolean contestProcessed = QuizCreditUtilThread.getProcessedContestMap(contestId);
					System.out.println("QUIZ CONTEST CREDITS : contestProcessed: "+contestProcessed);
					
					if(null == contestProcessed ) {
						QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
						String message = "";
						try {
							System.out.println("QUIZ CONTEST CREDITS: THREAD Started at : "+new Date()+", ContestID: "+contestIdStr);
							QuizCreditUtilThread  quizCreditUtilThread = new QuizCreditUtilThread(contest,contestId);
							Thread t = new Thread(quizCreditUtilThread);
							t.start();
							System.out.println("QUIZ CONTEST CREDITS: THREAD Ended at : "+new Date()+", ContestID: "+contestIdStr);
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				
			}
			dashboardInfo.setSts(1);
			resMsg = "Contest Data's are migrated from cassandra to sql server:";
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while migrating contest data from cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while migrating contest data from cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("FORCE MIGRATING CONTEST DATA FROM CASSANDRA TO SQL  :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		
		return dashboardInfo;
	
	}
	
	 
	@RequestMapping(value = "/GetCustomerStatsDetails", method=RequestMethod.POST)
	public @ResponsePayload QuizCustomerStatsInfo getCustomerStatsDetails(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizCustomerStatsInfo quizCustomerStatsInfo = new QuizCustomerStatsInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizCustomerStatsInfo.setError(authError);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,authError.getDescription());
				return quizCustomerStatsInfo;
			}*/
			
			String customerIdStr = request.getParameter("customerId");
			String friendCustomerIdStr = request.getParameter("friendCustomerId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Customer Id is mandatory");
				return quizCustomerStatsInfo;
			}
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Question No is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERINFO,"Question No is mandatory");
				return quizCustomerStatsInfo;
			}*/
			if(TextUtil.isEmptyOrNull(friendCustomerIdStr)){
				error.setDescription("Friend Customer Id is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.GETCUSTOMERINFO,"Friend Customer Id is mandatory");
				return quizCustomerStatsInfo;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Customer Id is Invalid");
				return quizCustomerStatsInfo;
			}
			
			Customer friendCustomer = CustomerUtil.getCustomerById(Integer.parseInt(friendCustomerIdStr));
			if(friendCustomer == null) {
				error.setDescription("Friend Customer Id is Invalid");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Friend Customer Id is Invalid");
				return quizCustomerStatsInfo;
			}
			String friendStatus ="Add Friend";// DAORegistry.getQueryManagerDAO().getcustomerFrientStatus(customer.getId(), friendCustomer.getId());
			Boolean isSender = false;
			
			if(customer.getId().equals(friendCustomer.getId())) {
				friendStatus = FriendStatus.SameCustoemr.toString();
			} else {
	 			List<QuizCustomerFriend> custoemrFriends = DAORegistry.getQuizCustomerFriendDAO().getQuizCustomerFriendRecords(customer.getId(), friendCustomer.getId());
				if(custoemrFriends != null) {
					for (QuizCustomerFriend customerFriend : custoemrFriends) {
						if(customerFriend.getStatus().equals(FriendStatus.Requested) ||
								customerFriend.getStatus().equals(FriendStatus.Friend)) {
							
							friendStatus = customerFriend.getStatus().toString();
							
							if(customerFriend.getCustomerId().equals(customer.getId())) {
								isSender = customerFriend.getIsSender();	
							} else {
								isSender = !customerFriend.getIsSender();
							}
						}
					}
				}
			}
			if(!isSender && friendStatus.equals(FriendStatus.Requested.toString())) {
				friendStatus = FriendStatus.Accept.toString();
			}
			CustomerStatsDetails custoemrStats = new CustomerStatsDetails(friendCustomer);
			custoemrStats.setFriendStatus(friendStatus);
			custoemrStats.setIsSender(isSender);
			quizCustomerStatsInfo.setCustomerStatsDetails(custoemrStats);
			
			quizCustomerStatsInfo.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Stats Details.");
			quizCustomerStatsInfo.setError(error);
			quizCustomerStatsInfo.setStatus(0);
			TrackingUtils.contestAPITracking(request, WebServiceActionType.QUIZGETCUSTOMERSTATS,"Error occured while Fetching Customer Stats Details.");
			return quizCustomerStatsInfo;
		}
		log.info("QUIZ CUT STATS DTLS : "+request.getParameter("customerId")+" :fCId: "+request.getParameter("friendCustomerId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizCustomerStatsInfo;
	
	}
	
	@RequestMapping(value = "/SubmitTrivia", method=RequestMethod.POST)
	public @ResponsePayload QuizGenericResponse submitContestQuestion(HttpServletRequest request,HttpServletResponse response){
		QuizGenericResponse genericResponse =new QuizGenericResponse();
		Error error = new Error();
		Date start = new Date();
		try {
			String questionText = request.getParameter("questionText");
			String optionA = request.getParameter("optionA");
			String optionB = request.getParameter("optionB");
			String optionC = request.getParameter("optionC");
			String customerIdStr = request.getParameter("customerId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Customer Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Customer Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			Integer customerId=null;
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Customer Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send valid Customer Id.");
				return genericResponse;
			}
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer== null){
				genericResponse.setStatus(0);
				error.setDescription("Customer not found with given Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Customer not found with given Id.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(questionText)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Question Text.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Question Text.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(optionA)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Answer.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Answer.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(optionB)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Option B.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Option B.");
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(optionC)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Option C.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Please send Option C.");
				return genericResponse;
			}
			
			CustomerContestQuestion question = new CustomerContestQuestion();
			question.setQuestionText(questionText);
			question.setOptionA(optionA);
			question.setOptionB(optionB);
			question.setOptionC(optionC);
			question.setCreatedDate(new Date());
			question.setAnswer("A");
			question.setSubmittedBy(customer.getUserId());
			
			QuizDAORegistry.getCustomerContestQuestionDAO().save(question);
			genericResponse.setStatus(1);
			genericResponse.setMessage("Your Trivia Question submitted.");
			return genericResponse;
			
		} catch (Exception e) {
			e.printStackTrace();
			genericResponse.setStatus(0);
			error.setDescription("Error Occured while Submitting trivia.");
			genericResponse.setError(error);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZSUBMITTRIVIA,"Error Occured while Submitting trivia.");
		}
		log.info("QUIZ SBT TRIVIA : "+request.getParameter("customerId")+" :qtex: "+request.getParameter("questionText")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return genericResponse;
	}
	
	

	@RequestMapping(value = "/CreateWinnerOrder", method=RequestMethod.POST)
	public @ResponsePayload QuizGenericResponse createOrderForWinner(HttpServletRequest request,HttpServletResponse response){
		QuizGenericResponse genericResponse =new QuizGenericResponse();
		genericResponse.setIsShippingAddress(true);
		Error error = new Error();
		Date start = new Date();
		try {
			String customerIdStr = request.getParameter("customerId");
			String eventIdStr = request.getParameter("eventId");
			String contestIdStr = request.getParameter("contestId");
			String platform = request.getParameter("platForm");
			String shippingAddressIdStr = request.getParameter("shippingAddressId");
			
			String grandWinnerIdStr = request.getParameter("grandWinnerId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Customer Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send Customer Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Event Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send Event Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please send Contest Id.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send Contest Id.");
				genericResponse.setError(error);
				return genericResponse;
			}
			if(TextUtil.isEmptyOrNull(platform)){
				genericResponse.setStatus(0);
				error.setDescription("Please send platform.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send platform.");
				genericResponse.setError(error);
				return genericResponse;
			}
			
			
			ApplicationPlatForm platForm = null;
			Integer customerId=null;
			Integer eventId=null;
			Integer contestId=null;
			try {
				platForm = ApplicationPlatForm.valueOf(platform);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid platform.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid platform.");
				genericResponse.setError(error);
				return genericResponse;
			}
			try {
				customerId = Integer.parseInt(customerIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Customer Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid Customer Id.");
				return genericResponse;
			}
			try {
				eventId = Integer.parseInt(eventIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Event Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid Event Id.");
				return genericResponse;
			}
			try {
				contestId = Integer.parseInt(contestIdStr);
			} catch (Exception e) {
				genericResponse.setStatus(0);
				error.setDescription("Please send valid Contest Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid Contest Id.");
				return genericResponse;
			}
			
			Customer customer = CustomerUtil.getCustomerById(customerId);
			if(customer== null){
				genericResponse.setStatus(0);
				error.setDescription("Customer not found with given Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Customer not found with given Id.");
				return genericResponse;
			}
			Event event = DAORegistry.getEventDAO().get(eventId);
			if(event== null){
				genericResponse.setStatus(0);
				error.setDescription("Event not found with given Id.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Event not found with given Id.");
				return genericResponse;
			}
			
			ContestGrandWinner winner = null;
			if(grandWinnerIdStr != null && !grandWinnerIdStr.isEmpty()) {
				try {
					winner = QuizDAORegistry.getContestGrandWinnerDAO().get(Integer.parseInt(grandWinnerIdStr));
				} catch (Exception e) {
					genericResponse.setStatus(0);
					error.setDescription("Please send valid Grand Winner ID.");
					genericResponse.setError(error);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please send valid Grand Winner ID.");
					return genericResponse;
				}
			}else {
				winner = QuizDAORegistry.getContestGrandWinnerDAO().getGrandWinnerByCustomerId(customerId,contestId);
			}
			
			CustomerLoyalty loyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			
			if(winner== null){
				genericResponse.setStatus(0);
				error.setDescription("Winner Record is not found for given customer.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Winner Record is not found for given customer.");
				return genericResponse;
			}
			
			
			if(TextUtil.isEmptyOrNull(shippingAddressIdStr)){
				genericResponse.setStatus(0);
				error.setDescription("Please choose valid shipping address.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please choose valid shipping address");
				return genericResponse;
			}
			
			Integer shippingAddressId = null;
			UserAddress shipping = null;
			try {
				shippingAddressId = Integer.parseInt(shippingAddressIdStr.trim());
				shipping = DAORegistry.getUserAddressDAO().get(shippingAddressId);
			}catch(Exception e){
				genericResponse.setStatus(0);
				error.setDescription("Please choose valid shipping address.");
				genericResponse.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Please choose valid shipping address");
				return genericResponse;
			}
			
			
			Date today = new Date();
			
			CategoryTicketGroup tgGroup = new CategoryTicketGroup();
			tgGroup.setEventId(event.getEventId());
			
			tgGroup.setOriginalPrice(0.00);
			tgGroup.setSectionRange("TBD");
			tgGroup.setRowRange("TBD");
			tgGroup.setStatus(TicketStatus.SOLD);
			tgGroup.setShippingMethod("ETICKETS");
			tgGroup.setCreatedBy("Contest");
			tgGroup.setCreatedDate(today);
			tgGroup.setLastUpdatedDate(today);
			tgGroup.setSoldPrice(0.00);
			tgGroup.setProducttype(ProductType.REWARDTHEFAN);
			tgGroup.setSoldQuantity(winner.getRewardTickets());
			tgGroup.setQuantity(winner.getRewardTickets());
			tgGroup.setInternalNotes("Contest Ticket");
			tgGroup.setFacePrice(0.00);
			tgGroup.setCost(0.00);
			tgGroup.setWholeSalePrice(0.00);
			tgGroup.setRetailPrice(0.00);
			tgGroup.setBroadcast(true);
			tgGroup.setOriginalTax(0.00);
			tgGroup.setOriginalTaxPerc(0.00);
			tgGroup.setActualSection("TBD");
			tgGroup.setRow(null);
			tgGroup.setLongSection(null);
			tgGroup.setBrokerId(1001);
			
			/*CustomerOrderRequest orderRequest = new CustomerOrderRequest();
			//Setting up Ticket Original Price
			orderRequest.setTicketPrice(originalTicketPrice);
			orderRequest.setTicketOnDayBeforeTheEvent(false);
			orderRequest.setSessionId(sessionId);
			orderRequest.setOrderId(customerOrder.getId());
			orderRequest.setOrderLoyaltyHistoryId(customerLoyaltyHistory.getId());
			orderRequest.setOrderLoyaltyInfoId(customerLoyalty.getId());
			orderRequest.setOrderTotal(serverOrderTotal);
			orderRequest.setOrderType(orderType);
			orderRequest.setOriginalOrderTotal(originalOrderTotal);
			orderRequest.setAppPlatForm(customerOrder.getAppPlatForm());
			orderRequest.setCategoryTicketGroupId(categoryTicketGroupId);
			orderRequest.setCustomerId(customerId);
			orderRequest.setEventId(eventId);
			orderRequest.setInvoiceId(-1);
			orderRequest.setIsLongSale(false);
			orderRequest.setIsLoyalFan(isFanPrice);
			orderRequest.setPrimaryPayAmt(primaryPayAmt);
			orderRequest.setPrimaryPaymentMethod(primaryPaymentMethod);
			orderRequest.setPrimaryTransactionId(primaryTransactionId);
			orderRequest.setSecondaryPayAmt(secondaryPayAmt);
			orderRequest.setSecondaryPaymentMethod(secondaryPaymentMethod);
			orderRequest.setSecondaryTransactionId(secondaryTransactionId);
			orderRequest.setQuantity(customerOrder.getQty());
			orderRequest.setSoldPrice(soldPrice);
			orderRequest.setThirdPayAmt(customerOrder.getThirdPayAmtAsDouble());
			orderRequest.setThirdPaymentMethod(customerOrder.getThirdPaymentMethod());
			orderRequest.setThirdTransactionId(customerOrder.getThirdTransactionId());
			orderRequest.setTicketGroupId(null);
			
			String customerIpAddress = "";
			if(customerOrder.getAppPlatForm().equals(ApplicationPlatForm.IOS) || 
					customerOrder.getAppPlatForm().equals(ApplicationPlatForm.ANDROID)){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			orderRequest.setIpAddress(customerIpAddress);
			orderRequest.setStatus(OrderStatus.PAYMENT_PENDING);
			orderRequest.setCreatedTime(new Date());
			orderRequest.setUpdatedTime(new Date());*/
			
			
			
			Invoice invoice = new Invoice();
			invoice.setInvoiceTotal(0.00);
			invoice.setCustomerId(customerId);
			invoice.setStatus(InvoiceStatus.Outstanding);
			invoice.setInvoiceType("CONTEST");
			invoice.setCreatedDate(today);
			invoice.setLastUpdated(today);
			invoice.setCreatedBy(customer.getUserId());
			invoice.setRealTixMap("NO");
			invoice.setLastUpdatedBy(customer.getUserId());
			invoice.setIsSent(false);
			invoice.setTicketCount(winner.getRewardTickets());
			invoice.setShippingMethodId(1);
			invoice.setRealTixDelivered(false);
			invoice.setBrokerId(1001);
			
			
			CustomerOrder order = new CustomerOrder();
			order.setCustomer(customer);
			order.setQty(winner.getRewardTickets());
			order.setStatus(OrderStatus.ACTIVE);
			order.setSection("TBD");
			order.setEventId(event.getEventId());
			order.setEventName(event.getEventName());
			order.setEventDateTemp(event.getEventDate());
			order.setEventTime(event.getEventTime());
			order.setVenueId(event.getVenueId());
			order.setVenueName(event.getVenueName());
			order.setDiscountAmount(0.00);
			order.setCreateDateTemp(today);
			order.setLastUpdatedDateTemp(today);
			order.setProductType(ProductType.REWARDTHEFAN);
			order.setShippingMethod("ETICKETS");
			order.setVenueCity(event.getCity());
			order.setVenueState(event.getState());
			order.setVenueCountry(event.getCountry());
			order.setSectionDescription("");
			order.setOriginalOrderTotal(0.00);
			order.setPrimaryPaymentMethod(PaymentMethod.CONTEST_WINNER);
			
			Property property =  DAORegistry.getPropertyDAO().get("rtf.contest.order.payment.suffix");
			Long primaryTrxId =  Long.valueOf(property.getValue());
			primaryTrxId++;
			String trxId = PaginationUtil.contestOrderPaymentPrefix+"-"+contestId+"-"+primaryTrxId;
			property.setValue(String.valueOf(primaryTrxId));
			
			order.setPrimaryTransactionId(trxId);
			order.setVenueCategory(event.getVenueCategoryName());
			order.setAppPlatForm(platForm);
			order.setIsInvoiceSent(false);
			order.setOrderType(OrderType.CONTEST);
			order.setOrderTotalAsDouble(0.00);
			order.setPrimaryPayAmtAsDouble(0.00);
			order.setSecondaryPayAmtAsDouble(0.00);
			order.setTaxesAsDouble(0.00);
			order.setTicketSoldPriceAsDouble(0.00);
			order.setTicketPriceAsDouble(0.00);
			order.setThirdPayAmtAsDouble(0.00);
			order.setIsLongSale(false);
			order.setIsTicketDownloadSent(false);
			order.setIsOrderCancelSent(false);
			order.setBrokerId(1001);
			order.setBrokerServicePerc(0.00);
			order.setIsPromoCodeApplied(false);
			order.setShowDiscPriceArea(false);
			order.setNormalTixPrice(0.00);
			order.setDiscountedTixPrice(0.00);
			Date date = new DateTime(event.getEventDate()).minusDays(1).toDate();
			order.setShippingDateTemp(date);
			order.setTicketOnDayBeforeTheEvent(false);
			
			/*Ulaganathan : Order Payment breakups - Begins*/
			OrderPaymentBreakup paymentBreakUp = new OrderPaymentBreakup();
			paymentBreakUp.setPaymentAmount(0.00);
			paymentBreakUp.setPaymentMethod(PaymentMethod.CONTEST_WINNER);
			paymentBreakUp.setTransactionId(trxId);
			paymentBreakUp.setDoneBy("Customer");
			paymentBreakUp.setOrderSequence(1);
			paymentBreakUp.setPaymentDate(new Date());
			/*Ulaganathan : Order Payment breakups - Ends*/
			
			//List<UserAddress> billings = DAORegistry.getUserAddressDAO().getUserAddressInfo(customerId, AddressType.BILLING_ADDRESS);
			Boolean isShippingAddress = true,updateCustTopLevelObj = false;
			if(shipping == null ){
				genericResponse.setIsShippingAddress(false);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Customer Shipping address is not found.");
				isShippingAddress =false;
			}
			
			CustomerOrderDetail orderDetail = new CustomerOrderDetail();
			orderDetail.setBillingAddress1("");
			orderDetail.setBillingAddress2("");
			orderDetail.setBillingCity("");
			orderDetail.setBillingCountry("");
			orderDetail.setBillingCountryId(null);
			orderDetail.setBillingEmail("");
			orderDetail.setBillingFirstName("");
			orderDetail.setBillingLastName("");
			orderDetail.setBillingPhone1("");
			orderDetail.setBillingPhone2("");
			orderDetail.setBillingState("");
			orderDetail.setBillingStateId(null);
			orderDetail.setBillingZipCode("");
			orderDetail.setOrderType(OrderType.TRIVIAORDER.toString());
			if(isShippingAddress){
				
				if(null == customer.getCustomerName() || customer.getCustomerName().isEmpty()){
					customer.setCustomerName(shipping.getFirstName());
					customer.setLastName(shipping.getLastName());
					
					updateCustTopLevelObj = true;
				}
				
				orderDetail.setShippingAddress1(shipping.getAddressLine1());
				orderDetail.setShippingAddress2(shipping.getAddressLine2());
				orderDetail.setShippingCity(shipping.getCity());
				orderDetail.setShippingCountry(shipping.getCountryName());
				orderDetail.setShippingCountryId(shipping.getCountry().getId());
				orderDetail.setShippingEmail(shipping.getEmail());
				orderDetail.setShippingFirstName(shipping.getFirstName());
				orderDetail.setShippingLastName(shipping.getLastName());
				orderDetail.setShippingPhone1(shipping.getPhone1());
				orderDetail.setShippingPhone2(shipping.getPhone2());
				orderDetail.setShippingState(shipping.getStateName());
				orderDetail.setShippingStateId(shipping.getState().getId());
				orderDetail.setShippingZipCode(shipping.getZipCode());
			}else{
				orderDetail.setShippingAddress1("");
				orderDetail.setShippingAddress2("");
				orderDetail.setShippingCity("");
				orderDetail.setShippingCountry("");
				orderDetail.setShippingCountryId(null);
				orderDetail.setShippingEmail("");
				orderDetail.setShippingFirstName("");
				orderDetail.setShippingLastName("");
				orderDetail.setShippingPhone1("");
				orderDetail.setShippingPhone2("");
				orderDetail.setShippingState("");
				orderDetail.setShippingStateId(null);
				orderDetail.setShippingZipCode("");
			}
			
			CustomerLoyaltyHistory history = new CustomerLoyaltyHistory();
			history.setCustomerId(customerId);
			history.setOrderTotal(0.00);
			history.setRewardConversionRate(0.01);
			history.setRewardEarnAmount(0.00);
			history.setRewardSpentAmount(0.00);
			history.setInitialRewardAmount(loyalty.getActivePointsAsDouble());
			history.setBalanceRewardAmount(loyalty.getActivePointsAsDouble());
			history.setDollarConversionRate(0.00);
			history.setOrderType(OrderType.CONTEST);
			history.setRewardStatus(RewardStatus.ACTIVE);
			history.setUpdatedDate(today);
			history.setCreateDate(today);
			history.setPointsEarnedAsDouble(0.00);
			history.setPointsSpentAsDouble(0.00);
			history.setInitialRewardPointsAsDouble(loyalty.getActivePointsAsDouble());
			history.setBalanceRewardPointsAsDouble(loyalty.getActivePointsAsDouble());
			history.setIsRewardsEmailSent(true);
			history.setEmailDescription("Email Not require.");
			
			try {
				DAORegistry.getInvoiceDAO().save(invoice);
				
				tgGroup.setInvoiceId(invoice.getId());
				DAORegistry.getCategoryTicketGroupDAO().save(tgGroup);
				
				order.setCategoryTicketGroupId(tgGroup.getId());
				DAORegistry.getCustomerOrderDAO().save(order);
				
				paymentBreakUp.setOrderId(order.getId());
				DAORegistry.getOrderPaymentBreakupDAO().save(paymentBreakUp);
				
				orderDetail.setOrderId(order.getId());
				DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
				
				history.setOrderId(order.getId());
				DAORegistry.getCustomerLoyaltyHistoryDAO().save(history);
				
				invoice.setCustomerOrderId(order.getId());
				DAORegistry.getInvoiceDAO().update(invoice);
				
				//update contest order payment transaction id 
				DAORegistry.getPropertyDAO().update(property);
				
				if(updateCustTopLevelObj){
					DAORegistry.getCustomerDAO().update(customer);
					try{
						CustomerUtil.updatedCustomerUtil(customer);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				List<CategoryTicket> tickets = new ArrayList<CategoryTicket>();
				for(int i=0;i<order.getQty();i++){
					CategoryTicket catTicket = new CategoryTicket();
					catTicket.setActualPrice(0.00);
					catTicket.setIsProcessed(true);
					catTicket.setSoldPrice(0.00);
					catTicket.setUserName("AUTO");
					catTicket.setCategoryTicketGroupId(tgGroup.getId());
					catTicket.setInvoiceId(invoice.getId());
					tickets.add(catTicket);
				}
				DAORegistry.getCategoryTicketDAO().saveAll(tickets);
				
				/*try{
					MapUtil.copySVGMapandText(event.getVenueId(), event.getVenueCategoryName(),order.getId());
				}catch(Exception e){
					e.printStackTrace();
				}*/
				
				winner.setStatus(WinnerStatus.ORDERED);
				winner.setOrderId(order.getId());
				QuizDAORegistry.getContestGrandWinnerDAO().update(winner);
				
				//Tamil: refresh customer is_contest_order for dash board
				List<ContestGrandWinner> grandWiners = QuizDAORegistry.getQuizQueryManagerDAO().getAllActiveGrandWinnersByCustomerId(customerId);
				Boolean isContestOrder = false;
				if(grandWiners != null && !grandWiners.isEmpty()) {
					isContestOrder = true;
				}
				if(!customer.getIsContestOrder().equals(isContestOrder)) {
					customer.setIsContestOrder(isContestOrder);
					DAORegistry.getCustomerDAO().update(customer);
					try{
						CustomerUtil.updatedCustomerUtil(customer);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				genericResponse.setStatus(1);
				genericResponse.setOrderId(order.getId());
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Your Order is created, OrderNo is "+order.getId());
				genericResponse.setMessage("Your Order is created, OrderNo is "+order.getId());
				log.info("QUIZ ORDER CREATE 1 : "+request.getParameter("customerId")+" :coId: "+request.getParameter("contestId")+" :coId: "+request.getParameter("eventId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
				return genericResponse;
				
				
			} catch (Exception e) {
				e.printStackTrace();
				genericResponse.setStatus(0);
				error.setDescription("Error Occured while creating order.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZWINNERORDER,"Error Occured while creating order.");
				genericResponse.setError(error);
				return genericResponse;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("QUIZ ORDER CREATE 2 : "+request.getParameter("customerId")+" :coId: "+request.getParameter("contestId")+" :coId: "+request.getParameter("eventId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		
		return genericResponse;
	}
	
	
	@RequestMapping(value = "/GetContestEvents", method=RequestMethod.POST)
	public @ResponsePayload ContestEventResponse getContestEvents(HttpServletRequest request,HttpServletResponse response,Model model){
		ContestEventResponse apiResponse =new ContestEventResponse();
		Error error = new Error();
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				apiResponse.setError(authError);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,authError.getDescription());
				return apiResponse;
			}
			*/
			String customerIdStr = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId");
			String artistIdStr = request.getParameter("artistId");
			String searchKey = request.getParameter("searchKey");
			String state = request.getParameter("state");
			String platForm = request.getParameter("platForm");
			String pageNumberStr = request.getParameter("pageNumber");
			String startDateStr = request.getParameter("startDate");
			String endDateStr = request.getParameter("endDate");
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Customer Id is mandatory");
				return apiResponse;
			}
			if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Contest Id is mandatory");
				return apiResponse;
			}
			
			Integer contestId = null;
			try{
				contestId = Integer.parseInt(contestIdStr.trim());
			}catch(Exception e){
				error.setDescription("Invalid Contest Id");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Invalid Contest Id");
				return apiResponse;
			}
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			if(contest == null) {
				error.setDescription("Contest Id is Invalid");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Contest Id is Invalid");
				return apiResponse;
			}
			
			String contestName = null != contest.getExtendedName() && !contest.getExtendedName().isEmpty()?contest.getExtendedName():contest.getContestName();
			apiResponse.setContestName(contestName);
			apiResponse.setQuantity(contest.getFreeTicketsPerWinner());
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr));
			if(customer == null) {
				error.setDescription("Customer Id is Invalid");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Customer Id is Invalid");
				return apiResponse;
			}
			
			Integer artistId = null;
			if(!TextUtil.isEmptyOrNull(artistIdStr)){
				
				try{
					artistId = Integer.parseInt(artistIdStr.trim());
				}catch(Exception e){
					error.setDescription("Invalid artist Id");
					apiResponse.setError(error);
					apiResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Invalid artist Id");
					return apiResponse;
				}
			}
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			
			Date eventStartDate = null, eventEndDate=null;
			if(startDateStr !=null && !startDateStr.isEmpty()){
				try{
					eventStartDate = df.parse(startDateStr.trim());
				}catch(Exception e){
					error.setDescription("Valid Start Date Format is MM/dd/yyyy");
					apiResponse.setError(error);
					apiResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Valid Start Date Format is MM/dd/yyyy");
					return apiResponse;
				}
			}
			
			if(endDateStr !=null && !endDateStr.isEmpty()){
				try{
					eventEndDate = df.parse(endDateStr.trim());
				}catch(Exception e){
					error.setDescription("Valid End Date Format is MM/dd/yyyy");
					apiResponse.setError(error);
					apiResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Valid End Date Format is MM/dd/yyyy");
					return apiResponse;
				}
			}
			if(TextUtil.isEmptyOrNull(pageNumberStr)){
				error.setDescription("Page Number is mandatory.");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Page Number is mandatory");
				return apiResponse;
			}
			
			Integer pageNumber = null;
			try{
				pageNumber = Integer.parseInt(pageNumberStr.trim());
			}catch(Exception e){
				error.setDescription("Page Number Should be Integer");
				apiResponse.setError(error);
				apiResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Page Number Should be Integer");
				return apiResponse;
			}
			List<Event> events = new ArrayList<Event>();
			Integer maxRows = PaginationUtil.contestTicketsMaxRow;;//
			
			/*Map<Integer, Boolean> favouriteEventMap = new HashMap<Integer, Boolean>();
			Map<Integer, Boolean> favouriteArtistMap = new HashMap<Integer, Boolean>();
			List<Integer> unFavoritedEventIds = new ArrayList<Integer>();
			if(null != customerIdStr && !customerIdStr.isEmpty()){
				favouriteEventMap = CustomerFavoriteEventUtil.getFavoriteEventFlagByCustomerId(Integer.parseInt(customerIdStr.trim()));
				favouriteArtistMap = CustomerFavoriteUtil.getFavoriteArtistByCustomerId(Integer.parseInt(customerIdStr.trim()));
				unFavoritedEventIds = CustomerFavoriteEventUtil.getUnFavoritedEventIdsByCustomerId(Integer.parseInt(customerIdStr.trim()));
				customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim())); //DAORegistry.getCustomerDAO().get(Integer.parseInt(customerIdStr.trim()));
			}*/
			
			int totalEventCount = 0;
			
			
			events = DAORegistry.getEventDAO().getAllContestEventsByCriteria(contestId, pageNumber, maxRows, eventStartDate, eventEndDate,state,searchKey);

			/*if(pageNumber <= 1 && (null == events || events.isEmpty())) {
				String zones = "";
				if(null != contest.getZone() && !contest.getZone().isEmpty()){
					String[] temp = contest.getZone().split(",");
					int i=0;
					for (String zone : temp) {
						if(i==0){
							zones = "'"+zone+"'";
							i++;
							continue;
						}
						zones = zones+",'"+zone+"'";
						i++;
					}
					
				}
				Double tixPrice = 0.00;
				if(null != contest.getSingleTicketPrice() && contest.getSingleTicketPrice() > 0){
					tixPrice = contest.getSingleTicketPrice();
				}
				
				events = DAORegistry.getEventDAO().getAllEventsByContestId(contestId, pageNumber, maxRows, 
						eventStartDate, eventEndDate,state,searchKey,zones,tixPrice,contest.getFreeTicketsPerWinner());
			}*/
			
			NormalSearchResult normalSearchResult = new NormalSearchResult();
			List<Event> normalSearchEvents = new ArrayList<Event>();
			int maxEventCount = 0,i=0;
			
			if(null != events && !events.isEmpty()) {
				totalEventCount = events.get(0).getTotalEvents();
				normalSearchEvents.addAll(events);
				maxEventCount = events.size();
				i++;
			}
			boolean showMoreEvents;
			if(maxRows <= maxEventCount) {
				showMoreEvents = true;
			} else {
				showMoreEvents = false;
			}
			//normalSearchResult.setMaxEventsPerPage(maxRows);
			normalSearchResult.setTotalEventPages(PaginationUtil.getTotalNoOfPages(totalEventCount, maxRows));
			normalSearchResult.setTotalEventCount(maxEventCount);
			normalSearchResult.setEventShowMore(showMoreEvents);
			normalSearchResult.setEvents(normalSearchEvents);
			apiResponse.setStatus(1);
			apiResponse.setContestId(contestId);
			apiResponse.setNormalSearchResult(normalSearchResult);
			System.out.println("End API Begins: "+new Date());
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Success");
			System.out.println("End API ENd: "+new Date());
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching contest event Details.");
			apiResponse.setError(error);
			apiResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTEVENTS,"Error occured while Fetching contest event Details.");
			return apiResponse;
		}
		return apiResponse;
	}
	

	@RequestMapping(value = "/RefreshNextContestList", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo refreshNextContestList(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizcontestDetails = new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			//String contestIDStr = request.getParameter("contestId");
			String cType = request.getParameter("cType");
			//String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(cType)){
				error.setDescription("Contest Type is mandatory");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.NEXTCONTESTLISTREFRESH,"Contest Type is mandatory");
				 
			}
			log.info("REFRESHCONTESTLIST: Updating Data in caches:"+new Date());
			QuizContestUtil.refreshNextContestList(cType);
			log.info("REFRESHCONTESTLIST: Updating Data in cache- Ends:"+new Date());
			
			quizcontestDetails.setMessage("Contest Cache Data Refreshed successfully.");
			//quizCustomerDetails.setError(error);
			quizcontestDetails.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error Occured While Refreshing Contest List.");
			quizcontestDetails.setError(error);
			quizcontestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.NEXTCONTESTLISTREFRESH,"Error Occured While Refreshing Contest List.");
			return quizcontestDetails;
		}
		log.info("REFRESHCONTESTLIST : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime()));
		
		return quizcontestDetails;
	}
	
	
	@RequestMapping(value = "/RefreshContestCacheData", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo refreshContestCacheData(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizcontestDetails = new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			String contestIDStr = request.getParameter("contestId");
			//String platForm = request.getParameter("platForm");
			//String productTypeStr = request.getParameter("productType");
			if(TextUtil.isEmptyOrNull(contestIDStr)){
				error.setDescription("Contest Id is mandatory");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREFRESHCACHE,"Contest Id is mandatory");
				return quizcontestDetails;
			}
			QuizContest contest = QuizContestUtil.getQuizContestByContestId(Integer.parseInt(contestIDStr));
			if(contest == null) {
				error.setDescription("Contest Id is Invalid");
				quizcontestDetails.setError(error);
				quizcontestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREFRESHCACHE,"Contest Id is Invalid");
				return quizcontestDetails;
			}
			log.info("REFRESHCONTESTCACHE: REMOVING DATA FROM CACHE- Begins:"+new Date());
			QuizContestUtil.refreshCacheDataByContestId(contest.getId());
			log.info("REFRESHCONTESTCACHE: REMOVING DATA FROM CACHE- Ends:"+new Date());
			
			quizcontestDetails.setMessage("Contest Cache Data Refreshed successfully.");
			//quizCustomerDetails.setError(error);
			quizcontestDetails.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Refreshing contest cache data :");
			quizcontestDetails.setError(error);
			quizcontestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTREFRESHCACHE,"Error Occured While Refreshing Contest Cache Data.");
			return quizcontestDetails;
		}
		log.info("QUIZ REFRESH CACHE : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime()));
		
		return quizcontestDetails;
	}
	
	@RequestMapping(value = "/SendQuizManualNotification", method=RequestMethod.POST)
	public @ResponsePayload QuizContestSummaryInfo sendQuizManualNotification(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestSummaryInfo quizContestSummary =new QuizContestSummaryInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			Error authError = authorizationValidation(request);
			if(authError != null) {
				quizContestSummary.setError(authError);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,authError.getDescription());
				return quizContestSummary;
			}
			
			//String customerIdStr = request.getParameter("customerId");
			//String contestIdStr = request.getParameter("contestId");
			String message = request.getParameter("message");
			
			//if(quizSummaryType.equals(QuizSummaryType.CONTEST)) {
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Contest Id is mandatory");
				return quizContestSummary;
			}*/
			if(TextUtil.isEmptyOrNull(message)){
				error.setDescription("Message is Mandatory");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Message is Mandatory");
				return quizContestSummary;
			}
			
			/*QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			if(quizContest == null) {
				error.setDescription("Contest Id is Invalid");
				quizContestSummary.setError(error);
				quizContestSummary.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Contest Id is Invalid");
				return quizContestSummary;
			}*/

			QuizContestStartNotification.sendBulkNotifications(message, null, true);
			
			List<CustomerNotificationDetails> custNotificationList = DAORegistry.getQueryManagerDAO().getAllOtpVerifiedCustomersNotificationDeviceDetails();
			QuizContestStartNotification.sendQuizContestNotifications(custNotificationList, message, null, true);
			
			quizContestSummary.setStatus(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Success");
			
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while sending manual notification.");
			quizContestSummary.setError(error);
			quizContestSummary.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZMANUALNOTIFICATION,"Error occured while sending manual notification.");
			return quizContestSummary;
		}
		log.info("QUIZ MAnual NOTIFIcation : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : msg: "+request.getParameter("message")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date());
		return quizContestSummary;
	
	}
	 
	
	@RequestMapping(value = "/UpdateCustContStats", method=RequestMethod.POST)
	public @ResponsePayload CommonRespInfo updateCustContStats(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CommonRespInfo commonRespInfo = new CommonRespInfo();
		CassError error = new CassError();
		Date start = new Date();
		String contestIdStr = request.getParameter("coId");
		String resMsg = "";
		String platForm = request.getParameter("pfm");
		String deviceType = request.getParameter("dyType");
		Integer contestId = null;
		try {
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				quizCustomerStatsInfo.setError(authError);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.UPDATECUSTCONTESTSTATS,authError.getDescription());
				return quizCustomerStatsInfo;
			}*/
			
			/*if(TextUtil.isEmptyOrNull(contestIdStr)){
				error.setDescription("Contest Id is mandatory");
				quizCustomerStatsInfo.setError(error);
				quizCustomerStatsInfo.setStatus(0);
				TrackingUtils.contestAPITracking(request, WebServiceActionType.UPDATECUSTCONTESTSTATS,"Contest Id is mandatory");
				return quizCustomerStatsInfo;
			}*/
			
			//QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(Integer.parseInt(contestIdStr));
			QuizContest contest = CassContestUtil.getCurrentContestByContestId(Integer.parseInt(contestIdStr));
			if(contest == null) {
				resMsg = "Contest Id is Invalid:"+contestIdStr;
				error.setDesc("Contest Id is Invalid");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			contestId = contest.getId();
			
			if(contest.getIsCustomerStatsUpdated()) {
				resMsg = "Customer Stats Already Updated for This Contest.";
				error.setDesc("Customer Stats Already Updated for This Contest.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			
			Boolean flag = CassContestUtil.updateCustomerContestDetailsinCassandra(contest.getId());
			if(!flag) {
				resMsg = "Error occured while Updating CustomerUtil Contest stats.";
				error.setDesc("Error occured while Updating CustomerUtil Contest stats.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			
			
			try {
				System.out.println("ContestMigration- Process Started at "+ new Date());
				//Migrate Data from Cassandra to Sql Database while ending contest.
				migrateToSqlFromCassandra(contest.getId());
				System.out.println("ContestMigration- Process Successfully ended at "+ new Date());
			}catch(Exception e) {
				resMsg = "Error occured while Migrating Contest Data to SQL from Cassandra.";
				e.printStackTrace();
				error.setDesc("Error occured while Migrating Contest Data to SQL from Cassandra.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			String msg = "";
			try {
				msg = updatePostMigrationStatusonServlet(contestId);
				if(msg != null) {
					resMsg = "Error occured while Invoking Update Post Migration status API to Servlet."+msg;
					error.setDesc("Error occured while Invoking Update Post Migration status API to Servlet.");
					commonRespInfo.setErr(error);
					commonRespInfo.setSts(0);
					return commonRespInfo;
				}
			} catch (Exception e) {
				resMsg = "Error occured while updating Post Migration status to Servlet."+msg;
				e.printStackTrace();
				error.setDesc("Error occured while updating Post Migration status to Servlet.");
				commonRespInfo.setErr(error);
				commonRespInfo.setSts(0);
				return commonRespInfo;
			}
			/*try {
				//Truncate table data after migrating contest data from cassandra to sql.
				truncateCassandraContestData(contestIdStr);
			}catch(Exception e) {
				e.printStackTrace();
			}*/
			
			resMsg = "Success";
			commonRespInfo.setSts(1);
			//quizContestDetails.setQuizCustomer(quizCustomer);
		}catch(Exception e){
			resMsg = "Error occured while Updating Customer Contest stats.";
			e.printStackTrace();
			error.setDesc("Error occured while Updating Customer Contest stats.");
			commonRespInfo.setErr(error);
			commonRespInfo.setSts(0);
			return commonRespInfo;
		}finally {
			TrackingUtil.contestAPITracking(platForm, deviceType, request.getHeader("deviceId"), WebServiceActionType.UPDATECUSTCONTESTSTATS, resMsg, 
					contestId, null, start, new Date(), request.getHeader("X-Forwarded-For"),null);
			log.info("CASS UPD CUST CONT STATS : "+request.getParameter("customerId")+" : coId: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime())+" : "+new Date()+":"+resMsg);
		}
		return commonRespInfo;
	
	}
	
	
	@RequestMapping(value = "/AddSuperFanWinners")
	public @ResponsePayload DashboardInfo addSuperFanWinners(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		String resMsg = "";
		String coIdStr = request.getParameter("coId");
		String minNoOfChances = request.getParameter("minNoOfChances");
		try {
			CassandraDAORegistry.getCassSuperFanWinnerDAO().truncate();
			Integer noOfChances = Integer.parseInt(minNoOfChances.trim());
			Integer coId = Integer.parseInt(coIdStr);
			List<QuizSuperFanStat> superFanStatsList = QuizDAORegistry.getQuizSuperFanStatDAO().getSuperFanWinnersByChances(noOfChances);
			Long now = new Date().getTime();
			Integer noOfSuperFanStats = 0, totalINserted = 0, totalFailedInsert = 0;
			if(null != superFanStatsList && !superFanStatsList.isEmpty()) {
				noOfSuperFanStats = superFanStatsList.size();
				
				Integer sfEligibleStars = Integer.parseInt(DAORegistry.getPropertyDAO().get("quiz.superfan.eligible.stars").getValue().trim());
				
				for (QuizSuperFanStat obj : superFanStatsList) {
					try {
						
						CassCustomer customer  = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(obj.getCustomerId());
						if(null == customer) {
							continue;
						}
						
						Integer noOfChance = obj.getTotalNoOfChances() / sfEligibleStars;
						CassSuperFanWinner cassSuperFanWinner = new CassSuperFanWinner(coId, obj.getCustomerId(), now,noOfChance);
						CassandraDAORegistry.getCassSuperFanWinnerDAO().save(cassSuperFanWinner);
						totalINserted++;
					}catch(Exception e){
						totalFailedInsert++;
					}
				}
			}
			resMsg = "Success: TotalSuperFans: "+noOfSuperFanStats+", Inserted: "+totalINserted+", Failed: "+totalFailedInsert;
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while inserting super fan winners to cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while inserting super fan winners to cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} 
		
		return dashboardInfo;
	
	}
	
	
	
	@RequestMapping(value = "/SendSuperFanWinner")
	public @ResponsePayload DashboardInfo sendSuperFanWinnerMessage(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		String resMsg = "";
		try {
			List<CassSuperFanWinner> list = CassandraDAORegistry.getCassSuperFanWinnerDAO().getSuperFanWinnersByContestId(100);
			Integer noOfSuperFanStats = 0, totalINserted = 0, totalFailedInsert = 0, exceptionCount = 0, processing =0;
			if(null != list && !list.isEmpty()) {
				noOfSuperFanStats = list.size();
				for (CassSuperFanWinner obj : list) {
					try {
						CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(obj.getCuId());
						if(cassCustomer != null) {
							TwilioSMSServices.sendSuperFanWinnerMessage(cassCustomer.getPhone());
							totalINserted++;
						}else {
							totalFailedInsert++;
						}
					}catch(Exception e){
						exceptionCount++;
					}
					processing++;
					
					System.out.println("Total : "+noOfSuperFanStats+", Processing : "+processing+", Success: "+totalINserted+", TotalFailed: "+totalFailedInsert+", Exception: "+exceptionCount);
				}
			}
			resMsg = "Success: TotalSuperFans: "+noOfSuperFanStats+", Inserted: "+totalINserted+", Failed: "+totalFailedInsert;
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while inserting super fan winners to cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while inserting super fan winners to cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} 
		
		return dashboardInfo;
	
	}
	
	
	
	@RequestMapping(value = "/GetCustomerStatsByUserId", method=RequestMethod.POST)
	public @ResponsePayload CustomerStats getCustomerSummaryByUserId(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			String userIdStr = request.getParameter("userId");
			if(TextUtil.isEmptyOrNull(userIdStr)) {
				error.setDescription("User id is mandatory.");
				dtoResponse.setStatus(0);
				dtoResponse.setError(error);
				return dtoResponse;
			}

			Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userIdStr);
			
			if(null == customer) {
				error.setDescription("Invalid Customer.UserId is not valid.");
				dtoResponse.setStatus(0);
				dtoResponse.setError(error);
				return dtoResponse;
			}
			
			List<Integer> customerIds = new ArrayList<Integer>();
			Set<Integer> tireOneCustomerIds = new HashSet<Integer>();
			
			customerIds.add(customer.getId());
			
			tireOneCustomerIds.add(customer.getId());
			
			QuizCustomerReferralTracking rootTracking = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(customer.getId());
			
			if(null != rootTracking) {
				
				QuizCustomerReferralTracking primaryCustTracking  = null;
				
				boolean getTracking = true;
				
				customerIds.add(rootTracking.getReferralCustomerId());
				
				Integer primaryCustomerId = rootTracking.getReferralCustomerId();
				
				while (getTracking) {
					
					primaryCustTracking  = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getReferralTrackingByCustomerId(primaryCustomerId);
					
					if(null == primaryCustTracking || !tireOneCustomerIds.add(primaryCustTracking.getCustomerId())) {
						getTracking = false;
					}else {
						customerIds.add(primaryCustTracking.getReferralCustomerId());
						primaryCustomerId = rootTracking.getReferralCustomerId();
					}
				}
				
			}
			
			List<Integer> botIds = RTFBotsUtil.getAllBots();
			
			CustomerStatistics statistics = null;
			List<CustomerStatistics> list = new ArrayList<CustomerStatistics>();
			
			Integer totalQuestionCount = QuizDAORegistry.getQuizQueryManagerDAO().getTotalNoOfContestQuestions();
			
			SimpleDateFormat dateFormat =  new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			
			for (Integer customerId : customerIds) {
				
				try {
					Customer customerObj = DAORegistry.getCustomerDAO().get(customerId);
					
					statistics = new CustomerStatistics();
					statistics.setFullName(customerObj.getCustomerName()+" "+customerObj.getLastName());
					statistics.setCustomerId(customerObj.getId());
					statistics.setUserId(customerObj.getUserId());
					statistics.setEmail(customerObj.getEmail());
					statistics.setPhoneNo(customerObj.getPhone());
					statistics.setIsOtpVerified(customerObj.getIsOtpVerified()?"YES":"NO");
					statistics.setReferrerCode(customerObj.getUserId());
					statistics.setSignupDate(dateFormat.format(customerObj.getSignupDate()));
					statistics.setSignUpDeviceId(customerObj.getDeviceId());
					statistics.setQuizCustomerLives(customerObj.getQuizCustomerLives());
					//statistics.setQuizNoOfPointsWon(customerObj.getQuizNoOfPointsWon());
					//statistics.setQuizNoOfTicketsWon(customerObj.getQuizNoOfTicketsWon());
					statistics.setTotalContestPlayed(customerObj.getTotalContestPlayed());
					statistics.setTotalContestWins(customerObj.getTotalContestWins());
					
					Integer correctAnswerCount = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getCorrectAnsweredQuestionByCustomer(customerObj.getId());
					Integer ansQuesCount = QuizDAORegistry.getQuizCustomerContestAnswersDAO().getNoOfQuesAnsweredByCustomer(customerObj.getId());
					
					statistics.setNoOfQuestionAnswered(ansQuesCount);
					statistics.setCorrectAnsQuestions(correctAnswerCount);
					statistics.setTotalNoOfQuestions(totalQuestionCount);
					
					Integer totalNoOfReferrals = QuizDAORegistry.getQuizCustomerReferralTrackingDAO().getTotalNoOfReferrals(customerObj.getId());
					
					statistics.setNoOfCustomerReferred(totalNoOfReferrals);
					
					CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerObj.getId());
					
					Double activeRewards = customerLoyalty.getActivePointsAsDouble();
					if(null == activeRewards) {
						activeRewards = 0.0;
					}
					
					Double pendingRewards = customerLoyalty.getPendingPointsAsDouble();
					if(null == pendingRewards) {
						pendingRewards = 0.0;
					}
					
					Double referralPoints = customerLoyalty.getTotalReferralDollars();
					if(null == referralPoints) {
						referralPoints = 0.0;
					}
					
					Double spentPoints = DAORegistry.getCustomerLoyaltyHistoryDAO().getSpentPoints(customerObj.getId());
					if(null == spentPoints) {
						spentPoints = 0.0;
					}
					
					Double contestGamePoints = DAORegistry.getCustomerLoyaltyTrackingDAO().getContestPoints(customerObj.getId());
					if(null == contestGamePoints) {
						contestGamePoints = 0.0;
					}
					
					Double purchasePoints = (activeRewards + spentPoints) - (referralPoints + contestGamePoints); 
					
					if(purchasePoints <= 0) {
						purchasePoints = 0.00;
					}else {
						purchasePoints = TicketUtil.getRoundedUpValue(purchasePoints);
					}
					
					activeRewards = TicketUtil.getRoundedUpValue(activeRewards);
					spentPoints = TicketUtil.getRoundedUpValue(spentPoints);
					referralPoints = TicketUtil.getRoundedUpValue(referralPoints);
					contestGamePoints = TicketUtil.getRoundedUpValue(contestGamePoints);
					pendingRewards = TicketUtil.getRoundedUpValue(pendingRewards);
					
					System.out.println("activeRewards: "+activeRewards+", spentPoints: "+spentPoints+", referralPoints: "+referralPoints+", contestGamePoints: "+contestGamePoints+", contestGamePoints: "+contestGamePoints);
					
					statistics.setCurrentActiveRewards(activeRewards);
					statistics.setContestGameRewards(contestGamePoints);
					statistics.setPendingPurchase(pendingRewards);
					statistics.setPurchaseRewards(purchasePoints);
					statistics.setReferralRewards(referralPoints);
					statistics.setSpendRewards(spentPoints);
					
					if(null != botIds && botIds.contains(customerObj.getId())) {
						statistics.setIsBot("YES");
					}else {
						statistics.setIsBot("NO");
					}
					
					list.add(statistics);
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
			
			dtoResponse.setCustomerStatistics(list);
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Customer Stats were successfully fectched.");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while fecthing customer stats.Quick Attention Required!");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		
		return dtoResponse;
	}
	
	@RequestMapping(value = "/GetQuizContestInfo", method=RequestMethod.POST)
	public @ResponsePayload QuizContestInfo getContestInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizContestDetails =new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		String resMsg = "";
		String customerIdStr = request.getParameter("customerId");
		String platForm = request.getParameter("platForm");
		String deviceType = request.getParameter("deviceType");
		try {
			
			if(null != platForm && !platForm.isEmpty() && platForm.equals("DESKTOP_SITE")) {
				
			}else {
				error.setDescription("Please update to our latest build to experience our new promotions.!");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"UPDATE THE BUILD");
				return quizContestDetails;
			}
			
			/*Error authError = authorizationValidation(request);
			if(authError != null) {
				resMsg = authError.getDescription();
				quizContestDetails.setError(authError);
				quizContestDetails.setStatus(0);
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}*/
			
			
			//String productTypeStr = request.getParameter("productType");
			/*if(TextUtil.isEmptyOrNull(platForm)){
				resMsg = "Application Platform is mandatory";
				error.setDescription("Application Platform is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}
			if(TextUtil.isEmptyOrNull(deviceType)){
				resMsg = "Device Type is mandatory";
				error.setDescription("Device Type is mandatory");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}*/
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					resMsg = "Please send valid application platform";
					error.setDescription("Please send valid application platform");
					quizContestDetails.setError(error);
					quizContestDetails.setStatus(0);
					TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
					return quizContestDetails;
				}
			}
			String contestType="MOBILE";
			if(!applicationPlatForm.equals(ApplicationPlatForm.ANDROID) && !applicationPlatForm.equals(ApplicationPlatForm.IOS)) {
				//Show Mobile Contest Now on Desktop
				contestType="WEB";
			}
			
			
			//Please update to our latest build to experience our new promotions


			
			Integer customerId = null;
			Customer customer = null;
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				 
				try {
					customerId = Integer.parseInt(customerIdStr.trim());
					customer = CustomerUtil.getCustomerById(customerId);
					//customer = DAORegistry.getCustomerDAO().get(customerId);
					if(customer == null) {
						resMsg = "Customer Id is Invalid";
						error.setDescription("Customer Id is Invalid");
						quizContestDetails.setError(error);
						quizContestDetails.setStatus(0);
						TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
						return quizContestDetails;
					}
				} catch(Exception e) {
					resMsg = "Customer Id Not Exist";
					error.setDescription("Customer Id Not Exist");
					quizContestDetails.setError(error);
					quizContestDetails.setStatus(0);
					TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
					return quizContestDetails;
				}
			}
			
			/*Property property = DAORegistry.getPropertyDAO().get("quiz.video.source.url");
			if(property == null || property.getValue() == null) {
				error.setDescription("Quiz Source Video Url is not yet declared.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.QUIZCONTESTINFO,"Quiz Source Video Url is not yet declared.");
				return quizContestDetails;
			}*/
			//QuizConfigSettings quizConfigSettings = QuizDAORegistry.getQuizConfigSettingsDAO().getConfigSettings();
			QuizConfigSettings quizConfigSettings = QuizConfigSettingsUtil.getConfigSettings();
			if(quizConfigSettings == null) {
				resMsg = "Quiz Configuration Settings not yet declared.";
				error.setDescription("Quiz Configuration Settings not yet declared.");
				quizContestDetails.setError(error);
				quizContestDetails.setStatus(0);
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}
			quizContestDetails.setVideoSourceUrl(quizConfigSettings.getVideoSourceUrl());
			//quizContestDetails.setAppSyncUrl(quizConfigSettings.getAppSyncUrl());
			//quizContestDetails.setAppSyncToken(quizConfigSettings.getAppSyncToken());
			//quizContestDetails.setStartingCountdownVideoUrl(properties.getRtfQuizVideo()+""+quizConfigSettings.getStartingCountdownVideoUrl());
			
			//quizContestDetails.setSourceId(quizConfigSettings.getSourceId());
			//quizContestDetails.setPartnerId(quizConfigSettings.getPartnerId());
			//quizContestDetails.setEntryId(quizConfigSettings.getEntryId());
			//quizContestDetails.setLiveStreamUrl(quizConfigSettings.getLiveStreamUrl());
			//quizContestDetails.setUiConfId(quizConfigSettings.getUiConfId());
			
			//if(applicationPlatForm.equals(ApplicationPlatForm.IOS)){
				//quizContestDetails.setJwPlayerLicenceKey(quizConfigSettings.getJwPlayerLicenceKeyIOS());
			//} else if (applicationPlatForm.equals(ApplicationPlatForm.ANDROID)) {
				//quizContestDetails.setJwPlayerLicenceKey(quizConfigSettings.getJwPlayerLicenceKeyAndroid());
			//}
			
			Integer pendingRequestCount = null;
			if(customer != null) {
				//2018-12-16 Added as per amit request to consider all dshboard users to be otp verified users.
				customer.setIsOtpVerified(Boolean.TRUE);
				
				quizContestDetails.setCustomer(customer);
				if(customer.getCustImagePath() != null) {
					quizContestDetails.setCustomerProfilePicWebView(URLUtil.profilePicWebURByImageName(customer.getCustImagePath(),applicationPlatForm));
				} else {
					quizContestDetails.setCustomerProfilePicWebView(null);
				}
				//Need to Fix it				
				
				/*CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
				//customer.setQuizNoOfPointsWon(customerLoyalty.getActivePointsAsDouble());
				if(customerLoyalty != null) {
					quizContestDetails.setTotalActiveRewards(customerLoyalty.getActivePointsAsDouble());
				}*/
				
				//pendingRequestCount = DAORegistry.getQueryManagerDAO().getAllPendingFriendsRequestCountByCustomerId(customer.getId());
				pendingRequestCount = 0;
			}
			if(pendingRequestCount != null) {
				quizContestDetails.setPendingRequests(pendingRequestCount);
			} else {
				quizContestDetails.setPendingRequests(0);
			}
			
			Date todayDate = dbDateTimeFormat.parse(dbDateFormat.format(new Date())+" 00:00");
			QuizContest quizContest = QuizDAORegistry.getQuizContestDAO().getNextQuizContest(todayDate,contestType);
			//List<QuizContest> contestList = QuizDAORegistry.getQuizContestDAO().getUpCommingQuizContests(todayDate, contestType);
			//List<QuizContest> contestList = QuizContestUtil.getNextContestList(contestType);
			/*if(contestList != null && contestList.size() > 0) {
				quizContest = contestList.get(0);
				for (QuizContest quizContestObj : contestList) {
					if(quizContestObj.getStatus().equals("STARTED")) {
						quizContest = quizContestObj;
					}
				}
			}*/
			
			List<QuizContest> contestList = new ArrayList<>();
			
			contestList.add(quizContest);
			String labelMsg = "PLAY GAME";
			Boolean showJoinButton = false;
			/*if(!deviceType.equals("IPAD") && !deviceType.equals("TABLET")) {
				labelMsg = "";
				if(customer == null) {
					labelMsg = "PLAY GAME";
					showJoinButton = true;
				} else if(!customer.getIsOtpVerified()) {
					labelMsg = "PLAY GAME";
					showJoinButton = true;
				} else if (quizContest != null && quizContest.getStatus().equals("STARTED")) {
					labelMsg = "JOIN";
					showJoinButton = true;
				}
			}*/
			quizContestDetails.setJoinButtonLabel(labelMsg);
			quizContestDetails.setShowJoinButton(showJoinButton);
			
			
			Integer contestPromoCodesCount = 0;//QuizDAORegistry.getQuizQueryManagerDAO().getAllValidContestPromoCodesCount();
			quizContestDetails.setcPromoCodesCount(contestPromoCodesCount);
			
			if(quizContest == null) {
				resMsg = "Next Contest Not Yet Declared.";
				quizContestDetails.setJoinButtonPopupMsg("Next Contest will be Announced Shortly.");
				quizContestDetails.setGrandPriceText("Next Contest will be Announced Shortly.");
				quizContestDetails.setContestStartTime("TBD");
				//error.setDescription("Next Contest Not Yet Declared.");
				//quizContestDetails.setError(error);
				quizContestDetails.setStatus(1);
				quizContestDetails.setMessage("Next Contest will be Announced Shortly.");
				TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
				return quizContestDetails;
			}
			SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat timeFt = new SimpleDateFormat("hh:mmaa z");
			//timeFt.setTimeZone(TimeZone.getTimeZone("UTC"));
			//quizContestDetails.setNextContestStartDateTime(quizContest.getContestStartDateTime());
			quizContestDetails.setContestStartDate(quizContest.getContestStartDate());
			quizContestDetails.setContestStartTime(quizContest.getContestStartTime());
			quizContestDetails.setContestName(quizContest.getContestName());
			quizContestDetails.setContestId(quizContest.getId());
			quizContestDetails.setMaxFreeTixWinners(quizContest.getMaxFreeTicketWinners());
			//quizContestDetails.setGrandPriceText(quizContest.getFreeTicketsPerWinner()+" Free Tickets "+quizContest.getContestName());
			quizContestDetails.setGrandPriceText(quizContest.getContestName());
			
			String grandWinerText = "";
			if(quizContest.getPromoExpiryDate() != null) {
				String hours = QuizContestUtil.computehoursBetweenDates(new Date(), quizContest.getPromoExpiryDate());
				grandWinerText = "Expires in " + hours + " hours";
			}
			quizContestDetails.setGrandWinerExpiryText(grandWinerText);
			//quizContestDetails.setContestPriceText("4 Free tickets: Hamilton ");

//			//quizContestDetails.setNoOfQuestions(quizContest.getNoOfQuestions());
			
			quizContestDetails.setStatus(1);
			quizContestDetails.setContestTotalRewards(quizContest.getTotalRewards());
			quizContestDetails.setFreeTicketsPerWinner(quizContest.getFreeTicketsPerWinner());
			quizContestDetails.setContestList(contestList);
			//quizContestDetails.setQuizCustomer(quizCustomer);
			
			String joinButtomMsg = "Next Contest will start ";
			if(quizContestDetails.getContestStartTime() != null) { 
					if(quizContestDetails.getContestStartTime().contains("Today")) {
						joinButtomMsg = joinButtomMsg + quizContestDetails.getContestStartTime().replace("Today", "Today at");
					} else if(quizContestDetails.getContestStartTime().contains("Tomorrow")) {
						joinButtomMsg = joinButtomMsg + quizContestDetails.getContestStartTime().replace("Tomorrow", "Tomorrow at");
					} else {
						joinButtomMsg = joinButtomMsg + "at "+quizContestDetails.getContestStartTime();
					}
			}
			quizContestDetails.setJoinButtonPopupMsg(joinButtomMsg);
			//quizContestDetails.setJoinButtonPopupMsg("Next Contest will start at "+quizContestDetails.getContestStartDate()+" "+quizContestDetails.getContestStartTime());
			
			/*if(customer != null) {
				CustomerLoyalty customerLoyalty  = null;
				if(quizContest != null && quizContest.getStatus().equals("STARTED")) {
					Double contesTAnserRewards = QuizContestUtil.getCustomerContestAnswerRewards(quizContest.getId(), customer.getId());
					quizContestDetails.setContestAnswerRewards(contesTAnserRewards);
					
					boolean isExistingContestant = QuizContestUtil.isExistingContestant(quizContest.getId(), customer.getId());
					//boolean isExistingContestant = QuizDAORegistry.getQuizContestParticipantsDAO().isExistingContestant(customer.getId(), quizContest.getId());
					quizContestDetails.setIsExistingContestant(isExistingContestant);
					
					customerLoyalty = QuizContestUtil.getCustomerLoyalty(customer.getId());
				} else {
					customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
				}
				
				//customer.setQuizNoOfPointsWon(customerLoyalty.getActivePointsAsDouble());
				if(customerLoyalty != null) {
					quizContestDetails.setTotalActiveRewards(customerLoyalty.getActivePointsAsDouble());
				}
			}*/
			if(quizContest.getStatus().equals("STARTED")) {
				quizContestDetails.setIsContestStarted(Boolean.TRUE);
				
				/* Change Hall of fame summary page max data size to 5 - By Ulaganathan on 11/27/2018 PM - Start */
				try {
					
					Integer hallOfFamePageMaxDataSize = QuizContestUtil.getHallOfFamePageMaxDataSize();
					if(hallOfFamePageMaxDataSize != 5) {
						QuizContestUtil.setHallOfFamePageMaxDataSize(5);
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				/* Change Hall of fame summary page max data size to 5 - By Ulaganathan on 11/27/2018 PM - Ends */
				
			} else {
				quizContestDetails.setIsContestStarted(Boolean.FALSE);
			}
			
			resMsg = "Success";
			if(customer != null) {
				resMsg += " :otp:"+customer.getIsOtpVerified()+":"+customer.getId();
			}
			//TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
		}catch(Exception e){
			resMsg = "Error occured while Fetching Contest Information";
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Contest Information.");
			quizContestDetails.setError(error);
			quizContestDetails.setStatus(0);
			TrackingUtils.contestDashboardAPITracking(request, WebServiceActionType.QUIZCONTESTINFO,resMsg);
			return quizContestDetails;
		} finally {
			log.info("GETQUIZ INFO IOS: "+customerIdStr+" :msg: "+resMsg+" : "+(new Date().getTime()-start.getTime()));
		}
		
		return quizContestDetails;
	
	}
	
	
	@RequestMapping(value = "/AddTestAccountAsSuperFanWinners")
	public @ResponsePayload DashboardInfo addTestAccountAsSuperFanWinners(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		String resMsg = "";
		String coIdStr = request.getParameter("coId");
		try {
			CassandraDAORegistry.getCassSuperFanWinnerDAO().truncate();
			Integer coId = Integer.parseInt(coIdStr);
			List<Integer> testCutomerIds = DAORegistry.getQueryManagerDAO().getAllTestAccounts();
			Long now = new Date().getTime();
			Integer noOfSuperFanStats = 0, totalINserted = 0, totalFailedInsert = 0;
			if(null != testCutomerIds && !testCutomerIds.isEmpty()) {
				noOfSuperFanStats = testCutomerIds.size();
				for (Integer obj : testCutomerIds) {
					try {
						CassSuperFanWinner cassSuperFanWinner = new CassSuperFanWinner(coId, obj, now,4);
						CassandraDAORegistry.getCassSuperFanWinnerDAO().save(cassSuperFanWinner);
						totalINserted++;
					}catch(Exception e){
						totalFailedInsert++;
					}
				}
			}
			resMsg = "Success: TotalSuperFans: "+noOfSuperFanStats+", Inserted: "+totalINserted+", Failed: "+totalFailedInsert;
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg(resMsg);
		}catch(Exception e){
			resMsg = "Error occured while inserting super fan winners to cassandra";
			e.printStackTrace();
			error.setDesc("Error occured while inserting super fan winners to cassandra");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			
			return dashboardInfo;
		} 
		
		return dashboardInfo;
	
	}
	
	
public void migrateToSqlFromCassandra(Integer contestId) {
		
		Date start = new Date();
		
		ContestMigrationStats migrationStatObj = new ContestMigrationStats();
		migrationStatObj.setContestId(contestId);
		migrationStatObj.setStartDate(start);
		migrationStatObj.setStatus("RUNNING");
		migrationStatObj.setMigrationType("CONTESTDATA");
		DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
		
		/*QuizContestParticipants participant = null;*/
		QuizContestWinners contestWinner = null;
		ContestGrandWinner contestGrandWinner = null;
		
		/*List<QuizContestParticipants> toBeSavedParticipants = new ArrayList<QuizContestParticipants>();*/
		List<QuizContestWinners> toBeSavedContWinnerList = new ArrayList<QuizContestWinners>();
		List<ContestGrandWinner> toBeSavedGrandWinnerList = new ArrayList<ContestGrandWinner>();
		
		/*List<ContestParticipants> cassContPrticipantsList = CassandraDAORegistry.getContestParticipantsDAO().getAllParticipantsByContestId(contestId);*/
		List<CassContestWinners> cassContWinnersList = CassandraDAORegistry.getCassContestWinnersDAO().getContestWinnersByContestId(contestId);
		List<CassContestGrandWinner> grandWinnerList = CassandraDAORegistry.getCassContestGrandWinnerDAO().getContestGrandWinnersByContestId(contestId);
		List<CustContDtls> cassCustContDtlList = CassandraDAORegistry.getCustContDtlsDAO().getAllCustContDtls();
		
		int totalGrandWiners =0, totalSummaryWinners =0, totalPlayedCustomers =0, totalJackpotWinners =0;
		for (CassContestGrandWinner obj : grandWinnerList) {
			try {
				contestGrandWinner = new ContestGrandWinner();
				contestGrandWinner.setContestId(obj.getCoId());
				contestGrandWinner.setCreatedDate(new Date(obj.getCrDated()));
				contestGrandWinner.setCustomerId(obj.getCuId());
				contestGrandWinner.setExpiryDate(new Date(obj.getExDate()));
				contestGrandWinner.setStatus(WinnerStatus.ACTIVE);
				contestGrandWinner.setRewardTickets(obj.getrTix());
				contestGrandWinner.setWinnerType(WinnerType.REGULAR);
				contestGrandWinner.setNotes("");
				toBeSavedGrandWinnerList.add(contestGrandWinner);
				totalGrandWiners++;
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		 
		  QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
		
		  List<CassContestGrandWinner> jackpotWinneList = CassandraDAORegistry.getJackpotWinnerDAO().getMegaJackpotWinnersByContestId(contestId);
		  
		  if(null != jackpotWinneList && !jackpotWinneList.isEmpty()) {
			WinnerType winnerType = (null != contest.getContestJackpotType() && contest.getContestJackpotType().equals(ContestJackpotType.MEGA))?WinnerType.MEGAJACKPOT:WinnerType.MINIJACKPOT;
			
			ContestJackpotType jackpotType = contest.getContestJackpotType();
			
			Integer expairyDays = 1;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, expairyDays);
					
			for (CassContestGrandWinner obj : jackpotWinneList) {
				try {
					contestGrandWinner = new ContestGrandWinner();
					contestGrandWinner.setContestId(obj.getCoId());
					contestGrandWinner.setCreatedDate(new Date(obj.getCrDated()));
					contestGrandWinner.setCustomerId(obj.getCuId());
					contestGrandWinner.setExpiryDate(cal.getTime());
					contestGrandWinner.setStatus(WinnerStatus.ACTIVE);
					contestGrandWinner.setQuestionId(obj.getqId());
					contestGrandWinner.setWinnerType(winnerType);
					contestGrandWinner.setNotes(obj.getJackpotSt());
					QuizContestQuestions contestQuestion = QuizDAORegistry.getQuizContestQuestionsDAO().get(obj.getqId());
					
					contestGrandWinner.setJackpotCreditType(contestQuestion.getJackpotCreditType());
					if(contestQuestion.getJackpotCreditType().equals(MiniJackpotType.TICKET)) {
						contestGrandWinner.setRewardTickets(contestQuestion.getjQtyPerWinner().intValue());
					}
					 
					toBeSavedGrandWinnerList.add(contestGrandWinner);
					totalJackpotWinners++;
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		} 
		
		//Map<Integer, CustContestStat> contestCustAnsMap = new HashMap<Integer, CustContestStat>(); 
		
		
		Map<Integer, QuizContestWinners> contestWinnerMap = new HashMap<Integer, QuizContestWinners>(); 
		int processCount = 0;
		for (CassContestWinners obj : cassContWinnersList) {
			try {
				contestWinner = new QuizContestWinners();
				contestWinner.setCustomerId(obj.getCuId());
				contestWinner.setContestId(contestId);
				contestWinner.setCreatedDateTime(new Date(obj.getCrDated()));
				contestWinner.setRewardTickets(obj.getrTix());
				contestWinner.setRewardType(RewardType.POINTS);
				contestWinner.setRewardPoints(obj.getrPoints());
				contestWinner.setRewardRank(0);
				toBeSavedContWinnerList.add(contestWinner);
				processCount++;
				
				if(processCount % QuizMigrationUtil.maxBatchIndex == 0) {
					System.out.println("ContestMigration- Batch Update ContestWinners(insert) Begins at :"+processCount+" : "+new Date());
					try {
						ContestWinnersSQLDAO.saveAllContestWinners(toBeSavedContWinnerList);
						System.out.println("ContestMigration- Batch Update ContestWinners(insert) Ends at "+new Date());
					}catch(Exception e) {
						System.out.println("ContestMigration- Batch Update ContestWinners(insert) Exception occurred at "+new Date());
						e.printStackTrace();
					}
					toBeSavedContWinnerList = new ArrayList<>();
				}
				totalSummaryWinners++;
				contestWinnerMap.put(obj.getCuId(), contestWinner);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if(null != toBeSavedContWinnerList && !toBeSavedContWinnerList.isEmpty() && toBeSavedContWinnerList.size() > 0) {
			System.out.println("ContestMigration- Batch Update ContestWinners(insert) Begins at  :"+processCount+" : "+new Date());
			try {
				ContestWinnersSQLDAO.saveAllContestWinners(toBeSavedContWinnerList);
				System.out.println("ContestMigration- Batch Update ContestWinners(insert) Ends at "+new Date());
			}catch(Exception e) {
				System.out.println("ContestMigration- Batch Update ContestWinners(insert) Exception occurred at "+new Date());
				e.printStackTrace();
			} 
		} 
		
		if(null != toBeSavedGrandWinnerList && !toBeSavedGrandWinnerList.isEmpty()) {
			QuizDAORegistry.getContestGrandWinnerDAO().saveAll(toBeSavedGrandWinnerList);
		}
		
		/* Grand Winner Gift Card Order Creation Code Start Here- By Ulaganathan*/
		try {
			if(null != toBeSavedGrandWinnerList && !toBeSavedGrandWinnerList.isEmpty() && null != contest.getGiftCardValueId() && contest.getGiftCardValueId() > 0 
					&& null != contest.getGiftCardPerWinner() && contest.getGiftCardPerWinner() > 0 ) {
				
				for (ContestGrandWinner grandWinner : toBeSavedGrandWinnerList) {
					try {
						if(null != grandWinner.getWinnerType() && grandWinner.getWinnerType().equals(WinnerType.REGULAR)) {
							QuizMiniJackpotWinnerCredit.createGiftCardOrder(contest, grandWinner.getCustomerId());
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
				} 
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		/* Grand Winner Gift Card Order Creation Code End Here- By Ulaganathan*/
		
		/* MINI & MEGA Jackpot Winner Credits Code Start Here- By Ulaganathan*/
		try {
			QuizJackpotWinnerUtil.creditJackpotWinnerBenefits(contest);
		}catch(Exception e) {
			e.printStackTrace();
		}
		/* MINI & MEGA Jackpot Winner Credits Code End Here- By Ulaganathan*/
		
		Integer livesBeforeContest = null, livesUsed =null, livesAfterContest = null;
		Integer rtfPointsBfContest=0,rtfPointsAfterContest=0;
		Double contestQuesRewards=null,contestWinnerRewards=null;
		
		CustomerCassandra customerCassandra = null;
		CustomerLoyaltyTracking loyaltyRewardTracking = null;
		
		List<CustomerCassandra> custCassandraList = new ArrayList<>();;
		List<CustomerLoyaltyTracking> custLoyaltyTrackingList = new ArrayList<>();;
		List<Customer> custList = new ArrayList<>();
		
		System.out.println("ContestMigration- Load all Customer[Lives + Rewards] Data on Cache. Process started at "+new Date());
		Map<Integer, Customer> customerMap = CustomerSQLDAO.getAllOtpVerifiedCustomerData();
		System.out.println("ContestMigration- Load all Customer[Lives + Rewards] Data on Cache. Process ended at "+new Date());
		
		Date createDate = new Date();
		processCount = 0;
		System.out.println("ContestMigration- totalSummaryWinners: "+totalSummaryWinners+", totalJackpotWinners: "+totalJackpotWinners+", totalSummaryWinners: "+totalSummaryWinners);
		
		Boolean isSuperFanLevel = Boolean.FALSE;
		Map<Integer,Double> questionLevelRewards = new HashMap<Integer,Double>();
		Map<Integer,SuperFanContCustLevels> sfContCustLevelsMap = new HashMap<Integer,SuperFanContCustLevels>();
		if(contest.getContestCategory() != null && contest.getContestCategory().equalsIgnoreCase("SUPERFAN")) {
			isSuperFanLevel = Boolean.FALSE;
			try {
				questionLevelRewards = CassContestUtil.GetContestQuestionLevelCumulativeRewardsMap(contest);
				sfContCustLevelsMap = CassContestUtil.GetSuperFanContCustLevelMap(contest);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Integer questRtfPoints = 0;
		Boolean isQuestPoints = false;
		
		if(contest.getQuestionRewardType() != null && contest.getQuestionRewardType().equals(ContestQuestRewardType.POINTS)) {
			isQuestPoints = true;
		}
		
		for (CustContDtls obj : cassCustContDtlList) {
			try {
				
				totalPlayedCustomers++;
				
				questRtfPoints = 0;
				rtfPointsBfContest=0;
				rtfPointsAfterContest=0;
				livesUsed = 0;
				if(null != obj.getCuLife() ) {
					livesUsed = obj.getCuLife();
				}
				
				Double cumulativeAnsRwds = obj.getCuRwds();
				if(isSuperFanLevel) {
					SuperFanContCustLevels sfcontCustLevel = sfContCustLevelsMap.remove(obj.getCuId());
					if(sfcontCustLevel != null && sfcontCustLevel.getqNo() > 0) {
						Double rewards = obj.getCuRwds();
						if(obj.getSflRwds() > 0) {
							 rewards = rewards - obj.getSflRwds();
						}
						Double qLevelRewards = questionLevelRewards.get(sfcontCustLevel.getqNo());
						if(qLevelRewards != null) {
							rewards = rewards + qLevelRewards;
						}
						cumulativeAnsRwds = rewards;
					}
				}
				
				Double ansRewards = 0.00;
				if(null != obj.getCuRwds()) {
					if(isQuestPoints) {
						questRtfPoints = cumulativeAnsRwds.intValue();
						obj.setCuRwds(0.0);
					} else {
						ansRewards = cumulativeAnsRwds;
					}
				}
				
				Integer custId = obj.getCuId();
				
				processCount++;
				
				livesBeforeContest = 0;
				livesAfterContest = 0;
				
				contestQuesRewards=0.00;
				contestWinnerRewards=0.00;
				
				Customer customerObj = customerMap.get(custId);
				QuizContestWinners contestWinnerObj = contestWinnerMap.get(custId);
				if(null != contestWinnerObj) {
					contestWinnerRewards = contestWinnerObj.getRewardPoints();
				}
				contestQuesRewards = ansRewards;
				
				livesBeforeContest = customerObj.getQuizCustomerLives();
				livesAfterContest = livesBeforeContest - livesUsed;
				
				rtfPointsBfContest=customerObj.getRtfPoints();
				rtfPointsAfterContest = rtfPointsBfContest + questRtfPoints;
				
				customerCassandra = new CustomerCassandra(custId, contestId, createDate);
				customerCassandra.setContestQusReward(contestQuesRewards);
				customerCassandra.setContestWinnerReward(contestWinnerRewards);
				customerCassandra.setCumulativeContestRewards(contestQuesRewards + contestWinnerRewards);
				customerCassandra.setLivesAfterContest(livesAfterContest);
				customerCassandra.setLivesBeforeContest(livesBeforeContest);
				customerCassandra.setLivesUsed(livesUsed);
				customerCassandra.setQuestRtfPoints(questRtfPoints);
				customerCassandra.setRtfPointsBeforeContest(rtfPointsBfContest);
				custCassandraList.add(customerCassandra);
				
				loyaltyRewardTracking = new CustomerLoyaltyTracking();
				loyaltyRewardTracking.setContestId(contestId);
				loyaltyRewardTracking.setCreatedBy("AUTO");
				loyaltyRewardTracking.setCreatedDate(createDate);
				loyaltyRewardTracking.setCustomerId(custId);
				loyaltyRewardTracking.setRewardPoints(customerCassandra.getCumulativeContestRewards());
				loyaltyRewardTracking.setRtfPoints(questRtfPoints);
				loyaltyRewardTracking.setRewardType("CONTEST");
				custLoyaltyTrackingList.add(loyaltyRewardTracking);
				
				customerObj.setThisContestRewards(customerCassandra.getCumulativeContestRewards());
				Double rewards = (null == customerObj.getRewardDollar())?0:customerObj.getRewardDollar();
				customerObj.setRewardDollar(rewards + customerCassandra.getCumulativeContestRewards());
				customerObj.setQuizCustomerLives(livesAfterContest);
				customerObj.setRtfPoints(rtfPointsAfterContest);
				
				custList.add(customerObj);
				
				if(processCount % QuizMigrationUtil.maxBatchIndex == 0) {
					System.out.println("ContestMigration- Batch Update Process Begins at  :"+processCount+" : "+new Date());
					QuizMigrationUtil.updateContestCustomerDatainBatch(custList);
					System.out.println("ContestMigration- Batch Update Process Ends at "+new Date());
					processCount = 0;
					custList = new ArrayList<>(); 
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		
		if(null != custList && !custList.isEmpty() && custList.size() > 0) {
			System.out.println("ContestMigration- Batch Update Process(Last) Begins at  :"+processCount+" : "+new Date());
			QuizMigrationUtil.updateContestCustomerDatainBatch(custList);
			System.out.println("ContestMigration- Batch Update Process(Last) Ends at "+new Date());
			custList = new ArrayList<>(); 
		}
		/* Superfan Level Custoemrs Who not played Migration - Starts */
		custList = new ArrayList<Customer>();
		List<SuperFanContCustLevels> sfContCustList = new ArrayList<>(sfContCustLevelsMap.values());
		for (SuperFanContCustLevels obj : sfContCustList) {
			try {
				//totalPlayedCustomers++;
				
				questRtfPoints = 0;
				rtfPointsBfContest=0;
				rtfPointsAfterContest=0;
				livesUsed = 0;
				livesBeforeContest = 0;
				livesAfterContest = 0;
				contestWinnerRewards=0.00;
				contestQuesRewards=0.00;
				Integer custId = obj.getCuId();
				
				processCount++;
				Double cumulativeAnsRwds = 0.0;
				if(isSuperFanLevel) {
					SuperFanContCustLevels sfcontCustLevel = sfContCustLevelsMap.remove(obj.getCuId());
					if(sfcontCustLevel != null && sfcontCustLevel.getqNo() > 0) {
						Double qLevelRewards = questionLevelRewards.get(sfcontCustLevel.getqNo());
						if(qLevelRewards != null) {
							cumulativeAnsRwds = cumulativeAnsRwds + qLevelRewards;
						}
					}
				}
				
				
				//Double ansRewards = 0.00;
				if(null != cumulativeAnsRwds) {
					if(isQuestPoints) {
						questRtfPoints = cumulativeAnsRwds.intValue();
						cumulativeAnsRwds=0.0;
					} else {
						contestQuesRewards = cumulativeAnsRwds;
					}
				}
				
				
				Customer customerObj = customerMap.get(custId);
				QuizContestWinners contestWinnerObj = contestWinnerMap.get(custId);
				if(null != contestWinnerObj) {
					contestWinnerRewards = contestWinnerObj.getRewardPoints();
				}
				//contestQuesRewards = ansRewards;
				
				livesBeforeContest = customerObj.getQuizCustomerLives();
				livesAfterContest = livesBeforeContest - livesUsed;
				
				rtfPointsBfContest=customerObj.getRtfPoints();
				rtfPointsAfterContest = rtfPointsBfContest + questRtfPoints;
				
				customerCassandra = new CustomerCassandra(custId, contestId, createDate);
				customerCassandra.setContestQusReward(contestQuesRewards);
				customerCassandra.setContestWinnerReward(contestWinnerRewards);
				customerCassandra.setCumulativeContestRewards(contestQuesRewards + contestWinnerRewards);
				customerCassandra.setLivesAfterContest(livesAfterContest);
				customerCassandra.setLivesBeforeContest(livesBeforeContest);
				customerCassandra.setLivesUsed(livesUsed);
				customerCassandra.setQuestRtfPoints(questRtfPoints);
				customerCassandra.setRtfPointsBeforeContest(rtfPointsBfContest);
				custCassandraList.add(customerCassandra);
				
				loyaltyRewardTracking = new CustomerLoyaltyTracking();
				loyaltyRewardTracking.setContestId(contestId);
				loyaltyRewardTracking.setCreatedBy("AUTO");
				loyaltyRewardTracking.setCreatedDate(createDate);
				loyaltyRewardTracking.setCustomerId(custId);
				loyaltyRewardTracking.setRewardPoints(customerCassandra.getCumulativeContestRewards());
				loyaltyRewardTracking.setRtfPoints(questRtfPoints);
				loyaltyRewardTracking.setRewardType("CONTEST");
				custLoyaltyTrackingList.add(loyaltyRewardTracking);
				
				customerObj.setThisContestRewards(customerCassandra.getCumulativeContestRewards());
				Double rewards = (null == customerObj.getRewardDollar())?0:customerObj.getRewardDollar();
				customerObj.setRewardDollar(rewards + customerCassandra.getCumulativeContestRewards());
				customerObj.setQuizCustomerLives(livesAfterContest);
				customerObj.setRtfPoints(rtfPointsAfterContest);
				
				custList.add(customerObj);
				
				if(processCount % QuizMigrationUtil.maxBatchIndex == 0) {
					System.out.println("ContestMigration- Batch Update Process Begins at  :"+processCount+" : "+new Date());
					QuizMigrationUtil.updateContestCustomerDatainBatch(custList);
					System.out.println("ContestMigration- Batch Update Process Ends at "+new Date());
					processCount = 0;
					custList = new ArrayList<>(); 
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(null != custList && !custList.isEmpty() && custList.size() > 0) {
			System.out.println("ContestMigration- Batch Update Process(Last) Begins at  :"+processCount+" : "+new Date());
			QuizMigrationUtil.updateContestCustomerDatainBatch(custList);
			System.out.println("ContestMigration- Batch Update Process(Last) Ends at "+new Date());
			custList = new ArrayList<>(); 
		}
		/* Superfan Level Custoemrs Who not played Migration - Ends */
		
		System.out.println("ContestMigration- totalSummaryWinners: "+totalSummaryWinners+", totalJackpotWinners: "+totalJackpotWinners+", totalSummaryWinners: "+totalSummaryWinners+", totalPlayedCustomers: "+totalPlayedCustomers);
		
		CassContestUtil.HIDE_DASHBOARD_REWARDS = false;
		migrationStatObj.setEndDate(new Date());
		migrationStatObj.setStatus("COMPLETED");
		DAORegistry.getContestMigrationStatsDAO().saveOrUpdate(migrationStatObj);
		
		try {
			CassContestUtil.updateCustomerPostContestDetailsinSQL(contestId);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("ContestMigration- Loyalty Tracking Thread invoked at  "+new Date());
			QuizMigrationUtilThread1  loyaltyTrackinThread = new QuizMigrationUtilThread1(custLoyaltyTrackingList,contestId);
			Thread t = new Thread(loyaltyTrackinThread);
			t.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("ContestMigration- Customer Cassandra Table Thread invoked at  "+new Date());
			QuizMigrationUtilThread2  custCassandraThread = new QuizMigrationUtilThread2(custCassandraList,contestId);
			Thread t2 = new Thread(custCassandraThread);
			t2.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			System.out.println("ContestMigration- Insert Customer Contest Details Table Data. Process started at "+new Date());
			
			QuizMigrationUtil.migrateCustContestDetailsToSQL(contestId);
			
			System.out.println("ContestMigration- Insert Customer Contest Details Table Data. Process ended at "+new Date());
		} catch(Exception e) {
			e.printStackTrace();
			
			System.out.println("Error Occured in ContestMigration- Insert Customer Contest Details Table Data. "+new Date());
		}
		
		/*CustContestAns to SQl migration starts*/
		
		try {
			System.out.println("ContestMigration- Insert Customer Contest Answers Table Data. Process started at "+new Date());
			
			QuizMigrationUtil.processCustContestAnswerstoSql(contestId);
			
			System.out.println("ContestMigration- Insert Customer Contest Answers Table Data. Process ended at "+new Date());
		} catch(Exception e) {
			e.printStackTrace();
			
			System.out.println("Error Occured in ContestMigration- Insert Customer Contest Answers Table Data. "+new Date());
		}
		/*CustContestAns to SQl migration Ends*/
		
		/*HallofFame Table Data Update starts*/
		try {
			System.out.println("ContestMigration- HallofFame Table Data Update. Process started at "+new Date());
			SQLDaoUtil.updateContestSummaryThisWeekDataTableSql();
			
			SQLDaoUtil.updateContestSummaryTillDateDataTableSql();
			System.out.println("ContestMigration- HallofFame Table Data Update. Process ended at "+new Date());

		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error Occured in ContestMigration- HallofFame Table Data Update. "+new Date());
		}
		
		/*HallofFame Table Data Update Ends*/
	}
 

@RequestMapping(value = "/CreditSuperFanQuestionsLevels")
public @ResponsePayload DashboardInfo creditSuperFanQuestionsLevels(HttpServletRequest request,HttpServletResponse response,Model model){
	DashboardInfo dashboardInfo =new DashboardInfo();
	CassError error = new CassError();
	String resMsg = "";
	String coIdStr = request.getParameter("coId");
	try {
		CassandraDAORegistry.getCassSuperFanWinnerDAO().truncate(); 
		Integer coId = Integer.parseInt(coIdStr);
		List<QuizSuperFanStat> superFanStatsList = QuizDAORegistry.getQuizSuperFanStatDAO().getAllSuperFanCustomerData();
		List<QuizSuperFanLevelConfig> levelList = QuizDAORegistry.getQuizSuperFanLevelConfigDAO().getAllActiveLevels();
		Map<Integer, QuizSuperFanLevelConfig> levelMap = new HashMap<>();
		
		QuizSuperFanLevelConfig maxLevelObj = null;
		Integer maxLevelNo = 1;
				
		for (QuizSuperFanLevelConfig levelObj : levelList) {
			levelMap.put(levelObj.getStarFrom(), levelObj);
			if(maxLevelNo < levelObj.getLevelNo()) {
				maxLevelNo = levelObj.getLevelNo();
				maxLevelObj = levelObj;
			}
		}
		Integer custSfStars=0;
		
		QuizSuperFanCustomerLevel quizSuperFanCustomerLevel =  null; 
		List<QuizSuperFanCustomerLevel> custLevelList = new ArrayList<>();
		for (QuizSuperFanStat obj : superFanStatsList) {
			boolean isLevelMapped = false;
			custSfStars = obj.getTotalNoOfChances();
			for(Integer noStartWith : levelMap.keySet()) {
				QuizSuperFanLevelConfig levelConfig = levelMap.get(noStartWith);
				if(TextUtil.between(custSfStars, levelConfig.getStarFrom(), levelConfig.getStarTo())) {
					quizSuperFanCustomerLevel = new QuizSuperFanCustomerLevel();
					quizSuperFanCustomerLevel.setContestId(coId);
					quizSuperFanCustomerLevel.setCustomerId(obj.getCustomerId());
					quizSuperFanCustomerLevel.setLevelId(levelConfig.getId());
					quizSuperFanCustomerLevel.setQuestionSlNo(levelConfig.getLevelNo());
					quizSuperFanCustomerLevel.setSfStar(obj.getTotalNoOfChances());
					quizSuperFanCustomerLevel.setStatus("ACTIVE");
					custLevelList.add(quizSuperFanCustomerLevel);
					isLevelMapped = true;
					break;
				}
			}
			if(!isLevelMapped) {
				quizSuperFanCustomerLevel = new QuizSuperFanCustomerLevel();
				quizSuperFanCustomerLevel.setContestId(coId);
				quizSuperFanCustomerLevel.setCustomerId(obj.getCustomerId());
				quizSuperFanCustomerLevel.setLevelId(maxLevelObj.getId());
				quizSuperFanCustomerLevel.setQuestionSlNo(maxLevelObj.getLevelNo());
				quizSuperFanCustomerLevel.setSfStar(obj.getTotalNoOfChances());
				quizSuperFanCustomerLevel.setStatus("ACTIVE");
				custLevelList.add(quizSuperFanCustomerLevel);
			}
		}
		
		
		Integer noOfSuperFanStats = custLevelList.size(), totalINserted=0, totalFailedInsert=0;
		
		SuperFanContCustLevels contCustLevels = null; 
		for (QuizSuperFanCustomerLevel custLevelObj : custLevelList) {
			try {
				contCustLevels = new SuperFanContCustLevels(custLevelObj.getCustomerId(), custLevelObj.getContestId(), custLevelObj.getQuestionSlNo(), custLevelObj.getSfStar());
				CassandraDAORegistry.getSuperFanContCustLevelsDAO().save(contCustLevels);
				QuizDAORegistry.getQuizSuperFanCustomerLevelDAO().save(custLevelObj);
				//DAORegistry.getCustomerDAO().updateSfLevelNo(custLevelObj.getCustomerId(), custLevelObj.getQuestionSlNo());
				//CassandraDAORegistry.getCassCustomerDAO().updateCustomerSfQuestionLevel(custLevelObj.getCustomerId(), custLevelObj.getQuestionSlNo());
				totalINserted++;
			}catch(Exception e) {
				e.printStackTrace();
				totalFailedInsert++;
			}
		}
		resMsg = "Success: TotalSuperFans: "+noOfSuperFanStats+", Inserted: "+totalINserted+", Failed: "+totalFailedInsert;
		dashboardInfo.setSts(1);
		dashboardInfo.setMsg(resMsg);
	}catch(Exception e){
		resMsg = "Error occured while inserting super fan winners to cassandra";
		e.printStackTrace();
		error.setDesc("Error occured while inserting super fan winners to cassandra");
		dashboardInfo.setErr(error);
		dashboardInfo.setSts(0);
		
		return dashboardInfo;
	} 
	
	return dashboardInfo;

}

@RequestMapping(value = "/ResetSuperFanLevelsData")
public @ResponsePayload DashboardInfo resetSuperFanLevelsData(HttpServletRequest request,HttpServletResponse response,Model model){
	DashboardInfo dashboardInfo =new DashboardInfo();
	CassError error = new CassError();
	try {
		CassandraDAORegistry.getSuperFanContCustLevelsDAO().truncate();
		//CassandraDAORegistry.getCassCustomerDAO().resetCustomerSuperFanLevels();
		//DAORegistry.getCustomerDAO().resetCustomerSuperFanLevels();
		dashboardInfo.setSts(1);
		dashboardInfo.setMsg("Super Fan Levels Data Successfully Removed.!");
	}catch(Exception e){
		e.printStackTrace();
		error.setDesc("Error occured while removing super fan levels data");
		dashboardInfo.setErr(error);
		dashboardInfo.setSts(0);
		return dashboardInfo;
	} 
	return dashboardInfo;
}

 
	
}	