package com.rtftrivia.ws.controller;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.list.DashboardInfo;
import com.quiz.cassandra.list.JackpotCreditResponse;
import com.quiz.cassandra.list.QuizCreditProcess;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.dao.implementaion.QuizQueryManagerDAO;
import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.rtfquiz.webservices.data.ContestReferrerRewardCredit;
import com.rtfquiz.webservices.data.QuizContest;
import com.rtfquiz.webservices.thread.QuizCreditUtilThread;
import com.rtfquiz.webservices.utils.QuizJackpotWinnerUtil;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.GiftCardUploadNotificationUtil;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.GiftCardUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({
	"/ForceStartContestCreditProcess",
	"/GetJackpotWinnerStatus", 
	"/SampleGiftCardOrderNotifcationEmail",
	"/SendFullFilledNotificationToGiftCardOrders",
	"/GetRecentGiftCardOrderDate",
	"/SomethingToTest"
	})
public class QuizCreditsController {
	
	private MailManager mailManager;
	private ZonesProperty properties;
	public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public MailManager getMailManager() {
		
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	     
	@RequestMapping(value="/ForceStartContestCreditProcess")
	public @ResponsePayload QuizCreditProcess verifyContestCreditProcess(HttpServletRequest request,HttpServletResponse response,Model model){
		QuizCreditProcess resObj =new QuizCreditProcess();
		Error error = new Error();
		try {
			String contestIdStr = request.getParameter("contestId"); 
			Integer contestId = Integer.parseInt(contestIdStr);
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			if(null == contest) {
				error.setDescription("Invalid Contest Id");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.CONTESTCREDITPROCESS,"Wrong Contest ID");
				return resObj;
			}
			List<ContestReferrerRewardCredit> contestCreditList = QuizDAORegistry.getContestReferrerRewardCreditDAO().getAllContestCredits(contestId);
			boolean runCreditUtil = false, runReferralCreditUtil = false, runAffiliateReferralCreditUtil = false;
			
			Date now = new Date();
			ContestReferrerRewardCredit superFanCreditObj = null;
			ContestReferrerRewardCredit referralCreditObj = null;
			ContestReferrerRewardCredit affliateCreditObj = null;
			
			boolean isContestCreditNotDone = true, isReferralNotDone = true, isAffiliateCreditNotDone = true;
			
			for (ContestReferrerRewardCredit obj : contestCreditList) {
				
				long minutes = DateUtil.getDifferenceMintues(obj.getCreatedDate(),now);
				
				if(obj.getReferralProgramType().equals("CONTEST_CREDITS")) {
					if(obj.getStatus().equals("STARTED") && minutes > 20) {
						runCreditUtil = true;
						superFanCreditObj = obj;
					}
					isContestCreditNotDone = false;
				}else if(obj.getReferralProgramType().equals("CUSTOMER_REFERRAL")) {
					if(obj.getStatus().equals("STARTED") && minutes > 20) {
						runReferralCreditUtil = true;
						referralCreditObj = obj;
					}
					isReferralNotDone = false;
				}else if(obj.getReferralProgramType().equals("AFFILIATE")) {
					if(obj.getStatus().equals("STARTED") && minutes > 20) {
						runAffiliateReferralCreditUtil = true;
						affliateCreditObj = obj;
					}
					isAffiliateCreditNotDone = false;
				}
			}
			
			if(runCreditUtil && null != superFanCreditObj) {
				QuizDAORegistry.getContestReferrerRewardCreditDAO().delete(superFanCreditObj);
			}
			
			if(runReferralCreditUtil && null != referralCreditObj) {
				QuizDAORegistry.getContestReferrerRewardCreditDAO().delete(referralCreditObj);
			}
			
			if(runAffiliateReferralCreditUtil && null != affliateCreditObj) {
				QuizDAORegistry.getContestReferrerRewardCreditDAO().delete(affliateCreditObj);
			}
			
			
			if(runCreditUtil || runReferralCreditUtil || runAffiliateReferralCreditUtil 
					|| isContestCreditNotDone|| isReferralNotDone || isAffiliateCreditNotDone ) {
				
				QuizCreditUtilThread.refereshMap();
				
				QuizCreditUtilThread  quizCreditUtilThread = new QuizCreditUtilThread(contest,contestId);
				Thread t = new Thread(quizCreditUtilThread);
				t.start();
				
				String message = "Contest Credit Process started just now. Time: "+new Date()+", ContestID: "+contestIdStr;
				resObj.setProcessedParticipants(0);
				resObj.setTotalParticipants(0);
				resObj.setStatus(1);
				resObj.setMessage(message);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.CONTESTCREDITPROCESS,message);
				return resObj;
			}
			String message = "All Contest Credit Process are completed successfully. ContestID: "+contestIdStr;
			resObj.setProcessedParticipants(0);
			resObj.setTotalParticipants(0);
			resObj.setStatus(1);
			resObj.setMessage(message);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.CONTESTCREDITPROCESS,message);
			return resObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Exception occured while checking. Immediate attention required.!");
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.CONTESTCREDITPROCESS,"Exception occured while checking. Immediate attention required.!");
			return resObj;
		}
	}
	
	@RequestMapping(value="/GetJackpotWinnerStatus")
	public @ResponsePayload JackpotCreditResponse startMiniJackpotWinnerCredit(HttpServletRequest request,HttpServletResponse response,Model model){
		JackpotCreditResponse resObj =new JackpotCreditResponse();
		try {
			String contestIdStr = request.getParameter("contestId");
			Integer contestId = Integer.parseInt(contestIdStr);
			QuizContest contest = QuizDAORegistry.getQuizContestDAO().get(contestId);
			if(null == contest) {
				resObj.setSts(0);
				resObj.setMsg("WRONG CONTESTID.");
				return resObj;
			}
			
			ContestReferrerRewardCredit credit = QuizDAORegistry.getContestReferrerRewardCreditDAO().getRewardCreditByContestIdAndType(contest.getId(),"JACKPOT_WINNERS_CREDIT");
			
			if(null == credit) {
				QuizJackpotWinnerUtil.creditJackpotWinnerBenefits(contest);
			}
			
			List<ContestGrandWinner> jackPotWinnerList = QuizDAORegistry.getContestGrandWinnerDAO().getAllProcessedJackpotWinnersByContestId(contest.getId());
			if(null != jackPotWinnerList) {
				for (ContestGrandWinner winner : jackPotWinnerList) {
					Customer customer = DAORegistry.getCustomerDAO().get(winner.getCustomerId());
					winner.setUserId(customer.getUserId());
				}
			}
			resObj.setMsg("Success.");
			resObj.setjWinList(jackPotWinnerList);
			resObj.setSts(1);
			return resObj;
		} catch (Exception e) {
			e.printStackTrace();
			resObj.setSts(0);
			resObj.setMsg("Some Exception occured. Please check the the logs");
			return resObj;
		}
	}
	 
	
	@RequestMapping(value="/GetRecentGiftCardOrderDate")
	public @ResponsePayload DashboardInfo getRecentGiftCardOrderDate(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo resObj =new DashboardInfo();
		try {
			
			String custIdStr = request.getParameter("custId");
			try {
				Date lastPurcahseDate = DAORegistry.getQueryManagerDAO().getRecentOrderDate(Integer.parseInt(custIdStr.trim()));
				System.out.println("Date: "+lastPurcahseDate);
				
				if(null != lastPurcahseDate) {
					
					Boolean blockPurchase = GiftCardUtil.validatePurchase(lastPurcahseDate);
					
					if(null != blockPurchase && blockPurchase){
						resObj.setSts(1);
						resObj.setMsg("We cannot proceed");
						return resObj;
					}
				}

			}catch(Exception e) {
				e.printStackTrace();
			}
			
			resObj.setSts(1);
			resObj.setMsg("Proceed to checkout..");
			return resObj;
		} catch (Exception e) {
			resObj.setSts(1);
			resObj.setMsg("Some Exception occured. Please check the the logs");
			return resObj;
		}
	}
	
	
	@RequestMapping(value="/SendFullFilledNotificationToGiftCardOrders")
	public @ResponsePayload DashboardInfo sendEmailToGiftCardOrders(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo resObj =new DashboardInfo();
		try {
			
			 Collection<RtfGiftCardOrder> orders = DAORegistry.getRtfGiftCardOrderDAO().getAll();
			 
			 int size = orders.size(), i =1;
			 
			 for (RtfGiftCardOrder rtfGiftCardOrder : orders) {
				 
				 	Customer customer = DAORegistry.getCustomerDAO().get(rtfGiftCardOrder.getCustomerId());
				 	
				 	System.out.println(size+"---"+i+"---: Order No: "+rtfGiftCardOrder.getId()+", Email ID: "+customer.getEmail()+", Customer ID: "+customer.getId());
				 
				 	String mailTemplate = "email-gift-card-download-attachment.html";
				 
				 	Map<String,Object> mailMap = new HashMap<String,Object>();
					
					String subject = "Reward The Fan: Your GIFT Card Available for download";
					 
					mailMap.put("orderId", rtfGiftCardOrder.getId()); 
					
					String email=customer.getEmail();
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
					try {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
								null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
								subject,mailTemplate, mailMap, "text/html", null,mailAttachment,null);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					i++;
			 }
			resObj.setSts(1);
			resObj.setMsg("Done");
			return resObj;
		} catch (Exception e) {
			resObj.setSts(1);
			resObj.setMsg("Some Exception occured. Please check the the logs");
			return resObj;
		}
	}
	
	
	@RequestMapping(value="/SampleGiftCardOrderNotifcationEmail")
	public @ResponsePayload DashboardInfo sampleGiftCardOrderNotifcationEmail(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo resObj =new DashboardInfo();
		try {
			
			 
			Integer orderNo = 99999,customerId=342693;
			 int size =1, i =1;
		 	 Customer customer = DAORegistry.getCustomerDAO().get(customerId);
		 	
		 	System.out.println(size+"---"+i+"---: Order No: "+orderNo+", Email ID: "+customer.getEmail()+", Customer ID: "+customer.getId());
		 
		 	String mailTemplate = "email-gift-card-download-attachment.html";
		 
		 	Map<String,Object> mailMap = new HashMap<String,Object>();
			
		 	String subject = "Reward The Fan: Your GIFT Card Available for download";
			 
			mailMap.put("orderId", orderNo); 
			
			String email=customer.getEmail();
			
			//inline(image in mail body) image add code
			//MailAttachment[] mailAttachment = Util.getOrderConfirmationEmailAttachmentImages();
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
			try {
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
						null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
						subject,mailTemplate, mailMap, "text/html", null,mailAttachment,null);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			i++;
			resObj.setSts(1);
			resObj.setMsg("Proceed to checkout..");
			return resObj;
		} catch (Exception e) {
			resObj.setSts(1);
			resObj.setMsg("Some Exception occured. Please check the the logs");
			return resObj;
		}
	}
	
	
	@RequestMapping(value="/SomethingToTest")
	public @ResponsePayload DashboardInfo somethingToTest(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo resObj =new DashboardInfo();
		try {
		 
			GiftCardUploadNotificationUtil util = new GiftCardUploadNotificationUtil();
			util.init();
			 
			resObj.setSts(1);
			resObj.setMsg("Proceed to checkout..");
			return resObj;
		} catch (Exception e) {
			resObj.setSts(1);
			resObj.setMsg("Some Exception occured. Please check the the logs");
			return resObj;
		}
	}
	
	
	/*@RequestMapping(value="/SendContestQuestionWrongEmail")
	public @ResponsePayload DashboardInfo sendContestQuestionWrongEmail(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo resObj =new DashboardInfo();
		try {
			
			List<Integer> customerIds = QuizDAORegistry.getQuizQueryManagerDAO().getContestQuestionIssueCustomerIds();
			 
			for (Integer custId : customerIds) {
				int size =1, i =1;
			 	Customer customer = DAORegistry.getCustomerDAO().get(custId);
			 	
			 	System.out.println(size+"---"+i+"---: Email ID: "+customer.getEmail()+", Customer ID: "+customer.getId());
			 
			 	String mailTemplate = "email-contest-question-issue.html";
			 
			 	Map<String,Object> mailMap = new HashMap<String,Object>();
				
			 	String subject = "Reward The Fan: Your GIFT Card Available for download";
				 
				mailMap.put("orderId", orderNo); 
				
				String email=customer.getEmail();
				
				//inline(image in mail body) image add code
				//MailAttachment[] mailAttachment = Util.getOrderConfirmationEmailAttachmentImages();
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
				try {
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
							null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
							subject,mailTemplate, mailMap, "text/html", null,mailAttachment,null);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				i++;
			}
			 
			 
			resObj.setSts(1);
			resObj.setMsg("Proceed to checkout..");
			return resObj;
		} catch (Exception e) {
			resObj.setSts(1);
			resObj.setMsg("Some Exception occured. Please check the the logs");
			return resObj;
		}
	}*/
	
}