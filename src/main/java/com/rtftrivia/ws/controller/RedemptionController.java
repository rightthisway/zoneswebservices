package com.rtftrivia.ws.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.quiz.cassandra.utils.CassCustomerUtil;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizSuperFanLevelConfig;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.rtfquiz.webservices.data.RtfCustomerRewardRedemptionHistory;
import com.rtfquiz.webservices.data.RtfPointsConversionSetting;
import com.rtfquiz.webservices.sqldao.implementation.SQLDaoUtil;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyTracking;
import com.zonesws.webservices.enums.PointRedemptionType;
import com.zonesws.webservices.enums.PowerUpType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.MyPowerUp;
import com.zonesws.webservices.utils.list.MyPowerUpResponse;
import com.zonesws.webservices.utils.list.MyReward;
import com.zonesws.webservices.utils.list.MySfLevelResponse;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/GetMyPowerUps","/PowerUpRedemption","/GetMySFLevel"})
public class RedemptionController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(RedemptionController.class);
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	 
	@RequestMapping(value="/GetMyPowerUps",method=RequestMethod.POST)
	public @ResponsePayload MyPowerUpResponse getMyPowerUps(HttpServletRequest request, Model model,HttpServletResponse response){
		MyPowerUpResponse responseObj =new MyPowerUpResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"You are not authorized");
				return responseObj;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						responseObj.setError(error);
						responseObj.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"You are not authorized");
						return responseObj;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"You are not authorized");
					return responseObj;
				}
			}else{
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"You are not authorized");
				return responseObj;
			}
			
			String customerIdStr = request.getParameter("cId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"You are not authorized");
				return responseObj;
				
			}
			
			Integer customerId = null;
			try {
				customerId = Integer.parseInt(customerIdStr);
				CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(null == cassCustomer) {
					error.setDescription("Customer not recognized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"Customer not recognized");
					return responseObj;
				}
			}catch(Exception e) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"You are not authorized");
				return responseObj;
			}
			
			MyReward myReward = SQLDaoUtil.getCustomerRewards(customerId);
			
			
			List<RtfPointsConversionSetting> list = QuizDAORegistry.getRtfPointsConversionSettingDAO().getActiveConversionSetting();
			
			Map<PowerUpType, List<RtfPointsConversionSetting>> powerUpMap = new HashMap<PowerUpType, List<RtfPointsConversionSetting>>();
			
			for (RtfPointsConversionSetting obj : list) {
				List<RtfPointsConversionSetting> tempList = powerUpMap.get(obj.getPowerUpType());
				if(null != tempList && !tempList.isEmpty() && tempList.size() > 0) {
					tempList.add(obj);
				}else {
					tempList = new ArrayList<RtfPointsConversionSetting>();
					tempList.add(obj);
				}
				powerUpMap.put(obj.getPowerUpType(),tempList);
			}
			
			List<MyPowerUp> myPowerUpList = new ArrayList<>();
			MyPowerUp myPowerUpObj = null;
			
			for (PowerUpType powerUpType : powerUpMap.keySet()) {
				myPowerUpObj = new MyPowerUp();
				switch (powerUpType) {
					case Lives:
						myPowerUpObj.setHd("Earn Lives");
						myPowerUpObj.setHinfo("You can convert your reward points to lives.");
						break;
						
					case MagicWands:
						myPowerUpObj.setHd("Erasers");
						myPowerUpObj.setHinfo("You can convert your reward points to erasers.");
						break;
						
					case RewardDollars:
						myPowerUpObj.setHd("Reward$");
						myPowerUpObj.setHinfo("You can convert your reward points to reward$.");
						break;
					default:
						break;
				}
				
				myPowerUpObj.setPowerUpType(powerUpType);
				myPowerUpObj.setConvList(powerUpMap.get(powerUpType));
				myPowerUpList.add(myPowerUpObj);
			}
			responseObj.setMpuList(myPowerUpList);
			responseObj.setMr(myReward);
			responseObj.setStatus(1);
			responseObj.setMsg("Success.!");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"Success");
			return responseObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting my power ups");
			responseObj.setError(error);
			responseObj.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"Error occured while gettting my power ups");
			return responseObj;
		}
	}
	
	
	@RequestMapping(value="/PowerUpRedemption",method=RequestMethod.POST)
	public @ResponsePayload MyPowerUpResponse powerUpRedemption(HttpServletRequest request, Model model,HttpServletResponse response){
		MyPowerUpResponse responseObj =new MyPowerUpResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You are not authorized");
				return responseObj;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						responseObj.setError(error);
						responseObj.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You are not authorized");
						return responseObj;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You are not authorized");
					return responseObj;
				}
			}else{
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You are not authorized");
				return responseObj;
			}
			
			String customerIdStr = request.getParameter("cId");
			String powerUpIdStr = request.getParameter("powerUpId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You are not authorized");
				return responseObj;
				
			}
			
			Integer customerId = null;
			Customer customerObj = null;
			try {
				customerId = Integer.parseInt(customerIdStr);
				//CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				customerObj = DAORegistry.getCustomerDAO().get(customerId);
				if(null == customerObj) {
					error.setDescription("Customer not recognized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"Customer not recognized");
					return responseObj;
				}
				
				if(null == customerObj.getRtfPoints() || customerObj.getRtfPoints() <= 0) {
					error.setDescription("You do not have enough reward points.!");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You do not have enough reward points.!");
					return responseObj;
				}
			}catch(Exception e) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You are not authorized");
				return responseObj;
			}
			
			if(TextUtil.isEmptyOrNull(powerUpIdStr)) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You are not authorized");
				return responseObj;
				
			}
			
			Integer powerUpId = null;
			RtfPointsConversionSetting powerUpObj = null; 
			try {
				powerUpId = Integer.parseInt(powerUpIdStr);
				powerUpObj = QuizDAORegistry.getRtfPointsConversionSettingDAO().get(powerUpId);
				if(null == powerUpObj) {
					error.setDescription("Power up selection not recognized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"Power up selection not recognized");
					return responseObj;
				}
			}catch(Exception e) {
				error.setDescription("Power up selection not recognized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"Power up selection not recognized");
				return responseObj;
			}
			
			if(customerObj.getRtfPoints() < powerUpObj.getRtfPoints()) {
				error.setDescription("You do not have enough reward points.Your available reward points are "+customerObj.getRtfPoints());
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PPOWERUPREDEMPTION,"You do not have enough reward points.Your available reward points are "+customerObj.getRtfPoints());
				return responseObj;
			}
			
			
			RtfCustomerRewardRedemptionHistory history = new RtfCustomerRewardRedemptionHistory();
			history.setCreatedDate(new Date());
			history.setCustomerId(customerId);
			history.setRtfPointsDebited(powerUpObj.getRtfPoints());
			
			try {
				
				customerObj.setRtfPoints(customerObj.getRtfPoints() - powerUpObj.getRtfPoints());
				
				switch (powerUpObj.getPowerUpType()) {
					case Lives:
						customerObj.setQuizCustomerLives(customerObj.getQuizCustomerLives()+powerUpObj.getQty());
						//DAORegistry.getCustomerDAO().updateQuizCustomerLives(customerId, powerUpObj.getQty());
						CustomerUtil.updatedCustomerUtil(customerObj);
						try {
							 CassandraDAORegistry.getCassCustomerDAO().updateCustomerLivesAndRtfPoints(customerObj.getId(), customerObj.getQuizCustomerLives(),customerObj.getRtfPoints());
						}catch(Exception e) {
							e.printStackTrace();
						}
						history.setPointRedemptionType(PointRedemptionType.LIFE);
						history.setNoOfLives(powerUpObj.getQty());
						break;
						
	
					case MagicWands:
						
						 customerObj.setMagicWands(customerObj.getMagicWands()+powerUpObj.getQty());
						 //DAORegistry.getCustomerDAO().saveOrUpdate(customerObj);;
						 CustomerUtil.updatedCustomerUtil(customerObj);
						 CassandraDAORegistry.getCassCustomerDAO().updateCustomerMagicWandAndRtfPoints(customerObj.getId(), customerObj.getMagicWands(),customerObj.getRtfPoints());
						 history.setPointRedemptionType(PointRedemptionType.MAGICWAND);
						 history.setNoOfMagicWands(powerUpObj.getQty());
						break;
						
	
					case RewardDollars:
						CustomerLoyalty custLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
						
						if(custLoyalty != null) {
							CustomerLoyaltyTracking loyaltyTracking = new CustomerLoyaltyTracking();
							//participantLoyaltyTracking.setContestId(contest.getId());
							loyaltyTracking.setCreatedBy(powerUpObj.getPowerUpType().toString()+" Redemption from RTF Points");
							loyaltyTracking.setCreatedDate(new Date());
							loyaltyTracking.setCustomerId(customerId);
							loyaltyTracking.setRewardPoints(powerUpObj.getQty().doubleValue());
							loyaltyTracking.setRewardType(powerUpObj.getPowerUpType().toString());
							
							custLoyalty.setActivePointsAsDouble(custLoyalty.getActivePointsAsDouble() + powerUpObj.getQty().doubleValue());
							custLoyalty.setDollerConversion(1.00);
							custLoyalty.setLastUpdate(new Date());
							custLoyalty.setLatestEarnedPointsAsDouble(powerUpObj.getQty().doubleValue());
							custLoyalty.setLatestSpentPointsAsDouble(0.00);
							custLoyalty.setTotalEarnedPoints(custLoyalty.getTotalEarnedPoints()+powerUpObj.getQty().doubleValue());
							custLoyalty.setNotifiedMessage(powerUpObj.getPowerUpType().toString()+" Redemption from RTF Points : "+powerUpObj.getQty().doubleValue());
							 
							DAORegistry.getCustomerLoyaltyTrackingDAO().save(loyaltyTracking);
							DAORegistry.getCustomerLoyaltyDAO().update(custLoyalty);
							
							try {
								CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsAndRtfPointsByCustomerId(customerId,custLoyalty.getActivePointsAsDouble(),customerObj.getRtfPoints());
							}catch(Exception e) {
								e.printStackTrace();
							}
							history.setPointRedemptionType(PointRedemptionType.REWARDDOLLARS);
							history.setRewardDollar(powerUpObj.getQty().doubleValue());
						}
						break;
		
					default:
						break;
				}
				
				DAORegistry.getCustomerDAO().update(customerObj);
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			QuizDAORegistry.getRtfCustomerRewardRedemptionHistoryDAO().save(history);
			
			MyReward myReward = SQLDaoUtil.getCustomerRewards(customerId);
			responseObj.setMr(myReward);
			responseObj.setStatus(1);
			responseObj.setMsg("You have redeemed successfully.!");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"Success");
			return responseObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting my power ups");
			responseObj.setError(error);
			responseObj.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYPOWERUP,"Error occured while gettting my power ups");
			return responseObj;
		}
	}
	
	/*
	@RequestMapping(value="/GetMySFLevel",method=RequestMethod.POST)
	public @ResponsePayload MySfLevelResponse getMySFLevel(HttpServletRequest request, Model model,HttpServletResponse response){
		MySfLevelResponse responseObj =new MySfLevelResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						responseObj.setError(error);
						responseObj.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
						return responseObj;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
					return responseObj;
				}
			}else{
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
			}
			
			String customerIdStr = request.getParameter("cId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
				
			}
			
			Integer customerId = null;
			try {
				customerId = Integer.parseInt(customerIdStr);
				CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(null == cassCustomer) {
					error.setDescription("Customer not recognized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"Customer not recognized");
					return responseObj;
				}
			}catch(Exception e) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
			}
			
			QuizSuperFanStat superFanObj = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(customerId);
			Integer sfStar = null != superFanObj?superFanObj.getTotalNoOfChances():0;
			List<QuizSuperFanLevelConfig> levelList = QuizDAORegistry.getQuizSuperFanLevelConfigDAO().getAllActiveLevels();
			
			int i=0;
			Boolean iscomputed = false;
			for (QuizSuperFanLevelConfig levelObj : levelList) {
				i++;
				
				if(levelObj.getStarFrom() <= sfStar){
					if(levelObj.getStarTo() >= sfStar) {
						levelObj.setCustSfStars(sfStar);
						levelObj.setShowUserLevel(true);
						iscomputed = true;
						break;
					} 
				} 
				
				if(i == levelList.size() && !iscomputed) {
					levelObj.setCustSfStars(sfStar);
					levelObj.setShowUserLevel(true);
					iscomputed = true;
					break;
				}
			}
			 
			responseObj.setLevels(levelList);
			responseObj.setStatus(1);
			responseObj.setMsg("Success.!");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"Success");
			return responseObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting my super fan levels");
			responseObj.setError(error);
			responseObj.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"Error occured while gettting my super fan levels");
			return responseObj;
		}
	}
	*/
	
	
	@RequestMapping(value="/GetMySFLevel",method=RequestMethod.POST)
	public @ResponsePayload MySfLevelResponse getMySFLevelNew(HttpServletRequest request, Model model,HttpServletResponse response){
		MySfLevelResponse responseObj =new MySfLevelResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						responseObj.setError(error);
						responseObj.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
						return responseObj;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
					return responseObj;
				}
			}else{
				error.setDescription("You are not authorized.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
			}
			
			String customerIdStr = request.getParameter("cId");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
				
			}
			
			Integer customerId = null;
			try {
				customerId = Integer.parseInt(customerIdStr);
				CassCustomer cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(null == cassCustomer) {
					error.setDescription("Customer not recognized.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"Customer not recognized");
					return responseObj;
				}
			}catch(Exception e) {
				error.setDescription("Customer Id is required.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"You are not authorized");
				return responseObj;
			}
			
			QuizSuperFanStat superFanObj = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(customerId);
			Integer sfStar = null != superFanObj?superFanObj.getTotalNoOfChances():0;
			List<QuizSuperFanLevelConfig> levelList = QuizDAORegistry.getQuizSuperFanLevelConfigDAO().getAllActiveLevels();
			for (QuizSuperFanLevelConfig levelObj : levelList) {
				if(TextUtil.between(sfStar, levelObj.getStarFrom(), levelObj.getStarTo())) {
					levelObj.setCustSfStars(sfStar);
					levelObj.setShowUserLevel(true);
					levelObj.setReleaseLock(true);
				}else if(TextUtil.greaterThanMax(sfStar, levelObj.getStarTo())) {
					levelObj.setReleaseLock(true);
				} 
			}
			responseObj.setSflDesc("EARN STARS TO UNLOCK NEW LEVELS EACH TIME TO GET A FREE PASS TO THOSE LEVEL QUESTIONS.");
			responseObj.setLevels(levelList);
			responseObj.setStatus(1);
			responseObj.setMsg("Success.!");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"Success");
			return responseObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting my super fan levels");
			responseObj.setError(error);
			responseObj.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETMYSFLEVELS,"Error occured while gettting my super fan levels");
			return responseObj;
		}
	}
	
	
	
}	