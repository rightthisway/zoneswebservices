package com.rtftrivia.ws.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.dao.implementation.CassandraDAORegistry;
import com.quiz.cassandra.data.CassCustomer;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.rtfquiz.webservices.dao.implementaion.QuizDAORegistry;
import com.rtfquiz.webservices.data.QuizSuperFanStat;
import com.rtfquiz.webservices.data.RtfGiftCardRespose;
import com.rtfquiz.webservices.utils.RTFBotsUtil;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CategoryTicket;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.GiftCardBrand;
import com.zonesws.webservices.data.GiftCardBrandDTO;
import com.zonesws.webservices.data.GiftCardFileAttachment;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.data.OrderPaymentBreakup;
import com.zonesws.webservices.data.OrderTicketGroup;
import com.zonesws.webservices.data.Property;
import com.zonesws.webservices.data.RtfCatTicket;
import com.zonesws.webservices.data.RtfGiftCard;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.data.RtfGiftCardQuantity;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.InvoiceStatus;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PartialPaymentMethod;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.enums.TransactionType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.ShareDriveUtil;
import com.zonesws.webservices.utils.CountryUtil;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.GiftCardUtil;
import com.zonesws.webservices.utils.PaginationUtil;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TixByQty;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.CustomerOrderResponse;
import com.zonesws.webservices.utils.list.RtfEvent;
import com.zonesws.webservices.utils.list.RtfOrderConfirmation;
import com.zonesws.webservices.utils.list.RtfTixList;
import com.zonesws.webservices.utils.list.SuperFanStat;
import com.zonesws.webservices.utils.mail.MailManager;

import jcifs.smb.SmbFile;

@Controller
@RequestMapping({"/GetGiftCards","/CreateGiftCardOrder","/ViewGiftCardOrder","/GetRtfCatsTix","/RtfCreateOrder","/ViewTriviaOrder","/GetSuperFanStats",
	"/ListMyGiftCards","/UpdateGiftCardOrder","/LoadTestCustomersMap","/GetGiftCardBrands","/GetGiftCardsByBrand"})
public class RtfTriviaTicketController {
	
	private MailManager mailManager;
	private ZonesProperty properties;
	public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat dtFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	@RequestMapping(value="/GetGiftCardBrands",method=RequestMethod.POST)
	public @ResponsePayload RtfGiftCardRespose getGiftCardBrands(HttpServletRequest request,HttpServletResponse response){
		RtfGiftCardRespose cards =new RtfGiftCardRespose();
		Error error = new Error();
		String pageNoStr = request.getParameter("pNo");
		String customerIdStr = request.getParameter("cId");
		String platForm = request.getParameter("pfm"); 
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see tickets.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Please login to see Gift Cards");
				return cards;
			}
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Customer is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Customer is not recognized");
				return cards;
			}
			List<GiftCardBrand> list = DAORegistry.getGiftCardBrandDAO().getAllActiveGiftCardBrands();
			List<GiftCardBrandDTO> responseList = new ArrayList<GiftCardBrandDTO>();
			GiftCardBrandDTO object = new GiftCardBrandDTO();
			for (GiftCardBrand giftCardBrand : list) {
				List<RtfGiftCard> giftCardQtyList =DAORegistry.getQueryManagerDAO().getAvailableGiftCardsByBrand(giftCardBrand.getId()); 
				String imageUrl = giftCardBrand.getImageUrl();
				Boolean noactiveQtyflag = false; 
				if(null == giftCardQtyList || giftCardQtyList.isEmpty() || giftCardQtyList.size() <= 0) {
					if(!giftCardBrand.getDisplayOutOfStack()) {
						continue;
					} 
					imageUrl = giftCardBrand.getOutOfStackImageUrl();
					noactiveQtyflag = true;
				}
				object = new GiftCardBrandDTO();
				object.setbId(giftCardBrand.getId());
				object.setbName(giftCardBrand.getName());
				object.setbDesc(giftCardBrand.getDescription());
				object.setImgUrl(AWSFileService.getGiftCardBrandImage(imageUrl));
				object.setNacards(noactiveQtyflag);
				responseList.add(object);
			}
			cards.setSts(1);
			cards.setGfBrands(responseList);
			return cards;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Something is not right. You do not have any gift cards right now.!");
			cards.setErr(error);
			cards.setSts(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GIFTCARDBRANDS,"Error Occured While Getting Gift Card Brands");
			return cards;
		}
	}
	
	

	@RequestMapping(value="/GetGiftCardsByBrand",method=RequestMethod.POST)
	public @ResponsePayload RtfGiftCardRespose GetGiftCardsByBrand(HttpServletRequest request,HttpServletResponse response){
		RtfGiftCardRespose cards =new RtfGiftCardRespose();
		Error error = new Error();
		String pageNoStr = request.getParameter("pNo");
		String customerIdStr = request.getParameter("cId");
		String giftCardBrandIdStr = request.getParameter("gfcbId");
		String platForm = request.getParameter("pfm"); 
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see tickets.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Please login to see Gift Cards");
				return cards;
			}
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Customer is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Customer is not recognized");
				return cards;
			}
			
			Integer giftCardBrandId = null;
			GiftCardBrand giftCardBrand = null;
			try {
				giftCardBrandId=Integer.parseInt(giftCardBrandIdStr);
				giftCardBrand = DAORegistry.getGiftCardBrandDAO().get(giftCardBrandId);
				if(giftCardBrand==null){
					error.setDescription("Gift Card Brand is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Gift Card Brand is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Gift Card Brand is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Gift Card Brand is not recognized");
				return cards;
			}
			
			List<RtfGiftCard> list =DAORegistry.getQueryManagerDAO().getAvailableGiftCardsByBrand(giftCardBrandId); 
			for(RtfGiftCard card : list){
				List<RtfGiftCardQuantity> qList = new ArrayList<RtfGiftCardQuantity>();
				for(int i=1;i<=card.getMaxThresholdQty();i++){
					RtfGiftCardQuantity quantity = new RtfGiftCardQuantity();
					quantity.setTotalAmount(card.getAmount()*i);
					if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
						quantity.setRequireRewards(card.getRewardDollarConvRate() * (card.getAmount()*i));
					}else{
						quantity.setRequireRewards(card.getRewardDollarConvRate() * (card.getAmount()*i));
					}
					quantity.setRedeemPopupMsg("Total reward dollars spend : $"+quantity.getRequiredRewardString());
					quantity.setQty(i);
					qList.add(quantity);
				}
				card.setImageUrl(AWSFileService.getGiftCardBrandImage(giftCardBrand.getImageUrl()));
				card.setQuantity(qList);
			}
			
			/*GiftCardBrandDTO object = new GiftCardBrandDTO();
			object.setbId(giftCardBrand.getId());
			object.setbName(giftCardBrand.getName());
			object.setbDesc(giftCardBrand.getDescription());
			object.setImgUrl(AWSFileService.getGiftCardBrandImage(1));
			object.setlCost("5");
			cards.setBrandDTO(object);*/
			
			cards.setSts(1);
			cards.setCards(list);
			return cards;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while getting gift cards.");
			cards.setErr(error);
			cards.setSts(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGIFTCARDS,"Error occured while getting gift cards");
			return cards;
		}
	}
	
	
	
	@RequestMapping(value="/GetGiftCards",method=RequestMethod.POST)
	public @ResponsePayload RtfGiftCardRespose getGiftCards(HttpServletRequest request,HttpServletResponse response){
		RtfGiftCardRespose cards =new RtfGiftCardRespose();
		Error error = new Error();
		String pageNoStr = request.getParameter("pNo");
		String customerIdStr = request.getParameter("cId");
		String platForm = request.getParameter("pfm"); 
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see tickets.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Please login to see Gift Cards");
				return cards;
			}
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					cards.setErr(error);
					cards.setSts(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Customer is not recognized");
					return cards;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				cards.setErr(error);
				cards.setSts(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Customer is not recognized");
				return cards;
			}
			List<RtfGiftCard> list =DAORegistry.getQueryManagerDAO().getAvailableGiftCards(); 
			for(RtfGiftCard card : list){
				List<RtfGiftCardQuantity> qList = new ArrayList<RtfGiftCardQuantity>();
				for(int i=1;i<=card.getMaxThresholdQty();i++){
					RtfGiftCardQuantity quantity = new RtfGiftCardQuantity();
					quantity.setTotalAmount(card.getAmount()*i);
					if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
						quantity.setRequireRewards(card.getRewardDollarConvRate() * (card.getAmount()*i));
					}else{
						quantity.setRequireRewards(card.getRewardDollarConvRate() * (card.getAmount()*i));
					}
					quantity.setRedeemPopupMsg("Total reward dollars spend : $"+quantity.getRequiredRewardString());
					quantity.setQty(i);
					qList.add(quantity);
				}
				card.setQuantity(qList);
			}
			cards.setSts(1);
			cards.setCards(list);
			return cards;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while getting gift cards.");
			cards.setErr(error);
			cards.setSts(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Error occured while getting gift cards");
			return cards;
		}
	}
	
	
	@RequestMapping(value="/CreateGiftCardOrder",method=RequestMethod.POST)
	public synchronized @ResponsePayload RtfOrderConfirmation createGiftCardOrder(HttpServletRequest request,HttpServletResponse response){
		RtfOrderConfirmation orderConfirmation = new RtfOrderConfirmation();
		Error error = new Error();
		String gcIdStr = request.getParameter("gcId");
		String customerIdStr = request.getParameter("cId");
		String qtyStr = request.getParameter("qty");
		String platFormStr = request.getParameter("pfm");
		String shippingIdStr = request.getParameter("sId");
		
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to create order.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Please login to create order");
				return orderConfirmation;
			}
			if(null == gcIdStr || gcIdStr.isEmpty()) {
				error.setDescription("Gift Card is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Gift Card is not recognized");
				return orderConfirmation;
			}
			if(null == qtyStr || qtyStr.isEmpty()) {
				error.setDescription("Please select valid Quantity.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Please select valid Quantity");
				return orderConfirmation;
			}
			if(null == shippingIdStr || shippingIdStr.isEmpty()) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			Integer shippingId = Integer.parseInt(shippingIdStr);
			UserAddress shipping = DAORegistry.getUserAddressDAO().get(shippingId);
			
			if(null == shipping) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			Integer customerId = null;
			Integer qty = null;
			Integer gcvId = null;
			Customer customer = null;
			RtfGiftCard card = null;
			RtfGiftCardQuantity giftCardQty = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				
				if(customerId == 0) {
					customerId = getRandomCustomerIdForTest1();
				}
				
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Customer is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Customer is not recognized");
				return orderConfirmation;
			}
			try {
				qty=Integer.parseInt(qtyStr);
			} catch (Exception e) {
				error.setDescription("Please select valid Quantity.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Please select valid Quantity");
				return orderConfirmation;
			}
			try {
				gcvId=Integer.parseInt(gcIdStr);
				giftCardQty = DAORegistry.getRtfGiftCardQuantityDAO().get(gcvId);
				card = DAORegistry.getRtfGiftCardDAO().get(giftCardQty.getCardId());
				if(card == null || !card.getStatus() || giftCardQty == null){
					error.setDescription("Gift card id is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Gift card id is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Gift card id is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Gift card id is not recognized");
				return orderConfirmation;
			}
			
			if(qty > giftCardQty.getMaxThresholdQty()){
				error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"User trying to purchase more than the threshold qty.");
				return orderConfirmation;
			}
			
			if(GiftCardUtil.enableNextOrder) {
				
				/*Date lastPurcahseDate = DAORegistry.getQueryManagerDAO().getRecentOrderDate(customerId);
				
				if(null != lastPurcahseDate) {
					
					Boolean blockPurchase = GiftCardUtil.validatePurchase(lastPurcahseDate);
					
					if(null != blockPurchase && blockPurchase){
						error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
						orderConfirmation.setError(error);
						orderConfirmation.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"User already purcahsed maximum therhold qty..");
						return orderConfirmation;
					}
				}*/
				
				String curDate = dateFormat.format(new Date());
				String fromDateStr = curDate+" 00:01:00.000",endDateStr = curDate+" 23:59:59.000";;
				Boolean isCustomerCreatedSingleOrder = DAORegistry.getQueryManagerDAO().isCustomerCreatedSingleOrder(customerId,fromDateStr,endDateStr);
				
				if(null != isCustomerCreatedSingleOrder && isCustomerCreatedSingleOrder) {
					error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"User already purcahsed maximum therhold qty..");
					return orderConfirmation;
				}
				
			}else {
				Integer qtyPurchasedByThisCust = DAORegistry.getQueryManagerDAO().getTotalPurchasedQtyByCustomerAndGiftCard(customerId, gcvId);
				
				System.out.println("GIFTCARD ORDER:  CARD ID: "+giftCardQty.getCardId()+", GCVIDDB: "+giftCardQty.getId()+", Gift Card Value ID: "+gcvId+", CustomerId: "+customerId+", QTY: "+qty+", THreshold QTY: "+giftCardQty.getMaxThresholdQty()+", QTY PUrcahsed: "+qtyPurchasedByThisCust);
				
				if(null != qtyPurchasedByThisCust && qtyPurchasedByThisCust >= giftCardQty.getMaxThresholdQty()){
					error.setDescription("At the current time, gift card redemptions are limited to one gift card per user.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"User already purcahsed maximum therhold qty..");
					return orderConfirmation;
				}
			}
			
			
			if(qty > giftCardQty.getFreeQty()){
				error.setDescription("We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				return orderConfirmation;
			}
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			CustomerLoyalty customerRewards = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			
			if(customerRewards == null){
				customerRewards = new CustomerLoyalty();
				customerRewards.setActivePoints("0.00");
				customerRewards.setActivePointsAsDouble(0.00);
				customerRewards.setActiveRewardDollers(0.00);
				customerRewards.setContestRewardDollars("0.00");
				customerRewards.setCurRewardPointBalance("0.00");
				customerRewards.setCustomerId(customerId);
				customerRewards.setDollerConversion(1.00);
				customerRewards.setLastAffiliateReferralDollars(0.00);
				customerRewards.setLastReferralDollars(0.00);
				customerRewards.setLastUpdate(new Date());
				customerRewards.setLatestEarnedPoints("0.00");
				customerRewards.setLatestEarnedPointsAsDouble(0.00);
				customerRewards.setLatestSpentPointsAsDouble(0.00);
				customerRewards.setPendingPoints("0.00");
				customerRewards.setPendingPointsAsDouble(0.00);
				customerRewards.setRevertedPoints("0.00");
				customerRewards.setRevertedPointsAsDouble(0.00);
				customerRewards.setShowAffiliateReward(false);
				customerRewards.setShowReferralReward(true);
				customerRewards.setTotalAffiliateReferralDollars(0.00);
				customerRewards.setTotalAffiliateReferralDollarsStr("0.00");
				customerRewards.setTotalEarnedPoints("0.00");
				customerRewards.setTotalEarnedPointsAsDouble(0.00);
				customerRewards.setTotalReferralDollars(0.00);
				customerRewards.setTotalReferralDollarsStr("0.00");
				customerRewards.setTotalSpentPoints("0.00");
				customerRewards.setTotalSpentPointsAsDouble(0.00);
				customerRewards.setVoidedPoints("0.00");
				customerRewards.setVoidedPointsAsDouble(0.00);
				DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerRewards);
			}
			Double oldActiveRewards =customerRewards.getActivePointsAsDouble();
			
			Double requiredAmount = 0.00;
			
			if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
				requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardDollarConvRate());
			}else{
				requiredAmount = TicketUtil.getRoundedUpValue((giftCardQty.getAmount() * qty) * card.getRewardDollarConvRate());
			}
			
			Double activeRewards = TicketUtil.getRoundedUpValue(customerRewards.getActivePointsAsDouble());
			 
			if(requiredAmount > activeRewards) {
				error.setDescription("You do not have enough reward dollars to redeem these gift card.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"You do not have enough reward dollars to redeem these gift card.");
				return orderConfirmation;
			}
			
			List<Integer> botCustIds = RTFBotsUtil.getAllBots();
			Boolean isCustomerCreatedTwoOrder = DAORegistry.getQueryManagerDAO().isCustomerCreatedTwoOrder(customerId);
			if(botCustIds.contains(customerId) || isCustomerCreatedTwoOrder){
				error.setDescription("Sorry, another Fan has already redeemed this gift card.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Sorry, another Fan has already redeemed this gift card.");
				return orderConfirmation;
			}
			
			
			RtfGiftCardOrder order  = new RtfGiftCardOrder();
			order.setCardDescription(card.getDescription());
			order.setCardId(gcvId);
			order.setCardTitle(card.getTitle());
			order.setCreatedBy(customer.getUserId());
			order.setCreatedDate(new Date());
			order.setCustomerId(customerId);
			order.setIsEmailSent(false);
			order.setIsOrderFulfilled(false);
			order.setIsOrderFilledMailSent(false);
			order.setOrderType(OrderType.REGULAR);
			order.setOriginalCost(requiredAmount);
			order.setQuantity(qty);
			order.setRedeemedRewards(requiredAmount);
			order.setStatus("OUTSTANDING");
			order.setShippingAddressId(shippingId);
			
			RtfGiftCardQuantity validationObj = DAORegistry.getRtfGiftCardQuantityDAO().get(gcvId);
			if(validationObj.getFreeQty() == 0 || qty > validationObj.getFreeQty()){
				error.setDescription("We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"We�re sorry, this "+card.getTitle()+" gift card has already been redeemed by another fan");
				return orderConfirmation;
			}
			
			Integer freeQty = validationObj.getFreeQty()-qty;
			Integer usedQty = validationObj.getUsedQty()+qty;
			
			validationObj.setFreeQty(freeQty);
			validationObj.setUsedQty(usedQty);
			
			DAORegistry.getRtfGiftCardQuantityDAO().update(validationObj);
			DAORegistry.getRtfGiftCardOrderDAO().save(order);
			
			customerRewards.setLastUpdate(new Date());
			customerRewards.setActivePointsAsDouble(customerRewards.getActivePointsAsDouble()-requiredAmount);
			customerRewards.setTotalSpentPointsAsDouble(customerRewards.getTotalSpentPointsAsDouble()+requiredAmount);
			customerRewards.setLatestSpentPointsAsDouble(requiredAmount);
			customerRewards.setLastUpdate(new Date());
			
			try {
				UserAddress shippingAddr = DAORegistry.getUserAddressDAO().get(shippingId);
				CustomerOrderDetail orderDetail = new CustomerOrderDetail();
				orderDetail.setShippingAddress1(shippingAddr.getAddressLine1());
				orderDetail.setShippingAddress2(shippingAddr.getAddressLine2());
				orderDetail.setShippingCountryId(shippingAddr.getCountry().getId());
				orderDetail.setShippingStateId(shippingAddr.getState().getId());
				orderDetail.setShippingCity(shippingAddr.getCity());
				orderDetail.setShippingCountry(shippingAddr.getCountryName());
				orderDetail.setShippingEmail(shippingAddr.getEmail());
				orderDetail.setShippingFirstName(shippingAddr.getFirstName());
				orderDetail.setShippingLastName(shippingAddr.getLastName());
				orderDetail.setCtyPhCode(null != shippingAddr.getCtyPhCode() && !shippingAddr.getCtyPhCode().isEmpty()?shippingAddr.getCtyPhCode():CountryUtil.defaultCountryPhoneCode);
				orderDetail.setShippingPhone1(shippingAddr.getPhone1());
				orderDetail.setShippingPhone2(shippingAddr.getPhone2());
				orderDetail.setShippingState(shippingAddr.getStateName());
				orderDetail.setShippingZipCode(shippingAddr.getZipCode());
				orderDetail.setOrderId(order.getId());
				orderDetail.setOrderType("GIFTCARD");
				DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			CustomerLoyaltyHistory offerRewardHistory = new CustomerLoyaltyHistory();
			offerRewardHistory.setCustomerId(customerId);
			offerRewardHistory.setOrderId(order.getId());
			offerRewardHistory.setRewardConversionRate(loyaltySettings.getRewardConversion());
			offerRewardHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
			offerRewardHistory.setPointsSpentAsDouble(requiredAmount);
			offerRewardHistory.setRewardSpentAmount(0.00);
			offerRewardHistory.setPointsEarnedAsDouble(0.00);
			offerRewardHistory.setRewardEarnAmount(0.00);
			offerRewardHistory.setInitialRewardPointsAsDouble(oldActiveRewards);
			offerRewardHistory.setInitialRewardAmount(customerRewards.getActiveRewardDollers());
			offerRewardHistory.setOrderType(OrderType.GIFTCARDORDER);
			offerRewardHistory.setCreateDate(new Date());
			offerRewardHistory.setUpdatedDate(new Date());
			offerRewardHistory.setOrderTotal(requiredAmount);
			offerRewardHistory.setOrderId(order.getId());
			offerRewardHistory.setRewardStatus(RewardStatus.ACTIVE);
			
			Double balanceRewardPointsNew = oldActiveRewards + requiredAmount - 0;
			Double balanceRewardAmountNew = RewardConversionUtil.getRewardDoller(balanceRewardPointsNew,loyaltySettings.getDollerConversion());
			
			offerRewardHistory.setBalanceRewardPointsAsDouble(balanceRewardPointsNew);
			offerRewardHistory.setBalanceRewardAmount(balanceRewardAmountNew);
			
			try {
				//NEED TO DISCUSS
				CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customerId, customerRewards.getActivePointsAsDouble());
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			DAORegistry.getCustomerLoyaltyDAO().update(customerRewards);
			DAORegistry.getCustomerLoyaltyHistoryDAO().save(offerRewardHistory);
			
			orderConfirmation.setoId(order.getId());
			
			if(null != platForm &&  platForm.equals(ApplicationPlatForm.ANDROID)) {
				orderConfirmation.setStatus(2);
				error.setDescription("Gift Card Order Created Successfully.");
				orderConfirmation.setError(error);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDER,"Success");
				return orderConfirmation;
			}else {
				orderConfirmation.setStatus(1);
			}
			orderConfirmation.setMsg("Gift Card Order Created Successfully");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDER,"Success");
			return orderConfirmation;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error Occured while creating gift card order.");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Error Occured while creating gift card order.");
			return orderConfirmation;
		}
	}
	
	
	@RequestMapping(value="/UpdateGiftCardOrder",method=RequestMethod.POST)
	public @ResponsePayload RtfOrderConfirmation updateGiftCardOrder(HttpServletRequest request,HttpServletResponse response){
		RtfOrderConfirmation orderConfirmation = new RtfOrderConfirmation();
		Error error = new Error();
		String oIdStr = request.getParameter("oId");
		String customerIdStr = request.getParameter("cId");
		String platFormStr = request.getParameter("pfm");
		String shippingIdStr = request.getParameter("sId");
		
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to create order.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Please login to create order");
				return orderConfirmation;
			}
			if(null == oIdStr || oIdStr.isEmpty()) {
				error.setDescription("Gift Card is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Gift Card is not recognized");
				return orderConfirmation;
			}
			 
			if(null == shippingIdStr || shippingIdStr.isEmpty()) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			Integer shippingId = Integer.parseInt(shippingIdStr);
			UserAddress shipping = DAORegistry.getUserAddressDAO().get(shippingId);
			
			if(null == shipping) {
				error.setDescription("Shipping address is mandatory.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Shipping address is mandatory.");
				return orderConfirmation;
			}
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			Integer customerId = null;
			Integer qty = null;
			Integer oId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = CustomerUtil.getCustomerById(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Customer is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Customer is not recognized");
				return orderConfirmation;
			}
			 
			
			RtfGiftCardOrder order = null;
			try {
				oId=Integer.parseInt(oIdStr);
				order = DAORegistry.getRtfGiftCardOrderDAO().get(oId);
				 
				if(order == null){
					error.setDescription("Gift card order is not recognized.");
					orderConfirmation.setError(error);
					orderConfirmation.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Gift card order is not recognized");
					return orderConfirmation;
				}
			} catch (Exception e) {
				error.setDescription("Gift card order is not recognized.");
				orderConfirmation.setError(error);
				orderConfirmation.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Gift card order is not recognized");
				return orderConfirmation;
			}
			order.setShippingAddressId(shippingId);
			
			
			try {
				UserAddress shippingAddr = DAORegistry.getUserAddressDAO().get(shippingId);
				CustomerOrderDetail orderDetail = new CustomerOrderDetail();
				orderDetail.setShippingAddress1(shippingAddr.getAddressLine1());
				orderDetail.setShippingAddress2(shippingAddr.getAddressLine2());
				orderDetail.setShippingCountryId(shippingAddr.getCountry().getId());
				orderDetail.setShippingStateId(shippingAddr.getState().getId());
				orderDetail.setShippingCity(shippingAddr.getCity());
				orderDetail.setShippingCountry(shippingAddr.getCountryName());
				orderDetail.setShippingEmail(shippingAddr.getEmail());
				orderDetail.setShippingFirstName(shippingAddr.getFirstName());
				orderDetail.setShippingLastName(shippingAddr.getLastName());
				orderDetail.setShippingPhone1(shippingAddr.getPhone1());
				orderDetail.setShippingPhone2(shippingAddr.getPhone2());
				orderDetail.setShippingState(shippingAddr.getStateName());
				orderDetail.setShippingZipCode(shippingAddr.getZipCode());
				orderDetail.setOrderId(order.getId());
				orderDetail.setOrderType("GIFTCARD");
				DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			order.setStatus("OUTSTANDING");
			DAORegistry.getRtfGiftCardOrderDAO().update(order);
			orderConfirmation.setoId(order.getId());
			orderConfirmation.setStatus(1);
			orderConfirmation.setMsg("Order Updated Successfully");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UPDATEGIFTCARDORDER,"Success");
			return orderConfirmation;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error Occured while updating gift card order.");
			orderConfirmation.setError(error);
			orderConfirmation.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.UPDATEGIFTCARDORDER,"Error Occured while updating gift card order.");
			return orderConfirmation;
		}
	}
	
	
	
	
	@RequestMapping(value="/ViewGiftCardOrder",method=RequestMethod.POST)
	public @ResponsePayload CustomerOrderResponse viewGiftCardOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerOrderResponse resObj = new CustomerOrderResponse();
		Error error = new Error();
		String orderIdStr = request.getParameter("oId");
		String customerIdStr = request.getParameter("cId");
		String platFormStr = request.getParameter("pfm"); 
		String msg = "";
		try {
			 
			String ip =((HttpServletRequest)request).getHeader("X-Forwarded-For");
			String configIdString = request.getParameter("configId");
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				msg ="Please login to see your gift card order.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,msg);
				return resObj;
			}
			
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = DAORegistry.getCustomerDAO().get(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Customer is not recognized");
					return resObj;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Customer is not recognized");
				return resObj;
			}
			
			Integer orderId = Integer.parseInt(orderIdStr);
			
			CustomerOrder order = new CustomerOrder();
			RtfGiftCardOrder giftCardOrder = DAORegistry.getRtfGiftCardOrderDAO().get(orderId);
			
			if(giftCardOrder == null) {
				error.setDescription("Invalid Order.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Invalid Order");
				return resObj;
			}
			
			if(!giftCardOrder.getCustomerId().equals(customerId)) {
				error.setDescription("You do not have the access to view this order..");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"You do not have access to view this order..");
				return resObj;
			}
			CustomerLoyaltyHistory latDownloadRewardHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getLateDownloadCustomerLoyaltyHistoryByGiftCardOrderId(orderId);
			CustomerLoyalty custLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			UserAddress shippingAddr = DAORegistry.getUserAddressDAO().get(giftCardOrder.getShippingAddressId());
			
			RtfGiftCardQuantity cardValueObj = DAORegistry.getRtfGiftCardQuantityDAO().get(giftCardOrder.getCardId());
			
			RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(cardValueObj.getCardId());
			if(null != card.getGfcBrandId()) {
				GiftCardBrand giftCardBrand = DAORegistry.getGiftCardBrandDAO().get(card.getGfcBrandId());
				card.setImageUrl(AWSFileService.getGiftCardBrandImage(giftCardBrand.getImageUrl()));
			}
			
			order.setId(giftCardOrder.getId());
			order.setCreateDateTemp(giftCardOrder.getCreatedDate());
			order.setQty(giftCardOrder.getQuantity());
			order.setOrderTotalAsDouble(giftCardOrder.getRedeemedRewards());
			order.setOriginalOrderTotal(giftCardOrder.getOriginalCost());
			order.setTotalServiceFees(TicketUtil.getRoundedValueString(0.00));
			order.setSingleTixServiceFees(TicketUtil.getRoundedValueString(0.00));
			order.setCustomerLoyaltyHistory(latDownloadRewardHistory);
			order.setCustomer(customer);
			order.setIsShippingRequired(true);
			order.setEventName(giftCardOrder.getCardTitle());
			order.setDesktopShareLinkUrl(card.getImageUrl());
			order.setDescription(card.getDescription());
			order.setGfCardValue(TicketUtil.getRoundedValueString(cardValueObj.getAmount()));
			 
			/*CustomerOrderDetail orderDetail = new CustomerOrderDetail();
			orderDetail.setShippingAddress1(shippingAddr.getAddressLine1());
			orderDetail.setShippingAddress2(shippingAddr.getAddressLine2());
			orderDetail.setShippingCity(shippingAddr.getCity());
			orderDetail.setShippingCountry(shippingAddr.getCountryName());
			//orderDetail.setShippingCountryId(shippingAddr.getCountry().getId());
			orderDetail.setShippingEmail(shippingAddr.getEmail());
			orderDetail.setShippingFirstName(shippingAddr.getFirstName());
			orderDetail.setShippingLastName(shippingAddr.getLastName());
			orderDetail.setShippingPhone1(shippingAddr.getPhone1());
			orderDetail.setShippingPhone2(shippingAddr.getPhone2());
			orderDetail.setShippingState(shippingAddr.getStateName());
			orderDetail.setShippingZipCode(shippingAddr.getZipCode());
			orderDetail.setOrderId(order.getId());*/
			
			CustomerOrderDetail orderDetail = DAORegistry.getCustomerOrderDetailDAO().getGiftCardCustomerOrderDetailByOrderId(giftCardOrder.getId());
			order.setCustomerOrderDetail(orderDetail);
			
			String fontSizeStr = TicketUtil.getFontSize(request, platForm);
			order.setViewOrderType(String.valueOf(giftCardOrder.getOrderType()));
			
			String orderDeliveryNote= null;
			if(giftCardOrder.getStatus().equalsIgnoreCase("PENDING")){
				order.setButtonOption("GETSHIPPING");
				order.setTicketDownloadUrl(null);
				order.setShowInfoButton(Boolean.TRUE);
				order.setOrderDeliveryNote("Update Shipping");
				order.setDeliveryInfo(null);
			} else if(giftCardOrder.getStatus().equalsIgnoreCase("VOIDED")){
				orderDeliveryNote = "Order Cancelled";
				order.setButtonOption("OC");
				order.setOrderDeliveryNote(orderDeliveryNote);
				order.setDeliveryInfo(null);
			}else if(giftCardOrder.getIsOrderFulfilled()){
				
				List<GiftCardFileAttachment> fileAttachments = DAORegistry.getGiftCardFileAttachmentDAO().getAllAttachemntByOrderId(giftCardOrder.getId());
				
				List<InvoiceTicketAttachment> tickets = new ArrayList<>();
				InvoiceTicketAttachment attachment = null;
				for (GiftCardFileAttachment attachmentObj : fileAttachments) {
					attachment = new InvoiceTicketAttachment();
					attachment.setDownloadUrl(URLUtil.getGiftCardDownloadUrl(request,attachmentObj.getId(), giftCardOrder.getId(), configIdString,customerId,platForm,ip));
					attachment.setType(attachmentObj.getType()); 
					attachment.setPosition(attachmentObj.getPosition());
					attachment.setExtension(URLUtil.getFileExtension(attachmentObj.getFilePath()));
					attachment.setFileName(URLUtil.getGiftCardFileName(attachmentObj.getFilePath()));
					attachment.setTrxType(TransactionType.GFC);
					tickets.add(attachment);
				}
				
				orderDeliveryNote = "Download Gift Cards";
				order.setOrderDeliveryNote(orderDeliveryNote);
				order.setButtonOption("DL");
				order.setInvoiceTicketAttachment(tickets);
				order.setShippingMethod("E-Ticket");
				order.setDeliveryInfo(null);
			}else if(giftCardOrder.getStatus().equalsIgnoreCase("OUTSTANDING")){
				order.setButtonOption("PROGRESS");
				order.setTicketDownloadUrl(null);
				order.setShowInfoButton(Boolean.TRUE);
				order.setOrderDeliveryNote("INFO");
				TicketUtil.getGiftCardDeliveryInFo(order,fontSizeStr);
				TicketUtil.getGiftCardProgressPopupMsg(order,fontSizeStr);
				order.setShippingMethod("E-Ticket");
				order.setDeliveryInfo(null);
			}
			
			String convRateString = "";
			try {
				if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
					convRateString = "Conversion Rate : "+(TicketUtil.getRoundedValue(card.getRewardDollarConvRate())*100)+"%";
				}else{
					convRateString = "Conversion Rate : "+TicketUtil.getRoundedValueString(card.getRewardDollarConvRate());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			order.setConvRateString(convRateString);
			
			//order.setAllCustomerRelatedObjects();
			
			
			List<CustomerOrder> customerOrderList = new ArrayList<CustomerOrder>();
			customerOrderList.add(order);
			resObj.setCustRewards(custLoyalty.getActivePoints());
			resObj.setCustomerOrders(customerOrderList);
			resObj.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDERVIEW,"Success");
			return resObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Seems there was some issue while fecthing your order information.");
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Seems there was some issue while fecthing your order information.");
			return resObj;
		}
	}
	
	
	@RequestMapping(value="/ListMyGiftCards",method=RequestMethod.POST)
	public @ResponsePayload CustomerOrderResponse listMyGiftCards(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerOrderResponse resObj = new CustomerOrderResponse();
		Error error = new Error();
		String oStatus = request.getParameter("oStatus"); //ACTIVE, PAST
		String customerIdStr = request.getParameter("cId");
		String platFormStr = request.getParameter("pfm"); 
		String msg = "";
		try {
			
			String ip =((HttpServletRequest)request).getHeader("X-Forwarded-For");
			String configIdString = request.getParameter("configId");
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				msg ="Please login to see your gift card orders.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,msg);
				return resObj;
			}
			
			if(null == oStatus || oStatus.isEmpty()) {
				msg ="Order Status is mandatory.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,msg);
				return resObj;
			}
			
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			
			Integer customerId = null;
			Customer customer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				customer = DAORegistry.getCustomerDAO().get(customerId);
				if(customer==null){
					error.setDescription("Customer is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,"Customer is not recognized");
					return resObj;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,"Customer is not recognized");
				return resObj;
			}
			List<RtfGiftCardOrder> list = DAORegistry.getRtfGiftCardOrderDAO().getAllOrdersByCustomerId(customerId);
			if(list == null) {
				resObj.setCustomerOrders(new ArrayList<>());
				resObj.setStatus(1);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.LISTGIFTCARDS,"Success");
				return resObj;
			}
			
			CustomerOrder order = null;
			List<CustomerOrder> customerOrderList = new ArrayList<CustomerOrder>();
			for (RtfGiftCardOrder giftCardOrder : list) {
				
				order = new CustomerOrder();
				
				RtfGiftCardQuantity rtfGiftCardQuantity = DAORegistry.getRtfGiftCardQuantityDAO().get(giftCardOrder.getCardId());
				
				RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(rtfGiftCardQuantity.getCardId());
				
				if(null != card.getGfcBrandId()) {
					GiftCardBrand giftCardBrand = DAORegistry.getGiftCardBrandDAO().get(card.getGfcBrandId());
					card.setImageUrl(AWSFileService.getGiftCardBrandImage(giftCardBrand.getImageUrl()));
				}
				
				order.setId(giftCardOrder.getId());
				order.setCreateDateTemp(giftCardOrder.getCreatedDate());
				order.setQty(giftCardOrder.getQuantity());
				order.setOrderTotalAsDouble(giftCardOrder.getRedeemedRewards());
				order.setEventName(giftCardOrder.getCardTitle());
				order.setDesktopShareLinkUrl(card.getImageUrl());
				order.setDescription(card.getDescription());
				order.setGfCardValue(TicketUtil.getRoundedValueString(rtfGiftCardQuantity.getAmount()));
				
				String fontSizeStr = TicketUtil.getFontSize(request, platForm);
				order.setViewOrderType(String.valueOf(giftCardOrder.getOrderType()));
				
				String orderDeliveryNote= null;
				
				if(giftCardOrder.getStatus().equalsIgnoreCase("PENDING")){
					order.setButtonOption("GETSHIPPING");
					order.setTicketDownloadUrl(null);
					order.setShowInfoButton(Boolean.TRUE);
					order.setOrderDeliveryNote("Update Shipping");
					order.setDeliveryInfo(null);
				} else if(giftCardOrder.getStatus().equalsIgnoreCase("VOIDED")){
					orderDeliveryNote = "Order Cancelled";
					order.setButtonOption("OC");
					order.setOrderDeliveryNote(orderDeliveryNote);
					order.setDeliveryInfo(null);
				}else if(giftCardOrder.getIsOrderFulfilled()){
					
					List<GiftCardFileAttachment> fileAttachments = DAORegistry.getGiftCardFileAttachmentDAO().getAllAttachemntByOrderId(giftCardOrder.getId());
					List<InvoiceTicketAttachment> tickets = new ArrayList<>();
					InvoiceTicketAttachment attachment = null;
					for (GiftCardFileAttachment attachmentObj : fileAttachments) {
						attachment = new InvoiceTicketAttachment();
						attachment.setDownloadUrl(URLUtil.getGiftCardDownloadUrl(request,attachmentObj.getId(), giftCardOrder.getId(), configIdString,customerId,platForm,ip));
						attachment.setType(attachmentObj.getType()); 
						attachment.setPosition(attachmentObj.getPosition());
						attachment.setExtension(URLUtil.getFileExtension(attachmentObj.getFilePath()));
						attachment.setFileName(URLUtil.getGiftCardFileName(attachmentObj.getFilePath()));
						attachment.setTrxType(TransactionType.GFC);
						tickets.add(attachment);
					}
					
					orderDeliveryNote = "Download Gift Cards";
					order.setOrderDeliveryNote(orderDeliveryNote);
					order.setButtonOption("DL");
					order.setInvoiceTicketAttachment(tickets);
					order.setShippingMethod("E-Ticket");
					order.setDeliveryInfo(null);
					
				}else if(giftCardOrder.getStatus().equalsIgnoreCase("OUTSTANDING")){
					order.setButtonOption("PROGRESS");
					order.setTicketDownloadUrl(null);
					order.setShowInfoButton(Boolean.TRUE);
					order.setOrderDeliveryNote("INFO");
					TicketUtil.getGiftCardDeliveryInFo(order,fontSizeStr);
					TicketUtil.getGiftCardProgressPopupMsg(order,fontSizeStr);
					order.setShippingMethod("E-Ticket");
					order.setDeliveryInfo(null);
				}
				String convRateString = "";
				try {
					if(card.getConversionType().equalsIgnoreCase("PERCENTAGE")){
						convRateString = "Conversion Rate : "+(TicketUtil.getRoundedValue(card.getRewardDollarConvRate())*100)+"%";
					}else{
						convRateString = "Conversion Rate : "+TicketUtil.getRoundedValueString(card.getRewardDollarConvRate());
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				order.setConvRateString(convRateString);
				customerOrderList.add(order);
				
			}
			resObj.setCustomerOrders(customerOrderList);
			resObj.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.LISTGIFTCARDS,"Success");
			return resObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Seems there was some issue while fecthing your order information.");
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.LISTGIFTCARDS,"Seems there was some issue while fecthing your order information.");
			return resObj;
		}
	}
	
	
	 
	@RequestMapping(value="/GetRtfCatsTix",method=RequestMethod.POST)
	public @ResponsePayload RtfTixList getTicketList(HttpServletRequest request,HttpServletResponse response,Model model){
		RtfTixList ticketList =new RtfTixList();
		Error error = new Error();
		String eventIdStr = request.getParameter("eId");
		String customerIdStr = request.getParameter("cId");
		String platForm = request.getParameter("pfm"); 
		try {
			if(null == eventIdStr || eventIdStr.isEmpty()) {
				error.setDescription("Choose valid event.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Choose valid event");
				return ticketList;
			}
			
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see tickets.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Please login to see tickets");
				return ticketList;
			}
			
			Integer eventId= null;
			Event event = null;
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				try {
					eventId=Integer.parseInt(eventIdStr);
					event = DAORegistry.getEventDAO().getEventByEventId(eventId, ProductType.REWARDTHEFAN);
				} catch (Exception e) {
					error.setDescription("Event is not recognized.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Event is not recognized");
					return ticketList;
				}
			}else{
				error.setDescription("Event is not recognized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Event is not recognized");
				return ticketList;
			}
			 
			Integer customerId = null;
			CassCustomer cassCustomer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(cassCustomer==null){
					error.setDescription("Customer is not recognized.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Customer is not recognized");
					return ticketList;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Customer is not recognized");
				return ticketList;
			}
			
			List<RtfCatTicket> catTickets = DAORegistry.getRtfCatTicketDAO().getActiveTickets(eventId);
			
			if(null == catTickets || catTickets.isEmpty()) {
				error.setDescription("No Tickets found for selected event.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"No Tickets found for selected event.");
				return ticketList;
			}
			try {
				String startDateStr = "2020-04-15";
				Date startDate = dateFormat.parse(startDateStr);
				if(event.getEventDate() != null && event.getEventDate().before(startDate)) {
					error.setDescription("No Tickets found for selected event.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Event Date is before April 15-2020.");
					return ticketList;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			Double price = 0.00;
			Integer ticketId = null;
			
			for (RtfCatTicket catTix : catTickets) {
				if(catTix.getQty().equals(2)) {
					price = catTix.getPrice();
					ticketId = catTix.getId();
				}
			}
			 
			List<TixByQty>  tixByQties = new ArrayList<>();
			
			TixByQty tixByQtyObj = new TixByQty();
			tixByQtyObj.setQty(1);
			tixByQtyObj.setQtyMsg("1 ticket");
			tixByQtyObj.setTixObj( new RtfCatTicket(ticketId, eventId, 1, TicketUtil.getRoundedValue(price * 1),price));
			tixByQties.add(tixByQtyObj);
			
			tixByQtyObj = new TixByQty();
			tixByQtyObj.setQty(2);
			tixByQtyObj.setQtyMsg("2 tickets seated together");
			tixByQtyObj.setTixObj( new RtfCatTicket(ticketId, eventId, 2, TicketUtil.getRoundedValue(price * 2),price));
			tixByQties.add(tixByQtyObj);
			
			tixByQtyObj = new TixByQty();
			tixByQtyObj.setQty(3);
			tixByQtyObj.setQtyMsg("3 tickets seated together");
			tixByQtyObj.setTixObj( new RtfCatTicket(ticketId, eventId, 3, TicketUtil.getRoundedValue(price * 3),price));
			tixByQties.add(tixByQtyObj);
			
			tixByQtyObj = new TixByQty();
			tixByQtyObj.setQty(4);
			tixByQtyObj.setQtyMsg("4 tickets seated together");
			tixByQtyObj.setTixObj( new RtfCatTicket(ticketId, eventId, 4, TicketUtil.getRoundedValue(price * 4),price));
			tixByQties.add(tixByQtyObj);
		 
			RtfEvent rtfEvent = new RtfEvent(event.getEventId(), event.getEventName(), event.getEventDateTime(), event.getEventDateStr(), 
					event.getEventTimeStr(), event.getVenueId(), event.getVenueName(), event.getCity(), event.getState(), event.getCountry(), 
					event.getPinCode(), event.getTicketPriceTag());
			
			ticketList.setTixList(tixByQties);
			ticketList.setEvent(rtfEvent);
			ticketList.setCuRwards(cassCustomer.getcRewardDbl());
			ticketList.setDisCuRwards(cassCustomer.getcReward());;
			ticketList.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIATICKET,"Success");
			return ticketList;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Seems there was some issue while fecthing ticket information.");
			ticketList.setError(error);
			ticketList.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIATICKET,"Exception Occurred");
			return ticketList;
		}
	}
	
	
	@RequestMapping(value="/RtfCreateOrder",method=RequestMethod.POST)
	public @ResponsePayload RtfOrderConfirmation createOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		RtfOrderConfirmation resObj =new RtfOrderConfirmation();
		Error error = new Error();
		String eventIdStr = request.getParameter("eId");
		String customerIdStr = request.getParameter("cId");
		String ticketIdStr = request.getParameter("tId");
		String qtyStr = request.getParameter("qty");
		String shippingIdStr = request.getParameter("sId");
		String platFormStr = request.getParameter("pfm"); 
		String msg = "";
		try {
			if(null == eventIdStr || eventIdStr.isEmpty()) {
				msg ="Choose valid event.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				msg ="Please login to redeem tickets.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			
			if(null == qtyStr || qtyStr.isEmpty()) {
				msg ="Order quantity is mandatory.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			
			if(null == shippingIdStr || shippingIdStr.isEmpty()) {
				msg ="Shipping address is mandatory.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			Integer shippingId = Integer.parseInt(shippingIdStr);
			UserAddress shipping = DAORegistry.getUserAddressDAO().get(shippingId);
			
			if(null == shipping) {
				msg ="Shipping address is mandatory.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			
			if(null == ticketIdStr || ticketIdStr.isEmpty()) {
				msg ="Please login to see tickets.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			
			Integer eventId= null;
			Event event = null;
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				try {
					eventId=Integer.parseInt(eventIdStr);
					event = DAORegistry.getEventDAO().getEventByEventId(eventId, ProductType.REWARDTHEFAN);
				} catch (Exception e) {
					error.setDescription("Event is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Event is not recognized");
					return resObj;
				}
			}else{
				error.setDescription("Event is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Event is not recognized");
				return resObj;
			}
			 
			Integer customerId = null;
			CassCustomer cassCustomer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(cassCustomer==null){
					error.setDescription("Customer is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Customer is not recognized");
					return resObj;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Customer is not recognized");
				return resObj;
			}
			
			RtfCatTicket tix = null;
			try {
				Integer tixId = Integer.parseInt(ticketIdStr);
				tix = DAORegistry.getRtfCatTicketDAO().get(tixId);
				if(tix == null) {
					error.setDescription("It looks like this was just redeemed and is no longer available for sale.<br /> "
							+ "If you've just made a purchase, this may be because you've managed to buy the last ticket.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Customer is not recognized");
					return resObj;
				}
			}catch(Exception e) {
				e.printStackTrace();
				error.setDescription("It looks like this was just redeemed and is no longer available for sale.<br /> "
						+ "If you've just made a purchase, this may be because you've managed to buy the last ticket.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Customer is not recognized");
				return resObj;
			}
			
			
			Integer qty = Integer.parseInt(qtyStr);
			Double price = tix.getPrice();
			Double requiredRewards = TicketUtil.getRoundedUpValue(price * qty);
			
			CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customerId);
			
			if(null == customerLoyalty) {
				msg = "You do not have enough reward dollars to redeem these tickets.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			
			Double activeRewards = TicketUtil.getRoundedUpValue(customerLoyalty.getActivePointsAsDouble());
			
			if(requiredRewards > activeRewards) {
				msg = "You do not have enough reward dollars to redeem these tickets.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,msg);
				return resObj;
			}
			
			Date today = new Date();
			
			CategoryTicketGroup tgGroup = new CategoryTicketGroup();
			tgGroup.setEventId(event.getEventId());
			
			tgGroup.setOriginalPrice(0.00);
			tgGroup.setSectionRange("TBD");
			tgGroup.setRowRange("TBD");
			tgGroup.setStatus(TicketStatus.SOLD);
			tgGroup.setShippingMethod("ETICKETS");
			tgGroup.setCreatedBy("AUTO");
			tgGroup.setCreatedDate(today);
			tgGroup.setLastUpdatedDate(today);
			tgGroup.setSoldPrice(0.00);
			tgGroup.setProducttype(ProductType.REWARDTHEFAN);
			tgGroup.setSoldQuantity(qty);
			tgGroup.setQuantity(qty);
			tgGroup.setInternalNotes("Trivia Tickets");
			tgGroup.setFacePrice(0.00);
			tgGroup.setCost(0.00);
			tgGroup.setWholeSalePrice(0.00);
			tgGroup.setRetailPrice(0.00);
			tgGroup.setBroadcast(true);
			tgGroup.setOriginalTax(0.00);
			tgGroup.setOriginalTaxPerc(0.00);
			tgGroup.setActualSection("TBD");
			tgGroup.setRow(null);
			tgGroup.setLongSection(null);
			tgGroup.setBrokerId(1001);
			DAORegistry.getCategoryTicketGroupDAO().save(tgGroup);
			
			CustomerOrder customerOrder = new CustomerOrder();
			Customer customerDetail = new Customer();
			customerDetail.setId(customerId);
			customerOrder.setCategoryTicketGroupId(tgGroup.getId());
			customerOrder.setOrderType(OrderType.TRIVIAORDER);
			customerOrder.setCustomer(customerDetail);
			customerOrder.setEventDateTemp(event.getEventDate());
			customerOrder.setEventId(event.getEventId());
			customerOrder.setEventName(event.getEventName());
			customerOrder.setEventTime(null != event.getEventTime()?event.getEventTime():null);
			customerOrder.setVenueId(event.getVenueId());
			customerOrder.setVenueName(event.getVenueName());
			customerOrder.setVenueCity(event.getCity());
			customerOrder.setVenueState(event.getState());
			customerOrder.setVenueCountry(event.getCountry());
			customerOrder.setProductType(ProductType.REWARDTHEFAN);
			customerOrder.setShippingMethod("ETICKETS");
			customerOrder.setTaxesAsDouble(0.00);
			customerOrder.setBrokerId(1001);
			customerOrder.setBrokerServicePerc(0.00);
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(event.getEventDate());
			calendar.add(Calendar.DAY_OF_MONTH, -5);
			
			Property property = DAORegistry.getPropertyDAO().get("rtf.fullrewards.auto.primarytrxid");
			Long primaryTrxId =  Long.valueOf(property.getValue());
			primaryTrxId++;
			String trxId = PaginationUtil.fullRewardPaymentPrefix+primaryTrxId;
			property.setValue(String.valueOf(primaryTrxId));
			
			customerOrder.setShippingDateTemp(calendar.getTime());
			customerOrder.setStatus(OrderStatus.ACTIVE);
			customerOrder.setQty(qty);			
			customerOrder.setSection("TBD");
			customerOrder.setSectionDescription("TBD");
			customerOrder.setLastUpdatedDateTemp(new Date());
			customerOrder.setCreateDateTemp(new Date());			
			customerOrder.setTicketPriceAsDouble(price);
			customerOrder.setTicketSoldPriceAsDouble(price);
			customerOrder.setOriginalOrderTotal(requiredRewards);
			customerOrder.setDiscountAmount(0.00);
			customerOrder.setOrderTotalAsDouble(requiredRewards);
			customerOrder.setPrimaryPayAmtAsDouble(requiredRewards);
			customerOrder.setSecondaryPayAmtAsDouble(0.00);
			customerOrder.setPrimaryPaymentMethod(PaymentMethod.FULL_REWARDS);
			customerOrder.setPrimaryTransactionId(trxId);
			customerOrder.setSecondaryPaymentMethod(PartialPaymentMethod.NULL);
			customerOrder.setSecondaryTransactionId("");
			customerOrder.setAppPlatForm(platForm);
			customerOrder.setVenueCategory(event.getVenueCategoryName());
			customerOrder.setIsInvoiceSent(false);
			customerOrder.setIsLongSale(false);
			DAORegistry.getCustomerOrderDAO().saveOrUpdate(customerOrder);
			
			
			Invoice invoice  = new Invoice();
			invoice.setCustomerOrderId(customerOrder.getId());
			invoice.setInvoiceTotal(customerOrder.getOrderTotalAsDouble());
			invoice.setTicketCount(customerOrder.getQty());
			invoice.setCustomerId(customerOrder.getCustomer().getId());
			invoice.setExtPONo(null);
			invoice.setStatus(InvoiceStatus.Outstanding);
			invoice.setInvoiceType("INVOICE");
			invoice.setCreatedDate(new Date());
			invoice.setCreatedBy(customerOrder.getCustomer().getCustomerName());
			invoice.setIsSent(false);
			invoice.setSentDate(null);
			invoice.setRealTixMap("NO");
			invoice.setBrokerId(customerOrder.getBrokerId());
			DAORegistry.getInvoiceDAO().save(invoice);
			
			/*Ulaganathan : Order Payment breakups - Begins*/
			OrderPaymentBreakup paymentBreakUp = new OrderPaymentBreakup();
			paymentBreakUp.setPaymentAmount(requiredRewards);
			paymentBreakUp.setPaymentMethod(PaymentMethod.FULL_REWARDS);
			paymentBreakUp.setTransactionId(trxId);
			paymentBreakUp.setDoneBy("Customer");
			paymentBreakUp.setOrderSequence(1);
			paymentBreakUp.setPaymentDate(new Date());
			paymentBreakUp.setOrderId(customerOrder.getId());
			DAORegistry.getOrderPaymentBreakupDAO().save(paymentBreakUp);
			/*Ulaganathan : Order Payment breakups - Ends*/
			
			
			CustomerOrderDetail orderDetail = new CustomerOrderDetail();
			orderDetail.setBillingAddress1("");
			orderDetail.setBillingAddress2("");
			orderDetail.setBillingCity("");
			orderDetail.setBillingCountry("");
			orderDetail.setBillingCountryId(null);
			orderDetail.setBillingEmail("");
			orderDetail.setBillingFirstName("");
			orderDetail.setBillingLastName("");
			orderDetail.setBillingPhone1("");
			orderDetail.setBillingPhone2("");
			orderDetail.setBillingState("");
			orderDetail.setBillingStateId(null);
			orderDetail.setBillingZipCode("");
			orderDetail.setShippingAddress1(shipping.getAddressLine1());
			orderDetail.setShippingAddress2(shipping.getAddressLine2());
			orderDetail.setShippingCity(shipping.getCity());
			orderDetail.setShippingCountry(shipping.getCountryName());
			orderDetail.setShippingCountryId(shipping.getCountry().getId());
			orderDetail.setShippingEmail(shipping.getEmail());
			orderDetail.setShippingFirstName(shipping.getFirstName());
			orderDetail.setShippingLastName(shipping.getLastName());
			orderDetail.setCtyPhCode(null != shipping.getCtyPhCode() && !shipping.getCtyPhCode().isEmpty()?shipping.getCtyPhCode():CountryUtil.defaultCountryPhoneCode);
			orderDetail.setShippingPhone1(shipping.getPhone1());
			orderDetail.setShippingPhone2(shipping.getPhone2());
			orderDetail.setShippingState(shipping.getStateName());
			orderDetail.setShippingStateId(shipping.getState().getId());
			orderDetail.setShippingZipCode(shipping.getZipCode());
			orderDetail.setOrderId(customerOrder.getId());
			orderDetail.setOrderType("TRIVIAORDER");
			DAORegistry.getCustomerOrderDetailDAO().save(orderDetail);
			
			List<CategoryTicket> tickets = new ArrayList<CategoryTicket>();
			for(int i=0;i<qty;i++){
				CategoryTicket catTicket = new CategoryTicket();
				catTicket.setActualPrice(0.00);
				catTicket.setIsProcessed(true);
				catTicket.setSoldPrice(0.00);
				catTicket.setUserName("AUTO");
				catTicket.setCategoryTicketGroupId(tgGroup.getId());
				catTicket.setInvoiceId(invoice.getId());
				tickets.add(catTicket);
			}
			DAORegistry.getCategoryTicketDAO().saveAll(tickets);
			
			tgGroup.setInvoiceId(invoice.getId());
			
			DAORegistry.getCategoryTicketGroupDAO().update(tgGroup);
			 
			
			if(null != property){
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
			}
			
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			
			
			Double oldActiveRewards =customerLoyalty.getActivePointsAsDouble();
			
			customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()-requiredRewards);
			customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble()+requiredRewards);
			customerLoyalty.setLatestSpentPointsAsDouble(requiredRewards);
			customerLoyalty.setLastUpdate(new Date());

			CustomerLoyaltyHistory offerRewardHistory = new CustomerLoyaltyHistory();
			offerRewardHistory.setCustomerId(customerId);
			offerRewardHistory.setOrderId(customerOrder.getId());
			offerRewardHistory.setRewardConversionRate(loyaltySettings.getRewardConversion());
			offerRewardHistory.setDollarConversionRate(loyaltySettings.getDollerConversion());
			offerRewardHistory.setPointsSpentAsDouble(requiredRewards);
			offerRewardHistory.setRewardSpentAmount(0.00);
			offerRewardHistory.setPointsEarnedAsDouble(0.00);
			offerRewardHistory.setRewardEarnAmount(0.00);
			offerRewardHistory.setInitialRewardPointsAsDouble(oldActiveRewards);
			offerRewardHistory.setInitialRewardAmount(customerLoyalty.getActiveRewardDollers());
			offerRewardHistory.setOrderType(OrderType.TRIVIAORDER);
			offerRewardHistory.setCreateDate(new Date());
			offerRewardHistory.setUpdatedDate(new Date());
			offerRewardHistory.setOrderTotal(requiredRewards);
			offerRewardHistory.setOrderId(customerOrder.getId());
			offerRewardHistory.setRewardStatus(RewardStatus.ACTIVE);
			
			Double balanceRewardPointsNew = oldActiveRewards + requiredRewards - 0;
			Double balanceRewardAmountNew = RewardConversionUtil.getRewardDoller(balanceRewardPointsNew,loyaltySettings.getDollerConversion());
			
			offerRewardHistory.setBalanceRewardPointsAsDouble(balanceRewardPointsNew);
			offerRewardHistory.setBalanceRewardAmount(balanceRewardAmountNew);
			
			DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
			DAORegistry.getCustomerLoyaltyHistoryDAO().saveOrUpdate(offerRewardHistory);
			
			CassandraDAORegistry.getCassCustomerDAO().updateCustomerRewardsByCustomerId(customerId, customerLoyalty.getActivePointsAsDouble());
			
			resObj.setoId(customerOrder.getId());
			resObj.setStatus(1);
			resObj.setMsg("Order Created Successfully");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDER,"Success");
			return resObj;
		} catch (Exception e) {
			error.setDescription("Seems there was some issue while redeem this ticket.");
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDER,"Seems there was some issue while redeem this ticket.");
			return resObj;
		}
	}
	
	
	@RequestMapping(value="/ViewTriviaOrder",method=RequestMethod.POST)
	public @ResponsePayload CustomerOrderResponse viewTriviaOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerOrderResponse resObj = new CustomerOrderResponse();
		Error error = new Error();
		String orderIdStr = request.getParameter("oId");
		String customerIdStr = request.getParameter("cId");
		String platFormStr = request.getParameter("pfm"); 
		String msg = "";
		try {
			
			String ip =((HttpServletRequest)request).getHeader("X-Forwarded-For");
			String configIdString = request.getParameter("configId");
			/*Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDERVIEW,"You are not authorized");
				return resObj;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						resObj.setError(error);
						resObj.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDERVIEW,"You are not authorized");
						return resObj;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDERVIEW,"You are not authorized");
					return resObj;
				}
			}else{
				error.setDescription("You are not authorized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDERVIEW,"You are not authorized");
				return resObj;
			}
			if(null == orderIdStr || orderIdStr.isEmpty()) {
				msg ="Please .";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,msg);
				return resObj;
			}*/
			
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				msg ="Please login to view your ticket order.";
				error.setDescription(msg);
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,msg);
				return resObj;
			}
			
			ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(platFormStr);
			
			Integer customerId = null;
			CassCustomer cassCustomer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(cassCustomer==null){
					error.setDescription("Customer is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Customer is not recognized");
					return resObj;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Customer is not recognized");
				return resObj;
			}
			
			Integer orderId = Integer.parseInt(orderIdStr);
			
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(orderId);
			
			if(customerOrder == null) {
				error.setDescription("Invalid Order.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Invalid Order");
				return resObj;
			}
			
			if(!customerOrder.getCustomer().getId().equals(customerId)) {
				error.setDescription("You do not have the access to view this order..");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"You do not have access to view this order..");
				return resObj;
			}
			
			Boolean showLongInfo= false;
			boolean isLongSale = false;
			List<OrderTicketGroup> orderTicketGroups = null;
			
			orderTicketGroups = DAORegistry.getOrderTicketGroupDAO().getAllTicketGroupsByOrderId(orderId);
			if(null != orderTicketGroups && !orderTicketGroups.isEmpty()){
				showLongInfo= true;
			}
			customerOrder.setIsOrderFullFilled(showLongInfo);
			/*
			if(showLongInfo){
				customerOrder.setSectionDescription("<font><b>------</b></font>");
				customerOrder.setIsOrderFullFilled(true);
				String zone = "", section="",row="",seat="";
				Double ticketPrice = 0.00;
				int i =0;
				for (OrderTicketGroup orderTicketGroup : orderTicketGroups) {
					if(i == 0 ){
						zone = orderTicketGroup.getZone();
						section= orderTicketGroup.getSection();
						row= orderTicketGroup.getRow();
						seat= orderTicketGroup.getSeat();
					}else{
						zone = zone +","+orderTicketGroup.getZone();
						section= section +","+orderTicketGroup.getSection();
						row= row +","+orderTicketGroup.getRow();
						seat= seat +","+orderTicketGroup.getSeat();
					}
					ticketPrice = ticketPrice + TicketUtil.getRoundedValue(orderTicketGroup.getSoldPrice() * orderTicketGroup.getQuantity());
					i++;
				}
				customerOrder.setZone(zone);
				customerOrder.setRealSection(section);
				customerOrder.setRealRow(row);
				customerOrder.setRealSeat(seat);
				if(null != customerOrder.getIsLongSale() && customerOrder.getIsLongSale()){
					customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(ticketPrice));
				}else{
					customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(customerOrder.getTicketSoldPriceAsDouble() * customerOrder.getQty()));
				}
			} else{
				customerOrder.setRealTicketPrice(TicketUtil.getRoundedValueString(customerOrder.getTicketSoldPriceAsDouble() * customerOrder.getQty()));
			}*/
			
			customerOrder.setTotalServiceFees(TicketUtil.getRoundedValueString(0.00));
			customerOrder.setSingleTixServiceFees(TicketUtil.getRoundedValueString(0.00));
			
			Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByCustomerOrder(customerOrder.getId(), customerId);
			String fontSizeStr = TicketUtil.getFontSize(request, platForm);
			customerOrder.setViewOrderType(String.valueOf(customerOrder.getOrderType()));
			
			/*Order Delivery Details: 

			1. Download
			2. Cancelled
			3. In Progress
			Shipping Information
			Usually the tickets are available  a day before the event.*/
			
			String orderDeliveryNote= null;
			String isRealTixMap = null;
			if(null == invoice || invoice.getStatus().equals(InvoiceStatus.Voided)){
				orderDeliveryNote = "Order Cancelled";
				customerOrder.setButtonOption("OC");
				customerOrder.setOrderDeliveryNote(orderDeliveryNote);
				customerOrder.setDeliveryInfo(null);
				
			}else{
				
				isRealTixMap = invoice.getRealTixMap();
				if(isRealTixMap.equals("Yes") || isRealTixMap.equals("YES")){
					customerOrder.setShowInfoButton(Boolean.FALSE);
					if(invoice.getShippingMethodId()!=null &&!invoice.getShippingMethodId().equals(3)){
						orderDeliveryNote = "Download Tickets";
						customerOrder.setButtonOption("DL");
				
						List<InvoiceTicketAttachment> invoiceTicketAttachmentList = DAORegistry.getInvoiceTicketAttachmentDAO().getTicketAttachmentByInvoiceId(invoice.getId());
						
						List<InvoiceTicketAttachment> finalTicketAttachments = null;
						
						if(invoiceTicketAttachmentList!=null){
							
							finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
							
							for (InvoiceTicketAttachment tixAttachemnt : invoiceTicketAttachmentList) {
								try {
									File file = new File (URLUtil.findETicketDownloadURLFromDirectory(tixAttachemnt.getFilePath()));
									if(null != file && file.exists()){
										tixAttachemnt.setDownloadUrl(URLUtil.getETicketDownloadUrl(request, customerOrder, configIdString,
												customerId,invoice,tixAttachemnt.getId(),platForm,ip));
										finalTicketAttachments.add(tixAttachemnt);
									}
									tixAttachemnt.setExtension(URLUtil.getFileExtension(tixAttachemnt.getFilePath()));
									tixAttachemnt.setFileName(URLUtil.getFileNameForTriviaTickets(tixAttachemnt.getFilePath()));
								}catch(Exception e) {
									e.printStackTrace();
								}
							}
							
							try{
								Collections.sort(finalTicketAttachments, TicketUtil.ticketAttachmentsByPosition);
							}catch(Exception e){
								e.printStackTrace();
							}
						}else{
							finalTicketAttachments = new ArrayList<InvoiceTicketAttachment>();
						}
						
						
						if(null != finalTicketAttachments && !finalTicketAttachments.isEmpty() && finalTicketAttachments.size() >0){
							orderDeliveryNote = "Download Tickets";
							customerOrder.setOrderDeliveryNote(orderDeliveryNote);
							customerOrder.setButtonOption("DL");
							customerOrder.setInvoiceTicketAttachment(finalTicketAttachments);
							customerOrder.setShippingMethod("E-Ticket");
							customerOrder.setDeliveryInfo(null);
						}else{
							
							Date eventDate = customerOrder.getEventDateTemp();
							Date curDate =  new Date();
							
							long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
							
							if(noOfDays < 0){
								orderDeliveryNote = "No Ticket Attached";
								customerOrder.setButtonOption("OC");
								//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
								//order.setShippingMethod("E-Ticket");
								customerOrder.setOrderDeliveryNote(orderDeliveryNote);
								customerOrder.setDeliveryInfo(null);
							}else{
								customerOrder.setButtonOption("PROGRESS");
								customerOrder.setTicketDownloadUrl(null); 
								customerOrder.setShowInfoButton(Boolean.TRUE);
								customerOrder.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(customerOrder));
								TicketUtil.getTicketDeliveryInFo(customerOrder,fontSizeStr);
								TicketUtil.getProgressPopupMsg(customerOrder,fontSizeStr);
								customerOrder.setShippingMethod("E-Ticket");
								customerOrder.setDeliveryInfo(null);
							}
							
						}
						 
					}else if(invoice.getShippingMethodId()!=null && invoice.getShippingMethodId().equals(3)){
						String fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
						orderDeliveryNote = "FedEx : "+fedExTrackingNo;
						customerOrder.setButtonOption("FEDEX");
						customerOrder.setTicketDownloadUrl(URLUtil.getFedExTrackingURL(invoice));
						customerOrder.setDeliveryInfo("");
						customerOrder.setShippingMethod("FedEx");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);
					}
				}else{
					
					Date eventDate = customerOrder.getEventDateTemp();
					Date curDate =  new Date();
					long noOfDays = DateUtil.getDifferenceDays(curDate, eventDate);
					Date tmpDate  = dtFormat.parse("01/01/3000");
					Boolean isNullEventFlag = false;
					if(eventDate.compareTo(tmpDate) == 0) {
						isNullEventFlag = true;
						customerOrder.setEventDate(null);
						customerOrder.setEventDateTemp(null);
						customerOrder.setEventTime(null);
					}
					if(noOfDays < 0 && !isNullEventFlag){
						orderDeliveryNote = "Order Cancelled";
						customerOrder.setButtonOption("OC");
						//order.setTicketDownloadUrl(URLUtil.getRTFOrderDownloadUrl(request, order, configIdString,customer.getId(),invoice));
						//order.setShippingMethod("E-Ticket");
						customerOrder.setOrderDeliveryNote(orderDeliveryNote);
						customerOrder.setDeliveryInfo(null);
					}else{
						customerOrder.setButtonOption("PROGRESS");
						customerOrder.setTicketDownloadUrl(null);
						customerOrder.setShowInfoButton(Boolean.TRUE);
						customerOrder.setOrderDeliveryNote(TicketUtil.getInProgressButtonText(customerOrder));
						TicketUtil.getTicketDeliveryInFo(customerOrder,fontSizeStr);
						TicketUtil.getProgressPopupMsg(customerOrder,fontSizeStr);
						customerOrder.setShippingMethod("E-Ticket");
						customerOrder.setDeliveryInfo(null);
					}
				}
			}
			
			customerOrder.setAllCustomerRelatedObjects();
			List<CustomerOrder> customerOrderList = new ArrayList<CustomerOrder>();
			customerOrderList.add(customerOrder);
			resObj.setCustRewards(cassCustomer.getcReward());
			resObj.setCustomerOrders(customerOrderList);
			resObj.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TRIVIAORDERVIEW,"Success");
			return resObj;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Seems there was some issue while fecthing your order information.");
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TRIVIAORDERVIEW,"Seems there was some issue while fecthing your order information.");
			return resObj;
		}
	}
	
	

	@RequestMapping(value="/GetSuperFanStats")
	public @ResponsePayload SuperFanStat getSuperFanStats(HttpServletRequest request,HttpServletResponse response,Model model){
		SuperFanStat resObj =new SuperFanStat();
		Error error = new Error();
		String customerIdStr = request.getParameter("cId");
		try {
			if(null == customerIdStr || customerIdStr.isEmpty()) {
				error.setDescription("Please login to see your super fan stats.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETSUPERFANSTATS,"Please login to see your super fan stats");
				return resObj;
			}
			
			Integer customerId = null;
			CassCustomer cassCustomer = null;
			try {
				customerId=Integer.parseInt(customerIdStr);
				cassCustomer = CassandraDAORegistry.getCassCustomerDAO().getCustomerById(customerId);
				if(cassCustomer==null){
					error.setDescription("Customer is not recognized.");
					resObj.setError(error);
					resObj.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETSUPERFANSTATS,"Customer is not recognized");
					return resObj;
				}
			} catch (Exception e) {
				error.setDescription("Customer is not recognized.");
				resObj.setError(error);
				resObj.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETSUPERFANSTATS,"Customer is not recognized");
				return resObj;
			}
			
			QuizSuperFanStat superFanStat = QuizDAORegistry.getQuizSuperFanStatDAO().getQuizSuperFanStat(customerId);
			
			if(null != superFanStat) {
				resObj.setGames(String.valueOf(superFanStat.getNoOfGamePlayed()));
				resObj.setReferrals(String.valueOf(superFanStat.getReferralChances()));
				if(superFanStat.getTotalNoOfChances() >= 60) {
					resObj.setSuperFanEntries("You are in WINNERS circle");
				}else {
					resObj.setSuperFanEntries(String.valueOf(superFanStat.getTotalNoOfChances()));
				}
			}else {
				resObj.setGames("0");
				resObj.setReferrals("0");
				resObj.setSuperFanEntries("0");
			}
			 
			resObj.setStatus(1);
			resObj.setCustId(customerId);
			
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSUPERFANSTATS,"Success");
			return resObj;
		} catch (Exception e) {
			error.setDescription(e.getMessage());
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETSUPERFANSTATS,"Exception Occurred");
			return resObj;
		}
	} 

	//Prod - 470000 to 626700
	//Sandbox - 136422 to 206423
	/*public static Integer startCustomerId = 136422;
	public static Integer endCustomerId = 206423;
	public static Integer currentCustomerId = startCustomerId;*/
	
	/*public static Integer getRandomCustomerIdForTest() {
		if(currentCustomerId >= endCustomerId) {
			currentCustomerId = startCustomerId;
		}
		currentCustomerId +=1;
		return currentCustomerId;
	}
*/
	
	@RequestMapping(value="/LoadTestCustomersMap")
	public @ResponsePayload SuperFanStat loadTestCustomersMap(HttpServletRequest request,HttpServletResponse response,Model model){
		SuperFanStat resObj =new SuperFanStat();
		Error error = new Error();
		try {
			loadTestCust();
			resObj.setStatus(1);
			resObj.setCustId(testCustMap.size());
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSUPERFANSTATS,"Success");
			return resObj;
		} catch (Exception e) {
			error.setDescription(e.getMessage());
			resObj.setError(error);
			resObj.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETSUPERFANSTATS,"Exception Occurred");
			return resObj;
		}
	} 
	
	public static Integer startCustomerId = 1;
	public static Integer endCustomerId = 206423;
	public static Integer currentCustomerId = startCustomerId;
	
	public static Map<Integer, Integer> testCustMap = new HashMap<Integer, Integer>();
	
	
	public static List<Integer> getCustomerList(){
		List<Integer> testCutomerIds = new ArrayList<>();
		for(int i =1001; i <= 1500;i++) {
			testCutomerIds.add(i);
		}
		return testCutomerIds;
	}
	
	public static void loadTestCust(){
		testCustMap = new HashMap<Integer, Integer>();
		List<Integer> testCutomerIds = DAORegistry.getQueryManagerDAO().getAllTestAccounts();
		int i =1;
		for (Integer custId : testCutomerIds) {
			testCustMap.put(i, custId);
			i++;
		}
		endCustomerId = i;
	}
	
	public static Integer getRandomCustomerIdForTest1() {
		if(currentCustomerId >= endCustomerId) {
			currentCustomerId = startCustomerId;
		}
		currentCustomerId +=1;
		return testCustMap.get(currentCustomerId);
	}
	
	public static void main(String[] args) {
		for(int i =1; i <= 50;i++) {
			System.out.println(getRandomCustomerIdForTest1());;
		}
	}

		 
}