package com.rtftrivia.ws.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.rtfquiz.webservices.aws.AWSFileService;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.FantasyTicketsInformationPage;
import com.zonesws.webservices.data.GetProductInformation;
import com.zonesws.webservices.data.RewardInformationPage;
import com.zonesws.webservices.data.ZoneticketsInformation;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.MapUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.list.RTFProductInfoDtls;
import com.zonesws.webservices.utils.list.RTFProductInformation;

@Controller
@RequestMapping({"/GetProductInformation","/GetRTFProductInfo","/GetRewardPointsPageInformtion",
	"/GetFSTPageInformtion","/GetEarnRewardPointsPageInformtion","/GetUseRewardPointsPageInformtion"})
public class SupportController {
	private static String apiServerSvgMapsBaseURL = URLUtil.apiServerSvgMapsBaseURL;
	
	@RequestMapping(value="/GetProductInformation",method=RequestMethod.POST)
	public @ResponsePayload GetProductInformation getZoneticketsInformation(HttpServletRequest request, Model model,HttpServletResponse response){
		GetProductInformation getZoneticketsInformation =new GetProductInformation();
		List<ZoneticketsInformation> listZoneticketsInformation = new ArrayList<ZoneticketsInformation>();
		ZoneticketsInformation zoneticketsInformation = new ZoneticketsInformation();
		com.zonesws.webservices.utils.Error error = new com.zonesws.webservices.utils.Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			String platFormStr = request.getParameter("platForm");
			//String deviceType = request.getParameter("deviceType");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				getZoneticketsInformation.setError(error);
				getZoneticketsInformation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CUSTOMERSIGNUP,"You are not authorized");
				return getZoneticketsInformation;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(DAORegistry.getIpConfigDAO().getIpConfigByIpAndConfigId(configIdString,ip)==null){
						error.setDescription("You are not authorized.");
						getZoneticketsInformation.setError(error);
						getZoneticketsInformation.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"You are not authorized");
						return getZoneticketsInformation;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					getZoneticketsInformation.setError(error);
					getZoneticketsInformation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"You are not authorized");
					return getZoneticketsInformation;
				}
			}else{
				error.setDescription("You are not authorized.");
				getZoneticketsInformation.setError(error);
				getZoneticketsInformation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"You are not authorized");
				return getZoneticketsInformation;
			}
			
			/*if(TextUtil.isEmptyOrNull(platFormStr)){
				error.setDescription("Platform is mandatory.");
				getZoneticketsInformation.setError(error);
				getZoneticketsInformation.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"  is mandatory");
				return getZoneticketsInformation;
			}*/
			
			ApplicationPlatForm applicationPlatForm=null;
			if(!TextUtil.isEmptyOrNull(platFormStr)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platFormStr);
				}catch(Exception e){
					error.setDescription("Please send valid platfrom");
					getZoneticketsInformation.setError(error);
					getZoneticketsInformation.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"Please send valid platfrom");
					return getZoneticketsInformation;
				}
			}
			
			String deviceType = null != request.getParameter("deviceType")?request.getParameter("deviceType"):"";
			 String imageSizeStr = "";
			 String fontTagStartStr = "";
			 String fontTagEndStr="";
			 String fontSizeStr="";
			 if(applicationPlatForm.equals(ApplicationPlatForm.IOS)){
					try{
						if(deviceType.equals("IPHONE")){
							imageSizeStr ="width:310px; height:260px;";
							fontSizeStr ="size='4'";
						}else if(deviceType.equals("IPAD")){
							fontSizeStr ="size='6'";
						}
					}catch (Exception e) {
						//fontSizeStr ="size='4'";
					}
				}
			 if(imageSizeStr.equals("")) {
				 imageSizeStr = "max-width:100%; height:auto;";
			 }
			 if(!fontSizeStr.equals("")) {
				 fontTagStartStr = "<font "+fontSizeStr+">";
				 fontTagEndStr = "</font>";
			 }
			
			String infoType = request.getParameter("infoType");
			String headerData ="<html><body><div>";
			String footerData = "</div></body></html>";
			StringBuilder sb = new StringBuilder();
			if(infoType.equalsIgnoreCase("about")){
				String color = "color:#2D9DC6",style = "justify",justColor="#2D9DC6";				
				getZoneticketsInformation.setStatus(1);
				zoneticketsInformation.setHeader("About");
				
				zoneticketsInformation.setData("<html><body>"+
						"<div><p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Reward The Fan</strong> is a ticket marketplace that rewards customers for:</font></p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>1)</strong>&nbsp;&nbsp;Being a loyal fan - Earn a 10% discount on EVERY purchase for your \"Loyal Fan\" team or artist."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>2)</strong>&nbsp;&nbsp;Using our service - Earn 10% in Reward Dollars on ALL your purchases."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>3)</strong>&nbsp;&nbsp;Referring others - Earn 10% in Reward Dollars on the first purchase of EVERYONE you refer."+fontTagEndStr+"</p>"+

						"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Our goal is to reward your loyalty with dollars that can be used to:</strong></font></p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>1)</strong>&nbsp;&nbsp;Get great deals on tickets to your favorite events."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>2)</strong>&nbsp;&nbsp;Get FREE Fantasy Sports Tickets " +
								/*"<a data-toggle='modal' reef='#' onclick='showCJEPackageInfoDialog();'>" +
						"<img src='https://www.rewardthefan.com/resources/img/info_1.png'/></a> "*/
						"to see your favorite team in the Super Bowl, NBA Finals or World Series etc.Fantasy Sports Tickets are FREE tickets to those once in a lifetime special events like the Super Bowl, World Series, NBA Finals, " +
				"the World Cup, Final Four, etc.Fantasy Tickets are not for sale and can only be obtained by earning and redeeming " +
				"Reward The Fan Dollars."+fontTagEndStr+"</p>"+ 


						"<p align="+style+">"+fontTagStartStr+"All tickets on Reward The Fan are sold using a simple, color-coded ZONE TICKET format."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"ZONE TICKETS are always SEATED TOGETHER within a grouping of sections, similar to the seating chart below."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"Reward The Fan dollars are not available for purchase and cannot be redeemed for cash, they are only available to you, THE FAN to thank you for using our service and referring others."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"It's that simple!"+fontTagEndStr+"</p></div>" +
						"<div><p>"+fontTagStartStr+"Phone # - "+fontTagEndStr+"<span style='color:#ed1c24;'>"+fontTagStartStr+"800-601-6100"+fontTagEndStr+"</span></p>" + //<p>Fax - <span style='color:#ed1c24;'>212-382-2800</span></p>
						"<p><span style='font-weight: bold;' >"+fontTagStartStr+"Mailing Address "+fontTagEndStr+"</span><br>" +
						"<span>"+fontTagStartStr+"Reward The Fan<br>10 Times Square<br>3rd Floor<br>New York, NY 10018"+fontTagEndStr+"</span></p>" +
						"</div> <div style='width:100%;height:99%; position: absolute;text-align: center;' >" +
						"<img style='"+imageSizeStr+"' src='"+MapUtil.getAboutUsImage()+"' /> </div>" +
								/*"" +
								"" +
								"" +
								"" +
								"<div id='noTixModal' class='modal fade sign-pop notixmodal' role='dialog'><div class='modal-dialog'><div class='modal-content'>" +
								"<div class='modal-body reward_popup_bg'><h4 align='center'><img src='../resources/img/info.png' /></h4>" +
								"<div class='reward_popup_contant reward_popup_contant_border'><div id='notix-alert-body-div'>" +
								"<span id='noTixText'></span></div></div></div><div class='col-sm-12'><div style='text-align:center'>" +
								"<a href='#' ><button type='submit'  class='orangebtn btn' style='width:40%;' > OK </button></a>" +
								"</div></div><div class='clearfix'></div></div></div></div>" +
								"" +
								"" +*/
								"</body></html>");
				
				/*"<div style='width:100%;height:99%; position: absolute;text-align: center;' >" +
				"<img style='max-width:100%; height:auto;' src='"+MapUtil.getAboutUsImage()+"' /></div>");*/
			
				listZoneticketsInformation.add(zoneticketsInformation);
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
				
			}
			/*else if(infoType.equalsIgnoreCase("terms")){
				getZoneticketsInformation.setStatus(1);
				zoneticketsInformation.setHeader("Terms Of Use");
				sb.append("<ul><li><h5 style='color:#ed1c24;'>Introduction</h5>Welcome to RewardTheFan.com. By visiting our website and accessing the information, resources,");
				sb.append("services, products, and tools we provide, you understand and agree to accept and adhere to the following terms and conditions as stated in this ");
				sb.append("policy (hereafter referred to as 'User Agreement').This agreement is in effect as of June 1st, 2015.<br>");
				sb.append("We reserve the right to change this User Agreement from time to time without notice. You acknowledge and agree that it is your responsibility to");
				sb.append("review this User Agreement periodically to familiarize yourself with any modifications. Your continued use of this site after such modifications");
				sb.append("will constitute acknowledgment and agreement of the modified terms and conditions.</li>");
				sb.append("<li><h5 style=color:#ed1c24;''>Ticket Guarantee</h5>RewardTheFan.com guarantees all purchases made through its website. All orders are guaranteed at ");
				sb.append("the time of purchase. Immediately after your credit card is processed, you will receive an order confirmation via the email address you submitted at");
				sb.append("the time of purchase. This confirmation email will serve as your official receipt of purchase and will document the details of your order. You are");
				sb.append("guaranteed to receive a zone of 'equal or better quality' to the zone in your confirmation email prior to the start of your event.<br>");
				sb.append("If you do not receive an email confirmation from us after you place your order, please contact us at 800-601-6100 or email info@RewardTheFan.com.");
				sb.append("A problem receiving confirmation from us is not grounds to cancel an order. When in doubt, we ask that you contact us for clarification regarding your order details.</li>");
				sb.append("<li><h5 style='color:#ed1c24'>Price of Tickets</h5>By visiting RewardTheFan.com you understand that you are participating "
						+ "in the secondary ticket market. Tickets on the Secondary Ticket Market are often times listed/sold at a price that is "
						+ "different than the price printed on the actual ticket (the 'face value' or 'box office' price).  In some cases, the price "
						+ "may be significantly higher than the original face value price printed on the ticket; however, in other cases the "
						+ "Secondary Ticket Market price may be substantially lower than the face value price of the ticket. Individual prices "
						+ "will be determined solely at the professional discretion of our staff after assessing market conditions, including "
						+ "supply, demand, date/location of the event, quality of the seat, and volatility of the marketplace, as well as other "
						+ "proprietary formulas.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Credit Card Usage</h5>Please note that any purchases made by credit card do not afford "
						+ "you any special considerations or variance from our User Agreement. In certain instances, RewardTheFan.com may require "
						+ "you to submit additional proof of identity prior to your order being processed/shipped. Such instances may include, but "
						+ "are not limited to: high dollar value orders, high-profile events, billing/shipping information that does not match your "
						+ "credit card, etc. Additional proof of identity may be conducted online, via email, or via phone.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Shipping, Delivery, and Fulfillment</h5>All tickets purchased and confirmed through "
						+ "RewardTheFan.com are guaranteed to be received by you prior to the start of your event. Exact fulfillment and method "
						+ "will vary depending on time, date and location. In most instances tickets will be sent via email, Fedex, messenger or "
						+ "left at the box office.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Event Dates, Times &amp; Venues</h5>Event date and times are printed on the ticket you will "
						+ "receive. However, please note that games and show times can/do change and sometimes differ from the information printed on "
						+ "your ticket. This change can take place at any time before or after your purchase. While we attempt to make sure all of our "
						+ "listings are accurate, it is your responsibility to confirm the start time and date of your event prior to purchasing your "
						+ "tickets. By purchasing your ticket, you agree that RewardTheFan.com is not responsible for any changes in date and time. "
						+ "Refunds will not be issued, except where permitted by this User Agreement.");
				sb.append("In addition to event dates and times changing, it is not uncommon for events to change their venue due to a number of "
						+ "circumstances. If a new venue is announced and the event location is changed, your tickets for the original venue will "
						+ "be honored or new tickets will be issued. A change of venue does not automatically entitle you to a refund of your "
						+ "purchase in whole or part. Refunds will only be issued in accordance with the Returns/Refunds section of this agreement.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Returns/Refunds</h5>There are no returns, refunds or exchanges once your order has been "
						+ "placed and confirmed. Since we acquire our inventory from a number of sources (including VIP ticket holders, professional "
						+ "vendors, individual ticket holders, etc.), it is very difficult for us to cancel an order or accept returns for ticket "
						+ "orders that have already been confirmed. However, refunds will be issued if your event is officially canceled by the "
						+ "primary ticket source (team, artist, promoter, or venue.) For officially canceled events, refunds must be requested no "
						+ "later than 10 days after the scheduled event date. Tickets must be mailed back to our office to receive the refund.<br>");
				sb.append("Refunds will not be offered for postponed, rescheduled or relocated events. If your event is rescheduled for a new date "
						+ "and/or time or relocated to a new venue and your ticket remains valid, your purchase does not qualify for a refund.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Resources</h5>Our Resources include, but are not limited to any web content, ticket data, "
						+ "services, communications, and/or proprietary software that comprise this user experience.  Wherein, you understand that:<br>");
				sb.append("a) In order to access our Resources, you may be required to provide certain information about yourself "
						+ "(such as identification, contact details, etc.). You agree that any information you provide will always be accurate, "
						+ "correct, and up to date.<br>");
				sb.append("b) You are responsible for maintaining the confidentiality of any login information associated with any account you use "
						+ "to access our Resources. Accordingly, you are responsible for all activities that occur under your account/s.  <br>");
				sb.append("c) Accessing (or attempting to access) any of our Resources by any means other than through the means we provide, is "
						+ "strictly prohibited. You specifically agree not to access (or attempt to access) any of our Resources through any "
						+ "automated, unethical or unconventional means <br> ");
				sb.append("d) Engaging in any activity that disrupts or interferes with our Resources, including the servers and/or networks to "
						+ "which our Resources are located or connected, is strictly prohibited.  <br>");
				sb.append("e) Attempting to copy, duplicate, reproduce, sell, trade, or resell our Resources is strictly prohibited.  <br>");
				sb.append("f) You are solely responsible for any consequences, losses, or damages that we may directly or indirectly incur or "
						+ "suffer due to any unauthorized activities conducted by you, as explained above, and may incur criminal or civil "
						+ "liability. <br> ");
				sb.append("g) We may provide various open communication tools on our website, such as blog comments, blog posts, public chat, "
						+ "forums, message boards, newsgroups, product ratings and reviews, various social media services, etc. You understand "
						+ "that generally we do not pre-screen or monitor the content posted by users of these various communication tools, which "
						+ "means that if you choose to use these tools to submit any type of content to our website, then it is your personal "
						+ "responsibility to use these tools in a responsible and ethical manner. By posting information or otherwise using any "
						+ "open communication tools as mentioned, you agree that you will not upload, post, share, or otherwise distribute any "
						+ "content that: i) Is illegal, threatening, defamatory, abusive, harassing, degrading, intimidating, fraudulent, "
						+ "deceptive, invasive, racist, or contains any type of suggestive, inappropriate, or explicit language; ii) Infringes on "
						+ "any trademark, patent, trade secret, copyright, or other proprietary right of any party; iii) Contains any type of "
						+ "unauthorized or unsolicited advertising; iv) Impersonates any person or entity, including our employees or "
						+ "representatives.  We have the right at our sole discretion to remove any content that, we feel in our judgment does "
						+ "not comply with this User Agreement, along with any content that we feel is otherwise offensive, harmful, "
						+ "objectionable, inaccurate, or violates any 3rd party copyrights or trademarks. We are not responsible for any delay or "
						+ "failure in removing such content. If you post content that we choose to remove, you hereby consent to such removal, "
						+ "and consent to waive any claim against us. <br> ");
				sb.append("h) We do not assume any liability for any content posted by you or any other 3rd party users of our website. However, any "
						+ "content posted by you using any open communication tools on our website, provided that it doesn't violate or infringe on any "
						+ "3rd party copyrights or trademarks, becomes the property of RewardTheFan.com, and as such, gives us a perpetual, irrevocable, "
						+ "worldwide, royalty-free, exclusive license to reproduce, modify, adapt, translate, publish, publicly display and/or distribute "
						+ "as we see fit. This only refers and applies to content posted via open communication tools as described, and does not refer "
						+ "to information that is provided as part of the registration process necessary in order to use our Resources. <br>");
				sb.append("i) You agree to indemnify and hold harmless RewardTheFan.com and its parent company and affiliates, their directors, "
						+ "officers, managers, employees, donors, agents, and licensors, from and against all losses, expenses, damages and costs, "
						+ "including reasonable attorneys' fees, resulting from any violation of this User Agreement or the failure to fulfill any "
						+ "obligations relating to your account incurred by you or any other person using your account. We reserve the right to take "
						+ "over the exclusive defense of any claim for which we are entitled to indemnification under this User Agreement. In such "
						+ "event, you shall provide us with such cooperation as is reasonably requested by us.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Limitation of Warranties</h5>By using our website, you understand and agree that all "
						+ "Resources we provide are 'as is' and 'as available'. This means that we do not represent or warrant to you that:<br>  ");
				sb.append("i) the use of our Resources will meet your needs or requirements. <br> "+
						  "ii) the use of our Resources will be uninterrupted, timely, secure or free from errors.<br>  "+
						  "iii) the information obtained by using our Resources will be accurate or reliable, and<br>  "+
						  "iv) any defects in the operation or functionality of any Resources we provide will be repaired or corrected.  Furthermore, you understand and agree that: <br> "+
						  "v) any content downloaded or otherwise obtained through the use of our Resources is done at your own discretion and "
						  + "risk, and that you are solely responsible for any damage to your computer or other devices for any loss of data "
						  + "that may result from the download of such content. <br> "+
						  "vi) no information or advice, whether expressed, implied, oral or written, obtained by you from RewardTheFan.com or "
						  + "through any Resources we provide shall create any warranty, guarantee, or conditions of any kind, except for those "
						  + "expressly outlined in this User Agreement.</li>");	
				sb.append("<li><h5 style='color:#ed1c24;'>Limitation of Liability</h5>In conjunction with the Limitation of Warranties as explained "
						+ "above, you expressly understand and agree that any claim against us shall be limited to the amount you paid, if any, for use "
						+ "of products and/or services. RewardTheFan.com will not be liable for any direct, indirect, incidental, consequential or "
						+ "exemplary loss or damages which may be incurred by you as a result of using our Resources, or as a result of any changes, "
						+ "data loss or corruption, cancellation, loss of access, or downtime to the full extent that applicable limitation of liability "
						+ "laws apply.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Copyrights/Trademarks</h5>All content and materials available on RewardTheFan.com, "
						+ "including but not limited to text, graphics, website name, code, images and logos are the intellectual property of "
						+ "RewardTheFan.com, and are protected by applicable copyright and trademark law. Any inappropriate use, including but "
						+ "not limited to the reproduction, distribution, display or transmission of any content on this site is strictly "
						+ "prohibited, unless specifically authorized by RewardTheFan.com.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Termination of Use</h5>You agree that we may, at our sole discretion, suspend or terminate your "
						+ "access to all or part of our website and Resources with or without notice and for any reason, including, without limitation, "
						+ "breach of this User Agreement. Any suspected illegal, fraudulent or abusive activity may be grounds for terminating your "
						+ "relationship and may be referred to appropriate law enforcement authorities. Upon suspension or termination, your right to use "
						+ "the Resources we provide will immediately cease, and we reserve the right to remove or delete any information that you may have "
						+ "on file with us, including any account or login information.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Governing Law</h5>This website is controlled by RewardTheFan.com from our offices located in the "
						+ "state of New York in the United States of America. It can be accessed by most countries around the world. As each country has "
						+ "laws that may differ from those of New York, by accessing our website, you agree that the statutes and laws of the state of "
						+ "New York, without regard to the conflict of laws and the United Nations Convention on the International Sales of Goods, will "
						+ "apply to all matters relating to the use of this website and the purchase of any products or services through this site. "
						+ "Furthermore, any action to enforce this User Agreement shall be brought in the federal or state courts located in New York, "
						+ "USA. You hereby agree to personal jurisdiction by such courts, and waive any jurisdictional, venue, or inconvenient forum "
						+ "objections to such courts.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Guarantee</h5>UNLESS OTHERWISE EXPRESSED, RewardTheFan.com EXPRESSLY DISCLAIMS ALL WARRANTIES "
						+ "AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES AND CONDITIONS OF "
						+ "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Contact Information</h5>If you have any questions or comments about these our Terms of Service "
						+ "as outlined above, you can contact us at:  <br><br>RewardTheFan.com <br>");
				sb.append("575 7th ave<br>3rd floor<br>New York, NY 10018<br><a style='color:black;' href='mailto:info@RewardTheFan.com'>"
						+ "info@RewardTheFan.com</a><br>800-601-6100</li></ul>");
				listZoneticketsInformation.add(zoneticketsInformation);
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
				zoneticketsInformation.setData(headerData + sb.toString() + footerData);
				zoneticketsInformation.setData("Welcome to RewardTheFan.com. By visiting our website and accessing the information, resources, services, products, and tools "+
						"we provide, you understand and agree to accept and adhere to the following terms and conditions as stated in this policy "+
						"(hereafter referred to as 'User Agreement'). This agreement is in effect as of June 1st, 2015."+
						"We reserve the right to change this User Agreement from time to time without notice. You acknowledge and agree that it is "+
						"your responsibility to review this User Agreement periodically to familiarize yourself with any modifications."+
						"Your continued use of this site after such modifications will constitute acknowledgment and agreement of the modified "+
						"terms and conditions.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Ticket Guarantee");
				zoneticketsInformation.setData("RewardTheFan.com guarantees all purchases made through its website. All orders are guaranteed at the time of purchase. Immediately after "+
						"your credit card is processed, you will receive an order confirmation via the email address you submitted at the time of purchase. This "+
						"confirmation email will serve as your official receipt of purchase and will document the details of your order. You are guaranteed to "+
						"receive a zone of equal or better quality to the zone in your confirmation email prior to the start of your event."+
						"If you do not receive an email confirmation from us after you place your order, please contact us at 800-601-6100 or email "+
						"info@RewardTheFan.com. A problem receiving confirmation from us is not grounds to cancel an order. When in doubt, we ask that you "+
						"contact us for clarification regarding your order details.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();	
				zoneticketsInformation.setHeader("Price of Tickets");
				zoneticketsInformation.setData("By visiting RewardTheFan.com you understand that you are participating in the secondary ticket market. Tickets on the Secondary Ticket "+
						"Market are often times listed/sold at a price that is different than the price printed on the actual ticket (the face value or box office "+
						"price). In some cases, the price may be significantly higher than the original face value price printed on the ticket; however, in other cases "+
						"the Secondary Ticket Market price may be substantially lower than the face value price of the ticket. Individual prices will be determined "+
						"solely at the professional discretion of our staff after assessing market conditions, including supply, demand, date/location of the event, "+
						"quality of the seat, and volatility of the marketplace, as well as other proprietary formulas.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();	
				zoneticketsInformation.setHeader("Credit Card Usage");
				zoneticketsInformation.setData("Please note that any purchases made by credit card do not afford you any special considerations or variance from our User Agreement. In "+
						"certain instances, RewardTheFan.com may require you to submit additional proof of identity prior to your order being processed/shipped. "+
						"Such instances may include, but are not limited to: high dollar value orders, high-profile events, billing/shipping information that does not "+
						"match your credit card, etc. Additional proof of identity may be conducted online, via email, or via phone.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Shipping, Delivery, and Fulfillment");
				zoneticketsInformation.setData("All tickets purchased and confirmed through RewardTheFan.com are guaranteed to be received by you prior to the start of your event. Exact "+
						"fulfillment and method will vary depending on time, date and location. In most instances tickets will be sent via email, Fedex, messenger or "+
						"left at the box office.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Event Dates, Times & Venues");
				zoneticketsInformation.setData("Event date and times are printed on the ticket you will receive. However, please note that games and show times can/do change and "+
						"sometimes differ from the information printed on your ticket. This change can take place at any time before or after your purchase. While "+
						"we attempt to make sure all of our listings are accurate, it is your responsibility to confirm the start time and date of your event prior to "+
						"purchasing your tickets. By purchasing your ticket, you agree that RewardTheFan.com is not responsible for any changes in date and time. "+
						"Refunds will not be issued, except where permitted by this User Agreement. In addition to event dates and times changing, it is not "+
						"uncommon for events to change their venue due to a number of circumstances. If a new venue is announced and the event location is "+
						"changed, your tickets for the original venue will be honored or new tickets will be issued. A change of venue does not automatically entitle "+
						"you to a refund of your purchase in whole or part. Refunds will only be issued in accordance with the Returns/Refunds section of this "+
						"agreement.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Returns/Refunds");
				zoneticketsInformation.setData("There are no returns, refunds or exchanges once your order has been placed and confirmed. Since we acquire our inventory from a "+
						"number of sources (including VIP ticket holders, professional vendors, individual ticket holders, etc.), it is very difficult for us to cancel an "+
						"order or accept returns for ticket orders that have already been confirmed. However, refunds will be issued if your event is officially "+
						"canceled by the primary ticket source (team, artist, promoter, or venue.) For officially canceled events, refunds must be requested no later "+
						"than 10 days after the scheduled event date. Tickets must be mailed back to our office to receive the refund. "+
						"Refunds will not be offered for postponed, rescheduled or relocated events. If your event is rescheduled for a new date and/or time or "+
						"relocated to a new venue and your ticket remains valid, your purchase does not qualify for a refund.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Resources");
				zoneticketsInformation.setData("Our Resources include, but are not limited to any web content, ticket data, services, communications, and/or proprietary software that "+
						"comprise this user experience. Wherein, you understand that:\n"+
						"a) In order to access our Resources, you may be required to provide certain information about yourself (such as identification, contact "+
						"details, etc.). You agree that any information you provide will always be accurate, correct, and up to date. "+
						"b) You are responsible for maintaining the confidentiality of any login information associated with any account you use to access our "+
						"Resources. Accordingly, you are responsible for all activities that occur under your account/s. "+
						"c) Accessing (or attempting to access) any of our Resources by any means other than through the means we provide, is strictly prohibited. "+
						"You specifically agree not to access (or attempt to access) any of our Resources through any automated, unethical or unconventional "+
						"means "+
						"d) Engaging in any activity that disrupts or interferes with our Resources, including the servers and/or networks to which our Resources "+
						"are located or connected, is strictly prohibited. "+
						"e) Attempting to copy, duplicate, reproduce, sell, trade, or resell our Resources is strictly prohibited. "+
						"f) You are solely responsible for any consequences, losses, or damages that we may directly or indirectly incur or suffer due to any "+
						"unauthorized activities conducted by you, as explained above, and may incur criminal or civil liability. "+
						"g) We may provide various open communication tools on our website, such as blog comments, blog posts, public chat, forums, message "+
						"boards, newsgroups, product ratings and reviews, various social media services, etc. You understand that generally we do not pre-screen "+
						"or monitor the content posted by users of these various communication tools, which means that if you choose to use these tools to "+
						"submit any type of content to our website, then it is your personal responsibility to use these tools in a responsible and ethical manner. By "+
						"posting information or otherwise using any open communication tools as mentioned, you agree that you will not upload, post, share, or "+
						"otherwise distribute any content that: i) Is illegal, threatening, defamatory, abusive, harassing, degrading, intimidating, fraudulent, "+
						"deceptive, invasive, racist, or contains any type of suggestive, inappropriate, or explicit language; ii) Infringes on any trademark, patent, "+
						"trade secret, copyright, or other proprietary right of any party; iii) Contains any type of unauthorized or unsolicited advertising; iv) "+
						"Impersonates any person or entity, including our employees or representatives. We have the right at our sole discretion to remove any "+
						"content that, we feel in our judgment does not comply with this User Agreement, along with any content that we feel is otherwise "+
						"offensive, harmful, objectionable, inaccurate, or violates any 3rd party copyrights or trademarks. We are not responsible for any delay or "+
						"failure in removing such content. If you post content that we choose to remove, you hereby consent to such removal, and consent to "+
						"waive any claim against us. "+
						"h) We do not assume any liability for any content posted by you or any other 3rd party users of our website. However, any content posted "+
						"by you using any open communication tools on our website, provided that it doesn't violate or infringe on any 3rd party copyrights or "+
						"trademarks, becomes the property of RewardTheFan.com, and as such, gives us a perpetual, irrevocable, worldwide, royalty-free, exclusive "+
						"license to reproduce, modify, adapt, translate, publish, publicly display and/or distribute as we see fit. This only refers and applies to "+
						"content posted via open communication tools as described, and does not refer to information that is provided as part of the registration "+
						"process necessary in order to use our Resources."+
						"i) You agree to indemnify and hold harmless RewardTheFan.com and its parent company and affiliates, their directors, officers, managers, "+
						"employees, donors, agents, and licensors, from and against all losses, expenses, damages and costs, including reasonable attorneys' fees, "+
						"resulting from any violation of this User Agreement or the failure to fulfill any obligations relating to your account incurred by you or any "+
						"other person using your account. We reserve the right to take over the exclusive defense of any claim for which we are entitled to "+
						"indemnification under this User Agreement. In such event, you shall provide us with such cooperation as is reasonably requested by us.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Limitation of Warranties");
				zoneticketsInformation.setData("By using our website, you understand and agree that all Resources we provide are as is and as available. This means that we do not "+
						"represent or warrant to you that: "+
						"i) the use of our Resources will meet your needs or requirements. "+
						"ii) the use of our Resources will be uninterrupted, timely, secure or free from errors."+
						"iii) the information obtained by using our Resources will be accurate or reliable, and "+
						"iv) any defects in the operation or functionality of any Resources we provide will be repaired or corrected. Furthermore, you understand "+
						"and agree that: "+
						"v) any content downloaded or otherwise obtained through the use of our Resources is done at your own discretion and risk, and that you "+
						"are solely responsible for any damage to your computer or other devices for any loss of data that may result from the download of such "+
						"content. "+
						"vi) no information or advice, whether expressed, implied, oral or written, obtained by you from RewardTheFan.com or through any Resources "+
						"we provide shall create any warranty, guarantee, or conditions of any kind, except for those expressly outlined in this User Agreement.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Limitation of Liability");
				zoneticketsInformation.setData("In conjunction with the Limitation of Warranties as explained above, you expressly understand and agree that any claim against us shall be "+
						"limited to the amount you paid, if any, for use of products and/or services. RewardTheFan.com will not be liable for any direct, indirect, "+
						"incidental, consequential or exemplary loss or damages which may be incurred by you as a result of using our Resources, or as a result of "+
						"any changes, data loss or corruption, cancellation, loss of access, or downtime to the full extent that applicable limitation of liability laws "+
						"apply.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Copyrights/Trademarks");
				zoneticketsInformation.setData("All content and materials available on RewardTheFan.com, including but not limited to text, graphics, website name, code, images and logos "+
						"are the intellectual property of RewardTheFan.com, and are protected by applicable copyright and trademark law. Any inappropriate use, "+
						"including but not limited to the reproduction, distribution, display or transmission of any content on this site is strictly prohibited, unless "+
						"specifically authorized by RewardTheFan.com.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Termination of Use");
				zoneticketsInformation.setData("You agree that we may, at our sole discretion, suspend or terminate your access to all or part of our website and Resources with or "+
						"without notice and for any reason, including, without limitation, breach of this User Agreement. Any suspected illegal, fraudulent or "+
						"abusive activity may be grounds for terminating your relationship and may be referred to appropriate law enforcement authorities. Upon "+
						"suspension or termination, your right to use the Resources we provide will immediately cease, and we reserve the right to remove or "+
						"delete any information that you may have on file with us, including any account or login information.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Governing Law");
				zoneticketsInformation.setData("This website is controlled by RewardTheFan.com from our offices located in the state of New York in the United States of America. It can be "+
						"accessed by most countries around the world. As each country has laws that may differ from those of New York, by accessing our website, "+
						"you agree that the statutes and laws of the state of New York, without regard to the conflict of laws and the United Nations Convention on "+
						"the International Sales of Goods, will apply to all matters relating to the use of this website and the purchase of any products or services "+
						"through this site. Furthermore, any action to enforce this User Agreement shall be brought in the federal or state courts located in New "+
						"York, USA. You hereby agree to personal jurisdiction by such courts, and waive any jurisdictional, venue, or inconvenient forum objections "+
						"to such courts.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Guarantee");
				zoneticketsInformation.setData("UNLESS OTHERWISE EXPRESSED, RewardTheFan.com EXPRESSLY DISCLAIMS ALL WARRANTIES AND CONDITIONS OF ANY KIND, WHETHER "+
						"EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A "+
						"PARTICULAR PURPOSE AND NON-INFRINGEMENT.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("Contact Information");
				zoneticketsInformation.setData("If you have any questions or comments about these our Terms of Service as outlined above, you can contact us at: "+
						"RewardTheFan.com "+
						"575 7th ave, 3rd floor "+
						"New York, NY 10018 "+
						"info@RewardTheFan.com "+
						"800-601-6100");
				listZoneticketsInformation.add(zoneticketsInformation);	
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
			}else if(infoType.equalsIgnoreCase("faq")){
				getZoneticketsInformation.setStatus(1);
				zoneticketsInformation.setHeader("FAQ");
				sb.append("<ul><li><h5 style='color:#ed1c24;'>What is a zone?</h5><p>A zone is a user friendly way to purchase tickets to events "
						+ "using color-coded zones to indicate seating locations. A zone offers several advantages such as: zero service fees, "
						+ "no hidden charges, free delivery on every order and $5 in Reward Dollars for every $100 you spend.</p></li>");
				sb.append("<li><h5 style='color:#ed1c24;''>Is my order guaranteed?</h5><p>Yes. All orders are guaranteed.</p></li>");
				sb.append("<li><h5 style='color:#ed1c24;'>How soon after my order will I receive my tickets?</h5><p>We get our ticket inventory "
						+ "from a number of sources, including VIP vendors, season ticket holders and other fans. While tickets are often ready "
						+ "to ship in the days following the placement of your order, there are times when tickets are not printed, released or "
						+ "delivered until much closer to the date of the event. This is a very common occurrence in the ticket resale market, so "
						+ "there is no reason to worry. You order is guaranteed once you complete your purchase and your tickets will be delivered "
						+ "in plenty of time for you to enjoy the event.</p></li>");
				sb.append("<li><h5 style='color:#ed1c24;'>How will I receive my tickets?</h5><p>In most cases tickets are sent via email. "
						+ "Occasionally tickets may be sent via Federal Express, by messenger or left at the Box Office.</p></li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Will my seats be together?</h5><p>Yes. We guarantee that all tickets in each order that you "
						+ "place will be seated together.</p></li>");
				sb.append("<li><h5 style='color:#ed1c24;'>What is your refund policy?</h5><p>Refunds are issued only if the event is officially "
						+ "cancelled. If the event is postponed your tickets must be used for the rescheduled date. For officially cancelled "
						+ "events, refunds must be requested no later than 10 days after the original event date. Tickets must be mailed back to "
						+ "our office to receive a refund.</p></li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Why is someone else's name printed on my tickets?</h5><p>The name printed on the "
						+ "ticket is the name of the credit card holder who purchased those tickets directly from the venue (or the venues agent). "
						+ "That person has resold those tickets on the secondary market.</p></li>");
				sb.append("<li><h5 style='color:#ed1c24;'>Why is the price that is printed on the ticket different from the price that I paid?</h5>"
						+ "<p>The price printed on your ticket is not the price you paid but represents the price paid by the person who originally "
						+ "purchased the tickets from the venue and has since resold them on the secondary market.</p></li></ul>");
				zoneticketsInformation.setData(headerData + sb.toString() + footerData);
				//listZoneticketsInformation.add(zoneticketsInformation);
				//getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
				zoneticketsInformation.setHeader("What is Reward The Fan?");
				zoneticketsInformation.setData("Reward The Fan is a ticket marketplace that rewards customers for using our service and for referring others to do the same." +
"Fans earn 10% in Reward Points on all purchases and can also earn an additional 10% in points on the first purchase of every fan they refer!" +
"Fans can declare their loyalty to any one team or artist and receive a 10% discount on ALL ticket purchases to see them (in addition to earning the 10% Reward Points!)." +
"Reward The Fan points are not for sale, the system was designed so we could Reward you, for using our services and referring others, its that simple!");
 
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();		
				zoneticketsInformation.setHeader("What does it mean if I declare myself as a Loyal Fan of a team or artist?");
				zoneticketsInformation.setData("By declaring yourself as a �Loyal Fan� of a specific team or artist you will receive a 10% discount every time you purchase tickets to see them (in addition to the 10% rewards points you will earn on that purchase!). " +
						"Loyal Fan team or artists do not expire but you can elect to change your team/artist 12 months after your first LOYAL FAN purchase.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();	
				zoneticketsInformation.setHeader("How do I refer a friend?");
				zoneticketsInformation.setData("Visit our Refer a Friend tab where you can send your friends a referral code either via email or by sharing through Facebook, Google+ or your contact list and both you and your referred friend will earn 10% " +
						"in Rewards Points for their first purchase.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();			
				zoneticketsInformation.setHeader("How can I use my Reward Points?");
				zoneticketsInformation.setData("1. Reduce Your Purchase Price - Use your reward points to lower the cost of any future purchase. For example, if you purchase $300 worth of tickets and you have 100 Reward Points, you can apply " +
						"those 100 Reward points and we REWARD YOU by reducing your cost to $200 for the same tickets." +
"2. Free Fantasy Tickets to Fantasy Events - See your favorite teams at the Super Bowl! Here's how it works - redeem 200 of your Reward Points for the dream of seeing your favorite team make it to " +
"the Super Bowl. If they make it to the game, we will REWARD YOU with 2 FREE TICKETS! If they do not make it, you pay nothing and your Rewards account was simply reduced by 200 points.");

				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("What is a Fantasy Ticket?");
				zoneticketsInformation.setData("A Fantasy Ticket is a FREE TICKET to see your favorite team at a Fantasy Event like the Super Bowl, World Series, Final Four, BCS Championship, NBA Finals or the NHL Stanley Cup Finals." +
"Here's how it works - redeem 200 of your Reward Points for the dream of seeing your favorite team make it to the Super Bowl. If they make it to the game, we will REWARD YOU with 2 FREE TICKETS! " +
"If they do not make it, you pay nothing and your Rewards account was simply reduced by 200 points." +
"The only way to obtain a FREE FANTASY TICKET is to use your REWARD THE FAN points." +
"Fantasy Tickets are not for sale and are one of the ways we at Reward The Fan look to reward YOU the Fan, for using and referring our service.");

				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("When will my Reward Points be available for use?");
				zoneticketsInformation.setData("All Rewards Points become available 24 hours after the date of the event which you had purchased.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Does it matter whether I use the app, website or mobile website for my purchase?");
				zoneticketsInformation.setData("No, feel free to use whichever channel works best for you as your account runs across all of our sites and apps.");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Will my tickets be seated together?");
				zoneticketsInformation.setData("Yes. We guarantee that all tickets purchased in each order will be seated together within the Zone you selected.");
				listZoneticketsInformation.add(zoneticketsInformation);
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("How and when will I receive my tickets?");
				zoneticketsInformation.setData("We will send you an alert as soon as your tickets are ready for download in the MY TICKETS tab. In the event your tickets need to be shipped we will contact you to make arrangements at NO additional " +
						"cost to you. Please feel free to visit the MY TICKETS tab for updates on delivery status.");
				listZoneticketsInformation.add(zoneticketsInformation);
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Why is someone else's name printed on my tickets?");
				zoneticketsInformation.setData("The name printed on the ticket is the name of the person who purchased those tickets directly from the venue (primary market). That person has resold those tickets on the secondary market. However, " +
						"please keep in mind the name on your ticket has no bearing on whether you will be admitted to the event, the barcode is the only important piece of information on your ticket that will be scanned at the entrance.");
				listZoneticketsInformation.add(zoneticketsInformation);
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Why is the price printed on the ticket different from the price that I paid?");
				zoneticketsInformation.setData("The price printed on your ticket is not the price you paid but represents the price paid by the person who originally purchased the tickets from the venue and has since resold them on the secondary market.");
				listZoneticketsInformation.add(zoneticketsInformation);
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("What is your refund policy?");
				zoneticketsInformation.setData("Refunds are issued only if the event is officially cancelled. If the event is postponed, your tickets must be used for the rescheduled date. For officially cancelled events, refunds must be requested no later " +
						"than 10 days after the original event date. Tickets must be mailed back to our office to receive a refund.");

	listZoneticketsInformation.add(zoneticketsInformation);
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Can I cancel an order I have already placed?");
				zoneticketsInformation.setData("As we mention in our Terms and Conditions, all sales are final and barring the event itself being cancelled (not postponed) we do not issue refunds. ");
				listZoneticketsInformation.add(zoneticketsInformation);
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
				String color = "color:#2D9DC6",style = "justify",justColor="#2D9DC6";
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("How and when will I receive my tickets?");
				zoneticketsInformation.setData("<p align="+style+">We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab.<br/>"
						+ "In the event your tickets need to be shipped we will contact you to make arrangements at NO additional cost to you.<br/>"
						+ "We at Reward The Fan, along with most venues, artists and teams use extensive fraud protection methods that may sometimes delay the delivery of your tickets.<br/>"
						+ "Regardless of any delays, your order is guaranteed once you complete your purchase and your tickets will be delivered in plenty of time for you to enjoy the event.</p>");
				listZoneticketsInformation.add(zoneticketsInformation);	
				
						
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Why is someone else's name printed on my tickets?");
				zoneticketsInformation.setData("<p align="+style+">The name printed on the ticket is the name of the credit card holder who purchased those tickets directly from the venue "
						+ "(or the venue's agent). That person has resold those tickets on the secondary market.</p>");
				listZoneticketsInformation.add(zoneticketsInformation);			
						 

				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Why is the price that is printed on the ticket different from the price that I paid?");
				zoneticketsInformation.setData("<p align="+style+">The price printed on your ticket is not the price you paid but represents the price paid by the person "
						+ "who originally purchased the tickets from the venue and has since resold them on the secondary market.</p>");
				listZoneticketsInformation.add(zoneticketsInformation);			
						 	

				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("What is your refund policy?");
				zoneticketsInformation.setData("<p align="+style+">Full refunds are issued for any event that is officially cancelled. "
						+ "If the event is postponed your tickets are valid for the rescheduled date.</p>");
				listZoneticketsInformation.add(zoneticketsInformation);			 
						 
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
				
			}*/
			
						
			/*else if(infoType.equalsIgnoreCase("contact")){
				getZoneticketsInformation.setStatus(1);
				zoneticketsInformation.setHeader("Contact Us");
				sb.append("<p>Phone # - <span style='color:#ed1c24;'>800-601-6100</span></p><p>Fax - <span style='color:#ed1c24;'>212-382-2800</span></p></div>");
				sb.append("<div><p><span>Mailing Address </span><span>RewardTheFan<br>575 7th ave <br>3rd floor,<br>New York</span></p></div>");
				zoneticketsInformation.setData(headerData + sb.toString() + footerData);
				listZoneticketsInformation.add(zoneticketsInformation);
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
				zoneticketsInformation.setHeader("Phone #");
				zoneticketsInformation.setData("800-601-6100");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Fax");
				zoneticketsInformation.setData("212-382-2800");
				listZoneticketsInformation.add(zoneticketsInformation);		
				
				zoneticketsInformation = new ZoneticketsInformation();
				zoneticketsInformation.setHeader("Mailing Address");
				zoneticketsInformation.setData("RewardTheFan, 575 7th ave, 3rd floor, New York");
				listZoneticketsInformation.add(zoneticketsInformation);
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
			}else if(infoType.equalsIgnoreCase("feedbackandsupport")){
				getZoneticketsInformation.setStatus(1);
				zoneticketsInformation.setHeader("Contact Us");
				zoneticketsInformation.setData("sales@rewardthefan.com");
				listZoneticketsInformation.add(zoneticketsInformation);
				getZoneticketsInformation.setZoneticketsInformation(listZoneticketsInformation);
			}*/
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"Success");
		}catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while reseting password Request.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"Error occured while reseting password Request");
		}
		return getZoneticketsInformation;
	}
	
	@RequestMapping(value = "/GetRTFProductInfo",method=RequestMethod.POST)
	public @ResponsePayload RTFProductInformation getRTFProducteInfo(HttpServletRequest request,HttpServletResponse response,Model model){
		
		RTFProductInformation RTFProductInfo =new RTFProductInformation();
		Error error = new Error();
		try { 
			
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
            Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				RTFProductInfo.setError(error);
				RTFProductInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
				return RTFProductInfo;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						RTFProductInfo.setError(error);
						RTFProductInfo.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
						return RTFProductInfo;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					RTFProductInfo.setError(error);
					RTFProductInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
					return RTFProductInfo;
				}
			}else{
				error.setDescription("You are not authorized.");
				RTFProductInfo.setError(error);
				RTFProductInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
				return RTFProductInfo;
			}

			String productTypeStr = request.getParameter("productType");			
			String pageType = request.getParameter("pageType");
			
			if(TextUtil.isEmptyOrNull(pageType)){
					error.setDescription("Page Type is Mandatory");
					RTFProductInfo.setError(error);
					RTFProductInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"Page Type is Mandatory");
					return RTFProductInfo;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					RTFProductInfo.setError(error);
					RTFProductInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"Please send valid product type");
					return RTFProductInfo;
				}
			}
			
			/*if(TextUtil.isEmptyOrNull(platFormStr)){
			error.setDescription("Platform is mandatory.");
			getZoneticketsInformation.setError(error);
			getZoneticketsInformation.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATECUSTOMERLOYALFAN,"  is mandatory");
			return getZoneticketsInformation;
		}*/
		
		String platFormStr = request.getParameter("platForm");
		//String deviceType = request.getParameter("deviceType");
		ApplicationPlatForm applicationPlatForm=null;
		if(!TextUtil.isEmptyOrNull(platFormStr)){
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				error.setDescription("Please send valid platfrom");
				RTFProductInfo.setError(error);
				RTFProductInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"Please send valid platfrom");
				return RTFProductInfo;
			}
		}
		
		String deviceType = null != request.getParameter("deviceType")?request.getParameter("deviceType"):"";
		String imageSizeStr = "";
		 String fontTagStartStr = "";
		 String fontTagEndStr="";
		 String fontSizeStr="";
		 String headerFontSizeStr="";
		 if(applicationPlatForm.equals(ApplicationPlatForm.IOS)){
				try{
					if(deviceType.equals("IPHONE")){
						imageSizeStr ="width:310px; height:260px;";
						//if(pageType.equals("quizFaq")|| pageType.equals("quizHowToPlay")){
							fontSizeStr ="size='5'";
						//} else {
							//fontSizeStr ="size='4'";	
						//}
							headerFontSizeStr ="size='5'";
						
					}else if(deviceType.equals("IPAD")){
						fontSizeStr ="size='6'";
						headerFontSizeStr ="size='6'";
					}
				}catch (Exception e) {
					//fontSizeStr ="size='4'";
				}
			}
		 if(imageSizeStr.equals("")) {
			 imageSizeStr = "max-width:100%; height:auto;";
		 }
		 if(!fontSizeStr.equals("")) {
			 fontTagStartStr = "<font "+fontSizeStr+">";
			 fontTagEndStr = "</font>";
		 }
		 
			String color = "color:#2D9DC6",style = "justify",justColor="#2D9DC6";
			
			if(pageType.equals("RewardTheFan")){
				
				RTFProductInfo.setDescription(
						"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong><u>Reward The Fan</u></strong></font></p>" +
				    	"<p align="+style+">"+fontTagStartStr+"Reward The Fan is a ticket resale marketplace where every venue is broken down by ZONES and all ticket groups are always seated together."+fontTagEndStr+"</p>"+
						
						
						"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong><u>Reward Dollars</u></strong></font></p>" +
		    	"<p align="+style+">"+fontTagStartStr+"All fans earn 10% in reward dollars on every purchase. "+fontTagEndStr+"</p>"+ //with each point worth $1 DOLLAR towards ANY future ticket purchase!

				"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong><u>Loyal Fan 10% Discount Program</u></strong></font></p>"+
				"<p align="+style+">"+fontTagStartStr+"Register today in our LOYAL FAN discount program where fans can choose to receive a 10% discount " +
						"on ALL purchases by their favorite team of their favorite state."+fontTagEndStr+"</p>"+
						
				"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong><u>Referral Program</u></strong></font></p>"+
				"<p align="+style+">"+fontTagStartStr+"Earn reward dollars on the first purchase of EVERYONE you refer."+fontTagEndStr+"</p>"+
						
				"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong><u>Reward The Fan APP</u></strong></font></p>"+
				"<p align="+style+">"+fontTagStartStr+"Download our APP today and receive an additional 5% discount on ALL in-App purchases."+fontTagEndStr+"</p>"+
				
				"<p align="+style+">"+fontTagStartStr+"<br/>Reward The Fan dollars are not available for purchase and cannot be redeemed for cash, they are only available to you, THE FAN to thank you for using our service and referring others."+fontTagEndStr+"</p>"+

				
				"<div><p>"+fontTagStartStr+"Phone # - "+fontTagEndStr+"<span style='color:#ed1c24;'>"+fontTagStartStr+"800-601-6100"+fontTagEndStr+"</span></p>" + //<p>Fax - <span style='color:#ed1c24;'>212-382-2800</span></p>
				"<p><span style='font-weight: bold;' >"+fontTagStartStr+"Mailing Address "+fontTagEndStr+"</span><br><span>"+fontTagStartStr+"Reward The Fan<br>10 Times Square<br>3rd Floor<br>New York, NY 10018"+fontTagEndStr+"</span></p>");
				/*"<div style='width:100%;height:99%; position: absolute;text-align: center;' >" +
				"<img style='"+imageSizeStr+"' src='"+MapUtil.getAboutUsImage()+"' /></div>" );*/
				
			}else if(pageType.equals("faq")){
		
			RTFProductInfo.setDescription("<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>How and when will I receive my tickets?</strong></font></p>" +			
	
				"<p align="+style+">"+fontTagStartStr+"We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab.<br/>"
						+ "In the event your tickets need to be shipped we will contact you to make arrangements at NO additional cost to you.<br/>"
						+ "We at Reward The Fan, along with most venues, artists and teams use extensive fraud protection methods that may sometimes delay the delivery of your tickets.<br/>"
						+ "Regardless of any delays, once you complete your purchase, your order is guaranteed and your tickets will be delivered in plenty of time for you to enjoy the event."+fontTagEndStr+"</p>"+
				
				"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Why is someone else's name printed on my tickets?</strong></font></p>"+
				"<p align="+style+">"+fontTagStartStr+"The name printed on the ticket is the name of the credit card holder who purchased those tickets directly from the venue "
						+ "(or the venue's agent). That person has resold those tickets on the secondary market."+fontTagEndStr+"</p>"+
				
				"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Why is the price that is printed on the ticket different from the price that I paid?</strong></font></p>"+
				"<p align="+style+">"+fontTagStartStr+"The price printed on your ticket is not the price you paid but represents the price paid by the person "
						+ "who originally purchased the tickets from the venue and has since resold them on the secondary market."+fontTagEndStr+"</p>"+
				
				"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What is your refund policy?</strong></font></p>"+					
				"<p align="+style+">"+fontTagStartStr+"Full refunds are issued for any event that is officially cancelled. "
						+ "If the event is postponed your tickets are valid for the rescheduled date."+fontTagEndStr+"</p>");
	
			
			/*RTFProductInfo.setDescription("<p style="+style+">At Reward The Fan, we set out to create a system that Rewards you, " +
					"the Fan for:</p><p>1) Being a Loyal Fan</p><p>2) Using our service</p><p>3) Referring others.</p>" +
					"<p style="+style+">Our goal is to reward your loyalty with points that can be used " +
					"to get great deals on tickets to your favorite events, &nbsp;as well as obtaining <font color="+justColor+">" +
					"<strong>FREE TICKETS</strong></font> to once in a lifetime &ldquo;<strong>Crown Jewel Events</strong>&rdquo; " +
					"like the Super Bowl, NBA Finals, or the World Series. We want to ensure that the system we create is based purely " +
					"on the merits of you being a fan and supporting our marketplace.</p><p style="+style+">So, " +
					"along those lines, our Reward Points are not for sale and cannot be purchased. " +
					"The system is designed to Reward you, the fan for using our services and referring others, " +
					"it&rsquo;s that simple! </p>");*/
			
		} else if(pageType.equals("quizReferFriends")){
			justColor = "#012e4f";
			style="center";
			// if(applicationPlatForm.equals(ApplicationPlatForm.ANDROID)){
				 RTFProductInfo.setHeaderText("");//REFER FRIENDS

					RTFProductInfo.setDescription("<p align="+style+"><font "+fontSizeStr+" color="+justColor+" >"
							+ "<strong>PLAY</strong> together, <strong>EARN</strong> together! <br>"
							+ "<em>For every friend you refer earn:</em> <br>"
							+ "<strong>ONE FREE LIFE! </strong><br/> "
							+ "&  <br>"
							+ "Every game your friends play <br>"
							+ "you will earn an additional <br>"
							+ "<strong>$0.05</strong> in Reward Dollars <br>"
							+ "for each question they get right!</font></p>"
							);
					
			/*} else {
				RTFProductInfo.setDescription("<p align="+style+"><font "+headerFontSizeStr+" color="+justColor+" >"
						+ " <strong>REFER FRIENDS</strong></font><br> "
						+ "<font "+fontSizeStr+" color="+justColor+" ><strong>PLAY together, GET PAID together!</strong> <br>"
						+ "<em>For every friend you refer:</em> <br>"
						+ "<strong>ONE EXTRA LIFE! </strong><br/> "
						+ "&  <br>"
						+ "<strong>You wil EARN $0.01 Reward dollars for every QUESTION a Referral gets RIGHT!</strong></font></p>"
						);
			}*/
			
		} else if(pageType.equals("quizFaq")){
			justColor="#000000";
			style = "left";
			fontTagStartStr = "<font "+fontSizeStr+" color="+justColor+">";
			String fontTagStartStr1 =  "<font "+fontSizeStr+" color='#FFFFFF'>";
			fontTagEndStr = "</font>";
			 
			RTFProductInfo.setDescription(
					/*"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How to earn rewards?</strong></font></p>"+					
					"<table cellspacing='10' align='center' style='width: 100%;'>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Answer Question Right"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"10 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Answer By Referral"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"10 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Watch a RTF TV Video"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"10 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Upload Video On Fanclubs"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"15 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Upload Fan Freakout Video"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"25 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Like 5 Videos"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"5 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Like 5 Posts"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"5 Points"+fontTagEndStr+"</td></tr>"+
					//"<tr><td width='67%'>"+fontTagStartStr+"Share a Video"+fontTagEndStr+"</td>"+
					//"<td style='background-color:#153b62'>"+fontTagStartStr1+"10 Stars"+fontTagEndStr+"</td></tr>"+
					//"<tr><td width='67%'>"+fontTagStartStr+"Scratch & Win"+fontTagEndStr+"</td>"+
					//"<td style='background-color:#153b62'>"+fontTagStartStr1+"10 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Referral & Referee"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"100 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Create Fanclub Post"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"10 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Create Fanclub Event"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"10 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Interest 5 Fanclub Event"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"5 Points"+fontTagEndStr+"</td></tr>"+
					"<tr><td width='67%'>"+fontTagStartStr+"Fill Profile Info"+fontTagEndStr+"</td>"+
					"<td style='background-color:#153b62'>"+fontTagStartStr1+"2500 Points"+fontTagEndStr+"</td></tr>"+
					//"<tr><td width='67%'>"+fontTagStartStr+"Fill Fandom Profile Info"+fontTagEndStr+"</td>"+
					//"<td style='background-color:#153b62'>"+fontTagStartStr1+"2000 Points"+fontTagEndStr+"</td></tr>"+
					"</table>"+*/
					
					"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>What is Reward The Fan?</strong></font></p>"+
					"<p align="+style+">"+fontTagStartStr+"Reward The Fan is a LIVE Shopping Game Show that gives you, THE FAN, the power to earn bigger and bigger discounts in real time, just by answering questions correctly during our LIVE GAME SHOW."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>How Much Does It Cost?</strong></font></p>"+
					"<p align="+style+">"+fontTagStartStr+"The App is free to download from the App Store and Google Play Store and you do NOT need to spend money in order to play or win the game. You can however, use your \"smarts\" by answering trivia questions correctly to earn discounts and reward points to score a great deal."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>How Do I Play?</strong></font></p>"+
					"<p align="+style+">"+fontTagStartStr+"To learn about how to play the Reward The Fan�s LIVE Shopping Game Show, please check out our How To Play section (located in About Us)."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What are Reward Points?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Reward Points are all about rewarding you for registering, creating a FANDOM profile, referring others and for purchasing items � basically they were created to reward you for your loyalty! Reward Points can also be redeemed at the time of purchase to provide you with additional discounts."+fontTagEndStr+"</p>" +
					
					
					"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>What happens to my existing rewards account?</strong></font></p>"+
					"<p align="+style+">"+fontTagStartStr+"We have simplified our loyalty program by converting our previous REWARD DOLLAR model to a more widely accepted REWARD POINTS program, with every ONE of your reward dollars now equal to ONE HUNDRED reward points (1 Reward Dollar = 100 Reward Points) For those of you who had also earned lives, wands and points, we first converted all those into reward dollars before converting your reward dollars into the new reward points system."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>What happens to my tickets for live events?</strong></font></p>"+
					"<p align="+style+">"+fontTagStartStr+"Tickets redeemed using reward dollars"+fontTagEndStr+"</p>"+
					"<p align="+style+">"+fontTagStartStr+"If you redeemed tickets for a LIVE event that has been rescheduled you may still use your tickets for that event. In the event you no longer want to attend the event (for any reason at all) feel free to contact us and we�ll cancel your redemption, convert your reward dollars to reward points and credit your account. If you redeemed tickets for an event that has been officially cancelled, we may have already credited your account but if not, please feel free to send an email to <a href=\"mailto:care@rewardthefan.com\">care@rewardthefan.com</a> with the details of your redemption and we will adjust accordingly."+fontTagEndStr+"</p>"+
					"<p align="+style+">"+fontTagStartStr+"<strong>Grand Prize Ticket Winners</strong>"+fontTagEndStr+"</p>"+
					"<p align="+style+">"+fontTagStartStr+"If you were a Grand Prize Ticket Winner, who received tickets to an event that was rescheduled or canceled and have yet to contact us regarding your tickets, please email us at <a href=\"mailto:care@rewardthefan.com\">care@rewardthefan.com</a> and we will address your order in the same manner as tickets redeemed."+fontTagEndStr+"</p>"+
					
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>When will I receive the item I purchased?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"All items listed have an approximate delivery date associated with them, please visit the MY ORDERS section to check the status of your delivery."+fontTagEndStr+"</p>"+

					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>Why does Reward The Fan require a phone number to sign up?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"We require a phone number in order to verify and protect all accounts. This verification process helps to prevent users from trying to sign up on multiple devices."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What is My Referral Code?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Your referral code is your username. Share your username with your friends and earn 250 reward points when they register and play their first game and earn another 1000 reward points after they make their first purchase."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>Where can I add a Referral Code after registering?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"On the home screen, tap the three-dot button image and choose the Add Referral option. Type in your referral code and tap Okay."+fontTagEndStr+"</p>"+
					
					/*"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>Can i change my username?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Sure! Tap on the three-dot button image on your home page. Tap on your currently listed username and type in a new one!"+fontTagEndStr+"</p>"+*/
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>My stream is not clear. How can I fix this issue?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"If you are having any streaming issues, Tap on the Reward The Fan icon on the upper left corner of the game. Exit the game and re-enter by tapping on the Play Game Button on the bottom left of the home screen. If the issue persists, make sure you are connected to a strong WiFi connection or have strong cell service. If this does not fix your issue, please email us at <a href=\"mailto:help@rewardthefan.com\">help@rewardthefan.com</a>"+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I'd like a shoutout for my birthday, or another special event. How can I submit that information?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"We�d love to give you a shoutout! Please email us at <a href=\"mailto:shoutouts@rewardthefan.com\">shoutouts@rewardthefan.com</a>."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I'd like to send feedback or suggest a cool feature. How can I do that?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"We love your feedback! Email us at <a href=\"mailto:feedback@rewardthefan.com\">feedback@rewardthefan.com</a> or feel free to message us on Social Media."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I�m writing an article or looking to interview someone about Reward The Fan. Who can I reach out to? </strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Awesome! Email us at <a href=\"mailto:press@rewardthefan.com\">press@rewardthefan.com</a> with details."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How can I submit trivia questions to be used in the live games?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Check out our Submit Trivia tab under our About section."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I'd like to partner with Reward The Fan, who can I contact?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Please email us at <a href=\"mailto:partnerships@rewardthefan.com\">partnerships@rewardthefan.com</a> with all of the details!"+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How do I submit a complaint about copyright, impersonation, trademark, or other Terms of Service issues?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Review our Terms of Service and Rules for more information. They can be found under the About section in the Settings tab. For any other issues, email us at <a href=\"mailto:feedback@rewardthefan.com\">feedback@rewardthefan.com</a>. "+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>Having any other trouble?</strong></font></p>"+		
					"<p align="+style+">"+fontTagStartStr+"Contact us at <a href=\"mailto:help@rewardthefan.com\">help@rewardthefan.com</a> with any issues you're having."+fontTagEndStr+"</p>");

					
					
					/*"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How do I get Extra Lives?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"To earn an extra life, refer your friends using your referral code (User ID). You can also receive extra lives by playing our �Scratch & Win� Games, and through in-game Reward Box prizes!"+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How do I use an Extra Life during a game?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"If you answer a question incorrectly, you will be prompted with a pop up asking if you want to use an Extra Life. Tap, �Use Life� to use it in that game and get back in on the action! You can only use one Extra Life per game, and not on the final question."+fontTagEndStr+"</p>"+

					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How many lives can I use per game?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"You can use one life per game, just not on the final question!"+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What are wands?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"A wand eliminates an incorrect answer choice, leaving you with 2 answer choices instead of 3 for a given question. Only one may be used per game, and not on the final question."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How can I use a wand?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Tap on the wand icon to the left of the question once it shows up on your screen. One of the answer choices will disappear, leaving you with a 50/50 chance of getting the question right."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What is the Refer Friends feature?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"For every friend you refer using your Referral Code, you will receive an extra life, and so will they! In addition to that, you will also earn 10 points in Reward Points for every question they answer correctly."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>My friend joined using my Referral Code, but I haven't received my Extra Life.</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Not to worry! First, confirm with your friend that they spelled your UserID correctly. You'll receive an Extra Life ONLY once they've played their first game."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How can I view the Reward Dollars I�ve earned through referring friends?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"On the home screen of the app, tap on your image located at the bottom right. Then tap on your Reward dollars at the top of the screen. You will see your Current Reward Dollar Balance, and right below you�ll find your Referral Reward Dollars."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What is a Reward Box?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Reward Boxes appear during games and are a way for fans to win rewards without having to get every question right! There�s at least one per game, so keep your eyes peeled."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How do I redeem my Reward Box Prize?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Most reward box prizes are automatically placed into your account, but if you are having any other issues please contact us at <a href=\"mailto:care@rewardthefan.com\">care@rewardthefan.com</a>"+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I won the Grand Prize Ticket Lottery and can�t find any events near me, what should I do?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Grand Prize Ticket Lottery winners receive a list of events to choose from. Please verify that your location has been input correctly. If you are still not seeing a list of events in your area, please contact us at <a href=\"mailto:care@rewardthefan.com\">care@rewardthefan.com</a>."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I won the Grand Prize Ticket Lottery, can I transfer my tickets to someone else?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"For any ticket related issues, please contact us at <a href=\"mailto:sales@rewardthefan.com\">sales@rewardthefan.com</a>. "+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I redeemed my Reward Dollars for tickets but haven�t received them yet, what should I do?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"For any ticket related issues, please contact us at <a href=\"mailto:sales@rewardthefan.com\">sales@rewardthefan.com</a>. "+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>Can I combine my Reward Dollars with another player�s?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Reward The Fan accounts can not be combined. For further questions, please email us at <a href=\"mailto:sales@rewardthefan.com\">sales@rewardthefan.com</a>! "+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How many Reward Dollars do I need to redeem for an event?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"That depends on the event. You can check specific events in the Tickets section by tapping on the �Present� icon in the top right corner of the home screen."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How can I get a Gift Card?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Gift Cards can be won during Reward Boxes throughout our live games. They are also often released after games in the Gift Card section of the app. Those can be redeemed using Reward Dollars."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>Why are there so few Gift Cards?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"We�re rolling them out in a limited basis for now, so you have to act fast to claim them! Hopefully we will be able to release more at a time in the future. "+fontTagEndStr+"</p>"+
					
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What is the Superfan game?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"The Superfan games are unique games where reward dollars, gift cards, and tickets are given away on a larger scale than we normally do!"+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What are stars?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Stars are earned by playing games, having your referrals play games, and through Reward Boxes. If you reach a certain number of stars, you gain automatic entry into every lotto for the next Superfan Game."+fontTagEndStr+"</p>"+
					
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How can I check how many stars I need for a Superfan Game?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Anyone can play the Superfan Game regardless of how many stars they have. But to find the threshold for automatic entry, tap on the number of stars you currently have on the home screen."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How can I see when the next games are?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"There is a schedule of the upcoming games on the home screen of the app."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What is �Scratch & Win�?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Scratch & Win is an opinion based polling game where you can win prizes. Just tap the answer you prefer, and scratch to reveal if you�ve won a prize. Prizes include wands, lives, and stars."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>What is RTF TV?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"RTF TV is a place where fans can watch RTF content, in addition to submitting their own videos and photos to be featured on the show and on RTF TV. "+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>How can I make sure I don�t miss a game?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"Be sure to turn on push notifications! Tap on the little bell that appears at the top right of each individual game to turn on your push notifications. "+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I want to work at Reward The Fan. How can I apply?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"We'd love to have you on the team! Email us at <a href=\"mailto:jobs@rewardthefan.com\">jobs@rewardthefan.com</a> to inquire."+fontTagEndStr+"</p>"+
					
					"<p align="+style+"><font "+fontSizeStr+"  color="+justColor+"><strong>I LOVE Reward The Fan, how can I leave a review on the App or Google Play stores?</strong></font></p>"+					
					"<p align="+style+">"+fontTagStartStr+"We are so glad you�re enjoying our app! Check us out on the App Store and Google Play Store to leave a review! "+fontTagEndStr+"</p>");*/
					
					
			
			/*RTFProductInfo.setDescription("<p style="+style+">At Reward The Fan, we set out to create a system that Rewards you, " +
					"the Fan for:</p><p>1) Being a Loyal Fan</p><p>2) Using our service</p><p>3) Referring others.</p>" +
					"<p style="+style+">Our goal is to reward your loyalty with points that can be used " +
					"to get great deals on tickets to your favorite events, &nbsp;as well as obtaining <font color="+justColor+">" +
					"<strong>FREE TICKETS</strong></font> to once in a lifetime &ldquo;<strong>Crown Jewel Events</strong>&rdquo; " +
					"like the Super Bowl, NBA Finals, or the World Series. We want to ensure that the system we create is based purely " +
					"on the merits of you being a fan and supporting our marketplace.</p><p style="+style+">So, " +
					"along those lines, our Reward Points are not for sale and cannot be purchased. " +
					"The system is designed to Reward you, the fan for using our services and referring others, " +
					"it&rsquo;s that simple! </p>");*/
			
		}else if(pageType.equals("quizHowToPlay")){
		
			justColor="#909192";
			style = "center";
			fontTagStartStr = "<font "+fontSizeStr+" color="+justColor+">";
			fontTagEndStr = "</font>";
			
			String imageBaseUrl =  AWSFileService.awsS3Url;
					
			List<RTFProductInfoDtls> infoDtlsList = new ArrayList<RTFProductInfoDtls>();
			RTFProductInfoDtls infoDtls = new RTFProductInfoDtls();
			
			String description = "<p align="+style+">"+fontTagStartStr+"<strong>Join our live game!</strong><br/>" +
					"We will send a push notification. " +
					"Don�t be late or you�ll only be able to watch!"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew1.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Tap your answer!</strong><br/>" +
			"Tap the answer you think is correct " +
			"in under 10 seconds."+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew2.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>One wrong &amp; you're out!</strong><br/>" +
			"If you tap the incorrect answer, " +
			"or run out of time, you�ll be " +
			"eliminated from the game."+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew3.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Use a life to continue playing!</strong><br/>" +
			"If you tap the incorrect answer, you can use " +
			"a life to save yourself and move onto the next question. " +
			fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew4.png");
			infoDtlsList.add(infoDtls);

			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Get them all right and you win!</strong><br/>" +
			"If you answer all the questions correctly, you win " +
			"reward dollars. More than one winner? " +
			"Split the prize! "+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew5.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Plus, Grand Prize Winner!</strong><br/>" +
			"In addition to winning Reward Dollars, " +
			"all finalists are entered into grand " +
			"prize lottery. Where one lucky winner will " +
			"win the Grand Prize!"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew6.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Use Reward Dollars to Redeem Gift Cards!</strong><br/>" +
			"Reward Dollars may be used to redeem " +
			"gift cards, merchandise and tickets. There is no minimum amount necessary. "+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew7.png");
			infoDtlsList.add(infoDtls);

			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Still have questions?</strong><br/>" +
			"Contact us at: " +
			"help@rewardthefan.com or message us on Instagram, Twitter or Facebook!"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplaynew8.png");
			infoDtlsList.add(infoDtls);
			
			RTFProductInfo.setProductInfoDetails(infoDtlsList);
			
			
		} else if(pageType.equals("HOWITWORKS")){
			justColor="#909192";
			style = "center";
			fontTagStartStr = "<font "+fontSizeStr+" color="+justColor+">";
			fontTagEndStr = "</font>";
					
			List<RTFProductInfoDtls> infoDtlsList = new ArrayList<RTFProductInfoDtls>();
			RTFProductInfoDtls infoDtls = new RTFProductInfoDtls();
			
			
			String description = "<p align="+style+">"+fontTagStartStr+"<strong>How To PLAY and SHOP:</strong>"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Just tap or click on the correct answer." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Answer correctly and advance, get it wrong and you are knocked out but can always watch or shop." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"The more you answer correctly the BIGGER the discount." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Get them all correct and you are automatically entered into the GRAND PRIZE digital randomizer." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
		/*	infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"All product listings and DISCOUNT CODES expire within 24 hours, with NEW listings posted just prior to showtime." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);*/
			
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"You can also use your reward points to earn even BIGGER discounts on purchases." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
						
			RTFProductInfo.setProductInfoDetails(infoDtlsList);
		}else if(pageType.equals("REWARDPOINTS")){
			justColor="#909192";
			style = "center";
			fontTagStartStr = "<font "+fontSizeStr+" color="+justColor+">";
			fontTagEndStr = "</font>";
					
			List<RTFProductInfoDtls> infoDtlsList = new ArrayList<RTFProductInfoDtls>();
			RTFProductInfoDtls infoDtls = new RTFProductInfoDtls();
			
			String description = "<p align="+style+">"+fontTagStartStr+"<strong>How to earn reward points:</strong>"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Earn 1000 reward points just for registering with us." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Earn and additional 1000 reward points by completing your FANDOM profile." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Earn 1 point for every dollar you spend." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Refer a friend and earn 1000 reward points on their first purchase.." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			infoDtls = new RTFProductInfoDtls();
			description = "<ul><li>"+fontTagStartStr+
					"Refer a friend and earn 250 reward points when they play their first game." +fontTagEndStr+"</li></ul>";
			infoDtls.setDescriptionText(description);
			infoDtlsList.add(infoDtls);
			
			
			
						
			RTFProductInfo.setProductInfoDetails(infoDtlsList);
		}else if(pageType.equals("REFUNDPOLICY")){
			justColor="#909192";
			style = "center";
			fontTagStartStr = "<font "+fontSizeStr+" color="+justColor+">";
			fontTagEndStr = "</font>";
					
			/*RTFProductInfo.setDescription(
					"<p>"+fontTagStartStr+"<strong>Return Policy</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"Our return policy varies based on seller and product, so please be sure to check the fine print for all deals before purchasing."+fontTagEndStr+"</p>"+
					"<p><ul><li>"+fontTagStartStr+"You can return most products for a refund within 30 days of receiving them."+fontTagEndStr+"</li>"+
					"<li>"+fontTagStartStr+"Refunds will be processed 1�2 business days after we receive your return."+fontTagEndStr+"</li>"+
					"<li>"+fontTagStartStr+"Please note that original shipping charges are non-refundable unless the returned item is defective."+fontTagEndStr+"</li></ul></p>"+
					
					"<p>"+fontTagStartStr+"Please ship your returns to:<br/>Reward The Fan<br/>159-16 Union Turnpike Suite 212, Fresh Meadows, NY 11366."+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"If you have any additional questions regarding our return policy, please email us at <a href=\"mailto:returns@rewardthefan.com\">returns@rewardthefan.com</a>!"+fontTagEndStr+"</p>"
						
					
					);*/
			RTFProductInfo.setDescription(
					"<p>"+fontTagStartStr+"<strong>What is our Return Policy?</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"Although it is not the norm, sometimes items just don't work correctly straight from the manufacturer and we apologize in advance, should that happen with the item you purchased. Usually there's nothing you or Reward The Fan could have done to prevent it. Returns are allowed if Reward The Fan receives your request within 30 calendar days from delivery. However, all returns must be received complete (all original packaging and contents) by Reward The Fan within 14 calendar days after the return has been approved."+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"Return shipping costs are covered by Reward The Fan for any damaged or defective product during delivery to our customers. Some items are final sales and may not be eligible for returns."+fontTagEndStr+"</p>"+
					
					"<p>"+fontTagStartStr+"<strong>Can I start return Process?</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"Yes, you can initiate your own return by emailing to <a href=\"mailto:support@rewardthefan.com\">support@rewardthefan.com</a> but please remember that your order must have been placed within the last 30 days"+fontTagEndStr+"</p>"+
					
					"<p>"+fontTagStartStr+"<strong>Are there exceptions to this return policy?</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"Yes, Following are our policies:"+fontTagEndStr+"</p>"+
					
					"<p>"+fontTagStartStr+"<strong>Apparel and shoes</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"If you are not satisfied with your purchase or size of the item, return it in new and unworn condition in the original packaging for a full refund. Any product(s) that have been resized, damaged, or otherwise altered after delivery will not be accepted for return."+fontTagEndStr+"</p>"+
					
					"<p>"+fontTagStartStr+"<strong>Heavy/Bulky items</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"For heavy or bulky items shipped by back to us there is a flat $50 return fee."+fontTagEndStr+"</p>"+
					
					"<p>"+fontTagStartStr+"<strong>Food and perishables</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"Food and perishables are not returnable to Reward The Fan, but in some special cases may be refundable or replaceable based on customer issues and availability."+fontTagEndStr+"</p>"+
					
					"<p>"+fontTagStartStr+"<strong>Electronic Devices?</strong>"+fontTagEndStr+"</p>"+
					"<p>"+fontTagStartStr+"Any electronic device that is damaged through customer misuse, is missing parts, or is in unsellable condition due to customer tampering will not be accepted for return."+fontTagEndStr+"</p>"
										
					);
		}else if(pageType.equals("CrownJewelEvents")){
				
				/*RTFProductInfo.setDescription("<p align="+style+"><font color="+justColor+"><strong>What are Fantasy Events?</strong></font></p>" +
						"<p align="+style+">These are the very special, once in a lifetime experiences. For some sports fans it might be the " +
								"Super Bowl, for others the World Series or the Final FourFor Concertgoers, it could be the chance to sit up close, " +
								"on the floor and perhaps even a meet and greet with your favorite artist.See your favorite team in the Super Bowl.</p>" +
								"<p align="+style+"><font color="+justColor+"><strong>Here&rsquo;s how it works&ndash;</strong></font></p>" +
								"<p align="+style+">Use your Reward Points today on the dream of seeing your favorite team make it to the " +
								"Super Bowl and if they actually make it to the game, we will REWARD YOU with 2 FREE TICKETS! If they" +
								" do not make it, you pay nothing and your Rewards account was simply reduced by the Reward Points " +
								"designated for your team.");*/
				
				
				RTFProductInfo.setDescription("<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>What are Fantasy Sports Tickets?</strong></font></p>" +
						 

						"<p align="+style+">"+fontTagStartStr+"Fantasy Sports Tickets are FREE tickets to those once in a lifetime special events like the Super Bowl, World Series, NBA Finals, the World Cup, Final Four, etc."+fontTagEndStr+"</p>"+


						"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Here&rsquo;s how it works...</strong></font></p>"+

						"<p align="+style+">"+fontTagStartStr+"Use the Reward Dollars you earned to secure a Fantasy Sports Ticket to see your favorite team make it to the Super Bowl.<br/>"+
						"If your team makes it to the Super Bowl, we will reward you, THE FAN with FREE TICKETS.<br/>"+
						"If your team does not make it, we return your reward dollars to your rewards account."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"Fantasy Sports Tickets gives you, THE FAN, the potential to see your favorite team at the big game for FREE- simply by using our service and referring others who end up using Reward The Fan as well!"+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"It&rsquo;s that simple!"+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<i>Fantasy Sports Tickets may not be purchased or sold for cash and are only available to you, THE FAN,  by redeeming your REWARD THE FAN DOLLARS.</i>"+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<b>Please visit our Reward Dollars section to learn all the ways you can earn dollars that can be redeemed for FREE Fantasy Sports Tickets.</b>"+fontTagEndStr+"</p>");
				
					/*RTFProductInfo.setDescription("<html><body><h5 style="+color+"> What are Crown Jewel Events?</h5> <div> <p> These are the very special, once in a lifetime experiences. For some sports fans it might be the Super Bowl, for others the World Series or the Final Four" +
						"For Concertgoers, it could be the chance to sit up close, on the floor and perhaps even a meet and greet with your favorite artist." + 
						"See your favorite team in the Super Bowl. Heres how it works � use your Reward Points today on the dream of seeing your favorite team " +
						"make it to the Super Bowl and if they actually make it to the game, we will REWARD YOU with 2 FREE TICKETS! If they do not make it, " +
						"you pay nothing and your Rewards account was simply reduced  by the Reward Points designated for your team.</p>" +
						"</div></body></html>");*/
			}else if(pageType.equals("ReferFriend")){
					RTFProductInfo.setDescription("<p align="+style+">"+fontTagStartStr+"Earn <strong>10%</strong> on the first purchase of every fan you refer"+fontTagEndStr+"</p>" +
							"</body></html>");
				
			}else if(pageType.equals("LoyalFan")){
				String center="center",temp="text-align: justify;",tempColor="color: #2D9DC6;";

				/*RTFProductInfo.setDescription("<p style="+temp+">In addition to receiving 10% in rewards points on all purchases " +
						"and 10% on the first sale of other fans you refer, you can also declare yourself as a <strong>" +
						"<font color="+justColor+">Loyal Fan</font></strong> of any ONE team or ONE artist and every time you purchase " +
								"tickets for that ONE team or ONE artist&ndash; you will receive a <strong>10% discount</strong>.</p>" +
								"<center><em><strong> Once team or artist is choosen cannot be reverted back to 12 months.</strong></em></center>");*/
				
				RTFProductInfo.setDescription("<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>ALL LOYAL FANS!</strong></font></p>" +

				"<p align="+style+">"+fontTagStartStr+"SPORTS FANS � choose your favorite team and receive a 10% discount on any of their games, whether home or away!"+fontTagEndStr+"</p>"+
				"<p align="+style+" style='text-align:center;' >"+fontTagStartStr+" <strong>OR</strong> "+fontTagEndStr+"</p>"+
				"<p align="+style+">"+fontTagStartStr+"CONCERT FANS � choose any STATE and receive a 10% discount on all CONCERT tickets in your STATE."+fontTagEndStr+"</p>"+
				"<p align="+style+" style='text-align:center;' >"+fontTagStartStr+" <strong>OR</strong> "+fontTagEndStr+"</p>"+
				"<p align="+style+">"+fontTagStartStr+"THEATER FANS � choose any STATE and receive a 10% discount on any theatrical performances in your STATE."+fontTagEndStr+"</p><br>"+
				"<p>"+fontTagStartStr+"Please remember, you can only select <strong>ONE</strong> of the above but one year after your first purchase, you can choose to keep or change your LOYAL FAN status."+fontTagEndStr+"</p>");

				RTFProductInfo.setDescriptionText("<p align="+style+">"+fontTagStartStr+"In addition to earning 10% Rewards Dollars on ALL your purchases and on the first purchase of EVERYONE your refer..."+fontTagEndStr+"</p>"+

						"<p>"+fontTagStartStr+"Select your favorite team or artist, declare yourself as their LOYAL FAN and receive a 10% discount EVERY time you purchase tickets to see them, whether HOME or AWAY!"+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<b>*LOYAL FAN status begins with the first purchase of your FAVORITE team/artist and cannot be changed to another team/artist for 12 months, at which point you can elect to continue or change to another team/artist.</b>"+fontTagEndStr+"</p>");
				
				
			/*	RTFProductInfo.setDescription("<p style="+temp+">In addition to receiving 10% in rewards points on all purchases " +
						"and 10% on the first sale of other fans you refer, you can also declare yourself as a <strong>" +
						"<span style="+tempColor+">Loyal Fan</span></strong> of any ONE team or ONE artist and every time you purchase " +
						"tickets for that ONE team or ONE artist&ndash; you will receive a <strong>10% discount</strong>.</p>" +
						"<center><em><strong> Once team or artist is choosen cannot be reverted back to 12 months.</strong></em></center>");*/
				
				
					/*RTFProductInfo.setDescription("<p align="+style+">In addition to receiving 10% in rewards points on all purchases and 10% " +
									"on the first sale of other fans you refer, you can also declare yourself as a <strong><font color="+justColor+">Loyal " +
									"Fan</font></strong> of any ONE team or ONE artist and every time you purchase tickets for that ONE team or ONE " +
									"artist&ndash; you will receive a <strong>10% discount</strong>.</p><p align="+center+"><em><strong> " +
									"Once team or artist is choosen cannot be reverted back to 12 months.</strong></em></p>");*/
					
					/*RTFProductInfo.setDescription("<html><body>In addition to receiving 10% in rewards points on all purchases and " +
							"10% on the first sale of other fans you refer, you can also declare yourself as a loyal fan of any ONE team or " +
							"ONE artist and every time you purchase tickets for that ONE team or ONE artist� you will receive a 10% discount. Once " +
							"team or artist is choosen cannot be reverted back to 12 months." +
							"</div></body></html>");*/
					
					
					

				
			}else if(pageType.equals("LoyalFanPercentage")){
					RTFProductInfo.setDescription("10%");
				
			
				
			}else if(pageType.equals("RewardPoints")){
					/*RTFProductInfo.setDescription("<p align="+style+"><font color="+justColor+"><strong>How to Earn Reward Points:</strong></font></p>" +
									"<p align="+style+"><strong>Purchase Tickets-</strong> Every fan earns 10% in reward points on ALL purchases.</p>" +
									"<p align="+style+"><strong>Refer a Friend&ndash;</strong> Earn 10% on the first purchase of every fan you refer.</p>" +
									"<p align="+style+"><strong>Loyal Fans&ndash;</strong> In addition to receiving 10% in rewards points on all " +
									"purchases and 10% on the first sale of other fans you refer, you can also declare yourself as a loyal fan of any " +
									"ONE team or ONE artist and every time you purchase tickets for that ONE team or ONE artist&ndash; you will receive " +
									"a 10% discount.</p><p align="+style+">&nbsp;</p><p align="+style+"><font color="+justColor+"><strong>How to use your " +
									"points: </strong></font></p><p align="+style+"><strong>Reduce Your Purchase Price-</strong> Use your reward points " +
									"to lower the cost of any future purchase. For example, you purchase $300 worth of tickets and you have 100 Reward " +
									"Points, you can apply those 100 Reward points and we REWARD YOU by reducing you cost to $200 for the same tickets.</p>" +
									"<p align="+style+"><strong>Free Tickets To Fantasy Events&ndash;</strong>&nbsp;To see your favorite teams in the " +
									"Super Bowl. Here&rsquo;s how it works - spend 200 of your Reward Points today on the dream of seeing your favorite " +
									"team make it to the Super Bowl and if they actually make it to the game, we will REWARD YOU with 2 FREE TICKETS! If " +
									"they do not make it, you pay nothing and your Rewards account was simply reduced &nbsp;by 200 points");*/
					
					
					RTFProductInfo.setDescription("<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>HOW TO EARN REWARD DOLLARS</strong></font></p>" +
					"<p align="+style+">"+fontTagStartStr+"<strong>Purchase Tickets&ndash;</strong> Every fan earns 10% in reward dollars on ALL purchases."+fontTagEndStr+"</p>"+
					"<p align="+style+">"+fontTagStartStr+"<strong>Refer a Friend&ndash;</strong> Earn 10% on the first purchase of every fan you refer."+fontTagEndStr+"</p>"+

					"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>HOW TO USE YOUR REWARD DOLLARS</strong></font></p>"+

					"<p align="+style+">"+fontTagStartStr+"<strong>Discounted Tickets&ndash;</strong> "+
					"Every Reward The Fan dollar is worth $1 towards ANY future purchase! - Use your Reward Dollars to lower your cost. For example, you purchase $300 worth of tickets and you have 100 Reward Dollars, you can apply those 100 Reward Dollars and we REWARD YOU by reducing your cost to $200 for the same tickets."+fontTagEndStr+"</p>"+

					"<p align="+style+">"+fontTagStartStr+"<strong>Fantasy Sports Tickets&ndash;</strong> "+
					"Use the Reward Dollars you earned to secure a <strong>FANTASY SPORTS TICKET</strong> to see your favorite team make it to events like the Super Bowl.If your team goes, so do you! If your team does not make it, we return your reward dollars to your rewards account."+fontTagEndStr+"</p>"+

					"<p align="+style+">"+fontTagStartStr+"<strong>Fantasy Sports Tickets</strong> gives you, THE FAN, the potential to see your favorite team at the big game for FREE- simply by using our service and referring others who use Reward The Fan as well!"+fontTagEndStr+"</p>"+
					
 					"<p align="+style+">"+fontTagStartStr+"<strong>Fantasy Sports Tickets</strong> may not be purchased for cash and can ONLY be purchased by using your <strong>Reward The Fan</strong> dollars!"+fontTagEndStr+"</p>"+
 					
 					"<p align="+style+">"+fontTagStartStr+"<strong>Reward The Fan</strong> dollars are also not available to purchase or redeem for cash and are only awarded to you, THE FAN, as our way of saying thank you for " +
 							"your loyalty."+fontTagEndStr+"</p>");
					
					//"<p align="+style+">"+fontTagStartStr+"<i>*Reward Points convert from Pending to Available for use within 24 hours after the <u>date of the event you purchased</u>.</i>"+fontTagEndStr+"</p>");
					
					
					
					
					/*RTFProductInfo.setDescription("<html><body><div style="+color+"><b >How to Earn Reward Points :</b>" +
							"</div><div><b>Purchase Tickets -</b> Every fan earns 10% in reward points on ALL purchases." +
							"</div> <div><b>Refer a Friend �</b> Earn 10% on the first purchase of every fan you refer." +
							"</div> <div><b>Loyal Fans �</b> In addition to receiving 10% in rewards points on all " +
							"purchases and 10% on the first sale of other fans you refer, you can also declare yourself as a loyal fan of any ONE team or " +
							"ONE artist and every time you purchase tickets for that ONE team or ONE artist� you will receive a 10% discount." +
							"</div> &nbsp;&nbsp;&nbsp;&nbsp;<div style="+color+"><b >How to use your points :</b></div> " +
							"" +
							"<div><b>REDUCE YOUR PURCHASE PRICE -</b> Use your reward points to lower the cost of any future purchase. " +
							"For example, you purchase $300 worth of tickets and you have 100 Reward Points, you can apply those 100 Reward " +
							"points and we REWARD YOU by reducing you cost to $200 for the same tickets. </div> " +
							"<div><b>FREE TICKETS TO CROWN JEWEL EVENTS �</b> to see your favorite teams in the Super Bowl. Heres how it works - " +
							"spend 200 of your Reward Points today on the dream of seeing your favorite team make it to the Super Bowl and if they " +
							"actually make it to the game, we will REWARD YOU with 2 FREE TICKETS! " +
							"If they do not make it, you pay nothing and your Rewards account was simply reduced  by 200 points.</div> " +
							"</body></html>");*/
				
			}else if(pageType.equals("termsOfUse")){
				
				RTFProductInfo.setDescription("<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Terms & Conditions</strong></font></p>" +
						"<p align="+style+">"+fontTagStartStr+"Please read this Sales Terms and Conditions Agreement (T&C) before registering with Reward The Fan as it will govern your use of Rewardthefan.com, our mobile applications, our call center or any other Reward The Fan offerings. By accessing or using any part of the Reward The Fan offerings, you agree to be legally bound and abide by this agreement or any other policies/agreements we post on our offerings, which will collectively constitute our agreement with you. If you do not agree with any part of this agreement, please do not use any Reward the Fan offering."+fontTagEndStr+"</p>"+
						"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Tickets</strong></font></p>" +

						"<p align="+style+">"+fontTagStartStr+"<strong>All Sales are Final</strong>&nbsp;-&nbsp;"+
						"There are no refunds, exchanges, or cancellations on any ticket request once it is submitted to us. When you place your ticket request, you have committed that you are buying the tickets. Please order tickets only after you are certain you want them."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Event dates and times</strong>&nbsp;-&nbsp;"+
						"Event dates and times are always subject to change. It is your responsibility to check for any possible changes in date and time. You agree that Reward The Fan is not responsible for changes in date or time of the event, and that refunds will not be issued due to event dates or times being changed."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Pricing Errors</strong>&nbsp;-&nbsp;"+
						"USER acknowledges that on rare occasions when pricing errors will occur, Reward The Fan will not be liable to the customer for this error."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Upgrades</strong>&nbsp;-&nbsp;"+
						"USER acknowledges that Reward The Fan reserves the right to upgrade your Zone at no additional cost to you without notice."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Delivery Methods</strong>&nbsp;-&nbsp;"+
						"USER acknowledges that Reward The Fan reserves the right to change the ticket delivery method of your tickets, at no additional cost to you."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Cancelled and Postponed Events&nbsp;-&nbsp;</strong> "+
						"Reward The Fan will refund the full purchase price, including delivery charges, for canceled events. To qualify for a refund, you must return your tickets to Reward The Fan within 10 days of notice from Reward The Fan that the event is deemed 'Cancelled'. No refunds will be given without the original tickets, unless otherwise determined by Reward The Fan, in its sole discretion.<br/>"+
						"Reward The Fan will determine when an event is canceled based upon the best information available. Postponed or rescheduled events will not be refunded.<br/>"+
						"Event date, times, venue and subject matter may change and we are not always notified if a show is postponed, rescheduled or canceled. It is the buyer's responsibility to monitor the event and to confirm any changes to the event with the entity putting on the event. In certain instances, a venue, promoter, or any entity putting on the event will require a ticket holder to relocate his or her seat. Reward The Fan shall not be held responsible for any such change and will not be obligated to provide a refund or any other compensation. Reward The Fan, in its sole discretion, may cancel orders for a postponed event for any reason, including but not limited to a situation where the event may materially change when rescheduled"+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Lost, Stolen, or Damaged Tickets&nbsp;-&nbsp;</strong> "+
						"USER acknowledges that once Reward The Fan delivers you your tickets, you are responsible to keep them safe.<br/>"+
						"Reward The Fan is not responsible for lost, stolen, damaged, destroyed tickets and will not refund your order if you cannot locate your tickets.<br/>"+
						"Please remember to bring your tickets with you to your event as Reward The Fan will not be responsible for you forgetting your tickets and will not be able to issue any reprints.<br/>"+
						"Many venues require PDF tickets to be printed and will not accept them in any other form."+fontTagEndStr+"</p>"+
						
						"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>General Terms and Conditions</strong></font></p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Intellectual Property&nbsp;-&nbsp;</strong> "+
						"USER acknowledges that all the content, organization, graphics, software, seating charts, design, compilation and 'look and feel' of any Reward The Fan Offerings, including the information,<br/>"+ 
						"are all the intellectual property of Reward The Fan.<br/>"+
						"USER agrees that you will not modify, adapt, translate, prepare derivative works from, decompile, reverse-engineer, disassemble, or otherwise attempt to derive source code from any Reward The Fan offering and you will not remove, obscure, or alter Reward The Fan's trademarks or other proprietary rights notices affixed to, contained within, or accessed in conjunction with or by any of Reward The Fan offerings.<br/>"+
						"Use of the Site and other Reward The Fan Offerings and the information and content available on the Reward The Fan Offerings are protected by copyright laws throughout the world."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>Comments, Feedbacks and Suggestions&nbsp;-&nbsp;</strong> "+
						"USER acknowledges that any comments, feedback, suggestions, ideas, pictures, video etc.(Comments) disclosed, submitted or offered to Reward The Fan, shall remain the exclusive property of Reward The Fan and may be used by Reward The Fan in any medium and for any purpose without obtaining your specific consent. Reward The Fan is not under any obligation to maintain your name or Comments in confidence or to pay to you any compensation for any Comments submitted, or to respond to any of your Comments. You agree that you will be solely responsible for the content of any Comments you make."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Collection&nbsp;-&nbsp;</strong> "+
						"USER acknowledges that if you authorize on your device, we access geo-location data from your mobile device to provide you a customized experience. We also access your mobile device's music library and collect any other music preference information you provide to offer personalized recommendations on upcoming events and a customized user experience."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>Protection&nbsp;-&nbsp;</strong> "+
						"We use several security and anti-fraud measures to protect your information to reduce the risks of loss or misuse."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Limitation on Liability&nbsp;-&nbsp;</strong> "+
						"USER acknowledges that under no circumstance will Reward The Fan be liable to you for more than the amount received by Reward The Fan as a result of your use of any Reward The Fan offerings.<br/>"+
						"Except in jurisdictions where such provisions are restricted, in no event will Reward The Fan be liable to a USER for any indirect, vicarious, consequential, exemplary, incidental, special or punitive damages, including lost profits, even if Reward The Fan has been advised of the possibility of such damages. Some jurisdictions do not allow the disclaimer of warranties or the exclusion or limitation of incidental or consequential damages, so the above limitation may not apply to USER."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>Indemnification&nbsp;-&nbsp;</strong> "+
						"USER agrees to indemnify, defend and hold Reward The Fan, its parents, affiliates, licensors, suppliers, advertisers and sponsors, and their respective employees, consultants, agents and other representatives ('Indemnified Parties') harmless from and against any and all claims, damages, losses, costs (including reasonable attorneys' fees) and other expenses that arise directly or indirectly out of or from: (a) your breach of any of these terms and conditions; (b) any allegation that any information you submit or transmit to the site infringe or otherwise violates the copyright, trademark, trade secret or other intellectual property or other rights of any third party; (c) any federal, state, or county tax obligation or amounts due or owing under any tax regulation, law, order or decree or any dispute concerning the tax status of Reward The Fan; and/or (d) your activities in connection with your use of this site."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Termination or Suspension&nbsp;-&nbsp;</strong> "+
						"Reward The Fan may terminate or suspend your right to use Reward The Fan Offerings at any time for any or no reason and will notify you after taking any such action."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Dispute Resolution&nbsp;-&nbsp;</strong> "+
						"All claims and disputes (excluding claims for injunctive or other equitable relief as set forth below) in connection with this Agreement or the use of any Reward The Fan offering that cannot be resolved informally or in small claims court shall be resolved by binding arbitration on an individual basis under the terms of this arbitration agreement.<br/>"+ 
						"Unless otherwise agreed, all arbitration proceedings will be held in English. This arbitration agreement applies to you and Reward The Fan, and to any subsidiaries, affiliates, agents, employees, predecessors in interest, successors, and assigns, as well as all authorized or unauthorized users or beneficiaries of Reward The Fan offerings provided under this Agreement.<br/>"+
						"Before either party may seek arbitration, the party must first send to the other party a written Notice of Dispute ('Notice') describing the nature and basis of the claim or dispute, and the requested relief. If you and Reward The Fan do not resolve the claim or dispute within thirty (30) days after the Notice is received, either party may begin an arbitration proceeding.<br/>"+
						"Arbitration shall be initiated through the American Arbitration Association ('AAA'), an established alternative dispute resolution provider ('ADR Provider') that offers arbitration as set forth in this section. If AAA is not available to arbitrate, the parties shall agree to select an alternative ADR Provider. The rules of the ADR Provider shall govern all aspects of the arbitration, including but not limited to the method of initiating and/or demanding arbitration, except to the extent such rules are in conflict with this Agreement. <br/>"+
						"The arbitration shall be conducted by a single, neutral arbitrator. Any claims or disputes where the total amount of the award sought is less than Ten Thousand U.S. Dollars (US $10,000.00) may be resolved through binding non-appearance-based arbitration, at the option of the party seeking relief. For claims or disputes where the total amount of the award sought is Ten Thousand U.S. Dollars (US $10,000.00) or more, the right to a hearing will be determined by the Arbitration Rules. Any hearing will be held in a location within 100 miles of New York City. Any judgment on the award rendered by the arbitrator may be entered in any court of competent jurisdiction. Each party shall bear its own costs (including attorneys' fees) and disbursements arising out of the arbitration and shall pay an equal share of the fees and costs of the ADR Provider.<br/>"+
						"Time Limits. If you or Reward The Fan pursue arbitration, the arbitration action must be initiated and/or demanded within the statute of limitations (i.e., the legal deadline for filing a claim) and within any deadline imposed under the AAA Rules for the pertinent claim.<br/>"+
						"Waiver of Jury Trial. THE PARTIES HEREBY WAIVE THEIR CONSTITUTIONAL AND STATUTORY RIGHTS TO GO TO COURT AND HAVE A TRIAL IN FRONT OF A JUDGE OR A JURY, instead electing that all claims and disputes shall be resolved by arbitration under this arbitration agreement. Arbitration procedures are typically more limited, more efficient and less costly than rules applicable in a court and are subject to very limited review by a court. In the event any litigation should arise between you and Reward The Fan in any state or federal court in a suit to vacate or enforce an arbitration award or otherwise, YOU AND Reward The Fan WAIVE ALL RIGHTS TO A JURY TRIAL, instead electing that the dispute be resolved by a judge.<br/>"+
						"Waiver of Class or Consolidated Actions. ALL CLAIMS AND DISPUTES WITHIN THE SCOPE OF THIS ARBITRATION AGREEMENT MUST BE ARBITRATED OR LITIGATED ON AN INDIVIDUAL BASIS AND NOT ON A CLASS BASIS, AND CLAIMS OF MORE THAN ONE CUSTOMER OR USER CANNOT BE ARBITRATED OR LITIGATED JOINTLY OR CONSOLIDATED WITH THOSE OF ANY OTHER CUSTOMER OR USER.<br/>"+
						"Confidentiality. All aspects of the arbitration proceeding, including but not limited to the award of the arbitrator and compliance therewith, shall be strictly confidential. The parties agree to maintain confidentiality unless otherwise required by law. This paragraph shall not prevent a party from submitting to a court of law any information necessary to enforce this Agreement, to enforce an arbitration award, or to seek injunctive or equitable relief.<br/>"+
						"If any part or parts of this arbitration agreement are found under the law to be invalid or unenforceable by a court of competent jurisdiction, then such specific part or parts shall be of no force and effect and shall be severed and the remainder of the Agreement shall continue in full force and effect.<br/>"+
						"This arbitration agreement will survive the termination of your relationship with Reward The Fan.<br/>"+
						"Notwithstanding the foregoing, either you or Reward The Fan may bring an individual action in small claims court.<br/>"+
						"Notwithstanding the foregoing, either party may seek emergency equitable relief before a state or federal court in order to maintain the status quo pending arbitration. A request for interim measures shall not be deemed a waiver of any other rights or obligations under this arbitration agreement.<br/>"+
						"Courts. In any circumstances where the foregoing arbitration agreement permits the parties to litigate in court, the parties hereby agree to submit to the exclusive personal jurisdiction of the courts located within New York County, New York, for such purpose."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Changes in Terms and Conditions&nbsp;-&nbsp;</strong> "+
						"Reward The Fan reserves the right, in its sole discretion, to change these Terms at any time. If Reward The Fan changes any term or condition, said modification, revision and additional information shall be posted here and shall automatically replace the terms and conditions and become binding on all users of this site. Your continued use of the site following Reward The Fan's posting of revised terms and conditions constitute your acceptance of the revised agreement."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Force Majeure&nbsp;-&nbsp;</strong> "+
						"Reward The Fan shall not be deemed in default or otherwise liable under these Terms due to its inability to perform its obligations by reason of any act of nature such as, fire, earthquake, blizzard, flood, epidemic, accident, explosion, casualty, strike, lockout, labor controversy, riot, civil disturbance, act of public enemy, embargo, war, any law ordinance or regulation, legal order (unless caused by Reward The Fan's default hereunder), any failure or delay of any transportation, power, or communications system or any other similar cause not under Reward The Fan's control."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Release&nbsp;-&nbsp;</strong> "+
						"You hereby release Reward The Fan and its successors from claims, demands, any and all losses, damages, rights, and actions of any kind, including personal injuries, death, and property damage, that is either directly or indirectly related to or arises from your use of the reward The Fan offerings, including but not limited to, any interactions with or conduct of other users or third-party websites of any kind arising in connection with or as a result of this Agreement or your use of the Reward The Fan Offerings. If you are a California resident, you hereby waive California Civil Code Section 1542, which states, 'A general release does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the release, which, if known by him must have materially affected his settlement with the debtor."+fontTagEndStr+"</p>");
						
			}else if(pageType.equals("RTFRules")){
				
				RTFProductInfo.setDescription(
						"<p align="+style+">"+fontTagStartStr+"<strong>Last Updated on Dec 15, 2020</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>NO PURCHASE NECESSARY TO ENTER OR WIN. MAKING A PURCHASE OR PAYMENT OF ANY KIND WILL NOT INCREASE YOUR CHANCE OF WINNING A PRIZE. VOID WHERE PROHIBITED BY LAW.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>THIS CONTEST IS IN NO WAY SPONSORED, ENDORSED, ADMINISTERED BY, OR ASSOCIATED WITH APPLE INC. OR GOOGLE LLC.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>ALL APPLICABLE FEDERAL, STATE AND LOCAL LAWS APPLY. WINNER IS RESPONSIBLE FOR ALL FEDERAL, STATE, AND LOCAL TAXES PAYABLE ON THE VALUE OF THE PRIZE.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"The Reward the Fan App Games Official Rules (the �Rules�) are intended to provide you (�Entrant�) with further information regarding your participation in the Reward the Fan App Games by playing any such Game(s) (�Game(s)� or each individually, a �Game�). Sponsor�s Reward the Fan Terms of Use (located at <a href=\"http://www.rewardthefan.com/Terms\">http://www.rewardthefan.com/Terms</a>) which govern a user�s use of, and access to, Sponsor�s mobile application called \"Reward The Fan\" (the \"<strong>App</strong>\") and Sponsor�s corresponding website, are expressly incorporated in these Official Rules, as if set forth fully herein."+fontTagEndStr+"</p>"+
						//"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Tickets</strong></font></p>" +

						//"<p align="+style+">"+fontTagStartStr+"<strong>Elgibilty</strong>&nbsp;-&nbsp;"+
						//"<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Elgibilty</strong></font></p>" +
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;1.&nbsp;Eligibility.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Participation in the Reward the Fan App Game(s) is open to legal residents of the fifty (50) United States and the District of Columbia, and Canada (excluding Quebec) who are at least thirteen (13) years old at the time of entry (minors must have parental/guardian permission). Void where prohibited or restricted by law. Employees, officers and directors of Sponsor, and of their subsidiaries, divisions, affiliates, and advertising or promotional agencies or individuals involved with the design, production, execution or distribution of the Game(s), and the immediate family members and household members of such individuals, are not eligible to enter or win. \"Immediate family members\" shall mean parents, stepparents, children, stepchildren, siblings, stepsiblings, or spouses, regardless of where they live. \"Household members\" shall mean people who share the same residence at least three months a year, whether related or not. To participate in the Game(s) and/or to receive a prize, you must fully comply with these Official Rules and you represent and warrant that you agree to be bound by these Official Rules and the decisions of the Sponsor, whose decisions shall be binding and final in all respects relating to the Game(s). In the event a potential prize winner is found to be ineligible to enter the Game(s) or receive a prize, such potential prize winner may be disqualified from the Game(s), at Sponsor's sole discretion."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Eligibility Verification</strong>&nbsp;-&nbsp;"+
						"Sponsor reserves the right to verify Entrant's eligibility before, during, or after the Game(s)."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;2.&nbsp;Sponsor.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Sponsor: Reward the Fan, LLC, 159-16 Union Turnpike Suite 212, Fresh Meadows, NY  11366."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;3.&nbsp;Let the Game(s) Begin! When to Play.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"The Reward the Fan App Game(s) program is offered until terminated by Sponsor. Each Game goes live at the corresponding announced Eastern Time zone time, and entry is open until the time period for the first question in that Game has expired (\"Game Entry Period\"). Each Game will end at the conclusion of the time period allotted for the final question (\"Game Period\"). Sponsor is the official time keeper of the Game(s). Sponsor may terminate this program at any time and/or any Game(s) at any time."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"There are two methods by which Sponsor will announce when a Game Entry Period is scheduled to commence:"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;(i)&nbsp;Posting the scheduled Game start time within the Reward the Fan App (the \"App\") or Website; and/or "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;(ii)&nbsp;Sending push notifications of the scheduled Game start time to registered users through the App. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>Late to the Game?</strong> If a registered user logs into the App while a Game is live, but after the answer period for the first question has expired, the registered user will not be able to play the applicable Game. Registered users may, however, view the Game(s) without entering the applicable Game(s). In that case, the registered user(s)'s use of the App will be governed by the Reward the Fan <a href=\"http://www.rewardthefan.com/Terms\">Terms of Use</a> and <a href=\"http://www.rewardthefan.com/PrivacyPolicy\">Privacy Policy</a>.  "+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;4.&nbsp;How to Enter and Play.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"To become an Entrant for and to play the Game(s), users of the App must successfully complete the following steps:"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;1.&nbsp;Download or open the App through the Apple App Store or Google Play Store on user's authorized mobile device. There is no cost to download the App or play the Game(s).  "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;2.&nbsp;Become a registered user by creating a Reward the Fan Account by providing the information requested, including a verified phone number, and following and completing all of the prompts.   "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;3.&nbsp;Open the App when you have been notified that a Game is about to begin."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;4.&nbsp;The instructions for each Game will be stated at the outset of the applicable Game. Once a Game is live, the Sponsor designated host (the \"Host\") will present a series of multiple choice trivia questions. Entrants must timely begin playing the applicable Game."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;5.&nbsp;Follow the Host's instructions by timely answering the multiple choice trivia questions. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;6.&nbsp;Entrants will have approximately ten (10) seconds to answer each question. Entrants who answer the question timely and correctly will move on to the next round. Entrants who do not answer timely and/or who select an incorrect answer will be eliminated from the applicable Game."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.&nbsp;If an Entrant timely and correctly answers every question in the Game, Entrant will be notified in the App that he/she is a potential winner, subject to verification."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>Limits:</strong> Limit one entry per person per account per email address and per Game Entry Period."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;5.&nbsp;Game Cancellations.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Sponsor reserves the right to postpone, suspend, or cancel any Game(s) for any reason or no reason at all, at Sponsor's sole discretion, without any limitations or restrictions, including if or when viruses, bugs or other causes beyond its control corrupt the administration, security or proper operation of any Game(s). In the event of postponement, suspension, or cancellation, Sponsor will endeavor to promptly post a notice on the App regarding such postponement, suspension, or cancellation."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;6.&nbsp;Prizes.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>Reward Points Prize(s).</strong>  Each verified Game(s) winner may receive a Reward Points prize (each a \"Reward Points Prize Winner\"). Reward Points prizes are credits that may be redeemed by the verified Game winner(s) for value to be applied toward qualifying product or service purchases and offered through Sponsor's App. The Game(s) Host will announce the value of the applicable Reward Points prize at the outset of each Game(s). In the event there are multiple Reward Points Winners for any single Game, the applicable Reward Points prize will be divided evenly amongst the applicable, eligible, Reward Points Prize Winners for such Game. Other restrictions may apply."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>Grand Prize Sweepstakes.</strong>  At the conclusion of each Game Period, each verified Reward Points Prize Winner(s), if any, will also receive one (1) entry into the corresponding Grand Prize Sweepstakes for the applicable Game, and Sponsor will randomly select one (1) verified Grand Prize Sweepstakes potential winner from all such verified Reward Points Prize Winners. Grand Prizes can be products, services or experiences selected in Sponsor's sole discretion. The Host will announce the potential Grand Prize Sweepstakes winner, if any, at the conclusion of the applicable Game Period. The potential Grand Prize Sweepstakes winner will receive a \"pop up\" notification in the App informing him/her that he/she is a potential Grand Prize Sweepstakes winner, and a notification will appear in the \"My Orders\" tab of the potential Grand Prize Sweepstakes winner's App. Each potential Grand Prize Sweepstakes winner must follow and complete the instructions detailed in the \"My Orders\" tab of his/her App within 24 hours of the Host's live announcement of the potential Grand Prize Sweepstakes winner in order to receive the Grand Prize. <strong>Any Grand Prize Sweepstakes Winner who fails to complete the instructions within the given timeframe may forfeit the Grand Prize, at Sponsor's sole discretion. Other restrictions may apply.</strong>. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Sponsor may contact potential and verified winners for any/all prizes directly using the phone number and/or email address such Entrants used to create his/her Reward the Fan Account. All potential winners will be subject to confirmation of the potential winner's eligibility to receive the applicable prize(s), consistent with these Rules and the Reward the Fan <a href=\"http://www.rewardthefan.com/Terms\">Terms of Use</a>.  "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"No cash equivalent for any prize(s), prizes are non-transferable and no substitution will be made except as provided herein at the Sponsor's sole discretion. Sponsor reserves the right but not the obligation to substitute the listed prize(s) for one of equal or greater value for any reason. All federal, state, and local tax liabilities, as well as any other costs and expenses not specified herein as being awarded are the sole responsibility of the winner(s). Winner(s) may be required to complete and return an IRS W-9 form (i.e., Request for Taxpayer Identification Number and Certification). Prize will be awarded only if the potential prize winner(s) fully complies with these Official Rules."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;7.&nbsp;Earning Discount Codes by Participating.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Entrants may also earn Discount Codes by participating in the Game(s), even if they do not win the Game(s) (\"Participation Discount Codes\"). For each trivia question that an Entrant answers timely and correctly within the applicable Game(s), Sponsor will award that Entrant with a Participation Discount Code, whose discount rate will increase the further into the game the Entrant advances. Participation Discount Codes will be credited to Entrant's My Discount Codes section at the conclusion of Entrant's participation in the applicable Game(s)."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;8.&nbsp;In-Game Contests.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Registered users of the Site(s) may also have a chance to play in-game contests during Game(s) (\"In-Game Contests\"). Any such In-Game Contests will be announced by the Host during Game(s) directly prior to the occurrence of such In-Game Contests, along with the prize available and the value thereof (if applicable) for such In-Game Contests. Available prizes for such In-Game Contests may include, but are not limited to, discount codes, Reward Points, gift cards, or products. All prizes for such In-Game Contests will be awarded in connection with these Rules. The winner of any In-Game Contests will receive a \"pop up\" notification in the App informing him/her that he/she is an In-Game Contest winner, and a notification will appear in either the Reward Point, Discount Code, Gift Card or My Orders tab of the In-Game Contest winner's App. Each In-Game Contest winner who is awarded a physical product or service, must follow and complete the instructions detailed in the \"My Orders\" tab of his/her App within 24 hours of the Host's live announcement of the In-Game Contest winner.   <strong>Any In-Game Contest winner who fails to complete the instructions within the given timeframe may forfeit the prize, at Sponsor's sole discretion. Other restrictions may apply.</strong>. "+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;9.&nbsp;Verification of Potential Winners.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>ALL POTENTIAL PRIZE WINNER(S) ARE SUBJECT TO VERIFICATION BY SPONSOR AT ANY TIME AND FOR EACH/ANY PRIZE(S). SPONSOR'S DECISIONS ARE FINAL AND BINDING IN ALL MATTERS RELATED TO THE ADMINISTRATION, OPERATION, AND SELECTION OF THE GAMES, INCLUDING WINNER DETERMINATION.</strong> "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"AN ENTRANT IS NOT A WINNER OF ANY PRIZE, EVEN IF THE GAME(S) SHOULD SO INDICATE, UNLESS AND UNTIL ENTRANT'S ELIGIBILITY AND THE POTENTIAL GAME PLAY HAS BEEN VERIFIED AND ENTRANT HAS BEEN NOTIFIED THAT VERIFICATION IS COMPLETE. SPONSOR WILL NOT ACCEPT SCREEN SHOTS OR OTHER EVIDENCE OF WINNING IN LIEU OF ITS VALIDATION PROCESS. ANY PLAY THAT OCCURS AFTER THE SYSTEM HAS FAILED FOR ANY REASON IS DEEMED A DEFECTIVE PLAY AND IS VOID AND WILL NOT BE HONORED."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Potential winners may be required to complete and return an Affidavit of Eligibility, Release of Liability and Publicity Release (where permitted by law) (collectively, the \"Affidavit\") by the date specified by Sponsor, or an alternate potential winner may be selected. Minors must have a parent or guardian authorize Game(s) play and complete the Affidavit. In the event: (a) a potential winner cannot be reached for whatever reason after a reasonable effort has been exerted or the potential winner notification or Affidavit is returned as undeliverable, (b) a potential winner declines or cannot accept, receive or use the prize for any reason, (c) of noncompliance within any of the aforesaid time periods, (d) a potential winner is found to be ineligible to enter the Game(s) or receive the prize, (e) a potential winner cannot or does not comply with these Official Rules, or (f) a potential winner fails for any reason to fulfill the Affidavit-related obligations, the potential winner shall be disqualified from the Game(s) and an alternate potential winner may be selected, at Sponsor's sole discretion, from among the other eligible entries received. Sponsor reserves the right to modify the notification and Affidavit procedures in connection with the selection of an alternate potential winner, if any."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;10.&nbsp;Entry Conditions and Release.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Each Entrant agrees to: (a) comply with and be bound by these Official Rules and the decisions of Sponsor which are binding and final in all matters relating to the Game(s); (b) defend, indemnify, release and hold harmless the Sponsor, Apple, Inc., and Google LLC, and their respective parent, subsidiary, and affiliated companies and any other person and organization responsible for sponsoring, fulfilling, administering, advertising or promoting the Game(s), and all of their respective past and present officers, directors, employees, agents and representatives (collectively, the \"Released Parties\") from and against any and all claims, expenses, and liability, including but not limited to negligence and damages of any kind to persons or property, and including but not limited to invasion of privacy (under appropriation, intrusion, public disclosure of private facts, false light in the public eye or other legal theory), defamation, slander, libel, violation of right of publicity, infringement of trademark, copyright or other intellectual property rights, property damage, or death or personal injury arising out of or relating to a participant's entry, creation of an entry or submission of an entry, participation in the Game(s), acceptance, possession, attendance at, defect in, delivery of, inability to use, use or misuse of a prize (including any travel or activity related thereto) and/or the broadcast, exploitation or use of entry. Winner acknowledges that all prizes are awarded as-is without warranty of any kind. Entrants are advised that California Civil Code � 1542 provides that: A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM MUST HAVE MATERIALLY AFFECTED HIS SETTLEMENT WITH THE DEBTOR. By entering a Game(s), each Entrant, on behalf of himself/herself and his/her heirs, successors, assigns, agents and representatives, acknowledges that he/she understands the significance and consequences of California Civil Code � 1542 and, to the extent it may be applicable, hereby waives the benefits of its provisions, with the intent that the releases and waivers of liability in these Official Rules shall include claims known or unknown, and unknown and unsuspected. Sponsor reserves the right to modify these Rules in any way at any time. You are encouraged to check back periodically to review these Rules for updates and modifications."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;11.&nbsp;Publicity.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Except where prohibited or restricted by applicable law, Entrant's participation in any Game(s) shall constitute consent for Sponsor and any of its designees to use Entrant's name, screen name, submissions, photograph, likeness, voice, address (city and state) and/or testimonials in all media, in perpetuity, in any manner for publicity purposes; and constitutes consent to disclose Entrant's personally identifiable information to third parties (including, without limitation, placing the winner's name on a winner's list). Entrant shall receive no compensation for the foregoing. Notwithstanding any rights of publicity, privacy or otherwise (whether or not statutory) anywhere in the world, entry in the Game(s) constitutes Entrant's authorization to (i) have Sponsor (and its agents, consultants and employees) photograph, record, tape, film and otherwise visually and audio-visually record Entrant, (ii) use, reproduce, disseminate, alter, edit, dub, modify, distort, add to, subtract from, process and otherwise exploit any results of such activity (including without limitation any manner in which such activity may be recorded or remembered or modified) or derivatives or extensions or imitations thereof in any manner that Sponsor sees fit, in any medium or technology known or hereinafter invented, throughout the universe in perpetuity, including without limitation for illustration, art, promotion, advertising, trade or any other purpose whatsoever; and (iii) have relinquished any right that Entrant may have to examine or approve the completed product or products or the advertising copy or printed matter that may be used in conjunction therewith or the use to which it may be applied."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;12.&nbsp;General Conditions.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Reward the Fan is an independent Game and unless otherwise indicated is not affiliated, sponsored or endorsed by the name of any artist, sports league, or live production. Sponsor and its subsidiaries, affiliates, divisions, partners, representatives, agents, successors, assigns, employees, officers and directors shall not have any obligation or responsibility, including any responsibility to award any prize to Entrants, with regard to: (a) entries that contain inaccurate information or do not comply with or violate these Official Rules; (b) entries, prize claims or notifications that are lost, late, incomplete, illegible, unintelligible, damaged or otherwise not received by the intended recipient, in whole or in part, due to computer, human, technical or other reason or error of any kind; (c) Entrants who have committed fraud or deception in entering or participating in the Game(s) or claiming a prize, including, without limitation, entrants that create multiple accounts, and Entrants that combine or share accounts; (d) telephone, electronic, hardware, software, network, Internet or computer malfunctions, failures or difficulties; (e) any inability of any winner to accept any prize for any reason; (f) if a prize cannot be awarded due to delays or interruptions due to Acts of God, natural disasters, terrorism, weather or any other similar event beyond Sponsor's reasonable control; or (g) any damages, injuries or losses of any kind caused by any prize or resulting from awarding, accepting, possession, use, misuse, loss or misdirection of any prize or resulting from participating in this Game(s) or any promotion or prize related activities. Sponsor reserves the right, in its sole discretion, to disqualify any person or entity it finds to be (a) tampering with the entry process or the operation of the Game(s), or with any platform, including the App and/or website promoting the Game(s); (b) acting in violation of these Official Rules; (c) entering or attempting to enter the Game(s) multiple times through the use of multiple email addresses or the use of any robotic or automated devices to submit entries, or (d) engaging in conduct Sponsor deems, in its sole discretion, to be improper, unfair, fraudulent, or otherwise adverse to the operation of the Game(s), or in any way detrimental to other participants. If Sponsor determines, in its sole discretion, that technical difficulties or unforeseen events compromise the integrity or viability of the Game(s), Sponsor reserves the right to void the entries at issue, and/or terminate the relevant portion of the Game(s), including the entire Game(s), and/or modify the Game(s) and/or award the prize from all eligible entries received as of the termination date."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"In addition to conduct in violation of these Official Rules, improper conduct includes, but is not limited to: "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;o&nbsp;Falsifying personal information, including payment information, required to claim a prize; "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;o&nbsp;Violating eligible payment method terms, including the terms of Reward Points;"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;o&nbsp;Violating any of these Rules, using unauthorized methods such as unauthorized scripts or other automated means;"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;o&nbsp;Tampering with the administration of any Game(s) or trying to in any way tamper with the platforms, technology, website(s), App(s) and/or other programs associated with the Game;"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;o&nbsp;Obtaining other users' and/or Entrants' information and spamming other users and/or Entrants; and any other form of abuse; or"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;o&nbsp;Otherwise violating these Official Rules."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;�&nbsp;Sponsor is in no way prevented, itself or through its designee(s), from informing the relevant authorities, and/or pursuing criminal or civil proceedings in connection with wrongful conduct."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;13.&nbsp;Limitations of Liability.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"The Released Parties are not responsible for: (a) any incorrect or inaccurate information, whether caused by Entrants or Sponsor or others, printing errors or by any of the equipment or programming associated with or utilized in the Game(s); (b) technical failures of any kind, including, but not limited to malfunctions, interruptions, or disconnections in phone lines or network hardware or software; (c) unauthorized human intervention in any part of the entry process or the Game(s); (d) technical or human error in the administration of the Game(s) or the processing of registrations; or (e) any injury or damage to persons or property which may be caused, directly or indirectly, in whole or in part, from Entrant's participation in the Game(s) or receipt or use or misuse of any prize. If for any reason an Entrant's registration is confirmed to have been erroneously deleted, lost, or otherwise destroyed or corrupted, Entrant's sole remedy is another Entry in the Game(s). No more than the stated amount of each prize will be awarded for any reason."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;14.&nbsp;Themed Games.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"From time to time, Sponsor may present a Game(s) with a theme. Without limitation, a theme may be centered on a celebrity, television show, film, or musical. Unless otherwise stated, the  <strong>GAME(S) IS IN NO WAY SPONSORED, ENDORSED, ADMINISTERED BY, OR ASSOCIATED WITH ANY INDIVIDUAL OR ENTITY, OTHER THAN SPONSOR.</strong>. "+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;15.&nbsp;Fraud.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Any person attempting to defraud or in any way tamper with the Game(s) may be prosecuted to the full extent of the law."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;16.&nbsp;Disputes and Arbitration</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>Except where prohibited by law, as a condition of participating in the Game(s), each Entrant agrees that (1) any and all disputes and causes of action arising out of or connected with the Game(s), including but not limited to prize awarded, shall be resolved individually, without resort to any form of class action, and exclusively by final and binding arbitration under the rules of JAMS; (2) the Federal Arbitration Act shall govern the interpretation, enforcement and all proceedings at such arbitration; and (3) judgment upon such arbitration award may be entered in any court having jurisdiction. Under no circumstances will any Entrant be permitted to obtain awards for, and Entrant hereby waives all rights to claim, punitive, incidental or consequential damages, or any other damages, including attorneys' fees, other than Entrant's actual out-of-pocket expenses (i.e., costs associated with participating in the Game(s)), and Entrant further waives all rights to have damages multiplied or increased.</strong>  Entrants further agree that any and all disputes, claims and causes of action arising out of or connected with the Game(s), or any prize awarded shall be resolved individually, without resort to any form of class action. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATIONS OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE MAY NOT APPLY TO YOU. All issues and questions concerning the construction, validity, interpretation and enforceability of these Official Rules, or the rights and obligations of the Entrant and Sponsor in connection with the Game(s), shall be governed by, and construed in accordance with, the laws of the State of New York, without giving effect to any choice of law or conflict of law rules (whether of the State of New York or any other jurisdiction), which would cause the application of the laws of any jurisdiction other than the State of New York."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;17.&nbsp;Entrant�s Personal Information. </strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"Online information, including information collected in the App, is subject to Sponsor's Privacy Policy, which is available at <a href=\"http://www.rewardthefan.com/PrivacyPolicy\">http://www.rewardthefan.com/PrivacyPolicy</a>. "+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;18.&nbsp;Game Results.</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"For names of winners of prizes in excess of $25, send a self-addressed, stamped envelope marked with the applicable entry date to Winner's List - Reward the Fan Games, 159-16 Union Turnpike Suite 212, Fresh Meadows, NY  11366. Requests for a winner's list must be received no later than 60 days from date of the applicable Game."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Questions?</strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"If you have any questions regarding these Rules please contact us at <a href=\"mailto:trivia@rewardthefan.com\">trivia@rewardthefan.com</a>."+fontTagEndStr+"</p>"+
						"");
						
			} else if(pageType.equals("quizTermsOfUse")){
				
				RTFProductInfo.setDescription(

						
						"<p align="+style+">"+fontTagStartStr+
						"The Site(s) is operated by Reward the Fan, LLC (\"us/our/we\" or \"Reward the Fan\"). Reward the Fan provides an online platform and related services that connects buyers and sellers of goods and services and facilitates the sale of goods and services between the two, in addition to operating a live App trivia game show (collectively, the \"Services\"). All rights are hereby reserved with respect to our service marks, trademarks, logos, and trade dress (\"Marks\"). \"Site(s)\" means the Site(s) located at <a href=\"http://www.rewardthefan.com\">www.rewardthefan.com</a>, and any subsequent URL which may replace any of these Site(s), and any and all associated mobile site(s) and/or application(s), including but not limited to the Reward The Fan App (\"App\"), and all associated website(s), URL(s) and micro site(s) provided by us. \"You/your\" means you as a user of the Site(s). \"User\" means all users of the Site(s). We offer the Site(s), including all information and services available from the Site(s), to you conditioned upon your acceptance of all of the terms, conditions, policies and notices stated herein. The Reward the Fan <a href=\"http://www.rewardthefan.com/Rules\">Official Rules</a> and <a href=\"http://www.rewardthefan.com/PrivacyPolicy\">Privacy Policy</a>  are part of and incorporated into these Terms of Use. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>YOUR CONTINUED USE OF THE SITE(S) CONSTITUTES YOUR AGREEMENT TO THESE TERMS OF USE.</strong> "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"<strong>YOU ACKNOWLEDGE AND AGREE THAT BY ACCESSING OR USING THE SITE(S), OR BY DOWNLOADING OR POSTING ANY CONTENT FROM OR THROUGH THE SITE(S), YOU ARE INDICATING THAT YOU HAVE READ, UNDERSTAND AND AGREE TO BE BOUND BY THESE TERMS.</strong> "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"If there is anything you do not understand, please email any inquiry to <a href=\\\"mailto:info@rewardthefan.com\\\">info@rewardthefan.com</a>. If at any time you do not agree to these Terms of Use, please do not use the Site(s). "+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"YOU SHALL NOT USE THE SITE(S) FOR ANY ILLEGAL PURPOSES, AND YOU WILL USE IT IN COMPLIANCE WITH ALL APPLICABLE LAWS AND REGULATIONS. YOU SHALL NOT USE THE SITE(S) IN A WAY THAT MAY CAUSE THE SITE(S) TO BE INTERRUPTED, DAMAGED, RENDERED LESS EFFICIENT OR SUCH THAT THE EFFECTIVENESS OR FUNCTIONALITY OF THE SITE(S) IS IN ANY WAY IMPAIRED. YOU AGREE NOT TO ATTEMPT ANY UNAUTHORIZED ACCESS TO ANY PART OR COMPONENT OF THE SITE(S)"+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;1.&nbsp;<u>Eligibility.</u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"You represent and warrant that: (i) you are at least eighteen (18) years old, or thirteen (13) years old with parental consent; (ii) you have the right, capacity and authority to be bound by these Terms, and (iii) you will abide by all these Terms."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;2.&nbsp;<u>Intellectual Property Ownership and Use.</u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;2.1&nbsp;You acknowledge and agree that the Marks, copyrights and any and all other intellectual property rights in all material or content contained within the Site(s) shall remain at all times vested in us or, in the cases where we are using such material or content under authority from a third party, in the owner of such material or content."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;2.2&nbsp;We grant you the limited right to access and make use of the Site(s) as our user. However, you shall not: a) reproduce, duplicate, copy, sell or otherwise exploit the Site(s) or any image, page layout, page design, trade dress, trademark, logo or other content (\"Site Content\") for any commercial purpose; b) use a robot, spider or data mining or extraction tool or process to monitor, extract or copy Site Content; c) use any meta tags, search terms, key terms, or the like that contain the Site(s)'s name or our Marks; d) engage in any activity that interferes with the Site(s) or another user's ability to use the Site(s); e) modify, create derivative works from, reverse engineer, decompile or disassemble any technology used to provide the Site(s) and the goods or services offered on the Site(s); or f) assist or encourage any third party in engaging in any activity prohibited by these Terms of Use."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;2.3&nbsp;You shall not use, copy, distribute, or exploit any of the Marks or Site Content in any manner without our prior written permission. You may not link to the Site(s) from any pornographic, obscene, profane, defamatory, libelous, threatening, unlawful or other website or material which could constitute or encourage unlawful conduct that would be considered a criminal offense, give rise to civil liability, or otherwise violate any law or regulation. LINKING TO THE SITE(S) INDICATES THAT YOU ACCEPT THESE TERMS OF USE."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;2.4&nbsp;All Site Content and all materials and content contained within the Site(s), including but not limited to the text, graphics, logos, button icons, images, audio clips, video clips, articles, posts and data compilations appearing on the Site(s), are owned by us, or used by us under authorization, and are protected by U.S. and foreign trademark and copyright laws. No portion of the materials or content on these pages may be reprinted or republished in any form without our express written permission, except as expressly permitted herein."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;2.5&nbsp;These Terms of Use will govern any upgrades or updates provided by us that replace and/or supplement the original Site(s)."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;3.&nbsp;<u>Infringement Notice.</u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;3.1&nbsp;We respect the intellectual property rights of others and require that our users do the same. If you believe your work has been copied in a manner that constitutes copyright infringement, or you believe your rights are otherwise infringed or violated by anything on the Site(s), please notify us by sending an email at the following address:  <a href=\"mailto:dmca@rewardthefan.com\">dmca@rewardthefan.com</a>."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;3.2&nbsp;In order for us to more effectively assist you, the notification must include all of the following:"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;a.&nbsp;A physical or electronic signature of the owner of the right claimed to be infringed or the person authorized to act on the owner's behalf; "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;b.&nbsp;A description of the copyrighted work or other right you claim has been infringed or violated;"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;c.&nbsp;Information reasonably sufficient to locate the material in question on the Site(s);"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;d.&nbsp;Your name, address, telephone number, e-mail address and all other information reasonably sufficient to permit us to contact you;"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;e.&nbsp;A statement by you that you have a good faith belief that the disputed use is not authorized by the rightful owner, its agent or the law; and "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;f.&nbsp;A statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the owner of the right claimed to be infringed or violated or are authorized to act on behalf of the owner. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;3.3&nbsp;Upon notification of potentially infringing material, it is our policy to: (i) remove or disable access to the potentially infringing material; and (ii) notify the user of the potentially infringing material that we have removed or disabled access to the material. We reserve the right but not the obligation to terminate a User's access to the Site(s) upon notice of such infringement(s)."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;3.4&nbsp;If the potentially infringing user believes that he/she has the right to post and use such material, such user may send a \"counter-notice\" to  <a href=\"mailto:dmca@rewardthefan.com\">dmca@rewardthefan.com</a> containing the following information: "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;a.&nbsp;A physical or electronic signature of the potentially infringing user; "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;b.&nbsp;A description of the potentially infringing material that has been removed or access disabled;"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;c.&nbsp;A statement that the user has a good faith belief that the material was removed or disabled as a result of a mistake or a misidentification of the material; and "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;d.&nbsp;User's name, address, telephone number, and, if available, e-mail address and a statement that such person or entity consents to the jurisdiction of the federal court for the judicial district in which the user's address is located, or if the user's address is located outside the United States, for any judicial district in which Reward the Fan is located, and that such person or entity will accept service of process from the person who provided notification of the alleged infringement.   "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;3.5&nbsp;We may send a copy of the counter-notice to the original complaining party informing that party that he/she may elect to withdraw his/her infringement notice within ten (10) business days of the sent date time stamp of our email notifying him/her of same. If such user elects to withdraw his/her infringement notice, the previously removed content may be restored to the Site(s), at our sole discretion. Save for a court order or the existence of related pending civil proceedings, we may decide at our sole discretion to restore the alleged infringing material to the Site(s) at any time. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;3.6&nbsp;For more information or to report an alleged infringement, please contact our Designated Agent at the following address: Reward The Fan, Attn: DMCA Designated Agent, 159-16 Union Turnpike Suite 212, Fresh Meadows, NY  11366 or email: <a href=\"mailto:dmca@rewardthefan.com\">dmca@rewardthefan.com</a>.  "+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;4.&nbsp;<u>Errors and Inaccuracies. </u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;4.1&nbsp;We strive to provide complete, accurate, up-to-date information on the Site(s). Unfortunately, despite those efforts, human or technological errors may occur. The Site(s) may contain typographical mistakes, inaccuracies, or omissions, some of which may relate to pricing and availability of products or the events we promote, and some information may not be complete or current. We reserve the right to correct any errors, inaccuracies or omissions, including after an order has been submitted, and to change or update information at any time without prior notice."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;4.2&nbsp;Force Majeure. We will not be liable to you for any delay or failure to perform any obligation or services related to the Site(s) if the delay or failure results from any cause beyond our reasonable control, including acts of God, labor disputes or other industrial disturbances, systemic electrical, telecommunications, or other utility failures, earthquake, storms or other elements of nature, blockages, embargoes, riots, acts or orders of government, acts of terrorism, or war."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;4.3&nbsp;At times, it may be necessary for us to install updates and/or maintenance to our Software and the Site(s), thereby potentially impacting your access to our Service(s). In this case, we will use commercially reasonable efforts to provide notice to you of said interruptions. "+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;5.&nbsp;<u>Changes to Site(s) or These Terms of Use. </u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;5.1&nbsp;Other than as may be required by law, we reserve the right to modify or remove, temporarily or permanently, the Site(s) (or any part thereof) with or without notice to you. You confirm that we shall not be liable for any modification or removal of the Site(s) or any portion of it. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;5.2&nbsp;We may alter these Terms of Use from time to time, and your use of the Site(s) (or any part thereof) following such change(s) shall constitute your acceptance of such change(s). It is your responsibility to review these Terms of Use regularly. If you do not agree to any change to the Terms of Use, you must immediately stop using the Site(s)."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;5.3&nbsp;The Site(s) is subject to constant change. You will not be eligible for any compensation in the case you cannot use any part of the Site(s), or because of a failure, suspension or withdrawal of all or part of the Site(s) and/or Service(s)."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;6.&nbsp;<u>Third Party Content. </u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"The Site(s) may contain links to other websites and other content, including without limitation graphics and photographs, that are owned and/or operated by third parties (\"Third Party Content\"). We are not responsible for the availability of any Third Party Content. Unless expressly noted, we do not endorse any third parties or Third Party Content. You acknowledge that we are not liable, directly or indirectly, for any claims arising out of Third Party Content, including without limitation defamation, misrepresentation, false advertising, economic loss, and offense."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;7.&nbsp;<u>Account Registration </u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.1&nbsp;In order to list or sell goods and services through the Site(s) you must register an account with Reward the Fan. To register, you must be at least eighteen (18) years old, or thirteen (13) old with parental consent, to enter into and form a legally binding contract. In addition, you must be in good standing and not an individual that has been previously barred from receiving Reward the Fan's Service(s) under the laws and statutes of the United States or other applicable jurisdiction, or in our sole discretion. We reserve the right to deny or terminate registration or membership for the Site(s) for any reason in our sole discretion. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.2&nbsp;When you set up an account, you are the sole authorized user of your account. You shall be responsible for maintaining the secrecy and confidentiality of your password and for all activities that transpire on or within your account. It is your responsibility for any act or omission of any user(s) that access your account information that, if undertaken by you, would be deemed a violation of these Terms of Use. It shall be your responsibility to notify us immediately if you notice any unauthorized access or use of your account or password or any other breach of security. We shall not be held liable for any loss and/or damage arising from any failure to comply with any portion or condition of these terms."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.3&nbsp;When you register to use and access to the Site(s) and Service(s), you must provide accurate and complete information and keep your account information updated. If you provide information that is not current or information that is incomplete or inaccurate, or if we have reasonable grounds to suspect the information you provided was not current, accurate or complete, we may deny you access to the Site(s) and/or Services(s), disqualify you, cancel the purchase or sale of goods or services and/or terminate your account, at our sole discretion. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.4&nbsp;You will be asked to select a username when registering your account. You acknowledge that you will not select or use a username with the intent to impersonate a third party or use a username subject to any rights of a person without appropriate authorization. If you register your account with a username that we, at our sole discretion, deem to be inappropriate, obscene, vulgar or offensive, we reserve the right to revoke your access to the Site(s)."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.5&nbsp;You are prohibited from ever using a third party's registration or account information without first obtaining permission from said third party, and you must notify us immediately of any change in your eligibility to use the Site(s), any breach of security, or any unauthorized use of your account. You may only create one account. For clarity, users may not share ownership in accounts. If we determine that you have opened, maintained, used or controlled more than one account, in addition to any other rights we may have, we reserve the right to suspend or terminate any or all of your accounts and terminate, withhold or revoke the awarding of any prizes and/or refund any goods or services sold or purchased."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.6&nbsp;When you register, we may collect information such as your name, e-mail address, and phone number. You can edit your account information, except your user ID, at any time. Once you register on the Site(s) and sign into our Services, you are no longer anonymous to us. By registering for Reward the Fan, you agree to receive automated promotional text messages and WhatsApp messages from or on behalf of Reward the Fan. You are not required to provide this consent as a condition of purchasing any property, goods, or services. Standard messaging rates may apply."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;7.7&nbsp;As a member of the Site(s), you may cancel or terminate your account, associated email address and/or access to our Services by submitting a cancellation or termination request to <a href=\"mailto:info@rewardthefan.com\">info@rewardthefan.com</a>. You agree that Reward the Fan may, without any prior written notice, immediately suspend, terminate, discontinue and/or limit your account, any email associated with your account, and access to any of our Services."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;8.&nbsp;<u>Discount Codes, Reward Point Transactions and Other Prizes.  </u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.1&nbsp;Registered Users of the Site(s) may have a chance to win Discount Codes & Reward Points by playing the Reward the Fan App Trivia Game. Registered Users may redeem said Discount Codes & Reward Points for goods and services on the Reward the Fan Marketplace. We do not own the goods or services available on our Site(s).  "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.2&nbsp;<strong>Delivery of Goods and Services.</strong>  In most cases, the goods and services will be delivered promptly upon redemption of Discount Codes and Reward Points, either via mail or e-mail. In any case that the goods and services are not available for delivery at the time of the Discount Code or Reward Point redemption transaction, Reward the Fan will email the Registered User notifying him/her when his/her goods and services are available. In some cases, the goods and services may have a delayed delivery. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.3&nbsp;<u><strong>ALL TRANSACTIONS ARE FINAL.</strong></u>. THERE ARE NO REFUNDS, EXCHANGES, OR CANCELLATIONS ON ANY TRANSACTION ONCE IT IS SUBMITTED TO US. PLEASE REDEEM YOUR DISCOUNT CODES AND REWARD POINTS FOR THE GOODS AND SERVICES <u><strong>ONLY</u></strong> AFTER YOU ARE CERTAIN YOU WANT THEM."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.4&nbsp;You acknowledge that if or when pricing errors occur, Reward the Fan will not be liable to you for this error."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.5&nbsp;You acknowledge that we reserve the right to change the method of delivery for the goods and services you purchased and or redeemed without notice, at no additional cost to you."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.6&nbsp;Delivery dates and times are always subject to change. It is your responsibility to check for any possible changes on the status of your delivery. You agree that we are not responsible for changes to the date or time of the delivery and that refunds will not be issued due to delivery delays."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.7&nbsp;Your receipt of an electronic or other form of order confirmation does not signify our acceptance of your order, nor does it constitute confirmation of our offer to provide goods and services to you in return for Reward Points. We reserve the right at any time after receipt of your order to accept or decline your order for any reason or to supply less than the quantity you ordered of any item."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;8.8&nbsp;Registered Users of the Site(s) may also have a chance to play in-game contests when playing the Reward the Fan App Shoppable Trivia Game. Any such in-game contests and corresponding prizes will be announced during the Reward the Fan App Shoppable Trivia Game and all prizes will be awarded in connection with these Terms of Use. "+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;9.&nbsp;<u>Returns</u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;9.1&nbsp;In the event that we cancel your order, we will reinstate the full amount of Reward Points redeemed for orders we cancel. We will determine, at our sole discretion, if and when an order is canceled \"canceled\" for the purpose of issuing refunds. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;9.2&nbsp;In order to qualify for and receive a return of Reward Points, users who redeemed goods or services must return the physical goods purchased, if any, to Reward the Fan by following the instructions and prompts contained in the notice email. In any event, returned goods must be postmarked no later than ten (10) days after the sent date timestamp of Reward the Fan's email notifying user of his/her right to a refund. In the case that no physical goods were shipped, users seeking a refund must follow the instructions and prompts contained in the notice email."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;10.&nbsp;<u>Lost or Stolen Goods.</u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"We are not responsible for lost or stolen goods and you will not be eligible for a refund in the case the goods you purchased or redeemed are lost or stolen. "+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;11.&nbsp;<u>User Generated Content.  </u></strong>"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.1&nbsp;The Site(s) contains or may contain various interactive portions, such as a user forum, message board or other types of interactive features that allow users to post content on our Site(s) (\"Online Services\"). We have no obligation to offer and/or actively monitor the Online Services, but we reserve the right to do so. We are not responsible for, nor do we vouch for the accuracy of, the content of any user comments or other content that may be posted or uploaded by a user. User comments and other content posted or uploaded by a user (\"User Content\") express the views and opinions of the user and do not necessarily reflect our views or opinions. We reserve the right, in our sole discretion, to edit, delete, or refuse to post User Content, for any reason whatsoever. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.2&nbsp;If you believe that any User Content is inaccurate or objectionable, you may contact us by sending an email to  <a href=\"mailto:info@rewardthefan.com\">info@rewardthefan.com</a>. and include the words \"OBJECTIONABLE CONTENT\" in the subject. Please provide us with detailed information about the nature and location of the alleged objectionable material so that we may easily locate and investigate the same. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.3&nbsp;By using the Site(s), you agree that:"+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;a.&nbsp;You will not upload, post, email or otherwise transmit any material or other content that: (i) is defamatory, libelous, disruptive, threatening, invasive of a person's privacy, harmful, abusive, harassing, obscene, hateful, or racially, ethnically or otherwise objectionable; or that otherwise violates any law; (ii) contains software viruses or any other computer codes, files or programs designed to interrupt, destroy, or limit the functionality of any computer software or hardware or telecommunications equipment; (iii) infringes any person or entity's intellectual property rights (including but not limited to, patent, trademark, trade secret, copyright or other intellectual property right). "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;b.&nbsp;You will not impersonate any person or entity or otherwise misrepresent your affiliation with a person or entity."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;c.&nbsp;You will not repeatedly post the same or similar message (\"flooding\") or post excessively large or inappropriate images or content. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;d.&nbsp;You will not distribute or publish unsolicited promotions, advertising or solicitations for funds, goods or services, including but not limited to, junk mail, spam and chain letters."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.4&nbsp;We do not guarantee that any User Content will be displayed on the Site(s), and we reserve the right, but do not have any obligation to: (i) remove, edit or modify or otherwise manipulate any User Content in our sole discretion, at any time, without notice to you and for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such User Content, or if we are concerned that you may have violated these Terms of Use), or for no reason at all; and (ii) to remove or block any User Content from the Site(s)."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.5&nbsp;User Content may become public information, in whole or in part. You should be very careful about posting personally identifiable information such as your name, address, telephone number or email address. If you post personal information online, you may receive unsolicited messages from other users in return."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.6&nbsp;You are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer. You agree to accept responsibility for all activities that occur under your account or password, regardless of whether such use is authorized by you or not."+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.7&nbsp;If you submit any User Content, you grant to us, and any of our successors, licensees, assigns, and affiliates, a royalty-free, worldwide, perpetual, irrevocable, non-exclusive and fully sub-licensable right and license to use, reproduce, modify, edit, adapt, publish, translate, create derivative works from, distribute, perform and display the User Content, and in any other media, now known or hereafter devised. We may use your User Content for any purposes, including without limitation, to promote the Site(s) and Reward the Fan more generally. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.8&nbsp;You represent and warrant that you have all rights to grant such licenses to us without infringement or violation of any third-party rights, including without limitation, any privacy rights, publicity rights, copyrights, trademarks, contract rights, or any other intellectual property or proprietary rights. "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.9&nbsp;If you choose to communicate or meet with other users of the Site(s), you are doing so at your own risk. We do not, and have no obligation to, verify the identity of or otherwise screen our users for any reason. You acknowledge that there are risks, including the risk of physical harm, when dealing with strangers or people acting under false pretenses. You assume all risks associated with dealing with other users with whom you may come in contact through the Site(s). "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"&nbsp;11.10&nbsp;The Site(s) is meant for users eighteen (18) years old and over, or thirteen (13) years old with parental consent. We will not knowingly allow any user less than eighteen (18) years of age, or thirteen (13) with parental consent, to submit any User Content to our Site(s) "+fontTagEndStr+"</p>"+

			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;12.&nbsp;<u>Comments, Feedbacks and Suggestions </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"You acknowledge that any comments, feedback, suggestions, ideas, pictures, video etc. disclosed, submitted or offered to us (collectively, \"Feedback\"), shall remain our exclusive property that we may use in any medium and for any purpose without obtaining your specific consent. You further acknowledge that we are not under any obligation to maintain your name or Feedback in confidence or to pay to you any compensation for any Feedback you may submit. We are not obligated to respond to any of your Feedback but may choose to do so at our sole discretion. You agree that you will be solely responsible for the content of any Feedback you create and submit."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;13.&nbsp;<u>General Conditions   </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;13.1&nbsp;In connection with your use of the Site(s), you will not: (i) transmit or communicate any data or information that is unlawful, harmful, false, misleading, threatening, abusive, harassing, stalking, defamatory, vulgar, obscene, invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable; (ii) upload or input to the Site(s) any information which contains software viruses, or any computer code, files or programs designed to interrupt, destroy or limit the functionality of the Site(s), any computer software or hardware or telecommunications equipment; (iii) impersonate any person or entity or falsely state or otherwise misrepresent your affiliation with a person or entity; (iv) use the Site(s) for any unlawful or unsafe purposes; (v) forge headers or otherwise manipulate identifiers in order to disguise the origin of any content transmitted to other users; (vi) violate any applicable local, state, federal or international law and any regulations requirements, procedures or policies; (vii) use any automated means, including, without limitation, agents, robots, scripts, or spiders, to access, monitor, or copy any part of the Site(s); (viii) transmit, access or communicate any content that you do not have a right to transmit under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under non-disclosure agreements); (ix) monitor traffic on the Site(s), obtain or accumulate personal information about other users, or collect or store personal data about other users; (x) modify, delete or damage any information contained on the mobile device or personal computer of any other users; (xi) infringe on the rights of any third party, including but not limited to trademark, copyright or the rights of publicity; (xii) use the Site(s) in any manner that in our sole judgment, adversely affects the performance or function of the Site(s) or interferes with the ability of other users to access or utilize the Site(s); or (xiii) undertake any acts not expressly permitted under these Terms of Use. You warrant and represent that you undertake to use the Site(s) only for purposes that are in strict compliance with (a) these Terms of Use and the license granted hereunder, and (b) any applicable law, regulation or generally accepted practices or guidelines in the relevant jurisdictions, and you shall take no actions which would cause us to be in violation of any applicable law, ruling or regulation. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;13.2&nbsp;You may not create, develop, license, install, use, or deploy any third-party software or services to circumvent, enable, modify or provide access, permissions or rights to work around any technical limitations in the Site(s). You may not copy (except as expressly permitted by these Terms of Use) or publish the Site(s) or contents thereof for others to copy, decompile, reverse engineer, disassemble, attempt to derive the source code of, or modify, or create derivative works of the Site(s), or any updates, or part thereof (except as and only to the extent any foregoing restriction is prohibited by applicable law or to the extent as may be permitted by the licensing terms governing use of any open source components included with the Site(s), if any). Any attempt to do so is a violation of our rights. If you breach this restriction, you may be subject to prosecution and damages. You may not use the Site(s) in any manner not specifically authorized hereunder or in any way that is against any applicable laws or regulations."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;13.3&nbsp;We reserve the right to suspend or cease providing access to the Site(s) and the Service(s) offered therein, with or without notice, and we shall have no liability or responsibility to you if we do so."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;14.&nbsp;<u>Disclaimers  </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;14.1&nbsp;WE MAKE NO WARRANTIES, WHETHER EXPRESS OR IMPLIED IN RELATION TO THE ACCURACY OF ANY INFORMATION ON THE SITE(S). THE SITE(S) IS PROVIDED ON AN \"AS IS\" AND \"AS AVAILABLE\" BASIS WITHOUT ANY REPRESENTATION. WE MAKE NO WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, IN RELATION TO THE SITE(S), INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, COMPATIBILITY, SECURITY, ACCURACY, CONDITION OR COMPLETENESS, OR ANY IMPLIED WARRANTY ARISING FROM COURSE OF DEALING OR USAGE OR TRADE. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;14.2&nbsp;WE MAKE NO WARRANTY THAT THE SITE(S) WILL MEET YOUR REQUIREMENTS OR WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT THE SITE(S) OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR BUGS OR ARE FULLY FUNCTIONAL, ACCURATE, OR RELIABLE."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;14.3&nbsp;AS SET FORTH IN THE SITE(S) PRIVACY POLICY, YOU ACKNOWLEDGE THAT WE CANNOT GUARANTEE, AND THEREFORE SHALL NOT BE IN ANY WAY RESPONSIBLE FOR, THE SECURITY OR PRIVACY OF THE SITE(S) AND ANY INFORMATION PROVIDED TO OR TAKEN FROM THE SITE(S) BY YOU."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;15.&nbsp;<u>Limitations on Liability </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"IN NO EVENT WILL REWARD THE FAN, OR ITS OFFICERSM MEMBERS, DIRECTORS, EMPLOYEES, PARENTS, AFFILIATES, SUCCESSORS OR ASSIGNS (THE \"RELEASED PARTIES\"), BE LIABLE TO ANY PARTY FOR ANY ECONOMIC LOSSES (INCLUDING WITHOUT LIMITATION ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL DAMAGES, LOSS OF REVENUES, PROFITS, CONTRACTS, BUSINESS OR ANTICIPATED SAVINGS) OR ANY LOSS OF GOODWILL OR REPUTATION, OR ANY LOSS OR CORRUPTION OF DATA, OR ANY SPECIAL OR INDIRECT OR CONSEQUENTIAL LOSSES ARISING OUT OF YOUR USE OF THE SITE(S); IN ANY CASE WHETHER OR NOT SUCH LOSSES WERE WITHIN THE CONTEMPLATION OF US AT THE DATE ON WHICH THE EVENT GIVING RISE TO THE LOSS OCCURRED. THIS INCLUDES BUT IS NOT LIMITED TO ANY INJURY CAUSED BY ANY VIRUSES, BUGS, HUMAN ACTION OR INACTION OR ANY COMPUTER SYSTEM, PHONE LINE, HARDWARE, SOFTWARE, OR PROGRAM MALFUNCTIONS, OR ANY OTHER ERRORS, FAILURES OR DELAYS IN COMPUTER TRANSMISSIONS OR NETWORK CONNECTIONS, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), OR ANY OTHER LEGAL THEORY"+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;16.&nbsp;<u>Indemnification  </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"BY USING THE SITE(S), YOU AGREE TO INDEMNIFY, DEFEND, AND HOLD HARMLESS THE RELEASED PARTIES AGAINST ALL CLAIMS, LIABILITY, DAMAGES, LOSSES, COSTS AND EXPENSES, INCLUDING LEGAL FEES, SUFFERED BY THE RELEASED PARTIES AND ARISING OUT OF ANY USER CONTENT YOU POST TO THE SITE(S) AND ANY BREACH OF YOUR REPRESENTATIONS AND WARRANTIES OR THESE TERMS OF USE BY YOU OR ANY OTHER LIABILITIES ARISING OUT OF YOUR USE OF THE SITE(S), OR THE USE BY ANY OTHER PERSON ACCESSING THE SITE(S) USING YOUR COMPUTER OR INTERNET ACCESS ACCOUNT. WE RESERVE THE RIGHT TO CONTROL THE DEFENSE OF ANY IDEMNIFIED MATTERS THROUGH COUNSEL OF OUR OWN CHOOSING."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;17.&nbsp;<u>Investigations of Violations of These Terms </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"We may investigate any reported violation of these Terms of Use and take any action that we deem appropriate. Such action may include, but is not limited to, issuing warnings, removing posted content and reporting any activity that we suspect violates any law or regulation to appropriate law enforcement officials, regulators, or other third parties."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;18.&nbsp;<u>State Specific Notices   </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;18.1&nbsp;<u>Notice for California Users</u>. Under California Civil Code Section 1789.3, California Website users are entitled to know that they may file grievances and complaints with: the Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs, in writing at 1625 North Market Blvd., Suite N 112, Sacramento CA 95834, or by telephone at (916) 445-1254 or (800) 952-5210 or by email  at<a href=\"mailto:dca@dca.ca.gov\">dca@dca.ca.gov</a>."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;18.2&nbsp;<u>Notice For New Jersey Users.</u> Any disclaimer, limitation of liability, indemnification or damages provisions contained herein shall apply to New Jersey residents or New Jersey transactions only to the extent permitted by New Jersey law or New Jersey public policy."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;19.&nbsp;<u>Choice of Law, Binding Arbitration Clause </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;19.1&nbsp;These Terms will be interpreted in accordance with the laws of the State of New York and the United States of America, without regard to its conflict-of-law provisions."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;19.2&nbsp;By visiting the Site(s), you agree that, except as otherwise specified herein, the laws of the State of New York without regard to principles of conflict of laws, will govern any dispute of any sort that might arise between us or any of our affiliates regarding your visit and use of the Site(s) and Services. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;19.3&nbsp;We and you agree that in the event of any dispute, the party wishing to address the dispute must contact the other party in writing, including by e-mail, and advise the other party of the dispute in reasonable detail as well as informing the other party of the remedy being sought. We may send you notices via the email address or physical address you provide to us, and all notices to us shall be sent to the following email address <a href=\"mailto:info@rewardthefan.com\">info@rewardthefan.com</a>. and include the words \"DISPUTE NOTICE\" in the subject. The parties shall then make a good faith effort to resolve the dispute before resorting to more formal means of resolution. In the event that the dispute is not resolved through this procedure, the party raising the dispute may proceed to mandatory arbitration as set forth below."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;19.4&nbsp;ANY AND ALL DISPUTES BETWEEN YOU AND US WILL BE RESOLVED BY BINDING ARBITRATION. YOU HEREBY AGREE TO GIVE UP YOUR RIGHT TO GO TO COURT to assert or defend your rights under these Terms of Use, except for matters that may be taken to small claims court. Your rights will be determined by a neutral arbitrator, NOT a judge or jury. You agree that any dispute arising out of or relating to these Terms of Use, including with respect to the interpretation of any provision of these Terms of Use or other agreements between you and us, or concerning the performance or obligations of you and us, shall be resolved by mandatory and binding arbitration submitted to JAMS in accordance with its Commercial Arbitration Rules at the request of either us or you pursuant to the following conditions:"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;a.&nbsp;Place of Arbitration Hearings. Unless you elect to conduct the arbitration by telephone or written submission, an in-person arbitration hearing will conducted at a JAMS facility in your area or at a JAMS facility in New York, New York."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;b.&nbsp;Selection of Arbitrator shall be made pursuant to JAMS' Streamlined Arbitration Rules & Procedures or JAMS' Comprehensive Arbitration Rules & Procedures, depending on the amount of the claim as specified herein."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;c.&nbsp;Conduct of Arbitration. The arbitration shall be conducted by a single neutral arbitrator under JAMS' Streamlined Arbitration Rules & Procedures. For claims exceeding $5,000.00, the arbitration shall be conducted under JAMS' Comprehensive Arbitration Rules & Procedures Subject to the applicable JAMS procedure, the arbitrator shall allow reasonable discovery in the forms permitted by the Federal Rules of Civil Procedure, to the extent consistent with the purpose of the arbitration. The arbitrator(s) shall have no power or authority to amend or disregard any provision of this section or any other provision of these Terms of Use, except as necessary to comply with JAMS' Policy on Consumer Arbitrations Pursuant to Pre-Dispute Clauses Minimum Standards of Procedural Fairness. The arbitration hearing shall be commenced promptly and conducted expeditiously. If more than one day is necessary, the arbitration hearing shall be conducted on consecutive days unless otherwise agreed in writing by the parties. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;d.&nbsp;Findings and Conclusions. The arbitrator(s) shall, after reaching judgment and award, prepare and distribute to the parties written findings of fact and conclusions of law relevant to such judgment and award and containing an opinion setting forth the reasons for the giving or denial of any award. The award of the arbitrator(s) shall be final and binding on the parties, and judgment thereon may be entered in a court of competent jurisdiction."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;e.&nbsp;Costs and Fees. You will be subject to a filing fee, set by JAMS, to initiate the arbitration. To the extent permitted by JAMS procedures, each party shall bear its own costs and expenses and an equal share of the arbitrators' and administrative fees of arbitration, and we will remain responsible for its share of costs, expenses and fees plus any costs, expenses and fees required under JAMS procedures."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;f.&nbsp;Litigation. The Federal Arbitration Act and federal arbitration law apply to these Terms. Either party also may, without waiving any remedy under these Terms of Use, seek from any court having jurisdiction any interim or provisional relief that is necessary to protect the rights or property of that party, pending the establishment of the arbitral tribunal (or pending the arbitral tribunal's determination of the merits of the controversy)."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;19.5&nbsp;Notwithstanding the arbitration clause herein, the parties also agree that Reward the Fan may bring suit in a court located in New York, New York, to enjoin infringement or other misuse of our intellectual property rights."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;20.&nbsp;<u>Class-Action Waiver </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"Any arbitration, claim or other proceedings by or between you and us shall be conducted on an individual basis and not in any class action, mass action, or on a consolidated or representative basis. You further agree that the arbitrator shall have no authority to award class-wide relief or to combine or aggregate similar claims or unrelated transactions. You acknowledge and agree that this agreement specifically prohibits you from commencing arbitration proceedings as a representative of others. If for any reason a claim proceeds in court rather than in arbitration, each party waives any right to a jury trial. Any claim that all or part of this Class Action Waiver is unenforceable, unconscionable, void, or voidable may be determined only by a court of competent jurisdiction and not by an arbitrator."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;21.&nbsp;<u>Miscellaneous   </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;21.1&nbsp;If any part of these Terms of Use shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed to be severable from these Terms of Use and shall not affect the validity and enforceability of any of the remaining provisions of the Terms of Use."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;21.2&nbsp;These Terms as well as our  <a href=\"http://www.rewardthefan.com/PrivacyPolicy\">Privacy Policy</a>, the Reward the Fan  <a href=\"http://www.rewardthefan.com/Rules\">Official Rules</a>, and any other terms or agreements that may be posted on the Site(s) (as may be amended from time to time) (\"Site(s) Agreements\") contain the entire agreement between you and us relating to the Site(s) and your use of the Site(s), and supersede any previous agreements, arrangements, undertakings or proposals, written or oral, between you and us in relation to such matters. No oral explanation or oral information shall alter the interpretation of these Site(s) Agreements. You confirm that, in agreeing to accept these Site(s) Agreements, you have not relied on any representation except insofar as the same has expressly been made a representation in these Site(s) Agreements, and you agree that you shall have no remedy in respect of any representation which has not become a term of these Site(s) Agreements. These Site(s) Agreements will be exclusively governed by and construed in accordance with the laws of the State of New York and, subject to the Arbitration Clause contained herein, the courts located in the City of New York, New York will have exclusive jurisdiction in any dispute, except that we have the right, at our sole discretion, to commence and pursue proceedings in alternative jurisdictions (again subject to the arbitration clause contained herein)."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;21.3&nbsp;To understand our privacy practices, please review our <a href=\"http://www.rewardthefan.com/PrivacyPolicy\">Privacy Policy</a> which governs your visit to this Site, and which is hereby incorporated by reference into these terms."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;21.4&nbsp;You shall not transfer, assign, sublicense nor pledge in any manner whatsoever, any of your rights or obligations under these Terms of Use. We may transfer, assign, sublicense or pledge in any manner whatsoever, any of our rights and obligations under these Terms of Use to a subsidiary, affiliate, or successor thereof or to any third party whatsoever, without notifying you or receiving your consent."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;21.5&nbsp;Our failure to exercise or enforce any right or provision of these Terms of Use shall not constitute a waiver of such right or provision. No independent contractor, partnership, joint venture or employer-employee relationship is intended or created. No action, regardless of form, which arises from or is related in any way whatsoever to these Terms of Use, may be commenced by you more than twelve (12) months after such cause of action accrues. All notices and other communications under the Terms must be in writing and will be deemed to have been duly given when actually received. You may provide notices to us via (i) email to <a href=\"mailto:info@rewardthefan.com\">info@rewardthefan.com</a>, or (ii) by writing to us at Reward The Fan, 159-16 Union Turnpike Suite 212, Fresh Meadows, NY  11366."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"If you have any questions regarding these Terms of Use or the Website, App or Services, please contact us at <a href=\"mailto:info@rewardthefan.com\">info@rewardthefan.com</a>."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"<strong>DATE LAST MODIFIED: Dec 15, 2020</strong>"+fontTagEndStr+"</p>");
						
						
			} else if(pageType.equals("quizPrivacyPolicies")){
				
				RTFProductInfo.setDescription("" +
						"<p align="+style+">"+fontTagStartStr+"Reward The Fan LLC and its subsidiaries and affiliates (individually or collectively �we/our/us� or �Reward the Fan�) respect your privacy and are committed to protecting it through compliance with this policy. Our privacy policy, is designed to explain the information we collect and how we use it to provide our services and give Users a better experience (�Privacy Policy�). This Privacy Policy is part of, and incorporated into, our Terms of Use.  "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"This Privacy Policy describes the type of information we may collect from you and/or that you may provide us when you visit and/or use any Site(s), mobile site(s) and/or app(s) provided by us, including without limitation <a href=\"http://www.rewardthefan.com\">www.rewardthefan.com</a> and the Reward the Fan mobile application (individually or collectively, �Site(s)�). �You/your/user(s)� means you as a user of our Site(s), including any information, tools and services available from the Site(s), conditioned upon your acceptance of all the terms, conditions, policies and notices stated here, which you accept by continuing to utilize the Site(s).  "+fontTagEndStr+"</p>"+
						"<p align="+style+">"+fontTagStartStr+"By accessing and using the Site(s), you consent to our collection, storage, use and disclosure of your personal information and other information as described in this Privacy Policy. "+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong><u>Guide to Contents</u></strong><br/>"+
						"<ol><li>Scope</li><li>Types of Information We Collect</li><li>How We Use Your Information</li>"+
						"<li>How We Share and Disclose Information with Third Parties</li>"+
						"<li>Non-Personally Identifiable Information</li><li>Your Choices/Opt-Out</li>"+
						"<li>Your Security</li><li>Links to Other Site(s)</li><li>Children</li>"+
						"<li>Changes to Our Privacy Policy</li><li>California Privacy Rights</li>"+
						"<li>Disclosure for Legal Purposes</li><li>Non-Confidential Information</li>"+
						"<li>Assignment</li><li>Disputes</li><li>Class Action Waiver</li><li>Contact Us</li></ol>"+fontTagEndStr+"</p>"+


			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;1.&nbsp;<u>Scope  </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;1.1&nbsp;We value our users and respect your privacy. This Privacy Policy describes the Personal Information (as defined below) we collect about you online on the Site(s), why we collect it, how we use it, and when we share it with third parties. This Privacy Policy also describes the choices you can make about how we collect and use certain of that information."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;1.2&nbsp;The Privacy Policy does not apply to information collected by us offline or through any other means, including any third party or any application or content (including advertising) that may link to or be accessible from or on the Websites. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;1.3&nbsp;Although many areas of the Site(s) do not require registration and may be accessed by you without having to disclose any personal information, we or our service providers and/or third party advertisers may ask you for certain kinds of personal information, including but not limited to your name, email address, mailing address, telephone number, age, credit/debit card information, user name and password, demographic information and other information that might identify you as an individual (�Personal Information�). The Personal Information we collect about you may also be combined or supplemented with information from other sources. Any Personal Information used in any manner will be used consistent with this Privacy Policy. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;1.4&nbsp;By accessing the Site(s), you acknowledge this Privacy Policy and agree to be bound by the terms hereof and the Terms of Use set forth on the Site(s). If there is anything you do not understand please email any inquiry to. <a href=\"mailto:privacy@rewardthefan.com\">privacy@rewardthefan.com</a>. <strong>If at any time you do not agree to this Privacy Policy, please do not use the Site(s)</strong>. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;1.5&nbsp;We reserve the right to change or update this Privacy Policy by posting such changes or updates to the Site(s). Amendments to this Privacy Policy will be posted at <a href=\"http://www.rewardthefan.com/Terms\">www.rewardthefan.com/Terms</a> and will be effective when posted. It is your responsibility to review any such changes or updates and to check the Site(s) regularly to be sure you understand all terms and conditions, agreements and policies of the Site(s) and are in compliance with same. The �Last Modified� date indicates if and as of when changes have been made to this Privacy Policy. Your continued use of the Site(s) following the posting of any amendment, modification or change shall constitute your acceptance thereof. "+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;2.&nbsp;<u>Types of Information We Collect.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.1&nbsp;We collect both \"Personal Information\" and \"Anonymous Information\" about our users. Personal Information is information that can be used to contact or identify you, such as your full name, email address, phone number, street address, as well as information that is linked to such information. Anonymous Information is information that cannot be used to contact or identify you, and is not linked to information that can be used to do so. It includes passively collected information about your activities on the Site(s), including, without limitation, usage data.  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.2&nbsp;Users can access and browse a majority of the Site(s), including downloading the Reward the Fan mobile application (the �App�) without disclosing any Personal Information. However, akin to most website and mobile app providers, we passively collect certain information from your devices, such as your IP address, browser information, unique device identifier (�UDID�) and/or your mobile operating system. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.3&nbsp;We may collect information that you provide directly to us. You may provide different types of Personal Information to us if and when you engage in certain activities that may be provided on the Site(s), such as creating an account, playing the Reward the Fan App Trivia Game, ordering a product or service, requesting information about career opportunities, submitting, posting or accessing various content or features, responding to and submitting a form(s), participating in our blogs, podcasts or forums, entering a sweepstakes, contest, promotion or other special initiative, signing up for a special offer, completing a survey, sending feedback, requesting or submitting information, or directly contacting us. It is optional for you to engage in such activity; however, if you choose to do so, we may require that you provide us certain Personal Information. Depending upon the activity, some of the information we ask you to provide is required and some is voluntary. If you do not provide required information for a particular activity, you may not be permitted to engage in that activity."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.4&nbsp;We may also collect certain information automatically, including through use of cookies, web beacons and other technologies. This information may include the following: your browser and mobile device type, operating system and name of your Internet service provider; web pages you visit on the Site(s); IP address, browser type, profile information, geo-location information; information about your interactions with email messages, such as whether the messages were opened and the links clicked in those emails; and standard server log information."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.5&nbsp;We collect email lists and information related to meeting requests and marketing services. When you use the Site(s), you may be given the option to subscribe to or to be added to our subscription or email list, to schedule a personal call/meeting and to request certain resource materials. Whether or not you select this/these option(s), your Personal Information may be shared with third parties consistent with this Privacy Policy. You may opt-out or unsubscribe from these email list or call/meeting requests at any time."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.6&nbsp;We collect Personal Information that you provide when you set up your account/become a registered user. You have the opportunity to create account(s) on the Site(s), which may include a shopping account with us. A shopping account may allow you to shop faster and easier, check on order status and history, post comments to the Site(s) and to use certain other features of the Site(s). Using your email and password of your choice, you may access your account online at any time to add, delete, or change information. The Personal Information provided by you is not used for any purpose other than as outlined in this Privacy Policy. If you ever use a public computer to access the Site(s) account, we strongly encourage you to log out at the conclusion of your session. By doing so, although your Personal Information may still be stored with us, it should not be accessible to anyone else from that computer.  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.7&nbsp;We collect Personal Information provided by registered users. In creating an account and saving it for your next visit, you authorize us to store your name, address, email address, telephone number and other Personal Information that you may provide. With the exception of email addresses, and cell phone numbers, registered users� voluntary personal profile information, including pictures, may be displayed to other registered users in order to facilitate user interaction. Registered users� user names are displayed to all users, although each verified user has the option of having his or her full name displayed to all users instead of his or her user name. We therefore encourage you to choose a user name that is not your actual name. Under no circumstances are email addresses, or cell phone numbers directly revealed to other users or disclosed to third parties other than as set forth in this Privacy Policy."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.8&nbsp;Only some payment processing information is collected by us. We may also use a third party(ies) for payment processing. We do not want you to send us your credit card or bank account information by email or other means except where requested as part of the checkout process. Please review the terms of use and privacy policies of the third party payment processor prior to providing Personal Information to them. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.9&nbsp;If you access the Services through a desktop or mobile device, we may access, collect, monitor and/or remotely store geo-location data. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.10&nbsp;You may also be able to register for or login to the Site(s) through certain social media accounts, such as Facebook. In these situations, we will have access to the Personal Information that you have provided to such social media account. Please note that if you choose to connect your social media contacts and address book information with us, we may collect contact information for your listed friends pursuant to the terms of your social media account. In such case, you represent and warrant that you are authorized to provide us with all such contact information and are in compliance with your agreements with any such social media accounts."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.11&nbsp;We may also obtain information from other sources and combine that with information we collect directly. For example, we may collect information about you from third parties, including but not limited to identity verification services, fraud detection service providers, credit bureaus, mailing list providers and publicly available sources. If you create or log into your account through a social media site, we will have access to certain information from that site, such as your name, account information and friends lists, in accordance with the authorization procedures determined by such social media site. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.12&nbsp;We may also use third party services, such as Google Analytics, to track and analyze online behavior data of users. If you would like to opt out of Google Analytics, please download and install the browser plugin at <a href=\"http://tools.google.com/dlpage/gaoptout?hl=en\">http://tools.google.com/dlpage/gaoptout?hl=en</a>. Users can also opt out of third party interest based advertising by visiting the Network Advertising Initiative�s deactivation website at <a href=\"www.networkadvertising.org/choices/\">www.networkadvertising.org/choices/</a>. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.13&nbsp;<u>Other Collection Methods:</u>  If you choose to use our referral service to tell a friend about our Site(s), we will ask you for your friend�s name and email address. We will automatically send your friend an email inviting him/her to visit the Site(s). We may send up to three (3) follow up emails. Reward The Fan stores this information for the purpose of sending this email(s) and tracking the success of our referral program. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.14&nbsp;Some features of the Services allow you to provide content to the Services, such as video clips. All content submitted by you to the Services may be retained by us indefinitely, even after you terminate your Account. We may continue to disclose such content to third parties in a manner consistent with this Privacy Policy. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;2.15&nbsp;We may communicate with you via push notification, Whatsapp, email, SMS, MMS or other text message (to the extent you permit us to do so), and we may collect information regarding such communications, including, without limitation, confirmation when you open an email, read a message or receive a push notification.  By registering for Reward the Fan, you agree to receive automated promotional text messages and Whatsapp messages from or on behalf of Reward the Fan.  You are not required to provide this consent as a condition of purchasing any property, goods or services.  Standard messaging rates may apply.  Some of your activity on and through the Services is public. This may include, but is not limited to, content you have posted publicly on, through or in connection with the Services, including without limitation on public portions of third party services, such as social media platforms. "+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>3.&nbsp;<u>How We Use Your Information.</u></strong> "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"We may use the Personal Information we collect for various purposes, including to:<br/>"+
			"<ul><li>personalize and tailor the features, performance and support of the Site(s)</li>" +
			"<li>send you promotional/marketing information, newsletters, offers or other information from us or on behalf of our sponsors or partners</li>"+
			"<li>perform internal operations, including fraud detection</li>"+
			"<li>analyze, benchmark and conduct research on user data and interactions with the Services</li>" +
			"<li>authenticate users</li>"+
			"<li>provide the materials, goods and/or services we offer and/or you request</li>" +
			"<li>enable you to participate in features such as sending and receiving invitations, surveys, reviews, blogs or forums, or to participate in special initiatives and to communicate with you about them</li>"
			+ "<li>complete your ticket redemption and/or any purchase transactions, fulfill your orders, keep you informed about the status of our goods and services and your orders </li>"+
			"<li>identify your product and service preferences, so we can notify you of new or additional products, services, and promotions that might be of interest to you </li>"+
			"<li>conduct market research and to customized offers;</li>" +
			"<li>improve our services and any merchandise selections, customer service, and overall Site(s) experience by aggregating and analyzing our customer data</li>" +
			"<li>analyze the use of our products and services and information about visitors to the Site(s) to enhance our marketing efforts</li>" +
			"<li>update and maintain the accuracy of information about our customers</li>"
			+ "<li>communicate with you by email, text message, app notifications, or other means about our company, our products, or other information that we believe may be of interest to you </li>" +
			"<li>provide others with your Personal Information for the limited purposes described in this Privacy Policy</li>" +
			"<li>if you join our electronic mailing list, to send you our electronic communications, including our newsletters and our company announcements, alerts, notices and updates </li>"
			+ "<li>send you notices of a transactional, administrative or relationship nature or as required by law. </li></ul><br/><br/>"
			+ "We may use the Anonymous Information we collect for various purposes, including to:<br/>"
			+ "<ul><li>perform internal operations on the Site(s)</li>" +
			"<li>improve the Site(s) and customize the user experience</li>" +
			"<li>aggregate the information collected via Cookies and Pixels to use in statistical analysis to help us track trends and analyze patterns</li></ul>"+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;4.	How We Share and Disclose Information with Third Parties.    </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;4.1&nbsp;We know how important it is to keep your information confidential. We will not rent, sell, or share your Personal Information with third parties, except as set forth herein. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;4.2&nbsp;If you do not want us to use or disclose Personal Information collected about you in the ways identified in this Privacy Policy, you may choose not to use Site(s), or provide your Personal Information at any time. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;4.3&nbsp;We may disclose your Personal Information to outside individuals and/or companies that help us bring you the products and services we offer and to create, operate, and maintain our Website. For example, we may work with third parties to: (a) manage a database of customer information; (b) assist us in distributing e-mails; (c) assist us with direct marketing and data collection; (d) provide data storage and analysis; (e) provide fraud prevention; (f) provide customer service; and (g) provide other services designed to assist us in developing and running our Website and maximizing our business potential. We require that these outside companies agree to keep all information shared with them confidential and to use the information only to perform their obligations to us. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;4.4&nbsp;You agree to allow us to share your geo-location information."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;4.5&nbsp;We may disclose information we have collected about you if required to do so by law or if we, in our sole discretion, believe that disclosure is reasonable to: (i) satisfy any applicable law, regulation, legal process or governmental request; (ii) enforce this Privacy Policy and our Terms of Use, including investigation of potential violations hereof; (iii) detect, prevent, or otherwise address fraud, security or technical issues; (iv) respond to user support requests; or (v) protect our rights, property or safety, our users and the public. This includes exchanging information with other companies and organizations for fraud protection and spam/malware prevention. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;4.6&nbsp;<u>Aggregate Anonymous Information:</u> Aggregated Anonymous Information is the combination of your Anonymous Information with the Anonymous Information of other Users (�Aggregated Anonymous Information�). Aggregated Anonymous Information does not allow you to be identified or contacted. We may share such Aggregated Anonymous Information with third parties. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;4.7&nbsp;We may share your Personal Information with any of our subsidiaries, joint venturers, or other companies under common control, in which case we will require them to honor this Privacy Policy. Additionally, in the event we undergo a business transition such as a merger, acquisition by another company, or sale of all or a portion of our assets, your Personal Information may be among the assets transferred, and your Personal Information may be shared as part of the negotiation of the transaction. You acknowledge that such transfers may occur and are permitted by this Privacy Policy, and that any entity that acquires us, is merged with us or that acquires our assets may continue to process your Personal Information as set forth in this Privacy Policy."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;5.&nbsp;Non-Personally Identifiable Information.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;5.1&nbsp;Through your use of the Site(s), we may also collect certain non-personally identifiable information from you (�NPII�). NPII is not associated with you as an individual. It is measured anonymously and only in the aggregate. We use NPII to maintain and administer the Site(s), analyze trends, gather demographic information and comply with applicable law. We may share this information with others without express notice to you or consent from you, and we may exploit, use and disclose your NPII without limitation of any kind. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;5.2&nbsp;The Site(s) may use cookies. A cookie is a small piece of information sent by a Site(s) that is saved on your hard drive by your computer's browser. Cookies help us remember you when you return to the Site(s). Cookies also hold information to personalize and enhance your experience and to gather Site(s) statistical data, such as which pages are visited, the Internet provider's domain name and the addresses of the sites you visited immediately before coming to and immediately after leaving the Site(s). The information in the cookies lets us trace your \"clickstream\" activity (i.e., the paths taken by users of the Site(s) as they move from page to page) to enable us to better serve you by revealing which portions of the Site(s) are the most popular. We may also allow our affiliate and service providers and advertisers to serve cookies from the Site(s) to allow them to assist us in various activities such as doing analysis and research on the effectiveness of the Site(s), its content and advertising. Some third party advertising companies may be advertising networks that are members of the Network Advertising Initiative, which offer a single location to opt out of ad targeting from member companies. For more information, see: <a href=\"www.networkadvertising.org\">www.networkadvertising.org</a>.  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;5.3&nbsp;We may also use \"pixel tags\" (sometimes called \"web beacons\" or \"clear gifs\"), which are tiny graphic images, on the Site(s). Pixel tags help us analyze users� online behavior and measure the effectiveness of the Site(s) and our advertising and marketing. Pixel tags or Clear gif files are tiny graphics with a unique label that work in a similar way to cookies and are used to monitor the user�s online activities. In contrast to cookies that are saved on a user�s computer hard disk, clear gif files are embedded invisibly in websites and are about as big as the full stop at the end of this sentence. Where appropriate, we may combine the information collected by such pixel tags with the Personal Information of our customers. We may also use other analytical tools to evaluate site performance through the use of aggregated data, which contain no Personal Information. We work with service providers that help us track, collect, and analyze this information. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;5.4&nbsp;Cookies, pixel tags, and/or other analytical tools that we may use on the Site(s) may collect information about your visit, including the pages you view, the features you use, the links you click, and other actions you take in connection with the Site(s). This information may include your computer's Internet protocol (IP) address, your browser type, your operating system, date and time information, and other technical information about your computer. We may also track certain information about the identity of the Site(s) you visited immediately before coming to the Site(s). Cookies, pixel tags, and/or other analytical tools in our emails may also be used to track your interactions with those messages, such as when you receive, open, or click a link in an email message from us. We may also work with businesses that use tracking technologies to deliver advertisements on our behalf across the Internet. These companies may collect information about your visits to the Site(s) and your interaction with our advertising and other communications, but no Personal Information is shared with them. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;5.5&nbsp;We may combine the NPII collected through cookies, pixel tags and other analytical tools with other information we may have collected from you. This information may be used to improve the Site(s), to personalize your online experience, to help us deliver information to you, to determine the effectiveness of advertising, and for other internal business purposes. We may use and share aggregated and anonymous information to conduct market research and analysis for ourselves and/or for our business partners. Given the anonymous, non-personally identifiable nature of such information, there are no restrictions under this Privacy Policy on how we may use or disclose such information. For example, we may freely share such information with third parties who may use such data for their own marketing, advertising, research, or other business purposes. We may also freely share such information with our service providers in order for them to perform services to or for us. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;5.6&nbsp;We authorize certain service providers to utilize NPII for their business purposes and in accordance with their privacy policies, such as to report on usage or industry trends to their customer base.  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;5.7&nbsp;Third Party Functionalities: The Site(s) contains links to and/or enables certain third party functionalities to enhance your experience on the Site(s), including social plug-ins, tools and APIs. Prior to using any third party functionalities (e.g., Facebook �Like� button) on the Site(s), you should consult the privacy notices of the third party providers of such functionalities (e.g., Facebook), as we have no control over information that is submitted to, or collected by, such third parties, or how they may use the information. The privacy policies and data practices of such third parties may significantly differ from ours, and we make no representation or warranty whatsoever about their policies and practices. Your communications and interactions with such third parties are solely between you and them, and are at your own risk."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;6.&nbsp;<u>Your Choices/Opt-Out.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.1&nbsp;You consent to receive certain communications from us. You may opt out of: (a) receiving email communications such as email newsletters and promotional emails by following the instructions provided at the bottom of each email, clicking the �unsubscribe� button at the bottom of emails we sent you; and/or (b) receiving promotional email communications and newsletters by emailing us at <a href=\"mailto:privacy@rewardthefan.com\">privacy@rewardthefan.com</a> and including the word �UNSUBSCRIBE� in the subject text. Please allow up to ten (10) business days for changes to your email preferences to take effect. During that time, you may continue to receive email communications from us that were already in process. Opting out of receiving Site(s) communications will not affect your receipt of service-related communications, such as payment confirmations and delivery status updates, as we must be able to communicate with you regarding your purchases. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.2&nbsp;Opting out of online and mobile website advertising. You can opt out of Internet-based and mobile advertising on your mobile device by visiting TRUSTe�s Ad Preference Manager, currently available at <a href=\"https://preferences-mgr.truste.com/\">https://preferences-mgr.truste.com/</a>.  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.3&nbsp;Our products and services may use Google Analytics Advertising Features and its associated tracking technologies to help display our ads you see on other sites, and to help us manage and optimize our online advertising efforts. To opt out of Google Analytics Advertising Features, visit Google�s Ad Settings page, currently located at <a href=\"https://www.google.com/settings/ads/anonymous?sig=ACi0TChBBUZhbil6jSzh_t9gc_hilJmxPFILo3fzsOJJ6mtq1DpanjQIychv5x00ayD5xiJnE_7py3LRuGOQtj8YQx5_1ZS2FA&hl=en\">https://www.google.com/settings/ads/anonymous?sig=ACi0TChBBUZhbil6jSzh_t9gc_hilJmxPFILo3fzsOJJ6mtq1DpanjQIychv5x00ayD5xiJnE_7py3LRuGOQtj8YQx5_1ZS2FA&hl=en</a>. Website users can also access the Google Analytics Opt Out Browser Add-on, currently located at <a href=\"https://tools.google.com/dlpage/gaoptout\">https://tools.google.com/dlpage/gaoptout</a> or <a href=\"http://tools.google.com/dlpage/gaoptout?hl=en\">http://tools.google.com/dlpage/gaoptout?hl=en</a>. We may also work with Facebook and Apple to provide analytics in connection with our Site(s), including our mobile applications. For more information about Facebook�s privacy practices, you may visit <a href=\"https://www.facebook.com/about/privacy\">https://www.facebook.com/about/privacy</a>, and for Apple�s privacy policy, click here <a href=\"http://www.apple.com/privacy/privacy-policy/\">http://www.apple.com/privacy/privacy-policy/</a>.   "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.4&nbsp;You may can opt out of receiving online behavioral or internet based advertising by using the tools located at the Digital Advertising Alliance�s consumer choice page, currently available at http://www.aboutads.info/choices/ or the Network Advertising Initiative (NAI) opt out tool currently available at <a href=\"http://www.networkadvertising.org/choices/\">http://www.networkadvertising.org/choices/</a>.   "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.5&nbsp;When using the ad industry opt out tools described in Sections 6.2 through 6.4, note that: (a) if you opt-out we may still collect some data about your online activity for operational purposes (such as fraud prevention), but it will not be used by us for the purpose of targeting ads to you; (b) if you use multiple browsers or devices you may need to execute this opt out on each browser or device; and (c) other ad companies� opt-outs may function differently than our opt-out, and we have no control over the practices of any third parties. We do not make any representations or warranties about such opt-out services. Such services are independent from us, and we have no control over, or responsibility for their performance."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.6&nbsp;You can opt out of accepting cookies or disable them from your browser. The �Help� function on most browsers contains information on how you can set your browser to notify you before accepting cookies, or can disable them entirely. If you opt out of cookies, you will not be able to take advantage of various features of the Site(s) that are available to other users. For example, we may use cookies to recognize you by name when you return to this site so you don�t have to login again and provide your password each time.  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.7&nbsp;If you prefer not to receive text or wireless promotional communications on your mobile device, you can opt out by replying STOP to any text you receive from us. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.8&nbsp;You may ask us to delete your Personal Information from our system, however, because we archive and keep track of past transactions, you cannot delete information associated with past transactions or archived information on the Site(s). Please also update your Personal Information if it changes. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.9&nbsp;Registered users may terminate their account registration at any time, for any reason, by contacting us. Terminating your account registration will not necessarily remove previous public comments or other user content on public display on the Site(s). You understand it may be impossible to delete Personal Information entirely because of backups and records of deletions. Further, please note that we may be required to maintain certain Personal Information, or to maintain certain Personal Information for a period of time, in which case we will comply with your deletion request only after we have fulfilled such requirements. When we delete any information, it will be deleted from our active database, but may remain in our archives. We may retain your information for fraud detection or similar purposes. Note that if we have already disclosed some of your Personal Information to third parties, we cannot access that Personal Information any longer and cannot compel the deletion or modification of any such information by the parties to whom we have made those disclosures. If you wish to cancel your account on the Site(s) or delete your Personal Information previously provided to us, you may send your request to us at <a href=\"mailto:privacy@rewardthefan.com\">privacy@rewardthefan.com</a> and include the words �DELETE ME� in the subject line with your specific request in the body of your communication.  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.10&nbsp;If you do not receive a response from us to any emails you send to us within ten (10) business days, please send us another email as your original email may not have been received. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;6.11&nbsp; Please note that despite any indicated marketing preferences, we may send you administrative emails regarding the Site(s), including, for example, notices of updates to our Privacy Policy.  "+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;7.&nbsp;<u>Your Security.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;7.1&nbsp;We strive to keep your Personal Information private and safe. We take commercially reasonable physical, electronic and administrative steps to maintain the security of Personal Information collected, including limiting the number of people who have physical access to database servers, as well as employing electronic security systems and password protections that guard against unauthorized access. In addition, it is our policy to never send your credit card number via e-mail. Any payment transactions will be encrypted. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;7.2&nbsp;Unfortunately, despite our best efforts, the transmission of data over the Internet cannot be guaranteed to be 100% secure. While we will use reasonable means to ensure the security of information you transmit through the Site(s), any transmission of Personal Information is at your own risk. We cannot guarantee that such information will not be intercepted by third parties and we shall not be liable for any breach of the security of your Personal Information resulting from causes or events that are beyond our control, including, without limitation, your own act or omission, corruption of storage media, defects in third party data security products or services, power failures, natural phenomena, riots, acts of vandalism, hacking, sabotage, or terrorism, and we are not responsible for circumvention of any privacy settings or security measures contained on the Site(s).  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;7.3&nbsp;We will make any legally required disclosures of any breach of the security, confidentiality, or integrity of your unencrypted electronically stored \"personal data\" (as defined in applicable state statutes on security breach notification) to you via email and/or conspicuous posting on the Site(s) in the most expedient time possible and without unreasonable delay, insofar as it is consistent with (i) the legitimate needs of law enforcement; or (ii) any measures necessary to determine the scope of the breach and restore the reasonable integrity of the data system.  "+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;8.&nbsp;<u>Links to Other Site(s).</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;8.1&nbsp;While visiting the Site(s), you may link to websites operated by third parties or you may have come to the Site(s) using a link found in another website. This does not mean that we endorse these Site(s) or the goods or services they provide. We do not make any representations or warranties about any Site(s) that may be linked to the Site(s). Such other Site(s) are independent from us, and we have no control over, or responsibility for their information, products or activities."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;8.2&nbsp;In addition, our privacy practices may differ from those of these other Site(s). If you provide Personal Information at one of those Site(s), you are subject to the privacy policy of the operator of that Site(s), not our Privacy Policy. Please make sure you understand any other website's privacy policy before providing Personal Information. "+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;9.&nbsp;<u>Children</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"This Site(s) is not directed to or intended for children under 13 years of age. We do not knowingly solicit, collect or maintain information from those we actually know are under 13 years of age, and no part of our Site(s) is targeted to attract anyone under 13 years of age. We also do not send e-mail correspondence to anyone who advises that they are under the age of 13 years of age. If we later obtain actual knowledge that a User is under 13 years of age we will take steps to remove that User�s Personal Information from our systems. If you are the parent or guardian of a child whom you believe has disclosed Personal Information to us, please contact us at <a href=\"mailto:privacy@rewardthefan.com\">privacy@rewardthefan.com</a>. so that we may delete and remove such information from our system. "+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;10.&nbsp;<u>Changes to Our Privacy Policy.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"We reserve the right to change, modify, add or remove portions of this Privacy Policy at any time and without prior notice, and any changes will become effective immediately upon being posted unless we advise you otherwise. However, we will not use your Personal Information in a way that is adverse and materially different from the uses described in this Privacy Policy without giving you an opportunity to opt out. Your continued use of the Site(s) after this Privacy Policy has been amended shall be deemed to be your continued acceptance of the terms and conditions of the Privacy Policy, as amended. We encourage you to review this Privacy Policy regularly."+fontTagEndStr+"</p>"+

			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;11.&nbsp;<u>California Privacy Rights.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;11.1&nbsp;Under California Civil Code sections 1798.83-1798.84, California residents are entitled to ask us for a notice describing what categories of Personal Information we share with third parties or corporate affiliates for those third parties or corporate affiliates' direct marketing purposes. That notice will identify the categories of information shared and will include a list of the third parties and affiliates with which it was shared, along with their names and addresses. If you are a California resident and would like a copy of this notice, please submit an email request to the following email address: privacy@rewardthefan.com and include the words �CALIFORNIA PRIVACY� in the subject line, and you must put the statement �Your California Privacy Rights� in the body of the request and state the name of our specific website with respect to which you are requesting the information as well as your name, street address, city, state, and zip code. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;11.2&nbsp;In addition, please note the following:  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;(a)&nbsp;Users can visit the Site(s) anonymously;  "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;(b)&nbsp;We will add a link to this Privacy Policy on our home page, or at a minimum, on the first significant page after entering the Site(s);"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;(c)&nbsp;Our Privacy Policy link includes the word �Privacy� and can be easily be found on the page specified above; "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;(d)&nbsp;Users will be notified of any privacy policy changes on our Privacy Policy page; "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;(e)&nbsp;Users are able to change their Personal Information by emailing us or by calling us;"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;(f)&nbsp;Some Internet browsers include the ability to transmit �Do Not Track� signals that give you control over the collection and use of web browsing information. Because uniform standards for �Do Not Track� signals have not yet been adopted, we do not process or respond to such signals in users� web browsers at this time; and "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;&nbsp;(g)&nbsp;We allow the collection of users� behavioral tracking (but not Personal Information) by third parties. We do not authorize the collection of Personal Information on the Site(s) by third parties. "+fontTagEndStr+"</p>"+

			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;12.&nbsp;<u>Disclosure for Legal Purposes. </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"You hereby authorize us to disclose any of your Personal Information pursuant to judicial and administrative proceedings and to law enforcement or government agencies if we believe the disclosure is necessary or appropriate. You also authorize us to disclose Personal Information if we believe the disclosure is necessary or appropriate in the event of an investigation of fraud, improper or illegal conduct in connection with the Site(s), such as fraud, misrepresentation, intellectual property infringement, or other activity that may put us at risk for liability."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;13.&nbsp;<u>Non-Confidential Information. </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"We do not want you to send to us any confidential or proprietary information through email or otherwise. Any information, materials, suggestions, ideas or comments sent to us will be considered non-confidential, and by submitting the same to us, you are giving us the absolute right to use, modify, reproduce, transmit, display and distribute the information for any purpose whatsoever, with no payment or other compensation to you. However, we will not use your name unless we are required by law to identify the source of the materials, information, suggestions, ideas or comments, or unless we first obtain your permission."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;14.&nbsp;<u>Assignment </u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"We may freely assign this Privacy Policy or any of our rights and/or obligations hereunder."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;15.&nbsp;<u>Disputes.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.1&nbsp;This Privacy Policy will be interpreted in accordance with the laws of the State of New York and the United States of America, without regard to its conflict-of-law provisions. By visiting the Site(s), you agree that, except as otherwise specified herein, the laws of the State of New York without regard to principles of conflict of laws, will govern any dispute of any sort that might arise between us or any of our affiliates regarding your visit and use of the Site(s) and Services."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.2&nbsp;We and you agree that in the event of any dispute, the party wishing to address the dispute must contact the other party in writing, including by e-mail, and advise the other party of the dispute in reasonable detail as well as informing the other party of the remedy being sought. We may send you notices via the email address or physical address you provide to us, and all notices to us shall be sent to the following email address: <a href=\"mailto:privacy@rewardthefan.com\">privacy@rewardthefan.com</a> and include the words �DISPUTE NOTICE� in the subject. The parties shall then make a good faith effort to resolve the dispute before resorting to more formal means of resolution. In the event that the dispute is not resolved through this procedure, the party raising the dispute may proceed to mandatory arbitration as set forth below. "+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.3&nbsp;ANY AND ALL DISPUTES BETWEEN YOU AND US WILL BE RESOLVED BY BINDING ARBITRATION. YOU HEREBY AGREE TO GIVE UP YOUR RIGHT TO GO TO COURT to assert or defend your rights under this Privacy Policy, except for matters that may be taken to small claims court. Your rights will be determined by a neutral arbitrator, NOT a judge or jury. You agree that any dispute arising out of or relating to this Privacy Policy, including with respect to the interpretation of any provision of this Privacy Policy or other agreements between you and us, or concerning the performance or obligations of you and us, shall be resolved by mandatory and binding arbitration submitted to JAMS in accordance with its Commercial Arbitration Rules at the request of either us or you pursuant to the following conditions:"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.4&nbsp;Place of Arbitration Hearings. Unless you elect to conduct the arbitration by telephone or written submission, an in-person arbitration hearing will conducted at a JAMS facility in your area or at a JAMS facility in New York, New York."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.5&nbsp;Selection of Arbitrator shall be made pursuant to JAMS� Streamlined Arbitration Rules & Procedures or JAMS� Comprehensive Arbitration Rules & Procedures, depending on the amount of the claim as specified herein."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.6&nbsp;Conduct of Arbitration. The arbitration shall be conducted by a single neutral arbitrator under JAMS� Streamlined Arbitration Rules & Procedures. For claims exceeding $5,000.00, the arbitration shall be conducted under JAMS� Comprehensive Arbitration Rules & Procedures Subject to the applicable JAMS procedure, the arbitrator shall allow reasonable discovery in the forms permitted by the Federal Rules of Civil Procedure, to the extent consistent with the purpose of the arbitration. The arbitrator(s) shall have no power or authority to amend or disregard any provision of this section or any other provision of these Terms of Use, except as necessary to comply with JAMS� Policy on Consumer Arbitrations Pursuant to Pre-Dispute Clauses Minimum Standards of Procedural Fairness. The arbitration hearing shall be commenced promptly and conducted expeditiously. If more than one day is necessary, the arbitration hearing shall be conducted on consecutive days unless otherwise agreed in writing by the parties."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.7&nbsp;Findings and Conclusions. The arbitrator(s) shall, after reaching judgment and award, prepare and distribute to the parties written findings of fact and conclusions of law relevant to such judgment and award and containing an opinion setting forth the reasons for the giving or denial of any award. The award of the arbitrator(s) shall be final and binding on the parties, and judgment thereon may be entered in a court of competent jurisdiction."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.8&nbsp;Costs and Fees. You will be subject to a filing fee, set by JAMS, to initiate the arbitration. To the extent permitted by JAMS procedures, each party shall bear its own costs and expenses and an equal share of the arbitrators� and administrative fees of arbitration, and we will remain responsible for its share of costs, expenses and fees plus any costs, expenses and fees required under JAMS procedures."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.9&nbsp;Litigation. The Federal Arbitration Act and federal arbitration law apply to these Terms. Either party also may, without waiving any remedy under these Terms, seek from any court having jurisdiction any interim or provisional relief that is necessary to protect the rights or property of that party, pending the establishment of the arbitral tribunal (or pending the arbitral tribunal�s determination of the merits of the controversy)."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.10&nbsp;Notwithstanding the arbitration clause herein, the parties also agree that we may bring suit in a court located in New York, New York, to enjoin infringement or other misuse of our intellectual property rights."+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"&nbsp;15.11&nbsp;Other. The Federal Arbitration Act and federal arbitration law apply to this Privacy Policy. Both you and we expressly waive any ability to maintain any class action proceedings in any forum."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;16.&nbsp;<u>Class-Action Waiver.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"Any arbitration, claim or other proceedings by or between you and us shall be conducted on an individual basis and not in any class action, mass action, or on a consolidated or representative basis. You further agree that the arbitrator shall have no authority to award class-wide relief or to combine or aggregate similar claims or unrelated transactions. You acknowledge and agree that this agreement specifically prohibits you from commencing arbitration proceedings as a representative of others. If for any reason a claim proceeds in court rather than in arbitration, each party waives any right to a jury trial. Any claim that all or part of this Class Action Waiver is unenforceable, unconscionable, void, or voidable may be determined only by a court of competent jurisdiction and not by an arbitrator."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>&nbsp;17.&nbsp;<u>Contact Us.</u></strong>"+fontTagEndStr+"</p>"+
			"<p align="+style+">"+fontTagStartStr+"If you have any questions regarding our Privacy Policy, you can contact us via email at <a href=\"mailto:privacy@rewardthefan.com\">privacy@rewardthefan.com</a> or write to us at Reward The Fan, 159-16 Union Turnpike Suite 212, Fresh Meadows, NY 11366."+fontTagEndStr+"</p>"+
			
			"<p align="+style+">"+fontTagStartStr+"<strong>Last updated Dec 15, 2020</strong>"+fontTagEndStr+"</p>");
						
						
			}else if(pageType.equals("privacyPolicies")){
				
				RTFProductInfo.setDescription("<p align="+style+"><font "+fontSizeStr+" color="+justColor+"><strong>Privacy Policy</strong></font></p>" +
						"<p align="+style+">"+fontTagStartStr+"This Privacy Statement describes how Reward The Fan collects and uses the personal information you provide."+fontTagEndStr+"</p>"+

						"<p align="+style+">"+fontTagStartStr+"<strong>What information do we collect?</strong><br/>"+
						"&nbsp;1)&nbsp;When you visit any of our Reward The Fan offerings, we track your IP address, referral website, browser type, web navigation, location etc.<br/>" +
						"&nbsp;2)&nbsp;When you create an account you provide us with your name, email address, billing address, phone number and payment information in order to process your order.<br/>" +
						"&nbsp;3)&nbsp;We also use cookies and various other tracking technologies, such as beacons, tags and scripts to improve our behavioral targeting, retargeting and your overall shopping experience."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Is my Personal Information used for any other purpose?</strong><br/>"+
						"&nbsp;We may use your personal and account information to provide products or services you have requested, send you promotional or product information or send you updates on important information about our company and services."+fontTagEndStr+"</p>"+
						
						"<p align="+style+">"+fontTagStartStr+"<strong>Protection</strong><br/>"+
						"&nbsp;We use industry standard security and anti-fraud measures to protect your information and to reduce the risks of loss or misuse.<br/>" +
						"Reward The Fan will not be responsible or liable for any damages that result from a lapse in compliance with this Privacy Policy because of a security breach or technical malfunction."+fontTagEndStr+"</p>");
						
			}else if(pageType.equals("DMCA")){
				
				RTFProductInfo.setDescription(
						"<p align="+style+">"+fontTagStartStr+"<strong>Reward The Fan (&#34;<strong>RTF</strong>,&#34; &#34;<strong>us</strong>,&#34; <strong>our</strong>&#34; or &#34;<strong>we</strong>&#34;) has "
								+ "adopted the following procedures to respond to alleged copyright infringement in accordance with the Digital Millennium Copyright "
								+ "Act (&#34;<strong>DMCA</strong>&#34;). The address of our designated agent to receive notification of infringement "
								+ "(&#34;<strong>Designated Agent</strong>&#34;) is "
								+ "listed at the end of this policy.  </strong>"+fontTagEndStr+"</p>"+
								
						"<p align="+style+">"+fontTagStartStr+"It is our policy to (1) block access to or remove material that we believe in good faith to be copyrighted material "
								+ "that has been illegally copied and distributed by any of our users; and (2) remove and discontinue service to repeat offenders."+fontTagEndStr+"</p>"+
						 	 
						"<p align="+style+">"+fontTagStartStr+"<strong><u>A.&nbsp;How to Report Copyright Infringement</u></strong>"+fontTagEndStr+"</p>"+
						 
						"<p align="+style+">"+fontTagStartStr+"If you believe that material or content residing on or accessible through the Website infringes a copyright,"
								+ "please send a notice of copyright infringement containing the following information to the Designated Agent "
								+ "listed below:"+fontTagEndStr+"</p>"+
						
								
							"<p align="+style+">"+fontTagStartStr+"1.&nbsp;A physical or electronic signature of a person authorized to act on behalf of the "
									+ "owner of the copyright that has been allegedly infringed;"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"2.&nbsp;Identification of works or materials being infringed;"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"3.&nbsp;Identification of the material that is claimed to be infringing including information regarding the "
									+ "location of the infringing materials that the copyright owner seeks to have removed, with sufficient detail so "
									+ "that we are capable of finding and verifying its existence;"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"4.&nbsp;Contact information about the copyright owner including address, telephone number and, if available, email address. "
									+ "If you are not the owner of the copyright that has been allegedly infringed, please describe your relationship to the "
									+ "copyright owner;"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"5.&nbsp;A statement that you have a good faith belief that the material is not authorized "
									+ "by the copyright owner, its agent, or the law; and"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"6.&nbsp;A statement made under penalty of perjury that the information provided is accurate and the notifying party "
									+ "is authorized to make the complaint on behalf of the copyright owner."+fontTagEndStr+"</p>"+
							
									
							"<p align="+style+">"+fontTagStartStr+"<strong><u>B.&nbsp;Upon Notification to the Designated Agent</u></strong>"+fontTagEndStr+"</p>"+
							 
							"<p align="+style+">"+fontTagStartStr+"It is our policy:"+fontTagEndStr+"</p>"+
													
															
							"<p align="+style+">"+fontTagStartStr+"1.&nbsp;to remove or disable access to the infringing material;</strong>"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"2.&nbsp;to notify the user that we have removed or disabled access to the material; and"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"3.&nbsp;that repeat offenders will have the infringing material removed from the system and that "
									+ "we will terminate such user&rsquo;s access to the Website."+fontTagEndStr+"</p>"+
 
							"<p align="+style+">"+fontTagStartStr+"<strong><u>C.&nbsp;Counter-Notice by Content Provider</u></strong>"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"If the user believes that the material that was removed or to which access was disabled is either not infringing, "
									+ "or the user believes that it has the right to post and use such material from the copyright owner, "
									+ "the copyright owner&rsquo;s agent, or pursuant to the law, the user must send a counter-notice containing "
									+ "the following information to the Designated Agent listed below:"+fontTagEndStr+"</p>"+
													
															
							"<p align="+style+">"+fontTagStartStr+"1.&nbsp;A physical or electronic signature of the user;"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"2.&nbsp;Identification of the material that has been removed or to which access to has "
									+ "been disabled and the location at which the material appeared before it was removed or disabled;"+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"3.&nbsp;A statement that the user has a good faith belief that the material was removed "
									+ "or disabled as a result of mistake or a misidentification of the material; and"+fontTagEndStr+"</p>"+	
							
							"<p align="+style+">"+fontTagStartStr+"4.&nbsp;User&rsquo;s name, address, telephone number, and, if available, e-mail address and "
									+ "a statement that such person or entity consents to the jurisdiction of the federal court "
									+ "for the judicial district in which the user&rsquo;s address is located, or if the user&rsquo;s address "
									+ "is located outside the United States, for any judicial district in which RTF is located, "
									+ "and that such person or entity will accept service of process from the person who provided "
									+ "notification of the alleged infringement."+fontTagEndStr+"</p>"+	
							
							"<p align="+style+">"+fontTagStartStr+"&nbsp;"+fontTagEndStr+"</p>"+	
							
							"<p align="+style+">"+fontTagStartStr+"If a counter-notice is received by the Designated Agent, we may send a copy of the counter-notice "
									+ "to the original complaining party informing that person that it may replace the removed material "
									+ "or cease disabling it in ten (10) business days. Unless the copyright owner files an action seeking "
									+ "a court order against the user, the removed material may be replaced or access to it restored in "
									+ "fourteen (14) business days or more after receipt of the counter-notice, solely at our discretion."+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"Please contact our Designated Agent at the following address: "
									+ "Reward The Fan, Attn: DMCA Designated Agent, 159-16 Union Turnpike Suite 212, Fresh Meadows, NY 11366 or email: <u><a href=\"mailto:dmca@rewardthefan.com\">dmca@rewardthefan.com</a></u>."+fontTagEndStr+"</p>"+
							
							"<p align="+style+">"+fontTagStartStr+"You hereby acknowledge that if you fail to comply with the requirements set forth above, "
									+ "your DMCA notice may not be valid."+fontTagEndStr+"</p>");
						
			} 
			RTFProductInfo.setPageType(pageType);
			RTFProductInfo.setStatus(1);
			return RTFProductInfo;
		}catch (Exception e) {
			error.setDescription(e.getMessage());
			RTFProductInfo.setError(error);
			RTFProductInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFPRODUCTINFO,"Exception Occurred");
			return RTFProductInfo;
		}
	}
	
	
	@RequestMapping(value="/GetUseRewardPointsPageInformtion",method=RequestMethod.POST)
	public @ResponsePayload RewardInformationPage getUseRewardPointsInformation(HttpServletRequest request, Model model,HttpServletResponse response){
		RewardInformationPage obj =new RewardInformationPage();
		String productTypeStr = request.getParameter("productType");
		String platFormStr = request.getParameter("platForm");
		if(TextUtil.isEmptyOrNull(platFormStr)){
			obj.setStatus(0);
			return obj;
		}
		ApplicationPlatForm platForm = null;
		if(!TextUtil.isEmptyOrNull(platFormStr)){
			try{
				platForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				obj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETFANTASYEVENTTEAMS,"Please send valid platform");
				return obj;
			}
		}
		
		String fontSizeStr = TicketUtil.getFontSize(request, platForm);
		
		obj.setBottomText("<font "+fontSizeStr+" ><b>Reward The Fan dollars are not available to purchase or redeem for cash and are only awarded to you, " +
				"THE FAN, as our way of saying thank you for your loyalty.</b></font>");
		obj.setFirstHeader("1 Point = 1$ Dollar");
		obj.setSecondHeader("If your team goes, YOU GO!");
		obj.setFirstImageTxt("<center><font "+fontSizeStr+"><b>1 Point = $1 Dollar <br>towards any future purchase</b></font></center>");
		obj.setFirstImageUrl(MapUtil.getRewardPointsConcertImage());
		obj.setSecondImageTxt("<center><font "+fontSizeStr+">Use your reward points to secure Fantasy Sports Tickets to see your favorite team in events like the Super Bowl " +
				"or World Series. <b>If your team goes, YOU GO! At NO ADDITIONAL COST</b>.</font></center>");
		obj.setSecondImageUrl(MapUtil.getRewardPointsSportsImage());
		obj.setStatus(1);
		return obj;
	}
	
	
	@RequestMapping(value="/GetEarnRewardPointsPageInformtion",method=RequestMethod.POST)
	public @ResponsePayload RewardInformationPage getRewardPointsInformation(HttpServletRequest request, Model model,HttpServletResponse response){
		RewardInformationPage obj =new RewardInformationPage();
		String productTypeStr = request.getParameter("productType");
		String platFormStr = request.getParameter("platForm");
		if(TextUtil.isEmptyOrNull(platFormStr)){
			obj.setStatus(0);
			return obj;
		}
		ApplicationPlatForm platForm = null;
		if(!TextUtil.isEmptyOrNull(platFormStr)){
			try{
				platForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				obj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETFANTASYEVENTTEAMS,"Please send valid platform");
				return obj;
			}
		}
		String fontSizeStr = TicketUtil.getFontSize(request, platForm);
		obj.setFirstHeader("HOW TO EARN REWARD POINTS");
		
		/*Earn reward points on every purchase you make
		PLUS
		Earn reward points on the first purchase of everyone you refer
		AND
		Each reward point is worth $1 towards any future purchase!*/
		
		String text = "<center><font "+fontSizeStr+"><b>Earn reward dollars on every purchase you make" +
				"<br>" +
				"PLUS" +
				"<br>" +
				"Earn reward dollars on the first purchase of everyone you refer" +
				"<br>" +
				"AND" +
				"<br>" +
				"Each reward dollar is worth $1 towards any future purchase!</b></font></center>";
		
		obj.setText1(text);
		obj.setStatus(1);
		return obj;
	}
	
	@RequestMapping(value="/GetFSTPageInformtion",method=RequestMethod.POST)
	public @ResponsePayload FantasyTicketsInformationPage getFSTPageInformation(HttpServletRequest request, Model model,HttpServletResponse response){
		FantasyTicketsInformationPage obj =new FantasyTicketsInformationPage();
		
		
		String productTypeStr = request.getParameter("productType");
		String platFormStr = request.getParameter("platForm");
		if(TextUtil.isEmptyOrNull(platFormStr)){
			obj.setStatus(0);
			return obj;
		}
		ApplicationPlatForm platForm = null;
		if(!TextUtil.isEmptyOrNull(platFormStr)){
			try{
				platForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				obj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETFANTASYEVENTTEAMS,"Please send valid platform");
				return obj;
			}
		}
		String justColor="#2D9DC6";
		String fontSizeStr = TicketUtil.getFontSize(request, platForm);
		String bodyText = "<p><font "+fontSizeStr+" color="+justColor+"><strong>What are Fantasy Sports Tickets?</strong></font></p><br>" +
				"<p><font "+fontSizeStr+">Fantasy Sports Tickets are tickets you secure using our Reward Dollars program to see your " +
				"favorite team in those special \"once in a lifetime\" events like the Super Bowl or World Series, etc.</font></p>" +
				"<p><font "+fontSizeStr+">If your team goes to the big game, so do you - at <strong>NO ADDITIONAL COST!</strong></font></p>"+
				"<p><font "+fontSizeStr+">If your team does not make it, we return your reward dollars back to your account.</font></p>"+
				"<p><font "+fontSizeStr+">Fantasy Sports Tickets gives you, THE FAN, the potential to see your favorite team at the big " +
						"game for FREE- simply by using and referring others to Reward The Fan!</font></p>"+
				"<p><font "+fontSizeStr+">It's that simple!</font></p>"+
				"<p><font "+fontSizeStr+"><strong>Fantasy Sports Tickets may not be purchased or sold for cash and are only available to you, " +
						"THE FAN, by redeeming your REWARD THE FAN DOLLARS.</strong></font></p>";
		obj.setImageURL(MapUtil.getRewardPointsSportsImage());
		obj.setBodyText(bodyText);
		obj.setStatus(1);
		return obj;
	}
	
	
	@RequestMapping(value = "/Faq",method=RequestMethod.POST)
	public @ResponsePayload RTFProductInformation getFaq(HttpServletRequest request,HttpServletResponse response,Model model){
		
		RTFProductInformation RTFProductInfo =new RTFProductInformation();
		Error error = new Error();
		try { 
			
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
            Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				RTFProductInfo.setError(error);
				RTFProductInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
				return RTFProductInfo;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						RTFProductInfo.setError(error);
						RTFProductInfo.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
						return RTFProductInfo;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					RTFProductInfo.setError(error);
					RTFProductInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
					return RTFProductInfo;
				}
			}else{
				error.setDescription("You are not authorized.");
				RTFProductInfo.setError(error);
				RTFProductInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"You are not authorized");
				return RTFProductInfo;
			}

			String productTypeStr = request.getParameter("productType");			
			String pageType = request.getParameter("pageType");
			
			if(TextUtil.isEmptyOrNull(pageType)){
					error.setDescription("Page Type is Mandatory");
					RTFProductInfo.setError(error);
					RTFProductInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"Page Type is Mandatory");
					return RTFProductInfo;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					RTFProductInfo.setError(error);
					RTFProductInfo.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.RTFPRODUCTINFO,"Please send valid product type");
					return RTFProductInfo;
				}
			}
		
		String platFormStr = request.getParameter("platForm");
		//String deviceType = request.getParameter("deviceType");
		ApplicationPlatForm applicationPlatForm=null;
		if(!TextUtil.isEmptyOrNull(platFormStr)){
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				error.setDescription("Please send valid platfrom");
				RTFProductInfo.setError(error);
				RTFProductInfo.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETZONETICKETSINFORMATION,"Please send valid platfrom");
				return RTFProductInfo;
			}
		}
		
		String deviceType = null != request.getParameter("deviceType")?request.getParameter("deviceType"):"";
		String imageSizeStr = "";
		 String fontTagStartStr = "";
		 String fontTagEndStr="";
		 String fontSizeStr="";
		 String headerFontSizeStr="";
		 if(applicationPlatForm.equals(ApplicationPlatForm.IOS)){
				try{
					if(deviceType.equals("IPHONE")){
						imageSizeStr ="width:310px; height:260px;";
						//if(pageType.equals("quizFaq")|| pageType.equals("quizHowToPlay")){
							fontSizeStr ="size='5'";
						//} else {
							//fontSizeStr ="size='4'";	
						//}
							headerFontSizeStr ="size='5'";
						
					}else if(deviceType.equals("IPAD")){
						fontSizeStr ="size='6'";
						headerFontSizeStr ="size='6'";
					}
				}catch (Exception e) {
					//fontSizeStr ="size='4'";
				}
			}
			 if(imageSizeStr.equals("")) {
				 imageSizeStr = "max-width:100%; height:auto;";
			 }
			 if(!fontSizeStr.equals("")) {
				 fontTagStartStr = "<font "+fontSizeStr+">";
				 fontTagEndStr = "</font>";
			 }
		 
			String color = "color:#2D9DC6",style = "justify",justColor="#2D9DC6";
			
			justColor="#000000";
			style = "center";
			fontTagStartStr = "<font "+fontSizeStr+" color="+justColor+">";
			fontTagEndStr = "</font>";
			
			String imageBaseUrl =  AWSFileService.awsS3Url;
					
			List<RTFProductInfoDtls> infoDtlsList = new ArrayList<RTFProductInfoDtls>();
			RTFProductInfoDtls infoDtls = new RTFProductInfoDtls();
			
			String description = "<p align="+style+">"+fontTagStartStr+"<strong>Join our live game!</strong><br/>" +
					"You�ll receive a push notification reminding " +
					"you to play the game. " +
					"Don�t be late or you�ll only be able to watch!"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay1.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Tap your answer!</strong><br/>" +
			"Tap the answer you think is correct " +
			"in under 10 seconds."+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay2.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>One wrong &amp; you're out!</strong><br/>" +
			"If you tap the incorrect answer, " +
			"or run out of time, you�ll be " +
			"eliminated from the game."+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay3.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Use a life to continue playing!</strong><br/>" +
			"If you choose the incorrect answer, you can use " +
			"an extra life to re-enter the game & advance " +
			"to the next question!"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay4.png");
			infoDtlsList.add(infoDtls);

			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Get them all right and you win!</strong><br/>" +
			"If you answer all the questions correctly, you win " +
			"reward dollars. More than one winner? " +
			"Split the prize! Reward Dollars can " +
			"be used to redeem free tickets."+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay5.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Plus, Grand Prize Tickets!</strong><br/>" +
			"In addition to winning Reward Dollars, " +
			"all finalists are entered into our grand " +
			"prize lottery. One lucky winner will " +
			"be chosen at random & receive " +
			"our grand prize tickets!"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay6.png");
			infoDtlsList.add(infoDtls);
			
			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Use Reward Dollars to Redeem Tickets!</strong><br/>" +
			"Reward Dollars can be used to redeem " +
			"tickets on the Reward the Fan app " +
			"or website! "+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay7.png");
			infoDtlsList.add(infoDtls);

			infoDtls = new RTFProductInfoDtls();
			description = "<p align="+style+">"+fontTagStartStr+"<strong>Still have questions?</strong><br/>" +
			"Contact us at: " +
			"care@rewardthefan.com"+fontTagEndStr+"</p>";
			infoDtls.setDescriptionText(description);
			infoDtls.setImageUrl(imageBaseUrl+"howtoplay/howtoplay8.png");
			infoDtlsList.add(infoDtls);
			
			RTFProductInfo.setProductInfoDetails(infoDtlsList);
			 
			RTFProductInfo.setPageType(pageType);
			RTFProductInfo.setStatus(1);
			return RTFProductInfo;
		}catch (Exception e) {
			error.setDescription(e.getMessage());
			RTFProductInfo.setError(error);
			RTFProductInfo.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.RTFPRODUCTINFO,"Exception Occurred");
			return RTFProductInfo;
		}
	}
}

	