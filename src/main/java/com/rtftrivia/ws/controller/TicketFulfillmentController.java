package com.rtftrivia.ws.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.ByteStreams;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentHistory;
import com.paypal.api.payments.RelatedResources;
import com.paypal.api.payments.Sale;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.OAuthTokenCredential;
import com.rtfquiz.webservices.utils.list.CustomerStats;
import com.stripe.Stripe;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Dispute;
import com.stripe.model.DisputeCollection;
import com.stripe.model.Refund;
import com.stripe.model.RefundCollection;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Affiliate;
import com.zonesws.webservices.data.AffiliatePromoCodeTracking;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.AutoCatsLockedTickets;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCardInfo;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderCreditCard;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.CustomerWallet;
import com.zonesws.webservices.data.CustomerWalletHistory;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.InvoiceRefund;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.data.PaypalApi;
import com.zonesws.webservices.data.Property;
import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;
import com.zonesws.webservices.data.RTFPromotionalOfferHdr;
import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.data.ReferralDiscountSettings;
import com.zonesws.webservices.data.StripeDispute;
import com.zonesws.webservices.data.StripeTransaction;
import com.zonesws.webservices.data.WalletTransaction;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CodeType;
import com.zonesws.webservices.enums.FileType;
import com.zonesws.webservices.enums.PartialPaymentMethod;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.enums.PaypalRefundMode;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.PromotionalType;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.enums.WalletTransactionType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.filter.ReferralCodeGenerator;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.ShareDriveUtil;
import com.zonesws.webservices.notifications.RTFOrderDownloadNotification;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.FavoriteUtil;
import com.zonesws.webservices.utils.MapUtil;
import com.zonesws.webservices.utils.PaginationUtil;
import com.zonesws.webservices.utils.RTFAffiliateBrokerUtil;
import com.zonesws.webservices.utils.RTFPromotionalOfferUtil;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.ArtistList;
import com.zonesws.webservices.utils.list.CustomerDetails;
import com.zonesws.webservices.utils.list.InvoiceRefundResponse;
import com.zonesws.webservices.utils.list.ReferralCodeValidation;
import com.zonesws.webservices.utils.list.StripeCredentials;
import com.zonesws.webservices.utils.list.StripeDisputeResponse;
import com.zonesws.webservices.utils.list.TicketDownloadNotification;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

import jcifs.smb.SmbFile;

@Controller
@RequestMapping({"/GetStripeDisputes","/GetInvoiceRefundList","/OrderCancelledNotification","/TicketDownloadNotification",
	"/GetStripeCredentials","/InvoiceRefund","/CustomerWalletTransaction","/GetCustomerCardsList",
	"/ValidatePromoCodeTicTracker","/ValidatePromoCodeTicTrackerLongOrder","/SearchArtistOrTeamByKey",
	"/StripeTransaction","/StripeTransactionLongOrder","/UpdateOneSignalCustomers","/GetOneSignalCustomers"})
public class TicketFulfillmentController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(TicketFulfillmentController.class);
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	
	@RequestMapping(value="/GetInvoiceRefundList",method=RequestMethod.POST)
	public InvoiceRefundResponse getInvoiceRefundList(HttpServletRequest request){
		Error error = new Error();
		InvoiceRefundResponse invoiceRefundResponse = new InvoiceRefundResponse();
		try {
			String startDate = "";
			String endDate = "";
			String limit = "";
			String refundType = request.getParameter("refundType");
			if(TextUtil.isEmptyOrNull(refundType)){
				error.setDescription("Refund Type is mandatory.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETINVOICEREFUNDLIST,"Refund Type is mandatory.");
				return invoiceRefundResponse;
			}
			
			if(refundType.equals(PaymentMethod.CREDITCARD.toString())){
				limit = request.getParameter("limit");
				if(TextUtil.isEmptyOrNull(limit)){
					error.setDescription("Limit Parameter is mandatory.");
					invoiceRefundResponse.setError(error);
					invoiceRefundResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETINVOICEREFUNDLIST,"Limit Parameter is mandatory.");
					return invoiceRefundResponse;
				}
			}else if(refundType.equals(PaymentMethod.PAYPAL.toString())){
				startDate = request.getParameter("startDate");
				if(TextUtil.isEmptyOrNull(startDate)){
					error.setDescription("Start Date is mandatory.");
					invoiceRefundResponse.setError(error);
					invoiceRefundResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PAYPALREFUNDTRANLIST,"Refund Type is mandatory.");
					return invoiceRefundResponse;
				}
				
				endDate = request.getParameter("endDate");
				if(TextUtil.isEmptyOrNull(endDate)){
					error.setDescription("End Date is mandatory.");
					invoiceRefundResponse.setError(error);
					invoiceRefundResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.PAYPALREFUNDTRANLIST,"Refund Amount is mandatory.");
					return invoiceRefundResponse;
				}
			}
			
			if(refundType.equals(PaymentMethod.CREDITCARD.toString())) {
				invoiceRefundResponse = getStripRefunds(limit, request);
			}
				
		//	else invoiceRefundResponse = getPaypalRefunds(startDate, endDate, request);
				
			return invoiceRefundResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting Stripe refunds");
			invoiceRefundResponse.setError(error);
			invoiceRefundResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETINVOICEREFUNDLIST,"Error occured while gettting Stripe refunds");
			return invoiceRefundResponse;
		}
	}
	
	
	@RequestMapping(value="/GetStripeDisputes",method=RequestMethod.POST)
	public StripeDisputeResponse getStripeDisputes(HttpServletRequest request){
		Error error = new Error();
		StripeDisputeResponse disputeResponse = new StripeDisputeResponse();
		try {
			String limit = request.getParameter("limit");
			String configIdString = request.getParameter("configId");
			Integer lim = null;
			if(TextUtil.isEmptyOrNull(limit)){
				error.setDescription("Limit is Mendatory");
				disputeResponse.setError(error);
				disputeResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETINVOICEREFUNDLIST,"Limit is Mendatory");
				return disputeResponse;
			}
			if(TextUtil.isEmptyOrNull(configIdString)){
				error.setDescription("You are not Authorized");
				disputeResponse.setError(error);
				disputeResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETINVOICEREFUNDLIST,"You are not Authorized");
				return disputeResponse;
			}
			
			try {
				lim = Integer.parseInt(limit);
			} catch (Exception e) {
				e.printStackTrace();
				error.setDescription("Limit is not valid Integer value");
				disputeResponse.setError(error);
				disputeResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETINVOICEREFUNDLIST,"Limit is not valid Integer value");
				return disputeResponse;
			}
			
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			Stripe.apiKey = key;
			
			Map<String, Object> disputeParams = new HashMap<String, Object>();
			disputeParams.put("limit",lim);
			DisputeCollection disputes =  Dispute.list(disputeParams);
			List<StripeDispute> disputeList = new ArrayList<StripeDispute>();
			if(disputes!=null && disputes.getData()!=null && !disputes.getData().isEmpty()){
				List<Dispute> list = disputes.getData();
				StripeDispute dispute = null;
				for(Dispute dip : list){
					dispute = new StripeDispute();
					Double amt = dip.getAmount()/100.00;
					dispute.setAmount(amt);
					dispute.setCharge(dip.getCharge());
					dispute.setCreated(new Date(dip.getCreated() * 1000));
					dispute.setCurrency(dip.getCurrency());
					dispute.setId(dip.getId());
					dispute.setIsChargeRefundable(dip.getIsChargeRefundable());
					dispute.setReason(dip.getReason());
					dispute.setStatus(dip.getStatus());
					if(dip.getEvidenceSubObject()!=null){
						dispute.setCustomerEmailAddress(dip.getEvidenceSubObject().getCustomerEmailAddress());
						dispute.setCustomerPurchaseIp(dip.getEvidenceSubObject().getCustomerPurchaseIp());
						dispute.setDuplicateChargeId(dip.getEvidenceSubObject().getDuplicateChargeId());
					}
					disputeList.add(dispute);
				}
			}
			disputeResponse.setDisputes(disputeList);
			disputeResponse.setStatus(1);
			return disputeResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting Stripe refunds");
			disputeResponse.setError(error);
			disputeResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETINVOICEREFUNDLIST,"Error occured while gettting Stripe refunds");
			return disputeResponse;
		}
	}
	

	
	public InvoiceRefundResponse getStripRefunds(String limit,HttpServletRequest request){
		Error error = new Error();
		InvoiceRefundResponse invoiceRefundResponse = new InvoiceRefundResponse();
		try {
			
			Map<String, Object> refundParams = new HashMap<String, Object>();
			refundParams.put("limit", limit);
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			Stripe.apiKey = key;
			RefundCollection refunds = Refund.list(refundParams);
			List<InvoiceRefund> invoiceRefundList = new ArrayList<InvoiceRefund>();
			if(refunds!=null && !refunds.getData().isEmpty()){
				List<Refund> refundList  = refunds.getData();
				InvoiceRefund invoiceRefund = null;
				for(Refund refund : refundList){
					invoiceRefund = new InvoiceRefund();
					Double amt = refund.getAmount()/100.00;
					invoiceRefund.setAmount(amt);
					invoiceRefund.setReason(refund.getReason());
					invoiceRefund.setRefundId(refund.getId());
					invoiceRefund.setStatus(refund.getStatus());
					invoiceRefund.setTransactionId(refund.getCharge());
					invoiceRefund.setRefundType(PaymentMethod.CREDITCARD.toString());
					invoiceRefund.setCreatedTime(new Date(refund.getCreated() * 1000));
					invoiceRefundList.add(invoiceRefund);
				}
			}
			invoiceRefundResponse.setStripeRefunds(invoiceRefundList);
			invoiceRefundResponse.setStatus(1);
			return invoiceRefundResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting Stripe refunds");
			invoiceRefundResponse.setError(error);
			invoiceRefundResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTRIPEREFUNDS,"Error occured while gettting Stripe refunds");
			return invoiceRefundResponse;
		}
	}
	
	public InvoiceRefundResponse createStripRefund(String orderId,String refundAmount,String refundType,HttpServletRequest request){
		Error error = new Error();
		InvoiceRefundResponse invoiceRefundResponse = new InvoiceRefundResponse();
		try {			
			CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(Integer.parseInt(orderId));
			if(order==null){
				error.setDescription("Order not found.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATESTRIPEREFUND,"Order not found.");
				return invoiceRefundResponse;
			}
			
			String charge = "";
			if((order.getPrimaryPaymentMethod().equals(PaymentMethod.CREDITCARD) || order.getPrimaryPaymentMethod().equals(PaymentMethod.GOOGLEPAY) 
					|| order.getPrimaryPaymentMethod().equals(PaymentMethod.IPAY)) && order.getPrimaryTransactionId()!=null
					&& !order.getPrimaryTransactionId().isEmpty()){
				charge = order.getPrimaryTransactionId();
			}else if(order.getSecondaryPaymentMethod() != null && (order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) 
					 || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.IPAY))
					&& order.getSecondaryTransactionId()!=null && !order.getSecondaryTransactionId().isEmpty()){
				charge = order.getSecondaryTransactionId();
			}else if(order.getThirdPaymentMethod()!=null && (order.getThirdPaymentMethod().equals(PartialPaymentMethod.CREDITCARD) 
					 || order.getThirdPaymentMethod().equals(PartialPaymentMethod.GOOGLEPAY) || order.getThirdPaymentMethod().equals(PartialPaymentMethod.IPAY))
					&& order.getThirdTransactionId()!=null && !order.getThirdTransactionId().isEmpty()){
				charge = order.getThirdTransactionId();
			}
			
			if(TextUtil.isEmptyOrNull(charge)){
				error.setDescription("No Creditcard transaction found on given order.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATESTRIPEREFUND,"No Creditcard transaction found on given order.");
				return invoiceRefundResponse;
			}
			
			Map<String, Object> refundParams = new HashMap<String, Object>();
			refundParams.put("charge", charge);
			
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			Stripe.apiKey = key;
			Refund refund = null;
			
			Double amount = Double.parseDouble(refundAmount);
			amount  = TicketUtil.getRoundedValue(amount);
			amount  = amount*100;
			refundParams.put("amount", amount.intValue());
			refund = Refund.create(refundParams);
			
			InvoiceRefund stripeRefund = null;
			if(refund!=null && refund.getStatus().equals("succeeded")){
				stripeRefund = new InvoiceRefund();
				Double amt = (refund.getAmount()/100.00);
				stripeRefund.setOrderId(order.getId());
				stripeRefund.setAmount(amt);
				stripeRefund.setReason(refund.getReason());
				stripeRefund.setRefundId(refund.getId());
				stripeRefund.setStatus(refund.getStatus());
				stripeRefund.setTransactionId(refund.getCharge());
				stripeRefund.setRefundType(PaymentMethod.CREDITCARD.toString());
				stripeRefund.setCreatedTime(new Date(TimeUnit.SECONDS.toMillis(refund.getCreated())));
			}
			invoiceRefundResponse.setInvoiceRefund(stripeRefund);
			invoiceRefundResponse.setStatus(1);
			return invoiceRefundResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting Stripe refunds");
			invoiceRefundResponse.setError(error);
			invoiceRefundResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTRIPEREFUNDS,"Error occured while gettting Stripe refunds");
			return invoiceRefundResponse;
		}
	}
	
	
	public InvoiceRefundResponse getPaypalRefunds(String startDate, String endDate, HttpServletRequest request){
		InvoiceRefundResponse response = new InvoiceRefundResponse();
		Error error = new Error();
		
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						response.setError(error);
						response.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUNDTRANLIST,"You are not authorized");
						return response;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					response.setError(error);
					response.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUNDTRANLIST,"You are not authorized");
					return response;
				}
			}else{
				error.setDescription("You are not authorized.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUNDTRANLIST,"You are not authorized");
				return response;
			}
			
			String paypalTransactionEnv = URLUtil.paypalEnvironment;
			Map<String, String> sdkConfig = new HashMap<String, String>();
			sdkConfig.put("mode", paypalTransactionEnv);
			
			String accessToken = "";
			List<PaypalApi> paypalApiList = DAORegistry.getQueryManagerDAO().getPaypalCredentialByType();
			for(PaypalApi apiKey : paypalApiList){
				if(apiKey.getCredentialType().equalsIgnoreCase(paypalTransactionEnv)){
					accessToken = new OAuthTokenCredential(apiKey.getClientId(),
							apiKey.getSecretKey(), sdkConfig).getAccessToken();
					break;
				}
			  }
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
			SimpleDateFormat zoneDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			zoneDateFormatter.setTimeZone(TimeZone.getTimeZone("EST"));
			
			Date startDateParsed = dateFormatter.parse(startDate);
			startDate = zoneDateFormatter.format(startDateParsed);
			
			Date endDateParsed = dateFormatter.parse(endDate);
			endDate = zoneDateFormatter.format(endDateParsed);
			
			APIContext apiContext = new APIContext(accessToken);
			apiContext.setConfigurationMap(sdkConfig);
			
			//paypal search parameters needs to be added in below map
			Map<String, String> containerMap = new HashMap<String, String>();
			containerMap.put("count", "100");
			containerMap.put("start_time", startDate);
			containerMap.put("end_time", endDate);
			
			PaymentHistory paymentHistorypaymentHistory = Payment.list(apiContext,
					containerMap);		
			
			List<InvoiceRefund> invoiceRefundList = new ArrayList<InvoiceRefund>();
			if(paymentHistorypaymentHistory != null){
				for(Payment payment : paymentHistorypaymentHistory.getPayments()){
					InvoiceRefund invoiceRefund = new InvoiceRefund();
					for(Transaction transaction : payment.getTransactions()){
						for(RelatedResources relatedResources : transaction.getRelatedResources()){
							com.paypal.api.payments.Refund refund = relatedResources.getRefund();
							if(refund != null){
								invoiceRefund.setAmount(Double.valueOf(refund.getAmount().getTotal()));
								invoiceRefund.setRefundId(refund.getId());
								invoiceRefund.setTransactionId(payment.getId());
								invoiceRefund.setStatus(refund.getState());
								invoiceRefund.setRefundType(PaymentMethod.PAYPAL.toString());
								invoiceRefund.setCreatedTime(zoneDateFormatter.parse(refund.getCreateTime()));
								invoiceRefundList.add(invoiceRefund);
								break;
							}
						}
						break;
					}
				}
			}
			
			response.setPaypalRefunds(invoiceRefundList);
			response.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUNDTRANLIST,"Success");
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while doing paypal Refund");
			response.setError(error);
			response.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUNDTRANLIST,"Error occured while doing paypal Refund");
			return response;
		}
	}
	
	
	
	@RequestMapping(value="/TicketDownloadNotification",method=RequestMethod.POST)
	public @ResponsePayload TicketDownloadNotification triggerTicketDownloadNotification(HttpServletRequest request,HttpServletResponse response){
		Error error = new Error();
		TicketDownloadNotification invoiceRefundResponse = new TicketDownloadNotification();
		try {
			
			String orderIdStr = request.getParameter("orderId");
			String attachTicketStr = request.getParameter("attachTicket");
			String fileSizeStr = request.getParameter("fileSize");
			String uploadSizeStr = request.getParameter("uploadSize");
			String fileTypeStr = request.getParameter("fileType");
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is mandatory.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Order No is mandatory.");
				return invoiceRefundResponse;
			}
			
			if(TextUtil.isEmptyOrNull(fileTypeStr)){
				error.setDescription("File Type is Mendatory.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"File Type is Mendatory.");
				return invoiceRefundResponse;
			}
			FileType fileType = null;
			try {
				fileType = FileType.valueOf(fileTypeStr);
			} catch (Exception e) {
				error.setDescription("Invalid File Type.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Invalid File Type.");
				return invoiceRefundResponse;
			}
			
			
			if(TextUtil.isEmptyOrNull(attachTicketStr)){
				attachTicketStr = "false";
			}
			
			Integer orderId = null;
			CustomerOrder order = null;
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
				order = DAORegistry.getCustomerOrderDAO().get(orderId);
			}catch(Exception e){
				error.setDescription("Order No is not valid");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Order No is not valid");
				return invoiceRefundResponse;
			}
			Integer fileSize = 0;
			if(!TextUtil.isEmptyOrNull(fileSizeStr)){
				try{
					fileSize = Integer.parseInt(fileSizeStr.trim());
				}catch(Exception e){
					error.setDescription("File Size is not valid Integer");
					invoiceRefundResponse.setError(error);
					invoiceRefundResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"File Size is not valid Integer");
					return invoiceRefundResponse;
				}
			}
			
			Integer uploadSize = 0;
			if(!TextUtil.isEmptyOrNull(uploadSizeStr)){
				try{
					uploadSize = Integer.parseInt(uploadSizeStr.trim());
				}catch(Exception e){
					error.setDescription("Upload File Size is not valid Integer");
					invoiceRefundResponse.setError(error);
					invoiceRefundResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Upload File Size is not valid Integer");
					return invoiceRefundResponse;
				}
			}
			
			
			Customer customer = CustomerUtil.getCustomerById(order.getCustomer().getId());
			
			if(customer == null){
				error.setDescription("Order No is not valid");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Order No is not valid");
				return invoiceRefundResponse;
			}
			
			if(uploadSize > 0){
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				List<MultipartFile> files = new ArrayList<MultipartFile>();
				for(int i=1;i<=uploadSize;i++){
					files.add(multipartRequest.getFile("upload_"+i));
				}
				if(null != files && !files.isEmpty()){
					for (MultipartFile multipartFile : files) {
						String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
						String fileName = FilenameUtils.getBaseName(multipartFile.getOriginalFilename());
						File newFile = new File(URLUtil.getSVGMapPath()+"Tickets//"+fileName+"."+ext);
						newFile.createNewFile();
						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
				        FileCopyUtils.copy(multipartFile.getInputStream(), stream);
						stream.close();
					}
				}
			}
			
			Boolean attachTickets = Boolean.valueOf(attachTicketStr);
			MailAttachment[] ticketAttachments =null;
			if(attachTickets){
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				List<MultipartFile> files = new ArrayList<MultipartFile>();
				for(int i=1;i<=fileSize;i++){
					files.add(multipartRequest.getFile("file_"+i));
				}
				if(null == files || files.isEmpty()){
					error.setDescription("Please attach files.");
					invoiceRefundResponse.setError(error);
					invoiceRefundResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Please attach files.");
					return invoiceRefundResponse;
				}
				
				Map<String, List<String>> filesMap = new HashMap<String, List<String>>();
				int ticketAttachementSize = 0;
				if(files != null && files.size() > 0) {
					for (MultipartFile multipartFile : files) {
						if(URLUtil.isSharedDriveOn) {
							String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
							String filePath = Util.getTicketDownloadFilePath(multipartFile.getOriginalFilename());
							BufferedOutputStream stream = null;
							try {
								SmbFile file = ShareDriveUtil.getSmbFile(filePath);
								stream = new BufferedOutputStream(file.getOutputStream());
								FileCopyUtils.copy(multipartFile.getInputStream(), stream);
								
								List<String> filePaths= filesMap.get(multipartFile.getContentType());
								if(null != filePaths && !filePaths.isEmpty()){
									filePaths.add(filePath);
								}else{
									filePaths = new ArrayList<String>();
									filePaths.add(filePath);
								}
								filesMap.put(multipartFile.getContentType(), filePaths);
								ticketAttachementSize++;
								
							}catch(Exception e) {
								e.printStackTrace();
							}finally {
								if(null != stream) {
									stream.close();
								}
							}
						}else {
							String ext = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
							
							String filePath = Util.getTicketDownloadFilePath(multipartFile.getOriginalFilename());
							
							File newFile = new File(filePath);
							newFile.createNewFile();
							BufferedOutputStream stream = new BufferedOutputStream(
									new FileOutputStream(newFile));
					        FileCopyUtils.copy(multipartFile.getInputStream(), stream);
							stream.close();
							List<String> filePaths= filesMap.get(multipartFile.getContentType());
							if(null != filePaths && !filePaths.isEmpty()){
								filePaths.add(filePath);
							}else{
								filePaths = new ArrayList<String>();
								filePaths.add(filePath);
							}
							filesMap.put(multipartFile.getContentType(), filePaths);
							ticketAttachementSize++;
						}
						
					}
				}
				
				 if(filesMap!= null && !filesMap.isEmpty()){
					 ticketAttachments = new MailAttachment[ticketAttachementSize];
					 int i=0;
					 for (String mimeType : filesMap.keySet()) {
						List<String> filePaths= filesMap.get(mimeType);
						for (String filePath : filePaths) {
							
							if(URLUtil.isSharedDriveOn) {
								SmbFile file = ShareDriveUtil.getSmbFile(filePath);
								BufferedInputStream stream = new BufferedInputStream(file.getInputStream());
								byte[] bytes = ByteStreams.toByteArray(stream);
								String fileName = Util.getFileNameFromPath(filePath);
								String ext = FilenameUtils.getExtension(fileName);
								ticketAttachments[i] = new MailAttachment(bytes,mimeType,"Tickets"+(i+1)+"."+ext,filePath);
								i++;
							}else {
								BufferedInputStream stream = new BufferedInputStream(new FileInputStream(filePath));
								byte[] bytes = ByteStreams.toByteArray(stream);
								String fileName = Util.getFileNameFromPath(filePath);
								String ext = FilenameUtils.getExtension(fileName);
								ticketAttachments[i] = new MailAttachment(bytes,mimeType,"Tickets"+(i+1)+"."+ext,filePath);
								i++;
							}
						} 
					 }
				 }
			}
			
			RTFOrderDownloadNotification.trigger(order, customer,ticketAttachments,fileType,false);
			invoiceRefundResponse.setMessage("Success");
			invoiceRefundResponse.setStatus(1);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Success");
			return invoiceRefundResponse;
		} catch (Exception e) {
			System.out.println("Ticket Upload is failed");
			e.printStackTrace();
			invoiceRefundResponse.setMessage("Fail");
			invoiceRefundResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Ticket Upload is failed");
			return invoiceRefundResponse;
		}
	}
	
	@RequestMapping(value="/OrderCancelledNotification",method=RequestMethod.POST)
	public TicketDownloadNotification orderCancelledNotification(HttpServletRequest request,HttpServletResponse response){
		Error error = new Error();
		TicketDownloadNotification invoiceRefundResponse = new TicketDownloadNotification();
		try {
			
			String orderIdStr = request.getParameter("orderNo");
			
			if(TextUtil.isEmptyOrNull(orderIdStr)){
				error.setDescription("Order No is mandatory.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Order No is mandatory.");
				return invoiceRefundResponse;
			}
			
			Integer orderId = null;
			CustomerOrder order = null;
			try{
				orderId = Integer.parseInt(orderIdStr.trim());
				order = DAORegistry.getCustomerOrderDAO().get(orderId);
			}catch(Exception e){
				error.setDescription("Order No is not valid");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Order No is not valid");
				return invoiceRefundResponse;
			}
			
			Customer customer = CustomerUtil.getCustomerById(order.getCustomer().getId());
			
			if(customer == null){
				error.setDescription("Order No is not valid");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.DOWNLOADNOTIFICATION,"Order No is not valid");
				return invoiceRefundResponse;
			}
			
			
			String zones = order.getSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
			String svgWebViewUrl = MapUtil.getSvgWebViewForOrder(order.getVenueId(), order.getVenueCategory(),zones,order.getId(),false);
			Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderId);  
			CustomerOrderDetail orderDetails = DAORegistry.getCustomerOrderDetailDAO().getCustomerOrderDetailByOrderId(order.getId());
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("customer", customer);  
			mailMap.put("customerOrder", order);
			mailMap.put("venueMap", svgWebViewUrl);
			mailMap.put("orderTotal", TicketUtil.getRoundedValueString(invoice.getRefundedAmount()));
			MailAttachment[] mailAttachment = Util.getOrderConfirmationEmailAttachmentImages();
			String email=null;
			if(orderDetails.getShippingEmail()!=null && !orderDetails.getShippingEmail().isEmpty()){
				email = orderDetails.getShippingEmail();
			}else if(orderDetails.getBillingEmail()!=null && !orderDetails.getBillingEmail().isEmpty()){
				email = orderDetails.getBillingEmail();
			}else{
				email =customer.getEmail();
			}
			try{
				mailManager.sendMailNow("text/html",URLUtil.fromEmail,email, 
						null, URLUtil.fromEmail+","+URLUtil.bccEmails, 
						"Your order has been cancelled",
						"mail-cancel-customer-order.html", mailMap, "text/html", null,mailAttachment,null);
			}catch(Exception e) {
				error.setDescription("Error Occured while sending mail to customer.");
				invoiceRefundResponse.setError(error);
				invoiceRefundResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Error Occured while sending mail to customer.");
				return invoiceRefundResponse;
			}
			invoiceRefundResponse.setMessage("Success");
			invoiceRefundResponse.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.CANCELORDER,"Success");
			return invoiceRefundResponse;
		} catch (Exception e) {
			e.printStackTrace();
			invoiceRefundResponse.setMessage("Success");
			invoiceRefundResponse.setStatus(1);
			return invoiceRefundResponse;
		}
	}
	

	@RequestMapping(value="/GetStripeCredentials",method=RequestMethod.POST)
	public @ResponsePayload StripeCredentials GetStripeCredentials(HttpServletRequest request, Model model,HttpServletResponse response){
		StripeCredentials stripeCredentials =new StripeCredentials();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						stripeCredentials.setError(error);
						stripeCredentials.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTRIPECREDENTIALS,"You are not authorized");
						return stripeCredentials;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					stripeCredentials.setError(error);
					stripeCredentials.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTRIPECREDENTIALS,"You are not authorized");
					return stripeCredentials;
				}
			}else{
				error.setDescription("You are not authorized.");
				stripeCredentials.setError(error);
				stripeCredentials.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTRIPECREDENTIALS,"You are not authorized");
				return stripeCredentials;
			}
			stripeCredentials= DAORegistry.getQueryManagerDAO().getStripeCredentials();
			stripeCredentials.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTRIPECREDENTIALS,"Success");
			return stripeCredentials;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting stripe credentials informations");
			stripeCredentials.setError(error);
			stripeCredentials.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTRIPECREDENTIALS,"Error occured while gettting stripe credentials informations");
			return stripeCredentials;
		}
	}
	
	@RequestMapping(value="/InvoiceRefund",method=RequestMethod.POST)
	public InvoiceRefundResponse invoiceRefund(HttpServletRequest request,HttpServletResponse reponse){
		InvoiceRefundResponse response = new InvoiceRefundResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						response.setError(error);
						response.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.INVOICEREFUND,"You are not authorized");
						return response;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					response.setError(error);
					response.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.INVOICEREFUND,"You are not authorized");
					return response;
				}
			}else{
				error.setDescription("You are not authorized.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.INVOICEREFUND,"You are not authorized");
				return response;
			}
			
			String orderId = request.getParameter("orderId");
			if(TextUtil.isEmptyOrNull(orderId)){
				error.setDescription("OrderId is mandatory.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.INVOICEREFUND,"OrderId is mandatory.");
				return response;
			}
			

			String refundType = request.getParameter("refundType");
			if(TextUtil.isEmptyOrNull(refundType)){
				error.setDescription("Refund Type is mandatory.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.INVOICEREFUND,"Refund Type is mandatory.");
				return response;
			}
			
			String refundAmount = request.getParameter("refundAmount");
			if(TextUtil.isEmptyOrNull(refundAmount)){
				error.setDescription("Refund Amount is mandatory.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.INVOICEREFUND,"Refund Amount is mandatory.");
				return response;
			}
			
			if(refundType.equals(PaymentMethod.CREDITCARD.toString()))
				response = createStripRefund(orderId, refundAmount, refundType, request);
			else
				response = createPaypalRefund(orderId, refundAmount, refundType, request);
				
			return response;
			
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while doing paypal Refund");
			response.setError(error);
			response.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.INVOICEREFUND,"Error occured while doing paypal Refund");
			return response;
		}
	}
	

	
	public InvoiceRefundResponse createPaypalRefund(String orderId,String refundAmount,String refundType,HttpServletRequest request){
		InvoiceRefundResponse response = new InvoiceRefundResponse();
		Error error = new Error();
		try {			
			CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(Integer.parseInt(orderId));
			if(order==null){
				error.setDescription("Order not found.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUND,"Order not found.");
				return response;
			}
			
			String transactionId = "";
			if(order.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL) && order.getPrimaryTransactionId()!=null
					&& !order.getPrimaryTransactionId().isEmpty()){
				transactionId = order.getPrimaryTransactionId();
			}else if(order.getSecondaryPaymentMethod()!=null && order.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL) 
					&& order.getSecondaryTransactionId()!=null && !order.getSecondaryTransactionId().isEmpty()){
				transactionId = order.getSecondaryTransactionId();
			}else if(order.getThirdPaymentMethod() != null && order.getThirdPaymentMethod().equals(PartialPaymentMethod.PAYPAL) 
					&& order.getThirdTransactionId()!=null && !order.getThirdTransactionId().isEmpty()){
				transactionId = order.getThirdTransactionId();
			}
			
			if(TextUtil.isEmptyOrNull(transactionId)){
				error.setDescription("No Paypal transaction found on given order.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUND,"No Creditcard transaction found on given order.");
				return response;
			}
			
			String paypalRefundMode = "";
			
			if(order.getOrderTotalAsDouble() == Double.parseDouble(refundAmount)){
				paypalRefundMode = PaypalRefundMode.Full.toString();
			}else{
				paypalRefundMode = PaypalRefundMode.Partial.toString();
			}
			
			String paypalTransactionEnv = URLUtil.paypalEnvironment;
			Map<String, String> sdkConfig = new HashMap<String, String>();
			sdkConfig.put("mode", paypalTransactionEnv);
			
			String accessToken = "";
			List<PaypalApi> paypalApiList = DAORegistry.getQueryManagerDAO().getPaypalCredentialByType();
			for(PaypalApi apiKey : paypalApiList){
				if(apiKey.getCredentialType().equalsIgnoreCase(paypalTransactionEnv)){
					accessToken = new OAuthTokenCredential(apiKey.getClientId(),
							apiKey.getSecretKey(), sdkConfig).getAccessToken();
					break;
				}
			  }
			
			APIContext apiContext = new APIContext(accessToken);
			apiContext.setConfigurationMap(sdkConfig);
			SimpleDateFormat zoneDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			zoneDateFormatter.setTimeZone(TimeZone.getTimeZone("EST"));
			Payment payment = Payment.get(apiContext,transactionId);
			
			try{
				for(Transaction transaction : payment.getTransactions()){
					for(RelatedResources relatedResources : transaction.getRelatedResources()){
						if(relatedResources != null && relatedResources.getSale() != null){
							transactionId = relatedResources.getSale().getId();
							break;
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Not able to find saleId by paymentId.");
				response.setError(error);
				response.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUND,"Error occured while doing paypal Refund");
				return response;
			}
			
			com.paypal.api.payments.Refund refund = new com.paypal.api.payments.Refund();

			Amount paypalAmount = new Amount();
			paypalAmount.setTotal(refundAmount);
			paypalAmount.setCurrency("USD");
			refund.setAmount(paypalAmount);
			
			Sale sale = new Sale();
			sale.setPaymentMode(paypalRefundMode);
			sale.setId(transactionId);
			
		    // Refund sale
		    sale.refund(apiContext, refund);
		    
		    ObjectMapper mapper = new ObjectMapper();
		    JsonNode responseObj = mapper.readTree(Sale.getLastResponse());
		    InvoiceRefund paypalRefund = new InvoiceRefund();
		    paypalRefund.setOrderId(order.getId());
		    paypalRefund.setRefundType(PaymentMethod.PAYPAL.toString());
		    paypalRefund.setRefundId(responseObj.path("id").asText());
		    paypalRefund.setStatus(responseObj.path("state").asText());
		    paypalRefund.setTransactionId(responseObj.path("sale_id").asText());
		    JsonNode amountNode = responseObj.path("amount");
		    paypalRefund.setAmount(Double.parseDouble(amountNode.path("total").asText()));
		    paypalRefund.setCreatedTime(new Date());
		    
			/*SimpleDateFormat zoneDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			zoneDateFormatter.setTimeZone(TimeZone.getTimeZone("EST"));*/
		    
			response.setInvoiceRefund(paypalRefund);
			response.setMessage("Paypal Refund transaction successfully initiated.");
			response.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUND,"Success");
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while doing paypal Refund");
			response.setError(error);
			response.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.PAYPALREFUND,"Error occured while doing paypal Refund");
			return response;
		}
	}	
	
	@RequestMapping(value="/CustomerWalletTransaction",method=RequestMethod.POST)
	public WalletTransaction walletTransaction(HttpServletRequest request){
		Error error = new Error();
		WalletTransaction transaction = new WalletTransaction();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						transaction.setError(error);
						transaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
						return transaction;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					transaction.setError(error);
					transaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
					return transaction;
				}
			}else{
				error.setDescription("You are not authorized.");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
				return transaction;
			}
			
			String platForm = request.getParameter("platForm");
			String productTypeStr = request.getParameter("productType");
			String transactionAmountStr = request.getParameter("transactionAmount");
			String rtfCustIdStr=request.getParameter("customerId");
			String orderIdStr=request.getParameter("orderId");
			String transactionTypeStr=request.getParameter("transactionType");
			String userName=request.getParameter("userName");
			
			System.out.println("=================================WALLET TRANSACTION BEGINS===============================");
			System.out.println("--------------------Param-------------------------");
			System.out.println("platForm ---->"+platForm);
			System.out.println("productTypeStr ---->"+productTypeStr);
			System.out.println("transactionAmountStr ---->"+transactionAmountStr);
			System.out.println("rtfCustIdStr ---->"+rtfCustIdStr);
			System.out.println("transactionTypeStr ---->"+transactionTypeStr);
			System.out.println("orderIdStr ---->"+orderIdStr);
			System.out.println("----------------------Param-----------------------");
			
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Application Platform is mandatory.");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Application Platform is mandatory");
				return transaction;
			}
			
			ApplicationPlatForm appPlatForm = null;
			try{
				appPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid application platform");
				return transaction;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Product Type is mandatory");
				return transaction;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					transaction.setError(error);
					transaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid product type");
					return transaction;
				}
			}
			
			if(TextUtil.isEmptyOrNull(transactionTypeStr)){
				error.setDescription("Invalid Transaction Type");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Type");
				return transaction;
			}
			
			WalletTransactionType transactionType = null;
			try{
				transactionType = WalletTransactionType.valueOf(transactionTypeStr);
			}catch(Exception e){
				error.setDescription("Invalid Transaction Type");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Type");
				return transaction;
			}
			
			if(TextUtil.isEmptyOrNull(transactionAmountStr)){
				error.setDescription("Invalid Transaction Amount");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Amount");
				return transaction;
			}
			Double transactionAmount = Double.parseDouble(transactionAmountStr);
			
			if(transactionAmount <= 0){
				error.setDescription("Invalid Transaction Amount. Minimum Amount should be greater than 0");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Amount. Minimum Amount should be greater than 0");
				return transaction;
			}
			
			if(TextUtil.isEmptyOrNull(rtfCustIdStr)){
				error.setDescription("Customer Id is mandatory");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Customer Id is mandatory");
				return transaction;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(rtfCustIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					transaction.setError(error);
					transaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid Customer Id");
					return transaction;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				transaction.setError(error);
				transaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid Customer Id");
				return transaction;
			}
			
			
			CustomerWallet customerWallet = DAORegistry.getCustomerWalletDAO().getCustomerWalletByCustomerId(customerId);
			CustomerWalletHistory customerWalletHistory = null;
			Property property = null;
			
			switch (transactionType) {
			
				case CREDIT:
					
					if(TextUtil.isEmptyOrNull(orderIdStr)){
						error.setDescription("Invalid Order No");
						transaction.setError(error);
						transaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Order No");
						return transaction;
					}
					
					try{
						
						CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(Integer.parseInt(orderIdStr.trim()));
						
						if(null == customerOrder){
							error.setDescription("Invalid Order No");
							transaction.setError(error);
							transaction.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Order No");
							return transaction;
						}
					}catch(Exception e){
						error.setDescription("Invalid Order No");
						transaction.setError(error);
						transaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Order No");
						return transaction;
					}
					
					if(null == customerWallet){
						customerWallet = new CustomerWallet();	
						customerWallet.setCustomerId(customerId);
						customerWallet.setActiveCredit(transactionAmount);
						customerWallet.setTotalCredit(transactionAmount);
						customerWallet.setTotalUsedCredit(0.00);
						customerWallet.setPendingDebit(0.00);
					}else{
						customerWallet.setActiveCredit(customerWallet.getActiveCredit()+transactionAmount);
						customerWallet.setTotalCredit(customerWallet.getTotalCredit()+transactionAmount);
					}
					
					customerWallet.setLastUpdated(new Date());
					
					
					customerWalletHistory = new CustomerWalletHistory();
					customerWalletHistory.setCreatedOn(new Date());
					customerWalletHistory.setCreditedBy(userName);
					customerWalletHistory.setCustomerId(customerId);
					customerWalletHistory.setOrderId(Integer.parseInt(orderIdStr.trim()));
					customerWalletHistory.setTransactionType("CREDIT");
					customerWalletHistory.setTransactionAmount(transactionAmount);
					
					property = DAORegistry.getPropertyDAO().get("rtf.customer.wallet.trx.credit.id");
					Integer trxGenCount = Integer.parseInt(property.getValue());
					
					customerWalletHistory.setTransactionId(ReferralCodeGenerator.generateCustWalletCreditTrxId(customerId, trxGenCount, PaginationUtil.walletCreditTrxPrefix));
					customerWalletHistory.setTransactionStatus("APPROVED");
					customerWalletHistory.setUpdatedOn(new Date());
					
					trxGenCount++;
					property.setValue(String.valueOf(trxGenCount));
					
					break;
					
				case DEBIT:
					
					if(null == customerWallet){
						error.setDescription("No money on customer wallet");
						transaction.setError(error);
						transaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"No money on customer wallet");
						return transaction;
					}
					
					if(transactionAmount > customerWallet.getActiveCredit()){
						error.setDescription("Payment is declined. Insufficient amount in your wallet. Your active wallet is "+customerWallet.getActiveCredit()+".");
						transaction.setError(error);
						transaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.CREATEORDER,"No money on customer wallet");
						return transaction;
					}
					Integer orderId = -1;
					if(!TextUtil.isEmptyOrNull(orderIdStr)){
						orderId = Integer.parseInt(orderIdStr.trim());
					}
					
					customerWallet.setPendingDebit(transactionAmount);
					customerWallet.setActiveCredit(customerWallet.getActiveCredit() - transactionAmount );
					customerWallet.setTotalUsedCredit(customerWallet.getTotalUsedCredit() + transactionAmount);
					customerWallet.setLastUpdated(new Date());
					
					customerWalletHistory = new CustomerWalletHistory();
					customerWalletHistory.setCreatedOn(new Date());
					customerWalletHistory.setCreditedBy("RewardTheFan");
					customerWalletHistory.setCustomerId(customerId);
					customerWalletHistory.setOrderId(orderId);
					customerWalletHistory.setTransactionType("DEBIT");
					customerWalletHistory.setTransactionAmount(transactionAmount);
					
					property = DAORegistry.getPropertyDAO().get("rtf.customer.wallet.trx.debit.id");
					trxGenCount = Integer.parseInt(property.getValue());
					
					customerWalletHistory.setTransactionId(ReferralCodeGenerator.generateCustWalletCreditTrxId(customerId, trxGenCount, PaginationUtil.walletDebitTrxPrefix));
					customerWalletHistory.setTransactionStatus("PENDING");
					customerWalletHistory.setUpdatedOn(new Date());
					
					trxGenCount++;
					
					property.setValue(String.valueOf(trxGenCount));
					
					break;
	
				default:
					break;
			}
			
			DAORegistry.getCustomerWalletDAO().saveOrUpdate(customerWallet);
			DAORegistry.getCustomerWalletHistoryDAO().saveOrUpdate(customerWalletHistory);
			DAORegistry.getPropertyDAO().saveOrUpdate(property);
			
			transaction.setStatus(1);
			transaction.setWallet(customerWallet);
			transaction.setWalletHistory(customerWalletHistory);
			return transaction;
		}catch(Exception e){
			error.setDescription("Payment is declined. Please contact administrator");
			transaction.setError(error);
			transaction.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,"Payment is declined. Please contact administrator");
			return transaction;
		}
	}
	

	
	@RequestMapping(value = "/GetCustomerCardsList", method=RequestMethod.POST)
	public @ResponsePayload CustomerDetails getCustomerCards(HttpServletRequest request,HttpServletResponse response,Model model){
		
		CustomerDetails customerDetails =new CustomerDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"You are not authorized");
				return customerDetails;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"Customer Id is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"Product Type is mandatory");
				return customerDetails;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"Please send valid Customer Id");
					return customerDetails;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"Please send valid Customer Id");
				return customerDetails;
			}
			
			List<CustomerCardInfo> cardList = DAORegistry.getCustomerCardInfoDAO().getAllActiveCardsByCustomerId(customerId);
			
			List<CustomerCardInfo> finalCardList = new ArrayList<CustomerCardInfo>();
			
			Set<String> cardNoList = new HashSet<String>();
			for (CustomerCardInfo cardObj : cardList) {
				String key = cardObj.getLastFourDigit()+"_"+cardObj.getMonth()+"_"+cardObj.getYear()+"_"+cardObj.getCardType();
				if(cardNoList.add(key)){
					finalCardList.add(cardObj);
				}
			}
			customerDetails.setStatus(1);
			customerDetails.setCustomer(customer);
			customerDetails.setCardList(finalCardList);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"Success");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Fetching Customer Information.");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCUSTOMERCARDS,"Error occured while Fetching Customer Information");
			return customerDetails;
		}
		return customerDetails;
	}
	
	

	@RequestMapping(value = "/ValidatePromoCodeTicTrackerLongOrder", method=RequestMethod.POST)
	public @ResponsePayload ReferralCodeValidation validatePromoCodeTicTrackerLongOrder(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ReferralCodeValidation customerDetails =new ReferralCodeValidation();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
				return customerDetails;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String referralCode = request.getParameter("referralCode");
			String productTypeStr = request.getParameter("productType");
			String platFormStr = request.getParameter("platForm");
			String sessionId = request.getParameter("deviceId");			
			String tgIdStr = request.getParameter("tgId");
			String eventIdStr = request.getParameter("eventId");	
			String orderTotalStr = request.getParameter("orderTotal");
			String clientIPAddress = request.getParameter("clientIPAddress");
			String isFanPriceStr = request.getParameter("isFanPrice");
			String orderQtyStr = request.getParameter("orderQty");
			String isLongSaleStr = request.getParameter("isLongSale");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Customer Id is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(sessionId)){
				try{
					String deviceId = ((HttpServletRequest)request).getHeader("deviceId");
					sessionId = deviceId;
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(TextUtil.isEmptyOrNull(referralCode)){
				error.setDescription("Please enter a Referral Code");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Please enter a Referral Code");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Product Type is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(isFanPriceStr)){
				error.setDescription("Please send me valid price flag.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Please send me valid price flag.");
				return customerDetails;
			}
			
			
			
			if(TextUtil.isEmptyOrNull(platFormStr)){
				error.setDescription("Platform is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm platForm = null;
			try{
				platForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Platform is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Platform is mandatory");
				return customerDetails;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(, ProductType.REWARDTHEFAN);
			if(null == customer){
				error.setDescription("Customer not recognized");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Customer not recognized");
				return customerDetails;
			}
			
			if(customer.getReferrerCode() != null && customer.getReferrerCode().equals(referralCode.trim())){
				error.setDescription("You cannot enter your own Referral Code. Please try again");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You cannot enter your own Referral Code. Please try again");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(orderTotalStr)){
				error.setDescription("Order Total is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Order Total is mandatory");
				return customerDetails;
			}
			
			Double orderTotal = null;
			try{
				orderTotal = Double.valueOf(orderTotalStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Order Total is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Order Total is mandatory");
				return customerDetails;
			}
			
			
			if(TextUtil.isEmptyOrNull(tgIdStr)){
				error.setDescription("Ticket Group Id is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Ticket Group Id is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Event Id is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Event Id is mandatory");
				return customerDetails;
			}
			
			Integer eventId=null;
			
			try{
				eventId = Integer.parseInt(eventIdStr);
			}catch(Exception e){
				error.setDescription("Event Id is not valid.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Event Id is not valid");
				return customerDetails;
			}
			
			Boolean isFanPrice = Boolean.valueOf(isFanPriceStr);
			Boolean isLongSale = Boolean.valueOf(isLongSaleStr);
			Integer qty = Integer.parseInt(orderQtyStr.trim());
			
			Double normalPrice = 0.00, discountedPrice = 0.00;
			
			if(isLongSale){
				
			}else{
			}
			
			LoyaltySettings loyaltySettings =DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings(); 
			
			RTFCustomerPromotionalOffer custPromoOffer = DAORegistry.getRtfCustomerPromotionalOfferDAO().getPromoOfferCustomerId(customer.getId());
			
			Boolean isValidRTFPromoCode = FavoriteUtil.isValidCustomerPromotionalCode(custPromoOffer, referralCode.trim());
			
			if(null != isValidRTFPromoCode && isValidRTFPromoCode){
				
				Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, ProductType.REWARDTHEFAN);
				Integer orderCount = DAORegistry.getQueryManagerDAO().getOrderCount(customer.getId());
				if(null != orderCount&& orderCount > 0){
					error.setDescription("You are eligible to buy at one time using this promotional code "+referralCode);
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You are eligible to buy at one time using this promotional code "+referralCode);
					return customerDetails;
				}
				
				if(custPromoOffer.getIsFlatDiscount() && null != custPromoOffer.getFlatOfferOrderThreshold() && custPromoOffer.getFlatOfferOrderThreshold()> 0){
					if(orderTotal < custPromoOffer.getFlatOfferOrderThreshold()){
						error.setDescription("Your minimum selected ticket price should be more than $"+TicketUtil.getRoundedValue(custPromoOffer.getFlatOfferOrderThreshold())+" " +
						"in order to avail this promotional code");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You entered an invalid Promotional Code");
						return customerDetails;
					}
					
				}
				Double serviceFees = 0.00;
				Double additionalDiscountConv = 0.00d;
				Double discAmt = 0.00d;
				Double priceDisc = 0.00d;
				if(custPromoOffer.getIsFlatDiscount()){
					additionalDiscountConv = custPromoOffer.getDiscount();
					discAmt = custPromoOffer.getDiscount();
					priceDisc = TicketUtil.getRoundedValue(additionalDiscountConv / qty);
				}else{
					additionalDiscountConv = custPromoOffer.getDiscount() / 100;
					discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
					priceDisc = additionalDiscountConv;
				}
				
				if(!isLongSale){
					discountedPrice = normalPrice - TicketUtil.getRoundedValue(discAmt / qty);
				}
				
				
				RTFPromotionalOfferTracking promoCodeTracking = null;
				try{
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, custPromoOffer.getId(), customer.getId(), 
							sessionId, -1, eventId, referralCode,isLongSale,PromotionalType.CUSTOMER_PROMO);
					if(null == promoCodeTracking ){
						promoCodeTracking = new RTFPromotionalOfferTracking();
						promoCodeTracking.setCustomerId(customer.getId());
						promoCodeTracking.setPromotionalOfferId(custPromoOffer.getId());
						promoCodeTracking.setTicketGroupId(-1);
						promoCodeTracking.setEventId(eventId);
						promoCodeTracking.setIsLongTicket(isLongSale);
						promoCodeTracking.setPlatForm(platForm);
						promoCodeTracking.setSessionId(sessionId);
						promoCodeTracking.setStatus("PENDING");
						promoCodeTracking.setOfferType(PromotionalType.CUSTOMER_PROMO);
						promoCodeTracking.setOrderId(null);
						promoCodeTracking.setPromoCode(referralCode);
						promoCodeTracking.setIsFlatDiscount(custPromoOffer.getIsFlatDiscount());
						promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
						promoCodeTracking.setAdditionalDiscountAmt(discAmt);
						promoCodeTracking.setServiceFees(serviceFees);
						promoCodeTracking.setTotalPrice(orderTotal);
						promoCodeTracking.setTicketPrice(normalPrice);
						promoCodeTracking.setDiscountedPrice(discountedPrice);
						promoCodeTracking.setCreatedDate(new Date());
						promoCodeTracking.setUpdatedDate(new Date());
					}else{
						promoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				customerDetails.setRewardConv(0.00);
				Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, loyaltySettings.getRewardConversion());
				customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
				customerDetails.setRewardPointsAsDouble(earningRewardPoints);
				customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
				customerDetails.setDiscountConv(priceDisc);
				customerDetails.setPromoTrackingId(promoCodeTracking.getId());
				customerDetails.setIsFlatDiscount(custPromoOffer.getIsFlatDiscount());
				customerDetails.setCodeType(CodeType.PROMOTIONAL);
				customerDetails.setStatus(1);
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setReferralCode(referralCode);
				customerDetails.setMessage("Prmotional Code was applied.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Prmotional Code was applied.");
				return customerDetails;
			}

			isValidRTFPromoCode = RTFPromotionalOfferUtil.validateRTFPromoCode(referralCode.trim());
			if(null != isValidRTFPromoCode && isValidRTFPromoCode){
				
				RTFPromotionalOfferHdr promotionalOffer = RTFPromotionalOfferUtil.getPromoCodeObjByPromoCode(referralCode.trim());
				Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, ProductType.REWARDTHEFAN);
				Boolean isValid = FavoriteUtil.isValidPromotionalCode(promotionalOffer, event,isFanPrice, customer);
				if(!isValid){
					error.setDescription("You entered an invalid Promotional Code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You entered an invalid Referral Code. Please try again");
					return customerDetails;
				}
				
				Integer orderCount = DAORegistry.getQueryManagerDAO().getCompletedOrderByCustomerAndPromoCode(customer.getId(),referralCode.trim());
				if(null != orderCount && orderCount > 0){
					error.setDescription("This promotional code "+referralCode+" is valid for your first purchase");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This promotional code "+referralCode+" is valid for your first purchase");
					return customerDetails;
				}
				
				if(promotionalOffer.getIsFlatDiscount()){
					if(orderTotal < promotionalOffer.getFlatOfferOrderThreshold()){
						error.setDescription("Your minimum selected ticket price should be more than $"+TicketUtil.getRoundedValue(promotionalOffer.getFlatOfferOrderThreshold())+" " +
						"in order to avail this promotional code");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You entered an invalid Promotional Code");
						return customerDetails;
					}
					
				}
				Double serviceFees = 0.00;
				Double additionalDiscountConv = 0.00d;
				Double discAmt = 0.00d;
				Double priceDisc = 0.00d;
				if(promotionalOffer.getIsFlatDiscount()){
					additionalDiscountConv = promotionalOffer.getDiscount();
					discAmt = promotionalOffer.getDiscount();
					priceDisc = TicketUtil.getRoundedValue(additionalDiscountConv / qty);
				}else{
					additionalDiscountConv = promotionalOffer.getDiscount() / 100;
					discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
					priceDisc = additionalDiscountConv;
				}
				
				if(!isLongSale){
					discountedPrice = normalPrice - TicketUtil.getRoundedValue(discAmt / qty);
				}
				
				RTFPromotionalOfferTracking promoCodeTracking = null;
				try{
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, promotionalOffer.getId(), customer.getId(), 
							sessionId, -1, eventId, referralCode,isLongSale,PromotionalType.NORMAL_PROMO);
					if(null == promoCodeTracking ){
						promoCodeTracking = new RTFPromotionalOfferTracking();
						promoCodeTracking.setCustomerId(customer.getId());
						promoCodeTracking.setPromotionalOfferId(promotionalOffer.getId());
						promoCodeTracking.setTicketGroupId(-1);
						promoCodeTracking.setEventId(eventId);
						promoCodeTracking.setIsLongTicket(isLongSale);
						promoCodeTracking.setPlatForm(platForm);
						promoCodeTracking.setSessionId(sessionId);
						promoCodeTracking.setStatus("PENDING");
						promoCodeTracking.setOfferType(PromotionalType.NORMAL_PROMO);
						promoCodeTracking.setOrderId(null);
						promoCodeTracking.setPromoCode(referralCode);
						promoCodeTracking.setIsFlatDiscount(promotionalOffer.getIsFlatDiscount());
						promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
						promoCodeTracking.setAdditionalDiscountAmt(discAmt);
						promoCodeTracking.setServiceFees(serviceFees);
						promoCodeTracking.setTotalPrice(orderTotal);
						promoCodeTracking.setTicketPrice(normalPrice);
						promoCodeTracking.setDiscountedPrice(discountedPrice);
						promoCodeTracking.setCreatedDate(new Date());
						promoCodeTracking.setUpdatedDate(new Date());
					}else{
						promoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				customerDetails.setRewardConv(0.00);
				Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, loyaltySettings.getRewardConversion());
				customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
				customerDetails.setRewardPointsAsDouble(earningRewardPoints);
				customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
				customerDetails.setPromoTrackingId(promoCodeTracking.getId());
				customerDetails.setDiscountConv(priceDisc);
				customerDetails.setIsFlatDiscount(promotionalOffer.getIsFlatDiscount());
				customerDetails.setCodeType(CodeType.PROMOTIONAL);
				customerDetails.setStatus(1);
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setReferralCode(referralCode);
				customerDetails.setMessage("Prmotional Code was applied.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Prmotional Code was applied.");
				return customerDetails;
			}
			
			Boolean isAffilicatePromoCode = RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim());
			if(null != isAffilicatePromoCode && isAffilicatePromoCode){
				
				Affiliate affiliate =RTFAffiliateBrokerUtil.getAffiliateObjByPromoCode(referralCode.trim());
				
				if(null != affiliate.getAffiliateRepeatBusiness() && affiliate.getAffiliateRepeatBusiness()){
					
					// Customer can purchase many tickets using this kind of affiliate promotional code
				}else{
					Integer orderCount = DAORegistry.getQueryManagerDAO().getCompletedOrderByCustomerAndPromoCode(customer.getId(),referralCode.trim());
					if(null != orderCount && orderCount > 0){
						error.setDescription("This promotional code "+referralCode+" is valid for your first purchase");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This promotional code "+referralCode+" is valid for your first purchase");
						return customerDetails;
					}
				}
				
				
				Double serviceFees = 0.00;
				Double additionalDiscountConv = affiliate.getPhoneCustomerDiscount();
				Double discAmt = 0.00d;
				Double priceDisc = 0.00d;
				
				discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
				priceDisc = additionalDiscountConv;
				
				if(!isLongSale){
					discountedPrice = normalPrice - TicketUtil.getRoundedValue(discAmt / qty);
				}
				
				RTFPromotionalOfferTracking promoCodeTracking = null;
				try{
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, affiliate.getOfferId(), customer.getId(), 
							sessionId, -1, eventId, referralCode,isLongSale,PromotionalType.AFFILIATE_PROMO_CODE);
					if(null == promoCodeTracking ){
						promoCodeTracking = new RTFPromotionalOfferTracking();
						promoCodeTracking.setCustomerId(customer.getId());
						promoCodeTracking.setPromotionalOfferId(affiliate.getOfferId());
						promoCodeTracking.setTicketGroupId(-1);
						promoCodeTracking.setEventId(eventId);
						promoCodeTracking.setIsLongTicket(isLongSale);
						promoCodeTracking.setPlatForm(platForm);
						promoCodeTracking.setSessionId(sessionId);
						promoCodeTracking.setStatus("PENDING");
						promoCodeTracking.setOfferType(PromotionalType.AFFILIATE_PROMO_CODE);
						promoCodeTracking.setOrderId(null);
						promoCodeTracking.setPromoCode(referralCode);
						promoCodeTracking.setIsFlatDiscount(false);
						promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
						promoCodeTracking.setAdditionalDiscountAmt(discAmt);
						promoCodeTracking.setServiceFees(serviceFees);
						promoCodeTracking.setTotalPrice(orderTotal);
						promoCodeTracking.setTicketPrice(normalPrice);
						promoCodeTracking.setDiscountedPrice(discountedPrice);
						promoCodeTracking.setCreatedDate(new Date());
						promoCodeTracking.setUpdatedDate(new Date());
					}else{
						promoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				AffiliatePromoCodeTracking affPromoCodeTracking = null;
				try{
					
					affPromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(platForm, affiliate.getUserId(), customer.getId(), 
							sessionId, -1, eventId, referralCode,isLongSale);
					if(null == affPromoCodeTracking ){
						affPromoCodeTracking = new AffiliatePromoCodeTracking();
						affPromoCodeTracking.setCustomerId(customer.getId());
						affPromoCodeTracking.setUserId(affiliate.getUserId());
						affPromoCodeTracking.setIsLongTicket(isLongSale);
						affPromoCodeTracking.setTicketGroupId(-1);
						affPromoCodeTracking.setEventId(eventId);
						affPromoCodeTracking.setIpAddress(clientIPAddress);
						affPromoCodeTracking.setPlatForm(platForm);
						affPromoCodeTracking.setSessionId(sessionId);
						affPromoCodeTracking.setStatus("PENDING");
						affPromoCodeTracking.setOrderId(null);
						affPromoCodeTracking.setPromoCode(referralCode);
						affPromoCodeTracking.setCustRewardConv(0.00);
						affPromoCodeTracking.setCustRewardPoints(0.00);
						affPromoCodeTracking.setCreditConv(0.00);
						affPromoCodeTracking.setCreditedCash(0.00);
						affPromoCodeTracking.setCreatedDate(new Date());
						affPromoCodeTracking.setUpdatedDate(new Date());
					}else{
						affPromoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getAffiliatePromoCodeTrackingDAO().saveOrUpdate(affPromoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				customerDetails.setRewardConv(0.00);
				if(!isAffilicatePromoCode || (isAffilicatePromoCode && affiliate!=null && affiliate.getIsEarnRewardPoints())){
					Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, loyaltySettings.getRewardConversion());
					customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
					customerDetails.setRewardPointsAsDouble(earningRewardPoints);
					customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
				}else{
					customerDetails.setRewardPoints("");
					customerDetails.setRewardPointsAsDouble(0.00);
				}
				customerDetails.setPromoTrackingId(promoCodeTracking.getId());
				customerDetails.setAffPromoTrackingId(affPromoCodeTracking.getId());
				customerDetails.setDiscountConv(priceDisc);
				customerDetails.setIsFlatDiscount(false);
				customerDetails.setCodeType(CodeType.PROMOTIONAL);
				customerDetails.setStatus(1);
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setReferralCode(referralCode);
				customerDetails.setMessage("Prmotional Code was applied.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Prmotional Code was applied.");
				return customerDetails;
			}
			
			Customer referralCodeCust = DAORegistry.getCustomerDAO().getCustomerByReferralCode(referralCode.trim());
			if(null == referralCodeCust){
				error.setDescription("You entered an invalid discount code. Please enter valid discount code.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You entered an invalid discount code. Please enter valid discount code.");
				return customerDetails;
			}
			
			if(customer.getReferrerCode() != null && customer.getReferrerCode().equals(referralCode)){
				error.setDescription("You can not use your discount code. This is not valid for you.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You can not use your discount code. This is not valid for you.");
				return customerDetails;
			}
			
			Integer orderCount = DAORegistry.getQueryManagerDAO().getCompletedOrderByCustomerAndPromoCode(customer.getId(),referralCode.trim());
			if(null != orderCount && orderCount > 0){
				error.setDescription("This discount code "+referralCode+" is valid for your first purchase");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This discount code "+referralCode+" is valid for your first purchase");
				return customerDetails;
			}
			ReferralDiscountSettings discountSettings = DAORegistry.getReferralDiscountSettingsDAO().getActiveDiscountSettingsByPlatForm(platForm);
			if(null == discountSettings){
				error.setDescription("This discount code "+referralCode+" is expired!");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This discount code "+referralCode+" is expired!");
				return customerDetails;
			}
			
			AutoCatsLockedTickets lockedTickets = null;
			Double serviceFees = 0.00;
			Double additionalDiscountConv = 0.00d;
			Double discAmt = 0.00d;
			Double priceDisc = 0.00;
			additionalDiscountConv = discountSettings.getOrderDiscountPerc() / 100;
			discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
			
			priceDisc = TicketUtil.getRoundedValue(discAmt / qty);
			
			if(!isLongSale){
				discountedPrice = normalPrice - priceDisc;
			}
			
			RTFPromotionalOfferTracking promoCodeTracking = null;
			try{
				promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, discountSettings.getId(), customer.getId(), 
						sessionId, -1, eventId, referralCode,isLongSale,PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE);
				if(null == promoCodeTracking ){
					promoCodeTracking = new RTFPromotionalOfferTracking();
					promoCodeTracking.setCustomerId(customer.getId());
					promoCodeTracking.setPromotionalOfferId(discountSettings.getId());
					promoCodeTracking.setTicketGroupId(-1);
					promoCodeTracking.setIsLongTicket(isLongSale);
					promoCodeTracking.setEventId(eventId);
					promoCodeTracking.setPlatForm(platForm);
					promoCodeTracking.setSessionId(sessionId);
					promoCodeTracking.setStatus("PENDING");
					promoCodeTracking.setOfferType(PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE);
					promoCodeTracking.setOrderId(null);
					promoCodeTracking.setPromoCode(referralCode);
					promoCodeTracking.setIsFlatDiscount(false);
					promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
					promoCodeTracking.setAdditionalDiscountAmt(discAmt);
					promoCodeTracking.setServiceFees(serviceFees);
					promoCodeTracking.setTicketPrice(normalPrice);
					promoCodeTracking.setDiscountedPrice(discountedPrice);
					promoCodeTracking.setTotalPrice(orderTotal);
					promoCodeTracking.setCreatedDate(new Date());
					promoCodeTracking.setUpdatedDate(new Date());
					promoCodeTracking.setReferredBy(referralCodeCust.getId());
					promoCodeTracking.setCustomerEarnRewardConv(discountSettings.getCustEarnRewardConv());
					promoCodeTracking.setReferrarEarnRewardConv(discountSettings.getReferrerEarnRewardConv());
				}else{
					promoCodeTracking.setUpdatedDate(new Date());
				}
				DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			customerDetails.setRewardConv(discountSettings.getCustEarnRewardConv());
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, discountSettings.getCustEarnRewardConv());
			customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
			customerDetails.setRewardPointsAsDouble(earningRewardPoints);
			customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
			customerDetails.setDiscountConv(additionalDiscountConv);
			customerDetails.setPromoTrackingId(promoCodeTracking.getId());
			customerDetails.setIsFlatDiscount(false);
			customerDetails.setCodeType(CodeType.PROMOTIONAL);
			customerDetails.setStatus(1);
			customerDetails.setCustomerId(customer.getId());
			customerDetails.setReferralCode(referralCode);
			customerDetails.setMessage("Promotional Discount Code was applied.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"Promotional Discount Code was applied.");
			return customerDetails;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while validating referral code");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Error occured while validating referral code");
			return customerDetails;
		}
	}
	

	@RequestMapping(value = "/ValidatePromoCodeTicTracker", method=RequestMethod.POST)
	public @ResponsePayload ReferralCodeValidation validatePromoCodeTicTracker(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ReferralCodeValidation customerDetails =new ReferralCodeValidation();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
				return customerDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
						return customerDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
					return customerDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You are not authorized");
				return customerDetails;
			}
			
			String customerIdStr = request.getParameter("customerId");
			String referralCode = request.getParameter("referralCode");
			String productTypeStr = request.getParameter("productType");
			String platFormStr = request.getParameter("platForm");
			String sessionId = request.getParameter("deviceId");			
			String tgIdStr = request.getParameter("tgId");
			String eventIdStr = request.getParameter("eventId");	
			String orderTotalStr = request.getParameter("orderTotal");
			String clientIPAddress = request.getParameter("clientIPAddress");
			String isFanPriceStr = request.getParameter("isFanPrice");
			String orderQtyStr = request.getParameter("orderQty");
			String isLongSaleStr = request.getParameter("isLongSale");
			
			if(TextUtil.isEmptyOrNull(customerIdStr)){
				error.setDescription("Customer Id is mandatory");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Customer Id is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(sessionId)){
				try{
					String deviceId = ((HttpServletRequest)request).getHeader("deviceId");
					sessionId = deviceId;
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(TextUtil.isEmptyOrNull(referralCode)){
				error.setDescription("Please enter a Referral Code");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Please enter a Referral Code");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Product Type is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(isFanPriceStr)){
				error.setDescription("Please send me valid price flag.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Please send me valid price flag.");
				return customerDetails;
			}
			
			
			
			if(TextUtil.isEmptyOrNull(platFormStr)){
				error.setDescription("Platform is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Platform is mandatory");
				return customerDetails;
			}
			ApplicationPlatForm platForm = null;
			try{
				platForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Platform is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Platform is mandatory");
				return customerDetails;
			}
			
			Customer customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));//DAORegistry.getCustomerDAO().getCustomerByIdByProduct(, ProductType.REWARDTHEFAN);
			if(null == customer){
				error.setDescription("Customer not recognized");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Customer not recognized");
				return customerDetails;
			}
			
			if(customer.getReferrerCode() != null && customer.getReferrerCode().equals(referralCode.trim())){
				error.setDescription("You cannot enter your own Referral Code. Please try again");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You cannot enter your own Referral Code. Please try again");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(orderTotalStr)){
				error.setDescription("Order Total is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Order Total is mandatory");
				return customerDetails;
			}
			
			Double orderTotal = null;
			try{
				orderTotal = Double.valueOf(orderTotalStr);
			}catch(Exception e){
				e.printStackTrace();
				error.setDescription("Order Total is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Order Total is mandatory");
				return customerDetails;
			}
			
			
			if(TextUtil.isEmptyOrNull(tgIdStr)){
				error.setDescription("Ticket Group Id is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Ticket Group Id is mandatory");
				return customerDetails;
			}
			
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Event Id is mandatory.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Event Id is mandatory");
				return customerDetails;
			}
			
			Integer eventId=null,tgId=null;
			
			try{
				eventId = Integer.parseInt(eventIdStr);
				tgId = Integer.parseInt(tgIdStr);
			}catch(Exception e){
				error.setDescription("Event Id is not valid.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Event Id is not valid");
				return customerDetails;
			}
			
			Boolean isFanPrice = Boolean.valueOf(isFanPriceStr);
			Boolean isLongSale = Boolean.valueOf(isLongSaleStr);
			Integer qty = Integer.parseInt(orderQtyStr.trim());
			
			Double normalPrice = 0.00, discountedPrice = 0.00;
			
			if(isLongSale){
				
			}else{
				CategoryTicketGroup catTix = DAORegistry.getCategoryTicketGroupDAO().get(tgId);
				normalPrice = catTix.getOriginalPrice();
				if(isFanPrice != null && isFanPrice){
					normalPrice = catTix.getLoyalFanPriceDouble();
				}
			}
			
			LoyaltySettings loyaltySettings =DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings(); 
			
			RTFCustomerPromotionalOffer custPromoOffer = DAORegistry.getRtfCustomerPromotionalOfferDAO().getPromoOfferCustomerId(customer.getId());
			
			Boolean isValidRTFPromoCode = FavoriteUtil.isValidCustomerPromotionalCode(custPromoOffer, referralCode.trim());
			
			if(null != isValidRTFPromoCode && isValidRTFPromoCode){
				
				Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, ProductType.REWARDTHEFAN);
				Integer orderCount = DAORegistry.getQueryManagerDAO().getOrderCount(customer.getId());
				if(null != orderCount&& orderCount > 0){
					error.setDescription("You are eligible to buy at one time using this promotional code "+referralCode);
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You are eligible to buy at one time using this promotional code "+referralCode);
					return customerDetails;
				}
				
				if(custPromoOffer.getIsFlatDiscount() && null != custPromoOffer.getFlatOfferOrderThreshold() && custPromoOffer.getFlatOfferOrderThreshold()> 0){
					if(orderTotal < custPromoOffer.getFlatOfferOrderThreshold()){
						error.setDescription("Your minimum selected ticket price should be more than $"+TicketUtil.getRoundedValue(custPromoOffer.getFlatOfferOrderThreshold())+" " +
						"in order to avail this promotional code");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You entered an invalid Promotional Code");
						return customerDetails;
					}
					
				}
				Double serviceFees = 0.00;
				Double additionalDiscountConv = 0.00d;
				Double discAmt = 0.00d;
				Double priceDisc = 0.00d;
				if(custPromoOffer.getIsFlatDiscount()){
					additionalDiscountConv = custPromoOffer.getDiscount();
					discAmt = custPromoOffer.getDiscount();
					priceDisc = TicketUtil.getRoundedValue(additionalDiscountConv / qty);
				}else{
					additionalDiscountConv = custPromoOffer.getDiscount() / 100;
					discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
					priceDisc = additionalDiscountConv;
				}
				
				if(!isLongSale){
					discountedPrice = normalPrice - TicketUtil.getRoundedValue(discAmt / qty);
				}
				
				
				RTFPromotionalOfferTracking promoCodeTracking = null;
				try{
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, custPromoOffer.getId(), customer.getId(), 
							sessionId, tgId, eventId, referralCode,isLongSale,PromotionalType.CUSTOMER_PROMO);
					if(null == promoCodeTracking ){
						promoCodeTracking = new RTFPromotionalOfferTracking();
						promoCodeTracking.setCustomerId(customer.getId());
						promoCodeTracking.setPromotionalOfferId(custPromoOffer.getId());
						promoCodeTracking.setTicketGroupId(tgId);
						promoCodeTracking.setEventId(eventId);
						promoCodeTracking.setIsLongTicket(isLongSale);
						promoCodeTracking.setPlatForm(platForm);
						promoCodeTracking.setSessionId(sessionId);
						promoCodeTracking.setStatus("PENDING");
						promoCodeTracking.setOfferType(PromotionalType.CUSTOMER_PROMO);
						promoCodeTracking.setOrderId(null);
						promoCodeTracking.setPromoCode(referralCode);
						promoCodeTracking.setIsFlatDiscount(custPromoOffer.getIsFlatDiscount());
						promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
						promoCodeTracking.setAdditionalDiscountAmt(discAmt);
						promoCodeTracking.setServiceFees(serviceFees);
						promoCodeTracking.setTotalPrice(orderTotal);
						promoCodeTracking.setTicketPrice(normalPrice);
						promoCodeTracking.setDiscountedPrice(discountedPrice);
						promoCodeTracking.setCreatedDate(new Date());
						promoCodeTracking.setUpdatedDate(new Date());
					}else{
						promoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				customerDetails.setRewardConv(0.00);
				Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, loyaltySettings.getRewardConversion());
				customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
				customerDetails.setRewardPointsAsDouble(earningRewardPoints);
				customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
				customerDetails.setDiscountConv(priceDisc);
				customerDetails.setIsFlatDiscount(custPromoOffer.getIsFlatDiscount());
				customerDetails.setCodeType(CodeType.PROMOTIONAL);
				customerDetails.setStatus(1);
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setReferralCode(referralCode);
				customerDetails.setMessage("Prmotional Code was applied.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Prmotional Code was applied.");
				return customerDetails;
			}

			isValidRTFPromoCode = RTFPromotionalOfferUtil.validateRTFPromoCode(referralCode.trim());
			if(null != isValidRTFPromoCode && isValidRTFPromoCode){
				
				RTFPromotionalOfferHdr promotionalOffer = RTFPromotionalOfferUtil.getPromoCodeObjByPromoCode(referralCode.trim());
				Event event = DAORegistry.getEventDAO().getEventByEventId(eventId, ProductType.REWARDTHEFAN);
				Boolean isValid = FavoriteUtil.isValidPromotionalCode(promotionalOffer, event,isFanPrice, customer);
				if(!isValid){
					error.setDescription("You entered an invalid Promotional Code.");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"You entered an invalid Referral Code. Please try again");
					return customerDetails;
				}
				
				Integer orderCount = DAORegistry.getQueryManagerDAO().getCompletedOrderByCustomerAndPromoCode(customer.getId(),referralCode.trim());
				if(null != orderCount && orderCount > 0){
					error.setDescription("This promotional code "+referralCode+" is valid for your first purchase");
					customerDetails.setError(error);
					customerDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This promotional code "+referralCode+" is valid for your first purchase");
					return customerDetails;
				}
				
				if(promotionalOffer.getIsFlatDiscount()){
					if(orderTotal < promotionalOffer.getFlatOfferOrderThreshold()){
						error.setDescription("Your minimum selected ticket price should be more than $"+TicketUtil.getRoundedValue(promotionalOffer.getFlatOfferOrderThreshold())+" " +
						"in order to avail this promotional code");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You entered an invalid Promotional Code");
						return customerDetails;
					}
					
				}
				Double serviceFees = 0.00;
				Double additionalDiscountConv = 0.00d;
				Double discAmt = 0.00d;
				Double priceDisc = 0.00d;
				if(promotionalOffer.getIsFlatDiscount()){
					additionalDiscountConv = promotionalOffer.getDiscount();
					discAmt = promotionalOffer.getDiscount();
					priceDisc = TicketUtil.getRoundedValue(additionalDiscountConv / qty);
				}else{
					additionalDiscountConv = promotionalOffer.getDiscount() / 100;
					discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
					priceDisc = additionalDiscountConv;
				}
				
				if(!isLongSale){
					discountedPrice = normalPrice - TicketUtil.getRoundedValue(discAmt / qty);
				}
				
				RTFPromotionalOfferTracking promoCodeTracking = null;
				try{
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, promotionalOffer.getId(), customer.getId(), 
							sessionId, tgId, eventId, referralCode,isLongSale,PromotionalType.NORMAL_PROMO);
					if(null == promoCodeTracking ){
						promoCodeTracking = new RTFPromotionalOfferTracking();
						promoCodeTracking.setCustomerId(customer.getId());
						promoCodeTracking.setPromotionalOfferId(promotionalOffer.getId());
						promoCodeTracking.setTicketGroupId(tgId);
						promoCodeTracking.setEventId(eventId);
						promoCodeTracking.setIsLongTicket(isLongSale);
						promoCodeTracking.setPlatForm(platForm);
						promoCodeTracking.setSessionId(sessionId);
						promoCodeTracking.setStatus("PENDING");
						promoCodeTracking.setOfferType(PromotionalType.NORMAL_PROMO);
						promoCodeTracking.setOrderId(null);
						promoCodeTracking.setPromoCode(referralCode);
						promoCodeTracking.setIsFlatDiscount(promotionalOffer.getIsFlatDiscount());
						promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
						promoCodeTracking.setAdditionalDiscountAmt(discAmt);
						promoCodeTracking.setServiceFees(serviceFees);
						promoCodeTracking.setTotalPrice(orderTotal);
						promoCodeTracking.setTicketPrice(normalPrice);
						promoCodeTracking.setDiscountedPrice(discountedPrice);
						promoCodeTracking.setCreatedDate(new Date());
						promoCodeTracking.setUpdatedDate(new Date());
					}else{
						promoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				customerDetails.setRewardConv(0.00);
				Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, loyaltySettings.getRewardConversion());
				customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
				customerDetails.setRewardPointsAsDouble(earningRewardPoints);
				customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
				customerDetails.setDiscountConv(priceDisc);
				customerDetails.setIsFlatDiscount(promotionalOffer.getIsFlatDiscount());
				customerDetails.setCodeType(CodeType.PROMOTIONAL);
				customerDetails.setStatus(1);
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setReferralCode(referralCode);
				customerDetails.setMessage("Prmotional Code was applied.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Prmotional Code was applied.");
				return customerDetails;
			}
			
			Boolean isAffilicatePromoCode = RTFAffiliateBrokerUtil.validateAffiliatePromoCode(referralCode.trim());
			if(null != isAffilicatePromoCode && isAffilicatePromoCode){
				
				Affiliate affiliate =RTFAffiliateBrokerUtil.getAffiliateObjByPromoCode(referralCode.trim());
				
				if(null != affiliate.getAffiliateRepeatBusiness() && affiliate.getAffiliateRepeatBusiness()){
					
					// Customer can purchase many tickets using this kind of affiliate promotional code
				}else{
					Integer orderCount = DAORegistry.getQueryManagerDAO().getCompletedOrderByCustomerAndPromoCode(customer.getId(),referralCode.trim());
					if(null != orderCount && orderCount > 0){
						error.setDescription("This promotional code "+referralCode+" is valid for your first purchase");
						customerDetails.setError(error);
						customerDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This promotional code "+referralCode+" is valid for your first purchase");
						return customerDetails;
					}
				}
				
				
				Double serviceFees = 0.00;
				Double additionalDiscountConv = affiliate.getPhoneCustomerDiscount();
				Double discAmt = 0.00d;
				Double priceDisc = 0.00d;
				
				discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
				priceDisc = additionalDiscountConv;
				
				if(!isLongSale){
					discountedPrice = normalPrice - TicketUtil.getRoundedValue(discAmt / qty);
				}
				
				RTFPromotionalOfferTracking promoCodeTracking = null;
				try{
					promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, affiliate.getOfferId(), customer.getId(), 
							sessionId, tgId, eventId, referralCode,isLongSale,PromotionalType.AFFILIATE_PROMO_CODE);
					if(null == promoCodeTracking ){
						promoCodeTracking = new RTFPromotionalOfferTracking();
						promoCodeTracking.setCustomerId(customer.getId());
						promoCodeTracking.setPromotionalOfferId(affiliate.getOfferId());
						promoCodeTracking.setTicketGroupId(tgId);
						promoCodeTracking.setEventId(eventId);
						promoCodeTracking.setIsLongTicket(isLongSale);
						promoCodeTracking.setPlatForm(platForm);
						promoCodeTracking.setSessionId(sessionId);
						promoCodeTracking.setStatus("PENDING");
						promoCodeTracking.setOfferType(PromotionalType.AFFILIATE_PROMO_CODE);
						promoCodeTracking.setOrderId(null);
						promoCodeTracking.setPromoCode(referralCode);
						promoCodeTracking.setIsFlatDiscount(false);
						promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
						promoCodeTracking.setAdditionalDiscountAmt(discAmt);
						promoCodeTracking.setServiceFees(serviceFees);
						promoCodeTracking.setTotalPrice(orderTotal);
						promoCodeTracking.setTicketPrice(normalPrice);
						promoCodeTracking.setDiscountedPrice(discountedPrice);
						promoCodeTracking.setCreatedDate(new Date());
						promoCodeTracking.setUpdatedDate(new Date());
					}else{
						promoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				AffiliatePromoCodeTracking affPromoCodeTracking = null;
				try{
					
					affPromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(platForm, affiliate.getUserId(), customer.getId(), 
							sessionId, tgId, eventId, referralCode,isLongSale);
					if(null == affPromoCodeTracking ){
						affPromoCodeTracking = new AffiliatePromoCodeTracking();
						affPromoCodeTracking.setCustomerId(customer.getId());
						affPromoCodeTracking.setUserId(affiliate.getUserId());
						affPromoCodeTracking.setIsLongTicket(isLongSale);
						affPromoCodeTracking.setTicketGroupId(tgId);
						affPromoCodeTracking.setEventId(eventId);
						affPromoCodeTracking.setIpAddress(clientIPAddress);
						affPromoCodeTracking.setPlatForm(platForm);
						affPromoCodeTracking.setSessionId(sessionId);
						affPromoCodeTracking.setStatus("PENDING");
						affPromoCodeTracking.setOrderId(null);
						affPromoCodeTracking.setPromoCode(referralCode);
						affPromoCodeTracking.setCustRewardConv(0.00);
						affPromoCodeTracking.setCustRewardPoints(0.00);
						affPromoCodeTracking.setCreditConv(0.00);
						affPromoCodeTracking.setCreditedCash(0.00);
						affPromoCodeTracking.setCreatedDate(new Date());
						affPromoCodeTracking.setUpdatedDate(new Date());
					}else{
						affPromoCodeTracking.setUpdatedDate(new Date());
					}
					DAORegistry.getAffiliatePromoCodeTrackingDAO().saveOrUpdate(affPromoCodeTracking);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				customerDetails.setRewardConv(0.00);
				if(!isAffilicatePromoCode || (isAffilicatePromoCode && affiliate!=null && affiliate.getIsEarnRewardPoints())){
					Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, loyaltySettings.getRewardConversion());
					customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
					customerDetails.setRewardPointsAsDouble(earningRewardPoints);
					customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
				}else{
					customerDetails.setRewardPoints("");
					customerDetails.setRewardPointsAsDouble(0.00);
				}
				
				customerDetails.setDiscountConv(priceDisc);
				customerDetails.setIsFlatDiscount(false);
				customerDetails.setCodeType(CodeType.PROMOTIONAL);
				customerDetails.setStatus(1);
				customerDetails.setCustomerId(customer.getId());
				customerDetails.setReferralCode(referralCode);
				customerDetails.setMessage("Prmotional Code was applied.");
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Prmotional Code was applied.");
				return customerDetails;
			}
			
			Customer referralCodeCust = DAORegistry.getCustomerDAO().getCustomerByReferralCode(referralCode.trim());
			if(null == referralCodeCust){
				error.setDescription("You entered an invalid discount code. Please enter valid discount code.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You entered an invalid discount code. Please enter valid discount code.");
				return customerDetails;
			}
			
			if(customer.getReferrerCode()!=null && customer.getReferrerCode().equals(referralCode)){
				error.setDescription("You can not use your discount code. This is not valid for you.");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"You can not use your discount code. This is not valid for you.");
				return customerDetails;
			}
			
			Integer orderCount = DAORegistry.getQueryManagerDAO().getCompletedOrderByCustomerAndPromoCode(customer.getId(),referralCode.trim());
			if(null != orderCount && orderCount > 0){
				error.setDescription("This discount code "+referralCode+" is valid for your first purchase");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This discount code "+referralCode+" is valid for your first purchase");
				return customerDetails;
			}
			ReferralDiscountSettings discountSettings = DAORegistry.getReferralDiscountSettingsDAO().getActiveDiscountSettingsByPlatForm(platForm);
			if(null == discountSettings){
				error.setDescription("This discount code "+referralCode+" is expired!");
				customerDetails.setError(error);
				customerDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"This discount code "+referralCode+" is expired!");
				return customerDetails;
			}
			
			AutoCatsLockedTickets lockedTickets = null;
			Double serviceFees = 0.00;
			Double additionalDiscountConv = 0.00d;
			Double discAmt = 0.00d;
			Double priceDisc = 0.00;
			additionalDiscountConv = discountSettings.getOrderDiscountPerc() / 100;
			discAmt = TicketUtil.getDiscountedPrice(orderTotal, additionalDiscountConv);
			
			priceDisc = TicketUtil.getRoundedValue(discAmt / qty);
			
			if(!isLongSale){
				discountedPrice = normalPrice - priceDisc;
			}
			
			RTFPromotionalOfferTracking promoCodeTracking = null;
			try{
				promoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(platForm, discountSettings.getId(), customer.getId(), 
						sessionId, tgId, eventId, referralCode,isLongSale,PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE);
				if(null == promoCodeTracking ){
					promoCodeTracking = new RTFPromotionalOfferTracking();
					promoCodeTracking.setCustomerId(customer.getId());
					promoCodeTracking.setPromotionalOfferId(discountSettings.getId());
					promoCodeTracking.setTicketGroupId(tgId);
					promoCodeTracking.setIsLongTicket(isLongSale);
					promoCodeTracking.setEventId(eventId);
					promoCodeTracking.setPlatForm(platForm);
					promoCodeTracking.setSessionId(sessionId);
					promoCodeTracking.setStatus("PENDING");
					promoCodeTracking.setOfferType(PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE);
					promoCodeTracking.setOrderId(null);
					promoCodeTracking.setPromoCode(referralCode);
					promoCodeTracking.setIsFlatDiscount(false);
					promoCodeTracking.setAdditionalDiscountConv(additionalDiscountConv);
					promoCodeTracking.setAdditionalDiscountAmt(discAmt);
					promoCodeTracking.setServiceFees(serviceFees);
					promoCodeTracking.setTicketPrice(normalPrice);
					promoCodeTracking.setDiscountedPrice(discountedPrice);
					promoCodeTracking.setTotalPrice(orderTotal);
					promoCodeTracking.setCreatedDate(new Date());
					promoCodeTracking.setUpdatedDate(new Date());
					promoCodeTracking.setReferredBy(referralCodeCust.getId());
					promoCodeTracking.setCustomerEarnRewardConv(discountSettings.getCustEarnRewardConv());
					promoCodeTracking.setReferrarEarnRewardConv(discountSettings.getReferrerEarnRewardConv());
				}else{
					promoCodeTracking.setUpdatedDate(new Date());
				}
				DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(promoCodeTracking);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			customerDetails.setRewardConv(discountSettings.getCustEarnRewardConv());
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(orderTotal, discountSettings.getCustEarnRewardConv());
			customerDetails.setRewardPoints("You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b>");
			customerDetails.setRewardPointsAsDouble(earningRewardPoints);
			customerDetails.setRewardConv(loyaltySettings.getRewardConversion());
			customerDetails.setDiscountConv(additionalDiscountConv);
			customerDetails.setIsFlatDiscount(false);
			customerDetails.setCodeType(CodeType.PROMOTIONAL);
			customerDetails.setStatus(1);
			customerDetails.setCustomerId(customer.getId());
			customerDetails.setReferralCode(referralCode);
			customerDetails.setMessage("Promotional Discount Code was applied.");
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODE,"Promotional Discount Code was applied.");
			return customerDetails;
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while validating referral code");
			customerDetails.setError(error);
			customerDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEREFERRALCODETICTRACKER,"Error occured while validating referral code");
			return customerDetails;
		}
	}


	
	@RequestMapping(value="/SearchArtistOrTeamByKey",method=RequestMethod.POST)
	public @ResponsePayload ArtistList getTeamOrArtistBySearchKey(HttpServletRequest request, Model model,HttpServletResponse response){
		ArtistList artistList =new ArtistList();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"You are not authorized");
				return artistList;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						artistList.setError(error);
						artistList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"You are not authorized");
						return artistList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					artistList.setError(error);
					artistList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"You are not authorized");
					return artistList;
				}
			}else{
				error.setDescription("You are not authorized.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"You are not authorized");
				return artistList;
			}
			
			String key = request.getParameter("searchKey");
			String pageNumberStr = "1";
			String productTypeStr = request.getParameter("productType");
			String parent = request.getParameter("parentType");
			String customerId = request.getParameter("customerId");
			
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"Product Type is mandatory");
				return artistList;
			}
			
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					artistList.setError(error);
					artistList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"Please send valid product type");
					return artistList;
				}
			}
			
			if(TextUtil.isEmptyOrNull(key)){
				error.setDescription("Please enter a Search Keyword");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"Please enter a Search Keyword");
				return artistList;
			}
			
			key =TextUtil.removeExtraWhitespaces(key);
			
			Integer resultCount = 25 ;
			Integer requestPage = Integer.parseInt(pageNumberStr.trim());
			List<Artist> artists = DAORegistry.getArtistDAO().getAllSportsArtistByKey(key, requestPage, resultCount);
			artistList.setArtists(artists);
			artistList.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"Success");
			return artistList;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while searching artist details.");
			artistList.setError(error);
			artistList.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.SEARCHARTIST,"Error occured while searching artist details");
			return artistList;
		}
	}
	
	
	@RequestMapping(value="/StripeTransactionLongOrder",method=RequestMethod.POST)
	public StripeTransaction stripeTransactionLongOrder(HttpServletRequest request){
		Error error = new Error();
		StripeTransaction stripeTransaction = new StripeTransaction();
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			String platForm = request.getParameter("platForm");
			
			ApplicationPlatForm appPlatForm = null;
			try{
				appPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid application platform");
				return stripeTransaction;
			}
			
			if(appPlatForm.equals(ApplicationPlatForm.TICK_TRACKER)) {
				
			}else {
				error.setDescription("You can only redeem tickets using reward dollars");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You can only redeem tickets using reward dollars");
				return stripeTransaction;
			}
			
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
						return stripeTransaction;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
					return stripeTransaction;
				}
			}else{
				error.setDescription("You are not authorized.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
				return stripeTransaction;
			}
			
			
			String productTypeStr = request.getParameter("productType");
			String orderIdStr=request.getParameter("orderId");
			String paymentMethodStr = request.getParameter("paymentMethod");
			String transactionAmountStr = request.getParameter("transactionAmount");
			String token = request.getParameter("token");
			String description = request.getParameter("description");
			String rtfCustIdStr=request.getParameter("rtfCustId");
			String saveCustCardStr=request.getParameter("saveCustCard");
			String trxBySavedCardStr=request.getParameter("trxBySavedCard");
			String tGroupRefIdsStr=request.getParameter("tGroupRefIds");
			String custCardIdStr=request.getParameter("custCardId");
			String deviceId=request.getParameter("deviceId");
			String quantityStr=request.getParameter("quantity");
			String eventIdStr=request.getParameter("eventId");
			
			System.out.println("=================================STRIPE BEGINS===============================");
			System.out.println("--------------------Param-------------------------");
			System.out.println("platForm ---->"+platForm);
			System.out.println("productTypeStr ---->"+productTypeStr);
			System.out.println("paymentMethodStr ---->"+paymentMethodStr);
			System.out.println("transactionAmountStr ---->"+transactionAmountStr);
			System.out.println("token ---->"+token);
			System.out.println("description ---->"+description);
			System.out.println("rtfCustIdStr ---->"+rtfCustIdStr);
			System.out.println("saveCustCardStr ---->"+saveCustCardStr);
			System.out.println("trxBySavedCardStr ---->"+trxBySavedCardStr);
			System.out.println("quantityStr ---->"+quantityStr);
			System.out.println("eventIdStr ---->"+eventIdStr);
			System.out.println("tGroupRefIdsStr ---->"+tGroupRefIdsStr);
			System.out.println("custCardIdStr ---->"+custCardIdStr);
			System.out.println("----------------------Param-----------------------");
			
			
			if(TextUtil.isEmptyOrNull(platForm)){
				/*error.setDescription("Application Platform is mandatory.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Application Platform is mandatory");
				return stripeTransaction;
				platForm = "DESKTOP_SITE";*/
			}
			 
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Product Type is mandatory");
				return stripeTransaction;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid product type");
					return stripeTransaction;
				}
			}
			
			if(TextUtil.isEmptyOrNull(paymentMethodStr)){
				error.setDescription("Payment Method is Mandatory");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Payment Method is Mandatory");
				return stripeTransaction;
			}
			
			PaymentMethod paymentMethod = PaymentMethod.CREDITCARD;
			try{
				paymentMethod = PaymentMethod.valueOf(paymentMethodStr);
			}catch(Exception e){
				error.setDescription("Please send valid payment type");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid payment type");
				return stripeTransaction;
			}
			
			
			if(TextUtil.isEmptyOrNull(transactionAmountStr)){
				error.setDescription("Invalid Transaction Amount");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Amount");
				return stripeTransaction;
			}
			Double transactionAmount = Double.parseDouble(transactionAmountStr);
			transactionAmount = transactionAmount * 100;
			if(transactionAmount < 50){
				error.setDescription("Invalid Transaction Amount. Minimum Amount must be atleast $0.50");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Amount. Minimum Amount must be atleast $0.50");
				return stripeTransaction;
			}
			
			if(TextUtil.isEmptyOrNull(description)){
				error.setDescription("Stripe description is mandatory.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe description is mandatory");
				return stripeTransaction;
			}
			
			if(TextUtil.isEmptyOrNull(rtfCustIdStr)){
				error.setDescription("Customer Id is mandatory");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Customer Id is mandatory");
				return stripeTransaction;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(rtfCustIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid Customer Id");
					return stripeTransaction;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid Customer Id");
				return stripeTransaction;
			}
			
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Event Id is mandatory");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Event Id is mandatory");
				return stripeTransaction;
			}
			
			Integer eventId=null;
			Event event = null;
			try{
				eventId = Integer.parseInt(eventIdStr.trim());
				event = DAORegistry.getEventDAO().getEventById(eventId, productType);
				if(null == event){
					error.setDescription("Invalid Event");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Event");
					return stripeTransaction;
				}
			}catch(Exception e){
				error.setDescription("Please send valid EVENT Or TICKET Id");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid EVENT Or TICKET Id");
				return stripeTransaction;
			}
			
			/*if(platForm != null && (platForm.equals("IOS") || platForm.equals("ANDROID"))){
				
				String deviceId = ((HttpServletRequest)request).getHeader("deviceId");
				AutoCatsLockedTickets autoCatsLockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().
				getLockedTicketBySessionCategoryTicketGroupIdEventId(deviceId, ticketGroupId, event.getEventId());
				
				RTFPromotionalOfferTracking rtfPromoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().
				getPendingPromoTracking(ApplicationPlatForm.valueOf(platForm), customer.getId(), 
						deviceId, ticketGroupId, event.getEventId());
				
				if(rtfPromoCodeTracking != null){
					
					System.out.println("------------------------STRIPETRANSACTIONAPI: Referral Code is Applied- Begins----------");
					
					System.out.println("Original Transaction Amount:: "+transactionAmountStr);
					 
					Double trxAmt = rtfPromoCodeTracking.getTotalPrice() - rtfPromoCodeTracking.getAdditionalDiscountAmt() + rtfPromoCodeTracking.getServiceFees();
					transactionAmount = trxAmt * 100;
					System.out.println("Promotional Transaction Amount::"+trxAmt);
					
					System.out.println("------------------------STRIPETRANSACTIONAPI: Referral Code is Applied- Begins----------");
				}
			}*/
			
			CustomerCardInfo customerCardInfo = null;
			CustomerOrderCreditCard orderCreditCard = null;
			Map<String, Object> chargeParams = new HashMap<String, Object>();
			chargeParams.put("amount", transactionAmount.intValue());
			chargeParams.put("currency", "usd");
			chargeParams.put("description", description);
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			
			Stripe.apiKey = key;
			
			Boolean saveStripeOrder = false;
			
			switch (paymentMethod) {
			
				case CREDITCARD:
					
					
					if(TextUtil.isEmptyOrNull(trxBySavedCardStr)){
						error.setDescription("Transaction by Saved Card is Mandatory");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						stripeTransaction.setTransactionStatus("Fail");
						TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Transaction by Saved Card is Mandatory");
						return stripeTransaction;
					}
					
					Boolean trxBySavedCard = Boolean.valueOf(trxBySavedCardStr);
					
					if(trxBySavedCard){
						
						if(TextUtil.isEmptyOrNull(custCardIdStr)){
							error.setDescription("Customer Card Id is Mandatory");
							stripeTransaction.setError(error);
							stripeTransaction.setStatus(0);
							stripeTransaction.setTransactionStatus("Fail");
							TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Customer Card Id is Mandatory");
							return stripeTransaction;
						}
						
						customerCardInfo = DAORegistry.getCustomerCardInfoDAO().getActiveCustomerCardInfoById(Integer.parseInt(custCardIdStr.trim()));
						
						chargeParams.put("customer", customerCardInfo.getsCustId());
						chargeParams.put("card", customerCardInfo.getsCustCardId());
						saveStripeOrder = true;
						
						
					}else{
						
						try{
						if(TextUtil.isEmptyOrNull(token)){
							error.setDescription("Stripe token is mandatory.");
							stripeTransaction.setError(error);
							stripeTransaction.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe token is mandatory");
							return stripeTransaction;
						}
						
						if(TextUtil.isEmptyOrNull(saveCustCardStr)){
							saveCustCardStr="false";
						}
						
						
						List<CustomerCardInfo> customerCardInfos = DAORegistry.getCustomerCardInfoDAO().getAllActiveStripeRegisteredCardsByCustomerId(customerId); 
						
						String stripeCustId ="";
						
						if(null != customerCardInfos && !customerCardInfos.isEmpty()){
							for (CustomerCardInfo cardInfo : customerCardInfos) {
								stripeCustId = cardInfo.getsCustId();
								break;
							}
						}
						
						com.stripe.model.Customer stripeCustomer = null;
						Card card = null;
						
						
							if(null != stripeCustId && !stripeCustId.isEmpty() ){
								
								System.out.println("SC: Stripe Customer Id----->"+stripeCustId);
								
								stripeCustomer = com.stripe.model.Customer.retrieve(stripeCustId);
								
								Map<String, Object> params = new HashMap<String, Object>();
								params.put("source", token);
								
								card = (Card)stripeCustomer.getSources().create(params);
								
								System.out.println("SC: Stripe Customer card Id----->"+card.getId());
							    
							}else{
								
								String custName = customer.getCustomerName()+" "+customer.getLastName();
								// Create a Customer
								Map<String, Object> customerParams = new HashMap<String, Object>();
								customerParams.put("source", token);
								customerParams.put("description", "Customer Name:"+custName+", Customer Email :"+customer.getEmail());
								
								Map<String, String> initialMetadata = new HashMap<String, String>();
							    initialMetadata.put("customer_id", String.valueOf(customer.getId()));
							    initialMetadata.put("customer_email", customer.getEmail());
							    initialMetadata.put("customer_name", custName);
							    customerParams.put("metadata", initialMetadata);
							    stripeCustomer = com.stripe.model.Customer.create(customerParams);
							    
							    card = (Card) stripeCustomer.getSources().getData().get(0);
							    
							    System.out.println("NC: Stripe Customer Id----->"+stripeCustomer.getId());
							    System.out.println("NC: Stripe Customer Card Id----->"+card.getId());
							}

							customerCardInfo = new CustomerCardInfo();
							customerCardInfo.setCustomerId(customerId);
							customerCardInfo.setsCustCardId(card.getId());
							customerCardInfo.setsCustId(stripeCustomer.getId());
							customerCardInfo.setShowToCustomer(Boolean.valueOf(saveCustCardStr));
							customerCardInfo.setCardType(card.getBrand());
							customerCardInfo.setCreateDate(new Date());
							customerCardInfo.setLastUpdated(new Date());
							customerCardInfo.setMonth(card.getExpMonth());
							customerCardInfo.setYear(card.getExpYear());
							customerCardInfo.setLastFourDigit(card.getLast4());
							customerCardInfo.setStatus(Status.ACTIVE);
							
							chargeParams.put("customer", stripeCustomer.getId());
							chargeParams.put("card", card.getId());
						}catch(Exception e){
							e.printStackTrace();
							 System.out.println("Stripe Server error while creating customer : "+e.getLocalizedMessage());
							error.setDescription("Stripe Server error while creating customer");
							stripeTransaction.setError(error);
							stripeTransaction.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe Server error while creating customer");
							return stripeTransaction;
							
						}
						}
					
					break;
					
				case IPAY:
					
					if(TextUtil.isEmptyOrNull(token)){
						error.setDescription("Stripe token is mandatory.");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Product Type is mandatory");
						return stripeTransaction;
					}
					
					chargeParams.put("source", token);
								
					break;
				
				case GOOGLEPAY:
					
					if(TextUtil.isEmptyOrNull(token)){
						error.setDescription("Stripe token is mandatory.");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe token is mandatory");
						return stripeTransaction;
					}
					
					chargeParams.put("source", token);
					
					break;
	
	
				default:
					break;
			}
			
			System.out.println("INSIDE STRIPE TRANSACTION");
			
			Charge charge = null;
			try{
				
				Map<String, String> initialMetadata = new HashMap<String, String>();
				initialMetadata.put("Platform", String.valueOf(appPlatForm));
				initialMetadata.put("Source", String.valueOf(paymentMethod));
				initialMetadata.put("CustomerName", customer.getCustomerName()+" "+customer.getLastName());
				initialMetadata.put("CustomerEmail", customer.getEmail());
			    initialMetadata.put("EventName", event.getEventName());
			    initialMetadata.put("EventDate", event.getEventDateTime());
			    initialMetadata.put("VenueName", event.getVenueName());
			    initialMetadata.put("Zone", "LONG TICKET");
			    initialMetadata.put("Quantity", quantityStr);
			    chargeParams.put("metadata", initialMetadata);
			    
				System.out.println("STRIPE TRANSACTION - BEGINS: Payment Method :"+paymentMethodStr+", token :"+token+", description: "+description);
				
				charge = Charge.create(chargeParams);
				
				
				if(charge.getStatus().equalsIgnoreCase("succeeded")){
					
					System.out.println("STRIPE TRANSACTION STATUS :"+charge.getStatus()+", TRXID :"+charge.getId());
					
					try{
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(paymentMethodStr, description, transactionAmount, 
								token, charge.getId(), charge.getStatus());
						
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					try{
						
						if(customerCardInfo != null && !saveStripeOrder){
							
							DAORegistry.getCustomerCardInfoDAO().saveOrUpdate(customerCardInfo);
							
							orderCreditCard = new CustomerOrderCreditCard();
							orderCreditCard.setCustomerId(customerId);
							orderCreditCard.setCardId(customerCardInfo.getId());
							orderCreditCard.setTicketGroupId(-1);
							orderCreditCard.setTrxAmount(transactionAmount);
							orderCreditCard.setTransactionId(charge.getId());
							DAORegistry.getCustomerOrderCreditCardDAO().saveOrUpdate(orderCreditCard);
							
						}else if(null != customerCardInfo && saveStripeOrder){
								
							orderCreditCard = new CustomerOrderCreditCard();
							orderCreditCard.setCustomerId(customerId);
							orderCreditCard.setCardId(customerCardInfo.getId());
							orderCreditCard.setTicketGroupId(-1);
							orderCreditCard.setTrxAmount(transactionAmount);
							orderCreditCard.setTransactionId(charge.getId());
							DAORegistry.getCustomerOrderCreditCardDAO().saveOrUpdate(orderCreditCard);
							
						}
						
					}catch (Exception e) {
						e.printStackTrace();
					}
					
				}else{
					
					String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():charge.getStatus();
					System.out.println("STRIPE TRANSACTION FAILURE : "+failure);
					try{
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(paymentMethodStr, description, transactionAmount, 
								token, "", failure);
						
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					error.setDescription(failure);
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					stripeTransaction.setTransactionStatus("Fail");
					TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,charge.getFailureMessage());
					return stripeTransaction;
				}
				
				System.out.println("STRIPE TRANSACTION - ENDS: Payment Method :"+paymentMethodStr+", token :"+token+", description: "+description);
				
			}catch (Exception e) {
				try{	
					
					String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():e.getMessage();
					DAORegistry.getQueryManagerDAO().trackStripeTransaction(paymentMethodStr, description, transactionAmount, 
							token, "", failure);
					
				}catch (Exception e1) {
					e1.printStackTrace();
				}
				error.setDescription(e.getMessage());
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,"Exception Occurred");
				return stripeTransaction;
			}
			stripeTransaction.setStatus(1);
			stripeTransaction.setTransactionStatus(charge.getStatus());
			stripeTransaction.setTransactionId(charge.getId());
			TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,"Success");
			return stripeTransaction;
		}catch(Exception e){
			error.setDescription(e.getMessage());
			stripeTransaction.setError(error);
			stripeTransaction.setStatus(0);
			stripeTransaction.setTransactionStatus("Fail");
			TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,"Exception Occurred");
			return stripeTransaction;
		}
	}
	

	@RequestMapping(value="/StripeTransaction",method=RequestMethod.POST)
	public StripeTransaction stripeTransactionForCustomer(HttpServletRequest request){
		Error error = new Error();
		StripeTransaction stripeTransaction = new StripeTransaction();
		String platForm = request.getParameter("platForm");
		try{
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			ApplicationPlatForm appPlatForm = null;
			try{
				appPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid application platform");
				return stripeTransaction;
			}
			
			if(appPlatForm.equals(ApplicationPlatForm.TICK_TRACKER)) {
				
			}else {
				error.setDescription("You can only redeem tickets using reward dollars");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You can only redeem tickets using reward dollars");
				return stripeTransaction;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
						return stripeTransaction;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
					return stripeTransaction;
				}
			}else{
				error.setDescription("You are not authorized.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"You are not authorized");
				return stripeTransaction;
			}
			
			
			String productTypeStr = request.getParameter("productType");
			String orderIdStr=request.getParameter("orderId");
			String paymentMethodStr = request.getParameter("paymentMethod");
			String transactionAmountStr = request.getParameter("transactionAmount");
			String token = request.getParameter("token");
			String description = request.getParameter("description");
			String rtfCustIdStr=request.getParameter("rtfCustId");
			String saveCustCardStr=request.getParameter("saveCustCard");
			String trxBySavedCardStr=request.getParameter("trxBySavedCard");
			String tGroupIdStr=request.getParameter("tGroupId");
			String custCardIdStr=request.getParameter("custCardId");
			String deviceId=request.getParameter("deviceId");
			
			System.out.println("=================================STRIPE BEGINS===============================");
			System.out.println("--------------------Param-------------------------");
			System.out.println("platForm ---->"+platForm);
			System.out.println("productTypeStr ---->"+productTypeStr);
			System.out.println("paymentMethodStr ---->"+paymentMethodStr);
			System.out.println("transactionAmountStr ---->"+transactionAmountStr);
			System.out.println("token ---->"+token);
			System.out.println("description ---->"+description);
			System.out.println("rtfCustIdStr ---->"+rtfCustIdStr);
			System.out.println("saveCustCardStr ---->"+saveCustCardStr);
			System.out.println("trxBySavedCardStr ---->"+trxBySavedCardStr);
			System.out.println("tGroupIdStr ---->"+tGroupIdStr);
			System.out.println("custCardIdStr ---->"+custCardIdStr);
			System.out.println("----------------------Param-----------------------");
			
			
			if(TextUtil.isEmptyOrNull(platForm)){
				/*error.setDescription("Application Platform is mandatory.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Application Platform is mandatory");
				return stripeTransaction;
				platForm = "DESKTOP_SITE";*/
			}
			
			
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Product Type is mandatory");
				return stripeTransaction;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid product type");
					return stripeTransaction;
				}
			}
			
			if(TextUtil.isEmptyOrNull(paymentMethodStr)){
				error.setDescription("Payment Method is Mandatory");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Payment Method is Mandatory");
				return stripeTransaction;
			}
			
			PaymentMethod paymentMethod = PaymentMethod.CREDITCARD;
			try{
				paymentMethod = PaymentMethod.valueOf(paymentMethodStr);
			}catch(Exception e){
				error.setDescription("Please send valid payment type");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid payment type");
				return stripeTransaction;
			}
			
			
			if(TextUtil.isEmptyOrNull(transactionAmountStr)){
				error.setDescription("Invalid Transaction Amount");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Amount");
				return stripeTransaction;
			}
			Double transactionAmount = Double.parseDouble(transactionAmountStr);
			transactionAmount = transactionAmount * 100;
			if(transactionAmount < 50){
				error.setDescription("Invalid Transaction Amount. Minimum Amount must be atleast $0.50");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Transaction Amount. Minimum Amount must be atleast $0.50");
				return stripeTransaction;
			}
			
			if(TextUtil.isEmptyOrNull(description)){
				error.setDescription("Stripe description is mandatory.");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe description is mandatory");
				return stripeTransaction;
			}
			
			if(TextUtil.isEmptyOrNull(rtfCustIdStr)){
				error.setDescription("Customer Id is mandatory");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Customer Id is mandatory");
				return stripeTransaction;
			}
			
			Integer customerId=null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(rtfCustIdStr.trim());
				customer =  CustomerUtil.getCustomerById(customerId);
				if(null == customer){
					error.setDescription("Please send valid Customer Id");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid Customer Id");
					return stripeTransaction;
				}
			}catch(Exception e){
				error.setDescription("Please send valid Customer Id");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid Customer Id");
				return stripeTransaction;
			}
			
			if(TextUtil.isEmptyOrNull(tGroupIdStr)){
				error.setDescription("Ticket Id is mandatory");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Ticket Id is mandatory");
				return stripeTransaction;
			}
			
			Integer ticketGroupId=null;
			CategoryTicketGroup categoryTicketGroup= null;
			Event event = null;
			try{
				ticketGroupId = Integer.parseInt(tGroupIdStr.trim());
				if(platForm != null && platForm.equals("TICK_TRACKER")){
					categoryTicketGroup =  DAORegistry.getCategoryTicketGroupDAO().get(ticketGroupId);
				}else{
					categoryTicketGroup =  DAORegistry.getCategoryTicketGroupDAO().get(ticketGroupId);
				}
				
				if(null == categoryTicketGroup){
					error.setDescription("Invalid Ticket");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Ticket");
					return stripeTransaction;
				}
				
				event = DAORegistry.getEventDAO().getEventByEventId(categoryTicketGroup.getEventId(), productType);
				if(event==null){
					event = DAORegistry.getEventDAO().getEventById(categoryTicketGroup.getEventId(), productType);
				}
				if(null == event){
					error.setDescription("Invalid Event");
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Invalid Event");
					return stripeTransaction;
				}
					
			}catch(Exception e){
				error.setDescription("Please send valid EVENT Or TICKET Id");
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Please send valid EVENT Or TICKET Id");
				return stripeTransaction;
			}
			
			/*if(platForm != null && (platForm.equals("IOS") || platForm.equals("ANDROID"))){
				
				String deviceId = ((HttpServletRequest)request).getHeader("deviceId");
				AutoCatsLockedTickets autoCatsLockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().
				getLockedTicketBySessionCategoryTicketGroupIdEventId(deviceId, ticketGroupId, event.getEventId());
				
				RTFPromotionalOfferTracking rtfPromoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().
				getPendingPromoTracking(ApplicationPlatForm.valueOf(platForm), customer.getId(), 
						deviceId, ticketGroupId, event.getEventId());
				
				if(rtfPromoCodeTracking != null){
					
					System.out.println("------------------------STRIPETRANSACTIONAPI: Referral Code is Applied- Begins----------");
					
					System.out.println("Original Transaction Amount:: "+transactionAmountStr);
					 
					Double trxAmt = rtfPromoCodeTracking.getTotalPrice() - rtfPromoCodeTracking.getAdditionalDiscountAmt() + rtfPromoCodeTracking.getServiceFees();
					transactionAmount = trxAmt * 100;
					System.out.println("Promotional Transaction Amount::"+trxAmt);
					
					System.out.println("------------------------STRIPETRANSACTIONAPI: Referral Code is Applied- Begins----------");
				}
			}*/
			
			CustomerCardInfo customerCardInfo = null;
			CustomerOrderCreditCard orderCreditCard = null;
			Map<String, Object> chargeParams = new HashMap<String, Object>();
			chargeParams.put("amount", transactionAmount.intValue());
			chargeParams.put("currency", "usd");
			chargeParams.put("description", description);
			String key= DAORegistry.getQueryManagerDAO().getStripeCredentialsByType();
			
			Stripe.apiKey = key;
			
			Boolean saveStripeOrder = false;
			
			switch (paymentMethod) {
			
				case CREDITCARD:
					
					
					if(TextUtil.isEmptyOrNull(trxBySavedCardStr)){
						error.setDescription("Transaction by Saved Card is Mandatory");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						stripeTransaction.setTransactionStatus("Fail");
						TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Transaction by Saved Card is Mandatory");
						return stripeTransaction;
					}
					
					Boolean trxBySavedCard = Boolean.valueOf(trxBySavedCardStr);
					
					if(trxBySavedCard){
						
						if(TextUtil.isEmptyOrNull(custCardIdStr)){
							error.setDescription("Customer Card Id is Mandatory");
							stripeTransaction.setError(error);
							stripeTransaction.setStatus(0);
							stripeTransaction.setTransactionStatus("Fail");
							TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Customer Card Id is Mandatory");
							return stripeTransaction;
						}
						
						customerCardInfo = DAORegistry.getCustomerCardInfoDAO().getActiveCustomerCardInfoById(Integer.parseInt(custCardIdStr.trim()));
						
						chargeParams.put("customer", customerCardInfo.getsCustId());
						chargeParams.put("card", customerCardInfo.getsCustCardId());
						saveStripeOrder = true;
						
						
					}else{
						
						try{
						if(TextUtil.isEmptyOrNull(token)){
							error.setDescription("Stripe token is mandatory.");
							stripeTransaction.setError(error);
							stripeTransaction.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe token is mandatory");
							return stripeTransaction;
						}
						
						if(TextUtil.isEmptyOrNull(saveCustCardStr)){
							saveCustCardStr="false";
						}
						
						
						List<CustomerCardInfo> customerCardInfos = DAORegistry.getCustomerCardInfoDAO().getAllActiveStripeRegisteredCardsByCustomerId(customerId); 
						
						String stripeCustId ="";
						
						if(null != customerCardInfos && !customerCardInfos.isEmpty()){
							for (CustomerCardInfo cardInfo : customerCardInfos) {
								stripeCustId = cardInfo.getsCustId();
								break;
							}
						}
						
						com.stripe.model.Customer stripeCustomer = null;
						Card card = null;
						
						
							if(null != stripeCustId && !stripeCustId.isEmpty() ){
								
								System.out.println("SC: Stripe Customer Id----->"+stripeCustId);
								
								stripeCustomer = com.stripe.model.Customer.retrieve(stripeCustId);
								
								Map<String, Object> params = new HashMap<String, Object>();
								params.put("source", token);
								
								card = (Card)stripeCustomer.getSources().create(params);
								
								System.out.println("SC: Stripe Customer card Id----->"+card.getId());
							    
							}else{
								
								String custName = customer.getCustomerName()+" "+customer.getLastName();
								// Create a Customer
								Map<String, Object> customerParams = new HashMap<String, Object>();
								customerParams.put("source", token);
								customerParams.put("description", "Customer Name:"+custName+", Customer Email :"+customer.getEmail());
								
								Map<String, String> initialMetadata = new HashMap<String, String>();
							    initialMetadata.put("customer_id", String.valueOf(customer.getId()));
							    initialMetadata.put("customer_email", customer.getEmail());
							    initialMetadata.put("customer_name", custName);
							    customerParams.put("metadata", initialMetadata);
							    stripeCustomer = com.stripe.model.Customer.create(customerParams);
							    
							    card = (Card) stripeCustomer.getSources().getData().get(0);
							    
							    System.out.println("NC: Stripe Customer Id----->"+stripeCustomer.getId());
							    System.out.println("NC: Stripe Customer Card Id----->"+card.getId());
							}

							customerCardInfo = new CustomerCardInfo();
							customerCardInfo.setCustomerId(customerId);
							customerCardInfo.setsCustCardId(card.getId());
							customerCardInfo.setsCustId(stripeCustomer.getId());
							customerCardInfo.setShowToCustomer(Boolean.valueOf(saveCustCardStr));
							customerCardInfo.setCardType(card.getBrand());
							customerCardInfo.setCreateDate(new Date());
							customerCardInfo.setLastUpdated(new Date());
							customerCardInfo.setMonth(card.getExpMonth());
							customerCardInfo.setYear(card.getExpYear());
							customerCardInfo.setLastFourDigit(card.getLast4());
							customerCardInfo.setStatus(Status.ACTIVE);
							
							chargeParams.put("customer", stripeCustomer.getId());
							chargeParams.put("card", card.getId());
						}catch(Exception e){
							e.printStackTrace();
							 System.out.println("Stripe Server error while creating customer : "+e.getLocalizedMessage());
							error.setDescription("Stripe Server error while creating customer");
							stripeTransaction.setError(error);
							stripeTransaction.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe Server error while creating customer");
							return stripeTransaction;
							
						}
						}
					
					break;
					
				case IPAY:
					
					if(TextUtil.isEmptyOrNull(token)){
						error.setDescription("Stripe token is mandatory.");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Product Type is mandatory");
						return stripeTransaction;
					}
					
					chargeParams.put("source", token);
								
					break;
				
				case GOOGLEPAY:
					
					if(TextUtil.isEmptyOrNull(token)){
						error.setDescription("Stripe token is mandatory.");
						stripeTransaction.setError(error);
						stripeTransaction.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.STRIPETRANSACTIONFORCUSTOMER,"Stripe token is mandatory");
						return stripeTransaction;
					}
					
					chargeParams.put("source", token);
					
					break;
	
	
				default:
					break;
			}
			
			System.out.println("INSIDE STRIPE TRANSACTION");
			
			Charge charge = null;
			try{
				
				Map<String, String> initialMetadata = new HashMap<String, String>();
				initialMetadata.put("Platform", String.valueOf(appPlatForm));
				initialMetadata.put("Source", String.valueOf(paymentMethod));
				initialMetadata.put("CustomerName", customer.getCustomerName()+" "+customer.getLastName());
				initialMetadata.put("CustomerEmail", customer.getEmail());
			    initialMetadata.put("EventName", event.getEventName());
			    initialMetadata.put("EventDate", event.getEventDateTime());
			    initialMetadata.put("VenueName", event.getVenueName());
			    initialMetadata.put("Zone", categoryTicketGroup.getActualSection());
			    initialMetadata.put("Quantity", String.valueOf(categoryTicketGroup.getQuantity()));
			    chargeParams.put("metadata", initialMetadata);
			    
				System.out.println("STRIPE TRANSACTION - BEGINS: Payment Method :"+paymentMethodStr+", token :"+token+", description: "+description);
				
				charge = Charge.create(chargeParams);
				
				
				if(charge.getStatus().equalsIgnoreCase("succeeded")){
					
					System.out.println("STRIPE TRANSACTION STATUS :"+charge.getStatus()+", TRXID :"+charge.getId());
					
					try{
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(paymentMethodStr, description, transactionAmount, 
								token, charge.getId(), charge.getStatus());
						
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					try{
						
						if(customerCardInfo != null && !saveStripeOrder){
							
							DAORegistry.getCustomerCardInfoDAO().saveOrUpdate(customerCardInfo);
							
							orderCreditCard = new CustomerOrderCreditCard();
							orderCreditCard.setCustomerId(customerId);
							orderCreditCard.setCardId(customerCardInfo.getId());
							orderCreditCard.setTicketGroupId(categoryTicketGroup.getId());
							orderCreditCard.setTrxAmount(transactionAmount);
							orderCreditCard.setTransactionId(charge.getId());
							DAORegistry.getCustomerOrderCreditCardDAO().saveOrUpdate(orderCreditCard);
							
						}else if(null != customerCardInfo && saveStripeOrder){
								
							orderCreditCard = new CustomerOrderCreditCard();
							orderCreditCard.setCustomerId(customerId);
							orderCreditCard.setCardId(customerCardInfo.getId());
							orderCreditCard.setTicketGroupId(categoryTicketGroup.getId());
							orderCreditCard.setTrxAmount(transactionAmount);
							orderCreditCard.setTransactionId(charge.getId());
							DAORegistry.getCustomerOrderCreditCardDAO().saveOrUpdate(orderCreditCard);
							
						}
						
					}catch (Exception e) {
						e.printStackTrace();
					}
					
				}else{
					
					String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():charge.getStatus();
					System.out.println("STRIPE TRANSACTION FAILURE : "+failure);
					try{
						DAORegistry.getQueryManagerDAO().trackStripeTransaction(paymentMethodStr, description, transactionAmount, 
								token, "", failure);
						
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					error.setDescription(failure);
					stripeTransaction.setError(error);
					stripeTransaction.setStatus(0);
					stripeTransaction.setTransactionStatus("Fail");
					TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,charge.getFailureMessage());
					return stripeTransaction;
				}
				
				System.out.println("STRIPE TRANSACTION - ENDS: Payment Method :"+paymentMethodStr+", token :"+token+", description: "+description);
				
			}catch (Exception e) {
				try{	
					
					String failure = null !=charge.getFailureMessage()?charge.getFailureMessage():e.getMessage();
					DAORegistry.getQueryManagerDAO().trackStripeTransaction(paymentMethodStr, description, transactionAmount, 
							token, "", failure);
					
				}catch (Exception e1) {
					e1.printStackTrace();
				}
				error.setDescription(e.getMessage());
				stripeTransaction.setError(error);
				stripeTransaction.setStatus(0);
				stripeTransaction.setTransactionStatus("Fail");
				TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,"Exception Occurred");
				return stripeTransaction;
			}
			stripeTransaction.setStatus(1);
			stripeTransaction.setTransactionStatus(charge.getStatus());
			stripeTransaction.setTransactionId(charge.getId());
			TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,"Success");
			return stripeTransaction;
		}catch(Exception e){
			error.setDescription(e.getMessage());
			stripeTransaction.setError(error);
			stripeTransaction.setStatus(0);
			stripeTransaction.setTransactionStatus("Fail");
			TrackingUtils.webServiceTracking(request,WebServiceActionType.STRIPETRANSACTION,"Exception Occurred");
			return stripeTransaction;
		}
	}
	
	
	@RequestMapping(value = "/GetOneSignalCustomers", method=RequestMethod.POST)
	public @ResponsePayload CustomerStats getOneSignalCustomers(HttpServletRequest request,HttpServletResponse response){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			   URL url = new URL("https://onesignal.com/api/v1/players/csv_export?app_id=e15a0313-b54a-41ed-af32-a95991037a40");
			   HttpURLConnection con = (HttpURLConnection)url.openConnection();
			   con.setUseCaches(false);
			   con.setDoOutput(true);
			   con.setDoInput(true);

			   String apiKey = "ZjVhNTE5ZGItNjY2OC00ZWZkLWEzYmMtMDc0MDQ5ZWQwMWFm";
			   
			   con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			   con.setRequestProperty("Authorization", "Basic "+apiKey);
			   con.setRequestMethod("POST");
		
			   int httpResponse = con.getResponseCode();
			   System.out.println("httpResponse: " + httpResponse);
			   System.out.println("Content Lenght : "+con.getContentLength());
			  
			   
			   BufferedReader  br = new BufferedReader(new InputStreamReader((con.getInputStream())));
			   StringBuilder sb = new StringBuilder();
			   String output;
			   while ((output = br.readLine()) != null) {
			     sb.append(output);
			   }
			   
			   JSONObject json = new JSONObject(sb.toString());
			   String fileUrl =  json.getString("csv_file_url");
			   System.out.println(sb.toString());
			   con.disconnect();
		       dtoResponse.setStatus(1);
		       dtoResponse.setMessage(fileUrl);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while Generating Csv file.");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		return dtoResponse;
	}
	
	
	
	@RequestMapping(value = "/UpdateOneSignalCustomers", method=RequestMethod.POST)
	public @ResponsePayload CustomerStats updateOneSignalNotificationIds(HttpServletRequest request,HttpServletResponse response){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			 	String fileUrl = request.getParameter("fileUrl");
			 	String apiKey = "ZjVhNTE5ZGItNjY2OC00ZWZkLWEzYmMtMDc0MDQ5ZWQwMWFm";
			   
			    URL fileReqest = new URL(fileUrl);
			    HttpURLConnection con1 = (HttpURLConnection)fileReqest.openConnection();
			    con1.setUseCaches(false);
				con1.setDoOutput(true);
				//con1.setDoInput(true);
			    con1.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			    con1.setRequestProperty("Authorization", "Basic "+apiKey);
			    con1.setRequestMethod("GET");
			    
			    try {
			    	    InputStream fileInputStream = fileReqest.openStream();
				        BufferedInputStream bis = new BufferedInputStream(fileInputStream);
				        File file = new File("//rtw//RewardTheFan//notificationUsersZip.csv.gz");
				        FileOutputStream fis = new FileOutputStream(file);
				        byte[] buffer = new byte[1024];
				        int count=0;
				        while((count = bis.read(buffer,0,1024)) != -1)
				        {
				            fis.write(buffer, 0, count);
				        }
				        fis.close();
				        bis.close();
				        con1.disconnect();
				        
				        try {
				        	File deCompressedFile = new File("//rtw//RewardTheFan//notificationUsers.csv");
					        FileInputStream fileIn = new FileInputStream(file);
					        GZIPInputStream gZIPInputStream = new GZIPInputStream(fileIn);
					        FileOutputStream fileOutputStream = new FileOutputStream(deCompressedFile);
					        int bytes_read;
					        while ((bytes_read = gZIPInputStream.read(buffer)) > 0) {
					        	fileOutputStream.write(buffer, 0, bytes_read);

					        }
					        gZIPInputStream.close();
					        fileOutputStream.close();
					        
					        //StringBuffer sql = new StringBuffer("INSERT INTO onesignal_notification (notification_id) values ");
					        BufferedReader reader = new BufferedReader(new FileReader(deCompressedFile));
					        String line = "";
					        String notificationId = "";
					        boolean isHeaderRow = true;
					        List<String> ids = DAORegistry.getQueryManagerDAO().getAllOneSignalNotifications();
					        if(ids==null){
					        	 dtoResponse.setStatus(0);
							     dtoResponse.setMessage("No onesignal notification ids found in system.");
					        }
					        int skippedCount = 0;
					        int insertCount = 0;
					        while((line = reader.readLine()) !=null){
					        	String row[] = line.split(",");
					        	if(row.length > 1 && !isHeaderRow){
					        		notificationId = row[1];
					        		if(notificationId!=null && !notificationId.isEmpty() && !ids.contains(notificationId.trim())){
					        			DAORegistry.getQueryManagerDAO().insertOneSignalNotifications(notificationId);
					        			insertCount++;
					        		}else{
					        			skippedCount++;
					        		}
					        	}
					        	isHeaderRow = false;
					        }
					        System.out.println("INSERTED NEW RECORDS : "+insertCount);
					        System.out.println("SKIPPED OLD RECORDS : "+skippedCount);
					        dtoResponse.setStatus(0);
					        dtoResponse.setMessage(insertCount+" new notification ids inserted into database.");
							return dtoResponse;
						} catch (Exception e) {
							e.printStackTrace();
							error.setDescription("Error occured while update onesignal notifications ids.");
							dtoResponse.setError(error);
							dtoResponse.setStatus(0);
							return dtoResponse;
						}
				        
				} catch (Exception e) {
					e.printStackTrace();
					error.setDescription("Error occured while Downloading onesignal csv file.");
					dtoResponse.setError(error);
					dtoResponse.setStatus(0);
					return dtoResponse;
				}
			    
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Inserting onesignal notifications ids.");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
	}
}	