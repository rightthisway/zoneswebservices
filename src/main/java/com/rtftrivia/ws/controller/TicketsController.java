package com.rtftrivia.ws.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.AffiliatePromoCodeTracking;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.AutoCatsLockedTickets;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.CheckOutOfferAppliedAudit;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCardInfo;
import com.zonesws.webservices.data.CustomerCheckoutVisitOffer;
import com.zonesws.webservices.data.CustomerFavoritesHandler;
import com.zonesws.webservices.data.CustomerSuperFan;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.FavouriteEvents;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.data.PreOrderPageAudit;
import com.zonesws.webservices.data.PreOrderPageEmailTracking;
import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.data.ZoneRGBColor;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.enums.LockedTicketStatus;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.PromotionalType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.WebServiceUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.FavoriteUtil;
import com.zonesws.webservices.utils.MapUtil;
import com.zonesws.webservices.utils.PaginationUtil;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TicketGroupQty;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.ArtistList;
import com.zonesws.webservices.utils.list.GetTicketGroup;
import com.zonesws.webservices.utils.list.GrandChildCategoryResponse;
import com.zonesws.webservices.utils.list.TicketGroupLockStatus;
import com.zonesws.webservices.utils.list.TicketList;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/GetEventDetails","/GetTicketGroup","/GetAllGrandChildCategory","/AutoCatsLockTicketGroup","/GetArtistByGrandChildCateoryId",
	"/GetLockedTicketsTimeLeft","/UnLockTicketGroup"})
public class TicketsController {
	private MailManager mailManager;
	private ZonesProperty properties;

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}

	@RequestMapping(value="/AutoCatsLockTicketGroup",method=RequestMethod.POST)
	public @ResponsePayload TicketGroupLockStatus autoCatsLockTicketGroup(HttpServletRequest request, Model model,HttpServletResponse response){
		TicketGroupLockStatus ticketGroupLockStatus =new TicketGroupLockStatus();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"You are not authorized");
				return ticketGroupLockStatus;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"You are not authorized");
						return ticketGroupLockStatus;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"You are not authorized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("You are not authorized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"You are not authorized");
				return ticketGroupLockStatus;
			}
			
			try{
				//Add WebConfig SourceKey in the response for Credit Card Encryption
				ticketGroupLockStatus.setEncryptionKey(ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey());
			}catch(Exception e){
				e.printStackTrace();
			}
				
			String productTypeStr = request.getParameter("productType");
			String sessionIdOrDeviceIdStr =request.getParameter("sessionIdOrDeviceId");
			String eventIdStr = request.getParameter("eventId");
			String ticketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String platForm = request.getParameter("platForm");
			String customerIdStr =request.getParameter("customerId");
			String ipAddress = (null != request.getParameter("clientIPAddress"))?request.getParameter("clientIPAddress"):ip;
			String isFanPriceStr = request.getParameter("isFanPrice");
			String lockIdStr =request.getParameter("lockId");
			//Offer for downloading tickets on day before the event
			String ticketDownloadOfferStr =request.getParameter("onDayBeforeTheEvent");
		
									  
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Product Type is mandatory");
				return ticketGroupLockStatus;
			}
			ProductType productType;
			try{
				productType  = ProductType.valueOf(productTypeStr);
			}catch(Exception e){
				error.setDescription("Please Choose valid product type.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Please Choose valid product type.");
				return ticketGroupLockStatus;
			}
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
					
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Event Id is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Event Id is mandatory");
				return ticketGroupLockStatus;
			}
			
			Integer eventId= null;
			Event event = null;
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				try {
					eventId=Integer.parseInt(eventIdStr);
					
					event = DAORegistry.getEventDAO().getEventByEventId(eventId, productType);
						
					if(event==null || ( null == event.getVenueCategoryName() || null == event.getVenueId()) ){
						error.setDescription("Event is not recognized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Event is not recognized");
						return ticketGroupLockStatus;
					}
					
					/*//Ulaganathan : To add Venue Map
					MapUtil.setVenueZoneMap(event);*/
					MapUtil.getSVGTextFromUtil(event, applicationPlatForm);
					
					
					if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
						error.setDescription("Event is not recognized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Event is not recognized");
						return ticketGroupLockStatus;
					}
					
				} catch (Exception e) {
					error.setDescription("Event is not recognized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Event is not recognized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("Event is not recognized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Event is not recognized");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(sessionIdOrDeviceIdStr)){
				error.setDescription("SessionId or DeviceId is mandatory to lock ticket.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"SessionId or DeviceId is mandatory to lock ticket");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(ticketGroupIdStr)){
				error.setDescription("Ticket Group Id is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Ticket Group Id is mandatory");
				return ticketGroupLockStatus;
			}
			
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Platform  is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Platform is mandatory");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(ticketDownloadOfferStr)){
				ticketDownloadOfferStr = "false";
			}
			
			
			CustomerSuperFan superFan = null;
			Integer customerId = null;
			Customer customer=null;
			
				if(!TextUtil.isEmptyOrNull(customerIdStr)){
					
					try {
						customerId=Integer.parseInt(customerIdStr);
				        customer=CustomerUtil.getCustomerById(customerId);
				        if(customer==null){
							error.setDescription("Customer is not recognized.");
							ticketGroupLockStatus.setError(error);
							ticketGroupLockStatus.setStatus(0);
							TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Customer is not recognized");
							return ticketGroupLockStatus;
						}
					} catch (Exception e) {
						error.setDescription("Customer is not recognized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Customer is not recognized");
						return ticketGroupLockStatus;
				}
				CustomerSuperFan customerSuperFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
				if(null != customerSuperFan  &&  event !=  null){
					event.setIsSuperFanEvent(FavoriteUtil.markArtistAsLoyalFan(customerSuperFan, event));
				}
				
				FavouriteEvents favouriteEvent = DAORegistry.getFavouriteEventsDAO().getActiveFavouriteEventsById(eventId, customerId);
				if(null != favouriteEvent ){
					event.setIsFavoriteEvent(true);
				}
				event.setCustomerId(customerId);
			}
			
			Integer ticketGroupId = null;
			if(null != ticketGroupIdStr && !ticketGroupIdStr.isEmpty()){
				
				try{
					ticketGroupId = Integer.parseInt(ticketGroupIdStr);
				}catch(Exception e){
					error.setDescription("Category Ticket Group Id is not recognized .");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Category Ticket Group Id is not recognized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("Category Ticket Group Id is not recognized .");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Category Ticket Group Id is not recognized");
				return ticketGroupLockStatus;
			}
			
			
			String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			
			Integer lockId = null;
			AutoCatsLockedTickets lockedTicket = null;
			if(null != lockIdStr && !lockIdStr.isEmpty()){
				try{
					lockId = Integer.parseInt(lockIdStr);
					lockedTicket = DAORegistry.getAutoCatsLockedTicketDAO().get(lockId);
					
					if(lockedTicket == null ){
						ticketGroupLockStatus.setLockId(null);
						ticketGroupLockStatus.setStatus(1);
						ticketGroupLockStatus.setButtonValue("Find more tickets for this event");
						ticketGroupLockStatus.setOverRideButton(true);
						ticketGroupLockStatus.setLockStatus("Failure");
						ticketGroupLockStatus.setMessage("Your time to purchase the ticket has expired.<br />  " +
								"Please visit the event again to select the same tickets if available or try purchasing different set of tickets.");
						return ticketGroupLockStatus;
					}
					
				}catch(Exception e){
					ticketGroupLockStatus.setLockId(null);
					ticketGroupLockStatus.setStatus(1);
					ticketGroupLockStatus.setButtonValue("Find more tickets for this event");
					ticketGroupLockStatus.setOverRideButton(true);
					ticketGroupLockStatus.setLockStatus("Failure");
					ticketGroupLockStatus.setMessage("Your time to purchase the ticket has expired. <br /> " +
							"Please visit the event again to select the same tickets if available or try purchasing different set of tickets.");
					return ticketGroupLockStatus;
				}
			}
			
						
			boolean lockedBySameDevice =false,lockedByDifferentDevice=false;
			
			Collection<ZoneRGBColor> zoneRgbColors =DAORegistry.getZoneRGBColorDAO().getAll();
			Map<String, ZoneRGBColor> rgbColorMap = new HashMap<String, ZoneRGBColor>();
			for (ZoneRGBColor zoneRGBColor : zoneRgbColors) {
				if(ProductType.PRESALEZONETICKETS.equals(productType)){
					rgbColorMap.put(zoneRGBColor.getZone().replaceAll("_","").toUpperCase(), zoneRGBColor);
				}else{
					rgbColorMap.put(zoneRGBColor.getZone().replaceAll(" +","").toUpperCase(), zoneRGBColor);
				}
				
			}
			
			//List<AutoCatsTicketGroup> ticketGroups = new ArrayList<AutoCatsTicketGroup>();
			CategoryTicketGroup ticketGroup = null;
			
			if(null != lockedTicket && lockedTicket.getLockStatus().equals(LockedTicketStatus.DELETED)){
				
				ticketGroupLockStatus.setLockId(null);
				ticketGroupLockStatus.setStatus(1);
				ticketGroupLockStatus.setButtonValue("Find more tickets for this event");
				ticketGroupLockStatus.setOverRideButton(true);
				ticketGroupLockStatus.setLockStatus("Failure");
				ticketGroupLockStatus.setMessage("Your time to purchase the ticket has expired. <br /> " +
				"Please visit the event again to select the same tickets if available or try purchasing different set of tickets.");
				return ticketGroupLockStatus;
				
			}else if (null != lockedTicket && lockedTicket.getLockStatus().equals(LockedTicketStatus.ACTIVE)){
				
				lockedBySameDevice =true;
				
				ticketGroup = DAORegistry.getCategoryTicketGroupDAO().getBroadCastedCategoryTicketsbyId(ticketGroupId);
				
				if(null == ticketGroup){
					ticketGroupLockStatus.setLockId(null);
					ticketGroupLockStatus.setStatus(1);
					ticketGroupLockStatus.setButtonValue("Find more tickets for this event");
					ticketGroupLockStatus.setOverRideButton(true);
					ticketGroupLockStatus.setLockStatus("Failure");
					ticketGroupLockStatus.setMessage("It looks like someone else already purchased this ticket. <br />" +
							"Please try to visit the event again and select the same tickets if they were released again or " +
							"try purchasing different set of tickets.");
					return ticketGroupLockStatus;
				}
				
			}else{
				
				List<AutoCatsLockedTickets> lockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketByEventId(eventId,productType);
				
				
				for (AutoCatsLockedTickets lockTicket : lockedTickets) {
					Integer lockticketGroupId =lockTicket.getCatgeoryTicketGroupId();
					if(lockTicket.getSessionId().equalsIgnoreCase(sessionIdOrDeviceIdStr) && 
							(lockticketGroupId.equals(ticketGroupId))){
						lockedTicket= lockTicket;
						lockedBySameDevice =true;
						break;
					}else if(lockticketGroupId.equals(ticketGroupId)){
						lockedByDifferentDevice=true;
						break;
					}
				}
				
				
				 switch (productType) {
					case MINICATS:
						//ticketGroups = DAORegistry.getMinicatsQueryManagerDAO().getMinicatsTicketsByTicketGroupId(ticketGroupId,rgbColorMap,lockedTicketMap);
						break;
						
					case LASTROWMINICATS:
						//ticketGroups = DAORegistry.getLastrowMinicatsQueryManagerDAO().getLastrowMinicatsTicketsByTicketGroupId(ticketGroupId,rgbColorMap,lockedTicketMap);
						break;
						
					case VIPMINICATS:
						//ticketGroups = DAORegistry.getVipMinicatsQueryManagerDAO().getVipMinicatsTicketsByTicketGroupId(ticketGroupId,rgbColorMap,lockedTicketMap);
						break;
						
					case VIPLASTROWMINICATS:
						//ticketGroups = DAORegistry.getVipLastrowMinicatsQueryManagerDAO().getVipLastrowMinicatsTicketsByTicketGroupId(ticketGroupId,rgbColorMap,lockedTicketMap);
						break;
						
					case PRESALEZONETICKETS:
						//ticketGroups = DAORegistry.getPresaleZoneTicketsQueryManagerDAO().getPresaleZoneTicketsTicketsByTicketGroupId(ticketGroupId,rgbColorMap,lockedTicketMap);
						break;
						
					case REWARDTHEFAN:
						
						ticketGroup = DAORegistry.getCategoryTicketGroupDAO().getBroadCastedCategoryTicketsbyId(ticketGroupId);
						
						if(null == ticketGroup){
							ticketGroupLockStatus.setLockId(null);
							ticketGroupLockStatus.setStatus(1);
							ticketGroupLockStatus.setButtonValue("Find more tickets for this event");
							ticketGroupLockStatus.setOverRideButton(true);
							ticketGroupLockStatus.setLockStatus("Failure");
							ticketGroupLockStatus.setMessage("It looks like someone else already purchased this ticket. <br />" +
									"Please try to visit the event again and select the same tickets if they were released again or " +
									"try purchasing different set of tickets.");
							return ticketGroupLockStatus;
						}
						break;
		
					default:
						break;
				}
			}
			
			String zone = ticketGroup.getActualSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
			ZoneRGBColor zoneRGBColor = rgbColorMap.get(zone);
			if(null != zoneRGBColor){
				ticketGroup.setColorCode(zoneRGBColor.getColor());
				ticketGroup.setRgbColor(zoneRGBColor.getRgbColor());
			}else{
				ticketGroup.setColorCode("#F4ED6D");
				ticketGroup.setRgbColor("R:244 G:237 B:109");
			}
			
			Boolean isMobileDiscount = false;
			if(applicationPlatForm.equals(ApplicationPlatForm.IOS) || applicationPlatForm.equals(ApplicationPlatForm.ANDROID)){
				//isMobileDiscount = true;
			}
			
			Boolean isTicketDownloadOffer = Boolean.valueOf(ticketDownloadOfferStr.trim());
			
			//Re-Compute Ticket Price by Platform
			ticketGroup.computeTicketPrice(applicationPlatForm);
			
			
			//Get Ticket Shipping Method and Delivery Info
			TicketUtil.getTicketDeliveryInFo(ticketGroup,fontSizeStr);
			Double serverOrderTotal = null,serviceFees = 0.00;
			Boolean isFanPrice = Boolean.valueOf(isFanPriceStr);
			
			Double originalTixPrice = ticketGroup.getOriginalPrice();
			Double ticketPrice = ticketGroup.getOriginalPrice();
			Double normalPrice = ticketPrice;
			
			if(isFanPrice != null && isFanPrice){
				normalPrice = ticketGroup.getLoyalFanPriceDouble();
			}
			
			RTFPromotionalOfferTracking rtfPromoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPendingPromoTracking(applicationPlatForm, customer.getId(), 
					sessionIdOrDeviceIdStr, ticketGroupId, eventId);
			
			System.out.println("Original Price : "+ticketPrice);
			System.out.println("Loyal Fan Price : "+ticketGroup.getLoyalFanPriceDouble());
			
			
			Double mobileAppDiscConv = 0.00;
			
			if(isMobileDiscount){
				mobileAppDiscConv = 0.05;
			}
			Double discAmt = 0.00;
			
			String promoCode = null;
			
			boolean isCheckoutOfferEligible = false;
			boolean isCheckoutFlatOff = false;
			boolean checkoutOtherEvent = true;
			double discount = 0.00;
			double minOrderTotal = 0.00;
			
			if(!customer.getTicketPurchased() && rtfPromoCodeTracking == null){
				
				List<PreOrderPageEmailTracking> emailTrackingList = DAORegistry.getPreOrderPageEmailTrackingDAO().getAllCustomerTrackingRecords(customerId);
				
				if(null != emailTrackingList && !emailTrackingList.isEmpty()){
					
					CustomerCheckoutVisitOffer checkoutVisitOffer = DAORegistry.getCustomerCheckoutVisitOfferDAO().getActivetOffer();
					
					isCheckoutOfferEligible = true;
					isCheckoutFlatOff = checkoutVisitOffer.getIsFlatOff();
					discount = checkoutVisitOffer.getOtherEventDiscValue();
					minOrderTotal = checkoutVisitOffer.getOtherEventMinOrderTotal();
					
					Date curDate = new Date();
					Calendar calendar = new GregorianCalendar();
					boolean eventVisited = false;
					for (PreOrderPageEmailTracking obj : emailTrackingList) {
						calendar.setTime(obj.getEmailSentDate());
						calendar.add(Calendar.DAY_OF_MONTH, obj.getOfferExpiryDays());
						if(curDate.after(calendar.getTime())){
							break;
						}
						if(obj.getEventIds().contains(eventIdStr)){
							 eventVisited = true;
							 break;
						}
					}
					if(eventVisited){
						discount = checkoutVisitOffer.getVisitedEventDiscValue();
						checkoutOtherEvent = false;
					}
				}
				
				if(checkoutOtherEvent){
					Double orderTotal = ticketPrice * ticketGroup.getQuantity();
					if(orderTotal < minOrderTotal){
						isCheckoutOfferEligible = false;
					}
				}
			}
			
			if(null != rtfPromoCodeTracking){
				
				if(lockedBySameDevice){
					 
					if(rtfPromoCodeTracking.getIsFlatDiscount()){
						discAmt = TicketUtil.getSingleTicketFlatDiscount(ticketGroup.getQuantity(), rtfPromoCodeTracking.getAdditionalDiscountConv());
						ticketGroup.setIsFlatDiscount(true);
						ticketGroup.setFlatDiscount(discAmt);
					}else{
						Double additionalDiscConv = mobileAppDiscConv + rtfPromoCodeTracking.getAdditionalDiscountConv();
						discAmt = TicketUtil.getDiscountedPrice(ticketPrice, additionalDiscConv);
						ticketGroup.setIsFlatDiscount(false);
						ticketGroup.setFlatDiscount(discAmt);
					}
					System.out.println("Flat Off Discount : "+discAmt);
					if(isFanPrice != null && isFanPrice){
						if(!rtfPromoCodeTracking.getIsFlatDiscount()){
							
							/*if(isMobileDiscount){
								Double mobileAppDisc = TicketUtil.getDiscountedPrice(ticketPrice, additionalDiscConv);
							}*/
							
							ticketPrice = ticketPrice - discAmt;
							ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
						}
						System.out.println("Discounted Loyal Fan Price For Flat Off: "+ticketGroup.getLoyalFanPriceDouble());
					}else{
						ticketPrice = ticketPrice - discAmt;
						ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
						System.out.println("Discounted Original Price For Flat Off: "+ticketPrice);
					}
					ticketGroup.setNormalTixPrice(normalPrice);
					ticketGroup.setDiscountedTixPrice(rtfPromoCodeTracking.getDiscountedPrice());
					ticketGroup.setShowDiscPriceArea(true);
					
					if( isMobileDiscount && rtfPromoCodeTracking.getPromoCode().equals("MOBILEDISCOUNT")){
						ticketGroup.setIsPromoCodeApplied(true);
						ticketGroup.setPromotionalCode("MobileDiscount");
						ticketGroup.setPromotionalCodeMessage("We have applied mobile discount");
					}else{
						ticketGroup.setIsPromoCodeApplied(true);
						ticketGroup.setPromotionalCode(rtfPromoCodeTracking.getPromoCode());
						ticketGroup.setPromotionalCodeMessage("You have applied "+rtfPromoCodeTracking.getPromoCode()+" Promotional Code");
					}
					
					promoCode = rtfPromoCodeTracking.getPromoCode();
				}
			}else if(isMobileDiscount){
				
				discAmt = TicketUtil.getDiscountedPrice(ticketPrice, mobileAppDiscConv);
				ticketGroup.setIsFlatDiscount(false);
				ticketGroup.setFlatDiscount(discAmt);
				
				if(isFanPrice != null && isFanPrice){
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("Discounted Loyal Fan Price For Mobile App: "+ticketGroup.getLoyalFanPriceDouble());
				}else{
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("Discounted Original Price For Mobile App: "+ticketPrice);
				}
				ticketGroup.setNormalTixPrice(normalPrice);
				ticketGroup.setDiscountedTixPrice(isFanPrice != null && isFanPrice?ticketGroup.getLoyalFanPriceDouble():ticketGroup.getOriginalPrice());
				ticketGroup.setShowDiscPriceArea(true);
				
				ticketGroup.setIsPromoCodeApplied(true);
				ticketGroup.setPromotionalCode("MobileDiscount");
				ticketGroup.setPromotionalCodeMessage("We have applied mobile discount");
				
			}else if(isCheckoutOfferEligible){
			
				if(isCheckoutFlatOff){
					discAmt = TicketUtil.getSingleTicketFlatDiscount(ticketGroup.getQuantity(), discount);
					ticketGroup.setIsFlatDiscount(true);
					ticketGroup.setFlatDiscount(discAmt);
				}else{
					Double additionalDiscConv = discount / 100;;
					discAmt = TicketUtil.getDiscountedPrice(ticketPrice, additionalDiscConv);
					ticketGroup.setIsFlatDiscount(false);
					ticketGroup.setFlatDiscount(discAmt);
				}
				System.out.println("Flat Off Discount : "+discAmt);
				if(isFanPrice != null && isFanPrice){
					if(!isCheckoutFlatOff){
						ticketPrice = ticketPrice - discAmt;
						ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					}
					System.out.println("Discounted Loyal Fan Price For Flat Off: "+ticketGroup.getLoyalFanPriceDouble());
				}else{
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("Discounted Original Price For Flat Off: "+ticketPrice);
				}
				ticketGroup.setNormalTixPrice(normalPrice);
				ticketGroup.setDiscountedTixPrice(ticketPrice);
				ticketGroup.setShowDiscPriceArea(true);
				ticketGroup.setIsPromoCodeApplied(true);
				ticketGroup.setPromotionalCode("");
				ticketGroup.setPromotionalCodeMessage("We have applied discount for this order");
				promoCode = "";
				
				CheckOutOfferAppliedAudit audit = DAORegistry.getCheckOutOfferAppliedAuditDAO().getCheckoutOffer(applicationPlatForm, customer.getId(), 
						sessionIdOrDeviceIdStr, ticketGroupId, isFanPrice);
				
				if(null == audit){
					audit= new CheckOutOfferAppliedAudit();
					audit.setCategoryTicketGroupId(ticketGroupId);
					audit.setCreatedDate(new Date());
					audit.setCustomerId(customerId);
					audit.setDiscount(discount);
					audit.setEventId(eventId);
					audit.setIsFanPrice(isFanPrice);
					audit.setIsFlatOff(isCheckoutFlatOff);
					audit.setIsVisitedEvent(checkoutOtherEvent?false:true);
					audit.setOtherEventOrderThreshold(minOrderTotal);
					audit.setPlatForm(applicationPlatForm);
					audit.setSessionId(sessionIdOrDeviceIdStr);
					audit.setStatus("INITIATED");
					audit.setTicketNormalPrice(normalPrice);
					audit.setTicketSellingPrice(ticketPrice);
					audit.setUpdatedDate(new Date());
					DAORegistry.getCheckOutOfferAppliedAuditDAO().saveOrUpdate(audit);
				}
			} 
			
			
			if(isTicketDownloadOffer){
				
				discAmt = TicketUtil.getDiscountedPrice(ticketPrice, PaginationUtil.onDayBeforeEventTicketDownloadRewardOffer);
				ticketGroup.setIsFlatDiscount(false);
				ticketGroup.setFlatDiscount(discAmt);
				
				if(isFanPrice != null && isFanPrice){
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("On Day Before Event Ticket Download Offer Original Price: "+ticketGroup.getLoyalFanPriceDouble());
				}else{
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("On Day Before Event Ticket Download Offer Original Price: "+ticketPrice);
				}
				ticketGroup.setNormalTixPrice(normalPrice);
				ticketGroup.setDiscountedTixPrice(isFanPrice != null && isFanPrice?ticketGroup.getLoyalFanPriceDouble():ticketGroup.getOriginalPrice());
				ticketGroup.setShowDiscPriceArea(true);
				
				/*ticketGroup.setIsPromoCodeApplied(false);
				ticketGroup.setPromotionalCode("MobileDiscount");
				ticketGroup.setPromotionalCodeMessage("We have applied mobile discount");*/
				
			}
			
			if(isFanPrice != null && isFanPrice){
				ticketPrice = ticketGroup.getLoyalFanPriceDouble();
			}
			
			if(ticketGroup.getNormalTixPrice() < 1){
				ticketGroup.setNormalTixPrice(ticketPrice);
			}
			System.out.println("Final Ticket Price For The Ticket : "+ticketPrice);
			
			serviceFees = TicketUtil.getBrokerServiceFees(ticketGroup.getBrokerId(), ticketPrice, ticketGroup.getQuantity(), ticketGroup.getOriginalTaxPerc());
			serverOrderTotal =  ((ticketPrice * ticketGroup.getQuantity()) + serviceFees);
			
			if(isMobileDiscount){
				if(rtfPromoCodeTracking != null){
					//DAORegistry.getRtfPromotionalOfferTrackingDAO().updateOfferTrackingDiscount(rtfPromoCodeTracking.getId(), mobileAppDiscConv, discAmt,ticketPrice);
				}else{
					rtfPromoCodeTracking = new RTFPromotionalOfferTracking();
					rtfPromoCodeTracking.setCustomerId(customer.getId());
					rtfPromoCodeTracking.setPromotionalOfferId(-1);
					rtfPromoCodeTracking.setTicketGroupId(ticketGroupId);
					rtfPromoCodeTracking.setIsLongTicket(false);
					rtfPromoCodeTracking.setEventId(eventId);
					rtfPromoCodeTracking.setPlatForm(applicationPlatForm);
					rtfPromoCodeTracking.setSessionId(sessionIdOrDeviceIdStr);
					rtfPromoCodeTracking.setStatus("PENDING");
					rtfPromoCodeTracking.setOfferType(PromotionalType.MOBILE_APP_DISCOUNT);
					rtfPromoCodeTracking.setOrderId(null);
					rtfPromoCodeTracking.setPromoCode("MOBILEDISCOUNT");
					rtfPromoCodeTracking.setIsFlatDiscount(false);
					rtfPromoCodeTracking.setAdditionalDiscountConv(0.00);
					rtfPromoCodeTracking.setMobileAppDiscConv(mobileAppDiscConv);
					rtfPromoCodeTracking.setAdditionalDiscountAmt(discAmt);
					rtfPromoCodeTracking.setServiceFees(serviceFees);
					rtfPromoCodeTracking.setTotalPrice(serverOrderTotal - serviceFees);
					rtfPromoCodeTracking.setTicketPrice(normalPrice);
					rtfPromoCodeTracking.setDiscountedPrice(ticketPrice);
					rtfPromoCodeTracking.setCreatedDate(new Date());
					rtfPromoCodeTracking.setUpdatedDate(new Date());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(rtfPromoCodeTracking);
				}
			}
			
			
			/*if(isFanPrice != null && isFanPrice){
				serviceFees = TicketUtil.getBrokerServiceFees(ticketGroup.getBrokerId(), ticketGroup.getLoyalFanPriceDouble(), ticketGroup.getQuantity(), ticketGroup.getOriginalTax());
				serverOrderTotal =  ((ticketGroup.getLoyalFanPriceDouble() * ticketGroup.getQuantity()) + serviceFees);
				ticketPrice = ticketGroup.getLoyalFanPriceDouble();
			}else{
				serviceFees = TicketUtil.getBrokerServiceFees(ticketGroup.getBrokerId(), ticketGroup.getOriginalPrice(), ticketGroup.getQuantity(), ticketGroup.getOriginalTax());
				serverOrderTotal =  ((ticketGroup.getOriginalPrice() * ticketGroup.getQuantity()) + serviceFees);
				ticketPrice = ticketGroup.getOriginalPrice();
			}*/
			
			ticketGroup.setTaxesAsDouble(serviceFees);
			
			Double singleTixFees = TicketUtil.getRoundedValue(serviceFees / ticketGroup.getQuantity());
			ticketGroup.setTotalServiceFees(TicketUtil.getRoundedValueString(serviceFees));
			ticketGroup.setSingleTixServiceFees(TicketUtil.getRoundedValueString(singleTixFees));
			
			LoyaltySettings loyaltySettings =DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getRewardConversion());
			
			ticketGroup.setOrderTotalAsDouble(serverOrderTotal);
			ticketGroup.setOrderTotal(TicketUtil.getRoundedValueString(serverOrderTotal));
			
			/*AffiliatePromoCodeTracking promoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
					sessionIdOrDeviceIdStr, ticketGroupId, eventId);
			if(null != promoCodeTracking){
				earningRewardPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getAffiliatePromoRewardConv());
				ticketGroup.setIsPromoCodeApplied(true);
				ticketGroup.setPromotionalCode(promoCodeTracking.getPromoCode());
				ticketGroup.setPromotionalCodeMessage("You have applied "+promoCodeTracking.getPromoCode()+" Promotional Code");
				
			}*/
			
			ticketGroup.setRewardPoints("<font color=#012e4f>You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b></font>");
			ticketGroupLockStatus.setCategoryTicketGroup(ticketGroup);
			ticketGroupLockStatus.setEvent(event);
			
			List<CustomerCardInfo> cardList = DAORegistry.getCustomerCardInfoDAO().getAllActiveCardsByCustomerId(customerId);
			if(null != cardList && !cardList.isEmpty()){
				ticketGroupLockStatus.setShowSavedCards(true);
			}
			String  zones = ticketGroup.getActualSection();
			
			//To Get Web View for Android and iOS App
			MapUtil.getSvgWebViewUrl(event, zones,applicationPlatForm);
			
			Double ticketDownloadOfferPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, PaginationUtil.onDayBeforeEventTicketDownloadRewardOffer);
			
			ticketGroupLockStatus.setTicketDownloadOption1(TicketUtil.getTicketDownloadOfferOption1(fontSizeStr));
			ticketGroupLockStatus.setTicketDownloadOption2(TicketUtil.getTicketDownloadOfferOption2(event, fontSizeStr,TicketUtil.getRoundedValueString(ticketDownloadOfferPoints)));
			ticketGroupLockStatus.setOnDayBeforeEventTixDownload(isTicketDownloadOffer);
			
			if(lockedByDifferentDevice){
				ticketGroupLockStatus.setLockId(null);
				ticketGroupLockStatus.setStatus(1);
				ticketGroupLockStatus.setButtonValue("Find more tickets for this event");
				ticketGroupLockStatus.setOverRideButton(true);
				ticketGroupLockStatus.setLockStatus("Failure");
				ticketGroupLockStatus.setMessage("It looks like someone else already purchased this ticket. <br />" +
						"Please try to visit the event again and select the same tickets if they were released again or " +
						"try purchasing different set of tickets.");
				//ticketGroupLockStatus.setMessage("Ticket was sold. Please Choose different ticket.");
				return ticketGroupLockStatus;
			}else if(lockedBySameDevice){
				ticketGroupLockStatus.setLockId(lockedTicket.getId());
				ticketGroupLockStatus.setStatus(1);
				ticketGroupLockStatus.setLockStatus("Success");
				ticketGroupLockStatus.setMessage("Ticket Group already locked by this Device Id or Session Id");
				ticketGroupLockStatus.setCatgeoryTicketGroupId(ticketGroupId);
				String timeLeft = TicketUtil.getTimeLeft(lockedTicket.getCreationDate(), new Date());
				ticketGroupLockStatus.setTimeLeft(timeLeft);
				return ticketGroupLockStatus;
			}
			
			ticketGroupLockStatus.setDeviceId(sessionIdOrDeviceIdStr);
			ticketGroupLockStatus.setLockStatus("Success");
			ticketGroupLockStatus.setMessage("Ticket Group successfully locked by this Device Id or Session Id");
			ticketGroupLockStatus.setTimeLeft("10:00");
			ticketGroupLockStatus.setCatgeoryTicketGroupId(ticketGroupId);
			
			if(!customer.getTicketPurchased()){
				try{
					PreOrderPageAudit pageAudit = DAORegistry.getPreOrderPageAuditDAO().getAudit(applicationPlatForm, customerId, sessionIdOrDeviceIdStr, ticketGroupId, eventId);
					if(null == pageAudit){
						pageAudit = new PreOrderPageAudit();
						pageAudit.setCustomer(customer);
						pageAudit.setEventId(eventId);
						pageAudit.setCategoryTicketGroupId(ticketGroup.getId());
						pageAudit.setIsLoyalFan(isFanPrice);
						pageAudit.setQty(ticketGroup.getQuantity());
						pageAudit.setSection(ticketGroup.getActualSection());
						pageAudit.setTicketPrice(originalTixPrice);
						pageAudit.setTicketSoldPrice(ticketPrice);
						pageAudit.setSessionId(sessionIdOrDeviceIdStr);
						pageAudit.setIpAddress(ipAddress);
						pageAudit.setPlatForm(applicationPlatForm);
						pageAudit.setPromotionalCode(promoCode);
						pageAudit.setStatus("LOCKED");
						pageAudit.setReason("Order Initiated");
						pageAudit.setIsEmailSent(false);
						pageAudit.setCreateDate(new Date());
						pageAudit.setUpdatedDate(new Date());
						DAORegistry.getPreOrderPageAuditDAO().saveOrUpdate(pageAudit);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			lockedTicket =new AutoCatsLockedTickets();
			lockedTicket.setEventId(eventId);
			lockedTicket.setSessionId(sessionIdOrDeviceIdStr);
			lockedTicket.setCreationDate(new Date());
			
			lockedTicket.setIsFanPrice(isFanPrice);
			lockedTicket.setTicketQty(ticketGroup.getQuantity());
			lockedTicket.setTicketPrice(ticketPrice);
			lockedTicket.setTotalPrice(serverOrderTotal - serviceFees);
			lockedTicket.setServiceFees(serviceFees);
			
			lockedTicket.setCatgeoryTicketGroupId(ticketGroupId);
			lockedTicket.setLockStatus(LockedTicketStatus.ACTIVE);
			lockedTicket.setProductType(productType);
			lockedTicket.setPlatform(platForm);
			lockedTicket.setBrokerId(ticketGroup.getBrokerId());
			DAORegistry.getAutoCatsLockedTicketDAO().saveOrUpdate(lockedTicket);
			
			
			//isTicketDownloadOffer
			ticketGroupLockStatus.setOnDayBeforeEventTixDownload(isTicketDownloadOffer);
			ticketGroupLockStatus.setLockId(lockedTicket.getId());
			ticketGroupLockStatus.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Success");
			return ticketGroupLockStatus;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while locking the ticket group.");
			ticketGroupLockStatus.setError(error);
			ticketGroupLockStatus.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Error occured while locking the ticket group");
			return ticketGroupLockStatus;
		}
	}

	
	@RequestMapping(value="/GetEventDetails",method=RequestMethod.POST)
	public @ResponsePayload TicketList getTicketList(HttpServletRequest request, Model model,HttpServletResponse response){
		TicketList ticketList =new TicketList();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.TICKET,"You are not authorized");
				return ticketList;
			}
//			Integer configId= null;
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.TICKET,"You are not authorized");
						return ticketList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.TICKET,"You are not authorized");
					return ticketList;
				}
			}else{
				error.setDescription("You are not authorized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.TICKET,"You are not authorized");
				return ticketList;
			}
			String productTypeStr = request.getParameter("productType");
			String eventIdStr = request.getParameter("eventId");
			String sessionId = request.getParameter("sessionIdOrDeviceId");
			String customerIdStr = request.getParameter("customerId");
			String platForm = request.getParameter("platForm");
			String aRefValueStr = request.getParameter("aRefValue");
			String aRefTypeStr = request.getParameter("aRefType");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.TICKET,"Product Type is mandatory");
				return ticketList;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.TICKET,"Please send valid product type");
					return ticketList;
				}
			}
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			
			Integer eventId= null;
			Event event = null;
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				try {
					eventId=Integer.parseInt(eventIdStr);
					
					event = DAORegistry.getEventDAO().getEventByEventId(eventId, productType);
						
					if(event==null || ( null == event.getVenueCategoryName() || null == event.getVenueId()) ){
						error.setDescription("Event is not recognized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Event is not recognized");
						return ticketList;
					}
					
					/*//Ulaganathan : To add Venue Map
					MapUtil.setVenueZoneMap(event);*/
					MapUtil.getSVGTextFromUtil(event, applicationPlatForm);
					
					
					if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
						error.setDescription("Event is not recognized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.AUTOCATSTICKETGROUPLOCKING,"Event is not recognized");
						return ticketList;
					}
					
				} catch (Exception e) {
					error.setDescription("Event is not recognized.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"Event is not recognized");
					return ticketList;
				}
			}else{
				error.setDescription("Event is not recognized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"Event is not recognized");
				return ticketList;
			}
			String aRefValue = null;
			ArtistReferenceType aRefType = null;
			if((aRefValueStr !=null && !aRefValueStr.isEmpty()) ||
					(aRefTypeStr != null && !aRefTypeStr.isEmpty())){
				try {
					aRefType = ArtistReferenceType.valueOf(aRefTypeStr);
					
					if(aRefType == null) {
						error.setDescription("Artist Ref Type is not valid");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Artist Ref Type is not valid");
						return ticketList;
					}
					try{
						aRefValue = aRefValueStr;
						if(aRefType.equals(ArtistReferenceType.PARENT_ID) || aRefType.equals(ArtistReferenceType.CHILD_ID) ||
								aRefType.equals(ArtistReferenceType.GRANDCHILD_ID) || aRefType.equals(ArtistReferenceType.RTFCATS_ID)) {
							Integer.parseInt(aRefValue);
						}
					}catch(Exception e){
						error.setDescription("Artist Ref Value is not valid for given ref Type");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Artist Ref Value is not valid for given ref Type");
						return ticketList;
					}
				} catch(Exception e){
					error.setDescription("Artist Ref Type or Ref Value is not valid");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EVENTSEARCH,"Artist Ref Type or Ref Value is not valid");
					return ticketList;
				}
			}
			
			LoyaltySettings loyaltySettings =DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			
			List<AutoCatsLockedTickets> sessionLockedTickets  = new ArrayList<AutoCatsLockedTickets>();
			Map<Integer, Boolean> sessionLockedTicketMap = new HashMap<Integer, Boolean>();
			if(null != sessionId && !sessionId.isEmpty()){
				sessionLockedTickets  = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketBySessionId(sessionId,productType);
				 
				 for (AutoCatsLockedTickets lockedTicket : sessionLockedTickets) {
					 sessionLockedTicketMap.put(lockedTicket.getId(), Boolean.TRUE);
				}
			}
			
			List<AutoCatsLockedTickets> lockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketByEventId(eventId,productType);
			Map<Integer, Boolean> lockedTicketMap = new HashMap<Integer, Boolean>();
			
			for (AutoCatsLockedTickets lockedTicket : lockedTickets) {
				
				if(null != sessionLockedTicketMap.get(lockedTicket.getId()) && sessionLockedTicketMap.get(lockedTicket.getId())){
					continue;
				}
				lockedTicketMap.put(lockedTicket.getCatgeoryTicketGroupId(), Boolean.TRUE);
			}
			
			/*List<ZoneTicketsAdditionalMarkup> ztAdditionalMarkups = DAORegistry.getZoneTicketsAdditionalMarkupDAO().getZoneTicketsAdditionalMarkupsByEventId(event_id);
			Map<String, ZoneTicketsAdditionalMarkup> ztAdditionalMarkupMap = new HashMap<String, ZoneTicketsAdditionalMarkup>();
			for (ZoneTicketsAdditionalMarkup ztAdditionalMarkup : ztAdditionalMarkups) {
				if(null != ztAdditionalMarkup.getAdditionalMarkup() && !ztAdditionalMarkup.getAdditionalMarkup().equals(0.0)){
					ztAdditionalMarkupMap.put(ztAdditionalMarkup.getZone().replaceAll("\\s+"," "),ztAdditionalMarkup);
				}
			}*/
			
			Collection<ZoneRGBColor> zoneRgbColors =DAORegistry.getZoneRGBColorDAO().getAll();
			Map<String, ZoneRGBColor> rgbColorMap = new HashMap<String, ZoneRGBColor>();
			
			for (ZoneRGBColor zoneRGBColor : zoneRgbColors) {
				rgbColorMap.put(zoneRGBColor.getZone().toUpperCase(), zoneRGBColor);
			}
			
			Integer customerId = null;
			Customer customer=null;
			
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				
				try {
					customerId=Integer.parseInt(customerIdStr);
					customer=CustomerUtil.getCustomerById(customerId);
					if(customer==null){
						error.setDescription("Customer is not recognized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"Customer is not recognized");
						return ticketList;
					}
				} catch (Exception e) {
					error.setDescription("Customer is not recognized.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"Customer is not recognized");
					return ticketList;
				}
				
				/*CustomerSuperFan customerSuperFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
				if(null != customerSuperFan  &&  event !=  null){
					event.setIsSuperFanEvent(FavoriteUtil.markArtistAsLoyalFan(customerSuperFan, event));
				}
				
				FavouriteEvents favouriteEvent = DAORegistry.getFavouriteEventsDAO().getActiveFavouriteEventsById(eventId, customerId);
				if(null != favouriteEvent ){
					event.setIsFavoriteEvent(true);
				}*/
				event.setCustomerId(customerId);
				
				
				/* Share Link Validation for Share Icon - Added By Ulaganathan*/
				String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
				/*if(null != customer){
					ticketList.setEligibleToShareRefCode(customer.getEligibleToShareRefCode());
					ticketList.setShareErrorMessage(TextUtil.getCustomerShareLinkValidationMessage(fontSizeStr));
				}*/
				/* Share Link Validation for Share Icon - Added By Ulaganathan*/
			}
			
			//event.setEventImageUrl(applicationPlatForm);
			//event = URLUtil.getShareLinkURL(event, customer,applicationPlatForm);
			
			
			Collection<CategoryTicketGroup> allBrodcastedTickets = DAORegistry.getCategoryTicketGroupDAO().getAllBroadcastedCategoryTicketsbyEventId(eventId);
			
			if(null == allBrodcastedTickets || allBrodcastedTickets.isEmpty() || allBrodcastedTickets.size() <= 0){
				
				error.setDescription("No Tickets found for selected event.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				ticketList.setEvent(event);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"No Tickets found for selected event.");
				return ticketList;
			}
			
			Collection<CategoryTicketGroup> ticketGroups = TicketUtil.filterCheapestTicketByZoneByQty(allBrodcastedTickets);
			
			Map<Integer, Map<String, List<CategoryTicketGroup>>> qtyMap = new HashMap<Integer, Map<String,List<CategoryTicketGroup>>>();
			
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			
			String  zones = "";
			Set<String> uniqueSection = new HashSet<String>();
			
			for (CategoryTicketGroup ticketGroup : ticketGroups) {
				
				Boolean isLockedTicket = lockedTicketMap.get(ticketGroup.getId());				
				if(null != isLockedTicket && isLockedTicket){
					continue;
				}
				
				
				//Re-Compute Ticket Price by Platform
				ticketGroup.computeTicketPrice(applicationPlatForm);
				
				if(uniqueSection.add(ticketGroup.getActualSection())){
					zones += ticketGroup.getActualSection()+",";
				}
				
				String zone = ticketGroup.getActualSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
				ZoneRGBColor zoneRGBColor = rgbColorMap.get(zone);
				if(null != zoneRGBColor){
					ticketGroup.setColorCode(zoneRGBColor.getColor());
					ticketGroup.setRgbColor(zoneRGBColor.getRgbColor());
				}else{
					ticketGroup.setColorCode("#F4ED6D");
					ticketGroup.setRgbColor("R:244 G:237 B:109");
				}
				//TextUtil.ge
				
				Map<String, List<CategoryTicketGroup>> sectionMap = qtyMap.get(ticketGroup.getQuantity());
				
				if( null !=sectionMap && !sectionMap.isEmpty() ){
					
					List<CategoryTicketGroup> sectionTicketGroups = sectionMap.get(ticketGroup.getActualSection());
					
					if( null != sectionTicketGroups && !sectionTicketGroups.isEmpty()){
						sectionTicketGroups.add(ticketGroup);
					}else{
						sectionTicketGroups = new ArrayList<CategoryTicketGroup>();
						sectionTicketGroups.add(ticketGroup);
					}
					
					sectionMap.put(ticketGroup.getActualSection(), sectionTicketGroups);
				}else{
					
					Map<String, List<CategoryTicketGroup>> map = new HashMap<String, List<CategoryTicketGroup>>();
					List<CategoryTicketGroup> sectionTicketGroups = new ArrayList<CategoryTicketGroup>();
					sectionTicketGroups.add(ticketGroup);
					map.put(ticketGroup.getActualSection(), sectionTicketGroups);
					qtyMap.put(ticketGroup.getQuantity(), map);
				}
			}
			
			TicketGroupQty ticketGroupQty = null;
			List<TicketGroupQty> qtyList = new ArrayList<TicketGroupQty>();
			List<CategoryTicketGroup> allQtyTicketGroupList = new ArrayList<CategoryTicketGroup>();
			for (Integer qty : qtyMap.keySet()) {
				
				Map<String, List<CategoryTicketGroup>> sectionMap = qtyMap.get(qty);
				List<CategoryTicketGroup> qtyTicketGroupList = new ArrayList<CategoryTicketGroup>();
				ticketGroupQty = new TicketGroupQty();
				
				for (String section : sectionMap.keySet()) {
					
					List<CategoryTicketGroup> priorityTicketGroups = sectionMap.get(section);
					
					//Collections.sort(priorityTicketGroups, ServicesController.ticketGroupSortingComparatorByPriority);
					
					for (CategoryTicketGroup ticketGroup : priorityTicketGroups) {
						qtyTicketGroupList.add(ticketGroup);
						break;
					}
				}
				//Collections.sort(qtyTicketGroupList, TicketUtil.ticketGroupSortingComparatorBySection);
				Collections.sort(qtyTicketGroupList, TicketUtil.ticketGroupSortingComparatorByPrice);
				ticketGroupQty.setQty(qty);
				ticketGroupQty.setCategoryTicketGroups(qtyTicketGroupList);
				allQtyTicketGroupList.addAll(qtyTicketGroupList);
				qtyList.add(ticketGroupQty);
			}
			
			
			Collections.sort(allQtyTicketGroupList, TicketUtil.ticketGroupSortingComparatorByPrice);
			/*for (CategoryTicketGroup ticketGroup : allQtyTicketGroupList) {
				System.out.println(ticketGroup.getSection()+"----"+ticketGroup.getQuantity());
			}*/
			ticketGroupQty = new TicketGroupQty(); 
			ticketGroupQty.setQty(0);
			ticketGroupQty.setCategoryTicketGroups(allQtyTicketGroupList);
			qtyList.add(ticketGroupQty);
			
			//To Get Web View for Android and iOS App
			MapUtil.getSvgWebViewUrl(event, zones, applicationPlatForm);
			
			ticketList.setTicketGroupQtyList(qtyList);
			ticketList.setEvent(event);
			
			if(aRefType != null && aRefValue != null) {
				if(event.getGrandChildCategoryId() != null) {
					ticketList.setaRefType(ArtistReferenceType.GRANDCHILD_ID.toString());
					ticketList.setaRefValue(event.getGrandChildCategoryId().toString());
				}
			}
			if(ticketList.getaRefType() == null) {
				ticketList.setaRefType("");
			}
			if(ticketList.getaRefValue() == null) {
				ticketList.setaRefValue("");
			}
			
			if(ticketGroups==null || ticketGroups.isEmpty()){
				error.setDescription("No Tickets found for selected event.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"No Tickets found for selected event.");
				return ticketList;
			}
			ticketList.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.TICKET,"Success");
			return ticketList;
		} catch (Exception e) {
			error.setDescription("No Tickets found for selected event.");
			ticketList.setError(error);
			ticketList.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"Exception Occurred");
			return ticketList;
		}
	}
	
	
	@RequestMapping(value="/GetTicketGroup",method=RequestMethod.POST)
	public @ResponsePayload GetTicketGroup getTicketGroup(HttpServletRequest request, Model model,HttpServletResponse response){
		GetTicketGroup ticketList =new GetTicketGroup();
		Error error = new Error();
		try { 
			
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETGROUP,"You are not authorized");
				return ticketList;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETGROUP,"You are not authorized");
						return ticketList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETGROUP,"You are not authorized");
					return ticketList;
				}
			}else{
				error.setDescription("You are not authorized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETGROUP,"You are not authorized");
				return ticketList;
			}

			try{
				//Add WebConfig SourceKey in the response for Credit Card Encryption
				ticketList.setEncryptionKey(ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey());
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			String productTypeStr = request.getParameter("productType");
			String ticketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String sessionId = request.getParameter("sessionIdOrDeviceId");
			String platForm = request.getParameter("platForm");
			String customerIdStr = request.getParameter("customerId");
			String eventIdStr = request.getParameter("eventId");
			String isFanPriceStr = request.getParameter("isFanPrice");
			//Offer for downloading tickets on day before the event
			String ticketDownloadOfferStr =request.getParameter("onDayBeforeTheEvent");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETGROUP,"Product Type is mandatory");
				return ticketList;
			}
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETGROUP,"Please send valid product type");
					return ticketList;
				}
			}
			
			ApplicationPlatForm applicationPlatForm = ApplicationPlatForm.ANDROID;
			if(!TextUtil.isEmptyOrNull(platForm)){
				try{
					applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
				}catch(Exception e){
					applicationPlatForm = ApplicationPlatForm.ANDROID;
				}
			}
			
			Integer ticketGroupId = null;
			if(null != ticketGroupIdStr && !ticketGroupIdStr.isEmpty()){
				
				try{
					ticketGroupId = Integer.parseInt(ticketGroupIdStr);
				}catch(Exception e){
					error.setDescription("Category Ticket Group Id is not recognized .");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Category Ticket Group Id is not recognized");
					return ticketList;
				}
			}else{
				error.setDescription("Category Ticket Group Id is not recognized .");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Category Ticket Group Id is not recognized");
				return ticketList;
			}
			

			Integer eventId= null;
			Event event = null;
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				try {
					eventId=Integer.parseInt(eventIdStr);
					
					event = DAORegistry.getEventDAO().getEventByEventId(eventId, productType);
						
					if(event==null || ( null == event.getVenueCategoryName() || null == event.getVenueId()) ){
						error.setDescription("Event is not recognized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Event is not recognized");
						return ticketList;
					}
					
					/*//Ulaganathan : To add Venue Map
					MapUtil.setVenueZoneMap(event);*/
					MapUtil.getSVGTextFromUtil(event, applicationPlatForm);
					//MapUtil.getSvgWebViewUrl(event);
					if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
						error.setDescription("Event is not recognized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Event is not recognized");
						return ticketList;
					}
					
				} catch (Exception e) {
					error.setDescription("Event is not recognized.");
					ticketList.setError(error);
					ticketList.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Event is not recognized");
					return ticketList;
				}
			}else{
				error.setDescription("Event is not recognized.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Event is not recognized");
				return ticketList;
			}
			
			CategoryTicketGroup ticketGroup = DAORegistry.getCategoryTicketGroupDAO().getBroadCastedCategoryTicketsbyId(ticketGroupId);
			
			if(null == ticketGroup){
				error.setButtonValue("Find Other Tickets to this Event");
				error.setOverRideButton(true);
				error.setDescription("It looks like this was just purchased and is no longer available for sale.<br /> " +
				"If you've just made a purchase, this may be because you've managed to buy the last ticket.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Ticket was sold.");
				return ticketList;
			}
			
			String fontSizeStr = TicketUtil.getFontSize(request, applicationPlatForm);
			
			LoyaltySettings loyaltySettings =DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			
			List<AutoCatsLockedTickets> sessionLockedTickets  = new ArrayList<AutoCatsLockedTickets>();
			Map<Integer, Boolean> sessionLockedTicketMap = new HashMap<Integer, Boolean>();
			if(null != sessionId && !sessionId.isEmpty()){
				sessionLockedTickets  = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketBySessionId(sessionId,productType);
				 
				 for (AutoCatsLockedTickets lockedTicket : sessionLockedTickets) {
					 sessionLockedTicketMap.put(lockedTicket.getId(), Boolean.TRUE);
				}
			}
			
			List<AutoCatsLockedTickets> lockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketByEventId(eventId,productType);
			Map<Integer, Boolean> lockedTicketMap = new HashMap<Integer, Boolean>();
			
			for (AutoCatsLockedTickets lockedTicket : lockedTickets) {
				
				if(null != sessionLockedTicketMap.get(lockedTicket.getId()) && sessionLockedTicketMap.get(lockedTicket.getId())){
					continue;
				}
				lockedTicketMap.put(lockedTicket.getCatgeoryTicketGroupId(), Boolean.TRUE);
			}
			
			Boolean isLockedTicket = lockedTicketMap.get(ticketGroup.getId());				
			if(null != isLockedTicket && isLockedTicket){
				error.setButtonValue("Find Other Tickets to this Event");
				error.setOverRideButton(true);
				error.setDescription("It looks like this was just purchased and is no longer available for sale.<br /> " +
				"If you've just made a purchase, this may be because you've managed to buy the last ticket.");
				ticketList.setError(error);
				ticketList.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Ticket was sold.");
				return ticketList;
			}
			Collection<ZoneRGBColor> zoneRgbColors =DAORegistry.getZoneRGBColorDAO().getAll();
			Map<String, ZoneRGBColor> rgbColorMap = new HashMap<String, ZoneRGBColor>();
			
			for (ZoneRGBColor zoneRGBColor : zoneRgbColors) {
				rgbColorMap.put(zoneRGBColor.getZone().toUpperCase(), zoneRGBColor);
			}
			
			CustomerSuperFan superFan = null;
			Integer customerId = null;
			Customer customer=null;
			
				if(!TextUtil.isEmptyOrNull(customerIdStr)){
					
					try {
						customerId=Integer.parseInt(customerIdStr);
						customer=CustomerUtil.getCustomerById(customerId);
						if(customer==null){
							error.setDescription("Customer is not recognized.");
							ticketList.setError(error);
							ticketList.setStatus(0);
							TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"Customer is not recognized");
							return ticketList;
						}
		
					} catch (Exception e) {
						error.setDescription("Customer is not recognized.");
						ticketList.setError(error);
						ticketList.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.TICKET,"Customer is not recognized");
						return ticketList;
				}
				
				CustomerSuperFan customerSuperFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customerId);
				if(null != customerSuperFan  &&  event !=  null){
					event.setIsSuperFanEvent(FavoriteUtil.markArtistAsLoyalFan(customerSuperFan, event));
				}
				
				FavouriteEvents favouriteEvent = DAORegistry.getFavouriteEventsDAO().getActiveFavouriteEventsById(eventId, customerId);
				if(null != favouriteEvent ){
					event.setIsFavoriteEvent(true);
				}
				event.setCustomerId(customerId);
			}
			
			String zone = ticketGroup.getActualSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
			ZoneRGBColor zoneRGBColor = rgbColorMap.get(zone);
			if(null != zoneRGBColor){
				ticketGroup.setColorCode(zoneRGBColor.getColor());
				ticketGroup.setRgbColor(zoneRGBColor.getRgbColor());
			}else{
				ticketGroup.setColorCode("#F4ED6D");
				ticketGroup.setRgbColor("R:244 G:237 B:109");
			}
			
			
			List<CustomerCardInfo> cardList = DAORegistry.getCustomerCardInfoDAO().getAllActiveCardsByCustomerId(customerId);
			if(null != cardList && !cardList.isEmpty()){
				ticketList.setShowSavedCards(true);
			}
			
			Boolean isMobileDiscount = false;
			if(applicationPlatForm.equals(ApplicationPlatForm.IOS) || applicationPlatForm.equals(ApplicationPlatForm.ANDROID)){
				//isMobileDiscount = true;
			}
			
			if(TextUtil.isEmptyOrNull(ticketDownloadOfferStr)){
				ticketDownloadOfferStr = "false";
			}
			
			Boolean isTicketDownloadOffer = Boolean.valueOf(ticketDownloadOfferStr.trim());
			
			//Re-Compute Ticket Price by Platform
			ticketGroup.computeTicketPrice(applicationPlatForm);
			
			String  zones = ticketGroup.getActualSection();
			//To Get Web View for Android and iOS App
			MapUtil.getSvgWebViewUrl(event, zones, applicationPlatForm);
			
			//Get Ticket Shipping Method and Delivery Info
			TicketUtil.getTicketDeliveryInFo(ticketGroup,fontSizeStr);
			
			Double serverOrderTotal = null,serviceFees = 0.00;
			Boolean isFanPrice = Boolean.valueOf(isFanPriceStr);
			
			Double ticketPrice = ticketGroup.getOriginalPrice();
			Double normalPrice = ticketPrice;
			if(isFanPrice != null && isFanPrice){
				normalPrice = ticketGroup.getLoyalFanPriceDouble();
			}
			
			RTFPromotionalOfferTracking rtfPromoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPendingPromoTracking(applicationPlatForm, customer.getId(), 
					sessionId, ticketGroupId, eventId);
			
			Double mobileAppDiscConv = 0.00;
			if(isMobileDiscount){
				mobileAppDiscConv = 0.05;
			}
			Double discAmt = 0.00;
			
			boolean isCheckoutOfferEligible = false;
			boolean isCheckoutFlatOff = false;
			boolean checkoutOtherEvent = true;
			double discount = 0.00;
			double minOrderTotal = 0.00;
			
			if(!customer.getTicketPurchased() && rtfPromoCodeTracking == null){
				
				List<PreOrderPageEmailTracking> emailTrackingList = DAORegistry.getPreOrderPageEmailTrackingDAO().getAllCustomerTrackingRecords(customerId);
				
				if(null != emailTrackingList && !emailTrackingList.isEmpty()){
					
					CustomerCheckoutVisitOffer checkoutVisitOffer = DAORegistry.getCustomerCheckoutVisitOfferDAO().getActivetOffer();
					
					isCheckoutOfferEligible = true;
					isCheckoutFlatOff = checkoutVisitOffer.getIsFlatOff();
					discount = checkoutVisitOffer.getOtherEventDiscValue();
					minOrderTotal = checkoutVisitOffer.getOtherEventMinOrderTotal();
					
					Date curDate = new Date();
					Calendar calendar = new GregorianCalendar();
					boolean eventVisited = false;
					for (PreOrderPageEmailTracking obj : emailTrackingList) {
						calendar.setTime(obj.getEmailSentDate());
						calendar.add(Calendar.DAY_OF_MONTH, obj.getOfferExpiryDays());
						if(curDate.after(calendar.getTime())){
							break;
						}
						if(obj.getEventIds().contains(eventIdStr)){
							 eventVisited = true;
							 break;
						}
					}
					if(eventVisited){
						discount = checkoutVisitOffer.getVisitedEventDiscValue();
						checkoutOtherEvent = false;
					}
				}
				
				if(checkoutOtherEvent){
					Double orderTotal = ticketPrice * ticketGroup.getQuantity();
					if(orderTotal < minOrderTotal){
						isCheckoutOfferEligible = false;
					}
				}
			}
			
			
			if(null != rtfPromoCodeTracking){
				
				if(rtfPromoCodeTracking.getIsFlatDiscount()){
					discAmt = TicketUtil.getSingleTicketFlatDiscount(ticketGroup.getQuantity(), rtfPromoCodeTracking.getAdditionalDiscountConv());
					ticketGroup.setIsFlatDiscount(true);
					ticketGroup.setFlatDiscount(discAmt);
				}else{
					Double additionalDiscConv = mobileAppDiscConv + rtfPromoCodeTracking.getAdditionalDiscountConv();
					discAmt = TicketUtil.getDiscountedPrice(ticketPrice, additionalDiscConv);
					ticketGroup.setIsFlatDiscount(false);
					ticketGroup.setFlatDiscount(discAmt);
				}
				System.out.println("Flat Off Discount : "+discAmt);
				if(isFanPrice != null && isFanPrice){
					if(!rtfPromoCodeTracking.getIsFlatDiscount()){
						ticketPrice = ticketPrice - discAmt;
						ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					}
					
					System.out.println("Discounted Loyal Fan Price For Flat Off: "+ticketGroup.getLoyalFanPriceDouble());
				}else{
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("Discounted Original Price For Flat Off: "+ticketPrice);
				}
				ticketGroup.setNormalTixPrice(normalPrice);
				ticketGroup.setDiscountedTixPrice(rtfPromoCodeTracking.getDiscountedPrice());
				ticketGroup.setShowDiscPriceArea(true);
				
				if( isMobileDiscount && rtfPromoCodeTracking.getPromoCode().equals("MOBILEDISCOUNT")){
					ticketGroup.setIsPromoCodeApplied(true);
					ticketGroup.setPromotionalCode("MobileDiscount");
					ticketGroup.setPromotionalCodeMessage("We have applied mobile discount");
				}else{
					ticketGroup.setIsPromoCodeApplied(true);
					ticketGroup.setPromotionalCode(rtfPromoCodeTracking.getPromoCode());
					ticketGroup.setPromotionalCodeMessage("You have applied "+rtfPromoCodeTracking.getPromoCode()+" Promotional Code");
				}
			}else if(isMobileDiscount){
				
				discAmt = TicketUtil.getDiscountedPrice(ticketPrice, mobileAppDiscConv);
				ticketGroup.setIsFlatDiscount(false);
				ticketGroup.setFlatDiscount(discAmt);
				
				if(isFanPrice != null && isFanPrice){
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("Discounted Loyal Fan Price For Mobile App: "+ticketGroup.getLoyalFanPriceDouble());
				}else{
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("Discounted Original Price For Mobile App: "+ticketPrice);
				}
				ticketGroup.setNormalTixPrice(normalPrice);
				ticketGroup.setDiscountedTixPrice(isFanPrice != null && isFanPrice?ticketGroup.getLoyalFanPriceDouble():ticketGroup.getOriginalPrice());
				ticketGroup.setShowDiscPriceArea(true);
				
				ticketGroup.setIsPromoCodeApplied(true);
				ticketGroup.setPromotionalCode("MobileDiscount");
				ticketGroup.setPromotionalCodeMessage("We have applied mobile discount");
			}else if(isCheckoutOfferEligible){
			
				if(isCheckoutFlatOff){
					discAmt = TicketUtil.getSingleTicketFlatDiscount(ticketGroup.getQuantity(), discount);
					ticketGroup.setIsFlatDiscount(true);
					ticketGroup.setFlatDiscount(discAmt);
				}else{
					Double additionalDiscConv = discount / 100;;
					discAmt = TicketUtil.getDiscountedPrice(ticketPrice, additionalDiscConv);
					ticketGroup.setIsFlatDiscount(false);
					ticketGroup.setFlatDiscount(discAmt);
				}
				System.out.println("Flat Off Discount : "+discAmt);
				if(isFanPrice != null && isFanPrice){
					if(!isCheckoutFlatOff){
						ticketPrice = ticketPrice - discAmt;
						ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					}
					System.out.println("Discounted Loyal Fan Price For Flat Off: "+ticketGroup.getLoyalFanPriceDouble());
				}else{
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("Discounted Original Price For Flat Off: "+ticketPrice);
				}
				ticketGroup.setNormalTixPrice(normalPrice);
				ticketGroup.setDiscountedTixPrice(ticketPrice);
				ticketGroup.setShowDiscPriceArea(true);
				ticketGroup.setIsPromoCodeApplied(true);
				ticketGroup.setPromotionalCode("");
				ticketGroup.setPromotionalCodeMessage("We have applied discount for this order");
				
				CheckOutOfferAppliedAudit audit = DAORegistry.getCheckOutOfferAppliedAuditDAO().getCheckoutOffer(applicationPlatForm, customer.getId(), 
						sessionId, ticketGroupId, isFanPrice);
				
				if(null == audit){
					audit= new CheckOutOfferAppliedAudit();
					audit.setCategoryTicketGroupId(ticketGroupId);
					audit.setCreatedDate(new Date());
					audit.setCustomerId(customerId);
					audit.setDiscount(discount);
					audit.setEventId(eventId);
					audit.setIsFanPrice(isFanPrice);
					audit.setIsFlatOff(isCheckoutFlatOff);
					audit.setIsVisitedEvent(checkoutOtherEvent?false:true);
					audit.setOtherEventOrderThreshold(minOrderTotal);
					audit.setPlatForm(applicationPlatForm);
					audit.setSessionId(sessionId);
					audit.setStatus("INITIATED");
					audit.setTicketNormalPrice(normalPrice);
					audit.setTicketSellingPrice(ticketPrice);
					audit.setUpdatedDate(new Date());
					DAORegistry.getCheckOutOfferAppliedAuditDAO().saveOrUpdate(audit);
				}
			}
			
			if(isTicketDownloadOffer){
				
				discAmt = TicketUtil.getDiscountedPrice(ticketPrice, PaginationUtil.onDayBeforeEventTicketDownloadRewardOffer);
				ticketGroup.setIsFlatDiscount(false);
				ticketGroup.setFlatDiscount(discAmt);
				
				if(isFanPrice != null && isFanPrice){
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("On Day Before Event Ticket Download Offer Original Price: "+ticketGroup.getLoyalFanPriceDouble());
				}else{
					ticketPrice = ticketPrice - discAmt;
					ticketGroup.setOriginalPrice(TicketUtil.getRoundedValue(ticketPrice));
					System.out.println("On Day Before Event Ticket Download Offer Original Price: "+ticketPrice);
				}
				ticketGroup.setNormalTixPrice(normalPrice);
				ticketGroup.setDiscountedTixPrice(isFanPrice != null && isFanPrice?ticketGroup.getLoyalFanPriceDouble():ticketGroup.getOriginalPrice());
				ticketGroup.setShowDiscPriceArea(true);
				
			}
			
			if(isFanPrice != null && isFanPrice){
				ticketPrice = ticketGroup.getLoyalFanPriceDouble();
			}
			
			if(ticketGroup.getNormalTixPrice() < 1){
				ticketGroup.setNormalTixPrice(ticketPrice);
			}
			
			serviceFees = TicketUtil.getBrokerServiceFees(ticketGroup.getBrokerId(), ticketPrice, ticketGroup.getQuantity(), ticketGroup.getOriginalTaxPerc());
			serverOrderTotal =  ((ticketPrice * ticketGroup.getQuantity()) + serviceFees);
			ticketGroup.setTaxesAsDouble(serviceFees);
			Double singleTixFees = TicketUtil.getRoundedValue(serviceFees / ticketGroup.getQuantity());
			ticketGroup.setTotalServiceFees(TicketUtil.getRoundedValueString(serviceFees));
			ticketGroup.setSingleTixServiceFees(TicketUtil.getRoundedValueString(singleTixFees));
			
			if(isMobileDiscount){
				if(rtfPromoCodeTracking != null){
					//DAORegistry.getRtfPromotionalOfferTrackingDAO().updateOfferTrackingDiscount(rtfPromoCodeTracking.getId(), mobileAppDiscConv, discAmt,ticketPrice);
				}else{
					rtfPromoCodeTracking = new RTFPromotionalOfferTracking();
					rtfPromoCodeTracking.setCustomerId(customer.getId());
					rtfPromoCodeTracking.setPromotionalOfferId(-1);
					rtfPromoCodeTracking.setTicketGroupId(ticketGroupId);
					rtfPromoCodeTracking.setIsLongTicket(false);
					rtfPromoCodeTracking.setEventId(eventId);
					rtfPromoCodeTracking.setPlatForm(applicationPlatForm);
					rtfPromoCodeTracking.setSessionId(sessionId);
					rtfPromoCodeTracking.setStatus("PENDING");
					rtfPromoCodeTracking.setOfferType(PromotionalType.MOBILE_APP_DISCOUNT);
					rtfPromoCodeTracking.setOrderId(null);
					rtfPromoCodeTracking.setPromoCode("MOBILEDISCOUNT");
					rtfPromoCodeTracking.setIsFlatDiscount(false);
					rtfPromoCodeTracking.setAdditionalDiscountConv(0.00);
					rtfPromoCodeTracking.setMobileAppDiscConv(mobileAppDiscConv);
					rtfPromoCodeTracking.setAdditionalDiscountAmt(discAmt);
					rtfPromoCodeTracking.setServiceFees(serviceFees);
					rtfPromoCodeTracking.setTotalPrice(serverOrderTotal - serviceFees);
					rtfPromoCodeTracking.setTicketPrice(normalPrice);
					rtfPromoCodeTracking.setDiscountedPrice(ticketPrice);
					rtfPromoCodeTracking.setCreatedDate(new Date());
					rtfPromoCodeTracking.setUpdatedDate(new Date());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().saveOrUpdate(rtfPromoCodeTracking);
				}
			}
			
			
			Double earningRewardPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getRewardConversion());
			
			/*AffiliatePromoCodeTracking promoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(applicationPlatForm, customer.getId(), 
					sessionId, ticketGroupId, eventId);
			if(null != promoCodeTracking){
				earningRewardPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, loyaltySettings.getAffiliatePromoRewardConv());
				ticketGroup.setIsPromoCodeApplied(true);
				ticketGroup.setPromotionalCode(promoCodeTracking.getPromoCode());
				ticketGroup.setPromotionalCodeMessage("You have applied "+promoCodeTracking.getPromoCode()+" Promotional Code");
			}*/
			
			Double ticketDownloadOfferPoints = RewardConversionUtil.getRewardPoints(serverOrderTotal, PaginationUtil.onDayBeforeEventTicketDownloadRewardOffer);
			
			ticketList.setTicketDownloadOption1(TicketUtil.getTicketDownloadOfferOption1(fontSizeStr));
			ticketList.setTicketDownloadOption2(TicketUtil.getTicketDownloadOfferOption2(event, fontSizeStr,TicketUtil.getRoundedValueString(ticketDownloadOfferPoints)));
			
			
			ticketGroup.setOrderTotalAsDouble(TicketUtil.getRoundedValue(serverOrderTotal));
			ticketGroup.setOrderTotal(TicketUtil.getRoundedValueString(serverOrderTotal));
			ticketGroup.setRewardPoints("<font color=#012e4f>You are about to earn <b>"+TicketUtil.getRoundedValueString(earningRewardPoints)+" Reward Dollars.</b></font>");
			ticketList.setCategoryTicketGroup(ticketGroup);
			ticketList.setOnDayBeforeEventTixDownload(isTicketDownloadOffer);
			ticketList.setEvent(event);
			//ticketList.setDiscountDetails(discountDetails);
			ticketList.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETGROUP,"Success");
			return ticketList;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription(e.getMessage());
			ticketList.setError(error);
			ticketList.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETTICKETGROUP,"Exception Occurred");
			return ticketList;
		}
	}
	
	@RequestMapping(value = "/GetAllGrandChildCategory",method=RequestMethod.POST)
	public @ResponsePayload GrandChildCategoryResponse getAllGrandChildCategory(HttpServletRequest request,HttpServletResponse response,Model model){
		
		GrandChildCategoryResponse grandChildList =new GrandChildCategoryResponse();
		Error error = new Error();
		try { 
			
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				grandChildList.setError(error);
				grandChildList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGRANDCHILDCATEGORY,"You are not authorized");
				return grandChildList;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						grandChildList.setError(error);
						grandChildList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGRANDCHILDCATEGORY,"You are not authorized");
						return grandChildList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					grandChildList.setError(error);
					grandChildList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGRANDCHILDCATEGORY,"You are not authorized");
					return grandChildList;
				}
			}else{
				error.setDescription("You are not authorized.");
				grandChildList.setError(error);
				grandChildList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGRANDCHILDCATEGORY,"You are not authorized");
				return grandChildList;
			}

			String productTypeStr = request.getParameter("productType");
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					grandChildList.setError(error);
					grandChildList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGRANDCHILDCATEGORY,"Please send valid product type");
					return grandChildList;
				}
			}
			
			String pageNumber = request.getParameter("pageNumber");			
			if(pageNumber == null || pageNumber.isEmpty()){
				error.setDescription("PageNumber is Mandatory.");
				grandChildList.setError(error);
				grandChildList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGRANDCHILDCATEGORY,"PageNumber is Mandatory.");
				return grandChildList;
			}
			
			Integer maxRows = 10;
			
			List<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getAllGrandChildCategoryByPageNumber(Integer.parseInt(pageNumber), maxRows);
			
			if(grandChildCategories == null || grandChildCategories.isEmpty()){
				error.setDescription("No GrandChildCAtegory Available.");
				grandChildList.setError(error);
				grandChildList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGRANDCHILDCATEGORY,"No GrandChildCAtegory Available.");
				return grandChildList;
			}
			
			grandChildList.setStatus(1);
			grandChildList.setGrandChildCategory(grandChildCategories);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGRANDCHILDCATEGORY,"Success");
		}catch (Exception e) {
			error.setDescription(e.getMessage());
			grandChildList.setError(error);
			grandChildList.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETGRANDCHILDCATEGORY,"Exception Occurred");
			return grandChildList;
		}
		return grandChildList;
	}
	
	@RequestMapping(value = "/GetArtistByGrandChildCateoryId",method=RequestMethod.POST)
	public @ResponsePayload ArtistList getArtistByGrandChildCateoryId(HttpServletRequest request,HttpServletResponse response,Model model){
		
		ArtistList artistList =new ArtistList();
		Error error = new Error();
		try { 
			
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"You are not authorized");
				return artistList;
			}
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						artistList.setError(error);
						artistList.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"You are not authorized");
						return artistList;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					artistList.setError(error);
					artistList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"You are not authorized");
					return artistList;
				}
			}else{
				error.setDescription("You are not authorized.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"You are not authorized");
				return artistList;
			}

			String productTypeStr = request.getParameter("productType");
			
			ProductType productType=null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					artistList.setError(error);
					artistList.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"Please send valid product type");
					return artistList;
				}
			}
			
			String pageNumber = request.getParameter("pageNumber");			
			if(pageNumber == null || pageNumber.isEmpty()){
				error.setDescription("PageNumber is Mandatory.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"PageNumber is Mandatory.");
				return artistList;
			}
			
			Integer maxRows = 10;
			
			String grandChildCategoryId = request.getParameter("grandChildCategoryId");
			if(grandChildCategoryId == null || grandChildCategoryId.isEmpty()){
				error.setDescription("GrandChildCategoryId is Mandatory.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"GrandChildCategoryId is Mandatory.");
				return artistList;
			}
			
			String customerId = request.getParameter("customerId");
			
			Integer superFanArtistId = 0;
			Map<Integer, Boolean> favArtistMap = new HashMap<Integer, Boolean>();
			if(null != customerId && !customerId.isEmpty()){
				List<CustomerFavoritesHandler> favArtists = DAORegistry.getCustomerFavoritesHandlerDAO().getallArtistByCustomerId(Integer.parseInt(customerId));
				if(null !=favArtists && !favArtists.isEmpty()){
					for (CustomerFavoritesHandler favArtist : favArtists) {
						favArtistMap.put(favArtist.getArtistId(), Boolean.TRUE);
					}
				}
				
				List<CustomerSuperFan> superFans = DAORegistry.getCustomerSuperFanDAO().getAllSuperFansByCustomerId(Integer.parseInt(customerId.trim()));
				if(null != superFans && !superFans.isEmpty()){
					Artist artist = WebServiceUtil.getArtistById(superFans.get(0).getArtistOrTeamId());
					if(null != artist){
						superFanArtistId = artist.getId();
					}
				}
			}
			
			List<Artist> artistLists = DAORegistry.getArtistDAO().getAllArtistByGrandChildCategoryAndPageNumber(Integer.parseInt(grandChildCategoryId), Integer.parseInt(pageNumber), maxRows, productType, favArtistMap, superFanArtistId);
			
			if(artistLists == null || artistLists.isEmpty()){
				error.setDescription("No Artist Available.");
				artistList.setError(error);
				artistList.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETARTISTBYGRANDCHILD,"No Artist Available.");
				return artistList;
			}
			
			artistList.setStatus(1);
			artistList.setArtists(artistLists);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETARTISTBYGRANDCHILD,"Success");
		}catch (Exception e) {
			error.setDescription(e.getMessage());
			artistList.setError(error);
			artistList.setStatus(0);
			TrackingUtils.webServiceTracking(request,WebServiceActionType.GETARTISTBYGRANDCHILD,"Exception Occurred");
			return artistList;
		}
		return artistList;
	}
	
	
	 
	
	@RequestMapping(value="/GetLockedTicketsTimeLeft",method=RequestMethod.POST)
	public @ResponsePayload TicketGroupLockStatus getLockedTicketsTimeLeft(HttpServletRequest request, Model model,HttpServletResponse response){
		TicketGroupLockStatus ticketGroupLockStatus =new TicketGroupLockStatus();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"You are not authorized");
				return ticketGroupLockStatus;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"You are not authorized");
						return ticketGroupLockStatus;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"You are not authorized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("You are not authorized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"You are not authorized");
				return ticketGroupLockStatus;
			}
			
			try{
				//Add WebConfig SourceKey in the response for Credit Card Encryption
				ticketGroupLockStatus.setEncryptionKey(ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey());
			}catch(Exception e){
				e.printStackTrace();
			}
				
			String productTypeStr = request.getParameter("productType");
			String sessionIdOrDeviceIdStr =request.getParameter("sessionIdOrDeviceId");
			String eventIdStr = request.getParameter("eventId");
			String ticketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String platForm = request.getParameter("platForm");
			String customerIdStr =request.getParameter("customerId");
			String ipAddress = (null != request.getParameter("ipAddress"))?request.getParameter("ipAddress"):ip;
			//Offer for downloading tickets on day before the event
			String ticketDownloadOfferStr =request.getParameter("onDayBeforeTheEvent");
			
			if(TextUtil.isEmptyOrNull(ticketDownloadOfferStr)){
				ticketDownloadOfferStr = "false";
			}
									  
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Product Type is mandatory");
				return ticketGroupLockStatus;
			}
			ProductType productType;
			try{
				productType  = ProductType.valueOf(productTypeStr);
			}catch(Exception e){
				error.setDescription("Please Choose valid product type.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Please Choose valid product type.");
				return ticketGroupLockStatus;
			}
					
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Event Id is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Event Id is mandatory");
				return ticketGroupLockStatus;
			}
			
			Integer eventId= null;
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				try {
					eventId=Integer.parseInt(eventIdStr);
				} catch (Exception e) {
					error.setDescription("Event is not recognized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Event is not recognized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("Event is not recognized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Event is not recognized");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(sessionIdOrDeviceIdStr)){
				error.setDescription("SessionId or DeviceId is mandatory to lock ticket.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"SessionId or DeviceId is mandatory to lock ticket");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(ticketGroupIdStr)){
				error.setDescription("Ticket Group Id is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Ticket Group Id is mandatory");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Platform  is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Platform is mandatory");
				return ticketGroupLockStatus;
			}
			
			ApplicationPlatForm applicationPlatForm = null;
			try{
				applicationPlatForm = ApplicationPlatForm.valueOf(platForm);
			}catch(Exception e){
				error.setDescription("Platform is wrong.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Platform is wrong");
				return ticketGroupLockStatus;
			}
			
			CustomerSuperFan superFan = null;
			Integer customerId = null;
			Customer customer=null;
			
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
					
				try {
					customerId=Integer.parseInt(customerIdStr);
			        customer=CustomerUtil.getCustomerById(customerId);
			        if(customer==null){
						error.setDescription("Customer is not recognized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Customer is not recognized");
						return ticketGroupLockStatus;
					}
				} catch (Exception e) {
					error.setDescription("Customer is not recognized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Customer is not recognized");
					return ticketGroupLockStatus;
				}
			}
			
			Integer ticketGroupId = null;
			if(null != ticketGroupIdStr && !ticketGroupIdStr.isEmpty()){
				
				try{
					ticketGroupId = Integer.parseInt(ticketGroupIdStr);
				}catch(Exception e){
					error.setDescription("Category Ticket Group Id is not recognized .");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Category Ticket Group Id is not recognized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("Category Ticket Group Id is not recognized .");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Category Ticket Group Id is not recognized");
				return ticketGroupLockStatus;
			}
						
			boolean lockedBySameDevice =false,lockedByDifferentDevice=false;
		
			List<AutoCatsLockedTickets> lockedTickets = DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketByEventId(eventId,productType);
			AutoCatsLockedTickets lockedTicket = null;
			
			for (AutoCatsLockedTickets lockTicket : lockedTickets) {
				Integer lockticketGroupId =lockTicket.getCatgeoryTicketGroupId();
				if(lockTicket.getSessionId().equalsIgnoreCase(sessionIdOrDeviceIdStr) && 
						(lockticketGroupId.equals(ticketGroupId))){
					lockedTicket= lockTicket;
					lockedBySameDevice =true;
					break;
				}else if(lockticketGroupId==ticketGroupId){
					lockedByDifferentDevice=true;
					break;
				}
			}
			//List<AutoCatsTicketGroup> ticketGroups = new ArrayList<AutoCatsTicketGroup>();
			CategoryTicketGroup ticketGroup = DAORegistry.getCategoryTicketGroupDAO().getBroadCastedCategoryTicketsbyId(ticketGroupId);
			
			if(null == ticketGroup){
				ticketGroupLockStatus.setStatus(1);
				ticketGroupLockStatus.setButtonValue("Find Other Tickets to this Event");
				ticketGroupLockStatus.setOverRideButton(true);
				ticketGroupLockStatus.setLockStatus("Failure");
				ticketGroupLockStatus.setMessage("It looks like this was just purchased and is no longer available for sale. " +
						"If you've just made a purchase, this may be because you've managed to buy the last ticket.");
				return ticketGroupLockStatus;
			}
			
			//Re-Compute Ticket Price by Platform
			ticketGroup.computeTicketPrice(applicationPlatForm);
			
			//Get Ticket Shipping Method and Delivery Info
			//TicketUtil.getTicketDeliveryInFo(ticketGroup);
			
			ticketGroupLockStatus.setOnDayBeforeEventTixDownload(Boolean.valueOf(ticketDownloadOfferStr));
			ticketGroupLockStatus.setCategoryTicketGroup(ticketGroup);
			
			if(lockedByDifferentDevice){
				ticketGroupLockStatus.setStatus(1);
				ticketGroupLockStatus.setButtonValue("Find Other Tickets to this Event");
				ticketGroupLockStatus.setOverRideButton(true);
				ticketGroupLockStatus.setLockStatus("Failure");
				ticketGroupLockStatus.setMessage("It looks like this was just purchased and is no longer available for sale. " +
				"If you've just made a purchase, this may be because you've managed to buy the last ticket.");
				//ticketGroupLockStatus.setMessage("Ticket was sold. Please Choose different ticket.");
				return ticketGroupLockStatus;
			}else if(lockedBySameDevice){
				ticketGroupLockStatus.setStatus(1);
				ticketGroupLockStatus.setLockStatus("Success");
				ticketGroupLockStatus.setMessage("Ticket Group already locked by this Device Id or Session Id");
				ticketGroupLockStatus.setCatgeoryTicketGroupId(ticketGroupId);
				String timeLeft = TicketUtil.getTimeLeft(lockedTicket.getCreationDate(), new Date());
				ticketGroupLockStatus.setTimeLeft(timeLeft);
				return ticketGroupLockStatus;
			}
			ticketGroupLockStatus.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Success");
			return ticketGroupLockStatus;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while locking the ticket group.");
			ticketGroupLockStatus.setError(error);
			ticketGroupLockStatus.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Error occured while locking the ticket group");
			return ticketGroupLockStatus;
		}
	}
	
	
	@RequestMapping(value="/UnLockTicketGroup",method=RequestMethod.POST)
	public @ResponsePayload TicketGroupLockStatus unLockTicketGroup(HttpServletRequest request, Model model,HttpServletResponse response){
		TicketGroupLockStatus ticketGroupLockStatus =new TicketGroupLockStatus();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"You are not authorized");
				return ticketGroupLockStatus;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"You are not authorized");
						return ticketGroupLockStatus;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"You are not authorized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("You are not authorized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"You are not authorized");
				return ticketGroupLockStatus;
			}
			
			try{
				//Add WebConfig SourceKey in the response for Credit Card Encryption
				ticketGroupLockStatus.setEncryptionKey(ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey());
			}catch(Exception e){
				e.printStackTrace();
			}
				
			String productTypeStr = request.getParameter("productType");
			String sessionIdOrDeviceIdStr =request.getParameter("sessionIdOrDeviceId");
			String eventIdStr = request.getParameter("eventId");
			String ticketGroupIdStr = request.getParameter("categoryTicketGroupId");
			String platForm = request.getParameter("platForm");
			String customerIdStr =request.getParameter("customerId");
			String ipAddress = (null != request.getParameter("ipAddress"))?request.getParameter("ipAddress"):ip;
									  
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"Product Type is mandatory");
				return ticketGroupLockStatus;
			}
			ProductType productType;
			try{
				productType  = ProductType.valueOf(productTypeStr);
			}catch(Exception e){
				error.setDescription("Please Choose valid product type.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"Please Choose valid product type.");
				return ticketGroupLockStatus;
			}
					
			if(TextUtil.isEmptyOrNull(eventIdStr)){
				error.setDescription("Event Id is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"Event Id is mandatory");
				return ticketGroupLockStatus;
			}
			
			Integer eventId= null;
			if(eventIdStr!=null && !eventIdStr.isEmpty()){
				try {
					eventId=Integer.parseInt(eventIdStr);
				} catch (Exception e) {
					error.setDescription("Event is not recognized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.UNLOCKTICKETGROUP,"Event is not recognized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("Event is not recognized.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UNLOCKTICKETGROUP,"Event is not recognized");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(sessionIdOrDeviceIdStr)){
				error.setDescription("SessionId or DeviceId is mandatory to lock ticket.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"SessionId or DeviceId is mandatory to lock ticket");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(ticketGroupIdStr)){
				error.setDescription("Ticket Group Id is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"Ticket Group Id is mandatory");
				return ticketGroupLockStatus;
			}
			
			if(TextUtil.isEmptyOrNull(platForm)){
				error.setDescription("Platform  is mandatory.");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"Platform is mandatory");
				return ticketGroupLockStatus;
			}
			
			/*CustomerSuperFan superFan = null;
			Integer customerId = null;
			Customer customer=null;
			
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
					
				try {
					customerId=Integer.parseInt(customerIdStr);
			        customer=CustomerUtil.getCustomerById(customerId);
			        if(customer==null){
						error.setDescription("Customer is not recognized.");
						ticketGroupLockStatus.setError(error);
						ticketGroupLockStatus.setStatus(0);
						TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Customer is not recognized");
						return ticketGroupLockStatus;
					}
				} catch (Exception e) {
					error.setDescription("Customer is not recognized.");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.GETLOCKEDTICKETSTIMELEFT,"Customer is not recognized");
					return ticketGroupLockStatus;
				}
			}*/
			
			Integer ticketGroupId = null;
			if(null != ticketGroupIdStr && !ticketGroupIdStr.isEmpty()){
				
				try{
					ticketGroupId = Integer.parseInt(ticketGroupIdStr);
				}catch(Exception e){
					error.setDescription("Category Ticket Group Id is not recognized .");
					ticketGroupLockStatus.setError(error);
					ticketGroupLockStatus.setStatus(0);
					TrackingUtils.webServiceTracking(request,WebServiceActionType.UNLOCKTICKETGROUP,"Category Ticket Group Id is not recognized");
					return ticketGroupLockStatus;
				}
			}else{
				error.setDescription("Category Ticket Group Id is not recognized .");
				ticketGroupLockStatus.setError(error);
				ticketGroupLockStatus.setStatus(0);
				TrackingUtils.webServiceTracking(request,WebServiceActionType.UNLOCKTICKETGROUP,"Category Ticket Group Id is not recognized");
				return ticketGroupLockStatus;
			}
			
			
			AutoCatsLockedTickets lockTicketGroup= DAORegistry.getAutoCatsLockedTicketDAO().getLockedTicketBySessionAndCategoryTicketGroupId(sessionIdOrDeviceIdStr, productType, ticketGroupId);
			
			if(null == lockTicketGroup){
				ticketGroupLockStatus.setStatus(1);
				ticketGroupLockStatus.setLockStatus("UnLocked Successfully");
				ticketGroupLockStatus.setMessage("Ticket Group UnLocked Successfully");
				ticketGroupLockStatus.setCatgeoryTicketGroupId(ticketGroupId);
				return ticketGroupLockStatus;
			}else{
				lockTicketGroup.setLockStatus(LockedTicketStatus.DELETED);
				try{
					DAORegistry.getAutoCatsLockedTicketDAO().saveOrUpdate(lockTicketGroup);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			RTFPromotionalOfferTracking rtfPromoCodeTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().getPromoTracking(ApplicationPlatForm.valueOf(platForm), Integer.valueOf(customerIdStr), 
					sessionIdOrDeviceIdStr, ticketGroupId, eventId);
			
			if(null != rtfPromoCodeTracking){
				rtfPromoCodeTracking.setStatus("CANCELLED");
				rtfPromoCodeTracking.setUpdatedDate(new Date());
				DAORegistry.getRtfPromotionalOfferTrackingDAO().update(rtfPromoCodeTracking);
			}else{
				AffiliatePromoCodeTracking promoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().getPromoTracking(ApplicationPlatForm.valueOf(platForm), Integer.valueOf(customerIdStr), 
						sessionIdOrDeviceIdStr, ticketGroupId, eventId);
				if(null != promoCodeTracking){
					promoCodeTracking.setStatus("CANCELLED");
					promoCodeTracking.setUpdatedDate(new Date());
					DAORegistry.getAffiliatePromoCodeTrackingDAO().update(promoCodeTracking);
				}
			}
			ticketGroupLockStatus.setStatus(1);
			ticketGroupLockStatus.setLockStatus("UnLocked Successfully");
			ticketGroupLockStatus.setMessage("Ticket Group UnLocked Successfully");
			ticketGroupLockStatus.setCatgeoryTicketGroupId(ticketGroupId);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"Success");
			return ticketGroupLockStatus;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while releasing locked ticket group.");
			ticketGroupLockStatus.setError(error);
			ticketGroupLockStatus.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.UNLOCKTICKETGROUP,"Error occured while releasing locked ticket group");
			return ticketGroupLockStatus;
		}
	}
}