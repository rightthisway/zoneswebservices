package com.rtftrivia.ws.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.quiz.cassandra.list.CassError;
import com.quiz.cassandra.list.DashboardInfo;
import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.OrderProducts;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.rtfquiz.webservices.aws.AwsS3Response;
import com.rtfquiz.webservices.utils.QuizContestUtil;
import com.rtfquiz.webservices.utils.RTFBotsUtil;
import com.rtfquiz.webservices.utils.list.BotDetail;
import com.rtfquiz.webservices.utils.list.CustomerStats;
import com.rtfquiz.webservices.utils.list.QuizContestInfo;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Country;
import com.zonesws.webservices.data.CountryCity;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerFeedBackTracking;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerTicketDownloads;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.GiftCardFileAttachment;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.FeedBackType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.WebServiceActionType;
import com.zonesws.webservices.filter.SecurityUtil;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.CustomerGiftCardOrderUtil;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.ZonesRGBColorUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.ImageUtil;
import com.zonesws.webservices.utils.MapUtil;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.TrackingUtils;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.list.CityDetails;
import com.zonesws.webservices.utils.list.CustomerDetails;
import com.zonesws.webservices.utils.list.Downloaddetails;
import com.zonesws.webservices.utils.list.EmailResponse;
import com.zonesws.webservices.utils.list.EventList;
import com.zonesws.webservices.utils.list.GetPromotionalDialogStatus;
import com.zonesws.webservices.utils.list.StateCountryDetails;
import com.zonesws.webservices.utils.list.ZipCodeValidationResponse;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

@Controller
@RequestMapping({"/GetStateAndCountry","/GetPromotionalDialogStatus","/GetZipCodeDetails","/CustomerFeedBackAndSupport",
	"/LoadBotsToCache", "/GetBotsReport","/LoadTest","/RefreshCustomerLoyaltyMap","/validateZipCode","/GetCityDetails",
	"/ResizeAllCustomerImages","/AttachDefaultImageToCustomers","/CreateImageForMissingCustomers",
	"/MobileSvgMapView","/ViewMySeatingChart","/LoadBotsValue","/DownloadRTFTickets","/DownloadRTFOrders",
	"/GetGiftCardFile","/GetProductFile","/HitApi","/ContactUs","/subscription"})
public class UtilityController implements ServletContextAware {
	
	private static Logger log = LoggerFactory.getLogger(UtilityController.class);
	
	@Autowired
	ServletContext context; 
	 
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
	
	private MailManager mailManager;
	private ZonesProperty properties;
	
	private static Integer apiHitCount = 0;
	
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}
	 
	@RequestMapping(value="/GetStateAndCountry",method=RequestMethod.POST)
	public @ResponsePayload StateCountryDetails getStateAndCountry(HttpServletRequest request, Model model,HttpServletResponse response){
		StateCountryDetails stateCountryDetails =new StateCountryDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				stateCountryDetails.setError(error);
				stateCountryDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSERVERTIME,"You are not authorized");
				return stateCountryDetails;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						stateCountryDetails.setError(error);
						stateCountryDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTATECOUNTRY,"You are not authorized");
						return stateCountryDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					stateCountryDetails.setError(error);
					stateCountryDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTATECOUNTRY,"You are not authorized");
					return stateCountryDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				stateCountryDetails.setError(error);
				stateCountryDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTATECOUNTRY,"You are not authorized");
				return stateCountryDetails;
			}
			
			Collection<Country> countries = DAORegistry.getCountryDAO().getActiveCountries();
			
			for (Country country : countries) {
				country.setStateList(DAORegistry.getStateDAO().getAllStateByCountryId(country.getId()));
			}
			stateCountryDetails.setCountryList(countries);
			stateCountryDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTATECOUNTRY,"Success");
			return stateCountryDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting state and country details");
			stateCountryDetails.setError(error);
			stateCountryDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETSTATECOUNTRY,"Error occured while gettting state and country details");
			return stateCountryDetails;
		}
	}
	
	@RequestMapping(value="/GetPromotionalDialogStatus",method=RequestMethod.POST)
	public @ResponsePayload GetPromotionalDialogStatus getpomotionalDialogStatus(HttpServletRequest request, Model model,HttpServletResponse response){
		GetPromotionalDialogStatus responseObj =new GetPromotionalDialogStatus();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			String productTypeStr = request.getParameter("productType");
			String customerIdStr = request.getParameter("customerId");
			String platFormStr = request.getParameter("platForm");
			String sessionId = request.getParameter("deviceId");			
			String clientIPAddress = request.getParameter("clientIPAddress");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PROMODIALOGSTATUS,"Product Type is mandatory");
				return responseObj;
			}
			if(TextUtil.isEmptyOrNull(sessionId)){
				error.setDescription("Device Id is mandatory.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PROMODIALOGSTATUS,"Device Id is mandatory");
				return responseObj;
			}
			
			ApplicationPlatForm appPlatForm = null;
			try{
				appPlatForm = ApplicationPlatForm.valueOf(platFormStr);
			}catch(Exception e){
				error.setDescription("Please send valid application platform");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PROMODIALOGSTATUS,"Please send valid application platform");
				return responseObj;
			}
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory.");
				responseObj.setError(error);
				responseObj.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.PROMODIALOGSTATUS,"Product Type is mandatory");
				return responseObj;
			}
			ProductType productType = ProductType.REWARDTHEFAN;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please send valid product type");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.PROMODIALOGSTATUS,"Please send valid product type");
					return responseObj;
				}
			}
			
			Integer customerId=  null;
			Customer customer = null;
			if(!TextUtil.isEmptyOrNull(customerIdStr)){
				
				try{
					customerId = Integer.parseInt(customerIdStr.trim());
					customer = CustomerUtil.getCustomerById(customerId, productType);
					if(null == customer){
						error.setDescription("Customer Information Not Found.");
						responseObj.setError(error);
						responseObj.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.PROMODIALOGSTATUS,"Customer Information Not Found.");
						return responseObj;
					}
				}catch(Exception e){
					error.setDescription("Customer Information Not Found.");
					responseObj.setError(error);
					responseObj.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.PROMODIALOGSTATUS,"Customer Information Not Found.");
					return responseObj;
				}
			}
			boolean showDialog = false;
			if(null != customer){
				RTFCustomerPromotionalOffer custPromoOffer = DAORegistry.getRtfCustomerPromotionalOfferDAO().getPromoOfferCustomerId(customerId);
				if(null == custPromoOffer){
					responseObj.setMainButtonValue("Get Promotional Code");
					responseObj.setSecondButtonValue("Cancel");
					responseObj.setShowSignUpPage(false);
				}else{
					showDialog = false;
				}
			}else{
				responseObj.setMainButtonValue("Sign Up");
				responseObj.setSecondButtonValue("Cancel");
				responseObj.setShowSignUpPage(true);
			}
			responseObj.setImageUrl(URLUtil.getPromoDialogImage("CustomerPromoDialog_1.jpg"));
			responseObj.setShowImageText(false);
			responseObj.setHeaderName("Promotional Offer");
			responseObj.setShowDialog(showDialog);
			responseObj.setStatus(1);
			return responseObj;
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.setShowDialog(false);
			responseObj.setStatus(1);
			return responseObj;
		}
	}
	
	@RequestMapping(value="/GetZipCodeDetails",method=RequestMethod.POST)
	public CityDetails getCityDetailsByZipCode(HttpServletRequest request,HttpServletResponse reponse){
		CityDetails cityDetails =new CityDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						cityDetails.setError(error);
						cityDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIESBYZIPCODE,"You are not authorized");
						return cityDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					cityDetails.setError(error);
					cityDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIESBYZIPCODE,"You are not authorized");
					return cityDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				cityDetails.setError(error);
				cityDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIESBYZIPCODE,"You are not authorized");
				return cityDetails;
			}
			
			String zipCode = request.getParameter("zipCode");
			if(TextUtil.isEmptyOrNull(zipCode)){
				error.setDescription("Please enter valid zipcode");
				cityDetails.setError(error);
				cityDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIESBYZIPCODE,"Please enter valid zipcode");
				return cityDetails;
			}
			
			zipCode =TextUtil.removeExtraWhitespaces(zipCode);
			
			List<CountryCity> countryCities = DAORegistry.getQueryManagerDAO().getCityDetailsByZipcode(zipCode);
			if(null != countryCities && !countryCities.isEmpty()){
				cityDetails.setCityObj(countryCities.get(0));
				cityDetails.setNoZipCode(false);
			}else{
				cityDetails.setNoZipCode(true);
			}
			cityDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIESBYZIPCODE,"Success");
			return cityDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting city informations");
			cityDetails.setError(error);
			cityDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIESBYZIPCODE,"Error occured while gettting city informations");
			return cityDetails;
		}
	}
	
	@RequestMapping(value="/CustomerFeedBackAndSupport",method=RequestMethod.POST)
	public @ResponsePayload EmailResponse customerFeedEbackSupport(HttpServletRequest request, Model model,HttpServletResponse response){
		EmailResponse emailResponse =new EmailResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			String platformStr = request.getParameter("platForm");
			
			Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
			
			if(!isAuthrozed){
				error.setDescription("You are not authorized.");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"You are not authorized");
				return emailResponse;
			}
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						emailResponse.setError(error);
						emailResponse.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"You are not authorized");
						return emailResponse;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					emailResponse.setError(error);
					emailResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"You are not authorized");
					return emailResponse;
				}
			}else{
				error.setDescription("You are not authorized.");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"You are not authorized");
				return emailResponse;
			}
			
			String feedBackTypeStr = request.getParameter("feedBackType");
			String customerIdStr = request.getParameter("customerId");
			String message = request.getParameter("message");
			String productTypeStr = request.getParameter("productType");
			
			if(TextUtil.isEmptyOrNull(productTypeStr)){
				error.setDescription("Product Type is mandatory");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Product Type is empty or null");
				return emailResponse;
			}
			
			if(TextUtil.isEmptyOrNull(feedBackTypeStr)){
				error.setDescription("Feedback Type is mandatory");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Feedback Type is empty or null");
				return emailResponse;
			}
			
			ProductType productType = null;
			if(!TextUtil.isEmptyOrNull(productTypeStr)){
				try{
					productType = ProductType.valueOf(productTypeStr);
				}catch(Exception e){
					error.setDescription("Please choose valid product type");
					emailResponse.setError(error);
					emailResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Please choose valid product type");
					return emailResponse;
				}
			}
			
			ApplicationPlatForm platform = null;
			if(!TextUtil.isEmptyOrNull(platformStr)){
				try{
					platform = ApplicationPlatForm.valueOf(platformStr);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			FeedBackType feedBackType = null;
			if(!TextUtil.isEmptyOrNull(feedBackTypeStr)){
				try{
					feedBackType = FeedBackType.valueOf(feedBackTypeStr);
				}catch(Exception e){
					error.setDescription("Please choose valid Feedback type");
					emailResponse.setError(error);
					emailResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Please choose valid Feedback type");
					return emailResponse;
				}
			}
			
			Integer customerId = null;
			Customer customer = null;
			try{
				customerId = Integer.parseInt(customerIdStr.trim());
				customer = CustomerUtil.getCustomerById(customerId);
				
				if(customer == null){
					error.setDescription("Customer is not recognized");
					emailResponse.setError(error);
					emailResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Customer is not recognized");
					return emailResponse;
				}
				
			}catch(Exception e){
				error.setDescription("Customer is not recognized");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Customer is not recognized");
				return emailResponse;
			}
			
			String userName = customer.getCustomerName()+" "+customer.getLastName();
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("fullName", userName);
			mailMap.put("phoneNumber", customer.getPhone());
			mailMap.put("email", customer.getEmail());
			mailMap.put("UserID", customer.getUserId());
			mailMap.put("CustomerID", customer.getId());
			
			userName = customer.getUserId();
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
			
			Boolean iscustEmailSent = Boolean.FALSE;
			Boolean isFeedbackEmailSent = Boolean.FALSE;
			try{
				
				
				switch (feedBackType) {
				
					case LOVETHISAPP:
						mailMap.put("subject", userName+" Loves our app");
						mailMap.put("message", "RewardTheFan app is awesome");
						mailMap.put("headerMessage","");
						mailManager.sendMailNow("text/html",URLUtil.fromEmail,URLUtil.fromEmail, 
								null,URLUtil.bccEmails, customer.getUserId()+" Loves our app",
					    		"email-customer-feedback.html", mailMap, "text/html", null,mailAttachment,null);
						
						isFeedbackEmailSent = true;
						break;
						
					case HAVEFEEDBACK:
						if(TextUtil.isEmptyOrNull(message)){
							error.setDescription("Message is mandatory");
							emailResponse.setError(error);
							emailResponse.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Message is empty or null");
							return emailResponse;
						}
						mailMap.put("subject", userName+" have some feedbacks");
						mailMap.put("message", message);
						mailMap.put("headerMessage","Thank you for taking the time to submit your feedback");
						mailManager.sendMailNow("text/html",URLUtil.fromEmail,URLUtil.fromEmail, 
								null,URLUtil.bccEmails, userName+" have some feedbacks",
					    		"email-customer-feedback.html", mailMap, "text/html", null,mailAttachment,null);
						
						isFeedbackEmailSent = true;
						break;
						
					case HAVEISSUE:
						if(TextUtil.isEmptyOrNull(message)){
							error.setDescription("Message is mandatory");
							emailResponse.setError(error);
							emailResponse.setStatus(0);
							TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Message is empty or null");
							return emailResponse;
						}
						mailMap.put("subject", userName+" have some issues");
						mailMap.put("message", message);
						mailMap.put("headerMessage","We have received your feedback.  Thank you for letting us know and we will reach out to you regarding your message as soon as possible.");
						mailManager.sendMailNow("text/html",URLUtil.fromEmail,URLUtil.fromEmail, 
								null,URLUtil.bccEmails, userName+" have some issues",
					    		"email-customer-feedback.html", mailMap, "text/html", null,mailAttachment,null);
						
						isFeedbackEmailSent = true;
						break;
	
					default:
						break;
				} 
				try{
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,null, "Reward The Fan : Feedback",
				    		"mail-rewardfan-feedback.html", mailMap, "text/html",null,mailAttachment,null);
					
					iscustEmailSent = true;
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			try {
				try {
					ip = request.getHeader("X-Forwarded-For");
					//System.out.println("Feedback inside ip null : "+ip+":"+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				CustomerFeedBackTracking fbTrack = new CustomerFeedBackTracking();
				fbTrack.setCustomerId(customerId);
				fbTrack.setApplicationPlatForm(platform);
				fbTrack.setProductType(productType);
				fbTrack.setFeedBackType(feedBackType);
				fbTrack.setIpAddress(ip);
				fbTrack.setMessage(message);
				fbTrack.setIsCustEmailSent(iscustEmailSent);
				fbTrack.setIsFeedBackEmailSent(isFeedbackEmailSent);
				fbTrack.setCreatedDate(new Date());
				
				DAORegistry.getCustomerFeedBackTrackingDAO().save(fbTrack);
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error while inserting Feedback tracking in DB : "+customerIdStr+":"+message+":"+new Date());
			}
			
			emailResponse.setMessage("Thanks for your feedback.");
			emailResponse.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Success");
			return emailResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Emailing Customer feedback.");
			emailResponse.setError(error);
			emailResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERFEEDBACK,"Error occured while Emailing Customer feedback.");
			return emailResponse;
		}
	}
	
	
	@RequestMapping(value = "/GetBotsReport", method=RequestMethod.POST)
	public @ResponsePayload CustomerStats getBotsReport(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			Map<String, Object> resultMap = RTFBotsUtil.getAllBotsDetails();
			dtoResponse.setBotList((List<BotDetail>)resultMap.get("BotList"));
			dtoResponse.setReferrlCodeList(new TreeSet<String>((Set<String>) resultMap.get("ReferralCodeList")));
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Bots Report were successfully fectched.");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error occured while fecthing Bots Report.Quick Attention Required!");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		return dtoResponse;
	}
	
	
	@RequestMapping(value = "/LoadBotsToCache")
	public @ResponsePayload CustomerStats loadBotsToCache(HttpServletRequest request,HttpServletResponse response,Model model){
		CustomerStats dtoResponse =new CustomerStats();
		Error error = new Error();
		try {
			RTFBotsUtil.init();
			dtoResponse.setStatus(1);
			dtoResponse.setMessage("Bots are loaded to cache.");
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error while adding bots to cache.");
			dtoResponse.setError(error);
			dtoResponse.setStatus(0);
			return dtoResponse;
		}
		return dtoResponse;
	}
	
	
	@RequestMapping(value = "/RefreshCustomerLoyaltyMap", method=RequestMethod.GET)
	public @ResponsePayload QuizContestInfo refreshCustomerLoyaltyMap(HttpServletRequest request,HttpServletResponse response,Model model){
		
		QuizContestInfo quizcontestDetails = new QuizContestInfo();
		Error error = new Error();
		Date start = new Date();
		try {
			//String contestIDStr = request.getParameter("contestId");
			//String cType = request.getParameter("cType");
			//String productTypeStr = request.getParameter("productType");
			
			
			log.info("CUSTLOYALTYMAPREFRESH: Updating Data in caches:"+new Date());
			QuizContestUtil.refreshCustomerLoyaltyMapForContest();
			log.info("CUSTLOYALTYMAPREFRESH: Updating Data in cache- Ends:"+new Date());
			
			quizcontestDetails.setMessage("CUSTLOYALTYMAPREFRESH completed successfully.");
			//quizCustomerDetails.setError(error);
			quizcontestDetails.setStatus(1);
		}catch(Exception e){
			e.printStackTrace();
			error.setDescription("Error Occured While Refreshing Contest List.");
			quizcontestDetails.setError(error);
			quizcontestDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.NEXTCONTESTLISTREFRESH,"Error Occured While Refreshing Contest List.");
			return quizcontestDetails;
		}
		log.info("REFRESHCONTESTLIST : "+request.getParameter("customerId")+" :coID: "+request.getParameter("contestId")+" : "+(new Date().getTime()-start.getTime()));
		
		return quizcontestDetails;
	} 
	@RequestMapping(value = "/LoadTest")
	public @ResponsePayload DashboardInfo testing(HttpServletRequest request,HttpServletResponse response,Model model){
		DashboardInfo dashboardInfo =new DashboardInfo();
		CassError error = new CassError();
		Date start = new Date();
		try {
			apiHitCount++;
			Thread.sleep(1000);
			dashboardInfo.setSts(1);
			dashboardInfo.setMsg("Success: "+apiHitCount);
		}catch(Exception e){
			e.printStackTrace();
			error.setDesc("Error in LoadTest API");
			dashboardInfo.setErr(error);
			dashboardInfo.setSts(0);
			return dashboardInfo;
		} finally {
			log.info("LOAD TEST: apiHitCount:"+apiHitCount+", Date:"+(new Date().getTime()-start.getTime()));
		}
		return dashboardInfo;
	
	}
	
	@RequestMapping(value="/GetCityDetails",method=RequestMethod.POST)
	public CityDetails getCityDetails(HttpServletRequest request){
		CityDetails cityDetails =new CityDetails();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						cityDetails.setError(error);
						cityDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIES,"You are not authorized");
						return cityDetails;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					cityDetails.setError(error);
					cityDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIES,"You are not authorized");
					return cityDetails;
				}
			}else{
				error.setDescription("You are not authorized.");
				cityDetails.setError(error);
				cityDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIES,"You are not authorized");
				return cityDetails;
			}
			
			String city = request.getParameter("city");
			if(TextUtil.isEmptyOrNull(city)){
				error.setDescription("Search Parameter is mandatory.");
				cityDetails.setError(error);
				cityDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIES,"Search Parameter is mandatory.");
				return cityDetails;
			}
			
			city =TextUtil.removeExtraWhitespaces(city);
			
			List<CountryCity> countryCities = DAORegistry.getQueryManagerDAO().getCityDetailsByCityName(city);
			
			Set<String> distinctKeys = new HashSet<String>();
			List<CountryCity> countryCitiesList =  new ArrayList<CountryCity>();
			
			for (CountryCity countryCity : countryCities) {
				String key = countryCity.getCity().replaceAll("\\W", "").toUpperCase()+"_"+ countryCity.getState().replaceAll("\\W", "").toUpperCase()+"_"+ countryCity.getCountry().replaceAll("\\W", "").toUpperCase();
				if(distinctKeys.add(key)){
					countryCitiesList.add(countryCity);
				}
			}
			
			cityDetails.setCityList(countryCitiesList);
			cityDetails.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIES,"Success");
			return cityDetails;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while gettting city informations");
			cityDetails.setError(error);
			cityDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETCITIES,"Error occured while gettting city informations");
			return cityDetails;
		}
	}
	
	
	@RequestMapping(value="/validateZipCode",method=RequestMethod.POST)
	public ZipCodeValidationResponse validateZipCode(HttpServletRequest request){
		ZipCodeValidationResponse zipCodeValidationResponse =new ZipCodeValidationResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("configId");
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
						error.setDescription("You are not authorized.");
						zipCodeValidationResponse.setError(error);
						zipCodeValidationResponse.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEZIPCODE,"You are not authorized");
						return zipCodeValidationResponse;
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					zipCodeValidationResponse.setError(error);
					zipCodeValidationResponse.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEZIPCODE,"You are not authorized");
					return zipCodeValidationResponse;
				}
			}else{
				error.setDescription("You are not authorized.");
				zipCodeValidationResponse.setError(error);
				zipCodeValidationResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEZIPCODE,"You are not authorized");
				return zipCodeValidationResponse;
			}
			
			String zipCode = request.getParameter("zipCode");
			if(TextUtil.isEmptyOrNull(zipCode)){
				error.setDescription("ZipCode is mandatory.");
				zipCodeValidationResponse.setError(error);
				zipCodeValidationResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEZIPCODE,"ZipCode is mandatory.");
				return zipCodeValidationResponse;
			}
			
			zipCode =TextUtil.removeExtraWhitespaces(zipCode);
			
			boolean flag = DAORegistry.getQueryManagerDAO().validateZipCode(zipCode);
			
			if(!flag){
				error.setDescription("Zipcode doesn't match with Country, Please enter valid zipcode.");
				zipCodeValidationResponse.setError(error);
				zipCodeValidationResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEZIPCODE,"Zipcode doesn't match with Country, Please enter valid zipcode.");
				return zipCodeValidationResponse;
			}
			
			zipCodeValidationResponse.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEZIPCODE,"Success");
			return zipCodeValidationResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while validating zipcode informations");
			zipCodeValidationResponse.setError(error);
			zipCodeValidationResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.VALIDATEZIPCODE,"Error occured while validating zipcode informations");
			return zipCodeValidationResponse;
		}
	}
	
	@RequestMapping(value="/CreateImageForMissingCustomers",method=RequestMethod.GET)
	public @ResponsePayload CustomerDetails createImageForMissingCustomers(HttpServletRequest request, Model model,
			HttpServletResponse response) throws Exception{
		CustomerDetails customerDetails =new CustomerDetails();
		try { 
			List<Customer> list = DAORegistry.getCustomerDAO().getAllOtpVerifiedCustomers();
			String basePath = URLUtil.profilePicBaseDirectory();
			for(Customer cust: list) {
				try {
					if(null != cust.getCustImagePath()) {
						File imageFile = new File(basePath+cust.getCustImagePath());
						if(imageFile != null && imageFile.exists()){
							continue;
						}else {
							String fileName = URLUtil.getCustomerDefaultprofilePicName(cust.getId());
							cust.setCustImagePath(fileName);
							CustomerUtil.updatedCustomerUtil(cust);
							DAORegistry.getCustomerDAO().updateCustImagePath(fileName, cust.getId());
						}
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerDetails;
	}
	
	
	
	@RequestMapping(value="/AttachDefaultImageToCustomers",method=RequestMethod.GET)
	public @ResponsePayload CustomerDetails attachDefaultImageToCustomers(HttpServletRequest request, Model model,
			HttpServletResponse response) throws Exception{
		CustomerDetails customerDetails =new CustomerDetails();
		try { 
			String isRewardImport = request.getParameter("isRewardImport");
			String isImageImport = request.getParameter("isImageImport");
			
			if(null == isRewardImport || isRewardImport.isEmpty()) {
				isRewardImport = "false";
			}
			
			if(null == isImageImport || isImageImport.isEmpty()) {
				isImageImport = "false";
			}
			
			Boolean importRewards = Boolean.valueOf(isRewardImport.trim());
			Boolean importImage = Boolean.valueOf(isImageImport.trim());
			
			List<Customer> list = DAORegistry.getCustomerDAO().getAllOtpVerifiedCustomers();
			for(Customer cust: list) {
				
				if(importImage) {
					try {
						if(null != cust.getCustImagePath()) {
							 
						}else {
							String fileName = URLUtil.getCustomerDefaultprofilePicName(cust.getId());
							cust.setCustImagePath(fileName);
							CustomerUtil.updatedCustomerUtil(cust);
							DAORegistry.getCustomerDAO().updateCustImagePath(fileName, cust.getId());
						}
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				
				if(importRewards) {
					CustomerLoyalty customerLoyalty =DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(cust.getId());
					if(null != customerLoyalty) {
						continue;
					}
					customerLoyalty = new CustomerLoyalty();
					customerLoyalty.setCustomerId(cust.getId());
					customerLoyalty.setActivePointsAsDouble(0.00);
					customerLoyalty.setLatestEarnedPointsAsDouble(0.00);
					customerLoyalty.setLatestSpentPointsAsDouble(0.00);
					customerLoyalty.setTotalEarnedPointsAsDouble(0.00);
					customerLoyalty.setTotalSpentPointsAsDouble(0.00);
					customerLoyalty.setLastUpdate(new Date());
					customerLoyalty.setLastNotifiedTime(new Date());
					customerLoyalty.setPendingPointsAsDouble(0.00);
					customerLoyalty.setTotalReferralDollars(0.00);
					customerLoyalty.setLastReferralDollars(0.00);
					customerLoyalty.setTotalAffiliateReferralDollars(0.00);
					customerLoyalty.setLastAffiliateReferralDollars(0.00);
					DAORegistry.getCustomerLoyaltyDAO().saveOrUpdate(customerLoyalty);
				}
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerDetails;
	}
	
	
	
	@RequestMapping(value="/ResizeAllCustomerImages",method=RequestMethod.GET)
	public @ResponsePayload CustomerDetails ResizeAllCustomerImages(HttpServletRequest request, Model model,
			HttpServletResponse response) throws Exception{
		CustomerDetails customerDetails =new CustomerDetails();
		try { 
			 ImageUtil.startProcessingImages();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerDetails;
	}
	
	@RequestMapping(value="/")
	public @ResponsePayload EmailResponse getHome(HttpServletRequest request,Model model,HttpServletResponse response){
		EmailResponse response2 = new EmailResponse();
		response2.setMessage("RewardTheFan WebService is Working!");
		response2.setStatus(1);
		return response2;
	}
	
	
	@RequestMapping(value="/LoadBotsValue")
	public @ResponsePayload List<Integer> loadBotsValue(HttpServletRequest request,Model model,HttpServletResponse response){
		try {
			RTFBotsUtil.init();
			int i = 1;
			for(Integer custId : RTFBotsUtil.getAllBots()) {
				System.out.println(i+"========================>"+custId);
				i++;
			}
			return RTFBotsUtil.getAllBots();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	@RequestMapping(value="/MobileSvgMapView")
	public String mobileSvgMapView(HttpServletRequest request,Model model,HttpServletResponse response, ModelMap map){
			EventList eventList =new EventList();
			Error error = new Error();
			try {
				HttpSession session = request.getSession();
				String ip =(String)session.getAttribute("ip");
				String configIdString = request.getParameter("configId");
//				Integer configId= null;
				if(configIdString!=null && !configIdString.isEmpty()){
					try {
						if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
							error.setDescription("You are not authorized.");
							eventList.setError(error);
							eventList.setStatus(0);
							map.put("error", error.getDescription());
							return "page-mobile-svg-map-view";
						
						}
					} catch (Exception e) {
						error.setDescription("You are not authorized.");
						eventList.setError(error);
						eventList.setStatus(0);
						map.put("error", error.getDescription());
						return "page-mobile-svg-map-view";
					}
				}else{
					error.setDescription("You are not authorized.");
					eventList.setError(error);
					eventList.setStatus(0);
					map.put("error", error.getDescription());
					return "page-mobile-svg-map-view";
				}
				String venueId = request.getParameter("venueId");
				String categoryGroupName = request.getParameter("categoryGroupName");
				String zones = request.getParameter("zones");
				String orderIdStr = request.getParameter("orderId");
				
				if(venueId == null || venueId.isEmpty()){
					map.put("error", "Unable to load the map");
					return "page-mobile-svg-map-view";
				}
				if(categoryGroupName == null || categoryGroupName.isEmpty()){
					map.put("error", "Unable to load the map");
					return "page-mobile-svg-map-view";
				}
				Event event = null;
				if(orderIdStr!=null && !orderIdStr.isEmpty()){
					Integer orderId = Integer.parseInt(orderIdStr);
					event = MapUtil.getSVGTextFromUtil(venueId,categoryGroupName,orderId);
				}else{
					event = MapUtil.getSVGTextFromUtil(venueId,categoryGroupName,null);
				}
				
				
				Collection<String> zonesList = new ArrayList<String>();
				if(zones != null && !zones.isEmpty() && zones.length() > 0){
					for(String category : zones.split(",")){
						if(null != category && !category.isEmpty() && category.length() > 0){
							zonesList.add(category);
						}
					}
					map.put("colorsMap", ZonesRGBColorUtil.getRGBColorsByZone(zonesList));
				}
				map.put("zoneEvent", event);
				map.put("zonesTemp", zones);
				
				return "page-mobile-svg-map-view";
		}catch (Exception e) {
			e.printStackTrace();
			map.put("error", "Unable to load the map");
			return "page-mobile-svg-map-view";
		}
	}
	
	
	@RequestMapping(value="/ViewMySeatingChart")
	public String viewMySeatingChart(HttpServletRequest request,Model model,HttpServletResponse response, ModelMap map){
			EventList eventList =new EventList();
			Error error = new Error();
			try {
				HttpSession session = request.getSession();
				String ip =(String)session.getAttribute("ip");
				String configIdString = request.getParameter("configId");
//				Integer configId= null;
				if(configIdString!=null && !configIdString.isEmpty()){
					try {
						if(ApiConfigUtil.getIpConfigByIpAndConfigId(configIdString, ip)==null){
							error.setDescription("You are not authorized.");
							eventList.setError(error);
							eventList.setStatus(0);
							map.put("error", error.getDescription());
							return "page-mobile-svg-map-view";
						
						}
					} catch (Exception e) {
						error.setDescription("You are not authorized.");
						eventList.setError(error);
						eventList.setStatus(0);
						map.put("error", error.getDescription());
						return "page-mobile-svg-map-view";
					}
				}else{
					error.setDescription("You are not authorized.");
					eventList.setError(error);
					eventList.setStatus(0);
					map.put("error", error.getDescription());
					return "page-mobile-svg-map-view";
				}
				String venueId = request.getParameter("venueId");
				String categoryGroupName = request.getParameter("categoryGroupName");
				String zones = request.getParameter("zones");
				if(venueId == null || venueId.isEmpty()){
					map.put("error", "Unable to load the map");
					return "page-mobile-svg-map-view";
				}
				if(categoryGroupName == null || categoryGroupName.isEmpty()){
					map.put("error", "Unable to load the map");
					return "page-mobile-svg-map-view";
				}
				Event event = MapUtil.getCustomerOrderSeatingChart(venueId,categoryGroupName);
				 
				Collection<String> zonesList = new ArrayList<String>();
				if(zones != null && !zones.isEmpty() && zones.length() > 0){
					for(String category : zones.split(",")){
						if(null != category && !category.isEmpty() && category.length() > 0){
							zonesList.add(category);
						}
					}
					map.put("colorsMap", ZonesRGBColorUtil.getRGBColorsByZone(zonesList));
				}
				map.put("zoneEvent", event);
				
				return "page-mobile-svg-map-view";
		}catch (Exception e) {
			e.printStackTrace();
			map.put("error", "Unable to load the map");
			return "page-mobile-svg-map-view";
		}
	}
	

    @RequestMapping(value = "/DownloadRTFOrders")
    public String download(HttpServletRequest request,Model model,HttpServletResponse response)  {
    	Downloaddetails downloadDetails = new Downloaddetails();	
    	Error error = new Error();
		 try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("cId");
			String platform = request.getParameter("platForm");
			String encryptedStr = request.getQueryString();
			String clientIPAddress = request.getParameter("cTrack");
			
			encryptedStr = encryptedStr.replace("identity=","");
			String keyString = ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey();
			String customerId = "";
			String inoiceNoStr = "",ticketAttachedIdStr="";	
			String decodedText = SecurityUtil.doDecryption(encryptedStr, keyString);
			String origText[] = decodedText.split("_");
			configIdString = origText[0];
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
//					configId=Integer.parseInt(configIdString);
					if(DAORegistry.getIpConfigDAO().getIpConfigByIpAndConfigId(configIdString,ip)==null){
						error.setDescription("You are not authorized.");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
						
					
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
					
				}
			}else{
				error.setDescription("You are not authorized.");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
				
			}
			
			if(encryptedStr != null && !encryptedStr.isEmpty()){
				/*decodedText = SecurityUtil.doDecryption(encryptedStr, keyString);
				String origText[] = decodedText.split("_");*/
				customerId = origText[1];
				inoiceNoStr = origText[2];
				ticketAttachedIdStr = origText[3];
			}

			Integer invoiceId = null;
			Invoice invoice = null;
			try{
				invoiceId=Integer.parseInt(inoiceNoStr.trim());
				invoice = DAORegistry.getInvoiceDAO().get(invoiceId);
			}catch(Exception e){
				error.setDescription("OrderNumber is Mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"OrderNumber is Mandatory");
				//return downloadDetails;
			}
			Integer ticketAttachedId = null;
			InvoiceTicketAttachment attachment = null;
			try{
				ticketAttachedId=Integer.parseInt(ticketAttachedIdStr.trim());
				
				attachment = DAORegistry.getInvoiceTicketAttachmentDAO().get(ticketAttachedId);
				
				if(attachment == null){
					error.setDescription("Ticket Attachment Id is not valid");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket Attachment Id is not valid");
				}
				
			}catch(Exception e){
				error.setDescription("Ticket Attachment Id is Mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket Attachment Id is Mandatory");
				//return downloadDetails;
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customet ID  is mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Customet ID is mandatory");
				//return downloadDetails;
			}
		
		File file = new File (URLUtil.findETicketDownloadURLFromDirectory(attachment.getFilePath()));
        log.trace("Write response...");
        InputStream fileInputStream = null;
        OutputStream output = null;
        try{
        	fileInputStream = new FileInputStream(file);
        	output = response.getOutputStream(); 

            response.reset();

            response.setContentType("application/octet-stream");
            response.setContentLength((int) (file.length()));
            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

            IOUtils.copyLarge(fileInputStream, output);
            output.flush();
            try{	
            	 CustomerTicketDownloads ticketDownload = new CustomerTicketDownloads();
                 ticketDownload.setCustomerId(Integer.parseInt(customerId));
                 ticketDownload.setInvoiceId(invoiceId);
                 ticketDownload.setOrderId(null != invoice?invoice.getId():null);
                 ticketDownload.setDownloadDateTime(new Date());
                 ticketDownload.setTicketName(attachment.getFilePath());
                 ticketDownload.setTicketType(attachment.getType().toString());
                 ticketDownload.setPlatform(platform);
                 ticketDownload.setIpAddress(null != clientIPAddress && !clientIPAddress.isEmpty()?clientIPAddress:ip);
                 DAORegistry.getCustomerTicketDownloadsDAO().save(ticketDownload);
            }catch(Exception e){
            	e.printStackTrace();
            }
        } catch (IOException e) {
        	e.printStackTrace();
            log.error(e.getMessage(), e);
            error.setDescription("Ticket not found for this order");
			downloadDetails.setError(error);
			downloadDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket not found for this order");
			//return downloadDetails;
        }finally{
        	if(null != output){
        		output.close();
        	}
        	if(null != fileInputStream){
        		fileInputStream.close();
        	}
        }

    } catch (Exception e) {
		e.printStackTrace();
		error.setDescription("Error occured while downloading.");
		downloadDetails.setError(error);
		downloadDetails.setStatus(0);
		TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Error occured while gettting customer details");
		
	}

    return null;
    }
    
    
    @RequestMapping(value = "/DownloadRTFTickets")
    public String downloadTickets(HttpServletRequest request,Model model,HttpServletResponse response)  {
    	Downloaddetails downloadDetails = new Downloaddetails();	
    	Error error = new Error();
		 try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("cId");
			String platform = request.getParameter("platForm");
			String encryptedStr = request.getQueryString();
			String clientIPAddress = request.getParameter("cTrack");
			String ipAddress = request.getRemoteAddr();
			
			encryptedStr = encryptedStr.replace("identity=","");
			String keyString = ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey();
			String customerId = "";
			String orderIdStr = "";	
			String decodedText = SecurityUtil.doDecryption(encryptedStr, keyString);
			String origText[] = decodedText.split("_");
			configIdString = origText[0];
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(DAORegistry.getIpConfigDAO().getIpConfigByIpAndConfigId(configIdString,ip)==null){
						error.setDescription("You are not authorized.");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
					
				}
			}else{
				error.setDescription("You are not authorized.");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"You are not authorized");
				
			}
			
			if(encryptedStr != null && !encryptedStr.isEmpty()){
				customerId = origText[1];
				orderIdStr = origText[2];
			}

			Integer orderId = null;
			CustomerOrder order = null;
			Invoice invoice = null;
			try{
				orderId=Integer.parseInt(orderIdStr.trim());
				order = DAORegistry.getCustomerOrderDAO().get(orderId);
				invoice = DAORegistry.getInvoiceDAO().getInvoiceByOrderId(orderId);
			}catch(Exception e){
				error.setDescription("OrderNumber is Mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"OrderNumber is Mandatory");
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customet ID  is mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Customet ID is mandatory");
			}
		
		File file = new File(URLUtil.findETicketDownloadURLFromDirectory("tickets_"+order.getId()+".zip"));
        log.trace("Write response...");
        InputStream fileInputStream = null;
        OutputStream output = null;
        try{
        	fileInputStream = new FileInputStream(file);
        	output = response.getOutputStream(); 

            response.reset();

            response.setContentType("application/octet-stream");
            response.setContentLength((int) (file.length()));
            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

            IOUtils.copyLarge(fileInputStream, output);
            output.flush();
            try{	
            	 CustomerTicketDownloads ticketDownload = new CustomerTicketDownloads();
                 ticketDownload.setCustomerId(Integer.parseInt(customerId));
                 ticketDownload.setInvoiceId(invoice.getId());
                 ticketDownload.setOrderId(null != invoice?invoice.getId():null);
                 ticketDownload.setDownloadDateTime(new Date());
                 ticketDownload.setTicketName(file.getAbsolutePath());
                 ticketDownload.setTicketType("ZIP");
                 ticketDownload.setPlatform(platform);
                 ticketDownload.setIpAddress(null != ipAddress && !ipAddress.isEmpty()?ipAddress:clientIPAddress);
                 DAORegistry.getCustomerTicketDownloadsDAO().save(ticketDownload);
            }catch(Exception e){
            	e.printStackTrace();
            }
        } catch (IOException e) {
        	e.printStackTrace();
            log.error(e.getMessage(), e);
            error.setDescription("Ticket not found for this order");
			downloadDetails.setError(error);
			downloadDetails.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Ticket not found for this order");
			//return downloadDetails;
        }finally{
        	if(null != output){
        		output.close();
        	}
        	if(null != fileInputStream){
        		fileInputStream.close();
        	}
        }

    } catch (Exception e) {
		e.printStackTrace();
		error.setDescription("Error occured while downloading.");
		downloadDetails.setError(error);
		downloadDetails.setStatus(0);
		TrackingUtils.webServiceTracking(request, WebServiceActionType.GETTICKETDOWNLOAD,"Error occured while gettting customer details");
	}
    return null;
    }
    
    
    @RequestMapping(value = "/GetGiftCardFile")
    public String getGiftCardFile(HttpServletRequest request,Model model,HttpServletResponse response)  {
    	Downloaddetails downloadDetails = new Downloaddetails();	
    	Error error = new Error();
		 try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("cId");
			String platform = request.getParameter("platForm");
			String encryptedStr = request.getQueryString();
			String clientIPAddress = request.getParameter("cTrack");
			String ipAddress = request.getRemoteAddr();
			
			encryptedStr = encryptedStr.replace("identity=","");
			String keyString = ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey();
			String customerId = "";
			String orderIdStr = "";	
			String decodedText = SecurityUtil.doDecryption(encryptedStr, keyString);
			String origText[] = decodedText.split("_");
			configIdString = origText[0];
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(DAORegistry.getIpConfigDAO().getIpConfigByIpAndConfigId(configIdString,ip)==null){
						error.setDescription("You are not authorized.");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"You are not authorized");
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"You are not authorized");
					
				}
			}else{
				error.setDescription("You are not authorized.");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"You are not authorized");
				
			}
			String attachmentIdStr = "", giftCardOrderIdStr = "";
			if(encryptedStr != null && !encryptedStr.isEmpty()){
				customerId = origText[1];
				attachmentIdStr = origText[2];
				giftCardOrderIdStr = origText[3];
			}
			//configId+"_"+customerId+"_"+giftCardId+"_"+giftCardOrderId;
			Integer attachmentId = null,giftCardOrderId = Integer.parseInt(giftCardOrderIdStr);
			GiftCardFileAttachment attachment = null;
			try{
				attachmentId=Integer.parseInt(attachmentIdStr.trim());
				attachment = DAORegistry.getGiftCardFileAttachmentDAO().get(attachmentId);
			}catch(Exception e){
				error.setDescription("OrderNumber is Mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"OrderNumber is Mandatory");
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customet ID  is mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"Customet ID is mandatory");
			}
		
			//File file = new File (URLUtil.getGiftCardFile(attachment.getFilePath()));
	        log.trace("Write response...");
	        InputStream fileInputStream = null;
	        OutputStream output = null;
	        try{
	        	
	        	AwsS3Response resp = AWSFileService.downLoadFileFromS3(AWSFileService.BUCKET_NAME_RTFASSESTS, AWSFileService.GIFTCARD_FILES_DIRECTORY, URLUtil.getGiftCardFileName(attachment.getFilePath()), URLUtil.getGiftCardAbsoluteAssetPath("GCOD"));
	        	if(resp == null || resp.getStatus() != 1){
	        		error.setDescription("Gift Card file not found for this order.");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"Gift Card file not found for this order.");
	        	}
	        	
	        	File file = new File (resp.getFullFilePath());
	        	
	        	fileInputStream = new FileInputStream(file);
	        	output = response.getOutputStream(); 
	            response.reset();
	            response.setContentType("application/octet-stream");
	            response.setContentLength((int) (file.length()));
	            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

	            IOUtils.copyLarge(fileInputStream, output);
	            output.flush();
	            try{	
	            	 CustomerTicketDownloads ticketDownload = new CustomerTicketDownloads();
	                 ticketDownload.setCustomerId(Integer.parseInt(customerId));
	                 ticketDownload.setInvoiceId(-1);
	                 ticketDownload.setOrderId(giftCardOrderId);
	                 ticketDownload.setDownloadDateTime(new Date());
	                 ticketDownload.setTicketName(attachment.getFilePath());
	                 ticketDownload.setTicketType(attachment.getFileTypeId().toString());
	                 ticketDownload.setPlatform(platform);
	                 ticketDownload.setIpAddress(null != clientIPAddress && !clientIPAddress.isEmpty()?clientIPAddress:ip);
	                 DAORegistry.getCustomerTicketDownloadsDAO().save(ticketDownload);
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
	        } catch (IOException e) {
	        	e.printStackTrace();
	            log.error(e.getMessage(), e);
	            error.setDescription("Gift Card not found for this order");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"Gift Card not found for this order");
				//return downloadDetails;
	        }finally{
	        	if(null != output){
	        		output.close();
	        	}
	        	if(null != fileInputStream){
	        		fileInputStream.close();
	        	}
	        }

    } catch (Exception e) {
		e.printStackTrace();
		error.setDescription("Error while downloading gift card order");
		downloadDetails.setError(error);
		downloadDetails.setStatus(0);
		TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"Error while downloading gift card order");
		
	}
    return null;
    }
    
    
    
    
    @RequestMapping(value = "/GetProductFile")
    public String getProductFile(HttpServletRequest request,Model model,HttpServletResponse response)  {
    	Downloaddetails downloadDetails = new Downloaddetails();	
    	Error error = new Error();
		 try {
			HttpSession session = request.getSession();
			String ip =(String)session.getAttribute("ip");
			String configIdString = request.getParameter("cId");
			String platform = request.getParameter("platForm");
			String encryptedStr = request.getQueryString();
			String clientIPAddress = request.getParameter("cTrack");
			String ipAddress = request.getRemoteAddr();
			
			encryptedStr = encryptedStr.replace("identity=","");
			String keyString = ApiConfigUtil.getWebServiceConfigById(configIdString).getSecureKey();
			String customerId = "";
			String orderIdStr = "";	
			String decodedText = SecurityUtil.doDecryption(encryptedStr, keyString);
			String origText[] = decodedText.split("_");
			configIdString = origText[0];
			
			if(configIdString!=null && !configIdString.isEmpty()){
				try {
					if(DAORegistry.getIpConfigDAO().getIpConfigByIpAndConfigId(configIdString,ip)==null){
						error.setDescription("You are not authorized.");
						downloadDetails.setError(error);
						downloadDetails.setStatus(0);
						TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"You are not authorized");
					}
				} catch (Exception e) {
					error.setDescription("You are not authorized.");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"You are not authorized");
					
				}
			}else{
				error.setDescription("You are not authorized.");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"You are not authorized");
				
			}
			String prodIdStr = "";
			if(encryptedStr != null && !encryptedStr.isEmpty()){
				customerId = origText[1];
				prodIdStr = origText[2];
				orderIdStr = origText[3];
			}
			//configId+"_"+customerId+"_"+giftCardId+"_"+giftCardOrderId;
			Integer prodId = null,orderId = Integer.parseInt(orderIdStr);
			OrderProducts prod = null;
			try{
				prodId=Integer.parseInt(prodIdStr.trim());
				prod = EcommDAORegistry.getOrderProductSetDAO().getOrderProductByOrderAndProductId(orderId, prodId);
			}catch(Exception e){
				error.setDescription("OrderNumber is Mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"OrderNumber is Mandatory");
			}
			if(TextUtil.isEmptyOrNull(customerId)){
				error.setDescription("Customet ID  is mandatory");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"Customet ID is mandatory");
			}
		
			//File file = new File (URLUtil.getProductFile(prod.getDelFileName()));
	        log.trace("Write response...");
	        InputStream fileInputStream = null;
	        OutputStream output = null;
	        try{
	        	
	        	AwsS3Response resp = AWSFileService.downLoadFileFromS3(AWSFileService.BUCKET_NAME_RTFASSESTS, AWSFileService.RTFPROD_FOLDER, prod.getDelFileName(),  URLUtil.getGiftCardAbsoluteAssetPath("PGCOD"));
	        	if(resp == null || resp.getStatus() != 1){
	        		error.setDescription("File not found for this order.");
					downloadDetails.setError(error);
					downloadDetails.setStatus(0);
					TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"File not found for this order.");
	        	}
	        	
	        	File file = new File (resp.getFullFilePath());
	        	
	        	fileInputStream = new FileInputStream(file);
	        	output = response.getOutputStream(); 
	            response.reset();
	            response.setContentType("application/octet-stream");
	            response.setContentLength((int) (file.length()));
	            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

	            IOUtils.copyLarge(fileInputStream, output);
	            output.flush();
	            try{	
	            	 CustomerTicketDownloads ticketDownload = new CustomerTicketDownloads();
	                 ticketDownload.setCustomerId(Integer.parseInt(customerId));
	                 ticketDownload.setInvoiceId(-1);
	                 ticketDownload.setOrderId(orderId);
	                 ticketDownload.setDownloadDateTime(new Date());
	                 ticketDownload.setTicketName(prod.getDelFileName());
	                 ticketDownload.setTicketType("PRODORDER");
	                 ticketDownload.setPlatform(platform);
	                 ticketDownload.setIpAddress(null != clientIPAddress && !clientIPAddress.isEmpty()?clientIPAddress:ip);
	                 DAORegistry.getCustomerTicketDownloadsDAO().save(ticketDownload);
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
	        } catch (IOException e) {
	        	e.printStackTrace();
	            log.error(e.getMessage(), e);
	            error.setDescription("File not found for this order");
				downloadDetails.setError(error);
				downloadDetails.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"File not found for this order");
				//return downloadDetails;
	        }finally{
	        	if(null != output){
	        		output.close();
	        	}
	        	if(null != fileInputStream){
	        		fileInputStream.close();
	        	}
	        }

    } catch (Exception e) {
		e.printStackTrace();
		error.setDescription("Error while downloading gift card order");
		downloadDetails.setError(error);
		downloadDetails.setStatus(0);
		TrackingUtils.webServiceTracking(request, WebServiceActionType.GETGIFTCARDFILE,"Error while downloading gift card order");
		
	}
    return null;
    }
    
    
    
    
    
    @RequestMapping(value="/HitApi")
	public @ResponsePayload Downloaddetails hitApi(HttpServletRequest request,Model model,HttpServletResponse response){
    	Downloaddetails obj = new Downloaddetails();	
    	try {
			CustomerGiftCardOrderUtil util = new CustomerGiftCardOrderUtil();
			util.init();
			obj.setStatus(1);
			return obj;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
    
    
    @RequestMapping(value="/ContactUs",method=RequestMethod.POST)
	public @ResponsePayload EmailResponse emailCustomerRequestInfo(HttpServletRequest request, Model model,HttpServletResponse response){
		EmailResponse emailResponse =new EmailResponse();
		Error error = new Error();
		try {
			HttpSession session = request.getSession();
			
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String email = request.getParameter("email");
			String phoneNumber = request.getParameter("phoneNumber");
			String subject = request.getParameter("subject");
			String message = request.getParameter("message");
			 
			
			if(TextUtil.isEmptyOrNull(firstName)){
				error.setDescription("Please enter your First Name");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Please enter your First Name");
				return emailResponse;
			}
			
			if(TextUtil.isEmptyOrNull(lastName)){
				lastName ="";
			}
			
			if(TextUtil.isEmptyOrNull(email)){
				error.setDescription("Please enter your Email");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Please enter your Email");
				return emailResponse;
			}
			
			if(TextUtil.isEmptyOrNull(phoneNumber)){
				error.setDescription("Please enter your Phone Number");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Please enter your Phone Number");
				return emailResponse;
			}
			
			if(TextUtil.isEmptyOrNull(subject)){
				
				subject = "Customer Request";
				
				/*error.setDescription("Subject is empty or null.");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Subject is empty or null");
				return emailResponse;*/
			}
			
			if(TextUtil.isEmptyOrNull(message)){
				error.setDescription("Message is mandatory");
				emailResponse.setError(error);
				emailResponse.setStatus(0);
				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Message is empty or null");
				return emailResponse;
			}
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("fullName", firstName+" "+lastName);
			mailMap.put("phoneNumber", phoneNumber);
			mailMap.put("email", email);
			mailMap.put("subject", subject);
			mailMap.put("message", message);
			
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
			try{
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, "sales@rewardthefan.com", 
						null,URLUtil.bccEmails, subject+" [From : "+firstName+" "+lastName+" ]",
			    		"email-customer-contact-request.html", mailMap, "text/html", null,mailAttachment,null);
				}catch(Exception e){
				e.printStackTrace();
			}
			emailResponse.setMessage("Thanks for your request");
			emailResponse.setStatus(1);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Success");
			return emailResponse;
		} catch (Exception e) {
			e.printStackTrace();
			error.setDescription("Error occured while Emailing Customer Request details.");
			emailResponse.setError(error);
			emailResponse.setStatus(0);
			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Error occured while Emailing Customer Request details.");
			return emailResponse;
		}
	}
    
    @RequestMapping(value="/website/contactus",method=RequestMethod.POST)
   	public @ResponsePayload EmailResponse contactUs(HttpServletRequest request, Model model,HttpServletResponse response){
   		EmailResponse emailResponse =new EmailResponse();
   		Error error = new Error();
   		try {
   			HttpSession session = request.getSession();
   			
   			String firstName = request.getParameter("firstName");
   			String lastName = request.getParameter("lastName");
   			String email = request.getParameter("email");
   			String phoneNumber = request.getParameter("phoneNumber");
   			String subject = request.getParameter("subject");
   			String message = request.getParameter("message"); 
   			String requestType = request.getParameter("requestType"); 
   			
   			if(TextUtil.isEmptyOrNull(firstName)){
   				error.setDescription("Please enter your First Name");
   				emailResponse.setError(error);
   				emailResponse.setStatus(0);
   				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Please enter your First Name");
   				return emailResponse;
   			}
   			
   			if(TextUtil.isEmptyOrNull(lastName)){
   				error.setDescription("Please enter your Last Name");
   				emailResponse.setError(error);
   				emailResponse.setStatus(0);
   				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Please enter your Last Name");
   				return emailResponse;
   			}
   			
   			if(TextUtil.isEmptyOrNull(email)){
   				error.setDescription("Please enter your Email");
   				emailResponse.setError(error);
   				emailResponse.setStatus(0);
   				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Please enter your Email");
   				return emailResponse;
   			}
   			
   			if(TextUtil.isEmptyOrNull(phoneNumber)){
   				error.setDescription("Please enter your Phone Number");
   				emailResponse.setError(error);
   				emailResponse.setStatus(0);
   				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Please enter your Phone Number");
   				return emailResponse;
   			}
   			
   			if(TextUtil.isEmptyOrNull(subject)){
   				subject = "Customer Request"; 
   			}
   			
   			if(TextUtil.isEmptyOrNull(message)){
   				error.setDescription("Message is mandatory");
   				emailResponse.setError(error);
   				emailResponse.setStatus(0);
   				TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Message is empty or null");
   				return emailResponse;
   			}
   			
   			Map<String,Object> mailMap = new HashMap<String,Object>();
   			mailMap.put("fullName", firstName+" "+lastName);
   			mailMap.put("phoneNumber", phoneNumber);
   			mailMap.put("email", email);
   			mailMap.put("subject", subject);
   			mailMap.put("message", message);
   			
   			
   			//inline(image in mail body) image add code
   			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
   			try{
   				mailManager.sendMailNow("text/html",URLUtil.fromEmail, "sales@rewardthefan.com", 
   						null,URLUtil.bccEmails, subject+" [From : "+firstName+" "+lastName+" ]",
   			    		"email-customer-contact-request.html", mailMap, "text/html", null,mailAttachment,null);
   				}catch(Exception e){
   				e.printStackTrace();
   			}
   			emailResponse.setMessage("Thanks for your request");
   			emailResponse.setStatus(1);
   			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Success");
   			return emailResponse;
   		} catch (Exception e) {
   			e.printStackTrace();
   			error.setDescription("Error occured while Emailing Customer Request details.");
   			emailResponse.setError(error);
   			emailResponse.setStatus(0);
   			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Error occured while Emailing Customer Request details.");
   			return emailResponse;
   		}
   	}
    
    @RequestMapping(value="/subscription",method=RequestMethod.POST)
   	public @ResponsePayload EmailResponse subscription(HttpServletRequest request, Model model,HttpServletResponse response){
   		EmailResponse emailResponse =new EmailResponse();
   		Error error = new Error();
   		try {
   			HttpSession session = request.getSession(); 
   			String email = request.getParameter("email"); 
   			
   			Map<String,Object> mailMap = new HashMap<String,Object>(); 
   			mailMap.put("email", email); 
   			try{
   				MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
   				mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
					null,URLUtil.bccEmails, "Thanks for subscribing RewardTheFan.com.",
		    		"email-customer-subscription.html", mailMap, "text/html", null,mailAttachment,null);
   			}catch(Exception e){
   				e.printStackTrace();
   			}
   			emailResponse.setMessage("Thanks for subscribing.");
   			emailResponse.setStatus(1);
   			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Success");
   			return emailResponse;
   		} catch (Exception e) {
   			e.printStackTrace();
   			error.setDescription("Error occured in subscription api.");
   			emailResponse.setError(error);
   			emailResponse.setStatus(0);
   			TrackingUtils.webServiceTracking(request, WebServiceActionType.EMAILCUSTOMERREQUEST,"Error occured in subscription api");
   			return emailResponse;
   		}
   	}
    
     
	
}	