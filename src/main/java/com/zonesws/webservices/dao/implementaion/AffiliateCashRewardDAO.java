package com.zonesws.webservices.dao.implementaion;

import java.util.Date;

import com.zonesws.webservices.data.AffiliateCashReward;

/**
 * class having db related methods for AffiliateCashReward
 * @author Ulaganathan
 *
 */
public class AffiliateCashRewardDAO extends HibernateDAO<Integer, AffiliateCashReward> implements com.zonesws.webservices.dao.services.AffiliateCashRewardDAO{
	
	
	public AffiliateCashReward getAffiliateByUserId(Integer userId){
		return findSingle("from AffiliateCashReward where userId=? ", new Object[]{userId});
	}
	
	public void updateAffiliateCashByCustomer(Double cash,  Integer userId) {
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update AffiliateCashReward set totalCreditedCash=totalCreditedCash + ?," +
					"lastCreditedCash = ?, activeCash= activeCash + ? , pendingCash = pendingCash - ?, lastUpdate=? where userId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{cash, cash, cash,cash,new Date(), userId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId){
		return findSingle("from CustomerLoyalty where customerId=? ", new Object[]{customerId});
	}
	
	public void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set activePointsAsDouble= ?, latestSpentPointsAsDouble=?, totalSpentPointsAsDouble = ? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{pointsRemaining, latestSpendPoints, totalSpendPoints, customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints(){
		return find("from CustomerLoyalty where activePointsAsDouble > 0 order by  customerId");
	}
	
	*/

}
