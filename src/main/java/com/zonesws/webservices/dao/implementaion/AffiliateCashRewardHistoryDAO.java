package com.zonesws.webservices.dao.implementaion;

import java.util.Date;

import com.zonesws.webservices.data.AffiliateCashRewardHistory;
import com.zonesws.webservices.enums.RewardStatus;

/**
 * class having db related methods for AffiliateCashRewardHistory
 * @author Ulaganathan
 *
 */
public class AffiliateCashRewardHistoryDAO extends HibernateDAO<Integer, AffiliateCashRewardHistory> implements com.zonesws.webservices.dao.services.AffiliateCashRewardHistoryDAO{
	
	public void updateCashRewardHistoryToActive(Integer id/*,boolean isEmailSent,String description*/) throws Exception{
		 /*bulkUpdate("UPDATE AffiliateCashRewardHistory set rewardStatus=?,updatedDate=?," +
		 		"isRewardsEmailSent=?,emailDescription=? WHERE rewardStatus=? and id=?", 
				 new Object[]{RewardStatus.ACTIVE,new Date(),isEmailSent,description,RewardStatus.PENDING,id});*/
		bulkUpdate("UPDATE AffiliateCashRewardHistory set rewardStatus=?,updatedDate=?," +
		 		"isRewardsEmailSent=?,emailDescription=? WHERE rewardStatus=? and id=?", 
				 new Object[]{RewardStatus.ACTIVE,new Date(),false,"",RewardStatus.PENDING,id});
	}

}
