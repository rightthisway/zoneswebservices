package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.AffiliatePromoCodeTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * class having db related methods for AffiliatePromoCodeTracking
 * @author Ulaganathan
 *
 */
public class AffiliatePromoCodeTrackingDAO extends HibernateDAO<Integer, AffiliatePromoCodeTracking> implements com.zonesws.webservices.dao.services.AffiliatePromoCodeTrackingDAO{
	
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer userId,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId,String promoCode,Boolean isLongTicket){
		return findSingle("from AffiliatePromoCodeTracking where userId=? and customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? and promoCode=? " +
				"and status=?  and isLongTicket=?", new Object[]{userId,customerId,eventId,ticketGroupId,sessionId,platForm,promoCode,"PENDING",isLongTicket});
	}
	
	public AffiliatePromoCodeTracking getPromoTrackingByTrackingId(ApplicationPlatForm platForm,Integer userId,Integer customerId, 
			String sessionId, Integer trackingId){
		return findSingle("from AffiliatePromoCodeTracking where userId=? and customerId=? " +
				"and id=? and sessionId=? and status=?", new Object[]{userId,customerId,trackingId,sessionId,"PENDING"});
	}
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId){
		return findSingle("from AffiliatePromoCodeTracking where customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? " +
				"and status=?", new Object[]{customerId,eventId,ticketGroupId,sessionId,platForm,"PENDING"});
	}
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer userId,Integer customerId, String sessionId,
			String promoCode,String ipAddress){
		return findSingle("from AffiliatePromoCodeTracking where userId=? and customerId=? " +
				"and sessionId=? and platForm=? and promoCode=? and status=? and ipAddress=?", 
				new Object[]{userId,customerId,sessionId,platForm,promoCode,"PENDING",ipAddress});
	}
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,String ipAddress){
		return findSingle("from AffiliatePromoCodeTracking where customerId=? " +
				"and sessionId=? and platForm=? and status=? and ipAddress=?", 
				new Object[]{customerId,sessionId,platForm,"PENDING",ipAddress});
	}
	
	/*public AffiliateCashReward getAffiliateByUserId(Integer userId){
		return findSingle("from AffiliateCashReward where userId=? ", new Object[]{userId});
	}*/
	
	/*public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId){
		return findSingle("from CustomerLoyalty where customerId=? ", new Object[]{customerId});
	}
	
	public void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set activePointsAsDouble= ?, latestSpentPointsAsDouble=?, totalSpentPointsAsDouble = ? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{pointsRemaining, latestSpendPoints, totalSpendPoints, customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints(){
		return find("from CustomerLoyalty where activePointsAsDouble > 0 order by  customerId");
	}
	
	public void updateCustomerLoyaltyByCustomer(Double earnPoints,  Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set totalEarnedPointsAsDouble=totalEarnedPointsAsDouble + ?," +
					"latestEarnedPointsAsDouble = ?, activePointsAsDouble= activePointsAsDouble + ? , pendingPointsAsDouble = pendingPointsAsDouble - ?, lastUpdate=? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{earnPoints, earnPoints, earnPoints,earnPoints,new Date(), customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

}
