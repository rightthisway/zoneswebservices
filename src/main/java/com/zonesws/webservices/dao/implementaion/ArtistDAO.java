package com.zonesws.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.enums.ArtistStatus;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.utils.list.AutoPageResult;

public class ArtistDAO extends HibernateDAO<Integer, Artist> implements com.zonesws.webservices.dao.services.ArtistDAO{

	private static Session artistSession = null;
	
	public static Session getArtistSession() {
		return artistSession;
	}

	public static void setArtistSession(Session artistSession) {
		ArtistDAO.artistSession = artistSession;
	}

	public  List<Artist> getAllArtistBySearchKeyAndPageNumber(String searchKey,Integer pageNumber,Integer maxRows,
		Map<Integer, Boolean> favArtistMap,Integer superFanArtistId) {
		
		
	 String sql ="select distinct a.* from artist a WITH(NOLOCK) inner join events_rtf ea WITH(NOLOCK) on " +
	 		"ea.artist_id=a.id where a.name like '%"+searchKey+"%' and a.artist_status=1 and a.display_on_search=1 " +
	 		"order by a.name  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
		
		List<Artist> artists =new ArrayList<Artist>();
		Query query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			Artist artist = null;
			for (Object[] object : result) {
				artist= new Artist();
				Integer artistId = (Integer)object[0] ;
				Boolean isCustFavFlag = favArtistMap.get(artistId);
				if(null != isCustFavFlag && isCustFavFlag){
					artist.setCustFavFlag(true);
				}else{
					artist.setCustFavFlag(false);
				}
				if(artistId.equals(superFanArtistId) || superFanArtistId == artistId){
					artist.setCustSuperFanFlag(true);
				}
				artist.setId(artistId);
				artist.setName((String)object[2]);
				artists.add(artist);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return artists;
	}
	
	
	public List<Artist> getAllSportsArtistByKey(String searchKey,Integer pageNumber,Integer maxRows) {
			
			
		 String sql ="select distinct a.* from artist a WITH(NOLOCK) inner join events_rtf ea WITH(NOLOCK) on " +
		 		"ea.artist_id=a.id where a.name like '%"+searchKey+"%' and a.artist_status=1 and a.display_on_search=1 " +
		 		" and ea.parent_category_id = 1 order by a.name  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
			
			List<Artist> artists =new ArrayList<Artist>();
			Query query = null;
			Session session = null;
			try {
				session = getSessionFactory().openSession();
				query = session.createSQLQuery(sql);
				
				List<Object[]> result = (List<Object[]>)query.list();
				
				Artist artist = null;
				for (Object[] object : result) {
					artist= new Artist();
					Integer artistId = (Integer)object[0] ;
					artist.setId(artistId);
					artist.setName((String)object[2]);
					artists.add(artist);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally  {
				session.close();
			}
			return artists;
		}
 
 
 
 public  List<Artist> getAllArtistBySearchKey(String searchKey,Integer pageNumber,Integer maxRows,
			Map<Integer, Boolean> favArtistMap,Integer childId,Integer grandChildId,Integer parentCategoryId,String grandChildCateoryIds,String childCateoryIds) {
			
	 		
		 	StringBuilder queryString = new StringBuilder();
			queryString.append("DECLARE @searchinput varchar(50),@svlikestart varchar(60), @svlikemid varchar(60),@svlikelast varchar(60),");
			queryString.append("@svnotlikestart varchar(60), @svnotlikemid varchar(60),@svnotlikelast varchar(60);");
			queryString.append("SET @searchinput  = '"+searchKey+"';SET  @svlikestart = '%[ ]' + @searchinput + '%';SET  @svlikemid = '%' + @searchinput + '[ ]%';SET  @svlikelast = '%[ ]' + @searchinput + '[ ]%';");
			queryString.append("SET  @svnotlikestart = '%[^ ]' + @searchinput + '%';SET  @svnotlikemid = '%' + @searchinput + '[^ ]%';SET  @svnotlikelast = '%[^ ]' + @searchinput + '[^ ]%';");
			
			queryString.append("select B.id, B.name,count(1) over () totalRows from (select distinct 0 as sort_col, A.id,A.name from ( ");
			queryString.append(" select distinct a.* from artist a WITH(NOLOCK) inner join events_rtf e WITH(NOLOCK) on a.id=e.artist_id ");
			queryString.append(" inner join artist_category_mapping acm WITH(NOLOCK) on acm.artist_id=a.id");
			queryString.append(" where a.artist_status=1 and a.display_on_search=1 ");
			
			/* 
					  
					
					 and acm.parent_category_id =2 )A  ) 
					B order by B.name OFFSET (1-1)*10000 ROWS FETCH NEXT 10000 ROWS ONLY
					*/
			
			/*queryString.append("select B.id, B.name,count(1) over () totalRows from (select distinct 0 as sort_col, A.id,A.name from (");
			queryString.append(" select distinct a.*, zp.minPrice from artist a inner join event_details e on a.id=e.artist_id and e.reward_thefan_enabled=1");
			queryString.append(" CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK)");
			queryString.append(" where zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp");
			queryString.append(" where e.status=1 and e.event_date>GETDATE() and a.artist_status=1");*/
			
			if(childId !=null && !childId.equals("") && childId >0){
	 			queryString.append(" and acm.child_category_id ="+childId);
			}else if(null != grandChildId && !grandChildId.equals("") && grandChildId > 0){
				queryString.append(" and acm.grand_child_category_id ="+grandChildId);
			}else if(childCateoryIds !=null && !childCateoryIds.isEmpty()){
	 			queryString.append(" and acm.child_category_id in ("+childCateoryIds+")");
			}else if(null != grandChildCateoryIds && !grandChildCateoryIds.isEmpty()){
				queryString.append(" and acm.grand_child_category_id in ("+grandChildCateoryIds+")");
			}else if(null != parentCategoryId && !parentCategoryId.equals("") && parentCategoryId > 0){
				queryString.append(" and acm.parent_category_id ="+parentCategoryId);
			}else if(null != searchKey && !searchKey.isEmpty()){
				queryString.append(" and (a.name = @searchinput or a.name  like @svlikestart  or a.name  like @svlikemid or a.name  like @svlikelast ) ) A where A.minPrice is not null");
				queryString.append(" UNION ALL");
				queryString.append(" select distinct 0 as sort_col, A.id,A.name from (select distinct a.*, zp.minPrice");
				queryString.append(" from artist a WITH(NOLOCK) inner join events_rtf e WITH(NOLOCK) on a.id=e.artist_id ");
				queryString.append(" inner join artist_category_mapping acm WITH(NOLOCK) on acm.artist_id=a.id ");
				queryString.append(" where a.artist_status=1 and a.display_on_search=1 and ( a.name  like @svnotlikestart  or a.name  like @svnotlikemid or a.name  like @svnotlikelast )");
			}
			
			queryString.append(" ) A  ) B order by B.name OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY");//where A.minPrice is not null
			
	 
	 		/*StringBuilder queryString = new StringBuilder();
	 		
	 		queryString.append("DECLARE @searchinput varchar(50), @svword varchar(60), @svlikestart varchar(60), @svlikemid varchar(60),@svlikelast varchar(60);");
	 		queryString.append("SET @searchinput  = 'Once';SET  @svword = '''' + @searchinput + '''';SET  @svlikestart = '%[^ ]' + @searchinput + '%';");
	 		queryString.append("SET  @svlikemid = '%' + @searchinput + '[^ ]%';SET  @svlikelast = '%[^ ]' + @searchinput + '[^ ]%';");
	 		queryString.append("select distinct A.id,A.name from (select distinct a.*, zp.minPrice from artist a inner join event_details e on a.id=e.artist_id ");
	 		queryString.append("and e.reward_thefan_enabled=1 CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) ");
	 		queryString.append("where zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp ");
	 		queryString.append("where e.status=1 and e.event_date>GETDATE() and a.artist_status=1  and contains(a.name,@svword) ");
	 		queryString.append(") A where A.minPrice is not null ");
	 		queryString.append("UNION ALL ");
	 		queryString.append("select distinct A.id,A.name from (select distinct a.*, zp.minPrice from artist a inner join event_details e on a.id=e.artist_id ");
	 		queryString.append("and e.reward_thefan_enabled=1 CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) ");
	 		queryString.append("where zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp ");
	 		queryString.append("where e.status=1 and e.event_date>GETDATE() and a.artist_status=1 ");
	 		
	 		if(childId !=null && !childId.equals("") && childId >0){
	 			queryString.append(" and e.child_category_id ="+childId);
			}else if(null != grandChildId && !grandChildId.equals("") && grandChildId > 0){
				queryString.append(" and e.grand_child_category_id ="+grandChildId);
			}else if(null != parentCategoryId && !parentCategoryId.equals("") && parentCategoryId > 0){
				queryString.append(" and e.parent_category_id ="+parentCategoryId);
			}else{
				queryString.append(" and ( a.name  like @svlikestart  or a.name  like @svlikemid or a.name  like @svlikelast ) ) ");
			}
	 		
	 		queryString.append("A where A.minPrice is not null");*/
	 		
			/*String sql ="select B.*,count(1) over () totalRows from (select distinct A.id,A.name from (select distinct a.*,zp.minPrice  " +
		 		"from artist a inner join event_details e on a.id=e.artist_id and e.reward_thefan_enabled=1 CROSS APPLY " +
		 		"( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) where " +
		 		"zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp " +
		 		"where e.status=1 and e.event_date>GETDATE() and a.artist_status=1 ";*/
			 
			/*if(childId !=null && !childId.equals("") && childId >0){
				sql += " and e.child_category_id ="+childId;
			}else if(null != grandChildId && !grandChildId.equals("") && grandChildId > 0){
				sql += " and e.grand_child_category_id ="+grandChildId;
			}else if(null != parentCategoryId && !parentCategoryId.equals("") && parentCategoryId > 0){
				sql += " and e.parent_category_id ="+parentCategoryId;
			}else{
				sql += " and (a.name like '%"+searchKey+"%' ";
				//sql += " e.child_category_name like '%"+searchKey+"%' OR ";
				//sql += " e.grand_child_category_name like '%"+searchKey+"%' ";
				sql += " ) ";
			}
			sql = sql+" ) A where A.minPrice is not null ) B order by B.name OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";*/
					
			List<Artist> artists =new ArrayList<Artist>();
			try {
				
				artistSession = ArtistDAO.getArtistSession();
				if(artistSession==null || !artistSession.isOpen() || !artistSession.isConnected()){
					artistSession = getSession();
					ArtistDAO.setArtistSession(artistSession);
				}
				
				Query query = artistSession.createSQLQuery(queryString.toString());
				List<Object[]> result = (List<Object[]>)query.list();
				//Map<String, Boolean> uniqueMap = new HashMap<String, Boolean>();
				Artist artist = null;
				for (Object[] object : result) {
					artist= new Artist();
					Integer artistId = (Integer)object[0] ;
					Boolean isCustFavFlag = favArtistMap.get(artistId);
					if(null != isCustFavFlag && isCustFavFlag){
						artist.setCustFavFlag(true);
					}else{
						artist.setCustFavFlag(false);
					}
					artist.setId(artistId);
					artist.setName((String)object[1]);
					artist.setTotalArtists((Integer)object[2]);
					
					//String text = artist.getName().replaceFirst("^The ", "").replaceFirst("^A ", "");
					//if(uniqueMap.get(text)!=null) {
					//	continue;
					//}
					//artist.setName(text);
					//uniqueMap.put(text, true);
					artists.add(artist);
				}
				
			} catch (Exception e) {
				if(artistSession != null && artistSession.isOpen()){
					artistSession.close();
				}
				e.printStackTrace();
			}
			return artists;
		}
 
 
 /*public  List<AutoPageResult> getAllArtistAndVenueBySearchKey(String searchKey,Integer pageNumber,Integer maxRows,
			Map<Integer, Boolean> favArtistMap) {
			
	 		StringBuilder queryString = new StringBuilder();
	 		queryString.append("DECLARE @searchinput varchar(50),@svlikestart varchar(60), @svlikemid varchar(60),@svlikelast varchar(60),");
	 		queryString.append("@svnotlikestart varchar(60), @svnotlikemid varchar(60),@svnotlikelast varchar(60);SET  @searchinput  = '"+searchKey+"';");
	 		queryString.append("SET  @svlikestart = '%[ ]' + @searchinput + '%';SET  @svlikemid = '%' + @searchinput + '[ ]%';SET  @svlikelast = '%[ ]' + @searchinput + '[ ]%';");
	 		queryString.append("SET  @svnotlikestart = '%[^ ]' + @searchinput + '%';SET  @svnotlikemid = '%' + @searchinput + '[^ ]%';SET  @svnotlikelast = '%[^ ]' + @searchinput + '[^ ]%';");
	 		queryString.append("select B.id,B.name ,B.type ,count(1) over () totalRows from (select distinct 0 as sort_col,A.id,A.name ,A.type");
	 		queryString.append(" from (select distinct 0 as sort_col,e.artist_id as id , e.artist_name as name, 'ARTIST' as type from event_details e WITH(NOLOCK) where");
	 		
	 		if(null != searchKey && !searchKey.isEmpty()){
				queryString.append("  (e.artist_name = @searchinput or e.artist_name like @svlikestart or e.artist_name like @svlikemid or e.artist_name like @svlikelast)");
			}
	 		
	 		queryString.append(" union all");
	 		queryString.append(" select distinct 0 as sort_col,e.venue_id as id , e.building as name, 'VENUE' as type from event_details e WITH(NOLOCK) where");
	 			 		
	 		if(childCategoryIds !=null && !childCategoryIds.isEmpty()){
				queryString.append(" e.child_category_id in ("+childCategoryIds+")");
			}else if(grandChildCategoryIds !=null && !grandChildCategoryIds.isEmpty()){
				queryString.append(" e.grand_child_category_id in ("+grandChildCategoryIds+")");
			}else if(null != searchKey && !searchKey.isEmpty()){
				queryString.append(" (e.building = @searchinput or e.building like @svlikestart or e.building like @svlikemid or e.building like @svlikelast)");
			}
	 		
	 		queryString.append(" union all");
	 		queryString.append(" select distinct 0 as sort_col,e.artist_id as id , e.artist_name as name, 'ARTIST' as type from event_details e WITH(NOLOCK) where");
	 		
	 		if(childCategoryIds !=null && !childCategoryIds.isEmpty()){
				queryString.append(" e.child_category_id in ("+childCategoryIds+")");
			}else if(grandChildCategoryIds !=null && !grandChildCategoryIds.isEmpty()){
				queryString.append(" e.grand_child_category_id in ("+grandChildCategoryIds+")");
			}else if(null != searchKey && !searchKey.isEmpty()){
				queryString.append(" ( e.artist_name like @svnotlikestart or e.artist_name like @svnotlikemid or e.artist_name like @svnotlikelast)");
			}
	 		
	 		queryString.append(" union all");
	 		queryString.append(" select distinct 0 as sort_col,e.venue_id as id , e.building as name, 'VENUE' as type from event_details e WITH(NOLOCK) where");
	 		
	 		if(childCategoryIds !=null && !childCategoryIds.isEmpty()){
				queryString.append(" e.child_category_id in ("+childCategoryIds+")");
			}else if(grandChildCategoryIds !=null && !grandChildCategoryIds.isEmpty()){
				queryString.append(" e.grand_child_category_id in ("+grandChildCategoryIds+")");
			}else if(null != searchKey && !searchKey.isEmpty()){
				queryString.append(" ( e.building like @svnotlikestart or e.building like @svnotlikemid or e.building like @svnotlikelast)");
			}
	 		
	 		queryString.append(" ) A ) B order by B.name OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY");
			List<AutoPageResult> artists =new ArrayList<AutoPageResult>();
			try {
				
				artistSession = ArtistDAO.getArtistSession();
				if(artistSession==null || !artistSession.isOpen() || !artistSession.isConnected()){
					artistSession = getSession();
					ArtistDAO.setArtistSession(artistSession);
				}
				
				Query query = artistSession.createSQLQuery(queryString.toString());
				List<Object[]> result = (List<Object[]>)query.list();
				AutoPageResult artist = null;
				//Map<String, Boolean> uniqueMap = new HashMap<String, Boolean>();
				for (Object[] object : result) {
					artist= new AutoPageResult();
					Integer artistId = (Integer)object[0] ;
					Boolean isCustFavFlag = favArtistMap.get(artistId);
					if(null != isCustFavFlag && isCustFavFlag){
						artist.setIsFavoriteFlag(true);
					}else{
						artist.setIsFavoriteFlag(false);
					}
					artist.setId(artistId);
					artist.setName((String)object[1]);
					String resultType = (String)object[2];
					artist.setIsVenue(resultType.equals("VENUE")?true:false);
					artist.setIsArtist(resultType.equals("ARTIST")?true:false);
					artist.setTotalRows((Integer)object[3]);
					
					//if(artist.getIsArtist()) {
					//	String text = artist.getName().replaceFirst("^The ", "").replaceFirst("^A ", "");
					//	if(uniqueMap.get(text)!=null) {
					//		continue;
					//	}
					//	artist.setName(text);
					//	uniqueMap.put(text, true);
					//}
					artists.add(artist);
				}
								
			} catch (Exception e) {
				if(artistSession != null && artistSession.isOpen()){
					artistSession.close();
				}
				e.printStackTrace();
			}
			return artists;
		}
 */
 
 
 
 /*public  List<Artist> getAllArtistBySearchKeyAndPageNumber(String searchKey,Integer pageNumber,Integer maxRows) {
			
			
			String sql ="select distinct a.* from artist a inner join event_details e on a.id=e.artist_id " +
					"and e.reward_thefan_enabled=1 CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice " +
					"from category_ticket_group zt WITH(NOLOCK) where  zt.event_id=e.event_id and zt.status='ACTIVE' " +
					"and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and e.event_date>GETDATE() " +
					"and a.name like '%"+searchKey+"%' and a.artist_status=1 ";			
			
			sql = sql+" order by a.name  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
			
			List<Artist> artists =new ArrayList<Artist>();
			Query query = null;
			Session session = null;
			try {
				session = getSessionFactory().openSession();
				query = session.createSQLQuery(sql);
				List<Object[]> result = (List<Object[]>)query.list();
				Artist artist = null;
				for (Object[] object : result) {
					artist= new Artist();
					Integer artistId = (Integer)object[0] ;
					artist.setId(artistId);
					artist.setName((String)object[1]);
					artists.add(artist);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally  {
				session.close();
			}
			return artists;
		}*/

public  List<Artist> getAllArtistByGrandChildCategoryAndPageNumber(Integer grandChildCategoryId,Integer pageNumber,Integer maxRows,ProductType productType,
		Map<Integer, Boolean> favArtistMap,Integer superFanArtistId) {
		
		
		String sql ="select distinct a.id,a.name FROM artist a WITH(NOLOCK) " +
					"where a.grand_child_category_id = "+grandChildCategoryId+" and a.artist_status = "+ArtistStatus.ACTIVE.ordinal()+" " +
					" and a.display_on_search=1 order by a.name  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
		
		List<Artist> artists =new ArrayList<Artist>();
		Query query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			Artist artist = null;
			for (Object[] object : result) {
				artist= new Artist();
				Integer artistId = (Integer)object[0] ;
				Boolean isCustFavFlag = favArtistMap.get(artistId);
				if(null != isCustFavFlag && isCustFavFlag){
					artist.setCustFavFlag(true);
				}else{
					artist.setCustFavFlag(false);
				}
				if(artistId.equals(superFanArtistId) || superFanArtistId == artistId){
					artist.setCustSuperFanFlag(true);
				}
				artist.setId(artistId);
				artist.setName((String)object[1]);
				artists.add(artist);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return artists;
	}

	/**
	 * Get list of favorite artist info for customer
	 */
	@SuppressWarnings("unchecked")
	public List<Artist> getFavoriteArtistForCustomer(Integer customerId) {
		String sql = "select a.id as id, a.name as name,a.artist_status as artistStatus,'true' as custFavFlag  from cust_favorites_artist_handler ca WITH(NOLOCK) " +
				"inner join artist a WITH(NOLOCK) on(a.id=ca.artist_id) " +
				"where ca.status='ACTIVE' and a.display_on_search=1 and ca.customer_id="+customerId+" order by a.name asc";
		
		SQLQuery query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			
			Properties paramArtistStatus = new Properties();
			paramArtistStatus.put("enumClass", "com.zonesws.webservices.enums.ArtistStatus");
			paramArtistStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			query = session.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("artistStatus", Hibernate.custom(EnumType.class,paramArtistStatus));
			query.addScalar("custFavFlag", Hibernate.BOOLEAN);
			//query.addScalar("grandChildCategory", Hibernate.INTEGER);
			List<Artist> favouriteArtists = query.setResultTransformer(Transformers.aliasToBean(Artist.class)).list();
			return favouriteArtists;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally  {
			session.close();
		}
	}
	public List<Artist> getArtistByGrandChildCategoryId(Integer grandChildCategoryId,Integer pageNumber,Integer maxRows,ProductType productType)	 {
			
			
			String sql ="select distinct a.id,a.name FROM artist a " +
						"where a.grand_child_category_id = "+grandChildCategoryId+" and a.artist_status = "+ArtistStatus.ACTIVE.ordinal()+" " +
						" and a.display_on_search=1 order by a.name  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
			
			List<Artist> artists =new ArrayList<Artist>();
			Query query = null;
			Session session = null;
			try {
				session = getSessionFactory().openSession();
				query = session.createSQLQuery(sql);
				
				List<Object[]> result = (List<Object[]>)query.list();
				
				Artist artist = null;
				for (Object[] object : result) {
					artist= new Artist();
					Integer artistId = (Integer)object[0] ;
					artist.setId(artistId);
					artist.setName((String)object[1]);
					artists.add(artist);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally  {
				session.close();
			}
			return artists;
		}

	@SuppressWarnings("unchecked")
	public Collection<Artist> getSuperFanArtistDetails() {
		String SUPER_FAN_ARTIST = "select a.id as id, a.name as name,a.artist_status as artistStatus " +
				"from customer_super_fan cf WITH(NOLOCK) " +
				"inner join artist a WITH(NOLOCK) on(a.id=cf.artist_id) where " +
				"a.artist_status = "+ArtistStatus.ACTIVE.ordinal()+"  and a.display_on_search=1";
		
		SQLQuery query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			
			Properties paramArtistStatus = new Properties();
			paramArtistStatus.put("enumClass", "com.zonesws.webservices.enums.ArtistStatus");
			paramArtistStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			query = session.createSQLQuery(SUPER_FAN_ARTIST);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("artistStatus", Hibernate.custom(EnumType.class,paramArtistStatus));
			
			Collection<Artist> superFanArtistList = query.setResultTransformer(Transformers.aliasToBean(Artist.class)).list();
			return superFanArtistList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			session.close();
		}
		
	}
	
	
		
	}


