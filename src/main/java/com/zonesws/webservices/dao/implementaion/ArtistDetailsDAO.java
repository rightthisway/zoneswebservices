package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.ArtistDetails;

public class ArtistDetailsDAO extends HibernateDAO<Integer, ArtistDetails> implements com.zonesws.webservices.dao.services.ArtistDetailsDAO{

	/**
	 * 
	 */
	public ArtistDetails getArtistInfo(Integer artistId) throws Exception {
		return findSingle("FROM ArtistDetails WHERE id = ?", new Object[]{artistId});
	}

	
}
