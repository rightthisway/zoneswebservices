package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.AutoCatsLockedTickets;
import com.zonesws.webservices.enums.LockedTicketStatus;
import com.zonesws.webservices.enums.ProductType;

 


public class AutoCatsLockedTicketDAO extends HibernateDAO<Integer, AutoCatsLockedTickets> implements com.zonesws.webservices.dao.services.AutoCatsLockedTicketDAO{
	
	public List<AutoCatsLockedTickets> getLockedTicketByItemId(String  itemId){
		return find("FROM AutoCatsLockedTickets lt WHERE lt.itemId = ? and lt.lockedTicketsStatus=?",new Object[]{itemId,LockedTicketStatus.ACTIVE});
	}
	
	public List<AutoCatsLockedTickets> getLockedTicketByLockedTickettatus(LockedTicketStatus lockedTicketStatus){
		return find("FROM AutoCatsLockedTickets lt WHERE lt.lockedTicketsStatus = ?",new Object[]{LockedTicketStatus.ACTIVE});		
	}
	
	public List<AutoCatsLockedTickets> getLockedTicketByCartId(Integer cartId){
		return find("FROM AutoCatsLockedTickets lt WHERE lt.cartItem.id = ? and lt.lockedTicketsStatus=?",new Object[]{cartId,LockedTicketStatus.ACTIVE});
	}
	public AutoCatsLockedTickets getLockedTicketByZPCartId(Integer cartId){
		return findSingle("FROM AutoCatsLockedTickets lt WHERE lt.cartItem.id = ? and lt.lockStatus=?",new Object[]{cartId,LockedTicketStatus.ACTIVE});
	}
	public AutoCatsLockedTickets getLockedTicketByCategoryGroupId(Integer eventId,Integer catgeoryTicketGroupId){
		return findSingle("FROM AutoCatsLockedTickets lt WHERE lt.eventId = ? and lt.catgeoryTicketGroupId = ? and lt.lockStatus=?",new Object[]{eventId,catgeoryTicketGroupId,LockedTicketStatus.ACTIVE});
	}
	
	public AutoCatsLockedTickets getLockedTicketByTicketGroupId(Integer ticketGroupId){
		return findSingle("FROM AutoCatsLockedTickets lt WHERE lt.ticketId = ? and lt.lockStatus=?",new Object[]{ticketGroupId,LockedTicketStatus.ACTIVE});
	}
	
	public void deleteByCartId (Integer cartId) {
		try {
		//getHibernateTemplate().getSessionFactory().
			deleteAll(getLockedTicketByCartId(cartId));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to get all locked tickets
	 * @return
	 */
	public List<AutoCatsLockedTickets> getAllLockedTicket () {
		return find("FROM AutoCatsLockedTickets where lockStatus=?",new Object[]{LockedTicketStatus.ACTIVE});
	}

	public List<AutoCatsLockedTickets> getLockedTicketByEventId(Integer eventID,ProductType productType) {
		return find("FROM AutoCatsLockedTickets lt WHERE lt.eventId = ? and lt.lockStatus=? " +
				"and lt.productType=?",new Object[]{eventID,LockedTicketStatus.ACTIVE,productType});
	}
	
	public List<AutoCatsLockedTickets> getLockedTicketBySessionId(String sessionId,ProductType productType){
		return find("FROM AutoCatsLockedTickets lt WHERE lt.sessionId = ? and lt.lockStatus=? and lt.productType=? ",
				new Object[]{sessionId,LockedTicketStatus.ACTIVE,productType});
	}
	
	public AutoCatsLockedTickets getLockedTicketBySessionAndCategoryTicketGroupId(String sessionId,ProductType productType,Integer categoryTicketGroupId){
		return findSingle("FROM AutoCatsLockedTickets lt WHERE lt.sessionId = ? and lt.lockStatus=? and lt.productType=? and lt.catgeoryTicketGroupId=? ",
				new Object[]{sessionId,LockedTicketStatus.ACTIVE,productType,categoryTicketGroupId});
	}
	
	public AutoCatsLockedTickets getLockedTicketBySessionCategoryTicketGroupId(String sessionId,Integer categoryTicketGroupId){
		return findSingle("FROM AutoCatsLockedTickets lt WHERE lt.sessionId = ? and lt.lockStatus=? and lt.catgeoryTicketGroupId=? ",
				new Object[]{sessionId,LockedTicketStatus.ACTIVE,categoryTicketGroupId});
	}
	
	public AutoCatsLockedTickets getLockedTicketBySessionCategoryTicketGroupIdEventId(String sessionId,Integer categoryTicketGroupId,Integer eventID){
		return findSingle("FROM AutoCatsLockedTickets lt WHERE lt.sessionId = ? and lt.lockStatus=? and lt.catgeoryTicketGroupId=? and lt.eventId = ?",
				new Object[]{sessionId,LockedTicketStatus.ACTIVE,categoryTicketGroupId,eventID});
	}
	
	public void deleteByLockId(Integer lockId){
		bulkUpdate("DELETE FROM AutoCatsLockedTickets where id=?",new Object[]{lockId});
	}
 
}
