package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;

import com.zonesws.webservices.data.AutoCatsOrderItem;


/**
 * class having db related methods for autocats  order Item
 * @author Ulaganathan
 *
 */

public class AutoCatsOrderItemDAO extends HibernateDAO<Integer, AutoCatsOrderItem> implements com.zonesws.webservices.dao.services.AutoCatsOrderItemDAO{

	

	public List<AutoCatsOrderItem> getOrderDetailsByUserOrdrId(
			List<Integer> userOrderIds) {
		List<AutoCatsOrderItem> result = null;
		 
		  StringBuffer buffer = new StringBuffer ("from OrderItem where " +
		    " userOrder.id in (:userOrderIds)");
		  Query query = null;
		  Session session = null;
		try {
		   SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
		   session  = sessionFactory.openSession();    
		   query = session.createQuery(buffer.toString());
		   query.setParameterList("userOrderIds", userOrderIds);    
		   result = query.list();
		  } catch (Exception e) {
		   
		   e.printStackTrace();
		  } finally  {
		   session.close ();
		  }
		  
		  return result;
		
		
	}
	
	public AutoCatsOrderItem getOrderItemByOrderId(Integer orderId) {

		return findSingle("from AutoCatsOrderItem where userOrder.id=?", new Object[]{orderId});
		 
	}

	

	

}
