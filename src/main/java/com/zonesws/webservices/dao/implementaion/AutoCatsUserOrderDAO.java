package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.AutoCatsUserOrder;
/**
 * class having db related methods for autocats user order
 * @author Ulaganathan
 *
 */
public class AutoCatsUserOrderDAO extends HibernateDAO<Integer, AutoCatsUserOrder> implements com.zonesws.webservices.dao.services.AutoCatsUserOrderDAO{

	public List<AutoCatsUserOrder> getOrdersByCustomerId(Integer customerId) {
		
		return find("From AutoCatsUserOrder where customerId = ?",new Object[]{customerId});
	}
	
	public List<AutoCatsUserOrder> getPendingOrders(Date toDate) {
		
		return find("From AutoCatsUserOrder where orderStatus='PENDING' and orderDate<=? ",new Object[]{toDate});
	}
	
}
