package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.Cards;

public class CardsDAO extends HibernateDAO<Integer, Cards> implements com.zonesws.webservices.dao.services.CardsDAO {
	
	public Cards getCardsByCardPosition(Integer productId,Integer desktopPosition,Integer mobilePosition) throws Exception {
		return findSingle("FROM Cards WHERE productId=? and desktopPosition=? and mobilePosition=?", new Object[]{productId,desktopPosition,mobilePosition});
	}
	
	public List<Cards> getAllCardsByProductId(Integer productId) throws Exception {
		return find("FROM Cards WHERE productId=? order by desktopPosition,mobilePosition", new Object[]{productId});
	}
	
	public Cards getExistingCardsByCardPostionsAndCardId(Integer productId,Integer cardId,Integer desktopPosition,Integer mobilePosition) throws Exception {
		if(cardId == null) {
			return findSingle("FROM Cards WHERE productId=? and (desktopPosition=? or mobilePosition=?) ", new Object[]{productId,desktopPosition,mobilePosition});	
		} else {
			return findSingle("FROM Cards WHERE productId=? and (desktopPosition=? or mobilePosition=?)  and id<>? ", new Object[]{productId,desktopPosition,mobilePosition,cardId});
		}
		
	}
}
