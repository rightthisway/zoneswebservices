package com.zonesws.webservices.dao.implementaion;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.Category;


public class CategoryDAO extends HibernateDAO<Integer, Category> implements com.zonesws.webservices.dao.services.CategoryDAO{

	public List<String> getCategoryByArtistId(Integer id) {
		Session session = null;
		try{
			session = getSession();
			Query query = session.createSQLQuery("SELECT DISTINCT group_Name FROM category_new c INNER JOIN category_mapping_new cm on c.id = cm.category_id WHERE c.tour_Id=" + id);
			List<String> result = query.list();
			releaseSession(session);
			return result;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}
	
	
}
