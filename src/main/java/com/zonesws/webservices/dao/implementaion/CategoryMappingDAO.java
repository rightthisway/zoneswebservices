package com.zonesws.webservices.dao.implementaion;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CategoryMapping;


public class CategoryMappingDAO extends HibernateDAO<Integer, CategoryMapping> implements com.zonesws.webservices.dao.services.CategoryMappingDAO{

	public String getCategoryGroupNameByEventId(Integer eventId) {
		Session session = null;
		try{
			session = getSession();
			Query query = session.createQuery("SELECT DISTINCT categoryGroupName WHERE event.id="+eventId);
			List<String> result = query.list();
			if(result.iterator().hasNext()){
				return result.iterator().next();
			}
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}
	
	
}
