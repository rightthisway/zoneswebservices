package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CategoryTicket;

public class CategoryTicketDAO extends HibernateDAO<Integer, CategoryTicket> implements com.zonesws.webservices.dao.services.CategoryTicketDAO{

	public List<CategoryTicket> getUnmappedCategoryTicketByInvoiceId(Integer invoiceId)throws Exception{
		return find("FROM CategoryTicket WHERE invoiceId=? AND ticketId IS NULL order by id", new Object[]{invoiceId});
		
	}

	public List<CategoryTicket> getMappedCategoryTicketByInvoiceId(Integer invoiceId) throws Exception {
		return find("FROM CategoryTicket WHERE invoiceId=? AND ticketId IS NOT NULL order by id", new Object[]{invoiceId});
	}
	
	public int updateCategoryTicket(Integer ticketGroupId, Integer invoiceId, Double soldPrice) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE category_ticket SET invoice_id = "+invoiceId+", sold_price="+soldPrice+" WHERE  category_ticket_group_id = "+ticketGroupId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		return count;
	}
	
}
