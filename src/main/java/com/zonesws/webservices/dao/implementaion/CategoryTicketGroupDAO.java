package com.zonesws.webservices.dao.implementaion;

import java.util.Collection;
import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.TicketStatus;

public class CategoryTicketGroupDAO extends HibernateDAO<Integer, CategoryTicketGroup> implements com.zonesws.webservices.dao.services.CategoryTicketGroupDAO{


	public Collection<CategoryTicketGroup> getAllActiveCategoryTicketsbyEventId(Integer eventId) throws Exception {
		return find("FROM CategoryTicketGroup WHERE eventId=? and status=? ", new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
	public Collection<CategoryTicketGroup> getAllActiveCategoryTicketsbyEventIdAndProductType(Integer eventId,ProductType productType) throws Exception {
		return find("FROM CategoryTicketGroup WHERE eventId=? and status=? and  producttype= ? and price > 0 ", 
				new Object[]{eventId,TicketStatus.ACTIVE,productType});
	}
	
	public CategoryTicketGroup getCategoryTicketsbyCategoryTicketgroupIdAndProductType(Integer categoryTicketgroupId,ProductType productType) throws Exception {
		return findSingle("FROM CategoryTicketGroup WHERE id=? and status=? and  producttype= ? and price > 0 ", 
				new Object[]{categoryTicketgroupId,TicketStatus.ACTIVE,productType});
	}
	
	public CategoryTicketGroup getCategoryTicketsbyCategoryTicketgroupId(Integer categoryTicketgroupId) throws Exception {
		return findSingle("FROM CategoryTicketGroup WHERE id=? and status=? and price > 0 ", 
				new Object[]{categoryTicketgroupId,TicketStatus.ACTIVE});
	}
	
	public CategoryTicketGroup getLockedTicketById(Integer categoryTicketgroupId) throws Exception {
		return findSingle("FROM CategoryTicketGroup WHERE id=? and status=? ", 
				new Object[]{categoryTicketgroupId,TicketStatus.LOCKED});
	}
	
	public CategoryTicketGroup getLockedTicketsbyTktgroupIdAndProductType(Integer categoryTicketgroupId,ProductType productType) throws Exception {
		return findSingle("FROM CategoryTicketGroup WHERE id=? and status=? and  producttype= ?  ", 
				new Object[]{categoryTicketgroupId,TicketStatus.LOCKED,productType});
	}
	
	public Collection<CategoryTicketGroup> getAllBroadcastedCategoryTicketsbyEventId(Integer eventId) throws Exception {
		return find("FROM CategoryTicketGroup WHERE eventId=? and status=? and producttype in (?,?) and price > 0 and broadcast=? ", 
				new Object[]{eventId,TicketStatus.ACTIVE,ProductType.REWARDTHEFAN,ProductType.MANUAL,Boolean.TRUE});
	}
	
	public CategoryTicketGroup getBroadCastedCategoryTicketsbyId(Integer categoryTicketgroupId) throws Exception {
		return findSingle("FROM CategoryTicketGroup WHERE id=? and status=? and producttype in (?,?) and price > 0 and broadcast=? ", 
				new Object[]{categoryTicketgroupId,TicketStatus.ACTIVE,ProductType.REWARDTHEFAN,ProductType.MANUAL,Boolean.TRUE});
	}
	
	public void updateCategoryTicketGroupAsSold(Integer categoryTicketGroupId,Integer invoiceId, Double soldPrice, Integer soldQty) {
		try {
			String sql = "update CategoryTicketGroup set invoiceId=?,status=?,soldPrice=?,soldQuantity=?,lastUpdatedDate=?  where id = ?";
			bulkUpdate(sql, new Object[]{invoiceId,TicketStatus.SOLD,soldPrice,soldQty,new Date(),categoryTicketGroupId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateTicketAsSold(Integer categoryTicketGroupId,Integer invoiceId) {
		try {
			String sql = "update CategoryTicketGroup set invoiceId=?,status=?,lastUpdatedDate=? where id = ?";
			bulkUpdate(sql, new Object[]{invoiceId,TicketStatus.SOLD,new Date(),categoryTicketGroupId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void updateCategoryTicketGroupAsLocked(Integer categoryTicketGroupId,Integer invoiceId, Double soldPrice, Integer soldQty) {
		try {
			String sql = "update CategoryTicketGroup set invoiceId=?,status=?,soldPrice=?,soldQuantity=?,lastUpdatedDate=?  where id = ?";
			bulkUpdate(sql, new Object[]{invoiceId,TicketStatus.LOCKED,soldPrice,soldQty,new Date(),categoryTicketGroupId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void revertSoldTicketToActive(Integer categoryTicketGroupId) {
		try {
			String sql = "update CategoryTicketGroup set invoiceId=?,status=?,soldPrice=?,soldQuantity=?,lastUpdatedDate=?  where id = ?";
			bulkUpdate(sql, new Object[]{null,TicketStatus.ACTIVE,0.00,0,new Date(),categoryTicketGroupId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public int updateCategoryTicket(int ticketId, int invoiceId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE category_ticket_group SET invoice_id = "+invoiceId+" WHERE status = 'ACTIVE' and id = "+ticketId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
			
			
			
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}*/
}
