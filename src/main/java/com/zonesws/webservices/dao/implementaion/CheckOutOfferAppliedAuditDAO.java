package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CheckOutOfferAppliedAudit;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public class CheckOutOfferAppliedAuditDAO extends HibernateDAO<Integer, CheckOutOfferAppliedAudit> implements com.zonesws.webservices.dao.services.CheckOutOfferAppliedAuditDAO {
	
	public CheckOutOfferAppliedAudit getCheckoutOffer(ApplicationPlatForm platForm,Integer customerId, String sessionId, Integer ticketId
			,Boolean isFanPrice ){
		return findSingle("from CheckOutOfferAppliedAudit where customerId=? " +
				"and categoryTicketGroupId=? and sessionId=? and platForm=? and isFanPrice=? " +
				"and status=?", new Object[]{customerId,ticketId,sessionId,platForm, isFanPrice,"INITIATED"});
	}
}
