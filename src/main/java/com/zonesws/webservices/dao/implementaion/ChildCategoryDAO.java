package com.zonesws.webservices.dao.implementaion; 

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.GrandChildCategory;
/**
 * class having method related to Child Category 
 * @author hamin
 *
 */
public class ChildCategoryDAO extends HibernateDAO<Integer,ChildCategory> implements
 com.zonesws.webservices.dao.services.ChildCategoryDAO {

 
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<ChildCategory> getAll() {
		return find("FROM ChildCategory");	 
	}
	/**
	 * method to get child category using its name
	 * @param name, name of child category  
	 * @return ChildCategory
	 */	
	public ChildCategory getChildCategoryByName(String name) {
		 return findSingle("FROM ChildCategory where name = ?", new Object[]{name});	 
			
	}
	/**
	 * method to get child category using its id
	 * @param id, child category id
	 * @return ChildCategory
	 */
	public ChildCategory getChildCategoryById(Integer id) {
		  return findSingle("FROM ChildCategory where id = ?", new Object[]{id});	 
			
	}
	
	/**
	 * method to get child category using its parent category name
	 * @param parentCategoryName, parent category name
	 * @return ChildCategory
	 */
	public List<ChildCategory> getChildCategoryByParentCategoryName(
			String parentCategoryName) {
		return find("FROM ChildCategory WHERE parentCategory.name like ? order by name" ,new Object[]{parentCategoryName});
		
	}
	public List<ChildCategory> getChildCategoriesByName(String name) {
		String hql="FROM ChildCategory WHERE name like ? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}
	
	public String getChildCategoryIdsByName(String searchKey){
		Session session = null;
		String childCategoryIds = "";
		
		try{			
			StringBuilder queryString = new StringBuilder();
			queryString.append("select id from child_category where name = '"+searchKey+"'");
			
			session = getSessionFactory().openSession();
			Query query = session.createSQLQuery(queryString.toString());
			
			List<Object> result = (List<Object>)query.list();
			boolean flag = true;
			for (Object object : result) {
				if(flag){
					childCategoryIds = String.valueOf(object);
					flag = false;
				}else{
					childCategoryIds = childCategoryIds + "," + String.valueOf(object);
				}					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return childCategoryIds;
	}
	
	
}
