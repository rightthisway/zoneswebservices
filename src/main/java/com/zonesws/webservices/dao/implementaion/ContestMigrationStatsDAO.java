package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.ContestMigrationStats;

/**
 * class having db related methods for ContestMigrationStats
 * @author Ulaganathan
 *
 */
public class ContestMigrationStatsDAO extends HibernateDAO<Integer, ContestMigrationStats> implements com.zonesws.webservices.dao.services.ContestMigrationStatsDAO{
	
	public List<ContestMigrationStats> getAllStatsByContestId(Integer contestId) {
		return find("FROM ContestMigrationStats WHERE contestId = ?", new Object[] {contestId});
	}
	 
	
}
