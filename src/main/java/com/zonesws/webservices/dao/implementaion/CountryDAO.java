package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.Country;

/**
 * class having db related methods for Country
 * @author hamin
 *
 */
public class CountryDAO extends HibernateDAO<Integer, Country> implements com.zonesws.webservices.dao.services.CountryDAO{
	/**
	 * method to get List of Countries
	 * @return List of Countries
	 */
	public List<Country> getAllCountry() {
		
		return find("from Country  ", new Object[]{});
				
	}
	/**
	 * method to get Country by id
	 * @param id, company id
	 * @return
	 */
	public Country geCountryById(Integer id) {
		return findSingle("from Country where id=? ", new Object[]{id});
	}
	/**
	 * method to get id of US. 
	 * @return Id for US
	 */
	public Integer getIdOfUs() {
		Country country = findSingle("from Country  where countryName = ?", new Object[]{"UNITED STATES"});
		return country!=null?country.getId():0;
		
	}
	
	public Country getCountryByCountryName(String countryName) {
		return findSingle("from Country  where countryName = ?", new Object[]{countryName});
		
	}
	
	public List<Country> getActiveCountries() {
		return find("from Country where id in (217,38) ");
	}

}
