package com.zonesws.webservices.dao.implementaion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CrownJewelCategoryTicket;
import com.zonesws.webservices.enums.ZonesQuantityTypes;

public class CrownJewelCategoryTicketDAO extends HibernateDAO<Integer, CrownJewelCategoryTicket> implements com.zonesws.webservices.dao.services.CrownJewelCategoryTicketDAO{

	private static Session ticketSession = null;
	
	public static Session getTicketSession() {
		return CrownJewelCategoryTicketDAO.ticketSession;
	}

	public static void setTicketSession(Session ticketSession) {
		CrownJewelCategoryTicketDAO.ticketSession = ticketSession;
	}
	
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventId(Integer eventId) throws Exception{
		return find("FROM CrownJewelCategoryTicket where status='ACTIVE' and eventId=? ", new Object[] {eventId});
	}
	
	public CrownJewelCategoryTicket getCrownjewelTicketById(Integer ticketId,Integer eventId) throws Exception{
		return findSingle("FROM CrownJewelCategoryTicket where status='ACTIVE' and id=? and eventId=? ", new Object[] {ticketId,eventId});
	}
	
	public void updateCrownJewelCategoryTicketstoDeletedByEventId(Integer eventId) throws Exception{
		 bulkUpdate("UPDATE CrownJewelCategoryTicket set status='DELETED',lastUpdated=? WHERE status='ACTIVE' and eventID=?", new Object[]{new Date(),eventId});
	}
	
	
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventIdByTeamIdByLeagueId(Integer eventId,
			Integer teamId,Integer leagueId) throws Exception{
		try {
			String whereClause="select ct.id , ct.event_id,CONVERT(VARCHAR(150),ct.section) as section,ct.quantity,ct.zone_price," +
					"CONVERT(VARCHAR(150),ct.section_range) as section_range,CONVERT(VARCHAR(150),ct.row_range) as row_range," +
					"ct.status,tz.tickets_count,CASE WHEN zsc.total_sold_count is not null THEN zsc.total_sold_count " +
					"WHEN zsc.total_sold_count is null THEN 0 END as total_sold_count, CONVERT(VARCHAR(150),tz.quantity_type) as quantityType from " +
					"crownjewel_category_ticket ct WITH(NOLOCK) inner join crownjewel_team_zones tz WITH(NOLOCK) on tz.zone=ct.section and tz.team_id ="+teamId+" " +
					"left join crownjewel_zones_sold_count zsc WITH(NOLOCK) on zsc.zone=tz.zone and zsc.league_id="+leagueId+" and zsc.team_id="+teamId+" " +
					"where ct.status='ACTIVE' and tz.status='ACTIVE' and ct.event_id="+eventId+" order by ct.section , ct.quantity";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<CrownJewelCategoryTicket> finalList = new ArrayList<CrownJewelCategoryTicket>();
			CrownJewelCategoryTicket ticket=null;
			for (Object[] object : result) {
				
				try{
					Integer maxTixCount = (Integer)object[8];
					Integer soldTixCount = (Integer)object[9];
					if( maxTixCount <= soldTixCount ){
						continue;
					}
					if(null == object[10]){
						continue;
					}
					ticket = new CrownJewelCategoryTicket();
					ticket.setId((Integer)object[0]);
					ticket.setEventId((Integer)object[1]);
					ticket.setSection((String)object[2]);
					ticket.setQuantity((Integer)object[3]);
					BigDecimal price = (BigDecimal)object[4];
					ticket.setPrice(price.doubleValue());
					ticket.setSectionRange((String)object[5]);
					ticket.setRowRange((String)object[6]);
					ticket.setStatus((String)object[7]);
					ticket.setMaxTixCount(maxTixCount);
					ticket.setSoldTixCount(soldTixCount);
					ticket.setQuantityType(ZonesQuantityTypes.valueOf((String)object[10]));
					finalList.add(ticket);
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public List<CrownJewelCategoryTicket> getAllTeamZonesTickets(Integer eventId,
			Integer teamId,Integer leagueId) throws Exception{
		try {
			String whereClause="select tz.id , tz.event_id,CONVERT(VARCHAR(150),tz.zone) as section,tz.tickets_count,tz.price," +
					"'' as section_range,'' as row_range,CONVERT(VARCHAR(150),tz.status) as status,tz.tickets_count,CASE WHEN zsc.total_sold_count is not null " +
					"THEN zsc.total_sold_count WHEN zsc.total_sold_count is null THEN 0 END as total_sold_count," +
					"CONVERT(VARCHAR(150),tz.quantity_type) as quantity_type from crownjewel_team_zones " +
					"tz WITH(NOLOCK) left join crownjewel_zones_sold_count zsc WITH(NOLOCK) on zsc.zone_id = tz.id where tz.status='ACTIVE' and " +
					"tz.team_id="+teamId+" and tz.event_id="+eventId+" " +
					"order by tz.zone , tz.tickets_count";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<CrownJewelCategoryTicket> finalList = new ArrayList<CrownJewelCategoryTicket>();
			CrownJewelCategoryTicket ticket=null;
			for (Object[] object : result) {
				try{
					Integer maxTixCount = (Integer)object[8];
					Integer soldTixCount = (Integer)object[9];
					if( maxTixCount <= soldTixCount ){
						continue;
					}
					if(null == object[10]){
						continue;
					}
					
					ticket = new CrownJewelCategoryTicket();
					ticket.setId((Integer)object[0]);
					ticket.setEventId((Integer)object[1]);
					ticket.setSection((String)object[2]);
					ticket.setQuantity((Integer)object[3]);
					BigDecimal price = (BigDecimal)object[4];
					ticket.setPrice(price.doubleValue());
					ticket.setSectionRange((String)object[5]);
					ticket.setRowRange((String)object[6]);
					ticket.setStatus((String)object[7]);
					ticket.setMaxTixCount(maxTixCount);
					ticket.setSoldTixCount(soldTixCount);
					ticket.setQuantityType(ZonesQuantityTypes.valueOf((String)object[10]));
					finalList.add(ticket);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public CrownJewelCategoryTicket getFakeCategoryTicketByTicketIdByEventId(Integer ticketId,Integer eventId) throws Exception{
		try {
			String whereClause="select tz.id , tz.event_id,CONVERT(VARCHAR(150),tz.zone) as section,tz.tickets_count,tz.price," +
					"'' as section_range,'' as row_range,CONVERT(VARCHAR(150),tz.status) as status,tz.tickets_count,CASE WHEN zsc.total_sold_count is not null " +
					"THEN zsc.total_sold_count WHEN zsc.total_sold_count is null THEN 0 END as total_sold_count from " +
					"crownjewel_team_zones tz WITH(NOLOCK) left join crownjewel_zones_sold_count zsc WITH(NOLOCK) on " +
					"zsc.zone_id=tz.id where tz.id="+ticketId+" and tz.event_id= "+eventId+" " +
					"and tz.status='ACTIVE' and tz.quantity_type is not null and tz.price is not null ";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			CrownJewelCategoryTicket ticket=null;
			for (Object[] object : result) {
				try{
					Integer maxTixCount = (Integer)object[8];
					Integer soldTixCount = (Integer)object[9];
					if( maxTixCount <= soldTixCount ){
						continue;
					}
					ticket = new CrownJewelCategoryTicket();
					ticket.setId((Integer)object[0]);
					ticket.setEventId((Integer)object[1]);
					ticket.setSection((String)object[2]);
					ticket.setQuantity((Integer)object[3]);
					BigDecimal price = (BigDecimal)object[4];
					ticket.setPrice(price.doubleValue());
					ticket.setSectionRange((String)object[5]);
					ticket.setRowRange((String)object[6]);
					ticket.setStatus((String)object[7]);
					ticket.setMaxTixCount(maxTixCount);
					ticket.setSoldTixCount(soldTixCount);
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		return ticket;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	/*public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTickets(Integer eventExcludeDays) throws Exception{
		try {
			String whereClause="select * from (select distinct ct.id , ct.event_id,CONVERT(VARCHAR(150),ct.section) as section,ct.quantity,ct.zone_price," +
					"CONVERT(VARCHAR(150),ct.section_range) as section_range,CONVERT(VARCHAR(150),ct.row_range) as row_range," +
					"ct.status,tz.tickets_count as maximumSoldTickets,CASE WHEN zsc.total_sold_count is not null THEN zsc.total_sold_count " +
					"WHEN zsc.total_sold_count is null THEN 0 END as total_sold_count,t.id as teamId,t.league_id as leagueId,CONVERT(VARCHAR(50),t.city) as city from " +
					"crownjewel_category_ticket ct WITH(NOLOCK) inner join crownjewel_team_zones tz WITH(NOLOCK) on tz.zone=ct.section " +
					"inner join crownjewel_teams t WITH(NOLOCK) " +
					"on t.id=tz.team_id and t.status='ACTIVE' inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and l.status='ACTIVE' " +
					"left join crownjewel_zones_sold_count zsc WITH(NOLOCK) on zsc.zone=tz.zone  and zsc.team_id=tz.team_id " +
					"and zsc.status='ACTIVE' where ct.status='ACTIVE' and tz.status='ACTIVE' ) a " +
					"where a.event_id in (select * from (select event_id from crownjewel_leagues WITH(NOLOCK) where event_id is not null " +
					"and status='ACTIVE' and is_real_event=1 " +
					"union " +
					"select distinct event_id from crownjewel_teams WITH(NOLOCK) where event_id is not null and status='ACTIVE') e " +
					"inner join event_details ee WITH(NOLOCK) on ee.event_id = e.event_id where ee.status=1 and ee.event_date > GETDATE() + "+eventExcludeDays+")";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<CrownJewelCategoryTicket> finalList = new ArrayList<CrownJewelCategoryTicket>();
			CrownJewelCategoryTicket ticket=null;
			for (Object[] object : result) {
				
				try{
					
					Integer maxTixCount = (Integer)object[8];
					Integer soldTixCount = (Integer)object[9];
					if( maxTixCount <= soldTixCount ){
						continue;
					}
					ticket = new CrownJewelCategoryTicket();
					ticket.setId((Integer)object[0]);
					ticket.setEventId((Integer)object[1]);
					ticket.setSection((String)object[2]);
					ticket.setQuantity((Integer)object[3]);
					BigDecimal price = (BigDecimal)object[4];
					ticket.setPrice(price.doubleValue());
					ticket.setSectionRange((String)object[5]);
					ticket.setRowRange((String)object[6]);
					ticket.setStatus((String)object[7]);
					ticket.setMaxTixCount(maxTixCount);
					ticket.setSoldTixCount(soldTixCount);
					ticket.setTeamId((Integer)object[10]);
					ticket.setLeagueId((Integer)object[11]);
					ticket.setCity((String)object[12]);
					finalList.add(ticket);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}*/
	
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTickets(Integer eventExcludeDays) throws Exception{
		try {
			String whereClause="select * from (select distinct tz.id , tz.event_id,CONVERT(VARCHAR(150),tz.zone) as section," +
					"tz.tickets_count as quantity,tz.price,'' as section_range,'' as row_range,CONVERT(VARCHAR(50),tz.status) as status ," +
					"tz.tickets_count as maximumSoldTickets," +
					"CASE WHEN zsc.total_sold_count is not null THEN zsc.total_sold_count WHEN zsc.total_sold_count is null THEN 0 END as total_sold_count," +
					"t.id as teamId,t.league_id as leagueId,CONVERT(VARCHAR(50),t.city) as city,CONVERT(VARCHAR(50),'MANUAL') as ticketType from crownjewel_team_zones tz " +
					"WITH(NOLOCK) inner join crownjewel_teams t WITH(NOLOCK) on t.id=tz.team_id and t.status='ACTIVE' inner join crownjewel_leagues l " +
					"WITH(NOLOCK) on l.id=t.league_id and l.status='ACTIVE' and l.is_real_event=0 left join crownjewel_zones_sold_count zsc " +
					"WITH(NOLOCK) on zsc.zone=tz.zone  and zsc.team_id=tz.team_id and zsc.status='ACTIVE' where tz.status='ACTIVE' " +
					"union all " +
					"select distinct ct.id , ct.event_id,CONVERT(VARCHAR(150),ct.section) as section,ct.quantity,ct.zone_price as price," +
					"CONVERT(VARCHAR(150),ct.section_range) as section_range,CONVERT(VARCHAR(150),ct.row_range) as row_range,CONVERT(VARCHAR(50),ct.status) as status ," +
					"tz.tickets_count as maximumSoldTickets,CASE WHEN zsc.total_sold_count is not null THEN zsc.total_sold_count " +
					"WHEN zsc.total_sold_count is null THEN 0 END as total_sold_count,t.id as teamId,t.league_id as leagueId," +
					"CONVERT(VARCHAR(50),t.city) as city,CONVERT(VARCHAR(50),'REAL') as ticketType from crownjewel_category_ticket ct WITH(NOLOCK) " +
					"inner join crownjewel_team_zones tz WITH(NOLOCK) on tz.zone=ct.section inner join crownjewel_teams t " +
					"WITH(NOLOCK) on t.id=tz.team_id and t.status='ACTIVE' inner join crownjewel_leagues l WITH(NOLOCK) on " +
					"l.id=t.league_id and l.status='ACTIVE' and l.is_real_event=1 left join crownjewel_zones_sold_count zsc " +
					"WITH(NOLOCK) on zsc.zone=tz.zone  and zsc.team_id=tz.team_id and zsc.status='ACTIVE' where ct.status='ACTIVE' " +
					"and tz.status='ACTIVE' ) a where a.event_id in (select distinct e.event_id from (select event_id from crownjewel_leagues " +
					"WITH(NOLOCK) where event_id is not null and status='ACTIVE' union select distinct event_id from crownjewel_teams " +
					"WITH(NOLOCK) where event_id is not null and status='ACTIVE' ) e inner join event_details ee WITH(NOLOCK) on " +
					"ee.event_id = e.event_id where ee.status=1 and ee.event_date > GETDATE() + "+eventExcludeDays+" )";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<CrownJewelCategoryTicket> finalList = new ArrayList<CrownJewelCategoryTicket>();
			CrownJewelCategoryTicket ticket=null;
			for (Object[] object : result) {
				try{
					Integer maxTixCount = (Integer)object[8];
					Integer soldTixCount = (Integer)object[9];
					if( maxTixCount <= soldTixCount ){
						continue;
					}
					ticket = new CrownJewelCategoryTicket();
					ticket.setId((Integer)object[0]);
					ticket.setEventId((Integer)object[1]);
					ticket.setSection((String)object[2]);
					ticket.setQuantity((Integer)object[3]);
					BigDecimal price = (BigDecimal)object[4];
					ticket.setPrice(price.doubleValue());
					ticket.setSectionRange((String)object[5]);
					ticket.setRowRange((String)object[6]);
					ticket.setStatus((String)object[7]);
					ticket.setMaxTixCount(maxTixCount);
					ticket.setSoldTixCount(soldTixCount);
					ticket.setTeamId((Integer)object[10]);
					ticket.setLeagueId((Integer)object[11]);
					ticket.setCity((String)object[12]);
					ticket.setTicketType((String)object[13]);
					finalList.add(ticket);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	public Set<Integer> getAllActiveLeagueIds() throws Exception{
		try {
			String whereClause="select distinct a.leagueId from (select distinct t.league_id as leagueId  from crownjewel_team_zones tz " +
					"WITH(NOLOCK) inner join crownjewel_teams t WITH(NOLOCK) on t.id=tz.team_id and t.status='ACTIVE' and t.odds >0 inner join " +
					"crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and l.status='ACTIVE' and l.event_id is not null where tz.status='ACTIVE' " +
					"and tz.quantity_type is not null and t.status='ACTIVE'  " +
					"union all " +
					"select distinct t.league_id as leagueId from crownjewel_category_ticket ct WITH(NOLOCK) " +
					"inner join crownjewel_team_zones tz WITH(NOLOCK) on tz.zone=ct.section inner join crownjewel_teams t " +
					"WITH(NOLOCK) on t.id=tz.team_id and t.status='ACTIVE' and t.odds >0 inner join crownjewel_leagues l WITH(NOLOCK) on " +
					"l.id=t.league_id and l.status='ACTIVE' and l.event_id is not null where ct.status='ACTIVE' and tz.status='ACTIVE' and tz.quantity_type is not null " +
					"and t.status='ACTIVE' ) a ";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Integer> result = (List<Integer>)query.list();
			Set<Integer> finalList = new HashSet<Integer>();
			for (Integer object : result) {
				try{
					finalList.add(object);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public Set<String> getAllActiveTeamIds() throws Exception{
		try {
			String whereClause="select distinct a.teamId,a.leagueId from (select distinct t.id as teamId,t.league_id as leagueId from " +
					"crownjewel_team_zones tz WITH(NOLOCK) inner join crownjewel_teams t WITH(NOLOCK) on t.id=tz.team_id and t.status='ACTIVE' " +
					"and t.odds >0 inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and l.status='ACTIVE' where tz.status='ACTIVE' and " +
					"tz.quantity_type is not null and t.status='ACTIVE' " +
					"union all " +
					"select distinct t.id as teamId,t.league_id as leagueId from crownjewel_category_ticket ct WITH(NOLOCK) inner join " +
					"crownjewel_team_zones tz WITH(NOLOCK) on tz.zone=ct.section inner join crownjewel_teams t WITH(NOLOCK) on t.id=tz.team_id " +
					"and t.status='ACTIVE' and t.odds >0 inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and l.status='ACTIVE' where " +
					"ct.status='ACTIVE' and t.status='ACTIVE' and tz.status='ACTIVE' and tz.quantity_type is not null) a ";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			Set<String> finalList = new HashSet<String>();
			for (Object[] object : result) {
				try{
					finalList.add((Integer)object[1]+"_"+(Integer)object[0]);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	public Set<String> getAllActiveLeaguesTeamsCities() throws Exception{
		try {
			String whereClause="select distinct a.leagueId,a.city from (select distinct t.league_id as leagueId," +
					" CONVERT(VARCHAR(150),t.city) as city from crownjewel_team_zones tz WITH(NOLOCK) inner join crownjewel_teams t WITH(NOLOCK) on t.id=tz.team_id " +
					"and t.status='ACTIVE' inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and l.status='ACTIVE' where " +
					"tz.status='ACTIVE' and tz.quantity_type is not null and t.status='ACTIVE' and t.city is not null " +
					"union all " +
					"select distinct t.league_id as leagueId,CONVERT(VARCHAR(150),t.city) as city from crownjewel_category_ticket ct WITH(NOLOCK) " +
					"inner join crownjewel_team_zones tz WITH(NOLOCK) on tz.zone=ct.section inner join crownjewel_teams t WITH(NOLOCK) " +
					"on t.id=tz.team_id and t.status='ACTIVE' inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and " +
					"l.status='ACTIVE' where ct.status='ACTIVE' and t.status='ACTIVE' and tz.status='ACTIVE' and tz.quantity_type is not null" +
					" and t.city is not null) a";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			Set<String> finalList = new HashSet<String>();
			for (Object[] object : result) {
				try{
					String city = (String)object[1];
					finalList.add((Integer)object[0]+"_"+city.replaceAll(" +","").toUpperCase());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public Set<String> getAllActiveEvents() throws Exception {
		try {
			String whereClause="select distinct a.leagueId,a.event_id from (select distinct t.league_id as leagueId,tz.event_id " +
					"from crownjewel_team_zones tz WITH(NOLOCK) inner join event e WITH(NOLOCK) on e.id = tz.event_id and e.status=1 " +
					"and e.event_date > GETDATE() inner join crownjewel_teams t WITH(NOLOCK) on t.id=tz.team_id and t.status='ACTIVE' " +
					"inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id  and l.status='ACTIVE' where tz.status='ACTIVE' " +
					"and t.status='ACTIVE' " +
					"union all " +
					"select distinct t.league_id as leagueId,ct.event_id from crownjewel_category_ticket ct WITH(NOLOCK) inner join event e " +
					"WITH(NOLOCK) on e.id = ct.event_id and e.status=1 and e.event_date > GETDATE() inner join crownjewel_team_zones tz " +
					"WITH(NOLOCK) on tz.zone=ct.section inner join crownjewel_teams t WITH(NOLOCK) on t.id=tz.team_id and t.status='ACTIVE' " +
					"inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and l.status='ACTIVE' where ct.status='ACTIVE' and " +
					"t.status='ACTIVE' and tz.status='ACTIVE' and tz.quantity_type is not null) a ";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			Set<String> finalList = new HashSet<String>();
			for (Object[] object : result) {
				try{
					finalList.add((Integer)object[0]+"_"+(Integer)object[1]);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public Set<String> getAllEventsByLeagueAndTeams() throws Exception {
		try {
			String whereClause="select distinct a.leagueId,a.teamId,a.event_id from (select distinct t.league_id as leagueId," +
					"t.id as teamId,tz.event_id from crownjewel_team_zones tz WITH(NOLOCK) inner join event e WITH(NOLOCK) on " +
					"e.id = tz.event_id and e.status=1 and (e.event_date is null or e.event_date > GETDATE())  inner join crownjewel_teams t WITH(NOLOCK) " +
					"on t.id=tz.team_id and t.status='ACTIVE' inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id  " +
					"and l.status='ACTIVE' where tz.status='ACTIVE' and tz.quantity_type is not null and t.status='ACTIVE' " +
					"union all " +
					"select distinct t.league_id as leagueId,t.id as teamId,ct.event_id from crownjewel_category_ticket ct WITH(NOLOCK) " +
					"inner join event e WITH(NOLOCK) on e.id = ct.event_id and e.status=1 and (e.event_date is null or e.event_date > GETDATE())  inner join " +
					"crownjewel_team_zones tz WITH(NOLOCK) on tz.zone=ct.section inner join crownjewel_teams t WITH(NOLOCK) on " +
					"t.id=tz.team_id and t.status='ACTIVE' inner join crownjewel_leagues l WITH(NOLOCK) on l.id=t.league_id and " +
					"l.status='ACTIVE' where ct.status='ACTIVE' and t.status='ACTIVE' and tz.status='ACTIVE' " +
					"and tz.quantity_type is not null) a";
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			Set<String> finalList = new HashSet<String>();
			for (Object[] object : result) {
				try{
					finalList.add((Integer)object[0]+"_"+(Integer)object[1]+"_"+(Integer)object[2]);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public List<CrownJewelCategoryTicket> getAllActiveFanatsyRealTicketsByCategoryId(Integer categoryId) throws Exception{
		try {
			String whereClause="select fct.id,fct.event_id as tmatEventId,CONVERT(VARCHAR(50),fct.section) as zone ,fct.quantity,fct.zone_price," +
					"CASE WHEN zsc.total_sold_count is not null THEN zsc.total_sold_count WHEN zsc.total_sold_count is null THEN 0 END as total_sold_count" +
					",fe.id as fantasyEventId,fe.grandchild_id from crownjewel_category_ticket fct inner join fantasy_events fe " +
					"on fe.event_id = fct.event_id left join fantasy_event_zones_sold_count zsc on zsc.tmat_event_id=fe.event_id " +
					"and zsc.zone=fct.section and fe.status='ACTIVE' and fe.grandchild_id="+categoryId;
			
			ticketSession = CrownJewelCategoryTicketDAO.getTicketSession();
			if(ticketSession==null || !ticketSession.isOpen() || !ticketSession.isConnected()){
				ticketSession = getSession();
				CrownJewelCategoryTicketDAO.setTicketSession(ticketSession);
			}
			Query query = ticketSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<CrownJewelCategoryTicket> finalList = new ArrayList<CrownJewelCategoryTicket>();
			CrownJewelCategoryTicket ticket=null;
			for (Object[] object : result) {
				try{
					ticket = new CrownJewelCategoryTicket();
					ticket.setId((Integer)object[0]);
					ticket.setEventId((Integer)object[1]);
					ticket.setSection((String)object[2]);
					ticket.setQuantity((Integer)object[3]);
					BigDecimal price = (BigDecimal)object[4];
					ticket.setPrice(price.doubleValue());
					ticket.setSoldTixCount((Integer)object[5]);
					ticket.setFantasyEventId((Integer)object[6]);
					ticket.setGrandChildId((Integer)object[7]);
					finalList.add(ticket);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		return finalList;
		}catch (Exception e) {
			if(ticketSession != null && ticketSession.isOpen() ){
				ticketSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	public CrownJewelCategoryTicket getRealTiketByTicketId(Integer ticketId) throws Exception{
		return findSingle("FROM CrownJewelCategoryTicket where status='ACTIVE' and id=? ", new Object[] {ticketId});
	}
	
}
