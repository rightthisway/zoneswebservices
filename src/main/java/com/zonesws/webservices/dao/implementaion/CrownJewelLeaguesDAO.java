package com.zonesws.webservices.dao.implementaion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.zonesws.webservices.data.CrownJewelLeagues;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.list.FantasyCategories;
import com.zonesws.webservices.utils.list.FantasyChildCategories;
import com.zonesws.webservices.utils.list.FantasyEventTeamTicket;
import com.zonesws.webservices.utils.list.FantasyGrandChildCategories;
import com.zonesws.webservices.utils.list.FantasyTicketsBean;
public class CrownJewelLeaguesDAO extends HibernateDAO<Integer, CrownJewelLeagues> implements com.zonesws.webservices.dao.services.CrownJewelLeaguesDAO{
	
	private static Session leagueSession = null;
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	
	public static Session getLeagueSession() {
		return CrownJewelLeaguesDAO.leagueSession;
	}


	public static void setLeagueSession(Session leagueSession) {
		CrownJewelLeaguesDAO.leagueSession = leagueSession;
	}


	public List<CrownJewelLeagues> getAllLeaguesByParentType(String parentType) throws Exception {
		
		return find("FROM CrownJewelLeagues WHERE parentType=? order by name",new Object[]{parentType});
	}

	
	public CrownJewelLeagues getFantasyEventById(Integer fantasyEventId) throws Exception {
		return findSingle("FROM CrownJewelLeagues WHERE id = ? and status='ACTIVE'", new Object[]{fantasyEventId});
	}
	
	
		/*String sql ="select id,league_id AS leagueId,name from crownjewel_teams where status='ACTIVE' and " +
				"league_id ="+leagueId;
		SQLQuery query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			List<CrownJewelTeams> superFanTeams = query.setResultTransformer(Transformers.aliasToBean(CrownJewelTeams.class)).list();
			return superFanTeams;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally  {
			session.close();
		}
	}*/

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<CrownJewelLeagues> getAllSuperFanLeagues() {
		return find("FROM CrownJewelLeagues where status='ACTIVE'");
	}
	
 public List<CrownJewelLeagues> getAllActiveOtherCrownJewelLeagues(){
		
		try {
			String whereClause="select distinct l.id ,CONVERT(VARCHAR(150),l.name) as name,CONVERT(VARCHAR(150),l.parent_type) as parent_type " +
					"from crownjewel_leagues l WITH(NOLOCK) inner join crownjewel_teams t WITH(NOLOCK) on l.id=t.league_id " +
					"and l.status='ACTIVE' and l.event_id is not null and t.odds >0 CROSS APPLY " +
					"(select zt.id from crownjewel_category_ticket zt WITH(NOLOCK) where  zt.event_id=t.event_id and zt.status='ACTIVE' " +
					"and zt.zone_price >0) zp where l.event_id is null and l.status='ACTIVE' and l.parent_type not like 'SPORTS' and " +
					"t.event_id is not null and t.status='ACTIVE'";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<CrownJewelLeagues> finalList = new ArrayList<CrownJewelLeagues>();
			CrownJewelLeagues league=null;
			for (Object[] object : result) {
				league= new CrownJewelLeagues();
				league.setId((Integer)object[0]);
				league.setName((String)object[1]);
				finalList.add(league);
			}
		return finalList;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 public List<FantasyTicketsBean> getAllActiveFantasyEventTeamsByGrandChild(Integer grandChildId){
		
		try {
			String whereClause="select fe.id as fantasyEventId,CONVERT(VARCHAR(150),fe.name) as eventName,ft.id as teamId," +
					"ft.odds ,fe.event_id as tmatEventId,fe.is_real_event as realEvent, CASE WHEN e.event_date is not null " +
					"THEN CONVERT(VARCHAR(19),e.event_date,101) WHEN e.event_date is null THEN 'TBD' END as eventDate,CASE WHEN " +
					"e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null " +
					"THEN 'TBD' END as eventTime,CONVERT(VARCHAR(150),v.building) as venueName,CONVERT(VARCHAR(150),v.city) as city ," +
					"CONVERT(VARCHAR(150),v.state) as state ,CONVERT(VARCHAR(150),v.country) as country,only_package As onlyPackageCost  from fantasy_events fe WITH(NOLOCK) " +
					"inner join fantasy_event_teams ft WITH(NOLOCK) on ft.fantasy_event_id=fe.id and ft.status='ACTIVE' and ft.odds > 0 inner join " +
					"event e WITH(NOLOCK) on e.id=fe.event_id and e.status=1 inner join venue v WITH(NOLOCK) on v.id=e.venue_id  where " +
					"fe.grandchild_id="+grandChildId+" and fe.status='ACTIVE' and fe.event_id is not null order by fe.name asc";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<FantasyTicketsBean> finalList = new ArrayList<FantasyTicketsBean>();
			FantasyTicketsBean bean=null;
			for (Object[] object : result) {
				bean= new FantasyTicketsBean();
				bean.setFantasyEventId((Integer)object[0]);
				bean.setFantasyEventName((String)object[1]);
				bean.setFantasyTeamId((Integer)object[2]);
				BigDecimal teamOdds = (BigDecimal)object[3];
				bean.setTeamOdds(teamOdds.doubleValue());
				bean.setTmatEventId((Integer)object[4]);
				bean.setRealEvent((Boolean)object[5]);
				
				String eventDate = (String)object[6];
				
				if(null != eventDate && !eventDate.equals("TBD")){
					String timeStr = DateUtil.formatTime((String)object[7]);
					bean.setEventDateTime((String)object[6]+" "+timeStr);
					bean.setEventDateStr((String)object[6]);
					bean.setEventTimeStr(timeStr);
					bean.setTempEventDate(df.parse((String)object[6]));
					bean.setShowEventDateAsTBD(false);
				}else{
					bean.setShowEventDateAsTBD(true);
					bean.setEventDateTBDValue("TBD");
				}
				bean.setVenueName((String)object[8]);
				bean.setCity((String)object[9]);
				bean.setState((String)object[10]);
				bean.setCountry((String)object[11]);
				Boolean isPackage = (Boolean)object[12];
				bean.setOnlyPackageCost(isPackage);
				
				
				finalList.add(bean);
			}
			return finalList;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 
 public List<FantasyCategories> getAllActiveFanstasySportsCategories(){
		
		try {
			
			String whereClause="select distinct fgc.id,CONVERT(VARCHAR(150),fgc.name) as name,CONVERT(VARCHAR(150),fci.image_url) as imageurl," +
			"fgc.package_cost,CASE WHEN fgc.package_line1 is not null THEN CONVERT(VARCHAR(150),fgc.package_line1) WHEN fgc.package_line1 is null THEN '' END as line1," +
			"CASE WHEN fgc.package_line2 is not null THEN CONVERT(VARCHAR(150),fgc.package_line2) WHEN fgc.package_line2 is null THEN '' END as line2," +
			"CASE WHEN fgc.package_line3 is not null THEN CONVERT(VARCHAR(150),fgc.package_line3) WHEN fgc.package_line3 is null THEN '' END as line3," +
			"CASE WHEN fgc.package_line4 is not null THEN CONVERT(VARCHAR(150),fgc.package_line4) WHEN fgc.package_line4 is null THEN '' END as line4," +
			"CASE WHEN fgc.package_line5 is not null THEN CONVERT(VARCHAR(150),fgc.package_line5) WHEN fgc.package_line5 is null THEN '' END as line5," +
			"CASE WHEN fgc.package_line6 is not null THEN CONVERT(VARCHAR(150),fgc.package_line6) WHEN fgc.package_line6 is null THEN '' END as line6 " +
			",CONVERT(VARCHAR(150),fci.mobile_image_url) as mobileImageUrl from fantasy_grand_child_category_image fci inner join fantasy_grandchild_category fgc on " +
			"fgc.id=fci.fantasy_grand_child_category_id and fci.status='ACTIVE' inner join crownjewel_leagues l " +
			"on l.grandchild_id=fgc.id and l.status='ACTIVE' and fgc.name in ('NBA','NFL','NHL','MLB')";
			
			/*String whereClause="select distinct fgc.id,CONVERT(VARCHAR(150),fgc.name) as name,CONVERT(VARCHAR(150),fci.image_url) as imageurl " +
					"from fantasy_grand_child_category_image fci WITH(NOLOCK) inner join fantasy_grandchild_category fgc WITH(NOLOCK) on " +
					"fgc.id=fci.fantasy_grand_child_category_id and fci.status='ACTIVE' inner join crownjewel_leagues l WITH(NOLOCK) " +
					"on l.grandchild_id=fgc.id and l.status='ACTIVE' and fgc.name in ('NBA','NFL','NHL','MLB')";*/
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<FantasyCategories> finalList = new ArrayList<FantasyCategories>();
			FantasyCategories bean=null;
			for (Object[] object : result) {
				bean= new FantasyCategories();
				bean.setId((Integer)object[0]);
				bean.setName((String)object[1]);
				bean.setImageName((String)object[2]);
				BigDecimal pkgCost = (BigDecimal)object[3];
				bean.setPackageCost(pkgCost.doubleValue());
				bean.setPkgInfoLine1((String)object[4]);
				bean.setPkgInfoLine2((String)object[5]);
				bean.setPkgInfoLine3((String)object[6]);
				bean.setPkgInfoLine4((String)object[7]);
				bean.setPkgInfoLine5((String)object[8]);
				bean.setPkgInfoLine6((String)object[9]);
				bean.setMobileImageName((String)object[10]);
				finalList.add(bean);
			}
			return finalList;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 
 public List<FantasyEventTeamTicket> getAllActiveFanstasyEventTeamTickets(Integer grandChildId){
		
		try {
			String whereClause="select tz.id,tz.fantasy_event_id,tz.team_id,tz.event_id as tmatEventId,CONVERT(VARCHAR(150),tz.zone) as zone," +
					"tz.tickets_count,tz.price,CASE WHEN fsc.total_sold_count is not null THEN fsc.total_sold_count WHEN fsc.total_sold_count " +
					"is null THEN 0 END as total_sold_count from fantasy_event_team_zones tz WITH(NOLOCK) inner join fantasy_events fe WITH(NOLOCK) on " +
					"fe.id=tz.fantasy_event_id and fe.grandchild_id="+grandChildId+" left join fantasy_event_zones_sold_count fsc on " +
					"fsc.fantasy_event_id=fe.id and fsc.fantasy_team_id = tz.team_id and tz.zone=fsc.zone where tz.status='ACTIVE' " +
					"and fe.status='ACTIVE' and tz.price is not null order by tz.fantasy_event_id,tz.team_id,tz.id asc";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<FantasyEventTeamTicket> finalList = new ArrayList<FantasyEventTeamTicket>();
			FantasyEventTeamTicket bean=null;
			for (Object[] object : result) {
				
				Integer maxTixCount = (Integer)object[5];
				Integer soldTixCount = (Integer)object[7];
				if( maxTixCount <= soldTixCount ){
					continue;
				}
				
				bean= new FantasyEventTeamTicket();
				bean.setTicketId((Integer)object[0]);
				bean.setTeamZoneId((Integer)object[0]);
				bean.setFantasyEventId((Integer)object[1]);
				bean.setTeamId((Integer)object[2]);
				bean.setTmatEventId((Integer)object[3]);
				bean.setZone((String)object[4]);
				bean.setTicketsCount((Integer)object[5]);
				BigDecimal price = (BigDecimal)object[6];
				bean.setPrice(price.doubleValue());
				finalList.add(bean);
			}
			return finalList;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 public FantasyCategories getActiveFanstasySportsCategoriesById(Integer categoryId){
		
		try {
			String whereClause="select distinct fgc.id,CONVERT(VARCHAR(150),fgc.name) as name,CONVERT(VARCHAR(150),fci.image_url) as imageurl," +
					"fgc.package_cost," +
					"CASE WHEN fgc.package_line1 is not null THEN  CONVERT(VARCHAR(150),fgc.package_line1) WHEN fgc.package_line1 is null THEN '' END as line1," +
					"CASE WHEN fgc.package_line2 is not null THEN CONVERT(VARCHAR(150),fgc.package_line2) WHEN fgc.package_line2 is null THEN '' END as line2," +
					"CASE WHEN fgc.package_line3 is not null THEN CONVERT(VARCHAR(150),fgc.package_line3) WHEN fgc.package_line3 is null THEN '' END as line3," +
					"CASE WHEN fgc.package_line4 is not null THEN CONVERT(VARCHAR(150),fgc.package_line4) WHEN fgc.package_line4 is null THEN '' END as line4," +
					"CASE WHEN fgc.package_line5 is not null THEN CONVERT(VARCHAR(150),fgc.package_line5) WHEN fgc.package_line5 is null THEN '' END as line5," +
					"CASE WHEN fgc.package_line6 is not null THEN CONVERT(VARCHAR(150),fgc.package_line6) WHEN fgc.package_line6 is null THEN '' END as line6 " +
					",CONVERT(VARCHAR(150),fci.mobile_image_url) as mobileImageUrl from fantasy_grand_child_category_image fci inner join fantasy_grandchild_category fgc on " +
					"fgc.id=fci.fantasy_grand_child_category_id and fci.status='ACTIVE' inner join crownjewel_leagues l " +
					"on l.grandchild_id=fgc.id and l.status='ACTIVE' and fgc.id="+categoryId;
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			FantasyCategories bean=null;
			for (Object[] object : result) {
				bean= new FantasyCategories();
				bean.setId((Integer)object[0]);
				bean.setName((String)object[1]);
				bean.setImageName((String)object[2]);
				BigDecimal pkgCost = (BigDecimal)object[3];
				bean.setPackageCost(pkgCost.doubleValue());
				bean.setPkgInfoLine1((String)object[4]);
				bean.setPkgInfoLine2((String)object[5]);
				bean.setPkgInfoLine3((String)object[6]);
				bean.setPkgInfoLine4((String)object[7]);
				bean.setPkgInfoLine5((String)object[8]);
				bean.setPkgInfoLine6((String)object[9]);
				bean.setMobileImageName((String)object[10]);
			}
			return bean;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 /*
 public List<FantasyTicketsBean> getAllActiveSportsCrownJewelLeagues(){
		
		try {
			String whereClause="select fl.id as leagueId,CONVERT(VARCHAR(150),fl.name) as leagueName, fgc.id as grandChildId,CONVERT(VARCHAR(150),fgc.name) as grandChildName," +
					"fcc.id as childId , CONVERT(VARCHAR(150),fcc.name) as childName, fpc.id as parentId , CONVERT(VARCHAR(150),fpc.name) as parentName,fl.is_real_event as realEvent from " +
					"fantasy_parent_category fpc WITH(NOLOCK) inner join fantasy_child_category fcc WITH(NOLOCK) on fpc.id=fcc.parent_id " +
					"inner join fantasy_grandchild_category fgc WITH(NOLOCK) on fgc.child_id=fcc.id inner join crownjewel_leagues fl WITH(NOLOCK) " +
					"on fl.grandchild_id=fgc.id and fl.child_id=fcc.id order by fcc.name,fgc.name asc";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<FantasyTicketsBean> superFanTeams = query.setResultTransformer(Transformers.aliasToBean(FantasyTicketsBean.class)).list();
			return superFanTeams;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}*/
 public List<FantasyCategories> getAllFantasyParentCategories(){
		
		try {
			String whereClause="select id,name from fantasy_parent_category";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<FantasyCategories> parentCatgeoies = query.setResultTransformer(Transformers.aliasToBean(FantasyCategories.class)).list();
			return parentCatgeoies;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 public List<FantasyChildCategories> getAllFantasyChildCategories(){
		
		try {
			String whereClause="select id,name from fantasy_child_category";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<FantasyChildCategories> parentCatgeoies = query.setResultTransformer(Transformers.aliasToBean(FantasyChildCategories.class)).list();
			return parentCatgeoies;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 public List<FantasyGrandChildCategories> getAllFantasyGrandChildCategories(){
		
		try {
			String whereClause="select id,name from fantasy_grandchild_category";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<FantasyGrandChildCategories> parentCatgeoies = query.setResultTransformer(Transformers.aliasToBean(FantasyGrandChildCategories.class)).list();
			return parentCatgeoies;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 public FantasyTicketsBean getEventVenueInformation(Integer tmatEventId){
		
		try {
			String whereClause="select CONVERT(VARCHAR(150),v.building) as venueName,CONVERT(VARCHAR(150),v.city) as city ," +
					"CONVERT(VARCHAR(150),v.state) as state ,CONVERT(VARCHAR(150),v.country) as country  from " +
					"event e WITH(NOLOCK) inner join venue v WITH(NOLOCK) on v.id=e.venue_id  where e.id = "+tmatEventId;
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			FantasyTicketsBean bean=null;
			for (Object[] object : result) {
				bean= new FantasyTicketsBean();
				bean.setVenueName((String)object[0]);
				bean.setCity((String)object[1]);
				bean.setState((String)object[2]);
				bean.setCountry((String)object[3]);
			}
			return bean;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
 
 public List<Integer> getAllActiveGrandChilds(){
		
		try {
			String whereClause="select distinct grandchild_id from (select fe.grandchild_id,tz.id,tz.tickets_count," +
					"CASE WHEN fsc.total_sold_count is not null THEN fsc.total_sold_count WHEN fsc.total_sold_count " +
					"is null THEN 0 END as total_sold_count,tz.price from fantasy_event_team_zones tz WITH(NOLOCK) " +
					"inner join fantasy_events fe WITH(NOLOCK) on fe.id=tz.fantasy_event_id left join " +
					"fantasy_event_zones_sold_count fsc WITH(NOLOCK) on fsc.fantasy_event_id=fe.id and fsc.fantasy_team_id = tz.team_id " +
					"and tz.zone=fsc.zone  where tz.status='ACTIVE' and fe.status='ACTIVE' and tz.price is not null " +
					"and fe.grandchild_id is not null) a where a.tickets_count > a.total_sold_count ";
			
			leagueSession = CrownJewelLeaguesDAO.getLeagueSession();
			if(leagueSession==null || !leagueSession.isOpen() || !leagueSession.isConnected()){
				leagueSession = getSession();
				CrownJewelLeaguesDAO.setLeagueSession(leagueSession);
			}
			Query query = leagueSession.createSQLQuery(whereClause);
			List<Integer> result = query.list();
			return result;
		}catch (Exception e) {
			if(leagueSession != null && leagueSession.isOpen() ){
				leagueSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
}
