package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CrownJewelTeamZones;

public class CrownJewelTeamZonesDAO extends HibernateDAO<Integer, CrownJewelTeamZones> implements com.zonesws.webservices.dao.services.CrownJewelTeamZonesDAO{

	public List<CrownJewelTeamZones> getAllTeamZonesByLeagueId(Integer leagueId) throws Exception {
		return find("SELECT tz FROM CrownJewelTeamZones tz,CrownJewelTeams te where te.id=tz.teamId and te.leagueId=?", new Object[] {leagueId});	
	}
	public List<CrownJewelTeamZones> getAllTeamZonesByTeamId(Integer teamId) throws Exception {
		return find("FROM CrownJewelTeamZones where teamId=?", new Object[] {teamId});	
	}
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueId(Integer leagueId) throws Exception {
		return find("SELECT tz FROM CrownJewelTeamZones tz,CrownJewelTeams te where te.id=tz.teamId and tz.status='ACTIVE' and te.leagueId=?", new Object[] {leagueId});	
	}
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByTeamId(Integer teamId) throws Exception {
		return find("FROM CrownJewelTeamZones where status='ACTIVE' and teamId=?", new Object[] {teamId});	
	}
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueIdAndCity(Integer leagueId,String city) throws Exception {
		return find("SELECT tz FROM CrownJewelTeamZones tz,CrownJewelTeams te where te.id=tz.teamId " +
				" and tz.status='ACTIVE' and te.leagueId=? and te.event.city=?", new Object[] {leagueId,city});	
	}
	
	public CrownJewelTeamZones getTeamZoneByEventTeamZone(Integer teamId,Integer eventId,String zone) throws Exception {
		return findSingle("FROM CrownJewelTeamZones where teamId=? and eventId=? and zone=?", new Object[] {teamId,eventId,zone});	
	}
	
	public CrownJewelTeamZones getFakeTicketByTicketId(Integer ticketId) throws Exception {
		return findSingle("FROM CrownJewelTeamZones where id=? and status='ACTIVE'", new Object[] {ticketId});	
	}
}
