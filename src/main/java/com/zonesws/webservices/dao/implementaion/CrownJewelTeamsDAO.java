package com.zonesws.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CrownJewelTeams;
import com.zonesws.webservices.utils.list.CrownEventCity;
import com.zonesws.webservices.utils.list.SportsTeam;
public class CrownJewelTeamsDAO extends HibernateDAO<Integer, CrownJewelTeams> implements com.zonesws.webservices.dao.services.CrownJewelTeamsDAO{
	
	private static Session teamSession = null;
	
	public static Session getTeamSession() {
		return CrownJewelTeamsDAO.teamSession;
	}

	public static void setTeamSession(Session teamSession) {
		CrownJewelTeamsDAO.teamSession = teamSession;
	}

	public List<CrownJewelTeams> getAllTeamsByLeagueId(Integer leagueId) throws Exception {
		return find("FROM CrownJewelTeams where leagueId=? and status='ACTIVE'", new Object[] {leagueId});	
	}
	
	public List<CrownJewelTeams> getAllTeamsByLeagueIdByLeagueCity(Integer leagueId,String leagueCity) throws Exception {
		return find("FROM CrownJewelTeams where leagueId=? and city=? and status='ACTIVE'", new Object[] {leagueId,leagueCity});	
	}
	
	public CrownJewelTeams getAllTeamsByLeagueIdByEventId(Integer leagueId,Integer eventId) throws Exception {
		return findSingle("FROM CrownJewelTeams where leagueId=? and eventId=? and status='ACTIVE'", new Object[] {leagueId,eventId});	
	}

	public CrownJewelTeams getAllSuperFanTeamsByTeamId(Integer teamId) {
		return findSingle("FROM CrownJewelTeams where id=? and status='ACTIVE'", new Object[] {teamId});
	}
	
	
	public List<SportsTeam> getAllActiveSportsTeamsByLeagueId(Integer leagueId) throws Exception {
		
		try {
			String whereClause="select distinct t.id,CONVERT(VARCHAR(150),t.name) as name from crownjewel_leagues l WITH(NOLOCK) inner join " +
					"crownjewel_teams t WITH(NOLOCK) on t.league_id=l.id  and t.odds >0 inner join event e WITH(NOLOCK) " +
					"on e.id=l.event_id and e.status=1   where l.event_id " +
					"is not null and l.status='ACTIVE' and t.league_id="+leagueId;
			
			teamSession = CrownJewelTeamsDAO.getTeamSession();
			if(teamSession==null || !teamSession.isOpen() || !teamSession.isConnected()){
				teamSession = getSession();
				CrownJewelTeamsDAO.setTeamSession(teamSession);
			}
			Query query = teamSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<SportsTeam> finalList = new ArrayList<SportsTeam>();
			SportsTeam league=null;
			for (Object[] object : result) {
				league= new SportsTeam();
				league.setTeamId((Integer)object[0]);
				league.setTeamName((String)object[1]);
				finalList.add(league);
			}
		return finalList;
		}catch (Exception e) {
			if(teamSession != null && teamSession.isOpen() ){
				teamSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	public List<CrownEventCity> getAllActiveOtherTeamsByLeagueId(Integer leagueId){
		
		try {
			String whereClause="select distinct CONVERT(VARCHAR(150),t.city) as city,CONVERT(VARCHAR(150),e.state) as state," +
					"CONVERT(VARCHAR(150),e.country) as country from crownjewel_teams t WITH(NOLOCK) inner join event_details e " +
					"WITH(NOLOCK) on e.event_id=t.event_id CROSS APPLY (select zt.id from crownjewel_category_ticket zt WITH(NOLOCK) " +
					"where  zt.event_id=t.event_id and zt.status='ACTIVE' and zt.zone_price >0) zp where t.event_id is not null " +
					"and t.status='ACTIVE' and e.status=1 and e.event_date > GETDATE()+2 and t.league_id="+leagueId;
			
			teamSession = CrownJewelTeamsDAO.getTeamSession();
			if(teamSession==null || !teamSession.isOpen() || !teamSession.isConnected()){
				teamSession = getSession();
				CrownJewelTeamsDAO.setTeamSession(teamSession);
			}
			Query query = teamSession.createSQLQuery(whereClause);
			List<Object[]> results = (List<Object[]>)query.list();
			List<CrownEventCity> finalList = new ArrayList<CrownEventCity>();
			CrownEventCity league=null;
			
			for (Object[] object : results) {
				league= new CrownEventCity();
				league.setCity((String)object[0]);
				league.setState((String)object[1]);
				league.setCountry((String)object[2]);
				finalList.add(league);
			}
		return finalList;
		}catch (Exception e) {
			if(teamSession != null && teamSession.isOpen() ){
				teamSession.close();
			}
			e.printStackTrace();
		}
		return null;
		
	}
	
	public CrownJewelTeams getActiveFantasyTeamById(Integer teamId) throws Exception {
		return findSingle("FROM CrownJewelTeams where id=? and status='ACTIVE' and odds > 0", new Object[] {teamId});	
	}

}
