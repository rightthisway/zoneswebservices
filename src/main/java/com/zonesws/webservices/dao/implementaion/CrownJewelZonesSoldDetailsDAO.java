package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CrownJewelZonesSoldDetails;

public class CrownJewelZonesSoldDetailsDAO extends HibernateDAO<Integer, CrownJewelZonesSoldDetails> implements com.zonesws.webservices.dao.services.CrownJewelZonesSoldDetailsDAO{

	public CrownJewelZonesSoldDetails getZoneSoldDetailsByTeamIdByLeagueIdByEventIdByZone(Integer fEventId,Integer teamId,
			Integer zoneId, String zone) throws Exception {
		return findSingle("FROM CrownJewelZonesSoldDetails where fantasyEventId=? and teamId=? and (zoneId=? or zone=?) ", 
				new Object[] {fEventId,teamId,zoneId,zone});	
	}
}
