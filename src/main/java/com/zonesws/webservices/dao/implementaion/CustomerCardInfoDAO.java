package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerCardInfo;
import com.zonesws.webservices.enums.Status;

public class CustomerCardInfoDAO extends HibernateDAO<Integer, CustomerCardInfo> implements com.zonesws.webservices.dao.services.CustomerCardInfoDAO{

	public CustomerCardInfo getActiveCustomerCardInfoById(Integer cardId){
		return findSingle("FROM CustomerCardInfo WHERE id = ? and status=? ", new Object[]{cardId, Status.ACTIVE});
	}
	
	public List<CustomerCardInfo> getAllActiveCardsByCustomerId(Integer customerId){
		return find("FROM CustomerCardInfo WHERE customerId = ? and status=? and showToCustomer=? order by id", 
				new Object[]{customerId, Status.ACTIVE,Boolean.TRUE});
	}
	
	public List<CustomerCardInfo> getAllCardsByCustomerId(Integer customerId){
		return find("FROM CustomerCardInfo WHERE customerId = ? and status=? order by id", 
				new Object[]{customerId, Status.ACTIVE});
	}
	
	public List<CustomerCardInfo> getAllActiveStripeRegisteredCardsByCustomerId(Integer customerId){
		return find("FROM CustomerCardInfo WHERE customerId = ? and status=? and sCustId is not null and sCustId <> '' order by id", 
				new Object[]{customerId, Status.ACTIVE});
	}
	
	public CustomerCardInfo getActiveCustomerCardInfoByStripeId(String stripeId,String sCustId){
		return findSingle("FROM CustomerCardInfo WHERE sCustCardId = ? and sCustId = ? and status=? and showToCustomer=? ", 
				new Object[]{stripeId,sCustId, Status.ACTIVE,Boolean.TRUE});
	}
	
}
