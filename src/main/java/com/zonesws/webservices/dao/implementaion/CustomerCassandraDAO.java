package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerCassandra;

public class CustomerCassandraDAO extends HibernateDAO<Integer, CustomerCassandra> implements com.zonesws.webservices.dao.services.CustomerCassandraDAO{

	public List<CustomerCassandra> getCustomerDateByContestId(Integer contestId){
		return find("FROM CustomerCassandra WHERE contestId=?",new Object[] {contestId});
	}
}
