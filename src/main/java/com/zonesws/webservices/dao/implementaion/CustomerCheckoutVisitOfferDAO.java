package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CustomerCheckoutVisitOffer;

/**
 * class having db related methods for CustomerCheckoutVisitOffer
 * @author Ulaganathan
 *
 */
public class CustomerCheckoutVisitOfferDAO extends HibernateDAO<Integer, CustomerCheckoutVisitOffer> implements com.zonesws.webservices.dao.services.CustomerCheckoutVisitOfferDAO{
	
	public CustomerCheckoutVisitOffer getActivetOffer() {
		return findSingle("from CustomerCheckoutVisitOffer where status='ACTIVE' ", new Object[]{});
	}
	
}
