package com.zonesws.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.list.AutoPageResult;
/**
 * class having db related methods for Customer
 * @author hamin
 *
 */
public class CustomerDAO extends HibernateDAO<Integer, Customer> implements com.zonesws.webservices.dao.services.CustomerDAO {
	/**
	 * method to get Customer by email and password
	 * @param email, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 */
	public List<Customer> getCustomer(String email,String password){
		
		return find("from Customer where email = ? and password =? ", new Object[]{email,password});
		
	} 
	
	/**
	 * method to get Country by email
	 * @param email, email address of a customer	
	 * @return Customer
	 */
	public Customer getCustomerByEmailId(String emailId) {
		return findSingle("from Customer where email = ? ", new Object[]{emailId});
	}
	/**
	 * method to get Customer by email and password
	 * @param emailId, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 */
	public Customer getCustomerByEmailIdAndPassword(String emailId, String password) {
		return findSingle("from Customer where email = ? and password = ?", new Object[]{emailId,password});
	}
	/**
	 * method to get Customer by userName
	 * @param userName, user name of a customer
	 * @return Customer
	 */
	public Customer getCustomerByUserName(String userName) {
		return findSingle("from Customer where userName = ? ", new Object[]{userName});
	}
	/**
	 * method to get Customer by email and user name
	 * @param emailId, email address of a customer
	 * @param userName, user name of a customer
	 * @return Customer
	 */
	public Customer getCustomerByEmailAndUserName(String email, String userName) {
		
		return findSingle("from Customer where email= ? and userName = ? ", new Object[]{email,userName});
	}
	/**
	 *  method to get Customer by customer Id
	 * @param customerId,  customer Id
	 * @return Customer
	 */
	public Customer getCustomerById(Integer customerId) {
		return findSingle("from Customer where id= ? ", new Object[]{customerId});
		
	}
	
	/**
	 *  method to get Customer by customer Id
	 * @param customerId,  customer Id
	 * @return Customer
	 */
	public Customer getCustomerByIdByProduct(Integer customerId,ProductType productType) {
		return findSingle("from Customer where id= ? ", new Object[]{customerId});
		
	}
	
	/**
	 * method to get Customer user name and password	 
	 * @param userName, user name of a customer
	 * @param password , password of a customer
	 * @return Customer
	 */
	public Customer getCustomerByUserNameAndPassword(String userNmae,
			String password) {
		// TODO Auto-generated method stub
		return findSingle("from Customer where userName = ? and password = ?", new Object[]{userNmae,password});
	}
	/**
	 * method to get Customer by email	 
	 * @param email, email address of a customer
	 * @return Customer
	 */
	public Customer getCustomerByEmail(String email) {
		return findSingle("from Customer where email= ? ", new Object[]{email});
	}
	
	public Customer getCustomerByPhone(String phone) {
		return findSingle("from Customer where phone= ? ", new Object[]{phone});
	}
	
	public Customer getOtpVerifiedCustomerByPoneAndProductTypeExceptThisCustomer(String phone,ProductType productType,Integer customerId) {
		return findSingle("from Customer where isOtpVerified=? and phone= ? and productType=? and id<>? ", new Object[]{Boolean.TRUE,phone,productType,customerId});
	}
	
	/**
	 * method to get Customer by userId	 
	 * @param userId, userId of a customer
	 * @return Customer
	 */
	public Customer getCustomerByUserId(String userId) {
		return findSingle("from Customer where userId= ? ", new Object[]{userId});
	}

	public Customer getCustomerByCompanyId(Integer companyId){
		return findSingle("from Customer where companyId= ? ", new Object[]{companyId});
	}
	
	/**
	 * method to get Customer by email and product type 
	 * @param email, email address of a customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 */
	public Customer getCustomerByEmailByProductType(String email,ProductType productType) {
		return findSingle("from Customer where email= ? and productType=?", new Object[]{email,productType});
	}
	
	public Customer getCustomerByPhoneAndProductType(String phone,ProductType productType) {
		return findSingle("from Customer where phone= ? and productType=?", new Object[]{phone,productType});
	}
	
	public Customer getCustomerByUserIdAndProductType(String userId,ProductType productType) {
		return findSingle("from Customer where userId= ? and productType=?", new Object[]{userId,productType});
	}
	
	public Customer getCustomerByEmailAndProductTypeExceptThisCustomer(String email,ProductType productType,Integer customerId) {
		return findSingle("from Customer where email= ? and productType=? and id<>? ", new Object[]{email,productType,customerId});
	}
	public Customer getCustomerByUserIdAndProductTypeExceptThisCustomer(String userId,ProductType productType,Integer customerId) {
		return findSingle("from Customer where userId= ? and productType=? and id<>? ", new Object[]{userId,productType,customerId});
	}
	/**
	 * method to get Customer by email or user id and product type 
	 * @param email, email address of a customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 */
	
	public Customer getCustomerByUserNameByProductType(String userName,ProductType productType) {
		return findSingle("from Customer where (email= ? or userId= ?) and productType=?", new Object[]{userName,userName,productType});
	}
	
	/**
	 * method to get Customer by deviceId and product type 
	 * @param socialAccountId, FaceBook Account Id of the customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 */
	public Customer getCustomerBySocialAcctIdByProductType(String socialAccountId,ProductType productType) {
		return findSingle("from Customer where socialAccountId= ? ", new Object[]{socialAccountId});
	}

	public void updateCustImagePath(String imageExt, Integer customerId) {
		bulkUpdate("UPDATE Customer set custImagePath = ?, lastUpdatedDate = ? where id = ? ",new Object[]{imageExt,new Date(),customerId});
	}
	public void updateCustBlockedStatus(Boolean blockedStatus, Integer customerId) {
		bulkUpdate("UPDATE Customer set isBlocked = ?, lastUpdatedDate = ? where id = ? ",new Object[]{blockedStatus,new Date(),customerId});
	}
	
	public Customer getCustomerByReferralCode(String referralCode) {
		return findSingle("from Customer where referrerCode= ? ", new Object[]{referralCode});
	}
	public List <Customer> getAll() {
		return find("from Customer");
		
	}
	
	public List <Customer> getAllCustomersByLAstUpdateTime(Date lastUpdatedDate) {
		return find("from Customer where lastUpdatedDate>= ?", new Object[] {lastUpdatedDate});
		
	}
	public List <Customer> getAllOtpVerifiedCustomers() {
		return find("from Customer where isOtpVerified= ? ", new Object[] {Boolean.TRUE});
		
	}
	public List <Customer> getAllRewardTheFanCustomers() {
		return find("from Customer where productType=? ", new Object[] {ProductType.REWARDTHEFAN});
		
	}
	public void updateCustomerForTicketPurchase(Integer customerId) {
		bulkUpdate("UPDATE Customer set eligibleToShareRefCode = ? where id = ? ",new Object[]{true,customerId});
	}
	
	public void updateQuizCustomerLives(Integer customerId,Integer customerLives) {
		bulkUpdate("UPDATE Customer set quizCustomerLives = ? where id = ? ",new Object[]{customerLives,customerId});
	}
	public void updateQuizCustomerLivesAndRtfPoints(Integer customerId,Integer customerLives,Integer rtfPoints) {
		bulkUpdate("UPDATE Customer set quizCustomerLives = ?,rtfPoints=? where id = ? ",new Object[]{customerLives,rtfPoints,customerId});
	}
	
	public void updateLivesUsedByCustomer(Integer customerId,Integer livesUsed) {
		bulkUpdate("UPDATE Customer set quizCustomerLives = quizCustomerLives - ?  where id = ? ",new Object[]{livesUsed,customerId});
	}

	public List<Customer> getAllCustomersWithoutUserId(ProductType productType) {
		return find("from Customer where productType=? and (userId =? or userId = ?) ",new Object[]{productType,null,""});
	}
	
	public List<Customer> getAllCustomersWithoutUserIdForAutoUserIDCreation(ProductType productType,Date toDate) {
		return find("from Customer where productType=? and signupDate<? and (userId =? or userId = ?) ",new Object[]{productType,toDate,null,""});
	}
	
	public List<CustomerSearchDetails> getCustomerAutosearchForQuiz(String searchKey,Integer customerId) {
		Session session = getSession();
		List<CustomerSearchDetails> customerSearchDetailsList = null;
	try {
		/*eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}*/
		
		//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
		String queryStr = "select  top 15 id as customerId,user_id as userId,cust_name as customerName,last_name as lastName," +
				" cust_image_path as custImagePath " +
				" from customer where product_type='REWARDTHEFAN' and is_otp_verified=1  " +
				//" and cust_image_path is not null and user_id is not null" +
				" and user_id like '"+searchKey+"%' ";
		if(customerId != null) {
			queryStr += " and id != "+customerId;
		}
		queryStr += " order by user_id";
		
		SQLQuery query = session.createSQLQuery(queryStr);
		//List<Object[]> result = (List<Object[]>)query.list();
		query.addScalar("customerId",Hibernate.INTEGER);
		query.addScalar("customerName",Hibernate.STRING);
		query.addScalar("lastName",Hibernate.STRING);
		query.addScalar("userId",Hibernate.STRING);
		query.addScalar("custImagePath",Hibernate.STRING);

		customerSearchDetailsList = query.setResultTransformer(Transformers.aliasToBean(CustomerSearchDetails.class)).list();
		
		/*List<CustomerSearchDetails> customerSearchDetailsList = new ArrayList<CustomerSearchDetails>();
		CustomerSearchDetails customerSearchDetails = null;
		try{
			for (Object[] object : result) {
				customerSearchDetails = new CustomerSearchDetails();
				customerSearchDetails.setCustomerId((Integer)object[0]);
				customerSearchDetails.setCustomerName((String)object[1]);
				customerSearchDetails.setLastName((String)object[2]);
				customerSearchDetails.setUserId((String)object[3]);
				customerSearchDetails.setCustImagePath((String)object[4]);
				customerSearchDetailsList.add(customerSearchDetails);				
			}
		}catch(Exception e){
			e.printStackTrace();
		}*/
		
		return customerSearchDetailsList;
		}catch (Exception e) {
			/*if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}*/
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return null;
	
	}
	
	/**
	 * method to get Customer by userId	
	 * @param userId, userId of a customer
	 * @param customerId, Id of a customer
	 * @return Customer
	 */
	public Customer getCustomerByUserIdExceptThisCustomer(String userId,Integer customerId) {
		return findSingle("from Customer where userId= ? and id <> ?", new Object[]{userId,customerId});
	}
	
	public List<Customer> getCustomersByUserNameByProductType(String userName,ProductType productType) {
		return find("from Customer where (email= ? or userId= ?) and productType=?", new Object[]{userName,userName,productType});
	}
	
	public List <Customer> getAllCustomerEmailToBeSent() {
		return find("from Customer where isEmailed= ? and productType=? ", new Object[] {Boolean.FALSE,ProductType.REWARDTHEFAN});
	}
	
	public void updateQuizCustomerRtfPoints(Integer customerId,Integer rtfPoints) {
		bulkUpdate("UPDATE Customer set rtfPoints = rtfPoints + ? where id = ? ",new Object[]{rtfPoints,customerId});
	}
	
	public void updatePointsRedemption(Integer customerId,Integer rtfPoints) {
		bulkUpdate("UPDATE Customer set rtfPoints = rtfPoints - ? where id = ? ",new Object[]{rtfPoints,customerId});
	}
	
	public void updateMagicWandsRedemption(Integer customerId,Integer magicWands) {
		bulkUpdate("UPDATE Customer set magicWands = magicWands + ? where id = ? ",new Object[]{magicWands,customerId});
	}
	public List <Customer> getAllOtpVerifiedCustomers1() {
		return find("from Customer where isOtpVerified= ? ", new Object[] {Boolean.TRUE});
	}
	
	public void updateSfLevelNo(Integer customerId,Integer levelNo) {
		bulkUpdate("UPDATE Customer set sfQuesLevel = ?  where id = ? ",new Object[]{levelNo,customerId});
	}
	
	public void resetCustomerSuperFanLevels() {
		bulkUpdate("UPDATE Customer set sfQuesLevel = ?  ",new Object[]{0});
	}
	public void updateCustomerLoyaltyPoints(Integer customerId,Integer loyaltyPoints) {
		bulkUpdate("UPDATE Customer set loyaltyPoints = loyaltyPoints + ? where id = ? ",new Object[]{loyaltyPoints,customerId});
	}
	 
}
