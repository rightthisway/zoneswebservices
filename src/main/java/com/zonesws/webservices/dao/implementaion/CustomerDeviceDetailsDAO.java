package com.zonesws.webservices.dao.implementaion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.Status;
/**
 * class having db related methods for Customer App Device Details
 * @author Ulaganathan
 *
 */
public class CustomerDeviceDetailsDAO extends HibernateDAO<Integer, CustomerDeviceDetails> implements com.zonesws.webservices.dao.services.CustomerDeviceDetailsDAO {
	
	private static Session eventSession = null;
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	
	public static Session getEventSession() {
		return CustomerDeviceDetailsDAO.eventSession;
	}

	public static void setEventSession(Session eventSession) {
		CustomerDeviceDetailsDAO.eventSession = eventSession;
	}
	
	public List<CustomerDeviceDetails> getAllActiveDeviceDetailsByCustomerId(Integer customerId){
		return find("from CustomerDeviceDetails where status='ACTIVE' and customerId = ?", new Object[]{customerId});
	}
	
	public List<CustomerDeviceDetails> getAllActiveDeviceDetails(){
		return find("from CustomerDeviceDetails where status='ACTIVE' order by customerId", new Object[]{});
	}
	
	public CustomerDeviceDetails getActiveDeviceDetailsByCustomerIdAndDeviceId(Integer customerId,String deviceId){
		return findSingle("from CustomerDeviceDetails where status='ACTIVE' and customerId = ? and deviceId= ?", new Object[]{customerId,deviceId});
	}
	
	public CustomerDeviceDetails getActiveDeviceDetailsByDeviceId(String deviceId){
		return findSingle("from CustomerDeviceDetails where status='ACTIVE' and deviceId= ?", new Object[]{deviceId});
	}
	
	public CustomerDeviceDetails getActiveDeviceDetailsByDeviceIdAndNotificationRegId(String deviceId,String notificationRegId){
		return findSingle("from CustomerDeviceDetails where status='ACTIVE' and deviceId= ? and notificationRegId=?", new Object[]{deviceId,notificationRegId});
	}
	
	public Map<Integer, List<CustomerDeviceDetails>> getAllActiveFavoriteArtistCustomerDeviceDetails(List<Integer> artistIds){
		
		String list = artistIds.toString().replace("[", "").replace("]", "").replaceAll(", ", ",");
		try {
			String whereClause="select cfa.customer_id , cfa.artist_id," +
					"CONVERT(VARCHAR(200),REPLACE(cp.device_id,',','')) as device_id," +
					"CONVERT(VARCHAR(200),REPLACE(cp.notification_reg_id,',','')) as notification_reg_id," +
					"CONVERT(VARCHAR(10),REPLACE(cp.application_platform,',','')) as application_platform from " +
					"cust_favorites_artist_handler cfa inner join cust_app_device_details cp on cfa.customer_id=cp.cust_id where " +
					"cfa.status='ACTIVE' and cp.status='ACTIVE' and cp.application_platform in ('IOS','ANDROID') " +
					" and cfa.artist_id in ("+list+") and cp.notification_reg_id is not null order by cfa.customer_id ";
			
			
			
			eventSession = CustomerDeviceDetailsDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				CustomerDeviceDetailsDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			
			Map<Integer, List<CustomerDeviceDetails>> artistCustDeviceMap = new HashMap<Integer, List<CustomerDeviceDetails>>();
			
			CustomerDeviceDetails customerDeviceDetails=null;
			
			for (Object[] object : result) {
				customerDeviceDetails= new CustomerDeviceDetails();
				customerDeviceDetails.setCustomerId((Integer)object[0]);
				customerDeviceDetails.setArtistId((Integer)object[1]);
				customerDeviceDetails.setDeviceId((String)object[2]);
				customerDeviceDetails.setNotificationRegId((String)object[3]);
				customerDeviceDetails.setApplicationPlatForm(ApplicationPlatForm.valueOf((String)object[4]));
				
				List<CustomerDeviceDetails> finalList = artistCustDeviceMap.get(customerDeviceDetails.getArtistId());
				if(null != finalList && !finalList.isEmpty()){
					artistCustDeviceMap.get(customerDeviceDetails.getArtistId()).add(customerDeviceDetails);
				}else{
					finalList = new ArrayList<CustomerDeviceDetails>();
					finalList.add(customerDeviceDetails);
					artistCustDeviceMap.put(customerDeviceDetails.getArtistId(), finalList);
				}
			}
			return artistCustDeviceMap;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen() ){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}
	
	
 public Map<Integer, List<CustomerDeviceDetails>> getAllActiveLoaylFanCustomerDeviceDetails(List<Integer> artistIds){
		
		String list = artistIds.toString().replace("[", "").replace("]", "").replaceAll(", ", ",");
		try {
			String whereClause="select cfa.customer_id , cfa.artist_id," +
					"CONVERT(VARCHAR(200),REPLACE(cp.device_id,',','')) as device_id," +
					"CONVERT(VARCHAR(200),REPLACE(cp.notification_reg_id,',','')) as notification_reg_id," +
					"CONVERT(VARCHAR(10),REPLACE(cp.application_platform,',','')) as application_platform from " +
					"customer_super_fan cfa inner join cust_app_device_details cp on cfa.customer_id=cp.cust_id where " +
					" cp.status='ACTIVE' and cp.application_platform in ('IOS','ANDROID') " +
					" and cfa.artist_id in ("+list+") and cp.notification_reg_id is not null order by cfa.customer_id ";
			
			
			
			eventSession = CustomerDeviceDetailsDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				CustomerDeviceDetailsDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			
			Map<Integer, List<CustomerDeviceDetails>> artistCustDeviceMap = new HashMap<Integer, List<CustomerDeviceDetails>>();
			
			CustomerDeviceDetails customerDeviceDetails=null;
			
			for (Object[] object : result) {
				customerDeviceDetails= new CustomerDeviceDetails();
				customerDeviceDetails.setCustomerId((Integer)object[0]);
				customerDeviceDetails.setArtistId((Integer)object[1]);
				customerDeviceDetails.setDeviceId((String)object[2]);
				customerDeviceDetails.setNotificationRegId((String)object[3]);
				customerDeviceDetails.setApplicationPlatForm(ApplicationPlatForm.valueOf((String)object[4]));
				
				List<CustomerDeviceDetails> finalList = artistCustDeviceMap.get(customerDeviceDetails.getArtistId());
				if(null != finalList && !finalList.isEmpty()){
					artistCustDeviceMap.get(customerDeviceDetails.getArtistId()).add(customerDeviceDetails);
				}else{
					finalList = new ArrayList<CustomerDeviceDetails>();
					finalList.add(customerDeviceDetails);
					artistCustDeviceMap.put(customerDeviceDetails.getArtistId(), finalList);
				}
			}
			return artistCustDeviceMap;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen() ){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}

 
 public void updateCustomerDeviceDetails(Integer customerId, String deviceId) throws Exception{
	 bulkUpdate("UPDATE CustomerDeviceDetails set status=?,lastUpdated=? WHERE customerId=? and deviceId=?", 
			 new Object[]{String.valueOf(Status.DELETED),new Date(),customerId,deviceId});
}
	
}
