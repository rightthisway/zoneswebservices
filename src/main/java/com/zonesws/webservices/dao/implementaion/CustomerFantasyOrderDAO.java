package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerFantasyOrder;

public class CustomerFantasyOrderDAO extends HibernateDAO<Integer, CustomerFantasyOrder> implements com.zonesws.webservices.dao.services.CustomerFantasyOrderDAO{

	public CustomerFantasyOrder getFSTOrder(Integer orderId,Integer customerId){
		return findSingle("FROM CustomerFantasyOrder where id=? and customerId=? ", new Object[]{orderId,customerId});
	}
	
	public List<CustomerFantasyOrder> getAllActiveOrderByCustomerId(Integer customerId){
		return find("FROM CustomerFantasyOrder where customerId=? and status <> 'PENDING' order by id desc ", new Object[]{customerId});
	}
	
	/*public List<CustomerFantasyOrder> getAllActiveOrderByCustomerId(Integer customerId){
		return find("FROM FutureOrderConfirmation where customerId=? and status <> 'PENDING' order by id ", new Object[]{customerId});
	}*/
	
	/*public List<FutureOrderConfirmation> getOrdersByCustomerIdandOrderType(Integer customerId,OrderFetchType orderFetchType,Integer inculdeDays,OrderType orderType){
		Session session = null;
		String conditionSymbol = orderFetchType.equals(OrderFetchType.PastOrders)?"<":">=";
		List<CustomerOrder> customerOrders= null;
		try {
			session = getSessionFactory().openSession();
			
			String sql = "select * from customer_order where DATEDIFF(DAY,GETDATE(),event_date) "+conditionSymbol+" "+inculdeDays+" AND " +
					"customer_id="+customerId+" ";
			if(orderType.equals(OrderType.REGULAR)){
				sql += " AND order_type in ('"+String.valueOf(OrderType.REGULAR)+"','"+String.valueOf(OrderType.LOYALFAN)+"') ";
			}else{
				sql += " AND order_type='"+orderType.toString()+"' ";
			}
			sql += " order by created_date desc";
			
			Query query = session.createSQLQuery(sql).addEntity(CustomerOrder.class)
					.addScalar("id",Hibernate.INTEGER)
					.addScalar("customer_id",Hibernate.INTEGER)
					.addScalar("quantity",Hibernate.INTEGER)
					.addScalar("status",Hibernate.STRING)
					.addScalar("section",Hibernate.STRING)
					.addScalar("row",Hibernate.STRING)
					.addScalar("seat_low",Hibernate.STRING)
					.addScalar("seat_high",Hibernate.STRING)
					.addScalar("event_id",Hibernate.INTEGER)
					.addScalar("event_name",Hibernate.STRING)
					.addScalar("event_date",Hibernate.DATE)
					.addScalar("event_time",Hibernate.TIME)
					.addScalar("venue_id",Hibernate.INTEGER)
					.addScalar("venue_name",Hibernate.STRING)
					.addScalar("ticket_price",Hibernate.DOUBLE)
					.addScalar("order_total",Hibernate.DOUBLE)
					.addScalar("discount_amt",Hibernate.DOUBLE)
					.addScalar("ticket_group_notes",Hibernate.STRING)
					.addScalar("created_date",Hibernate.DATE)
					.addScalar("last_updated",Hibernate.DATE)
					.addScalar("accepted_date",Hibernate.DATE)
					.addScalar("accepted_by",Hibernate.STRING)
					.addScalar("product_type",Hibernate.STRING)
					.addScalar("shipping_method",Hibernate.STRING);
			
			List<Object[]> list = query.list();
			if(list!=null && !list.isEmpty()){
				customerOrders = new ArrayList<CustomerOrder>();
				for(Object[] ob : list){
					customerOrders.add((CustomerOrder)ob[0]);
				}
			}
			return customerOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}*/

}
