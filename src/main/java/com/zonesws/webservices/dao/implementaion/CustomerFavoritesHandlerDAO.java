package com.zonesws.webservices.dao.implementaion; 

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CustomerFavoritesHandler;
import com.zonesws.webservices.enums.Status;
/**
 * class having method related to CustomerFavoritesHandler
 * @author Ulaganathan
 *
 */
public class CustomerFavoritesHandlerDAO extends HibernateDAO<Integer,CustomerFavoritesHandler> implements
 com.zonesws.webservices.dao.services.CustomerFavoritesHandlerDAO {

 
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<CustomerFavoritesHandler> getAll() {
		return find("FROM CustomerFavoritesHandler");	 
	}
	/**
	 * method to get child category using its name
	 * @param name, name of child category  
	 * @return ChildCategory
	 */	
	public CustomerFavoritesHandler getChildCategoryByName(String name) {
		 return findSingle("FROM CustomerFavoritesHandler where name = ?", new Object[]{name});	 
			
	}
	/**
	 * method to get child category using its id
	 * @param id, child category id
	 * @return ChildCategory
	 */
	public CustomerFavoritesHandler getChildCategoryById(Integer id) {
		  return findSingle("FROM CustomerFavoritesHandler where id = ?", new Object[]{id});	 
			
	}
	
	/**
	 * method to get child category using its parent category name
	 * @param parentCategoryName, parent category name
	 * @return ChildCategory
	 */
	public List<CustomerFavoritesHandler> getChildCategoryByParentCategoryName(
			String parentCategoryName) {
		return find("FROM CustomerFavoritesHandler WHERE parentCategory.name like ? order by name" ,new Object[]{parentCategoryName});
		
	}
	public List<CustomerFavoritesHandler> getallArtistByCustomerId(Integer customerId) {
		return find("FROM CustomerFavoritesHandler WHERE customerId=? and status=? order by artistName" ,new Object[]{customerId,com.zonesws.webservices.enums.Status.ACTIVE});
	}
	
	public List<CustomerFavoritesHandler> getAllActiveFavoriteArtist() {
		return find("FROM CustomerFavoritesHandler WHERE status=? order by artistName" ,new Object[]{com.zonesws.webservices.enums.Status.ACTIVE});
	}
	
	public CustomerFavoritesHandler getFavoriteArtistByArtistIdAndCustomerId(Integer artistId,Integer customerId) {
		  return findSingle("FROM CustomerFavoritesHandler where artistId = ? and customerId=? ", new Object[]{artistId,customerId});	 
	}
	
	
	public void updateFavoriteArtistByCustomer(List<Integer> artistIds, Integer customerId) {
		String sql ="update cust_favorites_artist_handler set status = 'DELETED',last_updated=GETDATE() where customer_id="+customerId+" " +
				" and artist_id in (:artistIds) ";
		Query query = null;
		Session session = null;
		try {
			session = getSession();
			query = session.createSQLQuery(sql).setParameterList("artistIds", artistIds);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally  {
			session.close();
		}
	}
	
	public List<CustomerFavoritesHandler> getAllActiveFavArtistByCustomerId(Integer customerId) {
		return find("FROM CustomerFavoritesHandler where customerId=? and status=? ", new Object[]{customerId,Status.ACTIVE});
	}
	public List<CustomerFavoritesHandler> getAllFavoriteArtistByCustomerId(Integer customerId) {
		  return find("FROM CustomerFavoritesHandler where customerId=? ", new Object[]{customerId});	 
			
	}
}
