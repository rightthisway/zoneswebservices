package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerFavouriteEvent;
import com.zonesws.webservices.enums.ProductType;

/**
 * class having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public class CustomerFavouriteEventDAO extends HibernateDAO<Integer, CustomerFavouriteEvent> implements com.zonesws.webservices.dao.services.CustomerFavouriteEventDAO{
	
	public List<CustomerFavouriteEvent> getAllActiveFavouriteEventsByCustomerId(Integer customerId) {
		return find("from CustomerFavouriteEvent where customerId=? and status='ACTIVE' ", new Object[]{customerId});
				
	}
	
	
	public List<CustomerFavouriteEvent> getAllActiveFavouriteEventsByCustomerIdAndProduct(Integer customerId,ProductType productType) {
		return find("SELECT fc FROM CustomerFavouriteEvent fc ,Customer c  where fc.customerId=c.id AND c.productType=? AND " +
				" fc.customerId=? AND fc.status='ACTIVE' ", new Object[]{productType,customerId});
				
	}
	
	public List<Integer> getAllActiveFavouriteEventIdsByCustomerId(Integer customerId) {
		return find("select eventId from CustomerFavouriteEvent where customerId=? and status='ACTIVE' ", new Object[]{customerId});
				
	}
	
	public List<Integer> getAllActiveFavouriteEventIdsByCustomerIdByProduct(Integer customerId,ProductType productType) {
		return find("select fc.eventId from CustomerFavouriteEvent fc ,Customer c  where fc.customerId=c.id and fc.customerId=? " +
				"and fc.status='ACTIVE' and c.productType=? ", new Object[]{customerId,productType});
				
	}
	
	public CustomerFavouriteEvent getFavouriteEventsByCustomerIdEventId(Integer customerId,Integer eventId){
		return findSingle("from CustomerFavouriteEvent where customerId=? and eventId=? ", new Object[]{customerId,eventId});
	}
	
	public CustomerFavouriteEvent getFavouriteEventsByCustomerIdEventIdAndProduct(Integer customerId,Integer eventId,ProductType productType){
		return findSingle("SELECT fc FROM CustomerFavouriteEvent fc ,Customer c  where fc.customerId=c.id AND c.productType=? AND " +
				" fc.customerId=? AND fc.eventId=? ", new Object[]{productType,customerId,eventId});
		//return findSingle("from CustomerFavouriteEvent where customerId=? and eventId=? ", new Object[]{customerId,eventId});
	}
	
	
	public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId) {
		bulkUpdate("UPDATE CustomerFavouriteEvent set status='DELETE' where customerId=? and eventId=?",new Object[]{customerId,eventId});
	}
	
	
	public List<CustomerFavouriteEvent> getAllActiveFavouriteEvents() {
		return find("from CustomerFavouriteEvent where status='ACTIVE'");
				
	}
	

}
