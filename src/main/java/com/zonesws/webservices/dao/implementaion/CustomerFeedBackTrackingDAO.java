package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerFeedBackTracking;

/**
 * class having db related methods for QuizAffiliateRewardHistory
 * @author Ulaganathan
 *
 */
public class CustomerFeedBackTrackingDAO extends HibernateDAO<Integer, CustomerFeedBackTracking> implements com.zonesws.webservices.dao.services.CustomerFeedBackTrackingDAO{
	
	
}
