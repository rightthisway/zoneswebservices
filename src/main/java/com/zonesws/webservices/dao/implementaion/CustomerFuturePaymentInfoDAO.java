package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CustomerFuturePaymentInfo;


public class CustomerFuturePaymentInfoDAO extends HibernateDAO<Integer, CustomerFuturePaymentInfo> implements com.zonesws.webservices.dao.services.CustomerFuturePaymentInfoDAO{

	public CustomerFuturePaymentInfo getRefreshTokenByCustomerId(Integer customerId){
		return findSingle("FROM CustomerFuturePaymentInfo WHERE customerId = ?", new Object[]{customerId});
	}
	
	
}
