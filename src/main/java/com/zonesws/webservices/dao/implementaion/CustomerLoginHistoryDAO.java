package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerLoginHistory;
import com.zonesws.webservices.enums.ApplicationPlatForm;
/**
 * class having db related methods for Customer Login history
 * @author Ulaganathan
 *
 */
public class CustomerLoginHistoryDAO extends HibernateDAO<Integer, CustomerLoginHistory> implements com.zonesws.webservices.dao.services.CustomerLoginHistoryDAO {
	
	
	public List<CustomerLoginHistory> getAllLoginHistoryByCustomerId(Integer customerId){
		
		return find("from CustomerLoginHistory where customerId = ?", new Object[]{customerId});
		
	}
	
	public List<CustomerLoginHistory> getAllMobileLoginHistoryByCustomerId(Integer customerId){
		
		return find("from CustomerLoginHistory where customerId = ? and (applicationPlatForm=? or applicationPlatForm=?)" +
				" and notificationRegId not null order by lastLoginTime desc", new Object[]{customerId,ApplicationPlatForm.ANDROID,ApplicationPlatForm.IOS});
		
	}
	/**
	 * method to get Customer by email and password
	 * @param email, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 *//*
	public List<Customer> getCustomer(String email,String password){
		
		return find("from Customer where email = ? and password =? and locked = ? ", new Object[]{email,password,Status.ACTIVE });
		
	} 
	
	*//**
	 * method to get Country by email
	 * @param email, email address of a customer	
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailId(String emailId) {
		return findSingle("from Customer where email = ? and locked = ? ", new Object[]{emailId,Status.ACTIVE});
	}
	*//**
	 * method to get Customer by email and password
	 * @param emailId, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailIdAndPassword(String emailId, String password) {
		return findSingle("from Customer where email = ? and locked = ? and password = ?", new Object[]{emailId,Status.ACTIVE,password});
	}
	*//**
	 * method to get Customer by userName
	 * @param userName, user name of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByUserName(String userName) {
		return findSingle("from Customer where userName = ? and locked = ? ", new Object[]{userName,Status.ACTIVE});
	}
	*//**
	 * method to get Customer by email and user name
	 * @param emailId, email address of a customer
	 * @param userName, user name of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailAndUserName(String email, String userName) {
		
		return findSingle("from Customer where email= ? and userName = ? and locked = ? ", new Object[]{email,userName,Status.ACTIVE});
	}
	*//**
	 *  method to get Customer by customer Id
	 * @param customerId,  customer Id
	 * @return Customer
	 *//*
	public Customer getCustomerById(Integer customerId) {
		return findSingle("from Customer where id= ?  and locked = ? ", new Object[]{customerId,Status.ACTIVE});
		
	}
	
	*//**
	 *  method to get Customer by customer Id
	 * @param customerId,  customer Id
	 * @return Customer
	 *//*
	public Customer getCustomerByIdByProduct(Integer customerId,ProductType productType) {
		return findSingle("from Customer where id= ? and productType = ? ", new Object[]{customerId,productType});
		
	}
	
	*//**
	 * method to get Customer user name and password	 
	 * @param userName, user name of a customer
	 * @param password , password of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByUserNameAndPassword(String userNmae,
			String password) {
		// TODO Auto-generated method stub
		return findSingle("from Customer where userName = ? and locked = ? and password = ?", new Object[]{userNmae,Status.ACTIVE,password});
	}
	*//**
	 * method to get Customer by email	 
	 * @param email, email address of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByEmail(String email) {
		return findSingle("from Customer where email= ? and locked = ? ", new Object[]{email,Status.ACTIVE});
	}

	public Customer getCustomerByCompanyId(Integer companyId){
		return findSingle("from Customer where companyId= ? and locked = ? ", new Object[]{companyId,Status.ACTIVE});
	}
	
	*//**
	 * method to get Customer by email and product type 
	 * @param email, email address of a customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailByProductType(String email,ProductType productType) {
		return findSingle("from Customer where email= ?  and productType=? ", new Object[]{email,productType});
	}
	
	*//**
	 * method to get Customer by deviceId and product type 
	 * @param socialAccountId, FaceBook Account Id of the customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 *//*
	public Customer getCustomerBySocialAcctIdByProductType(String socialAccountId,ProductType productType) {
		return findSingle("from Customer where socialAccountId= ?  and productType=? ", new Object[]{socialAccountId,productType});
	}*/
	
}
