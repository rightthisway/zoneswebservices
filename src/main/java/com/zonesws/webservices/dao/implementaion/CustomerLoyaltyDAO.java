package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.CustomerLoyalty;

/**
 * class having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public class CustomerLoyaltyDAO extends HibernateDAO<Integer, CustomerLoyalty> implements com.zonesws.webservices.dao.services.CustomerLoyaltyDAO{
	
	/*public List<CustomerLoyalty> getAllActiveFavouriteEventsByCustomerId(Integer customerId) {
		return find("from CustomerLoyalty where customerId=? and status='ACTIVE' ", new Object[]{customerId});
				
	}

	
	public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId) {
		bulkUpdate("UPDATE CustomerLoyalty set status='DELETE' where customerId=? and eventId=?",new Object[]{customerId,eventId});
	}
	*/
	public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId){
		return findSingle("from CustomerLoyalty where customerId=? ", new Object[]{customerId});
	}
	
	public void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set activePointsAsDouble= ?, latestSpentPointsAsDouble=?, totalSpentPointsAsDouble = ? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{pointsRemaining, latestSpendPoints, totalSpendPoints, customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints(){
		return find("from CustomerLoyalty where activePointsAsDouble > 0  order by  customerId");
	}
	
	public void updateCustomerLoyaltyByCustomer(Double earnPoints,  Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set totalEarnedPointsAsDouble=totalEarnedPointsAsDouble + ?," +
					"latestEarnedPointsAsDouble = ?, activePointsAsDouble= activePointsAsDouble + ? , pendingPointsAsDouble = pendingPointsAsDouble - ?, lastUpdate=? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{earnPoints, earnPoints, earnPoints,earnPoints,new Date(), customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateCustomerLoyaltyByCustomerForManualCredits(Double earnPoints,  Integer customerId) {
		
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set totalEarnedPointsAsDouble=totalEarnedPointsAsDouble + ?," +
					"latestEarnedPointsAsDouble = ?, activePointsAsDouble= activePointsAsDouble + ? , lastUpdate=? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{earnPoints, earnPoints, earnPoints,new Date(), customerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateCustomerLoyaltyById(Integer id,String message) {
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set notifiedMessage= ?,lastNotifiedTime = ? where id = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{message, new Date(), id});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateCustomerLoyaltyByCustomerId(Integer custId,String message) {
		try {
			String SQL_UPDATE_CUST_LOYALTY = "update CustomerLoyalty set notifiedMessage= ?,lastNotifiedTime = ? where customerId = ?";
			bulkUpdate(SQL_UPDATE_CUST_LOYALTY, new Object[]{message, new Date(), custId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
