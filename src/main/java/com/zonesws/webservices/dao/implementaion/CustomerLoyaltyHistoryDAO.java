package com.zonesws.webservices.dao.implementaion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.RewardBreakUpType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.list.RewardDetails;

/**
 * class having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public class CustomerLoyaltyHistoryDAO extends HibernateDAO<Integer, CustomerLoyaltyHistory> implements com.zonesws.webservices.dao.services.CustomerLoyaltyHistoryDAO{
	
	private static SimpleDateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private static Session rewardSession = null;
	
	
	public static Session getRewardSession() {
		return CustomerLoyaltyHistoryDAO.rewardSession;
	}

	public static void setRewardSession(Session rewardSession) {
		CustomerLoyaltyHistoryDAO.rewardSession = rewardSession;
	}

	
	
	/*public List<CustomerLoyalty> getAllActiveFavouriteEventsByCustomerId(Integer customerId) {
		return find("from CustomerLoyalty where customerId=? and status='ACTIVE' ", new Object[]{customerId});
				
	}

	
	public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId) {
		bulkUpdate("UPDATE CustomerLoyalty set status='DELETE' where customerId=? and eventId=?",new Object[]{customerId,eventId});
	}
	*/
	public List<CustomerLoyaltyHistory> getCustomerContestReferralRewards(Integer customerId){
		return find("from CustomerLoyaltyHistory where customerId=? and orderType=? and contestId is not null and "
				+ "tireOneCustId is not null order by tireOneCustId", new Object[]{customerId,OrderType.CONTEST_REFERRAL});
	}
	
	public List<CustomerLoyaltyHistory> getReferralRewardsByPRimaryAndTireOneCustomerIds(Integer customerId, Integer tireOneCustomerId){
		return find("from CustomerLoyaltyHistory where customerId=? and tireOneCustId=? and orderType=? and contestId is not null", 
				new Object[]{customerId,tireOneCustomerId,OrderType.CONTEST_REFERRAL});
	}
	
	public CustomerLoyaltyHistory getCustomerLoyaltyByCustomerId(Integer customerId){
		return findSingle("from CustomerLoyaltyHistory where customerId=? ", new Object[]{customerId});
	}
	
	public CustomerLoyaltyHistory getCustomerLoyaltyByValues(Integer customerId,Integer orderId,Integer OrderItemId){
		return findSingle("from CustomerLoyaltyHistory where customerId=? and orderId=? and orderItemId=? ", new Object[]{customerId,orderId,OrderItemId});
	}
	
	/*public CustomerLoyaltyHistory getLatestOrderCustomerLoyaltyByCustomerIdandExcludeOrderId(Integer customerId,Integer excludeOrderId){
		List<CustomerLoyaltyHistory> list = find("from CustomerLoyaltyHistory where customerId=? and id <> ? order by id desc ", new Object[]{customerId,excludeOrderId});
		
		if(list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}*/

	public CustomerLoyaltyHistory getCustomerLoyaltyHistoryByOrderId(Integer orderId){
		try{
			return findSingle("FROM CustomerLoyaltyHistory WHERE orderId = ? AND (orderType=? OR orderType=? OR orderType=? OR orderType=? ) ", 
					new Object[]{orderId,OrderType.REGULAR,OrderType.LOYALFAN,OrderType.CONTEST,OrderType.TRIVIAORDER});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new CustomerLoyaltyHistory();
	}
	
	public CustomerLoyaltyHistory getCustCrownJewelLoyaltyByOrderId(Integer orderId){
		try{
			return findSingle("FROM CustomerLoyaltyHistory WHERE orderId = ? AND orderType=? ", 
					new Object[]{orderId,OrderType.FANTASYTICKETS});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new CustomerLoyaltyHistory();
	}
	
	public List<CustomerLoyaltyHistory> getAllCustomerPendingRewards(){
		
		try {
			String sql = "select rh.id ,rh.customer_id,rh.Order_id,rh.points_earned,rh.reward_earn_amount," +
			"rh.reward_conv_rate,rh.dollar_conv_rate,CONVERT(VARCHAR(19),co.created_date,120) as OrderDate,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null " +
			"THEN 'TBD' END as eventTime from cust_loyalty_reward_history rh WITH(NOLOCK) inner join customer_order co WITH(NOLOCK) on " +
			"rh.Order_id=co.id inner join invoice i WITH(NOLOCK) on i.order_id=co.id and i.status not in ('Voided','Disputed') left join event e WITH(NOLOCK) " +
			"on e.id=co.event_id where rh.status='PENDING' and rh.order_type in ('REGULAR','LOYALFAN','REFERRER','TICKETONDAYBEFOREEVENT') " +
			"and co.order_type in ('REGULAR','LOYALFAN') ";
			
			rewardSession = CustomerLoyaltyHistoryDAO.getRewardSession();
			if(rewardSession==null || !rewardSession.isOpen() || !rewardSession.isConnected()){
				rewardSession = getSession();
				CustomerLoyaltyHistoryDAO.setRewardSession(rewardSession);
			}
			Query query = rewardSession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			List<CustomerLoyaltyHistory> finalList = new ArrayList<CustomerLoyaltyHistory>();
			CustomerLoyaltyHistory rewardHistory=null;
			for (Object[] object : result) {
				try{
					rewardHistory = new CustomerLoyaltyHistory();
					rewardHistory.setId((Integer)object[0]);
					rewardHistory.setCustomerId((Integer)object[1]);
					rewardHistory.setOrderId((Integer)object[2]);
					
					BigDecimal pointsEarned = (BigDecimal)object[3];
					
					rewardHistory.setPointsEarnedAsDouble(pointsEarned.doubleValue());
					BigDecimal rewardEarnAmount = (BigDecimal)object[4];
					BigDecimal rewardConversionRate = (BigDecimal)object[5];
					BigDecimal dollarConversionRate = (BigDecimal)object[6];
					rewardHistory.setRewardEarnAmount(rewardEarnAmount.doubleValue());
					rewardHistory.setDollarConversionRate(dollarConversionRate.doubleValue());
					rewardHistory.setRewardConversionRate(rewardConversionRate.doubleValue());
					Date orderDate = dbDateTimeFormat.parse((String)object[7]);
					rewardHistory.setCreateDate(orderDate);
					rewardHistory.setEventDate((String)object[8]);
					String timeStr = DateUtil.formatTime((String)object[9]);
					rewardHistory.setEventTime(timeStr);
					finalList.add(rewardHistory);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		return finalList;
		}catch (Exception e) {
			if(rewardSession != null && rewardSession.isOpen() ){
				rewardSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public void updateRewardHistoryToActive(Integer id,boolean isEmailSent,String description) throws Exception{
		 bulkUpdate("UPDATE CustomerLoyaltyHistory set rewardStatus=?,updatedDate=?," +
		 		"isRewardsEmailSent=?,emailDescription=? WHERE rewardStatus=? and id=?", 
				 new Object[]{RewardStatus.ACTIVE,new Date(),isEmailSent,description,RewardStatus.PENDING,id});
	}
	
	public void updateRewardHistoryToActive(Integer id,Date activatedDate) throws Exception{
		 bulkUpdate("UPDATE CustomerLoyaltyHistory set updatedDate=? WHERE id=?", 
				 new Object[]{activatedDate,id});
	}
	
	public List<RewardDetails> getCustomerRewardsByRewardStatus(RewardStatus rewardStatus , Integer customerId,RewardBreakUpType breakUpType){
		
		try {
			String sql = "select rh.id ,rh.customer_id,rh.Order_id,rh.points_earned,rh.reward_earn_amount," +
			"rh.reward_conv_rate,rh.dollar_conv_rate,CONVERT(VARCHAR(19),co.created_date,120) as OrderDate,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null " +
			"THEN 'TBD' END as eventTime ,co.order_total,rh.order_type,CONVERT(VARCHAR(100),REPLACE(e.name,',','')) as eventName " +
			"from cust_loyalty_reward_history rh inner join customer_order co on " +
			"rh.Order_id=co.id left join event e on e.id=co.event_id where rh.status='"+rewardStatus+"' " +
			"and rh.order_type in ('REGULAR','LOYALFAN','REFERRER','TICKETONDAYBEFOREEVENT') and co.order_type in ('REGULAR','LOYALFAN') and " +
			"rh.customer_id ="+customerId+" ";
			
			switch (breakUpType) {
			
				case VOIDED_POINTS:
					sql = sql + " and rh.points_earned >0 ";
					break; 
					
				case REVERTED_POINTS:
					sql = sql + " and rh.points_spent > 0 ";
					break;
	
				default:
					break;
			}
		
			sql = sql + " order by co.created_date desc ";
			
			rewardSession = CustomerLoyaltyHistoryDAO.getRewardSession();
			if(rewardSession==null || !rewardSession.isOpen() || !rewardSession.isConnected()){
				rewardSession = getSession();
				CustomerLoyaltyHistoryDAO.setRewardSession(rewardSession);
			}
			Query query = rewardSession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			List<RewardDetails> finalList = new ArrayList<RewardDetails>();
			RewardDetails rewardDetails=null;
			for (Object[] object : result) {
				try{
					rewardDetails = new RewardDetails();
					rewardDetails.setOrderNo((Integer)object[2]);
					
					BigDecimal rewards = (BigDecimal)object[3];
					rewardDetails.setRewardsAsDouble(rewards.doubleValue());
					Date orderDate = dbDateTimeFormat.parse((String)object[7]);
					rewardDetails.setOrderDate(dateTimeFormat.format(orderDate));
					rewardDetails.setEventDate((String)object[8]);
					String timeStr = DateUtil.formatTime((String)object[9]);
					rewardDetails.setEventTime(timeStr);
					//rewardDetails.setEventTime((String)object[9]);
					BigDecimal orderTotal = (BigDecimal)object[10];
					rewardDetails.setOrderTotalAsDouble(orderTotal.doubleValue());
					rewardDetails.setOrderType(OrderType.valueOf((String)object[11]));
					rewardDetails.setEventName((String)object[12]);
					finalList.add(rewardDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		return finalList;
		}catch (Exception e) {
			if(rewardSession != null && rewardSession.isOpen() ){
				rewardSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	public List<RewardDetails> getCustomerRedeemedRewardsByCustomerId(Integer customerId){
		
		try {
			String sql = "select rh.id ,rh.customer_id,rh.Order_id,rh.points_spent,rh.reward_earn_amount," +
			"rh.reward_conv_rate,rh.dollar_conv_rate,CONVERT(VARCHAR(19),co.created_date,120) as OrderDate,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null " +
			"THEN 'TBD' END as eventTime ,co.order_total,rh.order_type,CONVERT(VARCHAR(100),REPLACE(e.name,',','')) as eventName " +
			"from cust_loyalty_reward_history rh inner join customer_order co on " +
			"rh.Order_id=co.id left join event e on e.id=co.event_id where rh.points_spent>0 and rh.customer_id ="+customerId+" ";
			
			rewardSession = CustomerLoyaltyHistoryDAO.getRewardSession();
			if(rewardSession==null || !rewardSession.isOpen() || !rewardSession.isConnected()){
				rewardSession = getSession();
				CustomerLoyaltyHistoryDAO.setRewardSession(rewardSession);
			}
			Query query = rewardSession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			List<RewardDetails> finalList = new ArrayList<RewardDetails>();
			RewardDetails rewardDetails=null;
			for (Object[] object : result) {
				try{
					rewardDetails = new RewardDetails();
					rewardDetails.setOrderNo((Integer)object[2]);
					BigDecimal rewards = (BigDecimal)object[3];
					rewardDetails.setRewardsAsDouble(rewards.doubleValue());
					Date orderDate = dbDateTimeFormat.parse((String)object[7]);
					rewardDetails.setOrderDate(dateTimeFormat.format(orderDate));
					rewardDetails.setEventDate((String)object[8]);
					String timeStr = DateUtil.formatTime((String)object[9]);
					rewardDetails.setEventTime(timeStr);
					//rewardDetails.setEventTime((String)object[9]);
					BigDecimal orderTotal = (BigDecimal)object[10];
					rewardDetails.setOrderTotalAsDouble(orderTotal.doubleValue());
					rewardDetails.setOrderType(OrderType.valueOf((String)object[11]));
					rewardDetails.setEventName((String)object[12]);
					finalList.add(rewardDetails);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		return finalList;
		}catch (Exception e) {
			if(rewardSession != null && rewardSession.isOpen() ){
				rewardSession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public CustomerLoyaltyHistory getLateDownloadCustomerLoyaltyHistoryByOrderId(Integer orderId){
		try{
			return findSingle("FROM CustomerLoyaltyHistory WHERE orderId = ? AND orderType=?", 
					new Object[]{orderId,OrderType.TICKETONDAYBEFOREEVENT});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new CustomerLoyaltyHistory();
	}
	
	public CustomerLoyaltyHistory getLateDownloadCustomerLoyaltyHistoryByGiftCardOrderId(Integer orderId){
		try{
			return findSingle("FROM CustomerLoyaltyHistory WHERE orderId = ? AND orderType=?", 
					new Object[]{orderId,OrderType.GIFTCARDORDER});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new CustomerLoyaltyHistory();
	}
	
	public Double getSpentPoints(Integer customerId) {
		List<Double> list = find("SELECT sum(pointsSpentAsDouble) FROM CustomerLoyaltyHistory where customerId=?",new Object[]{customerId});
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return 0.0;
	}
	
	public Double getTotalReferralPoints(Integer customerId) {
		List<Double> list = find("SELECT sum(pointsEarnedAsDouble) FROM CustomerLoyaltyHistory where customerId=? "
				+ "and orderType=? ",new Object[]{customerId,OrderType.CONTEST_REFERRAL});
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return 0.0;
	}
	
	public List<CustomerLoyaltyHistory> getMistakendata(Integer customerId){
		return find("from CustomerLoyaltyHistory where customerId=? and orderType=? and contestId <> 266 and "
				+ "tireOneCustId is not null order by id desc", new Object[]{customerId,OrderType.CONTEST_REFERRAL});
	} 
	
	public void deleteRewardsFOrContest266(Integer  customerId) throws Exception{
		 bulkUpdate("DELETE FROM CustomerLoyaltyHistory where contestId=266 and customerId=?", 
				 new Object[]{customerId});
	}
}
