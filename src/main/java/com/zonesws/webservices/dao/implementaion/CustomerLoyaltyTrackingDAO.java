package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerLoyaltyTracking;

public class CustomerLoyaltyTrackingDAO extends HibernateDAO<Integer, CustomerLoyaltyTracking> implements com.zonesws.webservices.dao.services.CustomerLoyaltyTrackingDAO{

	public List<CustomerLoyaltyTracking> getTrackingByCustomerId(Integer customerId){
		return find("FROM CustomerLoyaltyTracking WHERE customerId = ?", (new Object[]{customerId}));
	}
	
	public Double getContestPoints(Integer customerId) {
		List<Double> list = find("SELECT sum(rewardPoints) FROM CustomerLoyaltyTracking where customerId=? and rewardType=?",new Object[]{customerId,"CONTEST"});
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return 0.0;
	}
	public List<CustomerLoyaltyTracking> getRewardsByContestId(Integer contestId,Integer customerId){
		return find("FROM CustomerLoyaltyTracking WHERE contestId = ? and customerId = ?", (new Object[]{contestId,customerId}));
	}
	
}
