package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CustomerOrderCreditCard;

public class CustomerOrderCreditCardDAO extends HibernateDAO<Integer, CustomerOrderCreditCard> implements com.zonesws.webservices.dao.services.CustomerOrderCreditCardDAO{

	public CustomerOrderCreditCard getCreditDetailsByTicketGroupId(Integer ticketGroupId){
		return findSingle("FROM CustomerOrderCreditCard WHERE ticketGroupId = ?", new Object[]{ticketGroupId});
	}
	
	
}
