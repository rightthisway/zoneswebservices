package com.zonesws.webservices.dao.implementaion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.enums.OrderFetchType;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.SecondaryOrderType;

public class CustomerOrderDAO extends HibernateDAO<Integer, CustomerOrder> implements com.zonesws.webservices.dao.services.CustomerOrderDAO{
	
	public List<CustomerOrder> getOrdersByCustomerIdandOrderType(Integer customerId,OrderFetchType orderFetchType,Integer inculdeDays,OrderType orderType){
		Session session = null;
		String conditionSymbol = orderFetchType.equals(OrderFetchType.PastOrders)?"<":">=";
		List<CustomerOrder> customerOrders= null;
		try {
			session = getSessionFactory().openSession();
			
			String sql = "select * from customer_order where DATEDIFF(DAY,GETDATE(),event_date) "+conditionSymbol+" "+inculdeDays+" AND " +
							"customer_id="+customerId+" AND " +
							"status not in ('"+String.valueOf(OrderStatus.PAYMENT_PENDING)+"','"+String.valueOf(OrderStatus.PAYMENT_FAILED)+"')  " +
							"AND ( secondary_order_type is null  " +
							" or secondary_order_type like '"+String.valueOf(SecondaryOrderType.CROWNJEWEL)+"')";
			
			if(orderType.equals(OrderType.REGULAR)){
				sql += " AND order_type in ('"+String.valueOf(OrderType.REGULAR)+"','"+String.valueOf(OrderType.LOYALFAN)+"','"+String.valueOf(OrderType.CONTEST)+"'"
						+ ",'"+String.valueOf(OrderType.TRIVIAORDER)+"') ";
			}else{
				sql += " AND order_type='"+orderType.toString()+"' ";
			}
			sql += " order by created_date desc";
			
			Query query = session.createSQLQuery(sql).addEntity(CustomerOrder.class)
					.addScalar("id",Hibernate.INTEGER)
					.addScalar("customer_id",Hibernate.INTEGER)
					.addScalar("quantity",Hibernate.INTEGER)
					.addScalar("status",Hibernate.STRING)
					.addScalar("section",Hibernate.STRING)
					.addScalar("row",Hibernate.STRING)
					.addScalar("seat_low",Hibernate.STRING)
					.addScalar("seat_high",Hibernate.STRING)
					.addScalar("event_id",Hibernate.INTEGER)
					.addScalar("event_name",Hibernate.STRING)
					.addScalar("event_date",Hibernate.DATE)
					.addScalar("event_time",Hibernate.TIME)
					.addScalar("venue_id",Hibernate.INTEGER)
					.addScalar("venue_name",Hibernate.STRING)
					.addScalar("ticket_price",Hibernate.DOUBLE)
					.addScalar("order_total",Hibernate.DOUBLE)
					.addScalar("discount_amt",Hibernate.DOUBLE)
					.addScalar("ticket_group_notes",Hibernate.STRING)
					.addScalar("created_date",Hibernate.DATE)
					.addScalar("last_updated",Hibernate.DATE)
					.addScalar("accepted_date",Hibernate.DATE)
					.addScalar("accepted_by",Hibernate.STRING)
					.addScalar("product_type",Hibernate.STRING)
					.addScalar("shipping_method",Hibernate.STRING);
			
			List<Object[]> list = query.list();
			if(list!=null && !list.isEmpty()){
				customerOrders = new ArrayList<CustomerOrder>();
				for(Object[] ob : list){
					customerOrders.add((CustomerOrder)ob[0]);
				}
			}
			return customerOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}
	
	public List<CustomerOrder> getPastEventsCustomerOrders(Integer customerId){
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
		Date currentDate = new Date();
		Session session = null;
		List<CustomerOrder> customerOrders= null;
		try {
			session = getSessionFactory().openSession();
			Query query = session.createSQLQuery("select * from customer_order where (event_date <" +"'"+dateFormatter.format(currentDate)+"' OR (event_date="+"'"+dateFormatter.format(currentDate)+"' AND event_time <="+"'"+
					timeFormatter.format(currentDate)+"')) AND customer_id="+customerId).addEntity(CustomerOrder.class).addScalar("id",Hibernate.INTEGER)
					.addScalar("customer_id",Hibernate.INTEGER)
					.addScalar("quantity",Hibernate.INTEGER)
					.addScalar("status",Hibernate.STRING)
					.addScalar("section",Hibernate.STRING)
					.addScalar("row",Hibernate.STRING)
					.addScalar("seat_low",Hibernate.STRING)
					.addScalar("seat_high",Hibernate.STRING)
					.addScalar("event_id",Hibernate.INTEGER)
					.addScalar("event_name",Hibernate.STRING)
					.addScalar("event_date",Hibernate.DATE)
					.addScalar("event_time",Hibernate.TIME)
					.addScalar("venue_id",Hibernate.INTEGER)
					.addScalar("venue_name",Hibernate.STRING)
					.addScalar("ticket_price",Hibernate.DOUBLE)
					.addScalar("order_total",Hibernate.DOUBLE)
					.addScalar("discount_amt",Hibernate.DOUBLE)
					.addScalar("ticket_group_notes",Hibernate.STRING)
					.addScalar("created_date",Hibernate.DATE)
					.addScalar("last_updated",Hibernate.DATE)
					.addScalar("accepted_date",Hibernate.DATE)
					.addScalar("accepted_by",Hibernate.STRING)
					.addScalar("product_type",Hibernate.STRING)
					.addScalar("shipping_method",Hibernate.STRING);
			
			List<Object[]> list = query.list();
			if(list!=null && !list.isEmpty()){
				customerOrders = new ArrayList<CustomerOrder>();
				for(Object[] ob : list){
					customerOrders.add((CustomerOrder)ob[0]);
				}
			}
			return customerOrders;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}
	
	public CustomerOrder getCustomerOrderByOrderId(Integer orderId){
		return findSingle("FROM CustomerOrder WHERE id = ? and ( secondaryOrdertype is null or secondaryOrdertype= ?) " +
				"and status not in (?,?) ", 
				new Object[] {orderId,SecondaryOrderType.CROWNJEWEL,OrderStatus.PAYMENT_PENDING,OrderStatus.PAYMENT_FAILED});
		
		/*String sql = "select * from customer_order where DATEDIFF(DAY,GETDATE(),event_date) "+conditionSymbol+" "+inculdeDays+" AND " +
		"customer_id="+customerId+" AND ( secondary_order_type is null or secondary_order_type in ('"+String.valueOf(SecondaryOrderType.CROWNJEWEL)+"') " +
		" or secondary_order_type not like '"+String.valueOf(SecondaryOrderType.SEATGEEK)+"')";

if(orderType.equals(OrderType.REGULAR)){
	sql += " AND order_type in ('"+String.valueOf(OrderType.REGULAR)+"','"+String.valueOf(OrderType.LOYALFAN)+"') ";
}else{
	sql += " AND order_type='"+orderType.toString()+"' ";
}
sql += " order by created_date desc";*/
	}

	@SuppressWarnings("unchecked")
	public Collection<CustomerOrder> getCustomerOrderToSendEmail()
			throws Exception {
		return find("FROM CustomerOrder WHERE isInvoiceSent = ? and status=?", new Object[]{false,OrderStatus.ACTIVE});
	}
	
	public void updateOrderEmailSent(Integer orderId,String toEmail) {
		bulkUpdate("UPDATE CustomerOrder set isInvoiceSent = ? , orderToEmail=? where id = ? ",new Object[]{true,toEmail,orderId});
	}

	public void updateTicketDownloadEmailSent(Integer orderId,String toEmail) {
		bulkUpdate("UPDATE CustomerOrder set isTicketDownloadSent = ? , tdToEmail=? where id = ? ",new Object[]{true,toEmail,orderId});
	}

	public void updateOrderCancelEmailSent(Integer orderId,String toEmail) {
		bulkUpdate("UPDATE CustomerOrder set isOrderCancelSent = ? , ocToEmail=? where id = ? ",new Object[]{true,toEmail,orderId});
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<CustomerOrder> getAllCustomerActiveOrder()
			throws Exception {
		return find("FROM CustomerOrder WHERE eventDateTemp > ? ", new Object[]{new Date()});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<CustomerOrder> getAllRTFOrders()
			throws Exception {
		return find("FROM CustomerOrder WHERE productType = ? ", new Object[]{ProductType.REWARDTHEFAN});
	}
	
	public CustomerOrder getPaymentPendingOrderById(Integer orderId){
		return findSingle("FROM CustomerOrder WHERE id = ? and status=? ", 
				new Object[] {orderId,OrderStatus.PAYMENT_PENDING});
	}
	
}
