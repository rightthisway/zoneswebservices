package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CustomerOrderDetail;

public class CustomerOrderDetailDAO extends HibernateDAO<Integer, CustomerOrderDetail> implements com.zonesws.webservices.dao.services.CustomerOrderDetailDAO{

	public CustomerOrderDetail getCustomerOrderDetailByOrderId(Integer orderId){
		try{
			return findSingle("FROM CustomerOrderDetail WHERE orderId = ? and orderType != ?", new Object[]{orderId,"GIFTCARD"});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new CustomerOrderDetail();
	}
	
	
	public CustomerOrderDetail getGiftCardCustomerOrderDetailByOrderId(Integer orderId){
		try{
			return findSingle("FROM CustomerOrderDetail WHERE orderId = ? and orderType = ?", new Object[]{orderId,"GIFTCARD"});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new CustomerOrderDetail();
	}
	
}
