package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.CustomerOrderRequest;
import com.zonesws.webservices.enums.OrderStatus;

public class CustomerOrderRequestDAO extends HibernateDAO<Integer, CustomerOrderRequest> implements com.zonesws.webservices.dao.services.CustomerOrderRequestDAO{
	
	public CustomerOrderRequest getOrderRequestByOrderId(Integer orderId){
		return findSingle("FROM CustomerOrderRequest WHERE orderId = ? and status=? ", 
				new Object[] {orderId,OrderStatus.PAYMENT_PENDING});
	}
	
	public List<CustomerOrderRequest> getAllPaymentPendingOrders(Date toDate) {
		return find("From CustomerOrderRequest where status=? and createdTime<=? ",new Object[]{OrderStatus.PAYMENT_PENDING,toDate});
	}
}
