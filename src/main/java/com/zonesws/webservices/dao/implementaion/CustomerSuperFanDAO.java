package com.zonesws.webservices.dao.implementaion; 

import java.util.List;

import com.zonesws.webservices.data.CustomerSuperFan;
import com.zonesws.webservices.enums.Status;
/**
 * class having method related to CustomerSuperFan
 * @author Ulaganathan
 *
 */
public class CustomerSuperFanDAO extends HibernateDAO<Integer,CustomerSuperFan> implements
 com.zonesws.webservices.dao.services.CustomerSuperFanDAO {

 
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<CustomerSuperFan> getAll() {
		return find("FROM CustomerSuperFan");	 
	}
	/**
	 * method to get child category using its name
	 * @param name, name of child category  
	 * @return ChildCategory
	 */	
	public CustomerSuperFan getChildCategoryByName(String name) {
		 return findSingle("FROM CustomerSuperFan where name = ?", new Object[]{name});	 
			
	}
	/**
	 * method to get child category using its id
	 * @param id, child category id
	 * @return ChildCategory
	 */
	public CustomerSuperFan getChildCategoryById(Integer id) {
		  return findSingle("FROM CustomerSuperFan where id = ?", new Object[]{id});	 
			
	}
	
	/**
	 * method to get child category using its parent category name
	 * @param parentCategoryName, parent category name
	 * @return ChildCategory
	 */
	public List<CustomerSuperFan> getChildCategoryByParentCategoryName(
			String parentCategoryName) {
		return find("FROM CustomerSuperFan WHERE parentCategory.name like ? order by name" ,new Object[]{parentCategoryName});
		
	}
	public List<CustomerSuperFan> getAllSuperFansByCustomerId(Integer customerId) {
		return find("FROM CustomerSuperFan WHERE customerId = ? and status=? " ,new Object[]{customerId,Status.ACTIVE});
	}
	
	
	public CustomerSuperFan getSuperFansByCustomerIdByArtistId(Integer customerId,Integer artistId){
		return findSingle("FROM CustomerSuperFan WHERE customerId = ? AND artistId = ? AND status=?" ,new Object[]{customerId,artistId,Status.ACTIVE});
	}
	
	public CustomerSuperFan getActiveLoyalFanByCustomerId(Integer customerId){
		return findSingle("FROM CustomerSuperFan WHERE customerId = ? AND status=?" ,new Object[]{customerId,Status.ACTIVE});
	}
	
}
