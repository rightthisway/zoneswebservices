package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.CustomerTicketDownloads;


public class CustomerTicketDownloadsDAO extends HibernateDAO<Integer, CustomerTicketDownloads> implements com.zonesws.webservices.dao.services.CustomerTicketDownloadsDAO{

	public CustomerTicketDownloads getCustomerTicketDownloadsById(Integer id){
		return findSingle("FROM CustomerTicketDownloads Where id = ?" ,new Object[]{id});
	}
	
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByOrderId(List<Integer> orderId){
		return find("FROM CustomerTicketDownloads Where orderId = ?" ,new Object[]{orderId});
	}
	
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByCustomerId(List<Integer> customerId){
		return find("FROM CustomerTicketDownloads Where customerId = ?" ,new Object[]{customerId});
	}
}