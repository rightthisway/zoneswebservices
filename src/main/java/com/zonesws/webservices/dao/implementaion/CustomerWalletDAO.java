package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CustomerWallet;

public class CustomerWalletDAO extends HibernateDAO<Integer, CustomerWallet> implements com.zonesws.webservices.dao.services.CustomerWalletDAO{

	
	public CustomerWallet getCustomerWalletByCustomerId(Integer customerId) {
		return findSingle("FROM CustomerWallet WHERE customerId=?",new Object[]{customerId});
	}

}
