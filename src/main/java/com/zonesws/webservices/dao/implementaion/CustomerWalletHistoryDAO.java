package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.CustomerWalletHistory;

public class CustomerWalletHistoryDAO extends HibernateDAO<Integer, CustomerWalletHistory> implements com.zonesws.webservices.dao.services.CustomerWalletHistoryDAO{

	public CustomerWalletHistory getCustomerWalletHistoryByCustomerId(Integer customerId,String transactionId) {
		return findSingle("FROM CustomerWalletHistory WHERE customerId=? and transactionId=? and transactionType=? " +
				"and transactionStatus=?",new Object[]{customerId,transactionId,"DEBIT","PENDING"});
	}
	
	public CustomerWalletHistory getPendingDebitTransactionById(Integer id) {
		return findSingle("FROM CustomerWalletHistory WHERE id=? and transactionType=? and transactionStatus=?",new Object[]{id,"DEBIT","PENDING"});
	}
	
	public CustomerWalletHistory getPendingDebitTransactionByOrderId(Integer orderId) {
		return findSingle("FROM CustomerWalletHistory WHERE id=? and transactionType=? and transactionStatus=?",new Object[]{orderId,"DEBIT","PENDING"});
	}
	
}
