package com.zonesws.webservices.dao.implementaion;

import com.rtfquiz.webservices.dao.implementaion.QuizCustomerFriendDAO;
import com.rtfquiz.webservices.dao.services.QuizCustomerInvitedFriendsDAO;
import com.zonesws.webservices.dao.services.AffiliateCashRewardDAO;
import com.zonesws.webservices.dao.services.AffiliateCashRewardHistoryDAO;
import com.zonesws.webservices.dao.services.AffiliatePromoCodeTrackingDAO;
import com.zonesws.webservices.dao.services.ArtistDAO;
import com.zonesws.webservices.dao.services.AutoCatsLockedTicketDAO;
import com.zonesws.webservices.dao.services.AutoCatsOrderItemDAO;
import com.zonesws.webservices.dao.services.AutoCatsUserOrderDAO;
import com.zonesws.webservices.dao.services.BrokerOrderEmailTrackingDAO;
import com.zonesws.webservices.dao.services.CardsDAO;
import com.zonesws.webservices.dao.services.CategoryTicketGroupDAO;
import com.zonesws.webservices.dao.services.CountryDAO;
import com.zonesws.webservices.dao.services.CrownJewelCategoryTicketDAO;
import com.zonesws.webservices.dao.services.CrownJewelTeamZonesDAO;
import com.zonesws.webservices.dao.services.CrownJewelZonesSoldDetailsDAO;
import com.zonesws.webservices.dao.services.CustomerCassandraDAO;
import com.zonesws.webservices.dao.services.CustomerDAO;
import com.zonesws.webservices.dao.services.CustomerDeviceDetailsDAO;
import com.zonesws.webservices.dao.services.CustomerFavoritesHandlerDAO;
import com.zonesws.webservices.dao.services.CustomerFavouriteEventDAO;
import com.zonesws.webservices.dao.services.CustomerFeedBackTrackingDAO;
import com.zonesws.webservices.dao.services.CustomerLoginHistoryDAO;
import com.zonesws.webservices.dao.services.CustomerLoyaltyDAO;
import com.zonesws.webservices.dao.services.CustomerLoyaltyHistoryDAO;
import com.zonesws.webservices.dao.services.CustomerLoyaltyTrackingDAO;
import com.zonesws.webservices.dao.services.CustomerNotificationDeliveryTrackingDAO;
import com.zonesws.webservices.dao.services.CustomerOrderCreditCardDAO;
import com.zonesws.webservices.dao.services.CustomerOrderRequestDAO;
import com.zonesws.webservices.dao.services.CustomerOrderTrackingDAO;
import com.zonesws.webservices.dao.services.CustomerSuperFanDAO;
import com.zonesws.webservices.dao.services.CustomerWalletDAO;
import com.zonesws.webservices.dao.services.CustomerWalletHistoryDAO;
import com.zonesws.webservices.dao.services.DiscountCodeTrackingDAO;
import com.zonesws.webservices.dao.services.EventArtistDAO;
import com.zonesws.webservices.dao.services.EventDAO;
import com.zonesws.webservices.dao.services.FavouriteEventsDAO;
import com.zonesws.webservices.dao.services.GiftCardBrandDAO;
import com.zonesws.webservices.dao.services.GiftCardFileAttachmentDAO;
import com.zonesws.webservices.dao.services.GrandChildCategoryDAO;
import com.zonesws.webservices.dao.services.GrandChildCategoryImageDAO;
import com.zonesws.webservices.dao.services.HoldTicketDAO;
import com.zonesws.webservices.dao.services.InvoiceTicketAttachmentDAO;
import com.zonesws.webservices.dao.services.LockedTicketDAO;
import com.zonesws.webservices.dao.services.LoyaltySettingsDAO;
import com.zonesws.webservices.dao.services.OrderItemDAO;
import com.zonesws.webservices.dao.services.OrderPaymentBreakupDAO;
import com.zonesws.webservices.dao.services.OrderTicketGroupDAO;
import com.zonesws.webservices.dao.services.ParentCategoryDAO;
import com.zonesws.webservices.dao.services.PaypalTrackingDAO;
import com.zonesws.webservices.dao.services.PaypalTransactionDetailDAO;
import com.zonesws.webservices.dao.services.PopularEventsDAO;
import com.zonesws.webservices.dao.services.PreOrderPageAuditDAO;
import com.zonesws.webservices.dao.services.PreOrderPageEmailTrackingDAO;
import com.zonesws.webservices.dao.services.PresaleZoneTicketGroupDAO;
import com.zonesws.webservices.dao.services.PropertyDAO;
import com.zonesws.webservices.dao.services.QuizAffiliateRewardHistoryDAO;
import com.zonesws.webservices.dao.services.QuizAffiliateSettingDAO;
import com.zonesws.webservices.dao.services.RTFCustomerPromotionalOfferDAO;
import com.zonesws.webservices.dao.services.RTFOrderTrackingDAO;
import com.zonesws.webservices.dao.services.RTFPromotionalOfferDtlDAO;
import com.zonesws.webservices.dao.services.RTFPromotionalOfferHdrDAO;
import com.zonesws.webservices.dao.services.RTFPromotionalOfferTrackingDAO;
import com.zonesws.webservices.dao.services.ReferralContestDAO;
import com.zonesws.webservices.dao.services.ReferralContestEmailVersionDAO;
import com.zonesws.webservices.dao.services.ReferralContestParticipantsDAO;
import com.zonesws.webservices.dao.services.ReferralDiscountSettingsDAO;
import com.zonesws.webservices.dao.services.ReferredCodeTrackingDAO;
import com.zonesws.webservices.dao.services.ResetPasswordTokenDAO;
import com.zonesws.webservices.dao.services.RtfConfigContestClusterNodesDAO;
import com.zonesws.webservices.dao.services.RtfPromotionalEarnLifeOffersDAO;
import com.zonesws.webservices.dao.services.StateDAO;
import com.zonesws.webservices.dao.services.TicketGroupDAO;
import com.zonesws.webservices.dao.services.TrackingDAO;
import com.zonesws.webservices.dao.services.UserAddressDAO;
import com.zonesws.webservices.dao.services.UserOrderDAO;
import com.zonesws.webservices.dao.services.UserTypeDAO;
import com.zonesws.webservices.dao.services.VenueDAO;
import com.zonesws.webservices.dao.services.WebServiceConfigDAO;
import com.zonesws.webservices.dao.services.WebServiceTrackingCassandraDAO;
import com.zonesws.webservices.dao.services.ZPSessionCartDAO;
import com.zonesws.webservices.dao.services.ZoneRGBColorDAO;
import com.zonesws.webservices.dao.services.ZoneTicketGroupDAO;
import com.zonesws.webservices.dao.services.ZoneTicketsAdditionalMarkupDAO;
import com.zonesws.webservices.tmat.dao.implementaion.LastrowMinicatsQueryManagerDAO;
import com.zonesws.webservices.tmat.dao.implementaion.MinicatsQueryManagerDAO;
import com.zonesws.webservices.tmat.dao.implementaion.PresaleZoneTicketsQueryManagerDAO;
import com.zonesws.webservices.tmat.dao.implementaion.QueryManagerDAO;
import com.zonesws.webservices.tmat.dao.implementaion.VipLastrowMinicatsQueryManagerDAO;
import com.zonesws.webservices.tmat.dao.implementaion.VipMinicatsQueryManagerDAO;

public class DAORegistry {
	
	private static EventDAO eventDAO;
	//private static TicketDAO ticketDAO;
	private static WebServiceConfigDAO webServiceConfigDAO;
	private static IpConfigDAO ipConfigDAO;
	//private static CategoryMappingDAO categoryMappingDAO;
	//private static CategoryDAO categoryDAO;
	//private static ZonesEventCategoryMappingDAO zonesEventCategoryMappingDAO;
	//private static ZonesExposurePropertyDAO zonesExposurePropertyDAO;
	private static WebServiceTrackingDAO webServiceTrackingDAO;
	private static WebServiceTrackingCassandraDAO webServiceTrackingCassandraDAO;
	private static com.zonesws.webservices.dao.services.ContestMigrationStatsDAO contestMigrationStatsDAO;
	
	//private static EbayInventoryDAO ebayInventoryDAO;
	//private static EbayInventoryGroupDAO ebayInventoryGroupDAO;
	
	private static CustomerDAO customerDAO;
	private static CustomerCassandraDAO customerCassandraDAO;
	private static UserAddressDAO userAddressDAO;
	private static StateDAO stateDAO;
	private static CountryDAO countryDAO;
	private static UserOrderDAO userOrderDAO;
	private static OrderItemDAO orderItemDAO;
	private static LockedTicketDAO lockedTicketDAO;
	private static ZPSessionCartDAO zpSessionCartDAO;
	private static AutoCatsLockedTicketDAO autoCatsLockedTicketDAO;
	private static AutoCatsUserOrderDAO autoCatsUserOrderDAO;
	private static AutoCatsOrderItemDAO autoCatsOrderItemDAO;
	//private static WebConfigMappingDAO webConfigMappingDAO;
	private static TrackingDAO trackingDAO;
	private static PropertyDAO propertyDAO;
	private static UserTypeDAO userTypeDAO;
	//private static ExposureDAO exposureDAO;
	private static HoldTicketDAO holdTicketDAO;
	private static TicketTrackingDAO ticketTrackingDAO;
	private static GrandChildCategoryDAO grandChildCategoryDAO;
	private static ChildCategoryDAO childCategoryDAO;
	private static ParentCategoryDAO parentCategoryDAO;
	private static CustomerFavouriteEventDAO customerFavouriteEventDAO;
	private static LoyaltySettingsDAO loyaltySettingsDAO;
	private static ZoneRGBColorDAO zoneRGBColorDAO;
	private static ZoneTicketsAdditionalMarkupDAO zoneTicketsAdditionalMarkupDAO;
	private static QueryManagerDAO queryManagerDAO;
	//private static PresaleQueryManagerDAO presaleQueryManagerDAO;
	private static PresaleZoneTicketsQueryManagerDAO presaleZoneTicketsQueryManagerDAO;
	private static MinicatsQueryManagerDAO minicatsQueryManagerDAO;
	private static VipMinicatsQueryManagerDAO vipMinicatsQueryManagerDAO;
	private static LastrowMinicatsQueryManagerDAO lastrowMinicatsQueryManagerDAO;
	private static VipLastrowMinicatsQueryManagerDAO vipLastrowMinicatsQueryManagerDAO;
	private static ZoneTicketGroupDAO zoneTicketGroupDAO;
	private static PresaleZoneTicketGroupDAO presaleZoneTicketGroupDAO;
	private static CustomerLoyaltyDAO customerLoyaltyDAO;
	private static CustomerLoyaltyHistoryDAO customerLoyaltyHistoryDAO;
	private static ArtistDAO artistDAO;
	private static VenueDAO venueDAO;
	private static ResetPasswordTokenDAO resetPasswordTokenDAO;
	private static PopularEventsDAO popularEventsDAO;
	private static CustomerFavoritesHandlerDAO customerFavoritesHandlerDAO;
	private static CustomerSuperFanDAO customerSuperFanDAO;
	private static CustomerLoginHistoryDAO customerLoginHistoryDAO;
	private static FavouriteEventsDAO favouriteEventsDAO;
	private static CategoryTicketGroupDAO categoryTicketGroupDAO;
	private static CustomerOrderDAO customerOrderDAO;
	private static OrderPaymentBreakupDAO orderPaymentBreakupDAO;
	private static CustomerOrderDetailDAO customerOrderDetailDAO;
	private static CustomerCardInfoDAO customerCardInfoDAO;
	private static com.zonesws.webservices.dao.services.CustomerFantasyOrderDAO customerFantasyOrderDAO;
	private static CrownJewelLeaguesDAO crownJewelLeaguesDAO;
	private static CrownJewelTeamsDAO crownJewelTeamsDAO;
	private static CrownJewelCategoryTicketDAO crownJewelCategoryTicketDAO;
	private static InvoiceDAO invoiceDAO;
	private static CardsDAO cardsDAO;
	private static ArtistDetailsDAO artistDetailsDAO;
	private static ReferredCodeTrackingDAO referredCodeTrackingDAO;
	private static CustomerDeviceDetailsDAO customerDeviceDetailsDAO;
	private static GrandChildCategoryImageDAO grandChildCategoryImageDAO;
	private static CustomerOrderCreditCardDAO customerOrderCreditCardDAO;
	private static CrownJewelTeamZonesDAO crownJewelTeamZonesDAO;
	private static CrownJewelZonesSoldDetailsDAO crownJewelZonesSoldDetailsDAO;
	private static PaypalTransactionDetailDAO paypalTransactionDetailDAO;
	private static InvoiceTicketAttachmentDAO invoiceTicketAttachmentDAO;
	private static CategoryTicketDAO categoryTicketDAO;
	private static CustomerFuturePaymentInfoDAO customerFuturePaymentInfoDAO;
	private static CustomerTicketDownloadsDAO customerTicketDownloadsDAO;
	//private static UserPreferenceDAO userPreferenceDAO;
	
	private static EventArtistDAO eventArtistDAO;
	private static PaypalTrackingDAO paypalTrackingDAO;
	private static CustomerWalletDAO customerWalletDAO;
	private static CustomerWalletHistoryDAO customerWalletHistoryDAO;
	private static RtfConfigContestClusterNodesDAO rtfConfigContestClusterNodesDAO;
	
	private static AffiliateCashRewardDAO affiliateCashRewardDAO ;
	private static AffiliateCashRewardHistoryDAO affiliateCashRewardHistoryDAO;
	private static AffiliatePromoCodeTrackingDAO affiliatePromoCodeTrackingDAO;
	private static BrokerOrderEmailTrackingDAO brokerOrderEmailTrackingDAO;
	private static RTFPromotionalOfferTrackingDAO rtfPromotionalOfferTrackingDAO;
	private static CustomerOrderTrackingDAO customerOrderTrackingDAO;
	private static CustomerOrderRequestDAO customerOrderRequestDAO;
	private static RTFOrderTrackingDAO rtfOrderTrackingDAO;
	private static RTFPromotionalOfferHdrDAO rtfPromotionalOfferHdrDAO;
	private static RTFPromotionalOfferDtlDAO rtfPromotionalOfferDtlDAO;
	private static PreOrderPageEmailTrackingDAO preOrderPageEmailTrackingDAO;
	private static RTFCustomerPromotionalOfferDAO rtfCustomerPromotionalOfferDAO;
	private static ReferralDiscountSettingsDAO referralDiscountSettingsDAO;
	private static DiscountCodeTrackingDAO discountCodeTrackingDAO;
	private static PreOrderPageAuditDAO preOrderPageAuditDAO;
	private static TicketGroupDAO ticketGroupDAO;
	private static OrderTicketGroupDAO orderTicketGroupDAO;
	private static ReferralContestDAO referralContestDAO;
	private static ReferralContestParticipantsDAO referralContestParticipantsDAO;
	private static ReferralContestEmailVersionDAO referralContestEmailVersionDAO;
	private static ReferralContestWinnerDAO referralContestWinnerDAO;
	private static CustomerCheckoutVisitOfferDAO customerCheckoutVisitOfferDAO;
	private static CheckOutOfferAppliedAuditDAO checkOutOfferAppliedAuditDAO;
	private static QuizCustomerFriendDAO quizCustomerFriendDAO;
	private static QuizCustomerInvitedFriendsDAO quizCustomerInvitedFriendsDAO;
	private static CustomerLoyaltyTrackingDAO customerLoyaltyTrackingDAO;
	private static RtfPromotionalEarnLifeOffersDAO rtfPromotionalEarnLifeOffersDAO;
	private static CustomerNotificationDeliveryTrackingDAO customerNotificationDeliveryTrackingDAO;
	private static QuizAffiliateSettingDAO quizAffiliateSettingDAO;
	private static QuizAffiliateRewardHistoryDAO quizAffiliateRewardHistoryDAO;
	private static com.zonesws.webservices.dao.services.RtfCatTicketDAO rtfCatTicketDAO;
	private static CustomerFeedBackTrackingDAO customerFeedBackTrackingDAO;
	private static GiftCardBrandDAO giftCardBrandDAO;
	private static RtfGiftCardDAO rtfGiftCardDAO;
	private static RtfGiftCardQuantityDAO rtfGiftCardQuantityDAO;
	private static RtfGiftCardOrderDAO rtfGiftCardOrderDAO;
	private static GiftCardFileAttachmentDAO giftCardFileAttachmentDAO;
	private static PollingCustomerRewardsDAO pollingCustomerRewardsDAO;
	
	public final static CustomerCardInfoDAO getCustomerCardInfoDAO() {
		return customerCardInfoDAO;
	}

	public final void setCustomerCardInfoDAO(
			CustomerCardInfoDAO customerCardInfoDAO) {
		DAORegistry.customerCardInfoDAO = customerCardInfoDAO;
	}

	public final static CustomerOrderDAO getCustomerOrderDAO() {
		return customerOrderDAO;
	}

	public final void setCustomerOrderDAO(CustomerOrderDAO customerOrderDAO) {
		DAORegistry.customerOrderDAO = customerOrderDAO;
	}

	public final static OrderPaymentBreakupDAO getOrderPaymentBreakupDAO() {
		return orderPaymentBreakupDAO;
	}

	public final void setOrderPaymentBreakupDAO(
			OrderPaymentBreakupDAO orderPaymentBreakupDAO) {
		DAORegistry.orderPaymentBreakupDAO = orderPaymentBreakupDAO;
	}

	public final static CustomerOrderDetailDAO getCustomerOrderDetailDAO() {
		return customerOrderDetailDAO;
	}

	public final void setCustomerOrderDetailDAO(
			CustomerOrderDetailDAO customerOrderDetailDAO) {
		DAORegistry.customerOrderDetailDAO = customerOrderDetailDAO;
	}

	public final static FavouriteEventsDAO getFavouriteEventsDAO() {
		return favouriteEventsDAO;
	}

	public final void setFavouriteEventsDAO(FavouriteEventsDAO favouriteEventsDAO) {
		DAORegistry.favouriteEventsDAO = favouriteEventsDAO;
	}

	public final static EventDAO getEventDAO() {
		return eventDAO;
	}

	public final void setEventDAO(EventDAO eventDAO) {
		DAORegistry.eventDAO = eventDAO;
	}


	public final static WebServiceConfigDAO getWebServiceConfigDAO() {
		return webServiceConfigDAO;
	}

	public final void setWebServiceConfigDAO(WebServiceConfigDAO webServiceConfigDAO) {
		DAORegistry.webServiceConfigDAO = webServiceConfigDAO;
	}

	public final static IpConfigDAO getIpConfigDAO() {
		return ipConfigDAO;
	}

	public final void setIpConfigDAO(IpConfigDAO ipConfigDAO) {
		DAORegistry.ipConfigDAO = ipConfigDAO;
	}


	public final static WebServiceTrackingDAO getWebServiceTrackingDAO() {
		return webServiceTrackingDAO;
	}

	public final void setWebServiceTrackingDAO(
			WebServiceTrackingDAO webServiceTrackingDAO) {
		DAORegistry.webServiceTrackingDAO = webServiceTrackingDAO;
	}
	
	public final static WebServiceTrackingCassandraDAO getWebServiceTrackingCassandraDAO() {
		return webServiceTrackingCassandraDAO;
	}

	public final void setWebServiceTrackingCassandraDAO(WebServiceTrackingCassandraDAO webServiceTrackingCassandraDAO) {
		DAORegistry.webServiceTrackingCassandraDAO = webServiceTrackingCassandraDAO;
	}

	public final static com.zonesws.webservices.dao.services.ContestMigrationStatsDAO getContestMigrationStatsDAO() {
		return contestMigrationStatsDAO;
	}

	public final void setContestMigrationStatsDAO(
			com.zonesws.webservices.dao.services.ContestMigrationStatsDAO contestMigrationStatsDAO) {
		DAORegistry.contestMigrationStatsDAO = contestMigrationStatsDAO;
	}

	public final static CustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	public final void setCustomerDAO(CustomerDAO customerDAO) {
		DAORegistry.customerDAO = customerDAO;
	}

	public final static CustomerCassandraDAO getCustomerCassandraDAO() {
		return customerCassandraDAO;
	}

	public final void setCustomerCassandraDAO(CustomerCassandraDAO customerCassandraDAO) {
		DAORegistry.customerCassandraDAO = customerCassandraDAO;
	}

	public final static UserAddressDAO getUserAddressDAO() {
		return userAddressDAO;
	}

	public final void setUserAddressDAO(UserAddressDAO userAddressDAO) {
		DAORegistry.userAddressDAO = userAddressDAO;
	}

	public final static StateDAO getStateDAO() {
		return stateDAO;
	}

	public final void setStateDAO(StateDAO stateDAO) {
		DAORegistry.stateDAO = stateDAO;
	}

	public final static CountryDAO getCountryDAO() {
		return countryDAO;
	}

	public final void setCountryDAO(CountryDAO countryDAO) {
		DAORegistry.countryDAO = countryDAO;
	}

	public final static UserOrderDAO getUserOrderDAO() {
		return userOrderDAO;
	}

	public final void setUserOrderDAO(UserOrderDAO userOrderDAO) {
		DAORegistry.userOrderDAO = userOrderDAO;
	}

	public final static OrderItemDAO getOrderItemDAO() {
		return orderItemDAO;
	}

	public final void setOrderItemDAO(OrderItemDAO orderItemDAO) {
		DAORegistry.orderItemDAO = orderItemDAO;
	}


	public final static LockedTicketDAO getLockedTicketDAO() {
		return lockedTicketDAO;
	}

	public final void setLockedTicketDAO(LockedTicketDAO lockedTicketDAO) {
		DAORegistry.lockedTicketDAO = lockedTicketDAO;
	}

	public final static AutoCatsLockedTicketDAO getAutoCatsLockedTicketDAO() {
		return autoCatsLockedTicketDAO;
	}

	public final void setAutoCatsLockedTicketDAO(
			AutoCatsLockedTicketDAO autoCatsLockedTicketDAO) {
		DAORegistry.autoCatsLockedTicketDAO = autoCatsLockedTicketDAO;
	}

	public final static AutoCatsUserOrderDAO getAutoCatsUserOrderDAO() {
		return autoCatsUserOrderDAO;
	}

	public final void setAutoCatsUserOrderDAO(
			AutoCatsUserOrderDAO autoCatsUserOrderDAO) {
		DAORegistry.autoCatsUserOrderDAO = autoCatsUserOrderDAO;
	}

	public final static AutoCatsOrderItemDAO getAutoCatsOrderItemDAO() {
		return autoCatsOrderItemDAO;
	}

	public final void setAutoCatsOrderItemDAO(
			AutoCatsOrderItemDAO autoCatsOrderItemDAO) {
		DAORegistry.autoCatsOrderItemDAO = autoCatsOrderItemDAO;
	}

	public final static ZPSessionCartDAO getZpSessionCartDAO() {
		return zpSessionCartDAO;
	}

	public final void setZpSessionCartDAO(ZPSessionCartDAO zpSessionCartDAO) {
		DAORegistry.zpSessionCartDAO = zpSessionCartDAO;
	}

	/*public final static WebConfigMappingDAO getWebConfigMappingDAO() {
		return webConfigMappingDAO;
	}

	public final void setWebConfigMappingDAO(
			WebConfigMappingDAO webConfigMappingDAO) {
		DAORegistry.webConfigMappingDAO = webConfigMappingDAO;
	}*/


	public final static TrackingDAO getTrackingDAO() {
		return trackingDAO;
	}

	public final void setTrackingDAO(TrackingDAO trackingDAO) {
		DAORegistry.trackingDAO = trackingDAO;
	}

	public final static PropertyDAO getPropertyDAO() {
		return propertyDAO;
	}

	public final void setPropertyDAO(PropertyDAO propertyDAO) {
		DAORegistry.propertyDAO = propertyDAO;
	}

	public final static UserTypeDAO getUserTypeDAO() {
		return userTypeDAO;
	}

	public final void setUserTypeDAO(UserTypeDAO userTypeDAO) {
		DAORegistry.userTypeDAO = userTypeDAO;
	}


	public final static HoldTicketDAO getHoldTicketDAO() {
		return holdTicketDAO;
	}

	public final void setHoldTicketDAO(HoldTicketDAO holdTicketDAO) {
		DAORegistry.holdTicketDAO = holdTicketDAO;
	}

	public final static GrandChildCategoryDAO getGrandChildCategoryDAO() {
		return grandChildCategoryDAO;
	}

	public final void setGrandChildCategoryDAO(
			GrandChildCategoryDAO grandChildCategoryDAO) {
		DAORegistry.grandChildCategoryDAO = grandChildCategoryDAO;
	}

	public final static ChildCategoryDAO getChildCategoryDAO() {
		return childCategoryDAO;
	}

	public final void setChildCategoryDAO(ChildCategoryDAO childCategoryDAO) {
		DAORegistry.childCategoryDAO = childCategoryDAO;
	} 

	public final static ParentCategoryDAO getParentCategoryDAO() {
		return parentCategoryDAO;
	}

	public final void setParentCategoryDAO(ParentCategoryDAO parentCategoryDAO) {
		DAORegistry.parentCategoryDAO = parentCategoryDAO;
	}
	
	public final static TicketTrackingDAO getTicketTrackingDAO() {
		return ticketTrackingDAO;
	}

	public final void setTicketTrackingDAO(TicketTrackingDAO ticketTrackingDAO) {
		DAORegistry.ticketTrackingDAO = ticketTrackingDAO;
	}

	public final static CustomerFavouriteEventDAO getCustomerFavouriteEventDAO() {
		return customerFavouriteEventDAO;
	}

	public final void setCustomerFavouriteEventDAO(
			CustomerFavouriteEventDAO customerFavouriteEventDAO) {
		DAORegistry.customerFavouriteEventDAO = customerFavouriteEventDAO;
	}

	public final static LoyaltySettingsDAO getLoyaltySettingsDAO() {
		return loyaltySettingsDAO;
	}

	public final void setLoyaltySettingsDAO(LoyaltySettingsDAO loyaltySettingsDAO) {
		DAORegistry.loyaltySettingsDAO = loyaltySettingsDAO;
	}

	public final static ZoneRGBColorDAO getZoneRGBColorDAO() {
		return zoneRGBColorDAO;
	}

	public final void setZoneRGBColorDAO(ZoneRGBColorDAO zoneRGBColorDAO) {
		DAORegistry.zoneRGBColorDAO = zoneRGBColorDAO;
	}

	
	public final static ZoneTicketsAdditionalMarkupDAO getZoneTicketsAdditionalMarkupDAO() {
		return zoneTicketsAdditionalMarkupDAO;
	}

	public final void setZoneTicketsAdditionalMarkupDAO(
			ZoneTicketsAdditionalMarkupDAO zoneTicketsAdditionalMarkupDAO) {
		DAORegistry.zoneTicketsAdditionalMarkupDAO = zoneTicketsAdditionalMarkupDAO;
	}

	public final static QueryManagerDAO getQueryManagerDAO() {
		return queryManagerDAO;
	}

	public final void setQueryManagerDAO(QueryManagerDAO queryManagerDAO) {
		DAORegistry.queryManagerDAO = queryManagerDAO;
	}

	
	public final static PresaleZoneTicketsQueryManagerDAO getPresaleZoneTicketsQueryManagerDAO() {
		return presaleZoneTicketsQueryManagerDAO;
	}

	public final void setPresaleZoneTicketsQueryManagerDAO(
			PresaleZoneTicketsQueryManagerDAO presaleZoneTicketsQueryManagerDAO) {
		DAORegistry.presaleZoneTicketsQueryManagerDAO = presaleZoneTicketsQueryManagerDAO;
	}

	public final static MinicatsQueryManagerDAO getMinicatsQueryManagerDAO() {
		return minicatsQueryManagerDAO;
	}

	public final void setMinicatsQueryManagerDAO(
			MinicatsQueryManagerDAO minicatsQueryManagerDAO) {
		DAORegistry.minicatsQueryManagerDAO = minicatsQueryManagerDAO;
	}

	public final static VipMinicatsQueryManagerDAO getVipMinicatsQueryManagerDAO() {
		return vipMinicatsQueryManagerDAO;
	}

	public final void setVipMinicatsQueryManagerDAO(
			VipMinicatsQueryManagerDAO vipMinicatsQueryManagerDAO) {
		DAORegistry.vipMinicatsQueryManagerDAO = vipMinicatsQueryManagerDAO;
	}

	public final static LastrowMinicatsQueryManagerDAO getLastrowMinicatsQueryManagerDAO() {
		return lastrowMinicatsQueryManagerDAO;
	}

	public final void setLastrowMinicatsQueryManagerDAO(
			LastrowMinicatsQueryManagerDAO lastrowMinicatsQueryManagerDAO) {
		DAORegistry.lastrowMinicatsQueryManagerDAO = lastrowMinicatsQueryManagerDAO;
	}
	

	public final static VipLastrowMinicatsQueryManagerDAO getVipLastrowMinicatsQueryManagerDAO() {
		return vipLastrowMinicatsQueryManagerDAO;
	}

	public final void setVipLastrowMinicatsQueryManagerDAO(
			VipLastrowMinicatsQueryManagerDAO vipLastrowMinicatsQueryManagerDAO) {
		DAORegistry.vipLastrowMinicatsQueryManagerDAO = vipLastrowMinicatsQueryManagerDAO;
	}

	public final static ZoneTicketGroupDAO getZoneTicketGroupDAO() {
		return zoneTicketGroupDAO;
	}

	public final void setZoneTicketGroupDAO(ZoneTicketGroupDAO zoneTicketGroupDAO) {
		DAORegistry.zoneTicketGroupDAO = zoneTicketGroupDAO;
	}

	public final static PresaleZoneTicketGroupDAO getPresaleZoneTicketGroupDAO() {
		return presaleZoneTicketGroupDAO;
	}

	public final void setPresaleZoneTicketGroupDAO(
			PresaleZoneTicketGroupDAO presaleZoneTicketGroupDAO) {
		DAORegistry.presaleZoneTicketGroupDAO = presaleZoneTicketGroupDAO;
	}

	public final static CustomerLoyaltyDAO getCustomerLoyaltyDAO() {
		return customerLoyaltyDAO;
	}

	public final void setCustomerLoyaltyDAO(CustomerLoyaltyDAO customerLoyaltyDAO) {
		DAORegistry.customerLoyaltyDAO = customerLoyaltyDAO;
	}

	public final static CustomerLoyaltyHistoryDAO getCustomerLoyaltyHistoryDAO() {
		return customerLoyaltyHistoryDAO;
	}

	public final void setCustomerLoyaltyHistoryDAO(
			CustomerLoyaltyHistoryDAO customerLoyaltyHistoryDAO) {
		DAORegistry.customerLoyaltyHistoryDAO = customerLoyaltyHistoryDAO;
	}

	public final static ArtistDAO getArtistDAO() {
		return artistDAO;
	}

	public final void setArtistDAO(ArtistDAO artistDAO) {
		DAORegistry.artistDAO = artistDAO;
	}

	public final static VenueDAO getVenueDAO() {
		return venueDAO;
	}

	public final void setVenueDAO(VenueDAO venueDAO) {
		DAORegistry.venueDAO = venueDAO;
	}

	public final static ResetPasswordTokenDAO getResetPasswordTokenDAO() {
		return resetPasswordTokenDAO;
	}

	public final void setResetPasswordTokenDAO(
			ResetPasswordTokenDAO resetPasswordTokenDAO) {
		DAORegistry.resetPasswordTokenDAO = resetPasswordTokenDAO;
	}

	public final static PopularEventsDAO getPopularEventsDAO() {
		return popularEventsDAO;
	}

	public final void setPopularEventsDAO(PopularEventsDAO popularEventsDAO) {
		DAORegistry.popularEventsDAO = popularEventsDAO;
	}

	public final static CustomerFavoritesHandlerDAO getCustomerFavoritesHandlerDAO() {
		return customerFavoritesHandlerDAO;
	}

	public final void setCustomerFavoritesHandlerDAO(
			CustomerFavoritesHandlerDAO customerFavoritesHandlerDAO) {
		DAORegistry.customerFavoritesHandlerDAO = customerFavoritesHandlerDAO;
	}

	public final static CustomerSuperFanDAO getCustomerSuperFanDAO() {
		return customerSuperFanDAO;
	}

	public final void setCustomerSuperFanDAO(
			CustomerSuperFanDAO customerSuperFanDAO) {
		DAORegistry.customerSuperFanDAO = customerSuperFanDAO;
	}

	public final static CustomerLoginHistoryDAO getCustomerLoginHistoryDAO() {
		return customerLoginHistoryDAO;
	}

	public final void setCustomerLoginHistoryDAO(
			CustomerLoginHistoryDAO customerLoginHistoryDAO) {
		DAORegistry.customerLoginHistoryDAO = customerLoginHistoryDAO;
	}

	public final static CategoryTicketGroupDAO getCategoryTicketGroupDAO() {
		return categoryTicketGroupDAO;
	}

	public final void setCategoryTicketGroupDAO(
			CategoryTicketGroupDAO categoryTicketGroupDAO) {
		DAORegistry.categoryTicketGroupDAO = categoryTicketGroupDAO;
	}
	public final static CrownJewelLeaguesDAO getCrownJewelLeaguesDAO() {
		return crownJewelLeaguesDAO;
	}

	public final void setCrownJewelLeaguesDAO(
			CrownJewelLeaguesDAO crownJewelLeaguesDAO) {
		DAORegistry.crownJewelLeaguesDAO = crownJewelLeaguesDAO;
	}

	public final static CrownJewelTeamsDAO getCrownJewelTeamsDAO() {
		return crownJewelTeamsDAO;
	}

	public final void setCrownJewelTeamsDAO(CrownJewelTeamsDAO crownJewelTeamsDAO) {
		DAORegistry.crownJewelTeamsDAO = crownJewelTeamsDAO;
	}

	public final static InvoiceDAO getInvoiceDAO() {
		return invoiceDAO;
	}

	public final void setInvoiceDAO(InvoiceDAO invoiceDAO) {
		DAORegistry.invoiceDAO = invoiceDAO;
	}

	public final static CardsDAO getCardsDAO() {
		return cardsDAO;
	}

	public final void setCardsDAO(CardsDAO cardsDAO) {
		DAORegistry.cardsDAO = cardsDAO;
	}

	public final static CrownJewelCategoryTicketDAO getCrownJewelCategoryTicketDAO() {
		return crownJewelCategoryTicketDAO;
	}

	public final void setCrownJewelCategoryTicketDAO(
			CrownJewelCategoryTicketDAO crownJewelCategoryTicketDAO) {
		DAORegistry.crownJewelCategoryTicketDAO = crownJewelCategoryTicketDAO;
	}
	
	public final static ArtistDetailsDAO getArtistDetailsDAO() {
		return artistDetailsDAO;
	}

	public final void setArtistDetailsDAO(
			ArtistDetailsDAO artistDetailsDAO) {
		DAORegistry.artistDetailsDAO = artistDetailsDAO;
	}
	
	public final static ReferredCodeTrackingDAO getReferredCodeTrackingDAO() {
		return referredCodeTrackingDAO;
	}

	public final void setReferredCodeTrackingDAO(
			ReferredCodeTrackingDAO referredCodeTrackingDAO) {
		DAORegistry.referredCodeTrackingDAO = referredCodeTrackingDAO;
	}

	public final static CustomerDeviceDetailsDAO getCustomerDeviceDetailsDAO() {
		return customerDeviceDetailsDAO;
	}

	public final void setCustomerDeviceDetailsDAO(
			CustomerDeviceDetailsDAO customerDeviceDetailsDAO) {
		DAORegistry.customerDeviceDetailsDAO = customerDeviceDetailsDAO;
	}

	public final static GrandChildCategoryImageDAO getGrandChildCategoryImageDAO() {
		return grandChildCategoryImageDAO;
	}

	public final void setGrandChildCategoryImageDAO(
			GrandChildCategoryImageDAO grandChildCategoryImageDAO) {
		DAORegistry.grandChildCategoryImageDAO = grandChildCategoryImageDAO;
	}

	public final static CustomerOrderCreditCardDAO getCustomerOrderCreditCardDAO() {
		return customerOrderCreditCardDAO;
	}

	public final void setCustomerOrderCreditCardDAO(
			CustomerOrderCreditCardDAO customerOrderCreditCardDAO) {
		DAORegistry.customerOrderCreditCardDAO = customerOrderCreditCardDAO;
	}

	public final static CrownJewelTeamZonesDAO getCrownJewelTeamZonesDAO() {
		return crownJewelTeamZonesDAO;
	}

	public final void setCrownJewelTeamZonesDAO(
			CrownJewelTeamZonesDAO crownJewelTeamZonesDAO) {
		DAORegistry.crownJewelTeamZonesDAO = crownJewelTeamZonesDAO;
	}

	public final static CrownJewelZonesSoldDetailsDAO getCrownJewelZonesSoldDetailsDAO() {
		return crownJewelZonesSoldDetailsDAO;
	}

	public final void setCrownJewelZonesSoldDetailsDAO(
			CrownJewelZonesSoldDetailsDAO crownJewelZonesSoldDetailsDAO) {
		DAORegistry.crownJewelZonesSoldDetailsDAO = crownJewelZonesSoldDetailsDAO;
	}

	public final static PaypalTransactionDetailDAO getPaypalTransactionDetailDAO() {
		return paypalTransactionDetailDAO;
	}

	public final void setPaypalTransactionDetailDAO(
			PaypalTransactionDetailDAO paypalTransactionDetailDAO) {
		DAORegistry.paypalTransactionDetailDAO = paypalTransactionDetailDAO;
	}

	public final static InvoiceTicketAttachmentDAO getInvoiceTicketAttachmentDAO() {
		return invoiceTicketAttachmentDAO;
	}

	public final void setInvoiceTicketAttachmentDAO(
			InvoiceTicketAttachmentDAO invoiceTicketAttachmentDAO) {
		DAORegistry.invoiceTicketAttachmentDAO = invoiceTicketAttachmentDAO;
	}

	public final static CategoryTicketDAO getCategoryTicketDAO() {
		return categoryTicketDAO;
	}

	public final void setCategoryTicketDAO(CategoryTicketDAO categoryTicketDAO) {
		DAORegistry.categoryTicketDAO = categoryTicketDAO;
	}

	public static CustomerTicketDownloadsDAO getCustomerTicketDownloadsDAO() {
		return customerTicketDownloadsDAO;
	}

	public final void setCustomerTicketDownloadsDAO(
			CustomerTicketDownloadsDAO customerTicketDownloadsDAO) {
		DAORegistry.customerTicketDownloadsDAO = customerTicketDownloadsDAO;
	}

	public static EventArtistDAO getEventArtistDAO() {
		return eventArtistDAO;
	}

	public final void setEventArtistDAO(EventArtistDAO eventArtistDAO) {
		DAORegistry.eventArtistDAO = eventArtistDAO;
	}

	public static PaypalTrackingDAO getPaypalTrackingDAO() {
		return paypalTrackingDAO;
	}

	public final void setPaypalTrackingDAO(PaypalTrackingDAO paypalTrackingDAO) {
		DAORegistry.paypalTrackingDAO = paypalTrackingDAO;
	}

	public static CustomerWalletDAO getCustomerWalletDAO() {
		return customerWalletDAO;
	}

	public final void setCustomerWalletDAO(CustomerWalletDAO customerWalletDAO) {
		DAORegistry.customerWalletDAO = customerWalletDAO;
	}

	public static CustomerWalletHistoryDAO getCustomerWalletHistoryDAO() {
		return customerWalletHistoryDAO;
	}

	public final void setCustomerWalletHistoryDAO(
			CustomerWalletHistoryDAO customerWalletHistoryDAO) {
		DAORegistry.customerWalletHistoryDAO = customerWalletHistoryDAO;
	}

	public final static AffiliateCashRewardDAO getAffiliateCashRewardDAO() {
		return affiliateCashRewardDAO;
	}

	public final void setAffiliateCashRewardDAO(
			AffiliateCashRewardDAO affiliateCashRewardDAO) {
		DAORegistry.affiliateCashRewardDAO = affiliateCashRewardDAO;
	}

	public final static AffiliateCashRewardHistoryDAO getAffiliateCashRewardHistoryDAO() {
		return affiliateCashRewardHistoryDAO;
	}

	public final void setAffiliateCashRewardHistoryDAO(
			AffiliateCashRewardHistoryDAO affiliateCashRewardHistoryDAO) {
		DAORegistry.affiliateCashRewardHistoryDAO = affiliateCashRewardHistoryDAO;
	}

	public final static AffiliatePromoCodeTrackingDAO getAffiliatePromoCodeTrackingDAO() {
		return affiliatePromoCodeTrackingDAO;
	}

	public final void setAffiliatePromoCodeTrackingDAO(
			AffiliatePromoCodeTrackingDAO affiliatePromoCodeTrackingDAO) {
		DAORegistry.affiliatePromoCodeTrackingDAO = affiliatePromoCodeTrackingDAO;
	}

	public final static BrokerOrderEmailTrackingDAO getBrokerOrderEmailTrackingDAO() {
		return brokerOrderEmailTrackingDAO;
	}

	public final void setBrokerOrderEmailTrackingDAO(
			BrokerOrderEmailTrackingDAO brokerOrderEmailTrackingDAO) {
		DAORegistry.brokerOrderEmailTrackingDAO = brokerOrderEmailTrackingDAO;
	}

	public final static RTFPromotionalOfferTrackingDAO getRtfPromotionalOfferTrackingDAO() {
		return rtfPromotionalOfferTrackingDAO;
	}

	public final void setRtfPromotionalOfferTrackingDAO(
			RTFPromotionalOfferTrackingDAO rtfPromotionalOfferTrackingDAO) {
		DAORegistry.rtfPromotionalOfferTrackingDAO = rtfPromotionalOfferTrackingDAO;
	}

	public final static CustomerOrderTrackingDAO getCustomerOrderTrackingDAO() {
		return customerOrderTrackingDAO;
	}

	public final void setCustomerOrderTrackingDAO(
			CustomerOrderTrackingDAO customerOrderTrackingDAO) {
		DAORegistry.customerOrderTrackingDAO = customerOrderTrackingDAO;
	}

	public final static CustomerOrderRequestDAO getCustomerOrderRequestDAO() {
		return customerOrderRequestDAO;
	}

	public final void setCustomerOrderRequestDAO(
			CustomerOrderRequestDAO customerOrderRequestDAO) {
		DAORegistry.customerOrderRequestDAO = customerOrderRequestDAO;
	}

	public final static RTFOrderTrackingDAO getRtfOrderTrackingDAO() {
		return rtfOrderTrackingDAO;
	}

	public final void setRtfOrderTrackingDAO(
			RTFOrderTrackingDAO rtfOrderTrackingDAO) {
		DAORegistry.rtfOrderTrackingDAO = rtfOrderTrackingDAO;
	}

	public final static RTFPromotionalOfferHdrDAO getRtfPromotionalOfferHdrDAO() {
		return rtfPromotionalOfferHdrDAO;
	}

	public final void setRtfPromotionalOfferHdrDAO(
			RTFPromotionalOfferHdrDAO rtfPromotionalOfferHdrDAO) {
		DAORegistry.rtfPromotionalOfferHdrDAO = rtfPromotionalOfferHdrDAO;
	}

	public final static RTFPromotionalOfferDtlDAO getRtfPromotionalOfferDtlDAO() {
		return rtfPromotionalOfferDtlDAO;
	}

	public final void setRtfPromotionalOfferDtlDAO(
			RTFPromotionalOfferDtlDAO rtfPromotionalOfferDtlDAO) {
		DAORegistry.rtfPromotionalOfferDtlDAO = rtfPromotionalOfferDtlDAO;
	}
	
	public final static PreOrderPageEmailTrackingDAO getPreOrderPageEmailTrackingDAO() {
		return preOrderPageEmailTrackingDAO;
	}

	public final void setPreOrderPageEmailTrackingDAO(
			PreOrderPageEmailTrackingDAO preOrderPageEmailTrackingDAO) {
		DAORegistry.preOrderPageEmailTrackingDAO = preOrderPageEmailTrackingDAO;
	}

	public final static RTFCustomerPromotionalOfferDAO getRtfCustomerPromotionalOfferDAO() {
		return rtfCustomerPromotionalOfferDAO;
	}

	public final void setRtfCustomerPromotionalOfferDAO(
			RTFCustomerPromotionalOfferDAO rtfCustomerPromotionalOfferDAO) {
		DAORegistry.rtfCustomerPromotionalOfferDAO = rtfCustomerPromotionalOfferDAO;
	}

	public final static com.zonesws.webservices.dao.services.CustomerFantasyOrderDAO getCustomerFantasyOrderDAO() {
		return customerFantasyOrderDAO;
	}

	public final void setCustomerFantasyOrderDAO(
			com.zonesws.webservices.dao.services.CustomerFantasyOrderDAO customerFantasyOrderDAO) {
		DAORegistry.customerFantasyOrderDAO = customerFantasyOrderDAO;
	}

	public final static ReferralDiscountSettingsDAO getReferralDiscountSettingsDAO() {
		return referralDiscountSettingsDAO;
	}

	public final void setReferralDiscountSettingsDAO(
			ReferralDiscountSettingsDAO referralDiscountSettingsDAO) {
		DAORegistry.referralDiscountSettingsDAO = referralDiscountSettingsDAO;
	}

	public final static DiscountCodeTrackingDAO getDiscountCodeTrackingDAO() {
		return discountCodeTrackingDAO;
	}

	public final void setDiscountCodeTrackingDAO(
			DiscountCodeTrackingDAO discountCodeTrackingDAO) {
		DAORegistry.discountCodeTrackingDAO = discountCodeTrackingDAO;
	}

	public final static PreOrderPageAuditDAO getPreOrderPageAuditDAO() {
		return preOrderPageAuditDAO;
	}

	public final void setPreOrderPageAuditDAO(
			PreOrderPageAuditDAO preOrderPageAuditDAO) {
		DAORegistry.preOrderPageAuditDAO = preOrderPageAuditDAO;
	}

	public final static OrderTicketGroupDAO getOrderTicketGroupDAO() {
		return orderTicketGroupDAO;
	}

	public final void setOrderTicketGroupDAO(
			OrderTicketGroupDAO orderTicketGroupDAO) {
		DAORegistry.orderTicketGroupDAO = orderTicketGroupDAO;
	}

	public final static TicketGroupDAO getTicketGroupDAO() {
		return ticketGroupDAO;
	}

	public final void setTicketGroupDAO(TicketGroupDAO ticketGroupDAO) {
		DAORegistry.ticketGroupDAO = ticketGroupDAO;
	}

	public final static ReferralContestDAO getReferralContestDAO() {
		return referralContestDAO;
	}

	public final void setReferralContestDAO(ReferralContestDAO referralContestDAO) {
		DAORegistry.referralContestDAO = referralContestDAO;
	}

	public final static ReferralContestParticipantsDAO getReferralContestParticipantsDAO() {
		return referralContestParticipantsDAO;
	}

	public final void setReferralContestParticipantsDAO(
			ReferralContestParticipantsDAO referralContestParticipantsDAO) {
		DAORegistry.referralContestParticipantsDAO = referralContestParticipantsDAO;
	}

	public final static ReferralContestEmailVersionDAO getReferralContestEmailVersionDAO() {
		return referralContestEmailVersionDAO;
	}

	public final void setReferralContestEmailVersionDAO(
			ReferralContestEmailVersionDAO referralContestEmailVersionDAO) {
		DAORegistry.referralContestEmailVersionDAO = referralContestEmailVersionDAO;
	}

	public static ReferralContestWinnerDAO getReferralContestWinnerDAO() {
		return referralContestWinnerDAO;
	}

	public final void setReferralContestWinnerDAO(
			ReferralContestWinnerDAO referralContestWinnerDAO) {
		DAORegistry.referralContestWinnerDAO = referralContestWinnerDAO;
	} 
	
	public final static CustomerCheckoutVisitOfferDAO getCustomerCheckoutVisitOfferDAO() {
		return customerCheckoutVisitOfferDAO;
	}

	public final void setCustomerCheckoutVisitOfferDAO(
			CustomerCheckoutVisitOfferDAO customerCheckoutVisitOfferDAO) {
		DAORegistry.customerCheckoutVisitOfferDAO = customerCheckoutVisitOfferDAO;
	}

	public final static CheckOutOfferAppliedAuditDAO getCheckOutOfferAppliedAuditDAO() {
		return checkOutOfferAppliedAuditDAO;
	}

	public final void setCheckOutOfferAppliedAuditDAO(
			CheckOutOfferAppliedAuditDAO checkOutOfferAppliedAuditDAO) {
		DAORegistry.checkOutOfferAppliedAuditDAO = checkOutOfferAppliedAuditDAO;
	} 
	
	public static QuizCustomerFriendDAO getQuizCustomerFriendDAO() {
		return quizCustomerFriendDAO;
	}
	public final void setQuizCustomerFriendDAO(
			QuizCustomerFriendDAO quizCustomerFriendDAO) {
		DAORegistry.quizCustomerFriendDAO = quizCustomerFriendDAO;
	}

	public static QuizCustomerInvitedFriendsDAO getQuizCustomerInvitedFriendsDAO() {
		return quizCustomerInvitedFriendsDAO;
	}

	public final void setQuizCustomerInvitedFriendsDAO(
			QuizCustomerInvitedFriendsDAO quizCustomerInvitedFriendsDAO) {
		DAORegistry.quizCustomerInvitedFriendsDAO = quizCustomerInvitedFriendsDAO;
	}

	public static CustomerLoyaltyTrackingDAO getCustomerLoyaltyTrackingDAO() {
		return customerLoyaltyTrackingDAO;
	}

	public final void setCustomerLoyaltyTrackingDAO(
			CustomerLoyaltyTrackingDAO customerLoyaltyTrackingDAO) {
		DAORegistry.customerLoyaltyTrackingDAO = customerLoyaltyTrackingDAO;
	}

	public static RtfPromotionalEarnLifeOffersDAO getRtfPromotionalEarnLifeOffersDAO() {
		return rtfPromotionalEarnLifeOffersDAO;
	}

	public final void setRtfPromotionalEarnLifeOffersDAO(
			RtfPromotionalEarnLifeOffersDAO rtfPromotionalEarnLifeOffersDAO) {
		DAORegistry.rtfPromotionalEarnLifeOffersDAO = rtfPromotionalEarnLifeOffersDAO;
	}

	public static CustomerNotificationDeliveryTrackingDAO getCustomerNotificationDeliveryTrackingDAO() {
		return customerNotificationDeliveryTrackingDAO;
	}

	public final void setCustomerNotificationDeliveryTrackingDAO(
			CustomerNotificationDeliveryTrackingDAO customerNotificationDeliveryTrackingDAO) {
		DAORegistry.customerNotificationDeliveryTrackingDAO = customerNotificationDeliveryTrackingDAO;
	}

	public static CustomerFuturePaymentInfoDAO getCustomerFuturePaymentInfoDAO() {
		return customerFuturePaymentInfoDAO;
	}

	public static void setCustomerFuturePaymentInfoDAO(CustomerFuturePaymentInfoDAO customerFuturePaymentInfoDAO) {
		DAORegistry.customerFuturePaymentInfoDAO = customerFuturePaymentInfoDAO;
	}

	public static QuizAffiliateSettingDAO getQuizAffiliateSettingDAO() {
		return quizAffiliateSettingDAO;
	}

	public final void setQuizAffiliateSettingDAO(QuizAffiliateSettingDAO quizAffiliateSettingDAO) {
		DAORegistry.quizAffiliateSettingDAO = quizAffiliateSettingDAO;
	}

	public static QuizAffiliateRewardHistoryDAO getQuizAffiliateRewardHistoryDAO() {
		return quizAffiliateRewardHistoryDAO;
	}

	public final void setQuizAffiliateRewardHistoryDAO(QuizAffiliateRewardHistoryDAO quizAffiliateRewardHistoryDAO) {
		DAORegistry.quizAffiliateRewardHistoryDAO = quizAffiliateRewardHistoryDAO;
	}

	public static RtfConfigContestClusterNodesDAO getRtfConfigContestClusterNodesDAO() {
		return rtfConfigContestClusterNodesDAO;
	}

	public final void setRtfConfigContestClusterNodesDAO(RtfConfigContestClusterNodesDAO rtfConfigContestClusterNodesDAO) {
		DAORegistry.rtfConfigContestClusterNodesDAO = rtfConfigContestClusterNodesDAO;
	}

	public final static com.zonesws.webservices.dao.services.RtfCatTicketDAO getRtfCatTicketDAO() {
		return rtfCatTicketDAO;
	}

	public final void setRtfCatTicketDAO(com.zonesws.webservices.dao.services.RtfCatTicketDAO rtfCatTicketDAO) {
		DAORegistry.rtfCatTicketDAO = rtfCatTicketDAO;
	}

	public static CustomerFeedBackTrackingDAO getCustomerFeedBackTrackingDAO() {
		return customerFeedBackTrackingDAO;
	}

	public final void setCustomerFeedBackTrackingDAO(CustomerFeedBackTrackingDAO customerFeedBackTrackingDAO) {
		DAORegistry.customerFeedBackTrackingDAO = customerFeedBackTrackingDAO;
	}

	public static RtfGiftCardDAO getRtfGiftCardDAO() {
		return rtfGiftCardDAO;
	}

	public final void setRtfGiftCardDAO(RtfGiftCardDAO rtfGiftCardDAO) {
		DAORegistry.rtfGiftCardDAO = rtfGiftCardDAO;
	}

	public static GiftCardBrandDAO getGiftCardBrandDAO() {
		return giftCardBrandDAO;
	}

	public final void setGiftCardBrandDAO(GiftCardBrandDAO giftCardBrandDAO) {
		DAORegistry.giftCardBrandDAO = giftCardBrandDAO;
	}

	public static RtfGiftCardQuantityDAO getRtfGiftCardQuantityDAO() {
		return rtfGiftCardQuantityDAO;
	}

	public final void setRtfGiftCardQuantityDAO(RtfGiftCardQuantityDAO rtfGiftCardQuantityDAO) {
		DAORegistry.rtfGiftCardQuantityDAO = rtfGiftCardQuantityDAO;
	}

	public static RtfGiftCardOrderDAO getRtfGiftCardOrderDAO() {
		return rtfGiftCardOrderDAO;
	}

	public final void setRtfGiftCardOrderDAO(RtfGiftCardOrderDAO rtfGiftCardOrderDAO) {
		DAORegistry.rtfGiftCardOrderDAO = rtfGiftCardOrderDAO;
	}

	public static GiftCardFileAttachmentDAO getGiftCardFileAttachmentDAO() {
		return giftCardFileAttachmentDAO;
	}

	public final void setGiftCardFileAttachmentDAO(GiftCardFileAttachmentDAO giftCardFileAttachmentDAO) {
		DAORegistry.giftCardFileAttachmentDAO = giftCardFileAttachmentDAO;
	}
	
	public final static PollingCustomerRewardsDAO getPollingCustomerRewardsDAO() {
		return pollingCustomerRewardsDAO;
	}
	
	public final void  setPollingCustomerRewardsDAO(PollingCustomerRewardsDAO pollingCustomerRewardsDAO) {
		DAORegistry.pollingCustomerRewardsDAO = pollingCustomerRewardsDAO;
	}
	
	
}
