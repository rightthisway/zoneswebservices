package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.DiscountCodeTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public class DiscountCodeTrackingDAO extends HibernateDAO<Integer, DiscountCodeTracking> implements com.zonesws.webservices.dao.services.DiscountCodeTrackingDAO{

	public DiscountCodeTracking getTrackingByCustomerIdByTicketId(Integer cId, Integer eId,Integer tId,String sessionId,ApplicationPlatForm platForm){
		try{
			return findSingle("FROM DiscountCodeTracking WHERE customerId = ? and eventId = ? and catTicketGroupId=? " +
					"and sessionId=? and platform=? ", new Object[]{cId,eId,tId,sessionId,platForm});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new DiscountCodeTracking();
	}
	
}
