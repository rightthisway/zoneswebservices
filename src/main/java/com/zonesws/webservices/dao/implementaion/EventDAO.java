package com.zonesws.webservices.dao.implementaion;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.enums.EventStatus;
import com.zonesws.webservices.enums.HomeCardType;
import com.zonesws.webservices.enums.LoyalFanType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.MapUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.list.VenueResult;

public class EventDAO extends HibernateDAO<Integer, Event> implements com.zonesws.webservices.dao.services.EventDAO {
	
	private static Session eventSession = null;
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	private static String rtfQuizMasterLinkedServer;
	
	public static Session getEventSession() {
		return EventDAO.eventSession;
	}

	public static void setEventSession(Session eventSession) {
		EventDAO.eventSession = eventSession;
	}


	public static String getRtfQuizMasterLinkedServer() {
		return rtfQuizMasterLinkedServer;
	}

	public final void setRtfQuizMasterLinkedServer(String rtfQuizMasterLinkedServer) {
		EventDAO.rtfQuizMasterLinkedServer = rtfQuizMasterLinkedServer;
	}

	/*

	public static Session getSession() {
		return EventDAO.session;
	}

	public static void setSession(Session session) {
		EventDAO.session = session;
	}
*/
	public List<Event> getAllEvents(Integer eventId,String name,Date startDate,Date endDate,Integer artistId,Integer venueId,String location,String city,String state) {
		try {
		String whereClause="WHERE status =? AND ";
		List<Object> paramList = new ArrayList<Object>();
		boolean flag=false;
		paramList.add(1);
		if(eventId !=null ){
			flag=true;
			whereClause += "id = ? AND ";
			paramList.add(eventId);
		}
		if(name !=null && !name.isEmpty()){
			flag=true;
			whereClause += "name like ? AND ";
			paramList.add("%"+name+"%");
		}
		if(startDate !=null){
			flag=true;
			whereClause += "eventDate >= ? AND ";
			paramList.add(startDate);
		}
		if(endDate !=null){
			flag=true;
			whereClause += "eventDate <= ? AND ";
			paramList.add(endDate);
		}
		
		if(artistId !=null ){
			flag=true;
			whereClause += "artist.id = ? AND ";
			paramList.add(artistId);
		}
		if(venueId !=null ){
			flag=true;
			whereClause += "venue.id = ? AND ";
			paramList.add(venueId);
		}
		if(location !=null && !location.isEmpty()){
			flag=true;
			whereClause += "venue.location like ? AND ";
			paramList.add("%"+location+"%");
		}
		if(city !=null && !city.isEmpty()){
			flag=true;
			whereClause += "venue.city like ? AND ";
			paramList.add("%"+city+"%");
		}
		if(state !=null && !state.isEmpty()){
			flag=true;
			whereClause += "venue.state = ? AND ";
			paramList.add(state);
		}
		List<Event> finalList = new ArrayList<Event>();
		if(flag){
			whereClause = whereClause.substring(0, whereClause.length()-4);
			List<Event> events = find("FROM Event " + whereClause ,paramList.toArray());
			
			if(null != events && !events.isEmpty()) {
				/*Date curntDate = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			    Integer minutes = DAORegistry.getPropertyDAO().getEventAPIRemovalTime();
			    Integer nyMinutes = DAORegistry.getPropertyDAO().getEventAPIRemovalTimeForNY();
			    Long eventTime,curntTime,nyCurntTime;
			    curntTime = curntDate.getTime()+(1000*60*minutes);
			    nyCurntTime = curntDate.getTime()+(1000*60*nyMinutes);
				SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");*/
				
				for(Event event:events){
					
					/*if(event.getTime() != null) {
						eventTime = dateTimeFormat.parse(event.getEventDate()+" "+event.getTime()).getTime();
					} else {
						eventTime = event.getEventDate().getTime();
					}
					if(null != event.getVenue().getState() && event.getVenue().getState().equals("NY")) {
						if(eventTime <= nyCurntTime) {
							continue;
						}
					} else {
						if(eventTime <= curntTime) {
							continue;
						}
					}*/
					
					/*				
	 				ZonesEventCategoryMapping zonesEventCategoryMapping= DAORegistry.getZonesEventCategoryMappingDAO().getZonesEventCategoryMappingByEventId(event.getTmatEventId());
					if(zonesEventCategoryMapping!=null){
						event.setMapPath("http://zonesmaps.com/" + event.getVenue().getTmatVenueId()+ "_" + zonesEventCategoryMapping.getGroupName() + ".gif");
					}else{
						event.setMapPath("Map is not available");
					}
					*/
					List<String> category = null;//DAORegistry.getCategoryDAO().getCategoryByArtistId(event.getArtist().getId());
					if(category!=null && !category.isEmpty()){
						//event.setMapPath("http://zonesmaps.com/" + event.getVenue().getTmatVenueId()+ "_" + category.get(0) + ".gif");
					}else{
						event.setMapUrl("Map is not available");
					}
					
	//				event.setTime();
					finalList.add(event);
				}
			}
			return finalList;
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Event> getAllAwayEvents(Integer artistId) {
		String whereClause="WHERE status =? AND ";
		List<Object> paramList = new ArrayList<Object>();
		boolean flag=false;
		paramList.add(1);
		Artist artist = null;//TMATDAORegistry.getArtistDAO().get(artistId);
		if(artist !=null ){
			flag=true;
			whereClause += "name like ? ";
			paramList.add("%vs.%"+ artist.getName());
		}
		
		if(flag){
//			whereClause = whereClause.substring(0, whereClause.length()-4);
			List<Event> events = find("FROM Event " + whereClause ,paramList.toArray());
			for(Event event:events){
				/*				
 				ZonesEventCategoryMapping zonesEventCategoryMapping= DAORegistry.getZonesEventCategoryMappingDAO().getZonesEventCategoryMappingByEventId(event.getTmatEventId());
				if(zonesEventCategoryMapping!=null){
					event.setMapPath("http://zonesmaps.com/" + event.getVenue().getTmatVenueId()+ "_" + zonesEventCategoryMapping.getGroupName() + ".gif");
				}else{
					event.setMapPath("Map is not available");
				}
				*/
				List<String> category = null;//DAORegistry.getCategoryDAO().getCategoryByArtistId(event.getArtist().getId());
				if(category!=null && !category.isEmpty()){
					//event.setMapPath("http://zonesmaps.com/" + event.getVenue().getTmatVenueId()+ "_" + category.get(0) + ".gif");
				}else{
					event.setMapUrl("Map is not available");
				}
				
//				event.setTime();
			}
			return events;
		}
		return null;
	}
	
	/**
	 * method to get Event by TMAT TourId 
	 * @param tourId , TMAT TourId
	 * @return List of Events
	 */
	public Event getEventByTMATEventId(Integer eventId) {
		return findSingle("FROM Event WHERE tmatEventId = ? AND eventStatus = ?", new Object[]{eventId,Status.ACTIVE});
	}
	
	public List<Event> getEventsForArtistPriceRange(Integer artistId,Date startDate,Date endDate) {
		try {
			String whereClause="WHERE status =? ";
			List<Object> paramList = new ArrayList<Object>();
			boolean flag=false;
			paramList.add(1);
			
			if(artistId !=null ){
				flag=true;
				whereClause += " AND artist.id = ?";
				paramList.add(artistId);
			}
			if(startDate !=null){
				flag=true;
				whereClause += " AND eventDate >= ?";
				paramList.add(startDate);
			}
			if(endDate !=null){
				flag=true;
				whereClause += " AND eventDate <= ?";
				paramList.add(endDate);
			}
			
			List<Event> events = null;
			if(flag){
				//whereClause = whereClause.substring(0, whereClause.length()-4);
				events = find("FROM Event " + whereClause +" order by eventDate" ,paramList.toArray());
			}
			return events;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void deleteZoneEvents(List<Integer> eventIds) {
		String sql ="update event set status = 3,last_update=GETDATE() where tmat_event_id in (:eventIds) ";
		Query query = null;
		Session session = null;
		try {
			session = getSession();
			query = session.createSQLQuery(sql).setParameterList("eventIds", eventIds);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
	}
	
//	
//	public List<Event> getAllEventsByHomeCardType(HomeCardType homeCardType,Date startDate,Date endDate){
//		
//		try {
//			String whereClause="WHERE eventStatus =? AND ";
//			List<Object> paramList = new ArrayList<Object>();
//			boolean flag=true;
//			paramList.add(Status.ACTIVE);
//			
//			if(homeCardType.equals(HomeCardType.DATE_SEARCH_CARD)){
//				if(startDate !=null){
//					flag=true;
//					whereClause += "eventDate >= ? AND ";
//					paramList.add(startDate);
//				}
//				if(endDate !=null){
//					flag=true;
//					whereClause += "eventDate <= ? AND ";
//					paramList.add(endDate);
//				}
//			}
//			whereClause += " state like ? AND ";
//			paramList.add("%"+"NY"+"%");
//			
//			List<Event> finalList = new ArrayList<Event>();
//			if(flag){
//				whereClause = whereClause.substring(0, whereClause.length()-4);
//				finalList = find("FROM Event " + whereClause ,paramList.toArray());
//				
//				if(null != finalList && !finalList.isEmpty()) {
//					
//					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");    
//					SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//				}else{
//					finalList = new ArrayList<Event>();
//				}
//				return finalList;
//			}
//			}catch (Exception e) {
//				e.printStackTrace();
//			}
//			return null;
//		
//	}
	
	public List<Event> getAllEventsByHomeCardType(HomeCardType homeCardType,Date startDate,Date endDate,String state,
			Integer pageNumber,Integer maxRows,Set<Integer> skipEventIds){
		String whereClause= "";
		Session session = getSession();
		try {
			whereClause="select A.* from  (select event_id as eventId, 0 as artistId, " +
					"'' as artistName , e.child_category_id as childCategoryId, " +
					"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName," +
					" e.city as venueCity, e.state as venueState, e.country as venueCountry,e.postal_code as venueZipcode" +
					",CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
					" WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName, " +
					" e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
					"e.parent_category_id as parentId, e.parent_category_name as parentName, " +
					" e.reward_thefan_enabled as rewardTheFan," +
					" e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName," +
					" e.min_price as minPrice ,e.max_price as maxPrice,e.event_date,e.event_time " +
					"from events_rtf e WITH(NOLOCK) where e.status=1 and e.min_price > 0 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event ";
			
			if(startDate !=null && endDate !=null){
				whereClause += " AND (convert(date, e.event_date) between '"+dateTimeFormat.format(startDate)+"' AND  '"+dateTimeFormat.format(endDate)+"') ";
			}else if (startDate !=null){
				whereClause += " AND convert(date, e.event_date) >= '"+dateTimeFormat.format(startDate)+"' ";
			}else if(endDate !=null){
				whereClause += " AND convert(date, e.event_date) <= '"+dateTimeFormat.format(endDate)+"'  ";
			}
			
			if(null !=skipEventIds && !skipEventIds.isEmpty()){
				whereClause += " AND e.event_id not in (:skipEventIds) ";
			}
			
			if(null != state && !state.isEmpty()){
				whereClause += " AND e.state like '"+state+"' ";
			}
				
			whereClause +=" ) A where A.minPrice is not null order by A.event_date,A.event_time" +
					" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
			
			System.out.println(whereClause);
			
			Query query = null;
			if(null !=skipEventIds && !skipEventIds.isEmpty()){
				query = session.createSQLQuery(whereClause).setParameterList("skipEventIds", skipEventIds);
			}else{
				query = session.createSQLQuery(whereClause);
			}
			
			List<Object[]> result = (List<Object[]>)query.list();
			List<Event> finalList = new ArrayList<Event>();
			Event event=null;
			
			for (Object[] object : result) {
				event= new Event();
				event.setVenueId((Integer)object[5]);
				event.setVenueCategoryName((String)object[20]);
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					//continue;
				}*/
				
				event.setEventId((Integer)object[0]);
				//event.setArtistId((Integer)object[1]);
				//event.setArtistName((String)object[2]);
				event.setChildCategoryId((Integer)object[3]);
				event.setChildCategoryName((String)object[4]);
				
				event.setVenueName((String)object[6]);		
				event.setCity((String)object[7]);
				event.setState((String)object[8]);
				event.setCountry((String)object[9]);
				event.setPinCode((String)object[10]);
				String timeStr = DateUtil.formatTime((String)object[12]);
				event.setEventDateTime((String)object[11]+" "+timeStr);
				event.setEventDateStr((String)object[11]);
				event.setEventTimeStr(timeStr);
				event.setEventName((String)object[13]);
				event.setGrandChildCategoryId((Integer)object[14]);
				event.setGrandChildCategoryName((String)object[15]);
				event.setParentCategoryId((Integer)object[16]);
				event.setParentCategoryName((String)object[17]);
				//event.setRewardTheFanEnabled((Boolean)object[18]);
				event.setVenueCategoryId((Integer)object[19]);
				
				
				BigDecimal minPrice = (BigDecimal)object[21];
				BigDecimal maxPrice = (BigDecimal)object[22];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				finalList.add(event);
			}
			if(null != finalList && !finalList.isEmpty()) {
				
			}else{
				finalList = new ArrayList<Event>();
			}
			return finalList;
			}catch (Exception e) {
				System.err.println("EVENTDAO ERROR : "+whereClause);
				e.printStackTrace();
			}finally{
				session.close();
			}
			return null;
		
	}
	
	public List<Event> getAllEventsByHomeCardTypeOld(HomeCardType homeCardType,Date startDate,Date endDate,String state,
			Integer pageNumber,Integer maxRows,Set<Integer> skipEventIds){
		String whereClause= "";
		try {
			whereClause="select A.* from  (select event_id as eventId, 0 as artistId, " +
					"'' as artistName , e.child_category_id as childCategoryId, " +
					"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName," +
					" e.city as venueCity, e.state as venueState, e.country as venueCountry,e.postal_code as venueZipcode" +
					",CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
					" WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName, " +
					" e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
					"e.parent_category_id as parentId, e.parent_category_name as parentName, " +
					" e.reward_thefan_enabled as rewardTheFan," +
					" e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName," +
					"zp.minPrice , zp.maxPrice,e.event_date,e.event_time " +
					"from event_details e WITH(NOLOCK) CROSS APPLY ( select MIN(price) minPrice," +
					"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
					"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
					"zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event " +
					"and exists ( select * from category_ticket_group  ctg where status = 'ACTIVE' and ctg.event_id = e.event_id) " +
					"and exists (select event_id from event_artist ea where ea.event_id=e.event_id and tn_status=1 group by event_id having count(*)=1) ";
			
			if(startDate !=null && endDate !=null){
				whereClause += " AND (convert(date, e.event_date) between '"+dateTimeFormat.format(startDate)+"' AND  '"+dateTimeFormat.format(endDate)+"') ";
			}else if (startDate !=null){
				whereClause += " AND convert(date, e.event_date) >= '"+dateTimeFormat.format(startDate)+"' ";
			}else if(endDate !=null){
				whereClause += " AND convert(date, e.event_date) <= '"+dateTimeFormat.format(endDate)+"'  ";
			}
			
			if(null !=skipEventIds && !skipEventIds.isEmpty()){
				whereClause += " AND e.event_id not in (:skipEventIds) ";
			}
			
			if(null != state && !state.isEmpty()){
				whereClause += " AND e.state like '"+state+"' ";
			}
				
			whereClause +=" ) A where A.minPrice is not null order by A.event_date,A.event_time" +
					" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
			
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			
			Query query = null;
			if(null !=skipEventIds && !skipEventIds.isEmpty()){
				query = eventSession.createSQLQuery(whereClause).setParameterList("skipEventIds", skipEventIds);
			}else{
				query = eventSession.createSQLQuery(whereClause);
			}
			
			List<Object[]> result = (List<Object[]>)query.list();
			List<Event> finalList = new ArrayList<Event>();
			Event event=null;
			
			for (Object[] object : result) {
				event= new Event();
				event.setVenueId((Integer)object[5]);
				event.setVenueCategoryName((String)object[20]);
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					//continue;
				}*/
				
				event.setEventId((Integer)object[0]);
				//event.setArtistId((Integer)object[1]);
				//event.setArtistName((String)object[2]);
				event.setChildCategoryId((Integer)object[3]);
				event.setChildCategoryName((String)object[4]);
				
				event.setVenueName((String)object[6]);		
				event.setCity((String)object[7]);
				event.setState((String)object[8]);
				event.setCountry((String)object[9]);
				event.setPinCode((String)object[10]);
				String timeStr = DateUtil.formatTime((String)object[12]);
				event.setEventDateTime((String)object[11]+" "+timeStr);
				event.setEventDateStr((String)object[11]);
				event.setEventTimeStr(timeStr);
				event.setEventName((String)object[13]);
				event.setGrandChildCategoryId((Integer)object[14]);
				event.setGrandChildCategoryName((String)object[15]);
				event.setParentCategoryId((Integer)object[16]);
				event.setParentCategoryName((String)object[17]);
				//event.setRewardTheFanEnabled((Boolean)object[18]);
				event.setVenueCategoryId((Integer)object[19]);
				
				
				BigDecimal minPrice = (BigDecimal)object[21];
				BigDecimal maxPrice = (BigDecimal)object[22];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				finalList.add(event);
			}
			if(null != finalList && !finalList.isEmpty()) {
				
			}else{
				finalList = new ArrayList<Event>();
			}
			return finalList;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				System.err.println("EVENTDAO ERROR : "+whereClause);
				e.printStackTrace();
			}
			return null;
		
	}
	
	
public List<Event> getAllEventsByHomeCardTypeNew(HomeCardType homeCardType,Date startDate,Date endDate){
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			System.out.println("DAO -Begins :"+new Date());
			PreparedStatement st = eventSession.connection().prepareStatement("{call reward_the_fan_event_details_proc}");
			ResultSet resultSet = st.executeQuery();
			System.out.println(resultSet.getFetchSize());
			System.out.println("DAO -Ends :"+new Date());
			List<Event> finalList = new ArrayList<Event>();
			Event event=null;
			int i=1;
			while(resultSet.next()){
				i++;
			}
			System.out.println("TOATL SIZE :"+i);
			if(null != finalList && !finalList.isEmpty()) {
				
			}else{
				finalList = new ArrayList<Event>();
			}
			return finalList;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}
	public HashMap<String, Object> getAutoSearchResultNew(String searchKey,Integer eventPageNumber ,Integer maxRows) {
		Session session = getSession();
	try {
		/*eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}*/
		
		//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
		String queryStr = "select  top 15  'artist'  as search_type, e.artist_id as Id, e.artist_name as Name,0 as totalcount" +
				" from events_rtf e WITH(READCOMMITTED)" +
				" where e.status=1 and e.reward_thefan_enabled = 1 and e.artist_display_on_search=1 and e.Venu_map =1" +
				" AND e.artist_status=1 and e.artist_name is not null" +
				" and e.artist_name like '%"+searchKey+"%' group by e.artist_id, e.artist_name" +
				" union all" +
				" select  top 15  'venue'  as search_type, e.venue_id as Id, e.building as Name,0 as totalcount" +
				" from events_rtf e WITH(READCOMMITTED)" +
				" where e.status=1 and reward_thefan_enabled = 1 and e.venue_display_on_search=1 and e.venu_map=1" +
				" and e.building is not null AND e.building like '%"+searchKey+"%' group by e.venue_id, e.building";
		
		Query query = session.createSQLQuery(queryStr);
		//System.out.println(query);
		List<Object[]> result = (List<Object[]>)query.list();
		
		HashMap<String, Object> allObj = Util.autoSearchResultMapper(result);
		
		return allObj;
		}catch (Exception e) {
			/*if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}*/
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return null;
	
	}
	public HashMap<String, Object> getAutoSearchResult(String searchKey,Integer eventPageNumber ,Integer maxRows) {
		Session session = getSession();
	try {
		/*eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}*/
		
		//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
		String queryStr = "exec SP_get_event_Auto_info_rtf @searchinput = \""+searchKey+"\",@pageNo=1,@maxRow='"+maxRows+"',@excludeEventDays="+1;
		
		Query query = session.createSQLQuery(queryStr);
		//System.out.println(query);
		List<Object[]> result = (List<Object[]>)query.list();
		
		HashMap<String, Object> allObj = Util.autoSearchResultMapper(result);
		
		return allObj;
		}catch (Exception e) {
			/*if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}*/
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return null;
	
}
	
	
	public HashMap<String, Object> getAllEventsByParentIds(String searchKey,Integer eventPageNumber , Integer maxRows,String startDate,String endDate,
			Integer artistId,Integer grandChildId,Integer venueId,Integer childId,String searchType,Integer artistPageNo,Integer venuePageNo,
			String searchMode,Integer parentCatId,String zipCodeStr,String stateShortName,String artistRefValue,ArtistReferenceType artistRefType){
		Session session = getSession();
		try {
			/*eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}*/
			//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
			
			String queryStr = " ";
			if(searchMode.equals("EVENTS")) {
				queryStr = " select distinct 'event' as search_type,e.event_id as Id,e.event_name as Name," +
				" str_event_date as eventDate,str_event_time as eventTime,e.building as venuename,e.venue_id as venueid," +
				" e.venue_category_name as venuecategoryname,e.city as city,e.state as state,e.country as country," +
				" e.min_Price as minPrice,e.max_Price as maxPrice,e.event_datetime as Sorteventdate,null as artist_start_date," +
				" null as artist_end_date,'PARENT_ID' as ref_type,"+parentCatId+" as ref_value, 0 as total_count" +
				" from events_rtf e WITH(READCOMMITTED)  " +
				" where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and e.reward_thefan_enabled=1 and e.venu_map=1 " +
				" and e.parent_category_id="+parentCatId;
		
				if(startDate !=null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()){
					queryStr += " AND e.event_date BETWEEN '"+dateTimeFormat.format(df.parse(startDate))+"' and '"+dateTimeFormat.format(df.parse(endDate))+"' "; 
				}
				if(stateShortName !=null && !stateShortName.isEmpty()) {
					stateShortName = stateShortName.replaceAll("\\W", "%");
					queryStr = queryStr +" AND e.state='"+stateShortName+"'";
				}
				queryStr += " AND e.min_Price is not null order by e.event_datetime,e.event_name" +
						"  OFFSET ("+eventPageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+"  ROWS ONLY";
				
			} else if(searchMode.equals("ARTIST")) {
				queryStr = " select distinct 'artist' as search_type,e.artist_id as Id,e.artist_name as Name," +
						" NULL as eventdate,NULL as eventtime,NULL as venuename,NULL as venueid,NULL as venuecategoryname," +
						" NULL as city,NULL as state,NULL as country,0.00 as minPrice,0.00 as maxPrice,null as artist_id," +
						" null as artist_name,'PARENT_ID' as ref_type,convert(varchar,"+parentCatId+") as ref_value, 0 as total_count" +
						" from events_rtf e WITH(READCOMMITTED)" +
						" where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and e.reward_thefan_enabled = 1 and e.artist_display_on_search=1 and e.Venu_map =1" +
						" AND e.artist_status=1 and e.artist_id is not null  " +
						" and e.parent_category_id="+parentCatId;
		
				if(startDate !=null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()){
					queryStr += " AND e.event_date BETWEEN '"+dateTimeFormat.format(df.parse(startDate))+"' and '"+dateTimeFormat.format(df.parse(endDate))+"' "; 
				}
				if(stateShortName !=null && !stateShortName.isEmpty()) {
					stateShortName = stateShortName.replaceAll("\\W", "%");
					queryStr = queryStr +" AND e.state='"+stateShortName+"'";
				}
				queryStr += "  order by e.artist_name" +
						"  OFFSET ("+artistPageNo+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+"  ROWS ONLY";
			} else if(searchMode.equals("VENUE")) {
				queryStr = " select distinct 'venue' as search_type,e.venue_id as Id,e.building as Name," +
				" NULL as eventdate,NULL as eventtime,NULL as venuename,NULL as venueid,NULL as venuecategoryname," +
				" e.city as city,e.state as state,e.country as country,0.00 as minPrice,0.00 as maxPrice,null as artist_id," +
				" null as artist_name,'PARENT_ID' as ref_type,"+parentCatId+" as ref_value, 0 as total_count" +
				" from events_rtf e WITH(READCOMMITTED)" +
				" where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and reward_thefan_enabled = 1 and e.venue_display_on_search=1 and e.venu_map=1" +
				" and e.parent_category_id="+parentCatId;

				if(startDate !=null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()){
					queryStr += " AND e.event_date BETWEEN '"+dateTimeFormat.format(df.parse(startDate))+"' and '"+dateTimeFormat.format(df.parse(endDate))+"' "; 
				}
				if(stateShortName !=null && !stateShortName.isEmpty()) {
					stateShortName = stateShortName.replaceAll("\\W", "%");
					queryStr = queryStr +" AND e.state='"+stateShortName+"'";
				}
				queryStr += " order by e.building" +
						"  OFFSET ("+venuePageNo+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+"  ROWS ONLY";
				
			} 
			
			Query query = session.createSQLQuery(queryStr);
			//System.out.println(query);
			List<Object[]> result = (List<Object[]>)query.list();
			//System.out.println("GENERALISED_SEARCH DAO -Ends :"+new Date());
			
			HashMap<String, Object> allObj = new HashMap<String, Object>();	
			if(null != searchType){
				allObj = Util.normalSearchResultMapper(result);
			}
			
			return allObj;
			}catch (Exception e) {
				/*if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}*/
				e.printStackTrace();
			}
			finally{
				session.close();
			}
			return null;
		
	}
	
	public HashMap<String, Object> getAllEventsBySearchKey(Boolean showAllEvents, String searchKey,Integer eventPageNumber , Integer maxRows,String startDate,String endDate,
			Integer artistId,Integer grandChildId,Integer venueId,Integer childId,String searchType,Integer artistPageNo,Integer venuePageNo,
			String searchMode,Integer parentCatId,String zipCodeStr,String stateShortName,String artistRefValue,ArtistReferenceType artistRefType){
		Session session = getSession();
		try {
			/*eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}*/
			//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
			
			String searchInput = "";
			if(showAllEvents) {
				searchInput = "@searchinput=''";
			}else {
				searchInput = "@searchinput = \""+searchKey+"\"";
			}
			
			String queryStr = "exec SP_get_event_info_rtf_new "+searchInput+",@searchType='"+searchType+"',@pageNo='"+eventPageNumber+"',@maxRow='"+maxRows+"'";
			
			//queryStr = queryStr +",@eventStartDate = NULL, @eventEndDate = NULL";
			
			if((null != startDate && !startDate.isEmpty()) && (endDate != null && !endDate.isEmpty())){
				queryStr = queryStr +",@eventStartDate =\""+startDate+"\",@eventEndDate =\""+endDate+"\"";
			}else{
				queryStr = queryStr +",@eventStartDate = NULL, @eventEndDate = NULL";
			}
			
			if(artistId !=null) {
				queryStr = queryStr +",@artistId ="+artistId;
			} else {
				queryStr = queryStr +",@artistId =NULL";
			}
			if(grandChildId !=null) {
				queryStr = queryStr +",@grantChildId="+grandChildId;
			}else {
				queryStr = queryStr +",@grantChildId =NULL";
			}
			if(childId !=null) {
				queryStr = queryStr +",@childId="+childId;
			}else {
				queryStr = queryStr +",@childId =NULL";
			}
			if(venueId !=null) {
				queryStr = queryStr +",@venueId="+venueId;
			}else {
				queryStr = queryStr +",@venueId =NULL";
			}
			if(searchMode != null) {
				queryStr = queryStr +",@searchMode ='"+searchMode+"'";	
			} else {
				queryStr = queryStr +",@searchMode =NULL";
			}
			queryStr = queryStr +",@apageNo ="+artistPageNo;
			queryStr = queryStr +",@vpageNo ="+venuePageNo;
			
			if(parentCatId !=null) {
				if(showAllEvents) {
					queryStr = queryStr +",@parentId=NULL";
				}else {
					queryStr = queryStr +",@parentId="+parentCatId;
				}
				
			}else {
				queryStr = queryStr +",@parentId =NULL";
			}
			
			/*if(zipCodeStr !=null && !zipCodeStr.isEmpty()) {
				queryStr = queryStr +",@zipCodeParam=\""+zipCodeStr+"\"";
			}else {
				queryStr = queryStr +",@zipCodeParam =NULL";
			}*/
			
			if(stateShortName !=null && !stateShortName.isEmpty()) {
				stateShortName = stateShortName.replaceAll("\\W", "%");
				queryStr = queryStr +",@stateParam=\""+stateShortName+"\"";
			}else {
				queryStr = queryStr +",@stateParam =NULL";
			}
			if(artistId != null && artistRefType != null && artistRefValue != null) {
				queryStr = queryStr +",@aRefType=\""+artistRefType.toString()+"\"";
				queryStr = queryStr +",@aRefValue=\""+artistRefValue+"\"";
			} else {
				queryStr = queryStr +",@aRefType =NULL,@aRefValue=NULL";
			}
			
			if(showAllEvents) {
				queryStr = queryStr +",@searchAllEvents ='Yes'";	
			} else {
				queryStr = queryStr +",@searchAllEvents =NULL";
			}
			
			Query query = session.createSQLQuery(queryStr);
			//System.out.println(query);
			List<Object[]> result = (List<Object[]>)query.list();
			//System.out.println("GENERALISED_SEARCH DAO -Ends :"+new Date());
			
			HashMap<String, Object> allObj = new HashMap<String, Object>();	
			if(null != searchType){
				if(searchType.equals("NORMALSEARCH")){
					allObj = Util.normalSearchResultMapper(result);
				}else{
					allObj = Util.autoSearchResultMapper(result);
				}
			}
			
			return allObj;
			}catch (Exception e) {
				/*if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}*/
				e.printStackTrace();
			}
			finally{
				session.close();
			}
			return null;
		
	}
	
	
	
	
	/*public List<Event> getUpcomingEvents(Integer pageNumber , Integer maxRows,Date startDate, Date endDate){
		try {
			String whereClause="select A.* from  (select event_id as eventId, e.artist_id as artistId, " +
			"REPLACE(e.artist_name,',','') as artistName , e.child_category_id as childCategoryId, " +
			"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName," +
			" e.city as venueCity, e.state as venueState, e.country as venueCountry,e.postal_code as venueZipcode" +
			",CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
			" WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName, " +
			" e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
			"e.parent_category_id as parentId, e.parent_category_name as parentName, " +
			" e.reward_thefan_enabled as rewardTheFan," +
			" e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName," +
			"zp.minPrice , zp.maxPrice,e.event_date,e.event_time " +
			"from event_details  e WITH(NOLOCK) inner join popular_event pe on e.event_id=pe.tmat_event_id  CROSS APPLY ( select MIN(price) minPrice," +
			"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
			"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
			"zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and e.event_date > GETDATE()+2 ";
			
			if(startDate !=null){
				whereClause += " AND e.event_date >= '"+dateTimeFormat.format(startDate)+" 00:00:00' ";
			}
			if(endDate !=null){
				whereClause += " AND e.event_date <= '"+dateTimeFormat.format(endDate)+" 23:59:59'  ";
			}
			
			whereClause +=" ) A where A.minPrice is not null order by A.event_date,A.event_time " +
			" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
	
	eventSession = EventDAO.getEventSession();
	if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
		eventSession = getSession();
		EventDAO.setEventSession(eventSession);
	}
	Query query = eventSession.createSQLQuery(whereClause);
	List<Object[]> result = (List<Object[]>)query.list();
	List<Event> finalList = new ArrayList<Event>();
	Event event=null;
	
	for (Object[] object : result) {
		event= new Event();
		event.setVenueId((Integer)object[5]);
		event.setVenueCategoryName((String)object[20]);
		/*event = MapUtil.testSvgDetailsByEvent(event);
		if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
			continue;
		}*/
		/*event.setEventId((Integer)object[0]);
		event.setArtistId((Integer)object[1]);
		event.setArtistName((String)object[2]);
		event.setChildCategoryId((Integer)object[3]);
		event.setChildCategoryName((String)object[4]);
		event.setVenueId((Integer)object[5]);
		event.setVenueName((String)object[6]);		
		event.setCity((String)object[7]);
		event.setState((String)object[8]);
		event.setCountry((String)object[9]);
		event.setPinCode((String)object[10]);
		event.setEventDateTime((String)object[11]+" "+(String)object[12]);
		event.setEventDateStr((String)object[11]);
		event.setEventTimeStr((String)object[12]);
		event.setEventName((String)object[13]);
		event.setGrandChildCategoryId((Integer)object[14]);
		event.setGrandChildCategoryName((String)object[15]);
		event.setParentCategoryId((Integer)object[16]);
		event.setParentCategoryName((String)object[17]);
		//event.setRewardTheFanEnabled((Boolean)object[18]);
		event.setVenueCategoryId((Integer)object[19]);
		event.setVenueCategoryName((String)object[20]);
		
		BigDecimal minPrice = (BigDecimal)object[21];
		BigDecimal maxPrice = (BigDecimal)object[22];
		event.setTicketMinPrice(minPrice.intValue());
		event.setTicketMaxPrice(maxPrice.intValue());
		finalList.add(event);
	}
	if(null != finalList && !finalList.isEmpty()) {
		
	}else{
		finalList = new ArrayList<Event>();
	}
	return finalList;
	}catch (Exception e) {
		e.printStackTrace();
	}
	return null;

}*/
	

	
	
public List<Event> getUpcomingEvents(){
		
		try {
			String whereClause="select A.*,count(1) over () totalRows from  (select event_id as eventId, 0 as artistId, " +
					"'' as artistName , e.child_category_id as childCategoryId, " +
					"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName," +
					" e.city as venueCity, e.state as venueState, e.country as venueCountry,e.postal_code as venueZipcode" +
					",CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
					" WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName, " +
					" e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
					"e.parent_category_id as parentId, e.parent_category_name as parentName, " +
					" e.reward_thefan_enabled as rewardTheFan," +
					" e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName," +
					" iif(zp.minPrice IS NULL, 0, zp.minPrice)  as minPrice, iif(zp.maxPrice IS NULL, 0, zp.maxPrice)  as maxPrice," +
					" 0 as salesCount,e.event_date,e.event_time " +//zp.minPrice , zp.maxPrice,
					"from event_details e WITH(NOLOCK) CROSS APPLY ( select MIN(price) minPrice," +
					"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
					"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
					"zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and zp.minPrice > 0 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and " +
					"e.reward_thefan_enabled=1  and e.event_time is not null ";
			
			whereClause +=" ) A  order by A.event_date,A.event_time " +
					"OFFSET ("+1+"-1)*"+50+" ROWS FETCH NEXT "+50+" ROWS ONLY";//where A.minPrice is not null
			
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<Event> finalList = new ArrayList<Event>();
			Event event=null;
			
			for (Object[] object : result) {
				event= new Event();
				event.setVenueId((Integer)object[5]);
				event.setVenueCategoryName((String)object[20]);
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}*/
				event.setEventId((Integer)object[0]);
				//event.setArtistId((Integer)object[1]);
				//event.setArtistName((String)object[2]);
				event.setChildCategoryId((Integer)object[3]);
				event.setChildCategoryName((String)object[4]);
				event.setVenueId((Integer)object[5]);
				event.setVenueName((String)object[6]);		
				event.setCity((String)object[7]);
				event.setState((String)object[8]);
				event.setCountry((String)object[9]);
				event.setPinCode((String)object[10]);
				String timeStr = DateUtil.formatTime((String)object[12]);
				event.setEventDateTime((String)object[11]+" "+timeStr);
				event.setEventDateStr((String)object[11]);
				event.setTempEventDate(df.parse((String)object[11]));
				event.setEventTimeStr(timeStr);
				event.setEventName((String)object[13]);
				event.setGrandChildCategoryId((Integer)object[14]);
				event.setGrandChildCategoryName((String)object[15]);
				event.setParentCategoryId((Integer)object[16]);
				event.setParentCategoryName((String)object[17]);
				//event.setRewardTheFanEnabled((Boolean)object[18]);
				event.setVenueCategoryId((Integer)object[19]);
				event.setVenueCategoryName((String)object[20]);
				
				BigDecimal minPrice = (BigDecimal)object[21];
				BigDecimal maxPrice = (BigDecimal)object[22];
				event.setSalesCount(null != object[23]?(Integer)object[23]:0);
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				event.setTotalEvents((Integer)object[26]);
				finalList.add(event);
			}
			if(null != finalList && !finalList.isEmpty()) {
				
			}else{
				finalList = new ArrayList<Event>();
			}
			return finalList;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}

 
public List<Event> getPopularEvents(Integer pageNumber , Integer maxRows,Date startDate,Date endDate,String state){
		
		try {
			String whereClause="select A.*,count(1) over () totalRows from  (select event_id as eventId, 0 as artistId, " +
					"'' as artistName , e.child_category_id as childCategoryId, " +
					"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150)," +
					"REPLACE(e.building,',','')) as venueName,e.city as venueCity, e.state as venueState, " +
					"e.country as venueCountry,e.postal_code as venueZipcode,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
					"WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName, " +
					"e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
					"e.parent_category_id as parentId, e.parent_category_name as parentName, e.reward_thefan_enabled as rewardTheFan," +
					"e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName,zp.minPrice , zp.maxPrice," +
					"es.salesCount,e.event_date,e.event_time from event_details e WITH(NOLOCK) CROSS APPLY ( select MIN(price) minPrice," +
					"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) where  zt.event_id=e.event_id and zt.status='ACTIVE' " +
					"and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp  CROSS APPLY (select A.id,A.salesCount from (select pes.event_id as id ," +
					"1000 as salesCount  from popular_events pes WITH(NOLOCK) union all select tmat_event_id as id,sales_count as salesCount from " +
					"popular_event WITH(NOLOCK) union all select ea.event_id as id,500 as salesCount from event_artist ea WITH(NOLOCK) inner join " +
					"popular_artist pa WITH(NOLOCK) on pa.artist_id=ea.artist_id ) A ) es where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and " +
					"es.id=e.event_id ";
			
			if(startDate !=null && endDate !=null){
				whereClause += " AND (convert(date, e.event_date) between '"+dateTimeFormat.format(startDate)+"' AND  '"+dateTimeFormat.format(endDate)+"') ";
			}else if (startDate !=null){
				whereClause += " AND convert(date, e.event_date) >= '"+dateTimeFormat.format(startDate)+"' ";
			}else if(endDate !=null){
				whereClause += " AND convert(date, e.event_date) <= '"+dateTimeFormat.format(endDate)+"'  ";
			}
			
			if(null != state && !state.isEmpty()){
				whereClause += " AND e.state like '"+state+"' ";
			}
			
			/*if(startDate !=null){
				whereClause += " AND e.event_date >= '"+dateTimeFormat.format(startDate)+" 00:00:00' ";
			}
			if(endDate !=null){
				whereClause += " AND e.event_date <= '"+dateTimeFormat.format(endDate)+" 23:59:59'  ";
			}*/
			
			whereClause +=" ) A where A.minPrice is not null order by A.salesCount desc " +
					" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
			
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<Event> finalList = new ArrayList<Event>();
			Event event=null;
			
			for (Object[] object : result) {
				event= new Event();
				event.setVenueId((Integer)object[5]);
				event.setVenueCategoryName((String)object[20]);
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}*/
				event.setEventId((Integer)object[0]);
				//event.setArtistId((Integer)object[1]);
				//event.setArtistName((String)object[2]);
				event.setChildCategoryId((Integer)object[3]);
				event.setChildCategoryName((String)object[4]);
				event.setVenueId((Integer)object[5]);
				event.setVenueName((String)object[6]);		
				event.setCity((String)object[7]);
				event.setState((String)object[8]);
				event.setCountry((String)object[9]);
				event.setPinCode((String)object[10]);
				event.setEventDateTime((String)object[11]+" "+(String)object[12]);
				event.setEventDateStr((String)object[11]);
				event.setEventTimeStr((String)object[12]);
				event.setEventName((String)object[13]);
				event.setGrandChildCategoryId((Integer)object[14]);
				event.setGrandChildCategoryName((String)object[15]);
				event.setParentCategoryId((Integer)object[16]);
				event.setParentCategoryName((String)object[17]);
				//event.setRewardTheFanEnabled((Boolean)object[18]);
				event.setVenueCategoryId((Integer)object[19]);
				event.setVenueCategoryName((String)object[20]);
				
				BigDecimal minPrice = (BigDecimal)object[21];
				BigDecimal maxPrice = (BigDecimal)object[22];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				event.setSalesCount((Integer)object[23]);
				event.setTotalEvents((Integer)object[26]);
				finalList.add(event);
			}
			if(null != finalList && !finalList.isEmpty()) {
				
			}else{
				finalList = new ArrayList<Event>();
			}
			return finalList;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}
	

	public List<Event> getAllFavoriteEvents(Integer customerId , Integer pageNumber , Integer maxRows){
	
	try {
		String whereClause="select A.*,count(1) over () totalRows from  (select e.event_id as eventId, 0 as artistId, " +
				"'' as artistName , e.child_category_id as childCategoryId, " +
				"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName," +
				" e.city as venueCity, e.state as venueState, e.country as venueCountry,e.postal_code as venueZipcode" +
				",CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				" WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName, " +
				" e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
				"e.parent_category_id as parentId, e.parent_category_name as parentName, " +
				" e.reward_thefan_enabled as rewardTheFan," +
				" e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName," +
				"zp.minPrice , zp.maxPrice,e.event_date,e.event_time " +
				"from event_details e WITH(NOLOCK) inner join cust_favorites_event_handler cf on " +
				"e.event_id=cf.event_id CROSS APPLY ( select MIN(price) minPrice," +
				"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
				"zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and cf.status='ACTIVE'" +
				" and cf.customer_id="+customerId;
		
		
		whereClause +=" ) A where A.minPrice is not null order by A.event_date,A.event_time " +
				" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}
		Query query = eventSession.createSQLQuery(whereClause);
		List<Object[]> result = (List<Object[]>)query.list();
		List<Event> finalList = new ArrayList<Event>();
		Event event=null;
		
		for (Object[] object : result) {
			event= new Event();
			event.setVenueId((Integer)object[5]);
			event.setVenueCategoryName((String)object[20]);
			/*event = MapUtil.testSvgDetailsByEvent(event);
			if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
				continue;
			}*/
			event.setEventId((Integer)object[0]);
			event.setArtistId((Integer)object[1]);
			event.setArtistName((String)object[2]);
			event.setChildCategoryId((Integer)object[3]);
			event.setChildCategoryName((String)object[4]);
			event.setVenueId((Integer)object[5]);
			event.setVenueName((String)object[6]);		
			event.setCity((String)object[7]);
			event.setState((String)object[8]);
			event.setCountry((String)object[9]);
			event.setPinCode((String)object[10]);
			event.setEventDateTime((String)object[11]+" "+(String)object[12]);
			event.setEventDateStr((String)object[11]);
			event.setEventTimeStr((String)object[12]);
			event.setEventName((String)object[13]);
			event.setGrandChildCategoryId((Integer)object[14]);
			event.setGrandChildCategoryName((String)object[15]);
			event.setParentCategoryId((Integer)object[16]);
			event.setParentCategoryName((String)object[17]);
			//event.setRewardTheFanEnabled((Boolean)object[18]);
			event.setVenueCategoryId((Integer)object[19]);
			event.setVenueCategoryName((String)object[20]);
			
			BigDecimal minPrice = (BigDecimal)object[21];
			BigDecimal maxPrice = (BigDecimal)object[22];
			event.setTicketMinPrice(minPrice.doubleValue());
			event.setTicketMaxPrice(maxPrice.doubleValue());
			event.setTotalEvents((Integer)object[25]);
			finalList.add(event);
		}
		if(null != finalList && !finalList.isEmpty()) {
			
		}else{
			finalList = new ArrayList<Event>();
		}
		return finalList;
		}catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
		}
		return null;
	
}
	
	
	
	public List<VenueResult> getVenuesByPageNumber(Integer pageNumber, Integer maxRows,String state){
		
		try {
			String whereClause="select A.*,count(1) over () totalRows from  (select distinct e.venue_id as venueId," +
					" CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName,e.city , e.state,e.country  " +
					" from event_details e WITH(NOLOCK) CROSS APPLY ( select MIN(price) minPrice," +
					"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
					"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
					"zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and e.reward_thefan_enabled=1  ";// and zp.minPrice is not null 
			
			if(null != state && !state.isEmpty()){
				whereClause += " and e.state like '"+state+"' ";
			}
			
			whereClause += " ) A order by A.venueName  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
			
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<VenueResult> finalList = new ArrayList<VenueResult>();
			VenueResult venueResult=null;
			
			for (Object[] object : result) {
				venueResult= new VenueResult();
				venueResult.setVenueId((Integer)object[0]);
				venueResult.setVenueName((String)object[1]);
				venueResult.setVenueCity((String)object[2]);
				venueResult.setVenueState((String)object[3]);
				venueResult.setVenueCountry((String)object[4]);
				venueResult.setTotalVenueCount((Integer)object[5]);
				finalList.add(venueResult);
			}
			if(null != finalList && !finalList.isEmpty()) {
				
			}else{
				finalList = new ArrayList<VenueResult>();
			}
			return finalList;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}
	

public Event getEventByEventId(Integer eventId,ProductType productType) {
	return findSingle("FROM Event WHERE eventId = ? AND eventStatus = ?", new Object[]{eventId,Status.ACTIVE});
}

public Event getEventById(Integer eventId,ProductType productType) {
	return findSingle("FROM Event WHERE eventId = ?", new Object[]{eventId});
	/*
	Event event = null;
	String sqlQuery = "select e.id AS eventId,e.name AS eventName,e.event_date AS eventDate,e.event_time AS eventTime,e.venue_id AS venueId,"+
	   "v.building as venueName,v.city AS city,v.state AS state,v.country AS country,v.postal_code AS pinCode,gcc.name AS grandChildCategoryName," +
	   "cg.name AS childCategoryName,pc.name as parentCategoryName,pc.id AS parentCategoryId, " +
	   "vc.id AS venueCategoryId,vc.category_group AS venueCategoryName,gcc.id AS grandChildCategoryId,cg.id AS childCategoryId "+
	   "from event e join venue v on e.venue_id=v.id " +
	   "join event_category ec on e.id=ec.event_id " +
	   "join grand_child_category gcc on ec.grand_child_category_id=gcc.id " +
	   "join child_category cg on ec.child_category_id=cg.id " +
	   "join parent_category pc on ec.parent_category_id=pc.id " +
	   "left join event_ticket_count_details t on e.id=t.event_id " +
	   "left join venue_category vc on v.id=vc.venue_id " +
	   "WHERE e.id="+eventId;
	
	try {
		eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}
		SQLQuery query = eventSession.createSQLQuery(sqlQuery);
		query.addScalar("eventId", Hibernate.INTEGER);
		query.addScalar("eventName", Hibernate.STRING);
		query.addScalar("eventDate", Hibernate.DATE);
		query.addScalar("eventTime", Hibernate.TIME);
		query.addScalar("venueId", Hibernate.INTEGER);
		query.addScalar("venueName", Hibernate.STRING);
		query.addScalar("city", Hibernate.STRING);
		query.addScalar("state", Hibernate.STRING);
		query.addScalar("country", Hibernate.STRING);
		query.addScalar("pinCode", Hibernate.STRING);
		query.addScalar("venueCategoryId", Hibernate.INTEGER);
		query.addScalar("venueCategoryName", Hibernate.STRING);
		query.addScalar("grandChildCategoryId", Hibernate.INTEGER);
		query.addScalar("grandChildCategoryName", Hibernate.STRING);
		query.addScalar("childCategoryId", Hibernate.INTEGER);
		query.addScalar("childCategoryName", Hibernate.STRING);
		query.addScalar("parentCategoryId", Hibernate.INTEGER);
		query.addScalar("parentCategoryName", Hibernate.STRING);
		event = (Event) query.setResultTransformer(Transformers.aliasToBean(Event.class)).uniqueResult();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return event;
*/}
	
	/**
	 * Method to get the list of fav events for customer
	 * @param customerId
	 * @return
	 */
/*@SuppressWarnings("unchecked")
	public List<Event> getFavoriteEventForCustomer(Integer customerId, Integer pageNumber, Integer maxRows) {
		String SQL_FAVORITE_EVENTS = "select e.event_id as eventId,e.event_name as eventName," +
				"e.building as venueName,e.event_date as eventDate," +
				"e.city as city,e.state as state,e.country as country," +
				"e.postal_code as pinCode,e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName," +
				"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName,'true' as isFavoriteEvent"  +
				" from event_details e where e.event_id in(" +
				"select distinct fa.event_id from cust_favorites_event_handler fa " +
				"where fa.customer_id="+customerId+" and fa.status='ACTIVE' " +
				"union " +
				"select distinct e.event_id from event_details  e " +
				"inner join cust_favorites_artist_handler fa on fa.artist_id=e.artist_id " +
				"where fa.customer_id="+customerId+" and fa.status='ACTIVE')" +
				"order by e.event_date,e.event_time" +
				" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		String SQL_FAVORITE_EVENTS="select e.event_id as eventId,e.event_name as eventName," +
				"e.building as venueName,e.event_date as eventDate," +
				"e.event_time as eventTime," +
				"e.city as city,e.state as state,e.country as country," +
				"e.postal_code as pinCode,e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName," +
				"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName,'true' as isFavoriteEvent," +
				"zp.minPrice as ticketMinPrice,zp.maxPrice as ticketMaxPrice from event_details e " +
				"WITH(NOLOCK) CROSS APPLY ( select MIN(price) as minPrice," +
				"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
				"zt.product_type='REWARDTHEFAN' ) zp " +
				"where e.event_id in(" +
				"select distinct fa.event_id from cust_favorites_event_handler fa " +
				"where fa.customer_id=7 and fa.status='ACTIVE'" +
				"union " +
				"select distinct e.event_id from event_details  e " +
				"inner join cust_favorites_artist_handler fa on fa.artist_id=e.artist_id " +
				"where fa.customer_id=7 and fa.status='ACTIVE') " +
				"order by e.event_date,e.event_time" +
				" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		SQLQuery query = null;
		Session session = null;
		
		try {
			session = getSessionFactory().openSession();
			
			query = session.createSQLQuery(SQL_FAVORITE_EVENTS);
			query.addScalar("eventId", Hibernate.INTEGER);
			query.addScalar("eventName", Hibernate.STRING);
			query.addScalar("eventDate", Hibernate.DATE);
		 	query.addScalar("eventTime", Hibernate.TIME);
			query.addScalar("venueName", Hibernate.STRING);
			query.addScalar("city", Hibernate.STRING);
			query.addScalar("state", Hibernate.STRING);
			query.addScalar("country", Hibernate.STRING);
			query.addScalar("pinCode", Hibernate.STRING);
			query.addScalar("artistName", Hibernate.STRING);
			query.addScalar("grandChildCategoryName", Hibernate.STRING);
			query.addScalar("childCategoryName", Hibernate.STRING);
			query.addScalar("parentCategoryName", Hibernate.STRING);
			query.addScalar("isFavoriteEvent", Hibernate.BOOLEAN);
			query.addScalar("ticketMinPrice", Hibernate.INTEGER);
			query.addScalar("ticketMaxPrice", Hibernate.INTEGER);
			//query.addEntity(Event.class);
			//query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			
			List<Event> favoriteEventList = query.setResultTransformer(Transformers.aliasToBean(Event.class)).list();
			return favoriteEventList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/
	
	
	/**
	 * Method to get the list of fav events for customer
	 * @param customerId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Event> getFavoriteEventForCustomer(Integer customerId, Integer pageNumber, Integer maxRows,List<Integer> unFavoritedEventIds
			,Date startDate,Date endDate,String state) {
		
		String SQL_FAVORITE_EVENTS = "select A.*,count(1) over () totalRows from  ( " +
				"select e.event_id as eventId,e.event_name as eventName,e.building as venueName,e.str_event_date as eventDate, " +
				"e.str_event_time as eventTime,e.city as city,e.state as state,e.country as country,e.postal_code as pinCode, " +
				"e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName, " +
				"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName, " +
				"'true' as isFavoriteEvent,e.min_price  as minPrice,e.max_price  as maxPrice, " +
				"e.event_date,e.event_time,e.venue_category_name as venueCategoryName,e.venue_id as venueId " +
				"from events_rtf e WITH(READCOMMITTED) " +
				//" inner join event_artist ea WITH(READCOMMITTED)  on e.event_id=ea.event_id and e.artist_id = ea.artist_id " +
				"cross apply (select customer_id, artist_id, event_id from " +
				"(select ah.customer_id as customer_id, ah.artist_id as artist_id, null as event_id "+
				"from cust_favorites_artist_handler ah WITH(READCOMMITTED) where ah.status='ACTIVE' " +
				"union all " +
				"select eh.customer_id as customer_id, null as artist_id, eh.event_id as event_id " +
				"from cust_favorites_event_handler eh WITH(READCOMMITTED) where eh.status='ACTIVE' " +
				") eah  where eah.customer_id="+customerId+" and (eah.artist_id = e.artist_id or eah.event_id = e.event_id) " +
				") eahn where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event  AND e.reward_thefan_enabled=1 ";
				
				if(startDate !=null && endDate !=null){
					SQL_FAVORITE_EVENTS += " AND e.event_date between '"+dateTimeFormat.format(startDate)+"' AND  '"+dateTimeFormat.format(endDate)+"') ";
				}else if (startDate !=null){
					SQL_FAVORITE_EVENTS += " e.event_date >= '"+dateTimeFormat.format(startDate)+"' ";
				}else if(endDate !=null){
					SQL_FAVORITE_EVENTS += "  e.event_date <= '"+dateTimeFormat.format(endDate)+"'  ";
				}
				
				if(null != state && !state.isEmpty()){
					SQL_FAVORITE_EVENTS += " AND e.state like '"+state+"' ";
				}
		
				
				SQL_FAVORITE_EVENTS += "group by e.event_id ,e.event_name ,e.building ,e.str_event_date , e.str_event_time , " +
						"e.city ,e.state ,e.country ,e.postal_code ,e.artist_name ,e.grand_child_category_name , " +
						"e.child_category_name ,e.parent_category_name, e.min_price , e.max_price , " +
						"e.event_date,e.event_time,e.venue_category_name ,e.venue_id " +
						") A order by A.event_date,A.event_time OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL_FAVORITE_EVENTS);
			
			
			List<Object[]> favEvents = (List<Object[]>)query.list();
			
			List<Event> favEventList = new ArrayList<Event>();
			Event event=null;
			for(Object[] object : favEvents){
				
				
				if(null != unFavoritedEventIds && unFavoritedEventIds.contains((Integer)object[0])){
					continue;
				}
				event = new Event();
				event.setEventId((Integer)object[0]);
				event.setEventName((String)object[1]);
				/*event.setEventDate((java.sql.Date)object[3]);*/
				String timeStr = DateUtil.formatTime((String) object[4]);
				event.setEventDateTime((String)object[3]+" "+timeStr);
				event.setEventDateStr((String)object[3]);
				event.setEventTimeStr(timeStr);
				event.setVenueName((String)object[2]);
				event.setCity((String)object[5]);
				event.setState((String)object[6]);
				event.setCountry((String)object[7]);
				event.setPinCode((String)object[8]);
				event.setArtistName((String)object[9]);
				event.setGrandChildCategoryName((String)object[10]);
				event.setChildCategoryName((String)object[11]);
				event.setParentCategoryName((String)object[12]);
				Boolean boolean1 = Boolean.valueOf((String)object[13]);
				event.setIsFavoriteEvent(true);
				
				BigDecimal minPrice = (BigDecimal)object[14];
				BigDecimal maxPrice = (BigDecimal)object[15];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				
				event.setVenueCategoryName((String)object[18]);
				event.setVenueId((Integer)object[19]);
				event.setTotalEvents((Integer)object[20]);
				favEventList.add(event);
				
			}
			if(null != favEventList && !favEventList.isEmpty()) {
				System.out.println("size of fav event list :: " +favEventList.size());
			}else{
				favEventList = new ArrayList<Event>();
			}
			
			return favEventList;
		} catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 */
	public List<Event> getEventsByArtistCity(Integer artistId,String city)throws Exception {
		
		String hql="FROM EventDetails WHERE eventStatus=? AND artistId=? AND city=? ORDER BY eventName, eventDate, eventTime Asc";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(EventStatus.ACTIVE);
		parameters.add(artistId);
		parameters.add(city);
		return find(hql, parameters.toArray());
		
	}

	
	public List<Event> getActiveEventsForArtist(Integer artistId, Integer customerId,Integer pageNumber, Integer maxRows) throws Exception {
			
		String SQL_EVENT_FOR_ARTIST = null;
		if(customerId !=null){
			SQL_EVENT_FOR_ARTIST = "select A.*,count(1) over () totalRows from (select distinct e.event_id as eventId,e.event_name as eventName," +
			"e.building as venueName,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
			"WHEN e.event_time is null THEN 'TBD' END as eventTime, " +
			"e.city as city,e.state as state,e.country as country," +
			"e.postal_code as pinCode,e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName," +
			"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName,cf.isFavoriteEvent as favouriteEvent, " +
			"zp.minPrice ,zp.maxPrice,e.event_date,e.event_time, e.venue_category_name as venueCategoryName,e.venue_id as venueId " +
			"from event_details e WITH(NOLOCK) inner join category_ticket_group t WITH(NOLOCK) on(t.event_id=e.event_id)" +
			"CROSS APPLY ( select MIN(price) as minPrice," +
			"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK)" + 
			"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " + 
			"zt.product_type='REWARDTHEFAN') zp " +
			"left outer join (" +
			"select 'true' as isFavoriteEvent,event_id from cust_favorites_event_handler fa WITH(NOLOCK) " +
			"where fa.customer_id="+customerId+" and fa.status='ACTIVE')cf " +
			"on(cf.event_id=e.event_id) " +
			"where e.artist_id="+artistId+" and t.status='ACTIVE' AND datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event AND e.status=1 AND zp.minPrice is not null " +
			") A order by A.event_date,A.event_time "+
			"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		}else{
			SQL_EVENT_FOR_ARTIST = "select A.*,count(1) over () totalRows from (select distinct e.event_id as eventId,e.event_name as eventName," +
			"e.building as venueName,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
			"WHEN e.event_time is null THEN 'TBD' END as eventTime, " +
			"e.city as city,e.state as state,e.country as country," +
			"e.postal_code as pinCode,e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName," +
			"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName, 'false' as favouriteEvent, " +
			"zp.minPrice ,zp.maxPrice,e.event_date,e.event_time, e.venue_category_name as venueCategoryName,e.venue_id as venueId  " +
			"from events_rtf e WITH(NOLOCK) inner join category_ticket_group t WITH(NOLOCK) on(t.event_id=e.event_id) " +
			"CROSS APPLY ( select MIN(price) as minPrice, " +
			"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " + 
			"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " + 
			"zt.product_type='REWARDTHEFAN') zp " +
			"where e.artist_id="+artistId+" and t.status='ACTIVE' AND e.status=1 AND datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event AND zp.minPrice is not null " +
			") A order by A.event_date,A.event_time "+
			"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		}
		
		try {
			
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL_EVENT_FOR_ARTIST);
			
			List<Object[]> eventListByArtistId = (List<Object[]>)query.list();
			
			List<Event> eventListByArtist = new ArrayList<Event>();
			Event event=null;
			for(Object[] object : eventListByArtistId){
				event = new Event();
				event.setEventId((Integer)object[0]);
				event.setEventName((String)object[1]);
				/*event.setEventDate((java.sql.Date)object[3]);*/
				String timeStr = DateUtil.formatTime((String) object[4]);
				event.setEventDateTime((String)object[3]+" "+timeStr);
				event.setEventDateStr((String)object[3]);
				event.setEventTimeStr(timeStr);
				event.setVenueName((String)object[2]);
				event.setCity((String)object[5]);
				event.setState((String)object[6]);
				event.setCountry((String)object[7]);
				event.setPinCode((String)object[8]);
				event.setArtistName((String)object[9]);
				event.setGrandChildCategoryName((String)object[10]);
				event.setChildCategoryName((String)object[11]);
				event.setParentCategoryName((String)object[12]);
			    Boolean boolean1 = Boolean.valueOf((String)object[13]);
				event.setIsFavoriteEvent(boolean1);
				
				BigDecimal minPrice = (BigDecimal)object[14];
				BigDecimal maxPrice = (BigDecimal)object[15];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				event.setVenueCategoryName((String)object[18]);
				event.setVenueId((Integer)object[19]);
				event.setTotalEvents((Integer)object[20]);
				
				eventListByArtist.add(event);
			}
			if(null != eventListByArtist && !eventListByArtist.isEmpty()) {
				//System.out.println("size of fav event list :: " +eventListByArtist.size());
				return eventListByArtist;
			}else{
				eventListByArtist = new ArrayList<Event>();
			}
			return eventListByArtist;
		}
		catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}
	public List<Event> getNewlyCreatedEventByArtistId(Integer artistId,Date lastRunTime ,Date currentDate ) {
			
		return find("FROM Event WHERE artistId = ? and  createDate between ? and ?" , new Object[]{artistId,Status.ACTIVE,lastRunTime,currentDate} ); 
	}

	/**
	 * 
	 */
	public List<Event> getCustomerSuperFanEventDetails(Integer artistId,
			Integer customerId, Integer pageNumber, Integer maxRows)
			throws Exception {
		String SQL_SUPER_FAN_EVENTS="select A.*,count(1) over () totalRows from ( select e.event_id as eventId,e.event_name as eventName," +
			"e.building as venueName,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
			"WHEN e.event_time is null THEN 'TBD' END as eventTime, " +
			"e.city as city,e.state as state,e.country as country," +
			"e.postal_code as pinCode,e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName," +
			"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName,cf.isFavoriteEvent as favouriteEvent, " +
			"zp.minPrice ,zp.maxPrice,e.event_date,e.event_time " +
			"from artist a WITH(NOLOCK) inner join  event_artist ea WITH(NOLOCK)  on ea.artist_id=a.id " +
			"inner join event_details e WITH(NOLOCK) on(e.event_id=ea.event_id) " +
			"CROSS APPLY ( select MIN(price) as minPrice," +
			"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK)" + 
			"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " + 
			"zt.product_type='REWARDTHEFAN') zp " +
			"left outer join (" +
			"select 'true' as isFavoriteEvent,event_id from cust_favorites_event_handler fa WITH(NOLOCK) " +
			"where fa.customer_id="+customerId+" and fa.status='ACTIVE')cf " +
			"on(cf.event_id=e.event_id) " +
			"where a.id="+artistId+" AND zp.minPrice is not null and e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event ) A " +
			"order by A.event_date,A.event_time "+
			"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL_SUPER_FAN_EVENTS);
			
			List<Object[]> eventListByArtistId = (List<Object[]>)query.list();
			
			List<Event> eventListByArtist = new ArrayList<Event>();
			Event event=null;
			for(Object[] object : eventListByArtistId){
				event = new Event();
				event.setEventId((Integer)object[0]);
				event.setEventName((String)object[1]);
				/*event.setEventDate((java.sql.Date)object[3]);*/
				String timeStr = DateUtil.formatTime((String) object[4]);
				event.setEventDateTime((String)object[3]+" "+timeStr);
				event.setEventDateStr((String)object[3]);
				event.setEventTimeStr(timeStr);
				event.setVenueName((String)object[2]);
				event.setCity((String)object[5]);
				event.setState((String)object[6]);
				event.setCountry((String)object[7]);
				event.setPinCode((String)object[8]);
				event.setArtistName((String)object[9]);
				event.setGrandChildCategoryName((String)object[10]);
				event.setChildCategoryName((String)object[11]);
				event.setParentCategoryName((String)object[12]);
			    Boolean boolean1 = Boolean.valueOf((String)object[13]);
				event.setIsFavoriteEvent(boolean1);
				
				BigDecimal minPrice = (BigDecimal)object[14];
				BigDecimal maxPrice = (BigDecimal)object[15];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				event.setTotalEvents((Integer)object[18]);
				eventListByArtist.add(event);
			}
			if(null != eventListByArtist && !eventListByArtist.isEmpty()) {
				//System.out.println("size of fav event list :: " +eventListByArtist.size());
				return eventListByArtist;
			}else{
				eventListByArtist = new ArrayList<Event>();
			}
			return eventListByArtist;
		}
		catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 */
	public List<Event> getLoyalFanEventDetailsByState(String loyalFanName, Integer customerId, Integer pageNumber, 
			Integer maxRows,LoyalFanType fanType) throws Exception {
		
		String parentCategory = "";		
		if(fanType.equals(LoyalFanType.CONCERTS)){
			parentCategory = "'CONCERTS'";
		}else{
			parentCategory = "'THEATER','OTHER'";
		}
		
		String SQL_SUPER_FAN_EVENTS="select A.*,count(1) over () totalRows from ( select e.event_id as eventId,e.event_name as eventName," +
			"e.building as venueName,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
			"WHEN e.event_time is null THEN 'TBD' END as eventTime, " +
			"e.city as city,e.state as state,e.country as country," +
			"e.postal_code as pinCode,e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName," +
			"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName,cf.isFavoriteEvent as favouriteEvent, " +
			"zp.minPrice ,zp.maxPrice,e.event_date,e.event_time " +
			"from artist a WITH(NOLOCK) inner join  event_artist ea WITH(NOLOCK)  on ea.artist_id=a.id " +
			"inner join event_details e WITH(NOLOCK) on(e.event_id=ea.event_id) " +
			"CROSS APPLY ( select MIN(price) as minPrice," +
			"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK)" + 
			"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " + 
			"zt.product_type='REWARDTHEFAN') zp " +
			"left outer join (select 'true' as isFavoriteEvent,event_id from cust_favorites_event_handler fa WITH(NOLOCK) " +
			"where fa.customer_id="+customerId+" and fa.status='ACTIVE')cf " +
			"on(cf.event_id=e.event_id) where e.state in " +
			"(select distinct short_desc from state where country_id in (217,38) and (name like '%"+loyalFanName+"%' or short_desc like '%"+loyalFanName+"%')) " +
			"and e.parent_category_name in ("+parentCategory+") AND zp.minPrice is not null and e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event ) A " +
			"order by A.event_date,A.event_time OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL_SUPER_FAN_EVENTS);
			
			List<Object[]> eventListByArtistId = (List<Object[]>)query.list();
			
			List<Event> eventListByArtist = new ArrayList<Event>();
			Event event=null;
			for(Object[] object : eventListByArtistId){
				event = new Event();
				event.setEventId((Integer)object[0]);
				event.setEventName((String)object[1]);
				/*event.setEventDate((java.sql.Date)object[3]);*/
				String timeStr = DateUtil.formatTime((String) object[4]);
				event.setEventDateTime((String)object[3]+" "+timeStr);
				event.setEventDateStr((String)object[3]);
				event.setEventTimeStr(timeStr);
				event.setVenueName((String)object[2]);
				event.setCity((String)object[5]);
				event.setState((String)object[6]);
				event.setCountry((String)object[7]);
				event.setPinCode((String)object[8]);
				event.setArtistName((String)object[9]);
				event.setGrandChildCategoryName((String)object[10]);
				event.setChildCategoryName((String)object[11]);
				event.setParentCategoryName((String)object[12]);
			    Boolean boolean1 = Boolean.valueOf((String)object[13]);
				event.setIsFavoriteEvent(boolean1);
				
				BigDecimal minPrice = (BigDecimal)object[14];
				BigDecimal maxPrice = (BigDecimal)object[15];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				event.setTotalEvents((Integer)object[18]);
				eventListByArtist.add(event);
			}
			if(null != eventListByArtist && !eventListByArtist.isEmpty()) {
				//System.out.println("size of fav event list :: " +eventListByArtist.size());
				return eventListByArtist;
			}else{
				eventListByArtist = new ArrayList<Event>();
			}
			return eventListByArtist;
		}
		catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 
	 */
	public Map<Integer, List<Event>> getNewlyCreatedEvents(String fromDate,String toDate)
			throws Exception {
		String SQL_SUPER_FAN_EVENTS="select A.* from  (select e.event_id as eventId, 0 as artistId, " +
				"'' as artistName , e.child_category_id as childCategoryId, " +
				"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150)," +
				"REPLACE(e.building,',','')) as venueName,e.city as venueCity, e.state as venueState, " +
				"e.country as venueCountry,e.postal_code as venueZipcode,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName," +
				"e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName," +
				"e.parent_category_id as parentId, e.parent_category_name as parentName,e.reward_thefan_enabled as rewardTheFan," +
				"e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName,zp.minPrice , " +
				"zp.maxPrice,e.event_date,e.event_time from event_details e WITH(NOLOCK) CROSS APPLY ( select MIN(price) minPrice," +
				"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
				"zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and " +
				"e.create_date between '"+fromDate+"' and '"+toDate+"' ) A where " +
				"A.minPrice is not null order by A.event_date,A.event_time";
		
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL_SUPER_FAN_EVENTS);
			
			List<Object[]> eventListByArtistId = (List<Object[]>)query.list();
			
			Map<Integer, List<Event>> artistEventMap = new HashMap<Integer, List<Event>>();
			
			Event event=null;
			for(Object[] object : eventListByArtistId){
				
				event= new Event();
				event.setEventId((Integer)object[0]);
				//event.setArtistId((Integer)object[1]);
				event.setVenueId((Integer)object[5]);
				event.setVenueCategoryName((String)object[20]);
				event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}
				//event.setArtistName((String)object[2]);
				event.setChildCategoryId((Integer)object[3]);
				event.setChildCategoryName((String)object[4]);
				event.setVenueId((Integer)object[5]);
				event.setVenueName((String)object[6]);		
				event.setCity((String)object[7]);
				event.setState((String)object[8]);
				event.setCountry((String)object[9]);
				event.setPinCode((String)object[10]);
				event.setEventDateTime((String)object[11]+" "+(String)object[12]);
				event.setEventDateStr((String)object[11]);
				event.setEventTimeStr((String)object[12]);
				event.setEventName((String)object[13]);
				event.setGrandChildCategoryId((Integer)object[14]);
				event.setGrandChildCategoryName((String)object[15]);
				event.setParentCategoryId((Integer)object[16]);
				event.setParentCategoryName((String)object[17]);
				//event.setRewardTheFanEnabled((Boolean)object[18]);
				event.setVenueCategoryId((Integer)object[19]);
				event.setVenueCategoryName((String)object[20]);
				
				BigDecimal minPrice = (BigDecimal)object[21];
				BigDecimal maxPrice = (BigDecimal)object[22];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				
				List<Event> eventListByArtist = artistEventMap.get(event.getArtistId());
				
				if(null != eventListByArtist && !eventListByArtist.isEmpty()){
					artistEventMap.get(event.getArtistId()).add(event);
				}else{
					eventListByArtist = new ArrayList<Event>();
					eventListByArtist.add(event);
					artistEventMap.put(event.getArtistId(), eventListByArtist);
				}
			}
			return artistEventMap;
		}
		catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	public HashMap<String, Object> getAllEventsBySearchKey(String searchKey,Integer eventPageNumber ,Integer maxRows,
			String startDate,String endDate,String searchType,Integer artistPageNo,Integer venuePageNo,
			String searchMode,String stateShortName,Integer excludeEventDays,String artistRefValue,ArtistReferenceType artistRefType){
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			String queryStr = "exec SP_get_event_info_searchInp @searchinput = \""+searchKey+"\"," +
					"@pageNo='"+eventPageNumber+"',@maxRow='"+maxRows+"'";
			
			if((null != startDate && !startDate.isEmpty()) && (endDate != null && !endDate.isEmpty())){
				queryStr = queryStr +",@eventStartDate =\""+startDate+"\",@eventEndDate =\""+endDate+"\"";
			}else{
				queryStr = queryStr +",@eventStartDate = NULL, @eventEndDate = NULL";
			}
			
			if(searchMode != null) {
				queryStr = queryStr +",@searchMode ='"+searchMode+"'";	
			} else {
				queryStr = queryStr +",@searchMode =NULL";
			}
			queryStr = queryStr +",@apageNo ="+artistPageNo;
			queryStr = queryStr +",@vpageNo ="+venuePageNo;
			
			if(stateShortName !=null && !stateShortName.isEmpty()) {
				stateShortName = stateShortName.replaceAll("\\W", "%");
				queryStr = queryStr +",@stateParam=\""+stateShortName+"\"";
			}else {
				queryStr = queryStr +",@stateParam =NULL";
			}
			
			queryStr = queryStr +",@excludeEventDays ="+excludeEventDays;
			if(artistRefType != null && artistRefValue != null) {
				queryStr = queryStr +",@aRefType=\""+artistRefType.toString()+"\"";
				queryStr = queryStr +",@aRefValue=\""+artistRefValue+"\"";
			} else {
				queryStr = queryStr +",@aRefType =NULL,@aRefValue=NULL";
			}
			
			Query query = eventSession.createSQLQuery(queryStr);
			List<Object[]> result = (List<Object[]>)query.list();
			
			HashMap<String, Object> allObj = new HashMap<String, Object>();	
			if(null != searchType){
				if(searchType.equals("NORMALSEARCH")){
					allObj = Util.normalSearchResultMapper(result);
				}else{
					allObj = Util.autoSearchResultMapper(result);
				}
			}
			
			return allObj;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}
	
	
	
	
	public HashMap<String, Object> getAllEventsByArtistIdOrVenueId(Integer eventPageNumber ,Integer maxRows,
			String startDate,String endDate,Integer artistId,Integer venueId,String searchType,String searchMode,String stateShortName,
			Integer excludeEventDays,String artistRefValue,ArtistReferenceType artistRefType){
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			//System.out.println("GENERALISED_SEARCH DAO -Begins :"+new Date());
			String queryStr = "exec SP_get_event_info_avId @pageNo='"+eventPageNumber+"',@maxRow='"+maxRows+"'";
			
			if((null != startDate && !startDate.isEmpty()) && (endDate != null && !endDate.isEmpty())){
				queryStr = queryStr +",@eventStartDate =\""+startDate+"\",@eventEndDate =\""+endDate+"\"";
			}else{
				queryStr = queryStr +",@eventStartDate = NULL, @eventEndDate = NULL";
			}
			
			if(artistId !=null) {
				queryStr = queryStr +",@artistId ="+artistId;
			} else {
				queryStr = queryStr +",@artistId =NULL";
			}
			
			if(venueId !=null) {
				queryStr = queryStr +",@venueId="+venueId;
			}else {
				queryStr = queryStr +",@venueId =NULL";
			}
			
			if(stateShortName !=null && !stateShortName.isEmpty()) {
				stateShortName = stateShortName.replaceAll("\\W", "%");
				queryStr = queryStr +",@stateParam=\""+stateShortName+"\"";
			}else {
				queryStr = queryStr +",@stateParam =NULL";
			}
			queryStr = queryStr +",@excludeEventDays ="+excludeEventDays;
			if(artistId != null && artistRefType != null && artistRefValue != null) {
				queryStr = queryStr +",@aRefType=\""+artistRefType.toString()+"\"";
				queryStr = queryStr +",@aRefValue=\""+artistRefValue+"\"";
			} else {
				queryStr = queryStr +",@aRefType =NULL,@aRefValue=NULL";
			}
			
			Query query = eventSession.createSQLQuery(queryStr);
			List<Object[]> result = (List<Object[]>)query.list();
			
			HashMap<String, Object> allObj = new HashMap<String, Object>();	
			if(null != searchType){
				if(searchType.equals("NORMALSEARCH")){
					allObj = Util.normalSearchResultMapper(result);
				}else{
					allObj = Util.autoSearchResultMapper(result);
				}
			}
			
			return allObj;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}
	
	
	public Event getFantasyEventByEventId(Integer eventId) throws Exception {
		
		String SQL=" select e.id as eventId,REPLACE(e.name,',','') as eventName, " +
				"CASE WHEN e.event_date is not null THEN CONVERT(VARCHAR(19),e.event_date,101) WHEN " +
				"e.event_date is null THEN 'TBD' END as eventDate,CASE WHEN e.event_time is not null " +
				"THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null " +
				"THEN 'TBD' END as eventTime,v.id as venueId,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding,v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,v.postal_code as postalCode  from event e WITH(NOLOCK) " +
				"inner join venue v WITH(NOLOCK) on v.id=e.venue_id where e.id="+eventId;
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL);
			
			List<Object[]> eventListByArtistId = (List<Object[]>)query.list();
			Event event=null;
			for(Object[] object : eventListByArtistId){
				event = new Event();
				event.setEventId((Integer)object[0]);
				event.setEventName((String)object[1]);
				
				String eventDate = (String)object[2];
				
				if(null != eventDate && !eventDate.equals("TBD")){
					String timeStr = DateUtil.formatTime((String)object[3]);
					event.setEventDateTime((String)object[2]+" "+timeStr);
					event.setEventDateStr((String)object[2]);
					event.setEventTimeStr(timeStr);
					event.setTempEventDate(df.parse((String)object[2]));
					event.setShowEventDateAsTBD(false);
				}else{
					event.setShowEventDateAsTBD(true);
					event.setEventDateTBDValue("TBD");
				}
				
				event.setVenueId((Integer)object[4]);
				event.setVenueCategoryName((String)object[5]);
				event.setVenueName((String)object[6]);
				event.setCountry((String)object[7]);
				event.setState((String)object[8]);
				event.setCity((String)object[9]);
				event.setPinCode((String)object[10]);
			}
			return event;
		}
		catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Event> getAllFantasyEvents() throws Exception {

		String SQL=" select e.id as eventId,REPLACE(e.name,',','') as eventName, " +
				"CONVERT(VARCHAR(19),e.event_date,101) as eventDate,CASE WHEN e.event_time is not null " +
				"THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null " +
				"THEN 'TBD' END as eventTime,v.id as venueId,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding,v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,v.postal_code as postalCode  from event e WITH(NOLOCK) " +
				"inner join venue v WITH(NOLOCK) on v.id=e.venue_id where e.id in (select distinct event_id from crownjewel_team_zones WITH(NOLOCK))";
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL);
			
			List<Object[]> eventListByArtistId = (List<Object[]>)query.list();
			List<Event> events = new ArrayList<Event>();
			Event event=null;
			for(Object[] object : eventListByArtistId){
				event = new Event();
				event.setEventId((Integer)object[0]);
				event.setEventName((String)object[1]);

				String eventDate = (String)object[2];
				
				if(null != eventDate && !eventDate.equals("TBD")){
					String timeStr = DateUtil.formatTime((String)object[3]);
					event.setEventDateTime((String)object[2]+" "+timeStr);
					event.setEventDateStr((String)object[2]);
					event.setEventTimeStr(timeStr);
					event.setTempEventDate(df.parse((String)object[2]));
					event.setShowEventDateAsTBD(false);
				}else{
					event.setShowEventDateAsTBD(true);
					event.setEventDateTBDValue("TBD");
				}
				
				event.setVenueId((Integer)object[4]);
				event.setVenueCategoryName((String)object[5]);
				event.setVenueName((String)object[6]);
				event.setCountry((String)object[7]);
				event.setState((String)object[8]);
				event.setCity((String)object[9]);
				event.setPinCode((String)object[10]);
				events.add(event);
			}
			return events;
		}
		catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
		}
	
	public List<Event> getEventsByArtistId(Integer artistId, Integer pageNumber, Integer maxRows)
			throws Exception {
		String SQL_SUPER_FAN_EVENTS="select A.*,count(1) over () totalRows from ( select e.event_id as eventId,e.event_name as eventName," +
			"e.building as venueName,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
			"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
			"WHEN e.event_time is null THEN 'TBD' END as eventTime, " +
			"e.city as city,e.state as state,e.country as country," +
			"e.postal_code as pinCode,e.artist_name as artistName,e.grand_child_category_name as grandChildCategoryName," +
			"e.child_category_name as childCategoryName,e.parent_category_name as parentCategoryName,'false' as favouriteEvent, " +
			"zp.minPrice ,zp.maxPrice,e.event_date,e.event_time " +
			"from artist a WITH(NOLOCK) inner join  event_artist ea WITH(NOLOCK)  on ea.artist_id=a.id " +
			"inner join event_details e WITH(NOLOCK) on(e.event_id=ea.event_id) " +
			"CROSS APPLY ( select MIN(price) as minPrice," +
			"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK)" + 
			"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " + 
			"zt.product_type='REWARDTHEFAN') zp " +
			"where a.id="+artistId+" AND zp.minPrice is not null and e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event ) A " +
			"order by A.event_date,A.event_time "+
			"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		try {
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(SQL_SUPER_FAN_EVENTS);
			
			List<Object[]> eventListByArtistId = (List<Object[]>)query.list();
			
			List<Event> eventListByArtist = new ArrayList<Event>();
			Event event=null;
			for(Object[] object : eventListByArtistId){
				event = new Event();
				event.setEventId((Integer)object[0]);
				event.setEventName((String)object[1]);
				/*event.setEventDate((java.sql.Date)object[3]);*/
				String timeStr = DateUtil.formatTime((String) object[4]);
				event.setEventDateTime((String)object[3]+" "+timeStr);
				event.setEventDateStr((String)object[3]);
				event.setEventTimeStr(timeStr);
				event.setVenueName((String)object[2]);
				event.setCity((String)object[5]);
				event.setState((String)object[6]);
				event.setCountry((String)object[7]);
				event.setPinCode((String)object[8]);
				event.setArtistName((String)object[9]);
				event.setGrandChildCategoryName((String)object[10]);
				event.setChildCategoryName((String)object[11]);
				event.setParentCategoryName((String)object[12]);
			    Boolean boolean1 = Boolean.valueOf((String)object[13]);
				event.setIsFavoriteEvent(boolean1);
				
				BigDecimal minPrice = (BigDecimal)object[14];
				BigDecimal maxPrice = (BigDecimal)object[15];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				event.setTotalEvents((Integer)object[18]);
				eventListByArtist.add(event);
			}
			if(null != eventListByArtist && !eventListByArtist.isEmpty()) {
				//System.out.println("size of fav event list :: " +eventListByArtist.size());
				return eventListByArtist;
			}else{
				eventListByArtist = new ArrayList<Event>();
			}
			return eventListByArtist;
		}
		catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}
	
	
public List<Event> getAllEventsByParentCategoryName(String parentCategoryName,Integer pageNumber , Integer maxRows,Date startDate,Date endDate){
		
		try {
			String whereClause="select A.*,count(1) over () totalRows from  (select event_id as eventId, 0 as artistId, " +
					"'' as artistName , e.child_category_id as childCategoryId, " +
					"e.child_category_name as childCategoryname, e.venue_id as venueId,CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName," +
					" e.city as venueCity, e.state as venueState, e.country as venueCountry,e.postal_code as venueZipcode" +
					",CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
					" WHEN e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName, " +
					" e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
					"e.parent_category_id as parentId, e.parent_category_name as parentName, " +
					" e.reward_thefan_enabled as rewardTheFan," +
					" e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName," +
					" iif(zp.minPrice IS NULL, 0, zp.minPrice)  as minPrice, iif(zp.maxPrice IS NULL, 0, zp.maxPrice)  as maxPrice," +
					"e.event_date,e.event_time " +//zp.minPrice , zp.maxPrice,
					"from event_details e WITH(NOLOCK) CROSS APPLY ( select MIN(price) minPrice," +
					"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
					"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and " +
					"zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and e.reward_thefan_enabled=1 ";
			
			if(parentCategoryName !=null && !parentCategoryName.equals("")){
				whereClause += " AND e.parent_category_name = '"+parentCategoryName+"'";
			}
			
			if(startDate !=null){
				whereClause += " AND e.event_date >= '"+dateTimeFormat.format(startDate)+" 00:00:00' ";
			}
			if(endDate !=null){
				whereClause += " AND e.event_date <= '"+dateTimeFormat.format(endDate)+" 23:59:59'  ";
			}
			
			whereClause +=" ) A order by A.event_date,A.event_time " +//where A.minPrice is not null
					" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
			
			eventSession = EventDAO.getEventSession();
			if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
				eventSession = getSession();
				EventDAO.setEventSession(eventSession);
			}
			Query query = eventSession.createSQLQuery(whereClause);
			List<Object[]> result = (List<Object[]>)query.list();
			List<Event> finalList = new ArrayList<Event>();
			Event event=null;
			
			for (Object[] object : result) {
				event= new Event();
				event.setVenueId((Integer)object[5]);
				event.setVenueCategoryName((String)object[20]);
				/*event = MapUtil.testSvgDetailsByEvent(event);
				if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
					continue;
				}*/
				event.setEventId((Integer)object[0]);
				//event.setArtistId((Integer)object[1]);
				//event.setArtistName((String)object[2]);
				event.setChildCategoryId((Integer)object[3]);
				event.setChildCategoryName((String)object[4]);
				event.setVenueId((Integer)object[5]);
				event.setVenueName((String)object[6]);		
				event.setCity((String)object[7]);
				event.setState((String)object[8]);
				event.setCountry((String)object[9]);
				event.setPinCode((String)object[10]);
				event.setEventDateTime((String)object[11]+" "+(String)object[12]);
				event.setEventDateStr((String)object[11]);
				event.setEventTimeStr((String)object[12]);
				event.setEventName((String)object[13]);
				event.setGrandChildCategoryId((Integer)object[14]);
				event.setGrandChildCategoryName((String)object[15]);
				event.setParentCategoryId((Integer)object[16]);
				event.setParentCategoryName((String)object[17]);
				//event.setRewardTheFanEnabled((Boolean)object[18]);
				event.setVenueCategoryId((Integer)object[19]);
				event.setVenueCategoryName((String)object[20]);
				
				BigDecimal minPrice = (BigDecimal)object[21];
				BigDecimal maxPrice = (BigDecimal)object[22];
				event.setTicketMinPrice(minPrice.doubleValue());
				event.setTicketMaxPrice(maxPrice.doubleValue());
				event.setTotalEvents((Integer)object[25]);
				finalList.add(event);
			}
			if(null != finalList && !finalList.isEmpty()) {
				
			}else{
				finalList = new ArrayList<Event>();
			}
			return finalList;
			}catch (Exception e) {
				if(eventSession != null && eventSession.isOpen()){
					eventSession.close();
				}
				e.printStackTrace();
			}
			return null;
		
	}

 

public List<Event> getAllEventsByContestId(Integer contestId ,Integer pageNumber , Integer maxRows,Date startDate,Date endDate, String state,
		String searchKey, String zones,Double singleTixPrice,Integer requiredQty){
	
	try {
		
		String filters = "";
		if(null != zones && !zones.isEmpty()){
			filters = " and ctg.section in ("+zones+") ";
		}
		
		if(singleTixPrice > 0.00 || singleTixPrice > 0 ) {
			filters = filters+ " and ctg.tmat_price <= "+singleTixPrice+" and ctg.quantity >= "+requiredQty;
		}else {
			filters = filters+ " and ctg.quantity >= "+requiredQty;
		}
		
		String whereClause="select A.*,count(1) over () totalRows from  (select distinct e.event_id as eventId, 0 as artistId, '' as artistName , " +
				"e.child_category_id as childCategoryId, e.child_category_name as childCategoryname, e.venue_id as venueId," +
				"CONVERT(VARCHAR(150),REPLACE(e.building,',','')) as venueName, e.city as venueCity, e.state as venueState, " +
				"e.country as venueCountry,e.postal_code as venueZipcode,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))  WHEN " +
				"e.event_time is null THEN 'TBD' END as eventTime, REPLACE(e.event_name,',','') as eventName,  " +
				"e.grand_child_category_id as grandChildId, e.grand_child_category_name as grandChildName, " +
				"e.parent_category_id as parentId, e.parent_category_name as parentName,  e.reward_thefan_enabled as rewardTheFan, " +
				"e.venue_category_id as venueCategoryId, e.venue_category_name as venueCategoryName, " +
				"iif(zp.minPrice IS NULL, 0, zp.minPrice)  as minPrice, iif(zp.maxPrice IS NULL, 0, zp.maxPrice)  as maxPrice," +
				"e.event_date,e.event_time from events_rtf e WITH(NOLOCK) " +
				"inner join "+rtfQuizMasterLinkedServer+".contest c with(nolock) on e.event_datetime > c.contest_start_datetime " +
				"and c.promo_ref_id is not null and c.promo_ref_type is not null       " +
				"and c.promo_ref_id = ( case when c.promo_ref_type='ALL' then c.promo_ref_id  when c.promo_ref_type='ARTIST' then e.artist_id  " +
				"when c.promo_ref_type='EVENT' then e.event_id  when c.promo_ref_type='PARENT' then e.parent_category_id " +
				"when c.promo_ref_type='CHILD' then e.child_category_id when c.promo_ref_type='GRANDCHILD' then e.grand_child_category_id " +
				"when c.promo_ref_type='GRAND' then e.grand_child_category_id " +
				"end ) inner join venue v with(nolock) on e.venue_id = v.id cross apply (select event_id, min(price) as minPrice, " +
				"max(price) as maxPrice from category_ticket_group ctg with(nolock) where e.event_id = ctg.event_id and ctg.status='ACTIVE' " +
				" "+filters+" group by event_id ) zp where " +
				"e.status=1 and datediff(HOUR,GETDATE()," +
				"e.event_datetime)>e.exclude_hours_before_event and e.reward_thefan_enabled=1 and e.event_id not in " +
				"(select ce.event_id from "+rtfQuizMasterLinkedServer+".contest_events ce with(nolock) where ce.contest_id = "+contestId+") and c.id="+contestId+"";

		
		if(null != state && !state.isEmpty()){
			whereClause += " AND e.state like '"+state+"' ";
		}
		
		if(null != searchKey && !searchKey.isEmpty()){
			whereClause += " AND (e.artist_name like '%"+searchKey+"%' OR e.city like '%"+searchKey+"%' OR e.state like '%"+searchKey+"%') ";
		}
		
		if(startDate !=null){
			whereClause += " AND e.event_date >= '"+dateTimeFormat.format(startDate)+" 00:00:00' ";
		}
		if(endDate !=null){
			whereClause += " AND e.event_date <= '"+dateTimeFormat.format(endDate)+" 23:59:59'  ";
		}
		
		whereClause +=" ) A order by A.event_date,A.event_time " +//where A.minPrice is not null
				" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		System.out.println(whereClause);
		eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}
		
		Query query = eventSession.createSQLQuery(whereClause);
		
		List<Object[]> result = (List<Object[]>)query.list();
		List<Event> finalList = new ArrayList<Event>();
		Event event=null;
		
		String venueName = "",message="";
		
		for (Object[] object : result) {
			event= new Event();
			event.setVenueId((Integer)object[5]);
			event.setVenueCategoryName((String)object[20]);
			/*event = MapUtil.testSvgDetailsByEvent(event);
			if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
				continue;
			}*/
			event.setEventId((Integer)object[0]);
			//event.setArtistId((Integer)object[1]);
			//event.setArtistName((String)object[2]);
			event.setChildCategoryId((Integer)object[3]);
			event.setChildCategoryName((String)object[4]);
			event.setVenueId((Integer)object[5]);
			event.setVenueName((String)object[6]);		
			event.setCity((String)object[7]);
			event.setState((String)object[8]);
			event.setCountry((String)object[9]);
			event.setPinCode((String)object[10]);
			event.setEventDateTime((String)object[11]+" "+(String)object[12]);
			event.setEventDateStr((String)object[11]);
			event.setEventTimeStr((String)object[12]);
			event.setEventName((String)object[13]);
			event.setGrandChildCategoryId((Integer)object[14]);
			event.setGrandChildCategoryName((String)object[15]);
			event.setParentCategoryId((Integer)object[16]);
			event.setParentCategoryName((String)object[17]);
			//event.setRewardTheFanEnabled((Boolean)object[18]);
			event.setVenueCategoryId((Integer)object[19]);
			event.setVenueCategoryName((String)object[20]);
			
			venueName = (String)object[6]+", "+(String)object[7]+", "+(String)object[8]+", "+(String)object[9];
			
			message = "Please confirm that you are choosing "+(String)object[13]+" on "+(String)object[11]+" "+(String)object[12]+" at "+venueName+" as your Grand Prize Ticket Selection.";
			
			event.setContestTicketsConfirmation(message);
			
			BigDecimal minPrice = (BigDecimal)object[21];
			BigDecimal maxPrice = (BigDecimal)object[22];
			event.setTicketMinPrice(minPrice.doubleValue());
			event.setTicketMaxPrice(maxPrice.doubleValue());
			event.setTotalEvents((Integer)object[25]);
			finalList.add(event);
		}
		if(null != finalList && !finalList.isEmpty()) {
			
		}else{
			finalList = new ArrayList<Event>();
		}
		return finalList;
		}catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
		}
		return null;
	
}

public List<Event> getAllEventsByContestIdOptimized(Integer contestId ,Integer pageNumber , Integer maxRows,Date startDate,Date endDate, String state,
		String searchKey, String zones,Double singleTixPrice,Integer requiredQty){
	
	try {
		
		String filters = "";
		if(null != zones && !zones.isEmpty()){
			filters = " and ctg.section in ("+zones+") ";
		}
		
		if(singleTixPrice > 0.00 || singleTixPrice > 0 ) {
			filters = filters+ " and ctg.tmat_price <= "+singleTixPrice+" and ctg.quantity >= "+requiredQty;
		}else {
			filters = filters+ " and ctg.quantity >= "+requiredQty;
		}
		
		String whereClause="select A.*,count(1) over () totalRows from  (select e.* from events_for_contest_winners e with(nolock) "
				+ "cross apply (select event_id, min(price) as minPrice, max(price) as maxPrice  "
				+ "from category_ticket_group ctg with(nolock) where e.eventId = ctg.event_id and ctg.status='ACTIVE' "
				+ "and ctg.tmat_price > 0 "+filters+" group by event_id ) zp "
				+ " where e.contestID="+contestId+" and e.event_date > GETDATE()  ";
			//			+ "and e.eventId not in "
			//	+ "(select ce.event_id from "+rtfQuizMasterLinkedServer+".contest_events ce with(nolock) where ce.contest_id = "+contestId+") ";

		
		if(null != state && !state.isEmpty()){
			whereClause += " and e.venueState like '"+state+"' ";
		}
		
		if(null != searchKey && !searchKey.isEmpty()){
			whereClause += " and (e.artistName like '%"+searchKey+"%' OR e.venueCity like '%"+searchKey+"%' OR e.venueState like '%"+searchKey+"%') ";
		}
		
		if(startDate !=null){
			whereClause += " and e.event_date >= '"+dateTimeFormat.format(startDate)+" 00:00:00' ";
		}
		if(endDate !=null){
			whereClause += " and e.event_date <= '"+dateTimeFormat.format(endDate)+" 23:59:59'  ";
		}
		
		whereClause +=" ) A order by A.event_date,A.event_time " +
				" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		System.out.println(whereClause);
		eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}
		
		Query query = eventSession.createSQLQuery(whereClause);
		
		List<Object[]> result = (List<Object[]>)query.list();
		List<Event> finalList = new ArrayList<Event>();
		Event event=null;
		
		for (Object[] object : result) {
			event= new Event();
			event.setVenueId((Integer)object[5]);
			event.setVenueCategoryName((String)object[20]);
			/*event = MapUtil.testSvgDetailsByEvent(event);
			if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
				continue;
			}*/
			event.setEventId((Integer)object[0]);
			//event.setArtistId((Integer)object[1]);
			//event.setArtistName((String)object[2]);
			event.setChildCategoryId((Integer)object[3]);
			event.setChildCategoryName((String)object[4]);
			event.setVenueId((Integer)object[5]);
			event.setVenueName((String)object[6]);		
			event.setCity((String)object[7]);
			event.setState((String)object[8]);
			event.setCountry((String)object[9]);
			event.setPinCode((String)object[10]);
			event.setEventDateTime((String)object[11]+" "+(String)object[12]);
			event.setEventDateStr((String)object[11]);
			event.setEventTimeStr((String)object[12]);
			event.setEventName((String)object[13]);
			event.setGrandChildCategoryId((Integer)object[14]);
			event.setGrandChildCategoryName((String)object[15]);
			event.setParentCategoryId((Integer)object[16]);
			event.setParentCategoryName((String)object[17]);
			//event.setRewardTheFanEnabled((Boolean)object[18]);
			event.setVenueCategoryId((Integer)object[19]);
			event.setVenueCategoryName((String)object[20]);
			
			BigDecimal minPrice = (BigDecimal)object[21];
			BigDecimal maxPrice = (BigDecimal)object[22];
			event.setTicketMinPrice(minPrice.doubleValue());
			event.setTicketMaxPrice(maxPrice.doubleValue());
			event.setTotalEvents((Integer)object[28]);
			finalList.add(event);
		}
		if(null != finalList && !finalList.isEmpty()) {
			
		}else{
			finalList = new ArrayList<Event>();
		}
		return finalList;
		}catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
		}
		return null;
	
}


public List<Event> getAllContestEventsByCriteria(Integer contestId ,Integer pageNumber , Integer maxRows,Date startDate,Date endDate, String state, String searchKey){
	
	try {
		
		String whereClause="select A.*,count(1) over () totalRows from  (select e.* from events_for_contest_winners_new e with(nolock) "
				+ " where e.contestID="+contestId+" and e.event_date > GETDATE()+3 and eventId not in "
						+ "(select ce.event_id from "+rtfQuizMasterLinkedServer+".contest_events ce with(nolock) where ce.contest_id = "+contestId+") ";
		
		if(null != state && !state.isEmpty()){
			whereClause += " and e.venueState like '"+state+"' ";
		}
		
		//Tamil : 02/18/2019 Added Child grandchild and venue in filter options
		if(null != searchKey && !searchKey.isEmpty()){
			whereClause += " and (e.eventName like '%"+searchKey+"%' OR e.venueCity like '%"+searchKey+"%' OR e.venueState like '%"+searchKey+"%' "
					+ " OR childcategoryname like '%"+searchKey+"%' OR grandChildName like '%"+searchKey+"%' OR venueName like '%"+searchKey+"%') ";
		}
		
		if(startDate !=null){
			whereClause += " and e.event_date >= '"+dateTimeFormat.format(startDate)+" 00:00:00' ";
		}
		if(endDate !=null){
			whereClause += " and e.event_date <= '"+dateTimeFormat.format(endDate)+" 23:59:59'  ";
		}
		
		whereClause +=" ) A order by A.event_date,A.event_time " +
				" OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		System.out.println(whereClause);
		eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}
		
		System.out.println(whereClause);
		
		System.out.println("Query Execution Started: "+new Date());
		
		Query query = eventSession.createSQLQuery(whereClause);
		
		List<Object[]> result = (List<Object[]>)query.list();
		
		System.out.println("Query Execution Ended: "+new Date());
		
		List<Event> finalList = new ArrayList<Event>();
		Event event=null;
		String venueName = "", message="";
		
		System.out.println("Filter Results Started: "+new Date());
		
		for (Object[] object : result) {
			event= new Event();
			event.setVenueId((Integer)object[5]);
			event.setVenueCategoryName((String)object[20]);
			/*event = MapUtil.testSvgDetailsByEvent(event);
			if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
				continue;
			}*/
			event.setEventId((Integer)object[0]);
			//event.setArtistId((Integer)object[1]);
			//event.setArtistName((String)object[2]);
			event.setChildCategoryId((Integer)object[3]);
			event.setChildCategoryName((String)object[4]);
			event.setVenueId((Integer)object[5]);
			event.setVenueName((String)object[6]);		
			event.setCity((String)object[7]);
			event.setState((String)object[8]);
			event.setCountry((String)object[9]);
			event.setPinCode((String)object[10]);
			event.setEventDateTime((String)object[11]+" "+(String)object[12]);
			event.setEventDateStr((String)object[11]);
			event.setEventTimeStr((String)object[12]);
			event.setEventName((String)object[13]);
			event.setGrandChildCategoryId((Integer)object[14]);
			event.setGrandChildCategoryName((String)object[15]);
			event.setParentCategoryId((Integer)object[16]);
			event.setParentCategoryName((String)object[17]);
			//event.setRewardTheFanEnabled((Boolean)object[18]);
			event.setVenueCategoryId((Integer)object[19]);
			event.setVenueCategoryName((String)object[20]);
			
			venueName = (String)object[6]+", "+(String)object[7]+", "+(String)object[8]+", "+(String)object[9];
			
			message = "Please confirm that you are choosing "+(String)object[13]+" on "+(String)object[11]+" "+(String)object[12]+" at "+venueName+" as your Grand Prize Ticket Selection.";
			
			event.setContestTicketsConfirmation(message);
			
			BigDecimal minPrice = (BigDecimal)object[21];
			BigDecimal maxPrice = (BigDecimal)object[22];
			event.setTicketMinPrice(minPrice.doubleValue());
			event.setTicketMaxPrice(maxPrice.doubleValue());
			event.setTotalEvents((Integer)object[28]);
			finalList.add(event);
		}
		System.out.println("Filter Results Ended: "+new Date());
		if(null != finalList && !finalList.isEmpty()) {
			
		}else{
			finalList = new ArrayList<Event>();
		}
		return finalList;
		}catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
		}
		return null;
}


public Integer getContestEventsCount(Integer contestId){
	
	try {
		
		String whereClause="select count(e.eventId) as eventTotal from events_for_contest_winners_new e with(nolock) where e.contestID="+contestId+"";
		 
		eventSession = EventDAO.getEventSession();
		if(eventSession==null || !eventSession.isOpen() || !eventSession.isConnected()){
			eventSession = getSession();
			EventDAO.setEventSession(eventSession);
		}
		
		Query query = eventSession.createSQLQuery(whereClause);
		List<Integer> result = (List<Integer>)query.list();
		if(null != result && !result.isEmpty()) {
			return result.get(0);
		} 
		
		return 0;
		}catch (Exception e) {
			if(eventSession != null && eventSession.isOpen()){
				eventSession.close();
			}
			e.printStackTrace();
		}
		return 0;
}


public Boolean callContestGrandWinnerProcedure(Integer contestId,Integer insertOrDelete){
	Session session = getSession();
	try {
		//insertOrDelete: 0 -Insert, 1-Delete
		String queryStr = "exec SP_contest_grand_winner_tickets @contestID  ="+contestId +",@deleteOrInsert="+insertOrDelete;
		Query query = session.createSQLQuery(queryStr);
		query.executeUpdate();
		return true;
	}catch (Exception e) {
		e.printStackTrace();
		return false;
	}
	finally{
		session.close();
	}
}

public static void main(String[] args) {
	
	EventDAO eventDAO = new EventDAO();
	//eventDAO.getAllEventsByHomeCardType(HomeCardType.HOME_CARD, new Date(),  new Date(), "NY", 1, 500, null);
	eventDAO.getAllContestEventsByCriteria(183, 1, 5000, null, null, null, null);
	
}

	
}