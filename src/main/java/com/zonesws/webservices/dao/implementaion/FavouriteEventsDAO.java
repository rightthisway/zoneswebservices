package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.FavouriteEvents;
import com.zonesws.webservices.enums.ConstantPool;

public class FavouriteEventsDAO extends HibernateDAO<Integer, FavouriteEvents> implements com.zonesws.webservices.dao.services.FavouriteEventsDAO{

	/*public boolean saveOrUpdate(FavouriteEvents event) {
		
		Session session = getSession();
		boolean status = false;
		try{
			session.saveOrUpdate(event);
			status = true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		
		return status;
	}*/

	/*public FavouriteEvents getFavouriteEventsById(Integer eventId,Integer customerId){
		
		Session session = getSession();
		
		try{
			Query query = session.createQuery("FROM FavouriteEvents WHERE eventId = :eventId and customerId = :customerId")
			.setParameter("eventId", eventId).setParameter("customerId", customerId);
			List<FavouriteEvents> favouriteEvents = query.list();
			return favouriteEvents;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
		}
		return new FavouriteEvents();
	}*/
	
	public FavouriteEvents getFavouriteEventsById(Integer eventId,Integer customerId) {
		return findSingle("FROM FavouriteEvents WHERE eventId = ? and customerId = ? ", 
				new Object[]{eventId,customerId});
	}
	
	public FavouriteEvents getActiveFavouriteEventsById(Integer eventId,Integer customerId) {
		return findSingle("FROM FavouriteEvents WHERE status=? and eventId = ? and customerId = ? ", 
				new Object[]{ConstantPool.EVENT_STATUS_ACTIVE,eventId,customerId});
	}
	
	public List<FavouriteEvents> getAllFavouriteEventsByCustomerId(Integer customerId) {
		return find("FROM FavouriteEvents WHERE status=? AND customerId = ? ", new Object[]{ConstantPool.EVENT_STATUS_ACTIVE,customerId});
	}
	
	
	public List<FavouriteEvents> getAllActiveFavouriteEventsByCustomerId(Integer customerId) {
		return find("from FavouriteEvents where customerId=? and status='ACTIVE' ", new Object[]{customerId});
				
	}
	
	public List<FavouriteEvents> getAllActiveFavouriteEvents() {
		return find("from FavouriteEvents where status='ACTIVE'");
				
	}
	
	
	public List<FavouriteEvents> getAllUnFavouritedEventsByCustomerId(Integer customerId) {
		return find("from FavouriteEvents where customerId=? and status='DELETED' ", new Object[]{customerId});
				
	}
}
