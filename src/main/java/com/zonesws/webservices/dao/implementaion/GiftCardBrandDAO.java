package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.zonesws.webservices.data.GiftCardBrand;

public class GiftCardBrandDAO extends HibernateDAO<Integer, GiftCardBrand> implements com.zonesws.webservices.dao.services.GiftCardBrandDAO{

	private static Session genericSession;
	
	public static Session getGenericSession() {
		return genericSession;
	}

	public static void setGenericSession(Session genericSession) {
		GiftCardBrandDAO.genericSession = genericSession;
	}
	
	
	public List<GiftCardBrand> getAllActiveGiftCardBrands(){
		return find("FROM GiftCardBrand WHERE status = 'ACTIVE'");
	}
	
	/*
	public List<GiftCardBrand> getAllGiftCardBrands() {
		List<GiftCardBrand> brands = null;
		try {
			String sql = "select id as id,name as name,description as description,qty_limit as quantity,status as status from gift_card_brand" ;
			
			
			genericSession = GiftCardBrandDAO.getGenericSession();
			if(genericSession==null || !genericSession.isOpen() || !genericSession.isConnected()){
				genericSession = getSession();
				GiftCardBrandDAO.setGenericSession(genericSession);
			}
			SQLQuery query = genericSession.createSQLQuery(sql);
			query.addScalar("id", Hibernate.INTEGER);
			query.addScalar("name", Hibernate.STRING);
			query.addScalar("description", Hibernate.STRING);
			query.addScalar("quantity", Hibernate.INTEGER);
			query.addScalar("status", Hibernate.STRING);
			
			brands = query.setResultTransformer(Transformers.aliasToBean(GiftCardBrand.class)).list();
			
		} catch (Exception e) {
			if(genericSession != null && genericSession.isOpen()){
				genericSession.close();
			}
			e.printStackTrace(); 
		}
		return brands;
	}*/
}
