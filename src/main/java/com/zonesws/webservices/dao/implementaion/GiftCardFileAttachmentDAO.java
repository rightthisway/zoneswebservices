package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.GiftCardFileAttachment;
import com.zonesws.webservices.enums.FileType;


public class GiftCardFileAttachmentDAO extends HibernateDAO<Integer, GiftCardFileAttachment> implements com.zonesws.webservices.dao.services.GiftCardFileAttachmentDAO{

	public List<GiftCardFileAttachment> getRealTicketsByInvoiceId(Integer orderId) {
		return find("FROM GiftCardFileAttachment where orderId=?",new Object[]{orderId});
	}

	public GiftCardFileAttachment getAttachmentByInvoiceAndFileType(Integer orderId, FileType fileType) {
		try {
			return findSingle("FROM GiftCardFileAttachment where orderId=? AND type = ?", new Object[]{orderId,fileType});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<GiftCardFileAttachment> getAllAttachemntByOrderId(Integer orderId) {
		return find("FROM GiftCardFileAttachment where orderId=?",new Object[]{orderId});
	}
}
