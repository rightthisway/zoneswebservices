package com.zonesws.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.enums.ArtistStatus;
import com.zonesws.webservices.enums.PopularGrandChildCategoryStatus;
import com.zonesws.webservices.enums.ProductType;
 
/**
 * class having db related methods for GrandChildCategory
 * @author hamin
 *
 */
public class GrandChildCategoryDAO extends HibernateDAO<Integer,GrandChildCategory> implements
com.zonesws.webservices.dao.services.GrandChildCategoryDAO {

	/**
	 *  method to get all GrandChildCategories 
	 * @return List of GrandChildCategories
	 */
	public List<GrandChildCategory> getAll() {
		return find("FROM GrandChildCategory");	 
	}
	/**
	 * method to get  GrandChildCategoriy by name 
	 * @param name , Grand Child Category name
	 * @return GrandChildCategory
	 */
	public  List<GrandChildCategory> getGrandChildsByChildCategoryId(Integer childCategoryId) {		 	
		return find("FROM GrandChildCategory gc where gc.childCategory.id = ? ORDER BY gc.priority",new Object []{childCategoryId});	 
	}
	/**
	 * method to get  GrandChildCategoriy by id 
	 * @param id , GrandChildCategory id
	 * @return GrandChildCategory
	 */
	public  GrandChildCategory  getGrandChildCategoryById(Integer id) {		 	
		return findSingle("FROM GrandChildCategory gc where gc.id = ?",new Object []{id});	 
	}
	/**
	 * method to get  GrandChildCategoriy by id 
	 * @param childCategoryId , childCategory id
	 * @return GrandChildCategory
	 */
	public  GrandChildCategory  getGrandChildCategoryByName(String grandChild) {		 	
		return findSingle("FROM GrandChildCategory gc where gc.name = ?",new Object []{grandChild});	 
	}
	
	public List<GrandChildCategory> getGrandChildCategoriesByName(String name) {
		String hql="FROM GrandChildCategory WHERE name like ? order by name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}
	
	public  List<GrandChildCategory> getAllGrandChildCategoryByPageNumber(Integer pageNumber,Integer maxRows) {
			
			
			String sql ="select distinct g.id,g.name FROM popular_grand_child_category pg inner join grand_child_category g ON pg.grand_child_category_id = g.id" +
						" WHERE pg.status = '"+PopularGrandChildCategoryStatus.ACTIVE.name()+"' order by g.name  OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";
			
			List<GrandChildCategory> grandChildCategorys =new ArrayList<GrandChildCategory>();
			Query query = null;
			Session session = null;
			try {
				session = getSessionFactory().openSession();
				query = session.createSQLQuery(sql);
				
				List<Object[]> result = (List<Object[]>)query.list();
				
				Artist artist = null;
				for (Object[] object : result) {
					GrandChildCategory grandChildCategory = new GrandChildCategory();
					grandChildCategory.setId((Integer)object[0]);
					grandChildCategory.setName((String)object[1]);
					grandChildCategorys.add(grandChildCategory);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally  {
				session.close();
			}
			return grandChildCategorys;
		}
	
	public String getGrandChildIdsBySearchKey(String searchKey){
		
		Session session = null;
		String grandChildIds = "";
		
		try{
			
			StringBuilder queryString = new StringBuilder();
			queryString.append("DECLARE @searchinput varchar(50), @svlikestart varchar(60);SET @searchinput  = '"+searchKey+"';SET  @svlikestart = @searchinput + ' %';");
			queryString.append("select gc.id from grand_child_category gc ");
			queryString.append("where gc.name  like @svlikestart  or gc.name = @searchinput");
			
			session = getSessionFactory().openSession();
			Query query = session.createSQLQuery(queryString.toString());
			
			List<Object> result = (List<Object>)query.list();
			boolean flag = true;
			for (Object object : result) {
				if(flag){
					grandChildIds = String.valueOf(object);
					flag = false;
				}else{
					grandChildIds = grandChildIds + "," + String.valueOf(object);
				}					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return grandChildIds;
	}

}
