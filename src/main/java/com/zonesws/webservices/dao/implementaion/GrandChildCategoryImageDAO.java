package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.GrandChildCategoryImage;


public class GrandChildCategoryImageDAO extends HibernateDAO<Integer, GrandChildCategoryImage> implements com.zonesws.webservices.dao.services.GrandChildCategoryImageDAO {
	
	public GrandChildCategoryImage getGrandChildCategoryImageByGrandChildId(Integer grandChildId) throws Exception { 
		return findSingle("From GrandChildCategoryImage where grandChildCategory.id=?", new Object[]{grandChildId});		
	}
}
