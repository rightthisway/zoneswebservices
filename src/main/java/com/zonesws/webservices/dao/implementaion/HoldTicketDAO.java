package com.zonesws.webservices.dao.implementaion;


import java.util.Collection;

import com.zonesws.webservices.data.HoldTicket;
import com.zonesws.webservices.enums.HoldTicketStatus;
/**
 * class havingdb related methods for HoldTicket
 * @author Ulaganathan
 *
 */
public class HoldTicketDAO extends HibernateDAO<Integer, HoldTicket> implements com.zonesws.webservices.dao.services.HoldTicketDAO{
	
	public HoldTicket getHoldTicketByGroupId(Integer ticketGroupId){
		return findSingle("from HoldTicket where ticketGroupId=? and holdStatus =?", new Object[]{ticketGroupId,HoldTicketStatus.ACTIVE});
	}
	
	public Collection<HoldTicket> getActiveHoldTickets(){
		return find("from HoldTicket where  holdStatus =?", new Object[]{HoldTicketStatus.ACTIVE});
	}
	
	public Collection<HoldTicket> getActiveHoldTicketsByConfigId(String configId){
		return find("from HoldTicket where  holdStatus =? and configId=?", new Object[]{HoldTicketStatus.ACTIVE,configId});
	}

}
