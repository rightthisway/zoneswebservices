package com.zonesws.webservices.dao.implementaion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.utils.DateUtil;

public class InvoiceDAO extends HibernateDAO<Integer, Invoice> implements com.zonesws.webservices.dao.services.InvoiceDAO{

	public int updateRealTixStatus(int invoiceId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE invoice SET realtix_map = 'YES' WHERE id = "+invoiceId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}
	
	public int updateTixUploaded(int invoiceId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE invoice SET is_real_tix_upload = 'YES' WHERE id = "+invoiceId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}
	
	public int updateBarCode(String barCode, int invoiceId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE invoice SET tix_bar_code = "+barCode+" WHERE id = "+invoiceId;
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}

	public Invoice getInvoiceByCustomerOrder(Integer customerOrderId, Integer customerId) {
		return findSingle("FROM Invoice where customerOrderId = ? and customerId = ?", new Object[]{customerOrderId, customerId});
	}
	
	public Invoice getInvoiceByOrderId(Integer orderId) throws Exception {
		return findSingle("FROM Invoice WHERE customerOrderId=?",new Object[]{orderId});
	}

	/*	
		String SQL_CUSTOMER_ORDER = "select i.* from customer_order o inner join invoice i on(o.id=i.order_id) where " +
				"i.order_id= and i.customer_id=";
		
		Query query = null;
		Session session = null;
		
		try {
			session = getSessionFactory().openSession();
			
			query = session.createSQLQuery(SQL_CUSTOMER_ORDER);
			List<Invoice> invoiceListByCustomer = query.list();
			
			if(null != invoiceListByCustomer && !invoiceListByCustomer.isEmpty()) {
				//System.out.println("size of fav event list :: " +eventListByArtist.size());
				return invoiceListByCustomer;
			}else{
				invoiceListByCustomer = new ArrayList<Invoice>();
			}
			return eventListByArtist;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
*/
	
}
