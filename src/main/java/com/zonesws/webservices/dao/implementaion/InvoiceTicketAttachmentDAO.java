package com.zonesws.webservices.dao.implementaion;

import java.util.List;


import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.enums.FileType;


public class InvoiceTicketAttachmentDAO extends HibernateDAO<Integer, InvoiceTicketAttachment> implements com.zonesws.webservices.dao.services.InvoiceTicketAttachmentDAO{

	public List<InvoiceTicketAttachment> getRealTicketsByInvoiceId(Integer invoiceId) {
		return find("FROM InvoiceTicketAttachment where invoiceId=?",new Object[]{invoiceId});
	}

	public InvoiceTicketAttachment getAttachmentByInvoiceAndFileType(Integer invoiceId, FileType fileType) {
		try {
			return findSingle("FROM InvoiceTicketAttachment where invoiceId=? AND type = ?", new Object[]{invoiceId,fileType});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<InvoiceTicketAttachment> getTicketAttachmentByInvoiceId(Integer invoiceId) {
		return find("FROM InvoiceTicketAttachment where invoiceId=?",new Object[]{invoiceId});
	}
}
