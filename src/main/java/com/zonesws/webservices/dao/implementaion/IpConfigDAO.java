package com.zonesws.webservices.dao.implementaion;


import java.util.Collection;

import com.zonesws.webservices.data.IpConfig;


public class IpConfigDAO extends HibernateDAO<Integer, IpConfig> implements com.zonesws.webservices.dao.services.IpConfigDAO{

	public IpConfig getIpConfigByIpAndConfigId(String configId, String ip) {
		return findSingle("FROM IpConfig WHERE (ipAddress = ? AND webServiceConfig.configId = ?) OR (ipAddress = '*' AND webServiceConfig.configId = ?)", new Object[]{ip,configId,configId});
	}

	@SuppressWarnings("unchecked")
	public Collection<IpConfig> ipConfigList() {
		return find("FROM IpConfig");
	}
	
}
