package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.LockedTicket;
import com.zonesws.webservices.enums.HoldTicketStatus;
import com.zonesws.webservices.enums.LockedTicketStatus;

 


public class LockedTicketDAO extends HibernateDAO<Integer, LockedTicket> implements com.zonesws.webservices.dao.services.LockedTicketDAO{
	
	public List<LockedTicket> getLockedTicketByItemId(String  itemId){
		return find("FROM LockedTicket lt WHERE lt.itemId = ? and lt.lockedTicketsStatus=?",new Object[]{itemId,HoldTicketStatus.ACTIVE});
	}
	
	public List<LockedTicket> getLockedTicketByLockedTickettatus(LockedTicketStatus lockedTicketStatus){
		return find("FROM LockedTicket lt WHERE lt.lockedTicketsStatus = ?",new Object[]{HoldTicketStatus.ACTIVE});		
	}
	
	public List<LockedTicket> getLockedTicketByCartId(Integer cartId){
		return find("FROM LockedTicket lt WHERE lt.cartItem.id = ? and lt.lockedTicketsStatus=?",new Object[]{cartId,HoldTicketStatus.ACTIVE});
	}
	public LockedTicket getLockedTicketByZPCartId(Integer cartId){
		return findSingle("FROM LockedTicket lt WHERE lt.cartItem.id = ? and lt.lockedTicketsStatus=?",new Object[]{cartId,HoldTicketStatus.ACTIVE});
	}
	
	public LockedTicket getLockedTicketByTicketGroupId(Integer ticketGroupId){
		return findSingle("FROM LockedTicket lt WHERE lt.ticketGroupId = ? and lt.lockedTicketsStatus=?",new Object[]{ticketGroupId,HoldTicketStatus.ACTIVE});
	}
	
	public void deleteByCartId (Integer cartId) {
		try {
		//getHibernateTemplate().getSessionFactory().
			deleteAll(getLockedTicketByCartId(cartId));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to get all locked tickets
	 * @return
	 */
	public List<LockedTicket> getAllLockedTicket () {
		return find("FROM LockedTicket where lockedTicketsStatus=?",new Object[]{HoldTicketStatus.ACTIVE});
	}

	public List<LockedTicket> getLockedTicketByEventId(Integer eventID) {
		return find("FROM LockedTicket lt WHERE lt.cartItem.eventId = ? and lockedTicketsStatus=?",new Object[]{eventID,HoldTicketStatus.ACTIVE});
	}
	
	public List<LockedTicket> getLockedTicketBySessionId(String sessionId){
		return find("FROM LockedTicket lt WHERE lt.cartItem.sessionId = ? and lt.lockedTicketsStatus=?",new Object[]{sessionId,HoldTicketStatus.ACTIVE});
	}
 
}
