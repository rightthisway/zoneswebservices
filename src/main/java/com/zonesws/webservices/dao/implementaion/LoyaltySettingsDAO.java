package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.LoyaltySettings;

/**
 * class having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public class LoyaltySettingsDAO extends HibernateDAO<Integer, LoyaltySettings> implements com.zonesws.webservices.dao.services.LoyaltySettingsDAO{
	
	
	public LoyaltySettings getActivetLoyaltySettings() {
		return findSingle("from LoyaltySettings where status='ACTIVE' ", new Object[]{});
	}
	
}
