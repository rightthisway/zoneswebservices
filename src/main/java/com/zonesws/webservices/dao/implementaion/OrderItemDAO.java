package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;

import com.zonesws.webservices.data.OrderItem;

public class OrderItemDAO extends HibernateDAO<Integer, OrderItem> implements com.zonesws.webservices.dao.services.OrderItemDAO{

	

	public List<OrderItem> getOrderDetailsByUserOrdrId(
			List<Integer> userOrderIds) {
		List<OrderItem> result = null;
		 
		  StringBuffer buffer = new StringBuffer ("from OrderItem where " +
		    " userOrder.id in (:userOrderIds)");
		  Query query = null;
		  Session session = null;
		try {
		   SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
		   session  = sessionFactory.openSession();    
		   query = session.createQuery(buffer.toString());
		   query.setParameterList("userOrderIds", userOrderIds);    
		   result = query.list();
		  } catch (Exception e) {
		   
		   e.printStackTrace();
		  } finally  {
		   session.close ();
		  }
		  
		  return result;
		
		
	}
	
	public OrderItem getOrderItemByOrderId(Integer orderId) {

		return findSingle("from OrderItem where userOrder.id=?", new Object[]{orderId});
		 
	}

	

	

}
