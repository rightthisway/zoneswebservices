package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.OrderPaymentBreakup;

public class OrderPaymentBreakupDAO extends HibernateDAO<Integer, OrderPaymentBreakup> implements com.zonesws.webservices.dao.services.OrderPaymentBreakupDAO{
	public List<OrderPaymentBreakup> getPaymentBreaksByOrderId(Integer orderId){
		return find("FROM OrderPaymentBreakup where orderId=?", new Object[]{orderId});
	}
}
