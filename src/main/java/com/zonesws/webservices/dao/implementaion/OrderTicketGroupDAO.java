package com.zonesws.webservices.dao.implementaion;


import java.util.List;

import com.zonesws.webservices.data.OrderTicketGroup;

public class OrderTicketGroupDAO extends HibernateDAO<Integer, OrderTicketGroup> implements com.zonesws.webservices.dao.services.OrderTicketGroupDAO {

	public void updateOrderTicketGroupDetails(Integer orderId){
		bulkUpdate("UPDATE OrderTicketGroup set orderId=? where orderId =?", new Object[]{-1, orderId});
	}
	
	public List<OrderTicketGroup> getAllTicketGroupsByOrderId(Integer orderId){
		return find("FROM OrderTicketGroup  where orderId =?", new Object[]{orderId});
	}
	
}
