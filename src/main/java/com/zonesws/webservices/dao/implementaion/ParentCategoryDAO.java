package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.ParentCategory;
/**
 * class having db related methods for parent category of a tour 
 * @author hamin
 *
 */ 
public class ParentCategoryDAO extends HibernateDAO<Integer,ParentCategory> implements
com.zonesws.webservices.dao.services.ParentCategoryDAO {

	/**
	 * method to get all parent categories
	 * @return list of parent categories
	 */
	public List<ParentCategory> getAll() {
		System.out.println("getAll() of ParentCategoryDAO is called");
		return find("FROM ParentCategory");	 
	}
	/**
	 * method to get parent category  by its name
	 * @param name, its name
	 * @return  parent category
	 */
	public ParentCategory getParentCategoryByName(String parent) {
	  return findSingle("FROM ParentCategory where name = ?", new Object[]{parent});	 
		
	}
	/**
	 * method to get parent category  by its id
	 * @param id, own id
	 * @return  parent category
	 */
	public ParentCategory getParentCategoryById(Integer id) {
		  return findSingle("FROM ParentCategory where id = ?", new Object[]{id});	 
			
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDistinctParentCategoryNmaes() {
		  
		return getHibernateTemplate().find("SELECT distinct name FROM ParentCategory");
		
	
	}
	 

}
