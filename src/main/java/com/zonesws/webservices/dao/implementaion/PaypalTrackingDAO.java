package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.PaypalTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public class PaypalTrackingDAO extends HibernateDAO<Integer, PaypalTracking> implements com.zonesws.webservices.dao.services.PaypalTrackingDAO{

	public PaypalTracking getTrackingByCustomerIdByTicketId(Integer cId, Integer eId,Integer tId,String sessionId,ApplicationPlatForm platForm){
		try{
			return findSingle("FROM PaypalTracking WHERE customerId = ? and eventId = ? and catTicketGroupId=? " +
					"and sessionId=? and platform=? ", new Object[]{cId,eId,tId,sessionId,platForm});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new PaypalTracking();
	}
	
}
