package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.PaypalTransactionDetail;

public class PaypalTransactionDetailDAO extends HibernateDAO<Integer, PaypalTransactionDetail> implements com.zonesws.webservices.dao.services.PaypalTransactionDetailDAO{

	public PaypalTransactionDetail getCustomerOrderDetailByOrderId(Integer orderId){
		try{
			return findSingle("FROM PaypalTransactionDetail WHERE orderId = ?", new Object[]{orderId});
		}catch (Exception e) {
			e.printStackTrace();
		}
		return new PaypalTransactionDetail();
	}
	
}
