package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.PollingCustomerRewards;
/**
 * interface having db related methods for PollingCustomerRewards
 *
 */
public class PollingCustomerRewardsDAO extends HibernateDAO<Integer, PollingCustomerRewards> implements com.zonesws.webservices.dao.services.PollingCustomerRewardsDAO{
	
	@Override
	public List<PollingCustomerRewards> getAllCustomerWithMagicWands(){
		return find("from PollingCustomerRewards");
	}
	@Override
	public PollingCustomerRewards getCustomerMagicWandStat(Integer customerId) {
		return findSingle("from PollingCustomerRewards where custId=?", new Object[]{customerId});
	}
	
}
