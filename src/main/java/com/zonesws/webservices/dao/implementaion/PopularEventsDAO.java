package com.zonesws.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.PopularEvents;

/**
 * class having db related methods for PopularEvents
 * @author Ulaganathan
 *
 */
public class PopularEventsDAO extends HibernateDAO<Integer, PopularEvents> implements com.zonesws.webservices.dao.services.PopularEventsDAO{
	
	
	
	public PopularEvents getActivetPopularEvents() {
		return findSingle("from PopularEvents where status='ACTIVE' ", new Object[]{});
	}
	
	public List<Artist> getAllPopularArtists(Map<Integer, Boolean> favArtistMap) {
		
		String sql ="select distinct pe.artist_id,CONVERT(varchar,a.name) as artistName ,zp.minPrice,pc.name " +
				"FROM popular_artist pe inner join events_rtf e WITH(NOLOCK) on pe.artist_id=e.artist_id " +
				"inner join artist a WITH(NOLOCK) on a.id=e.artist_id  inner join artist_category_mapping acm WITH(NOLOCK) " +
				"on acm.artist_id=a.id inner join parent_category pc WITH(NOLOCK) on pc.id=acm.parent_category_id " +
				"CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp " +
				"where e.status=1 and e.event_date>GETDATE()" ;
		
		List<Artist> artists = new ArrayList<Artist>();
		Query query = null;
		Session session = null;
		try {
			session = getSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			Artist artist = null;
			for (Object[] object : result) {
				artist = new Artist();
				Integer artistId = (Integer)object[0] ;
				Boolean isCustFavFlag = favArtistMap.get(artistId);
				
				if(null != isCustFavFlag && isCustFavFlag){
					artist.setCustFavFlag(true);
					continue;
				}else{
					artist.setCustFavFlag(false);
				}
				
				artist.setId((Integer)object[0]);
				artist.setName((String)object[1]);
				artist.setParentType((String)object[2]);
				artists.add(artist);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return artists;
	}
	
 public List<Artist> getAllPopularArtistsRevised(Map<Integer, Boolean> favArtistMap) {
		
		String sql ="select distinct pe.artist_id,CONVERT(varchar,a.name) as artistName  FROM popular_artist pe WITH(NOLOCK) " +
				"inner join events_rtf ea WITH(NOLOCK) on ea.artist_id=pe.artist_id inner join artist a WITH(NOLOCK) on a.id=pe.artist_id" ;
		
		List<Artist> artists = new ArrayList<Artist>();
		Query query = null;
		Session session = null;
		try {
			session = getSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			Artist artist = null;
			for (Object[] object : result) {
				artist = new Artist();
				Integer artistId = (Integer)object[0] ;
				Boolean isCustFavFlag = favArtistMap.get(artistId);
				
				if(null != isCustFavFlag && isCustFavFlag){
					artist.setCustFavFlag(true);
					continue;
				}else{
					artist.setCustFavFlag(false);
				}
				
				artist.setId((Integer)object[0]);
				artist.setName((String)object[1]);
				//artist.setParentType((String)object[2]);
				artists.add(artist);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return artists;
	}
	
	public List<Artist> getAllPopularArtistBySalesCount(Map<Integer, Boolean> favArtistMap) {
		
		String sql = "select distinct top 50 ea.artist_id,CONVERT(varchar,a.name) as artistName," +
				" ea.parent_category_name,sum(pe.sales_count) as totalSales" +
				" from popular_event pe WITH(NOLOCK)" +
				" inner join events_rtf ea WITH(NOLOCK) on ea.event_id=pe.tmat_event_id" +
				" inner join artist a WITH(NOLOCK) on a.id=ea.artist_id" +
				" where ea.event_date>getdate()" +//pe.sales_count > 100 and
				" group by ea.artist_id,a.name,ea.parent_category_name" +
				" order by  totalSales desc";
		
		List<Artist> artists = new ArrayList<Artist>();
		Query query = null;
		Session session = null;
		try {
			session = getSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			Artist artist = null;
			for (Object[] object : result) {
				artist = new Artist();
				Integer artistId = (Integer)object[0] ;
				Boolean isCustFavFlag = favArtistMap.get(artistId);
				
				if(null != isCustFavFlag && isCustFavFlag){
					artist.setCustFavFlag(true);
					continue;
				}else{
					artist.setCustFavFlag(false);
				}
				
				artist.setId((Integer)object[0]);
				artist.setName((String)object[1]);
				artist.setParentType((String)object[2]);
				artists.add(artist);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return artists;
	}
	
}
