package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.PreOrderPageAudit;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public class PreOrderPageAuditDAO extends HibernateDAO<Integer, PreOrderPageAudit> implements com.zonesws.webservices.dao.services.PreOrderPageAuditDAO{

	public PreOrderPageAudit getAudit(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId){
		return findSingle("from PreOrderPageAudit where customer.id=? " +
				"and  eventId=? and categoryTicketGroupId=? and sessionId=? and platForm=? " +
				"and status=?", new Object[]{customerId,eventId,ticketGroupId,sessionId,platForm,"LOCKED"});
	}
	
	public List<PreOrderPageAudit> getAllPendingData(){
		return find("from PreOrderPageAudit where isEmailSent=? and status not in ( ?,?) ", new Object[]{false,"COMPLETED","NOTIFIED"});
	}
	
	public void deleteNotEligibleTracking(Integer customerId){
		bulkUpdate("delete from PreOrderPageAudit where customer.id=?", new Object[]{customerId});
	}
	
}
