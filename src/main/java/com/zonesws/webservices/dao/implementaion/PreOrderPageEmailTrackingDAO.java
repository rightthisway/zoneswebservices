package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.PreOrderPageEmailTracking;


public class PreOrderPageEmailTrackingDAO extends HibernateDAO<Integer, PreOrderPageEmailTracking> implements com.zonesws.webservices.dao.services.PreOrderPageEmailTrackingDAO{

	public List<PreOrderPageEmailTracking> getAllCustomerTrackingRecords(Integer customerId , Date fromDate, Date toDate){
		return find("FROM PreOrderPageEmailTracking where customerId=? and emailSentDate between ? and ?" ,
				new Object[]{customerId,fromDate,toDate});
	}
	
	public List<PreOrderPageEmailTracking> getAllCustomerTrackingRecords(Integer customerId){
		return find("FROM PreOrderPageEmailTracking where customerId=?" ,
				new Object[]{customerId});
	}
	
}
