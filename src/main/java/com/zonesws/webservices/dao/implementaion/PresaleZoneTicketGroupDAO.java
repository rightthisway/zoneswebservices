package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.PresaleZoneTicketGroup;
import com.zonesws.webservices.enums.Status;

/**
 * class having db related methods for PresaleZoneTicketGroup
 * @author Ulaganathan
 *
 */
public class PresaleZoneTicketGroupDAO extends HibernateDAO<Integer, PresaleZoneTicketGroup> implements com.zonesws.webservices.dao.services.PresaleZoneTicketGroupDAO{
	
	
	public PresaleZoneTicketGroup getActivetTicketGroup(Integer id) {
		return findSingle("from PresaleZoneTicketGroup where status=? and id=? ", new Object[]{Status.ACTIVE,id});
	}
	
	public List<PresaleZoneTicketGroup> getActivetTicketGroupsByEventId(Integer eventId) {
		return find("from PresaleZoneTicketGroup where status=? and id=? ", new Object[]{Status.ACTIVE,eventId});
	}
	
	public List<PresaleZoneTicketGroup> getAllTicketsByTmatEventId(Integer tmatEventId){
		return find("From PresaleZoneTicketGroup where tmatEventId = ?",new Object[]{tmatEventId});
	}
	public List<Integer> getAllSoldTickets(Date fromDate,Date toDate) {
		return find("select tmatTicketGroupId From PresaleZoneTicketGroup where status=? and lastUpdatedDate >=? and lastUpdatedDate<=?",new Object[]{Status.ACTIVE,fromDate,toDate});
	}
	
	/*public List<ZoneTicketGroup> getAllSoldTickets(String fromDateStr,String toDateStr) {
		List<ZoneTicketGroup> zoneTicketGroups =null;
		
		String sql = "select id as id,tmat_ticket_group_id as tmatTicketGroupId " +
			" from zone_ticket_group fc WITH(NOLOCK) " +
			" WHERE status=4 and last_updated_date>='"+fromDateStr+"' and last_updated_date<='"+toDateStr+"'";
		  
		SQLQuery query = null;
		Session session = null;
		try {
			session = getSession();
			if(session==null){
				session = sessionfa.openSession();
				QueryManagerDAO.setSession(session);
			}
			query = session.createSQLQuery(sql);
			zoneTicketGroups =  query.setResultTransformer(Transformers.aliasToBean(ZoneTicketGroup.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
//		   //session.close();
		}
		return zoneTicketGroups;
	}*/
	
	
}
