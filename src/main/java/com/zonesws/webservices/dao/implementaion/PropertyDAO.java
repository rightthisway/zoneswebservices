package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.Property;

/**
 * class having db related methods for zone property
 * @author hamin
 *
 */ 
public class  PropertyDAO extends HibernateDAO<String, Property> implements com.zonesws.webservices.dao.services.PropertyDAO {
	/**
	 * method to get all zone properies
	 * @return list of properies
	 */
	public List<Property> getAllProperty() {		
		return find("FROM Property");	
		
	}
	/**
	 * method to  zone property by name
	 * @return Property
	 */
	public Property getPropertyByName(String name) {		
		return findSingle("FROM Property WHERE name = ?" , new Object[]{name});
				
	}
	public Integer getEventAPIRemovalTime() {
		Integer minutes = 0;
		Property property = getPropertyByName("zonesapi.event.removal.time.common");
		
		if(null != property) {
			minutes = Integer.parseInt(property.getValue().trim());
		}
		return minutes; 
	}
	public Integer getEventAPIRemovalTimeForNY() {
		Integer minutes = 0;
		Property property = getPropertyByName("zonesapi.event.removal.time.ny");
		
		if(null != property) {
			minutes = Integer.parseInt(property.getValue().trim());
		}
		return minutes; 
	}
	
	public Date getEventFromDateByExchangeRemoval() {
		java.sql.Date startDate = null;
		
		Property property = getPropertyByName("zones.exchangeremoval.days");
		
		try { 
			if(null != property && !property.getValue().equals("0")) {
				long addDayValue = 1000 * 60 * 60 * 24*(Integer.parseInt(property.getValue()));
				startDate = new java.sql.Date(new Date().getTime() + addDayValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return startDate;
		
	}
	
}
