package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.QuizAffiliateRewardHistory;

/**
 * class having db related methods for QuizAffiliateRewardHistory
 * @author Ulaganathan
 *
 */
public class QuizAffiliateRewardHistoryDAO extends HibernateDAO<Integer, QuizAffiliateRewardHistory> implements com.zonesws.webservices.dao.services.QuizAffiliateRewardHistoryDAO{
	
	public List<QuizAffiliateRewardHistory> getAllRewardsBreak(Integer customerId){
		return find("from QuizAffiliateRewardHistory where affiliateCustomerId=? ", new Object[]{customerId});
	}
	
}
