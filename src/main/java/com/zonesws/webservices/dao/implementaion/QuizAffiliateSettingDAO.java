package com.zonesws.webservices.dao.implementaion;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.zonesws.webservices.data.QuizAffiliateSetting;

/**
 * class having db related methods for QuizAffiliateSetting
 * @author Ulaganathan
 *
 */
public class QuizAffiliateSettingDAO extends HibernateDAO<Integer, QuizAffiliateSetting> implements com.zonesws.webservices.dao.services.QuizAffiliateSettingDAO{
	
	public QuizAffiliateSetting getActiveAffiliateByCustomerId(Integer customerId){
		Date curDate = new Date();
		System.out.println(curDate);
		QuizAffiliateSetting obj = findSingle("from QuizAffiliateSetting where customerId=? and status=? and effectiveFromDate <= ? ", new Object[]{customerId,"ACTIVE", curDate});
		if(null == obj) {
			return null;
		}
		if(null != obj.getEffectiveToDate() && curDate.before(obj.getEffectiveToDate())) {
			return obj;
		}
		return null;
	}
	
	public QuizAffiliateSetting getAffiliate(Integer customerId){
		return findSingle("from QuizAffiliateSetting where customerId=? ", new Object[]{customerId});
	}
	
	public List<QuizAffiliateSetting> getAllActiveAffiliates(){
		return find("from QuizAffiliateSetting where status=? ", new Object[]{"ACTIVE"});
	}
	
	public static void main(String[] args) {
		
		Date curDate = new Date();
		
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		
		Date fromDate = calendar.getTime();
		
		System.out.println("From Date: "+fromDate);
		System.out.println("Current Date: "+curDate);
		
		if(fromDate.before(curDate) || fromDate.equals(curDate)) {
			System.out.println("From Date is before Curretn Date");
		}
	}
	
}
