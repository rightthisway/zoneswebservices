package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;

public class RTFCustomerPromotionalOfferDAO extends HibernateDAO<Integer, RTFCustomerPromotionalOffer> implements com.zonesws.webservices.dao.services.RTFCustomerPromotionalOfferDAO{
	
	
	public RTFCustomerPromotionalOffer getPromoOfferCustomerId(Integer customerId){
		return findSingle("from RTFCustomerPromotionalOffer where customerId=? ", new Object[]{customerId});
	}
	
	public void updatePromotionaOfferOrderCount(Integer offerId) {
		try {
			String sql = "update RTFCustomerPromotionalOffer set noOfOrders=noOfOrders + 1 , status=?  where id = ?";
			bulkUpdate(sql, new Object[]{"REDEEMED",offerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
