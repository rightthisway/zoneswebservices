package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.RTFPromotionalOfferDtl;

public class RTFPromotionalOfferDtlDAO extends HibernateDAO<Integer, RTFPromotionalOfferDtl> implements com.zonesws.webservices.dao.services.RTFPromotionalOfferDtlDAO{
	
	public List<RTFPromotionalOfferDtl> getPromoDtlbyPromoOfferId(Integer promoOfferId){
		return find("from RTFPromotionalOfferDtl where promoOfferId = ?", new Object[]{promoOfferId});
	}
}
