package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.RTFPromotionalOfferHdr;

public class RTFPromotionalOfferHdrDAO extends HibernateDAO<Integer, RTFPromotionalOfferHdr> implements com.zonesws.webservices.dao.services.RTFPromotionalOfferHdrDAO{
	
	
	public List<RTFPromotionalOfferHdr> getAllActivePromotionalOffers(){
		return find("from RTFPromotionalOfferHdr where status = ? and endDate >= ? and noOfOrders < maxOrders ",
				new Object[]{"ENABLED",new Date()});
	}
	
	public RTFPromotionalOfferHdr getActivePromotionalOfferByCode(String promoCode){
		return findSingle("from RTFPromotionalOfferHdr where status = ? and endDate >= ? and noOfOrders < maxOrders " +
				"and  promoCode=? ", new Object[]{"ENABLED",new Date(),promoCode});
	}
	
	public void updatePromotionaOfferOrderCount(Integer offerId) {
		try {
			String sql = "update RTFPromotionalOfferHdr set noOfOrders=noOfOrders + 1  where id = ?";
			bulkUpdate(sql, new Object[]{offerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
