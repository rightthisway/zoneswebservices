package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.PromotionalType;

/**
 * class having db related methods for RTFPromotionalOfferTracking
 * @author Ulaganathan
 *
 */
public class RTFPromotionalOfferTrackingDAO extends HibernateDAO<Integer, RTFPromotionalOfferTracking> implements com.zonesws.webservices.dao.services.RTFPromotionalOfferTrackingDAO{
	

	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatForm platForm,Integer promoOfferId,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId,String promoCode,Boolean isLongTicket,PromotionalType offerType){
		return findSingle("from RTFPromotionalOfferTracking where promotionalOfferId=? and customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? and promoCode=? " +
				"and status=? and isLongTicket=?", 
				new Object[]{promoOfferId,customerId,eventId,ticketGroupId,sessionId,platForm,promoCode,"PENDING",isLongTicket});
	}
	
	public RTFPromotionalOfferTracking getPromoTrackingByTrackingId(ApplicationPlatForm platForm,Integer customerId, String sessionId, Integer trackingId){
		return findSingle("from RTFPromotionalOfferTracking where customerId=? " +
				"and id=? and sessionId=? and platForm=? " +
				"and status=?", new Object[]{customerId,trackingId,sessionId,platForm,"PENDING"});
	}
	
	
	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId){
		return findSingle("from RTFPromotionalOfferTracking where customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? " +
				"and status=?", new Object[]{customerId,eventId,ticketGroupId,sessionId,platForm,"PENDING"});
	}
	
	public RTFPromotionalOfferTracking getPendingPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId){
		return findSingle("from RTFPromotionalOfferTracking where customerId=? " +
				"and  eventId=? and ticketGroupId=? and sessionId=? and platForm=? " +
				"and status=?", new Object[]{customerId,eventId,ticketGroupId,sessionId,platForm,"PENDING"});
	}
	
	public List<RTFPromotionalOfferTracking> getAllPendingPromoTracking(){
		return find("from RTFPromotionalOfferTracking where status=? order by createdDate asc", new Object[]{"PENDING"});
	}
	
	public void updateOfferTrackingDiscount(Integer offerId, Double mobileDiscount, Double discAmt, Double discountedPrice) {
		try {
			String sql = "update RTFPromotionalOfferTracking set mobileAppDiscConv=?,additionalDiscountAmt=?,discountedPrice=? where id = ?";
			bulkUpdate(sql, new Object[]{mobileDiscount,discAmt,discountedPrice,offerId});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<RTFPromotionalOfferTracking> getAllCompletedReferralOrders(Date fromDate, Date toDate){
		/*return find("from RTFPromotionalOfferTracking where status=? and offerType=? " +
				"and orderId is not null", 
				new Object[]{"COMPLETED", PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE });*/
		
		return find("from RTFPromotionalOfferTracking where status=? and offerType=? " +
				"and orderId is not null and createdDate between ? and ? order by createdDate asc", 
				new Object[]{"COMPLETED", PromotionalType.CUSTOMER_REFERAL_DISCOUNT_CODE,fromDate,toDate});
	}
	
}
