package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.ReferralContest;

public class ReferralContestDAO extends HibernateDAO<Integer, ReferralContest> implements com.zonesws.webservices.dao.services.ReferralContestDAO {
	
	public List<ReferralContest> getAllActiveContest(){
		return find("FROM ReferralContest where status=? and contestStatus=?" , new Object[]{true,"ACTIVE"});
	}
	
	
}
