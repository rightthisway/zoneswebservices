package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.ReferralContestEmailVersion;

public class ReferralContestEmailVersionDAO extends HibernateDAO<Integer, ReferralContestEmailVersion> implements com.zonesws.webservices.dao.services.ReferralContestEmailVersionDAO {
	
	
	public ReferralContestEmailVersion getContestDetailsByContetKey(String key){
		return findSingle("FROM ReferralContestEmailVersion where contestIdentityKey=?", new Object[]{key});
	}
	
	public List<ReferralContestEmailVersion> getAllContestByOrderIdandParticipantId(Integer orderId, Integer participantCustId){
		return find("FROM ReferralContestEmailVersion where purchaserOrderId=? and participantCustId=?", new Object[]{orderId,participantCustId});
	}
	
	public ReferralContestEmailVersion getContest(Integer contestId, Integer participantCustId, Integer purchaserId){
		
		return findSingle("FROM ReferralContestEmailVersion where purchaserCustId=? and participantCustId=? and contestId=?", 
				new Object[]{purchaserId,participantCustId,contestId});
	}
}
