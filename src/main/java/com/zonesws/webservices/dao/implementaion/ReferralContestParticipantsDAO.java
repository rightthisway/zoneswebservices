package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.ReferralContestParticipants;

public class ReferralContestParticipantsDAO extends HibernateDAO<Integer, ReferralContestParticipants> implements com.zonesws.webservices.dao.services.ReferralContestParticipantsDAO {
	
	public ReferralContestParticipants getContestParticipantByCustomerId(Integer contestId, Integer participantCustId, Integer purchaserCustId){
		return findSingle("FROM ReferralContestParticipants where contestId=? and customerId=? and purchaseCustomerId=?",
				new Object[]{contestId,participantCustId,purchaserCustId});
	}
	
	public ReferralContestParticipants getContestParticipantByCustomerId(Integer participantCustId, Integer purchaserCustId){
		return findSingle("FROM ReferralContestParticipants where customerId=? and purchaseCustomerId=?",
				new Object[]{participantCustId,purchaserCustId});
	}
	
	public List<ReferralContestParticipants> getAllContestParticipantsToSendMail(){
		return find("FROM ReferralContestParticipants where isEmailed=?",new Object[]{false});
	}
	
}
