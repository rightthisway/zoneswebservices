package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.ReferralContestWinner;

public class ReferralContestWinnerDAO extends HibernateDAO<Integer, ReferralContestWinner> implements com.zonesws.webservices.dao.services.ReferralContestWinnerDAO{

	public ReferralContestWinner getWinnerByContestId(Integer contestId){
		return findSingle("FROM ReferralContestWinner WHERE contestId=?",new Object[]{contestId});
	}
}
