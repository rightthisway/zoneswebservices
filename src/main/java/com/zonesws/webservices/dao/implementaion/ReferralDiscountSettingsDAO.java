package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.ReferralDiscountSettings;
import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * class having db related methods for ReferralDiscountSettings
 * @author Ulaganathan
 *
 */
public class ReferralDiscountSettingsDAO extends HibernateDAO<Integer, ReferralDiscountSettings> implements com.zonesws.webservices.dao.services.ReferralDiscountSettingsDAO{
	
	
	public ReferralDiscountSettings getActiveDiscountSettingsByPlatForm(ApplicationPlatForm platForm) {
		return findSingle("from ReferralDiscountSettings where status='ACTIVE' and platForm =? ", new Object[]{platForm});
	}
	
}
