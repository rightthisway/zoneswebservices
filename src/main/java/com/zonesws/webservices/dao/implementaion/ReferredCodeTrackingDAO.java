package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.ReferredCodeTracking;

/**
 * 
 * @author dthiyagarajan
 *
 */
public class ReferredCodeTrackingDAO extends HibernateDAO<Integer, ReferredCodeTracking> implements com.zonesws.webservices.dao.services.ReferredCodeTrackingDAO{

	/**
	 * 
	 */
	public ReferredCodeTracking getReferralCode(String referralCode) {
		return findSingle("FROM ReferredCodeTracking where referredCode = ?", new Object[]{referralCode});
	}
	
	public ReferredCodeTracking getTrackingInfoByReferralCodeAndReferrarId(String referralCode,Integer referrerId) {
		return findSingle("FROM ReferredCodeTracking where referredCode = ? and referredTo=?", new Object[]{referralCode,referrerId});
	}

}
