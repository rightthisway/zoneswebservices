package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.ResetPasswordToken;

/**
 * class having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public class ResetPasswordTokenDAO extends HibernateDAO<Integer, ResetPasswordToken> implements com.zonesws.webservices.dao.services.ResetPasswordTokenDAO{
	
	
	public ResetPasswordToken getResetPasswordTokenByToken(String token){
		return findSingle("from ResetPasswordToken where resetToken=? ", new Object[]{token});
	}
	
	

}
