package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.RtfCatTicket;
import com.zonesws.webservices.enums.TicketStatus;

public class RtfCatTicketDAO extends HibernateDAO<Integer, RtfCatTicket> implements com.zonesws.webservices.dao.services.RtfCatTicketDAO{


	public List<RtfCatTicket> getActiveTickets(Integer eventId){
		return find("FROM RtfCatTicket WHERE status=? and eId=?", new Object[] {TicketStatus.ACTIVE,eventId});
	}
	
	public RtfCatTicket getTicket(Integer tixId){
		return findSingle("FROM RtfCatTicket WHERE id=?", new Object[] {tixId});
	}
}
