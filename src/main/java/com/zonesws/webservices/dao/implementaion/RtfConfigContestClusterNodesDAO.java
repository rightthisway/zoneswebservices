package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.RtfConfigContestClusterNodes;

public class RtfConfigContestClusterNodesDAO extends HibernateDAO<Integer, RtfConfigContestClusterNodes> implements com.zonesws.webservices.dao.services.RtfConfigContestClusterNodesDAO{

	public List<RtfConfigContestClusterNodes> getAllActiveRtfConfigContestClusterNodes()  throws Exception {
		return find("FROM RtfConfigContestClusterNodes WHERE status=?",new Object[]{1});
	}
}
