package com.zonesws.webservices.dao.implementaion;

import java.util.Collection;
import java.util.List;

import com.zonesws.webservices.data.RtfGiftCardOrder;

public class RtfGiftCardOrderDAO extends HibernateDAO<Integer, RtfGiftCardOrder> implements com.zonesws.webservices.dao.services.RtfGiftCardOrderDAO{

	@SuppressWarnings("unchecked")
	public List<RtfGiftCardOrder> getAllOrdersByCustomerId(Integer customerId){
		return find("FROM RtfGiftCardOrder WHERE customerId = ?", new Object[]{customerId});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<RtfGiftCardOrder> getCustomerOrderToSendEmail() throws Exception {
		return find("FROM RtfGiftCardOrder WHERE isEmailSent = ? and status=? ", new Object[]{false,"OUTSTANDING"});
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<RtfGiftCardOrder> getFilledOrdersToSendEmail() throws Exception {
		return find("FROM RtfGiftCardOrder WHERE isOrderFulfilled=? and isOrderFilledMailSent = ? ", new Object[]{true,false});
	}
	
	@SuppressWarnings("unchecked")
	public void updateOrderEmailSent(Integer orderId) {
		bulkUpdate("UPDATE RtfGiftCardOrder set isEmailSent = ? where id = ? ",new Object[]{true,orderId});
	}
	
	@SuppressWarnings("unchecked")
	public void updateOrderFilledEmailSent(Integer orderId) {
		bulkUpdate("UPDATE RtfGiftCardOrder set isOrderFilledMailSent = ? where id = ? ",new Object[]{true,orderId});
	}
}
