package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.RtfGiftCardQuantity;

public class RtfGiftCardQuantityDAO extends HibernateDAO<Integer, RtfGiftCardQuantity> implements com.zonesws.webservices.dao.services.RtfGiftCardQuantityDAO{

	@Override
	public RtfGiftCardQuantity getGiftCardQuantityByCardId(Integer cardId) {
		return findSingle("FROM RtfGiftCardQuantity WHERE cardId=? and status=?",new Object[]{cardId,true});
	}

}
