package com.zonesws.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.rtfquiz.webservices.data.QuizContestWinners;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.RtfPromotionalEarnLifeOffers;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.list.AutoPageResult;
/**
 * class having db related methods for Customer
 * @author tamil
 *
 */
public class RtfPromotionalEarnLifeOffersDAO extends HibernateDAO<Integer, RtfPromotionalEarnLifeOffers> implements com.zonesws.webservices.dao.services.RtfPromotionalEarnLifeOffersDAO {

	public RtfPromotionalEarnLifeOffers getRtfPromotionEarnLifeOfferByPromoCode(String promoCode) {
		return findSingle("from RtfPromotionalEarnLifeOffers where promoCode = ? ", new Object[]{promoCode});
	}
	public RtfPromotionalEarnLifeOffers getActiveRtfPromotionEarnLifeOfferByPromoCode(String promoCode) {
		return findSingle("from RtfPromotionalEarnLifeOffers where status='ACTIVE' and promoCode = ? ", new Object[]{promoCode});
	}
}
