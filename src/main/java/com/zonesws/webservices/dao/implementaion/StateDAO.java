package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.State;

/**
 * class having db related methods State (used in address )
 * @author hamin
 *
 */
public class StateDAO extends HibernateDAO<Integer, State> implements com.zonesws.webservices.dao.services.StateDAO{
	
	
	/**
	 *  method to get State by country id
	 * @param countryId, Id of a country
	 * @return list of states
	 */
	public List<State> getAllStateByCountryId(Integer countryId) {
		if(countryId == 217)
			return find("from State  where countryId= ? and id between 1 and 51 order by name", new Object[]{countryId});
		else if(countryId == 38)
			return find("from State  where countryId= ? and id <> 2005 order by name", new Object[]{countryId});
		else if(countryId == 94)
			return find("from State  where countryId= ? order by name", new Object[]{countryId});
		
		return null;
	}
	/**
	 *  method to get State by  id
	 * @param id, own id
	 * @return State
	 */
	public State getStateById(Integer id) {
		return findSingle("from State  where id= ? ", new Object[]{id});
	}
	
	public State getStateByCountryIdAndStateName(Integer countryId,String stateName){
		return findSingle("from State  where countryId = ? and name = ? ", new Object[]{countryId,stateName});
	}
	
	
	public List<State> getAllStateBySearchKey(String stateName){
		return find("from State where countryId in (?,?) and ( name like ? or shortDesc like ? )", new Object[]{217,38,stateName,stateName});
	}
	
	public List<State> getAllStateBySearchKeyAutoSearch(String stateName){
		return find("from State where countryId in (?,?) and ( name like ? or shortDesc like ? )", new Object[]{217,38,"%"+stateName+"%","%"+stateName+"%"});
	}

}
