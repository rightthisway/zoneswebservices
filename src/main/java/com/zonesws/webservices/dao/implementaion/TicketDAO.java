package com.zonesws.webservices.dao.implementaion;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.Ticket;
import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.utils.list.TicketList;

public class TicketDAO extends HibernateDAO<Integer, Ticket> implements com.zonesws.webservices.dao.services.TicketDAO {

	public TicketList getTicketListByEventId(Integer id) {
		return null;
	}

	public Ticket getTicketByEventIdAndEbayGroupId(Integer eventId,
			Integer ticketGroupId) {
		Session session = null;
		try{
			session = getSession();
			String sql ="SELECT t.id,section,normalized_section,row,seat,t.event_id,ticket_status from ticket t " +
					" inner join ebay_inventory ei on ei.ticket_id=t.id " +
					" inner join ebay_inventory_group eg on eg.id=ei.group_id " +
					" where eg.event_id=  " + eventId  + 
					" and eg.status='ACTIVE' " +
					" and t.ticket_status='ACTIVE' " +
					" and eg.id= " + ticketGroupId +
					" and rank=1";
				Query query = session.createSQLQuery(sql);
				List<Object> list = query.list();
				if(list ==null || list.isEmpty()){
					return null;
				}
				
				Object[] tix = (Object[])list.get(0);
				Ticket ticket = new Ticket();
				ticket.setId((Integer)tix[0]);
				ticket.setSection((String)tix[1]);
				ticket.setNormalizedSection((String)tix[2]);
				ticket.setRow((String)tix[3]);
				ticket.setSeat((String)tix[4]);
				ticket.setEventId((Integer)tix[5]);
				ticket.setTicketStatus((TicketStatus)tix[6]);
				return ticket;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}
	
	public Ticket getActiveTicketByItemId(String itemId) {
		 return findSingle("FROM Ticket WHERE itemId = ? AND ticketStatus <> ? ORDER BY itemId", new Object[] {itemId, TicketStatus.DISABLED});
		 
	}
	
	public Ticket getActiveMXPTicketByEventIdAndZone(Integer eventId,String zone) {
		String alternateZone = zone.toUpperCase().contains("ZONE")?zone.toLowerCase().replaceAll("ZONE", ""):("ZONE "+zone);
		return findSingle("FROM Ticket WHERE eventId = ?  AND ticketStatus = ? AND seller = ? AND (section = ? OR section = ?) ORDER BY itemId", new Object[] {eventId,TicketStatus.ACTIVE,"AOZones",zone,alternateZone});
		 
	}
	
}
