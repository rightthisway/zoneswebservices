package com.zonesws.webservices.dao.implementaion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.zonesws.webservices.data.TicketTraking;

public class TicketTrackingDAO extends HibernateDAO<Integer, TicketTraking> implements com.zonesws.webservices.dao.services.TicketTrackingDAO{
	
	public List<TicketTraking> searchTrackingRecords(Date startDate,Date endDate,Integer companyId) {
		try
		{
			DateFormat  df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			List<Object> params= new ArrayList<Object>();
			if(endDate == null || startDate == null)
				return null;
			
			String hql = "from TicketTraking where date >= '"+df.format(startDate)+"' and date <= '"+df.format(endDate)+"'";
			
			hql+=" and company_id = ?";
			params.add(companyId);
			
			System.out.println("******hql "+ hql+"****param"+params.toArray());
			return find(hql,params.toArray());
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	
	public void processOrder(String orderIDS)
	{
		Session session = getSession();
		try{
			String sql = "update order_item set trxn_status=1 where (isLock is null or isLock=0) and id in("+ orderIDS +")";
			SQLQuery  query = session.createSQLQuery(sql);
			query.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}		
	}
	
	public void unProcessOrder(String orderIDS)
	{
		Session session = getSession();
		try{
			String sql = "update order_item set trxn_status=0 where (isLock is null or isLock=0) and id in("+ orderIDS +")";
			SQLQuery  query = session.createSQLQuery(sql);
			query.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}		
	}
	public void lockOrder(String orderIDS)
	{
		Session session = getSession();
		try{
			String sql = "update order_item set isLock=1 where trxn_status=1 and id in("+ orderIDS +")";
			SQLQuery  query = session.createSQLQuery(sql);
			query.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}		
	}
}

