package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import com.zonesws.webservices.data.Tracking;

public class TrackingDAO extends HibernateDAO<Integer, Tracking> implements com.zonesws.webservices.dao.services.TrackingDAO {



	public Tracking getTrackingBySessionId(String sessionId) {
		return  findSingle("FROM Tracking WHERE sessionId like ? ",new Object[]{"%"+sessionId+"%"});
	}

	public Tracking getTrackingBySessionIdAndArtistId(String sessionId, Integer artistId) {
		return  findSingle("FROM Tracking WHERE sessionId like ? AND artistId = ? AND userType.id != ?",new Object[]{"%"+sessionId+"%",artistId,2});
	}
	
	

	public List<Tracking> getLatestTracking(){
		Session session = null;
		try{
			session = getSession();
			return (List<Tracking>)session.createCriteria(Tracking.class).addOrder(Order.desc("id")).setMaxResults(500).list();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
	}

	public List<Tracking> getCustomerOrdersById(Integer customerId) {
		// TODO Auto-generated method stub
		return  find("FROM Tracking WHERE customer.id like ? ",new Object[]{customerId});
	}

	

}