package com.zonesws.webservices.dao.implementaion;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.enums.AddressType;
import com.zonesws.webservices.enums.Status;
/**
 * class having having db related methods for user address (used for billing , mailing purpose )
 * @author Ulaganathan
 *
 */
public class UserAddressDAO extends HibernateDAO<Integer, UserAddress> implements com.zonesws.webservices.dao.services.UserAddressDAO{
	
	private static Session userSession = null;
	
	public static Session getUserSession() {
		return userSession;
	}

	public static void setUserSession(Session userSession) {
		UserAddressDAO.userSession = userSession;
	}

	
	@Override
	public UserAddress get(Integer id) {
		// TODO Auto-generated method stub
		return findSingle("From UserAddress Where id = ? and status=? " , new Object[]{id,Status.ACTIVE});
	}
	public List<UserAddress> getUserAddressInfo(Integer customerId,AddressType addressType) {
		return find("From UserAddress Where customerId = ? and addressType = ? and status=? " +
				"and hideOnRTF <> ?" , new Object[]{customerId,addressType,Status.ACTIVE,true});
	}
	
	public List<UserAddress> getShippingAddressInfo(Integer customerId) {
		List<UserAddress> shippingAddressList = new ArrayList<UserAddress>();
		try{
			userSession = UserAddressDAO.getUserSession();
			if(userSession==null || !userSession.isOpen() || !userSession.isConnected()){
				userSession = getSession();
				UserAddressDAO.setUserSession(userSession);
			}
			
			Query query = userSession.createQuery("From UserAddress Where customerId = :customerId and addressType = :addressType " +
					"and status = :statusParam  and hideOnRTF <> :hideOnRTFParam")
							.setParameter("customerId", customerId)
							.setParameter("addressType", AddressType.SHIPPING_ADDRESS)
							.setParameter("statusParam", Status.ACTIVE)
							.setParameter("hideOnRTFParam", true);
			
			shippingAddressList = query.list();
			return shippingAddressList;
		}catch (Exception e) {
			if(userSession != null && userSession.isOpen() ){
				userSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}


	public Integer getShippingAddressCount(Integer customerId) {
		int count = 0;
		try {
			userSession = UserAddressDAO.getUserSession();
			if(userSession==null || !userSession.isOpen() || !userSession.isConnected()){
				userSession = getSession();
				UserAddressDAO.setUserSession(userSession);
			}
			count = ((Long) userSession.createQuery("select count(*) from UserAddress where customerId = :customerId and addressType = :addressType " +
					" and  status = :statusParam and hideOnRTF <> :hideOnRTFParam")
							.setParameter("customerId", customerId)
							.setParameter("addressType", AddressType.SHIPPING_ADDRESS)
							.setParameter("statusParam", Status.ACTIVE)
							.setParameter("hideOnRTFParam", true)
							.uniqueResult())
							.intValue();
			return count;
		} catch (Exception e) {
			if(userSession != null && userSession.isOpen() ){
				userSession.close();
			}
			e.printStackTrace();
			return null;
		}
	}

	public List<UserAddress> getUserAddressByCustomerId(Integer customerId){
		
		return find("FROM UserAddress WHERE customerId = ? and status=? and hideOnRTF <> ? ORDER BY id DESC",new Object[]{customerId,
				Status.ACTIVE, true});

	}
	
	
	public UserAddress getUserAddressByIdandAddressType(Integer customerId,Integer addressId,AddressType addressType) {
		return findSingle("From UserAddress Where customerId=? and id = ? and addressType = ? and status=? " +
				"and hideOnRTF <> ?", new Object[]{customerId,addressId,addressType,Status.ACTIVE,true});
	}
	
  public UserAddress getUserAddressInfo(Integer customerId) {
		
		return findSingle("From UserAddress Where customerId = ? and addressType = ? and hideOnRTF <> ?", 
				new Object[]{customerId,AddressType.BILLING_ADDRESS,true});
	}
  
  public void deleteCustomerExistingAddresses(Integer customerId) {
		
	/*bulkUpdate("UPDATE UserAddress set status=? WHERE customerId = ? ", 
			new Object[]{Status.DELETED, customerId});*/
	bulkUpdate("UPDATE UserAddress set hideOnRTF=? WHERE customerId = ? AND (rtwCustomerId is not null OR " +
			"rtw2CustomerId is not null OR tixCityCustomerId is not null)", new Object[]{true, customerId});
  }
  
  public List<UserAddress> getAllShippingAddresByOrder(Integer customerId) {
	return find("From UserAddress Where customerId = ? and addressType = ? and status=? " +
			"and hideOnRTF <> ? order by lastUpdated desc" , new Object[]{customerId,AddressType.SHIPPING_ADDRESS,Status.ACTIVE,true});
  }
  
  
}
