package com.zonesws.webservices.dao.implementaion;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.UserOrder;
/**
 * class having db related methods for user order
 * @author hamin
 *
 */
public class UserOrderDAO extends HibernateDAO<Integer, UserOrder> implements com.zonesws.webservices.dao.services.UserOrderDAO{

	public List<UserOrder> getOrdersByCustomerId(Integer customerId) {
		
		return find("From UserOrder where customerId = ? and id > 79 ",new Object[]{customerId});
	}
	
	public List<UserOrder> getPendingOrders(Date toDate) {
		
		return find("From UserOrder where orderStatus='PENDING' and orderDate<=? ",new Object[]{toDate});
	}
	
}
