package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.UserPreference;

public class UserPreferenceDAO extends HibernateDAO<Integer, UserPreference> implements com.zonesws.webservices.dao.services.UserPreferenceDAO{

	public UserPreference getUserPreferenceByUsername(String username) {
		return findSingle("FROM UserPreference where username like ?",new Object[]{username});
	}

	public UserPreference getUserPreferenceByIPAddress(String ipAddress) {
		return findSingle("FROM UserPreference where ipAddress like ?",new Object[]{ipAddress});
	}

}
