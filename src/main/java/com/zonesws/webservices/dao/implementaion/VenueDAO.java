package com.zonesws.webservices.dao.implementaion;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.Venue;
import com.zonesws.webservices.utils.list.VenueResult;

public class VenueDAO extends HibernateDAO<Integer, Venue> implements com.zonesws.webservices.dao.services.VenueDAO{

	public  List<VenueResult> getAllVenuesBySearchKeyAndParentId(String searchKey,Integer pageNumber,Integer maxRows,Integer parentCategoryId) {
	 
			/*String sql ="select B.*,count(1) over () totalRows from (select distinct A.venue_id,A.building from " +
					"(select distinct e.venue_id,e.building,zp.minPrice  from event_details e  CROSS APPLY " +
					"( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) " +
					"where zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) " +
					"zp where e.status=1 and e.event_date>GETDATE() and e.reward_thefan_enabled=1  ";
		
			if(null != parentCategoryId && !parentCategoryId.equals("") && parentCategoryId > 0){
				sql += " and e.parent_category_id ="+parentCategoryId;
			}else{
				sql += " and e.building like '%"+searchKey+"%' ";
			}
			sql = sql+" ) A where A.minPrice is not null ) B order by B.building OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY ";*/
		
			StringBuilder queryString = new StringBuilder();
			queryString.append(" DECLARE @searchinput varchar(50),@svlikestart varchar(60), @svlikemid varchar(60),@svlikelast varchar(60),");
			queryString.append(" @svnotlikestart varchar(60), @svnotlikemid varchar(60),@svnotlikelast varchar(60);");
			queryString.append(" SET @searchinput  = '"+searchKey+"';SET  @svlikestart = '%[ ]' + @searchinput + '%';SET  @svlikemid = '%' + @searchinput + '[ ]%';");
			queryString.append(" SET  @svlikelast = '%[ ]' + @searchinput + '[ ]%';SET  @svnotlikestart = '%[^ ]' + @searchinput + '%';");
			queryString.append(" SET  @svnotlikemid = '%' + @searchinput + '[^ ]%';SET  @svnotlikelast = '%[^ ]' + @searchinput + '[^ ]%';");
			queryString.append(" select b.venue_id,b.building,count(1) over () totalRows from (select distinct 0 as sort_col, A.venue_id,A.building from ( ");
			queryString.append(" select distinct e.venue_id,e.building,zp.minPrice from event_details e WITH(NOLOCK)");
			queryString.append(" CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK)");
			queryString.append(" where zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp");
			queryString.append(" where e.status=1 and e.event_date>GETDATE() and e.reward_thefan_enabled=1");
			
			if(null != parentCategoryId && !parentCategoryId.equals("") && parentCategoryId > 0){
				queryString.append(" and e.parent_category_id ="+parentCategoryId);
			}else if(null != searchKey && !searchKey.isEmpty()){
				queryString.append(" and ( e.building = @searchinput or e.building  like @svlikestart  or e.building  like @svlikemid or e.building  like @svlikelast) A where A.minPrice is not null");
				queryString.append(" UNION ALL");
				queryString.append(" select distinct 0 as sort_col, A.venue_id,A.building from (select distinct e.venue_id,e.building,zp.minPrice from event_details e WITH(NOLOCK) ");
				queryString.append(" CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK)");
				queryString.append(" where zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp");
				queryString.append(" where e.status=1 and e.event_date>GETDATE() and e.reward_thefan_enabled=1 ");
				queryString.append(" and ( e.building  like @svnotlikestart  or e.building  like @svnotlikemid or e.building  like @svnotlikelast)");
			}
			
			
			queryString.append(") A ) B order by B.building OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY");//where A.minPrice is not null,order by B.sort_col
			
			List<VenueResult> venues =new ArrayList<VenueResult>();
			Query query = null;
			Session session = null;
			try {
				session = getSessionFactory().openSession();
				query = session.createSQLQuery(queryString.toString());
				List<Object[]> result = (List<Object[]>)query.list();
				VenueResult venue = null;
				for (Object[] object : result) {
					venue= new VenueResult();
					venue.setVenueId((Integer)object[0]);
					venue.setVenueName((String)object[1]);
					venue.setTotalVenues((Integer)object[2]);
					venues.add(venue);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally  {
				session.close();
			}
			return venues;
		}
 
 
 
}
