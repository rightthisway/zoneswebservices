package com.zonesws.webservices.dao.implementaion;

import com.zonesws.webservices.data.WebConfigMapping;

public class WebConfigMappingDAO extends HibernateDAO<Integer, WebConfigMapping> implements
com.zonesws.webservices.dao.services.WebConfigMappingDAO {
	
	
	public WebConfigMapping getWebConfigByCompanyId(Integer companyId){
		return findSingle("FROM WebConfigMapping where company.id=? ", new Object[]{companyId});
	}
	
	public WebConfigMapping getWebConfigByWebConfigId(Integer webConfigId){
		
		return findSingle("FROM WebConfigMapping where webServiceConfig.id=? ", new Object[]{webConfigId});
	}
	

}
