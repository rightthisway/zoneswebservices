package com.zonesws.webservices.dao.implementaion;


import java.util.Collection;

import com.zonesws.webservices.data.WebServiceConfig;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.Status;


public class WebServiceConfigDAO extends HibernateDAO<Integer, WebServiceConfig> implements com.zonesws.webservices.dao.services.WebServiceConfigDAO{
	
	public WebServiceConfig getWebServiceConfgiByConfigId(String configId){
		return findSingle("FROM WebServiceConfig where configId=? and status=? ", new Object[]{configId,Status.ACTIVE});
	}

	@SuppressWarnings("unchecked")
	public Collection<WebServiceConfig> getWebServiceConfig() {
		return find("FROM WebServiceConfig where status=? ", new Object[]{Status.ACTIVE});
	}
	
	public WebServiceConfig getWebServiceConfgiByConfigId(String configId,ApplicationPlatForm platForm){
		return findSingle("FROM WebServiceConfig where configId=? and status=? and platForm=?", new Object[]{configId,Status.ACTIVE,platForm});
	}
}
