package com.zonesws.webservices.dao.implementaion;


import com.zonesws.webservices.data.ZPSessionCart;
import com.zonesws.webservices.enums.HoldTicketStatus;
/**
 * class havingdb related methods for ZPSessionCart (user session cart) 
 * @author Ulaganathan
 *
 */
public class ZPSessionCartDAO extends HibernateDAO<Integer, ZPSessionCart> implements com.zonesws.webservices.dao.services.ZPSessionCartDAO{
	/**
	 * method to get ZPSessionCart  by  session Id
	 * @param sessionId, http session Id
	 * @return
	 */
	public ZPSessionCart getZPSessionBySessionId(String sessionId){
		return findSingle("From ZPSessionCart where sessionId = ?",
				new Object[]{sessionId});
	}


}
