package com.zonesws.webservices.dao.implementaion;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.data.ZoneRGBColor;

/**
 * class having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public class ZoneRGBColorDAO extends HibernateDAO<Integer, ZoneRGBColor> implements com.zonesws.webservices.dao.services.ZoneRGBColorDAO{
	
	public Collection<ZoneRGBColor> getZoneRGBColorDetailsByZones(Collection<String> zones){
		String hql ="From ZoneRGBColor where zone in (:zones) ";
		Query query = null;
		Session session = null;
		Collection<ZoneRGBColor> list = null;
		try {
			session = getSession();
			query = session.createQuery(hql).setParameterList("zones", zones);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return list;
	}
}
