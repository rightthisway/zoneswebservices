package com.zonesws.webservices.dao.implementaion;

import java.util.List;

import com.zonesws.webservices.data.LockedTicket;
import com.zonesws.webservices.data.ZoneTicketsAdditionalMarkup;
import com.zonesws.webservices.enums.HoldTicketStatus;

/**
 * class having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public class ZoneTicketsAdditionalMarkupDAO extends HibernateDAO<Integer, ZoneTicketsAdditionalMarkup> implements com.zonesws.webservices.dao.services.ZoneTicketsAdditionalMarkupDAO{
	
	public List<ZoneTicketsAdditionalMarkup> getZoneTicketsAdditionalMarkupsByEventId(Integer eventId) {
		return find("FROM ZoneTicketsAdditionalMarkup  WHERE eventId = ? and status='ACTIVE' order by zone",new Object[]{eventId});
	}
	
	public ZoneTicketsAdditionalMarkup getZoneTicketsAdditionalMarkupsByEventIdandZone(Integer eventId,String zone) {
		return findSingle("FROM ZoneTicketsAdditionalMarkup  WHERE eventId = ? and zone=?",new Object[]{eventId,zone});
	}
	
}
