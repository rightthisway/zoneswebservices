package com.zonesws.webservices.dao.implementaion;


import com.zonesws.webservices.data.ZonesEventCategoryMapping;


public class ZonesEventCategoryMappingDAO extends HibernateDAO<Integer, ZonesEventCategoryMapping> implements com.zonesws.webservices.dao.services.ZonesEventCategoryMappingDAO{

	public ZonesEventCategoryMapping getZonesEventCategoryMappingByEventId(Integer eventId) {
		return findSingle("FROM ZonesEventCategoryMapping WHERE eventId = ?", new Object[]{eventId});
	}
	
	
}
