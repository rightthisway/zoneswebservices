package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.AffiliateCashReward;
/**
 * interface having db related methods for AffiliateCashReward
 * @author Ulaganathan
 *
 */
public interface AffiliateCashRewardDAO extends RootDAO<Integer, AffiliateCashReward>{
	
	public AffiliateCashReward getAffiliateByUserId(Integer userId);
	public void updateAffiliateCashByCustomer(Double cash,  Integer userId);
	
	/*public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId);
	void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId);
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints();
	public void updateCustomerLoyaltyByCustomer(Double earnPoints,  Integer customerId);*/
}
