package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.AffiliateCashRewardHistory;
/**
 * interface having db related methods for AffiliateCashRewardHistory
 * @author Ulaganathan
 *
 */
public interface AffiliateCashRewardHistoryDAO extends RootDAO<Integer, AffiliateCashRewardHistory>{
	
	public void updateCashRewardHistoryToActive(Integer id/*,boolean isEmailSent,String description*/) throws Exception;
	
}
