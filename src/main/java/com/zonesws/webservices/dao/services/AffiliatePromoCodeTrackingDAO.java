package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.AffiliatePromoCodeTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;
/**
 * interface having db related methods for AffiliatePromoCodeTracking
 * @author Ulaganathan
 *
 */
public interface AffiliatePromoCodeTrackingDAO extends RootDAO<Integer, AffiliatePromoCodeTracking>{
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer userId,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId,String promoCode,Boolean isLongTicket);
	
	public AffiliatePromoCodeTracking getPromoTrackingByTrackingId(ApplicationPlatForm platForm,Integer userId,Integer customerId, 
			String sessionId, Integer trackingId);
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer userId,Integer customerId, String sessionId,
			String promoCode,String ipAddress);
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId);
	
	public AffiliatePromoCodeTracking getPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,String ipAddress);
	
	/*public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId);
	void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId);
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints();
	public void updateCustomerLoyaltyByCustomer(Double earnPoints,  Integer customerId);*/
}
