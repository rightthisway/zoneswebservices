package com.zonesws.webservices.dao.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.enums.ProductType;

public interface ArtistDAO extends RootDAO<Integer, Artist>{
	
	List<Artist> getAllArtistBySearchKeyAndPageNumber(String searchKey,Integer pageNumber,Integer maxRows,
			Map<Integer, Boolean> favArtistMap,Integer superFanArtistId);

	public  List<Artist> getAllArtistByGrandChildCategoryAndPageNumber(Integer grandChildCategoryId,Integer pageNumber,Integer maxRows,ProductType productType,
			Map<Integer, Boolean> favArtistMap,Integer superFanArtistId);
	public List<Artist> getArtistByGrandChildCategoryId(Integer grandChildCategoryId,Integer pageNumber,Integer maxRows,ProductType productType);
	
	/**
	 * Get list of favourite events for customer
	 * @param customerId
	 * @return
	 */
	public List<Artist> getFavoriteArtistForCustomer(Integer customerId);
	
	public Collection<Artist> getSuperFanArtistDetails();
	public  List<Artist> getAllArtistBySearchKey(String searchKey,Integer pageNumber,Integer maxRows,
			Map<Integer, Boolean> favArtistMap,Integer childId,Integer grandChildId,Integer parentCategoryId,String grandChildCateoryIds,String childCateoryIds);
	
	public List<Artist> getAllSportsArtistByKey(String searchKey,Integer pageNumber,Integer maxRows);
}
