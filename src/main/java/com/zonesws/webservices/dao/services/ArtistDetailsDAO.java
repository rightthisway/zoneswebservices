package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.ArtistDetails;

public interface ArtistDetailsDAO extends RootDAO<Integer, ArtistDetails>{

	ArtistDetails getArtistInfo(Integer artistId) throws Exception;
}
