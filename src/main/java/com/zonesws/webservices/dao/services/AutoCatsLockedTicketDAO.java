package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.AutoCatsLockedTickets;
import com.zonesws.webservices.enums.LockedTicketStatus;
import com.zonesws.webservices.enums.ProductType;

public interface AutoCatsLockedTicketDAO extends RootDAO<Integer, AutoCatsLockedTickets>{
	public List<AutoCatsLockedTickets> getLockedTicketByItemId(String  itemId);
	public List<AutoCatsLockedTickets> getLockedTicketByLockedTickettatus(LockedTicketStatus lockedTicketStatus);
	public List<AutoCatsLockedTickets> getLockedTicketByCartId(Integer cartId);
	public AutoCatsLockedTickets getLockedTicketByZPCartId(Integer cartId);
	public List<AutoCatsLockedTickets> getLockedTicketByEventId(Integer eventID,ProductType productType);
	
	public List<AutoCatsLockedTickets> getAllLockedTicket ();
	public void deleteByCartId (Integer cartId);
	
	//public List<LockedTicket> getLockedTicketByHoldTicketId(Integer holdTicketId);
	public AutoCatsLockedTickets getLockedTicketByTicketGroupId(Integer ticketGroupId);
	public List<AutoCatsLockedTickets> getLockedTicketBySessionId(String sessionId,ProductType productType);
	public AutoCatsLockedTickets getLockedTicketByCategoryGroupId(Integer eventID,Integer CategoryGroupId);
	public AutoCatsLockedTickets getLockedTicketBySessionAndCategoryTicketGroupId(String sessionId,ProductType productType,Integer categoryTicketGroupId);
	public AutoCatsLockedTickets getLockedTicketBySessionCategoryTicketGroupId(String sessionId,Integer categoryTicketGroupId);
	public AutoCatsLockedTickets getLockedTicketBySessionCategoryTicketGroupIdEventId(String sessionId,Integer categoryTicketGroupId,Integer eventID);
	
	public void deleteByLockId(Integer lockId);
}
