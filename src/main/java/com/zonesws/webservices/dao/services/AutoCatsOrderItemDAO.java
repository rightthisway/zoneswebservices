package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.AutoCatsOrderItem;

/**
 * class having db related methods for autocats  order Item
 * @author Ulaganathan
 *
 */

public interface AutoCatsOrderItemDAO extends RootDAO<Integer, AutoCatsOrderItem> {

	List<AutoCatsOrderItem> getOrderDetailsByUserOrdrId(List<Integer> userOrderIds);
	
	AutoCatsOrderItem getOrderItemByOrderId(Integer orderId);

}
