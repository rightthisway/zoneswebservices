package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.AutoCatsOrderItem;
import com.zonesws.webservices.data.AutoCatsUserOrder;
/**
 * interface having db related methods for AutoCats user order
 * @author Ulaganathan
 *
 */
public interface AutoCatsUserOrderDAO extends RootDAO<Integer, AutoCatsUserOrder>{

	List<AutoCatsUserOrder> getOrdersByCustomerId(Integer customerId);
	public List<AutoCatsUserOrder> getPendingOrders(Date toDate);

}
