package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.Cards;

public interface CardsDAO extends RootDAO<Integer, Cards>{
	
	public Cards getCardsByCardPosition(Integer productId,Integer desktopPosition,Integer mobilePosition) throws Exception;
	public List<Cards> getAllCardsByProductId(Integer productId) throws Exception;
	public Cards getExistingCardsByCardPostionsAndCardId(Integer productId,Integer cardId,Integer desktopPosition,Integer mobilePosition) throws Exception;
}
