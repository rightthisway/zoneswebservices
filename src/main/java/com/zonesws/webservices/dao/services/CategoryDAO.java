package com.zonesws.webservices.dao.services;



import java.util.List;

import com.zonesws.webservices.data.Category;



public interface CategoryDAO extends RootDAO<Integer, Category>{
	public List<String> getCategoryByArtistId(Integer id);
}
