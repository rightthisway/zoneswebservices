package com.zonesws.webservices.dao.services;



import com.zonesws.webservices.data.CategoryMapping;



public interface CategoryMappingDAO extends RootDAO<Integer, CategoryMapping>{
	String getCategoryGroupNameByEventId(Integer eventId);
}
