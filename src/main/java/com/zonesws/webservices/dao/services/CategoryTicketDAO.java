package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CategoryTicket;

public interface CategoryTicketDAO extends RootDAO<Integer, CategoryTicket>{
	public List<CategoryTicket> getUnmappedCategoryTicketByInvoiceId(Integer invoiceId) throws Exception;
	public List<CategoryTicket> getMappedCategoryTicketByInvoiceId(Integer invoiceId) throws Exception;
	public int updateCategoryTicket(Integer ticketGroupId, Integer invoiceId, Double soldPrice) throws Exception ;
}
