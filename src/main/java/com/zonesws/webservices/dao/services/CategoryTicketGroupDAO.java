package com.zonesws.webservices.dao.services;

import java.util.Collection;

import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.enums.ProductType;

public interface CategoryTicketGroupDAO extends RootDAO<Integer, CategoryTicketGroup> {

	public Collection<CategoryTicketGroup> getAllActiveCategoryTicketsbyEventId(Integer eventId) throws Exception;
	public Collection<CategoryTicketGroup> getAllActiveCategoryTicketsbyEventIdAndProductType(Integer eventId,ProductType productType) throws Exception;
	public CategoryTicketGroup getCategoryTicketsbyCategoryTicketgroupIdAndProductType(Integer categoryTicketgroupId,ProductType productType) throws Exception;
	public CategoryTicketGroup getCategoryTicketsbyCategoryTicketgroupId(Integer categoryTicketgroupId) throws Exception;
	/*int updateCategoryTicket(int ticketId, int invoiceId) throws Exception;*/
	public Collection<CategoryTicketGroup> getAllBroadcastedCategoryTicketsbyEventId(Integer eventId) throws Exception;
	public CategoryTicketGroup getBroadCastedCategoryTicketsbyId(Integer categoryTicketgroupId) throws Exception ;
	public void updateCategoryTicketGroupAsSold(Integer categoryTicketGroupId,Integer invoiceId, Double soldPrice, Integer soldQty);
	public void updateCategoryTicketGroupAsLocked(Integer categoryTicketGroupId,Integer invoiceId, Double soldPrice, Integer soldQty);
	public void updateTicketAsSold(Integer categoryTicketGroupId,Integer invoiceId);
	public void revertSoldTicketToActive(Integer categoryTicketGroupId);
	public CategoryTicketGroup getLockedTicketById(Integer categoryTicketgroupId) throws Exception;
	public CategoryTicketGroup getLockedTicketsbyTktgroupIdAndProductType(Integer categoryTicketgroupId,ProductType productType) throws Exception;
}
