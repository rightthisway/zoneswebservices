package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CheckOutOfferAppliedAudit;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public interface CheckOutOfferAppliedAuditDAO extends RootDAO<Integer, CheckOutOfferAppliedAudit>{
	
	public CheckOutOfferAppliedAudit getCheckoutOffer(ApplicationPlatForm platForm,Integer customerId, String sessionId, Integer ticketId
			,Boolean isFanPrice );
}