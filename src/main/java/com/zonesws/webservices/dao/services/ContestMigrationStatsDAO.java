package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.ContestMigrationStats;
/**
 * interface having db related methods for ContestMigrationStats
 * @author Ulaganathan
 *
 */
public interface ContestMigrationStatsDAO extends RootDAO<Integer, ContestMigrationStats>{
	
	 
	public List<ContestMigrationStats> getAllStatsByContestId(Integer contestId);
	
}
