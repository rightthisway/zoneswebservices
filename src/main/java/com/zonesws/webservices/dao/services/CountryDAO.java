package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.Country;
/**
 * interface having db related methods for Country
 * @author hamin
 *
 */
public interface CountryDAO extends RootDAO<Integer, Country>{
	/**
	 * method to get List of Countries
	 * @return List of Countries
	 */
	public List<Country> getAllCountry();
	/**
	 * method to get Country by id
	 * @param id, company id
	 * @return
	 */
	public Country geCountryById(Integer id);
	/**
	 * method to get id of US. 
	 * @return Id for US
	 */
	public Integer getIdOfUs();
	
	public Country getCountryByCountryName(String countryName);
	
	public List<Country> getActiveCountries();
}
