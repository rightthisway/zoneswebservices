package com.zonesws.webservices.dao.services;

import java.util.List;
import java.util.Set;

import com.zonesws.webservices.data.CrownJewelCategoryTicket;


public interface CrownJewelCategoryTicketDAO extends RootDAO<Integer, CrownJewelCategoryTicket> {
	
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventId(Integer eventId) throws Exception;
	public CrownJewelCategoryTicket getCrownjewelTicketById(Integer ticketId,Integer eventId) throws Exception;
	public void updateCrownJewelCategoryTicketstoDeletedByEventId(Integer eventId) throws Exception;
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTicketsByEventIdByTeamIdByLeagueId(Integer eventId,
			Integer teamId,Integer leagueId) throws Exception;
	public List<CrownJewelCategoryTicket> getAllTeamZonesTickets(Integer eventId,
			Integer teamId,Integer leagueId) throws Exception;
	public List<CrownJewelCategoryTicket> getAllActiveCrownjewelTickets(Integer eventExcludeDays) throws Exception;
	public CrownJewelCategoryTicket getFakeCategoryTicketByTicketIdByEventId(Integer ticketId,Integer eventId) throws Exception;
	public Set<Integer> getAllActiveLeagueIds() throws Exception;
	public Set<String> getAllActiveTeamIds() throws Exception;
	public Set<String> getAllActiveLeaguesTeamsCities() throws Exception;
	public Set<String> getAllActiveEvents() throws Exception;
	public Set<String> getAllEventsByLeagueAndTeams() throws Exception;
	public List<CrownJewelCategoryTicket> getAllActiveFanatsyRealTicketsByCategoryId(Integer categoryId) throws Exception;
	public CrownJewelCategoryTicket getRealTiketByTicketId(Integer ticketId) throws Exception;
}
