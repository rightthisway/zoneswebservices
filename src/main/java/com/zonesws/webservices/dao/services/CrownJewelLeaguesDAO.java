package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CrownJewelLeagues;
import com.zonesws.webservices.utils.list.FantasyCategories;
import com.zonesws.webservices.utils.list.FantasyEventTeamTicket;
import com.zonesws.webservices.utils.list.FantasyTicketsBean;


public interface CrownJewelLeaguesDAO extends RootDAO<Integer, CrownJewelLeagues> {

	List<CrownJewelLeagues> getAllLeaguesByParentType(String parentType) throws Exception;
	
	CrownJewelLeagues getFantasyEventById(Integer fantasyEventId) throws Exception;
	
	List<CrownJewelLeagues> getAllSuperFanLeagues();
	
	List<FantasyCategories> getAllActiveFanstasySportsCategories();
	
	List<FantasyTicketsBean> getAllActiveFantasyEventTeamsByGrandChild(Integer grandChildId);
	
	List<FantasyEventTeamTicket> getAllActiveFanstasyEventTeamTickets(Integer grandChildId);
	
	FantasyTicketsBean getEventVenueInformation(Integer tmatEventId);
	
	List<Integer> getAllActiveGrandChilds();
}
