package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CrownJewelTeamZones;

public interface CrownJewelTeamZonesDAO extends RootDAO<Integer, CrownJewelTeamZones> {
	
	public List<CrownJewelTeamZones> getAllTeamZonesByLeagueId(Integer leagueId) throws Exception;
	public List<CrownJewelTeamZones> getAllTeamZonesByTeamId(Integer teamId) throws Exception;
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueId(Integer leagueId) throws Exception;
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByTeamId(Integer teamId) throws Exception;
	public List<CrownJewelTeamZones> getAllActiveTeamZonesByLeagueIdAndCity(Integer leagueId,String city) throws Exception;
	public CrownJewelTeamZones getTeamZoneByEventTeamZone(Integer teamId,Integer eventId,String zone) throws Exception;
	public CrownJewelTeamZones getFakeTicketByTicketId(Integer ticketId) throws Exception;
}
