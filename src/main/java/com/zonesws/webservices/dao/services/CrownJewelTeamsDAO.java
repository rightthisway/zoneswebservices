package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CrownJewelTeams;
import com.zonesws.webservices.utils.list.CrownEventCity;
import com.zonesws.webservices.utils.list.SportsTeam;

public interface CrownJewelTeamsDAO extends RootDAO<Integer, CrownJewelTeams> {
	
	public List<CrownJewelTeams> getAllTeamsByLeagueId(Integer leagueId) throws Exception;
	
	public List<CrownJewelTeams> getAllTeamsByLeagueIdByLeagueCity(Integer leagueId,String leagueCity) throws Exception;
	
	public CrownJewelTeams getAllTeamsByLeagueIdByEventId(Integer leagueId,Integer eventId) throws Exception;
	
	public CrownJewelTeams getAllSuperFanTeamsByTeamId(Integer teamId) throws Exception;
	
	public List<SportsTeam> getAllActiveSportsTeamsByLeagueId(Integer leagueId) throws Exception;
	
	public List<CrownEventCity> getAllActiveOtherTeamsByLeagueId(Integer leagueId);
	
	
}
