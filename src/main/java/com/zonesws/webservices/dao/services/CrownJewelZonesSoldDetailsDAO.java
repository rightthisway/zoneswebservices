package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CrownJewelZonesSoldDetails;

public interface CrownJewelZonesSoldDetailsDAO extends RootDAO<Integer, CrownJewelZonesSoldDetails> {
	
	public CrownJewelZonesSoldDetails getZoneSoldDetailsByTeamIdByLeagueIdByEventIdByZone(Integer fEventId,Integer teamId,
				Integer zoneId, String zone) throws Exception;
}
