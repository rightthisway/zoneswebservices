package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerCardInfo;

public interface CustomerCardInfoDAO extends RootDAO<Integer, CustomerCardInfo>{

	public CustomerCardInfo getActiveCustomerCardInfoById(Integer cardId);
	public List<CustomerCardInfo> getAllActiveCardsByCustomerId(Integer customerId);
	public List<CustomerCardInfo> getAllCardsByCustomerId(Integer customerId);
	public CustomerCardInfo getActiveCustomerCardInfoByStripeId(String stripeId,String sCustId);
	public List<CustomerCardInfo> getAllActiveStripeRegisteredCardsByCustomerId(Integer customerId);
}
