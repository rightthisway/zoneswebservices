package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerCassandra;

public interface CustomerCassandraDAO extends RootDAO<Integer, CustomerCassandra>{

	public List<CustomerCassandra> getCustomerDateByContestId(Integer contestId);
}
