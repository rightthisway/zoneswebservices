package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerCheckoutVisitOffer;
/**
 * interface having db related methods for CustomerCheckoutVisitOffer
 * @author Ulaganathan
 *
 */
public interface CustomerCheckoutVisitOfferDAO extends RootDAO<Integer, CustomerCheckoutVisitOffer>{
	
	
	public CustomerCheckoutVisitOffer getActivetOffer();
	
	
}
