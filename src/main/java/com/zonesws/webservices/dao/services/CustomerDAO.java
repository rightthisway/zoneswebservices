package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.ProductType;
/**
 * interface having db related methods for Customer
 * @author hamin
 *
 */
public interface CustomerDAO  extends RootDAO<Integer, Customer>{
	
	/**
	 * method to get Customer by email and password
	 * @param email, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 */
	public List<Customer> getCustomer(String email,String password);
	/**
	 * method to get Country by email
	 * @param email, email address of a customer	
	 * @return Customer
	 */
	public Customer getCustomerByEmailId(String email);
	/**
	 * method to get customer by userId
	 * @param userId, userId of a customer	
	 * @return Customer
	 */
	public Customer getCustomerByUserId(String userId);
	
	public List<Customer> getAllCustomersWithoutUserId(ProductType productType);
	public List<Customer> getAllCustomersWithoutUserIdForAutoUserIDCreation(ProductType productType,Date toDate);
	
	public Customer getCustomerByUserIdAndProductType(String userId,ProductType productType);
	/**
	 * method to get Customer by email and password
	 * @param emailId, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 */
	public Customer getCustomerByEmailIdAndPassword(String emailId,String password);
	
	/**
	 * method to get Customer by userName
	 * @param userName, user name of a customer
	 * @return Customer
	 */
	public Customer getCustomerByUserName(String userName);
	/**
	 * method to get Customer by email and user name
	 * @param emailId, email address of a customer
	 * @param userName, user name of a customer
	 * @return Customer
	 */
	public Customer getCustomerByEmailAndUserName(String email, String userName);
	/**
	 *  method to get Customer by customer Id
	 * @param customerId,  customer Id
	 * @return Customer
	 */
	public Customer getCustomerById(Integer customerId);
	
	/**
	 *  method to get Customer by customer Id and Product Type
	 * @param customerId,  customer Id
	 * @param productType,  Product Type
	 * @return Customer
	 */
	public Customer getCustomerByIdByProduct(Integer customerId,ProductType productType);
	/**
	 * method to get Customer user name and password	 
	 * @param userName, user name of a customer
	 * @param password , password of a customer
	 * @return Customer
	 */
	public Customer getCustomerByUserNameAndPassword(String userName,
			String password);
	/**
	 * method to get Customer by email	 
	 * @param email, email address of a customer
	 * @return Customer
	 */
	public Customer getCustomerByEmail(String email);
	
	public Customer getCustomerByPhone(String phone);
	
	public Customer getCustomerByEmailAndProductTypeExceptThisCustomer(String email,ProductType productType,Integer customerId);
	public Customer getCustomerByUserIdAndProductTypeExceptThisCustomer(String userId,ProductType productType,Integer customerId);
	
	public Customer getCustomerByCompanyId(Integer companyId);
	 
	/**
	 * method to get Customer by email and product type 
	 * @param email, email address of a customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 */
	public Customer getCustomerByEmailByProductType(String email,ProductType productType);
	
	public Customer getCustomerByPhoneAndProductType(String phone,ProductType productType);
	/**
	 * method to get Customer by email or user id and product type 
	 * @param userName, email address or user id of a customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 */
	
	public Customer getCustomerByUserNameByProductType(String userName,ProductType productType);
	/**
	 * method to get Customer by deviceId and product type 
	 * @param socialAccountId, FaceBook Account Id of the customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 */
	public Customer getCustomerBySocialAcctIdByProductType(String socialAccountId,ProductType productType);
	
	public void updateCustImagePath(String imageExt, Integer customerId);
	public void updateCustBlockedStatus(Boolean blockedStatus, Integer customerId);
	
	public Customer getCustomerByReferralCode(String referralCode);
	
	public List <Customer> getAll();
	
	public List <Customer> getAllCustomersByLAstUpdateTime(Date lastUpdatedDate);
	
	public void updateCustomerForTicketPurchase(Integer customerId);
	
	public void updateQuizCustomerLives(Integer customerId,Integer customerLives);
	
	public void updateLivesUsedByCustomer(Integer customerId,Integer livesUsed);
	
	public List<CustomerSearchDetails> getCustomerAutosearchForQuiz(String searchKey,Integer customerId);
	
	public Customer getCustomerByUserIdExceptThisCustomer(String userId,Integer customerId);
	
	public List<Customer> getCustomersByUserNameByProductType(String userName,ProductType productType);
	
	public Customer getOtpVerifiedCustomerByPoneAndProductTypeExceptThisCustomer(String phone,ProductType productType,Integer customerId);
	
	public List <Customer> getAllOtpVerifiedCustomers();
	public List <Customer> getAllRewardTheFanCustomers();
	public List <Customer> getAllCustomerEmailToBeSent();
	public void updateQuizCustomerRtfPoints(Integer customerId,Integer rtfPoints);
	public void updatePointsRedemption(Integer customerId,Integer rtfPoints);
	public List <Customer> getAllOtpVerifiedCustomers1();
	public void updateSfLevelNo(Integer customerId,Integer levelNo);
	public void resetCustomerSuperFanLevels();
	public void updateQuizCustomerLivesAndRtfPoints(Integer customerId,Integer customerLives,Integer rtfPoints);
	public void updateCustomerLoyaltyPoints(Integer customerId,Integer loyaltyPoints);
}



