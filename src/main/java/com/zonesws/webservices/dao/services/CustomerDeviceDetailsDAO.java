package com.zonesws.webservices.dao.services;

import java.util.List;
import java.util.Map;

import com.zonesws.webservices.data.CustomerDeviceDetails;
/**
 * interface having db related methods for Customer App Device Details
 * @author Ulaganathan
 *
 */
public interface CustomerDeviceDetailsDAO  extends RootDAO<Integer, CustomerDeviceDetails>{
	

	public List<CustomerDeviceDetails> getAllActiveDeviceDetailsByCustomerId(Integer customerId);
	public List<CustomerDeviceDetails> getAllActiveDeviceDetails();
	public CustomerDeviceDetails getActiveDeviceDetailsByCustomerIdAndDeviceId(Integer customerId,String deviceId);
	public CustomerDeviceDetails getActiveDeviceDetailsByDeviceId(String deviceId);
	public Map<Integer, List<CustomerDeviceDetails>> getAllActiveFavoriteArtistCustomerDeviceDetails(List<Integer> artistIds);
	public Map<Integer, List<CustomerDeviceDetails>> getAllActiveLoaylFanCustomerDeviceDetails(List<Integer> artistIds);
	public void updateCustomerDeviceDetails(Integer customerId, String deviceId) throws Exception;
	public CustomerDeviceDetails getActiveDeviceDetailsByDeviceIdAndNotificationRegId(String deviceId,String notificationRegId);
	
}
