package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerFantasyOrder;

public interface CustomerFantasyOrderDAO extends RootDAO<Integer, CustomerFantasyOrder>{
	
	public List<CustomerFantasyOrder> getAllActiveOrderByCustomerId(Integer customerId);
	public CustomerFantasyOrder getFSTOrder(Integer orderId,Integer customerId);

}
