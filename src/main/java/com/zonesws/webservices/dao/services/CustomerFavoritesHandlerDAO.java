package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerFavoritesHandler;
 
/**
 * interface having method related to CustomerFavoritesHandler
 * @author Ulaganathan
 *
 */
public interface CustomerFavoritesHandlerDAO extends RootDAO<Integer, CustomerFavoritesHandler>{
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<CustomerFavoritesHandler> getAll();
	/**
	 * method to get child category using its name
	 * @param name, name of child category  
	 * @return ChildCategory
	 */
	public CustomerFavoritesHandler getChildCategoryByName(String name);
	/**
	 * method to get child category using its id
	 * @param id, child category id
	 * @return ChildCategory
	 */
	public CustomerFavoritesHandler getChildCategoryById(
			Integer id);

	public List<CustomerFavoritesHandler> getChildCategoryByParentCategoryName(String parentCategoryName);
	/**
	 * 
	 * @param customerId
	 * @return
	 */
	public List<CustomerFavoritesHandler> getallArtistByCustomerId(Integer customerId);
	
	public CustomerFavoritesHandler getFavoriteArtistByArtistIdAndCustomerId(Integer artistId,Integer customerId);
	
	public void updateFavoriteArtistByCustomer(List<Integer> artistIds, Integer customerId);
	
	public List<CustomerFavoritesHandler> getAllActiveFavArtistByCustomerId(Integer customerId);
	public List<CustomerFavoritesHandler> getAllActiveFavoriteArtist();
	public List<CustomerFavoritesHandler> getAllFavoriteArtistByCustomerId(Integer customerId);
}
