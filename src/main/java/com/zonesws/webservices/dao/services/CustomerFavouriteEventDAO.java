package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerFavouriteEvent;
import com.zonesws.webservices.enums.ProductType;
/**
 * interface having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public interface CustomerFavouriteEventDAO extends RootDAO<Integer, CustomerFavouriteEvent>{
	
	
	public List<CustomerFavouriteEvent> getAllActiveFavouriteEventsByCustomerId(Integer customerId);
	public List<CustomerFavouriteEvent> getAllActiveFavouriteEventsByCustomerIdAndProduct(Integer customerId,ProductType productType);
	
	public List<Integer> getAllActiveFavouriteEventIdsByCustomerId(Integer customerId);
	public List<Integer> getAllActiveFavouriteEventIdsByCustomerIdByProduct(Integer customerId,ProductType productType);
	public CustomerFavouriteEvent getFavouriteEventsByCustomerIdEventId(Integer customerId,Integer eventId);
	public CustomerFavouriteEvent getFavouriteEventsByCustomerIdEventIdAndProduct(Integer customerId,Integer eventId,ProductType productType);
	
	public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId);
	public List<CustomerFavouriteEvent> getAllActiveFavouriteEvents();
	
}
