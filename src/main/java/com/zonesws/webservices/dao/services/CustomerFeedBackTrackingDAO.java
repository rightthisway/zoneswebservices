package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerFeedBackTracking;
/**
 * interface having db related methods for QuizAffiliateRewardHistory
 * @author Ulaganathan
 *
 */
public interface CustomerFeedBackTrackingDAO extends RootDAO<Integer, CustomerFeedBackTracking>{
	
}
