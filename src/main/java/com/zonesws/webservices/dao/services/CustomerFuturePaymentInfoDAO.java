package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerFuturePaymentInfo;

public interface CustomerFuturePaymentInfoDAO extends RootDAO<Integer, CustomerFuturePaymentInfo>{

	public CustomerFuturePaymentInfo getRefreshTokenByCustomerId(Integer customerId);
	
}
