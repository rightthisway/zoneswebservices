package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerLoginHistory;
/**
 * interface having db related methods for Customer Login history
 * @author Ulaganathan
 *
 */
public interface CustomerLoginHistoryDAO  extends RootDAO<Integer, CustomerLoginHistory>{
	

	public List<CustomerLoginHistory> getAllLoginHistoryByCustomerId(Integer customerId);
	public List<CustomerLoginHistory> getAllMobileLoginHistoryByCustomerId(Integer customerId);
	
	
	/**
	 * method to get Customer by email and password
	 * @param email, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 *//*
	public List<Customer> getCustomer(String email,String password);
	*//**
	 * method to get Country by email
	 * @param email, email address of a customer	
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailId(String email);
	*//**
	 * method to get Customer by email and password
	 * @param emailId, email address of a customer
	 * @param password , password of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailIdAndPassword(String emailId,String password);
	
	*//**
	 * method to get Customer by userName
	 * @param userName, user name of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByUserName(String userName);
	*//**
	 * method to get Customer by email and user name
	 * @param emailId, email address of a customer
	 * @param userName, user name of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailAndUserName(String email, String userName);
	*//**
	 *  method to get Customer by customer Id
	 * @param customerId,  customer Id
	 * @return Customer
	 *//*
	public Customer getCustomerById(Integer customerId);
	
	*//**
	 *  method to get Customer by customer Id and Product Type
	 * @param customerId,  customer Id
	 * @param productType,  Product Type
	 * @return Customer
	 *//*
	public Customer getCustomerByIdByProduct(Integer customerId,ProductType productType);
	*//**
	 * method to get Customer user name and password	 
	 * @param userName, user name of a customer
	 * @param password , password of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByUserNameAndPassword(String userName,
			String password);
	*//**
	 * method to get Customer by email	 
	 * @param email, email address of a customer
	 * @return Customer
	 *//*
	public Customer getCustomerByEmail(String email);
	
	
	public Customer getCustomerByCompanyId(Integer companyId);
	 
	*//**
	 * method to get Customer by email and product type 
	 * @param email, email address of a customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 *//*
	public Customer getCustomerByEmailByProductType(String email,ProductType productType);
	
	*//**
	 * method to get Customer by deviceId and product type 
	 * @param socialAccountId, FaceBook Account Id of the customer
	 * @param productType, Which product customer belongs too
	 * @return Customer
	 *//*
	public Customer getCustomerBySocialAcctIdByProductType(String socialAccountId,ProductType productType);
	*/
}
