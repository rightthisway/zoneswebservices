package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerLoyalty;
/**
 * interface having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public interface CustomerLoyaltyDAO extends RootDAO<Integer, CustomerLoyalty>{
	
	public CustomerLoyalty getCustomerLoyaltyByCustomerId(Integer customerId);
	
	/*public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId);
	public List<CustomerLoyalty> getAllActiveFavouriteEventsByCustomerId(Integer customerId);*/
	
	void updateCustomerLoyalty(Double pointsRemaining, Double latestSpendPoints, Double totalSpendPoints, Integer customerId);
	public List<CustomerLoyalty> getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints();
	public void updateCustomerLoyaltyByCustomer(Double earnPoints,  Integer customerId);
	public void updateCustomerLoyaltyById(Integer id,String message);
	public void updateCustomerLoyaltyByCustomerId(Integer custId,String message);
	public void updateCustomerLoyaltyByCustomerForManualCredits(Double earnPoints,  Integer customerId);
}
