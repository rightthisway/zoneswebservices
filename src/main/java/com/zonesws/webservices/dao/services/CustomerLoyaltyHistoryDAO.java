package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.enums.RewardBreakUpType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.utils.list.RewardDetails;
/**
 * interface having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public interface CustomerLoyaltyHistoryDAO extends RootDAO<Integer, CustomerLoyaltyHistory>{
	
	public CustomerLoyaltyHistory getCustomerLoyaltyByCustomerId(Integer customerId);
	public CustomerLoyaltyHistory getCustomerLoyaltyByValues(Integer customerId,Integer orderId,Integer OrderItemId);
	//public CustomerLoyaltyHistory getLatestOrderCustomerLoyaltyByCustomerIdandExcludeOrderId(Integer customerId,Integer excludeOrderId);
	public CustomerLoyaltyHistory getCustomerLoyaltyHistoryByOrderId(Integer orderId);
	public CustomerLoyaltyHistory getCustCrownJewelLoyaltyByOrderId(Integer orderId);
	/*public void deleteFavouriteEventByEventIdAndCustomerId(Integer eventId,Integer customerId);
	*/
	public List<CustomerLoyaltyHistory> getAllCustomerPendingRewards();
	public void updateRewardHistoryToActive(Integer id,boolean isEmailSent,String description) throws Exception;
	public List<RewardDetails> getCustomerRewardsByRewardStatus(RewardStatus rewardStatus , Integer customerId,RewardBreakUpType breakUpType);
	public List<RewardDetails> getCustomerRedeemedRewardsByCustomerId(Integer customerId);
	public void updateRewardHistoryToActive(Integer id,Date activatedDate) throws Exception;
	public CustomerLoyaltyHistory getLateDownloadCustomerLoyaltyHistoryByOrderId(Integer orderId);
	public CustomerLoyaltyHistory getLateDownloadCustomerLoyaltyHistoryByGiftCardOrderId(Integer orderId);
	public List<CustomerLoyaltyHistory> getCustomerContestReferralRewards(Integer customerId);
	public List<CustomerLoyaltyHistory> getReferralRewardsByPRimaryAndTireOneCustomerIds(Integer customerId, Integer tireOneCustomerId);
	public Double getSpentPoints(Integer customerId);
	public List<CustomerLoyaltyHistory> getMistakendata(Integer customerId);
	public Double getTotalReferralPoints(Integer customerId) ;
	public void deleteRewardsFOrContest266(Integer  customerId) throws Exception;
}
