package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerLoyaltyTracking;

public interface CustomerLoyaltyTrackingDAO extends RootDAO<Integer, CustomerLoyaltyTracking>{
	
	public List<CustomerLoyaltyTracking> getTrackingByCustomerId(Integer customerId);
	public List<CustomerLoyaltyTracking> getRewardsByContestId(Integer contestId,Integer customerId);
	public Double getContestPoints(Integer customerId);
}
