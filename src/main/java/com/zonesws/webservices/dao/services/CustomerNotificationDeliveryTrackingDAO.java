package com.zonesws.webservices.dao.services;

import java.util.List;
import java.util.Map;

import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerNotificationDeliveryTracking;
/**
 *
 */
public interface CustomerNotificationDeliveryTrackingDAO  extends RootDAO<Integer, CustomerNotificationDeliveryTracking>{
	

	//public List<CustomerDeviceDetails> getAllActiveDeviceDetailsByCustomerId(Integer customerId);
	//public List<CustomerDeviceDetails> getAllActiveDeviceDetails();
	
}
