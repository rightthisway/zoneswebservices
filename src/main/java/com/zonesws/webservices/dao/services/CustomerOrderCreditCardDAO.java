package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerOrderCreditCard;

public interface CustomerOrderCreditCardDAO extends RootDAO<Integer, CustomerOrderCreditCard>{

	public CustomerOrderCreditCard getCreditDetailsByTicketGroupId(Integer ticketGroupId);
	
}
