package com.zonesws.webservices.dao.services;

import java.util.Collection;
import java.util.List;

import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.enums.OrderFetchType;
import com.zonesws.webservices.enums.OrderType;

public interface CustomerOrderDAO extends RootDAO<Integer, CustomerOrder>{

	public List<CustomerOrder> getOrdersByCustomerIdandOrderType(Integer customerId,OrderFetchType orderFetchType,Integer inculdeDays,OrderType orderType);
	public List<CustomerOrder> getPastEventsCustomerOrders(Integer customerId);
	public CustomerOrder getCustomerOrderByOrderId(Integer orderId);
	Collection<CustomerOrder> getCustomerOrderToSendEmail() throws Exception;
	public void updateOrderEmailSent(Integer orderId,String toEmail);
	public void updateTicketDownloadEmailSent(Integer orderId,String toEmail);
	public void updateOrderCancelEmailSent(Integer orderId,String toEmail);
	public Collection<CustomerOrder> getAllCustomerActiveOrder() throws Exception;
	public Collection<CustomerOrder> getAllRTFOrders() throws Exception;
	public CustomerOrder getPaymentPendingOrderById(Integer orderId);
}
