package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerOrderDetail;

public interface CustomerOrderDetailDAO extends RootDAO<Integer, CustomerOrderDetail>{

	public CustomerOrderDetail getCustomerOrderDetailByOrderId(Integer orderId);
	public CustomerOrderDetail getGiftCardCustomerOrderDetailByOrderId(Integer orderId);
	
}
