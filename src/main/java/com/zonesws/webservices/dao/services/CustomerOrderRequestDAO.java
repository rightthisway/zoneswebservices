package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.CustomerOrderRequest;

public interface CustomerOrderRequestDAO extends RootDAO<Integer, CustomerOrderRequest>{

	public CustomerOrderRequest getOrderRequestByOrderId(Integer orderId);
	public List<CustomerOrderRequest> getAllPaymentPendingOrders(Date toDate);
}
