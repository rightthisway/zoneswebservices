package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerOrderTracking;

public interface CustomerOrderTrackingDAO extends RootDAO<Integer, CustomerOrderTracking>{

}
