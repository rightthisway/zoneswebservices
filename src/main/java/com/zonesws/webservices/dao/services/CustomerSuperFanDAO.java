package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerSuperFan;
 
/**
 * interface having method related to CustomerSuperFan
 * @author Ulaganathan
 *
 */
public interface CustomerSuperFanDAO extends RootDAO<Integer, CustomerSuperFan>{
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<CustomerSuperFan> getAll();
	/**
	 * method to get child category using its name
	 * @param name, name of child category  
	 * @return ChildCategory
	 */
	public CustomerSuperFan getChildCategoryByName(String name);
	/**
	 * method to get child category using its id
	 * @param id, child category id
	 * @return ChildCategory
	 */
	public CustomerSuperFan getChildCategoryById(
			Integer id);

	public List<CustomerSuperFan> getChildCategoryByParentCategoryName(String parentCategoryName);
	/**
	 * 
	 * @param name
	 * @return
	 */
	public List<CustomerSuperFan> getAllSuperFansByCustomerId(Integer customerId);
	
	public CustomerSuperFan getSuperFansByCustomerIdByArtistId(Integer customerId,Integer artistId);
	public CustomerSuperFan getActiveLoyalFanByCustomerId(Integer customerId);
}
