package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerTicketDownloads;


public interface CustomerTicketDownloadsDAO extends RootDAO<Integer, CustomerTicketDownloads> {
	
	public CustomerTicketDownloads getCustomerTicketDownloadsById(Integer id);
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByOrderId(List<Integer> orderId);
	public List<CustomerTicketDownloads> getCustomerTicketDownloadsByCustomerId(List<Integer> customerId);

}
