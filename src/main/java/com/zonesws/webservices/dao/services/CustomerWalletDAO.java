package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerWallet;

public interface CustomerWalletDAO extends RootDAO<Integer, CustomerWallet>{

	public CustomerWallet getCustomerWalletByCustomerId(Integer customerId);
}
