package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.CustomerWalletHistory;


public interface CustomerWalletHistoryDAO extends RootDAO<Integer, CustomerWalletHistory>{

	public CustomerWalletHistory getCustomerWalletHistoryByCustomerId(Integer customerId,String transactionId);
	public CustomerWalletHistory getPendingDebitTransactionById(Integer id);
	public CustomerWalletHistory getPendingDebitTransactionByOrderId(Integer orderId);
}
