package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.DiscountCodeTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public interface DiscountCodeTrackingDAO extends RootDAO<Integer, DiscountCodeTracking>{

	public DiscountCodeTracking getTrackingByCustomerIdByTicketId(Integer cId, Integer eId,Integer tId,String sessionId,ApplicationPlatForm platForm);
	
}
