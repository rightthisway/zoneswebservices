package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.EventArtist;
 
/**
 * interface having method related to EventArtist
 * @author Ulaganathan
 *
 */
public interface EventArtistDAO extends RootDAO<Integer, EventArtist>{
	/**
	 * method to get List of child category  
	 @return List of ChildCategory 
	 */
	public List<EventArtist> getAll();
	
}
