package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.enums.HomeCardType;
import com.zonesws.webservices.enums.LoyalFanType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.utils.list.VenueResult;

public interface EventDAO extends RootDAO<Integer, Event> {
	List<Event> getAllEvents(Integer eventId,String name,Date startDate,Date endDate,Integer tourId,Integer venueId,String location,String city,String state);
	List<Event> getAllAwayEvents(Integer tourId);
	/**
	 * method to get Event by TMAT event Id 
	 * @param eventId , TMAT event Id
	 * @return List of Events
	 */
	public Event getEventByTMATEventId(Integer eventId);
	public List<Event> getEventsForArtistPriceRange(Integer artistId,Date startDate,Date endDate);
	
	public List<Event> getAllEventsByHomeCardType(HomeCardType homeCardType,Date startDate,Date endDate,String state,Integer pageNumber,Integer maxRows,
			Set<Integer> skipEventIds);
	public HashMap<String, Object> getAllEventsBySearchKey(Boolean showAllEvents, String searchKey,Integer eventPageNumber , Integer maxRows,String startDate,String endDate,
			Integer artistId,Integer grandChildId,Integer venueId,Integer childId,String searchType,Integer artistPageNo,Integer venuePageNo,
			String searchMode,Integer parentCatId,String zipCodeStr,String stateShortName,String artistRefValue,ArtistReferenceType artistRefType);
	
	public HashMap<String, Object> getAllEventsByParentIds(String searchKey,Integer eventPageNumber , Integer maxRows,String startDate,String endDate,
			Integer artistId,Integer grandChildId,Integer venueId,Integer childId,String searchType,Integer artistPageNo,Integer venuePageNo,
			String searchMode,Integer parentCatId,String zipCodeStr,String stateShortName,String artistRefValue,ArtistReferenceType artistRefType);
			
	public HashMap<String, Object> getAllEventsBySearchKey(String searchKey,Integer eventPageNumber ,Integer maxRows,
			String startDate,String endDate,String searchType,Integer artistPageNo,Integer venuePageNo,
			String searchMode,String stateShortName,Integer excludeEventDays,String artistRefValue,ArtistReferenceType artistRefType);
	
	public HashMap<String, Object> getAutoSearchResult(String searchKey,Integer eventPageNumber ,Integer maxRows);
	public HashMap<String, Object> getAutoSearchResultNew(String searchKey,Integer eventPageNumber ,Integer maxRows);

	public HashMap<String, Object> getAllEventsByArtistIdOrVenueId(Integer eventPageNumber ,Integer maxRows,
			String startDate,String endDate,Integer artistId,Integer venueId,String searchType,String searchMode,String stateShortName,
			Integer excludeEventDays,String artistRefValue,ArtistReferenceType artistRefType);
	
	public List<Event> getAllEventsByParentCategoryName(String parentCategoryName,Integer pageNumber , Integer maxRows,Date startDate,Date endDate);
	
	//public List<Event> getUpcomingEvents(Integer pageNumber , Integer maxRows,Date startDate, Date endDate);
	public List<Event> getUpcomingEvents();
	public List<Event> getPopularEvents(Integer pageNumber , Integer maxRows,Date startDate,Date endDate,String state);
	public List<Event> getAllFavoriteEvents(Integer customerId , Integer pageNumber , Integer maxRows);
	public void deleteZoneEvents(List<Integer> eventIds);
	public List<VenueResult> getVenuesByPageNumber(Integer pageNumber, Integer maxRows,String state);
	public Event getEventByEventId(Integer eventId,ProductType productType);
	
	public List<Event> getNewlyCreatedEventByArtistId(Integer artistId,Date currentDate,Date lastRunTime );
	
	/**
	 * Method to get the list of fav events for customer
	 * @param customerId
	 * @return
	 */
	List<Event> getFavoriteEventForCustomer(Integer customerId, Integer pageNumber, Integer maxRows,List<Integer> unFavoritedEventIds
			,Date startDate,Date endDate,String state);
	
    List<Event> getEventsByArtistCity(Integer artistId,String city)throws Exception ;
    
    List<Event> getActiveEventsForArtist(Integer artistId, Integer customerId, Integer pageNumber, Integer maxRows) throws Exception;
    
    List<Event> getCustomerSuperFanEventDetails(Integer artistId, Integer customerId, Integer pageNumber, Integer maxRows) throws Exception;
    public Map<Integer, List<Event>> getNewlyCreatedEvents(String fromDate,String toDate) throws Exception ;
    public Event getFantasyEventByEventId(Integer eventId) throws Exception;
    public List<Event> getAllFantasyEvents() throws Exception;
    public Event getEventById(Integer eventId,ProductType productType) ;
    public List<Event> getLoyalFanEventDetailsByState(String loyalFanName, Integer customerId, Integer pageNumber, Integer maxRows,LoyalFanType fanType)
	throws Exception;
    public List<Event> getEventsByArtistId(Integer artistId, Integer pageNumber, Integer maxRows)
	throws Exception;
    public List<Event> getAllEventsByContestId(Integer contestId ,Integer pageNumber , Integer maxRows,Date startDate,Date endDate, String state,
    		String searchKey, String zones,Double singleTixPrice,Integer requiredQty);
    
    public List<Event> getAllEventsByContestIdOptimized(Integer contestId ,Integer pageNumber , Integer maxRows,Date startDate,Date endDate, String state,
    		String searchKey, String zones,Double singleTixPrice,Integer requiredQty);
    
    public Boolean callContestGrandWinnerProcedure(Integer contestId,Integer insertOrDelete );
    
    public List<Event> getAllContestEventsByCriteria(Integer contestId ,Integer pageNumber , Integer maxRows,Date startDate,Date endDate, String state, String searchKey);
    public Integer getContestEventsCount(Integer contestId);
}
