package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.Exposure;

public interface ExposureDAO extends RootDAO<Integer, Exposure>{

}
