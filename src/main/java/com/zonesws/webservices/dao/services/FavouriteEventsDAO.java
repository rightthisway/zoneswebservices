package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.CustomerFavouriteEvent;
import com.zonesws.webservices.data.FavouriteEvents;



public interface FavouriteEventsDAO extends RootDAO<Integer, FavouriteEvents>{

	//public boolean saveOrUpdate(FavouriteEvents event);
	
	//public FavouriteEvents getFavouriteEventsById(Integer eventId);
	
	public FavouriteEvents getFavouriteEventsById(Integer eventId,Integer customerId);
	
	public List<FavouriteEvents> getAllFavouriteEventsByCustomerId(Integer customerId);
	
	public FavouriteEvents getActiveFavouriteEventsById(Integer eventId,Integer customerId);
	
	public List<FavouriteEvents> getAllActiveFavouriteEventsByCustomerId(Integer customerId);
	
	public List<FavouriteEvents> getAllActiveFavouriteEvents();
	
	public List<FavouriteEvents> getAllUnFavouritedEventsByCustomerId(Integer customerId);
}
