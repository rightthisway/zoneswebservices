package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.GiftCardBrand;

public interface GiftCardBrandDAO  extends RootDAO<Integer, GiftCardBrand>{
	
	public List<GiftCardBrand> getAllActiveGiftCardBrands();
	//public List<GiftCardBrand> getAllGiftCardBrands() ;

}
