package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.GiftCardFileAttachment;
import com.zonesws.webservices.enums.FileType;



public interface GiftCardFileAttachmentDAO extends RootDAO<Integer, GiftCardFileAttachment>{
	public List<GiftCardFileAttachment> getRealTicketsByInvoiceId(Integer orderId);
	public GiftCardFileAttachment getAttachmentByInvoiceAndFileType(Integer orderId,FileType fileType);
	public List<GiftCardFileAttachment> getAllAttachemntByOrderId(Integer orderId);
	
	
}