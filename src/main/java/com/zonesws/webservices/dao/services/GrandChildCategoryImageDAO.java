package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.GrandChildCategoryImage;

public interface GrandChildCategoryImageDAO extends RootDAO<Integer, GrandChildCategoryImage>{
	
	public GrandChildCategoryImage getGrandChildCategoryImageByGrandChildId(Integer grandChildId) throws Exception;
}
