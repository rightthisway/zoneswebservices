package com.zonesws.webservices.dao.services;

import java.util.Collection;

import com.zonesws.webservices.data.HoldTicket;
/**
 *  interface havingdb related methods for ZPSessionCart (user session cart) 
 * @author Ulaganathan
 *
 */
public interface HoldTicketDAO extends RootDAO<Integer, HoldTicket> {
	
	public HoldTicket getHoldTicketByGroupId(Integer ticketGroupId);
	public Collection<HoldTicket> getActiveHoldTickets();
	public Collection<HoldTicket> getActiveHoldTicketsByConfigId(String configId);
}
