package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.Invoice;


public interface InvoiceDAO extends RootDAO<Integer, Invoice>{

	int updateRealTixStatus(int invoiceId) throws Exception;

	int updateTixUploaded(int invoiceId) throws Exception;

	int updateBarCode(String barCode, int invoiceId) throws Exception;
	
	Invoice getInvoiceByCustomerOrder(Integer customerOrderId, Integer customerId);
	
	public Invoice getInvoiceByOrderId(Integer orderId)throws Exception;

}
