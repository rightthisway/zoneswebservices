package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.enums.FileType;



public interface InvoiceTicketAttachmentDAO extends RootDAO<Integer, InvoiceTicketAttachment>{
	public List<InvoiceTicketAttachment> getRealTicketsByInvoiceId(Integer invoiceId);
	public InvoiceTicketAttachment getAttachmentByInvoiceAndFileType(Integer invoiceId,FileType fileType);
	public List<InvoiceTicketAttachment> getTicketAttachmentByInvoiceId(Integer invoiceId);
	
	
}