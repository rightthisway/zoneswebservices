package com.zonesws.webservices.dao.services;

import java.util.Collection;

import com.zonesws.webservices.data.IpConfig;


public interface IpConfigDAO extends RootDAO<Integer, IpConfig>{
		IpConfig getIpConfigByIpAndConfigId(String configId,String ip); 
		Collection<IpConfig> ipConfigList();
}
