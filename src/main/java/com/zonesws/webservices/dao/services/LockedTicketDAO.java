package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.LockedTicket;
import com.zonesws.webservices.enums.LockedTicketStatus;

public interface LockedTicketDAO extends RootDAO<Integer, LockedTicket>{
	public List<LockedTicket> getLockedTicketByItemId(String  itemId);
	public List<LockedTicket> getLockedTicketByLockedTickettatus(LockedTicketStatus lockedTicketStatus);
	public List<LockedTicket> getLockedTicketByCartId(Integer cartId);
	public LockedTicket getLockedTicketByZPCartId(Integer cartId);
	public List<LockedTicket> getLockedTicketByEventId(Integer eventID);
	
	public List<LockedTicket> getAllLockedTicket ();
	public void deleteByCartId (Integer cartId);
	
	//public List<LockedTicket> getLockedTicketByHoldTicketId(Integer holdTicketId);
	public LockedTicket getLockedTicketByTicketGroupId(Integer ticketGroupId);
	public List<LockedTicket> getLockedTicketBySessionId(String sessionId);
	 
}
