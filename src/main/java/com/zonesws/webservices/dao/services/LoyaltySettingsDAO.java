package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.LoyaltySettings;
/**
 * interface having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public interface LoyaltySettingsDAO extends RootDAO<Integer, LoyaltySettings>{
	
	
	public LoyaltySettings getActivetLoyaltySettings();
	
	
}
