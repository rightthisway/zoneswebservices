package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.OrderItem;

public interface OrderItemDAO extends RootDAO<Integer, OrderItem> {

	List<OrderItem> getOrderDetailsByUserOrdrId(List<Integer> userOrderIds);
	
	OrderItem getOrderItemByOrderId(Integer orderId);

}
