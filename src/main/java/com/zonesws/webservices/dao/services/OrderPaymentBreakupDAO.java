package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.OrderPaymentBreakup;


public interface OrderPaymentBreakupDAO extends RootDAO<Integer, OrderPaymentBreakup>{

	 public List<OrderPaymentBreakup> getPaymentBreaksByOrderId(Integer orderId);
}
