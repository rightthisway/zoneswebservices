package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.OrderTicketGroup;

public interface OrderTicketGroupDAO extends RootDAO<Integer, OrderTicketGroup> {
	
	public void updateOrderTicketGroupDetails(Integer orderId);
	public List<OrderTicketGroup> getAllTicketGroupsByOrderId(Integer orderId);
}
