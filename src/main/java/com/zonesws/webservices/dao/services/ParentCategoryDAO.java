package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.ParentCategory;
/**
 * interface having db related methods for parent category of a tour 
 * @author hamin
 *
 */ 
public interface ParentCategoryDAO extends RootDAO<Integer, ParentCategory>{
	/**
	 * method to get all parent categories
	 * @return list of parent categories
	 */
	public List<ParentCategory> getAll();
	/**
	 * method to get parent category  by its name
	 * @param name, its name
	 * @return  parent category
	 */
	public ParentCategory getParentCategoryByName(String name);
	/**
	 * method to get parent category  by its id
	 * @param id, own id
	 * @return  parent category
	 */
	public ParentCategory getParentCategoryById(Integer id);	
	
	public List<String> getDistinctParentCategoryNmaes();	

}
