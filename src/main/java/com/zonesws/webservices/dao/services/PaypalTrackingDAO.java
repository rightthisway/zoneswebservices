package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.PaypalTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public interface PaypalTrackingDAO extends RootDAO<Integer, PaypalTracking>{

	public PaypalTracking getTrackingByCustomerIdByTicketId(Integer cId, Integer eId,Integer tId,String sessionId,ApplicationPlatForm platForm);
	
}
