package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.PaypalTransactionDetail;

public interface PaypalTransactionDetailDAO extends RootDAO<Integer, PaypalTransactionDetail>{

	public PaypalTransactionDetail getCustomerOrderDetailByOrderId(Integer orderId);
	
}
