package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.PollingCustomerRewards;
/**
 * interface having db related methods for PollingCustomerRewards
 *
 */
public interface PollingCustomerRewardsDAO extends RootDAO<Integer, PollingCustomerRewards>{
	
	
	public List<PollingCustomerRewards> getAllCustomerWithMagicWands();
	public PollingCustomerRewards getCustomerMagicWandStat(Integer customerId);
	
}
