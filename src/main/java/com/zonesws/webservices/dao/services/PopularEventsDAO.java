package com.zonesws.webservices.dao.services;

import java.util.List;
import java.util.Map;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.PopularEvents;
/**
 * interface having db related methods for PopularEvents
 * @author Ulaganathan
 *
 */
public interface PopularEventsDAO extends RootDAO<Integer, PopularEvents>{
	
	
	public PopularEvents getActivetPopularEvents();
	public List<Artist> getAllPopularArtists(Map<Integer, Boolean> favArtistMap);
	public List<Artist> getAllPopularArtistBySalesCount(Map<Integer, Boolean> favArtistMap);
	public List<Artist> getAllPopularArtistsRevised(Map<Integer, Boolean> favArtistMap);
}
