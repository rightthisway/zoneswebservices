package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.PreOrderPageAudit;
import com.zonesws.webservices.enums.ApplicationPlatForm;

public interface PreOrderPageAuditDAO extends RootDAO<Integer, PreOrderPageAudit>{

	public PreOrderPageAudit getAudit(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId);
	
	public List<PreOrderPageAudit> getAllPendingData();
	
	public void deleteNotEligibleTracking(Integer customerId);
	
}
