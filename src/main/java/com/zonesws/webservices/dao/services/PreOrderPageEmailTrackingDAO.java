package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.PreOrderPageEmailTracking;

public interface PreOrderPageEmailTrackingDAO extends RootDAO<Integer, PreOrderPageEmailTracking>{

	public List<PreOrderPageEmailTracking> getAllCustomerTrackingRecords(Integer customerId , Date fromDate, Date toDate);
	public List<PreOrderPageEmailTracking> getAllCustomerTrackingRecords(Integer customerId);
}
