package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.PresaleZoneTicketGroup;
/**
 * interface having db related methods for PresaleZoneTicketGroup
 * @author Ulaganathan
 *
 */
public interface PresaleZoneTicketGroupDAO extends RootDAO<Integer, PresaleZoneTicketGroup>{
	
	
	public PresaleZoneTicketGroup getActivetTicketGroup(Integer id);
	
	public List<PresaleZoneTicketGroup> getActivetTicketGroupsByEventId(Integer eventId);
	
	public List<PresaleZoneTicketGroup> getAllTicketsByTmatEventId(Integer tmatEventId);
	
	public List<Integer> getAllSoldTickets(Date fromDate,Date toDate);
	
}
