package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.QuizAffiliateRewardHistory;
/**
 * interface having db related methods for QuizAffiliateRewardHistory
 * @author Ulaganathan
 *
 */
public interface QuizAffiliateRewardHistoryDAO extends RootDAO<Integer, QuizAffiliateRewardHistory>{
	
	public List<QuizAffiliateRewardHistory> getAllRewardsBreak(Integer customerId);
}
