package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.QuizAffiliateSetting;
/**
 * interface having db related methods for QuizAffiliateSetting
 * @author Ulaganathan
 *
 */
public interface QuizAffiliateSettingDAO extends RootDAO<Integer, QuizAffiliateSetting>{
	
	public QuizAffiliateSetting getActiveAffiliateByCustomerId(Integer customerId);
	public QuizAffiliateSetting getAffiliate(Integer customerId);
	public List<QuizAffiliateSetting> getAllActiveAffiliates();
}
