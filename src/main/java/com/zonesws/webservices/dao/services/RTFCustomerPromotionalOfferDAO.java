package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;

public interface RTFCustomerPromotionalOfferDAO extends RootDAO<Integer, RTFCustomerPromotionalOffer>{

	public RTFCustomerPromotionalOffer getPromoOfferCustomerId(Integer customerId);
	public void updatePromotionaOfferOrderCount(Integer offerId);
}
