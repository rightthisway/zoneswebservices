package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.RTFOrderTracking;

public interface RTFOrderTrackingDAO extends RootDAO<Integer, RTFOrderTracking>{

}
