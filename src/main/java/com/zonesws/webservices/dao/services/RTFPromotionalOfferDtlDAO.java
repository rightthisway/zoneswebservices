package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.RTFPromotionalOfferDtl;

public interface RTFPromotionalOfferDtlDAO extends RootDAO<Integer, RTFPromotionalOfferDtl>{

	public List<RTFPromotionalOfferDtl> getPromoDtlbyPromoOfferId(Integer promoOfferId);
}
