package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.RTFPromotionalOfferHdr;

public interface RTFPromotionalOfferHdrDAO extends RootDAO<Integer, RTFPromotionalOfferHdr>{

	public List<RTFPromotionalOfferHdr> getAllActivePromotionalOffers();
	public RTFPromotionalOfferHdr getActivePromotionalOfferByCode(String promoCode);
	public void updatePromotionaOfferOrderCount(Integer offerId);
}
