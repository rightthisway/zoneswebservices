package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.PromotionalType;
/**
 * interface having db related methods for RTFPromotionalOfferTracking
 * @author Ulaganathan
 *
 */
public interface RTFPromotionalOfferTrackingDAO extends RootDAO<Integer, RTFPromotionalOfferTracking>{
	
	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatForm platForm,Integer promoOfferId,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId,String promoCode,Boolean isLongTicket,PromotionalType offerType);
	
	public RTFPromotionalOfferTracking getPromoTrackingByTrackingId(ApplicationPlatForm platForm,Integer customerId, String sessionId, Integer trackingId);
	
	public RTFPromotionalOfferTracking getPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId);
	
	public RTFPromotionalOfferTracking getPendingPromoTracking(ApplicationPlatForm platForm,Integer customerId, String sessionId,
			Integer ticketGroupId, Integer eventId);
	
	public List<RTFPromotionalOfferTracking> getAllPendingPromoTracking();
	
	public void updateOfferTrackingDiscount(Integer offerId, Double discConv, Double discAmt, Double discountedPrice);
	
	public List<RTFPromotionalOfferTracking> getAllCompletedReferralOrders(Date fromDate, Date toDate);
}

