package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.ReferralContest;

public interface ReferralContestDAO extends RootDAO<Integer, ReferralContest>{

	public List<ReferralContest> getAllActiveContest();
}
