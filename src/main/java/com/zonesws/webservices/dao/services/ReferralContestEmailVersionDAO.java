package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.ReferralContestEmailVersion;

public interface ReferralContestEmailVersionDAO extends RootDAO<Integer, ReferralContestEmailVersion>{

	public ReferralContestEmailVersion getContestDetailsByContetKey(String key);
	
	public List<ReferralContestEmailVersion> getAllContestByOrderIdandParticipantId(Integer orderId, Integer participantCustId);
	
	public ReferralContestEmailVersion getContest(Integer contestId, Integer participantCustId, Integer purchaserId);
}
