package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.ReferralContestParticipants;

public interface ReferralContestParticipantsDAO extends RootDAO<Integer, ReferralContestParticipants>{
	
	public ReferralContestParticipants getContestParticipantByCustomerId(Integer contestId, Integer participantCustId, Integer purchaserCustId);
	public ReferralContestParticipants getContestParticipantByCustomerId(Integer participantCustId, Integer purchaserCustId);
	public List<ReferralContestParticipants> getAllContestParticipantsToSendMail();
}
