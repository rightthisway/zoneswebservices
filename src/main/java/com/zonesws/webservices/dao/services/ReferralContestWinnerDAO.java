package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.ReferralContestWinner;

public interface ReferralContestWinnerDAO extends RootDAO<Integer, ReferralContestWinner>{

	public ReferralContestWinner getWinnerByContestId(Integer contestId);
}
