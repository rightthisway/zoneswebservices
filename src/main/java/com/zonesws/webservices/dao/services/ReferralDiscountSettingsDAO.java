package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.ReferralDiscountSettings;
import com.zonesws.webservices.enums.ApplicationPlatForm;
/**
 * interface having db related methods for ReferralDiscountSettings
 * @author Ulaganathan
 *
 */
public interface ReferralDiscountSettingsDAO extends RootDAO<Integer, ReferralDiscountSettings>{
	
	public ReferralDiscountSettings getActiveDiscountSettingsByPlatForm(ApplicationPlatForm platForm);
	
}
