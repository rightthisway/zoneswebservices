package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.ReferredCodeTracking;


public interface ReferredCodeTrackingDAO extends RootDAO<Integer, ReferredCodeTracking>{

	ReferredCodeTracking getReferralCode(String referralCode);
	public ReferredCodeTracking getTrackingInfoByReferralCodeAndReferrarId(String referralCode,Integer referrerId);
}
