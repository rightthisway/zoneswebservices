package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.ResetPasswordToken;
/**
 * interface having db related methods for CustomerFavouriteEvent
 * @author Ulaganathan
 *
 */
public interface ResetPasswordTokenDAO extends RootDAO<Integer, ResetPasswordToken>{
	
	public ResetPasswordToken getResetPasswordTokenByToken(String token);
	
}
