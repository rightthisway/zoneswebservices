package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.RtfCatTicket;

public interface RtfCatTicketDAO extends RootDAO<Integer, RtfCatTicket> {

	public List<RtfCatTicket> getActiveTickets(Integer eventId);
	public RtfCatTicket getTicket(Integer tixId);
}
