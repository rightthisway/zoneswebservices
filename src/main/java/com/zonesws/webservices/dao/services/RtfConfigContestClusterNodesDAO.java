package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.RtfConfigContestClusterNodes;

public interface RtfConfigContestClusterNodesDAO extends RootDAO<Integer, RtfConfigContestClusterNodes>{

	public List<RtfConfigContestClusterNodes> getAllActiveRtfConfigContestClusterNodes()  throws Exception;
	
}
