package com.zonesws.webservices.dao.services;

import java.util.Collection;
import java.util.List;

import com.zonesws.webservices.data.RtfGiftCardOrder;

public interface RtfGiftCardOrderDAO extends RootDAO<Integer, RtfGiftCardOrder>{

	public List<RtfGiftCardOrder> getAllOrdersByCustomerId(Integer customerId);
	public Collection<RtfGiftCardOrder> getCustomerOrderToSendEmail() throws Exception;
	public void updateOrderEmailSent(Integer orderId);
	public Collection<RtfGiftCardOrder> getFilledOrdersToSendEmail() throws Exception;
	public void updateOrderFilledEmailSent(Integer orderId);
}
