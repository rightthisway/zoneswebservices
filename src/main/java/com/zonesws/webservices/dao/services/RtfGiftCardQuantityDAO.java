package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.RtfGiftCardQuantity;

public interface RtfGiftCardQuantityDAO extends RootDAO<Integer, RtfGiftCardQuantity>{

	public RtfGiftCardQuantity getGiftCardQuantityByCardId(Integer cardId);
}
