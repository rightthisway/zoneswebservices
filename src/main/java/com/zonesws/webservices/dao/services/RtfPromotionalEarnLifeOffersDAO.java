package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.RtfPromotionalEarnLifeOffers;
/**
 * interface having db related methods for Customer
 *
 */
public interface RtfPromotionalEarnLifeOffersDAO  extends RootDAO<Integer, RtfPromotionalEarnLifeOffers>{
	
	public RtfPromotionalEarnLifeOffers getRtfPromotionEarnLifeOfferByPromoCode(String promoCode);
	public RtfPromotionalEarnLifeOffers getActiveRtfPromotionEarnLifeOfferByPromoCode(String promoCode);
}



