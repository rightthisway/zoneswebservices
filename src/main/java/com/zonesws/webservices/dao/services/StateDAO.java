package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.State;
/**
 * interface having db related methods for State (used in address )
 * @author hamin
 *
 */
public interface StateDAO extends RootDAO<Integer, State>{
	/**
	 *  method to get State by country id
	 * @param countryId, Id of a country
	 * @return list of states
	having db related methods */
	public List<State> getAllStateByCountryId(Integer countryId);
	/**
	 *  method to get State by  id
	 * @param id, own id
	 * @return State
	 */
	public State getStateById(Integer id);
	
	public State getStateByCountryIdAndStateName(Integer countryId,String stateName);
	
	public List<State> getAllStateBySearchKey(String stateName);
	
	public List<State> getAllStateBySearchKeyAutoSearch(String stateName);
}
