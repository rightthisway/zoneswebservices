package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.Ticket;
import com.zonesws.webservices.utils.list.TicketList;

public interface TicketDAO extends RootDAO<Integer, Ticket> {
	public TicketList getTicketListByEventId(Integer id);
	public Ticket getTicketByEventIdAndEbayGroupId(Integer eventId,Integer ticketGroupId);
	public Ticket getActiveTicketByItemId(String itemId);
	Ticket getActiveMXPTicketByEventIdAndZone(Integer eventId,String zone);
	
}
