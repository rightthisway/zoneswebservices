package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.TicketTraking;

public interface TicketTrackingDAO extends RootDAO<Integer, TicketTraking> {

	public List<TicketTraking> searchTrackingRecords(Date startDate,Date endDate,Integer companyId);
	
	public void processOrder(String orderIDS);
	
	public void unProcessOrder(String orderIDS);
	
	public void lockOrder(String orderIDS);
}
