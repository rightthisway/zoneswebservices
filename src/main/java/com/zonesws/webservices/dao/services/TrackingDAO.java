package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.Tracking;

public interface TrackingDAO extends RootDAO<Integer, Tracking> {

	public  Tracking getTrackingBySessionId(String sessionId);
	public Tracking getTrackingBySessionIdAndArtistId(String sessionId, Integer artistId);
	public List<Tracking> getCustomerOrdersById(Integer customerId);
	
}
