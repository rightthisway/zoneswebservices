package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.enums.AddressType;


/**
 * interface having having db related methods for user address (used for billing , mailing purpose )
 * @author hamin
 *
 */
public interface UserAddressDAO extends RootDAO<Integer, UserAddress>{

	public List<UserAddress> getUserAddressInfo(Integer customerId,AddressType addressType);
	
	public List<UserAddress> getShippingAddressInfo(Integer customerId);
	
	public Integer getShippingAddressCount(Integer customerId);
	
	public List<UserAddress> getUserAddressByCustomerId(Integer customerId);
	
	public UserAddress getUserAddressByIdandAddressType(Integer customerId,Integer addressId,AddressType addressType);
	
	public UserAddress getUserAddressInfo(Integer customerId);
	
	public void deleteCustomerExistingAddresses(Integer customerId);
	
	public List<UserAddress> getAllShippingAddresByOrder(Integer customerId);

}
