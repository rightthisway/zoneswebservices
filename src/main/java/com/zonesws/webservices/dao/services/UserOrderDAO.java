package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.UserOrder;
/**
 * interface having db related methods for user order
 * @author hamin
 *
 */
public interface UserOrderDAO extends RootDAO<Integer, UserOrder>{

	List<UserOrder> getOrdersByCustomerId(Integer customerId);
	public List<UserOrder> getPendingOrders(Date toDate);

	

}
