package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.dao.services.RootDAO;
import com.zonesws.webservices.data.UserPreference;

public interface UserPreferenceDAO extends RootDAO<Integer, UserPreference>{

	public UserPreference getUserPreferenceByUsername(String username);
	public UserPreference getUserPreferenceByIPAddress(String ipAddress);
}
