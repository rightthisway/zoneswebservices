package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.dao.services.RootDAO;
import com.zonesws.webservices.data.UserType;

public interface UserTypeDAO extends RootDAO<Integer, UserType>{

}
