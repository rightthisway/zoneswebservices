package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.Venue;
import com.zonesws.webservices.utils.list.VenueResult;

public interface VenueDAO extends RootDAO<Integer, Venue> {

	public  List<VenueResult> getAllVenuesBySearchKeyAndParentId(String searchKey,Integer pageNumber,Integer maxRows,Integer parentCategoryId) ;
}
