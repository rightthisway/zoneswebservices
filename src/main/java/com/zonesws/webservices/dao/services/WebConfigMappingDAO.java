package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.WebConfigMapping;
 

public interface WebConfigMappingDAO  extends RootDAO<Integer, WebConfigMapping> {
	
	public WebConfigMapping getWebConfigByCompanyId(Integer companyId);
	public WebConfigMapping getWebConfigByWebConfigId(Integer webConfigId);
}
