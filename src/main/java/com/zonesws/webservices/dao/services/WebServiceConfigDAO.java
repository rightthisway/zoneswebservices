package com.zonesws.webservices.dao.services;



import java.util.Collection;

import com.zonesws.webservices.data.WebServiceConfig;
import com.zonesws.webservices.enums.ApplicationPlatForm;



public interface WebServiceConfigDAO extends RootDAO<Integer, WebServiceConfig>{
	
	public WebServiceConfig getWebServiceConfgiByConfigId(String configId);
	Collection<WebServiceConfig> getWebServiceConfig();
	public WebServiceConfig getWebServiceConfgiByConfigId(String configId,ApplicationPlatForm platForm);
}
