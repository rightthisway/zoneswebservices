package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.WebServiceTrackingCassandra;

public interface WebServiceTrackingCassandraDAO extends RootDAO<Integer, WebServiceTrackingCassandra>{
	
}
