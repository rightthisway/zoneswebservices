package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.ZPSessionCart;
/**
 *  interface havingdb related methods for ZPSessionCart (user session cart) 
 * @author Ulaganathan
 *
 */
public interface ZPSessionCartDAO extends RootDAO<Integer, ZPSessionCart> {
	/**
	 * method to get ZPSessionCart  by  session Id
	 * @param sessionId, http session Id
	 * @return
	 */
	public ZPSessionCart getZPSessionBySessionId(String sessionId);
	
}
