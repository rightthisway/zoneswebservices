package com.zonesws.webservices.dao.services;

import java.util.Collection;

import com.zonesws.webservices.data.ZoneRGBColor;
/**
 * interface having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public interface ZoneRGBColorDAO extends RootDAO<Integer, ZoneRGBColor>{
	
	public Collection<ZoneRGBColor> getZoneRGBColorDetailsByZones(Collection<String> zones);
}
