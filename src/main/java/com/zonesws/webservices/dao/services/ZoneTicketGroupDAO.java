package com.zonesws.webservices.dao.services;

import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.ZoneTicketGroup;
/**
 * interface having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public interface ZoneTicketGroupDAO extends RootDAO<Integer, ZoneTicketGroup>{
	
	
	public ZoneTicketGroup getActivetTicketGroup(Integer id);
	
	public List<ZoneTicketGroup> getActivetTicketGroupsByEventId(Integer eventId);
	
	public List<ZoneTicketGroup> getAllTicketsByTmatEventId(Integer tmatEventId);
	
	public List<Integer> getAllSoldTickets(Date fromDate,Date toDate);
	
}
