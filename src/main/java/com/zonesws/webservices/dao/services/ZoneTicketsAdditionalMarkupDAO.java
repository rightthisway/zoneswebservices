package com.zonesws.webservices.dao.services;

import java.util.List;

import com.zonesws.webservices.data.ZoneTicketsAdditionalMarkup;
/**
 * interface having db related methods for LoyaltySettings
 * @author Ulaganathan
 *
 */
public interface ZoneTicketsAdditionalMarkupDAO extends RootDAO<Integer, ZoneTicketsAdditionalMarkup>{
	
	public List<ZoneTicketsAdditionalMarkup> getZoneTicketsAdditionalMarkupsByEventId(Integer eventId);
	public ZoneTicketsAdditionalMarkup getZoneTicketsAdditionalMarkupsByEventIdandZone(Integer eventId,String zone);
}
