package com.zonesws.webservices.dao.services;



import com.zonesws.webservices.data.ZonesEventCategoryMapping;



public interface ZonesEventCategoryMappingDAO extends RootDAO<Integer, ZonesEventCategoryMapping>{
	ZonesEventCategoryMapping getZonesEventCategoryMappingByEventId(Integer eventId);
}
