package com.zonesws.webservices.dao.services;

import com.zonesws.webservices.data.ZonesExposureProperty;

public interface ZonesExposurePropertyDAO {
	
	ZonesExposureProperty getZonesExposurePropertyByEventIdAndEbayGroupId(Integer eventId, Integer ticketGroupId);
}
