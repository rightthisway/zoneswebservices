package com.zonesws.webservices.data;

import java.io.Serializable;

import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.utils.TextUtil;

public class AddressInfo implements Serializable{
	

	private String bFirstName;
	private String bLastName;
	private String bAddress1;
	private String bAddress2;
	private String bCity;
	private String bState;
	private String bCountry;
	private String bZipCode;
	private String bPhoneNumber;
	private String bEmail;
	private String sFirstName;
	private String sLastName;
	private String sAddress1;
	private String sAddress2;
	private String sCity;
	private String sState;
	private String sCountry;
	private String sZipCode;
	private String sPhoneNumber;
	private String sEmail;
	private PaymentMethod paymentMethod;
	private UserAddress billingAddress;
	private UserAddress shippingAddress;
	
	
	
	public AddressInfo setValues(String bFirstName,String bLastName,String bAddress1,String bAddress2,String bCity,String bState,String bCountry,String bZipCode,String bPhoneNumber,
			String bEmail,String sFirstName,String sLastName,String sAddress1,String sAddress2,String sCity,String sState,String sCountry,String sZipCode,String sPhoneNumber,String sEmail){
		
		setbFirstName(TextUtil.isEmptyOrNull(bFirstName)?null:bFirstName);
		setbLastName(TextUtil.isEmptyOrNull(bLastName)?null:bLastName);
		setbAddress1(TextUtil.isEmptyOrNull(bAddress1)?null:bAddress1);
		setbAddress2(TextUtil.isEmptyOrNull(bAddress2)?null:bAddress2);
		setbCity(TextUtil.isEmptyOrNull(bCity)?null:bCity);
		setbState(TextUtil.isEmptyOrNull(bState)?null:bState);
		setbCountry(TextUtil.isEmptyOrNull(bCountry)?null:bCountry);
		setbZipCode(TextUtil.isEmptyOrNull(bZipCode)?null:bZipCode);
		setbPhoneNumber(TextUtil.isEmptyOrNull(bPhoneNumber)?null:bPhoneNumber);
		setbEmail(TextUtil.isEmptyOrNull(bEmail)?null:bEmail);
		setsFirstName(TextUtil.isEmptyOrNull(sFirstName)?null:sFirstName);
		setsLastName(TextUtil.isEmptyOrNull(sLastName)?null:sLastName);
		setsAddress1(TextUtil.isEmptyOrNull(sAddress1)?null:sAddress1);
		setsAddress2(TextUtil.isEmptyOrNull(sAddress2)?null:sAddress2);
		setsCity(TextUtil.isEmptyOrNull(sCity)?null:sCity);
		setsState(TextUtil.isEmptyOrNull(sState)?null:sState);
		setsCountry(TextUtil.isEmptyOrNull(sCountry)?null:sCountry);
		setsZipCode(TextUtil.isEmptyOrNull(sZipCode)?null:sZipCode);
		setsPhoneNumber(TextUtil.isEmptyOrNull(sPhoneNumber)?null:sPhoneNumber);
		setsEmail(TextUtil.isEmptyOrNull(sEmail)?null:sEmail);
		return this;
	}
	
	
	public String getbAddress1() {
		return bAddress1;
	}
	public void setbAddress1(String bAddress1) {
		this.bAddress1 = bAddress1;
	}
	public String getbAddress2() {
		return bAddress2;
	}
	public void setbAddress2(String bAddress2) {
		this.bAddress2 = bAddress2;
	}
	public String getbCity() {
		return bCity;
	}
	public void setbCity(String bCity) {
		this.bCity = bCity;
	}
	public String getbState() {
		return bState;
	}
	public void setbState(String bState) {
		this.bState = bState;
	}
	public String getbCountry() {
		return bCountry;
	}
	public void setbCountry(String bCountry) {
		this.bCountry = bCountry;
	}
	public String getbZipCode() {
		return bZipCode;
	}
	public void setbZipCode(String bZipCode) {
		this.bZipCode = bZipCode;
	}
	public String getsAddress1() {
		return sAddress1;
	}
	public void setsAddress1(String sAddress1) {
		this.sAddress1 = sAddress1;
	}
	public String getsAddress2() {
		return sAddress2;
	}
	public void setsAddress2(String sAddress2) {
		this.sAddress2 = sAddress2;
	}
	public String getsCity() {
		return sCity;
	}
	public void setsCity(String sCity) {
		this.sCity = sCity;
	}
	public String getsState() {
		return sState;
	}
	public void setsState(String sState) {
		this.sState = sState;
	}
	public String getsCountry() {
		return sCountry;
	}
	public void setsCountry(String sCountry) {
		this.sCountry = sCountry;
	}
	public String getsZipCode() {
		return sZipCode;
	}
	public void setsZipCode(String sZipCode) {
		this.sZipCode = sZipCode;
	}
	public UserAddress getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(UserAddress billingAddress) {
		this.billingAddress = billingAddress;
	}
	public UserAddress getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(UserAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}


	public String getbPhoneNumber() {
		return bPhoneNumber;
	}


	public void setbPhoneNumber(String bPhoneNumber) {
		this.bPhoneNumber = bPhoneNumber;
	}


	public String getbEmail() {
		return bEmail;
	}


	public void setbEmail(String bEmail) {
		this.bEmail = bEmail;
	}


	public String getsPhoneNumber() {
		return sPhoneNumber;
	}


	public void setsPhoneNumber(String sPhoneNumber) {
		this.sPhoneNumber = sPhoneNumber;
	}


	public String getsEmail() {
		return sEmail;
	}


	public void setsEmail(String sEmail) {
		this.sEmail = sEmail;
	}


	public String getbFirstName() {
		return bFirstName;
	}


	public void setbFirstName(String bFirstName) {
		this.bFirstName = bFirstName;
	}


	public String getbLastName() {
		return bLastName;
	}


	public void setbLastName(String bLastName) {
		this.bLastName = bLastName;
	}


	public String getsFirstName() {
		return sFirstName;
	}


	public void setsFirstName(String sFirstName) {
		this.sFirstName = sFirstName;
	}


	public String getsLastName() {
		return sLastName;
	}


	public void setsLastName(String sLastName) {
		this.sLastName = sLastName;
	}
	
}
