package com.zonesws.webservices.data;

import java.io.Serializable;

public class Affiliate implements Serializable{
	
	private Integer userId;
	private String firstName;
	private String lastName;
	private String email;
	private String promotionalCode;
	private String description;
	private Double affiliateCashReward;
	private Double customerOrderDiscount;
	private Double phoneOrderCashReward;
	private Double phoneCustomerDiscount;
	private Boolean affiliateRepeatBusiness;
	private Boolean isEarnRewardPoints;
	private Integer offerId;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPromotionalCode() {
		return promotionalCode;
	}
	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getAffiliateCashReward() {
		return affiliateCashReward;
	}
	public void setAffiliateCashReward(Double affiliateCashReward) {
		this.affiliateCashReward = affiliateCashReward;
	}
	public Double getCustomerOrderDiscount() {
		return customerOrderDiscount;
	}
	public void setCustomerOrderDiscount(Double customerOrderDiscount) {
		this.customerOrderDiscount = customerOrderDiscount;
	}
	public Boolean getAffiliateRepeatBusiness() {
		return affiliateRepeatBusiness;
	}
	public void setAffiliateRepeatBusiness(Boolean affiliateRepeatBusiness) {
		this.affiliateRepeatBusiness = affiliateRepeatBusiness;
	}
	public Integer getOfferId() {
		return offerId;
	}
	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}
	
	public Double getPhoneOrderCashReward() {
		return phoneOrderCashReward;
	}
	public void setPhoneOrderCashReward(Double phoneOrderCashReward) {
		this.phoneOrderCashReward = phoneOrderCashReward;
	}
	public Double getPhoneCustomerDiscount() {
		return phoneCustomerDiscount;
	}
	public void setPhoneCustomerDiscount(Double phoneCustomerDiscount) {
		this.phoneCustomerDiscount = phoneCustomerDiscount;
	}
	public Boolean getIsEarnRewardPoints() {
		return isEarnRewardPoints;
	}
	public void setIsEarnRewardPoints(Boolean isEarnRewardPoints) {
		this.isEarnRewardPoints = isEarnRewardPoints;
	}
	
	
	
}
