package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents AffiliateCashReward entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("AffiliateCashReward")
@Entity
@Table(name="affiliate_cash_reward")
public class AffiliateCashReward  implements Serializable{
	
	@JsonIgnore
	private Integer id;
	private Integer userId;	
	private Double activeCash;
	private Double pendingCash;
	private Double lastCreditedCash;
	private Double lastDebitedCash;
	private Double lastVoidCash;
	private Double totalCreditedCash;
	private Double totalDebitedCash;
	private Double totalVoidedCash;	
	@JsonIgnore
	private Date lastUpdate;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="user_id")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="active_cash")
	public Double getActiveCash() {
		return activeCash;
	}
	public void setActiveCash(Double activeCash) {
		this.activeCash = activeCash;
	}
	
	@Column(name="pending_cash")
	public Double getPendingCash() {
		return pendingCash;
	}
	public void setPendingCash(Double pendingCash) {
		this.pendingCash = pendingCash;
	}
	
	@Column(name="last_credited_cash")
	public Double getLastCreditedCash() {
		return lastCreditedCash;
	}
	public void setLastCreditedCash(Double lastCreditedCash) {
		this.lastCreditedCash = lastCreditedCash;
	}
	
	@Column(name="latest_debited_cash")
	public Double getLastDebitedCash() {
		return lastDebitedCash;
	}
	public void setLastDebitedCash(Double lastDebitedCash) {
		this.lastDebitedCash = lastDebitedCash;
	}
	
	@Column(name="last_voided_cash")
	public Double getLastVoidCash() {
		return lastVoidCash;
	}
	public void setLastVoidCash(Double lastVoidCash) {
		this.lastVoidCash = lastVoidCash;
	}
	
	@Column(name="total_credited_cash")
	public Double getTotalCreditedCash() {
		return totalCreditedCash;
	}
	public void setTotalCreditedCash(Double totalCreditedCash) {
		this.totalCreditedCash = totalCreditedCash;
	}
	
	@Column(name="total_debited_cash")
	public Double getTotalDebitedCash() {
		return totalDebitedCash;
	}
	public void setTotalDebitedCash(Double totalDebitedCash) {
		this.totalDebitedCash = totalDebitedCash;
	}
	
	@Column(name="total_voided_cash")
	public Double getTotalVoidedCash() {
		return totalVoidedCash;
	}
	public void setTotalVoidedCash(Double totalVoidedCash) {
		this.totalVoidedCash = totalVoidedCash;
	}
	
	@Column(name="updated_time")
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
}
