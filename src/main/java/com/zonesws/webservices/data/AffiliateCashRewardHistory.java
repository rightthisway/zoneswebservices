package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.RewardStatus;

/**
 * represents AffiliateCashRewardHistory entity
 * @author ulaganathan
 *
 */
@XStreamAlias("AffiliateCashRewardHistory")
@Entity
@Table(name="affiliate_cash_reward_history")
public class AffiliateCashRewardHistory  implements Serializable{
	
	private Integer id;
	private Integer userId;
	private Integer customerId;
	private Integer orderId;
	private Double orderTotal;
	private Double creditedCash;
	private Double creditConv;
	private Date createDate;
	private Date updatedDate;
	private RewardStatus rewardStatus;
	@JsonIgnore
	private Boolean isRewardsEmailSent;
	@JsonIgnore
	private String emailDescription;
	@JsonIgnore
	private String promoCode;
	
	@JsonIgnore
	private String eventDate;
	@JsonIgnore
	private String eventTime;
	
	@JsonIgnore
	private String affiliatePremiumStatus;
	
                                    
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="customer_order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public RewardStatus getRewardStatus() {
		return rewardStatus;
	}
	public void setRewardStatus(RewardStatus rewardStatus) {
		this.rewardStatus = rewardStatus;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="is_email_sent")
	public Boolean getIsRewardsEmailSent() {
		return isRewardsEmailSent;
	}
	public void setIsRewardsEmailSent(Boolean isRewardsEmailSent) {
		this.isRewardsEmailSent = isRewardsEmailSent;
	}
	
	@Column(name="email_description")
	public String getEmailDescription() {
		return emailDescription;
	}
	public void setEmailDescription(String emailDescription) {
		this.emailDescription = emailDescription;
	}
	
	@Column(name="user_id")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="credited_cash")
	public Double getCreditedCash() {
		return creditedCash;
	}
	public void setCreditedCash(Double creditedCash) {
		this.creditedCash = creditedCash;
	}
	
	@Column(name="credit_perc")
	public Double getCreditConv() {
		return creditConv;
	}
	public void setCreditConv(Double creditConv) {
		this.creditConv = creditConv;
	}
	
	@Transient
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	@Transient
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="promo_code")
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	@Transient
	public String getAffiliatePremiumStatus() {
		return affiliatePremiumStatus;
	}
	public void setAffiliatePremiumStatus(String affiliatePremiumStatus) {
		this.affiliatePremiumStatus = affiliatePremiumStatus;
	}
	
	
}
