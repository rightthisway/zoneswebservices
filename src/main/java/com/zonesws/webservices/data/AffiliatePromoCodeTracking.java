package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;

@Entity
@Table(name = "affiliate_promo_code_tracking_new")
public class AffiliatePromoCodeTracking implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="event_id")
	private Integer eventId;
	
	@Column(name="ticket_group_id")
	private Integer ticketGroupId;
	
	@Column(name="session_id")
	private String sessionId; 
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	private ApplicationPlatForm platForm;
	
	@Column(name="ip_address")
	private String ipAddress;	
	
	@Column(name="order_id")
	private Integer orderId;
	
	@Column(name="customer_reward_conv")
	private Double custRewardConv;
	
	@Column(name="customer_reward_points")
	private Double custRewardPoints;
	
	@Column(name="credited_cash")
	private Double creditedCash;
	
	@Column(name="credit_conv")
	private Double creditConv;
	
	@Column(name="promo_code")
	private String promoCode;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="status")
	private String status;
	
	@Column(name="updated_date")
	private Date updatedDate;
	
	@Column(name="is_long_ticket")
	private Boolean isLongTicket;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Double getCreditedCash() {
		return creditedCash;
	}

	public void setCreditedCash(Double creditedCash) {
		this.creditedCash = creditedCash;
	}

	public Double getCreditConv() {
		return creditConv;
	}

	public void setCreditConv(Double creditConv) {
		this.creditConv = creditConv;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}

	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Double getCustRewardConv() {
		return custRewardConv;
	}

	public void setCustRewardConv(Double custRewardConv) {
		this.custRewardConv = custRewardConv;
	}

	public Double getCustRewardPoints() {
		return custRewardPoints;
	}

	public void setCustRewardPoints(Double custRewardPoints) {
		this.custRewardPoints = custRewardPoints;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getIsLongTicket() {
		return isLongTicket;
	}

	public void setIsLongTicket(Boolean isLongTicket) {
		this.isLongTicket = isLongTicket;
	}

	
}
