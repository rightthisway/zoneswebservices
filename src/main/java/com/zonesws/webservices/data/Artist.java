package com.zonesws.webservices.data;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.enums.ArtistStatus;

@Entity
@Table(name="artist")
public class Artist implements Serializable  {
	
	private Integer id;
	private String name;
	private String parentType;
	private Boolean custFavFlag;
	private Boolean custSuperFanFlag;
	private ArtistStatus artistStatus;
	private GrandChildCategory grandChildCategory;
	private String rewardPriceTag;
	private Integer totalArtists;
	private Timestamp fromDate;
	private Timestamp toDate;
	
	
	public Artist() {
		//super();
	}
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="artist_status")
	@Enumerated(EnumType.ORDINAL)
	public ArtistStatus getArtistStatus() {
		return artistStatus;
	}

	public void setArtistStatus(ArtistStatus artistStatus) {
		this.artistStatus = artistStatus;
	}

	@Transient
	public GrandChildCategory getGrandChildCategory() {
		return grandChildCategory;
	}

	public void setGrandChildCategory(GrandChildCategory grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}

	@Transient
	public String getParentType() {
		if(null != grandChildCategory){
			parentType = grandChildCategory.getChildCategory().getParentCategory().getName();
		}
		return parentType;
	}

	public void setParentType(String parentType) {
		this.parentType = parentType;
	}

	@Transient
	public Boolean getCustFavFlag() {
		if(null == custFavFlag ){
			custFavFlag = false;
		}
		return custFavFlag;
	}

	public void setCustFavFlag(Boolean custFavFlag) {
		this.custFavFlag = custFavFlag;
	}

	@Transient
	public Boolean getCustSuperFanFlag() {
		if(null == custSuperFanFlag ){
			custSuperFanFlag = false;
		}
		return custSuperFanFlag;
	}

	public void setCustSuperFanFlag(Boolean custSuperFanFlag) {
		this.custSuperFanFlag = custSuperFanFlag;
	}

	@Transient
	public String getRewardPriceTag() {
		return rewardPriceTag;
	}

	public void setRewardPriceTag(String rewardPriceTag) {
		this.rewardPriceTag = rewardPriceTag;
	}
	
	
	@Transient
	public Integer getTotalArtists() {
		if(null == totalArtists){
			totalArtists=0;
		}
		return totalArtists;
	}

	public void setTotalArtists(Integer totalArtists) {
		this.totalArtists = totalArtists;
	}

	@Transient
	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}

	@Transient
	public Timestamp getToDate() {
		return toDate;
	}

	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}
	
	
}

