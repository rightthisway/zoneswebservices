package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "artist_details")
public class ArtistDetails implements Serializable{
	
	@Id
	@Column(name="artist_id")
	private Integer id;
	
	@Column(name="artist_detail_type")
	private Integer artistDetailType;
	
	@Column(name="artist_details")
	private String artistDetails;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="last_updated")
	private Date lastUpdated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getArtistDetailType() {
		return artistDetailType;
	}

	public void setArtistDetailType(Integer artistDetailType) {
		this.artistDetailType = artistDetailType;
	}

	public String getArtistDetails() {
		return artistDetails;
	}

	public void setArtistDetails(String artistDetails) {
		this.artistDetails = artistDetails;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
	
}
