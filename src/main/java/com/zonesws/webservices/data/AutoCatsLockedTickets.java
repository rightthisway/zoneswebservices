package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.zonesws.webservices.enums.LockedTicketStatus;
import com.zonesws.webservices.enums.ProductType;

/**
 *  class to represent Ticket in a locked status, after click on buy button for a ticket ticket becomes LockedTicket 
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="autocats_locked_tickets")
public class AutoCatsLockedTickets implements Serializable{
	
	

	private Integer id;
	private ProductType productType;
	private String sessionId;
	private Date creationDate;
	private Integer eventId;
	private Integer catgeoryTicketGroupId;
	private LockedTicketStatus lockStatus;
	private String ipAddress;
	private String platform;
	private Integer brokerId;
	private Boolean isFanPrice;
	private Integer ticketQty;
	private Double ticketPrice;
	private Double totalPrice;
	private Double serviceFees;
	
	/**
	 * 
	 * @return id
	 */
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, 
	 */
	public void setId(Integer id) {
		this.id = id;
	}	
	
	@Column(name="lock_status", nullable=false)
	@Enumerated(EnumType.STRING)
	@Index(name="lockStatusIndex")
	public LockedTicketStatus getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(LockedTicketStatus lockStatus) {
		this.lockStatus = lockStatus;
	}
	
	@Column(name="product_type", nullable=false)
	@Enumerated(EnumType.STRING)
	@Index(name="productTypeIndex")	
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	@Column(name="session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="eventId")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="category_ticket_group_id")
	public Integer getCatgeoryTicketGroupId() {
		return catgeoryTicketGroupId;
	}
	public void setCatgeoryTicketGroupId(Integer catgeoryTicketGroupId) {
		this.catgeoryTicketGroupId = catgeoryTicketGroupId;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name="platform")
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Column(name="is_fan_price")
	public Boolean getIsFanPrice() {
		return isFanPrice;
	}
	public void setIsFanPrice(Boolean isFanPrice) {
		this.isFanPrice = isFanPrice;
	}
	
	@Column(name="ticket_qty")
	public Integer getTicketQty() {
		return ticketQty;
	}
	public void setTicketQty(Integer ticketQty) {
		this.ticketQty = ticketQty;
	}
	
	@Column(name="ticket_price")
	public Double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	
	@Column(name="total_price")
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Column(name="service_fees")
	public Double getServiceFees() {
		return serviceFees;
	}
	public void setServiceFees(Double serviceFees) {
		this.serviceFees = serviceFees;
	}
	
	
}
