package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * class to represent Autocats Item ordered by user
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="autocats_order_item")
public class AutoCatsOrderItem implements Serializable{
	
	
	private Integer id;
	private String category;
	private String section;
	private String row;
	private Integer eventId;
	private Integer ticketGroupId;
	private Double price;
	private Integer qty;
	private AutoCatsUserOrder autoCatsUserOrder;
	/**
	 * 
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return category
	 */
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	/**
	 * 
	 * @param category , category of a ticket (sports ,concert etc to set)
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	/**
	 * 
	 * @return eventId
	 */
	@Column(name="event_id")
	
	public Integer getEventId() {
		return eventId;
	}
	/**
	 * 
	 * @param eventId , Id of zones event to set
	 */
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	
	/**
	 * 
	 * @return price
	 */
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	/**
	 * 
	 * @param price, price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	/**
	 * 
	 * @return qty
	 */
	@Column(name="qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	/**
	 * 
	 * @return orderId
	 */
	@JoinColumn(name="order_id")
	@OneToOne
	public AutoCatsUserOrder getAutoCatsUserOrder() {
		return autoCatsUserOrder;
	}
	public void setAutoCatsUserOrder(AutoCatsUserOrder autoCatsUserOrder) {
		this.autoCatsUserOrder = autoCatsUserOrder;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	
}
