package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.utils.TicketGroup;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.ZoneEvent;

/**
 * class to represent autocats user order for tickets.
 * @author Ulaganathan
 *
 */
@XStreamAlias("AutoCatsUserOrder")
@Entity
@Table(name="autocats_user_order")
public class AutoCatsUserOrder implements Serializable {
	
	private static DecimalFormat decimalFormat = new DecimalFormat("###.##");
	private Integer id;
	
	@XStreamOmitField
	private Date orderDate;
	private Date deliveryDate;
	private String wsOrderDate;
	private String wsDeliveryDate;
	private Double orderTotal;
	private String orderStatus;
	private String transactionId;	
	private Integer customerId;
	private Customer customer;
	private String paymentMethod;
	private Double originalOrderTotal;
	private Double discountAmount;
	private String appPlatform;
	private String creditCardType;
	
	@XStreamOmitField
	private ZoneEvent zoneEvent;
	
	@XStreamOmitField
	private TicketGroup ticketGroup;
	
	@JsonIgnore
	private String deliveryType;
	
	@JsonIgnore
	private String discountAmountStr;
	
	@JsonIgnore
	private String orderTotalStr;
	
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return customerId
	 */
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	/**
	 * 
	 * @param customerId, customer-Id to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	
	/**
	 * 
	 * @return orderDate
	 */
	@Column(name="order_date")	
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * 
	 * @param orderDate, order date to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * 
	 * @return orderStatus
	 */
	@Column(name="order_status")
	public String getOrderStatus() {
		if(null == orderStatus || orderStatus.isEmpty()){
			orderStatus="";	
		}
		return orderStatus;
	}
	/**
	 * 
	 * @param orderStatus, orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * 
	 * @return orderTotal
	 */
	@Column(name="order_total")
	public Double getOrderTotal() {
		if(null == orderTotal){
			orderTotal=0d;	
		}
		return Double.valueOf(decimalFormat.format(orderTotal));
	}
	/**
	 * 
	 * @param orderTotal, orderTotal to set
	 */
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	/**
	 * 
	 * @return transactionId
	 */
	@Column(name="transaction_id")	
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * 
	 * @param transactionId, not in use right now, it will be covered in phase-II, 
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * 	
	 * @return customer
	 */
	@Transient
	public Customer getCustomer() {
		if(customerId != null){
			customer = DAORegistry.getCustomerDAO().get(customerId);
		}
		return customer;
	}
	/**
	 * 
	 * @param customer, customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	@Column(name="payment_menthod")	
	public String getPaymentMethod() {
		if(null == paymentMethod || paymentMethod.isEmpty()){
			paymentMethod="";	
		}
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	@Column(name="original_order_total")	
	public Double getOriginalOrderTotal() {
		if(null == originalOrderTotal){
			originalOrderTotal=0d;	
		}
		
		return Double.valueOf(decimalFormat.format(originalOrderTotal));
	}
	public void setOriginalOrderTotal(Double originalOrderTotal) {
		this.originalOrderTotal = originalOrderTotal;
	}
	
	
	
	@Column(name="discount_amount")	
	public Double getDiscountAmount() {
		if(null == discountAmount){
			discountAmount=0d;	
		}
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	
	
	
	@Transient
	public String getWsOrderDate() {
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		if(orderDate != null && (null == wsOrderDate || wsOrderDate.isEmpty()) ){
			wsOrderDate = formatter.format(orderDate);
		}
		if(null == wsOrderDate || wsOrderDate.isEmpty()){
			wsOrderDate="";	
		}

		return wsOrderDate;
	}
	public void setWsOrderDate(String wsOrderDate) {
		this.wsOrderDate = wsOrderDate;
	}
	
	
	@Column(name="delivery_date")	
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	@Column(name="app_platform")
	public String getAppPlatform() {
		return appPlatform;
	}
	public void setAppPlatform(String appPlatform) {
		this.appPlatform = appPlatform;
	}
	
	@Column(name="credit_card_type")
	public String getCreditCardType() {
		if(null ==creditCardType ){
			creditCardType="";
		}
		return creditCardType;
	}
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	
	@Column(name="delivery_type")
	public String getDeliveryType() {
		if(null ==deliveryType ){
			deliveryType="";
		}
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	@Transient
	public String getWsDeliveryDate() {
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		if(deliveryDate != null && (null == wsDeliveryDate || wsDeliveryDate.isEmpty()) ){
			wsDeliveryDate = formatter.format(deliveryDate);
		}
		if(null == wsDeliveryDate || wsDeliveryDate.isEmpty()){
			wsDeliveryDate="";	
		}
		return wsDeliveryDate;
	}
	public void setWsDeliveryDate(String wsDeliveryDate) {
		this.wsDeliveryDate = wsDeliveryDate;
	}
	@Transient
	public ZoneEvent getZoneEvent() {
		return zoneEvent;
	}
	public void setZoneEvent(ZoneEvent zoneEvent) {
		this.zoneEvent = zoneEvent;
	}
	
	@Transient
	public TicketGroup getTicketGroup() {
		return ticketGroup;
	}
	public void setTicketGroup(TicketGroup ticketGroup) {
		this.ticketGroup = ticketGroup;
	}
	
	@Transient
	public String getDiscountAmountStr() {
		if(null != discountAmount){
			try{
				discountAmountStr =TicketUtil.getRoundedValueString(discountAmount.doubleValue());
			}catch(Exception e){
				discountAmountStr = "0.00";
			}
		}
		return discountAmountStr;
	}
	public void setDiscountAmountStr(String discountAmountStr) {
		this.discountAmountStr = discountAmountStr;
	}
	
	
	
	@Transient
	public String getOrderTotalStr() {
		if(null != orderTotal){
			try{
				orderTotalStr =TicketUtil.getRoundedValueString(orderTotal.doubleValue());
			}catch(Exception e){
				orderTotalStr = "0.00";
			}
		}
		return orderTotalStr;
	}
	public void setOrderTotalStr(String orderTotalStr) {
		this.orderTotalStr = orderTotalStr;
	}
	
}

