package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseArtist implements Serializable {
	private Integer id;
	private String name;
	private Integer grandChildTourCategoryId;
	//private GrandChildTourCategory grandChildTourCategory;
	public BaseArtist() {
	}
	
	
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}


	@Column(name="grand_child_category_id")
	public Integer getGrandChildTourCategoryId() {
		return grandChildTourCategoryId;
	}

	public void setGrandChildTourCategoryId(Integer grandChildTourCategoryId) {
		this.grandChildTourCategoryId = grandChildTourCategoryId;
	}
	
}
