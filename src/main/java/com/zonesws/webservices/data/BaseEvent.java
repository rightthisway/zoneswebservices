package com.zonesws.webservices.data;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.Status;

@XStreamAlias("Event")
@MappedSuperclass
public class BaseEvent implements Serializable {

	/**
	 * serialVersionUID is added only to remove comment
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private Date eventDate;
	private Time eventTime;
	
//	private Venue venue;
	private Status eventStatus;
	private Artist artist;
	
	
//	public Event(com.admitone.tmat.data.Event event) {
//		this.name=event.getName();
//		this.eventDate=event.getLocalDate();
//		this.eventTime=event.getLocalTime();
//		this.tmatEventId=event.getId();
//	}
	
	public BaseEvent() {
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="tour_id")
	public Artist getArtist() {
		return artist;
	}
	
	public void setArtist(Artist artist) {
		this.artist = artist;
	}
	
	// store date in UTC
	@Column(name="event_date", columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	
	public void setEventDate(Date date) {
		this.eventDate = date;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="event_time")
	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time time) {
		this.eventTime = time;
	}

	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="status")
	public Status getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(Status eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	/*@OneToOne
	@JoinColumn(name="venue_id")
	public Venue getVenue() {
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	*/
	

}
