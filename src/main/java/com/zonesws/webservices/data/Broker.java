package com.zonesws.webservices.data;

import java.io.Serializable;

public class Broker implements Serializable{
	
	private Integer brokerId;
	private Integer userId;
	private String firstName;
	private String lastName;
	private String email;
	private String companyName;
	private Double serviceFeesPerc;
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Double getServiceFeesPerc() {
		return serviceFeesPerc;
	}
	public void setServiceFeesPerc(Double serviceFeesPerc) {
		this.serviceFeesPerc = serviceFeesPerc;
	}
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	
	
}
