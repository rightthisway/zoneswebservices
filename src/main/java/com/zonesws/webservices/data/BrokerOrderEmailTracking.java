package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "rtf_broker_orders_email_tracking")
public class BrokerOrderEmailTracking implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="broker_id")
	private Integer brokerId;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="order_id")
	private Integer orderId;
	
	@Column(name="order_email_sent")
	private boolean orderEmailSent;
	
	@Column(name="order_email_sent_to")
	private String orderEmailSentTo;
	
	@Column(name="order_email_sent_time")
	private Date orderEmailSentTime;
	
	@Column(name="order_cancel_email_sent")
	private boolean orderCancelEmailSent;
	
	@Column(name="order_cancel_email_sent_to")
	private String orderCancelEmailSentTo;
	
	@Column(name="order_cancel_time")
	private Date orderCancelEmailSentTime;

	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public boolean isOrderEmailSent() {
		return orderEmailSent;
	}

	public void setOrderEmailSent(boolean orderEmailSent) {
		this.orderEmailSent = orderEmailSent;
	}

	public String getOrderEmailSentTo() {
		return orderEmailSentTo;
	}

	public void setOrderEmailSentTo(String orderEmailSentTo) {
		this.orderEmailSentTo = orderEmailSentTo;
	}

	public Date getOrderEmailSentTime() {
		return orderEmailSentTime;
	}

	public void setOrderEmailSentTime(Date orderEmailSentTime) {
		this.orderEmailSentTime = orderEmailSentTime;
	}

	public boolean isOrderCancelEmailSent() {
		return orderCancelEmailSent;
	}

	public void setOrderCancelEmailSent(boolean orderCancelEmailSent) {
		this.orderCancelEmailSent = orderCancelEmailSent;
	}

	public String getOrderCancelEmailSentTo() {
		return orderCancelEmailSentTo;
	}

	public void setOrderCancelEmailSentTo(String orderCancelEmailSentTo) {
		this.orderCancelEmailSentTo = orderCancelEmailSentTo;
	}

	public Date getOrderCancelEmailSentTime() {
		return orderCancelEmailSentTime;
	}

	public void setOrderCancelEmailSentTime(Date orderCancelEmailSentTime) {
		this.orderCancelEmailSentTime = orderCancelEmailSentTime;
	}
	
	
	
	
	
	
	
}