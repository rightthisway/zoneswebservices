package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.enums.CardType;

@Entity
@Table(name="cards")
public class Cards implements Serializable  {
	
	private Integer id;
	private CardType cardType;
	private Integer productId;
	private String imageFileUrl;
	private String mobileImageFileUrl;
	private String imageText;
	private String lastUpdatedBy;
	private Date lastUpdated;
	private Integer artistId;
	private Artist artist;
	private Integer desktopPosition;
	private Integer mobilePosition;
	
	private Integer parentCategoryId;
	private Integer childCategoryId;
	private Integer grandChildCategoryId;
	private Boolean showEventData;
	private Integer noOfEvents;
	private Integer venueId;
	private Venue venue;
	private Integer parentCatId;
	private ParentCategory parentCategory;
	private Integer childCatId;
	private ChildCategory childCategory;
	private Integer grandChildCatId;
	private GrandChildCategory grandChildCategory; 
	private String promoCardType;
	
	
	public Cards() {
		//super();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="card_type")
	@Enumerated(EnumType.STRING)
	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Column(name="image_url")
	public String getImageFileUrl() {
		return imageFileUrl;
	}

	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}

	@Column(name="image_text")
	public String getImageText() {
		return imageText;
	}

	public void setImageText(String imageText) {
		this.imageText = imageText;
	}

	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	@Transient
	public Artist getArtist() {
		if(artistId == null) {
			return null;
		}
		if(artist == null) {
			artist = DAORegistry.getArtistDAO().get(artistId);
		}
		return artist;
	}

	@Transient
	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Column(name="desktop_position")
	public Integer getDesktopPosition() {
		return desktopPosition;
	}

	public void setDesktopPosition(Integer desktopPosition) {
		this.desktopPosition = desktopPosition;
	}

	@Column(name="mobile_position")
	public Integer getMobilePosition() {
		return mobilePosition;
	}

	public void setMobilePosition(Integer mobilePosition) {
		this.mobilePosition = mobilePosition;
	}

	
	@Column(name="parent_category_id")
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	@Column(name="child_category_id")
	public Integer getChildCategoryId() {
		return childCategoryId;
	}

	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}

	@Column(name="grand_child_category_id")
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}

	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}
	
	@Column(name="show_events_data")
	public Boolean getShowEventData() {
		return showEventData;
	}

	public void setShowEventData(Boolean showEventData) {
		this.showEventData = showEventData;
	}

	@Column(name="no_of_events")
	public Integer getNoOfEvents() {
		return noOfEvents;
	}

	public void setNoOfEvents(Integer noOfEvents) {
		this.noOfEvents = noOfEvents;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Transient
	public Venue getVenue() {
		if(venueId == null) {
			return null;
		}
		if(venue == null) {
			venue = DAORegistry.getVenueDAO().get(venueId);
		}		
		return venue;
	}

	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}

	@Column(name="parent_cat_id")
	public Integer getParentCatId() {
		return parentCatId;
	}

	public void setParentCatId(Integer parentCatId) {
		this.parentCatId = parentCatId;
	}

	@Transient
	public ParentCategory getParentCategory() {
		if(parentCatId == null) {
			return null;
		}
		if(parentCategory == null) {
			parentCategory = DAORegistry.getParentCategoryDAO().get(parentCatId);
		}		
		return parentCategory;
	}

	@Transient
	public void setParentCategory(ParentCategory parentCategory) {
		this.parentCategory = parentCategory;
	}

	@Column(name="child_cat_id")
	public Integer getChildCatId() {
		return childCatId;
	}

	public void setChildCatId(Integer childCatId) {
		this.childCatId = childCatId;
	}

	@Transient
	public ChildCategory getChildCategory() {
		if(childCatId == null) {
			return null;
		}
		if(childCategory == null) {
			childCategory = DAORegistry.getChildCategoryDAO().get(childCatId);
		}
		return childCategory;
	}

	@Transient
	public void setChildCategory(ChildCategory childCategory) {
		this.childCategory = childCategory;
	}

	@Column(name="grand_child_cat_id")
	public Integer getGrandChildCatId() {
		return grandChildCatId;
	}

	public void setGrandChildCatId(Integer grandChildCatId) {
		this.grandChildCatId = grandChildCatId;
	}

	@Transient
	public GrandChildCategory getGrandChildCategory() {
		if(grandChildCatId == null) {
			return null;
		}
		if(grandChildCategory == null) {
			grandChildCategory = DAORegistry.getGrandChildCategoryDAO().get(grandChildCatId);
		}		
		return grandChildCategory;
	}

	@Transient
	public void setGrandChildCategory(GrandChildCategory grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}

	@Column(name="category_type")
	public String getPromoCardType() {
		return promoCardType;
	}

	public void setPromoCardType(String promoCardType) {
		this.promoCardType = promoCardType;
	}

	@Column(name="mobile_image_url")
	public String getMobileImageFileUrl() {
		return mobileImageFileUrl;
	}

	public void setMobileImageFileUrl(String mobileImageFileUrl) {
		this.mobileImageFileUrl = mobileImageFileUrl;
	}
	
}

