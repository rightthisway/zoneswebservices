package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="category_ticket")
public class CategoryTicket implements Serializable{

	private Integer id;
	private Integer categoryTicketGroupId;
	private Double actualPrice;
	private Integer invoiceId;
	private Integer ticketId;
	private Boolean isProcessed;
	private String userName;
	private Integer exchangeRequestId;
	private Date fillDate;
	private Double soldPrice;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="category_ticket_group_id")
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	
	@Column(name="actual_price")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name="processed")
	public Boolean getIsProcessed() {
		return isProcessed;
	}
	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
	
	@Column(name="username")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="exchange_request_id")
	public Integer getExchangeRequestId() {
		return exchangeRequestId;
	}
	
	public void setExchangeRequestId(Integer exchangeRequestId) {
		this.exchangeRequestId = exchangeRequestId;
	}
	
	@Column(name="fill_date")
	public Date getFillDate() {
		return fillDate;
	}
	public void setFillDate(Date fillDate) {
		this.fillDate = fillDate;
	}
	
	@Column(name="sold_price")
	public Double getSoldPrice() {
		return soldPrice;
	}
	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}
	
	
	
}
