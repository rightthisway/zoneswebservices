package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.utils.TicketUtil;

@Entity
@Table(name="category_ticket_group")
public class CategoryTicketGroup implements Serializable{
	
		private Integer id;
		private Integer eventId;
		private String section;
		private String actualSection;
		@JsonIgnore
		private String longSection;
		private String row;
		@JsonIgnore
		private String actualSeat;
		private String svgKey;
		private Integer quantity;
		private String price;
		private String loyalFanPrice;
		private Double loyalFanPriceDouble;
		private Double originalPrice;
		private Boolean isPromoCodeApplied;
		private String promotionalCode;
		private String promotionalCodeMessage;
		private Boolean showDiscPriceArea;
		private Double normalTixPrice;
		private String normalTixPriceStr;
		private String priceWithFees;
		private Double discountedTixPrice;
		private String discountedTixPriceStr;
		private String sectionRange;
		private String rowRange;
		@JsonIgnore
		private TicketStatus status;
		private String shippingMethod;
		private String deliveryInfo;
		@JsonIgnore
		private Date createdDate;
		@JsonIgnore
		private Date lastUpdatedDate;
		@JsonIgnore
		private String createdBy;
		@JsonIgnore
		private Double soldPrice;
		@JsonIgnore
		private Integer soldQuantity;
		@JsonIgnore
		private ProductType producttype;
		
		private String ticketDescription;
		private String sectionDescription;
		private String colorCode;
		private String rgbColor;
		private Integer invoiceId;
		
		@JsonIgnore
		private String nearTermDisplayOption;
		@JsonIgnore
		private Double facePrice;
		@JsonIgnore
		private Double cost;
		@JsonIgnore
		private Double wholeSalePrice;
		@JsonIgnore
		private Double retailPrice;
		@JsonIgnore
		private Boolean broadcast;
		@JsonIgnore
		private String marketPlaceNotes;
		@JsonIgnore
		private Integer maxShowing;
		@JsonIgnore
		private String internalNotes;
		@JsonIgnore
		private String externalNotes;
		
		private String taxes;
		private Double taxesAsDouble;
		@JsonIgnore
		private Double originalTax;
		@JsonIgnore
		private Double originalTaxPerc;
		//private String loyalFanDiscount;
		private String rewardPoints;
		private Double orderTotalAsDouble;
		private String orderTotal;
		@JsonIgnore
		private Integer brokerId;
		
		@JsonIgnore
		private Boolean isFlatDiscount;
		@JsonIgnore
		private Double flatDiscount;
		
		private String totalServiceFees;
		private String singleTixServiceFees;
		
		@Id
		@Column(name="id")
		@GeneratedValue(strategy=GenerationType.AUTO)
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name = "event_id")
		public Integer getEventId() {
			return eventId;
		}
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		
		
		@Column(name = "quantity")
		public Integer getQuantity() {
			return quantity;
		}
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}
		
		@Transient
		public Double getLoyalFanPriceDouble() {
			if(originalPrice != null && originalPrice>0){
				try {
					if(isFlatDiscount != null && isFlatDiscount){
						loyalFanPriceDouble = TicketUtil.getLoyalFanPriceAsDouble(originalPrice);
						loyalFanPriceDouble = TicketUtil.getRoundedValue(loyalFanPriceDouble - flatDiscount);
					}else{
						loyalFanPriceDouble = TicketUtil.getLoyalFanPriceAsDouble(originalPrice);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return loyalFanPriceDouble;
		}
		
		public void setLoyalFanPriceDouble(Double loyalFanPriceDouble) {
			this.loyalFanPriceDouble = loyalFanPriceDouble;
		}
		
		@Transient
		public String getLoyalFanPrice() {
			if(originalPrice != null && originalPrice>0){
				try {
					getLoyalFanPriceDouble();
					loyalFanPrice = TicketUtil.getRoundedValueString(loyalFanPriceDouble);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return loyalFanPrice;
		}
		public void setLoyalFanPrice(String loyalFanPrice) {
			this.loyalFanPrice = loyalFanPrice;
		}
		
		@Transient
		public String getPrice() {
			if(null == price || price.isEmpty() || price.length() <= 0 ){
				try {
					price = TicketUtil.getRoundedValueString(originalPrice);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return price;
				}
			}
			return price;
		}
		public void setPrice(String price) {
			this.price = price;
		}
		
		@Column(name = "price")
		public Double getOriginalPrice() {
			return originalPrice;
		}
		public void setOriginalPrice(Double originalPrice) {
			this.originalPrice = originalPrice;
		}
		@Column(name = "section_range")
		public String getSectionRange() {
			return sectionRange;
		}
		public void setSectionRange(String sectionRange) {
			this.sectionRange = sectionRange;
		}
		
		@Column(name = "row_range")
		public String getRowRange() {
			return rowRange;
		}
		public void setRowRange(String rowRange) {
			this.rowRange = rowRange;
		}
		
		@Enumerated(EnumType.STRING)
		@Column(name = "status") 
		public TicketStatus getStatus() {
			return status;
		}
		public void setStatus(TicketStatus status) {
			this.status = status;
		}
		
		@Column(name = "shipping_method")
		public String getShippingMethod() {
			return shippingMethod;
		}
		public void setShippingMethod(String shippingMethod) {
			this.shippingMethod = shippingMethod;
		}
		
		@Column(name = "created_date")
		public Date getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		
		@Column(name = "last_updated")
		public Date getLastUpdatedDate() {
			return lastUpdatedDate;
		}
		public void setLastUpdatedDate(Date lastUpdatedDate) {
			this.lastUpdatedDate = lastUpdatedDate;
		}
		
		@Column(name = "created_by")
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		
		@Column(name = "sold_price")
		public Double getSoldPrice() {
			return soldPrice;
		}
		public void setSoldPrice(Double soldPrice) {
			this.soldPrice = soldPrice;
		}
		
		@Column(name = "sold_qty")
		public Integer getSoldQuantity() {
			return soldQuantity;
		}
		public void setSoldQuantity(Integer soldQuantity) {
			this.soldQuantity = soldQuantity;
		}
		
		@Enumerated(EnumType.STRING)
		@Column(name = "product_type")
		public ProductType getProducttype() {
			return producttype;
		}
		public void setProducttype(ProductType producttype) {
			this.producttype = producttype;
		}
		
		@Transient
		public String getTicketDescription() {
			if(null == ticketDescription){
				ticketDescription="";
			}
			return ticketDescription;
		}
		public void setTicketDescription(String ticketDescription) {
			this.ticketDescription = ticketDescription;
		}
		
		@Transient
		public String getSectionDescription() {
			if(null != section && !section.isEmpty()){
				sectionDescription = TicketUtil.getTicketGroupSectionDescription(quantity,sectionRange, rowRange, section);
			}
			return sectionDescription;
		}
		public void setSectionDescription(String sectionDescription) {
			this.sectionDescription = sectionDescription;
		}
		
		@Transient
		public String getColorCode() {
			if(null == colorCode){
				colorCode="";
			}
			return colorCode;
		}
		public void setColorCode(String colorCode) {
			this.colorCode = colorCode;
		}
		
		@Transient
		public String getRgbColor() {
			if(null == rgbColor){
				rgbColor="";
			}
			return rgbColor;
		}
		public void setRgbColor(String rgbColor) {
			this.rgbColor = rgbColor;
		}
		
		@Transient
		public String getSvgKey() {
			if(actualSection != null && !actualSection.isEmpty() && !actualSection.equals("")){
				svgKey = actualSection.toLowerCase();
			}
			return svgKey;
		}
		public void setSvgKey(String svgKey) {
			this.svgKey = svgKey;
		}
		
		
		@Transient
		public String getDeliveryInfo() {
			return deliveryInfo;
		}
		public void setDeliveryInfo(String deliveryInfo) {
			this.deliveryInfo = deliveryInfo;
		}
		
		@Column(name = "invoice_id")
		public Integer getInvoiceId() {
			return invoiceId;
		}
		public void setInvoiceId(Integer invoiceId) {
			this.invoiceId = invoiceId;
		}
		
		
		@Column(name="internal_notes")
		public String getInternalNotes() {
			return internalNotes;
		}
		public void setInternalNotes(String internalNotes) {
			this.internalNotes = internalNotes;
		}
		
		@Column(name="near_term_display_option")
		public String getNearTermDisplayOption() {
			return nearTermDisplayOption;
		}
		public void setNearTermDisplayOption(String nearTermDisplayOption) {
			this.nearTermDisplayOption = nearTermDisplayOption;
		}
		
		@Column(name="face_price")
		public Double getFacePrice() {
			return facePrice;
		}
		public void setFacePrice(Double facePrice) {
			this.facePrice = facePrice;
		}
		
		@Column(name="cost")
		public Double getCost() {
			return cost;
		}
		public void setCost(Double cost) {
			this.cost = cost;
		}
		
		@Column(name="wholesale_price")
		public Double getWholeSalePrice() {
			return wholeSalePrice;
		}
		public void setWholeSalePrice(Double wholeSalePrice) {
			this.wholeSalePrice = wholeSalePrice;
		}
		
		@Column(name="retail_price")
		public Double getRetailPrice() {
			return retailPrice;
		}
		public void setRetailPrice(Double retailPrice) {
			this.retailPrice = retailPrice;
		}
		
		@Column(name="broadcast")
		public Boolean getBroadcast() {
			return broadcast;
		}
		public void setBroadcast(Boolean broadcast) {
			this.broadcast = broadcast;
		}
		
		@Column(name="marketplace_notes")
		public String getMarketPlaceNotes() {
			return marketPlaceNotes;
		}
		public void setMarketPlaceNotes(String marketPlaceNotes) {
			this.marketPlaceNotes = marketPlaceNotes;
		}
		
		@Column(name="max_showing")
		public Integer getMaxShowing() {
			return maxShowing;
		}
		public void setMaxShowing(Integer maxShowing) {
			this.maxShowing = maxShowing;
		}
		@Column(name="external_notes")
		public String getExternalNotes() {
			return externalNotes;
		}
		public void setExternalNotes(String externalNotes) {
			this.externalNotes = externalNotes;
		}
		/*@Transient
		public String getLoyalFanDiscount() {
			return loyalFanDiscount;
		}
		public void setLoyalFanDiscount(String loyalFanDiscount) {
			this.loyalFanDiscount = loyalFanDiscount;
		}*/
		@Transient
		public String getRewardPoints() {
			return rewardPoints;
		}
		public void setRewardPoints(String rewardPoints) {
			this.rewardPoints = rewardPoints;
		}
		
		@Transient
		public String getOrderTotal() {
			return orderTotal;
		}
		public void setOrderTotal(String orderTotal) {
			this.orderTotal = orderTotal;
		}
		
		@Transient
		public Double getOrderTotalAsDouble() {
			return orderTotalAsDouble;
		}
		public void setOrderTotalAsDouble(Double orderTotalAsDouble) {
			this.orderTotalAsDouble = orderTotalAsDouble;
		}
		
		@Transient
		public Double getTaxesAsDouble() {
			/*if(quantity != null && null != originalTax){
				taxesAsDouble = originalTax * quantity;
			}*/
			return taxesAsDouble;
		}
		public void setTaxesAsDouble(Double taxesAsDouble) {
			this.taxesAsDouble = taxesAsDouble;
		}
		
		@Transient
		public String getTaxes() {
			if(taxesAsDouble == null){
				taxesAsDouble = 0.00;
			}
			try {
				taxes = TicketUtil.getRoundedValueString(taxesAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return taxes;
		}
		public void setTaxes(String taxes) {
			this.taxes = taxes;
		}
		
		@Column(name = "tax_amount")
		public Double getOriginalTax() {
			return originalTax;
		}
		public void setOriginalTax(Double originalTax) {
			this.originalTax = originalTax;
		}
		
		@Column(name = "tax_percentage")
		public Double getOriginalTaxPerc() {
			return originalTaxPerc;
		}
		public void setOriginalTaxPerc(Double originalTaxPerc) {
			this.originalTaxPerc = originalTaxPerc;
		}
		
		
		
		@Column(name = "section")
		public String getActualSection() {
			return actualSection;
		}
		public void setActualSection(String actualSection) {
			this.actualSection = actualSection;
		}
		
		@Transient
		public String getSection() {
			if(actualSection != null && !actualSection.isEmpty()){
				section = "ZONE "+actualSection;
			}
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		
		@Column(name = "row")
		public String getRow() {
			if(null == row){
				row= "";
			}
			return row;
		}
		public void setRow(String row) {
			this.row = row;
		}
		
		@Column(name = "actual_section")
		public String getLongSection() {
			if(null == longSection){
				longSection = "";
			}
			return longSection;
		}
		public void setLongSection(String longSection) {
			this.longSection = longSection;
		}
		
		@Transient
		public String getActualSeat() {
			if(null == actualSeat){
				actualSeat = "";
			}
			return actualSeat;
		}
		public void setActualSeat(String actualSeat) {
			this.actualSeat = actualSeat;
		}
		
		@Column(name = "broker_id")
		public Integer getBrokerId() {
			return brokerId;
		}
		public void setBrokerId(Integer brokerId) {
			this.brokerId = brokerId;
		}
		
		@Transient
		public Boolean getIsFlatDiscount() {
			return isFlatDiscount;
		}
		public void setIsFlatDiscount(Boolean isFlatDiscount) {
			this.isFlatDiscount = isFlatDiscount;
		}
		
		@Transient
		public Double getFlatDiscount() {
			return flatDiscount;
		}
		public void setFlatDiscount(Double flatDiscount) {
			this.flatDiscount = flatDiscount;
		}
		
		@Transient
		public Boolean getIsPromoCodeApplied() {
			if(null == isPromoCodeApplied){
				isPromoCodeApplied = false;
			}
			return isPromoCodeApplied;
		}
		public void setIsPromoCodeApplied(Boolean isPromoCodeApplied) {
			this.isPromoCodeApplied = isPromoCodeApplied;
		}
		
		@Transient
		public String getPromotionalCode() {
			if(null == promotionalCode){
				promotionalCode = "";
			}
			return promotionalCode;
		}
		public void setPromotionalCode(String promotionalCode) {
			this.promotionalCode = promotionalCode;
		}
		
		@Transient
		public String getPromotionalCodeMessage() {
			if(null == promotionalCodeMessage){
				promotionalCodeMessage = "";
			}
			return promotionalCodeMessage;
		}
		public void setPromotionalCodeMessage(String promotionalCodeMessage) {
			this.promotionalCodeMessage = promotionalCodeMessage;
		}
		
		@Transient
		public Boolean getShowDiscPriceArea() {
			if(null == showDiscPriceArea){
				showDiscPriceArea = false;
			}
			return showDiscPriceArea;
		}
		public void setShowDiscPriceArea(Boolean showDiscPriceArea) {
			this.showDiscPriceArea = showDiscPriceArea;
		}
		
		@Transient
		public Double getNormalTixPrice() {
			if(null == normalTixPrice){
				normalTixPrice = 0.00;
			}
			return normalTixPrice;
		}
		public void setNormalTixPrice(Double normalTixPrice) {
			this.normalTixPrice = normalTixPrice;
		}
		
		@Transient
		public String getNormalTixPriceStr() {
			try {
				normalTixPriceStr = TicketUtil.getRoundedValueString(normalTixPrice);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return normalTixPriceStr;
		}
		public void setNormalTixPriceStr(String normalTixPriceStr) {
			this.normalTixPriceStr = normalTixPriceStr;
		}
		
		@Transient
		public Double getDiscountedTixPrice() {
			if(null == discountedTixPrice){
				discountedTixPrice = 0.00;
			}
			return discountedTixPrice;
		}
		public void setDiscountedTixPrice(Double discountedTixPrice) {
			this.discountedTixPrice = discountedTixPrice;
		}
		
		@Transient
		public String getDiscountedTixPriceStr() {
			try {
				discountedTixPriceStr = TicketUtil.getRoundedValueString(discountedTixPrice);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return discountedTixPriceStr;
		}
		public void setDiscountedTixPriceStr(String discountedTixPriceStr) {
			this.discountedTixPriceStr = discountedTixPriceStr;
		}
		
		public void computeTicketPrice(ApplicationPlatForm platForm){
			if(platForm.equals(ApplicationPlatForm.IOS) || platForm.equals(ApplicationPlatForm.ANDROID)){
				Double mobileDiscConv = 0.00;
				Double discountPrice = 0.00;
				try {
					discountPrice = TicketUtil.getRoundedValue(originalPrice * mobileDiscConv);
					originalPrice = TicketUtil.getRoundedValue(originalPrice - discountPrice);
					setOriginalPrice(originalPrice);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				// No Discount For Desktop & Mobile Site.
			}
		}
		
		@Transient
		public String getTotalServiceFees() {
			return totalServiceFees;
		}
		public void setTotalServiceFees(String totalServiceFees) {
			this.totalServiceFees = totalServiceFees;
		}
		
		@Transient
		public String getSingleTixServiceFees() {
			return singleTixServiceFees;
		}
		public void setSingleTixServiceFees(String singleTixServiceFees) {
			this.singleTixServiceFees = singleTixServiceFees;
		}
		
		@Transient
		public String getPriceWithFees() {
			if(priceWithFees == null || priceWithFees.isEmpty() || priceWithFees.length() <= 0){
				try {
					//priceWithFees = TicketUtil.getRoundedValueString(((originalPrice)+(originalTax)));
					if(getOriginalTaxPerc() != null && getOriginalPrice() != null) {
						priceWithFees = TicketUtil.getRoundedValueString(getOriginalPrice()+TicketUtil.getBrokerServiceFees(getBrokerId(), getOriginalPrice(), 1, getOriginalTaxPerc()));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return priceWithFees;
		}
		public void setPriceWithFees(String priceWithFees) {
			this.priceWithFees = priceWithFees;
		}
		
		
		
		
		
	}