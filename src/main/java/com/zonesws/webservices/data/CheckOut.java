package com.zonesws.webservices.data;

import java.io.Serializable;


/**
 * class to represent checkout information of a user.
 * @author Ulaganathan
 *
 */
public class CheckOut implements Serializable {
	
	private Integer status;
	private com.zonesws.webservices.utils.Error error;
	private String trxError;
	private Integer customerId;
	private String deviceId;
	private String customerFirstName;
	private String customerLastName;
	private String  billingAddress1;
	private String  billingAddress2;
	private String  billingZipCode;
	private String  mailingAddress1;
	private String  mailingAddress2;
	private String  mailingZipCode;
	private String  mailingState;
	private String  mailingCity;
	private String cardType;
	private String nameOnCard;
	private String cardNo;
	private Integer expMonth;
	private Integer expYear;
	private String securityCode;
	private Double orderAmount;
	private String transactionId;
	private Customer customer;
	private UserAddress billingAddress;
	private UserAddress shippingAddress;
	
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	public String getBillingAddress1() {
		return billingAddress1;
	}
	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}
	public String getBillingAddress2() {
		return billingAddress2;
	}
	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}
	public String getBillingZipCode() {
		return billingZipCode;
	}
	public void setBillingZipCode(String billingZipCode) {
		this.billingZipCode = billingZipCode;
	}
	public String getMailingAddress1() {
		return mailingAddress1;
	}
	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}
	public String getMailingAddress2() {
		return mailingAddress2;
	}
	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}
	public String getMailingZipCode() {
		return mailingZipCode;
	}
	public void setMailingZipCode(String mailingZipCode) {
		this.mailingZipCode = mailingZipCode;
	}
	public String getMailingState() {
		return mailingState;
	}
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}
	public String getMailingCity() {
		return mailingCity;
	}
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public Integer getExpMonth() {
		return expMonth;
	}
	public void setExpMonth(Integer expMonth) {
		this.expMonth = expMonth;
	}
	public Integer getExpYear() {
		return expYear;
	}
	public void setExpYear(Integer expYear) {
		this.expYear = expYear;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	
	public Double getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public com.zonesws.webservices.utils.Error getError() {
		return error;
	}
	public void setError(com.zonesws.webservices.utils.Error error) {
		this.error = error;
	}
	public String getTrxError() {
		return trxError;
	}
	public void setTrxError(String trxError) {
		this.trxError = trxError;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public UserAddress getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(UserAddress billingAddress) {
		this.billingAddress = billingAddress;
	}
	public UserAddress getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(UserAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
}
