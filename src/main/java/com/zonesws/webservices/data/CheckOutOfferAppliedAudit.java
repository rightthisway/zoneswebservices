package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;

@Entity
@Table(name = "rtf_pre_order_offer_applied_audit")
public class CheckOutOfferAppliedAudit implements Serializable{

	private static final long serialVersionUID = -7941769011539361245L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "customer_id")
	private Integer customerId;
	
	@Column(name = "event_id")
	private Integer eventId;
	
	@Column(name = "order_id")
	private Integer orderId;
	
	@Column(name = "category_ticket_group_id")
	private Integer categoryTicketGroupId;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "platform")
	private ApplicationPlatForm platForm;
	
	@Column(name = "session_id")
	private String sessionId;
	
	@Column(name = "is_loyal_fan_price")
	private Boolean isFanPrice;
	
	@Column(name = "ticket_normal_price")
	private Double ticketNormalPrice;
	
	@Column(name = "ticket_selling_price")
	private Double ticketSellingPrice;
	
	@Column(name = "is_visited_event")
	private Boolean isVisitedEvent;
	
	@Column(name = "is_flat_off")
	private Boolean isFlatOff;
	
	@Column(name = "discount")
	private Double discount;
	
	@Column(name = "other_event_order_threshold")
	private Double otherEventOrderThreshold;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "updated_date")
	private Date updatedDate;
	
	@Column(name = "status")
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}

	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}

	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}

	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Boolean getIsFanPrice() {
		return isFanPrice;
	}

	public void setIsFanPrice(Boolean isFanPrice) {
		this.isFanPrice = isFanPrice;
	}

	public Double getTicketNormalPrice() {
		return ticketNormalPrice;
	}

	public void setTicketNormalPrice(Double ticketNormalPrice) {
		this.ticketNormalPrice = ticketNormalPrice;
	}

	public Double getTicketSellingPrice() {
		return ticketSellingPrice;
	}

	public void setTicketSellingPrice(Double ticketSellingPrice) {
		this.ticketSellingPrice = ticketSellingPrice;
	}

	public Boolean getIsVisitedEvent() {
		return isVisitedEvent;
	}

	public void setIsVisitedEvent(Boolean isVisitedEvent) {
		this.isVisitedEvent = isVisitedEvent;
	}

	public Boolean getIsFlatOff() {
		return isFlatOff;
	}

	public void setIsFlatOff(Boolean isFlatOff) {
		this.isFlatOff = isFlatOff;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getOtherEventOrderThreshold() {
		return otherEventOrderThreshold;
	}

	public void setOtherEventOrderThreshold(Double otherEventOrderThreshold) {
		this.otherEventOrderThreshold = otherEventOrderThreshold;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}