package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.zonesws.webservices.enums.SliderSearchType;

 
/**
 * class to represent Child Category of Tour.
 * @author hamin
 *
 */
@Entity
@Table(name="child_category")
public class ChildCategory implements Serializable{
	
	@XStreamOmitField
	private String name;
	@XStreamOmitField
	private Integer id;
	@XStreamOmitField
	@JsonIgnore
	private ParentCategory parentCategory;
	private String imageUrl;
	private String parentCategoryName;
	private SliderSearchType sliderSearchType;
	@JsonIgnore
	private Boolean displayOnSearch;
	/**
	 * @return the id
	 */
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the parentCategory
	 */

	@Transient
	public ParentCategory getParentCategory() {
		return parentCategory;
	}
	/**
	 * @param parentCategory the parentCategory to set
	 */
	public void setParentCategory(ParentCategory parentCategory) {
		this.parentCategory = parentCategory;
	}
	
	@Override
	public String toString() {		 
		return  "[ id:"+id+" name: "+name+"]";
	}
	
	@Override
	public boolean equals(Object obj) {
		ChildCategory temp = (ChildCategory) obj;
		return this.id.equals(temp.id);
	}
	
	@Transient
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Transient
	public String getParentCategoryName() {
		if(null != parentCategoryName && !parentCategoryName.isEmpty()){
			parentCategoryName = parentCategoryName.toUpperCase();
		}
		return parentCategoryName;
	}
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	
	@Transient
	public SliderSearchType getSliderSearchType() {
		return sliderSearchType;
	}
	public void setSliderSearchType(SliderSearchType sliderSearchType) {
		this.sliderSearchType = sliderSearchType;
	}
	
	@Column(name="display_on_search")
	public Boolean getDisplayOnSearch() {
		return displayOnSearch;
	}
	public void setDisplayOnSearch(Boolean displayOnSearch) {
		this.displayOnSearch = displayOnSearch;
	}
	
	
}
