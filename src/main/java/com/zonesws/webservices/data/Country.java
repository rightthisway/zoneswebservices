package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * class to represent a country 
 * @author hamin
 *
 */
@Entity
@Table(name="country")
@XStreamAlias("Country")
public class Country implements Serializable {
	private Integer id;
	private String countryName;
	private String shortDesc;
	private List<State> stateList;
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return countryName
	 */
	@Column(name="country_name")
	public String getCountryName() {
		return countryName;
	}
	/**
	 * 
	 * @param countryName,Country Name to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * 
	 * @return shortDesc
	 */
	@Column(name="short_desc")
	public String getShortDesc() {
		return shortDesc;
	}
	/**
	 * 
	 * @param shortDesc, short description to set (e.g US for united states)
	 */
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	
	@Transient
	public List<State> getStateList() {
		//stateList = DAORegistry.getStateDAO().getAllStateByCountryId(id);
		return stateList;
	}
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}
	
	
	
	

}
