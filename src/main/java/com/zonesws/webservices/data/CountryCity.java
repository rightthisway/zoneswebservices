package com.zonesws.webservices.data;

import java.io.Serializable;

/**
 * class to represent a country 
 * @author Ulaganathan
 *
 */

public class CountryCity implements Serializable {
	private String city;
	private Integer stateId;
	private String state;
	private Integer countryId;
	private String country;
	private String zipcode;
	private String stateFullName;
	private String countryFullName;
	private String latitude;
	private String longitude;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		if(zipcode == null){
			zipcode="";
		}
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getStateFullName() {
		return stateFullName;
	}
	public void setStateFullName(String stateFullName) {
		this.stateFullName = stateFullName;
	}
	public String getCountryFullName() {
		return countryFullName;
	}
	public void setCountryFullName(String countryFullName) {
		this.countryFullName = countryFullName;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	
	
}
