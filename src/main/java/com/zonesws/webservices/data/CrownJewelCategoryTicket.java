package com.zonesws.webservices.data;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.ZonesQuantityTypes;


@Entity
@Table(name="crownjewel_category_ticket")
public class CrownJewelCategoryTicket implements Serializable{


	private Integer id;
	private Integer eventId;
	@JsonIgnore
	private Integer fantasyEventId;
	@JsonIgnore
	private Integer grandChildId;
	private String section;
	private Integer quantity;
	private Double price;
	private String sectionRange;
	private String rowRange;
	private String status;
	private Integer pointsNeed;
	private Boolean isRealTicket;
	@JsonIgnore
	private ZonesQuantityTypes quantityType; 
	
	@JsonIgnore
	private Integer maxTixCount;
	@JsonIgnore
	private Integer soldTixCount;
	@JsonIgnore
	private Integer teamId;
	@JsonIgnore
	private Integer leagueId;
	@JsonIgnore
	private String city;
	@JsonIgnore
	private String ticketType;

	private String shippingMethod;
	@JsonIgnore
	private Date createDate;
	@JsonIgnore
	private Date updateDate;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
	
	
	@Column(name="section_range")
	public String getSectionRange() {
		if(sectionRange == null){
			sectionRange = "";
		}
		return sectionRange;
	}

	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

	@Column(name="row_range")
	public String getRowRange() {
		if(rowRange==null){
			rowRange="";
		}
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	
	@Column(name="zone_price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Transient
	public Integer getPointsNeed() {
		return pointsNeed;
	}
	public void setPointsNeed(Integer pointsNeed) {
		this.pointsNeed = pointsNeed;
	}
	
	@Transient
	public Integer getMaxTixCount() {
		return maxTixCount;
	}
	public void setMaxTixCount(Integer maxTixCount) {
		this.maxTixCount = maxTixCount;
	}
	
	@Transient
	public Integer getSoldTixCount() {
		return soldTixCount;
	}
	public void setSoldTixCount(Integer soldTixCount) {
		this.soldTixCount = soldTixCount;
	}
	
	@Transient
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	@Transient
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	
	@Transient
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Transient
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	@Transient
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	
	@Transient
	public Boolean getIsRealTicket() {
		if(null == isRealTicket){
			isRealTicket = true;
		}
		return isRealTicket;
	}
	public void setIsRealTicket(Boolean isRealTicket) {
		this.isRealTicket = isRealTicket;
	}
	
	@Column(name="created_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="last_updated")
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	@Transient
	public ZonesQuantityTypes getQuantityType() {
		return quantityType;
	}
	public void setQuantityType(ZonesQuantityTypes quantityType) {
		this.quantityType = quantityType;
	}
	@Transient
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	@Transient
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	
}

