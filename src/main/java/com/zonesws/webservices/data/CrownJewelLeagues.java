package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;


@Entity
@Table(name="fantasy_events")
public class CrownJewelLeagues implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	
	private Integer id;
	private Integer grandChildId;
	private String name;
	@JsonIgnore
	private String lastUpdateddBy;
	@JsonIgnore
	private Date lastUpdated;
	@JsonIgnore
	private String status;
	@JsonIgnore
	private String lastUpdatedStr;
	private Integer eventId;
	@JsonIgnore
	private Boolean isPackageCost;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="only_package")
	public Boolean getIsPackageCost() {
		return isPackageCost;
	}
	public void setIsPackageCost(Boolean isPackageCost) {
		this.isPackageCost = isPackageCost;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Transient
	public String getLastUpdatedStr() {
		if(this.lastUpdatedStr == null) {
			this.lastUpdatedStr = (getLastUpdated() == null)?"TBD":(dateFormat.format(getLastUpdated()));
		}
		return lastUpdatedStr;
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
	@Column(name="grandchild_id")
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	

}
