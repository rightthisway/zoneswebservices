package com.zonesws.webservices.data;

public enum CrownJewelOrderStatus {
	OUTSTANDING,APPROVED,REJECTED,PENDING
}