package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.enums.ZonesQuantityTypes;
import com.zonesws.webservices.jobs.FantasyTicketsEventUtil;

@Entity
@Table(name="fantasy_event_team_zones")
public class CrownJewelTeamZones implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	
	private Integer id;
	private Integer fantasyEventId;
	private Integer teamId;
	private Integer eventId;
	private String zone;
	private Double price;
	private Integer ticketsCount=0;
	private Integer soldTicketsCount=0;
	private String lastUpdateddBy;
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	private Integer availableTixCount=0;
	private Integer pointsRedeemed=0;
	private ZonesQuantityTypes quantityType; 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="team_id")
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Transient
	public Integer getSoldTicketsCount() {
		return soldTicketsCount;
	}
	public void setSoldTicketsCount(Integer soldTicketsCount) {
		this.soldTicketsCount = soldTicketsCount;
	}
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="tickets_count")
	public Integer getTicketsCount() {
		return ticketsCount;
	}
	public void setTicketsCount(Integer ticketsCount) {
		this.ticketsCount = ticketsCount;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	public String getLastUpdatedStr() {
		if(this.lastUpdatedStr == null) {
			this.lastUpdatedStr = (getLastUpdated() == null)?"TBD":(dateFormat.format(getLastUpdated()));
		}
		return lastUpdatedStr;
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	
	@Transient
	public Integer getAvailableTixCount() {
		return availableTixCount;
	}
	@Transient
	public void setAvailableTixCount(Integer availableTixCount) {
		this.availableTixCount = availableTixCount;
	}
	@Transient
	public Integer getPointsRedeemed() {
		return pointsRedeemed;
	}
	@Transient
	public void setPointsRedeemed(Integer pointsRedeemed) {
		this.pointsRedeemed = pointsRedeemed;
	}
	@Enumerated(EnumType.STRING)
	@Column(name = "quantity_type")
	public ZonesQuantityTypes getQuantityType() {
		return quantityType;
	}
	public void setQuantityType(ZonesQuantityTypes quantityType) {
		this.quantityType = quantityType;
	}
	
	@Column(name = "fantasy_event_id")
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	
}
