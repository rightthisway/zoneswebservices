package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="fantasy_event_teams")
public class CrownJewelTeams implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	
	private Integer id;
	private Integer fantasyEventId;
	private String name;
	private Double odds;
	private Integer ticketsCount;
	private String lastUpdateddBy;
	private Date lastUpdated;
	private String status;
	private String lastUpdatedStr;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="last_updated_by")
	public String getLastUpdateddBy() {
		return lastUpdateddBy;
	}
	public void setLastUpdateddBy(String lastUpdateddBy) {
		this.lastUpdateddBy = lastUpdateddBy;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="odds")
	public Double getOdds() {
		return odds;
	}
	public void setOdds(Double odds) {
		this.odds = odds;
	}
	
	@Column(name="tickets_count")
	public Integer getTicketsCount() {
		return ticketsCount;
	}
	public void setTicketsCount(Integer ticketsCount) {
		this.ticketsCount = ticketsCount;
	}
	
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Transient
	public String getLastUpdatedStr() {
		if(this.lastUpdatedStr == null) {
			this.lastUpdatedStr = (getLastUpdated() == null)?"TBD":(dateFormat.format(getLastUpdated()));
		}
		return lastUpdatedStr;
	}
	
	@Transient
	public void setLastUpdatedStr(String lastUpdatedStr) {
		this.lastUpdatedStr = lastUpdatedStr;
	}
	@Column(name="fantasy_event_id")
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	
	

}
