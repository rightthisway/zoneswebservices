package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fantasy_event_zones_sold_count")
public class CrownJewelZonesSoldDetails implements Serializable{
	
	/**
	 * serialVersionUID is added only to remove warning
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String zone;
	private Integer soldCount;
	private Integer tmatEventId;
	private Integer grandChildId;
	private Integer fantasyEventId;
	private Integer teamId;
	private Integer zoneId;
	private Date lastUpdated;
	private String status;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="fantasy_team_id")
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="total_sold_count")
	public Integer getSoldCount() {
		return soldCount;
	}
	public void setSoldCount(Integer soldCount) {
		this.soldCount = soldCount;
	}
	
	@Column(name="zone_id")
	public Integer getZoneId() {
		return zoneId;
	}
	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}
	
	@Column(name="tmat_event_id")
	public Integer getTmatEventId() {
		return tmatEventId;
	}
	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	
	@Column(name="grand_child_id")
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	@Column(name="fantasy_event_id")
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	
}
