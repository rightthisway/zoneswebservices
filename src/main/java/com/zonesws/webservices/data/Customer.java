package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CustomerType;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.SignupType;
import com.zonesws.webservices.utils.PasswordUtil;

/**
 * represents customer entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("Customer")
@Entity
@Table(name="customer")
public class Customer  implements Serializable{
	
	private Integer id;
	private String userName;
	private String customerName;
	private String email;
	private String companyName;
	@JsonIgnore
	private String secondaryEmail;
	
	@JsonIgnore
	private String password;
	private String ctyPhCode;
	private String	phone;
	private String extension;
	private String	otherPhone;
	@JsonIgnore
	private Date signupDate;
	@JsonIgnore
	private String representativeName;
	private CustomerType customerType;
	private SignupType signupType; 
	private ProductType productType;
	@JsonIgnore
	private ApplicationPlatForm	applicationPlatForm;
	@JsonIgnore
	private String	deviceId;
	@JsonIgnore
	private String	socialAccountId;
    @JsonIgnore
	private String	 notificationRegId;
	
	private String firstTimeLogin;
	private String superFanExpired;
	private List<UserAddress> billingAddress;
	private List<UserAddress> shippingAddress;
	private String referrerCode;
	private String custImagePath;
	private String lastName;
	//private String referralCode;
	private Boolean eventShareValidated;
	
	private String userId;
	
	private Integer quizCustomerLives;
	private Integer quizNoOfTicketsWon;
	private Double quizNoOfPointsWon;
	//private String quizShareCode;
	
	private Integer totalContestPlayed=0;
	private Integer totalContestWins=0;
	private Integer contestHighScore=0;
	private Integer rewardLifeContCount=0;
	
	private Boolean isOtpVerified=false;
	private Boolean isProomoCode=false;
	private Boolean isContestOrder=false;
	
	@JsonIgnore
	private Boolean isEmailed;
	
	@JsonIgnore
	private Integer brokerId;
	@JsonIgnore
	private Boolean isClient;
	@JsonIgnore
	private Boolean eligibleToShareRefCode;
	
	@JsonIgnore
	private Boolean ticketPurchased;
	
	@JsonIgnore
	private Date lastUpdatedDate;
	
	@JsonIgnore
	private Double rewardDollar;
	
	@JsonIgnore
	private Double thisContestRewards;
	
	private Integer rtfPoints;
	private Integer sfStars;
	private Integer magicWands;
	private Boolean isBlocked=false;
	private Integer loyaltyPoints;
	private String ageGroup;
	
	@JsonIgnore
	private Integer sfQuesLevel;
	
	

	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return userName
	 */
	@Column(name="username")
	public String getUserName() {
		if(null == userName || userName.isEmpty()){
			userName="";
		}
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * 
	 * @return email
	 */
	@Column(name="email")
	public String getEmail() {
		if(null == email || email.isEmpty()){
			email="";
		}
		return email;
	}
	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	@Column(name="company_name")
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	/**
	 * 
	 * @return password
	 */
	@Column(name="password")
	public String getPassword() {
		if(null == password || password.isEmpty()){
			password="";
		}
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Transient
	public void setEncryptPassword(String password) {
	    setPassword(PasswordUtil.generateEncrptedPassword(password));
	}
	
	/**
	 * 
	 * @return phone
	 */
	@Column(name="phone")
	public String getPhone() {
		if(null == phone ){
			phone = "";
		}
		return phone;
	}
	/**
	 * 
	 * @param phone , Phone no to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * 
	 * @return userId
	 */
	@Column(name="user_id")
	public String getUserId() {
		if(null == userId ){
			userId = "";
		}
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="product_type")
	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	@Column(name="cust_name")
	public String getCustomerName() {
		/*if((null == customerName || customerName.isEmpty()) && (userId != null && !userId.isEmpty())){
			customerName = userId;
		}*/
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Column(name="ext")
	public String getExtension() {
		if(null == extension ){
			extension = "";
		}
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	@Column(name="other_phone")
	public String getOtherPhone() {
		if(null == otherPhone ){
			otherPhone = "";
		}
		return otherPhone;
	}
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}
	
	@Column(name="signup_date")
	public Date getSignupDate() {
		return signupDate;
	}
	public void setSignupDate(Date signupDate) {
		this.signupDate = signupDate;
	}
	
	@Column(name="representativeName")
	public String getRepresentativeName() {
		if(null == representativeName || representativeName.isEmpty()){
			representativeName="";
		}
		return representativeName;
	}
	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="customer_type")
	public CustomerType getCustomerType() {
		return customerType;
	}
	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="signup_type")
	public SignupType getSignupType() {
		return signupType;
	}
	public void setSignupType(SignupType signupType) {
		this.signupType = signupType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="application_platform")
	public ApplicationPlatForm getApplicationPlatForm() {
		return applicationPlatForm;
	}
	public void setApplicationPlatForm(ApplicationPlatForm applicationPlatForm) {
		this.applicationPlatForm = applicationPlatForm;
	}
	
	@Column(name="device_id")
	public String getDeviceId() {
		if(null == deviceId || deviceId.isEmpty()){
			deviceId="";
		}
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	@Column(name="social_account_id")
	public String getSocialAccountId() {
		if(null == socialAccountId || socialAccountId.isEmpty()){
			socialAccountId="";
		}
		return socialAccountId;
	}
	public void setSocialAccountId(String socialAccountId) {
		this.socialAccountId = socialAccountId;
	}
	
	@Column(name="secondary_email")
	public String getSecondaryEmail() {
		if(null == secondaryEmail || secondaryEmail.isEmpty()){
			secondaryEmail="";
		}
		return secondaryEmail;
	}
	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}
	
	
	@Transient
	public String getFirstTimeLogin() {
		return firstTimeLogin;
	}
	public void setFirstTimeLogin(String firstTimeLogin) {
		this.firstTimeLogin = firstTimeLogin;
	}
	
	@Transient
	public String getSuperFanExpired() {
		if(null == superFanExpired || superFanExpired.isEmpty()){
			superFanExpired="No";
		}
		return superFanExpired;
	}
	public void setSuperFanExpired(String superFanExpired) {
		this.superFanExpired = superFanExpired;
	}
	@Transient
	public List<UserAddress> getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(List<UserAddress> billingAddress) {
		this.billingAddress = billingAddress;
	}
	@Transient
	public List<UserAddress> getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(List<UserAddress> shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	@Column(name = "referrer_code")
	public String getReferrerCode() {
		return referrerCode;
	}

	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}

	@Column(name = "cust_image_path")
	public String getCustImagePath() {
		return custImagePath;
	}

	public void setCustImagePath(String custImagePath) {
		this.custImagePath = custImagePath;
	}
	
	@Column(name="notification_reg_id")
	public String getNotificationRegId() {
		return notificationRegId;
	}

	public void setNotificationRegId(String notificationRegId) {
		this.notificationRegId = notificationRegId;
	}
	
	@Column(name="last_name")
	public String getLastName() {
		if(null == lastName){
			lastName="";
		}
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Transient
	public Boolean getEventShareValidated() {
		if(null == eventShareValidated){
			eventShareValidated= false;
		}
		return eventShareValidated;
	}

	public void setEventShareValidated(Boolean eventShareValidated) {
		this.eventShareValidated = eventShareValidated;
	}

	@Column(name = "is_mail_sent")
	public Boolean getIsEmailed() {
		return isEmailed;
	}

	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}

	@Column(name = "broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	@Column(name = "is_client")
	public Boolean getIsClient() {
		return isClient;
	}

	public void setIsClient(Boolean isClient) {
		this.isClient = isClient;
	}
	
	/*
	 @Column(name="cust_referral_code")
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	*/
	
	public static void main(String[] args) {
		 
		String enPass = PasswordUtil.generateEncrptedPassword("Ulaga123");
		
		System.out.println(enPass);
		
	}

	@Column(name = "eligible_to_share_ref_code")
	public Boolean getEligibleToShareRefCode() {
		if(null == eligibleToShareRefCode || !eligibleToShareRefCode){
			eligibleToShareRefCode = true;
		}
		return eligibleToShareRefCode;
	}

	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}

	@Column(name = "quiz_cust_lives")
	public Integer getQuizCustomerLives() {
		if(null == quizCustomerLives){
			quizCustomerLives=0;
		}
		return quizCustomerLives;
	}

	public void setQuizCustomerLives(Integer quizCustomerLives) {
		if(null == quizCustomerLives){
			this.quizCustomerLives=0;
		} else {
			this.quizCustomerLives = quizCustomerLives;
		}
	}

	@Column(name = "quiz_tickets_won")
	public Integer getQuizNoOfTicketsWon() {
		if(quizNoOfTicketsWon == null) {
			quizNoOfTicketsWon = 0;
		}
		return quizNoOfTicketsWon;
	}

	public void setQuizNoOfTicketsWon(Integer quizNoOfTicketsWon) {
		if(quizNoOfTicketsWon == null) {
			quizNoOfTicketsWon = 0;
		}
		this.quizNoOfTicketsWon = quizNoOfTicketsWon;
	}

	@Column(name = "quiz_points_won")
	public Double getQuizNoOfPointsWon() {
		if(quizNoOfPointsWon == null) {
			quizNoOfPointsWon = 0.0;
		}
		return quizNoOfPointsWon;
	}

	public void setQuizNoOfPointsWon(Double quizNoOfPointsWon) {
		if(quizNoOfPointsWon == null) {
			quizNoOfPointsWon = 0.0;
		}
		this.quizNoOfPointsWon = quizNoOfPointsWon;
	}
	
	
	/*@Column(name = "quiz_share_code")
	public String getQuizShareCode() {
		if(quizShareCode == null) {
			quizShareCode = "";
		}
		return quizShareCode;
	}

	public void setQuizShareCode(String quizShareCode) {
		this.quizShareCode = quizShareCode;
	}*/

	@Column(name = "total_contest_played")
	public Integer getTotalContestPlayed() {
		if(totalContestPlayed == null) {
			totalContestPlayed = 0;
		}
		return totalContestPlayed;
	}

	public void setTotalContestPlayed(Integer totalContestPlayed) {
		this.totalContestPlayed = totalContestPlayed;
	}

	@Column(name = "total_contest_wins")
	public Integer getTotalContestWins() {
		if(totalContestWins == null) {
			totalContestWins = 0;
		}
		return totalContestWins;
	}

	public void setTotalContestWins(Integer totalContestWins) {
		this.totalContestWins = totalContestWins;
	}

	@Column(name = "contest_high_score")
	public Integer getContestHighScore() {
		if(contestHighScore == null) {
			contestHighScore = 0;
		}
		return contestHighScore;
	}

	public void setContestHighScore(Integer contestHighScore) {
		this.contestHighScore = contestHighScore;
	}

	@Column(name = "last_updated_timestamp")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	@Column(name = "is_otp_verified")
	public Boolean getIsOtpVerified() {
		if(isOtpVerified == null) {
			isOtpVerified = false;
		}
		return isOtpVerified;
	}

	public void setIsOtpVerified(Boolean isOtpVerified) {
		this.isOtpVerified = isOtpVerified;
	}

	@Transient
	public Boolean getTicketPurchased() {
		if(id != null){
			try{
				Integer orderCount = DAORegistry.getQueryManagerDAO().getOrderCount(id);
				if(null != orderCount && orderCount > 0){
					ticketPurchased = true;
				}else{
					ticketPurchased = false;
				}
			}catch (Exception e) {
				ticketPurchased = false;
			}
			
		}
		return ticketPurchased;
	}

	public void setTicketPurchased(Boolean ticketPurchased) {
		this.ticketPurchased = ticketPurchased;
	}
	@Column(name = "is_promo_code")
	public Boolean getIsProomoCode() {
		if(isProomoCode == null) {
			isProomoCode = false;
		}
		return isProomoCode;
	}
	public void setIsProomoCode(Boolean isProomoCode) {
		this.isProomoCode = isProomoCode;
	}
	@Column(name = "is_contest_order")
	public Boolean getIsContestOrder() {
		if(isContestOrder == null) {
			isContestOrder = false;
		}
		return isContestOrder;
	}
	public void setIsContestOrder(Boolean isContestOrder) {
		this.isContestOrder = isContestOrder;
	}

	@Column(name = "reward_life_cont_count")
	public Integer getRewardLifeContCount() {
		if(rewardLifeContCount == null) {
			rewardLifeContCount = 0;
		}
		return rewardLifeContCount;
	}

	public void setRewardLifeContCount(Integer rewardLifeContCount) {
		this.rewardLifeContCount = rewardLifeContCount;
	}

	@Transient
	public Double getRewardDollar() {
		if(rewardDollar == null) {
			rewardDollar=0.00;
		}
		return rewardDollar;
	}

	public void setRewardDollar(Double rewardDollar) {
		this.rewardDollar = rewardDollar;
	}

	
	@Transient
	public Double getThisContestRewards() {
		if(thisContestRewards == null) {
			thisContestRewards=0.00;
		}
		return thisContestRewards;
	}

	public void setThisContestRewards(Double thisContestRewards) {
		this.thisContestRewards = thisContestRewards;
	}
	
	@Column(name="rtf_points")
	public Integer getRtfPoints() {
		if(rtfPoints == null) {
			rtfPoints = 0;
		}
		return rtfPoints;
	}
	public void setRtfPoints(Integer rtfPoints) {
		this.rtfPoints = rtfPoints;
	}

	@Column(name="sf_stars")
	public Integer getSfStars() {
		if(sfStars == null) {
			sfStars = 0;
		}
		return sfStars;
	}

	public void setSfStars(Integer sfStars) {
		this.sfStars = sfStars;
	}

	@Column(name="magic_wands")
	public Integer getMagicWands() {
		if(magicWands == null) {
			magicWands = 0;
		}
		return magicWands;
	}

	public void setMagicWands(Integer magicWands) {
		this.magicWands = magicWands;
	}

	@Column(name="country_phone_code")
	public String getCtyPhCode() {
		return ctyPhCode;
	}

	public void setCtyPhCode(String ctyPhCode) {
		this.ctyPhCode = ctyPhCode;
	}
	@Column(name="is_blocked")
	public Boolean getIsBlocked() {
		if(isBlocked == null) {
			isBlocked = false;
		}
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Column(name="sf_ques_level")
	public Integer getSfQuesLevel() {
		return sfQuesLevel;
	}

	public void setSfQuesLevel(Integer sfQuesLevel) {
		this.sfQuesLevel = sfQuesLevel;
	}

	

	@Column(name="rtf_cur_val_bal")
	public Integer getLoyaltyPoints() {
		if(loyaltyPoints == null) {
			loyaltyPoints = 0;
		}
		return loyaltyPoints;
	}
	public void setLoyaltyPoints(Integer loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	
	@Column(name="age_group")
	public String getAgeGroup() {
		return ageGroup;
	}
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}
	
	
	
	
}
