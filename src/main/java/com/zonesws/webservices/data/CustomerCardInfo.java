package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.Status;

@Entity
@Table(name = "stripe_customer_cards")
public class CustomerCardInfo implements Serializable{
	

	private Integer id;
	private Integer customerId;
	private String sCustId;
	private String sCustCardId;
	private String cardType;
	private String lastFourDigit;
	private Integer month;
	private Integer year;
	@JsonIgnore
	private Status status;
	@JsonIgnore
	private Date createDate;
	@JsonIgnore
	private Date lastUpdated;
	
	private String cardNo;
	@JsonIgnore
	private boolean showToCustomer;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	@Column(name = "card_type")
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	@Transient
	public String getCardNo() {
		if(lastFourDigit != null && !lastFourDigit.isEmpty()){
			cardNo = "XXXX XXXX XXXX "+lastFourDigit;
		}else{
			cardNo="";
		}
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	
	@Column(name = "card_last_four_digit")
	public String getLastFourDigit() {
		return lastFourDigit;
	}
	public void setLastFourDigit(String lastFourDigit) {
		this.lastFourDigit = lastFourDigit;
	}
	
	@Column(name = "expiry_month")
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	
	@Column(name = "expiry_year")
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	@Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="show_to_customer")
	public boolean getShowToCustomer() {
		return showToCustomer;
	}
	public void setShowToCustomer(boolean showToCustomer) {
		this.showToCustomer = showToCustomer;
	}
	
	
	@Column(name="stripe_cust_id")
	public String getsCustId() {
		return sCustId;
	}
	public void setsCustId(String sCustId) {
		this.sCustId = sCustId;
	}
	
	@Column(name="stripe_card_id")
	public String getsCustCardId() {
		return sCustCardId;
	}
	public void setsCustCardId(String sCustCardId) {
		this.sCustCardId = sCustCardId;
	}
	
	
	
	
}
