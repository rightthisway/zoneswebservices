package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * represents CustomerCassandra entity
 * @author Ulaganathan
 *
 */
 
@Entity
@Table(name="customer_contest_stats_from_cassandra")
public class CustomerCassandra  implements Serializable{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="contest_id")
	private Integer contestId;
	
	@Column(name="lives_before_contest")
	private Integer livesBeforeContest;  
	
	@Column(name="lives_used")
	private Integer livesUsed;  
	
	@Column(name="lives_after_contest")
	private Integer livesAfterContest;  
	
	@Column(name="created_datetime")
	private Date createdDate;
	
	@Column(name="contest_ques_rewards")
	private Double contestQusReward;
	
	@Column(name="contest_winner_rewards")
	private Double contestWinnerReward;
	
	@Column(name="cumulative_contest_rewards")
	private Double cumulativeContestRewards;
	
	@Column(name="que_rtf_points")
	private Integer questRtfPoints;
	
	@Column(name="rtf_points_bf_contest")
	private Integer rtfPointsBeforeContest;
	
	public CustomerCassandra(Integer customerId, Integer contestId, Date createdDate) {
		super();
		this.customerId = customerId;
		this.contestId = contestId;
		this.createdDate = createdDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public Integer getLivesBeforeContest() {
		return livesBeforeContest;
	}

	public void setLivesBeforeContest(Integer livesBeforeContest) {
		this.livesBeforeContest = livesBeforeContest;
	}

	public Integer getLivesUsed() {
		return livesUsed;
	}

	public void setLivesUsed(Integer livesUsed) {
		this.livesUsed = livesUsed;
	}

	public Integer getLivesAfterContest() {
		return livesAfterContest;
	}

	public void setLivesAfterContest(Integer livesAfterContest) {
		this.livesAfterContest = livesAfterContest;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Double getContestQusReward() {
		return contestQusReward;
	}

	public void setContestQusReward(Double contestQusReward) {
		this.contestQusReward = contestQusReward;
	}

	public Double getContestWinnerReward() {
		return contestWinnerReward;
	}

	public void setContestWinnerReward(Double contestWinnerReward) {
		this.contestWinnerReward = contestWinnerReward;
	}

	public Double getCumulativeContestRewards() {
		return cumulativeContestRewards;
	}

	public void setCumulativeContestRewards(Double cumulativeContestRewards) {
		this.cumulativeContestRewards = cumulativeContestRewards;
	}
	
	public Integer getQuestRtfPoints() {
		return questRtfPoints;
	}

	public void setQuestRtfPoints(Integer questRtfPoints) {
		this.questRtfPoints = questRtfPoints;
	}

	public Integer getRtfPointsBeforeContest() {
		return rtfPointsBeforeContest;
	}

	public void setRtfPointsBeforeContest(Integer rtfPointsBeforeContest) {
		this.rtfPointsBeforeContest = rtfPointsBeforeContest;
	}
	
}
 
	