package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * class to represent a CustomerCheckoutVisitOffer 
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="customer_checkout_visit_offer")
public class CustomerCheckoutVisitOffer implements Serializable {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	private ApplicationPlatForm platForm;
	
	@Column(name="is_flat_off")
	private Boolean isFlatOff;
	
	@Column(name="visited_event_discount_value")
	private Double visitedEventDiscValue;
	
	@Column(name="other_event_discount_value")
	private Double otherEventDiscValue;
	
	@Column(name="other_event_min_order_total")
	private Double otherEventMinOrderTotal;
	
	@Column(name="offer_expired_days")
	private Integer offerExpiredDays;
	
	@Column(name="updated_time")
	private Date updatedTime;
	
	@Column(name="status")
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}

	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}

	public Boolean getIsFlatOff() {
		return isFlatOff;
	}

	public void setIsFlatOff(Boolean isFlatOff) {
		this.isFlatOff = isFlatOff;
	}

	public Double getVisitedEventDiscValue() {
		return visitedEventDiscValue;
	}

	public void setVisitedEventDiscValue(Double visitedEventDiscValue) {
		this.visitedEventDiscValue = visitedEventDiscValue;
	}

	public Double getOtherEventDiscValue() {
		return otherEventDiscValue;
	}

	public void setOtherEventDiscValue(Double otherEventDiscValue) {
		this.otherEventDiscValue = otherEventDiscValue;
	}

	public Double getOtherEventMinOrderTotal() {
		return otherEventMinOrderTotal;
	}

	public void setOtherEventMinOrderTotal(Double otherEventMinOrderTotal) {
		this.otherEventMinOrderTotal = otherEventMinOrderTotal;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getOfferExpiredDays() {
		return offerExpiredDays;
	}

	public void setOfferExpiredDays(Integer offerExpiredDays) {
		this.offerExpiredDays = offerExpiredDays;
	}
	
}
