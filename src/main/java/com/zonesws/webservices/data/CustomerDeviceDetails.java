package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * Represents Customer App Device Details
 * @author Ulaganathan
 *
 */
@XStreamAlias("CustomerDeviceDetails")
@Entity
@Table(name="cust_app_device_details")
public class CustomerDeviceDetails  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private ApplicationPlatForm	applicationPlatForm;
	private String deviceId;
	private String notificationRegId;
	private String loginIp;
	private String status;
	private Date createdDate;
	private Date lastUpdated;
	@JsonIgnore
	private Integer artistId;
	
	@JsonIgnore
	private Date lastDeliveryDate;
	
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="cust_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="application_platform")
	public ApplicationPlatForm getApplicationPlatForm() {
		return applicationPlatForm;
	}
	public void setApplicationPlatForm(ApplicationPlatForm applicationPlatForm) {
		this.applicationPlatForm = applicationPlatForm;
	}
	
	
	@Column(name="device_id")
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
	@Column(name="login_ip")
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	
	
	@Column(name="notification_reg_id")
	public String getNotificationRegId() {
		return notificationRegId;
	}
	public void setNotificationRegId(String notificationRegId) {
		this.notificationRegId = notificationRegId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Transient
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="last_delivery_date")
	public Date getLastDeliveryDate() {
		return lastDeliveryDate;
	}
	public void setLastDeliveryDate(Date lastDeliveryDate) {
		this.lastDeliveryDate = lastDeliveryDate;
	}
	
	
	
}
