package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * class to represent a country 
 * @author hamin
 *
 */
@Entity
@Table(name="customer_favourite_events")

public class CustomerFavouriteEvent implements Serializable {
	
	private Integer id;
	private Integer customerId;
	private Integer eventId;
	private String status;
	public boolean isFavoriteEvent;
	

	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="isFavoriteEvent")
	public boolean getIsFavoriteEvent() {
		return isFavoriteEvent;
	}
	public void setIsFavoriteEvent(boolean isFavoriteEvent) {
		this.isFavoriteEvent = isFavoriteEvent;
	}
	
	
	
	
	
	

}
