package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.FeedBackType;
import com.zonesws.webservices.enums.ProductType;

/**
 * Represents Customer App Device Details
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="cust_feed_back_tracking")
public class CustomerFeedBackTracking  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private ApplicationPlatForm	applicationPlatForm;
	private FeedBackType feedBackType;
	private ProductType productType;
	private String ipAddress;
	private String message;
	private Boolean isFeedBackEmailSent;
	private Boolean isCustEmailSent;
	private Date createdDate;
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="cust_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	public ApplicationPlatForm getApplicationPlatForm() {
		return applicationPlatForm;
	}
	public void setApplicationPlatForm(ApplicationPlatForm applicationPlatForm) {
		this.applicationPlatForm = applicationPlatForm;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="feedback_type")
	public FeedBackType getFeedBackType() {
		return feedBackType;
	}
	public void setFeedBackType(FeedBackType feedBackType) {
		this.feedBackType = feedBackType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="product_type")
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name="message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Column(name="is_fb_email_sent")
	public Boolean getIsFeedBackEmailSent() {
		return isFeedBackEmailSent;
	}
	public void setIsFeedBackEmailSent(Boolean isFeedBackEmailSent) {
		this.isFeedBackEmailSent = isFeedBackEmailSent;
	}
	
	@Column(name="is_cust_email_sent")
	public Boolean getIsCustEmailSent() {
		return isCustEmailSent;
	}
	public void setIsCustEmailSent(Boolean isCustEmailSent) {
		this.isCustEmailSent = isCustEmailSent;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
		
}
