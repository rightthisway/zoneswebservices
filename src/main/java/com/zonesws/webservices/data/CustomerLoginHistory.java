package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.SignupType;

/**
 * represents customer login history entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("CustomerLoginHistory")
@Entity
@Table(name="cust_login_history")
public class CustomerLoginHistory  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private ApplicationPlatForm	applicationPlatForm;
	private SignupType loginType;
	private String deviceId;
	private String notificationRegId;
	private String loginIp;
	private String socialAccessToken;
	@JsonIgnore
	private Date lastLoginTime;
	
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="cust_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="application_platform")
	public ApplicationPlatForm getApplicationPlatForm() {
		return applicationPlatForm;
	}
	public void setApplicationPlatForm(ApplicationPlatForm applicationPlatForm) {
		this.applicationPlatForm = applicationPlatForm;
	}
	
	
	@Column(name="last_login_date")
	public Date getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	
	@Column(name="device_id")
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	@Column(name="social_access_token")
	public String getSocialAccessToken() {
		return socialAccessToken;
	}
	public void setSocialAccessToken(String socialAccessToken) {
		this.socialAccessToken = socialAccessToken;
	}
	
	@Column(name="login_ip")
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="login_type")
	public SignupType getLoginType() {
		return loginType;
	}
	public void setLoginType(SignupType loginType) {
		this.loginType = loginType;
	}
	@Column(name="notification_reg_id")
	public String getNotificationRegId() {
		return notificationRegId;
	}
	public void setNotificationRegId(String notificationRegId) {
		this.notificationRegId = notificationRegId;
	}
}
