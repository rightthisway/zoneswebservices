package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TicketUtil;

/**
 * represents customer entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("CustomerLoyalty")
@Entity
@Table(name="cust_loyalty_reward_info")
public class CustomerLoyalty  implements Serializable{
	
	@JsonIgnore
	private Integer id;
	private Integer customerId;
	
	private String curRewardPointBalance;
	private String activePoints;
	private String pendingPoints;
	private String totalEarnedPoints;
	private String totalSpentPoints;
	private String latestEarnedPoints;
	private String latestSpentPoints;
	private String voidedPoints;
	private String revertedPoints;
	
	private Double activePointsAsDouble;
	private Double pendingPointsAsDouble;
	private Double totalEarnedPointsAsDouble;
	private Double totalSpentPointsAsDouble;
	private Double latestEarnedPointsAsDouble;
	private Double latestSpentPointsAsDouble;
	private Double voidedPointsAsDouble;
	private Double revertedPointsAsDouble;
	
	private Double activeRewardDollers;
	private Double dollerConversion;
	@JsonIgnore
	private Date lastUpdate;
	
	@JsonIgnore
	private Date lastNotifiedTime;
	
	@JsonIgnore
	private String notifiedMessage;
	
	private String contestRewardDollars;
	private Double contestRewardDollarsDouble;
	
	private Double totalReferralDollars;
	private Double lastReferralDollars;
	
	private Double totalAffiliateReferralDollars;
	private Double lastAffiliateReferralDollars;
	
	private String totalReferralDollarsStr;
	private String totalAffiliateReferralDollarsStr;
	
	private Boolean showReferralReward;
	private Boolean showAffiliateReward;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="cust_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Transient
	public Double getDollerConversion() {
		LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
		dollerConversion = loyaltySettings.getDollerConversion();
		return dollerConversion;
	}
	public void setDollerConversion(Double dollerConversion) {
		this.dollerConversion = dollerConversion;
	}
	
	
	@Column(name="updated_time" )
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
	@Column(name="active_reward_points" )
	public Double getActivePointsAsDouble() {
		return activePointsAsDouble;
	}
	public void setActivePointsAsDouble(Double activePointsAsDouble) {
		this.activePointsAsDouble = activePointsAsDouble;
	}
	
	@Column(name="total_earn_points" )
	public Double getTotalEarnedPointsAsDouble() {
		return totalEarnedPointsAsDouble;
	}
	public void setTotalEarnedPointsAsDouble(Double totalEarnedPointsAsDouble) {
		this.totalEarnedPointsAsDouble = totalEarnedPointsAsDouble;
	}
	
	@Column(name="total_spent_points" )
	public Double getTotalSpentPointsAsDouble() {
		return totalSpentPointsAsDouble;
	}
	public void setTotalSpentPointsAsDouble(Double totalSpentPointsAsDouble) {
		this.totalSpentPointsAsDouble = totalSpentPointsAsDouble;
	}
	
	@Column(name="latest_earn_points" )
	public Double getLatestEarnedPointsAsDouble() {
		return latestEarnedPointsAsDouble;
	}
	public void setLatestEarnedPointsAsDouble(Double latestEarnedPointsAsDouble) {
		this.latestEarnedPointsAsDouble = latestEarnedPointsAsDouble;
	}
	
	@Column(name="latest_spent_points" )
	public Double getLatestSpentPointsAsDouble() {
		return latestSpentPointsAsDouble;
	}
	public void setLatestSpentPointsAsDouble(Double latestSpentPointsAsDouble) {
		this.latestSpentPointsAsDouble = latestSpentPointsAsDouble;
	}
	
	@Transient
	public Double getActiveRewardDollers() {
		if(null != activePointsAsDouble){
			activeRewardDollers =  RewardConversionUtil.getRewardDoller(activePointsAsDouble);
		}else{
			activeRewardDollers=0.00;
		}
		return activeRewardDollers;
	}
	
	public void setActiveRewardDollers(Double activeRewardDollers) {
		this.activeRewardDollers = activeRewardDollers;
	}
	
	@Column(name="pending_rewards")
	public Double getPendingPointsAsDouble() {
		return pendingPointsAsDouble;
	}
	public void setPendingPointsAsDouble(Double pendingPointsAsDouble) {
		this.pendingPointsAsDouble = pendingPointsAsDouble;
	}
	
	@Column(name="voided_reward_points")
	public Double getVoidedPointsAsDouble() {
		if(null == voidedPointsAsDouble){
			voidedPointsAsDouble = 0.0;
		}
		return voidedPointsAsDouble;
	}
	public void setVoidedPointsAsDouble(Double voidedPointsAsDouble) {
		this.voidedPointsAsDouble = voidedPointsAsDouble;
	}
	
	@Column(name="reverted_spent_points")
	public Double getRevertedPointsAsDouble() {
		if(null == revertedPointsAsDouble){
			revertedPointsAsDouble = 0.0;
		}
		return revertedPointsAsDouble;
	}
	public void setRevertedPointsAsDouble(Double revertedPointsAsDouble) {
		this.revertedPointsAsDouble = revertedPointsAsDouble;
	}
	
	@Transient
	public String getActivePoints() {
		if(activePointsAsDouble != null ){
			try {
				activePoints =  TicketUtil.getRoundedValueString(activePointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return activePoints;
	}
	public void setActivePoints(String activePoints) {
		this.activePoints = activePoints;
	}
	
	@Transient
	public String getPendingPoints() {
		if(pendingPointsAsDouble != null){
			try {
				pendingPoints =  TicketUtil.getRoundedValueString(pendingPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pendingPoints;
	}
	public void setPendingPoints(String pendingPoints) {
		this.pendingPoints = pendingPoints;
	}
	
	@Transient
	public String getTotalEarnedPoints() {
		if(totalEarnedPointsAsDouble != null){
			try {
				totalEarnedPoints =  TicketUtil.getRoundedValueString(totalEarnedPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return totalEarnedPoints;
	}
	public void setTotalEarnedPoints(String totalEarnedPoints) {
		this.totalEarnedPoints = totalEarnedPoints;
	}
	
	@Transient
	public String getTotalSpentPoints() {
		if(totalSpentPointsAsDouble != null){
			try {
				totalSpentPoints =  TicketUtil.getRoundedValueString(totalSpentPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return totalSpentPoints;
	}
	public void setTotalSpentPoints(String totalSpentPoints) {
		this.totalSpentPoints = totalSpentPoints;
	}
	
	@Transient
	public String getLatestEarnedPoints() {
		if(latestEarnedPointsAsDouble != null){
			try {
				latestEarnedPoints =  TicketUtil.getRoundedValueString(latestEarnedPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return latestEarnedPoints;
	}
	public void setLatestEarnedPoints(String latestEarnedPoints) {
		this.latestEarnedPoints = latestEarnedPoints;
	}
	
	@Transient
	public String getLatestSpentPoints() {
		if(latestSpentPointsAsDouble != null){
			try {
				latestSpentPoints =  TicketUtil.getRoundedValueString(latestSpentPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return latestSpentPoints;
	}
	public void setLatestSpentPoints(String latestSpentPoints) {
		this.latestSpentPoints = latestSpentPoints;
	}
	
	@Transient
	public String getVoidedPoints() {
		if(voidedPointsAsDouble != null){
			try {
				voidedPoints =  TicketUtil.getRoundedValueString(voidedPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return voidedPoints;
	}
	public void setVoidedPoints(String voidedPoints) {
		this.voidedPoints = voidedPoints;
	}
	
	@Transient
	public String getRevertedPoints() {
		if(revertedPointsAsDouble != null ){
			try {
				revertedPoints =  TicketUtil.getRoundedValueString(revertedPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return revertedPoints;
	}
	public void setRevertedPoints(String revertedPoints) {
		this.revertedPoints = revertedPoints;
	}
	
	@Transient
	public String getCurRewardPointBalance() {
		
		if(pendingPointsAsDouble !=  null && activePointsAsDouble != null){
			try {
				curRewardPointBalance = TicketUtil.getRoundedValueString(activePointsAsDouble+pendingPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return curRewardPointBalance;
	}
	public void setCurRewardPointBalance(String curRewardPointBalance) {
		this.curRewardPointBalance = curRewardPointBalance;
	}
	
	@Column(name="last_notified_time")
	public Date getLastNotifiedTime() {
		return lastNotifiedTime;
	}
	public void setLastNotifiedTime(Date lastNotifiedTime) {
		this.lastNotifiedTime = lastNotifiedTime;
	}
	
	@Column(name="notified_message")
	public String getNotifiedMessage() {
		return notifiedMessage;
	}
	public void setNotifiedMessage(String notifiedMessage) {
		this.notifiedMessage = notifiedMessage;
	}
	
	@Transient
	public String getContestRewardDollars() {
		if(getContestRewardDollarsDouble() != null){
			try {
				contestRewardDollars =  TicketUtil.getRoundedValueString(getContestRewardDollarsDouble());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			contestRewardDollars="0.00";
		}
		return contestRewardDollars;
	}
	@Transient
	public void setContestRewardDollars(String contestRewardDollars) {
		this.contestRewardDollars = contestRewardDollars;
	}
	@Transient
	public Double getContestRewardDollarsDouble() {
		if(null == contestRewardDollarsDouble){
			contestRewardDollarsDouble = 0.00;
		}
		return contestRewardDollarsDouble;
	}
	@Transient
	public void setContestRewardDollarsDouble(Double contestRewardDollarsDouble) {
		this.contestRewardDollarsDouble = contestRewardDollarsDouble;
	}
	
	@Column(name="total_referral_points")
	public Double getTotalReferralDollars() {
		if(null == totalReferralDollars) {
			totalReferralDollars = 0.00;
		}
		return totalReferralDollars;
	}
	public void setTotalReferralDollars(Double totalReferralDollars) {
		this.totalReferralDollars = totalReferralDollars;
	}
	
	@Column(name="last_referral_points")
	public Double getLastReferralDollars() {
		if(null == lastReferralDollars) {
			lastReferralDollars = 0.00;
		}
		return lastReferralDollars;
	}
	public void setLastReferralDollars(Double lastReferralDollars) {
		this.lastReferralDollars = lastReferralDollars;
	}
	
	@Transient
	public String getTotalReferralDollarsStr() {
		if(getTotalReferralDollars() != null){
			try {
				totalReferralDollarsStr =  TicketUtil.getRoundedValueString(getTotalReferralDollars());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			totalReferralDollarsStr="0.00";
		}
		return totalReferralDollarsStr;
	}
	public void setTotalReferralDollarsStr(String totalReferralDollarsStr) {
		this.totalReferralDollarsStr = totalReferralDollarsStr;
	}
	
	@Transient
	public Boolean getShowReferralReward() {
		if(null == showReferralReward) {
			showReferralReward = false;
		}
		return showReferralReward;
	}
	public void setShowReferralReward(Boolean showReferralReward) {
		this.showReferralReward = showReferralReward;
	}
	
	@Column(name="total_affiliate_referral_points")
	public Double getTotalAffiliateReferralDollars() {
		if(null == totalAffiliateReferralDollars) {
			totalAffiliateReferralDollars = 0.00;
		}
		return totalAffiliateReferralDollars;
	}
	public void setTotalAffiliateReferralDollars(Double totalAffiliateReferralDollars) {
		this.totalAffiliateReferralDollars = totalAffiliateReferralDollars;
	}
	
	@Column(name="last_affiliate_referral_points")
	public Double getLastAffiliateReferralDollars() {
		if(null == lastAffiliateReferralDollars) {
			lastAffiliateReferralDollars = 0.00;
		}
		return lastAffiliateReferralDollars;
	}
	public void setLastAffiliateReferralDollars(Double lastAffiliateReferralDollars) {
		this.lastAffiliateReferralDollars = lastAffiliateReferralDollars;
	}
	
	@Transient
	public String getTotalAffiliateReferralDollarsStr() {
		if(getTotalAffiliateReferralDollars() != null){
			try {
				totalAffiliateReferralDollarsStr =  TicketUtil.getRoundedValueString(getTotalAffiliateReferralDollars());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			totalAffiliateReferralDollarsStr="0.00";
		}
		return totalAffiliateReferralDollarsStr;
	}
	public void setTotalAffiliateReferralDollarsStr(String totalAffiliateReferralDollarsStr) {
		this.totalAffiliateReferralDollarsStr = totalAffiliateReferralDollarsStr;
	}
	
	@Transient
	public Boolean getShowAffiliateReward() {
		if(null == showAffiliateReward) {
			showAffiliateReward = false;
		}
		return showAffiliateReward;
	}
	public void setShowAffiliateReward(Boolean showAffiliateReward) {
		this.showAffiliateReward = showAffiliateReward;
	}
	
	
	
}
