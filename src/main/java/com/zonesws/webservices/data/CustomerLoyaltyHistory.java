package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.RewardStatus;
import com.zonesws.webservices.utils.TicketUtil;

/**
 * represents customer entity
 * @author ulaganathan
 *
 */
@XStreamAlias("CustomerLoyaltyHistory")
@Entity
@Table(name="cust_loyalty_reward_history")
public class CustomerLoyaltyHistory  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer orderId;
	private Double orderTotal;
	
	private String pointsEarned;
	private String pointsSpent;
	private String initialRewardPoints;
	private String balanceRewardPoints;
	
	private Double pointsEarnedAsDouble;
	private Double pointsSpentAsDouble;
	private Double initialRewardPointsAsDouble;
	private Double balanceRewardPointsAsDouble;
	
	private Double rewardConversionRate;
	private Double rewardEarnAmount;
	private Double rewardSpentAmount;
	private Double initialRewardAmount;
	private Double balanceRewardAmount; 
	private Date createDate;
	private Date updatedDate;
	private Double dollarConversionRate;
	private OrderType orderType;
	private RewardStatus rewardStatus;
	@JsonIgnore
	private String eventDate;
	@JsonIgnore
	private String eventTime;
	
	@JsonIgnore
	private Boolean isRewardsEmailSent;
	@JsonIgnore
	private String emailDescription;
	
	private Integer tireOneCustId; 
	private Integer contestId; 
	private Integer ansQuestion; 
	private Double tireOneCustEarnPoints; 
	
                                    
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="Order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	@Column(name="reward_conv_rate")
	public Double getRewardConversionRate() {
		return rewardConversionRate;
	}
	public void setRewardConversionRate(Double rewardConversionRate) {
		this.rewardConversionRate = rewardConversionRate;
	}
	
	@Column(name="reward_earn_amount")
	public Double getRewardEarnAmount() {
		return rewardEarnAmount;
	}
	public void setRewardEarnAmount(Double rewardEarnAmount) {
		this.rewardEarnAmount = rewardEarnAmount;
	}
	
	@Column(name="reward_spent_amount")
	public Double getRewardSpentAmount() {
		return rewardSpentAmount;
	}
	public void setRewardSpentAmount(Double rewardSpentAmount) {
		this.rewardSpentAmount = rewardSpentAmount;
	}
	
	@Column(name="initial_reward_amount")
	public Double getInitialRewardAmount() {
		return initialRewardAmount;
	}
	public void setInitialRewardAmount(Double initialRewardAmount) {
		this.initialRewardAmount = initialRewardAmount;
	}
	
	@Column(name="balance_reward_amount")
	public Double getBalanceRewardAmount() {
		return balanceRewardAmount;
	}
	public void setBalanceRewardAmount(Double balanceRewardAmount) {
		this.balanceRewardAmount = balanceRewardAmount;
	}
	
	
	
	@Column(name = "dollar_conv_rate")
	public Double getDollarConversionRate() {
		return dollarConversionRate;
	}
	public void setDollarConversionRate(Double dollarConversionRate) {
		this.dollarConversionRate = dollarConversionRate;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="order_type")
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public RewardStatus getRewardStatus() {
		return rewardStatus;
	}
	public void setRewardStatus(RewardStatus rewardStatus) {
		this.rewardStatus = rewardStatus;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="created_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Transient
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	@Transient
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="points_earned")
	public Double getPointsEarnedAsDouble() {
		return pointsEarnedAsDouble;
	}
	public void setPointsEarnedAsDouble(Double pointsEarnedAsDouble) {
		this.pointsEarnedAsDouble = pointsEarnedAsDouble;
	}
	
	@Column(name="points_spent")
	public Double getPointsSpentAsDouble() {
		return pointsSpentAsDouble;
	}
	public void setPointsSpentAsDouble(Double pointsSpentAsDouble) {
		this.pointsSpentAsDouble = pointsSpentAsDouble;
	}
	
	@Column(name="initial_reward_points")
	public Double getInitialRewardPointsAsDouble() {
		return initialRewardPointsAsDouble;
	}
	public void setInitialRewardPointsAsDouble(Double initialRewardPointsAsDouble) {
		this.initialRewardPointsAsDouble = initialRewardPointsAsDouble;
	}
	
	@Column(name="balance_reward_points")
	public Double getBalanceRewardPointsAsDouble() {
		return balanceRewardPointsAsDouble;
	}
	public void setBalanceRewardPointsAsDouble(Double balanceRewardPointsAsDouble) {
		this.balanceRewardPointsAsDouble = balanceRewardPointsAsDouble;
	}
	
	@Transient
	public String getPointsEarned(){
		if(null != pointsEarnedAsDouble ){
			try {
				pointsEarned = TicketUtil.getRoundedValueString(pointsEarnedAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pointsEarned;
	}
	public void setPointsEarned(String pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	
	@Transient
	public String getPointsSpent() {
		if(null != pointsSpentAsDouble && pointsSpentAsDouble > 0 ){
			try {
				pointsSpent = TicketUtil.getRoundedValueString(pointsSpentAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pointsSpent;
	}
	public void setPointsSpent(String pointsSpent) {
		this.pointsSpent = pointsSpent;
	}
	
	@Transient
	public String getInitialRewardPoints() {
		if(null != initialRewardPointsAsDouble && initialRewardPointsAsDouble > 0 ){
			try {
				initialRewardPoints = TicketUtil.getRoundedValueString(initialRewardPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return initialRewardPoints;
	}
	public void setInitialRewardPoints(String initialRewardPoints) {
		this.initialRewardPoints = initialRewardPoints;
	}
	
	@Transient
	public String getBalanceRewardPoints() {
		if(null != balanceRewardPointsAsDouble && balanceRewardPointsAsDouble > 0 ){
			try {
				balanceRewardPoints = TicketUtil.getRoundedValueString(balanceRewardPointsAsDouble);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return balanceRewardPoints;
	}
	public void setBalanceRewardPoints(String balanceRewardPoints) {
		this.balanceRewardPoints = balanceRewardPoints;
	}
	
	@Column(name="is_email_sent")
	public Boolean getIsRewardsEmailSent() {
		return isRewardsEmailSent;
	}
	public void setIsRewardsEmailSent(Boolean isRewardsEmailSent) {
		this.isRewardsEmailSent = isRewardsEmailSent;
	}
	
	@Column(name="email_description")
	public String getEmailDescription() {
		return emailDescription;
	}
	public void setEmailDescription(String emailDescription) {
		this.emailDescription = emailDescription;
	}
	
	@Column(name="tireone_cust_id")
	public Integer getTireOneCustId() {
		return tireOneCustId;
	}
	public void setTireOneCustId(Integer tireOneCustId) {
		this.tireOneCustId = tireOneCustId;
	}
	
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="ans_question")
	public Integer getAnsQuestion() {
		return ansQuestion;
	}
	public void setAnsQuestion(Integer ansQuestion) {
		this.ansQuestion = ansQuestion;
	}
	
	@Column(name="tireone_cust_earn_points")
	public Double getTireOneCustEarnPoints() {
		return tireOneCustEarnPoints;
	}
	public void setTireOneCustEarnPoints(Double tireOneCustEarnPoints) {
		this.tireOneCustEarnPoints = tireOneCustEarnPoints;
	}
	
	
}
