package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.rtfquiz.webservices.utils.list.CustomerNotificationDetails;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * Represents CustomerNotificationDeliveryTracking
 * @author Tamil
 *
 */
@XStreamAlias("CustomerNotificationDeliveryTracking")
@Entity
@Table(name="cust_notification_delivery_tracking")
public class CustomerNotificationDeliveryTracking  implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private String	applicationPlatForm;
	private String deviceId;
	private String notificationRegId;
	private String status;
	private String notificationMsg;
	private String errorMsg;
	private Date createdDate;
	private Date lastUpdated;
	private Date lastSentDate;
	private Integer contestId;
	private String senderType;
	
	
	public CustomerNotificationDeliveryTracking() {
	}
	public CustomerNotificationDeliveryTracking(CustomerNotificationDetails notificationDetails) {
		this.customerId = notificationDetails.getCustomerId();
		this.applicationPlatForm = notificationDetails.getApplicationPlatForm();
		this.deviceId = notificationDetails.getDeviceId();
		this.notificationRegId =notificationDetails.getNotificationRegId();
	}
	
	public CustomerNotificationDeliveryTracking(CustomerDeviceDetails notificationDetails) {
		this.customerId = notificationDetails.getCustomerId();
		this.applicationPlatForm = notificationDetails.getApplicationPlatForm().toString();
		this.deviceId = notificationDetails.getDeviceId();
		this.notificationRegId =notificationDetails.getNotificationRegId();
	}
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="cust_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="application_platform")
	public String getApplicationPlatForm() {
		return applicationPlatForm;
	}
	public void setApplicationPlatForm(String applicationPlatForm) {
		this.applicationPlatForm = applicationPlatForm;
	}
	
	
	@Column(name="device_id")
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
	@Column(name="notification_reg_id")
	public String getNotificationRegId() {
		return notificationRegId;
	}
	public void setNotificationRegId(String notificationRegId) {
		this.notificationRegId = notificationRegId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name="last_sent_date")
	public Date getLastSentDate() {
		return lastSentDate;
	}
	public void setLastSentDate(Date lastSentDate) {
		this.lastSentDate = lastSentDate;
	}
	
	@Column(name="notification_msg")
	public String getNotificationMsg() {
		return notificationMsg;
	}
	public void setNotificationMsg(String notificationMsg) {
		this.notificationMsg = notificationMsg;
	}
	@Column(name="error_msg")
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
	@Column(name="sender_type")
	public String getSenderType() {
		return senderType;
	}
	public void setSenderType(String senderType) {
		this.senderType = senderType;
	}
	
	
	
}
