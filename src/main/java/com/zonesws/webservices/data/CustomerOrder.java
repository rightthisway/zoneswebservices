package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PartialPaymentMethod;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.SecondaryOrderType;
import com.zonesws.webservices.utils.TicketUtil;

@Entity
@Table(name = "customer_order")
public class CustomerOrder implements Serializable{

	private static final long serialVersionUID = -7941769011539361245L;
	
	private Integer id;
	private OrderStatus status;
	private Double originalOrderTotal;
	private String orderTotal;
	private String primaryPayAmt;
	private String secondaryPayAmt;
	private String thirdPayAmt;
	private Double orderTotalAsDouble;
	private Double primaryPayAmtAsDouble;
	private Double secondaryPayAmtAsDouble;
	private Double thirdPayAmtAsDouble;
	private Double discountAmount;
	private String shippingMethod;
	@JsonIgnore
	private Date shippingDateTemp;
	private String shippingDate;
	private String fedExTrackingNo;
	private PaymentMethod primaryPaymentMethod;
	private String primaryTransactionId;
	private PartialPaymentMethod secondaryPaymentMethod;
	private String secondaryTransactionId;
	private PartialPaymentMethod thirdPaymentMethod;
	private String thirdTransactionId;
	private Integer eventId;
	private String eventName;
	@JsonIgnore
	private Date eventDateTemp;
	private String eventDate;
	private Date eventTime;
	private String eventTimeStr;
	private Integer venueId;
	private String venueName;
	private String venueCity;
	private String venueState;
	private String venueCountry;
	private String venueCategory;
	private Boolean isFavoriteEvent;
	private String shareLinkUrl;
	private String desktopShareLinkUrl;
	private Integer categoryTicketGroupId;
	private Integer qty;
	private String svgKey;
	private String section;
	private String actualSection;
	private String actualRow;
	private String actualSeat;
	private Boolean isOrderFullFilled;
	@JsonIgnore
	private Boolean isLongSale;
	private String seatLow;
	private String seatHigh;
	private Boolean showLongInfo;
	private String ticketPrice;
	private String ticketSoldPrice;
	private Double ticketPriceAsDouble;
	private Double ticketSoldPriceAsDouble;
	private String sectionDescription;
	private String orderDeliveryNote;
	private String progressPopupMsg;
	private String ticketGroupNotes;
	private Customer customer;
	@JsonIgnore
	private Date createDateTemp;
	private String createDate;
	@JsonIgnore
	private Date lastUpdatedDateTemp;
	private String lastUpdatedDate;
	@JsonIgnore
	private Date acceptedDateTemp;
	private String acceptedDate;
	private String acceptedBy;
	private ProductType productType;
	private ApplicationPlatForm appPlatForm;
	private CustomerOrderDetail customerOrderDetail;
	private CustomerOrderCreditCard cardDetails;
	private CustomerLoyaltyHistory customerLoyaltyHistory;
	@JsonIgnore
	private Boolean isInvoiceSent; 
	private String buttonOption;
	@JsonIgnore
	private DateFormat tf = new SimpleDateFormat("hh:mm aa");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	private SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private String ticketDownloadUrl;
	private OrderType orderType;
	@JsonIgnore
	private Date lastNotified;
	private Boolean showInfoButton;
	private String deliveryInfo;
	private List<InvoiceTicketAttachment> invoiceTicketAttachment;
	private String secondaryOrderId;
	private SecondaryOrderType secondaryOrdertype;
	private Boolean cjePackageEnabled;
	private String cjePackageNote;
	private Double cjeTicketPrice;
	private Double cjeTicketPoints;
	private Double cjePackagePrice;
	private Double cjePackagePoints;
	private Double cjeRequirePoints;
	
	private Boolean showCJEOrder;
	private String cjeOrderInfo;
	private String cjeOrderNo;
	private String viewOrderType;
	private String taxes;
	private Double taxesAsDouble;
	private String rewardPoints;
	private String svgText;
	private String isMapWithSvg;
	private String svgMapPath;
	private String colorCode;
	
	@JsonIgnore
	private String orderToEmail;
	@JsonIgnore
	private Boolean isTicketDownloadSent;
	@JsonIgnore
	private String tdToEmail;
	@JsonIgnore
	private Boolean isOrderCancelSent;
	@JsonIgnore
	private String ocToEmail;
	@JsonIgnore
	private Integer brokerId;
	private Double brokerServicePerc;
	
	private Boolean isPromoCodeApplied;
	private String promotionalCode;
	private String promotionalCodeMessage;
	private Boolean showDiscPriceArea;
	private Double normalTixPrice;
	private String normalTixPriceStr;
	private Double discountedTixPrice;
	private String discountedTixPriceStr;
	
	//private List<OrderTicketGroup> orderTicketGroups;
	
	private String zone;
	private String realSection;
	private String realRow;
	private String realSeat;
	private String realTicketPrice;
	private String totalServiceFees;
	private String singleTixServiceFees;
	@JsonIgnore
	private Boolean ticketOnDayBeforeTheEvent;
	
	private List<OrderPaymentBreakup> orderPaymentBreakupList;
	
	private Boolean isShippingRequired;
	private String shippingRequiredMsg;
	
	private Boolean isTriviaOrder;
	
	private String convRateString;
	
	private String description;
	
	private String gfCardValue;
	
	/*private Boolean isIOS;*/
	
	private String redeemText;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@ManyToOne
	@JoinColumn(name = "customer_id")
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@Column(name = "quantity")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public OrderStatus getStatus() {
		return status;
	}
	public void setStatus(OrderStatus status) {
		this.status = status;
	}
	
	
	@Column(name = "section")
	public String getSection() {
		if(null == section){
			section ="-";
		}
		return section;
	}
	
	public void setSection(String section) {
		this.section = section;
	}
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	@Column(name = "event_name")
	public String getEventName() {
		if(null != eventName && !eventName.isEmpty()){
			eventName = eventName.replaceAll("�", "-");
		}
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	@Column(name = "event_date")
	public Date getEventDateTemp() {
		return eventDateTemp;
	}
	public void setEventDateTemp(Date eventDateTemp) {
		this.eventDateTemp = eventDateTemp;
	}
	@Column(name = "event_time")
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	@Column(name = "venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	@Column(name = "venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name = "discount_amt")
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	@Column(name = "ticket_group_notes")
	public String getTicketGroupNotes() {
		return ticketGroupNotes;
	}
	public void setTicketGroupNotes(String ticketGroupNotes) {
		this.ticketGroupNotes = ticketGroupNotes;
	}
	@Column(name = "created_date")
	public Date getCreateDateTemp() {
		return createDateTemp;
	}
	public void setCreateDateTemp(Date createDateTemp) {
		this.createDateTemp = createDateTemp;
	}
	@Column(name = "last_updated")
	public Date getLastUpdatedDateTemp() {
		return lastUpdatedDateTemp;
	}
	public void setLastUpdatedDateTemp(Date lastUpdatedDateTemp) {
		this.lastUpdatedDateTemp = lastUpdatedDateTemp;
	}
	@Column(name = "accepted_date")
	public Date getAcceptedDateTemp() {
		return acceptedDateTemp;
	}
	public void setAcceptedDateTemp(Date acceptedDateTemp) {
		this.acceptedDateTemp = acceptedDateTemp;
	}
	@Column(name = "accepted_by")
	public String getAcceptedBy() {
		return acceptedBy;
	}
	public void setAcceptedBy(String acceptedBy) {
		this.acceptedBy = acceptedBy;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "product_type")
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	@Column(name = "shipping_method")
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	@Column(name = "venue_city")
	public String getVenueCity() {
		return venueCity;
	}

	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	@Column(name = "venue_state")
	public String getVenueState() {
		return venueState;
	}

	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	@Column(name = "venue_country")
	public String getVenueCountry() {
		return venueCountry;
	}	
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	@Column(name = "section_description")
	public String getSectionDescription() {
		if(null == sectionDescription || sectionDescription.isEmpty()){
			sectionDescription = "<font color=#012e4f>No zone description is not available</font>";
		}
		return sectionDescription;
	}
	public void setSectionDescription(String sectionDescription) {
		this.sectionDescription = sectionDescription;
	}
	@Transient
	public CustomerOrderDetail getCustomerOrderDetail() {
		return customerOrderDetail;
	}
	public void setCustomerOrderDetail(CustomerOrderDetail customerOrderDetail) {
		this.customerOrderDetail = customerOrderDetail;
	}
	
	@Transient
	public CustomerLoyaltyHistory getCustomerLoyaltyHistory() {
		return customerLoyaltyHistory;
	}
	public void setCustomerLoyaltyHistory(
			CustomerLoyaltyHistory customerLoyaltyHistory) {
		this.customerLoyaltyHistory = customerLoyaltyHistory;
	}
	
	
	@Column(name="original_order_total")
	public Double getOriginalOrderTotal() {
		return originalOrderTotal;
	}
	public void setOriginalOrderTotal(Double originalOrderTotal) {
		this.originalOrderTotal = originalOrderTotal;
	}
	
	@Column(name="shipping_date")
	public Date getShippingDateTemp() {
		return shippingDateTemp;
	}
	public void setShippingDateTemp(Date shippingDateTemp) {
		this.shippingDateTemp = shippingDateTemp;
	}
	
	@Column(name="fedex_tracking_id")
	public String getFedExTrackingNo() {
		return fedExTrackingNo;
	}
	public void setFedExTrackingNo(String fedExTrackingNo) {
		this.fedExTrackingNo = fedExTrackingNo;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="primary_payment_method")
	public PaymentMethod getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}
	public void setPrimaryPaymentMethod(PaymentMethod primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}
	
	@Column(name="primary_transaction_id")
	public String getPrimaryTransactionId() {
		return primaryTransactionId;
	}
	public void setPrimaryTransactionId(String primaryTransactionId) {
		this.primaryTransactionId = primaryTransactionId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="secondary_payment_method")
	public PartialPaymentMethod getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}
	public void setSecondaryPaymentMethod(
			PartialPaymentMethod secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}
	
	@Column(name="secondary_transaction_id")
	public String getSecondaryTransactionId() {
		return secondaryTransactionId;
	}
	public void setSecondaryTransactionId(String secondaryTransactionId) {
		this.secondaryTransactionId = secondaryTransactionId;
	}
	
	@Column(name="event_venue_category")
	public String getVenueCategory() {
		return venueCategory;
	}
	public void setVenueCategory(String venueCategory) {
		this.venueCategory = venueCategory;
	}
	
	@Column(name="category_ticket_group_id")
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	public ApplicationPlatForm getAppPlatForm() {
		return appPlatForm;
	}
	public void setAppPlatForm(ApplicationPlatForm appPlatForm) {
		this.appPlatForm = appPlatForm;
	}
	
	@Transient
	public String getEventTimeStr() {
		if(eventDate != null && !eventDate.equals("TBD")){
			String eventTimeLocal="TBD";
			if(null != eventTime ){
				eventTimeLocal = tf.format(eventTime);
			}
			eventTimeStr = eventTimeLocal;
		} else {
			eventTimeStr = "";
		}
		
		/*if(null != getIsIOS() && getIsIOS()) {
			eventTimeStr = getEventDate()+" "+eventTimeStr;
		}*/
		
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	
	@Transient
	public String getOrderDeliveryNote() {
		return orderDeliveryNote;
	}
	public void setOrderDeliveryNote(String orderDeliveryNote) {
		this.orderDeliveryNote = orderDeliveryNote;
	}
	
	
	@Transient
	public void setAllCustomerRelatedObjects(){
		try{
			
			CustomerOrderDetail orderDetail = DAORegistry.getCustomerOrderDetailDAO().getCustomerOrderDetailByOrderId(this.getId());
			if(null != orderDetail && (null == orderDetail.getShippingAddress1() || orderDetail.getShippingAddress1().isEmpty())){
				this.setIsShippingRequired(true);
			} 
			this.setCustomerOrderDetail(orderDetail);
			
			if(this.secondaryOrdertype != null && this.secondaryOrdertype.equals(SecondaryOrderType.CROWNJEWEL)){
				
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustCrownJewelLoyaltyByOrderId(Integer.parseInt(this.getSecondaryOrderId()));
				
				this.setCustomerLoyaltyHistory(customerLoyaltyHistory);
			}else{
				
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderId(this.getId());
				if(getTicketOnDayBeforeTheEvent() != null && getTicketOnDayBeforeTheEvent()){
					
					CustomerLoyaltyHistory latDownloadRewardHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getLateDownloadCustomerLoyaltyHistoryByOrderId(this.getId());
					
					if(null != latDownloadRewardHistory ){
						customerLoyaltyHistory.setPointsEarnedAsDouble(customerLoyaltyHistory.getPointsEarnedAsDouble()+latDownloadRewardHistory.getPointsEarnedAsDouble());
					}
				}
				
				this.setCustomerLoyaltyHistory(customerLoyaltyHistory);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Transient
	public String getSvgKey() {
		return svgKey;
	}
	public void setSvgKey(String svgKey) {
		this.svgKey = svgKey;
	}
	
	@Column(name="is_invoice_email_sent")
	public Boolean getIsInvoiceSent() {
		return isInvoiceSent;
	}
	public void setIsInvoiceSent(Boolean isInvoiceSent) {
		if(null == isInvoiceSent){
			isInvoiceSent = false;
		}
		this.isInvoiceSent = isInvoiceSent;
	}
	
	@Transient
	public String getTicketDownloadUrl() {
		return ticketDownloadUrl;
	}
	public void setTicketDownloadUrl(String ticketDownloadUrl) {
		this.ticketDownloadUrl = ticketDownloadUrl;
	}
	
	@Transient
	public String getButtonOption() {
		return buttonOption;
	}
	public void setButtonOption(String buttonOption) {
		this.buttonOption = buttonOption;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "order_type")
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
	@Column(name = "last_notified")
	public Date getLastNotified() {
		return lastNotified;
	}
	public void setLastNotified(Date lastNotified) {
		this.lastNotified = lastNotified;
	}
	
	@Transient
	public Boolean getShowInfoButton() {
		if(null == showInfoButton){
			showInfoButton= false;
		}
		return showInfoButton;
	}
	public void setShowInfoButton(Boolean showInfoButton) {
		this.showInfoButton = showInfoButton;
	}
	
	@Transient
	public String getDeliveryInfo() {
		return deliveryInfo;
	}
	
	public void setDeliveryInfo(String deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}
	
	@Transient
	public CustomerOrderCreditCard getCardDetails() {
		if(id != null && ((null != primaryPaymentMethod && primaryPaymentMethod.equals(PaymentMethod.CREDITCARD))
				|| (null != secondaryPaymentMethod && secondaryPaymentMethod.equals(PartialPaymentMethod.CREDITCARD)))){
			cardDetails =  DAORegistry.getCustomerOrderCreditCardDAO().getCreditDetailsByTicketGroupId(categoryTicketGroupId);
		}
		return cardDetails;
	}
	public void setCardDetails(CustomerOrderCreditCard cardDetails) {
		this.cardDetails = cardDetails;
	}
	
	@Transient
	public String getShippingDate() {
		if(null !=shippingDateTemp ){
			shippingDate =  dateFormat.format(shippingDateTemp);
		}
		return shippingDate;
	}
	public void setShippingDate(String shippingDate) {
		this.shippingDate = shippingDate;
	}
	
	@Transient
	public String getEventDate() {
		if(null != eventDateTemp ){
			eventDate =  dateFormat.format(eventDateTemp);
		} else {
			eventDate = "TBD";
		}
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	@Transient
	public String getCreateDate() {
		if(null != createDateTemp ){
			createDate =  dateTimeFormat1.format(createDateTemp);
		}
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	@Transient
	public String getLastUpdatedDate() {
		if(null != lastUpdatedDateTemp ){
			lastUpdatedDate =  dateTimeFormat.format(lastUpdatedDateTemp);
		}
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Transient
	public String getAcceptedDate() {
		if(null != acceptedDateTemp ){
			acceptedDate =  dateTimeFormat.format(acceptedDateTemp);
		}
		return acceptedDate;
	}
	public void setAcceptedDate(String acceptedDate) {
		this.acceptedDate = acceptedDate;
	}
	@Transient
	public List<InvoiceTicketAttachment> getInvoiceTicketAttachment() {
		return invoiceTicketAttachment;
	}
	public void setInvoiceTicketAttachment(
			List<InvoiceTicketAttachment> invoiceTicketAttachmentList) {
		this.invoiceTicketAttachment = invoiceTicketAttachmentList;
	}
	
	@Transient
	public Boolean getIsFavoriteEvent() {
		if(null == isFavoriteEvent){
			isFavoriteEvent= false;
		}
		return isFavoriteEvent;
	}

	public void setIsFavoriteEvent(Boolean isFavoriteEvent) {
		this.isFavoriteEvent = isFavoriteEvent;
	}
	@Transient
	public String getShareLinkUrl() {
		
		return shareLinkUrl;
	}

	public void setShareLinkUrl(String shareLinkUrl) {
		this.shareLinkUrl = shareLinkUrl;
	}
	
	@Column(name="secondary_order_id")
	public String getSecondaryOrderId() {
		return secondaryOrderId;
	}
	public void setSecondaryOrderId(String secondaryOrderId) {
		this.secondaryOrderId = secondaryOrderId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="secondary_order_type")
	public SecondaryOrderType getSecondaryOrdertype() {
		return secondaryOrdertype;
	}
	public void setSecondaryOrdertype(SecondaryOrderType secondaryOrdertype) {
		this.secondaryOrdertype = secondaryOrdertype;
	}
	
	@Column(name="cje_package_enabled")
	public Boolean getCjePackageEnabled() {
		if(null == cjePackageEnabled){
			cjePackageEnabled = false;
		}
		return cjePackageEnabled;
	}
	public void setCjePackageEnabled(Boolean cjePackageEnabled) {
		this.cjePackageEnabled = cjePackageEnabled;
	}
	
	@Column(name="cje_package_notes")
	public String getCjePackageNote() {
		return cjePackageNote;
	}
	public void setCjePackageNote(String cjePackageNote) {
		this.cjePackageNote = cjePackageNote;
	}
	
	@Column(name="cje_ticket_price")
	public Double getCjeTicketPrice() {
		return cjeTicketPrice;
	}
	public void setCjeTicketPrice(Double cjeTicketPrice) {
		this.cjeTicketPrice = cjeTicketPrice;
	}
	
	@Column(name="cje_ticket_points")
	public Double getCjeTicketPoints() {
		return cjeTicketPoints;
	}
	public void setCjeTicketPoints(Double cjeTicketPoints) {
		this.cjeTicketPoints = cjeTicketPoints;
	}
	
	
	@Column(name="cje_package_cost")
	public Double getCjePackagePrice() {
		return cjePackagePrice;
	}
	public void setCjePackagePrice(Double cjePackagePrice) {
		this.cjePackagePrice = cjePackagePrice;
	}
	
	@Column(name="cje_package_points")
	public Double getCjePackagePoints() {
		return cjePackagePoints;
	}
	public void setCjePackagePoints(Double cjePackagePoints) {
		this.cjePackagePoints = cjePackagePoints;
	}
	
	@Column(name="cje_required_points")
	public Double getCjeRequirePoints() {
		return cjeRequirePoints;
	}
	public void setCjeRequirePoints(Double cjeRequirePoints) {
		this.cjeRequirePoints = cjeRequirePoints;
	}
	
	@Transient
	public Boolean getShowCJEOrder() {
		if(null == showCJEOrder){
			showCJEOrder = false;
		}
		return showCJEOrder;
	}
	public void setShowCJEOrder(Boolean showCJEOrder) {
		this.showCJEOrder = showCJEOrder;
	}
	
	@Transient
	public String getCjeOrderInfo() {
		return cjeOrderInfo;
	}
	public void setCjeOrderInfo(String cjeOrderInfo) {
		this.cjeOrderInfo = cjeOrderInfo;
	}
	
	@Transient
	public String getCjeOrderNo() {
		return cjeOrderNo;
	}
	public void setCjeOrderNo(String cjeOrderNo) {
		this.cjeOrderNo = cjeOrderNo;
	}
	
	@Transient
	public String getViewOrderType() {
		return viewOrderType;
	}
	public void setViewOrderType(String viewOrderType) {
		this.viewOrderType = viewOrderType;
	}
	
	@Transient
	public String getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(String rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	@Transient
	public String getDesktopShareLinkUrl() {
		return desktopShareLinkUrl;
	}
	public void setDesktopShareLinkUrl(String desktopShareLinkUrl) {
		this.desktopShareLinkUrl = desktopShareLinkUrl;
	}
	
	@Transient
	public String getSvgText() {
		return svgText;
	}
	public void setSvgText(String svgText) {
		this.svgText = svgText;
	}
	
	@Transient
	public String getIsMapWithSvg() {
		return isMapWithSvg;
	}
	public void setIsMapWithSvg(String isMapWithSvg) {
		this.isMapWithSvg = isMapWithSvg;
	}
	
	@Transient
	public String getSvgMapPath() {
		return svgMapPath;
	}
	public void setSvgMapPath(String svgMapPath) {
		this.svgMapPath = svgMapPath;
	}
	
	@Transient
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	
	@Column(name = "order_total")
	public Double getOrderTotalAsDouble() {
		return orderTotalAsDouble;
	}
	public void setOrderTotalAsDouble(Double orderTotalAsDouble) {
		this.orderTotalAsDouble = orderTotalAsDouble;
	}
	
	@Column(name = "primary_payment_amount")
	public Double getPrimaryPayAmtAsDouble() {
		return primaryPayAmtAsDouble;
	}
	
	public void setPrimaryPayAmtAsDouble(Double primaryPayAmtAsDouble) {
		this.primaryPayAmtAsDouble = primaryPayAmtAsDouble;
	}
	
	@Column(name = "secondary_payment_amount")
	public Double getSecondaryPayAmtAsDouble() {
		if(null == secondaryPayAmtAsDouble){
			secondaryPayAmtAsDouble=0.00;
		}
		return secondaryPayAmtAsDouble;
	}
	public void setSecondaryPayAmtAsDouble(Double secondaryPayAmtAsDouble) {
		this.secondaryPayAmtAsDouble = secondaryPayAmtAsDouble;
	}
	
	@Transient
	public String getOrderTotal() {
		if(null == orderTotalAsDouble){
			orderTotalAsDouble = 0.0;
		}
		try {
			orderTotal = TicketUtil.getRoundedValueString(orderTotalAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orderTotal;
	}
	public void setOrderTotal(String orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	@Transient
	public String getPrimaryPayAmt() {
		if(null == primaryPayAmtAsDouble){
			primaryPayAmtAsDouble = 0.0;
		}
		try {
			primaryPayAmt = TicketUtil.getRoundedValueString(primaryPayAmtAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return primaryPayAmt;
	}
	public void setPrimaryPayAmt(String primaryPayAmt) {
		this.primaryPayAmt = primaryPayAmt;
	}
	
	@Transient
	public String getSecondaryPayAmt() {
		if(null == secondaryPayAmtAsDouble){
			secondaryPayAmtAsDouble = 0.0;
		}
		try {
			secondaryPayAmt = TicketUtil.getRoundedValueString(secondaryPayAmtAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return secondaryPayAmt;
	}
	public void setSecondaryPayAmt(String secondaryPayAmt) {
		this.secondaryPayAmt = secondaryPayAmt;
	}
	

	@Column(name = "tax_amount")
	public Double getTaxesAsDouble() {
		return taxesAsDouble;
	}
	public void setTaxesAsDouble(Double taxesAsDouble) {
		this.taxesAsDouble = taxesAsDouble;
	}
	
	@Transient
	public String getTaxes() {
		if(taxesAsDouble == null){
			taxesAsDouble = 0.00;
		}
		try {
			taxes = TicketUtil.getRoundedValueString(taxesAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return taxes;
	}
	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}	
	
	
	@Column(name="sold_price")
	public Double getTicketSoldPriceAsDouble() {
		return ticketSoldPriceAsDouble;
	}
	public void setTicketSoldPriceAsDouble(Double ticketSoldPriceAsDouble) {
		this.ticketSoldPriceAsDouble = ticketSoldPriceAsDouble;
	}
	
	@Column(name = "ticket_price")
	public Double getTicketPriceAsDouble() {
		if(null != ticketPriceAsDouble && ticketPriceAsDouble > 0){
			if(!ticketPriceAsDouble.equals(ticketSoldPriceAsDouble) || ticketPriceAsDouble != ticketSoldPriceAsDouble){
				ticketPriceAsDouble = ticketSoldPriceAsDouble;
			}
		}
		return ticketPriceAsDouble;
	}
	public void setTicketPriceAsDouble(Double ticketPriceAsDouble) {
		this.ticketPriceAsDouble = ticketPriceAsDouble;
	}
	
	@Transient
	public String getTicketPrice() {
		getTicketPriceAsDouble();
		if(null == ticketPriceAsDouble){
			ticketPriceAsDouble = 0.00;
		}
		try {
			ticketPrice = TicketUtil.getRoundedValueString(ticketPriceAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ticketPrice;
	}
	public void setTicketPrice(String ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	
	@Transient
	public String getTicketSoldPrice() {
		if(null == ticketSoldPriceAsDouble){
			ticketSoldPriceAsDouble = 0.00;
		}
		try {
			ticketSoldPrice = TicketUtil.getRoundedValueString(ticketSoldPriceAsDouble);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketSoldPrice;
	}
	
	public void setTicketSoldPrice(String ticketSoldPrice) {
		this.ticketSoldPrice = ticketSoldPrice;
	}
	
	@Transient
	public String getThirdPayAmt() {
		if(null == thirdPayAmtAsDouble){
			thirdPayAmtAsDouble = 0.0;
		}
		try {
			thirdPayAmt = TicketUtil.getRoundedValueString(thirdPayAmtAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return thirdPayAmt;
	}
	public void setThirdPayAmt(String thirdPayAmt) {
		this.thirdPayAmt = thirdPayAmt;
	}
	
	@Column(name="third_payment_amount")
	public Double getThirdPayAmtAsDouble() {
		return thirdPayAmtAsDouble;
	}
	public void setThirdPayAmtAsDouble(Double thirdPayAmtAsDouble) {
		this.thirdPayAmtAsDouble = thirdPayAmtAsDouble;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="third_payment_method")
	public PartialPaymentMethod getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}
	public void setThirdPaymentMethod(PartialPaymentMethod thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}
	@Column(name="third_payment_transaction_id")
	public String getThirdTransactionId() {
		return thirdTransactionId;
	}
	public void setThirdTransactionId(String thirdTransactionId) {
		this.thirdTransactionId = thirdTransactionId;
	}
	
	@Column(name = "actual_section")
	public String getActualSection() {
		if(actualSection ==null){
			actualSection="";
		}
		return actualSection;
	}
	public void setActualSection(String actualSection) {
		this.actualSection = actualSection;
	}
	
	@Column(name = "seat_details")
	public String getActualSeat() {
		if(actualSeat ==null){
			actualSeat="";
		}
		return actualSeat;
	}
	public void setActualSeat(String actualSeat) {
		this.actualSeat = actualSeat;
	}
	
	@Column(name = "is_long_sale")
	public Boolean getIsLongSale() {
		return isLongSale;
	}
	public void setIsLongSale(Boolean isLongSale) {
		this.isLongSale = isLongSale;
	}
	
	@Column(name="row")
	public String getActualRow() {
		if(actualRow ==null){
			actualRow="";
		}
		return actualRow;
	}
	public void setActualRow(String actualRow) {
		this.actualRow = actualRow;
	}
	
	
	@Column(name="seat_low")
	public String getSeatLow() {
		if(seatLow ==null){
			seatLow="";
		}
		return seatLow;
	}
	
	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}
	
	@Column(name="seat_high")
	public String getSeatHigh() {
		if(seatHigh ==null){
			seatHigh="";
		}
		return seatHigh;
	}
	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}
	
	@Transient
	public Boolean getShowLongInfo() {
		return showLongInfo;
	}
	public void setShowLongInfo(Boolean showLongInfo) {
		this.showLongInfo = showLongInfo;
	}
	
	@Column(name="is_ticket_download_email_sent")
	public Boolean getIsTicketDownloadSent() {
		return isTicketDownloadSent;
	}
	public void setIsTicketDownloadSent(Boolean isTicketDownloadSent) {
		this.isTicketDownloadSent = isTicketDownloadSent;
	}
	
	@Column(name="td_to_email")
	public String getTdToEmail() {
		return tdToEmail;
	}
	public void setTdToEmail(String tdToEmail) {
		this.tdToEmail = tdToEmail;
	}
	
	@Column(name="order_cancel_email_sent")
	public Boolean getIsOrderCancelSent() {
		return isOrderCancelSent;
	}
	public void setIsOrderCancelSent(Boolean isOrderCancelSent) {
		this.isOrderCancelSent = isOrderCancelSent;
	}
	
	@Column(name="oc_to_email")
	public String getOcToEmail() {
		return ocToEmail;
	}
	public void setOcToEmail(String ocToEmail) {
		this.ocToEmail = ocToEmail;
	}
	
	@Column(name="o_to_email")
	public String getOrderToEmail() {
		return orderToEmail;
	}
	public void setOrderToEmail(String orderToEmail) {
		this.orderToEmail = orderToEmail;
	}
	
	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	@Column(name="broker_service_perc")
	public Double getBrokerServicePerc() {
		return brokerServicePerc;
	}
	public void setBrokerServicePerc(Double brokerServicePerc) {
		this.brokerServicePerc = brokerServicePerc;
	}
	
	@Column(name="is_promo_applied")
	public Boolean getIsPromoCodeApplied() {
		return isPromoCodeApplied;
	}
	public void setIsPromoCodeApplied(Boolean isPromoCodeApplied) {
		this.isPromoCodeApplied = isPromoCodeApplied;
	}
	
	@Column(name="promo_code")
	public String getPromotionalCode() {
		return promotionalCode;
	}
	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}
	  
	@Transient
	public String getPromotionalCodeMessage() {
		if(isPromoCodeApplied != null && isPromoCodeApplied){
			promotionalCodeMessage = "You have applied "+promotionalCode+" Promotional Code";
			if(promotionalCode.equals("MOBILEDISCOUNT")){
				promotionalCodeMessage = "We have applied mobile discount";
			}else if(promotionalCode.equals("CHECKOUT OFFER")){
				promotionalCodeMessage = "We have applied checkout offer.";
			}
		}
		return promotionalCodeMessage;
	}
	public void setPromotionalCodeMessage(String promotionalCodeMessage) {
		this.promotionalCodeMessage = promotionalCodeMessage;
	}
	
	@Column(name="show_disc_price_area")
	public Boolean getShowDiscPriceArea() {
		return showDiscPriceArea;
	}
	public void setShowDiscPriceArea(Boolean showDiscPriceArea) {
		this.showDiscPriceArea = showDiscPriceArea;
	}
	
	@Column(name="normal_tix_price")
	public Double getNormalTixPrice() {
		return normalTixPrice;
	}
	public void setNormalTixPrice(Double normalTixPrice) {
		this.normalTixPrice = normalTixPrice;
	}
	
	@Transient
	public String getNormalTixPriceStr() {
		try {
			normalTixPriceStr = TicketUtil.getRoundedValueString(normalTixPrice);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return normalTixPriceStr;
	}
	public void setNormalTixPriceStr(String normalTixPriceStr) {
		this.normalTixPriceStr = normalTixPriceStr;
	}
	
	@Column(name="discounted_tix_price")
	public Double getDiscountedTixPrice() {
		return discountedTixPrice;
	}
	public void setDiscountedTixPrice(Double discountedTixPrice) {
		this.discountedTixPrice = discountedTixPrice;
	}
	
	@Transient
	public String getDiscountedTixPriceStr() {
		try {
			discountedTixPriceStr = TicketUtil.getRoundedValueString(discountedTixPrice);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return discountedTixPriceStr;
	}
	public void setDiscountedTixPriceStr(String discountedTixPriceStr) {
		this.discountedTixPriceStr = discountedTixPriceStr;
	}
	
	//@Column(name="is_order_full_filled")
	@Transient
	public Boolean getIsOrderFullFilled() {
		if(isOrderFullFilled == null){
			isOrderFullFilled = false;
		}
		return isOrderFullFilled;
	}
	public void setIsOrderFullFilled(Boolean isOrderFullFilled) {
		this.isOrderFullFilled = isOrderFullFilled;
	}
	
	@Transient
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Transient
	public String getRealSection() {
		return realSection;
	}
	public void setRealSection(String realSection) {
		this.realSection = realSection;
	}
	
	@Transient
	public String getRealRow() {
		return realRow;
	}
	public void setRealRow(String realRow) {
		this.realRow = realRow;
	}
	
	@Transient
	public String getRealSeat() {
		return realSeat;
	}
	public void setRealSeat(String realSeat) {
		this.realSeat = realSeat;
	}
	
	@Transient
	public String getRealTicketPrice() {
		return realTicketPrice;
	}
	public void setRealTicketPrice(String realTicketPrice) {
		this.realTicketPrice = realTicketPrice;
	}
	
	@Transient
	public String getTotalServiceFees() {
		return totalServiceFees;
	}
	public void setTotalServiceFees(String totalServiceFees) {
		this.totalServiceFees = totalServiceFees;
	}
	
	@Transient
	public String getSingleTixServiceFees() {
		return singleTixServiceFees;
	}
	public void setSingleTixServiceFees(String singleTixServiceFees) {
		this.singleTixServiceFees = singleTixServiceFees;
	}
	
	/*@Transient
	public List<OrderTicketGroup> getOrderTicketGroups() {
		return orderTicketGroups;
	}
	public void setOrderTicketGroups(List<OrderTicketGroup> orderTicketGroups) {
		this.orderTicketGroups = orderTicketGroups;
	}*/
	
	@Column(name="ticket_on_day_before_event")
	public Boolean getTicketOnDayBeforeTheEvent() {
		return ticketOnDayBeforeTheEvent;
	}
	public void setTicketOnDayBeforeTheEvent(Boolean ticketOnDayBeforeTheEvent) {
		this.ticketOnDayBeforeTheEvent = ticketOnDayBeforeTheEvent;
	}
	@Transient
	public List<OrderPaymentBreakup> getOrderPaymentBreakupList() {
		if(id != null){
			orderPaymentBreakupList = DAORegistry.getOrderPaymentBreakupDAO().getPaymentBreaksByOrderId(id);
		}
		return orderPaymentBreakupList;
	}
	public void setOrderPaymentBreakupList(
			List<OrderPaymentBreakup> orderPaymentBreakupList) {
		this.orderPaymentBreakupList = orderPaymentBreakupList;
	}
	@Transient
	public Boolean getIsShippingRequired() {
		if(isShippingRequired == null){
			isShippingRequired = false;
		}
		return isShippingRequired;
	}
	public void setIsShippingRequired(Boolean isShippingRequired) {
		this.isShippingRequired = isShippingRequired;
	}
	@Transient
	public String getShippingRequiredMsg() {
		if(this.getIsShippingRequired()){
			shippingRequiredMsg = "Please enter shipping address.";
		}else{
			shippingRequiredMsg = "";
		}
		return shippingRequiredMsg;
	}
	public void setShippingRequiredMsg(String shippingRequiredMsg) {
		this.shippingRequiredMsg = shippingRequiredMsg;
	}
	
	@Transient
	public Boolean getIsTriviaOrder() {
		getOrderType();
		if(null != orderType && (orderType.equals(OrderType.TRIVIAORDER) || orderType.equals(OrderType.CONTEST))) {
			isTriviaOrder = true;
		}else {
			isTriviaOrder = false;
		}
		return isTriviaOrder;
	}
	public void setIsTriviaOrder(Boolean isTriviaOrder) {
		this.isTriviaOrder = isTriviaOrder;
	}
	
	@Transient
	public String getProgressPopupMsg() {
		if(null == progressPopupMsg) {
			progressPopupMsg = "";
		}
		return progressPopupMsg;
	}
	public void setProgressPopupMsg(String progressPopupMsg) {
		this.progressPopupMsg = progressPopupMsg;
	}
	
	@Transient
	public String getConvRateString() {
		return convRateString;
	}
	public void setConvRateString(String convRateString) {
		this.convRateString = convRateString;
	}
	
	@Transient
	public String getDescription() {
		if(null == description ) {
			description = "-----";
		}
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Transient
	public String getGfCardValue() {
		if(null == gfCardValue) {
			gfCardValue = "$0.00";
		}else {
			gfCardValue = "$"+gfCardValue;
		}
		return gfCardValue;
	}
	public void setGfCardValue(String gfCardValue) {
		this.gfCardValue = gfCardValue;
	}
	
	@Transient
	public String getRedeemText() {
		return redeemText;
	}
	public void setRedeemText(String redeemText) {
		this.redeemText = redeemText;
	}
	
	/*@Transient
	public Boolean getIsIOS() {
		if(null == isIOS) {
			isIOS = false;
		}
		return isIOS;
	}
	public void setIsIOS(Boolean isIOS) {
		this.isIOS = isIOS;
	}
	*/
	
}
