package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.dao.implementaion.DAORegistry;

@Entity
@Table(name = "customer_order_credit_card_details")
public class CustomerOrderCreditCard implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private Integer ticketGroupId;
	private Integer cardId;
	private Double trxAmount;
	private String transactionId;
	private CustomerCardInfo customerCardInfo;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="credit_card_id")
	public Integer getCardId() {
		return cardId;
	}
	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}
	
	
	@Column(name="trx_amount")
	public Double getTrxAmount() {
		return trxAmount;
	}
	public void setTrxAmount(Double trxAmount) {
		this.trxAmount = trxAmount;
	}
	
	@Column(name="transaction_id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	@Transient
	public CustomerCardInfo getCustomerCardInfo() {
		if(null != cardId ){
			customerCardInfo = DAORegistry.getCustomerCardInfoDAO().get(cardId);
		}
		return customerCardInfo;
	}
	public void setCustomerCardInfo(CustomerCardInfo customerCardInfo) {
		this.customerCardInfo = customerCardInfo;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	
	
}
