package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "customer_order_details")
public class CustomerOrderDetail implements Serializable{

	private Integer id;
	private String billingAddress1;
	private String billingAddress2;
	private String billingCity;
	private String billingState;
	private String billingCountry;
	private String billingZipCode;
	private String billingFirstName;
	private String billingLastName;
	private String billingPhone1;
	private String billingPhone2;
	private String billingEmail;
	private String shippingAddress1;
	private String shippingAddress2;
	private String shippingCity;
	private String shippingState;
	private String shippingCountry;
	private String shippingZipCode;
	private String shippingFirstName;
	private String shippingLastName;
	private String ctyPhCode;
	private String shippingPhone1;
	private String shippingPhone2;
	private String shippingEmail;
	private Integer orderId;
	private Boolean isShAddEmpty;
	private Boolean isBiAddEmpty;
	private Integer shippingCountryId;
	private Integer shippingStateId;
	private Integer billingCountryId;
	private Integer billingStateId;
	
	@JsonIgnore
	private String orderType;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "bl_address1")
	public String getBillingAddress1() {
		return billingAddress1;
	}
	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}
	@Column(name = "bl_address2")
	public String getBillingAddress2() {
		return billingAddress2;
	}
	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}
	@Column(name = "bl_city")
	public String getBillingCity() {
		return billingCity;
	}
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	@Column(name = "bl_state_name")
	public String getBillingState() {
		return billingState;
	}
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	
	@Column(name = "bl_country_name")
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	@Column(name = "bl_zipcode")
	public String getBillingZipCode() {
		return billingZipCode;
	}
	public void setBillingZipCode(String billingZipCode) {
		this.billingZipCode = billingZipCode;
	}
	@Column(name = "bl_first_name")
	public String getBillingFirstName() {
		return billingFirstName;
	}
	public void setBillingFirstName(String billingFirstName) {
		this.billingFirstName = billingFirstName;
	}
	@Column(name = "bl_last_name")
	public String getBillingLastName() {
		return billingLastName;
	}
	public void setBillingLastName(String billingLastName) {
		this.billingLastName = billingLastName;
	}
	@Column(name = "bl_phone1")
	public String getBillingPhone1() {
		return billingPhone1;
	}
	public void setBillingPhone1(String billingPhone1) {
		this.billingPhone1 = billingPhone1;
	}
	@Column(name = "bl_phone2")
	public String getBillingPhone2() {
		return billingPhone2;
	}
	public void setBillingPhone2(String billingPhone2) {
		this.billingPhone2 = billingPhone2;
	}
	@Column(name = "sh_address1")
	public String getShippingAddress1() {
		return shippingAddress1;
	}
	public void setShippingAddress1(String shippingAddress1) {
		this.shippingAddress1 = shippingAddress1;
	}
	@Column(name = "sh_address2")
	public String getShippingAddress2() {
		return shippingAddress2;
	}
	public void setShippingAddress2(String shippingAddress2) {
		this.shippingAddress2 = shippingAddress2;
	}
	@Column(name = "sh_city")
	public String getShippingCity() {
		return shippingCity;
	}
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	
	
	@Column(name = "sh_state_name")
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	@Column(name = "sh_country_name")
	public String getShippingCountry() {
		return shippingCountry;
	}
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	@Column(name = "sh_zipcode")
	public String getShippingZipCode() {
		return shippingZipCode;
	}
	public void setShippingZipCode(String shippingZipCode) {
		this.shippingZipCode = shippingZipCode;
	}
	@Column(name = "sh_first_name")
	public String getShippingFirstName() {
		return shippingFirstName;
	}
	public void setShippingFirstName(String shippingFirstName) {
		this.shippingFirstName = shippingFirstName;
	}
	@Column(name = "sh_last_name")
	public String getShippingLastName() {
		return shippingLastName;
	}
	public void setShippingLastName(String shippingLastName) {
		this.shippingLastName = shippingLastName;
	}
	@Column(name = "sh_phone1")
	public String getShippingPhone1() {
		return shippingPhone1;
	}
	public void setShippingPhone1(String shippingPhone1) {
		this.shippingPhone1 = shippingPhone1;
	}
	@Column(name = "sh_phone2")
	public String getShippingPhone2() {
		return shippingPhone2;
	}
	public void setShippingPhone2(String shippingPhone2) {
		this.shippingPhone2 = shippingPhone2;
	}
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name = "bl_email")
	public String getBillingEmail() {
		if(null == billingEmail){
			billingEmail="";
		}
		return billingEmail;
	}
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	
	@Column(name = "sh_email")
	public String getShippingEmail() {
		if(null == shippingEmail){
			shippingEmail="";
		}
		return shippingEmail;
	}
	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}
	
	
	@Transient
	public Boolean getIsShAddEmpty() {
		if(shippingAddress1 != null && !shippingAddress1.isEmpty()){
			isShAddEmpty = true;
		}else{
			isShAddEmpty = false;
		}
		return isShAddEmpty;
	}
	public void setIsShAddEmpty(Boolean isShAddEmpty) {
		this.isShAddEmpty = isShAddEmpty;
	}
	
	@Transient
	public Boolean getIsBiAddEmpty() {
		if(billingAddress1 != null && !billingAddress1.isEmpty()){
			isBiAddEmpty = true;
		}else{
			isBiAddEmpty = false;
		}
		return isBiAddEmpty;
	}
	public void setIsBiAddEmpty(Boolean isBiAddEmpty) {
		this.isBiAddEmpty = isBiAddEmpty;
	}
	@Column(name = "sh_country")
	public Integer getShippingCountryId() {
		return shippingCountryId;
	}
	public void setShippingCountryId(Integer shippingCountryId) {
		this.shippingCountryId = shippingCountryId;
	}
	@Column(name = "sh_state")
	public Integer getShippingStateId() {
		return shippingStateId;
	}
	public void setShippingStateId(Integer shippingStateId) {
		this.shippingStateId = shippingStateId;
	}
	@Column(name = "bl_country")
	public Integer getBillingCountryId() {
		return billingCountryId;
	}
	public void setBillingCountryId(Integer billingCountryId) {
		this.billingCountryId = billingCountryId;
	}
	@Column(name = "bl_state")
	public Integer getBillingStateId() {
		return billingStateId;
	}
	public void setBillingStateId(Integer billingStateId) {
		this.billingStateId = billingStateId;
	}
	
	@Column(name = "order_type")
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
	@Column(name = "country_phone_code")
	public String getCtyPhCode() {
		return ctyPhCode;
	}
	public void setCtyPhCode(String ctyPhCode) {
		this.ctyPhCode = ctyPhCode;
	}
		
}
