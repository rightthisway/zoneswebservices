package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.enums.PartialPaymentMethod;
import com.zonesws.webservices.enums.PaymentMethod;

@Entity
@Table(name = "customer_order_request")
public class CustomerOrderRequest implements Serializable{

	private static final long serialVersionUID = -7941769011539361245L;
	
	private Integer id;
	private String sessionId;
	private String ipAddress;
	private Integer customerId;
	private Integer orderId;
	private Integer invoiceId;
	private Integer eventId;
	private Boolean isLongSale;
	private Integer categoryTicketGroupId;
	private Integer ticketGroupId;
	private Integer lockId;
	private Boolean isLoyalFan;
	private Integer oldLoyalFanId;
	private Integer newLoyalFanId;
	private Integer orderLoyaltyHistoryId;
	private Integer orderLoyaltyInfoId;
	private Integer affCashRewardId;
	private Integer affCashRewardHistoryId;
	private Integer affPromoTrackingId;
	private Integer refCustLoyaltyHistoryId;
	private Integer refCustLoyaltyInfoId;
	private Integer refTrackingId;
	private Integer rtfPromoTrackingId;
	private Integer rtfPromoOfferId;
	private Integer quantity;
	private Double originalOrderTotal;
	private Double orderTotal;
	private Double ticketPrice;
	private Double soldPrice;
	
	private PaymentMethod primaryPaymentMethod;
	private String primaryTransactionId;
	private Double primaryPayAmt;
	
	private PartialPaymentMethod secondaryPaymentMethod;
	private String secondaryTransactionId;
	private Double secondaryPayAmt;
	
	private PartialPaymentMethod thirdPaymentMethod;
	private String thirdTransactionId;
	private Double thirdPayAmt;
	
	private OrderType orderType;
	private ApplicationPlatForm appPlatForm;
	private OrderStatus status;
	private Date createdTime;
	private Date updatedTime;
	
	private Integer rtfCheckoutOfferId;
	
	private Integer orderOfferRewardHistoryId;
	private Boolean ticketOnDayBeforeTheEvent;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="category_ticket_group_id")
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	@Column(name = "invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	@Column(name = "is_long_sale")
	public Boolean getIsLongSale() {
		return isLongSale;
	}
	public void setIsLongSale(Boolean isLongSale) {
		this.isLongSale = isLongSale;
	}
	
	@Column(name = "ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	@Column(name = "lock_id")
	public Integer getLockId() {
		return lockId;
	}
	public void setLockId(Integer lockId) {
		this.lockId = lockId;
	}

	@Column(name = "is_loyal_fan")
	public Boolean getIsLoyalFan() {
		return isLoyalFan;
	}
	public void setIsLoyalFan(Boolean isLoyalFan) {
		this.isLoyalFan = isLoyalFan;
	}	
	
	@Column(name = "old_loyal_fan_id")
	public Integer getOldLoyalFanId() {
		return oldLoyalFanId;
	}
	public void setOldLoyalFanId(Integer oldLoyalFanId) {
		this.oldLoyalFanId = oldLoyalFanId;
	}
	@Column(name = "new_loyal_fan_id")
	public Integer getNewLoyalFanId() {
		return newLoyalFanId;
	}
	public void setNewLoyalFanId(Integer newLoyalFanId) {
		this.newLoyalFanId = newLoyalFanId;
	}
	@Column(name = "order_loyalty_history_id")
	public Integer getOrderLoyaltyHistoryId() {
		return orderLoyaltyHistoryId;
	}
	public void setOrderLoyaltyHistoryId(Integer orderLoyaltyHistoryId) {
		this.orderLoyaltyHistoryId = orderLoyaltyHistoryId;
	}
	@Column(name = "order_loyalty_info_id")
	public Integer getOrderLoyaltyInfoId() {
		return orderLoyaltyInfoId;
	}
	public void setOrderLoyaltyInfoId(Integer orderLoyaltyInfoId) {
		this.orderLoyaltyInfoId = orderLoyaltyInfoId;
	}	

	@Column(name = "aff_cash_reward_id")
	public Integer getAffCashRewardId() {
		return affCashRewardId;
	}
	public void setAffCashRewardId(Integer affCashRewardId) {
		this.affCashRewardId = affCashRewardId;
	}	

	@Column(name = "aff_cash_reward_history_id")
	public Integer getAffCashRewardHistoryId() {
		return affCashRewardHistoryId;
	}
	public void setAffCashRewardHistoryId(Integer affCashRewardHistoryId) {
		this.affCashRewardHistoryId = affCashRewardHistoryId;
	}	

	@Column(name = "aff_promo_tracking_id")
	public Integer getAffPromoTrackingId() {
		return affPromoTrackingId;
	}
	public void setAffPromoTrackingId(Integer affPromoTrackingId) {
		this.affPromoTrackingId = affPromoTrackingId;
	}	

	@Column(name = "ref_cust_loyalty_history_id")
	public Integer getRefCustLoyaltyHistoryId() {
		return refCustLoyaltyHistoryId;
	}
	public void setRefCustLoyaltyHistoryId(Integer refCustLoyaltyHistoryId) {
		this.refCustLoyaltyHistoryId = refCustLoyaltyHistoryId;
	}	

	@Column(name = "ref_cust_loyalty_info_id")
	public Integer getRefCustLoyaltyInfoId() {
		return refCustLoyaltyInfoId;
	}
	public void setRefCustLoyaltyInfoId(Integer refCustLoyaltyInfoId) {
		this.refCustLoyaltyInfoId = refCustLoyaltyInfoId;
	}	

	@Column(name = "ref_tracking_id")
	public Integer getRefTrackingId() {
		return refTrackingId;
	}
	public void setRefTrackingId(Integer refTrackingId) {
		this.refTrackingId = refTrackingId;
	}	

	@Column(name = "rtf_promo_tracking_id")
	public Integer getRtfPromoTrackingId() {
		return rtfPromoTrackingId;
	}
	public void setRtfPromoTrackingId(Integer rtfPromoTrackingId) {
		this.rtfPromoTrackingId = rtfPromoTrackingId;
	}	

	@Column(name = "rtf_promo_offer_id")
	public Integer getRtfPromoOfferId() {
		return rtfPromoOfferId;
	}
	public void setRtfPromoOfferId(Integer rtfPromoOfferId) {
		this.rtfPromoOfferId = rtfPromoOfferId;
	}	

	@Column(name = "quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}	

	@Column(name = "original_order_total")
	public Double getOriginalOrderTotal() {
		return originalOrderTotal;
	}
	public void setOriginalOrderTotal(Double originalOrderTotal) {
		this.originalOrderTotal = originalOrderTotal;
	}	

	@Column(name = "order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}	

	@Column(name = "ticket_price")
	public Double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}	

	@Column(name = "sold_price")
	public Double getSoldPrice() {
		return soldPrice;
	}
	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="primary_payment_method")
	public PaymentMethod getPrimaryPaymentMethod() {
		return primaryPaymentMethod;
	}
	public void setPrimaryPaymentMethod(PaymentMethod primaryPaymentMethod) {
		this.primaryPaymentMethod = primaryPaymentMethod;
	}
	
	@Column(name="primary_transaction_id")
	public String getPrimaryTransactionId() {
		return primaryTransactionId;
	}
	public void setPrimaryTransactionId(String primaryTransactionId) {
		this.primaryTransactionId = primaryTransactionId;
	}

	@Column(name = "primary_payment_amount")
	public Double getPrimaryPayAmt() {
		return primaryPayAmt;
	}
	public void setPrimaryPayAmt(Double primaryPayAmt) {
		this.primaryPayAmt = primaryPayAmt;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="secondary_payment_method")
	public PartialPaymentMethod getSecondaryPaymentMethod() {
		return secondaryPaymentMethod;
	}
	public void setSecondaryPaymentMethod(
			PartialPaymentMethod secondaryPaymentMethod) {
		this.secondaryPaymentMethod = secondaryPaymentMethod;
	}
	
	@Column(name="secondary_transaction_id")
	public String getSecondaryTransactionId() {
		return secondaryTransactionId;
	}
	public void setSecondaryTransactionId(String secondaryTransactionId) {
		this.secondaryTransactionId = secondaryTransactionId;
	}

	@Column(name = "secondary_payment_amount")
	public Double getSecondaryPayAmt() {
		return secondaryPayAmt;
	}
	public void setSecondaryPayAmt(Double secondaryPayAmt) {
		this.secondaryPayAmt = secondaryPayAmt;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="third_payment_method")
	public PartialPaymentMethod getThirdPaymentMethod() {
		return thirdPaymentMethod;
	}
	public void setThirdPaymentMethod(PartialPaymentMethod thirdPaymentMethod) {
		this.thirdPaymentMethod = thirdPaymentMethod;
	}
	
	@Column(name="third_payment_transaction_id")
	public String getThirdTransactionId() {
		return thirdTransactionId;
	}
	public void setThirdTransactionId(String thirdTransactionId) {
		this.thirdTransactionId = thirdTransactionId;
	}

	@Column(name = "third_payment_amount")
	public Double getThirdPayAmt() {
		return thirdPayAmt;
	}
	public void setThirdPayAmt(Double thirdPayAmt) {
		this.thirdPayAmt = thirdPayAmt;
	}	


	@Enumerated(EnumType.STRING)
	@Column(name="order_type")
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}	

	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	public ApplicationPlatForm getAppPlatForm() {
		return appPlatForm;
	}
	public void setAppPlatForm(ApplicationPlatForm appPlatForm) {
		this.appPlatForm = appPlatForm;
	}
	
	@Column(name="session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public OrderStatus getStatus() {
		return status;
	}
	public void setStatus(OrderStatus status) {
		this.status = status;
	}
	
	@Column(name="created_time")
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	@Column(name="rtf_checkout_offer_id")
	public Integer getRtfCheckoutOfferId() {
		return rtfCheckoutOfferId;
	}
	public void setRtfCheckoutOfferId(Integer rtfCheckoutOfferId) {
		this.rtfCheckoutOfferId = rtfCheckoutOfferId;
	}
	
	@Column(name="order_offer_reward_history_id")
	public Integer getOrderOfferRewardHistoryId() {
		return orderOfferRewardHistoryId;
	}
	public void setOrderOfferRewardHistoryId(Integer orderOfferRewardHistoryId) {
		this.orderOfferRewardHistoryId = orderOfferRewardHistoryId;
	}
	
	@Column(name="ticket_on_day_before_event")
	public Boolean getTicketOnDayBeforeTheEvent() {
		return ticketOnDayBeforeTheEvent;
	}
	public void setTicketOnDayBeforeTheEvent(Boolean ticketOnDayBeforeTheEvent) {
		this.ticketOnDayBeforeTheEvent = ticketOnDayBeforeTheEvent;
	}
	
	
}
