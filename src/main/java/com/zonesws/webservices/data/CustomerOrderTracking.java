package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ProductType;

@Entity
@Table(name="customer_order_tracking")
public class CustomerOrderTracking implements Serializable{
	private Integer id;
	private String serverIpAddress;
	private String customerIpAddress;
	private String webConfigId;
	private String webXSign;
	private String webXToken;
	private ApplicationPlatForm platForm;
	private String sessionId;
	private Integer customerId;
	private Integer eventId;
	private Integer orderId;
	private Integer orderTicketGroupId;
	private Double orderTotal;
	private Boolean isFanPrice;
	private java.util.Date orderDate;
	private String promoOrReferralCode;
	private ProductType productType;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="server_ip_address")
	public String getServerIpAddress() {
		return serverIpAddress;
	}
	public void setServerIpAddress(String serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}
	
	
	@Column(name="customer_ip_address")
	public String getCustomerIpAddress() {
		return customerIpAddress;
	}
	
	public void setCustomerIpAddress(String customerIpAddress) {
		this.customerIpAddress = customerIpAddress;
	}
	@Column(name="web_config_id")
	public String getWebConfigId() {
		return webConfigId;
	}
	public void setWebConfigId(String webConfigId) {
		this.webConfigId = webConfigId;
	}
	
	@Column(name="web_xsign")
	public String getWebXSign() {
		return webXSign;
	}
	public void setWebXSign(String webXSign) {
		this.webXSign = webXSign;
	}
	
	@Column(name="web_xtoken")
	public String getWebXToken() {
		return webXToken;
	}
	public void setWebXToken(String webXToken) {
		this.webXToken = webXToken;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	@Index(name="platformIndex")	
	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}
	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}
	
	@Column(name="session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="order_ticket_group_id")
	public Integer getOrderTicketGroupId() {
		return orderTicketGroupId;
	}
	public void setOrderTicketGroupId(Integer orderTicketGroupId) {
		this.orderTicketGroupId = orderTicketGroupId;
	}
	
	@Column(name="order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	@Column(name="is_fan_price")
	public Boolean getIsFanPrice() {
		return isFanPrice;
	}
	public void setIsFanPrice(Boolean isFanPrice) {
		this.isFanPrice = isFanPrice;
	}
	
	@Column(name="order_date")
	public java.util.Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(java.util.Date orderDate) {
		this.orderDate = orderDate;
	}
	
	@Column(name="promotional_or_ref_code")
	public String getPromoOrReferralCode() {
		return promoOrReferralCode;
	}
	public void setPromoOrReferralCode(String promoOrReferralCode) {
		this.promoOrReferralCode = promoOrReferralCode;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="product_type")
	@Index(name="productTypeIndex")
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	
}
