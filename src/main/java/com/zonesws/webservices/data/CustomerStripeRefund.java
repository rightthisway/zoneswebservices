package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stripe_card_refund")
public class CustomerStripeRefund  implements Serializable{

	private Integer id;
	private Integer orderId;
	private Double amount;
	private String transactionId;
	private String refundId;
	private String status;
	private String reason;
	private Integer revertedUsedRewardPoints;
	private Integer revertedEarnedRewardPoints;
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="amount")
	public Double getAmount() {
		return amount;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@Column(name="charge_id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	@Column(name="refund_id")
	public String getRefundId() {
		return refundId;
	}
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="reason")
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name="reverted_used_points")
	public Integer getRevertedUsedRewardPoints() {
		return revertedUsedRewardPoints;
	}
	public void setRevertedUsedRewardPoints(Integer revertedUsedRewardPoints) {
		this.revertedUsedRewardPoints = revertedUsedRewardPoints;
	}
	
	@Column(name="reverted_earned_points")
	public Integer getRevertedEarnedRewardPoints() {
		return revertedEarnedRewardPoints;
	}
	public void setRevertedEarnedRewardPoints(Integer revertedEarnedRewardPoints) {
		this.revertedEarnedRewardPoints = revertedEarnedRewardPoints;
	}
	
	
}
