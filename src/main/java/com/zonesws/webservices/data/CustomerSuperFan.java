package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.LoyalFanType;
import com.zonesws.webservices.enums.Status;

 
/**
 * class to represent CustomerSuperFan.
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="customer_loyal_fan")
public class CustomerSuperFan implements Serializable{
	
	private Integer id;
	private Integer customerId;
	private LoyalFanType loyalFanType;
	private Integer artistOrTeamId;
	private String loyalFanName;
	private String state;
	private String zipCode;
	private String country;
	@JsonIgnore
	private Date startDate;
	@JsonIgnore
	private Date endDate;
	@JsonIgnore
	private Date createdDate;
	@JsonIgnore
	private Date updatedDate;
	private Status status;
	private Boolean ticketPurchased;
	@JsonIgnore
	private Boolean isEmailSent; 
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="loyal_fan_type")
    public LoyalFanType getLoyalFanType() {
		return loyalFanType;
	}
	public void setLoyalFanType(LoyalFanType loyalFanType) {
		this.loyalFanType = loyalFanType;
	}
	
	@Column(name="artist_or_team_id")
	public Integer getArtistOrTeamId() {
		return artistOrTeamId;
	}
	public void setArtistOrTeamId(Integer artistOrTeamId) {
		this.artistOrTeamId = artistOrTeamId;
	}
	
	@Column(name="loyal_name")
	public String getLoyalFanName() {
		return loyalFanName;
	}
	public void setLoyalFanName(String loyalFanName) {
		this.loyalFanName = loyalFanName;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Column(name="start_date")
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
//	@Column(name="end_date")
	 @Column(name="end_date",insertable=false, updatable=false)
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	@Column(name="Ticket_purchased")
	public Boolean getTicketPurchased() {
		return ticketPurchased;
	}
	public void setTicketPurchased(Boolean ticketPurchased) {
		this.ticketPurchased = ticketPurchased;
	}
	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="zip_code")
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="is_email_sent")
	public Boolean getIsEmailSent() {
		if(isEmailSent == null){
			isEmailSent = false;
		}
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	
	
}
	
