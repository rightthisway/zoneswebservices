package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;

@Entity
@Table(name = "track_discount_code_sharing")
public class DiscountCodeTracking implements Serializable{

	private Integer id;
	private Integer customerId;
	private String sharingType;
	private String sharingOption;
	private String sharedUrl;
	private ApplicationPlatForm platform;
	private String sessionId;
	private Date createdDate;
	private Date lastUpdated;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "platform")
	public ApplicationPlatForm getPlatform() {
		return platform;
	}
	public void setPlatform(ApplicationPlatForm platform) {
		this.platform = platform;
	}
	
	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name = "session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name = "sharing_type")
	public String getSharingType() {
		return sharingType;
	}
	public void setSharingType(String sharingType) {
		this.sharingType = sharingType;
	}
	
	@Column(name = "sharing_option")
	public String getSharingOption() {
		return sharingOption;
	}
	public void setSharingOption(String sharingOption) {
		this.sharingOption = sharingOption;
	}
	
	@Column(name = "shared_url")
	public String getSharedUrl() {
		return sharedUrl;
	}
	public void setSharedUrl(String sharedUrl) {
		this.sharedUrl = sharedUrl;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
	
}
