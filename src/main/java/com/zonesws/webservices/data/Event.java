package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.jobs.TeamCardImageUtil;
import com.zonesws.webservices.utils.JsonDateTimeSerializer;
import com.zonesws.webservices.utils.TicketUtil;

@Entity
@Table(name="event_details")
public class Event implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer eventId;
	private String eventName;
	@JsonIgnore
	private Date eventDate;
	@JsonIgnore
	private Date tempEventDate;
	@JsonIgnore
	private Date eventTime;
	private String eventDateTime;
	private String eventDateStr;
	private String eventTimeStr;
	@JsonIgnore
	private Long dayDifference;
	@JsonIgnore
	private Status eventStatus;
	@JsonIgnore
	private String mapUrl;
	private Integer venueId;
	private String venueName;
	private String city;
	private String state;
	private String country;
	private String pinCode;
	
	private Integer artistId;
	private String artistName;
	@JsonIgnore
	private Integer venueCategoryId;
	@JsonIgnore
	private String venueCategoryName;
	@JsonIgnore
	private Integer grandChildCategoryId;
	private String grandChildCategoryName;
	@JsonIgnore
	private Integer childCategoryId;
	private String childCategoryName;
	@JsonIgnore
	private Integer parentCategoryId;
	private String parentCategoryName;
	@JsonIgnore
	private Boolean rewardTheFanEnabled;
	@JsonIgnore
	private Boolean categoryTicketsEnabled;
	@JsonIgnore
	private Boolean firstTenRowsEnabled;
	//private Boolean presaleZoneTicketsEnabled;
	@JsonIgnore
	private Double ticketMinPrice;
	@JsonIgnore
	private Double ticketMaxPrice;
	@JsonIgnore
	private Integer salesCount;
	private String ticketPriceTag;
	private Boolean isSuperFanEvent;
	private Boolean isFavoriteEvent;
	private Integer customerId;
	private String svgText;
	private String isMapWithSvg;
	private String svgMapPath;
	private String svgWebViewUrl;
	private String shareLinkUrl;
	private String shareMessage;
	private String desktopShareLinkUrl;
	private String desktopShareMessage;
	private String shareTitle;
	
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date createDate;
	private String eventImageUrl;
	private String encryptionKey;
	private Integer totalEvents;
	private String dataType;
	private List<Integer> artistIds;
	@JsonIgnore
	private Artist artist;
	private Boolean showEventDateAsTBD;
	private String eventDateTBDValue;
	private String gmailBodyEventDetails;
	
	private String contestTicketsConfirmation;
	
	@JsonIgnore
	private String rtfEventUrll;

	@JsonIgnore
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	@JsonIgnore
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	@JsonIgnore
	private DateFormat tf = new SimpleDateFormat("hh:mm aa");
	
	@Id
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="event_name")
	public String getEventName() {
		if(null != eventName && !eventName.isEmpty()){
			eventName = eventName.replaceAll("�", "-");
		}
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	
	@Transient
	public String getEventDateTime() {
		if(eventDate != null){
			String eventTimeStr="TBD",eventDateStr="";
			if(null != eventTime ){
				eventTimeStr = tf.format(eventTime);
			}
			eventDateStr = df.format(eventDate);
			eventDateTime=eventDateStr+" "+eventTimeStr;
		}
		return eventDateTime;
	}

	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	
	@Transient
	public String getEventDateStr() {
		if(eventDate != null){
			eventDateStr = df.format(eventDate);
		}
		return eventDateStr;
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	@Transient
	public String getEventTimeStr() {
		if(eventDate != null){
			String eventTimeLocal="TBD";
			if(null != eventTime ){
				eventTimeLocal = tf.format(eventTime);
			}
			eventTimeStr = eventTimeLocal;
		}
		return eventTimeStr;
	}

	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}

	@Column(name="event_time")
	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	@Column(name="status")
	public Status getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(Status eventStatus) {
		this.eventStatus = eventStatus;
	}

	@Transient
	public String getMapUrl() {
		if(venueId !=  null && venueCategoryName !=  null && !venueCategoryName.isEmpty()){
			
		}
		return mapUrl;
	}

	public void setMapUrl(String mapUrl) {
		this.mapUrl = mapUrl;
	}
	
	

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="building")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	@Column(name="city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	
	@Column(name="state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name="country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name="postal_code")
	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}

	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}

	@Column(name="venue_category_name")
	public String getVenueCategoryName() {
		return venueCategoryName;
	}

	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}
	

	@Column(name="grand_child_category_id")
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}

	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}

	@Column(name="grand_child_category_name")
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}

	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}

	@Column(name="child_category_id")
	public Integer getChildCategoryId() {
		return childCategoryId;
	}

	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}

	@Column(name="child_category_name")
	public String getChildCategoryName() {
		return childCategoryName;
	}

	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
        
	@Column(name="parent_category_id")
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	@Column(name="parent_category_name")
	public String getParentCategoryName() {
		return parentCategoryName;
	}

	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}

	@Column(name="reward_thefan_enabled")
	public Boolean getRewardTheFanEnabled() {
		return rewardTheFanEnabled;
	}

	public void setRewardTheFanEnabled(Boolean rewardTheFanEnabled) {
		this.rewardTheFanEnabled = rewardTheFanEnabled;
	}

	@Column(name="category_tickets_enabled")
	public Boolean getCategoryTicketsEnabled() {
		return categoryTicketsEnabled;
	}

	public void setCategoryTicketsEnabled(Boolean categoryTicketsEnabled) {
		this.categoryTicketsEnabled = categoryTicketsEnabled;
	}

	@Column(name="vipminicats_enabled")
	public Boolean getFirstTenRowsEnabled() {
		return firstTenRowsEnabled;
	}

	public void setFirstTenRowsEnabled(Boolean firstTenRowsEnabled) {
		this.firstTenRowsEnabled = firstTenRowsEnabled;
	}

	/*@Column(name="lastrow_minicat_enabled")
	public Boolean getPresaleZoneTicketsEnabled() {
		return presaleZoneTicketsEnabled;
	}

	public void setPresaleZoneTicketsEnabled(Boolean presaleZoneTicketsEnabled) {
		this.presaleZoneTicketsEnabled = presaleZoneTicketsEnabled;
	}*/

	@Transient
	public Double getTicketMinPrice() {
		return ticketMinPrice;
	}

	public void setTicketMinPrice(Double ticketMinPrice) {
		this.ticketMinPrice = ticketMinPrice;
	}

	@Transient
	public Double getTicketMaxPrice() {
		return ticketMaxPrice;
	}

	public void setTicketMaxPrice(Double ticketMaxPrice) {
		this.ticketMaxPrice = ticketMaxPrice;
	}

	@Transient
	public String getTicketPriceTag() {
		if(null != ticketMinPrice && ticketMinPrice > 0){
			try {
				//ticketPriceTag ="$"+TicketUtil.getRoundedValueIntger(ticketMinPrice)+" to $"+TicketUtil.getRoundedValueIntger(ticketMaxPrice);
				//ticketPriceTag ="From $"+TicketUtil.getRoundedValueIntger(ticketMinPrice);
				ticketPriceTag ="SELECT EVENT";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ticketPriceTag ="Sold Out";
			} 
		}else{
			ticketPriceTag ="Sold Out";
		}
		return ticketPriceTag;
	}

	public void setTicketPriceTag(String ticketPriceTag) {
		this.ticketPriceTag = ticketPriceTag;
	}

	@Transient
	public Boolean getIsFavoriteEvent() {
		if(null == isFavoriteEvent){
			isFavoriteEvent= false;
		}
		return isFavoriteEvent;
	}

	public void setIsFavoriteEvent(Boolean isFavoriteEvent) {
		this.isFavoriteEvent = isFavoriteEvent;
	}

	@Transient
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Transient
	public String getSvgText() {
		return svgText;
	}
	public void setSvgText(String svgText) {
		this.svgText = svgText;
	}
	
	@Transient
	public String getIsMapWithSvg() {
		return isMapWithSvg;
	}
	public void setIsMapWithSvg(String isMapWithSvg) {
		this.isMapWithSvg = isMapWithSvg;
	}
	
	@Transient
	public String getSvgMapPath() {
		return svgMapPath;
	}
	public void setSvgMapPath(String svgMapPath) {
		this.svgMapPath = svgMapPath;
	}

	@Transient
	public String getShareLinkUrl() {
		return shareLinkUrl;
	}

	public void setShareLinkUrl(String shareLinkUrl) {
		this.shareLinkUrl = shareLinkUrl;
	}
	
	@Transient
	public String getShareMessage() {
		return shareMessage;
	}

	public void setShareMessage(String shareMessage) {
		this.shareMessage = shareMessage;
	}

	@Transient
	public Boolean getIsSuperFanEvent() {
		if(null == isSuperFanEvent){
			isSuperFanEvent = false;
		}
		return isSuperFanEvent;
	}

	public void setIsSuperFanEvent(Boolean isSuperFanEvent) {
		this.isSuperFanEvent = isSuperFanEvent;
	}

	@Transient
	public String getSvgWebViewUrl() {
		return svgWebViewUrl;
	}

	public void setSvgWebViewUrl(String svgWebViewUrl) {
		this.svgWebViewUrl = svgWebViewUrl;
	}

	@Transient
	public Date getTempEventDate() {
		return tempEventDate;
	}

	public void setTempEventDate(Date tempEventDate) {
		this.tempEventDate = tempEventDate;
	}
	@Transient
	public Integer getSalesCount() {
		return salesCount;
	}

	public void setSalesCount(Integer salesCount) {
		this.salesCount = salesCount;
	}
	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Transient
	public Long getDayDifference() {
		if(null == dayDifference){
			dayDifference =0l;
		}
		return dayDifference;
	}

	public void setDayDifference(Long dayDifference) {
		this.dayDifference = dayDifference;
	}

	@Transient
	public String getEventImageUrl() {
		return eventImageUrl;
	}

	@Transient
	public void setEventImageUrl(ApplicationPlatForm platForm) {
		artistId = getArtistId();
		if(artistId != null){
			eventImageUrl = TeamCardImageUtil.getTeamCardImageByArtist(artistId, platForm);
		}else{
			eventImageUrl= null;
		}
	}
	@Transient
	public void setEventImageUrl(Integer artistId,ApplicationPlatForm platForm) {
		if(artistId != null){
			eventImageUrl = TeamCardImageUtil.getTeamCardImageByArtist(artistId, platForm);
		}else{
			eventImageUrl= null;
		}
	}

	@Transient
	public String getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	@Transient
	public Integer getTotalEvents() {
		if(null == totalEvents){
			totalEvents = 0;
		}
		return totalEvents;
	}

	public void setTotalEvents(Integer totalEvents) {
		this.totalEvents = totalEvents;
	}

	@Transient
	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	@Transient
	public String getDesktopShareLinkUrl() {
		return desktopShareLinkUrl;
	}

	public void setDesktopShareLinkUrl(String desktopShareLinkUrl) {
		this.desktopShareLinkUrl = desktopShareLinkUrl;
	}

	@Transient
	public List<Integer> getArtistIds() {
		return artistIds;
	}

	public void setArtistIds(List<Integer> artistIds) {
		this.artistIds = artistIds;
	}

	@Transient
	public Artist getArtist() {
		if(eventId != null){
			try{
				artist = DAORegistry.getQueryManagerDAO().getArtistByEventId(eventId);
			}catch (Exception e) {
				e.printStackTrace();
				artist = null;
			}
			
		}
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}
	
	@Transient
	public Integer getArtistId() {
		if(null != artist){
			artistId = artist.getId();
		}else{
			getArtist();
			if(null != artist){
				artistId = artist.getId();
			}
		}
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	@Transient
	public String getArtistName() {
		if(null != artist){
			artistName = artist.getName();
		}else{
			getArtist();
			if(null != artist){
				artistName = artist.getName();
			}
		}
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	@Transient
	public Boolean getShowEventDateAsTBD() {
		if(null == showEventDateAsTBD ){
			showEventDateAsTBD=false;
		}
		return showEventDateAsTBD;
	}

	public void setShowEventDateAsTBD(Boolean showEventDateAsTBD) {
		this.showEventDateAsTBD = showEventDateAsTBD;
	}

	@Transient
	public String getEventDateTBDValue() {
		if(null == eventDateTBDValue){
			eventDateTBDValue ="";
		}
		return eventDateTBDValue;
	}

	public void setEventDateTBDValue(String eventDateTBDValue) {
		this.eventDateTBDValue = eventDateTBDValue;
	}
	
	@Transient
	public String getGmailBodyEventDetails() {
		if(null !=desktopShareLinkUrl){
			String eventNameTemp = eventName.replaceAll("\\W+", " ");
			String venueNameTemp = venueName.replaceAll("\\W+", " ");
			String desktopShareLinkUrlTemp = desktopShareLinkUrl.replaceAll("&", "%26");
			gmailBodyEventDetails = eventNameTemp+","+eventDateTime+"%0A"+venueNameTemp+","+city+","+state+"%0A"+desktopShareLinkUrlTemp;
		}
		
		return gmailBodyEventDetails;
	}

	public void setGmailBodyEventDetails(String gmailBodyEventDetails) {
		this.gmailBodyEventDetails = gmailBodyEventDetails;
	}

	@Transient
	public String getDesktopShareMessage() {
		return desktopShareMessage;
	}

	public void setDesktopShareMessage(String desktopShareMessage) {
		this.desktopShareMessage = desktopShareMessage;
	}

	@Transient
	public String getShareTitle() {
		return shareTitle;
	}

	public void setShareTitle(String shareTitle) {
		this.shareTitle = shareTitle;
	}
	@Transient
	public String getRtfEventUrll() {
		return rtfEventUrll;
	}

	public void setRtfEventUrll(String rtfEventUrll) {
		this.rtfEventUrll = rtfEventUrll;
	}
	@Transient
	public String getContestTicketsConfirmation() {
		if(contestTicketsConfirmation == null) {
			contestTicketsConfirmation = "";
		}
		return contestTicketsConfirmation;
	}

	public void setContestTicketsConfirmation(String contestTicketsConfirmation) {
		this.contestTicketsConfirmation = contestTicketsConfirmation;
	}
	
	
}
