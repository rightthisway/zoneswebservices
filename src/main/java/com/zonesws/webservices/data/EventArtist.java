package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

 
/**
 * class to represent CustomerFavoritesHandler
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="event_artist")
public class EventArtist implements Serializable{
	
	private Integer eventId;
	private Integer artistId;
	private String artistName;
	private String eventName;
	
	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Transient
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	@Transient
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	
	
	
}
