package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="exposure")
public class Exposure implements Serializable {
 
 
	 private Integer id;
	 private String shortDescription;
	 private OwnerType ownerType;
	 private Integer exposure;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	 @Column(name="id")
	 public Integer getId() {
	  return id;
	 }
	 public void setId(Integer id) {
	  this.id = id;
	 }
	 @Column(name="short_description")
	 public String getShortDescription() {
	  return shortDescription;
	 }
	 public void setShortDescription(String shortDescription) {
	  this.shortDescription = shortDescription;
	 }
	 @OneToOne
	 @JoinColumn(name="owner_type_id")
	 public OwnerType getOwnerType() {
	  return ownerType;
	 }
	 public void setOwnerType(OwnerType ownerType) {
	  this.ownerType = ownerType;
	 }
	 @Column(name="exposure")
	 public Integer getExposure() {
	  return exposure;
	 }
	 public void setExposure(Integer exposure) {
	  this.exposure = exposure;
	 }
	 
}
