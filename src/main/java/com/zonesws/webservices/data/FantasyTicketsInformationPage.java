package com.zonesws.webservices.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

/**
 * represents FST info like about us, contact us, faq, terms of use
 * @author Ulaganathan
 *
 */
@XStreamAlias("FantasyTicketsInformationPage")
public class FantasyTicketsInformationPage {
	
	private Integer status;
	private Error error;
	private String bodyText;
	private String imageURL;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public String getBodyText() {
		return bodyText;
	}
	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	
	
}
