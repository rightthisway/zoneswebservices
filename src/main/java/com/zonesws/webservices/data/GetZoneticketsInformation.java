package com.zonesws.webservices.data;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

/**
 * represents zonetickets info like about us, contact us, faq, terms of use
 * @author Mehul
 *
 */
@XStreamAlias("ZoneticketsInformation")
public class GetZoneticketsInformation {
	private Integer status;
	private Error error;
	private List<ZoneticketsInformation> zoneticketsInformation;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<ZoneticketsInformation> getZoneticketsInformation() {
		return zoneticketsInformation;
	}
	public void setZoneticketsInformation(
			List<ZoneticketsInformation> zoneticketsInformation) {
		this.zoneticketsInformation = zoneticketsInformation;
	}
	
}
