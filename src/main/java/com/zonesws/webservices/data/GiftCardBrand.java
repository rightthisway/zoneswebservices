package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gift_card_brand")
public class GiftCardBrand  implements Serializable{

	private Integer id;
	private String name;
	private String description;
	private Integer quantity;
	private String status;
	private String imageUrl;
	private String outOfStackImageUrl;
	private Boolean displayOutOfStack;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "qty_limit")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "image_url")
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Column(name = "out_of_sotck_image_url")
	public String getOutOfStackImageUrl() {
		return outOfStackImageUrl;
	}
	public void setOutOfStackImageUrl(String outOfStackImageUrl) {
		this.outOfStackImageUrl = outOfStackImageUrl;
	}
	
	@Column(name = "diplay_out_of_stock")
	public Boolean getDisplayOutOfStack() {
		return displayOutOfStack;
	}
	public void setDisplayOutOfStack(Boolean displayOutOfStack) {
		this.displayOutOfStack = displayOutOfStack;
	}
	
}
