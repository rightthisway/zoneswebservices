package com.zonesws.webservices.data;

public class GiftCardBrandDTO {

	private Integer bId;
	private String bName;
	private String bDesc;  
	private String imgUrl;  
	private String lCost;
	private Boolean nacards;//No active cards to redeem 
	
	
	public Integer getbId() {
		return bId;
	}
	public void setbId(Integer bId) {
		this.bId = bId;
	}
	public String getbName() {
		return bName;
	}
	public void setbName(String bName) {
		this.bName = bName;
	}
	public String getbDesc() {
		return bDesc;
	}
	public void setbDesc(String bDesc) {
		this.bDesc = bDesc;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getlCost() {
		lCost = "";
		return lCost;
	}
	public void setlCost(String lCost) {
		this.lCost = lCost;
	}
	public Boolean getNacards() {
		if(nacards == null) {
			nacards = false;
		}
		return nacards;
	}
	public void setNacards(Boolean nacards) {
		this.nacards = nacards;
	}
	
}
