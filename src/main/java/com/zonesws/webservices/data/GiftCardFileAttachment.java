package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.FileType;

@Entity
@Table(name="giftcard_order_attachment")
public class GiftCardFileAttachment implements Serializable{

	private Integer id;
	@JsonIgnore
	private Integer orderId;
	private FileType type;
	@JsonIgnore
	private String filePath;
	private Integer position;
	@JsonIgnore
	private Integer fileTypeId;
	private String downloadUrl;
	@JsonIgnore
	private Boolean isFileSent;
	@JsonIgnore
	private Boolean isEmailSent;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="file_type")
	public FileType getType() {
		return type;
	}
	
	public void setType(FileType type) {
		this.type = type;
	}
	
	
	@Column(name="file_path")
	public String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
	@Column(name="position")
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	@Transient
	public Integer getFileTypeId() {
		if(type!=null){
			fileTypeId = type.ordinal();
		}
		return fileTypeId;
	}
	public void setFileTypeId(Integer fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	
	@Transient
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="is_file_sent")
	public Boolean getIsFileSent() {
		return isFileSent;
	}
	public void setIsFileSent(Boolean isFileSent) {
		this.isFileSent = isFileSent;
	}
	
	@Column(name="is_email_sent")
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	
	
	
	
	
}
