package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.zonesws.webservices.dao.implementaion.DAORegistry;

 /**
  * class to represent Grand child category of a tour.
  * @author hamin
  *
  */
@Entity
@Table(name="grand_child_category")
public class GrandChildCategory implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XStreamOmitField
	private String name;
	@XStreamOmitField
	private Integer id;
	private Integer childCatId;
	@JsonIgnore
	private ChildCategory  childCategory;
	@JsonIgnore
	private Boolean displayOnSearch;
	
	/**
	 * @return the id
	 */
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the parentCategory
	 *//*
	@OneToOne
	@JoinColumn(name="child_category_id")
	public ChildCategory getChildCategory() {
		return childCategory;
	}
	*//**
	 * @param parentCategory the parentCategory to set
	 *//*
	public void setChildCategory(ChildCategory childCategory) {
		this.childCategory = childCategory;
	}
	
	@Column(name="tmat_grand_child_category_id")
	public Integer getTmatGrandChildCategoryid() {
		return tmatGrandChildCategoryid;
	}
	public void setTmatGrandChildCategoryid(Integer tmatGrandChildCategoryid) {
		this.tmatGrandChildCategoryid = tmatGrandChildCategoryid;
	}
	*//**
	 * String representation
	 *//*
	@Override
	public String toString() {
		 
		return  "[ id:"+id+" name: "+name+" childCategory: "+childCategory.getName()+"]";
	}*/
	
	
	@Transient
	public Integer getChildCatId() {
		return childCatId;
	}
	public void setChildCatId(Integer childCatId) {
		this.childCatId = childCatId;
	}
	
	@Transient
	public ChildCategory getChildCategory() {
		if(null !=  childCatId && childCatId> 0){
			childCategory = DAORegistry.getChildCategoryDAO().get(childCatId);
		}
		return childCategory;
	}
	public void setChildCategory(ChildCategory childCategory) {
		this.childCategory = childCategory;
	}
	
	@Column(name="display_on_search")
	public Boolean getDisplayOnSearch() {
		return displayOnSearch;
	}
	public void setDisplayOnSearch(Boolean displayOnSearch) {
		this.displayOnSearch = displayOnSearch;
	}
}
