package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.zonesws.webservices.enums.HoldTicketStatus;
/**
 *
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="hold_tickets")
public class HoldTicket implements Serializable{
	private Integer id;
	private String configId;
	private Integer ticketGroupId;
	private HoldTicketStatus holdStatus;
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="config_id")
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="hold_status")
	@Index(name="holdStatusIndex")
	public HoldTicketStatus getHoldStatus() {
		return holdStatus;
	}
	public void setHoldStatus(HoldTicketStatus holdStatus) {
		this.holdStatus = holdStatus;
	}
	
	
	
	
		
	
}
