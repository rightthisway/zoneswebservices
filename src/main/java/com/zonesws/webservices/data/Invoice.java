package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.enums.InvoiceStatus;


@Entity
@Table(name = "invoice")
public class Invoice implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="invoice_total")
	private Double invoiceTotal;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="ext_po_no")
	private String extPONo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private InvoiceStatus status;
	
	@Column(name="order_id")
	private Integer customerOrderId;
	
	@Column(name="invoice_type")
	private String invoiceType;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="exp_delivery_date")
	private Date expDeliveryDate;
	
	@Column(name="void_date")
	private Date voidDate;
	
	@Column(name="last_updated")
	private Date lastUpdated;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="last_updated_by")
	private String lastUpdatedBy;
	
	@Column(name="tix_bar_code")
	private String barcode;
	
	@Column(name="is_sent")
	private boolean isSent;
	
	@Column(name="sent_date")
	private Date sentDate;
	
	@Column(name="ticket_count")
	private Integer ticketCount;

	@Column(name="realtix_map")
	private String realTixMap;
	
	@Transient
	private String realTixShippingMethod;
	
	@Column(name = "shipping_method_id")
	private Integer shippingMethodId;
	
	@Column(name="realtix_delivered")
	private Boolean realTixDelivered;
	
	@Column(name="is_real_tix_upload")
	private String isRealTixUploaded;
	
	@Column(name="tracking_no")
	private String trackingNo;
	
	@Column(name="real_ticket_file_path")
	private String realTicketFilePath;
	
	@Column(name="purchase_order_no")
	private String poNo;
	
	@Column(name="refund_amount")
	private Double refundedAmount;
	
	@Column(name = "broker_id")
	private Integer brokerId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getInvoiceTotal() {
		return invoiceTotal;
	}

	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getExtPONo() {
		return extPONo;
	}

	public void setExtPONo(String extPONo) {
		this.extPONo = extPONo;
	}


	public Integer getCustomerOrderId() {
		return customerOrderId;
	}

	public void setCustomerOrderId(Integer customerOrderId) {
		this.customerOrderId = customerOrderId;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public boolean getIsSent() {
		return isSent;
	}

	public void setIsSent(boolean isSent) {
		this.isSent = isSent;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public Integer getTicketCount() {
		return ticketCount;
	}

	public void setTicketCount(Integer ticketCount) {
		this.ticketCount = ticketCount;
	}

	public String getRealTixMap() {
		return realTixMap;
	}

	public void setRealTixMap(String realTixMap) {
		this.realTixMap = realTixMap;
	}

	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}

	public String getIsRealTixUploaded() {
		return isRealTixUploaded;
	}

	public void setIsRealTixUploaded(String isRealTixUploaded) {
		this.isRealTixUploaded = isRealTixUploaded;
	}

	public String getRealTixShippingMethod() {
		return realTixShippingMethod;
	}

	public void setRealTixShippingMethod(String realTixShippingMethod) {
		this.realTixShippingMethod = realTixShippingMethod;
	}

	public Boolean getRealTixDelivered() {
		return realTixDelivered;
	}

	public void setRealTixDelivered(Boolean realTixDelivered) {
		this.realTixDelivered = realTixDelivered;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public Date getExpDeliveryDate() {
		return expDeliveryDate;
	}
	public void setExpDeliveryDate(Date expDeliveryDate) {
		this.expDeliveryDate = expDeliveryDate;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Date getVoidDate() {
		return voidDate;
	}
	public void setVoidDate(Date voidDate) {
		this.voidDate = voidDate;
	}

	public Integer getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}

	public String getRealTicketFilePath() {
		return realTicketFilePath;
	}

	public void setRealTicketFilePath(String realTicketFilePath) {
		this.realTicketFilePath = realTicketFilePath;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}

	public Double getRefundedAmount() {
		return refundedAmount;
	}

	public void setRefundedAmount(Double refundedAmount) {
		this.refundedAmount = refundedAmount;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	
	
}