package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.Util;

@Entity
@Table(name = "invoice_refund")
public class InvoiceRefund  implements Serializable{

	private Integer id;
	private Integer orderId;
	private Double amount;
	private String transactionId;
	private String refundId;
	private String status;
	private String reason;
	private Double revertedUsedRewardPoints;
	private Double revertedEarnedRewardPoints;
	private Date createdTime;
	private String refundType;
	private String refundedBy;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="amount")
	public Double getAmount() {
		try{
			if(amount != null){
				return TicketUtil.getRoundedValue(amount);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return amount;
	}
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@Column(name="charge_id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	@Column(name="refund_id")
	public String getRefundId() {
		return refundId;
	}
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="reason")
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name="reverted_used_points")
	public Double getRevertedUsedRewardPoints() {
		try{
			if(revertedUsedRewardPoints != null){
				return TicketUtil.getRoundedValue(revertedUsedRewardPoints);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return revertedUsedRewardPoints;
	}
	public void setRevertedUsedRewardPoints(Double revertedUsedRewardPoints) {
		this.revertedUsedRewardPoints = revertedUsedRewardPoints;
	}
	
	@Column(name="reverted_earned_points")
	public Double getRevertedEarnedRewardPoints() {
		try{
			if(revertedEarnedRewardPoints != null){
				return TicketUtil.getRoundedValue(revertedEarnedRewardPoints);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return revertedEarnedRewardPoints;
	}
	public void setRevertedEarnedRewardPoints(Double revertedEarnedRewardPoints) {
		this.revertedEarnedRewardPoints = revertedEarnedRewardPoints;
	}
	
	@Column(name = "create_time")
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
	@Column(name = "refund_type")
	public String getRefundType() {
		return refundType;
	}
	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}
	
	@Column(name = "refunded_by")
	public String getRefundedBy() {
		return refundedBy;
	}
	public void setRefundedBy(String refundedBy) {
		this.refundedBy = refundedBy;
	}
	
}
