package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.FileType;
import com.zonesws.webservices.enums.TransactionType;

@Entity
@Table(name="invoice_ticket_attachment")
public class InvoiceTicketAttachment implements Serializable{

	private Integer id;
	@JsonIgnore
	private Integer invoiceId;
	private FileType type;
	@JsonIgnore
	private String filePath;
	private String extension;
	private Integer position;
	@JsonIgnore
	private Integer fileTypeId;
	private String downloadUrl;
	private String fileName;
	private TransactionType trxType;
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="file_type")
	@Enumerated(EnumType.ORDINAL)
	public FileType getType() {
		return type;
	}
	
	public void setType(FileType type) {
		this.type = type;
	}
	
	
	@Column(name="file_path")
	public String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	@Column(name="file_extension")
	public String getExtension() {
		if(null == extension) {
			extension= "";
		}
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	@Column(name="position")
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	@Transient
	public Integer getFileTypeId() {
		if(type!=null){
			fileTypeId = type.ordinal();
		}
		return fileTypeId;
	}
	public void setFileTypeId(Integer fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	
	@Transient
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	
	@Transient
	public String getFileName() {
		if(null == fileName) {
			fileName= "";
		}
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Transient
	public TransactionType getTrxType() {
		if(null == trxType) {
			trxType = TransactionType.TIX;
		}
		return trxType;
	}
	public void setTrxType(TransactionType trxType) {
		this.trxType = trxType;
	}
	
	
}
