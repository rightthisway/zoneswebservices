package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ipconfig")
public class IpConfig implements Serializable{
	private Integer id;
	private WebServiceConfig webServiceConfig;
	private String ipAddress;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="webconfig_id")
	public WebServiceConfig getWebServiceConfig() {
		return webServiceConfig;
	}
	public void setWebServiceConfig(WebServiceConfig webServiceConfig) {
		this.webServiceConfig = webServiceConfig;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}
