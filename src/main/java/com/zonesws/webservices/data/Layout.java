package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.Status;

@Entity
@Table(name="layout")
public class Layout  implements Serializable{
	
	/**
	 * This serialId is not related with anything.
	 * it has been added to remove warning from page
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String layoutName;
	private String description;
	private Status status;
	public static final Integer DEFAULT=1;
	
	
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the layoutName
	 */
	
	@Column(name="layoutName")
	public String getLayoutName() {
		return layoutName;
	}
	/**
	 * @param layoutName the layoutName to set
	 */
	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}
	/**
	 * @return the description
	 */
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * 
	 * @return Status
	 */
	@Column(name="status_id")
	public Status getStatus() {
		return status;
	}
	/**
	 * @param Status the Status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
}
