package com.zonesws.webservices.data;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

/*@SuppressWarnings("serial")
@Entity
@Table(name="artist")*/
public class League implements Serializable  {
	
	private Integer id;
	private String name;
	@JsonIgnore
	private String categoryName;
	@JsonIgnore
	private Integer grandChildId;
	@JsonIgnore
	private Integer childId;
	@JsonIgnore
	private String status;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	
	
}

