package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.zonesws.webservices.enums.HoldTicketStatus;
import com.zonesws.webservices.enums.TicketStatus;

/**
 *  class to represent Ticket in a locked status, after click on buy button for a ticket ticket becomes LockedTicket 
 * @author hamin
 *
 */
@Entity
@Table(name="locked_tickets")
public class LockedTicket implements Serializable{

	private Integer id;
	private ZPSessionCart cartItem;
	private Integer lockedQty;
	private Integer ticketGroupId;
	private HoldTicketStatus lockedTicketsStatus=HoldTicketStatus.ACTIVE;
	
	
	/**
	 * 
	 * @return id
	 */
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, 
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	/**
	 * 
	 * @return lockedQty
	 */
	@Column(name="locked_qty")
	public Integer getLockedQty() {
		return lockedQty;
	}
	public void setLockedQty(Integer lockedQty) {
		this.lockedQty = lockedQty;
	}
	
	
	 /**
	  * 
	  * @return lockedTicketsStatus 
	  */
	@Column(name="locked_tickets_status", nullable=false)
	@Enumerated(EnumType.STRING)
	@Index(name="lockedTicketsStatusIndex")
	public HoldTicketStatus getLockedTicketsStatus() {
		return lockedTicketsStatus;
	}
	/**
	 * 
	 * @param lockedTicketsStatus ,ticket status (locked , active ) to set
	 */
	public void setLockedTicketsStatus(HoldTicketStatus lockedTicketsStatus) {
		this.lockedTicketsStatus = lockedTicketsStatus;
	}

	/**
	 * 
	 * @return cartItem
	 */
	@ManyToOne
	@JoinColumn(name="cartitem_id")	 
	public ZPSessionCart getCartItem() {
		return cartItem;
	}
	/**
	 * 
	 * @param cartItem , Session cart Item to set
	 */
	public void setCartItem(ZPSessionCart cartItem) {
		this.cartItem = cartItem;
	}
	
	
	 
}
