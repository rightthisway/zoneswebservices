package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * class to represent a LoyaltySettings 
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="Loyalty_Settings")
public class LoyaltySettings implements Serializable {
	
	private Integer id;
	private Double rewardConversion;
	private Double dollerConversion;
	private Double affiliatePromoRewardConv;
	private Double affiliateCashCreditConv;
	private Date fromDate;
	private Date toDate;
	private Date updatedTime;
	private String status;
	private String referralRewardCreditType;
	private Double contestCustReferralRewardPerc; 
	
	private Integer quizAffiliateReferralCreditMaxDays;
	
	private Integer quizParticipantSuperFanStars;
	private Integer quizReferralSuperFanStars;
	private Boolean enableSpecialReferralOffer;
	private Double specialReferralReward; 
	
	/*private Boolean livesToTireOneCustomer;
	private Boolean livesToPrimaryCustomer;
	private Integer noOfConsecutiveWeeks;
	private Integer noOfGameEachWeek;
	private Double tireOneCustInstantRewardDollar;
	private Double primaryCustInstantRewardDollar;*/
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="Reward_Conversion")
	public Double getRewardConversion() {
		return rewardConversion;
	}
	public void setRewardConversion(Double rewardConversion) {
		this.rewardConversion = rewardConversion;
	}
	
	@Column(name="Dollar_Conversion")
	public Double getDollerConversion() {
		return dollerConversion;
	}
	public void setDollerConversion(Double dollerConversion) {
		this.dollerConversion = dollerConversion;
	}
	
	@Column(name="From_Date")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	@Column(name="To_Date")
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	
	@Column(name="affiliate_promo_reward_conv")
	public Double getAffiliatePromoRewardConv() {
		return affiliatePromoRewardConv;
	}
	public void setAffiliatePromoRewardConv(Double affiliatePromoRewardConv) {
		this.affiliatePromoRewardConv = affiliatePromoRewardConv;
	}
	
	@Column(name="affiliate_cash_credit")
	public Double getAffiliateCashCreditConv() {
		return affiliateCashCreditConv;
	}
	public void setAffiliateCashCreditConv(Double affiliateCashCreditConv) {
		this.affiliateCashCreditConv = affiliateCashCreditConv;
	}
	
	@Column(name="contest_cust_referral_reward_perc")
	public Double getContestCustReferralRewardPerc() {
		return contestCustReferralRewardPerc;
	}
	public void setContestCustReferralRewardPerc(Double contestCustReferralRewardPerc) {
		this.contestCustReferralRewardPerc = contestCustReferralRewardPerc;
	}
	
	@Column(name="referral_reward_credit_type")
	public String getReferralRewardCreditType() {
		return referralRewardCreditType;
	}
	public void setReferralRewardCreditType(String referralRewardCreditType) {
		this.referralRewardCreditType = referralRewardCreditType;
	}
	
	@Column(name="quiz_affiliate_referral_credit_max_days")
	public Integer getQuizAffiliateReferralCreditMaxDays() {
		return quizAffiliateReferralCreditMaxDays;
	}
	public void setQuizAffiliateReferralCreditMaxDays(Integer quizAffiliateReferralCreditMaxDays) {
		this.quizAffiliateReferralCreditMaxDays = quizAffiliateReferralCreditMaxDays;
	}
	
	@Column(name="participant_sf_star")
	public Integer getQuizParticipantSuperFanStars() {
		if(null == quizParticipantSuperFanStars) {
			quizParticipantSuperFanStars = 0;
		}
		return quizParticipantSuperFanStars;
	}
	public void setQuizParticipantSuperFanStars(Integer quizParticipantSuperFanStars) {
		this.quizParticipantSuperFanStars = quizParticipantSuperFanStars;
	}
	
	@Column(name="referral_sf_star")
	public Integer getQuizReferralSuperFanStars() {
		if(null == quizReferralSuperFanStars) {
			quizReferralSuperFanStars = 0;
		}
		return quizReferralSuperFanStars;
	}
	public void setQuizReferralSuperFanStars(Integer quizReferralSuperFanStars) {
		this.quizReferralSuperFanStars = quizReferralSuperFanStars;
	}
	
	@Column(name="enable_special_referral_offer")
	public Boolean getEnableSpecialReferralOffer() {
		if(null == enableSpecialReferralOffer) {
			enableSpecialReferralOffer = false;
		}
		return enableSpecialReferralOffer;
	}
	public void setEnableSpecialReferralOffer(Boolean enableSpecialReferralOffer) {
		this.enableSpecialReferralOffer = enableSpecialReferralOffer;
	}
	
	@Column(name="special_referral_reward")
	public Double getSpecialReferralReward() {
		if(null == specialReferralReward) {
			specialReferralReward = 0.00;
		}
		return specialReferralReward;
	}
	public void setSpecialReferralReward(Double specialReferralReward) {
		this.specialReferralReward = specialReferralReward;
	}
}
