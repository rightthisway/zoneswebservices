package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;

@Entity
@Table(name="order_payment_breakup")
public class OrderPaymentBreakup implements Serializable{
	private Integer id;
	private Integer orderId;
	private PaymentMethod paymentMethod;
	private String transactionId;
	private Double paymentAmount; 
	private String paymentAmountStr; 
	@JsonIgnore
	private String doneBy;
	@JsonIgnore
	private Date paymentDate;
	private Integer orderSequence;
	private String iconUrl;
	private String paymentDescription;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="payment_method")
	@Enumerated(EnumType.STRING)
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	@Column(name="payment_amount")
	public Double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
	@Transient
	public String getIconUrl() {
		if(id != null){
			URLUtil.getPaymentIcon(this);
		}
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	@Transient
	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	@Column(name="transaction_id")
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name="done_by")
	public String getDoneBy() {
		return doneBy;
	}

	public void setDoneBy(String doneBy) {
		this.doneBy = doneBy;
	}

	@Column(name="payment_date")
	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	@Column(name="payment_seq")
	public Integer getOrderSequence() {
		return orderSequence;
	}
	public void setOrderSequence(Integer orderSequence) {
		this.orderSequence = orderSequence;
	}
	
	@Transient
	public String getPaymentAmountStr() {
		if(null == paymentAmount){
			paymentAmount = 0.0;
		}
		try {
			paymentAmountStr = "US $"+TicketUtil.getRoundedValueString(paymentAmount);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return paymentAmountStr;
	}
	public void setPaymentAmountStr(String paymentAmountStr) {
		this.paymentAmountStr = paymentAmountStr;
	}
	
	
}
