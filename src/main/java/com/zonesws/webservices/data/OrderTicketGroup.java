package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.utils.TicketUtil;

/**
 * Entity class for OrderTicketGroup
 * @author kulaganathan
 *
 */
@Entity
@Table(name= "order_ticket_group_details")
public class OrderTicketGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="ticket_group_id")
	private Integer ticketGroupId;
	
	@Column(name="order_id")
	private Integer orderId;
	
	@Column(name="zone")
	private String zone;
	
	@Column(name="section")
	private String section;
	
	@Column(name="row")
	private String row;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="seat")
	private String seat;
	
	@Column(name="actual_price")
	private Double actualPrice;
	
	@Column(name="sold_price")
	private Double soldPrice;
	
	@Transient
	private String actualPriceStr;
	@Transient
	private String soldPriceStr;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getZone() {
		if(null !=zone && zone.equals("N/A")){
			zone = "--";
		}
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public Double getActualPrice() {
		if(actualPrice == null){
			actualPrice =0.00;
		}
		return actualPrice;
	}

	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}

	public Double getSoldPrice() {
		if(soldPrice == null){
			soldPrice = 0.00;
		}
		return soldPrice;
	}

	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}

	public String getActualPriceStr() {
		try {
			actualPriceStr = TicketUtil.getRoundedValueString(actualPrice);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return actualPriceStr;
	}

	public void setActualPriceStr(String actualPriceStr) {
		this.actualPriceStr = actualPriceStr;
	}

	public String getSoldPriceStr() {
		try {
			soldPriceStr = TicketUtil.getRoundedValueString(soldPrice);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return soldPriceStr;
	}

	public void setSoldPriceStr(String soldPriceStr) {
		this.soldPriceStr = soldPriceStr;
	}
	
	
}