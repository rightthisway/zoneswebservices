package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="owner_type")

public class OwnerType  implements Serializable {
 
 
	private Integer id;
	private String ownerDescription;
	
	
	 
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		 return id;
	}
	public void setId(Integer id) {
		 this.id = id;
	}
	 
	@Column(name="owner_description")
	public String getOwnerDescription() {
		 return ownerDescription;
	}
	public void setOwnerDescription(String ownerDescription) {
		 this.ownerDescription = ownerDescription;
	}
	
	

 
}
