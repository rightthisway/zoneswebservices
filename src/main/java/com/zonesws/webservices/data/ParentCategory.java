package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
/**
 * class to represent parent category (sports , concert , theater, none etc)
 * @author hamin
 *
 */
@Entity
@Table(name="parent_category")
public class ParentCategory implements Serializable{
	@XStreamOmitField
	private String name;
	@XStreamOmitField
	private Integer id;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the string representation of object
	 */
	@Override
	public String toString() {
		 
		return  "[ id:"+id+" name: "+name+"]";
	}
	
}
