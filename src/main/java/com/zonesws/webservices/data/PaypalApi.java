package com.zonesws.webservices.data;

import java.io.Serializable;

/**
 * class to represent a country 
 * @author Ulaganathan
 *
 */

public class PaypalApi implements Serializable {
	private String url;
	private String clientId;
	private String secretKey;
	private String credentialType;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getCredentialType() {
		return credentialType;
	}
	public void setCredentialType(String credentialType) {
		this.credentialType = credentialType;
	}
	
	
	
	
}
