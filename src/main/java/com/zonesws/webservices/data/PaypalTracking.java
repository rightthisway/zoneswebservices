package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.OrderType;

@Entity
@Table(name = "paypal_tracking")
public class PaypalTracking implements Serializable{

	private Integer id;
	private Integer customerId;
	private Integer eventId;
	private Integer catTicketGroupId;
	private Integer qty;
	private String zone;
	private Double orderTotal;
	private OrderType orderType;
	private ApplicationPlatForm platform;
	private String sessionId;
	private String status;
	private String paypalTransactionId;
	private Integer transactionId;
	private Date createdDate;
	private Date lastUpdated;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name = "event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name = "category_ticket_group_id")
	public Integer getCatTicketGroupId() {
		return catTicketGroupId;
	}
	public void setCatTicketGroupId(Integer catTicketGroupId) {
		this.catTicketGroupId = catTicketGroupId;
	}
	
	@Column(name = "qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@Column(name = "zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	
	@Column(name = "order_total")
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "order_type")
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "platform")
	public ApplicationPlatForm getPlatform() {
		return platform;
	}
	public void setPlatform(ApplicationPlatForm platform) {
		this.platform = platform;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "paypal_transaction_id")
	public String getPaypalTransactionId() {
		return paypalTransactionId;
	}
	public void setPaypalTransactionId(String paypalTransactionId) {
		this.paypalTransactionId = paypalTransactionId;
	}
	
	@Column(name = "transaction_id")
	public Integer getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}
	
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Column(name = "session_id")
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
	
}
