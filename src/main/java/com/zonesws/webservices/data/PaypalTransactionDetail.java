package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "paypal_transaction_details")
public class PaypalTransactionDetail implements Serializable{
	
	private Integer id;
	private Integer orderId;
	private String paymentId;
	private String cartId;
	private String saleTransactionId;
	private String payerId;
	private String payerEmail;
	private String payerFirstName;
	private String payerLastName;
	private String payerPhone;
	private Double salesAmount;
	private Double transactionFees;
	private String transactionDate;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "order_id")
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	@Column(name = "payment_id")
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	@Column(name = "cart_id")
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	
	@Column(name = "sale_transaction_id")
	public String getSaleTransactionId() {
		return saleTransactionId;
	}
	public void setSaleTransactionId(String saleTransactionId) {
		this.saleTransactionId = saleTransactionId;
	}
	
	@Column(name = "payer_id")
	public String getPayerId() {
		return payerId;
	}
	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	
	@Column(name = "payer_email")
	public String getPayerEmail() {
		return payerEmail;
	}
	public void setPayerEmail(String payerEmail) {
		this.payerEmail = payerEmail;
	}
	
	@Column(name = "payer_f_name")
	public String getPayerFirstName() {
		return payerFirstName;
	}
	public void setPayerFirstName(String payerFirstName) {
		this.payerFirstName = payerFirstName;
	}
	
	
	@Column(name = "payer_l_name")
	public String getPayerLastName() {
		return payerLastName;
	}
	
	
	public void setPayerLastName(String payerLastName) {
		this.payerLastName = payerLastName;
	}
	
	@Column(name = "payer_phone")
	public String getPayerPhone() {
		return payerPhone;
	}
	
	public void setPayerPhone(String payerPhone) {
		this.payerPhone = payerPhone;
	}
	
	@Column(name = "sales_amount")
	public Double getSalesAmount() {
		return salesAmount;
	}
	
	
	public void setSalesAmount(Double salesAmount) {
		this.salesAmount = salesAmount;
	}
	
	@Column(name = "transaction_fees")
	public Double getTransactionFees() {
		return transactionFees;
	}
	public void setTransactionFees(Double transactionFees) {
		this.transactionFees = transactionFees;
	}
	
	@Column(name = "transaction_date")
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	
}
