package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.utils.RewardConversionUtil;
import com.zonesws.webservices.utils.TicketUtil;

/**
 * represents customer Polling Rewards
 * 
 *
 */
@XStreamAlias("PollingCustomerRewards")
@Entity
@Table(name="polling_customer_rewards")
public class PollingCustomerRewards  implements Serializable{
	private Integer custId;

	private Integer magicWand;
	
	@Id
	@Column(name="cust_id")
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	@Column(name="magicwand")
	public Integer getMagicWand() {
		return magicWand;
	}
	public void setMagicWand(Integer magicWand) {
		this.magicWand = magicWand;
	}
	
		
}
