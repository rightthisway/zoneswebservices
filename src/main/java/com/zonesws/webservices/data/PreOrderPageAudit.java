package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.enums.ApplicationPlatForm;

@Entity
@Table(name = "rtf_pre_order_page_audit")
public class PreOrderPageAudit implements Serializable{

	private static final long serialVersionUID = -7941769011539361245L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	@Column(name = "event_id")
	private Integer eventId;
	@Column(name = "category_ticket_group_id")
	private Integer categoryTicketGroupId;
	@Column(name = "quantity")
	private Integer qty;
	@Column(name = "section")
	private String section;
	@Column(name = "ticket_price")
	private Double ticketPrice;
	@Column(name = "sold_price")
	private Double ticketSoldPrice;
	@Column(name = "is_loyal_fan")
	private Boolean isLoyalFan;
	@Column(name = "ip_address")
	private String ipAddress; 
	@Enumerated(EnumType.STRING)
	@Column(name = "platForm")
	private ApplicationPlatForm platForm;
	@Column(name = "promo_code")
	private String promotionalCode; 
	@Column(name = "session")
	private String sessionId; 
	@Column(name = "status")
	private String status; 
	@Column(name = "is_email_sent")
	private Boolean isEmailSent; 
	@Column(name = "created_time")
	private Date createDate;
	@Column(name = "updated_time")
	private Date updatedDate;
	@Column(name = "reason")
	private String reason;
	@Transient
	private Integer customerId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public Double getTicketSoldPrice() {
		return ticketSoldPrice;
	}
	public void setTicketSoldPrice(Double ticketSoldPrice) {
		this.ticketSoldPrice = ticketSoldPrice;
	}
	public Boolean getIsLoyalFan() {
		return isLoyalFan;
	}
	public void setIsLoyalFan(Boolean isLoyalFan) {
		this.isLoyalFan = isLoyalFan;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}
	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}
	public String getPromotionalCode() {
		return promotionalCode;
	}
	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getReason() {
		if(reason.equals("Order Initiated")){
			reason = "Customer Came till checkout page and left without proceeding furthur.";
		}
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	} 
	
	
	
}