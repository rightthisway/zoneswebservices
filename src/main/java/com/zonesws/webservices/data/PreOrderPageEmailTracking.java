package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rtf_pre_order_visit_offer_tracking")
public class PreOrderPageEmailTracking implements Serializable{

	private static final long serialVersionUID = -7941769011539361245L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "customer_id")
	private Integer customerId;
	
	@Column(name = "event_ids")
	private String eventIds;
	
	@Column(name = "email_sent_date")
	private Date emailSentDate;
	
	@Column(name="offer_expiry_days")
	private Integer offerExpiryDays;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Date getEmailSentDate() {
		return emailSentDate;
	}
	public void setEmailSentDate(Date emailSentDate) {
		this.emailSentDate = emailSentDate;
	}
	public String getEventIds() {
		return eventIds;
	}
	public void setEventIds(String eventIds) {
		this.eventIds = eventIds;
	}
	public Integer getOfferExpiryDays() {
		return offerExpiryDays;
	}
	public void setOfferExpiryDays(Integer offerExpiryDays) {
		this.offerExpiryDays = offerExpiryDays;
	}
	
}