package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.utils.TicketUtil;

@Entity
@Table(name="presale_zone_ticket_group")
public class PresaleZoneTicketGroup implements Serializable{
	
		private Integer id;
		private Integer eventId;
		private String zone;
		private Integer quantity;
		private Integer zonePrice;
		private Integer priority;
		private String sectionRange;
		private String rowRange;
		private Status status;
		private String shippingMethod;
		private Date lastUpdatedDate;
		private Integer soldPrice;
		
		@JsonIgnore
		private String zoneDescription;
		@JsonIgnore
		private String ticketDeliveryInfo;
		@JsonIgnore
		private String loyaltyInfo;
		@JsonIgnore
		private String unitPriceStr;
		
		@Id
		@Column(name="id")
		@GeneratedValue(strategy=GenerationType.AUTO)
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
		@Column(name = "event_id")
		public Integer getEventId() {
			return eventId;
		}
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		
		@Column(name = "section")
		public String getZone() {
			return zone;
		}
		public void setZone(String zone) {
			this.zone = zone;
		}
		@Column(name = "quantity")
		public Integer getQuantity() {
			return quantity;
		}
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}
		
		@Column(name = "zone_price")
		public Integer getZonePrice() {
			return zonePrice;
		}
		public void setZonePrice(Integer zonePrice) {
			this.zonePrice = zonePrice;
		}
		
		@Column(name = "sold_price")
		public Integer getSoldPrice() {
			return soldPrice;
		}
		public void setSoldPrice(Integer soldPrice) {
			this.soldPrice = soldPrice;
		}
		@Column(name = "priority")
		public Integer getPriority() {
			return priority;
		}
		public void setPriority(Integer priority) {
			this.priority = priority;
		}
		
		@Column(name = "section_range")
		public String getSectionRange() {
			return sectionRange;
		}
		public void setSectionRange(String sectionRange) {
			this.sectionRange = sectionRange;
		}
		
		@Column(name = "row_range")
		public String getRowRange() {
			return rowRange;
		}
		public void setRowRange(String rowRange) {
			this.rowRange = rowRange;
		}
		
		@Enumerated(EnumType.ORDINAL)
		@Column(name="ticket_status")
		public Status getStatus() {
			return status;
		}
		public void setStatus(Status status) {
			this.status = status;
		}

		@Column(name="shipping_method")
		public String getShippingMethod() {
			return shippingMethod;
		}


		public void setShippingMethod(String shippingMethod) {
			this.shippingMethod = shippingMethod;
		}
		
		@Column(name="last_updated_date")
		public Date getLastUpdatedDate() {
			return lastUpdatedDate;
		}
		public void setLastUpdatedDate(Date lastUpdatedDate) {
			this.lastUpdatedDate = lastUpdatedDate;
		}
		
		@Transient
		public String getZoneDescription() {
			return zoneDescription;
		}
		public void setZoneDescription(String zoneDescription) {
			this.zoneDescription = zoneDescription;
		}
		
		@Transient
		public String getTicketDeliveryInfo() {
			return ticketDeliveryInfo;
		}
		public void setTicketDeliveryInfo(String ticketDeliveryInfo) {
			this.ticketDeliveryInfo = ticketDeliveryInfo;
		}
		
		@Transient
		public String getLoyaltyInfo() {
			return loyaltyInfo;
		}
		public void setLoyaltyInfo(String loyaltyInfo) {
			this.loyaltyInfo = loyaltyInfo;
		}
		
		@Transient
		public String getUnitPriceStr() {
			if(null != zonePrice){
				try{
					unitPriceStr =TicketUtil.getRoundedValueString(zonePrice.doubleValue());
				}catch(Exception e){
					unitPriceStr = "0.00";
				}
			}
			return unitPriceStr;
		}
		public void setUnitPriceStr(String unitPriceStr) {
			this.unitPriceStr = unitPriceStr;
		}
		
		
	}