package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * class to represent db property related to  smtp settings, ebay, mapPath, paypal  etc.    
 * @author hamin
 *
 */
@Entity
@Table(name ="zones_property")
public class Property implements Serializable{
	
	private String name;
	private String value;
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name= "name")
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @param name , name of property to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 
	 * @return value
	 */
	@Column(name="value")
	public String getValue() {
		return value;
	}
	/**
	 * 
	 * @param value , value of property to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	

}
