package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents QuizAffiliateRewardHistory entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("QuizAffiliateRewardHistory")
@Entity
@Table(name="contest_affiliate_reward_history")
public class QuizAffiliateRewardHistory  implements Serializable{
	
	private Integer id;
	private Integer affiliateCustomerId;
	private Integer tireoneCustomerId;
	private Integer contestId;
	private String status;	 
	private String rewardType;//CASH_REWARD, REWARD_DOLLAR
	private Integer livesToAffiliateCustomer;
	private Integer livesToTireOneCustomer;
	private Double cashReward=0.00;
	private Double rewardDollar=0.00;
	private Date createdDate;
	private Date updatedDate;
	 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	 
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="reward_type")
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	
	@Column(name="create_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="affiliate_customer_id")
	public Integer getAffiliateCustomerId() {
		return affiliateCustomerId;
	}
	public void setAffiliateCustomerId(Integer affiliateCustomerId) {
		this.affiliateCustomerId = affiliateCustomerId;
	}
	
	@Column(name="tireone_customer_id")
	public Integer getTireoneCustomerId() {
		return tireoneCustomerId;
	}
	public void setTireoneCustomerId(Integer tireoneCustomerId) {
		this.tireoneCustomerId = tireoneCustomerId;
	}
	
	@Column(name="lives_to_affiliate_customer")
	public Integer getLivesToAffiliateCustomer() {
		return livesToAffiliateCustomer;
	}
	public void setLivesToAffiliateCustomer(Integer livesToAffiliateCustomer) {
		this.livesToAffiliateCustomer = livesToAffiliateCustomer;
	}
	
	@Column(name="lives_to_tireone_customer")
	public Integer getLivesToTireOneCustomer() {
		return livesToTireOneCustomer;
	}
	public void setLivesToTireOneCustomer(Integer livesToTireOneCustomer) {
		this.livesToTireOneCustomer = livesToTireOneCustomer;
	}
	
	@Column(name="cash_rewards")
	public Double getCashReward() {
		return cashReward;
	}
	public void setCashReward(Double cashReward) {
		this.cashReward = cashReward;
	}
	
	@Column(name="reward_dollars")
	public Double getRewardDollar() {
		return rewardDollar;
	}
	public void setRewardDollar(Double rewardDollar) {
		this.rewardDollar = rewardDollar;
	}
	@Column(name="contest_id")
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	
}
