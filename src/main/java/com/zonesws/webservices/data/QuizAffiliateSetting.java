package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents QuizAffiliateSetting entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("QuizAffiliateSetting")
@Entity
@Table(name="contest_affiliate_settings")
public class QuizAffiliateSetting  implements Serializable{
	
	private Integer id;
	private Integer customerId;	
	private String referralCode;	
	private String status;	
	private Date effectiveFromDate;
	private Date effectiveToDate;
	private String rewardType;//CASH_REWARD, REWARD_DOLLAR
	private Integer noOfLivesToAffiliateCustomer;
	private Integer noOfLivesToTireOneCustomer;
	private Double rewardValue;
	private Date createdDate;
	private Date updatedDate;
	
	private String updatedBy;
	private String createdBy;
	private Double activeCashReward=0.00;
	private Double debitedCashReward=0.00;
	private Double totalCashReward=0.00;
	private Double voidedCashReward=0.00;
	private Double totalCreditedRewardDollar=0.00;
	
	private Double tireOneCustRewards=0.00;
	private Integer tireOneCustSuperFanStars=0;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="referral_code")
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="effective_from_date")
	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}
	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}
	
	@Column(name="effective_to_date")
	public Date getEffectiveToDate() {
		return effectiveToDate;
	}
	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}
	
	@Column(name="reward_type")
	public String getRewardType() {
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	
	@Column(name="no_of_lives_to_affiliate_customer")
	public Integer getNoOfLivesToAffiliateCustomer() {
		return noOfLivesToAffiliateCustomer;
	}
	public void setNoOfLivesToAffiliateCustomer(Integer noOfLivesToAffiliateCustomer) {
		this.noOfLivesToAffiliateCustomer = noOfLivesToAffiliateCustomer;
	}
	
	@Column(name="no_of_lives_to_tireone_customer")
	public Integer getNoOfLivesToTireOneCustomer() {
		return noOfLivesToTireOneCustomer;
	}
	public void setNoOfLivesToTireOneCustomer(Integer noOfLivesToTireOneCustomer) {
		this.noOfLivesToTireOneCustomer = noOfLivesToTireOneCustomer;
	}
	
	@Column(name="reward_value")
	public Double getRewardValue() {
		return rewardValue;
	}
	public void setRewardValue(Double rewardValue) {
		this.rewardValue = rewardValue;
	}
	
	@Column(name="create_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="active_cash_reward")
	public Double getActiveCashReward() {
		return activeCashReward;
	}
	public void setActiveCashReward(Double activeCashReward) {
		this.activeCashReward = activeCashReward;
	}
	
	@Column(name="debited_cash_reward")
	public Double getDebitedCashReward() {
		return debitedCashReward;
	}
	public void setDebitedCashReward(Double debitedCashReward) {
		this.debitedCashReward = debitedCashReward;
	}
	
	@Column(name="total_cash_reward")
	public Double getTotalCashReward() {
		if(null == totalCashReward) {
			totalCashReward =0.00;
		}
		return totalCashReward;
	}
	public void setTotalCashReward(Double totalCashReward) {
		this.totalCashReward = totalCashReward;
	}
	
	@Column(name="voided_cash_reward")
	public Double getVoidedCashReward() {
		return voidedCashReward;
	}
	public void setVoidedCashReward(Double voidedCashReward) {
		this.voidedCashReward = voidedCashReward;
	}
	
	@Column(name="total_credited_reward_dollars")
	public Double getTotalCreditedRewardDollar() {
		if(null == totalCreditedRewardDollar) {
			totalCreditedRewardDollar =0.00;
		}
		return totalCreditedRewardDollar;
	}
	public void setTotalCreditedRewardDollar(Double totalCreditedRewardDollar) {
		this.totalCreditedRewardDollar = totalCreditedRewardDollar;
	}
	
	@Column(name="tireone_cust_reward_dollars")
	public Double getTireOneCustRewards() {
		if(null == tireOneCustRewards) {
			tireOneCustRewards = 0.00;
		}
		return tireOneCustRewards;
	}
	public void setTireOneCustRewards(Double tireOneCustRewards) {
		this.tireOneCustRewards = tireOneCustRewards;
	}
	
	@Column(name="tireone_cust_super_fan_stars")
	public Integer getTireOneCustSuperFanStars() {
		if(null == tireOneCustSuperFanStars) {
			tireOneCustSuperFanStars = 0;
		}
		return tireOneCustSuperFanStars;
	}
	public void setTireOneCustSuperFanStars(Integer tireOneCustSuperFanStars) {
		this.tireOneCustSuperFanStars = tireOneCustSuperFanStars;
	}
	 
	
}
