package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.PromotionalType;

@Entity
@Table(name = "rtf_customer_promotional_offers")
public class RTFCustomerPromotionalOffer implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="promo_code")
	private String promoCode; 
	
	@Enumerated(EnumType.STRING)
	@Column(name="promo_type")
	private PromotionalType promoType;
	
	@Column(name="discount")
	private Double discount;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="max_orders")
	private Integer maxOrders;
	
	@Column(name="no_of_orders")
	private Integer noOfOrders;
	
	@Column(name="is_flat_discount")
	private Boolean isFlatDiscount;
	
	@Column(name="status")
	private String status;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="modified_time")
	private Date modifiedDate;
	
	@Column(name="flat_offer_order_threshold")
	private Double flatOfferOrderThreshold;
	
	@Column(name="is_email_sent")
	private Boolean isEmailSent;
	
	@Column(name="to_email")
	private String toEmail;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getMaxOrders() {
		return maxOrders;
	}
	public void setMaxOrders(Integer maxOrders) {
		this.maxOrders = maxOrders;
	}
	public Integer getNoOfOrders() {
		return noOfOrders;
	}
	public void setNoOfOrders(Integer noOfOrders) {
		this.noOfOrders = noOfOrders;
	}
	public Boolean getIsFlatDiscount() {
		return isFlatDiscount;
	}
	public void setIsFlatDiscount(Boolean isFlatDiscount) {
		this.isFlatDiscount = isFlatDiscount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Double getFlatOfferOrderThreshold() {
		return flatOfferOrderThreshold;
	}
	public void setFlatOfferOrderThreshold(Double flatOfferOrderThreshold) {
		this.flatOfferOrderThreshold = flatOfferOrderThreshold;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	public String getToEmail() {
		return toEmail;
	}
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	public PromotionalType getPromoType() {
		return promoType;
	}
	public void setPromoType(PromotionalType promoType) {
		this.promoType = promoType;
	}
	
	
}
