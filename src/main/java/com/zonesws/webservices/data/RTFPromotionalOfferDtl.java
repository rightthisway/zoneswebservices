package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.enums.PromotionalType;

@Entity
@Table(name = "rtf_promotional_offer_dtl")
public class RTFPromotionalOfferDtl implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="promo_type")
	private PromotionalType promoType;
	
	@Column(name="artist_id")
	private Integer artistId;
	
	@Column(name="venue_id")
	private Integer venueId;
	
	@Column(name="grand_child_id")
	private Integer grandChildId;
	
	@Column(name="child_id")
	private Integer childId;
	
	@Column(name="parent_id")
	private Integer parentId;
	
	@Column(name="event_id")
	private Integer eventId;
	
	@Column(name="promo_offer_id")
	private Integer promoOfferId;
	
	/*@ManyToOne
	private RTFPromotionalOfferHdr rtfPromotionalOfferHdr;*/
	
	@Transient
	private String artistName;
	@Transient
	private String venueName;
	@Transient
	private String parentCategory;
	@Transient
	private String childCategory;
	@Transient
	private String grandChildCategory;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public PromotionalType getPromoType() {
		return promoType;
	}
	public void setPromoType(PromotionalType promoType) {
		this.promoType = promoType;
	}
	
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	
	public String getChildCategory() {
		return childCategory;
	}
	public void setChildCategory(String childCategory) {
		this.childCategory = childCategory;
	}
	
	public String getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	/*public RTFPromotionalOfferHdr getRtfPromotionalOfferHdr() {
		return rtfPromotionalOfferHdr;
	}
	public void setRtfPromotionalOfferHdr(
			RTFPromotionalOfferHdr rtfPromotionalOfferHdr) {
		this.rtfPromotionalOfferHdr = rtfPromotionalOfferHdr;
	}*/
	public Integer getPromoOfferId() {
		return promoOfferId;
	}
	public void setPromoOfferId(Integer promoOfferId) {
		this.promoOfferId = promoOfferId;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	
}
