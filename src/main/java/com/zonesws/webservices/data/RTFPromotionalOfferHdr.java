package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rtf_promotional_offer_hdr")
public class RTFPromotionalOfferHdr implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="promo_code")
	private String promoCode; 
	
	@Column(name="discount")
	private Double discount;
	
	@Column(name="mobile_discount")
	private Double mobileDiscount;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="max_orders")
	private Integer maxOrders;
	
	@Column(name="no_of_orders")
	private Integer noOfOrders;
	
	@Column(name="is_flat_discount")
	private Boolean isFlatDiscount;
	
	@Column(name="status")
	private String status;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="modified_by")
	private String modifiedBy;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="modified_time")
	private Date modifiedDate;
	
	@Column(name="flat_offer_order_threshold")
	private Double flatOfferOrderThreshold;
	
	/*@OneToMany(cascade=javax.persistence.CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name="promo_offer_id")
	private Set<RTFPromotionalOfferDtl> rtfPromotionalOfferDtls;*/
	
	@Transient
	private String promoType;	
	@Transient
	private Integer artistId;	
	@Transient
	private Integer venueId;	
	@Transient
	private Integer grandChildId;	
	@Transient
	private Integer childId;	
	@Transient
	private Integer parentId;
	@Transient
	private String artistName;
	@Transient
	private String venueName;
	@Transient
	private String parentCategory;
	@Transient
	private String childCategory;
	@Transient
	private String grandChildCategory;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getMaxOrders() {
		return maxOrders;
	}
	public void setMaxOrders(Integer maxOrders) {
		this.maxOrders = maxOrders;
	}
	public Integer getNoOfOrders() {
		return noOfOrders;
	}
	public void setNoOfOrders(Integer noOfOrders) {
		this.noOfOrders = noOfOrders;
	}
	public Boolean getIsFlatDiscount() {
		return isFlatDiscount;
	}
	public void setIsFlatDiscount(Boolean isFlatDiscount) {
		this.isFlatDiscount = isFlatDiscount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Double getFlatOfferOrderThreshold() {
		return flatOfferOrderThreshold;
	}
	public void setFlatOfferOrderThreshold(Double flatOfferOrderThreshold) {
		this.flatOfferOrderThreshold = flatOfferOrderThreshold;
	}
	public String getPromoType() {
		return promoType;
	}
	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	public String getChildCategory() {
		return childCategory;
	}
	public void setChildCategory(String childCategory) {
		this.childCategory = childCategory;
	}
	public String getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	/*public Set<RTFPromotionalOfferDtl> getRtfPromotionalOfferDtls() {
		return rtfPromotionalOfferDtls;
	}
	public void setRtfPromotionalOfferDtls(
			Set<RTFPromotionalOfferDtl> rtfPromotionalOfferDtls) {
		this.rtfPromotionalOfferDtls = rtfPromotionalOfferDtls;
	}
	*/
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getMobileDiscount() {
		return mobileDiscount;
	}
	public void setMobileDiscount(Double mobileDiscount) {
		this.mobileDiscount = mobileDiscount;
	}
	
}
