package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.PromotionalType;

@Entity
@Table(name = "rtf_promotional_order_tracking_new")
public class RTFPromotionalOfferTracking implements Serializable{	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="promotional_offer_id")
	private Integer promotionalOfferId;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="event_id")
	private Integer eventId;
	
	@Column(name="ticket_group_id")
	private Integer ticketGroupId;
	
	@Column(name="session_id")
	private String sessionId; 
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	private ApplicationPlatForm platForm;
	
	@Enumerated(EnumType.STRING)
	@Column(name="offer_type")
	private PromotionalType offerType;
	
	@Column(name="order_id")
	private Integer orderId;
	
	@Column(name="additional_discount_conv")
	private Double additionalDiscountConv;
	
	@Column(name="mobile_discount_conv")
	private Double mobileAppDiscConv;
	
	@Column(name="additional_discount_amount") 
	private Double additionalDiscountAmt;
	
	@Column(name="total_price")
	private Double totalPrice;
	
	@Column(name="service_fees")
	private Double serviceFees;
	
	@Column(name="promo_code")
	private String promoCode;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="status")
	private String status;
	
	@Column(name="updated_date")
	private Date updatedDate;
	
	@Column(name="is_long_ticket")
	private Boolean isLongTicket;

	@Column(name="is_flat_discount")
	private Boolean isFlatDiscount;
	
	@Column(name="referrer_cust_id")
	private Integer referredBy;
	
	@Column(name="cust_earn_reward_conv")
	private Double customerEarnRewardConv;
	
	@Column(name="referrer_earn_reward_conv")
	private Double referrarEarnRewardConv;
	
	@Column(name="ticket_price")
	private Double ticketPrice;
	
	@Column(name="discounted_price")
	private Double discountedPrice;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPromotionalOfferId() {
		return promotionalOfferId;
	}

	public void setPromotionalOfferId(Integer promotionalOfferId) {
		this.promotionalOfferId = promotionalOfferId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}

	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Double getAdditionalDiscountConv() {
		return additionalDiscountConv;
	}

	public void setAdditionalDiscountConv(Double additionalDiscountConv) {
		this.additionalDiscountConv = additionalDiscountConv;
	}

	public Double getAdditionalDiscountAmt() {
		return additionalDiscountAmt;
	}

	public void setAdditionalDiscountAmt(Double additionalDiscountAmt) {
		this.additionalDiscountAmt = additionalDiscountAmt;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getServiceFees() {
		return serviceFees;
	}

	public void setServiceFees(Double serviceFees) {
		this.serviceFees = serviceFees;
	}

	public Boolean getIsLongTicket() {
		return isLongTicket;
	}

	public void setIsLongTicket(Boolean isLongTicket) {
		this.isLongTicket = isLongTicket;
	}

	public Boolean getIsFlatDiscount() {
		return isFlatDiscount;
	}

	public void setIsFlatDiscount(Boolean isFlatDiscount) {
		this.isFlatDiscount = isFlatDiscount;
	}

	public PromotionalType getOfferType() {
		return offerType;
	}

	public void setOfferType(PromotionalType offerType) {
		this.offerType = offerType;
	}

	public Integer getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(Integer referredBy) {
		this.referredBy = referredBy;
	}

	public Double getCustomerEarnRewardConv() {
		return customerEarnRewardConv;
	}

	public void setCustomerEarnRewardConv(Double customerEarnRewardConv) {
		this.customerEarnRewardConv = customerEarnRewardConv;
	}

	public Double getReferrarEarnRewardConv() {
		return referrarEarnRewardConv;
	}

	public void setReferrarEarnRewardConv(Double referrarEarnRewardConv) {
		this.referrarEarnRewardConv = referrarEarnRewardConv;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public Double getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(Double discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	public Double getMobileAppDiscConv() {
		return mobileAppDiscConv;
	}

	public void setMobileAppDiscConv(Double mobileAppDiscConv) {
		this.mobileAppDiscConv = mobileAppDiscConv;
	}
	
}
