package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="referral_contest_email_versions")
public class ReferralContestEmailVersion implements Serializable  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	@Column(name="is_participated")
	private Boolean isParticipated;
	
	@Column(name="contest_id")
	private Integer contestId;
	
	@Column(name="order_id")
	private Integer purchaserOrderId;
	
	@Column(name="referrer_cust_id")
	private Integer participantCustId;
	
	@Column(name="purchaser_cust_id")
	private Integer purchaserCustId;
	
	@Column(name="contest_identity_key")
	private String contestIdentityKey;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="updated_date")
	private Date updatedDate;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsParticipated() {
		return isParticipated;
	}

	public void setIsParticipated(Boolean isParticipated) {
		this.isParticipated = isParticipated;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public Integer getPurchaserOrderId() {
		return purchaserOrderId;
	}

	public void setPurchaserOrderId(Integer purchaserOrderId) {
		this.purchaserOrderId = purchaserOrderId;
	}

	public Integer getParticipantCustId() {
		return participantCustId;
	}

	public void setParticipantCustId(Integer participantCustId) {
		this.participantCustId = participantCustId;
	}

	public Integer getPurchaserCustId() {
		return purchaserCustId;
	}

	public void setPurchaserCustId(Integer purchaserCustId) {
		this.purchaserCustId = purchaserCustId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getContestIdentityKey() {
		return contestIdentityKey;
	}

	public void setContestIdentityKey(String contestIdentityKey) {
		this.contestIdentityKey = contestIdentityKey;
	}
	
}

