package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="referral_contest_participants")
public class ReferralContestParticipants implements Serializable  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	@Column(name="contest_id")
	private Integer contestId;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="purchase_customer_id")
	private Integer purchaseCustomerId;
	
	@Column(name="referral_code")
	private String discountCode;
	
	@Column(name="created_date")
	private Date createdDate;
	
	
	@Column(name="is_emailed")
	private Boolean isEmailed;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getContestId() {
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getPurchaseCustomerId() {
		return purchaseCustomerId;
	}

	public void setPurchaseCustomerId(Integer purchaseCustomerId) {
		this.purchaseCustomerId = purchaseCustomerId;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsEmailed() {
		return isEmailed;
	}

	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}

	
	
	
	
}

