package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * class to represent a ReferralDiscountSettings 
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="customer_referral_discount_setting")
public class ReferralDiscountSettings implements Serializable {
	
	private Integer id;
	private ApplicationPlatForm platForm;
	private Double orderDiscountPerc;
	private Double orderDiscountConv;
	private Double custEarnRewardPerc;
	private Double custEarnRewardConv;
	private Double referrerEarnRewardPerc;
	private Double referrerEarnRewardConv;
	private Date fromDate;
	private Date toDate;
	private Date updatedTime;
	private String status;
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="from_date")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
	@Column(name="to_date")
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="updated_time")
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}
	
	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}
	
	@Column(name="order_discount_perc")
	public Double getOrderDiscountPerc() {
		return orderDiscountPerc;
	}
	public void setOrderDiscountPerc(Double orderDiscountPerc) {
		this.orderDiscountPerc = orderDiscountPerc;
	}
	
	@Column(name="order_discount_conv")
	public Double getOrderDiscountConv() {
		return orderDiscountConv;
	}
	public void setOrderDiscountConv(Double orderDiscountConv) {
		this.orderDiscountConv = orderDiscountConv;
	}
	
	@Column(name="cust_earn_reward_perc")
	public Double getCustEarnRewardPerc() {
		return custEarnRewardPerc;
	}
	public void setCustEarnRewardPerc(Double custEarnRewardPerc) {
		this.custEarnRewardPerc = custEarnRewardPerc;
	}
	
	@Column(name="cust_earn_reward_conv")
	public Double getCustEarnRewardConv() {
		return custEarnRewardConv;
	}
	public void setCustEarnRewardConv(Double custEarnRewardConv) {
		this.custEarnRewardConv = custEarnRewardConv;
	}
	
	@Column(name="referrer_earn_reward_perc")
	public Double getReferrerEarnRewardPerc() {
		return referrerEarnRewardPerc;
	}
	public void setReferrerEarnRewardPerc(Double referrerEarnRewardPerc) {
		this.referrerEarnRewardPerc = referrerEarnRewardPerc;
	}
	
	@Column(name="referrer_earn_reward_conv")
	public Double getReferrerEarnRewardConv() {
		return referrerEarnRewardConv;
	}
	public void setReferrerEarnRewardConv(Double referrerEarnRewardConv) {
		this.referrerEarnRewardConv = referrerEarnRewardConv;
	}
	
	
	
	
}
