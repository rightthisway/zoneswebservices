package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "referrer_code_tracking")
public class ReferredCodeTracking implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="referred_by")
	private Integer referredBy;
	
	@Column(name="referred_to")
	private Integer referredTo;
	
	@Column(name="referred_code")
	private String referredCode;
	
	@Column(name="referred_date")
	private Date referredDate;
	
	@Column(name="referrence_type")
	private String referrenceType;

	//Getters and setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(Integer referredBy) {
		this.referredBy = referredBy;
	}

	public Integer getReferredTo() {
		return referredTo;
	}

	public void setReferredTo(Integer referredTo) {
		this.referredTo = referredTo;
	}

	public String getReferredCode() {
		return referredCode;
	}

	public void setReferredCode(String referredCode) {
		this.referredCode = referredCode;
	}

	public Date getReferredDate() {
		return referredDate;
	}

	public void setReferredDate(Date referredDate) {
		this.referredDate = referredDate;
	}

	public String getReferrenceType() {
		return referrenceType;
	}

	public void setReferrenceType(String referrenceType) {
		this.referrenceType = referrenceType;
	}
	
	
}
