package com.zonesws.webservices.data;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

/**
 * represents RemoveZoneEvent entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("RemoveZoneEvent")
public class RemoveZoneEvent  implements Serializable{
	private Integer status;
	private Error error; 
	private  String message;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}

	
	
	
	
}
