package com.zonesws.webservices.data;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * represents customer entity
 * @author Ulaganathan
 *
 */
@XStreamAlias("ResetPassword")
public class ResetPassword  implements Serializable{
	
	private  String message;
	private  String email;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	
	
	
}
