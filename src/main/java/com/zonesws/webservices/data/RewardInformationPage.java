package com.zonesws.webservices.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

/**
 * represents Reward Points info like about us, contact us, faq, terms of use
 * @author Ulaganathan
 *
 */
@XStreamAlias("RewardInformationPage")
public class RewardInformationPage {
	
	private Integer status;
	private Error error;
	private String firstHeader;
	private String firstAnswer;
	private String secondHeader;
	private String firstImageTxt;
	private String firstImageUrl;
	private String secondImageTxt;
	private String secondImageUrl;
	private String bottomText;
	
	private String text1;
	private String text2;
	private String text3;
	private String text4;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getFirstHeader() {
		return firstHeader;
	}
	public void setFirstHeader(String firstHeader) {
		this.firstHeader = firstHeader;
	}
	public String getFirstAnswer() {
		return firstAnswer;
	}
	public void setFirstAnswer(String firstAnswer) {
		this.firstAnswer = firstAnswer;
	}
	public String getSecondHeader() {
		return secondHeader;
	}
	public void setSecondHeader(String secondHeader) {
		this.secondHeader = secondHeader;
	}
	public String getFirstImageTxt() {
		return firstImageTxt;
	}
	public void setFirstImageTxt(String firstImageTxt) {
		this.firstImageTxt = firstImageTxt;
	}
	public String getFirstImageUrl() {
		return firstImageUrl;
	}
	public void setFirstImageUrl(String firstImageUrl) {
		this.firstImageUrl = firstImageUrl;
	}
	public String getSecondImageTxt() {
		return secondImageTxt;
	}
	public void setSecondImageTxt(String secondImageTxt) {
		this.secondImageTxt = secondImageTxt;
	}
	public String getSecondImageUrl() {
		return secondImageUrl;
	}
	public void setSecondImageUrl(String secondImageUrl) {
		this.secondImageUrl = secondImageUrl;
	}
	public String getBottomText() {
		return bottomText;
	}
	public void setBottomText(String bottomText) {
		this.bottomText = bottomText;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getText2() {
		return text2;
	}
	public void setText2(String text2) {
		this.text2 = text2;
	}
	public String getText3() {
		return text3;
	}
	public void setText3(String text3) {
		this.text3 = text3;
	}
	public String getText4() {
		return text4;
	}
	public void setText4(String text4) {
		this.text4 = text4;
	}
	
	
	
}
