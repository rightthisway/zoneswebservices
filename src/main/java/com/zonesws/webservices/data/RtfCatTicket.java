package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.enums.TicketStatus;
import com.zonesws.webservices.utils.TicketUtil;

@Entity
@Table(name="rtf_cat_tickets")
public class RtfCatTicket implements Serializable{
	
	private Integer id;
	private Integer eId;
	private Integer qty;
	private Double price;
	private String disPrice;
	@JsonIgnore
	private TicketStatus status;
	@JsonIgnore
	private Date createdDate;
	@JsonIgnore
	private Date lastUpdatedDate;
	@JsonIgnore
	private Integer invoiceId;
	@JsonIgnore
	private Boolean broadcast;
	
	private Double singleTixPrice;
	
	private String sTixPrice;
	
	/*@JsonIgnore
	private Integer brokerId;*/
		
	public RtfCatTicket() {
		super();
	}
	 
	public RtfCatTicket(Integer id, Integer eId, Integer qty, Double price, Double sTixPrice) {
		super();
		this.id = id;
		this.eId = eId;
		this.qty = qty;
		this.price = price;
		this.singleTixPrice = sTixPrice;
	}
	
		
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer geteId() {
		return eId;
	}
	public void seteId(Integer eId) {
		this.eId = eId;
	}
	
	@Column(name="qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Transient
	public String getDisPrice() {
		if(price != null && price>0){
			try {
				disPrice = TicketUtil.getRoundedValueString(price);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return disPrice;
	}
	public void setDisPrice(String disPrice) {
		this.disPrice = disPrice;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status") 
	public TicketStatus getStatus() {
		return status;
	}
	public void setStatus(TicketStatus status) {
		this.status = status;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="invoice_id")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	@Column(name="broadcast")
	public Boolean getBroadcast() {
		return broadcast;
	}
	public void setBroadcast(Boolean broadcast) {
		this.broadcast = broadcast;
	}

	@Transient
	public Double getSingleTixPrice() {
		if(null == singleTixPrice) {
			singleTixPrice =0.00;
		}
		return singleTixPrice;
	}

	public void setSingleTixPrice(Double singleTixPrice) {
		this.singleTixPrice = singleTixPrice;
	}

	@Transient
	public String getsTixPrice() {
		sTixPrice = "IN RTF DOLLARS";
		/*if(singleTixPrice != null && singleTixPrice>0){
			try {
				sTixPrice = TicketUtil.getRoundedValueString(singleTixPrice);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
		return sTixPrice;
	}

	public void setsTixPrice(String sTixPrice) {
		this.sTixPrice = sTixPrice;
	}
	 
	
}