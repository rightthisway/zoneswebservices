package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;


@Entity
@Table(name="gift_cards")
public class RtfGiftCard implements Serializable{

	private Integer id;
	private Integer gfcBrandId;
	private String title;
	private String description;
	@JsonIgnore
	private String imagePath;
	private String imageUrl;
	@JsonIgnore
	private String conversionType;
	@JsonIgnore
	private Double rewardDollarConvRate;
	
	@JsonIgnore
	private Integer loyaltyPointsConvRate;
	
	@JsonIgnore
	private Double rewardPointConvRate;
	
	private String convRateString;
	
	@JsonIgnore
	private Integer availableQty;
	@JsonIgnore
	private Boolean status;
	@JsonIgnore
	private Date updateDate;
	@JsonIgnore
	private String updatedBy;
	
	private List<RtfGiftCardQuantity> quantity;
	private Double amount;
	
	private String amountDisp;
	
	@JsonIgnore
	private String cardStatus;
	
	@JsonIgnore
	private Integer maxThresholdQty;
	
	private String popupMsgRDollars ;
	private String popupMsgRPoints;
	private String popupMsgLoyaltyPoints;
	
	private Integer reqLPnts;
	private Double pricewLPnts;
	private String popupMsgActPrice;
	private String ActPriceStr;
	private String lpDiscMsg;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Column(name="status")
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
	@Column(name="updated_date")
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
	@Column(name="updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	@Column(name="conversion_type")
	public String getConversionType() {
		return conversionType;
	}
	public void setConversionType(String conversionType) {
		this.conversionType = conversionType;
	}
	
	
	@Transient
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@Transient
	public List<RtfGiftCardQuantity> getQuantity() {
		return quantity;
	}
	public void setQuantity(List<RtfGiftCardQuantity> quantity) {
		this.quantity = quantity;
	}
	
	@Transient
	public Integer getAvailableQty() {
		return availableQty;
	}
	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}
	
	@Transient
	public String getConvRateString() {
		try {
			if(conversionType.equalsIgnoreCase("PERCENTAGE")){
				convRateString = "Conversion Rate : "+(TicketUtil.getRoundedValue(rewardDollarConvRate)*100)+"%";
			}else{
				convRateString = "Conversion Rate : "+TicketUtil.getRoundedValueString(rewardDollarConvRate);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convRateString;
	}
	public void setConvRateString(String convRateString) {
		this.convRateString = convRateString;
	}
	
	@Column(name="image_path")
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	@Transient
	public String getImageUrl() {
		if((null == imageUrl || imageUrl.isEmpty()) && (null != imagePath && !imagePath.isEmpty())) {
			imageUrl = URLUtil.getGiftCardImageUrl(imagePath);
		}
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	

	@Column(name="card_status")
	public String getCardStatus() {
		return cardStatus;
	}
	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}
	
	@Transient
	public Integer getMaxThresholdQty() {
		return maxThresholdQty;
	}
	public void setMaxThresholdQty(Integer maxThresholdQty) {
		this.maxThresholdQty = maxThresholdQty;
	}
	
	@Column(name="gift_card_brand_id")
	public Integer getGfcBrandId() {
		return gfcBrandId;
	}
	public void setGfcBrandId(Integer gfcBrandId) {
		this.gfcBrandId = gfcBrandId;
	}
	
	@Column(name="conversion_rate")
	public Double getRewardDollarConvRate() {
		return rewardDollarConvRate;
	}
	public void setRewardDollarConvRate(Double rewardDollarConvRate) {
		this.rewardDollarConvRate = rewardDollarConvRate;
	}
	
	@Column(name="reward_points_conv_rate")
	public Double getRewardPointConvRate() {
		return rewardPointConvRate;
	}
	public void setRewardPointConvRate(Double rewardPointConvRate) {
		this.rewardPointConvRate = rewardPointConvRate;
	}
	@Transient
	public String getAmountDisp() {
		if(amount != null) {
			try {
				amountDisp = "$"+TicketUtil.getRoundedValueString(amount);
			}catch(Exception e) {
				amountDisp = "$"+amount;
			}
		}
		return amountDisp;
	}
	public void setAmountDisp(String amountDisp) {
		this.amountDisp = amountDisp;
	}

	@Transient
	public String getPopupMsgRDollars() {
		return popupMsgRDollars;
	}
	public void setPopupMsgRDollars(String popupMsgRDollars) {
		this.popupMsgRDollars = popupMsgRDollars;
	}
	
	@Transient
	public String getPopupMsgRPoints() {
		return popupMsgRPoints;
	}
	public void setPopupMsgRPoints(String popupMsgRPoints) {
		this.popupMsgRPoints = popupMsgRPoints;
	}
	
	@Transient
	public Integer getLoyaltyPointsConvRate() {
		if(loyaltyPointsConvRate == null){
			loyaltyPointsConvRate = 0;
		}
		return loyaltyPointsConvRate;
	}
	public void setLoyaltyPointsConvRate(Integer loyaltyPointsConvRate) {
		this.loyaltyPointsConvRate = loyaltyPointsConvRate;
	}
	
	@Transient
	public String getPopupMsgLoyaltyPoints() {
		return popupMsgLoyaltyPoints;
	}
	public void setPopupMsgLoyaltyPoints(String popupMsgLoyaltyPoints) {
		this.popupMsgLoyaltyPoints = popupMsgLoyaltyPoints;
	}
	@Transient
	public Integer getReqLPnts() {
		return reqLPnts;
	}
	public void setReqLPnts(Integer reqLPnts) {
		this.reqLPnts = reqLPnts;
	}
	@Transient
	public Double getPricewLPnts() {
		return pricewLPnts;
	}
	public void setPricewLPnts(Double pricewLPnts) {
		this.pricewLPnts = pricewLPnts;
	}
	@Transient
	public String getPopupMsgActPrice() {
		return popupMsgActPrice;
	}
	public void setPopupMsgActPrice(String popupMsgActPrice) {
		this.popupMsgActPrice = popupMsgActPrice;
	}
	@Transient
	public String getActPriceStr() {
		return ActPriceStr;
	}
	public void setActPriceStr(String actPriceStr) {
		ActPriceStr = actPriceStr;
	}
	@Transient
	public String getLpDiscMsg() {
		return lpDiscMsg;
	}
	public void setLpDiscMsg(String lpDiscMsg) {
		this.lpDiscMsg = lpDiscMsg;
	}
	
	
}
