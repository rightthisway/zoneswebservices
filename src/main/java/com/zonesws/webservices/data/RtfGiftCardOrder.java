package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.rtfquiz.webservices.enums.RedeemType;
import com.zonesws.webservices.enums.OrderType;

@Entity
@Table(name="giftcard_order")
public class RtfGiftCardOrder implements Serializable{

	private Integer id;
	private Integer cardId;
	private String cardTitle;
	private String cardDescription;
	private Integer customerId;
	private Integer quantity;
	private Double redeemedRewards;
	private Double OriginalCost;
	private OrderType orderType;
	private Integer shippingAddressId;
	private Boolean isEmailSent;
	private Boolean isOrderFulfilled;
	private Boolean isOrderFilledMailSent;
	private String cardFileUrl;
	private Date createdDate;
	private String createdBy;
	private String status;
	private RedeemType redeemType;
	private String pTrnxId;
	private String sTrnxId;
	private String pPayType;//Primary Payment Type	
	private String sPayType;//Secondary Payment Type
	 
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="card_id")
	public Integer getCardId() {
		return cardId;
	}
	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}
	
	@Column(name="card_title")
	public String getCardTitle() {
		return cardTitle;
	}
	public void setCardTitle(String cardTitle) {
		this.cardTitle = cardTitle;
	}
	
	@Column(name="card_description")
	public String getCardDescription() {
		return cardDescription;
	}
	public void setCardDescription(String cardDescription) {
		this.cardDescription = cardDescription;
	}
	
	@Column(name="customer_id")
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="redeemed_rewards")
	public Double getRedeemedRewards() {
		return redeemedRewards;
	}
	public void setRedeemedRewards(Double redeemedRewards) {
		this.redeemedRewards = redeemedRewards;
	}
	
	@Column(name="original_cost")
	public Double getOriginalCost() {
		return OriginalCost;
	}
	public void setOriginalCost(Double originalCost) {
		OriginalCost = originalCost;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="order_type")
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
	@Column(name="shipping_address_id")
	public Integer getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(Integer shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	
	@Column(name="is_email_sent")
	public Boolean getIsEmailSent() {
		return isEmailSent;
	}
	public void setIsEmailSent(Boolean isEmailSent) {
		this.isEmailSent = isEmailSent;
	}
	
	@Column(name="is_order_fulfilled")
	public Boolean getIsOrderFulfilled() {
		return isOrderFulfilled;
	}
	public void setIsOrderFulfilled(Boolean isOrderFulfilled) {
		this.isOrderFulfilled = isOrderFulfilled;
	}
	
	@Column(name="card_file_url")
	public String getCardFileUrl() {
		return cardFileUrl;
	}
	public void setCardFileUrl(String cardFileUrl) {
		this.cardFileUrl = cardFileUrl;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="is_order_filled_mail_sent")
	public Boolean getIsOrderFilledMailSent() {
		if(null == isOrderFilledMailSent) {
			isOrderFilledMailSent = false;
		}
		return isOrderFilledMailSent;
	}
	public void setIsOrderFilledMailSent(Boolean isOrderFilledMailSent) {
		this.isOrderFilledMailSent = isOrderFilledMailSent;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="redemption_type")
	public RedeemType getRedeemType() {
		return redeemType;
	}
	public void setRedeemType(RedeemType redeemType) {
		this.redeemType = redeemType;
	}
	
	@Column(name = "prm_tranx_id")
	public String getpTrnxId() {
		return pTrnxId;
	}
	public void setpTrnxId(String pTrnxId) {
		this.pTrnxId = pTrnxId;
	}
	@Column(name = "sec_tranx_id")
	public String getsTrnxId() {
		return sTrnxId;
	}
	public void setsTrnxId(String sTrnxId) {
		this.sTrnxId = sTrnxId;
	}
	
	@Column(name = "prm_pay_method")
	public String getpPayType() {
		return pPayType;
	}
	public void setpPayType(String pPayType) {
		this.pPayType = pPayType;
	}
	@Column(name = "sec_pay_method")
	public String getsPayType() {
		return sPayType;
	}
	public void setsPayType(String sPayType) {
		this.sPayType = sPayType;
	}
	
}
