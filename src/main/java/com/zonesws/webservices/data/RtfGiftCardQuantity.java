package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.utils.TicketUtil;


@Entity
@Table(name="gift_card_value")
public class RtfGiftCardQuantity implements Serializable{
	
	@JsonIgnore
	private Integer id;
	@JsonIgnore
	private Integer cardId;
	@JsonIgnore
	private Double amount;
	@JsonIgnore
	private Integer maxQty;
	@JsonIgnore
	private Integer usedQty;
	@JsonIgnore
	private Integer freeQty;
	@JsonIgnore
	private Boolean status;
	@JsonIgnore
	private Double requireRewards;
	
	@JsonIgnore
	private Double reqRewardDollars;
	
	@JsonIgnore
	private Double reqRewardPoints;
	
	private Integer reqLoyaltyPoints;
	
	@JsonIgnore
	private Double totalAmount;
	
	@JsonIgnore
	private Integer maxThresholdQty;
	
	private Integer qty;
	private String requiredRewardString;
	private String totalAmountString;
	private String RedeemPopupMsg;
	
	private String reqRewardDollarString;
	private String reqRewardPointString;
	
	private String popupMsgRDollars;
	private String popupMsgRPoints;
	private String popupMsgLoyaltyPoints;
	
	private Double pricewLPnts;
	private Double actPrice;
	private String actPriceStr;
	private String popupMsgActPrice;
	
	private String lpDiscMsg;
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="card_id")
	public Integer getCardId() {
		return cardId;
	}
	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}
	
	@Column(name="card_amount")
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@Column(name="max_quantity")
	public Integer getMaxQty() {
		return maxQty;
	}
	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}
	
	@Column(name="used_quantity")
	public Integer getUsedQty() {
		return usedQty;
	}
	public void setUsedQty(Integer usedQty) {
		this.usedQty = usedQty;
	}
	
	@Column(name="remaining_quantity")
	public Integer getFreeQty() {
		return freeQty;
	}
	public void setFreeQty(Integer freeQty) {
		this.freeQty = freeQty;
	}
	
	@Column(name="reqd_loyalpnts")
	public Integer getReqLoyaltyPoints() {
		return reqLoyaltyPoints;
	}
	public void setReqLoyaltyPoints(Integer reqLoyaltyPoints) {
		this.reqLoyaltyPoints = reqLoyaltyPoints;
	}
	
	@Column(name="status")
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Column(name="price_with_loyalty_points")
	public Double getPricewLPnts() {
		return pricewLPnts;
	}
	public void setPricewLPnts(Double pricewLPnts) {
		this.pricewLPnts = pricewLPnts;
	}
	@Transient
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@Transient
	public Double getRequireRewards() {
		return requireRewards;
	}
	public void setRequireRewards(Double requireRewards) {
		this.requireRewards = requireRewards;
	}
	
	@Transient
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	@Transient
	public String getRequiredRewardString() {
		if(requireRewards != null ){
			try {
				requiredRewardString =  TicketUtil.getRoundedValueString(requireRewards);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return requiredRewardString;
	}
	public void setRequiredRewardString(String requiredRewardString) {
		this.requiredRewardString = requiredRewardString;
	}
	
	@Transient
	public String getTotalAmountString() {
		if(totalAmount != null ){
			try {
				totalAmountString =  TicketUtil.getRoundedValueString(totalAmount);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return totalAmountString;
	}
	public void setTotalAmountString(String totalAmountString) {
		this.totalAmountString = totalAmountString;
	}
	
	@Transient
	public String getRedeemPopupMsg() {
		return RedeemPopupMsg;
	}
	public void setRedeemPopupMsg(String redeemPopupMsg) {
		RedeemPopupMsg = redeemPopupMsg;
	}
	
	@Column(name="max_qty_threshold")
	public Integer getMaxThresholdQty() {
		return maxThresholdQty;
	}
	public void setMaxThresholdQty(Integer maxThresholdQty) {
		this.maxThresholdQty = maxThresholdQty;
	}
	
	@Transient
	public Double getReqRewardDollars() {
		return reqRewardDollars;
	}
	public void setReqRewardDollars(Double reqRewardDollars) {
		this.reqRewardDollars = reqRewardDollars;
	}
	
	@Transient
	public Double getReqRewardPoints() {
		return reqRewardPoints;
	}
	public void setReqRewardPoints(Double reqRewardPoints) {
		this.reqRewardPoints = reqRewardPoints;
	}
	
	@Transient
	public String getReqRewardDollarString() {
		if(reqRewardDollars != null ){
			try {
				reqRewardDollarString =  TicketUtil.getRoundedValueString(reqRewardDollars);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return reqRewardDollarString;
	}
	public void setReqRewardDollarString(String reqRewardDollarString) {
		this.reqRewardDollarString = reqRewardDollarString;
	}
	
	@Transient
	public String getReqRewardPointString() {
		if(reqRewardPoints != null ){
			try {
				reqRewardPointString =  TicketUtil.getRoundedValueString(reqRewardPoints);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return reqRewardPointString;
	}
	public void setReqRewardPointString(String reqRewardPointString) {
		this.reqRewardPointString = reqRewardPointString;
	}
	
	@Transient
	public String getPopupMsgRDollars() {
		return popupMsgRDollars;
	}
	public void setPopupMsgRDollars(String popupMsgRDollars) {
		this.popupMsgRDollars = popupMsgRDollars;
	}
	
	@Transient
	public String getPopupMsgRPoints() {
		return popupMsgRPoints;
	}
	public void setPopupMsgRPoints(String popupMsgRPoints) {
		this.popupMsgRPoints = popupMsgRPoints;
	}
	
	@Transient
	public String getPopupMsgLoyaltyPoints() {
		return popupMsgLoyaltyPoints;
	}
	public void setPopupMsgLoyaltyPoints(String popupMsgLoyaltyPoints) {
		this.popupMsgLoyaltyPoints = popupMsgLoyaltyPoints;
	}
	
	@Transient
	public Double getActPrice() {
		return actPrice;
	}
	public void setActPrice(Double actPrice) {
		this.actPrice = actPrice;
	}
	@Transient
	public String getActPriceStr() {
		return actPriceStr;
	}
	public void setActPriceStr(String actPriceStr) {
		this.actPriceStr = actPriceStr;
	}
	@Transient
	public String getPopupMsgActPrice() {
		return popupMsgActPrice;
	}
	public void setPopupMsgActPrice(String popupMsgActPrice) {
		this.popupMsgActPrice = popupMsgActPrice;
	}
	@Transient
	public String getLpDiscMsg() {
		return lpDiscMsg;
	}
	public void setLpDiscMsg(String lpDiscMsg) {
		this.lpDiscMsg = lpDiscMsg;
	}

	
}
