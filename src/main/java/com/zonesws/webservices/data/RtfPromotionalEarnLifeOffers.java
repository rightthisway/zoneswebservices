package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * represents customer entity
 * @author Ulaganathan
 *
 */
@Entity
@Table(name="rtf_promotional_earn_life_offers")
public class RtfPromotionalEarnLifeOffers  implements Serializable{
	
	private Integer id;
	private String promoCode;
	private Date startDate;
	private Date endDate;
	private Integer maxLifePerCustomer;
	private Integer maxAccumalateCount;
	private Integer curAccumalateCount;
	private String status;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private String modifiedDate;
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id ,Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="promo_code")
	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	@Column(name="start_date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name="end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name="max_life_per_customer")
	public Integer getMaxLifePerCustomer() {
		return maxLifePerCustomer;
	}

	public void setMaxLifePerCustomer(Integer maxLifePerCustomer) {
		this.maxLifePerCustomer = maxLifePerCustomer;
	}

	@Column(name="maximum_accumulate_threshold")
	public Integer getMaxAccumalateCount() {
		return maxAccumalateCount;
	}

	public void setMaxAccumalateCount(Integer maxAccumalateCount) {
		this.maxAccumalateCount = maxAccumalateCount;
	}

	@Column(name="cur_accumulated_count")
	public Integer getCurAccumalateCount() {
		return curAccumalateCount;
	}

	public void setCurAccumalateCount(Integer curAccumalateCount) {
		this.curAccumalateCount = curAccumalateCount;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name="modified_time")
	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
}
