package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
/**
 * class to represent  State (used in Address)
 * @author hamin
 *
 */
@XStreamAlias("State")
@Entity
@Table(name="state")
public class State implements Serializable{
	private String name;
	private Integer id;
	private Integer countryId;
	private String shortDesc;
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, Id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @param name, name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 
	 * @return countryId
	 */
	@Column(name="country_id")
	public Integer getCountryId() {
		return countryId;
	}
	/**
	 * 
	 * @param countryId, id of a country to set
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	/**
	 * 
	 * @return ShortDesc
	 */
	@Column(name="Short_desc")
	public String getShortDesc() {
		return shortDesc;
	}
	/**
	 * 
	 * @param shortDesc, Short description of a state 
	 */
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	
	
	
	
}
