package com.zonesws.webservices.data;

import java.util.Date;



public class StripeDispute {

	String id;
	Double amount;
	String charge;
	Date created;
	String currency;
	Boolean isChargeRefundable;
	String reason;
	String status;
	String customerPurchaseIp;
	String customerEmailAddress;
	String duplicateChargeId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Boolean getIsChargeRefundable() {
		return isChargeRefundable;
	}
	public void setIsChargeRefundable(Boolean isChargeRefundable) {
		this.isChargeRefundable = isChargeRefundable;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomerPurchaseIp() {
		return customerPurchaseIp;
	}
	public void setCustomerPurchaseIp(String customerPurchaseIp) {
		this.customerPurchaseIp = customerPurchaseIp;
	}
	public String getCustomerEmailAddress() {
		return customerEmailAddress;
	}
	public void setCustomerEmailAddress(String customerEmailAddress) {
		this.customerEmailAddress = customerEmailAddress;
	}
	public String getDuplicateChargeId() {
		return duplicateChargeId;
	}
	public void setDuplicateChargeId(String duplicateChargeId) {
		this.duplicateChargeId = duplicateChargeId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	
	
	
}
