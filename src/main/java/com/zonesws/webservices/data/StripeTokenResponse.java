package com.zonesws.webservices.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
@XStreamAlias("StripeTokenResponse")
public class StripeTokenResponse {

	private Integer status;
	private Error error;
	private String tokenString;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getTokenString() {
		return tokenString;
	}
	public void setTokenString(String tokenString) {
		this.tokenString = tokenString;
	}
	
	
}
