package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

/*@SuppressWarnings("serial")
@Entity
@Table(name="artist")*/
public class SuperFanTeam implements Serializable  {
	
	private Integer id;
	private Integer leagueId;
	private String name;
	@JsonIgnore
	private Integer grandChildId;
	@JsonIgnore
	private Integer artistId;
	@JsonIgnore
	private String status;
	private String rewardTag;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	public Integer getGrandChildId() {
		return grandChildId;
	}
	public void setGrandChildId(Integer grandChildId) {
		this.grandChildId = grandChildId;
	}
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public String getRewardTag() {
		return rewardTag;
	}
	public void setRewardTag(String rewardTag) {
		this.rewardTag = rewardTag;
	}
	
	
}

