package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.zonesws.webservices.enums.TicketStatus;

@XStreamAlias("Ticket")
@Entity
@Table(name="ticket")
public class Ticket implements Serializable{
	
	private Integer id;
	private String section;
	private String normalizedSection;
	private String row;
	private String seat;
	private Integer eventId;

	@XStreamOmitField
	private Double currentPrice;
	
	@XStreamOmitField
	private Double purchasePrice;
	
	@XStreamOmitField
	private Double finalPrice;
	@XStreamOmitField
	private String siteId;
	@XStreamOmitField
	private String seller;
	@XStreamOmitField
	private String itemId;
	@XStreamOmitField
	private TicketStatus ticketStatus;
	
	@XStreamOmitField
	private Integer soldQuantity;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="normalized_section")
	public String getNormalizedSection() {
		return normalizedSection;
	}
	public void setNormalizedSection(String normalizedSection) {
		this.normalizedSection = normalizedSection;
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name="seat")
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	
	@Column(name="current_price")
	public Double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = (currentPrice == null) ? null : (!(currentPrice == this.currentPrice)) ? currentPrice : this.currentPrice;
	}
	
	@Transient
	public Double getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	@Transient
	public Double getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}
	
	@Column(name="site_id")
	@Index(name="siteIdIndex")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = (siteId == null) ? null : (!siteId.equals(this.siteId)) ? siteId : this.siteId;
	}
	@Column(name="seller")
	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = (seller == null) ? null : (!seller.equals(this.seller)) ? seller : this.seller;
	}
	
	@Column(name="item_id")
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = (itemId == null) ? null : (!itemId.equals(this.itemId)) ? itemId : this.itemId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="ticket_status")
	@Index(name="ticketStatusIndex")	
	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = (ticketStatus == null) ? null : (!ticketStatus.equals(this.ticketStatus)) ? ticketStatus : this.ticketStatus;
	}
	
	
	@Column(name="sold_quantity")
	public Integer getSoldQuantity() {
		return soldQuantity;
	}

	public void setSoldQuantity(Integer soldQuantity) {		
		this.soldQuantity = (soldQuantity == null) ? null : (!soldQuantity.equals(this.soldQuantity)) ? soldQuantity : this.soldQuantity;
	}
	
}
