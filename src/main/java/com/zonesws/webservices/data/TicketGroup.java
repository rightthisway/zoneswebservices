package com.zonesws.webservices.data;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zonesws.webservices.utils.TicketUtil;

/**
 * Entity class for TicketGroup
 * @author kulaganathan
 *
 */
@Entity
@Table(name= "ticket_group")
public class TicketGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="section")
	private String section;
	
	@Column(name="row")
	private String row;
	
	@Column(name="event_id")
	private Integer eventId;
	
	@Column(name="event_name")
	private String eventName;
	
	@Column(name="event_date")
	private Date eventDate;
	
	@Column(name="event_time")
	private Time eventTime;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="seat_low")
	private String seatLow;
	
	@Column(name="seat_high")
	private String seatHigh;
	
	@Column(name="price")
	private Double price;
	
	@Transient
	private String shippingMethod;
	
	@Column(name="shipping_method_id")
	private Integer shippingMethodId;
	
	@Column(name="created_date")
	private Date createdDate;
		
	@Column(name="last_updated")
	private Date lastUpdated;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="invoice_id")
	private Integer invoiceId;
	
	@Column(name="purchase_order_id")
	private Integer purchaseOrderId;

	@Column(name="notes")
	private String notes;
	
	@Column(name="on_hand")
	private String onHand;
	
	@Column(name="status")
	private String status;
	
	@Column(name="instant_download")
	private Boolean instantDownload;
	
	@Column(name="etickets_attached_date")
	private Date eticketsAttachedDate;
	
	@Column(name="etickets_attached")
	private Boolean eticketsAttached;
	
	@Column(name="broker_id")
	private Integer brokerId;
	
	@Column(name="broadcast")
	private Boolean broadcast;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getSeatLow() {
		return seatLow;
	}

	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}

	public String getSeatHigh() {
		return seatHigh;
	}

	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}

	public Double getPrice() {
		try{
			if(price != null){
				return TicketUtil.getRoundedValue(price);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	
	public String getOnHand() {
		return onHand;
	}

	public void setOnHand(String onHand) {
		this.onHand = onHand;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getInstantDownload() {
		return instantDownload;
	}

	public void setInstantDownload(Boolean instantDownload) {
		this.instantDownload = instantDownload;
	}

	public Date getEticketsAttachedDate() {
		return eticketsAttachedDate;
	}

	public void setEticketsAttachedDate(Date eticketsAttachedDate) {
		this.eticketsAttachedDate = eticketsAttachedDate;
	}

	public Boolean getEticketsAttached() {
		return eticketsAttached;
	}

	public void setEticketsAttached(Boolean eticketsAttached) {
		this.eticketsAttached = eticketsAttached;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public Boolean getBroadcast() {
		return broadcast;
	}

	public void setBroadcast(Boolean broadcast) {
		this.broadcast = broadcast;
	}

	
	
	
}