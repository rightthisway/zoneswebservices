package com.zonesws.webservices.data;

import java.io.Serializable;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.zonesws.webservices.utils.DateConverter;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("Item")

public class TicketItem implements Serializable  {
	
	/**
	 * 
	 */
	private static DecimalFormat df = new DecimalFormat("#.##");
	private static final long serialVersionUID = 1L;
	
	private String tourName;
	private String  eventName;
	@XStreamConverter(DateConverter.class)
	private Date eventDate;
	private String category;
	private Integer quantity=0;
	private Double price;
	private Double aoPrice;
	private Double markedPrice;
	private Double companyPrice;
	
	
	public String getTourName() {
		return tourName;
	}
	public void setTourName(String tourName) {
		this.tourName = tourName;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		if(null != price) {
			this.price  = Double.valueOf(df.format(price));
		} else {
			this.price = price;
		}
	}
	public Double getAoPrice() {
		return aoPrice;
	}
	public void setAoPrice(Double aoPrice) {
		if(aoPrice != null) {
			this.aoPrice = Double.valueOf(df.format(aoPrice));
		} else {
			this.aoPrice = aoPrice;
		}
	}
	public Double getMarkedPrice() {
		return markedPrice;
	}
	public void setMarkedPrice(Double markedPrice) {
		if(markedPrice != null) {
			this.markedPrice = Double.valueOf(df.format(markedPrice));
		} else {
			this.markedPrice = markedPrice;
		}
	}
	public Double getCompanyPrice() {
		return companyPrice;
	}
	public void setCompanyPrice(Double companyPrice) {
		if(companyPrice != null) {
			this.companyPrice = Double.valueOf(df.format(companyPrice));
		} else {
			this.companyPrice = companyPrice;
		}
	}
	
		
}
