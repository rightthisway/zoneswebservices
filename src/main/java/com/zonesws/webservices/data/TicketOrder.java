package com.zonesws.webservices.data;

import java.io.Serializable;
import java.text.DecimalFormat;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.TicketGroup;
import com.zonesws.webservices.utils.ZoneEvent;

@XStreamAlias("TicketOrder")

public class TicketOrder implements Serializable  {
	public static DecimalFormat df = new DecimalFormat("#.##");
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer orderNo;
	private Integer customerId;
	private String orderDate;
	private Double orderTotal;
	private Double discountAmount;
	private Double promotionalOffer;
	private Double loyaltySpent;
	private Double loyaltyEarned;
	private Integer quantity;
	private String zone;
	private String section;
	private String row;
	
	
	public Integer getOrderNo() {
		if(null == orderNo){
			orderNo = 0;
		}
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public Double getOrderTotal() {
		if(null == orderTotal){
			orderTotal = 0d;
		}
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Double getDiscountAmount() {
		if(null == discountAmount){
			discountAmount = 0d;
		}
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getPromotionalOffer() {
		if(null == promotionalOffer){
			promotionalOffer = 0d;
		}
		return promotionalOffer;
	}
	public void setPromotionalOffer(Double promotionalOffer) {
		this.promotionalOffer = promotionalOffer;
	}
	public Double getLoyaltySpent() {
		if(null == loyaltySpent){
			loyaltySpent = 0d;
		}
		return loyaltySpent;
	}
	public void setLoyaltySpent(Double loyaltySpent) {
		this.loyaltySpent = loyaltySpent;
	}
	public Double getLoyaltyEarned() {
		if(null == loyaltyEarned){
			loyaltyEarned = 0d;
		}
		return loyaltyEarned;
	}
	public void setLoyaltyEarned(Double loyaltyEarned) {
		this.loyaltyEarned = loyaltyEarned;
	}
	public Integer getQuantity() {
		if(null == quantity){
			quantity = 0;
		}
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getZone() {
		if(null == zone || zone.isEmpty()){
			zone = "";
		}
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	
	public String getSection() {
		if(null == section || section.isEmpty()){
			section = "";
		}
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		if(null == row || row.isEmpty()){
			row = "";
		}
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	
	
	
}
