package com.zonesws.webservices.data;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@Table(name="tkt_tracking")
public class TicketTraking implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tourName;
	private String  eventName;
	private Date eventDate;
	private String category;
	private Integer qnty=0;
	private Double price;
	private Double aoPrice;
	private Double markedPrice;
	private Double companyPrice;
	
	private Date date;
	private Double totalPrice;
	private Integer txnStatus;
	private Double tktPrice;
	private String feesProcessing;
	private String id;
	private Integer isLocked;
	private String locked;
	private String location;
	private Integer userOrderId;
	
	private Boolean checked = false;
	private Boolean isHdrOrder = false;
	private String businessType;
	private String companyType;
	private Integer companyId;
	private Double markup;
	private Integer busnModel;
	private Integer orderID;
	private String cmpName;
	
	@Transient
	public TicketItem getTicketItem() {
		TicketItem tktItem = new TicketItem();
		tktItem.setTourName(getTourName());
		tktItem.setEventDate(getEventDate());
		tktItem.setEventName(getEventName());
		tktItem.setCategory(getCategory());
		tktItem.setQuantity(getQnty());
		tktItem.setPrice(getPrice());
		tktItem.setAoPrice(getAoPrice());
		tktItem.setCompanyPrice(getCompanyPrice());
		tktItem.setMarkedPrice(getMarkedPrice());
		
		return tktItem;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="tour_name")
	public String getTourName() {
		return tourName;
	}
	public void setTourName(String tourName) {
		this.tourName = tourName;
	}
	
	@Id
	@Column(name="order_id")
	public Integer getOrderID() {
		return orderID;
	}
	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}
	
	@Transient
	public Double getMarkedPrice() {
		
		if(isHdrOrder) {
			if(totalPrice != null && markup != null)
			{
				Double tmp = totalPrice;
				tmp = (tmp*100)/(100+markup);
				markedPrice = (totalPrice-tmp);
				return markedPrice;
			}else
				markedPrice = 0.0;
				return 0.0;		
		} else {
			if(price != null && markup != null) {
				Double tmp = price*qnty;
				tmp = (tmp*100)/(100+markup);
				markedPrice = ((price*qnty)-tmp);
				return markedPrice;
			}else
				markedPrice = 0.0;
				return 0.0;	
		}
		
	}

	
	@Column(name="date",columnDefinition="DATE")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Column(name="event_date",columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	@Column(name="cmp_name")
	public String getCmpName() {
		return cmpName;
	}
	public void setCmpName(String cmpName) {
		this.cmpName = cmpName;
	}
	
	@Column(name="trxn_status")
	public Integer getTxnStatus() {
		return txnStatus;
	}
	public void setTxnStatus(Integer txnStatus) {
		this.txnStatus = txnStatus;
	}
	
	@Column(name="tkt_price")
	public Double getTktPrice() {
		return tktPrice;
	}
	public void setTktPrice(Double tktPrice) {
		this.tktPrice = tktPrice;
	}
	
	@Column(name="tkt_qnty")
	public Integer getQnty() {
		return qnty;
	}
	public void setQnty(Integer qnty) {
		
		if(null != qnty) {
			this.qnty = qnty;
		} else {
			this.qnty = 0;
		}
	}
	
	@Column(name="total_price")
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Transient
	public Double getCompanyPrice() {
		
		if(isHdrOrder) {
			companyPrice = totalPrice;
			return companyPrice;
		} else {
			if(price != null) {
				companyPrice = price*qnty;
				return companyPrice;
			} else {
				companyPrice = 0.0;
				return 0.0;
			}
		}
	}
	
	@Column(name="markup")
	public Double getMarkup() {
		return markup;
	}
	public void setMarkup(Double markup) {
		this.markup = markup;
	}
	
	@Column(name="b2b")
	public Integer getBusnModel() {
		return busnModel;
	}
	public void setBusnModel(Integer busnModel) {
		this.busnModel = busnModel;
	}
	
	@Transient
	public String getBusinessType() {
		if(companyType!=null && companyType.equals("C"))
			return "B2C";
		else if(companyType!=null && companyType.equals("B"))
			return "B2B";
		else if(companyType!=null && companyType.equals("A"))
			return "API";
		else 
			return null;
	}
	/*public String getBusinessType() {
		if(busnModel!=null && busnModel == 0)
			return "B2C";
		else if(busnModel!=null && busnModel == 1)
			return "B2B";
		else
			return null;
	}*/
	
	@Transient
	public String getFeesProcessing() {
		if(txnStatus!= null &&txnStatus == 0)
			return "N";
		else if(txnStatus!= null && txnStatus == 1)
			return "P";
		else
			return "N";
	}
	
	@Transient
	public Double getAoPrice() {
		
		if(isHdrOrder) {
			if(totalPrice != null && markup != null)
			{
				Double tmp = totalPrice;
				tmp = (tmp*100)/(100+markup);
				aoPrice = tmp;
				return aoPrice;
			}else if(markup == null && totalPrice !=null)
			{
				aoPrice = totalPrice;
				return aoPrice;
			}else
				aoPrice = 0.0;
				return aoPrice;
		} else {
			if(price != null && markup != null)
			{
				Double tmp = price*qnty;
				tmp = (tmp*100)/(100+markup);
				aoPrice = tmp;
				return aoPrice;
			}else if(markup == null && price !=null)
			{
				aoPrice = price*qnty;
				return aoPrice;
			}else
				aoPrice = 0.0;
				return aoPrice;
		}
		
	}
	
	@Transient
	public Boolean getChecked() {
		return checked;
	}
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	
	@Transient
	public String getId() {
		return String.valueOf(orderID);
	}
	
	@Column(name="isLock")
	public Integer getIsLocked() {
		return isLocked;
	}
	public void setIsLocked(Integer isLocked) {
		this.isLocked = isLocked;
	}
	
	@Transient
	public String getLocked() {
		
		if(isLocked!= null &&isLocked == 0)
			return "N";
		else if(isLocked!= null && isLocked == 1)
			return "L";
		else
			return "N";
		
	}
	
	@Column(name="loc")
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Column(name="company_type")
	public String getCompanyType() {
		return companyType;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Column(name="user_order_id")
	public Integer getUserOrderId() {
		return userOrderId;
	}
	public void setUserOrderId(Integer userOrderId) {
		this.userOrderId = userOrderId;
	}
	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Transient
	public Boolean getIsHdrOrder() {
		return isHdrOrder;
	}
	@Transient
	public void setIsHdrOrder(Boolean isHdrOrder) {
		this.isHdrOrder = isHdrOrder;
	}
	@Column(name="company_id")
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	
}
