package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.enums.Status;


@XStreamAlias("Tour")
@Entity
@Table(name="tour")
public class Tour implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	
	@XStreamOmitField
	private Integer tmatTourId;
//	@XStreamConverter(DateConverter.class)
//	private Date startDate;
//	@XStreamConverter(DateConverter.class)
//	private Date endDate;
	@XStreamAlias("status")
	private Status tourStatusId;
	
	@XStreamOmitField
	private Integer grandChildCategoryId;
//	private String image;
	
	private String parentCategroy;
	private String childCategroy;
	private String grandChildCategoryName;
	private String description;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
		 
	}
	/**
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="statusId")
	public Status getTourStatusId() {
		return tourStatusId;
	}
	/**
	 * @param tourStatusId the tourStatusId to set
	 */
	public void setTourStatusId(Status tourStatusId) {
		this.tourStatusId = tourStatusId;
	}
		
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="tourId")
	public Integer getTmatTourId() {
		return tmatTourId;
	}
	public void setTmatTourId(Integer tmatTourId) {
		this.tmatTourId = tmatTourId;
	}
	
	
	
	@Column(name="grandChildCategoryId")
	public Integer getGrandChildCategoryId() {
		if(null != grandChildCategoryId){
			GrandChildCategory grandChildCategory =DAORegistry.getGrandChildCategoryDAO().get(grandChildCategoryId);
			ChildCategory childCategoryObj =DAORegistry.getChildCategoryDAO().get(grandChildCategory.getChildCatId()); 
			this.parentCategroy = childCategoryObj.getParentCategory().getName();
			this.childCategroy = childCategoryObj.getName();
			this.grandChildCategoryName = grandChildCategory.getName();
		}
		return grandChildCategoryId;
	}
	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		if(null != grandChildCategoryId){
			GrandChildCategory grandChildCategory =DAORegistry.getGrandChildCategoryDAO().get(grandChildCategoryId);
			ChildCategory childCategoryObj =DAORegistry.getChildCategoryDAO().get(grandChildCategory.getChildCatId()); 
			this.parentCategroy = childCategoryObj.getParentCategory().getName();
			this.childCategroy = childCategoryObj.getName();
			this.grandChildCategoryName = grandChildCategory.getName();
		}
		this.grandChildCategoryId = grandChildCategoryId;
	}
	
	
	@Transient
	public String getParentCategroy() {
		
		if(null != grandChildCategoryId){
			GrandChildCategory grandChildCategory =DAORegistry.getGrandChildCategoryDAO().get(grandChildCategoryId);
			ChildCategory childCategoryObj =DAORegistry.getChildCategoryDAO().get(grandChildCategory.getChildCatId()); 
			this.parentCategroy = childCategoryObj.getParentCategory().getName();
		}
		return parentCategroy;
	}
	
	
	@Transient
	public String getChildCategroy() {
		if(null != grandChildCategoryId){
			GrandChildCategory grandChildCategory =DAORegistry.getGrandChildCategoryDAO().get(grandChildCategoryId);
			ChildCategory childCategoryObj =DAORegistry.getChildCategoryDAO().get(grandChildCategory.getChildCatId()); 
			this.childCategroy = childCategoryObj.getName();
		}
		return childCategroy;
	}
	
	
	@Transient
	public String getGrandChildCategoryName() {
		
		if(null != grandChildCategoryId){
			GrandChildCategory grandChildCategory =DAORegistry.getGrandChildCategoryDAO().get(grandChildCategoryId);
			this.grandChildCategoryName = grandChildCategory.getName();
		}
		return grandChildCategoryName;
	}
	
	
}


