package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zones_user_preference")
public class UserPreference implements Serializable {

	//private static final long serialVersionUID = -7941769011539361145L;
	
	private Integer id;
	private String ipAddress;
	private String userName;
	private boolean showApplink;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	@Column(name="username")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="show_applink")
	public boolean isShowApplink() {
		return showApplink;
	}
	public void setShowApplink(boolean showApplink) {
		this.showApplink = showApplink;
	}
	
	
}
