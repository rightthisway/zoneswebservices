package com.zonesws.webservices.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("WalletTransaction")
public class WalletTransaction {
	private Integer status;
	private Error error;
	private CustomerWallet wallet;
	private CustomerWalletHistory walletHistory;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public CustomerWallet getWallet() {
		return wallet;
	}
	public void setWallet(CustomerWallet wallet) {
		this.wallet = wallet;
	}
	public CustomerWalletHistory getWalletHistory() {
		return walletHistory;
	}
	public void setWalletHistory(CustomerWalletHistory walletHistory) {
		this.walletHistory = walletHistory;
	}
	
	
	
}
