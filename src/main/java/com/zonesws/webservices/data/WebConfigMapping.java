package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "company_configid_mapping")
public class WebConfigMapping implements Serializable {

	/**
	 * added only to remove comment
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	//private Company company;
	private WebServiceConfig webServiceConfig;
	private String configId;
	
	
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	
	
	/*
	@OneToOne
	@JoinColumn(name = "company_id")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}*/
	
	@OneToOne
	@JoinColumn(name = "web_service_config_id")
	public WebServiceConfig getWebServiceConfig() {
		return webServiceConfig;
	}

	public void setWebServiceConfig(WebServiceConfig webServiceConfig) {
		this.webServiceConfig = webServiceConfig;
	}

	@Column(name = "config_id")
	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	
	
}
