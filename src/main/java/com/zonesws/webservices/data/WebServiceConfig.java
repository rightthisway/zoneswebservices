package com.zonesws.webservices.data;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.Status;


@Entity
@Table(name="webservice_config")
public class WebServiceConfig implements Serializable{

	private Integer id;
	private String description;
	private String configId;
	private String token;
	private String secureKey;
	private Status status;
	private ApplicationPlatForm platForm;
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="config_id")
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
	
	@Column(name="token")
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	@Column(name="secure_key")
	public String getSecureKey() {
		return secureKey;
	}
	public void setSecureKey(String secureKey) {
		this.secureKey = secureKey;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="platForm")
	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}
	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}
	
	
	
}
