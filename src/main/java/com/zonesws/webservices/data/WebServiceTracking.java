package com.zonesws.webservices.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.WebServiceActionType;

@Entity
@Table(name="web_service_tracking_new")
public class WebServiceTracking implements Serializable{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="product_type")
	@Index(name="productTypeIndex")
	private ProductType productType;
	
	@Column(name="server_ip_address")
	private String serverIpAddress;
	
	@Column(name="customer_ip_address")
	private String customerIpAddress;
	
	@Column(name="web_config_id")
	private String webConfigId;
	
	@Column(name="web_xsign")
	private String webXSign;
	
	@Column(name="session_id")
	private String sessionId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	@Index(name="platFormIndex")
	private ApplicationPlatForm platForm;
	
	@Enumerated(EnumType.STRING)
	@Column(name="api_name")
	@Index(name="apiNameIndex")
	private WebServiceActionType actionType;
	
	@Column(name="hit_date")
	private java.util.Date hitDate;
	
	@Column(name="event_search_key")
	private String eventSearchKey;
	
	@Column(name="location_search")
	private String locationSearch;
	
	@Column(name="event_id")
	private Integer eventId;
	
	@Column(name="order_id")
	private Integer orderId;
	
	@Column(name="customer_id")
	private Integer customerId;
	
	@Column(name="category_ticket_group_id")
	private Integer categoryTicketGroupId;
	
	@Column(name="trx_amount")
	private Double trxAmount;
	
	@Column(name="authenticated_hit")
	private String authenticatedHit;
	
	@Column(name="action_result")
	private String actionResult;
	
	@Column(name="contest_id")
	private String contestId;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServerIpAddress() {
		return serverIpAddress;
	}

	public void setServerIpAddress(String serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}

	public String getCustomerIpAddress() {
		return customerIpAddress;
	}

	public void setCustomerIpAddress(String customerIpAddress) {
		this.customerIpAddress = customerIpAddress;
	}

	public String getWebConfigId() {
		return webConfigId;
	}

	public void setWebConfigId(String webConfigId) {
		this.webConfigId = webConfigId;
	}

	public String getWebXSign() {
		return webXSign;
	}

	public void setWebXSign(String webXSign) {
		this.webXSign = webXSign;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ApplicationPlatForm getPlatForm() {
		return platForm;
	}

	public void setPlatForm(ApplicationPlatForm platForm) {
		this.platForm = platForm;
	}

	public WebServiceActionType getActionType() {
		return actionType;
	}

	public void setActionType(WebServiceActionType actionType) {
		this.actionType = actionType;
	}

	public java.util.Date getHitDate() {
		return hitDate;
	}

	public void setHitDate(java.util.Date hitDate) {
		this.hitDate = hitDate;
	}

	public String getEventSearchKey() {
		return eventSearchKey;
	}

	public void setEventSearchKey(String eventSearchKey) {
		this.eventSearchKey = eventSearchKey;
	}

	public String getLocationSearch() {
		return locationSearch;
	}

	public void setLocationSearch(String locationSearch) {
		this.locationSearch = locationSearch;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}

	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}

	public Double getTrxAmount() {
		return trxAmount;
	}

	public void setTrxAmount(Double trxAmount) {
		this.trxAmount = trxAmount;
	}

	public String getAuthenticatedHit() {
		return authenticatedHit;
	}

	public void setAuthenticatedHit(String authenticatedHit) {
		this.authenticatedHit = authenticatedHit;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getContestId() {
		return contestId;
	}

	public void setContestId(String contestId) {
		this.contestId = contestId;
	}
	
	
}
