package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="web_service_tracking_cassandra")
public class WebServiceTrackingCassandra implements Serializable{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate;
	 
	@Column(name="cust_ip_addr")
	private String custIPAddr;
	
	@Column(name="api_name")
	private String apiName;
	
	@Column(name="platform")
	private String platForm;
	
	@Column(name="device_type")
	private String deviceType;
	
	@Column(name="session_id")
	private String sessionId;
	
	@Column(name="action_result")
	private String actionResult;
	
	@Column(name="cust_id")
	private Integer custId;
	
	@Column(name="contest_id")
	private Integer contestId;
	
	@Column(name="description")
	private String description;
	
	@Column(name="device_info")
	private String deviceInfo;
	
	@Column(name="app_ver")
	private String appVersion;
	
	@Column(name="created_datetime")
	private Date createdDateTime;
	
	@Column(name="fb_callback_time")
	private Date fbCallbackTime;
	
	@Column(name="device_api_start_time")
	private Date deviceAPIStartTime;
	
	@Column(name="res_status")
	private Integer responseStatus;
	
	@Column(name="question_no")
	private String questionNo;
	
	@Column(name="rt_count")
	private Integer retryCount;
	
	@Column(name="ans_count")
	private Integer ansCount;
	
	@Column(name="node_id")
	private Integer nodeId;
	
	@Transient
	private String startDateStr;
	
	@Transient
	private String endDateStr;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCustIPAddr() {
		return custIPAddr;
	}

	public void setCustIPAddr(String custIPAddr) {
		this.custIPAddr = custIPAddr;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getPlatForm() {
		return platForm;
	}

	public void setPlatForm(String platForm) {
		this.platForm = platForm;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}

	public Integer getCustId() {
		if(custId == null) {
			custId = 0;
		}
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public Integer getContestId() {
		if(contestId == null) {
			contestId =  0;
		}
		return contestId;
	}

	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public final Date getFbCallbackTime() {
		return fbCallbackTime;
	}

	public final void setFbCallbackTime(Date fbCallbackTime) {
		this.fbCallbackTime = fbCallbackTime;
	}

	public final Date getDeviceAPIStartTime() {
		return deviceAPIStartTime;
	}

	public final void setDeviceAPIStartTime(Date deviceAPIStartTime) {
		this.deviceAPIStartTime = deviceAPIStartTime;
	}

	public final Integer getResponseStatus() {
		if(responseStatus == null) {
			responseStatus =0;
		}
		return responseStatus;
	}

	public final void setResponseStatus(Integer responseStatus) {
		this.responseStatus = responseStatus;
	}

	public final String getQuestionNo() {
		return questionNo;
	}

	public final void setQuestionNo(String questionNo) {
		this.questionNo = questionNo;
	}

	public Integer getRetryCount() {
		if(retryCount == null) {
			retryCount = 0;
		}
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public Integer getAnsCount() {
		if(ansCount == null) {
			ansCount =0;
		}
		return ansCount;
	}

	public void setAnsCount(Integer ansCount) {
		this.ansCount = ansCount;
	}

	public Integer getNodeId() {
		if(nodeId == null) {
			nodeId =0;
		}
		return nodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	
}
