package com.zonesws.webservices.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.zonesws.webservices.enums.HoldTicketStatus;
import com.zonesws.webservices.enums.SessionCartType;
/**
 * class to represent user session,used to store information regarding the tickets which user buy within particular user  session 
 * @author hamin
 *
 */
@Entity
@Table(name="zp_session_cart")
public class ZPSessionCart implements Serializable{
	private Integer id;
	private String sessionId;
	private Date creationDate;
	private Integer quantity;
	private Integer eventId;
	private String category;
	private SessionCartType sessionCartType=SessionCartType.ZONES_WEB_SERVICES;
	
	/**
	 * 
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	/**
	 * 
	 * @param id, id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 
	 * @return sessionId
	 */
	@Column(name="session_id")
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * 
	 * @param sessionId, http session Id to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
	/**
	 * 
	 * @return creationDate
	 */
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	/**
	 * 
	 * @param creationDate, creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	/**
	 * 
	 * @return quantity
	 */
	@Column(name="qty")
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * 
	 * @param quantity, quantity of tickets to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * 
	 * @return eventId
	 */
	@Column(name="eventId")
	public Integer getEventId() {
		return eventId;
	}
	/**
	 * 
	 * @param eventId, Id of event to set
	 */
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	/**
	 * 
	 * @return category
	 */
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	/**
	 * 
	 * @param category, category (sports, concert, theater etc ) to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="cart_type")
	@Index(name="cartTypeIndex")	
	public SessionCartType getSessionCartType() {
		return sessionCartType;
	}
	
	
	public void setSessionCartType(SessionCartType sessionCartType) {
		this.sessionCartType = (sessionCartType == null) ? null : (!sessionCartType.equals(this.sessionCartType)) ? sessionCartType : this.sessionCartType;;
	}
	
	
}
