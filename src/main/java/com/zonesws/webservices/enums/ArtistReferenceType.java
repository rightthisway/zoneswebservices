package com.zonesws.webservices.enums;

public enum ArtistReferenceType {
	PARENT_ID,
	CHILD_ID,
	GRANDCHILD_ID,
	RTFCATS_ID,
	PARENT_NAME,
	CHILD_NAME,
	GRANDCHILD_NAME,
	RTFCATS_NAME
}
