package com.zonesws.webservices.enums;

public enum ArtistStatus {
	DELETED,ACTIVE, EXPIRED
}
