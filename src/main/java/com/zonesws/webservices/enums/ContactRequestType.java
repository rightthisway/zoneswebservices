package com.zonesws.webservices.enums;

public enum ContactRequestType {
	Partners, Sponsors, Media, Regular
}
