package com.zonesws.webservices.enums;

public enum EbayInventoryStatus {
	ACTIVE, DISABLED, INVALID, SUGGESTED, REPLACEMENT, DELETED
}
