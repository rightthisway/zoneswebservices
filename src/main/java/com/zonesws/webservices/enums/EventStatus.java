package com.zonesws.webservices.enums;

public enum EventStatus {
	ACTIVE, EXPIRED, DELETED,DELETEDEXPIRED
}
