package com.zonesws.webservices.enums;

public enum LockedTicketStatus {
	ACTIVE,DELETED,SOLD,ALREADY_SOLD
}
