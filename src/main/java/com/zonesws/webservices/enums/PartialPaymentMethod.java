package com.zonesws.webservices.enums;

public enum PartialPaymentMethod {
	CREDITCARD,PAYPAL,IPAY,GOOGLEPAY,NULL,CUSTOMER_WALLET,PARTIAL_REWARDS
}
