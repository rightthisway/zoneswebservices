package com.zonesws.webservices.enums;

public enum PopularGrandChildCategoryStatus {
	ACTIVE, EXPIRED, DELETED
}
