package com.zonesws.webservices.enums;

public enum RewardBreakUpType {
	PENDING_POINTS,REDEEMED_POINTS,EARNED_POINTS,VOIDED_POINTS,REVERTED_POINTS
}
