package com.zonesws.webservices.enums;

public enum RewardStatus {
	ACTIVE,PENDING,VOIDED,ORDERPENDING,PAID
}
