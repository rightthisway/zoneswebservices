package com.zonesws.webservices.enums;

public enum Status {
	DEFAULT,ACTIVE,EXPIRED, DELETED,SOLD
}
