package com.zonesws.webservices.enums;

public enum TicketStatus {
	ACTIVE, EXPIRED, SOLD, DISABLED ,LOCKED
}
