package com.zonesws.webservices.filter;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;


public class ReferralCodeGenerator {
	
	private static final String CHARACTER_SET = "0123456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";
	private static Random rnd = new Random();
	private static Integer maxLength = 8;
	
	public static String generateCustomerReferalCode(Long referrerNumber, String prefix) throws Exception{
	  StringBuilder builder = new StringBuilder();
	  String tempCharSet = new String(CHARACTER_SET);
	     for(int i = 0; i < maxLength; i++){
	         builder.append(tempCharSet.charAt(rnd.nextInt(tempCharSet.length())));
	     }
	     return builder.toString().toUpperCase();
	 }
	
	public static String generateCustomerReferalCodeOld(Long referrerNumber, String prefix) throws Exception{
		String uniqueString = UUID.randomUUID().toString();
		String sub = uniqueString.substring(0,4);
		String key = SecurityUtil.doBase64Encryption(sub+referrerNumber);
		key =key.replaceAll("=", "");
		String code = prefix+key;
		return code;
	}
	
	public static String generatePassword(String fisrtName, String prefix) throws Exception{
		if(fisrtName.length() > 3){
			fisrtName = fisrtName.substring(0,3);
		}
		String uniqueString = UUID.randomUUID().toString();
		String sub = uniqueString.substring(0,4);
		String key = SecurityUtil.doBase64Encryption(sub+fisrtName);
		key =key.replaceAll("=", "");
		String code = "RTF"+key;
		return code;
	}
	
	public static String generateCustWalletCreditTrxId(Integer customerId, Integer trxGenId, String prefix) throws Exception{
		String uniqueString = UUID.randomUUID().toString();
		String sub = uniqueString.substring(0,4);
		String key = SecurityUtil.doBase64Encryption(sub+customerId+trxGenId);
		key =key.replaceAll("=", "");
		String code = prefix+key;
		return code;
	}
	

	public static String randomString(Long referrerNumber, String prefix){
	    StringBuilder builder = new StringBuilder();
	    for(int i = 0; i < maxLength; i++){
	        builder.append(CHARACTER_SET.charAt(rnd.nextInt(CHARACTER_SET.length())));
	    }
	    return builder.toString();
	}
	
	 
	
	public static String generatePasswordForStaticCustomers(String email, String prefix) throws Exception{
		email = email.substring(0,3);
		String uniqueString = UUID.randomUUID().toString();
		String sub = uniqueString.substring(0,3);
		String key = SecurityUtil.doBase64Encryption(sub+email);
		key =key.replaceAll("=", "");
		String code = prefix+key;
		return code;
	}
	
	
	public static String generateCustomerPromotionalCode(String keyToHash) {
        String encryptedKey = null;
        try {
            String uniqueString = UUID.randomUUID().toString();
    		String sub = uniqueString.substring(0, 6);
    		String key = SecurityUtil.doBase64Encryption(sub + keyToHash);
    		key = key.replaceAll("=", "");
    		encryptedKey = key.substring(0, 6);
            
            String twoChracter = "";
            keyToHash = keyToHash.replaceAll(" ","");
            if(keyToHash.length() > 2){
            	twoChracter = keyToHash.substring(0,2);
            }
            encryptedKey = twoChracter + encryptedKey ;
        } 
        
        catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedKey.toUpperCase();
    }
	
	
	public static String generatedCustomerUserId(String email){
		  String userId="";
		  try {
		   if(email == null){
			   email="";
		   }
		   
		   
		   email = email.split("@")[0];
		   
		   email =  email.replaceAll("\\W+","").replaceAll("_", "");
		   
		   email = email.trim().replaceAll("'","");
		   email = email.trim().replaceAll("-","");
		   email = email.trim().replaceAll("_","");
		   email = email.trim().replaceAll("\\.","");
		   email = email.trim().replaceAll(" ","");
		   
		   userId = email.trim();
		   
		   if(userId.length() < 5){
		    int len = 5-userId.length();
		    for(int i=1;i<=len;i++){
		     userId += rnd.nextInt(10);
		    }
		   }else if(userId.length() > 12){
		    userId = userId.substring(0,12);
		   }
		   
		  Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
		   
		   if(customer != null){
		    if(userId.length() <= 10){
		     userId += rnd.nextInt(10); 
		     userId += rnd.nextInt(10);
		     customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
		    }
		    if(customer != null){
		     if(!email.trim().isEmpty()){
		      userId = email;
		     } 
		     
		     if(userId.length() < 5){
		      int len = 6-userId.length();
		      for(int i=1;i<=len;i++){
		       userId += rnd.nextInt(10);
		      }
		     }else if(userId.length() > 12){
		      userId = userId.substring(0,10);
		      userId += rnd.nextInt(10); 
		      userId += rnd.nextInt(10);
		     }else if(userId.length() < 9){
		      userId += rnd.nextInt(10); 
		      userId += rnd.nextInt(10);
		      userId += rnd.nextInt(10);
		     }
		    }
		   }
		  } catch (Exception e) {
		   e.printStackTrace();
		   userId = "";
		  }
		  return userId.toLowerCase();
		 }
	
	public static String generatedCustomerUserId(String fName,String lName){
		  String userId="";
		  try {
		   if(fName == null){
		    fName="";
		   }
		   if(lName == null){
		    lName="";
		   }
		   
		   fName = fName.trim().replaceAll("'","");
		   fName = fName.trim().replaceAll("-","");
		   fName = fName.trim().replaceAll("_","");
		   fName = fName.trim().replaceAll("\\.","");
		   fName = fName.trim().replaceAll(" ","");
		   
		   lName = lName.trim().replaceAll("'","");
		   lName = lName.trim().replaceAll("-","");
		   lName = lName.trim().replaceAll("_","");
		   lName = lName.trim().replaceAll("\\.","");
		   lName = lName.trim().replaceAll(" ","");
		   
		   if(!fName.trim().isEmpty() && !lName.trim().isEmpty()){
		    userId = String.valueOf(fName.trim().charAt(0));
		    userId += lName.trim();
		   }else if(!fName.trim().isEmpty() && lName.trim().isEmpty()){
		    userId = fName.trim();
		   }else if(fName.trim().isEmpty() && !lName.trim().isEmpty()){
		    userId = lName.trim();
		   }
		   
		   if(userId.length() < 5){
		    int len = 5-userId.length();
		    for(int i=1;i<=len;i++){
		     userId += rnd.nextInt(10);
		    }
		   }else if(userId.length() > 12){
		    userId = userId.substring(0,12);
		   }
		   
		   Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
		   
		   if(customer != null){
		    if(userId.length() <= 10){
		     userId += rnd.nextInt(10); 
		     userId += rnd.nextInt(10);
		     customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
		    }
		    if(customer != null){
		     if(!fName.trim().isEmpty()){
		      userId = fName;
		     }else if(!lName.trim().isEmpty()){
		      userId = lName;
		     }
		     if(userId.length() < 5){
		      int len = 6-userId.length();
		      for(int i=1;i<=len;i++){
		       userId += rnd.nextInt(10);
		      }
		     }else if(userId.length() > 12){
		      userId = userId.substring(0,10);
		      userId += rnd.nextInt(10); 
		      userId += rnd.nextInt(10);
		     }else if(userId.length() < 9){
		      userId += rnd.nextInt(10); 
		      userId += rnd.nextInt(10);
		      userId += rnd.nextInt(10);
		     }
		    }
		   }
		  } catch (Exception e) {
		   e.printStackTrace();
		   userId = "";
		  }
		  return userId.toLowerCase();
		 }
	
	
}
