package com.zonesws.webservices.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.WebServiceConfig;

public class RewardTheFanWebServiceFilter implements Filter{

	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpSession session = httpRequest.getSession();
		String ip =(String)session.getAttribute("ip");
		if(ip==null){
			session.setAttribute("ip",request.getRemoteAddr());
		}
		
		
		String sign = ((HttpServletRequest)request).getHeader("x-sign");
		String configId = ((HttpServletRequest)request).getHeader("x-token");
		try {
			// Get Data from WebServiceConfig table using token and secure key.. if match jai ho..
			WebServiceConfig config = DAORegistry.getWebServiceConfigDAO().getWebServiceConfgiByConfigId(configId);
			if(config==null){
				request.setAttribute("authorized", false); 
			}else{
				String token = SecurityUtil.doDecryption(sign, config.getSecureKey());
				System.out.println(token);
				if(token.equals(config.getToken())){
					request.setAttribute("authorized", true);
					
				}else{
					request.setAttribute("authorized", false);
				}
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.setAttribute("authorized", false);
		}
		
		System.out.println("Authorized ===>"+request.getParameter("authorized"));
		chain.doFilter(httpRequest, response);
	}

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
