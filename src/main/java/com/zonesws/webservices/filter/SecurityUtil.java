package com.zonesws.webservices.filter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.ReferredCodeTracking;
import com.zonesws.webservices.jobs.CustomerUtil;

public class SecurityUtil {
	
	public static void mainold(String[] st) throws Exception{
		
		/*('IOSRTFRTW','RTF IOS App Development','iosrtfrtw web service env.','RTF4#4PO==+ABIOS'),
		('ANDROIDRTFRTW','RTF ANDROID App Development','androidrtfrtw web service env.','RTF4#4PO==+ANDRO'),
		('DESKTOPRTFRTW','RTF DESKTOP App Development','desktoprtfrtw web service env.','RTF4#4PO==+DESKT')*/
		
		String keyString = "RTF4#4PO==+ANDRO";
		String token = "androidrtfrtw web service env.";
		
		String encryptedText = doEncryption(token, keyString);
		System.out.println("Encrypted Text is "+ encryptedText);
		
		String decodedText1 = doDecryption(encryptedText, keyString);
		System.out.println("Original Text is "+ decodedText1.trim());
		
		/*String externalSite = "8vtRQDaSt+h/kc6MxjZqs2Ev8X8bFo7+dENVZnVF4v0kUCcMcBwFz6mXWBn9RCqm";  // PHP
		String decodedText = doDecryption(externalSite, keyString);
		System.out.println("Original Text is "+ decodedText.trim());*/
		
		/*String cardNo = "454";
		String encryptedText = doEncryption(cardNo, keyString);
		System.out.println("Encrypted Text is :"+ encryptedText);
		
		//encryptedText="AwEbWTKp+WBm236Q3EAvvGV9OkZclH7u1gp8frHGNtophsNy0uy2iIz6XJfxp50dagytR+gX1wfqJsrWng9GhjDc4SfuhSRUVBPNmP/YaEmXZw==";
		
		String decodedText1 = doDecryption(encryptedText, keyString);
		System.out.println("Original Text is :"+ decodedText1.trim());*/
		
	}
	
	public static String doEncryption(String toBeEncryptedText, String encryptionKey) throws Exception{
		SecretKey secretKey = new SecretKeySpec(encryptionKey.getBytes(), "AES");
		String encryptedText = "";
		byte[] plainTextPasswordBytes = toBeEncryptedText.getBytes();
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptBytes = cipher.doFinal(plainTextPasswordBytes);
		encryptedText = Base64.encodeBase64String(encryptBytes);
		return encryptedText;
	}
	
	public static String doDecryption(String encryptedText, String keyString) throws Exception{
		SecretKey secretKey = new SecretKeySpec(keyString.getBytes(), "AES");
		String decryptedText = "";
		Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decodeBytes = null;// Base64.decodeBase64(encryptedText);
		decodeBytes = cipher.doFinal(Base64.decodeBase64(encryptedText));
		decryptedText = new String(decodeBytes);
		return decryptedText.trim();
	}
	
	public static String doBase64Decryption(String encryptedText){
		byte[] decodedBytes = Base64.decodeBase64(encryptedText.getBytes());
		String originalText = new String(decodedBytes);
		return originalText;
	}
	
	public static String doBase64Encryption(String originalText){
		byte[] encodedBytes = Base64.encodeBase64(originalText.getBytes());
		String encryptedText = new String(encodedBytes);
		return encryptedText;
	}
	
	
	
	/*public static CustomerCardInfo doCreditCardDecryption(CustomerCardInfo customerCardInfo , String keyString) throws Exception{
		customerCardInfo.setCardNoTemp(doBase64Decryption(customerCardInfo.getCardNoTemp()));
		customerCardInfo.setCardType(doBase64Decryption(customerCardInfo.getCardType()));
		customerCardInfo.setCardCvvNo(doBase64Decryption(customerCardInfo.getCardCvvNo()));
		customerCardInfo.setCardCustomerName(doBase64Decryption(customerCardInfo.getCardCustomerName()));
		customerCardInfo.setCardExpiryMonth(Integer.parseInt(doBase64Decryption(customerCardInfo.getCardExpiryMonthStr())));
		customerCardInfo.setCardExpiryYear(Integer.parseInt(doBase64Decryption(customerCardInfo.getCardExpiryYearStr())));
		return customerCardInfo;
	}*/
	
	public static void main(String[] args) {
		
		String prefix = "RTF";
		
		Set<String> refCode = new HashSet<String>();
		String uniqueString = UUID.randomUUID().toString();
		//System.out.println("UUID : "+uniqueString);
		String sub = uniqueString.substring(0,4);
		//System.out.println("UUID Sub : "+sub);
		String key = SecurityUtil.doBase64Encryption(sub+25641895);
		key =key.replaceAll("=", "");
		
		
		String code = prefix+key;
		System.out.println("Code : "+code);
		
		
	}
	
	
	public static Customer validateReferalIdentity(String referrerCustId,String refIdentity,Integer eventId,Integer toCustomerId,
			ReferredCodeTracking referredCodeTracking,Customer custReferal){
		
		String decodedRefCode = SecurityUtil.doBase64Decryption(refIdentity);
		String decodedCustId = SecurityUtil.doBase64Decryption(referrerCustId);
		
		try{
			Integer validateEventID = Integer.valueOf(decodedRefCode.trim());
			if(!validateEventID.equals(eventId)){
				custReferal.setEventShareValidated(false);
				return custReferal;
			}
		}catch(Exception e){
			e.printStackTrace();
			custReferal.setEventShareValidated(false);
			return custReferal;
		}
		
		try{
			Integer refCustomerId = Integer.valueOf(decodedCustId.trim());
			
			if(refCustomerId.equals(toCustomerId)){
				custReferal = new Customer();
				custReferal.setEventShareValidated(false);
				return custReferal;
			}
			
			custReferal = CustomerUtil.getCustomerById(refCustomerId);
			
			if(custReferal == null || null == custReferal.getId() ){
				custReferal = new Customer();
				custReferal.setEventShareValidated(false);
				return custReferal;
			}
			System.out.println("custReferal======>"+custReferal);
			referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().getTrackingInfoByReferralCodeAndReferrarId(custReferal.getReferrerCode(), toCustomerId);
			
			if(null != referredCodeTracking){
				custReferal.setEventShareValidated(false);
				return custReferal;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			custReferal.setEventShareValidated(false);
			return custReferal;
		}
		custReferal.setEventShareValidated(true);
		return custReferal;
	}
	
}
