package com.zonesws.webservices.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rtf.ecomerce.dao.impl.EcommDAORegistry;
import com.rtf.ecomerce.data.EcommWebServiceTracking;

public class WebServiceFilter implements Filter{

	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		HttpSession session = httpRequest.getSession();
		String ip =(String)session.getAttribute("ip");
		if(ip==null){
			session.setAttribute("ip",request.getRemoteAddr());
		}
		
		//System.out.println("configId : "+httpRequest.getParameter("configId")+"===="+request.getParameter("configId"));
		String sign = ((HttpServletRequest)request).getHeader("x-sign");
		String configId = ((HttpServletRequest)request).getHeader("x-token");
		String xPlatform = ((HttpServletRequest)request).getHeader("x-platform");
		
		if(null == xPlatform || xPlatform.isEmpty()){
			request.setAttribute("authorized", false);
		}else if(null == configId || configId.isEmpty()){
			request.setAttribute("authorized", false);
		}else{
			try {
				
				// Get Data from WebServiceConfig table using token and secure key.. if match jai ho..
				//WebServiceConfig config = ApiConfigUtil.getWebServiceConfigById(configId);
				
				/* 11/27/2018 1:04PM Accepting all request - Commented By Ulaga - Start*/
				//ApplicationPlatForm platForm = ApplicationPlatForm.valueOf(xPlatform);
				
				//WebServiceConfig config = ApiConfigUtil.getWebServiceConfigById(configId,platForm);
				
				request.setAttribute("authorized", true);
				
				/*if(config==null){
					request.setAttribute("authorized", false); 
				}else{
					String token = SecurityUtil.doDecryption(sign, config.getSecureKey());
					//System.out.println(token);
					if(token.equals(config.getToken())){
						request.setAttribute("authorized", true);
						
					}else{
						request.setAttribute("authorized", false);
					}
					
				}*/
				
				/* 11/27/2018 1:04PM - Commented By Ulaga - End*/
				
				/*Enumeration<String> headerNames = httpRequest.getHeaderNames();
				while(headerNames.hasMoreElements()) {
				  String headerName = headerNames.nextElement();
				  System.out.println("Header Name - " + headerName + ", Value - " + httpRequest.getHeader(headerName));
				}*/
				
				try {
					EcommWebServiceTracking track = new EcommWebServiceTracking();
					track.setApiUrl(httpRequest.getRequestURI());
					track.setCustomerId(httpRequest.getParameter("custId"));
					track.setIpAddress(httpRequest.getHeader("x-forwarded-for"));
					track.setPlatForm(xPlatform);
					EcommDAORegistry.getEcommWebServiceTrackingDAO().save(track);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("authorized", false);
			}
		}
		
		Boolean isAuthrozed = (Boolean)request.getAttribute("authorized");
		
		//System.out.println("Authorized ===>"+isAuthrozed);
		httpResponse.addHeader("Access-Control-Allow-Origin", "*");
		httpResponse.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		httpResponse.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,x-sign,x-token,x-platform");
		
		chain.doFilter(httpRequest, httpResponse);
	}

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
