package com.zonesws.webservices.jobs;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;

public class APIRunningUtil extends QuartzJobBean implements StatefulJob{
	 
	 
	public void init(){
		try {
			DAORegistry.getQueryManagerDAO().updateLastAPIRunTime();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	 
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}
}
