package com.zonesws.webservices.jobs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.AffiliateCashRewardHistory;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailManager;



public class ActivateAffiliateCash extends QuartzJobBean implements StatefulJob {

	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mmaa");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	private static Logger logger = LoggerFactory.getLogger(ActivateAffiliateCash.class);
	static MailManager mailManager = null;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		ActivateAffiliateCash.mailManager = mailManager;
	}


	public static void main(String[] args) throws Exception{
		
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		System.out.println("evenTDATE   :"+calendar.getTime());
		Date currentDate = new Date();
		Date eventDate = calendar.getTime();
		System.out.println("currentDate=========>"+currentDate);
		if(eventDate.after(currentDate)){
			System.out.println("Event Date is after current date");
		}
	}
	
	public static void activateAffiliateCash(){
		
		try{
			
			List<AffiliateCashRewardHistory> pendingCashRewards = DAORegistry.getQueryManagerDAO().getAllAffiliatePendingCash();
			Map<Integer, Boolean> emailSendMap = new HashMap<Integer, Boolean>();
			
			for (AffiliateCashRewardHistory obj : pendingCashRewards) {
				
				try{
					Date curDate = new Date();
					
					if(obj.getAffiliatePremiumStatus().equals("PLATINIUM")){
						Date orderDate = obj.getCreateDate();
						Calendar calendar = new GregorianCalendar();
						calendar.setTime(orderDate);
						calendar.add(Calendar.DAY_OF_MONTH, 7);
						orderDate = calendar.getTime();
						if(orderDate.after(curDate)){
							continue;
						}
					}else{
						String eventDateTime = obj.getEventDate();
						if(obj.getEventTime().equals("TBD")){
							eventDateTime = eventDateTime +" 12:00 AM";
						}else{
							eventDateTime = eventDateTime +" "+obj.getEventTime();
						}
						Date eventDate = dateTimeFormat.parse(eventDateTime);
						Calendar calendar = new GregorianCalendar();
						calendar.setTime(eventDate);
						calendar.add(Calendar.DAY_OF_MONTH, 1);
						eventDate = calendar.getTime();
						if(eventDate.after(curDate)){
							continue;
						}
					}
					
					System.out.println("ACTIVATEAFFILIATECASH : Reward ID:"+obj.getId()+", " +
							"Customer ID:"+obj.getCustomerId()+", Earned Cash :"+obj.getCreditedCash());
					DAORegistry.getAffiliateCashRewardDAO().updateAffiliateCashByCustomer(obj.getCreditedCash(), obj.getUserId());
					
					DAORegistry.getAffiliateCashRewardHistoryDAO().updateCashRewardHistoryToActive(obj.getId());
					
					/*CustomerLoyalty customerLoyalty=DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(obj.getCustomerId());
				
					boolean triggerEmail = false;
					String emailDesription="";
					//To send the email
					Customer customer = CustomerUtil.getCustomerById(pendingReward.getCustomerId());
					if(customer != null){
						try {
							
							Map<String,Object> mailMap = new HashMap<String,Object>();
							mailMap.put("pendingPoints",pendingReward.getPointsEarned());
							mailMap.put("activePoints",TicketUtil.getRoundedValueString(customerLoyalty.getActivePointsAsDouble()));
							mailMap.put("customerName",customer.getCustomerName()+" "+customer.getLastName());
							
							//inline(image in mail body) image add code
							MailAttachment[] mailAttachment = new MailAttachment[1];
							String filePath = URLUtil.getLogoImage();
							//byte[] fileContent = Util.convertFiletoByteArray(filePath, "png");
							mailAttachment[0] = new MailAttachment(null,"image/png","logo.png");
							
							
								mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
										null,"sales@rewardthefan.com,AODev@rightthisway.com", 
										"Reward The Fan: Your Active Rewards are "+TicketUtil.getRoundedValueString(customerLoyalty.getActivePointsAsDouble()),
							    		"email-customer-reward-information.html", mailMap, "text/html", null,mailAttachment,filePath);
								
								triggerEmail = true;
								emailDesription = "Rewards Activation Email Sent to "+customer.getEmail()+" this email. " +
										"Activated Points are "+pendingReward.getPointsEarned()+" and Total available points are "+customerLoyalty.getActivePoints();
						} catch (Exception e) {
							triggerEmail = false;
							emailDesription = "Error occurred while sending rewards activation email to "+customer.getEmail();
						}
					}*/
					
					}catch(Exception e){
						System.out.println("ACTIVATEAFFILIATECASH : Error While Activating this Reward Id:"+obj.getId()+", " +
								"Customer ID:"+obj.getCustomerId()+", Earned Points :"+obj.getCreditedCash());
						e.printStackTrace();
					}
				}	
				
		}catch(Exception e){
			System.out.println("ACTIVATEAFFILIATECASH : Error occured while runing this scheduler.");
			e.printStackTrace();
		}
	}


	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			//activateAffiliateCash();
		}
		
	}
	
}
