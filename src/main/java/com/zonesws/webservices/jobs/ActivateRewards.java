package com.zonesws.webservices.jobs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;



public class ActivateRewards extends QuartzJobBean implements StatefulJob {

	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mmaa");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	private static Logger logger = LoggerFactory.getLogger(ActivateRewards.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}

	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		ActivateRewards.mailManager = mailManager;
	}


	public static void main(String[] args) throws Exception{
		
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		System.out.println("evenTDATE   :"+calendar.getTime());
		Date currentDate = new Date();
		Date eventDate = calendar.getTime();
		System.out.println("currentDate=========>"+currentDate);
		if(eventDate.before(currentDate)){
			System.out.println("Event Date is before current date");
		}
	}
	
	public void activateRewardPoints(){
		
		try{
			
			List<CustomerLoyaltyHistory> pendingRewardList = DAORegistry.getCustomerLoyaltyHistoryDAO().getAllCustomerPendingRewards();
			Map<Integer, Boolean> emailSendMap = new HashMap<Integer, Boolean>();
			
			for (CustomerLoyaltyHistory pendingReward : pendingRewardList) {
				
				try{
					Date curDate = new Date();
					String eventDateTime = pendingReward.getEventDate();
					
					if(pendingReward.getEventTime().equals("TBD")){
						eventDateTime = eventDateTime +" 12:00 AM";
					}else{
						eventDateTime = eventDateTime +" "+pendingReward.getEventTime();
					}
					Date eventDate = dateTimeFormat.parse(eventDateTime);
					Calendar calendar = new GregorianCalendar();
					calendar.setTime(eventDate);
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					
					eventDate = calendar.getTime();
					
					if(eventDate.after(curDate)){
						continue;
					}
					
					if(pendingReward.getPointsEarnedAsDouble() == 0 || pendingReward.getPointsEarnedAsDouble() == 0.00){
						continue;
					}
					
					System.out.println("ACTIVATEREWARDS : Reward ID:"+pendingReward.getId()+", " +
							"Customer ID:"+pendingReward.getCustomerId()+", Earned Dollars :"+pendingReward.getPointsEarned());
					DAORegistry.getCustomerLoyaltyDAO().updateCustomerLoyaltyByCustomer(pendingReward.getPointsEarnedAsDouble(), pendingReward.getCustomerId());
					
					CustomerLoyalty customerLoyalty=DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(pendingReward.getCustomerId());
				
					boolean triggerEmail = false;
					String emailDesription="";
					//To send the email
					Customer customer = CustomerUtil.getCustomerById(pendingReward.getCustomerId());
					if(customer != null && customerLoyalty.getActivePointsAsDouble() > 0 ){
						try {
							
							Map<String,Object> mailMap = new HashMap<String,Object>();
							mailMap.put("pendingPoints",pendingReward.getPointsEarned());
							mailMap.put("activePoints",TicketUtil.getRoundedValueString(customerLoyalty.getActivePointsAsDouble()));
							mailMap.put("customerName",customer.getCustomerName());
							
							//inline(image in mail body) image add code
							MailAttachment[] mailAttachment = Util.getEmailAttachmentForRewardPoints();
							
								mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
										null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
										"You have $"+TicketUtil.getRoundedValueString(customerLoyalty.getActivePointsAsDouble())+" to spend at Reward The Fan!",
							    		"email-customer-reward-information.html", mailMap, "text/html", null,mailAttachment,null);
								
								triggerEmail = true;
								emailDesription = "Rewards Activation Email Sent to "+customer.getEmail()+" this email. " +
										"Activated Dollars are "+pendingReward.getPointsEarned()+" and Total available dollars are "+customerLoyalty.getActivePoints();
						} catch (Exception e) {
							triggerEmail = false;
							emailDesription = "Error occurred while sending rewards activation email to "+customer.getEmail();
						}
					}
					
					DAORegistry.getCustomerLoyaltyHistoryDAO().updateRewardHistoryToActive(pendingReward.getId(),triggerEmail,emailDesription);
					
					try{
						DAORegistry.getCustomerLoyaltyDAO().updateCustomerLoyaltyById(customerLoyalty.getId(), emailDesription);
					}catch(Exception e){
						e.printStackTrace();
					}
					
					List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(pendingReward.getCustomerId());
					if(deviceList != null && !deviceList.isEmpty() && deviceList.size() > 0){
						
						
						String message = "Congratulations! - Today "+pendingReward.getPointsEarned()+" Reward Dollars were converted from " +
								"pending status to available status. You now have a total of "+customerLoyalty.getActivePoints()+" dollars " +
								"accumulated in your account.Remember, you can use your reward dollars towards any future ticket " +
								"purchases with each dollar worth $1!. You can also use your reward dollars to secure FREE Fantasy Sports Tickets" +
								" - Fantasy Sports Tickets are FREE tickets to those once in a lifetime special events like the Super Bowl, " +
								"World Series or the NBA Finals. etc. Fantasy Sports Tickets gives you, THE FAN, the potential to see your " +
								"favorite team at the big game for FREE-simply by using and referring others to Reward The Fan!";
						
					
						String jsonString = "";
						for (CustomerDeviceDetails device : deviceList) {
							
							switch (device.getApplicationPlatForm()) {
							
							case ANDROID:
								
								NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
								notificationJsonObject.setNotificationType(NotificationType.MY_REWARD);
								notificationJsonObject.setMessage(message);
								notificationJsonObject.setCustomerId(pendingReward.getCustomerId());
								Gson gson = new Gson();	
								jsonString = gson.toJson(notificationJsonObject);
								
								//gcmNotificationService.sendMessage(NotificationType.REWARD_STATUS, device.getNotificationRegId(), jsonString);
								break;
								
							case IOS:
								Map<String, String> customFields = new HashMap<String, String>();
							    customFields.put("notificationType", String.valueOf(NotificationType.MY_REWARD));
							    customFields.put("customerId", String.valueOf(pendingReward.getCustomerId()));
								
							    //apnsNotificationService.sendNotification(NotificationType.REWARD_STATUS, device.getNotificationRegId(), message,customFields);
								break;
								
								default:
									break;
							}
						}
					}
						
					}catch(Exception e){
						System.out.println("ACTIVATEREWARDS : Error While Activating this Reward Id:"+pendingReward.getId()+", " +
								"Customer ID:"+pendingReward.getCustomerId()+", Earned Points :"+pendingReward.getPointsEarned());
						e.printStackTrace();
					}
				}	
				
		}catch(Exception e){
			System.out.println("ACTIVATEREWARDS : Error occured while runing this scheduler.");
			e.printStackTrace();
		}
	}


	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			activateRewardPoints();
		}
	}
	
}
