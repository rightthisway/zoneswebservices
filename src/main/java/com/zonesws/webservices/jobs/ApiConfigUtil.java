package com.zonesws.webservices.jobs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.IpConfig;
import com.zonesws.webservices.data.WebServiceConfig;
import com.zonesws.webservices.enums.ApplicationPlatForm;

/**
 * 
 * @author dthiyagarajan
 *
 */
public class ApiConfigUtil extends QuartzJobBean implements StatefulJob{
	
	private static Map<String, WebServiceConfig> webServiceConfigMapById = new ConcurrentHashMap<String, WebServiceConfig>();
	private static Map<String, IpConfig> ipConfigMapByIpAndConfigId = new ConcurrentHashMap<String, IpConfig>();
	private static Map<String, WebServiceConfig> webServiceConfigMapByIdPlatform = new ConcurrentHashMap<String, WebServiceConfig>();
	
	
	
	
	/**
	 * Method to return webservice config by config id
	 * @param configId
	 * @return
	 */
	public static WebServiceConfig getWebServiceConfigById(String configId){
		WebServiceConfig webServiceConfig = webServiceConfigMapById.get(configId);
		try {
			if(webServiceConfig == null){
				webServiceConfig = DAORegistry.getWebServiceConfigDAO().getWebServiceConfgiByConfigId(configId);
				if(webServiceConfig != null){
					webServiceConfigMapById.put(configId, webServiceConfig);
				}
			}
			return webServiceConfig;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method to return webservice config by config id
	 * @param configId
	 * @return
	 */
	public static WebServiceConfig getWebServiceConfigById(String configId,ApplicationPlatForm platForm){
		WebServiceConfig webServiceConfig = webServiceConfigMapByIdPlatform.get(configId+"_"+String.valueOf(platForm));
		try {
			if(webServiceConfig == null){
				webServiceConfig = DAORegistry.getWebServiceConfigDAO().getWebServiceConfgiByConfigId(configId,platForm);
				if(webServiceConfig != null){
					webServiceConfigMapByIdPlatform.put(configId+"_"+String.valueOf(platForm), webServiceConfig);
				}
			}
			return webServiceConfig;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method to get the config id 
	 * for the given  params
	 * @param configId
	 * @param ipAddr
	 * @return
	 */
	public static IpConfig getIpConfigByIpAndConfigId(String configId, String ipAddr){
		IpConfig ipConfig = ipConfigMapByIpAndConfigId.get(configId+":"+ipAddr);
		try {
			if(ipConfig == null){
				ipConfig = DAORegistry.getIpConfigDAO().getIpConfigByIpAndConfigId(configId,ipAddr);
				if(null != ipConfig){
					ipConfigMapByIpAndConfigId.put(configId+":"+ipAddr, ipConfig);
				}
			}
			return ipConfig;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void init(){
		try {
			Long startTime = System.currentTimeMillis();
			
			Collection<WebServiceConfig> webServiceConfigList = DAORegistry.getWebServiceConfigDAO().getWebServiceConfig();
			Map<String, WebServiceConfig> tempWebServiceConfig = new ConcurrentHashMap<String, WebServiceConfig>();
			Map<String, WebServiceConfig> tempWebServiceConfigbyPlatform = new ConcurrentHashMap<String, WebServiceConfig>();
			
			for(WebServiceConfig webServiceConfig : webServiceConfigList){
				tempWebServiceConfig.put(webServiceConfig.getConfigId(), webServiceConfig);
				tempWebServiceConfigbyPlatform.put(webServiceConfig.getConfigId()+"_"+webServiceConfig.getPlatForm(), webServiceConfig);
			}
			
			webServiceConfigMapById.clear();
			webServiceConfigMapById = null;
			webServiceConfigMapById = new HashMap<String, WebServiceConfig>(tempWebServiceConfig);
			
			webServiceConfigMapByIdPlatform.clear();
			webServiceConfigMapByIdPlatform = null;
			webServiceConfigMapByIdPlatform = new HashMap<String, WebServiceConfig>(tempWebServiceConfigbyPlatform);
			
			Collection<IpConfig> ipConfigList = DAORegistry.getIpConfigDAO().ipConfigList();
			Map<String, IpConfig> tempIpConfigMap = new ConcurrentHashMap<String, IpConfig>();
			
			for(IpConfig ipConfig : ipConfigList){
				tempIpConfigMap.put(ipConfig.getWebServiceConfig().getConfigId()+":"+ipConfig.getIpAddress(), ipConfig);
			}
			
			ipConfigMapByIpAndConfigId.clear();
			ipConfigMapByIpAndConfigId = null;
			ipConfigMapByIpAndConfigId = new HashMap<String, IpConfig>(tempIpConfigMap);
			
			System.out.println("TIME TO LAST API CONFIG UTIlS=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}

}
