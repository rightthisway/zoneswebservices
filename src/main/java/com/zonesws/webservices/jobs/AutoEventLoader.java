package com.zonesws.webservices.jobs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.EventDTO;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.data.ParentCategory;
import com.zonesws.webservices.data.Venue;
import com.zonesws.webservices.enums.ArtistStatus;
import com.zonesws.webservices.enums.Status;



public class AutoEventLoader extends QuartzJobBean implements StatefulJob {

	/**
	 * @param args
	 */
	private boolean running;
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		System.out.println("Auto Event Loader Started at "+ new Date());
		try{
			if(!running){
				running =true;
				//addEvents();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		running =false;
		
		System.out.println("Auto Event Loader Ended at "+ new Date());
	}
	/*
	@SuppressWarnings("deprecation")
	public void addEvents() {
		try{
		
			// fetch all events from zones db
			Map<Integer,Event> eventMap = new HashMap<Integer,Event>();
			Collection<Event> events = DAORegistry.getEventDAO().getAll();
			if(events != null && !events.isEmpty()){
				for(Event event : events){
					eventMap.put(event.getId(), event);
				}
			}
			
			// fetching zones artist,parent,child,grandchild,venue
			Map<Integer, Artist> artistMap = new HashMap<Integer, Artist>();
			Collection<Artist> artists = DAORegistry.getArtistDAO().getAll();
			System.out.println("Zones artist size is  "+artists.size());
			if(artists != null && !artists.isEmpty()){
				for(Artist artist:artists){
					artistMap.put(artist.getId(), artist);
				}
			}
			
			Collection<Venue> venues=DAORegistry.getVenueDAO().getAll();
			Map<Integer, Venue> venueMap = new HashMap<Integer, Venue>();
			if(venues != null && !venues.isEmpty()){
				for(Venue venue:venues){
						venueMap.put(venue.getId(), venue);
				}
			}
			
			Collection<GrandChildCategory> grandChildCategories=DAORegistry.getGrandChildCategoryDAO().getAll();
			Map<Integer, GrandChildCategory> grandChildCategoryMap = new HashMap<Integer, GrandChildCategory>();
			if(grandChildCategories != null && !grandChildCategories.isEmpty()){
				for(GrandChildCategory grandChildCategory:grandChildCategories){
					grandChildCategoryMap.put(grandChildCategory.getId(), grandChildCategory);
				}
			}
			
			Collection<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getAll();
			Map<Integer, ChildCategory> childCategoryMap = new HashMap<Integer, ChildCategory>();
			if(childCategories != null && !childCategories.isEmpty()){
				for(ChildCategory childCategory:childCategories){
					childCategoryMap.put(childCategory.getId(), childCategory);
				}
			}
			
			Collection<ParentCategory> parentCategories = DAORegistry.getParentCategoryDAO().getAll();
			Map<Integer, ParentCategory> parentCategoriesMap = new HashMap<Integer, ParentCategory>();
			if(parentCategories != null && !parentCategories.isEmpty()){
				for(ParentCategory parentCategory:parentCategories){
					parentCategoriesMap.put(parentCategory.getId(), parentCategory);
				}
			}
			
		String srcFileLoc = "\\\\C:\\ZoneEvents.csv";
		File srcFile = new File(srcFileLoc);
		BufferedReader bufRdr = new BufferedReader(new FileReader(srcFile));
		//System.out.println(bufRdr.readLine());
		
		String line = null;
		Integer i=0;
		Integer noMapEventsCount = 0;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		Boolean isDiscountZone;
		
		List<Event> eventsToAdd = new ArrayList<Event>();
		List<Event> eventsToEdit = new ArrayList<Event>();
		List<Event> eventsToRemove = new ArrayList<Event>();
		
		List<Event> eventsList = new ArrayList<Event>();
		//StringBuffer mapsBuffer = new StringBuffer();
		while ((line = bufRdr.readLine()) != null ) {
			
			if(null == line || line.isEmpty()){
				break;
			}
			
			String[] rowValues = StringUtils.stripAll(line.split(","));
			
			if(rowValues.length == 0){
				break;
			}
			
			i++;
			
			if(i==1){
				continue;
			}
			
			if(i==4){
				break;
			}
			
			//System.out.println(rowValues[1]);
			//break;
			EventDTO eventDTO = new EventDTO();
			eventDTO.setId(Integer.parseInt(rowValues[0]));
			eventDTO.setName(rowValues[1]);
			if(!rowValues[2].equalsIgnoreCase("tbd")){
			eventDTO.setEventDate(new Date(rowValues[2]));
			}else{
				eventDTO.setEventDate(null);
			}
			if(!rowValues[3].equalsIgnoreCase("tbd")){
			eventDTO.setEventTime(timeFormat.parse(rowValues[3]));
			}else{
				eventDTO.setEventTime(null);
			}
			eventDTO.setVenueId(Integer.parseInt(rowValues[4]));
			eventDTO.setVenueName(rowValues[5]);
			eventDTO.setCity(rowValues[6]);
			eventDTO.setState(rowValues[7]);
			eventDTO.setCountry(rowValues[8]);
			eventDTO.setPostalCode(rowValues[9]);
			eventDTO.setArtistId(Integer.parseInt(rowValues[10]));
			eventDTO.setArtistName(rowValues[11]);
			eventDTO.setParentCategoryId(Integer.parseInt(rowValues[12]));
			eventDTO.setParentCategoryName(rowValues[13]);
			eventDTO.setChildCategoryId(Integer.parseInt(rowValues[14]));
			eventDTO.setChildCategoryName(rowValues[15]);
			eventDTO.setGrandChildCategoryId(Integer.parseInt(rowValues[16]));
			eventDTO.setGrandChildCategoryName(rowValues[17]);
			eventDTO.setVenueCategoryId(Integer.parseInt(rowValues[18]));
			eventDTO.setVenueCategoryName(rowValues[19]);
			eventDTO.setProcess(rowValues[20]);
			eventDTO.setIsDiscountZone(rowValues[21]);
			
			String path = "\\\\S:\\TMATIMAGESFINAL\\"+eventDTO.getVenueId()+"_"+eventDTO.getVenueCategoryName();
			// \\\\admit1\\Shared\\TMATIMAGESFINAL\\
			File gifFilePath = new File(path+".gif");
			File swfFilePath = new File(path+".swf");

			// if map not exist then skip that event
			if(!gifFilePath.exists()){// || !swfFilePath.exists()){
				//event.setEventStatus(Status.DELETED);
				noMapEventsCount++;
				//mapsBuffer.append(eventDTO.getId() + "=" + eventDTO.getVenueId()+"_"+eventDTO.getVenueCategoryName() + "\n");
				continue;
			}
			
			Event event = eventMap.get(eventDTO.getId());
			
			if(event == null){
				event = new Event(eventDTO);
				event.setEventCreationDate(new java.sql.Timestamp(new Date().getTime()));
			}else{
				event.setName(eventDTO.getName());
				if(eventDTO.getEventDate() != null){
					event.setEventDate(new java.sql.Date(eventDTO.getEventDate().getTime()));
				}else{
					event.setEventDate(null);
				}
				if(eventDTO.getEventTime() != null){
					event.setTime(new java.sql.Time(eventDTO.getEventTime().getTime()));
				}else{
					event.setTime(null);
				}
				event.setId(eventDTO.getId());
				event.setVenueCategoryId(eventDTO.getVenueCategoryId());
				event.setVenueCategoryName(eventDTO.getVenueCategoryName());
			}
			
			if(eventDTO.getProcess().equalsIgnoreCase("a") || eventDTO.getProcess().equalsIgnoreCase("e")){
				event.setEventStatus(Status.ACTIVE);
			}else if(eventDTO.getProcess().equalsIgnoreCase("r")){
				event.setEventStatus(Status.DELETED);
			}
			
			if(eventDTO.getIsDiscountZone() == null || eventDTO.getIsDiscountZone().equals("") || eventDTO.getIsDiscountZone().equals("0")){
				isDiscountZone = false;
			}else if(eventDTO.getIsDiscountZone().equals("1")){
				isDiscountZone = true;
			}else{
				isDiscountZone = false;
			}
			event.setDiscountZoneFlag(isDiscountZone);
			Artist artist = artistMap.get(eventDTO.getArtistId());
			if(artist == null){
				
				ParentCategory parentCategory = parentCategoriesMap.get(eventDTO.getParentCategoryId());
				if(parentCategory == null){
					parentCategory = new ParentCategory();
					parentCategory.setName(eventDTO.getParentCategoryName());
					parentCategory.setId(eventDTO.getParentCategoryId());
					parentCategoriesMap.put(parentCategory.getId(), parentCategory);
					DAORegistry.getParentCategoryDAO().saveOrUpdate(parentCategory);
				}
				
				ChildCategory childCategory = childCategoryMap.get(eventDTO.getChildCategoryId());
				if(childCategory == null){
					childCategory = new ChildCategory();
					childCategory.setName(eventDTO.getChildCategoryName());
					childCategory.setParentCategory(parentCategory);
					childCategory.setId(eventDTO.getChildCategoryId());
					childCategoryMap.put(childCategory.getId(), childCategory);
					DAORegistry.getChildCategoryDAO().saveOrUpdate(childCategory);
				}
				
				GrandChildCategory grandChildCategory = grandChildCategoryMap.get(eventDTO.getGrandChildCategoryId());
				if(grandChildCategory == null){
					grandChildCategory = new GrandChildCategory();
					grandChildCategory.setName(eventDTO.getGrandChildCategoryName());
					grandChildCategory.setChildCatId(childCategory.getId());
					grandChildCategory.setId(eventDTO.getGrandChildCategoryId());
					DAORegistry.getGrandChildCategoryDAO().saveOrUpdate(grandChildCategory);
					grandChildCategoryMap.put(grandChildCategory.getId(), grandChildCategory);
				}
				artist = new Artist();
				artist.setArtistStatus(ArtistStatus.ACTIVE);
				artist.setName(eventDTO.getArtistName());
				artist.setId(eventDTO.getArtistId());
				artist.setGrandChildCategory(grandChildCategory);
				DAORegistry.getArtistDAO().saveOrUpdate(artist);
				artistMap.put(artist.getId(), artist);
				
			}
			event.setArtist(artist);
			Venue venue = venueMap.get(eventDTO.getVenueId());
			if(venue == null){
				venue = new Venue();
				venue.setBuilding(eventDTO.getVenueName());
				venue.setCity(eventDTO.getCity());
				venue.setCountry(eventDTO.getCountry());
				venue.setId(eventDTO.getVenueId());
				venue.setState(eventDTO.getState());
				venue.setPostalCode(eventDTO.getPostalCode());
				venueMap.put(venue.getId(), venue);
				DAORegistry.getVenueDAO().saveOrUpdate(venue);
			}
			event.setVenue(venue);
			
			event.setEventLastUpdateDate(new java.sql.Timestamp(new Date().getTime()));
			eventsList.add(event);
			File noMapsFile = new File("C:\\Users\\pmehul\\Desktop\\noMapsEvents.txt");
			FileWriter fw = new FileWriter(noMapsFile);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(mapsBuffer.toString());
			bw.flush();
			bw.close();
		}
			bufRdr.close();
			DAORegistry.getEventDAO().saveOrUpdateAll(eventsList);
			
			System.out.println("Events without map are -> "+noMapEventsCount);
		}catch(Exception e){
			e.printStackTrace();
		}
}*/

}
