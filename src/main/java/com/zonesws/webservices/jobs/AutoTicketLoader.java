package com.zonesws.webservices.jobs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.TicketDTO;
import com.zonesws.webservices.data.ZoneTicketGroup;
import com.zonesws.webservices.enums.Status;



public class AutoTicketLoader extends QuartzJobBean implements StatefulJob {

	private boolean running;
	
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		System.out.println("AutoTicketLoader started at "+new Date());
		try{
			if(!running){
				running =true;
				//addTickets();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		running =false;
		System.out.println("AutoTicketLoader ended at "+new Date());		
	}
	
	/*public void addTickets() {
		try{
			Collection<Event> events = ZonesDAORegistry.getEventDAO().getAll();
			Map<Integer , Event> eventsMap = new HashMap<Integer , Event>();
			for(Event event : events){
				eventsMap.put(event.getTmatEventId(), event);
			}
			
			String srcFileLoc = "\\\\C:\\TicketsCsv-1.csv";
			File srcFile = new File(srcFileLoc);
			BufferedReader bufRdr = new BufferedReader(new FileReader(srcFile));
			Integer i = 0;
			String line = null;
			List<TicketDTO> ticketsList = new ArrayList<TicketDTO>();
			List<ZoneTicketGroup> ticketsListToUpdate = new ArrayList<ZoneTicketGroup>();
			List<Integer> tmatEventIds = new ArrayList<Integer>();
			List<Integer> zonesEventIds = new ArrayList<Integer>();
			
			while ((line = bufRdr.readLine()) != null ) {
				
				if(null == line || line.isEmpty()){
					break;
				}
				
				String[] rowValues = StringUtils.stripAll(line.split(","));
				
				if(rowValues.length == 0){
					break;
				}
				
				i++;
				
				if(i==1){
					continue;
				}
				
				if(i == 5){
					break;
				}
				
				TicketDTO ticketDTO = new TicketDTO();
				ticketDTO.setTicketGroupId(Integer.parseInt(rowValues[0]));
				ticketDTO.setTmatEventId(Integer.parseInt(rowValues[1]));
				ticketDTO.setSection(rowValues[2]);
				ticketDTO.setQuantity(Integer.parseInt(rowValues[3]));
				ticketDTO.setPriority(Integer.parseInt(rowValues[4]));
				ticketDTO.setPrice(Double.parseDouble(rowValues[5]));
				ticketDTO.setRowRange(rowValues[6]);
				ticketDTO.setSectionRange(rowValues[7]);
				ticketDTO.setShippingMethod(rowValues[8]);
				ticketDTO.setProcess(rowValues[9]);
				
				ticketsList.add(ticketDTO);
				
				if(!tmatEventIds.contains(ticketDTO.getTmatEventId())){
					tmatEventIds.add(ticketDTO.getTmatEventId());
				}
				//System.out.println(i);
			}
			bufRdr.close();
			System.out.println("Reading tickets csv completed");
			Collection<Event> events = DAORegistry.getEventDAO().getAll();//getAllEventsByTMATEventIds(tmatEventIds);
			Map<Integer , Event> eventsMap = new HashMap<Integer , Event>();
			if(events != null){	
				for(Event event : events){
					eventsMap.put(event.getId(), event);
					zonesEventIds.add(event.getId());
				}
			}
			Collection<Ticket> tickets = ZonesDAORegistry.getTicketDAO().getAll();//.getAllTicketsByEventIds(zonesEventIds);
			Map<Integer , Ticket> ticketsMap = new HashMap<Integer , Ticket>();
			if(tickets != null){
				for(Ticket ticket : tickets){
					ticketsMap.put(ticket.getTicketGroupId(), ticket);
				}
			}
			
			List<ZoneTicketGroup> tickets = new ArrayList<ZoneTicketGroup>();//.getAllTicketsByEventIds(zonesEventIds);
			for(Integer tmatEventId : tmatEventIds){
				List<ZoneTicketGroup> tempTickets = DAORegistry.getZoneTicketGroupDAO().getAllTicketsByTmatEventId(tmatEventId);
				if(tempTickets != null){
					tickets.addAll(tempTickets);
				}
			}
			Map<Integer , ZoneTicketGroup> ticketsMap = new HashMap<Integer , ZoneTicketGroup>();
			if(tickets != null){
				for(ZoneTicketGroup ticket : tickets){
					ticketsMap.put(ticket.getId(), ticket);
				}
			}
			
			Integer count = 0;
			Integer skippedTickets = 0;
			StringBuffer skippedTicketsBuffer = new StringBuffer();
			for(TicketDTO ticketDTO : ticketsList){
				// we will skip those tickets whose event is either not in zonesplatform db or whose event's status is not active
				if(eventsMap.get(ticketDTO.getTmatEventId()) == null || !eventsMap.get(ticketDTO.getTmatEventId()).getEventStatus().equals(Status.ACTIVE)){
					skippedTicketsBuffer.append(ticketDTO.getTicketGroupId() + "-" + ticketDTO.getTmatEventId()+"\n");
					skippedTickets++;
					continue;
				}
				ZoneTicketGroup ticket  = ticketsMap.get(ticketDTO.getTicketGroupId());
				if(ticket == null){
					ticket = new ZoneTicketGroup();
				}
				ticket.setEventId(ticketDTO.getTmatEventId());
				ticket.setZonePrice(ticketDTO.getPrice().intValue());
				ticket.setPriority(ticketDTO.getPriority());
				ticket.setQuantity(ticketDTO.getQuantity());
				ticket.setRowRange(ticketDTO.getRowRange());
				ticket.setZone(ticketDTO.getSection());
				ticket.setSectionRange(ticketDTO.getSectionRange());
				ticket.setEventId(ticketDTO.getTmatEventId());
				ticket.setId(ticketDTO.getTicketGroupId());
				ticket.setShippingMethod(ticketDTO.getShippingMethod());
				
				if(ticketDTO.getProcess().equalsIgnoreCase("a") || ticketDTO.getProcess().equalsIgnoreCase("e")){
					ticket.setStatus(Status.ACTIVE);
				}else if(ticketDTO.getProcess().equalsIgnoreCase("r")){
					ticket.setStatus(Status.DELETED);
				}
				
				if(ticket.getZonePrice() <= 0){
					ticket.setStatus(Status.DELETED);
				}
				
				ticketsListToUpdate.add(ticket);
				count++;
				//System.out.println(count);
			}
			//System.out.println("reading csv completed...");
			System.out.println("Total tickets skipped "+skippedTickets);
			File noMapsFile = new File("C:\\SkippedTicketsDetails.txt");
			FileWriter fw = new FileWriter(noMapsFile);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(skippedTicketsBuffer.toString());
			bw.flush();
			bw.close();
			DAORegistry.getZoneTicketGroupDAO().saveOrUpdateAll(ticketsListToUpdate);
		}catch(Exception e){
			e.printStackTrace();
		}

	}*/
}
