package com.zonesws.webservices.jobs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.AutoCatsLockedTickets;
import com.zonesws.webservices.data.PreOrderPageEmailTracking;
import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.enums.LockedTicketStatus;

public class CleanUnwantedHistory extends QuartzJobBean implements StatefulJob{
	
	
	
	protected void cleanLockedTickets(){
		List<AutoCatsLockedTickets> list = DAORegistry.getAutoCatsLockedTicketDAO().getAllLockedTicket();
		Calendar apiCal= Calendar.getInstance();
		apiCal.add(Calendar.MINUTE, - 6);
		Date apiNow=apiCal.getTime(); 
		Long apiNowValue= apiNow.getTime();
		List<AutoCatsLockedTickets> tobeUpdatedTickets=new ArrayList<AutoCatsLockedTickets>();
		try{
			for(AutoCatsLockedTickets ticket:list){
				if(apiNowValue-ticket.getCreationDate().getTime() >= 0){
					ticket.setLockStatus(LockedTicketStatus.DELETED);
					tobeUpdatedTickets.add(ticket);
				}
			}
			if(tobeUpdatedTickets.size()>0){
				DAORegistry.getAutoCatsLockedTicketDAO().updateAll(tobeUpdatedTickets);
			}
			list=null;
		}catch (Exception e) {
			System.out.println(e.fillInStackTrace());
		}
		
	}
	
	
	protected void cleanPromotionalPendingDatas(){
		Calendar apiCal= Calendar.getInstance();
		apiCal.add(Calendar.MINUTE, - 10);
		Date apiNow=apiCal.getTime(); 
		Long apiNowValue= apiNow.getTime();
		List<RTFPromotionalOfferTracking> trackingsList = DAORegistry.getRtfPromotionalOfferTrackingDAO().getAllPendingPromoTracking();
		List<RTFPromotionalOfferTracking> tobeUpdatedTickets=new ArrayList<RTFPromotionalOfferTracking>();
		try{
			
			for(RTFPromotionalOfferTracking tracking: trackingsList){
				if(apiNowValue-tracking.getCreatedDate().getTime() >= 0){
					tracking.setStatus("CANCELLED");
					tobeUpdatedTickets.add(tracking);
				}
			}
			if(tobeUpdatedTickets.size()>0){
				DAORegistry.getRtfPromotionalOfferTrackingDAO().updateAll(tobeUpdatedTickets);
			}
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}
	
	protected void cleanCheckoutOfferAppliedDatas(){
		Calendar apiCal= Calendar.getInstance();
		apiCal.add(Calendar.MINUTE, - 10);
		Date apiNow=apiCal.getTime(); 
		Long apiNowValue= apiNow.getTime();
		Collection<PreOrderPageEmailTracking> trackingsList = DAORegistry.getPreOrderPageEmailTrackingDAO().getAll();
		List<PreOrderPageEmailTracking> tobeDeletedTickets=new ArrayList<PreOrderPageEmailTracking>();
		try{
			Date curDate = new Date();
			Calendar calendar = new GregorianCalendar();
			for(PreOrderPageEmailTracking tracking: trackingsList){
				calendar.setTime(tracking.getEmailSentDate());
				calendar.add(Calendar.DAY_OF_MONTH, tracking.getOfferExpiryDays());
				if(curDate.after(calendar.getTime())){
					tobeDeletedTickets.add(tracking);
				}
			}
			if(tobeDeletedTickets.size()>0){
				DAORegistry.getPreOrderPageEmailTrackingDAO().deleteAll(tobeDeletedTickets);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		cleanCheckoutOfferAppliedDatas();
		//cleanPromotionalPendingDatas();
	}
}
