package com.zonesws.webservices.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerDeviceDetails;

/**
 * 
 * @author kulaganathan
 *
 */
public class CustomerDeviceDetailsUtils extends QuartzJobBean implements StatefulJob{
	
	private static Map<Integer, List<CustomerDeviceDetails>> deviceDetailsMap = new ConcurrentHashMap<Integer, List<CustomerDeviceDetails>>();
	private static Map<Integer, List<String>> notificationTokenMap = new ConcurrentHashMap<Integer, List<String>>();
	
	
	public static void updatedCustomerDeviceDetailsToUtil(CustomerDeviceDetails deviceDetails){
		try{
					
			if(deviceDetails.getCustomerId()  != null) {
				
				List<CustomerDeviceDetails> tempList = deviceDetailsMap.get(deviceDetails.getCustomerId());
				List<String> tempTokenList = notificationTokenMap.get(deviceDetails.getCustomerId());
				
				if(null != tempList && !tempList.isEmpty()){
					deviceDetailsMap.get(deviceDetails.getCustomerId()).add(deviceDetails);
				}else{
					tempList = new ArrayList<CustomerDeviceDetails>();
					tempList.add(deviceDetails);
					deviceDetailsMap.put(deviceDetails.getCustomerId(), tempList);
				}
				
				if(null != tempTokenList && !tempTokenList.isEmpty()){
					notificationTokenMap.get(deviceDetails.getCustomerId()).add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
				}else{
					tempTokenList = new ArrayList<String>();
					tempTokenList.add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
					notificationTokenMap.put(deviceDetails.getCustomerId(), tempTokenList);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static List<CustomerDeviceDetails> getAllActiveDevicesByCustomerId(Integer customerId){
		List<CustomerDeviceDetails> custDeviceList = deviceDetailsMap.get(customerId);
		try{
			if(custDeviceList != null && !custDeviceList.isEmpty()){
				return custDeviceList;
			}else{
				
				List<CustomerDeviceDetails> deviceList = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveDeviceDetailsByCustomerId(customerId);
				
				for(CustomerDeviceDetails deviceDetails : deviceList){
					
					List<CustomerDeviceDetails> tempList = deviceDetailsMap.get(deviceDetails.getCustomerId());
					List<String> tempTokenList = notificationTokenMap.get(deviceDetails.getCustomerId());
					
					if(null != tempList && !tempList.isEmpty()){
						deviceDetailsMap.get(deviceDetails.getCustomerId()).add(deviceDetails);
					}else{
						tempList = new ArrayList<CustomerDeviceDetails>();
						tempList.add(deviceDetails);
						deviceDetailsMap.put(deviceDetails.getCustomerId(), tempList);
					}
					
					if(null != tempTokenList && !tempTokenList.isEmpty()){
						notificationTokenMap.get(deviceDetails.getCustomerId()).add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
					}else{
						tempTokenList = new ArrayList<String>();
						tempTokenList.add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
						notificationTokenMap.put(deviceDetails.getCustomerId(), tempTokenList);
					}
				}
				return deviceList;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<String> getAllActiveTokensByCustomerId(Integer customerId){
		List<String> tokenList = notificationTokenMap.get(customerId);
		try{
			if(tokenList != null && !tokenList.isEmpty()){
				return tokenList;
			}else{
				
				List<CustomerDeviceDetails> deviceList = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveDeviceDetailsByCustomerId(customerId);
				
				for(CustomerDeviceDetails deviceDetails : deviceList){
					
					List<CustomerDeviceDetails> tempList = deviceDetailsMap.get(deviceDetails.getCustomerId());
					List<String> tempTokenList = notificationTokenMap.get(deviceDetails.getCustomerId());
					
					if(null != tempList && !tempList.isEmpty()){
						deviceDetailsMap.get(deviceDetails.getCustomerId()).add(deviceDetails);
					}else{
						tempList = new ArrayList<CustomerDeviceDetails>();
						tempList.add(deviceDetails);
						deviceDetailsMap.put(deviceDetails.getCustomerId(), tempList);
					}
					
					if(null != tempTokenList && !tempTokenList.isEmpty()){
						notificationTokenMap.get(deviceDetails.getCustomerId()).add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
					}else{
						tempTokenList = new ArrayList<String>();
						tempTokenList.add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
						notificationTokenMap.put(deviceDetails.getCustomerId(), tempTokenList);
					}
				}
				return notificationTokenMap.get(customerId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
		
	
	/**
	 * Method to initiate the scheduler process
	 */
	public static void init(){
		
		try {
			Long startTime = System.currentTimeMillis();
			
			//Map to persist the CustomerDeviceDetails info into cache
			Map<Integer, List<CustomerDeviceDetails>> tempDeviceDetailsMap = new ConcurrentHashMap<Integer, List<CustomerDeviceDetails>>();
			Map<Integer, List<String>> tempNotificationTokenMap = new ConcurrentHashMap<Integer, List<String>>();
			
			
			//Collection of CustomerDeviceDetails
			List<CustomerDeviceDetails> deviceList = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveDeviceDetails();
			
			for(CustomerDeviceDetails deviceDetails : deviceList){
				
				if(deviceDetails.getCustomerId() != null) {
					
					List<CustomerDeviceDetails> tempList = tempDeviceDetailsMap.get(deviceDetails.getCustomerId());
					List<String> tempTokenList = tempNotificationTokenMap.get(deviceDetails.getCustomerId());
					
					if(null != tempList && !tempList.isEmpty()){
						tempDeviceDetailsMap.get(deviceDetails.getCustomerId()).add(deviceDetails);
					}else{
						tempList = new ArrayList<CustomerDeviceDetails>();
						tempList.add(deviceDetails);
						tempDeviceDetailsMap.put(deviceDetails.getCustomerId(), tempList);
					}
					
					if(null != tempTokenList && !tempTokenList.isEmpty()){
						tempNotificationTokenMap.get(deviceDetails.getCustomerId()).add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
					}else{
						tempTokenList = new ArrayList<String>();
						tempTokenList.add(deviceDetails.getApplicationPlatForm()+"-::-"+deviceDetails.getNotificationRegId());
						tempNotificationTokenMap.put(deviceDetails.getCustomerId(), tempTokenList);
					}
				}
			}
			
			deviceDetailsMap.clear();
			deviceDetailsMap = null;
			deviceDetailsMap = new HashMap<Integer, List<CustomerDeviceDetails>>(tempDeviceDetailsMap);
			
			notificationTokenMap.clear();
			notificationTokenMap = null;
			notificationTokenMap = new HashMap<Integer, List<String>>(tempNotificationTokenMap);
			
			System.out.println("TIME TO LAST CUSTOMER DEVICE DETAILS UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}

}
