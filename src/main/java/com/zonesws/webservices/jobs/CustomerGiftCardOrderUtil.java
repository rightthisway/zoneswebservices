package com.zonesws.webservices.jobs;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.RtfGiftCard;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.data.RtfGiftCardQuantity;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Customer Gift Card Order Util to send
 * email for successful order
 * @author kulaganathan
 *
 */
public class CustomerGiftCardOrderUtil extends QuartzJobBean implements StatefulJob{

	private static Logger logger = LoggerFactory.getLogger(CustomerGiftCardOrderUtil.class);
	
	static MailManager mailManager = null;;
	private ZonesProperty properties;
	public static boolean isRunning = false;
	

	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		CustomerGiftCardOrderUtil.mailManager = mailManager;
	}
	
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}

	public void init() throws Exception {
		
		if(isRunning) {
			return;
		}
		isRunning = true;
		
		try {
			SimpleDateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			logger.debug("Initiating the email scheduler process for customer gift card orders....");
			Collection<RtfGiftCardOrder> customerOrders = DAORegistry.getRtfGiftCardOrderDAO().getCustomerOrderToSendEmail();
			
			Integer custId = 0;
			boolean isEmailSent = true;
			for(RtfGiftCardOrder customerOrder : customerOrders){
				custId = customerOrder.getCustomerId();
				isEmailSent = customerOrder.getIsEmailSent();
				String mailTemplate = "mail-gift-card-order-confirmation.html";
				
				Customer customer = DAORegistry.getCustomerDAO().get(custId);
				
				if(isEmailSent == false && customer != null){
					
					CustomerLoyalty loyaltyInfo = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					
					RtfGiftCardQuantity cardValueObj = DAORegistry.getRtfGiftCardQuantityDAO().get(customerOrder.getCardId());
					
					RtfGiftCard card = DAORegistry.getRtfGiftCardDAO().get(cardValueObj.getCardId());
					
					Map<String,Object> mailMap = new HashMap<String,Object>();
					Double rewardRedeemed = customerOrder.getRedeemedRewards();
					String customerName = "";
					customerName = customer.getCustomerName()+" "+customer.getLastName();
					 
					mailMap.put("customerName", customerName);
					mailMap.put("orderNo", customerOrder.getId());
					mailMap.put("orderDate", dateTimeFormat1.format(customerOrder.getCreatedDate()));
					mailMap.put("giftCardName", card.getTitle());
					mailMap.put("giftCardValue",cardValueObj.getAmount());
					mailMap.put("giftCardQty",customerOrder.getQuantity());
					mailMap.put("deliveryInfo",TicketUtil.getGiftCardDeliveryInFo());
					if(customerOrder.getOrderType().equals(OrderType.CONTEST)) {
						rewardRedeemed = 0.00;
					}
					mailMap.put("redeemedRewards", TicketUtil.getRoundedValueString(rewardRedeemed));
					
					String email=customer.getEmail();
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getOrderConfirmationEmailAttachmentImages();
					try {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
								null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
								"Your RewardTheFan Gift Card Order.",
								mailTemplate, mailMap, "text/html", null,mailAttachment,null);
						System.out.println("GIFT CARD ORDER EMAIL BEGINS : "+new Date());
						DAORegistry.getRtfGiftCardOrderDAO().updateOrderEmailSent(customerOrder.getId());
						System.out.println("GIFT CARD ORDER EMAIL ENDS : "+new Date());
					
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		isRunning = false;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				Thread.sleep(1000);
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
