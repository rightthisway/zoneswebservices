package com.zonesws.webservices.jobs;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.AffiliateCashReward;
import com.zonesws.webservices.data.AffiliateCashRewardHistory;
import com.zonesws.webservices.data.AffiliatePromoCodeTracking;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderRequest;
import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.data.ReferredCodeTracking;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;



public class CustomerOrderCleaner extends QuartzJobBean implements StatefulJob {

	private boolean running;
	static MailManager mailManager = null;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		CustomerOrderCleaner.mailManager = mailManager;
	}
	
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, - 12);
		Date toDate = new Date(cal.getTimeInMillis());
		System.out.println(toDate);
	}
	
	public static void updatePendingOrderStatus() {
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, - 12);
		Date toDate = new Date(cal.getTimeInMillis());
		
		List<CustomerOrderRequest> orderRequestList =  DAORegistry.getCustomerOrderRequestDAO().getAllPaymentPendingOrders(toDate);
		
		for (CustomerOrderRequest orderRequest : orderRequestList) {
			
			boolean errorOccured = false;
			Customer customer = CustomerUtil.getCustomerById(orderRequest.getCustomerId());
			
			try{
				CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().getPaymentPendingOrderById(orderRequest.getOrderId());
				CustomerLoyalty customerLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getOrderLoyaltyInfoId());
				CustomerLoyaltyHistory customerLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getOrderLoyaltyHistoryId());
				
				if(null != orderRequest.getAffCashRewardId()){
					
					AffiliateCashReward affiliateCashReward = DAORegistry.getAffiliateCashRewardDAO().get(orderRequest.getAffCashRewardId());
					AffiliateCashRewardHistory affiliateCashRewardHistory = DAORegistry.getAffiliateCashRewardHistoryDAO().get(orderRequest.getAffCashRewardHistoryId());
					
					AffiliatePromoCodeTracking affiliatePromoCodeTracking = DAORegistry.getAffiliatePromoCodeTrackingDAO().get(orderRequest.getAffPromoTrackingId());
					
					affiliateCashReward.setPendingCash(affiliateCashReward.getPendingCash() - affiliateCashRewardHistory.getCreditedCash());
					affiliateCashReward.setLastUpdate(new Date());
					
					DAORegistry.getAffiliateCashRewardDAO().update(affiliateCashReward);
					
					DAORegistry.getAffiliatePromoCodeTrackingDAO().delete(affiliatePromoCodeTracking);
					DAORegistry.getAffiliateCashRewardHistoryDAO().delete(affiliateCashRewardHistory);
				}

				if(null != orderRequest.getRtfPromoTrackingId()){
					RTFPromotionalOfferTracking offerTracking = DAORegistry.getRtfPromotionalOfferTrackingDAO().get(orderRequest.getRtfPromoTrackingId());
					DAORegistry.getRtfPromotionalOfferTrackingDAO().delete(offerTracking);
				}
				
				if(null != orderRequest.getRefCustLoyaltyHistoryId()){
					
					CustomerLoyalty refLoyalty = DAORegistry.getCustomerLoyaltyDAO().get(orderRequest.getRefCustLoyaltyInfoId());
					CustomerLoyaltyHistory refLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().get(orderRequest.getRefCustLoyaltyHistoryId());
					ReferredCodeTracking referredCodeTracking = DAORegistry.getReferredCodeTrackingDAO().get(orderRequest.getRefTrackingId());
					
					refLoyalty.setPendingPointsAsDouble(refLoyalty.getPendingPointsAsDouble() - refLoyaltyHistory.getPointsEarnedAsDouble());
					refLoyalty.setLastUpdate(new Date());
					
					DAORegistry.getCustomerLoyaltyDAO().update(refLoyalty);
					DAORegistry.getCustomerLoyaltyHistoryDAO().delete(refLoyaltyHistory);
					DAORegistry.getReferredCodeTrackingDAO().delete(referredCodeTracking);
				}
				
				try{
					DAORegistry.getCategoryTicketGroupDAO().revertSoldTicketToActive(customerOrder.getCategoryTicketGroupId());
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if(null != orderRequest.getLockId() ){
					//remove the locked ticket information after it has been purchased by customer
					DAORegistry.getAutoCatsLockedTicketDAO().deleteByLockId(orderRequest.getLockId());
				}
				
				customerLoyalty.setPendingPointsAsDouble(customerLoyalty.getPendingPointsAsDouble() - customerLoyaltyHistory.getPointsEarnedAsDouble());
				if(null != customerOrder.getPrimaryPaymentMethod() && 
						(customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.FULL_REWARDS) 
								|| customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.PARTIAL_REWARDS))){
					customerLoyalty.setActivePointsAsDouble(customerLoyalty.getActivePointsAsDouble()+ customerLoyaltyHistory.getPointsSpentAsDouble());
					customerLoyalty.setTotalSpentPointsAsDouble(customerLoyalty.getTotalSpentPointsAsDouble() - customerLoyaltyHistory.getPointsSpentAsDouble());
				}
				customerLoyalty.setLastUpdate(new Date());
				DAORegistry.getCustomerLoyaltyDAO().update(customerLoyalty);
				DAORegistry.getCustomerLoyaltyHistoryDAO().delete(customerLoyaltyHistory);
				
				orderRequest.setStatus(OrderStatus.PAYMENT_FAILED);
				orderRequest.setUpdatedTime(new Date());
				
				customerOrder.setStatus(OrderStatus.PAYMENT_FAILED);
				customerOrder.setLastUpdatedDateTemp(new Date());
				DAORegistry.getCustomerOrderDAO().update(customerOrder);
				DAORegistry.getCustomerOrderRequestDAO().update(orderRequest);
			}catch(Exception e){
				e.printStackTrace();
				errorOccured = true;
			}
			
			try{
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("OrderNo",orderRequest.getOrderId());
				mailMap.put("OrderRequestNo",orderRequest.getOrderId());
				mailMap.put("customer",customer);
				mailMap.put("paymentMethod",String.valueOf(orderRequest.getPrimaryPaymentMethod()));
				String subject = "Order Successfully Removed by Cleaner Job.!";
				if(errorOccured){
					subject = "Attention Required. Could not remove this order by cleaner job.";
				}
				
				mailMap.put("message",subject);
				
				MailAttachment[] mailAttachment = new MailAttachment[1];
				String filePath = null;
				mailAttachment[0] = new MailAttachment(null,"image/png","logo.png",URLUtil.getLogoImage());
				mailManager.sendMailNow("text/html","sales@rewardthefan.com", "AODev@rightthisway.com", 
						null,"AODev@rightthisway.com", "DEV TEAM : "+subject,"mail-clean-payment-pending-orders.html", 
						mailMap, "text/html", null,mailAttachment,filePath);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void executeInternal(JobExecutionContext context)
		throws JobExecutionException {
		if(!running){
			running =true;
			try {
				if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
					//updatePendingOrderStatus();
				}
			} catch(Exception e){
				e.printStackTrace();
			}
			running =false;
		}
	}
	
}
