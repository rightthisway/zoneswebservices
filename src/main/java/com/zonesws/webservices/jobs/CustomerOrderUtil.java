package com.zonesws.webservices.jobs;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Broker;
import com.zonesws.webservices.data.BrokerOrderEmailTracking;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderDetail;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.OrderTicketGroup;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.utils.MapUtil;
import com.zonesws.webservices.utils.RTFAffiliateBrokerUtil;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Customer Order Util to send
 * email for successful order
 * @author dthiyagarajan
 *
 */
public class CustomerOrderUtil extends QuartzJobBean implements StatefulJob{

	private static Logger logger = LoggerFactory.getLogger(CustomerOrderUtil.class);
	
	static MailManager mailManager = null;;
	private ZonesProperty properties;
	public static boolean isRunning = false;
	

	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		CustomerOrderUtil.mailManager = mailManager;
	}
	
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}

	public void init() throws Exception {
		
		if(isRunning) {
			return;
		}
		isRunning = true;
		
		try {
			logger.debug("Initiating the email scheduler process for customer orders....");
			DAORegistry.getRtfGiftCardOrderDAO().getCustomerOrderToSendEmail();
			Collection<CustomerOrder> customerOrders = DAORegistry.getCustomerOrderDAO().getCustomerOrderToSendEmail();
			Integer custId = 0;
			boolean isEmailSent = true;
			for(CustomerOrder customerOrder : customerOrders){
				custId = customerOrder.getCustomer().getId();
				isEmailSent = customerOrder.getIsInvoiceSent();
				String mailTemplate = "mail-rewardfan-api-ticket-purchase.html";
				
				if(customerOrder.getOrderType().equals(OrderType.TRIVIAORDER)) {
					mailTemplate = "mail-rewardfan-api-ticket-purchase-trivia.html";
				}
				Customer customer = DAORegistry.getCustomerDAO().get(custId);
				if(isEmailSent == false && customer != null){
					
					customerOrder.setAllCustomerRelatedObjects();
					
					Boolean triggerEmailToBroker = TicketUtil.triggerOrderEmailToBroker(customerOrder.getBrokerId()); 
					
					CustomerLoyalty loyaltyInfo = DAORegistry.getCustomerLoyaltyDAO().getCustomerLoyaltyByCustomerId(customer.getId());
					//CustomerLoyaltyHistory custLoyaltyHistory = DAORegistry.getCustomerLoyaltyHistoryDAO().getCustomerLoyaltyHistoryByOrderId(customerOrder.getId());
					CustomerOrderDetail custOrderDetail = DAORegistry.getCustomerOrderDetailDAO().getCustomerOrderDetailByOrderId(customerOrder.getId()); 
					String zones = customerOrder.getSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
					String svgWebViewUrl = "";
					if(null != customerOrder.getOrderType() && customerOrder.getOrderType().equals(OrderType.CONTEST)){
						
						/*try{
							MapUtil.copySVGMapandText(customerOrder.getVenueId(), customerOrder.getVenueCategory(),order.getId());
						}catch(Exception e){
							e.printStackTrace();
						}*/
						
						if(customerOrder.getSection().equals("TBD")){
							List<String> zoneList =  DAORegistry.getQueryManagerDAO().getAllZonesByVenueCategoryId(customerOrder.getVenueCategory());
							if(null != zoneList && !zoneList.isEmpty()){
								String svgZone = "";
								for (String zone : zoneList) {
									svgZone = svgZone +","+ zone.toUpperCase().replace("ZONE", "").replaceAll(" +","");
								}
								svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), svgZone,null,true);
							}else{
								svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), zones,null,false);
							}
						}else{
							svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), zones,null,false);
						}
						
					}else{
						svgWebViewUrl = MapUtil.getSvgWebViewForOrder(customerOrder.getVenueId(), customerOrder.getVenueCategory(), zones,customerOrder.getId(),false);
					}
					
					TicketUtil.getTicketDeliveryInFo(customerOrder);
					
					System.out.println(customerOrder.getDeliveryInfo());
					
					Map<String,Object> mailMap = new HashMap<String,Object>();
					CustomerLoyaltyHistory history = customerOrder.getCustomerLoyaltyHistory();
					Double totalPrice = customerOrder.getQty() * customerOrder.getTicketPriceAsDouble();
					String customerName = "";
					customerName = customer.getCustomerName()+" "+customer.getLastName();
					if(customer.getCompanyName()!=null && !customer.getCompanyName().trim().isEmpty() 
							&& customerOrder.getAppPlatForm().equals(ApplicationPlatForm.TICK_TRACKER)){
						customerName += " - "+customer.getCompanyName();
					}
					mailMap.put("customer", customer);
					mailMap.put("customerName", customerName);
					mailMap.put("customerOrder", customerOrder);
					mailMap.put("custOrderDetail", custOrderDetail);
					mailMap.put("custLoyaltyInfo", loyaltyInfo);
					mailMap.put("venueMap", svgWebViewUrl);
					mailMap.put("custLoyaltyHistory",history);
					mailMap.put("totalPrice", TicketUtil.getRoundedValueString(totalPrice));
					mailMap.put("referFriend", URLUtil.getReferFriendsLink());
					mailMap.put("fantasyTicket", URLUtil.getFantasyTicketLink());
					String rewardPointBlock = "block";
					if(history == null || history.getPointsEarnedAsDouble() <= 0){
						rewardPointBlock = "none";
					}
					mailMap.put("displayReward",rewardPointBlock);
					Invoice invoice = DAORegistry.getInvoiceDAO().getInvoiceByCustomerOrder(customerOrder.getId(), customer.getId());
					if(null != customerOrder.getIsLongSale() && customerOrder.getIsLongSale()){
						List<OrderTicketGroup> orderTicketGroups = DAORegistry.getOrderTicketGroupDAO().getAllTicketGroupsByOrderId(customerOrder.getId());
						
						if(orderTicketGroups!=null && !orderTicketGroups.isEmpty()){
							mailTemplate = "mail-rewardfan-api-real-ticket-purchase.html";
							String section="",row="",seat="";
							int i =0;
							for (OrderTicketGroup orderTicketGroup : orderTicketGroups) {
								if(i == 0 ){
									section= orderTicketGroup.getSection();
									row= orderTicketGroup.getRow();
									seat= orderTicketGroup.getSeat();
								}else{
									section= section +","+orderTicketGroup.getSection();
									row= row +","+orderTicketGroup.getRow();
									seat= seat +","+orderTicketGroup.getSeat();
								}
								i++;
							}
							mailMap.put("section", section);
							mailMap.put("row", row);
							mailMap.put("seat",seat);
							
						}
						
						/*List<RealTicketSectionDetails> list = DAORegistry.getQueryManagerDAO().getRealTicketSectionDetailsByInvoiceId(invoice.getId());
						if(list!=null && !list.isEmpty()){
							mailTemplate = "mail-rewardfan-api-real-ticket-purchase.html";
							mailMap.put("section", list.get(0).getSection());
							mailMap.put("row", list.get(0).getRow());
							if(invoice.getTicketCount()==1){
								mailMap.put("seat", list.get(0).getSeatLow());
							}else{
								String seatDescription = "";
								boolean isFirst = true;
								for(RealTicketSectionDetails sectionDetails : list){
									if(isFirst){
										isFirst = false;
										seatDescription += sectionDetails.getSeatLow()+" to "+sectionDetails.getSeatHigh();
									}else{
										seatDescription += ", and "+sectionDetails.getSeatLow()+" to "+sectionDetails.getSeatHigh();
									}
									
								}
								mailMap.put("seat",seatDescription);
							}
						}*/
					}
					
					String email=customer.getEmail();
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getOrderConfirmationEmailAttachmentImages();
					try {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
								null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
								"Your RewardTheFan Ticket Order.",
								mailTemplate, mailMap, "text/html", null,mailAttachment,null);
						System.out.println("ORDER EMAIL BEGINS : "+new Date());
						DAORegistry.getCustomerOrderDAO().updateOrderEmailSent(customerOrder.getId(),email);
						System.out.println("ORDER EMAIL ENDS : "+new Date());
					
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(triggerEmailToBroker){
						BrokerOrderEmailTracking tracking =new BrokerOrderEmailTracking();
						tracking.setOrderId(customerOrder.getId());
						tracking.setBrokerId(customerOrder.getBrokerId());
						tracking.setCustomerId(customer.getId());
						tracking.setOrderEmailSentTime(new Date());
						try {
							
							System.out.println("ORDER EMAIL TO BROKER :  BEGINS : "+new Date());
							
							mailTemplate="mail-rewardfan-broker-ticket-sold.html";
							
							Broker broker = RTFAffiliateBrokerUtil.getBrokerByBrokerId(customerOrder.getBrokerId());
							
							String subject = "RewardTheFan : Delivery of your tickets #"+customerOrder.getId()+" *** Customer: "+customer.getCustomerName()+" "+customer.getLastName();
							
							mailManager.sendMailNow("text/html",URLUtil.fromEmail, broker.getEmail(), 
									null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
									subject,mailTemplate, mailMap, "text/html", null,mailAttachment,null);
							
							tracking.setOrderEmailSent(true);
							tracking.setOrderEmailSentTo(broker.getEmail());
							System.out.println("ORDER EMAIL TO BROKER :  ENDS : "+new Date());
						
						} catch (Exception e) {
							tracking.setOrderEmailSent(false);
							tracking.setOrderEmailSentTo("Error Occured While Sending email to Broker Id"+customerOrder.getBrokerId());
							e.printStackTrace();
						}
						
						DAORegistry.getBrokerOrderEmailTrackingDAO().save(tracking);
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		isRunning = false;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				Thread.sleep(1000);
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
