package com.zonesws.webservices.jobs;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Customer Order Util to send
 * email for successful order
 * @author Ulaganathan
 *
 */
public class CustomerSignupEmailUtil extends QuartzJobBean implements StatefulJob{

	private static Logger logger = LoggerFactory.getLogger(CustomerGiftCardOrderUtil.class);
	
	static MailManager mailManager = null;;
	private ZonesProperty properties;
	public static boolean isRunning = false;
	

	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		CustomerSignupEmailUtil.mailManager = mailManager;
	}
	
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}

	public void init() throws Exception {
		if(isRunning) {
			return;
		}
		isRunning = true;
		try {
			logger.debug("Initiating the email scheduler process for customer registration welcome email....");
			Collection<Customer> customers = DAORegistry.getCustomerDAO().getAllCustomerEmailToBeSent();
			Date now = new Date();
			for(Customer obj : customers){
				try{
					if(null == obj.getIsOtpVerified() || !obj.getIsOtpVerified() || null == obj.getEmail() || obj.getEmail().isEmpty()) {
						continue;
					}
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("userId",obj.getUserId());
					try{
						MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, obj.getEmail(), 
							null,URLUtil.bccEmails, "You have successfully registered with RewardTheFan.com.",
							"mail-rewardfan-customer-welcome-phoneno-login.html", mailMap, "text/html", null,mailAttachment,null);
					}catch(Exception e){
						e.printStackTrace();
					}
					obj.setIsEmailed(true);
					obj.setLastUpdatedDate(now);
					DAORegistry.getCustomerDAO().update(obj);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		isRunning = false;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				Thread.sleep(1000);
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
