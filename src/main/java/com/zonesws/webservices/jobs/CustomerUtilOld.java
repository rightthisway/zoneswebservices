package com.zonesws.webservices.jobs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Country;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.State;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.utils.URLUtil;

/**
 * 
 * @author dthiyagarajan
 *
 */
public class CustomerUtilOld extends QuartzJobBean implements StatefulJob{
	
	private static Map<Integer, Customer> customerMapById = new ConcurrentHashMap<Integer, Customer>();
	private static Map<Integer, State> stateMapById = new ConcurrentHashMap<Integer, State>();
	private static Map<Integer, Country> countryMapById = new ConcurrentHashMap<Integer, Country>();
	private static Map<Integer, List<State>> stateListMapByCountryId = new ConcurrentHashMap<Integer, List<State>>();
	private static Collection<Country> countryList = new ArrayList<Country>(); 
	private static Map<String, Customer> customerMapByProductType = new ConcurrentHashMap<String, Customer>();
	/*stateListByCountryId
	CountryList*/
	/**
	 * Method to get the customer info 
	 * by customer id
	 * @param customerId
	 * @return
	 */
	public static Customer getCustomerById(Integer customerId){
		Customer customer = customerMapById.get(customerId);
		try {
			if(customer == null || customer.getReferrerCode() == null || customer.getReferrerCode().isEmpty()){
				customer = DAORegistry.getCustomerDAO().get(customerId);
				if(customer != null){
					customerMapById.put(customerId, customer);
				}
			}
			return customer;	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Customer getCustomerById(Integer customerId,ProductType productType){
		Customer customer = customerMapByProductType.get(customerId+":"+productType);
		if(customer == null){
			customer = DAORegistry.getCustomerDAO().getCustomerByIdByProduct(customerId, productType);
			if(null != customer){
				customerMapByProductType.put(customerId+":"+productType, customer);
			}
		}
		return customer;
	}
	
	public static void updatedCustomerUtil(Customer customer){
		try {
			customerMapById.put(customer.getId(), customer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to get the state by id
	 * @param stateId
	 * @return
	 */
	public static State getStateById(Integer stateId){
		State state = stateMapById.get(stateId);
		try {
			if(state != null){
				stateMapById.put(stateId, state);
			}
			return state;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method to get the country by id
	 * @param stateId
	 * @return
	 */
	public static Country getCountryById(Integer countryId){
		Country country = countryMapById.get(countryId);
		try {
			if(country != null){
				countryMapById.put(countryId, country);
			}
			return country;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * Method to get the state list by country
	 * @param countryId
	 * @return
	 */
	public static List<State> getStateListByCountry(Integer countryId){
		List<State> stateList = stateListMapByCountryId.get(countryId);
		try {
			if(stateList != null && stateList.size() > 0){
				return stateList;
			}
		} catch (Exception e) {
			return null;
		}
		return stateList;
	}
	
	/**
	 * Method to get the list of countries
	 * @return
	 */
	public static Collection<Country> getCountryList(){
		try {
			if(countryList != null && countryList.size() > 0){
				return countryList;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return countryList;
	}
	
	/**
	 * Method to initiate the scheduler process
	 */
	public static void init(){
		
		try {
			Long startTime = System.currentTimeMillis();
			
			
			//Map to persist the customer info into cache
			Map<Integer, Customer> tempCustomerMapById = new ConcurrentHashMap<Integer, Customer>();
			//Map to persist the state info into cache
			Map<Integer, State> tempStateMapById = new ConcurrentHashMap<Integer, State>();
			//Map to persist the country info into cache
			Map<Integer, Country> tempCountryMapById = new ConcurrentHashMap<Integer, Country>();
			//State list map to persist the list of states by country in cache
			Map<Integer, List<State>> tempStateListByCountryId = new ConcurrentHashMap<Integer, List<State>>();
			Map<String, Customer> tempCustomerMapByProductType = new ConcurrentHashMap<String, Customer>();
			
			//Collection of customers
			Collection<Customer> customers = DAORegistry.getCustomerDAO().getAll();
			
			for(Customer customer : customers){
				try{
					tempCustomerMapById.put(customer.getId(), customer);
					tempCustomerMapByProductType.put(customer.getId()+":"+String.valueOf(customer.getProductType()), customer);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			customerMapById.clear();
			customerMapById = new HashMap<Integer, Customer>(tempCustomerMapById);
			
			customerMapByProductType.clear();
			customerMapByProductType = null;
			customerMapByProductType = new HashMap<String, Customer>(tempCustomerMapByProductType);
			
			//Collection of states
			Collection<State> states = DAORegistry.getStateDAO().getAll();
			//Collection of countries 
			Collection<Country> countries = DAORegistry.getCountryDAO().getAll();
			
			for(State state : states){
				tempStateMapById.put(state.getId(), state);
			}
			
			stateMapById.clear();
			stateMapById = new HashMap<Integer, State>(tempStateMapById);
			
			for(Country country : countries){
				try{
					tempCountryMapById.put(country.getId(), country);
					if(country.getId() == 217 || country.getId() == 38){
						List<State> stateListByCountryId = DAORegistry.getStateDAO().getAllStateByCountryId(country.getId());
						if(null != stateListByCountryId && stateListByCountryId.isEmpty()){
							tempStateListByCountryId.put(country.getId(), stateListByCountryId);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			
			countryMapById.clear();
			countryMapById = new HashMap<Integer, Country>(tempCountryMapById);
			
			//Put all states by country in cache
			stateListMapByCountryId.clear();
			stateListMapByCountryId = new HashMap<Integer, List<State>>(tempStateListByCountryId);
			
			//List of countries
			countryList.clear();
			countryList = new ArrayList<Country>(countries);
			
			System.out.println("TIME TO LAST CUSTOMER UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void updateCustomerUtilForContestCustomers(Integer contestId,Date fromDate) throws Exception {
		
		List<Customer> customersList = DAORegistry.getCustomerDAO().getAllCustomersByLAstUpdateTime(fromDate);
		if(customersList != null) {
			for (Customer customer : customersList) {
				updatedCustomerUtil(customer);
			}
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			init();
		}
	}

}
