package com.zonesws.webservices.jobs;

import java.util.Calendar;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.enums.FileType;
import com.zonesws.webservices.notifications.RTFOrderDownloadNotification;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.URLUtil;

public class DownloadTicketNotification extends QuartzJobBean implements StatefulJob{

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			//sendDownloadTicketNotification();
		}
		
	}

	
	public void sendDownloadTicketNotification(){
		try {
			FileType fileType = FileType.ETICKET;
			Calendar calender = Calendar.getInstance();
			calender.set(calender.HOUR, 00);
			calender.set(calender.MINUTE, 00);
			calender.set(calender.SECOND, 00);
			calender.set(calender.MILLISECOND, 00);
			List<CustomerOrder> orders  = DAORegistry.getQueryManagerDAO().getAllNotDownloadedInvoices();
			for(CustomerOrder o : orders){
				CustomerOrder order = DAORegistry.getCustomerOrderDAO().get(o.getId());
				Customer customer = DAORegistry.getCustomerDAO().get(order.getCustomer().getId());
				long diff = DateUtil.getDifferenceDays(order.getEventDateTemp(),calender.getTime());
				int dayOfMonth = calender.get(Calendar.DAY_OF_MONTH);
				if(order.getEventDateTemp().compareTo(calender.getTime()) < 0 ){
					continue;
				}
				if(diff == -7 || diff == 0 || dayOfMonth == 1){
					System.out.println("SENDING DOWNLOAD TICKET NOTIFICATION: "+diff);
					RTFOrderDownloadNotification.trigger(order, customer, null, fileType,true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
