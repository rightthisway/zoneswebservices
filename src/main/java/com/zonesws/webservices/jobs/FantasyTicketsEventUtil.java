package com.zonesws.webservices.jobs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Event;


public class FantasyTicketsEventUtil extends QuartzJobBean implements StatefulJob{
	
	private static Map<Integer, Event> fantasyTicketsEventMap = new ConcurrentHashMap<Integer, Event>();
	
	
	public static Event getFantasyEventByEventId(Integer eventId) throws Exception{
		Event event = fantasyTicketsEventMap.get(eventId);
		if(null == event){
			event = DAORegistry.getEventDAO().getFantasyEventByEventId(eventId);
			fantasyTicketsEventMap.put(eventId, event);
		}
		return event;
	}
	
	public static void init() {
		
		try{
			Long startTime = System.currentTimeMillis();
			List<Event> events = DAORegistry.getEventDAO().getAllFantasyEvents();
			
			Map<Integer, Event> fantasyTicketsEventMapTemp = new HashMap<Integer, Event>();
			for (Event event : events) {
				fantasyTicketsEventMapTemp.put(event.getEventId(), event);
			}
			
			if(null != fantasyTicketsEventMapTemp && !fantasyTicketsEventMapTemp.isEmpty()){
				fantasyTicketsEventMap.clear();
				fantasyTicketsEventMap.putAll(fantasyTicketsEventMapTemp);
			}
			
			fantasyTicketsEventMapTemp.clear();
			
			System.out.println("TIME TO LAST FANTASY EVENTS UTILS=" + (System.currentTimeMillis() - startTime) / 1000);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
		
	}
	
}
