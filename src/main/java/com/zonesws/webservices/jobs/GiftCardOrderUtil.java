package com.zonesws.webservices.jobs;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Customer Order Util to send
 * email for successful order
 * @author dthiyagarajan
 *
 */
public class GiftCardOrderUtil extends QuartzJobBean implements StatefulJob{

	private static Logger logger = LoggerFactory.getLogger(GiftCardOrderUtil.class);
	
	static MailManager mailManager = null;;
	private ZonesProperty properties;
	

	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		GiftCardOrderUtil.mailManager = mailManager;
	}
	
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}

	public void init() throws Exception {
		 
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
