package com.zonesws.webservices.jobs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.ZonesProperty;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Customer Gift Card Order Upload Notification Util to send
 * email for order full fillment
 * @author kulaganathan
 *
 */
public class GiftCardUploadNotificationUtil extends QuartzJobBean implements StatefulJob{

	private static Logger logger = LoggerFactory.getLogger(GiftCardUploadNotificationUtil.class);
	
	static MailManager mailManager = null;;
	private ZonesProperty properties;
	public static boolean isRunning = false;
	

	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		GiftCardUploadNotificationUtil.mailManager = mailManager;
	}
	
	public ZonesProperty getProperties() {
		return properties;
	}
	public void setProperties(ZonesProperty properties) {
		this.properties = properties;
	}

	public void init() throws Exception {
		
		if(isRunning) {
			return;
		}
		isRunning = true;
		
		try {
			logger.debug("Initiating the email scheduler process for gift card order fullfilled notification....");
			Collection<RtfGiftCardOrder> customerOrders = DAORegistry.getRtfGiftCardOrderDAO().getFilledOrdersToSendEmail();
			for(RtfGiftCardOrder order : customerOrders){
				try {
					Customer customer = DAORegistry.getCustomerDAO().get(order.getCustomerId());
				 	String mailTemplate = "email-gift-card-download-attachment.html";
				 	Map<String,Object> mailMap = new HashMap<String,Object>();
					String subject = "Reward The Fan: Your GIFT Card Available for download";
					mailMap.put("orderId", order.getId()); 
					String email=customer.getEmail();
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
					try {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
								null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
								subject,mailTemplate, mailMap, "text/html", null,mailAttachment,null);
						
					} catch (Exception e) {
						e.printStackTrace();
					}	
					order.setIsOrderFilledMailSent(true);
					DAORegistry.getRtfGiftCardOrderDAO().updateOrderFilledEmailSent(order.getId());
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		isRunning = false;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				Thread.sleep(1000);
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
