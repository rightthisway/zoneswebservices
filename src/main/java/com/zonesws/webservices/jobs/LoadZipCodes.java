package com.zonesws.webservices.jobs;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.utils.TextUtil;
import com.zonesws.webservices.utils.list.ZipCode;


public class LoadZipCodes extends QuartzJobBean implements StatefulJob{
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(LoadZipCodes.class);
	
	private static List<ZipCode> zipCodeList = new ArrayList<ZipCode>();
	
	public static void main(String[] args) {
		Boolean isDefaultLocSearch=false;
		
		String defaultLocSearchFlag ="false";
		
		isDefaultLocSearch = Boolean.valueOf(defaultLocSearchFlag);
		
		System.out.println(isDefaultLocSearch);
		
	}
	
	public static List<ZipCode> getAllZipCodes(){
		try {
			if(null == zipCodeList || zipCodeList.isEmpty() || zipCodeList.size() <= 0){
				
				List<Object[]> zipCodeObjectList = DAORegistry.getQueryManagerDAO().getZipCodes();
				ZipCode zipCode = null;
				List<ZipCode> zipCodes = new ArrayList<ZipCode>();
				for (Object[] object : zipCodeObjectList) {
					try{
						zipCode = new ZipCode();
						zipCode.setLatitude(Double.parseDouble(String.valueOf(object[1])));
						zipCode.setLongitude(Double.parseDouble(String.valueOf(object[2])));
						zipCode.setZipCode(String.valueOf((Integer)object[0]));
						zipCodes.add(zipCode);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				zipCodeList = new ArrayList<ZipCode>(zipCodes);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return zipCodeList;
	}
	
	public static void init() {
		
		try{
			Long startTime = System.currentTimeMillis();
			List<Object[]> zipCodeObjectList = DAORegistry.getQueryManagerDAO().getZipCodes();
			ZipCode zipCode = null;
			List<ZipCode> zipCodes = new ArrayList<ZipCode>();
			for (Object[] object : zipCodeObjectList) {
				try{
					zipCode = new ZipCode();
					zipCode.setLatitude(Double.parseDouble(String.valueOf(object[1])));
					zipCode.setLongitude(Double.parseDouble(String.valueOf(object[2])));
					zipCode.setZipCode(String.valueOf((Integer)object[0]));
					zipCodes.add(zipCode);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			zipCodeList = new ArrayList<ZipCode>(zipCodes);
			
			System.out.println("TIME TO LAST LOAD ZIP CODES =" + (System.currentTimeMillis() - startTime) / 1000);
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
		
	}
	
}
