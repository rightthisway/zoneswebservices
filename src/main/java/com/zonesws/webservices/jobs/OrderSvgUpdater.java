package com.zonesws.webservices.jobs;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Collection;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.util.FileCopyUtils;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.enums.OrderStatus;
import com.zonesws.webservices.utils.URLUtil;

public class OrderSvgUpdater extends QuartzJobBean implements StatefulJob{

	@Override
	protected void executeInternal(JobExecutionContext arg0)throws JobExecutionException {
		//init();
	}
	
	public void init(){
		try {
			
			Collection<CustomerOrder> orders = DAORegistry.getCustomerOrderDAO().getAllRTFOrders();
			for(CustomerOrder order : orders){
				if(!order.getStatus().equals(OrderStatus.ACTIVE)){
					continue;
				}
				String existingTextFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+ order.getVenueCategory()+"_"+order.getId()+".txt";
				File existingTextFile = new File(existingTextFilePath);
				if(!existingTextFile.exists()){
					String textFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+ order.getVenueCategory()+".txt";
					File textFile = new File(textFilePath);
					if(textFile!= null && textFile.exists()){
						String newTextFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+ order.getVenueCategory()+"_"+order.getId()+".txt";
						File newTextFile = new File(newTextFilePath);
						newTextFile.createNewFile();
						
						FileInputStream inputStream = new FileInputStream(textFile);
						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newTextFile));
						FileCopyUtils.copy(inputStream, stream);
						stream.close();
						inputStream.close();
						System.out.println("TXT COPIED STRD : "+order.getId());
					}else{
						String tFilePath = URLUtil.svgTextFilePath+ order.getVenueId()+"_"+order.getVenueCategory()+".txt";
						File tFile = new File(tFilePath);
						if(tFile!= null && tFile.exists()){
							String newTFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+ order.getVenueCategory()+"_"+order.getId()+".txt";
							File newTFile = new File(newTFilePath);
							newTFile.createNewFile();
							
							FileInputStream inputStream = new FileInputStream(tFile);
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newTFile));
							FileCopyUtils.copy(inputStream, stream);
							stream.close();
							inputStream.close();
							System.out.println("TXT COPIED MAIN : "+order.getId());
						}else{
							System.out.println("TXT NOT FOUND : "+order.getId()+" -- "+order.getVenueCategory());
						}
						
					}
				}
				
				String existingSvgFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+order.getVenueCategory()+"_"+order.getId()+ ".gif";
				File existingSvgFile = new File(existingSvgFilePath);
				if(!existingSvgFile.exists()){
					String svgFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+ order.getVenueCategory()+".gif";
					File svgFile = new File(svgFilePath);
					if(svgFile!= null && svgFile.exists()){
						String newSvgFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+ order.getVenueCategory()+"_"+order.getId()+".gif";
						File newSvgFile = new File(newSvgFilePath);
						newSvgFile.createNewFile();
						
						FileInputStream inputStream = new FileInputStream(svgFile);
						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newSvgFile));
						FileCopyUtils.copy(inputStream, stream);
						stream.close();
						inputStream.close();
						System.out.println("TXT COPIED STRD : "+order.getId());
					}else{
						String sFilePath = URLUtil.basePath+"SvgMaps//"+ order.getVenueId()+"_"+order.getVenueCategory()+".gif";
						File sFile = new File(sFilePath);
						if(sFile!= null && sFile.exists()){
							String newSFilePath = URLUtil.getStoredSVGMapPath() + order.getVenueId()+"_"+ order.getVenueCategory()+"_"+order.getId()+".gif";
							File newSFile = new File(newSFilePath);
							newSFile.createNewFile();
							
							FileInputStream inputStream = new FileInputStream(sFile);
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newSFile));
							FileCopyUtils.copy(inputStream, stream);
							stream.close();
							inputStream.close();
							System.out.println("TXT COPIED MAIN : "+order.getId());
						}else{
							System.out.println("GIF NOT FOUND : "+order.getId()+" -- "+order.getVenueCategory());
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
