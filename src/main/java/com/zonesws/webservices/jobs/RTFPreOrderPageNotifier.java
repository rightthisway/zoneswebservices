package com.zonesws.webservices.jobs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCheckoutVisitOffer;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.PreOrderPageAudit;
import com.zonesws.webservices.data.PreOrderPageEmailTracking;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

public class RTFPreOrderPageNotifier extends QuartzJobBean implements StatefulJob{
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private static SimpleDateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	static MailManager mailManager = null;
	static Date lastRunTime = null;
	
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFPreOrderPageNotifier.mailManager = mailManager;
	}
	
	
	
	public void preOrderPageVisitOfferForCustomers(){
		
		Date toDate = new Date();
		Date fromDate = null;
		
		if(lastRunTime == null){
			
			Calendar apiCal= Calendar.getInstance();
			apiCal.set(Calendar.HOUR_OF_DAY, 12);
			apiCal.set(Calendar.MINUTE, 10);
			
			if(toDate.before(apiCal.getTime())){	
				apiCal.set(Calendar.HOUR_OF_DAY, 0);
				apiCal.set(Calendar.MINUTE, 01);
				fromDate = apiCal.getTime();
			}else{
				apiCal.set(Calendar.HOUR_OF_DAY, 12);
				apiCal.set(Calendar.MINUTE, 01);
				fromDate = apiCal.getTime();
			}
		}else{
			fromDate = lastRunTime;
		}
		
		System.out.println("PRE-ORDER OFFER ::: FROM DATE: "+fromDate+",  TO DATE: "+toDate);
		try{
			
			List<PreOrderPageAudit> list = DAORegistry.getQueryManagerDAO().getAllCustomerEventVisitByDate(dbDateTimeFormat.format(fromDate), dbDateTimeFormat.format(toDate));
			
			Map<Integer, List<Integer>> eventsIdsByCustomerMap = new HashMap<Integer, List<Integer>>();
			
			String eventIdStr = "";
			int j = 0;
			for(PreOrderPageAudit obj:list){
				if(j == 0){
					eventIdStr = obj.getEventId().toString();
				}else{
					eventIdStr = eventIdStr + ","+ obj.getEventId().toString();
				}
				
				List<Integer> eventIds = eventsIdsByCustomerMap.get(obj.getCustomerId());
				if(null != eventIds && !eventIds.isEmpty()){
					eventsIdsByCustomerMap.get(obj.getCustomerId()).add(obj.getEventId());
				}else{
					eventIds = new ArrayList<Integer>();
					eventIds.add(obj.getEventId());
					eventsIdsByCustomerMap.put(obj.getCustomerId(), eventIds);
				}
				j++;
			}
			
			List<Event> events = null;
			
			CustomerCheckoutVisitOffer visitOffer = DAORegistry.getCustomerCheckoutVisitOfferDAO().getActivetOffer();
			
			String visitedEventDisc = "", otherEventDisc="", otherEventMinOrderTot = "$"+visitOffer.getOtherEventMinOrderTotal().intValue();
			String mailTitle = "Now Take "+visitOffer.getVisitedEventDiscValue().intValue()+"% OFF.";
			if(visitOffer.getIsFlatOff()){
				visitedEventDisc = "$"+visitOffer.getVisitedEventDiscValue().intValue();
				otherEventDisc="$"+visitOffer.getOtherEventDiscValue().intValue();
			}else{
				visitedEventDisc = visitOffer.getVisitedEventDiscValue().intValue()+"%";
				otherEventDisc= visitOffer.getOtherEventDiscValue().intValue()+"%";
			}
			
			for(Integer custId : eventsIdsByCustomerMap.keySet()){
				
				List<Integer> eventIds = eventsIdsByCustomerMap.get(custId);
				
				events = new ArrayList<Event>();
				
				String eventNames = "";
				int i = 0;
				for (Integer eventId : eventIds) {
					Event event = DAORegistry.getEventDAO().get(eventId);
					if(i == 0){
						eventNames = event.getEventName();
					}else{
						eventNames = eventNames+", "+event.getEventName();
					}
					i++;
					String url = "https://rewardthefan.com/GetEventDetails?id="+eventId;
					String anchorTag = "<a class='buttonClass' href='"+url+"'> Find Tickets </a>";
					event.setRtfEventUrll(anchorTag);
					events.add(event);
				}
				Customer customer = CustomerUtil.getCustomerById(custId);
				
				if(customer.getTicketPurchased()){
					try{
						DAORegistry.getPreOrderPageAuditDAO().deleteNotEligibleTracking(custId);
					}catch(Exception e){
						e.printStackTrace();
					}
					continue;
				}
				
				Calendar expiryCal= Calendar.getInstance();
				expiryCal.add(Calendar.DAY_OF_MONTH, visitOffer.getOfferExpiredDays()+1);
				String expiryDate = dateFormat.format(expiryCal.getTime());
				
				try {
					
					String mainMessage = "<font color='#FFFFFF'>Save "+visitedEventDisc+" On "+eventNames+" tickets!</font>";
					String secondaryMessage = "We noticed you were interested in <font color='#008000'><b>"+eventNames+"</b></font> tickets, " +
							"but didn't make a purchase. " +
							"Complete your order now, and we'll take <b><font color='#f06812'>"+visitedEventDisc+"</font> OFF*</b> when you check out!";
					String thirdMessage = "<b>*<font color='#f06812'>"+otherEventDisc+"</font> OFF</b> any ticket purchase " +
							"over <b><font color='#f06812'>"+otherEventMinOrderTot+"</font></b> before " +
							"fees by clicking the link in this email. " +
							"Offer expires <b><font color='#f06812'>"+expiryDate+"</font></b> and is " +
							"only valid for online or in APP purchases. Cannot be combined with other discount offers";
					
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("customer", customer);
					mailMap.put("mainMessage", mainMessage);
					mailMap.put("secondaryMessage", secondaryMessage);
					mailMap.put("thirdMessage", thirdMessage);
					mailMap.put("buttonValue", "SAVE "+visitedEventDisc+" ON YOUR TICKETS");
					mailMap.put("events", events);
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForPreOrderPageVisitOffer();
					
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.fromEmail+","+URLUtil.bccEmails, mailTitle,
				    		"mail-rtf-pre-order-page-visit-offer.html", mailMap, "text/html", null,mailAttachment,null);
					
					PreOrderPageEmailTracking emailTracking = new PreOrderPageEmailTracking();
					emailTracking.setCustomerId(custId);
					emailTracking.setEventIds(eventIdStr);
					emailTracking.setEmailSentDate(new Date());
					emailTracking.setOfferExpiryDays(visitOffer.getOfferExpiredDays());
					DAORegistry.getPreOrderPageEmailTrackingDAO().saveOrUpdate(emailTracking);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			lastRunTime = toDate;
			list = null;
		}catch (Exception e) {
			System.out.println(e.fillInStackTrace());
			e.printStackTrace();
		}
		
	}
	
	
 /*public void preOrderPageVisitOfferForCustomersNew(){
		
		Date toDate = new Date();
		Date fromDate = null;
		
		if(lastRunTime == null){
			
			Calendar apiCal= Calendar.getInstance();
			apiCal.set(Calendar.HOUR_OF_DAY, 12);
			apiCal.set(Calendar.MINUTE, 10);
			
			if(toDate.before(apiCal.getTime())){	
				apiCal.set(Calendar.HOUR_OF_DAY, 0);
				apiCal.set(Calendar.MINUTE, 01);
				fromDate = apiCal.getTime();
			}else{
				apiCal.set(Calendar.HOUR_OF_DAY, 12);
				apiCal.set(Calendar.MINUTE, 01);
				fromDate = apiCal.getTime();
			}
		}else{
			fromDate = lastRunTime;
		}
		
		System.out.println("PRE-ORDER OFFER ::: FROM DATE: "+fromDate+",  TO DATE: "+toDate);
		try{
			
			List<PreOrderPageAudit> list = DAORegistry.getQueryManagerDAO().getAllCustomerEventVisitByDate(dbDateTimeFormat.format(fromDate), dbDateTimeFormat.format(toDate));
			
			Map<Integer, List<Integer>> eventsIdsByCustomerMap = new HashMap<Integer, List<Integer>>();
			
			String eventIdStr = "";
			int j = 0;
			for(PreOrderPageAudit obj:list){
				if(j == 0){
					eventIdStr = obj.getEventId().toString();
				}else{
					eventIdStr = eventIdStr + ","+ obj.getEventId().toString();
				}
				
				List<Integer> eventIds = eventsIdsByCustomerMap.get(obj.getCustomerId());
				if(null != eventIds && !eventIds.isEmpty()){
					eventsIdsByCustomerMap.get(obj.getCustomerId()).add(obj.getEventId());
				}else{
					eventIds = new ArrayList<Integer>();
					eventIds.add(obj.getEventId());
					eventsIdsByCustomerMap.put(obj.getCustomerId(), eventIds);
				}
				j++;
			}
			
			List<Event> events = null;
			
			CustomerCheckoutVisitOffer visitOffer = DAORegistry.getCustomerCheckoutVisitOfferDAO().getActivetOffer();
			
			String visitedEventDisc = "", otherEventDisc="", otherEventMinOrderTot = "$"+visitOffer.getOtherEventMinOrderTotal().intValue();
			String mailTitle = "Now Take "+visitOffer.getVisitedEventDiscValue().intValue()+"% OFF.";
			if(visitOffer.getIsFlatOff()){
				visitedEventDisc = "$"+visitOffer.getVisitedEventDiscValue().intValue();
				otherEventDisc="$"+visitOffer.getOtherEventDiscValue().intValue();
			}else{
				visitedEventDisc = visitOffer.getVisitedEventDiscValue().intValue()+"%";
				otherEventDisc= visitOffer.getOtherEventDiscValue().intValue()+"%";
			}
			
			for(Integer custId : eventsIdsByCustomerMap.keySet()){
				
				List<Integer> eventIds = eventsIdsByCustomerMap.get(custId);
				
				events = new ArrayList<Event>();
				
				String eventNames = "";
				int i = 0;
				for (Integer eventId : eventIds) {
					Event event = DAORegistry.getEventDAO().get(eventId);
					if(i == 0){
						eventNames = event.getEventName();
					}else{
						eventNames = eventNames+", "+event.getEventName();
					}
					i++;
					String url = "https://rewardthefan.com/GetEventDetails?id="+eventId;
					String anchorTag = "<a class='buttonClass' href='"+url+"'> Find Tickets </a>";
					event.setRtfEventUrll(anchorTag);
					events.add(event);
				}
				Customer customer = CustomerUtil.getCustomerById(custId);
				
				
				try {
					
					String mainMessage = "<font color='#FFFFFF'>Save "+visitedEventDisc+" On "+eventNames+" tickets!</font>";
					String secondaryMessage = "We noticed you were interested in <font color='#008000'><b>"+eventNames+"</b></font> tickets, " +
							"but didn't make a purchase. " +
							"Complete your order now, and we'll take <b><font color='#f06812'>"+visitedEventDisc+"</font> OFF*</b> when you check out!";
					String thirdMessage = "<b>*<font color='#f06812'>"+otherEventDisc+"</font> OFF</b> any ticket purchase " +
							"over <b><font color='#f06812'>"+otherEventMinOrderTot+"</font></b> before " +
							"fees by clicking the link in this email. " +
							"Offer expires (make this date <b><font color='#f06812'>"+visitOffer.getOfferExpiredDays()+"</font> days</b> after the email is sent) and is " +
							"only valid for online or in APP purchases. Cannot be combined with other discount offers";
					
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("customer", customer);
					mailMap.put("mainMessage", mainMessage);
					mailMap.put("secondaryMessage", secondaryMessage);
					mailMap.put("thirdMessage", thirdMessage);
					mailMap.put("buttonValue", "SAVE "+visitedEventDisc+" ON YOUR TICKETS");
					mailMap.put("events", events);
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForPreOrderPageVisitOffer();
					
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, "AODev@rightthisway.com", 
							"rsamit@gmail.com","tselvan@rightthisway.com", mailTitle,
				    		"mail-rtf-pre-order-page-visit-offer.html", mailMap, "text/html", null,mailAttachment,null);
					
					PreOrderPageEmailTracking emailTracking = new PreOrderPageEmailTracking();
					emailTracking.setCustomerId(custId);
					emailTracking.setEventIds(eventIdStr);
					emailTracking.setEmailSentDate(new Date());
					emailTracking.setOfferExpiryDays(visitOffer.getOfferExpiredDays());
					//DAORegistry.getPreOrderPageEmailTrackingDAO().saveOrUpdate(emailTracking);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			lastRunTime = toDate;
			list = null;
		}catch (Exception e) {
			System.out.println(e.fillInStackTrace());
			e.printStackTrace();
		}
		
	}*/
	
	public void sendEmail() throws Exception{
		Map<String,Object> mailMap = new HashMap<String,Object>();
		
		//inline(image in mail body) image add code
		MailAttachment[] mailAttachment = null;
		
		mailManager.sendMailNow("text/html",URLUtil.fromEmail, "AODev@rightthisway.com", 
				"mamta@42works.net","tselvan@rightthisway.com", "RTF Pre Order Offer Template",
	    		"testing-sample.html", mailMap, "text/html", null,mailAttachment,null);
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			//preOrderPageVisitOfferForCustomers();
		}
	}
	
	public static void main(String[] args) {
		Calendar expiryCal= Calendar.getInstance();
		System.out.println("Current Date---->"+expiryCal.getTime());
		expiryCal.add(Calendar.DAY_OF_MONTH, 5+1);
		String expiryDate = dateFormat.format(expiryCal.getTime());
		System.out.println("expiryDate---->"+expiryDate);
	}
}
