package com.zonesws.webservices.jobs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Reward Notification Util to notify customer available points
 * @author kulaganathan
 *
 */
public class RTFRewardNotificationUtil extends QuartzJobBean implements StatefulJob{
	private static Logger logger = LoggerFactory.getLogger(RTFRewardNotificationUtil.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}

	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFRewardNotificationUtil.mailManager = mailManager;
	}
	
	public static void main(String[] args) {
		
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, 7);
		
		Date curDate = new Date();
		
		Date eventDate = calendar.getTime();
		
		System.out.println("Last Eamil : "+eventDate);
		System.out.println("Cur Date : "+curDate);
		System.out.println("No of Days : "+DateUtil.getDifferenceDays(curDate, eventDate));
		
	}

	public void sendRewardNotifications(){
		Calendar calender = Calendar.getInstance();
		int dayOfMonth = calender.get(Calendar.DAY_OF_MONTH);
		try{
			
		List<CustomerLoyalty> customerLoyaltyList=DAORegistry.getCustomerLoyaltyDAO().getAllCustomerLoyaltyWhichHasMorethanZeroActivePoints();
		
		for (CustomerLoyalty loyalty : customerLoyaltyList) {
			
			long noOfDays = DateUtil.getDifferenceDays(loyalty.getLastNotifiedTime(),calender.getTime());
			if(dayOfMonth > 1){
				continue;
			}
			if(noOfDays < 1){
				continue;
			}
				
			String emailDesription = "";
			
			Customer customer = CustomerUtil.getCustomerById(loyalty.getCustomerId());
			if(customer != null){
				try {
					
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("activePoints",TicketUtil.getRoundedValueString(loyalty.getActivePointsAsDouble()));
					mailMap.put("customerName",customer.getCustomerName());
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForRewardPoints();
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
							"You have $"+TicketUtil.getRoundedValueString(loyalty.getActivePointsAsDouble())+" to spend at Reward The Fan!",
				    		"email-customer-reward-information.html", mailMap, "text/html", null,mailAttachment,null);
					emailDesription = "Rewards Intimation Email Sent to "+customer.getEmail()+" this email. " +
							"Total available reward dollars are "+loyalty.getActivePoints();
				} catch (Exception e) {
					e.printStackTrace();
					emailDesription = "Error occurred while sending rewards activation email to "+customer.getEmail();
				}
				try{
					DAORegistry.getCustomerLoyaltyDAO().updateCustomerLoyaltyById(loyalty.getId(), emailDesription);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			
			String message = "You now have a total of "+TicketUtil.getRoundedValueString(loyalty.getActivePointsAsDouble())+" dollars " +
			"accumulated in your account.Remember, you can use your reward dollars towards any future ticket " +
			"purchases with each reward dollar worth $1!. You can also use your reward dollars to secure FREE Fantasy Sports Tickets" +
			" - Fantasy Sports Tickets are FREE tickets to those once in a lifetime special events like the Super Bowl, " +
			"World Series or the NBA Finals. etc. Fantasy Sports Tickets gives you, THE FAN, the potential to see your " +
			"favorite team at the big game for FREE-simply by using and referring others to Reward The Fan!";
			
			String jsonString = "";
			
			List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(loyalty.getCustomerId());
			
			if(deviceList==null || deviceList.isEmpty()){
				continue;
			}
		
			for (CustomerDeviceDetails device : deviceList) {
				
				switch (device.getApplicationPlatForm()) {
					case ANDROID:
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.MY_REWARD);
						notificationJsonObject.setMessage(message);
						notificationJsonObject.setCustomerId(loyalty.getCustomerId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						
						//gcmNotificationService.sendMessage(NotificationType.REWARD_STATUS, device.getNotificationRegId(), jsonString);
						break;
						
					case IOS:
					    Map<String, String> customFields = new HashMap<String, String>();
					    customFields.put("notificationType", String.valueOf(NotificationType.MY_REWARD));
					    customFields.put("customerId", String.valueOf(loyalty.getCustomerId()));
						
					    //apnsNotificationService.sendNotification(NotificationType.REWARD_STATUS, device.getNotificationRegId(), message,customFields);
						break;
		
					default:
						break;
				}
			}
		}
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			//sendRewardNotifications();
		}
	}
}
