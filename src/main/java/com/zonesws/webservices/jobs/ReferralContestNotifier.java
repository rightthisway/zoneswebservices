package com.zonesws.webservices.jobs;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.RTFPromotionalOfferTracking;
import com.zonesws.webservices.data.ReferralContest;
import com.zonesws.webservices.data.ReferralContestEmailVersion;
import com.zonesws.webservices.data.ReferralContestParticipants;
import com.zonesws.webservices.filter.SecurityUtil;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

public class ReferralContestNotifier extends QuartzJobBean implements StatefulJob{
	
	static MailManager mailManager = null;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		ReferralContestNotifier.mailManager = mailManager;
	}
	
	public static void main(String[] st) throws Exception{
		
		//String keyString = "RTF4#4PO==+ANDRO";
		
		decryptKey("/QaZGlHEhW8QrmYh4JKghW1pKLpRYe5xaC4ulNOHpik=");
		
	}
	
	public static String generateCotestIdentityKey(Integer participantCustId, Integer purchaserId,Integer contestId, 
			Integer purchaserOrderId){
		try {
			String keyString = "RTF4#4PO==+DESKT";
			String token = "";
			String encryptedText = null;
			token =  participantCustId+"_"+purchaserId+"_"+purchaserOrderId+"_"+contestId;
			encryptedText = SecurityUtil.doEncryption(token, keyString);
			
			System.out.println("Encrypted Text is "+ encryptedText);
			
			String decodedText1 = SecurityUtil.doDecryption(encryptedText, keyString);
			System.out.println("Original Text is "+ decodedText1.trim());
			
			return encryptedText;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String decryptKey(String key){
		try {
			String keyString = "RTF4#4PO==+DESKT";
			String decodedText1 = SecurityUtil.doDecryption(key, keyString);
			System.out.println("Original Text is "+ decodedText1.trim());
			return decodedText1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void triggerEmail(){
		
		try{
			List<ReferralContest> referralContestList = DAORegistry.getReferralContestDAO().getAllActiveContest();
			Map<Integer, ReferralContest> contestMap = new HashMap<Integer, ReferralContest>();
			for (ReferralContest referralContest : referralContestList) {
				contestMap.put(referralContest.getId(), referralContest);
			}
			
			
			Collection<ReferralContestEmailVersion> emailVersionList= DAORegistry.getReferralContestEmailVersionDAO().getAll();
			
			List<ReferralContest> referralContestListNew = new ArrayList<ReferralContest>();
			
			
			for (ReferralContestEmailVersion obj : emailVersionList) {
				
				if(obj.getPurchaserOrderId().equals(200551)){
					
				}else{
					//continue;
				}
				
				ReferralContest referralContest = contestMap.get(obj.getContestId());
				
				String encodedKey=URLEncoder.encode( obj.getContestIdentityKey(), "UTF-8" );  
				
				String url = "https://rewardthefan.com/ReferralContest?contestKey="+encodedKey;
				String anchorTag = "<a class='buttonClass' href='"+url+"'> Participate </a>";
				referralContest.setUrl(anchorTag);
				referralContestListNew.add(referralContest);
			}
			
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("contestList",referralContestListNew);
			mailMap.put("customerName","Ulaganathan Koothaiyan");
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(true);
			
			mailManager.sendMailNow("text/html",URLUtil.fromEmail, "AODev@rightthisway.com", 
					null,null, 
					"Reward The Fan Referral Contest: Choose your favorite contest to win free tickets",
		    		"email-referral-contest.html", mailMap, "text/html", null,mailAttachment,null);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void triggerEmailOld(){
		Calendar apiCal= Calendar.getInstance();
		apiCal.set(Calendar.HOUR_OF_DAY, 0);
		apiCal.set(Calendar.MINUTE, 1);
		Date fromDate = apiCal.getTime();
		apiCal.set(Calendar.HOUR_OF_DAY, 23);
		apiCal.set(Calendar.MINUTE, 59);
		Date toDate = apiCal.getTime();
		
		List<RTFPromotionalOfferTracking> list = DAORegistry.getRtfPromotionalOfferTrackingDAO().getAllCompletedReferralOrders(fromDate, toDate);
		List<ReferralContest> referralContestList = DAORegistry.getReferralContestDAO().getAllActiveContest();
		Map<Integer, ReferralContest> contestMap = new HashMap<Integer, ReferralContest>();
		for (ReferralContest referralContest : referralContestList) {
			contestMap.put(referralContest.getId(), referralContest);
		}
		
		try{
			List<ReferralContest> eligibleContestList = null;
			ReferralContestEmailVersion emailVersion = null;
			List<ReferralContestEmailVersion> contestEmailList = null;
			
			for(RTFPromotionalOfferTracking obj:list){
				
				try{
					List<ReferralContestEmailVersion> emailVersionList= DAORegistry.getReferralContestEmailVersionDAO().
					getAllContestByOrderIdandParticipantId(obj.getOrderId(), obj.getReferredBy());
					
					if(emailVersionList != null && !emailVersionList.isEmpty() && emailVersionList.size() > 0){
						System.out.println("REF-CONTEST: Already Email Triggered. OrderId: "+obj.getOrderId()+", Participant CustomerId: "+obj.getReferredBy());
						continue;
					}
					
					CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(obj.getOrderId());
					eligibleContestList = new ArrayList<ReferralContest>();
					contestEmailList = new ArrayList<ReferralContestEmailVersion>();
					
					for (ReferralContest referralContest : referralContestList) {
						
						if(customerOrder.getOrderTotalAsDouble() >= referralContest.getMinPurchaseAmount() ){
							
							emailVersion = new ReferralContestEmailVersion();
							emailVersion.setContestId(referralContest.getId());
							String key = generateCotestIdentityKey(obj.getReferredBy(), obj.getCustomerId(), referralContest.getId(), obj.getOrderId());
							emailVersion.setContestIdentityKey(key);
							emailVersion.setCreatedDate(new Date());
							emailVersion.setIsParticipated(false);
							emailVersion.setParticipantCustId(obj.getReferredBy());
							emailVersion.setPurchaserCustId(obj.getCustomerId());
							emailVersion.setPurchaserOrderId(obj.getOrderId());
							emailVersion.setUpdatedDate(new Date());
							contestEmailList.add(emailVersion);
							
							referralContest.setContestIdentityKey(key);
							
							String encodedKey=URLEncoder.encode( key, "UTF-8" );  
							String url = "https://rewardthefan.com/ReferralContest?contestKey="+encodedKey;
							//url = "http://localhost:8080/ReferralContest?contestKey="+encodedKey;
							
							//String anchorTag = "<a class='buttonClass' href='"+url+"'> Participate </a>";
							referralContest.setUrl(url);
							eligibleContestList.add(referralContest);
						}
					}
					
					if(eligibleContestList.isEmpty() || eligibleContestList.size() <=0 ){
						continue;
					}
					
					Customer customer = CustomerUtil.getCustomerById(obj.getReferredBy());
				
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("contestObj",eligibleContestList.get(0));
					//mailMap.put("artistName",eligibleContestList.get(0).getArtistName().toUpperCase());
					mailMap.put("customerName",customer.getCustomerName()+" "+customer.getLastName());
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentforContestEmail();
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.fromEmail+","+URLUtil.bccEmails,  
							"Thank You for referring a friend who just purchased tickets.",
				    		"email-referral-contest.html", mailMap, "text/html", null,mailAttachment,null);
					
					DAORegistry.getReferralContestEmailVersionDAO().saveOrUpdateAll(contestEmailList);
				
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			list = null;
			
			
			List<ReferralContestParticipants> allParticipants = DAORegistry.getReferralContestParticipantsDAO().getAllContestParticipantsToSendMail();
			if(allParticipants!=null && !allParticipants.isEmpty()){
				MailAttachment[] mailAttachment = Util.getEmailAttachmentforContestEmail();
				for(ReferralContestParticipants p : allParticipants){
					Customer customer = CustomerUtil.getCustomerById(p.getCustomerId());
					Map<String,Object> mailMap = new HashMap<String,Object>();
					ReferralContest contest = contestMap.get(p.getContestId());
					Boolean isMaiSent = false;
					if(contest == null){
						continue;
					}
					mailMap.put("contestName",contest.getName());
					mailMap.put("artistName",contest.getArtistName().toUpperCase());
					mailMap.put("customerName",customer.getCustomerName()+" "+customer.getLastName());
					
					try {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.fromEmail+","+URLUtil.bccEmails,  
								"Thank you for participating in Reward The Fan's "+contest.getArtistName().toUpperCase()+" ticket lottery.",
					    		"email-referral-contest-participation-confirmation.html", mailMap, "text/html", null,mailAttachment,null);
						isMaiSent = true;
					} catch (Exception e) {
						isMaiSent = false;
						System.out.println(e.fillInStackTrace());
					}
					if(isMaiSent){
						p.setIsEmailed(true);
						DAORegistry.getReferralContestParticipantsDAO().update(p);
					}
					
				}
			}
			
		}catch (Exception e) {
			System.out.println(e.fillInStackTrace());
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
			//triggerEmailOld();
		}
	}
}
