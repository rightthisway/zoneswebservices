package com.zonesws.webservices.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.utils.URLUtil;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;

/**
 * SVGTextUtil scheduler 
 * @author dthiyagarajan
 *
 */
public class SVGTextUtil extends QuartzJobBean implements StatefulJob{

	private static Logger logger = org.slf4j.LoggerFactory.getLogger(WebServiceUtil.class);
	private static Map<String, Boolean> svgTextMapExists = new ConcurrentHashMap<String, Boolean>();
	private static Map<String, String> svgTextMapInfo = new ConcurrentHashMap<String, String>();
	private static Map<String, String> orderedSVGMap = new ConcurrentHashMap<String, String>();
	
	 
	public static Map<String, Boolean> getSvgTextMapExists(){
		return svgTextMapExists;
	}
	
	public static Map<String, String> getSvgTextMapInfo(){
		return svgTextMapInfo;
	}
	 
	 
	
	/**
	 * Method to check whether SvgTextFile exist or not
	 * @param venueId
	 * @param venueCategoryName
	 * @return
	 * @throws FileNotFoundException
	 */
	public static Boolean checkSvgTextFileExist(Integer venueId, String venueCategoryName) throws FileNotFoundException{
		String fileName = null;
		try {
			fileName = venueId+"_"+venueCategoryName;
			Boolean fileExist = svgTextMapExists.get(fileName);
			if(null == fileExist){
				
				String url = URLUtil.getSVGTextFolderPath()+ venueId+"_"+venueCategoryName+".txt";
				
				if(URLUtil.isSharedDriveOn) {
					SmbFile file = ShareDriveUtil.getSmbFile(url);
					if(file != null && file.exists()){
						svgTextMapExists.put(venueId+"_"+venueCategoryName, true);
						return true;
					}
				}else {
					File file = new File(url);
					if(file != null && file.exists()){
						svgTextMapExists.put(venueId+"_"+venueCategoryName, true);
						return true;
					}
				}
				
				return false;
			}else{
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Method to read the SVG text file name from specified location
	 * @return
	 */
	public static List<String> readSvgFileName(){
		
		if(URLUtil.isSharedDriveOn) {
			List<String> svgTextFileList =  new ArrayList<String>();
			try {
				String url = URLUtil.rtfImageSharedPath+"Texts//";
				SmbFile dir = ShareDriveUtil.getSmbFile(url);
				String svgFileName = null;
				for (SmbFile file : dir.listFiles()) {
				    System.out.println(file.getName());
					
					if (file.isFile()) {
						svgFileName = file.getName();
						svgFileName = svgFileName.substring(0, svgFileName.lastIndexOf("."));
						svgTextFileList.add(svgFileName);
					} else{
						System.out.println("Files not exists");
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			return svgTextFileList;
		}else {
			File svgTextFolder = null;
			try {
				svgTextFolder = new File(URLUtil.svgTextFilePath);
				File[] listOfFiles = svgTextFolder.listFiles();
				
				List<String> svgTextFileList =  new ArrayList<String>();
				String svgFileName = null;
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						svgFileName = listOfFiles[i].getName();
						svgFileName = svgFileName.substring(0, svgFileName.lastIndexOf("."));
						svgTextFileList.add(svgFileName);
					} else{
						System.out.println("Files not exists");
					}
				}
				//System.out.println("svg file list :: " +svgTextFileList.size());
				return svgTextFileList;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	
	
	/**
	 * Method to read the SVG text file name from specified location
	 * @return
	 */
	public static List<String> readOrderedVenueSvgFileName(){
		if(URLUtil.isSharedDriveOn) {
			List<String> svgTextFileList =  new ArrayList<String>();
			try {
				String url = URLUtil.getStoredSVGMapPath();
				SmbFile dir = ShareDriveUtil.getSmbFile(url);
				String svgFileName = null;
				for (SmbFile file : dir.listFiles()) {
				    System.out.println(file.getName());
					if (file.isFile()) {
						svgFileName = file.getName();
						svgFileName = svgFileName.substring(0, svgFileName.lastIndexOf("."));
						svgTextFileList.add(svgFileName);
					} else{
						System.out.println("Files not exists");
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			return svgTextFileList;
		}else {
			File svgTextFolder = null;
			try {
				svgTextFolder = new File(URLUtil.getStoredSVGMapPath());
				File[] listOfFiles = svgTextFolder.listFiles();
				
				List<String> svgTextFileList =  new ArrayList<String>();
				String svgFileName = null;
				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						svgFileName = listOfFiles[i].getName();
						svgFileName = svgFileName.substring(0, svgFileName.lastIndexOf("."));
						svgTextFileList.add(svgFileName);
					} else{
						System.out.println("Files not exists");
					}
				}
				//System.out.println("svg file list :: " +svgTextFileList.size());
				return svgTextFileList;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	
	
	/**
	 * Test method to get the svg text details for the file name
	 * @param venueId
	 * @param venueCategoryName
	 * @return
	 */
	public static String testGetSvgTextDetails(Integer venueId, String venueCategoryName){
		
		if(URLUtil.isSharedDriveOn) {
			BufferedReader reader = null;
			String svgText = "";
			try{
				String url = URLUtil.getSVGTextFolderPath()+ venueId+"_"+venueCategoryName+".txt";
				SmbFile svgTxtFile = ShareDriveUtil.getSmbFile(url);
				
				if(svgTxtFile != null && svgTxtFile.exists()){
					reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(svgTxtFile)));
				    String line = reader.readLine();
				    while (line != null) {
				        line = reader.readLine();
				        svgText += line;
				    }
				}
				return svgText;
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}finally{
				try {
					if(null !=reader){
						reader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}else {
			FileReader fReader = null;
			BufferedReader reader = null;
			String svgText = "",str="";
			try{
				File svgTxtFile = new File(URLUtil.svgTextFilePath + venueId+"_"+ venueCategoryName+".txt");
				if(svgTxtFile != null && svgTxtFile.exists()){
					fReader = new FileReader(svgTxtFile);
					reader = new BufferedReader(fReader);
					while((str = reader.readLine()) != null){
						svgText += str;
					}
				}
				return svgText;

			}catch(Exception e){
				e.printStackTrace();
				return null;
			}finally{
				try {
					if(null !=reader){
						reader.close();
						fReader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	
	/**
	 * Method to get the SVG text details
	 * @param venueId
	 * @param venueCategoryName
	 * @return
	 */
	public static String getSvgTextDetails(Integer venueId, String venueCategoryName){
		String svgText = null;
		String fileName = null;
		try {
			fileName = venueId+"_"+venueCategoryName;
			if(svgTextMapInfo == null) {
				System.err.println("svgTextMapInfo is Null : "+new Date());
				return null;
			}
			svgText = svgTextMapInfo.get(fileName);
			if(svgText != null && !svgText.isEmpty()){
				return svgText;
			}else{
				
				if(URLUtil.isSharedDriveOn) {
					BufferedReader reader = null;
					String str="";
					svgText="";
					try{
						String url = URLUtil.getSVGTextFolderPath()+ venueId+"_"+venueCategoryName+".txt";
						SmbFile svgTxtFile = ShareDriveUtil.getSmbFile(url);
						
						if(svgTxtFile != null && svgTxtFile.exists()){
							reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(svgTxtFile)));
						    String line = reader.readLine();
						    while (line != null) {
						        line = reader.readLine();
						        svgText += line;
						    }
						    
						    svgTextMapInfo.put(fileName, svgText);
						}
						return svgText;

					}catch(Exception e){
						e.printStackTrace();
						return null;
					}finally{
						try {
							if(null !=reader){
								reader.close();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}else {
					FileReader fReader = null;
					BufferedReader reader = null;
					String str="";
					svgText="";
					try{
						File svgTxtFile = new File(URLUtil.svgTextFilePath + venueId+"_"+ venueCategoryName+".txt");
						if(svgTxtFile != null && svgTxtFile.exists()){
							svgText = "";
							fReader = new FileReader(svgTxtFile);
							reader = new BufferedReader(fReader);
							while((str = reader.readLine()) != null){
								svgText += str;
							}
							svgTextMapInfo.put(fileName, svgText);
						}
						return svgText;

					}catch(Exception e){
						e.printStackTrace();
						return null;
					}finally{
						try {
							if(null !=reader){
								reader.close();
								fReader.close();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	/**
	 * Init method to start the scheduler process
	 */
	public static void initSMBFileNotUsing(){
		boolean isSvgTextFileExists = false;
		SmbFile svgTextFile = null;
		FileReader fReader = null;
		BufferedReader reader = null;
		String svgText = null;
		String str= null;
		try {
			Long startTime = System.currentTimeMillis();
			logger.debug("SvgTextUtil scheduler start time :: " +startTime);
			
			Map<String, Boolean> tempSvgTextFileExist = new HashMap<String, Boolean>();
			Map<String, String> tempSvgTextDetails = new HashMap<String, String>();
			
			List<String> svgTextFileList = readSvgFileName();
			
			for(String fileName : svgTextFileList){
				
				String url = URLUtil.getSVGTextFolderPath()+ fileName+".txt";
				svgTextFile = ShareDriveUtil.getSmbFile(url);
				
				isSvgTextFileExists = svgTextFile.exists() && !svgTextFile.isDirectory() && svgTextFile.isFile();
				
				if(svgTextFile != null && svgTextFile.exists()){
					reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(svgTextFile)));
				    String line = reader.readLine();
				    svgText= "";
				    while (line != null) {
				        line = reader.readLine();
				        svgText += line;
				    }
				    tempSvgTextDetails.put(fileName, svgText);
					tempSvgTextFileExist.put(fileName, Boolean.TRUE);
				}else{
					tempSvgTextFileExist.put(fileName, Boolean.FALSE);
				}
			}
			
			svgTextMapExists.clear();
			//svgTextMapExists = null;
			svgTextMapExists = new HashMap<String, Boolean>(tempSvgTextFileExist);

			svgTextMapInfo.clear();
			//svgTextMapInfo = null;
			svgTextMapInfo = new HashMap<String, String>(tempSvgTextDetails);
			
			
			List<String> orderSvgTextFileList = readOrderedVenueSvgFileName();
			tempSvgTextFileExist = new HashMap<String, Boolean>();
			tempSvgTextDetails = new HashMap<String, String>();
			for(String fileName : orderSvgTextFileList){
				
				String url = URLUtil.getStoredSVGMapPath()+ fileName+".txt";
				svgTextFile = ShareDriveUtil.getSmbFile(url);
				
				isSvgTextFileExists = svgTextFile.exists() && !svgTextFile.isDirectory() && svgTextFile.isFile();
				
				if(svgTextFile != null && svgTextFile.exists()){
					reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(svgTextFile)));
				    String line = reader.readLine();
				    svgText= "";
				    while (line != null) {
				        line = reader.readLine();
				        svgText += line;
				    }
				    tempSvgTextDetails.put(fileName, svgText);
					tempSvgTextFileExist.put(fileName, Boolean.TRUE);
				}else{
					tempSvgTextFileExist.put(fileName, Boolean.FALSE);
				}
				 
			}
			orderedSVGMap.clear();
			orderedSVGMap = new HashMap<String, String>(tempSvgTextDetails);
			logger.debug("TIME TO LAST LAST SVG TEXT UTIlS=" + (System.currentTimeMillis() - startTime) / 1000);
			System.out.println("TIME TO LAST SVG TEXT UTIlS=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(null !=reader){
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void initRegularFileNotUsing(){
		boolean isSvgTextFileExists = false;
		File svgTextFile = null;
		FileReader fReader = null;
		BufferedReader reader = null;
		String svgText = null;
		String str= null;
		try {
			Long startTime = System.currentTimeMillis();
			logger.debug("SvgTextUtil scheduler start time :: " +startTime);
			
			Map<String, Boolean> tempSvgTextFileExist = new HashMap<String, Boolean>();
			Map<String, String> tempSvgTextDetails = new HashMap<String, String>();
			
			List<String> svgTextFileList = readSvgFileName();
			
			for(String fileName : svgTextFileList){
				
				svgTextFile = new File(URLUtil.svgTextFilePath + fileName + ".txt");
				isSvgTextFileExists = svgTextFile.exists() && !svgTextFile.isDirectory() && svgTextFile.isFile();
				
				if (svgTextFile != null && svgTextFile.exists()) {
					fReader = new FileReader(svgTextFile);
					reader = new BufferedReader(fReader);
					svgText= "";
					str = "";
					while ((str = reader.readLine()) != null) {
						svgText += str;
					}
					tempSvgTextDetails.put(fileName, svgText);
					tempSvgTextFileExist.put(fileName, Boolean.TRUE);
				}else{
					tempSvgTextFileExist.put(fileName, Boolean.FALSE);
				}
			}
			
			svgTextMapExists.clear();
			//svgTextMapExists = null;
			svgTextMapExists = new HashMap<String, Boolean>(tempSvgTextFileExist);

			svgTextMapInfo.clear();
			//svgTextMapInfo = null;
			svgTextMapInfo = new HashMap<String, String>(tempSvgTextDetails);
			
			
			List<String> orderSvgTextFileList = readOrderedVenueSvgFileName();
			tempSvgTextFileExist = new HashMap<String, Boolean>();
			tempSvgTextDetails = new HashMap<String, String>();
			for(String fileName : orderSvgTextFileList){
				svgTextFile = new File(URLUtil.getStoredSVGMapPath()+fileName+".txt");
				isSvgTextFileExists = svgTextFile.exists() && !svgTextFile.isDirectory() && svgTextFile.isFile();
				if (svgTextFile != null && svgTextFile.exists()) {
					fReader = new FileReader(svgTextFile);
					reader = new BufferedReader(fReader);
					svgText= "";
					str = "";
					while ((str = reader.readLine()) != null) {
						svgText += str;
					}
					tempSvgTextDetails.put(fileName, svgText);
					tempSvgTextFileExist.put(fileName, Boolean.TRUE);
				}else{
					tempSvgTextFileExist.put(fileName, Boolean.FALSE);
				}
			}
			orderedSVGMap.clear();
			orderedSVGMap = new HashMap<String, String>(tempSvgTextDetails);
			logger.debug("TIME TO LAST LAST SVG TEXT UTIlS=" + (System.currentTimeMillis() - startTime) / 1000);
			System.out.println("TIME TO LAST SVG TEXT UTIlS=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(null !=reader){
					reader.close();
					fReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void init(){
		/*if(URLUtil.isSharedDriveOn) {
			initSMBFile();
		}else {
			initRegularFile();
		}*/
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}
	
	
	
	/**
	 * Method to get the SVG text details
	 * @param venueId
	 * @param venueCategoryName
	 * @return
	 */
	public static String getOrderSvgTextDetails(Integer venueId, String venueCategoryName,Integer orderId){
		String svgText = null;
		String fileName = null;
		try {
			fileName = venueId+"_"+venueCategoryName;
			if(orderedSVGMap == null) {
				System.err.println("orderedSVGMap is Null : "+new Date());
				return null;
			}
			svgText = orderedSVGMap.get(fileName);
			if(svgText != null && !svgText.isEmpty()){
				return svgText;
			}else{
				
				if(URLUtil.isSharedDriveOn) {
					FileReader fReader = null;
					BufferedReader reader = null;
					String str="";
					svgText="";
					try{
						String url = URLUtil.getStoredSVGMapPath()+ fileName+".txt";
						SmbFile svgTextFile = ShareDriveUtil.getSmbFile(url);
						
						if(svgTextFile != null && svgTextFile.exists()){
							reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(svgTextFile)));
						    String line = reader.readLine();
						    svgText= "";
						    while (line != null) {
						        line = reader.readLine();
						        svgText += line;
						    }
							orderedSVGMap.put(fileName, svgText);
						}
						return svgText;

					}catch(Exception e){
						e.printStackTrace();
						return null;
					}finally{
						try {
							if(null !=reader){
								reader.close();
								fReader.close();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}else {
					FileReader fReader = null;
					BufferedReader reader = null;
					String str="";
					svgText="";
					try{
						File svgTxtFile = new File(URLUtil.getStoredSVGMapPath() + venueId+"_"+ venueCategoryName+"_"+orderId+".txt");
						if(svgTxtFile != null && svgTxtFile.exists()){
							svgText = "";
							fReader = new FileReader(svgTxtFile);
							reader = new BufferedReader(fReader);
							while((str = reader.readLine()) != null){
								svgText += str;
							}
							orderedSVGMap.put(fileName, svgText);
						}
						return svgText;

					}catch(Exception e){
						e.printStackTrace();
						return null;
					}finally{
						try {
							if(null !=reader){
								reader.close();
								fReader.close();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

}