package com.zonesws.webservices.jobs;

import java.io.FileInputStream;

import com.zonesws.webservices.utils.URLUtil;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

public class ShareDriveUtil {
	
	public static NtlmPasswordAuthentication connect() {
		 NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, URLUtil.rtfSharedDriveUserName, URLUtil.rtfSharedDrivePassword);
		 return auth;
	}
	
	public static SmbFile getSmbFile(String fileUrl) {
		try {
			SmbFile file = new SmbFile(fileUrl, ShareDriveUtil.connect());
			
			return file;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static void main(String[] args) throws Exception {
		
		String host = "smb://52.201.48.95//d$//Rewardthefan//REWARDTHEFAN_DP_334437.jpg";
		URLUtil.rtfSharedDriveUserName = "dev";
		URLUtil.rtfSharedDrivePassword = "Rb#32H8!";
		
		
		SmbFile file = getSmbFile(host);
		
		
		file.createNewFile();
		
		
		System.out.println(file.getName());
		
		FileInputStream in = new FileInputStream(file.getName());
	    SmbFileOutputStream out = new SmbFileOutputStream(file);
	    long t0 = System.currentTimeMillis();
	    byte[] b = new byte[8192];
	    int n, tot = 0;
	    while ((n = in.read(b)) > 0) {
	        out.write(b, 0, n);
	        tot += n;
	        System.out.print('#');
	    }
	    long t = System.currentTimeMillis() - t0;
	    System.out.println();
	    System.out.println(tot + " bytes transfered in " + (t / 1000) + " seconds at " + ((tot / 1000) / Math.max(1, (t / 1000))) + "Kbytes/sec");
	    in.close();
	    out.close();
		
	}
	
	public static void mainOld(String argv[]) throws Exception {
	    SmbFile f = new SmbFile(argv[0]);
	    FileInputStream in = new FileInputStream(f.getName());
	    SmbFileOutputStream out = new SmbFileOutputStream(f);
	    long t0 = System.currentTimeMillis();
	    byte[] b = new byte[8192];
	    int n, tot = 0;
	    while ((n = in.read(b)) > 0) {
	        out.write(b, 0, n);
	        tot += n;
	        System.out.print('#');
	    }
	    long t = System.currentTimeMillis() - t0;
	    System.out.println();
	    System.out.println(tot + " bytes transfered in " + (t / 1000) + " seconds at " + ((tot / 1000) / Math.max(1, (t / 1000))) + "Kbytes/sec");
	    in.close();
	    out.close();
	}
	
	
	 

}
