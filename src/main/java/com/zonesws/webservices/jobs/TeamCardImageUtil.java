package com.zonesws.webservices.jobs;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.list.TeamCardImageDetails;

/**
 * Team Card Image util to get the artist image details 
 * @author dthiyagarajan
 *
 */
public class TeamCardImageUtil extends QuartzJobBean implements StatefulJob{

	private static Map<Integer, String> artistTeamCardImageMap = new ConcurrentHashMap<Integer, String>();
	private static String basePath=URLUtil.basePath+"SvgMaps//";
	private static String rewardTheFanServerSvgMapsBaseUrl = URLUtil.rewardTheFanServerSvgMapsBaseUrl;
	private static String apiServerSvgMapsBaseURL = URLUtil.apiServerSvgMapsBaseURL;
	
	
	/*public static String getTeamCardImageByArtist(Integer artistId){
		String teamCardImageUrl = artistTeamCardImageMap.get(artistId);
		try {
			if(teamCardImageUrl == null){
				List<TeamCardImageDetails> teamCardImageDetails = DAORegistry.getQueryManagerDAO().getCardImage(artistId);
				if(teamCardImageDetails != null && teamCardImageDetails.size()>0){
					TeamCardImageDetails teamCardImage = teamCardImageDetails.get(0);
					teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(true, false, false, false, teamCardImage.getArtistImage());
					if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
						teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, true, false, false, teamCardImage.getGrandChildImage());
						if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
							teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, true, false, teamCardImage.getChildImage());
							if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
								teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, false, true, teamCardImage.getParentImage());
							}
						}
					}
					artistTeamCardImageMap.put(teamCardImage.getArtistId(), teamCardImageUrl);
				}
			}
			return teamCardImageUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/
	
	public static String getPrarentCategoryImage(Integer parentCatId ){
		//ParentCategoryImage_4.jpg
		return apiServerSvgMapsBaseURL+"ParentCategoryImage/ParentCategoryImage_"+parentCatId+".jpg";
		
	}
	
	public static String getTeamCardImageByArtist(Integer artistId,ApplicationPlatForm platform){
		String teamCardImageUrl = artistTeamCardImageMap.get(artistId);
		try {
			if(teamCardImageUrl == null){
				List<TeamCardImageDetails> teamCardImageDetails = DAORegistry.getQueryManagerDAO().getCardImage(artistId);
				
				if(teamCardImageDetails != null && teamCardImageDetails.size()>0){
					
					TeamCardImageDetails teamCardImage = teamCardImageDetails.get(0);
					//teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(true, false, false, false, teamCardImage.getArtistImage());
					
					teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, false, true, teamCardImage.getParentImage());
					
					/*if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
						teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, false, true, teamCardImage.getParentImage());
					}*/
					artistTeamCardImageMap.put(teamCardImage.getArtistId(), teamCardImageUrl);
				}
				/*if(teamCardImageDetails != null && teamCardImageDetails.size()>0){
					TeamCardImageDetails teamCardImage = teamCardImageDetails.get(0);
					teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(true, false, false, false, teamCardImage.getArtistImage());
					if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
						teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, true, false, false, teamCardImage.getGrandChildImage());
						if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
							teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, true, false, teamCardImage.getChildImage());
							if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
								teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, false, true, teamCardImage.getParentImage());
							}
						}
					}
					artistTeamCardImageMap.put(teamCardImage.getArtistId(), teamCardImageUrl);
				}*/
			}
			if(platform.equals(ApplicationPlatForm.DESKTOP_SITE)){
				teamCardImageUrl = rewardTheFanServerSvgMapsBaseUrl+teamCardImageUrl;
			}else{
				teamCardImageUrl = apiServerSvgMapsBaseURL + teamCardImageUrl;
			}
			return teamCardImageUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getTeamCardImage(boolean artistFlag,boolean grandChildFlag,boolean childFlag,boolean parentFlag,String imageName){
		String url ="";
		
		if(null == imageName || imageName.isEmpty()){
			return url;
		}
		if(artistFlag){
			url = TeamCardImageUtil.getartistImage(imageName);
		}/*else if (grandChildFlag){
			url = TeamCardImageUtil.getGrandChildCategoryImage(imageName);
		}else if (childFlag){
			url = TeamCardImageUtil.getChildCategoryImage(imageName);
		}*/else {
			url = TeamCardImageUtil.getParentCategoryImage(imageName);
		}
		return url;
	}
	
	//initialize the scheduler to start
	public static void init(/*List<TeamCardImageDetails> teamCardImageDetails*/){
		try {
			Long startTime = System.currentTimeMillis();
			
			//map to persist the artist card image details
			Map<Integer, String> tempArtistCardImageMap = new HashMap<Integer, String>();
			Collection<TeamCardImageDetails> teamCardImageDetails = DAORegistry.getQueryManagerDAO().getCardImage();
			String teamCardImageUrl = "";
			for(TeamCardImageDetails teamCardImage : teamCardImageDetails){
				if(teamCardImage != null){
					teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(true, false, false, false, teamCardImage.getArtistImage());
					if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
						teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, true, false, false, teamCardImage.getGrandChildImage());
						if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
							teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, true, false, teamCardImage.getChildImage());
							if(teamCardImageUrl == null || teamCardImageUrl.isEmpty() || teamCardImageUrl.equals("")){
								teamCardImageUrl = TeamCardImageUtil.getTeamCardImage(false, false, false, true, teamCardImage.getParentImage());
							}
						}
					}
					tempArtistCardImageMap.put(teamCardImage.getArtistId(), teamCardImageUrl);
				}
			}
			artistTeamCardImageMap.clear();
			artistTeamCardImageMap = null;
			artistTeamCardImageMap = new HashMap<Integer, String>(tempArtistCardImageMap);
			System.out.println("TIME TO LAST TEAM CARD IMAGE UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to read the getGrandChildCategoryImage from specified location
	 * @return
	 */
	public static String getGrandChildCategoryImage(String grandChildCategory){
		File grandChildCategoryImgFolder = null;
		String grandChildCategoryImg = null;
		boolean fileExists = false;
		try {
			grandChildCategoryImgFolder = new File(basePath+"GrandChildCategoryImage//");
			fileExists = new File(grandChildCategoryImgFolder, grandChildCategory).exists();
			if(fileExists == true){
				grandChildCategoryImg = "GrandChildCategoryImage/"+grandChildCategory;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return grandChildCategoryImg;
	}
	
	/**
	 * Method to get the ChildCategoryImage from specified location
	 * @return
	 */
	public static String getChildCategoryImage(String childCategory){
		File grandChildCategoryImgFolder = null;
		String childCategoryImg = null;
		boolean fileExists = false;
		try {
			grandChildCategoryImgFolder = new File(basePath+"ChildCategoryImage//");
			fileExists = new File(grandChildCategoryImgFolder, childCategory).exists();
			if(fileExists == true){
				childCategoryImg = "ChildCategoryImage/"+childCategory;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return childCategoryImg;
	}
	
	/**
	 * Method to get the ParentCategoryImage from specified location
	 * @return
	 */
	public static String getParentCategoryImage(String parentCategory){
		File grandChildCategoryImgFolder = null;
		String parentCategoryImg = null;
		boolean fileExists = false;
		try {
			grandChildCategoryImgFolder = new File(basePath+"ParentCategoryImage//");
			fileExists = new File(grandChildCategoryImgFolder, parentCategory).exists();
			if(fileExists == true){
				parentCategoryImg = "ParentCategoryImage/"+parentCategory;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return parentCategoryImg;
	}
	
	/**
	 * Method to get the artist image from specified location
	 * @return
	 */
	public static String getartistImage(String artistImage){
		File grandChildCategoryImgFolder = null;
		String artistImg = null;
		boolean fileExists = false;
		try {
			grandChildCategoryImgFolder = new File(basePath+"ArtistImage//"+artistImage);
			if(grandChildCategoryImgFolder != null){
				fileExists = grandChildCategoryImgFolder.exists();
				if(fileExists == true){
					artistImg = "ArtistImage/"+artistImage;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return artistImg;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}
	
	//Unit testing code
	public static void main(String[] args){
		TeamCardImageDetails teamCardImageDetails = new TeamCardImageDetails();
		List<TeamCardImageDetails> list = new ArrayList<TeamCardImageDetails>();
		
		teamCardImageDetails.setArtistId(2795);
		teamCardImageDetails.setArtistImage(null);
		teamCardImageDetails.setGrandChildCategoryId(51);
		teamCardImageDetails.setGrandChildImage("GrandChildCategoryImage_118.jpg");
		teamCardImageDetails.setChildCategoryId(50);
		teamCardImageDetails.setChildImage("ChildCategoryImage_1.jpg");
		teamCardImageDetails.setParentCategoryId(3);
		teamCardImageDetails.setParentImage("ParentCategoryImage_3.jpg");
		
		list.add(teamCardImageDetails);
		
		//init(list);
		
		
	}

}
