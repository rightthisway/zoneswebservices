package com.zonesws.webservices.jobs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.PromotionalType;
import com.zonesws.webservices.filter.ReferralCodeGenerator;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;



public class TriggerCustomerPromotionalCode extends QuartzJobBean implements StatefulJob {
	private static Logger logger = LoggerFactory.getLogger(TriggerCustomerPromotionalCode.class);
	static MailManager mailManager = null;
	
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		TriggerCustomerPromotionalCode.mailManager = mailManager;
	}


	
	/*public static void activateRewardPoints(){
		try{
			
			Customer customer = CustomerUtil.getCustomerById(334408, ProductType.REWARDTHEFAN);
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("promotionalCode",ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+customer.getEmail().split("@")[0]));
			mailMap.put("userName",customer.getEmail());
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = new MailAttachment[1];
			String filePath = null;
			//byte[] fileContent = Util.convertFiletoByteArray(filePath, "png");
			mailAttachment[0] = new MailAttachment(null,"image/png","logo_white.png",URLUtil.getWhiteLogoImage());
			//mailAttachment[1] = new MailAttachment(null,"image/png","playbutton.png",URLUtil.fetchIcons("playbutton"));
			mailAttachment[2] = new MailAttachment(null,"image/png","instagram.png",URLUtil.fetchIcons("instagram"));
			mailAttachment[3] = new MailAttachment(null,"image/png","linkedin.png",URLUtil.fetchIcons("linkedin"));
			mailAttachment[4] = new MailAttachment(null,"image/png","pinterest.png",URLUtil.fetchIcons("pinterest"));
			mailAttachment[5] = new MailAttachment(null,"image/png","twitter.png",URLUtil.fetchIcons("twitter"));
			
			
			
			
			mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
					null,"AODev@rightthisway.com", "Your promotional code for discount on your first purchase",
		    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,filePath);
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	
	public static void main(String[] args) {
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.DATE, 15);
		
		System.out.println(calendar.getTime());
		
		
	}
	
	public static void triggerEmail(){
		try{
			
			List<Integer> customerIds = new ArrayList<Integer>();
			
			customerIds.add(59679);
			
			RTFCustomerPromotionalOffer custPromoOffer = null;
			
			Calendar calendar = new GregorianCalendar();
			calendar.set(Calendar.DATE, 30);
			
			Integer size = customerIds.size();
			int i =1,orderHas = 0,exceptionOccured=0;
			
			for (Integer custId : customerIds) {
				
				try{
					Customer customer = CustomerUtil.getCustomerById(custId, ProductType.REWARDTHEFAN);
					
					System.out.println("Total Size :"+size+" , Current Size : "+i+", OrderCreated : "+orderHas+", Exception Occured :"+exceptionOccured);
					
					if(null != customer){
						
						Integer orderCount = DAORegistry.getQueryManagerDAO().getOrderCount(customer.getId());
						if(null != orderCount && orderCount > 0){
							orderHas++;
							//continue;
						}
						custPromoOffer = new RTFCustomerPromotionalOffer();
						custPromoOffer.setCreatedDate(new Date());
						custPromoOffer.setCustomerId(customer.getId());
						custPromoOffer.setDiscount(25.00);
						custPromoOffer.setStartDate(new Date());
						custPromoOffer.setEndDate(calendar.getTime());
						custPromoOffer.setFlatOfferOrderThreshold(1.00);
						custPromoOffer.setIsFlatDiscount(false);
						custPromoOffer.setMaxOrders(1);
						custPromoOffer.setModifiedDate(new Date());
						String promoCode = ReferralCodeGenerator.generateCustomerPromotionalCode(customer.getId()+""+customer.getEmail().split("@")[0]);
						
						custPromoOffer.setPromoCode(promoCode);
						custPromoOffer.setPromoType(PromotionalType.CUSTOMER_PROMO);
						custPromoOffer.setNoOfOrders(0);
						custPromoOffer.setStatus("ENABLED");
						boolean isEmailSent = true;
						String toEmail = customer.getEmail();
						System.out.println(toEmail+"============"+promoCode);
						try{
							Map<String,Object> mailMap = new HashMap<String,Object>();
							mailMap.put("promotionalCode",promoCode);
							mailMap.put("userName",customer.getEmail());
							//inline(image in mail body) image add code
							MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(false);
							
							mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
									null,"sales@rewardthefan.com,AODev@rightthisway.com", "Code for 25% OFF on your first purchase on REWARDTHEFAN",
						    		"mail-rewardfan-customer-promo-offer.html", mailMap, "text/html", null,mailAttachment,null);
							
							/*mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
									null,"sales@rewardthefan.com,AODev@rightthisway.com,", "Code for 25% OFF on your first purchase on REWARDTHEFAN",
						    		"mail-rewardfan-customer-welcome.html", mailMap, "text/html", null,mailAttachment,filePath);*/
							
						}catch(Exception e){
							e.printStackTrace();
							isEmailSent = false;
							toEmail ="";
						}
						custPromoOffer.setIsEmailSent(isEmailSent);
						custPromoOffer.setToEmail(toEmail);
						System.out.println(customer.getId()+"======"+customer.getEmail());
						DAORegistry.getRtfCustomerPromotionalOfferDAO().save(custPromoOffer);
					}
					i++;
					
				}catch(Exception e){
					e.printStackTrace();
					exceptionOccured++;
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		//triggerEmail();
	}
	
}
