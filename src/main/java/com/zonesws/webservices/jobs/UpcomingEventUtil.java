package com.zonesws.webservices.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Event;

/**
 * 
 * @author kulaganathan
 *
 */
public class UpcomingEventUtil extends QuartzJobBean implements StatefulJob{
	
	private static Map<Integer, Event> eventMapById = new ConcurrentHashMap<Integer, Event>();
	private static Map<Integer, Event> fantasyTicketsEventMap = new ConcurrentHashMap<Integer, Event>();
	private static List<Event> upcomingEvents = new ArrayList<Event>();
	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private static DateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	/**
	 * Method to get the event info 
	 * by event id
	 * @param eventId
	 * @return
	 */
	/*public static Event getEventById(Integer eventId){
		Event event = eventMapById.get(eventId);
		try {
			if(event == null){
				event = DAORegistry.getEventDAO().get(eventId);
				if(event != null){
					eventMapById.put(eventId, event);
				}
			}
			return event;	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/
	
	/*public static List<Event> getUpcomingEvents(){
		if(null == upcomingEvents || upcomingEvents.isEmpty()){
			Calendar calendar = new GregorianCalendar();
			calendar.add(Calendar.DAY_OF_MONTH, 2);
			List<Event> events = DAORegistry.getEventDAO().getUpcomingEvents(1,5, new Date(), calendar.getTime());	
			Collections.sort(events, UpcomingEventUtil.eventComparatorFordate);
			upcomingEvents.clear();
			upcomingEvents = null;
			upcomingEvents = new ArrayList<Event>(events);
		}else{
			
			
		}
		return upcomingEvents;
	}*/
	
	public static void main(String[] args) {
		List<String> names = new ArrayList<String>();
		
		for (int i = 1; i <= 25; i++) {
			names.add("Ulagnathan_"+i);
		}
		System.out.println(names.size());
		
		List<String> tempList = names.subList(1, 5);
		
		for (String string : tempList) {
			System.out.println(string);
		}
	}
	
	public static List<Event> getNext5UpcomingEvents(){
		List<Event> eventList=new ArrayList<Event>();
		return eventList;
	}
	
	public static List<Event> getNext5UpcomingEventsOld(){
		
		
		if(upcomingEvents != null && !upcomingEvents.isEmpty()){
			List<Event> temEventList = new ArrayList<Event>(upcomingEvents);
			
			List<Event> eventList=new ArrayList<Event>();
			
			for (Event event : temEventList) {
				if(event.getTicketMinPrice() > 0){
					eventList.add(event);
				}
			}
			
			try{
				System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
				Collections.sort(eventList, eventComparatorFordate);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			Integer startIndex=0;
			Integer endIndex=5;
			if(endIndex > eventList.size()-1){
				endIndex = eventList.size()-1;
			}
			eventList = eventList.subList(startIndex, endIndex);
			return eventList;
		}else{
			
			
			List<Event> events=DAORegistry.getEventDAO().getUpcomingEvents();
		
			if(null != events && !events.isEmpty()){
				upcomingEvents = new ArrayList<Event>(events);
				return getNext5UpcomingEvents();
			}else{
				return null;
			}
		}
	}
	
	
	public static List<Event> getAllUpcomingEvents(Integer pageNumber , Integer maxRows){
		
		if(upcomingEvents != null && !upcomingEvents.isEmpty()){
			List<Event> temEventList = new ArrayList<Event>(upcomingEvents);
			Collections.sort(temEventList, eventComparatorFordate);
			Integer startIndex=(pageNumber -1 ) * maxRows;
			Integer endIndex=(pageNumber * maxRows )-1;
			if(endIndex > upcomingEvents.size()-1){
				endIndex = upcomingEvents.size()-1;
			}
			temEventList = temEventList.subList(startIndex, endIndex);
			return temEventList;
		}else{
			List<Event> events=DAORegistry.getEventDAO().getUpcomingEvents();
			if(null != events && !events.isEmpty()){
				upcomingEvents = new ArrayList<Event>(events);
				return getAllUpcomingEvents(pageNumber,maxRows);
			}else{
				return null;
			}
			
			
		}
	}	
		public static List<Event> getAllPopularEvents(Integer pageNumber , Integer maxRows,Date eventStartDate,Date eventEndDate){
			if(upcomingEvents != null && !upcomingEvents.isEmpty()){
				List<Event> temEventList = new ArrayList<Event>(upcomingEvents);
				Collections.sort(temEventList, eventComparatorForsaleount);
				Integer startIndex=(pageNumber -1 ) * maxRows;
				Integer endIndex=(pageNumber * maxRows )-1;
				if(endIndex > upcomingEvents.size()-1){
					endIndex = upcomingEvents.size()-1;
				}
				temEventList = temEventList.subList(startIndex, endIndex);
				return temEventList;
			}else{
				List<Event> events=DAORegistry.getEventDAO().getUpcomingEvents();
				if(null != events && !events.isEmpty()){
					upcomingEvents = new ArrayList<Event>(events);
					return getAllPopularEvents(pageNumber, maxRows, eventStartDate, eventEndDate);
				}else{
					return null;
				}
			}
	}

	public static Comparator<Event> eventComparatorFordate = new Comparator<Event>() {

		public int compare(Event event1, Event event2) {
			try{
				
				Date event1Date = null, event2Date = null;
				
				try{
					String eventDate1Str = event1.getEventDateTime();
					String eventDate2Str = event2.getEventDateTime();
					if(eventDate1Str.contains("TBD")){
						eventDate1Str = eventDate1Str.replaceAll("TBD", "00:01 PM");
					}
					if(eventDate2Str.contains("TBD")){
						eventDate2Str = eventDate2Str.replaceAll("TBD", "00:01 PM");
					}
					
					event1Date = dateTimeFormat.parse(eventDate1Str);
					event2Date = dateTimeFormat.parse(eventDate2Str);
				}catch (Exception e) {
					return event1.getEventId().compareTo(event2.getEventId());
				}
				
			
				int cmp =event1Date.compareTo(
						event2Date);
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				
				if (cmp == 0) {
					return 1;
				}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				return event1.getEventId().compareTo(event2.getEventId());
		    }};
	
	    public static Comparator<Event> eventComparatorForsaleount = new Comparator<Event>() {

			public int compare(Event event1, Event event2) {
				int cmp = event1.getSalesCount().compareTo(
						event2.getSalesCount());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				return event1.getEventId().compareTo(event2.getEventId());
		    }};
	/**
	 * Method to initiate the scheduler process
	 */
	public static void init(){
		
		try {
			Long startTime = System.currentTimeMillis();
			
			
			//Map to persist the event info into cache
			Map<Integer, Event> tempEventMapById = new ConcurrentHashMap<Integer, Event>();
			
			
			//Collection of events
			Collection<Event> events = DAORegistry.getEventDAO().getUpcomingEvents();
			
			for(Event event : events){
				tempEventMapById.put(event.getEventId(), event);
			}
			
			eventMapById.clear();
			eventMapById = null;
			eventMapById = new HashMap<Integer, Event>(tempEventMapById);
			
			upcomingEvents = new ArrayList<Event>(events);
			
			
			System.out.println("TIME TO LAST EVENT UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}

}
