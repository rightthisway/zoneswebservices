package com.zonesws.webservices.jobs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.rtfquiz.webservices.utils.QuizContestUtil;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Country;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.State;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * 
 * @author Tamil
 *
 */
public class UserIdCreationScheduler extends QuartzJobBean implements StatefulJob{
	
	static MailManager mailManager = null;
	
	public static MailManager getMailManager() {
		return mailManager;
	}
	static boolean isRunning = false;

	public void setMailManager(MailManager mailManager) {
		UserIdCreationScheduler.mailManager = mailManager;
	}

	public static void createcustomerUserIds() {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -3);
		Date toDate = new Date(cal.getTimeInMillis());
		Integer count = 0,processod=0,error=0,emptyUser = 0;
		//List<Customer> customers = DAORegistry.getCustomerDAO().getAllCustomersWithoutUserId(ProductType.REWARDTHEFAN);
		List<Customer> customers = DAORegistry.getCustomerDAO().getAllCustomersWithoutUserIdForAutoUserIDCreation(ProductType.REWARDTHEFAN,toDate);
		if(customers != null) {
			count = customers.size();
			for (Customer customer : customers) {
				try {
					String userId = generateCustomerUserId(customer.getCustomerName(), customer.getLastName(), customer.getEmail());
					if(userId != null && !userId.equals("")) {
						customer.setUserId(userId.toLowerCase());
						DAORegistry.getCustomerDAO().saveOrUpdate(customer);
						CustomerUtil.updatedCustomerUtil(customer);
						processod++;
					} else {
						emptyUser++;
					}
				} catch(Exception e) {
					e.printStackTrace();
					error++;
				}
			}
		}
		if(error > 0 || emptyUser > 0) {
			System.out.println("Error Occured While Generating USerId : "+count+" :err: "+error+" :empty: "+emptyUser+" :pros: "+processod+" : "+new Date());
			System.err.println("Error Occured While Generating USerId : "+count+" :err: "+error+" :empty: "+emptyUser+" :pros: "+processod+" : "+new Date());
			/*Map<String,Object> mailMap = new HashMap<String,Object>();
			
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);		
			try {
				mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
						null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
						"Error Occured While Generating UserIDs :" +customerOrder.getId() ,
			    		"email-order-delivery-information.html", mailMap, "text/html", null,mailAttachment,null);
			} catch (Exception e) {
				e.printStackTrace();
			}*/
		}
	}
	
	public static String generateCustomerUserId(String fName,String lName,String email){
		  String userId="";
		  try {
		   if(fName == null){
		    fName="";
		   }
		   if(lName == null){
		    lName="";
		   }
		   if(email == null){
			   email="";
		   }
		   
		   fName = fName.replaceAll("[^a-zA-Z0-9]*", "");
		   lName = lName.replaceAll("[^a-zA-Z0-9]*", "");
		   
		   if(!fName.isEmpty() && !lName.isEmpty()){
		    userId = String.valueOf(fName.trim().charAt(0));
		    userId += lName.trim();
		   }else if(!fName.isEmpty() && lName.isEmpty()){
		    userId = fName.trim();
		   }else if(fName.isEmpty() && !lName.isEmpty()){
		    userId = lName.trim();
		   } else if (email != null && !email.isEmpty()) {
			   String emailArr[] = email.split("@");
			   if(emailArr != null && emailArr.length> 1) {
				   userId = emailArr[0].replaceAll("[^a-zA-Z0-9]*", "");
			   } else {
				   userId = email.replaceAll("[^a-zA-Z0-9]*", "");
			   }
		   }
		   Random rnd = new Random();
		   if(userId.length() < 5){
		    int len = 5-userId.length();
		    for(int i=1;i<=len;i++){
		     userId += rnd.nextInt(10);
		    }
		   }else if(userId.length() > 12){
		    userId = userId.substring(0,12);
		   }
		   boolean uniqueUserIdFlag = true; 
		   Customer customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
		   if(customer != null){
			   int len =12-userId.length() ;
			   for (int i = 0; i < len; i++) {
				   userId += rnd.nextInt(10); 
				   customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
				   if(customer == null){
					   uniqueUserIdFlag = true;
					   return userId.toLowerCase();
				   }
			   }
		   } else {
			   uniqueUserIdFlag = true;
			   return userId.toLowerCase(); 
		   }
		   userId = userId.substring(0,5);
		   int count =0;
		   while (!uniqueUserIdFlag&& count<1000) {
			   String tempUserId = userId;
			   for(int i=0;i<7;i++) {
				   tempUserId += rnd.nextInt(10);
				   customer = DAORegistry.getCustomerDAO().getCustomerByUserId(tempUserId);
				   if(customer == null){
					   uniqueUserIdFlag = true;
					   return tempUserId.toLowerCase();
					   //break;
				   }
				   count++;
			   }
		   }
		   if(!uniqueUserIdFlag) {
			   userId = "";
		   }
		  } catch (Exception e) {
		   e.printStackTrace();
		   userId = "";
		  }
		  return userId.toLowerCase();
	}

	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				if(!isRunning) {
					createcustomerUserIds();
					QuizContestUtil.updateCustomerProfileImages();
					isRunning = false;
				}
				System.out.println("Inside User Id Creation If : "+ new Date());
			} else {
				System.out.println("Inside User Id Creation Else : "+ new Date());
			}
		} catch (Exception e) {
			isRunning = false;
			e.printStackTrace();
		}
		
	}

}
