package com.zonesws.webservices.jobs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;


public class WebServiceUtil extends QuartzJobBean implements StatefulJob{
	private static Pattern numberOrLetterBlock = Pattern.compile("([^\\d]+)|(\\d+)");
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(WebServiceUtil.class);
	
	private static Map<Integer, Artist> artistMapById = new ConcurrentHashMap<Integer, Artist>();
	private static Map<String, Artist> artistMapByName = new ConcurrentHashMap<String, Artist>();
	//Map for customer super fan
	private static Map<Integer, Integer> customerMapBySuperFan = new ConcurrentHashMap<Integer, Integer>();
	//Map for super fan artist by artist id
	private static Map<Integer, Artist> superFanMapByArtistId = new ConcurrentHashMap<Integer, Artist>();
	
	
	
	public static void main(String[] args) {
		
		String ulaga = "The Book's of Mormonn'hg";
		
		System.out.println(ulaga.replaceAll("\\'", " ").replaceAll("\\s+", ""));
	}
	
	public static Map<String, Artist> getAllArtistByName(){
		return artistMapByName;
	}
	
	public static Map<Integer, Artist> getAllArtistById(){
		if(null == artistMapById || artistMapById.size() <= 20 ){
			Collection<Artist> artists = DAORegistry.getArtistDAO().getAll();
			for (Artist artist : artists) {
				artistMapById.put(artist.getId(),artist);
			}
		}
		return artistMapById;
	}
	
	public static Artist getArtistById(Integer artistId){
		
		Artist artist = artistMapById.get(artistId);
		if(null == artist){
			artist = DAORegistry.getArtistDAO().get(artistId);
			if(null != artist){
				artistMapById.put(artistId, artist);
			}
		}
		return artist;
	}
	
	
	/**
	 * Get super fan for customer
	 * @param customerId
	 * @return
	 */
	public static Integer getCustomerSuperFan(Integer customerId){
		Integer customerSuperFan = 0;
		try {
			customerSuperFan = customerMapBySuperFan.get(customerId);
			if(customerSuperFan != null){
				customerMapBySuperFan.put(customerId, customerSuperFan);
			}
			return customerSuperFan;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * 
	 * @param artistId
	 * @return
	 */
	/*public static Artist getSuperFanArtist(Integer artistId){
		Artist superFanArtist = null;
		try {
			superFanArtist = superFanMapByArtistId.get(artistId);
			if(superFanArtist != null){
				superFanMapByArtistId.put(artistId, superFanArtist);
			}
			return superFanArtist;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/
	
	
	public static void init() {
		
		try{
			Long startTime = System.currentTimeMillis();
			
			Collection<Artist> artists = DAORegistry.getArtistDAO().getAll();
			
			Map<Integer, Artist> tempArtistMapById = new ConcurrentHashMap<Integer, Artist>();
			Map<String, Artist> tempArtistMapByName = new ConcurrentHashMap<String, Artist>();
			
			for (Artist artist : artists) {
				
				tempArtistMapById.put(artist.getId(),artist);
				String name = artist.getName().replaceAll("\\'", " ").replaceAll("\\s+", "").toLowerCase();
				tempArtistMapByName.put(name, artist);
			}
			artistMapById.clear();
			artistMapById = null;
			artistMapById= new HashMap<Integer, Artist>(tempArtistMapById);
			
			artistMapByName.clear();
			artistMapByName = null;
			artistMapByName= new HashMap<String, Artist>(tempArtistMapByName);
			
			
			 
			System.out.println("TIME TO LAST WEB SERVICE UTIlS=" + (System.currentTimeMillis() - startTime) / 1000);
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
		
	}
	
}
