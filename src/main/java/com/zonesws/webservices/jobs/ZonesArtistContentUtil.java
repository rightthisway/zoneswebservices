package com.zonesws.webservices.jobs;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.validator.UrlValidator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.ArtistDetails;



/**
 * Scheduler to fetch the performer contents from TN
 * @author dthiyagarajan
 *
 */
public class ZonesArtistContentUtil extends QuartzJobBean implements StatefulJob{

	
	/**
	 * Method to initiate the scheduler process 
	 */
	public static void init(){
		try {
			Long startTime = System.currentTimeMillis();
			
			getArtistContents();
			
			System.out.println("TIME TO LAST ZONES ARTIST CONTENT UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	public static void getArtistContents(){
		CloseableHttpClient httpClient = null;
		try {
			httpClient = HttpClients.createDefault();
			
			Collection<Artist> performers = DAORegistry.getArtistDAO().getAll();
			
			System.out.println("Size of artist list :: " +performers.size());
			
			for(Artist artist : performers){
				ArtistDetails artistDetails = null;
				//String feedUrl = "http://www.ticketnetwork.com/tickets/sing-a-long-sound-of-music-tickets.aspx";
				//String invalidArtist = artist;
				//String performer = artist.replaceAll("\\s+", "-");
				String performer = artist.getName();
				performer = performer.replaceAll("\\s+", "-");
				String feedUrl = "http://www.ticketnetwork.com/tickets/"+performer+"-tickets.aspx";
				UrlValidator urlValidator = new UrlValidator();
				if(urlValidator.isValid(feedUrl)){
				
				
				HttpGet httpGet = new HttpGet(feedUrl);
				httpGet.setHeader("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpGet.addHeader("Accept","application/json, text/javascript, text/html, application/xhtml+xml, application/xml,*/*");
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			
				//Hit the ticket network site and get the content for the performer
				HttpResponse response = httpClient.execute(httpGet);
				
				 if(response == null || !response.getStatusLine().equals("200")){
					 artistDetails = new ArtistDetails();
						artistDetails.setId(artist.getId());
						artistDetails.setArtistDetailType(01);
						artistDetails.setArtistDetails(null);
						artistDetails.setCreatedDate(new Date());
						DAORegistry.getArtistDetailsDAO().saveOrUpdate(artistDetails);
				 }
				
				String performerContent = handleResponse(response);
				
				/*HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				String performerContent = EntityUtils.toString(entity).trim();*/
				
				if(performerContent != null && !performerContent.isEmpty()){
				//Jsoup html parsing code
				Document doc = Jsoup.parse(performerContent);
				Elements contentDiv = doc.select("div#performer_content_text");
				
				Elements elements = contentDiv.select("p");
				
				if(!elements.isEmpty()){
					System.out.println("======" + performer);
					for(Element element : contentDiv.select("p")){
						//System.out.println(element.text());
						artistDetails = new ArtistDetails();
						artistDetails.setId(artist.getId());
						artistDetails.setArtistDetailType(01);
						artistDetails.setArtistDetails(element.text());
						artistDetails.setCreatedDate(new Date());
						DAORegistry.getArtistDetailsDAO().saveOrUpdate(artistDetails);
					}
				}else{
					artistDetails = new ArtistDetails();
					artistDetails.setId(artist.getId());
					artistDetails.setArtistDetailType(01);
					artistDetails.setArtistDetails(null);
					artistDetails.setCreatedDate(new Date());
					DAORegistry.getArtistDetailsDAO().saveOrUpdate(artistDetails);
				}
				}
			 }else{
				 artistDetails = new ArtistDetails();
					artistDetails.setId(artist.getId());
					artistDetails.setArtistDetailType(01);
					artistDetails.setArtistDetails(null);
					artistDetails.setCreatedDate(new Date());
					DAORegistry.getArtistDetailsDAO().saveOrUpdate(artistDetails);
			 }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Method to get the response
	 * @param response
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
		int status = response.getStatusLine().getStatusCode();
		if (status >= 200 && status < 300) {
			HttpEntity entity = response.getEntity();
			return entity != null ? EntityUtils.toString(entity): null;
		}else {
			//throw new ClientProtocolException("Unexpected response status: " + status);
			return null;
		}
	}
	
	//Unit testing 
	public static void main(String[] args){
		/*List<String> performers = new ArrayList<String>();
		performers.add("Amy Grant");
		performers.add("justin bieber");
		performers.add("luke bryan");
		performers.add("new york yankees");
		performers.add("ISU World Figure Skating Championship ");
		performers.add("AL Wild Card Game: New York Yankees");*/
		getArtistContents();
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}

}
