package com.zonesws.webservices.jobs;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.ZoneRGBColor;


public class ZonesRGBColorUtil extends QuartzJobBean implements StatefulJob{
	private static Map<String, String> colorsMap = new ConcurrentHashMap<String, String>();
	private static Map<String, ZoneRGBColor> rgbColorMap = new ConcurrentHashMap<String, ZoneRGBColor>();
	
	
	public static Map<String, String> getRGBColorsByZone(Collection<String> zonesList){
		
		Map<String, String> zoneColorMap = new HashMap<String, String>();
		
		if(null != colorsMap && colorsMap.size() >0 ){
			for (String zone : zonesList) {
				String rgbColor = colorsMap.get(zone.toLowerCase());
				if(zone != null && !zone.isEmpty() && zone.length() > 0){
					zoneColorMap.put(zone.toLowerCase(), rgbColor);
				}
			}
		}else{
			
			Collection<ZoneRGBColor> zoneRGBColors = DAORegistry.getZoneRGBColorDAO().getAll();
			Map<String, String> colorsMapTemp = new HashMap<String, String>();
			for(ZoneRGBColor zoneRGBColor : zoneRGBColors){
				colorsMapTemp.put(zoneRGBColor.getZone().toLowerCase(), zoneRGBColor.getColor());
			}
			colorsMap.clear();
			colorsMap = null;
			colorsMap= new HashMap<String, String>(colorsMapTemp);
			
			return getRGBColorsByZone(zonesList);
		}
		
		
		return zoneColorMap;
	}
	
	
	
	public static void init() {
		
		try{
			Long startTime = System.currentTimeMillis();
			Collection<ZoneRGBColor> zoneRGBColors = DAORegistry.getZoneRGBColorDAO().getAll();
			Map<String, String> colorsMapTemp = new HashMap<String, String>();
			Map<String, ZoneRGBColor> rgbColorMapTemp = new HashMap<String, ZoneRGBColor>();
			
			for(ZoneRGBColor zoneRGBColor : zoneRGBColors){
				colorsMapTemp.put(zoneRGBColor.getZone().toLowerCase(), zoneRGBColor.getColor());
				rgbColorMapTemp.put(zoneRGBColor.getZone().replaceAll("_","").toUpperCase(), zoneRGBColor);
			}
			
			colorsMap.clear();
			colorsMap = null;
			colorsMap= new HashMap<String, String>(colorsMapTemp);
			
			if(null != rgbColorMapTemp){
				rgbColorMap.clear();
				rgbColorMap.putAll(rgbColorMapTemp);
			}
			
			System.out.println("TIME TO LAST ZONES RGB COLOR UTILS=" + (System.currentTimeMillis() - startTime) / 1000);
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
		
	}
	
}
