package com.zonesws.webservices.notifications;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;

import com.zonesws.webservices.utils.URLUtil;

import sun.net.www.protocol.https.*;


/**
 * @author Tamil
 *
 */
public class OneSignalNotificationUtil {
	
	public static String PRODUCTION_APP_ID="e15a0313-b54a-41ed-af32-a95991037a40";
	public static String PRODUCTION_API_KEY="ZjVhNTE5ZGItNjY2OC00ZWZkLWEzYmMtMDc0MDQ5ZWQwMWFm";
	
	public static String SANDBOX_APP_ID="1336d1dc-48ad-4fa2-a13b-2300b74f79a0";
	public static String SANDBOX_API_KEY="OWRjYjJiMDAtNDM5OC00MjU0LWFmZTAtMDJiMjQ0OGVjMzY1";
	
	public static void main(String[] args) throws Exception {
		sendBulkNotification("Test API Notification...");
	}
	public static void sendBulkNotification(String message) throws Exception {
		try {
			   String jsonResponse=null;
			   
			   URL url = new URL("https://onesignal.com/api/v1/notifications");
			   HttpURLConnection con = (HttpURLConnection)url.openConnection();
			   con.setUseCaches(false);
			   con.setDoOutput(true);
			   con.setDoInput(true);

			   	String apiKey = "";
			   	String appId = "";
			   	if(URLUtil.isProductionEnvironment) {
			   		apiKey = PRODUCTION_API_KEY;
			   		appId= PRODUCTION_APP_ID;
			   		
			   	} else {
			   		apiKey = SANDBOX_API_KEY;
			   		appId = SANDBOX_APP_ID;
			   	}
			   con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			   con.setRequestProperty("Authorization", "Basic "+apiKey);
			   con.setRequestMethod("POST");

			   String strJsonBody = "{"
			                      +   "\"app_id\": \""+appId+"\","
			                      //+   "\"included_segments\": [\"Active Users\"],"
			                      +   "\"included_segments\": [\"Active Users\", \"Inactive Users\"],"
			                      //+   "\"data\": {\"foo\": \"bar\"},"
			                      +   "\"contents\": {\"en\": \""+message+"\"}"
			                      + "}";
			         
			   
			   System.out.println("strJsonBody:\n" + strJsonBody);

			   byte[] sendBytes = strJsonBody.getBytes("UTF-8");
			   con.setFixedLengthStreamingMode(sendBytes.length);

			   OutputStream outputStream = con.getOutputStream();
			   outputStream.write(sendBytes);

			   int httpResponse = con.getResponseCode();
			   System.out.println("httpResponse: " + httpResponse);

			   if (  httpResponse >= HttpURLConnection.HTTP_OK
			      && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
			      //Scanner scanner = new Scanner(con.getInputStream());
			      //jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
			      //scanner.close();
			      jsonResponse = IOUtils.toString(con.getInputStream(), "UTF-8");
			      System.out.println("Out : "+jsonResponse);
			   }
			   else {
			      //Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
				   Scanner scanner = new Scanner(con.getErrorStream());
			      //jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
			      scanner.close();
			      jsonResponse = IOUtils.toString(con.getInputStream(), "UTF-8");
			      System.out.println("Out : "+jsonResponse);
			   }
			   System.out.println("jsonResponse:\n" + jsonResponse);
			   
			} catch(Exception e) {
			   e.printStackTrace();
			   throw e;
			}
	}
}