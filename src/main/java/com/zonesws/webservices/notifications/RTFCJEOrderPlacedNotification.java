package com.zonesws.webservices.notifications;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerFantasyOrder;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * 
 * @author Ulaganathan
 *
 */
public class RTFCJEOrderPlacedNotification extends QuartzJobBean implements StatefulJob{
	
	private static Logger logger = LoggerFactory.getLogger(RTFCJEOrderPlacedNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
		
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}

	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFCJEOrderPlacedNotification.mailManager = mailManager;
	}

	public void sendNotifications(CustomerFantasyOrder customerOrder){
		
		try{
			
			String message = "";
			
			message = "Congratulations! � you have just redeemed "+customerOrder.getRequiredPoints() +" of your Reward Dollars and if the "+ customerOrder.getTeamName()+" make this years "+customerOrder.getFantasyEventName()+", " +
			"You will receive "+customerOrder.getQuantity()+" FREE Fantasy Sports Tickets seated together in ZONE "+customerOrder.getZone()+". " +
			"Your order number is "+customerOrder.getId();
			
			Customer customer = CustomerUtil.getCustomerById(customerOrder.getCustomerId());
			if(customer != null){
				Map<String,Object> mailMap = new HashMap<String,Object>();
				mailMap.put("message", message);
				mailMap.put("customerOrder", customerOrder);
				mailMap.put("customer", customer);
				
				//inline(image in mail body) image add code
				MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
				
				if(customerOrder.getIsPackageSelected()==true){
				try {
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.bccEmails, 
							"Reward The Fan: Fantasy Sports Ticket Package Confirmation ",
				    		"mail-fantasy-ticket-package-confirmation.html", mailMap, "text/html", null,mailAttachment,null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				try {
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.bccEmails, 
							"Reward The Fan: Fantasy Sports Ticket Team Confirmation ",
				    		"mail-fantasy-ticket-team-confirmation.html", mailMap, "text/html", null,mailAttachment,null);
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
			}
		
			List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(customerOrder.getCustomerId());
			
			
			if(deviceList!=null && !deviceList.isEmpty()){
				
				
				String jsonString = "";
				
				for (CustomerDeviceDetails device : deviceList) {
					
					switch (device.getApplicationPlatForm()) {
					
					case ANDROID:
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.CJE_VIEW_ORDER);
						notificationJsonObject.setMessage(message);
						notificationJsonObject.setOrderId(customerOrder.getId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						gcmNotificationService.sendMessage(NotificationType.CJE_ORDER_PLACED, device.getNotificationRegId(), jsonString);
						break;
						
					case IOS:
					    Map<String, String> customFields = new HashMap<String, String>();
					    customFields.put("notificationType", String.valueOf(NotificationType.CJE_VIEW_ORDER));
					    customFields.put("orderId", String.valueOf(customerOrder.getId()));
						apnsNotificationService.sendNotification(NotificationType.CJE_ORDER_PLACED, device.getNotificationRegId(), message,customFields);
						break;
			
						default:
							break;
					}
				}
			}else{
				System.out.println("CJE_ORDER_PLACED_NOTIFICATION : No Active Devices are found.");
			}
			
			
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
}
