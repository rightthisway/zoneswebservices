package com.zonesws.webservices.notifications;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerFantasyOrder;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * 
 * @author Ulaganathan
 *
 */
public class RTFCJEToRegularOrderNotification extends QuartzJobBean implements StatefulJob{
	
	private static Logger logger = LoggerFactory.getLogger(RTFCJEToRegularOrderNotification.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
		
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}

	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}

	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFCJEToRegularOrderNotification.mailManager = mailManager;
	}

	public void sendNotifications(CustomerFantasyOrder customerOrder){
		//We are happy to inform you that you will soon recieve 2 tickets
		try{
		
			List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(customerOrder.getCustomerId());
			Map<Integer, Boolean> emailSendMap = new HashMap<Integer, Boolean>();
			if(deviceList!=null && !deviceList.isEmpty()){
				
				String message = "";
				//commented by Ulaganathan
				Event event= null;//customerOrder.getEvent();
				
				String eventDetails = event.getEventName()+", "+event.getEventDateTime()+", "+
				event.getVenueName()+", "+event.getCity()+", "+event.getState()+", "+event.getCountry();
				
				message = "We are happy to inform you that you will soon receive "+customerOrder.getQuantity()+" tickets for "+eventDetails+". " +
				"Your order number is "+customerOrder.getRegularOrderId();
				
				/*Boolean isEmailSent = emailSendMap.get(customerOrder.getCustomerId());
				
				if(null != isEmailSent && isEmailSent){
					
					
				}else{
				
					//To send the email
					Customer customer = CustomerUtil.getCustomerById(customerOrder.getCustomerId());
					if(customer != null){
						Map<String,Object> mailMap = new HashMap<String,Object>();
						mailMap.put("message", message);
						
						//inline(image in mail body) image add code
						MailAttachment[] mailAttachment = new MailAttachment[1];
						String filePath = Util.getFilePath(null, "logo.png", "/resources/images/");
						//byte[] fileContent = Util.convertFiletoByteArray(filePath, "png");
						mailAttachment[0] = new MailAttachment(null,"image/png","logo.png");
						
						try {
							mailManager.sendMailNow("text/html","sales@rewardthefan.com", customer.getEmail(), 
									"AODev@rightthisway.com", "tselvan@rightthisway.com", 
									"Reward The Fan: Your Fantasy Event order details",
						    		"email-cjeorder-to-regular-information.html", mailMap, "text/html", null,mailAttachment,filePath);
						} catch (Exception e) {
							e.printStackTrace();
						}
				
					}
				}*/
				String jsonString="";
				for (CustomerDeviceDetails device : deviceList) {
					
					switch (device.getApplicationPlatForm()) {
					
					case ANDROID:
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.REGULAR_VIEW_ORDER);
						notificationJsonObject.setMessage(message);
						notificationJsonObject.setOrderId(customerOrder.getRegularOrderId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						gcmNotificationService.sendMessage(NotificationType.CJE_ORDER_CONVERTED_TO_REGUALR, device.getNotificationRegId(), jsonString);
						break;
						
					case IOS:
					    Map<String, String> customFields = new HashMap<String, String>();
					    customFields.put("notificationType", String.valueOf(NotificationType.REGULAR_VIEW_ORDER));
					    customFields.put("orderId", String.valueOf(customerOrder.getRegularOrderId()));
						apnsNotificationService.sendNotification(NotificationType.CJE_ORDER_CONVERTED_TO_REGUALR, device.getNotificationRegId(), message,customFields);
						break;
			
						default:
							break;
					}
				}
			}else{
				System.out.println("CJE_ORDER_CONVERTED_TO_REGUALR : No Active Devices are found.");
			}
			
			
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
}
