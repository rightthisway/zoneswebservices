package com.zonesws.webservices.notifications;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;

/**
 * 
 * @author Selvi
 *
 */
public class RTFEventsNotification extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static Logger logger = LoggerFactory.getLogger(RTFEventsNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	private static Date lastRunTime;
	
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}

	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	
	public void sendEventNotificationsFinal(){
		
		try{
			Date currentDate=new Date();
			Calendar calendar = new GregorianCalendar();
			calendar.set(calendar.DAY_OF_MONTH,-4);
			calendar.set(calendar.MINUTE,-60);
			lastRunTime =calendar.getTime();
			
			String fromDate = dbDateFormat.format(lastRunTime);
			String toDate = dbDateFormat.format(currentDate);

			Map<Integer, List<Event>>  artistEventsMap = DAORegistry.getEventDAO().getNewlyCreatedEvents(fromDate, toDate);
			
			if(null != artistEventsMap && artistEventsMap.size() > 0){
				
				List<Integer> artistIds = new ArrayList<Integer>();
				artistIds.addAll(artistEventsMap.keySet());
				
				Map<Integer, List<CustomerDeviceDetails>> deviceMap = DAORegistry.getCustomerDeviceDetailsDAO().getAllActiveFavoriteArtistCustomerDeviceDetails(artistIds);
				
				List<String> androidTokens = null;
				List<String> iOSTokens = null;
				String message="";
				
				for (Integer artistId : artistEventsMap.keySet()) {
					
					List<CustomerDeviceDetails> customerDeviceDetails = deviceMap.get(artistId);
					List<Event> artistEvents = artistEventsMap.get(artistId);
					
					if(null == artistEvents || artistEvents.isEmpty()){
						continue;
					}
					
					if(null == customerDeviceDetails || customerDeviceDetails.isEmpty()){
						continue;
					}
					
					androidTokens = new ArrayList<String>();
					iOSTokens = new ArrayList<String>();
					
					for (CustomerDeviceDetails device : customerDeviceDetails) {
						
						if(device.getApplicationPlatForm().equals(ApplicationPlatForm.IOS)){
							iOSTokens.add(device.getNotificationRegId());
						}else{
							androidTokens.add(device.getNotificationRegId());
						}
					}
					
					for (Event event : artistEvents) {
						
						
						String eventDetails = event.getEventDateTime()+", "+
						event.getVenueName()+", "+event.getCity()+", "+event.getState()+", "+event.getCountry();
						
						System.out.println(event.getEventId()+": "+eventDetails);
						
						
						
						message="Your favorite "+event.getEventName()+" has been announced on "+eventDetails+". Please go to our app and be the first one to buy the tickets";

				
						if(null != androidTokens && !androidTokens.isEmpty()){
							NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
							notificationJsonObject.setNotificationType(NotificationType.EVENT_DETAILS);
							notificationJsonObject.setMessage(message);
							notificationJsonObject.setEventId(event.getEventId());
							Gson gson = new Gson();	
							message = gson.toJson(notificationJsonObject);
							gcmNotificationService.sendMessage(NotificationType.ANNOUNCED_EVENT, androidTokens, message);
						
							//gcmNotificationService.sendMessage(NotificationType.ANNOUNCED_EVENT, androidTokens, jsonString);
							
						}else if (null != iOSTokens && !iOSTokens.isEmpty()){
							Map<String, String> customFields = new HashMap<String, String>();
						    customFields.put("notificationType", String.valueOf(NotificationType.EVENT_DETAILS));
						    customFields.put("eventId", String.valueOf(event.getEventId()));
							apnsNotificationService.sendNotification(NotificationType.ANNOUNCED_EVENT, iOSTokens, message,customFields);
							
							//apnsNotificationService.sendNotification(NotificationType.ANNOUNCED_EVENT, iOSTokens, jsonString);
						}
					}
				}
			}
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	


	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//sendEventNotificationsFinal();
		
	}
	
	
}
