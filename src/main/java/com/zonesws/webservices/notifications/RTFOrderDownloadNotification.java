package com.zonesws.webservices.notifications;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.enums.FileType;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Order Status Notification
 * @author KUlaganathan
 *
 */
public class RTFOrderDownloadNotification {
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	private static Logger logger = LoggerFactory.getLogger(RTFOrderDownloadNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFOrderDownloadNotification.mailManager = mailManager;
	}


	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public static void main(String[] args) {
		
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, -10);
		long days = getDifferenceDays(calendar.getTime(), new Date());
		System.out.println("From Date :"+calendar.getTime());
		System.out.println("To Date :"+new Date());
		System.out.println(days);
	}
	

	public static void trigger(CustomerOrder customerOrder, Customer customer,MailAttachment[] ticketAttachments,FileType fileType,boolean isJob) throws Exception{
			
			String formattedEventDate = dateFormat.format(customerOrder.getEventDateTemp());
			String eventTimeLocal="TBD";
			if(null != customerOrder.getEventTime() ){
				eventTimeLocal = timeFormat.format(customerOrder.getEventTime());
			}
			
			String eventDetails = customerOrder.getEventName().toUpperCase();
			String dateString = formattedEventDate+" "+eventTimeLocal;
			String venueString = customerOrder.getVenueName()+", "+customerOrder.getVenueCity()+", "+customerOrder.getVenueState()+", "+customerOrder.getVenueCountry();
			String notificationMsg = "";
			String downloadText ="DOWNLOAD YOUR TICKETS";
			String downloadLink = "https://www.rewardthefan.com/GetMyRegularTickets";
			String template = "email-order-download-tickets.html";
			String message1 = "";
			String message2	= "";
			String message3 = "";
			String subject = "";
			String notiMsg = "is available for download.";
			
			
			if(ticketAttachments!= null && ticketAttachments.length > 0){
				template = "email-order-download-tickets-attachment.html";
			}
			if(isJob){
				subject = "Remember to download and print your "+customerOrder.getEventName()+" eTickets.";
			}else{
				subject = "Reward The Fan: Tickets are available for download for Your Order Number :" +customerOrder.getId();
			}
			
			if(fileType.equals(FileType.ETICKET)){
				if(isJob){
					message1 = "We noticed you have not yet downloaded or printed your eTickets for";
					message2 = "";
				}else{
					message1 = "Your Reward The Fan Ticket Order Number "+ customerOrder.getId()+" for";
					message2 = "is available for download.";
				}
				message3 = "Please note, you must print your eTickets and present them at the event in order to gain entry.";
			}else if(fileType.equals(FileType.QRCODE) || fileType.equals(FileType.BARCODE)){
				if(isJob){
					message1 = "We noticed you have not yet downloaded or printed your eTickets for";
					message2 = "";
				}else{
					message1 = "Your Reward The Fan Ticket Order Number "+ customerOrder.getId()+" for";
					notiMsg = "is available in your my tickets tab.";
					message2 = "is available for download.";
				}
				message3 = "Please remember you must display these tickets on your mobile device when entering the venue. You cannot print these tickets out. " ;
			}else if(fileType.equals(FileType.OTHER)){
				message1 = "Your Reward The Fan Ticket Order Number "+ customerOrder.getId()+" for";
				message2 = "have been transferred to you via Flash Seats.";
				notiMsg = "have been transferred to you via Flash Seats.";
				message3 = "You will need to login/register with Flash Seats using your email address. To use the tickets on the day of the event, either register an identification card in your Flash Seats account, or download the free Flash Seats app on your smart phone." ;
				downloadText ="www.flashseats.com";
				downloadLink = "https://www.flashseats.com";
			}
			notificationMsg  = message1+" "+eventDetails+" "+dateString+" At "+venueString +" "+ notiMsg+" "+message3;
			if(customer != null){
				try {
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("message1", message1);
					mailMap.put("message2", message2);
					mailMap.put("message3", message3);
					mailMap.put("eventDetails", eventDetails);
					mailMap.put("dateString", dateString);
					mailMap.put("venueString", venueString);
					mailMap.put("downloadText",downloadText);
					mailMap.put("downloadLink",downloadLink);
					mailMap.put("orderId",customerOrder.getId().toString());
					
					String email=customer.getEmail();
						
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
				
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
							null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
							subject,template, mailMap, "text/html", ticketAttachments,mailAttachment,null);
					System.out.println("ORDER TICKET UPLOAD EMAIL BEGINS : "+new Date());
					DAORegistry.getCustomerOrderDAO().updateTicketDownloadEmailSent(customerOrder.getId(),email);
					System.out.println("ORDER TICKET UPLOAD EMAIL ENDS : "+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			/*List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(customerOrder.getCustomer().getId());
			
			if(deviceList==null || deviceList.isEmpty()){
				return;
			}
			String jsonString="";
			for (CustomerDeviceDetails device : deviceList) {
				
				switch (device.getApplicationPlatForm()) {
				
				case ANDROID:
					NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
					notificationJsonObject.setNotificationType(NotificationType.REGULAR_VIEW_ORDER);
					notificationJsonObject.setMessage(notificationMsg);
					notificationJsonObject.setOrderId(customerOrder.getId());
					Gson gson = new Gson();	
					jsonString = gson.toJson(notificationJsonObject);
					//gcmNotificationService.sendMessage(NotificationType.ORDER_STATUS, device.getNotificationRegId(), jsonString);
					break;
					
				case IOS:
				    Map<String, String> customFields = new HashMap<String, String>();
				    customFields.put("notificationType", String.valueOf(NotificationType.REGULAR_VIEW_ORDER));
				    customFields.put("orderId", String.valueOf(customerOrder.getId()));
					//apnsNotificationService.sendNotification(NotificationType.ORDER_STATUS, device.getNotificationRegId(), notificationMsg,customFields);
					break;

				default:
					break;
				}
			}*/
		
	}
	
	/*
	public static void testNotification(MailAttachment[] ticketAttachments) throws Exception{
		
	 
		String template = "email-order-download-tickets.html";
		
		if(ticketAttachments!= null && ticketAttachments.length > 0){
			template = "email-order-download-tickets-attachment.html";
		}
		
		 
		try {
			Map<String,Object> mailMap = new HashMap<String,Object>();
			mailMap.put("message1", "");
			mailMap.put("message2",  "");
			mailMap.put("message3",  "");
			mailMap.put("eventDetails",  "");
			mailMap.put("dateString", "Dev Team Testing- Ticket Attachment.");
			mailMap.put("venueString", "Dev Team Testing- Ticket Attachment.");
			mailMap.put("downloadText","");
			mailMap.put("downloadLink","");
			mailMap.put("orderId","2000052552");
			
			String email="ulaganathantoall@gmail.com",subject="Dev Team Testing- Ticket Attachment.";
				
			//inline(image in mail body) image add code
			MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);
		
			mailManager.sendMailNow("text/html",URLUtil.fromEmail, email, 
					null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
					subject,template, mailMap, "text/html", ticketAttachments,mailAttachment,null);
			
			System.out.println("ORDER TICKET UPLOAD EMAIL BEGINS : "+new Date());
			 
			
			System.out.println("ORDER TICKET UPLOAD EMAIL ENDS : "+new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
}
 