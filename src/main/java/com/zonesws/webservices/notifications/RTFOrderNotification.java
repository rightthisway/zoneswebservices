package com.zonesws.webservices.notifications;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * Order Status Notification
 * @author KUlaganathan
 *
 */
public class RTFOrderNotification extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	private static Logger logger = LoggerFactory.getLogger(RTFOrderNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	static MailManager mailManager = null;
	
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}
	
	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFOrderNotification.mailManager = mailManager;
	}


	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public static void main(String[] args) {
		
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, -10);
		long days = getDifferenceDays(calendar.getTime(), new Date());
		System.out.println("From Date :"+calendar.getTime());
		System.out.println("To Date :"+new Date());
		System.out.println(days);
	}
	

	public void init() throws Exception{
		
		logger.debug("Initiating the email scheduler process for customer orders....");
		
		/* if events are within 5 days then do not send notification
		 * if event is 5 days away then send notification on next of order date
		 * if notification date is 30days pas then check same above condition and notifications
		 * consider unfilled orders only 
		saying You tickets are on the way and will be ready before DAY OF THE EVENT
		 */

		Integer eventDateInterval = 5;
		Integer notificationInterval = 30;
		
		//Collection<CustomerOrder> customerOrders = DAORegistry.getCustomerOrderDAO().getAllCustomerActiveOrder();
		
		List<Integer> customerOrderIDs = DAORegistry.getQueryManagerDAO().getAllCustomerIdsToSendNotification(eventDateInterval, notificationInterval);
		/*List<Integer> customerOrderIDs = new ArrayList<Integer>();
		customerOrderIDs.add(200338);
		customerOrderIDs.add(200335);*/
		
		//Map<Integer, Boolean> emailSendMap = new HashMap<Integer, Boolean>();
		
		//for(CustomerOrder customerOrder : customerOrders){
		
		for (Integer customerOrderId : customerOrderIDs) {
			
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(customerOrderId);
			//boolean isFirstTime =false;
			//if(null == customerOrder.getLastNotified()){
			//	customerOrder.setLastNotified(customerOrder.getCreateDateTemp());
			//	isFirstTime =true;
			//}
			
			Calendar calendar = new GregorianCalendar();
			//Date lastNotified = customerOrder.getLastNotified();
			Date eventDate = customerOrder.getEventDateTemp();
			Date now = calendar.getTime();
			
			Calendar cal1 = new GregorianCalendar();
			cal1.setTime(eventDate);
			cal1.add(Calendar.DAY_OF_MONTH, -1);
			String deliveryDate = dateFormat.format(cal1.getTime());
			
			//Long orderDeliveryDays = getDifferenceDays(now, eventDate);
			//Long notifyDayDiff = getDifferenceDays(lastNotified, now);
			
			//boolean sendNotification = false;
			/*if(orderDeliveryDays >= 1 && orderDeliveryDays <= 5){
				
				if(notifyDayDiff > 2){
					
					sendNotification = true;
				}else if(isFirstTime){
					sendNotification = true;
				}
				
			}else if(orderDeliveryDays >= 6 && orderDeliveryDays <= 30){
				
				if(notifyDayDiff > 7){
					
					sendNotification = true;
				}else if(isFirstTime){
					sendNotification = true;
				}
				
			}else if(orderDeliveryDays >= 31 &&  orderDeliveryDays <= 59){
				
				if(notifyDayDiff > 15 ){
					
					sendNotification = true;
				}else if(isFirstTime){
					sendNotification = true;
				}
			}else if(orderDeliveryDays >= 60 ){
				
				if(notifyDayDiff > 20 ){
					
					sendNotification = true;
				}else if(isFirstTime){
					sendNotification = true;
				}
			}*/
			String formattedEventDate = dateFormat.format(customerOrder.getEventDateTemp());
			String eventTimeLocal="TBD";
			if(null != customerOrder.getEventTime() ){
				eventTimeLocal = timeFormat.format(customerOrder.getEventTime());
			}
			
			//String eventDetails = customerOrder.getEventName()+", "+formattedEventDate+" "+eventTimeLocal+", "+
			//customerOrder.getVenueName()+", "+customerOrder.getVenueCity()+", "+customerOrder.getVenueState()+", "+customerOrder.getVenueCountry();
			
			//We just wanted to remind you about your tickets to see (Artist Name) at (venue) on (date) and will send you a separate notification as soon as your electronic-tickets are ready for downloading.
			
			//String message = "Your tickets are on the way and will be ready before DAY OF THE EVENT";
			//String message = "We just wanted to remind you that your order number "+ customerOrder.getId()+" for "+ eventDetails +" is "+ orderDeliveryDays +" days away and we will notify you as soon as your tickets are available for download.";
			//String message = "We just wanted to remind you about your tickets to see "+customerOrder.getEventName()+" at "+customerOrder.getVenueName()+" on "+formattedEventDate+" "+eventTimeLocal+" and will send you a separate notification as soon as your electronic-tickets are ready for downloading.";
			
			

			
			 
			

			

			
			String message1 = "We just wanted to let you know we did not forget about your tickets for";
			String message2 = "We will send you an alert as soon as your tickets are available for download.";
			String eventDetails = customerOrder.getEventName().toUpperCase();
			String venueString = customerOrder.getVenueName();
			String dateString = formattedEventDate+" "+eventTimeLocal;
			String notificationMsg = message1 +" "+eventDetails+" At "+venueString+" on "+dateString+" "+message2;
			//Boolean isEmailSent = emailSendMap.get(customerOrder.getCustomer().getId());
			//if(null != isEmailSent && isEmailSent){
			//}else{
			
				//To send the email
				Customer customer = CustomerUtil.getCustomerById(customerOrder.getCustomer().getId());
				if(customer != null){
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("message1", message1);
					mailMap.put("message2", message2);
					mailMap.put("eventDetails", eventDetails);
					mailMap.put("venueString", venueString);
					mailMap.put("dateString", dateString);
					mailMap.put("customerName",customer.getCustomerName()+" "+customer.getLastName());
					mailMap.put("deliveryDate", deliveryDate);
					
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);		
					try {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
								null,URLUtil.fromEmail+","+URLUtil.bccEmails, 
								"Reward The Fan: Regarding your tickets for Order Number :" +customerOrder.getId() ,
					    		"email-order-delivery-information.html", mailMap, "text/html", null,mailAttachment,null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			//}
			
			//if(sendNotification){
				
				List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(customerOrder.getCustomer().getId());
				
				if(deviceList !=null && !deviceList.isEmpty()){

					String jsonString="";
					for (CustomerDeviceDetails device : deviceList) {
						
						switch (device.getApplicationPlatForm()) {
						
						case ANDROID:
							NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
							notificationJsonObject.setNotificationType(NotificationType.REGULAR_VIEW_ORDER);
							notificationJsonObject.setMessage(notificationMsg);
							notificationJsonObject.setOrderId(customerOrder.getId());
							Gson gson = new Gson();	
							jsonString = gson.toJson(notificationJsonObject);
							gcmNotificationService.sendMessage(NotificationType.ORDER_STATUS, device.getNotificationRegId(), jsonString);
							break;
							
						case IOS:
						    Map<String, String> customFields = new HashMap<String, String>();
						    customFields.put("notificationType", String.valueOf(NotificationType.REGULAR_VIEW_ORDER));
						    customFields.put("orderId", String.valueOf(customerOrder.getId()));
							apnsNotificationService.sendNotification(NotificationType.ORDER_STATUS, device.getNotificationRegId(), notificationMsg,customFields);
							break;
							
		
							default:
								break;
						}
					}
				}
				
				customerOrder.setLastNotified(new Date());
				DAORegistry.getCustomerOrderDAO().update(customerOrder);
			//}
			
		}
		
	}
	
	
 public void initNew() throws Exception{
		
		logger.debug("Initiating the email scheduler process for customer orders....");
		

		Integer eventDateInterval = 5;
		Integer notificationInterval = 30;
		
		List<Integer> customerOrderIDs = new ArrayList<Integer>();
		customerOrderIDs.add(200699);
		
		
		for (Integer customerOrderId : customerOrderIDs) {
			
			CustomerOrder customerOrder = DAORegistry.getCustomerOrderDAO().get(customerOrderId);
			
			Calendar calendar = new GregorianCalendar();
			Date eventDate = customerOrder.getEventDateTemp();
			Date now = calendar.getTime();
			
			Calendar cal1 = new GregorianCalendar();
			cal1.setTime(eventDate);
			cal1.add(Calendar.DAY_OF_MONTH, -1);
			String deliveryDate = dateFormat.format(cal1.getTime());
			
			
			String formattedEventDate = dateFormat.format(customerOrder.getEventDateTemp());
			String eventTimeLocal="TBD";
			if(null != customerOrder.getEventTime() ){
				eventTimeLocal = timeFormat.format(customerOrder.getEventTime());
			}
			
			String message1 = "We just wanted to let you know we did not forget about your tickets for";
			String message2 = "We will send you a notification as soon as your tickets are available.";
			String eventDetails = customerOrder.getEventName().toUpperCase();
			String venueString = customerOrder.getVenueName();
			String dateString = formattedEventDate+" "+eventTimeLocal;
			String notificationMsg = message1 +" "+eventDetails+" At "+venueString+" on "+dateString+" "+message2;
			
				//To send the email
				Customer customer = CustomerUtil.getCustomerById(customerOrder.getCustomer().getId());
				if(customer != null){
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("message1", message1);
					mailMap.put("message2", message2);
					mailMap.put("eventDetails", eventDetails);
					mailMap.put("venueString", venueString);
					mailMap.put("dateString", dateString);
					mailMap.put("customerName",customer.getCustomerName()+" "+customer.getLastName());
					mailMap.put("deliveryDate", deliveryDate);
					
					//inline(image in mail body) image add code
					MailAttachment[] mailAttachment = Util.getEmailAttachmentForTicketTemplate(false);		
					try {
						mailManager.sendMailNow("text/html",URLUtil.fromEmail, "AODev@rightthisway.com", 
								null,"tselvan@rightthisway.com", 
								"Reward The Fan: Regarding your tickets for Order Number :" +customerOrder.getId() ,
					    		"email-order-delivery-information.html", mailMap, "text/html", null,mailAttachment,null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			
		}
		
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}