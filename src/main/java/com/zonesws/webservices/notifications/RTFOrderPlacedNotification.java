package com.zonesws.webservices.notifications;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * 
 * @author Ulaganathan
 *
 */
public class RTFOrderPlacedNotification extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	private static Logger logger = LoggerFactory.getLogger(RTFOrderPlacedNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	private static Map<Integer, String> orderPlacedMsgMap = new ConcurrentHashMap<Integer, String>();
	static MailManager mailManager = null;
		
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}

	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}
	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFOrderPlacedNotification.mailManager = mailManager;
	}
	
	/*public static void main(String[] args) {
	NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
	notificationJsonObject.setNotificationType(NotificationType.ORDER_PLACED);
	notificationJsonObject.setMessage("Congratulations! You have just purchased 2Tickets for event ");
	notificationJsonObject.setOrderId(7);
	
	Gson gson = new Gson();
	
	String jsonString = gson.toJson(notificationJsonObject);
	System.out.println(jsonString);
}
	*/

	public void sendRewardNotifications(CustomerOrder customerOrder){
		
		//Congratulations. You have just purchased the tickets for Event Name, Event Date, Venue name, city state location
		
		try{
		
			List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(customerOrder.getCustomer().getId());
			
			Map<Integer, Boolean> emailSendMap = new HashMap<Integer, Boolean>();
			
			if(deviceList!=null && !deviceList.isEmpty()){
				
				String eventDate = dateFormat.format(customerOrder.getEventDateTemp());
				String eventTimeLocal="TBD";
				if(null != customerOrder.getEventTime() ){
					eventTimeLocal = timeFormat.format(customerOrder.getEventTime());
				}
				
				String eventDetails = customerOrder.getEventName()+", "+eventDate+" "+eventTimeLocal+" at the "+
				customerOrder.getVenueName()+", "+customerOrder.getVenueCity()+", "+customerOrder.getVenueState()+", "+customerOrder.getVenueCountry()+"";
				
				
				/*Congratulations! You have just purchased two tickets for The Lion King, 12/29/2016 08:00 PM at the Minskoff Theater (can we add the address here?) 
				located at 200 West 45th Street),  New York,  NY  10036.*/
				
				String qtyString = "one ticket";
				if(customerOrder.getQty() > 1){
					qtyString = customerOrder.getQty()+" tickets";
				}
				
				String message = "Congratulations! You have just purchased "+qtyString+" for "+eventDetails +". " +
									"Your order number is "+customerOrder.getId();
				
				String jsonString="";
				for (CustomerDeviceDetails device : deviceList) {
					
					switch (device.getApplicationPlatForm()) {
					
					case ANDROID:
						NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
						notificationJsonObject.setNotificationType(NotificationType.REGULAR_VIEW_ORDER);
						notificationJsonObject.setMessage(message);
						notificationJsonObject.setOrderId(customerOrder.getId());
						Gson gson = new Gson();	
						jsonString = gson.toJson(notificationJsonObject);
						gcmNotificationService.sendMessage(NotificationType.ORDER_PLACED, device.getNotificationRegId(), jsonString);
						break;
						
					case IOS:
					    Map<String, String> customFields = new HashMap<String, String>();
					    customFields.put("notificationType", String.valueOf(NotificationType.REGULAR_VIEW_ORDER));
					    customFields.put("orderId", String.valueOf(customerOrder.getId()));
						apnsNotificationService.sendNotification(NotificationType.ORDER_PLACED, device.getNotificationRegId(), message,customFields);
						break;
		
			
						default:
							break;
					}
				}
			}else{
				System.out.println("RTFORDERPLACED_NOTIFICATION : No Active Devices are found.");
			}
			
			
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
}
