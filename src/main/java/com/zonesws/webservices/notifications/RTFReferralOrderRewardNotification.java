package com.zonesws.webservices.notifications;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.google.gson.Gson;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerDeviceDetails;
import com.zonesws.webservices.data.CustomerLoyaltyHistory;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.jobs.CustomerDeviceDetailsUtils;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.util.service.APNSNotificationService;
import com.zonesws.webservices.util.service.GCMNotificationService;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.URLUtil;
import com.zonesws.webservices.utils.Util;
import com.zonesws.webservices.utils.mail.MailAttachment;
import com.zonesws.webservices.utils.mail.MailManager;

/**
 * 
 * @author Ulaganathan
 *
 */
public class RTFReferralOrderRewardNotification extends QuartzJobBean implements StatefulJob{
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	private static Logger logger = LoggerFactory.getLogger(RTFReferralOrderRewardNotification.class);
	private static APNSNotificationService apnsNotificationService;
	private static GCMNotificationService gcmNotificationService;
	private static Map<Integer, String> orderPlacedMsgMap = new ConcurrentHashMap<Integer, String>();
	static MailManager mailManager = null;
		
	public APNSNotificationService getApnsNotificationService() {
		return apnsNotificationService;
	}

	public void setApnsNotificationService(
			APNSNotificationService apnsNotificationServiceTemp) {
		apnsNotificationService = apnsNotificationServiceTemp;
	}

	public GCMNotificationService getGcmNotificationService() {
		return gcmNotificationService;
	}

	public void setGcmNotificationService(
			GCMNotificationService gcmNotificationServiceTemp) {
		gcmNotificationService = gcmNotificationServiceTemp;
	}

	public MailManager getMailManager(){
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		RTFReferralOrderRewardNotification.mailManager = mailManager;
	}


	public void sendRewardNotifications(Customer customer , CustomerLoyaltyHistory customerLoyaltyHistory){
		
		/*Thank you for your referral.
		Amit Raut just placed an order using your referral code and you just earned (?) Rewards Points!*/
		
		try{
		
			List<CustomerDeviceDetails> deviceList = CustomerDeviceDetailsUtils.getAllActiveDevicesByCustomerId(customerLoyaltyHistory.getCustomerId());
			
			
				
				String message = "Thank you for your referral. " +
					customer.getCustomerName()+" "+customer.getLastName()+" just placed an order using your referral code and you just earned "+customerLoyaltyHistory.getPointsEarned()+" Rewards Dollars!";
	
				
				
				customer = CustomerUtil.getCustomerById(customerLoyaltyHistory.getCustomerId());
				if(customer != null){
					Map<String,Object> mailMap = new HashMap<String,Object>();
					mailMap.put("customer", customer); 
					mailMap.put("customerLoyaltyHistory", customerLoyaltyHistory);
					mailMap.put("rewardPointsLink", URLUtil.getRewardPoints());
					
					
					//inline(image in mail body) image add code
			    MailAttachment[] mailAttachment = Util.getEmailAttachmentForRegistrationTemplate(true);
				try {
					mailManager.sendMailNow("text/html",URLUtil.fromEmail, customer.getEmail(), 
							null,URLUtil.bccEmails, 
							"Reward The Fan:Referral Order Reward Dollars Information",
				    		"mail-reward-points-referral.html", mailMap, "text/html", null,mailAttachment,null);
						} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				String jsonString="";
				
				if(deviceList!=null && !deviceList.isEmpty()){
				
				for (CustomerDeviceDetails device : deviceList) {
					
					switch (device.getApplicationPlatForm()) {
					
						case ANDROID:
							NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
							notificationJsonObject.setNotificationType(NotificationType.MY_REWARD);
							notificationJsonObject.setMessage(message);
							notificationJsonObject.setCustomerId(customerLoyaltyHistory.getCustomerId());
							Gson gson = new Gson();	
							jsonString = gson.toJson(notificationJsonObject);
							gcmNotificationService.sendMessage(NotificationType.REFERRAL_ORDER_REWARD, device.getNotificationRegId(), jsonString);
							break;
							
						case IOS:
						    Map<String, String> customFields = new HashMap<String, String>();
						    customFields.put("notificationType", String.valueOf(NotificationType.MY_REWARD));
						    customFields.put("customerId", String.valueOf(customerLoyaltyHistory.getCustomerId()));
							apnsNotificationService.sendNotification(NotificationType.REFERRAL_ORDER_REWARD, device.getNotificationRegId(), message,customFields);
							break;

			
						default:
							break;
					}
				}
			}else{
				System.out.println("REFERRAL_ORDER_REWARD_NOTIFICATION : No Active Devices are found.");
			}
			
			
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
}
