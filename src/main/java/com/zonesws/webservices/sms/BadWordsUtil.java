package com.zonesws.webservices.sms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BadWordsUtil {

	public static void main(String[] args) throws Exception {

		File file = new File("C:\\Code\\badwords.txt");

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;

		Set<String> badWordsList = new HashSet<String>();
		
		badWordsList.addAll(getAllOthers());

		while ((st = br.readLine()) != null) {
			badWordsList.add(st.trim());
		}

		int i = 1;

		List<String> singleWordList = new ArrayList<String>();
		List<String> multipleWordList = new ArrayList<String>();

		for (String word : badWordsList) {
			 
			List<String> inputString = Arrays.asList(word.trim().split(" "));
			
			System.out.println("--------" + i + ":" + word + "--------"+inputString);
			i++;
			if (null != inputString && !inputString.isEmpty() && inputString.size() > 1) {
				multipleWordList.add(word);
				continue;
			}
			singleWordList.add(word);
		}
		System.out.println(badWordsList.size());
		System.out.println(singleWordList.size());
		System.out.println(multipleWordList.size());
		
		String singleWord = "", doubleWord = "";
		int k =0;
		for (String string : singleWordList) {
			string = string.trim();
			if(k == 0) {
				singleWord = string;
			}else {
				singleWord += ","+string;
			}
			k++;
		}
		
		k =0;
		for (String string : multipleWordList) {
			string = string.trim();
			if(k == 0) {
				doubleWord = string;
			}else {
				doubleWord += ","+string;
			}
			k++;
		}
		
		
		FileOutputStream outputStream = new FileOutputStream("C:\\Code\\BadWords\\Single.txt");
	    byte[] strToBytes = singleWord.getBytes();
	    outputStream.write(strToBytes);
	    outputStream.close();
	    
	    outputStream = new FileOutputStream("C:\\Code\\BadWords\\Double.txt");
	    byte[] strToBytes1 = doubleWord.getBytes();
	    outputStream.write(strToBytes1);
	    outputStream.close();
		
		  

	}
	
	
	public static Set<String> getAllOthers() throws Exception {

		File file = new File("C:\\Code\\badwordscomma.txt");

		BufferedReader br = new BufferedReader(new FileReader(file));

		String st;
		
		List<String> badWordsList = new ArrayList<String>();
		
		while ((st = br.readLine()) != null) {
			badWordsList.add(st.trim());
		}
		
		Set<String> uniqueBadWordsList = new HashSet<String>(); 
		
		for (String main : badWordsList) {
			List<String> inputString = Arrays.asList(main.trim().split(","));
			for (String sub : inputString) {
				uniqueBadWordsList.add(sub.trim());
			}
		}
		br.close();
		System.out.println(uniqueBadWordsList.size());
		return uniqueBadWordsList;
	}

}
