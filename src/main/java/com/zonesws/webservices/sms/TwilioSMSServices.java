package com.zonesws.webservices.sms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;



public class TwilioSMSServices  {
		
	// Find your Account Sid and Token at twilio.com/user/account
	  public static final String ACCOUNT_SID = "ACc0d5b89953472346ae11d9b6e0e3289e";
	  public static final String AUTH_TOKEN = "80b54238355b198fa9abd7be0560c456";
	  public static final String TWILIO_NUMBER = "+18665751799";
	  public static final String MESSAGE_SID = "MG6f3750be0942622d33029aa26a04f33a";
	  
	  public static final String[] devTeamNumbers = new String[]{"+17327255647", "+919884512528","+919952666784","+919740310298"};
	   
	  
	  
	 
	 
	 public static String testMessage(String phoneNo) {
		    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		    try{
		    	String msg = "Reward The Fann: Twilio SMS Service Testing By Dev Team.";
		    	Message message = Message.creator( new com.twilio.type.PhoneNumber(phoneNo), MESSAGE_SID,msg).create();
	    		System.out.println("Twilio SMS :  Send To : "+phoneNo+", SMS Status : "+message.getSid());
		    	return message.getSid();
		    
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return null;
		  }
	  

	  
	  
	  
	  /*public static String sendMessageHTML(String phoneNo) {
		    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		    try{
		    	
		    	String str = "<font color=#FFFFFF>Download the Rewardthefan App: <br /> " +
					"<b>IOS App : </b>https://itunes.apple.com/us/app/reward-the-fan/id1140367203?ls=1&mt=8<br />" +
					"<b>Andriod App : </b>https://play.google.com/store/apps/details?id=com.rewardthefan&hl=en</font>";
		    	
		    	Message message = Message
		        .creator(new PhoneNumber(phoneNo), new PhoneNumber(TWILIO_NUMBER),str)
		        .create();
		    	
		    	System.out.println("Sent From : "+TWILIO_NUMBER+", Send To : "+phoneNo+", SMS Status : "+message.getSid());
		    	
		    	return message.getSid();
		    
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return null;
		  }*/

	  public static String sendMessage(String phoneNo) {
	    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	    try{
	    	
	    	//Hello Reward The Fan Beta Users � we just wanted to emphasize that you MUST first DELETE the previous app BEFORE downloading the new App from App Store and from Google Play Store. See you all on Sunday, 8/19 @ 8pm
	    	
	    	String msg = "Thank you for downloading Reward The Fan, please complete your registration at your convenience."
	    			+ "Music Trivia Tonight @ 8pm ET- Win 2 FREE concert tickets! http://onelink.to/5xwz4g";
	    	
	    	Message message = Message
	        .creator(new PhoneNumber("+1"+phoneNo), new PhoneNumber(TWILIO_NUMBER),msg)
	        .create();
	    	System.out.println("Sent From : "+TWILIO_NUMBER+", Send To : "+phoneNo+", SMS Status : "+message.getSid());
	    	
	    	return message.getSid();
	    
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	    return null;
	  }
	  
	  public static String sendOTPOld(String phoneNo, String verifiationCode, Integer minute) {
		    
		    try{
		    	Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

		    	String text = verifiationCode+" is your Reward The Fan Verification Code! This code will expire in "+minute+" minutes";
		    	Message message = Message
		        .creator(new PhoneNumber("+1"+phoneNo), new PhoneNumber(TWILIO_NUMBER), text)
		        .create();
		    	System.out.println("OTP Sent From : "+TWILIO_NUMBER+", Send To : "+phoneNo+", SMS Status : "+message.getSid());
		    	return message.getSid();
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return null;
		  }
	  
	  
	  public static String sendOTP(String phoneNo, String verifiationCode, Integer minute) {
		    try{
		    	Twilio.init(ACCOUNT_SID, AUTH_TOKEN); 
		    	String text = verifiationCode+" is your Reward The Fan Verification Code! This code will expire in "+minute+" minutes.";
		    	Message message = Message.creator( new com.twilio.type.PhoneNumber("+1"+phoneNo), MESSAGE_SID,text).create();
		    	//System.out.println("Twilio SMS :  Send To : "+phoneNo+", OTP: "+verifiationCode+", SMS Status : "+message.getSid());
		    	return message.getSid();
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return null;
		  }
	  
	  
	  public static String sendSuperFanWinnerMessage(String phoneNo) {
		    try{
		    	Twilio.init(ACCOUNT_SID, AUTH_TOKEN); 
		    	String text ="Congratulations on your free pass into tonight�s RTF Super Fan Lottery at 6pm ET! http://onelink.to/5xwz4g";
		    	if(phoneNo.contains("+1")) {
		    		Message message = Message.creator( new com.twilio.type.PhoneNumber(phoneNo), MESSAGE_SID,text).create();
		    		System.out.println("Twilio SMS :  Send To : "+phoneNo+", SMS Status : "+message.getSid());
			    	return message.getSid();
		    	}else {
		    		Message message = Message.creator( new com.twilio.type.PhoneNumber("+1"+phoneNo), MESSAGE_SID,text).create();
		    		System.out.println("Twilio SMS :  Send To : "+phoneNo+", SMS Status : "+message.getSid());
			    	return message.getSid();
		    	}
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return null;
		  }
	  
	  
	  public static void notifyContestCreditJobStatus(String messageText) {
		  System.out.println("QUIZ CONTEST CREDIT : "+messageText); 
		  Twilio.init(ACCOUNT_SID, AUTH_TOKEN); 
		  for (String phone : devTeamNumbers) {
			  try{
		    	Message.creator(new com.twilio.type.PhoneNumber(phone), MESSAGE_SID,messageText).create();
			  }catch(Exception e){
			    e.printStackTrace();
			  }
		  }
	  }
	  
	  
	  public static String superFanGameNotification(String phoneNo) {
		    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		    try{
		    	
		    	String msg = "Reward The Fan here! " + 
		    			"See you @ 8pm ET for our SUPERFAN GAME! Win Free Tickets to ANY live event- we�re talking concerts, sporting events & theatre! PLUS our Grand Prize is Tickets FOR A YEAR along with AMC, OLIVE GARDEN AND DUNKIN DONUTS GIFT CARDS in game. Don�t be late, see ya @ 8! http://onelink.to/5xwz4g";
		    	
		    	Message message = Message.creator(new com.twilio.type.PhoneNumber(phoneNo), MESSAGE_SID,msg).create();
		    	System.out.println("Sent From : "+message.getStatus()+", Send To : "+phoneNo+", SMS Status : "+message.getSid());
		    	
		    	return message.getSid();
		    
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return null;
		  }

	  public static String superFanWinnerNotification(String phoneNo) {
		    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		    try{
		    	
		    	String msg = "Reward The Fan here! " + 
		    			"See you @ 8pm ET for our Live Triva Game! Don�t be late, see ya @ 8! http://onelink.to/5xwz4g . Reply STOP to opt out";
		    	Message message = Message.creator(new com.twilio.type.PhoneNumber(phoneNo), MESSAGE_SID,msg)
		    			.create();
		    	System.out.println("Sent From : "+message.getStatus()+", Send To : "+phoneNo+", SMS Status : "+message.getSid());
		    	
		    	return message.getSid();
		    
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    return null;
		  }
	  
	  
	  public static void mainNew(String[] args) throws Exception {
		  System.out.println("Begin : "+ new Date());
		  File file = new File("C:\\Code\\ToBeSent\\Phone.txt");

	  	  BufferedReader br = new BufferedReader(new FileReader(file));

	  	  String st;
	  	  
	  	  List<String> phoneNumbers = new ArrayList<String>();
	  	  
	  	  while ((st = br.readLine()) != null) {
	  	    
	  	    phoneNumbers.add(st);
	  	  }
	  	  
	  	  Date start = new Date();
	  	  
	  	 System.out.println("End : "+ new Date());
	  	  
	  	  int i=0;
	  	  for (String phone : phoneNumbers) {
	  		i++;
	  		  System.out.println("-----------------------------"+i+":"+phone+"-------------------------------------");
	  		  try {
	  			superFanGameNotification("+1"+phone);
	  		  }catch(Exception e) {
	  			  e.printStackTrace();
	  		  }
	  		System.out.println("-----------------------------"+i+":"+phone+"-------------------------------------");
	  		
		 } 
	  	  
	  	System.out.println(phoneNumbers.size());
	  	System.out.println("Start : "+ start+", End at :"+new Date());
		  
	 }
	  
	  public static void main(String[] args) {
		  testMessage("+17327255647");
			 
		}

}
