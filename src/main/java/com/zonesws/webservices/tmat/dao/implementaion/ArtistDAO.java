package com.zonesws.webservices.tmat.dao.implementaion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.zonesws.webservices.dao.implementaion.HibernateDAO;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.enums.ArtistStatus;


public class ArtistDAO extends HibernateDAO<Integer, Artist> implements com.zonesws.webservices.tmat.dao.services.ArtistDAO {

	public List<Artist> getArtistListByAliasName(String aliasName) {
		String hql="FROM Artist WHERE aliasArtistName like ?  AND artistStatus=? ORDER BY aliasArtistName";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + aliasName + "%");
		parameters.add(ArtistStatus.ACTIVE);
		return find(hql, parameters.toArray());
	}
	
	public Collection<Artist> getArtistsByName(String name){
		Collection<Artist> list = new ArrayList<Artist>();
		Session session = getSession();
		try{
			Query query = session.createQuery("from Artist where name like :name and artistStatus = :status");
			query.setParameter("status", ArtistStatus.ACTIVE);
			query.setParameter("name", "%" + name + "%");
			list = query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			releaseSession(session);
		}
		return list;
	}
	
	public List<Artist> getArtistByChildCategoryId(Integer childCategoryId) {
		List<Artist> artist = null;
		
		try{
			artist = find("FROM Artist WHERE  grandChildCategory.childCategory.id =? AND artistStatus=? order by aliasArtistName", new Object[]{childCategoryId,ArtistStatus.ACTIVE});
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return artist;
	}
	
	public List<Artist> getArtistByGrandChildCategoryId(Integer grandChildCategoryId) {
		
		List<Artist> artist= null;
		//List<Artist> artistList = new ArrayList<Artist>();
		try{
			artist = find("FROM Artist WHERE  grandChildCategory.id =? AND artistStatus=? ORDER BY aliasArtistName", new Object[]{grandChildCategoryId,ArtistStatus.ACTIVE});
			/*for(Tour tourOb : tour){
				Long count = getEventCountByTourId(tourOb.getId());
				tourOb.setEventCount(count);
				tourList.add(tourOb);
				
			}*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return artist;
	}
	
	public Artist getArtistById(Integer id) {
		return findSingle("FROM Artist WHERE id = ? AND artistStatus=?", new Object[]{id,ArtistStatus.ACTIVE});
	}
	
	public Map<Integer, Artist> getArtistsByArtistIds(List<Integer> tIds) {

		if(tIds != null && !tIds.isEmpty()){
		int i =0;
		int fromIndex=0;
		Map<Integer, List<Integer>> tIdsMap = new HashMap<Integer, List<Integer>>();
		for(int j =0 ;j<=tIds.size();j=j+2000){
			if(tIds.size()<2000){
				tIdsMap.put(i,tIds.subList(fromIndex,tIds.size()));
				
			}
			else{
				
				if(tIds.size()>(fromIndex)){
					if(tIds.size()>=(fromIndex+2000)){
						tIdsMap.put(i,tIds.subList(fromIndex, fromIndex+2000));
					}else{
						tIdsMap.put(i,tIds.subList(fromIndex,tIds.size()));
						System.out.println(tIds.size()-fromIndex+" message Size:"+"");
					}
						fromIndex += 2000;  
					
				}
					i++;
			}
		}
		Map<Integer,Artist> result = new  HashMap<Integer,Artist>();
		Session session  = getSession();
		try{
			for(int k=0;k<tIdsMap.size();k++){
				
				List<Integer> ids = tIdsMap.get(k);					
				Query query = session.createQuery("FROM Artist WHERE id in (:ids) AND artistStatus= :status");		   
				try{
					query.setParameterList("ids", ids);
					query.setParameter("status", ArtistStatus.ACTIVE);
					List<Artist> list= query.list();					
					for(Artist artist: list){
						result.put(artist.getId(),artist);
					}
				}catch (Exception e) {
					e.printStackTrace();
				}finally{
					query=null;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		
		return result;
	}
		return null;
}
	
}
