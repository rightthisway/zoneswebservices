package com.zonesws.webservices.tmat.dao.implementaion;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.zonesws.webservices.data.UserOrder;
import com.zonesws.webservices.data.ZoneRGBColor;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.utils.AutoCatsTicketGroup;
import com.zonesws.webservices.utils.TicketGroup;
import com.zonesws.webservices.utils.TicketUtil;
import com.zonesws.webservices.utils.ZoneEvent;

public class MinicatsQueryManagerDAO {
	
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	
	 public  List<ZoneEvent> getMinicatsEventsBySearchKeywordByLike(String searchText,Integer pageNumber,Integer maxRows) {
			
			
			String sql ="select count(1) over () totalRows, t.* from (select distinct e.id as eventId," +
					"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
					"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
					"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
					"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.price,e.discount_zone_flag,e.event_date," +
					"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
					"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 price  from minicat_category_ticket zt WITH(NOLOCK) " +
					"where  zt.event_id=e.id and zt.status=1 and zt.price >0 order by price ) zp where " +
					"( e.name like '%"+searchText+"%' or a.name like '%"+searchText+"%' or v.building like '%"+searchText+"%' or " +
					"v.city like '"+searchText+"%' or v.state like '"+searchText+"%' or  v.postal_code like '"+searchText+"%' ) and " +
					"v.building is not null and e.status=1  and e.minicats_enabled is not null and e.minicats_enabled=1   ) t order by t.event_date,t.event_time " +
					"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
			
			List<ZoneEvent> events =new ArrayList<ZoneEvent>();
			Query query = null;
			Session session = null;
			try {
				session = sessionFactory.openSession();
				query = session.createSQLQuery(sql);
				
				List<Object[]> result = (List<Object[]>)query.list();
				
				ZoneEvent zoneEvent = null;
				for (Object[] object : result) {
					zoneEvent= new ZoneEvent();
					zoneEvent.setTotalEvents((Integer)object[0]);
					zoneEvent.setEventId((Integer)object[1]);
					zoneEvent.setEventName((String)object[2]);
					zoneEvent.setArtistName((String)object[3]);
					zoneEvent.setVenueCountry((String)object[4]);
					zoneEvent.setVenueState((String)object[5]);
					zoneEvent.setVenueCity((String)object[6]);
					zoneEvent.setEventDate((String)object[7]);
					zoneEvent.setEventTime((String)object[8]);
					zoneEvent.setVenueZipCode((String)object[9]);
					if(object[10] == null){
						continue;
					}
					if(object[11] == null){
						continue;
					}
					if(null == object[12]){
						continue;
					}
					zoneEvent.setVenueId((Integer)object[10]);
					zoneEvent.setVenueCategoryName((String)object[11]);
					zoneEvent.setVenueName((String)object[12]);
					zoneEvent.setPriceStartFrom((Integer)object[13]);
					zoneEvent.setDiscountFlag((Boolean)object[14]);
					events.add(zoneEvent);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally  {
				session.close();
			}
			return events;
		}
	
	
public  List<ZoneEvent> getMinicatsEventsByZipCodeByLike(List<String> zipcodes,Integer pageNumber,Integer maxRows) {
		
		
		String sql ="select count(1) over () totalRows, t.* from (select distinct e.id as eventId," +
				"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.price,e.discount_zone_flag,e.event_date," +
				"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
				"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 price  from minicat_category_ticket zt WITH(NOLOCK) " +
				"where  zt.event_id=e.id and zt.status=1 and zt.price >0 order by price ) zp where " +
				" v.postal_code in (:zipcodes) and e.minicats_enabled is not null and e.minicats_enabled=1 and " +
				"v.building is not null and e.status=1  ) t order by t.event_date,t.event_time " +
				"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql).setParameterList("zipcodes", zipcodes);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setTotalEvents((Integer)object[0]);
				zoneEvent.setEventId((Integer)object[1]);
				zoneEvent.setEventName((String)object[2]);
				zoneEvent.setArtistName((String)object[3]);
				zoneEvent.setVenueCountry((String)object[4]);
				zoneEvent.setVenueState((String)object[5]);
				zoneEvent.setVenueCity((String)object[6]);
				zoneEvent.setEventDate((String)object[7]);
				zoneEvent.setEventTime((String)object[8]);
				zoneEvent.setVenueZipCode((String)object[9]);
				if(object[10] == null){
					continue;
				}
				if(object[11] == null){
					continue;
				}
				if(null == object[12]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[10]);
				zoneEvent.setVenueCategoryName((String)object[11]);
				zoneEvent.setVenueName((String)object[12]);
				zoneEvent.setPriceStartFrom((Integer)object[13]);
				zoneEvent.setDiscountFlag((Boolean)object[14]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}


 public List<AutoCatsTicketGroup> getMiniCatsTicketsByEventId(Integer eventId,Map<String, ZoneRGBColor> rgbColorMap,Map<Integer, Boolean> lockedTicketMap) {
		
		String sql =" select id,event_id,zone,section,row,quantity,price,shipping_method " +
				",CONVERT(VARCHAR(19),shipping_date,101) as shipping_date " +
				"from minicat_category_ticket WITH(NOLOCK) where event_id="+eventId+" and status=1 and " +
				"price >0 order by section,quantity";
		
		DecimalFormat df = new DecimalFormat("#.##");

		List<AutoCatsTicketGroup> ticketGroups = new ArrayList<AutoCatsTicketGroup>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			
			AutoCatsTicketGroup ticketGroup = null;
			
			for (Object[] object : result) {
				ticketGroup= new AutoCatsTicketGroup();
				Integer ticketGroupId = (Integer)object[0];
				
				Boolean isLockedTicket = lockedTicketMap.get(ticketGroupId);				
				if(null != isLockedTicket && isLockedTicket){
					continue;
				}
				
				ticketGroup.setId(ticketGroupId);
				ticketGroup.setEventId((Integer)object[1]);

				String zone = (String)object[2];
				ticketGroup.setZone(zone);
				
				zone = zone.toUpperCase().replace("ZONE", "").replaceAll(" +","");
				ZoneRGBColor zoneRGBColor = rgbColorMap.get(zone);
				
				if(null != zoneRGBColor){
					ticketGroup.setColorCode(zoneRGBColor.getColor());
					ticketGroup.setRgbColor(zoneRGBColor.getRgbColor());
				}else{
					ticketGroup.setColorCode("#F4ED6D");
					ticketGroup.setRgbColor("R:244 G:237 B:109");
				}
				
				String section = (String)object[3];
				String row = (String)object[4];	
				
				ticketGroup.setSection(section);
				ticketGroup.setRow(row);
				Integer qty =(Integer)object[5];
				Integer decimal = (Integer)object[6];
				Double price =decimal.doubleValue();
				
				ticketGroup.setQuantity(qty);
				ticketGroup.setUnitPrice(Double.valueOf(df.format(price)));
				ticketGroup.setTotal(Double.valueOf(df.format(qty * price)));
				ticketGroup.setStatus(Status.ACTIVE);
				
				section = section.replaceAll("-", " thru ");
				row = row.replaceAll("-", " thru ");
				ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+section+" between row(s) "+row+".");

				if(qty == 1){
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Section "+section);
				}else{
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be seated together in Section "+section);
				}
				
				boolean isGAZone = TicketUtil.isGeneralAdmissionZone(section);
				if(isGAZone){
					ticketGroup.setZoneDescription("Tickets will be in section(s) "+section+" between row(s) "+row+".");
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Section "+section);
				}
				ticketGroup.setMessage("");
				ticketGroup.setTicketDeliveryType((String)object[7]);
				ticketGroup.setTicketDeliveryDate((String)object[8]);
				ticketGroups.add(ticketGroup);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return ticketGroups;
	}


 public List<AutoCatsTicketGroup> getMinicatsTicketsByTicketGroupId(Integer ticketGroupId,Map<String, ZoneRGBColor> rgbColorMap,Map<Integer, Boolean> lockedTicketMap) {
	
		String sql =" select id,event_id,zone,section,row,quantity,price,shipping_method " +
		",CONVERT(VARCHAR(19),shipping_date,101) as shipping_date " +
		"from minicat_category_ticket WITH(NOLOCK) where id="+ticketGroupId+" and status=1 and " +
		"price >0 order by section,quantity";

		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println(sql);
		
		List<AutoCatsTicketGroup> ticketGroups = new ArrayList<AutoCatsTicketGroup>();
		Query query = null;
		Session session = null;
		try {
		session = sessionFactory.openSession();
		query = session.createSQLQuery(sql);
		List<Object[]> result = (List<Object[]>)query.list();
		
		AutoCatsTicketGroup ticketGroup = null;
		
		for (Object[] object : result) {
			ticketGroup= new AutoCatsTicketGroup();
			Integer id = (Integer)object[0];
			
			Boolean isLockedTicket = lockedTicketMap.get(id);				
			if(null != isLockedTicket && isLockedTicket){
				continue;
			}
			
			ticketGroup.setId(ticketGroupId);
			ticketGroup.setEventId((Integer)object[1]);
		
			String zone = (String)object[2];
			ticketGroup.setZone(zone);
			
			zone = zone.toUpperCase().replace("ZONE", "").replaceAll(" +","");
			ZoneRGBColor zoneRGBColor = rgbColorMap.get(zone);
			
			if(null != zoneRGBColor){
				ticketGroup.setColorCode(zoneRGBColor.getColor());
				ticketGroup.setRgbColor(zoneRGBColor.getRgbColor());
			}else{
				ticketGroup.setColorCode("#F4ED6D");
				ticketGroup.setRgbColor("R:244 G:237 B:109");
			}
			
			String section = (String)object[3];
			String row = (String)object[4];	
			
			ticketGroup.setSection(section);
			ticketGroup.setRow(row);
			Integer qty =(Integer)object[5];
			Integer decimal = (Integer)object[6];
			Double price =decimal.doubleValue();
			
			ticketGroup.setQuantity(qty);
			ticketGroup.setUnitPrice(Double.valueOf(df.format(price)));
			ticketGroup.setTotal(Double.valueOf(df.format(qty * price)));
			ticketGroup.setStatus(Status.ACTIVE);
			
			section = section.replaceAll("-", " thru ");
			row = row.replaceAll("-", " thru ");
			ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+section+" between row(s) "+row+".");
		
			if(qty == 1){
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Section "+section);
			}else{
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be seated together in Section "+section);
			}
			
			boolean isGAZone = TicketUtil.isGeneralAdmissionZone(section);
			if(isGAZone){
				ticketGroup.setZoneDescription("Tickets will be in section(s) "+section+" between row(s) "+row+".");
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Section "+section);
			}
			ticketGroup.setMessage("");
			ticketGroup.setTicketDeliveryType((String)object[7]);
			ticketGroup.setTicketDeliveryDate((String)object[8]);
			ticketGroups.add(ticketGroup);
		}
		
		} catch (Exception e) {
		e.printStackTrace();
		} finally  {
		session.close();
		}
		return ticketGroups;
}
 
 
 public List<AutoCatsTicketGroup> getMinicatsTicketsByTicketGroupId(Integer ticketGroupId) {
		
		String sql =" select id,event_id,zone,section,row,quantity,price,shipping_method " +
		",CONVERT(VARCHAR(19),shipping_date,101) as shipping_date " +
		"from minicat_category_ticket WITH(NOLOCK) where id="+ticketGroupId+" and status=1 and " +
		"price >0 order by section,quantity";

		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println(sql);
		
		List<AutoCatsTicketGroup> ticketGroups = new ArrayList<AutoCatsTicketGroup>();
		Query query = null;
		Session session = null;
		try {
		session = sessionFactory.openSession();
		query = session.createSQLQuery(sql);
		List<Object[]> result = (List<Object[]>)query.list();
		
		AutoCatsTicketGroup ticketGroup = null;
		
		for (Object[] object : result) {
			ticketGroup= new AutoCatsTicketGroup();
			ticketGroup.setId(ticketGroupId);
			ticketGroup.setEventId((Integer)object[1]);
		
			String zone = (String)object[2];
			ticketGroup.setZone(zone);
			
			String section = (String)object[3];
			String row = (String)object[4];	
			
			ticketGroup.setSection(section);
			ticketGroup.setRow(row);
			Integer qty =(Integer)object[5];
			Integer decimal = (Integer)object[6];
			Double price =decimal.doubleValue();
			
			ticketGroup.setQuantity(qty);
			ticketGroup.setUnitPrice(Double.valueOf(df.format(price)));
			ticketGroup.setTotal(Double.valueOf(df.format(qty * price)));
			ticketGroup.setStatus(Status.ACTIVE);
			
			section = section.replaceAll("-", " thru ");
			row = row.replaceAll("-", " thru ");
			ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+section+" between row(s) "+row+".");
		
			if(qty == 1){
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Section "+section);
			}else{
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be seated together in Section "+section);
			}
			boolean isGAZone = TicketUtil.isGeneralAdmissionZone(section);
			if(isGAZone){
				ticketGroup.setZoneDescription("Tickets will be in section(s) "+section+" between row(s) "+row+".");
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Section "+section);
			}
			ticketGroup.setMessage("");
			ticketGroup.setTicketDeliveryType((String)object[7]);
			ticketGroup.setTicketDeliveryDate((String)object[8]);
			ticketGroups.add(ticketGroup);
		}
		
		} catch (Exception e) {
		e.printStackTrace();
		} finally  {
		session.close();
		}
		return ticketGroups;
 }
 
 public int updateMiniTicketGroup(AutoCatsTicketGroup autoCatsTicketGroup) {
		
		String sql ="update  minicat_category_ticket set status=4,last_updated_date =GETDATE(),sold_price="+autoCatsTicketGroup.getTicketSoldPrice()+" " +
				"where id="+autoCatsTicketGroup.getId();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			return query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally  {
			session.close();
		}	
	}
 
 
 public  List<UserOrder> getUserOrdersByOrderId(Integer orderId,ProductType productType) {
	 
	 String sql ="select user_Order_id,CONVERT(VARCHAR(19),order_date,101) as orderDate,order_total,transaction_id,payment_menthod,original_order_total," +
	 		"discount_amount,CONVERT(VARCHAR(19),delivery_date,101) as deliveryDate," +
	 		"customer_id,event_id,event_name,CONVERT(VARCHAR(19),event_date,101) as eventDate," +
	 		"CASE WHEN event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_time, 100), 7)) " +
	 		"WHEN event_time is null THEN 'TBD' END as eventTime,CONVERT(VARCHAR(150),REPLACE(building,',','')) as venueBuilding," +
	 		"city,state,country,postal_code,ticket_group_id,category,section,row,qty,price,shipping_method," +
	 		"app_platform,credit_card_type from vw_user_order_customerwise WITH(NOLOCK) where product_type='"+productType.toString()+"' " +
	 		" and  user_Order_id="+orderId;
        
	List<UserOrder> userOrders = new ArrayList<UserOrder>();
	Query query = null;
	Session session = null;
	DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	try {
		session = sessionFactory.openSession();
		query = session.createSQLQuery(sql);
		List<Object[]> result = (List<Object[]>)query.list();
		UserOrder userOrder = null;
		
		for (Object[] object : result) {
			userOrder = new UserOrder();
			userOrder.setId((Integer)object[0]);
			userOrder.setWsOrderDate((String)object[1]);
			userOrder.setOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[2]).doubleValue()));
			userOrder.setTransactionId((String)object[3]);
			userOrder.setPaymentMethod((String)object[4]);
			userOrder.setOriginalOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[5]).doubleValue()));
			userOrder.setDiscountAmount(TicketUtil.getRoundedValue(((BigDecimal)object[6]).doubleValue()));
			userOrder.setWsDeliveryDate((String)object[7]);
			userOrder.setCustomerId((Integer)object[8]);	
			
			ZoneEvent zoneEvent = new ZoneEvent();
			zoneEvent.setEventId((Integer)object[9]);
			zoneEvent.setEventName((String)object[10]);
			zoneEvent.setEventDate((String)object[11]);
			zoneEvent.setEventTime((String)object[12]);
			zoneEvent.setVenueName((String)object[13]);
			zoneEvent.setVenueCity((String)object[14]);
			zoneEvent.setVenueState((String)object[15]);
			zoneEvent.setVenueCountry((String)object[16]);
			zoneEvent.setVenueZipCode((String)object[17]);
			
			AutoCatsTicketGroup ticketGroup = new AutoCatsTicketGroup();
			ticketGroup.setEventId((Integer)object[9]);
			ticketGroup.setId(Integer.parseInt((String)object[18]));
			String zone = (String)object[19];
			String section = (String)object[20];
			String row = (String)object[21];
			Integer qty = (Integer)object[22];
			ticketGroup.setZone(zone);
			ticketGroup.setSection(section);
			ticketGroup.setRow(row);
			ticketGroup.setUnitPrice((Double)object[23]);
			ticketGroup.setQuantity(qty);
			ticketGroup.setStatus(Status.SOLD);
			ticketGroup.setTicketDeliveryType((String)object[24]);
			
			section = section.replaceAll("-", " thru ");
			row = row.replaceAll("-", " thru ");
			ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+section+" between row(s) "+row+".");
			
			if(qty == 1){
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in section "+section);
			}else{
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be seated together in section "+section);
			}
			
			boolean isGAZone = TicketUtil.isGeneralAdmissionZone(section);
			if(isGAZone){
				ticketGroup.setZoneDescription("Tickets will be in section(s) "+section+" between row(s) "+row+".");
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in section "+section);
			}

			userOrder.setAppPlatform((String)object[25]);
			if(null !=object[26]){
				userOrder.setCreditCardType((String)object[26]);
			}
			
			if(userOrder.getPaymentMethod().equals("CREDITCARD")){
				userOrder.setPaymentMethod(userOrder.getPaymentMethod()+" ("+userOrder.getCreditCardType()+")");
			}
			
			if(null == userOrder.getDiscountAmount() || userOrder.getDiscountAmount() == 0){
				userOrder.setDiscountAmount(0.00);
			}
			
			Double unitDiscount = userOrder.getDiscountAmount() / qty;
			ticketGroup.setUnitPrice(ticketGroup.getUnitPrice() - unitDiscount);
			Double totalPrice =ticketGroup.getUnitPrice() * ticketGroup.getQuantity();
			ticketGroup.setTotal(TicketUtil.getRoundedValue(totalPrice));
			
			userOrder.setOrderDate(formatter.parse(userOrder.getWsOrderDate()));
			userOrder.setDeliveryDate(formatter.parse(userOrder.getWsDeliveryDate()));
			userOrder.setZoneEvent(zoneEvent);
			userOrder.setAutoCatsTicketGroup(ticketGroup);
			userOrders.add(userOrder);
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	} finally  {
		session.close();
	}
	return userOrders;
 }
 
 
public  List<UserOrder> getAllUserOrdersByCustomerIdAndProduct(Integer customerId,ProductType productType) {
	 
	 String sql ="select user_Order_id,CONVERT(VARCHAR(19),order_date,101) as orderDate,order_total,transaction_id,payment_menthod,original_order_total," +
	 		"discount_amount,CONVERT(VARCHAR(19),delivery_date,101) as deliveryDate," +
	 		"customer_id,event_id,event_name,event_date,event_time,building,city,state,country,postal_code," +
	 		"ticket_group_id,category,price,qty from vw_user_order_customerwise where customer_id="+customerId+" and " +
	 		"product_type='"+productType.toString()+"' order by order_date desc";
        
	List<UserOrder> userOrders = new ArrayList<UserOrder>();
	Query query = null;
	Session session = null;
	DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	try {
		session = sessionFactory.openSession();
		query = session.createSQLQuery(sql);
		List<Object[]> result = (List<Object[]>)query.list();
		UserOrder userOrder = null;
		
		for (Object[] object : result) {
			userOrder = new UserOrder();
			userOrder.setId((Integer)object[0]);
			userOrder.setWsOrderDate((String)object[1]);
			userOrder.setOrderDate(formatter.parse(userOrder.getWsOrderDate()));
			userOrder.setOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[2]).doubleValue()));
			userOrder.setTransactionId((String)object[3]);
			userOrder.setPaymentMethod((String)object[4]);
			userOrder.setOriginalOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[5]).doubleValue()));
			//userOrder.setTotalDeductedAmount(TicketUtil.getRoundedValue(((BigDecimal)object[6]).doubleValue()));
			userOrder.setDiscountAmount(TicketUtil.getRoundedValue(((BigDecimal)object[6]).doubleValue()));
			//userOrder.setLoyaltySpent(TicketUtil.getRoundedValue(((BigDecimal)object[8]).doubleValue()));
			//userOrder.setPromotionalOffer(TicketUtil.getRoundedValue(((BigDecimal)object[9]).doubleValue()));
			userOrder.setWsDeliveryDate((String)object[7]);
			userOrder.setDeliveryDate(formatter.parse(userOrder.getWsDeliveryDate()));
			userOrder.setCustomerId((Integer)object[8]);	
			userOrders.add(userOrder);
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	} finally  {
		session.close();
	}
	return userOrders;
 }

}
