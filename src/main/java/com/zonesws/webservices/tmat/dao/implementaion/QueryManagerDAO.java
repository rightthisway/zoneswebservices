
package com.zonesws.webservices.tmat.dao.implementaion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;

import com.rtf.ecomerce.data.CountryPhoneCode;
import com.rtfquiz.webservices.utils.list.CustomerFriendDetails;
import com.rtfquiz.webservices.utils.list.CustomerNotificationDetails;
import com.rtfquiz.webservices.utils.list.CustomerSearchDetails;
import com.zonesws.webservices.data.Affiliate;
import com.zonesws.webservices.data.AffiliateCashRewardHistory;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Broker;
import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.data.CountryCity;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.EventArtist;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.data.PaypalApi;
import com.zonesws.webservices.data.PreOrderPageAudit;
import com.zonesws.webservices.data.RTFCategorySearch;
import com.zonesws.webservices.data.RtfGiftCard;
import com.zonesws.webservices.data.Venue;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.utils.DateUtil;
import com.zonesws.webservices.utils.RealTicketSectionDetails;
import com.zonesws.webservices.utils.ZoneEvent;
import com.zonesws.webservices.utils.list.ArtistState;
import com.zonesws.webservices.utils.list.Item;
import com.zonesws.webservices.utils.list.NearestStates;
import com.zonesws.webservices.utils.list.StripeCredentials;
import com.zonesws.webservices.utils.list.TeamCardImageDetails;

public class QueryManagerDAO {
	
	private SessionFactory sessionFactory;
	private static String quizApiLinkedServer;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public static String getQuizApiLinkedServer() {
		return quizApiLinkedServer;
	}

	public void setQuizApiLinkedServer(String quizApiLinkedServer) {
		QueryManagerDAO.quizApiLinkedServer = quizApiLinkedServer;
	}
	
	
	private static Session querySession = null;
	
	
	public static Session getQuerySession() {
		return querySession;
	}

	public static void setQuerySession(Session querySession) {
		QueryManagerDAO.querySession = querySession;
	}
	
	
	public List<Integer> getAllTestCustomerIds() {
		String sql ="select distinct id from customer c where c.product_type='REWARDTHEFAN' and c.cust_image_path is null ";
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	}
	public List<String> getAllUserIdsFilter(String userId) {

		String sql ="select distinct user_id from customer c where c.user_id is not null and c.user_id !='' ";
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<String> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	
	}
	
	public List<CountryCity> getAllCountryCities(String searchText) {
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("DECLARE @searchinput varchar(50),@svlikestart varchar(60), @svlikemid varchar(60),@svlikelast varchar(60),");
		queryString.append("@svnotlikestart varchar(60), @svnotlikemid varchar(60),@svnotlikelast varchar(60);");
		queryString.append("SET @searchinput  = '"+searchText+"';");
		queryString.append("SET  @svlikestart = '%[ ]' + @searchinput + '%';SET  @svlikemid = '%' + @searchinput + '[ ]%';SET  @svlikelast = '%[ ]' + @searchinput + '[ ]%';");
		queryString.append("SET  @svnotlikestart = '%[^ ]' + @searchinput + '%';SET  @svnotlikemid = '%' + @searchinput + '[^ ]%';SET  @svnotlikelast = '%[^ ]' + @searchinput + '[^ ]%';");
		//queryString.append("union all ");
		queryString.append("select A.id, a.city,a.zip_code, a.state, a.state_full_name, a.country, a.country_full_name, a.longitude, a.latitude");
		queryString.append(" from (select 0 as sort_col, c.* from dbo.city_auto_search c");
		queryString.append(" where ( state_full_name = @searchinput or state_full_name like @svlikestart or state_full_name like @svlikemid or  state_full_name like @svlikelast)");
		
		queryString.append(" UNION ALL");
		queryString.append(" select 0 as sort_col, c1.* from dbo.city_auto_search c1");
		queryString.append(" where (state_full_name like @svnotlikestart or state_full_name like @svnotlikemid or  state_full_name like @svnotlikelast)");
		
		queryString.append(" ) A order by a.sort_col OFFSET (1-1)*50 ROWS FETCH NEXT 50 ROWS ONLY");
		
		/*String sql ="select distinct city,state,country,zip_code,longitude,latitude" +
				" from city_auto_search where city like '"+searchText+"%' order by city";*/
		List<CountryCity> countryCities = new ArrayList<CountryCity>();
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryString.toString());
			List<Object[]> result = (List<Object[]>)query.list();
			CountryCity countryCity = null;
			for (Object[] object : result) {
				countryCity = new CountryCity();
				countryCity.setCity((String)object[0]);
				countryCity.setState((String)object[1]);
				countryCity.setCountry((String)object[2]);
				countryCity.setZipcode((String)object[3]);
				countryCity.setStateFullName((String)object[4]);
				countryCity.setCountryFullName((String)object[5]);
				countryCity.setLongitude((String)object[4]);
				countryCity.setLatitude((String)object[5]);
				countryCities.add(countryCity);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return countryCities;
	}
	
	public List<CountryCity> getCityDetailsByCityName(String city){
		String sql = "select city as city,state as state,country as country, "+
		"state_full_name as stateFullName,country_full_name as countryFullName,longitude as longitude,"+
		"latitude as latitude,zip_code as zipcode from city_auto_search WITH(NOLOCK) where city like '"+city+"%'";
		Query query = null;
		List<CountryCity> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(CountryCity.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public boolean validateZipCode(String zipCode){
		String sql = "select country as country,zip_code as zipcode from city_auto_search WITH(NOLOCK) " +
				"where country in ('US','USA') and ( zip_code like '%"+zipCode+"%' or replace(zip_code,' ','') like '%"+zipCode+"%')";
		Query query = null;
		boolean flag = false;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			ArrayList<Object[]> list = (ArrayList<Object[]>) query.list();
			
			if(list != null && !list.isEmpty())
				flag = true;
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return flag;
	}
	
	
	public List<CountryCity> getCityDetailsByZipcode(String zipCode){
		String sql = "select distinct zip_code as zipcode,city,s.id as stateId,s.name as stateFullName," +
				"c.id as countryId,c.country_name as countryFullName,s.short_desc as state, c.short_desc as country " +
				"from city_auto_search  ca WITH(NOLOCK) left join country c WITH(NOLOCK) on c.short_desc=ca.country left join state s WITH(NOLOCK) on s.name=ca.state_full_name " +
				" where country in ('US','CA') and ( zip_code like '%"+zipCode+"%' or replace(zip_code,' ','') like '%"+zipCode+"%') ";
		Query query = null;
		List<CountryCity> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(CountryCity.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<ChildCategory> getGrandChildcategoryByIdandName(){
		List<ChildCategory> childCategory =null;
		String sql = "select distinct e.grand_child_category_id as id, e.grand_child_category_name as name  from event_details e WITH(NOLOCK) " +
		" inner join grand_child_category_image gc WITH(NOLOCK) on(gc.grand_child_category_id=e.grand_child_category_id) "+
		"where grand_child_category_name not in ( 'other','default','none') and gc.circle_image_url is not null";
				
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(ChildCategory.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return childCategory;
	}
	
	public StripeCredentials getStripeCredentials() {
		String sql ="select publishable_key,credential_type from stripe_credentials WITH(NOLOCK) where status='ACTIVE'";
		Query query = null;
		StripeCredentials stripeCredentials = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				stripeCredentials = new StripeCredentials();
				stripeCredentials.setPublishableKey((String)object[0]);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return stripeCredentials;
	}
	
	
	public List<Affiliate> getAllActiveAffiliates() {
		String sql ="select distinct tu.id,tu.firstname,tu.lastname,tu.email,aph.promotional_code,tr.description," +
				"afs.affiliates_discount,afs.customer_discount,afs.affiliates_repeat_business,afs.phone_order_cash_discount," +
				"afs.phone_order_customer_discount,afs.is_give_reward_point,afs.id offerId from " +
				"tracker_users tu WITH(NOLOCK) inner join tracker_user_roles tur WITH(NOLOCK) on tur.user_id=tu.id " +
				"inner join tracker_roles tr WITH(NOLOCK) on tr.id=tur.role_id and tr.id=4 and tu.promotional_code is not null " +
				"and tu.promotional_code != '' inner join  affiliate_promo_code_history aph on aph.user_id=tu.id and " +
				"aph.status='ACTIVE' inner join affiliates_settings afs on afs.user_id=tu.id  where tu.status=1";
		
		Query query = null;
		Affiliate affiliate = null;
		List<Affiliate> affiliates = new ArrayList<Affiliate>();
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				affiliate = new Affiliate();
				affiliate.setUserId((Integer)object[0]);
				affiliate.setFirstName((String)object[1]);
				affiliate.setLastName((String)object[2]);
				affiliate.setEmail((String)object[3]);
				affiliate.setPromotionalCode((String)object[4]);
				affiliate.setDescription((String)object[5]);
				BigDecimal cashReward = (BigDecimal)object[6];
				BigDecimal orderDiscount = (BigDecimal)object[7];
				affiliate.setAffiliateCashReward(cashReward.doubleValue());
				affiliate.setCustomerOrderDiscount(orderDiscount.doubleValue());
				affiliate.setAffiliateRepeatBusiness((Boolean)object[8]);
				BigDecimal phoneCashReward = (BigDecimal)object[9];
				BigDecimal phoneOrderDiscount = (BigDecimal)object[10];
				affiliate.setPhoneOrderCashReward(phoneCashReward.doubleValue());
				affiliate.setPhoneCustomerDiscount(phoneOrderDiscount.doubleValue());
				affiliate.setIsEarnRewardPoints((Boolean)object[11]);
				affiliate.setOfferId((Integer)object[12]);
				affiliates.add(affiliate);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return affiliates;
	}
	
	
	public List<Broker> getAllActiveBrokers() {
		String sql ="select tb.id as brokerId,tu.id as userId,tu.firstname,tu.lastname,tu.email,tb.company_name,tb.service_fees " +
				"from tracker_users tu WITH(NOLOCK) inner join tracker_brokers tb WITH(NOLOCK) on tb.id=tu.broker_id where broker_id is not null and status=1";
		Query query = null;
		Broker broker = null;
		List<Broker> brokers = new ArrayList<Broker>();
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				broker = new Broker();
				broker.setBrokerId((Integer)object[0]);
				broker.setUserId((Integer)object[1]);
				broker.setFirstName((String)object[2]);
				broker.setLastName((String)object[3]);
				broker.setEmail((String)object[4]);
				broker.setCompanyName((String)object[5]);
				BigDecimal serviceFeesPerc = (BigDecimal)object[6];
				broker.setServiceFeesPerc(serviceFeesPerc.doubleValue());
				brokers.add(broker);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return brokers;
	}
	
	
	public Broker getBrokerById(Integer brokerId) {
		String sql ="select tb.id as brokerId,tu.id as userId,tu.firstname,tu.lastname,tu.email,tb.company_name,tb.service_fees " +
				"from tracker_users tu WITH(NOLOCK) inner join tracker_brokers tb WITH(NOLOCK) on tb.id=tu.broker_id and tb.id="+brokerId+" where " +
				"broker_id is not null and status=1 " ;
		Query query = null;
		Broker broker = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				broker = new Broker();
				broker.setBrokerId((Integer)object[0]);
				broker.setUserId((Integer)object[1]);
				broker.setFirstName((String)object[2]);
				broker.setLastName((String)object[3]);
				broker.setEmail((String)object[4]);
				broker.setCompanyName((String)object[5]);
				BigDecimal serviceFeesPerc = (BigDecimal)object[6];
				broker.setServiceFeesPerc(serviceFeesPerc.doubleValue());
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return broker;
	}
	
	public Affiliate getActiveAffiliateByPromoCode(String prmoCode) {
		String sql ="select distinct tu.id,tu.firstname,tu.lastname,tu.email,aph.promotional_code,tr.description," +
				"afs.affiliates_discount,afs.customer_discount,afs.affiliates_repeat_business,afs.id offerId, " +
				"afs.phone_order_cash_discount,afs.phone_order_customer_discount,afs.is_give_reward_point from " +
				"tracker_users tu WITH(NOLOCK) inner join tracker_user_roles tur WITH(NOLOCK) on tur.user_id=tu.id " +
				"inner join tracker_roles tr WITH(NOLOCK) on tr.id=tur.role_id and tr.id=4 inner join " +
				"affiliate_promo_code_history aph on aph.user_id=tu.id and aph.status='ACTIVE' inner join " +
				"affiliates_settings afs on afs.user_id=tu.id where tu.status=1 and " +
				"aph.promotional_code='"+prmoCode+"'";
		Query query = null;
		Affiliate affiliate = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				affiliate = new Affiliate();
				affiliate.setUserId((Integer)object[0]);
				affiliate.setFirstName((String)object[1]);
				affiliate.setLastName((String)object[2]);
				affiliate.setEmail((String)object[3]);
				affiliate.setPromotionalCode((String)object[4]);
				affiliate.setDescription((String)object[5]);
				BigDecimal cashReward = (BigDecimal)object[6];
				BigDecimal orderDiscount = (BigDecimal)object[7];
				affiliate.setAffiliateCashReward(cashReward.doubleValue());
				affiliate.setCustomerOrderDiscount(orderDiscount.doubleValue());
				affiliate.setAffiliateRepeatBusiness((Boolean)object[8]);
				affiliate.setOfferId((Integer)object[9]);
				BigDecimal cashReward1 = (BigDecimal)object[10];
				BigDecimal orderDiscount1 = (BigDecimal)object[11];
				affiliate.setPhoneOrderCashReward(cashReward1.doubleValue());
				affiliate.setPhoneCustomerDiscount(orderDiscount1.doubleValue());
				affiliate.setIsEarnRewardPoints((Boolean)object[12]);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return affiliate;
	}
	
	public String getStripeCredentialsByType() {
		String sql ="select secret_key,publishable_key from stripe_credentials WITH(NOLOCK) where status='ACTIVE'";
		Query query = null;
		String stripeCredentials = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				stripeCredentials= (String)object[0];
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return stripeCredentials;
	}
	
	public List<Object[]> getZipCodes(){
		//String sql ="select distinct zip_code,latitude,longitude from city_auto_search";
		String sql ="select distinct postal_code,latitude,langitude from zipcodes WITH(NOLOCK)";
		Query query = null;
		List<Object[]> result = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			result = (List<Object[]>)query.list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return result;
	}
	
	public List<PaypalApi> getPaypalCredentialByType() {
		String sql ="select url,client_id,secret_key,credential_type from paypal_credentials WITH(NOLOCK)";
		PaypalApi paypalApi = null;
		Query query = null;
		List<PaypalApi> paypalApiList = new ArrayList<PaypalApi>();
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				paypalApi = new PaypalApi();
				paypalApi.setUrl((String)object[0]);
				paypalApi.setClientId((String)object[1]);
				paypalApi.setSecretKey((String)object[2]);
				paypalApi.setCredentialType((String)object[3]);
				paypalApiList.add(paypalApi);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return paypalApiList;
	}
	
	public List<ChildCategory> getChildcategoryByIdandName(){
		List<ChildCategory> childCategory =null;
		String sql = "select distinct A.id,A.name,A.imageUrl, A.parentCategoryName from (select distinct cc.id as id,cc.name as name, " +
				"CONVERT(VARCHAR(150),cci.circle_image_url) as imageUrl,zp.minPrice,CONVERT(VARCHAR(20),e.parent_category_name) as parentCategoryName " +
				"from child_category_image cci WITH(NOLOCK) inner join child_category cc WITH(NOLOCK) on cc.id=cci.child_category_id " +
				"inner join  event_details e WITH(NOLOCK) on e.child_category_id=cc.id and e.reward_thefan_enabled=1 CROSS APPLY ( select MIN(price) minPrice," +
				"MAX(price) as maxPrice from category_ticket_group zt WITH(NOLOCK) where  zt.event_id=e.event_id and " +
				"zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event  ) A " +
				" where A.minPrice is not null order by A.name asc";
				
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(ChildCategory.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return childCategory;
	}
	
	public List<ChildCategory> getSelectedChilds(){
		List<ChildCategory> childCategory =null;
		String sql = "select id, name from child_category where (name in ( 'Broadway') or id in (9))";
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(ChildCategory.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return childCategory;
	}
	
	
	public List<ChildCategory> getSelectedGrandChilds(){
		List<ChildCategory> childCategory =null;
		
		String sql = "select id, name from grand_child_category WITH(NOLOCK) where tn_grand_child_category_id in (16,19,30,32) ";
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(ChildCategory.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return childCategory;
	}
	
	public List<ChildCategory> getSelectedParentCategories(){
		List<ChildCategory> childCategory =null;
		String sql = "select id,name from parent_category WITH(NOLOCK) where name  in ('SPORTS','CONCERTS','THEATER')";
		//String sql = "select id,name from parent_category where name  in ('SPORTS','CONCERTS','THEATRE','OTHER')";
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(ChildCategory.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return childCategory;
	}
	
	
	public List<GrandChildCategory> getAllGrandChildCategoriesByChildId(Integer childId){
		List<GrandChildCategory> childCategory =null;
		String sql = "select distinct A.id,A.name,A.childCatId from (select distinct gc.id , gc.name,e.child_category_id as childCatId,zp.minPrice " +
				"from grand_child_category gc WITH(NOLOCK) inner join event_details e WITH(NOLOCK) on e.grand_child_category_id=gc.id and " +
				"e.reward_thefan_enabled=1 CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt " +
				"WITH(NOLOCK) where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp " +
				"where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and gc.name not in ('Other','Default','None') and e.child_category_id="+childId+" ) A " +
				" where A.name not like '-' order by A.name asc";
				
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(GrandChildCategory.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return childCategory;
	}
	
	
	public List<GrandChildCategory> getAllSportsGrandChildCategories(){
		List<GrandChildCategory> childCategory =null;
		String sql = "select distinct A.id,A.name from (select distinct gc.id , gc.name,e.child_category_id as childCatId,zp.minPrice " +
				"from grand_child_category gc WITH(NOLOCK) inner join event_details e WITH(NOLOCK) on e.grand_child_category_id=gc.id and " +
				"e.reward_thefan_enabled=1 CROSS APPLY ( select MIN(price) minPrice,MAX(price) as maxPrice from category_ticket_group zt " +
				"WITH(NOLOCK) where  zt.event_id=e.event_id and zt.status='ACTIVE' and zt.price >0 and zt.product_type='REWARDTHEFAN' ) zp " +
				"where e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and gc.name not in ('Other','Default','None') and e.parent_category_id=1 ) A " +
				" where A.name not like '-'  order by A.name asc";//where A.minPrice is not null
				
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(GrandChildCategory.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return childCategory;
	}
	
public ZoneEvent getZoneEventsByEventId(Integer eventId,ProductType productType) {
		
		String sql =" select e.id as eventId,REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, " +
				"v.country as venueCountry ,v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.id,e.venue_category_name,v.postal_code as postalCode," +
				"a.id as artistId,CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding,e.discount_zone_flag, " +
				" gc.name as grandchildname,cc.name as childname,pc.name as parentname,e.discount_per as discountPer," +
				" e.presale_disc_per as presaleDiscPer from event e WITH(NOLOCK) " +
				" inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v WITH(NOLOCK) on v.id=e.venue_id " +
				"inner join grand_child_category gc WITH(NOLOCK) on gc.id = a.grand_child_category_id  " +
				"inner join  child_category cc WITH(NOLOCK) on cc.id=gc.child_category_id inner join parent_category pc WITH(NOLOCK) " +
				"on pc.id = cc.category_id  " +
				"where e.id="+eventId+" ";
		
		
		switch (productType) {
			case MINICATS:
				sql = sql+" and e.minicats_enabled = 1 ";
				break;
				
			case VIPMINICATS:
				sql = sql+" and e.vipminicats_enabled = 1 ";
				break;
				
			case LASTROWMINICATS:
				sql = sql+" and e.lastrow_minicat_enabled = 1 ";
				break;
				
			case VIPLASTROWMINICATS:
				sql = sql+" and e.vip_lastrow_minicats_enabled = 1 ";
				break;
				
			case ZONETICKETS:
				sql = sql+" and e.zone_tickets_enabled = 1 ";
				break;
				
			case PRESALEZONETICKETS:
				sql = sql+" and e.presale_zonetickets_enabled = 1 ";
				break;
	
			default:
				break;
		}
		
		ZoneEvent zoneEvent = null;
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setEventId((Integer)object[0]);
				zoneEvent.setEventName((String)object[1]);
				zoneEvent.setArtistName((String)object[2]);
				zoneEvent.setVenueCountry((String)object[3]);
				zoneEvent.setVenueState((String)object[4]);
				zoneEvent.setVenueCity((String)object[5]);
				zoneEvent.setEventDate((String)object[6]);
				zoneEvent.setEventTime((String)object[7]);
				zoneEvent.setVenueId((Integer)object[8]);
				zoneEvent.setVenueCategoryName((String)object[9]);
				zoneEvent.setVenueZipCode((String)object[10]);
				zoneEvent.setArtistId((Integer)object[11]);
				if(null != object[12]){
					zoneEvent.setVenueName((String)object[12]);
				}
				zoneEvent.setDiscountFlag((Boolean)object[13]);
				zoneEvent.setGrandChildCategoryName((String)object[14]);
				zoneEvent.setChildCategoryName((String)object[15]);
				zoneEvent.setParentCategoryName((String)object[16]);
				
				if(productType.equals(ProductType.PRESALEZONETICKETS)){
					if(null != object[18]){
						zoneEvent.setDiscountPerc(((BigDecimal)object[18]).doubleValue());
					}
				}else{
					if(null != object[17]){
						zoneEvent.setDiscountPerc(((BigDecimal)object[17]).doubleValue());
					}
				}
			}
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return zoneEvent;
	}

@SuppressWarnings("unchecked")
public Collection<TeamCardImageDetails> getCardImage(){
	
	
	String sql ="select distinct a.id as artistId,ai.image_url as artistImage, " +
		"acm.grand_child_category_id as grandChildCategoryId, gci.image_url as grandChildImage, " +
		"acm.child_category_id as childCategoryId , cci.image_url as childImage , " +
		"acm.parent_category_id as parentCategoryId, pci.image_url as parentImage " +
		"from artist a WITH(NOLOCK) inner join artist_category_mapping  acm WITH(NOLOCK) on acm.artist_id=a.id " +
		"left join artist_image ai WITH(NOLOCK) on a.id=ai.artist_id  and ai.status = 'ACTIVE' " +
		"left join grand_child_category_image gci WITH(NOLOCK) on gci.grand_child_category_id=acm.grand_child_category_id " +
		"and gci.status = 'ACTIVE' left join child_category_image cci WITH(NOLOCK) on cci.child_category_id=acm.child_category_id  " +
		"and cci.status = 'ACTIVE' left join parent_category_image pci WITH(NOLOCK) on pci.parent_category_id=acm.parent_category_id " +
		"and pci.status = 'ACTIVE'";

 
		SQLQuery query = null;
		TeamCardImageDetails teamCardImageDetails = null;
		Collection<TeamCardImageDetails> teamCardImageList = new ArrayList<TeamCardImageDetails>();
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query
			.addScalar("artistId", Hibernate.INTEGER)
			.addScalar("artistImage", Hibernate.STRING)
			.addScalar("grandChildCategoryId", Hibernate.INTEGER)
			.addScalar("grandChildImage", Hibernate.STRING)
			.addScalar("childCategoryId", Hibernate.INTEGER)
			.addScalar("childImage", Hibernate.STRING)
			.addScalar("parentCategoryId", Hibernate.INTEGER)
			.addScalar("parentImage", Hibernate.STRING)
			.list();
			for(Object[] object : result){
				teamCardImageDetails = new TeamCardImageDetails();
				teamCardImageDetails.setArtistId((Integer)object[0]);
				teamCardImageDetails.setArtistImage((String)object[1]);
				teamCardImageDetails.setGrandChildCategoryId((Integer)object[2]);
				teamCardImageDetails.setGrandChildImage((String)object[3]);
				teamCardImageDetails.setChildCategoryId((Integer)object[4]);
				teamCardImageDetails.setChildImage((String)object[5]);
				teamCardImageDetails.setParentCategoryId((Integer)object[6]);
				teamCardImageDetails.setParentImage((String)object[7]);
				teamCardImageList.add(teamCardImageDetails);
			}
			//Collection<TeamCardImageDetails> teamCardImageList = query.setResultTransformer(Transformers.aliasToBean(TeamCardImageDetails.class)).list();
			return teamCardImageList;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
			return null;
		}
	}


@SuppressWarnings("unchecked")
public List<TeamCardImageDetails> getCardImage(Integer artistId){
	
	  String sql ="select distinct a.id as artistId,ai.image_url as artistImage, " +
	  		"acm.grand_child_category_id as grandChildCategoryId, gci.image_url as grandChildImage, " +
	  		"acm.child_category_id as childCategoryId , cci.image_url as childImage , " +
	  		"acm.parent_category_id as parentCategoryId, pci.image_url as parentImage " +
	  		"from artist a WITH(NOLOCK) inner join artist_category_mapping  acm WITH(NOLOCK) on acm.artist_id=a.id " +
	  		"left join artist_image ai WITH(NOLOCK) on a.id=ai.artist_id  and ai.status = 'ACTIVE' " +
	  		"left join grand_child_category_image gci WITH(NOLOCK) on gci.grand_child_category_id=acm.grand_child_category_id " +
	  		"and gci.status = 'ACTIVE' left join child_category_image cci WITH(NOLOCK) on cci.child_category_id=acm.child_category_id  " +
	  		"and cci.status = 'ACTIVE' left join parent_category_image pci WITH(NOLOCK) on pci.parent_category_id=acm.parent_category_id " +
	  		"and pci.status = 'ACTIVE' where a.id="+artistId;
 
		SQLQuery query = null;
		TeamCardImageDetails teamCardImageDetails = null;
		List<TeamCardImageDetails> teamCardImageList = new ArrayList<TeamCardImageDetails>();
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query
			.addScalar("artistId", Hibernate.INTEGER)
			.addScalar("artistImage", Hibernate.STRING)
			.addScalar("grandChildCategoryId", Hibernate.INTEGER)
			.addScalar("grandChildImage", Hibernate.STRING)
			.addScalar("childCategoryId", Hibernate.INTEGER)
			.addScalar("childImage", Hibernate.STRING)
			.addScalar("parentCategoryId", Hibernate.INTEGER)
			.addScalar("parentImage", Hibernate.STRING)
			.list();
			for(Object[] object : result){
				teamCardImageDetails = new TeamCardImageDetails();
				teamCardImageDetails.setArtistId((Integer)object[0]);
				teamCardImageDetails.setArtistImage((String)object[1]);
				teamCardImageDetails.setGrandChildCategoryId((Integer)object[2]);
				teamCardImageDetails.setGrandChildImage((String)object[3]);
				teamCardImageDetails.setChildCategoryId((Integer)object[4]);
				teamCardImageDetails.setChildImage((String)object[5]);
				teamCardImageDetails.setParentCategoryId((Integer)object[6]);
				teamCardImageDetails.setParentImage((String)object[7]);
				teamCardImageList.add(teamCardImageDetails);
			}
			//Collection<TeamCardImageDetails> teamCardImageList = query.setResultTransformer(Transformers.aliasToBean(TeamCardImageDetails.class)).list();
			return teamCardImageList;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
			return null;
		}
	}

	public int trackStripeTransaction(String paymentMethod,String description,Double transactionAmount , 
			String stripeToken , String transactionId,String response) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			
			String query = "insert into stripe_transaction_details(payment_method,trx_amount,stripe_trx_id,stripe_token,description,stripe_response,last_updated) " +
					"VALUES('"+paymentMethod+"',"+transactionAmount+",'"+transactionId+"','"+stripeToken+"','"+description+"','"+response+"',GETDATE())";
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}
	
	public int trackPaypalFuturePayment(Integer customerId,Integer ticketGroupId,String description,Double transactionAmount , 
			String authorizationCode ,String correlationId, String transactionId,String response) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			
			String query = "insert into paypal_future_payment_details(customer_id,ticket_group_id,trx_amount,paypal_trx_id,auth_code,correlation_id,description,paypal_response,last_updated) " +
					"VALUES("+customerId+","+ticketGroupId+","+transactionAmount+",'"+transactionId+"','"+authorizationCode+"','"+correlationId+"','"+description+"','"+response+"',GETDATE())";
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}

 /*public  List<ZoneEvent> getZoneEventsBySearchKeyword(String searchText) {
		
		String sql =" select distinct e.id as eventId,REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, " +
				"v.country as venueCountry ,v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				" CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding," +
				" e.event_date,e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id " +
				" left join venue v WITH(NOLOCK) on v.id=e.venue_id " +
				" inner join zone_ticket_group zt WITH(NOLOCK) on zt.event_id=e.id where " +
				" ( CONTAINS( e.name,'\""+searchText+"\"') or CONTAINS( a.name,'\""+searchText+"\"') or " +
				" CONTAINS( v.building,'\""+searchText+"\"') or CONTAINS( v.city,'\""+searchText+"\"') or " +
				" CONTAINS( v.state,'\""+searchText+"\"') or CONTAINS( v.postal_code,'\""+searchText+"\"') ) " +
				" and v.building is not null and zt.zone_price >0 and zt.ticket_status=1 and e.status=1 order by e.event_date,e.event_time  ";
		
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setEventId((Integer)object[0]);
				zoneEvent.setEventName((String)object[1]);
				zoneEvent.setArtistName((String)object[2]);
				zoneEvent.setVenueCountry((String)object[3]);
				zoneEvent.setVenueState((String)object[4]);
				zoneEvent.setVenueCity((String)object[5]);
				zoneEvent.setEventDate((String)object[6]);
				zoneEvent.setEventTime((String)object[7]);
				zoneEvent.setVenueZipCode((String)object[8]);
				if(object[9] == null){
					continue;
				}
				if(object[10] == null){
					continue;
				}
				if(null == object[11]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[9]);
				zoneEvent.setVenueCategoryName((String)object[10]);
				zoneEvent.setVenueName((String)object[11]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 */
 /*public  List<ZoneEvent> getZoneEventsBySearchKeywordByLike(String searchText,Integer pageNumber,Integer maxRows) {
		
		
		String sql ="select count(1) over () totalRows, t.* from (select distinct e.id as eventId," +
				"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.zone_price,e.discount_zone_flag,e.event_date," +
				"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
				"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from zone_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp where " +
				"( e.name like '%"+searchText+"%' or a.name like '%"+searchText+"%' or v.building like '%"+searchText+"%' or " +
				"v.city like '"+searchText+"%' or v.state like '"+searchText+"%' or  v.postal_code like '"+searchText+"%' ) and " +
				"v.building is not null and e.status=1 and e.zone_tickets_enabled is not null and e.zone_tickets_enabled=1 ) t order by t.event_date,t.event_time " +
				"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setTotalEvents((Integer)object[0]);
				zoneEvent.setEventId((Integer)object[1]);
				zoneEvent.setEventName((String)object[2]);
				zoneEvent.setArtistName((String)object[3]);
				zoneEvent.setVenueCountry((String)object[4]);
				zoneEvent.setVenueState((String)object[5]);
				zoneEvent.setVenueCity((String)object[6]);
				zoneEvent.setEventDate((String)object[7]);
				zoneEvent.setEventTime((String)object[8]);
				zoneEvent.setVenueZipCode((String)object[9]);
				if(object[10] == null){
					continue;
				}
				if(object[11] == null){
					continue;
				}
				if(null == object[12]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[10]);
				zoneEvent.setVenueCategoryName((String)object[11]);
				zoneEvent.setVenueName((String)object[12]);
				zoneEvent.setPriceStartFrom((Integer)object[13]);
				zoneEvent.setDiscountFlag((Boolean)object[14]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 
 
 public List<ChildCategory> getGrandChildcategoryByIdandName(){
		List<ChildCategory> childCategory =null;
		String sql = "select distinct e.grand_child_category_id as id, e.grand_child_category_name as name  from event_details e " +
		" inner join grand_child_category_image gc on(gc.grand_child_category_id=e.grand_child_category_id) "+
		"where grand_child_category_name not in ( 'other','default','none') and gc.circle_image_url is not null";
				
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			childCategory =  query.setResultTransformer(Transformers.aliasToBean(ChildCategory.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return childCategory;
	}
 
 
 
 public  List<ZoneEvent> getMinicatsEventsBySearchKeywordByLike(String searchText,Integer pageNumber,Integer maxRows) {
		
		
		String sql ="select count(1) over () totalRows, t.* from (select distinct e.id as eventId," +
				"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.price,e.discount_zone_flag,e.event_date," +
				"e.event_time from minicat_event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
				"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 price  from minicat_category_ticket zt WITH(NOLOCK) " +
				"where  zt.event_id=e.id and zt.status=1 and zt.price >0 order by price ) zp where " +
				"( e.name like '%"+searchText+"%' or a.name like '%"+searchText+"%' or v.building like '%"+searchText+"%' or " +
				"v.city like '"+searchText+"%' or v.state like '"+searchText+"%' or  v.postal_code like '"+searchText+"%' ) and " +
				"v.building is not null and e.status=1 and e.minicats_enabled is not null and e.minicats_enabled=1 ) t order by t.event_date,t.event_time " +
				"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setTotalEvents((Integer)object[0]);
				zoneEvent.setEventId((Integer)object[1]);
				zoneEvent.setEventName((String)object[2]);
				zoneEvent.setArtistName((String)object[3]);
				zoneEvent.setVenueCountry((String)object[4]);
				zoneEvent.setVenueState((String)object[5]);
				zoneEvent.setVenueCity((String)object[6]);
				zoneEvent.setEventDate((String)object[7]);
				zoneEvent.setEventTime((String)object[8]);
				zoneEvent.setVenueZipCode((String)object[9]);
				if(object[10] == null){
					continue;
				}
				if(object[11] == null){
					continue;
				}
				if(null == object[12]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[10]);
				zoneEvent.setVenueCategoryName((String)object[11]);
				zoneEvent.setVenueName((String)object[12]);
				zoneEvent.setPriceStartFrom((Integer)object[13]);
				zoneEvent.setDiscountFlag((Boolean)object[14]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 
 public  List<ZoneEvent> getZoneEventsByZipCodeByLike(List<String> zipcodes,Integer pageNumber,Integer maxRows) {
		
		
		String sql ="select count(1) over () totalRows, t.* from (select distinct e.id as eventId," +
				"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.zone_price,e.discount_zone_flag,e.event_date," +
				"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
				"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from zone_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp where " +
				" v.postal_code in (:zipcodes) and " +
				"v.building is not null and e.status=1 and e.zone_tickets_enabled is not null and e.zone_tickets_enabled=1  ) t order by t.event_date,t.event_time " +
				"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql).setParameterList("zipcodes", zipcodes);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setTotalEvents((Integer)object[0]);
				zoneEvent.setEventId((Integer)object[1]);
				zoneEvent.setEventName((String)object[2]);
				zoneEvent.setArtistName((String)object[3]);
				zoneEvent.setVenueCountry((String)object[4]);
				zoneEvent.setVenueState((String)object[5]);
				zoneEvent.setVenueCity((String)object[6]);
				zoneEvent.setEventDate((String)object[7]);
				zoneEvent.setEventTime((String)object[8]);
				zoneEvent.setVenueZipCode((String)object[9]);
				if(object[10] == null){
					continue;
				}
				if(object[11] == null){
					continue;
				}
				if(null == object[12]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[10]);
				zoneEvent.setVenueCategoryName((String)object[11]);
				zoneEvent.setVenueName((String)object[12]);
				zoneEvent.setPriceStartFrom((Integer)object[13]);
				zoneEvent.setDiscountFlag((Boolean)object[14]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 
 public  List<ZoneEvent> getMinicatsEventsByZipCodeByLike(List<String> zipcodes,Integer pageNumber,Integer maxRows) {
		
		
		String sql ="select count(1) over () totalRows, t.* from (select distinct e.id as eventId," +
				"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.zone_price,e.discount_zone_flag,e.event_date," +
				"e.event_time from minicat_event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
				"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 price  from minicat_category_ticket zt WITH(NOLOCK) " +
				" where  zt.event_id=e.id and zt.status=1 and zt.price >0 order by price ) zp where " +
				" v.postal_code in (:zipcodes) and " +
				"v.building is not null and e.status=1 and e.minicats_enabled is not null and e.minicats_enabled=1 ) t order by t.event_date,t.event_time " +
				"OFFSET ("+pageNumber+"-1)*"+maxRows+" ROWS FETCH NEXT "+maxRows+" ROWS ONLY";
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql).setParameterList("zipcodes", zipcodes);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setTotalEvents((Integer)object[0]);
				zoneEvent.setEventId((Integer)object[1]);
				zoneEvent.setEventName((String)object[2]);
				zoneEvent.setArtistName((String)object[3]);
				zoneEvent.setVenueCountry((String)object[4]);
				zoneEvent.setVenueState((String)object[5]);
				zoneEvent.setVenueCity((String)object[6]);
				zoneEvent.setEventDate((String)object[7]);
				zoneEvent.setEventTime((String)object[8]);
				zoneEvent.setVenueZipCode((String)object[9]);
				if(object[10] == null){
					continue;
				}
				if(object[11] == null){
					continue;
				}
				if(null == object[12]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[10]);
				zoneEvent.setVenueCategoryName((String)object[11]);
				zoneEvent.setVenueName((String)object[12]);
				zoneEvent.setPriceStartFrom((Integer)object[13]);
				zoneEvent.setDiscountFlag((Boolean)object[14]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 
 public  List<ZoneEvent> getZoneEventsByZipCodeWithoutPagination(List<String> zipcodes) {
		
		
		String sql ="select count(*) over () totalRows, t.* from (select distinct e.id as eventId," +
				"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.zone_price,e.discount_zone_flag,e.event_date," +
				"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
				"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from zone_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp where " +
				" v.postal_code in (:zipcodes) and " +
				"v.building is not null and e.status=1 and e.zone_tickets_enabled is not null and e.zone_tickets_enabled=1 ) t order by t.event_date,t.event_time " ;
				
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql).setParameterList("zipcodes", zipcodes);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setTotalEvents((Integer)object[0]);
				zoneEvent.setEventId((Integer)object[1]);
				zoneEvent.setEventName((String)object[2]);
				zoneEvent.setArtistName((String)object[3]);
				zoneEvent.setVenueCountry((String)object[4]);
				zoneEvent.setVenueState((String)object[5]);
				zoneEvent.setVenueCity((String)object[6]);
				zoneEvent.setEventDate((String)object[7]);
				zoneEvent.setEventTime((String)object[8]);
				zoneEvent.setVenueZipCode((String)object[9]);
				if(object[10] == null){
					continue;
				}
				if(object[11] == null){
					continue;
				}
				if(null == object[12]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[10]);
				zoneEvent.setVenueCategoryName((String)object[11]);
				zoneEvent.setVenueName((String)object[12]);
				zoneEvent.setPriceStartFrom((Integer)object[13]);
				zoneEvent.setDiscountFlag((Boolean)object[14]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 
 public  List<FavouriteEvent> getZoneEventsByEventIds(List<Integer> eventIds) {
		
		
		String sql ="select  t.* from (select distinct e.id as eventId," +
		"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
		"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
		"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
		"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
		"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.zone_price,e.discount_zone_flag,e.event_date," +
		"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
		"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from zone_ticket_group zt WITH(NOLOCK) " +
		"where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp where " +
		"( e.id in (:eventIds) ) and " +
		" v.building is not null and e.status=1 and e.zone_tickets_enabled is not null and e.zone_tickets_enabled=1 ) t order by t.event_date,t.event_time ";
		
		List<FavouriteEvent> events =new ArrayList<FavouriteEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql).setParameterList("eventIds", eventIds);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			FavouriteEvent favouriteEvent = null;
			for (Object[] object : result) {
				favouriteEvent= new FavouriteEvent();
				favouriteEvent.setEventId((Integer)object[0]);
				favouriteEvent.setEventName((String)object[1]);
				favouriteEvent.setArtistName((String)object[2]);
				favouriteEvent.setVenueCountry((String)object[3]);
				favouriteEvent.setVenueState((String)object[4]);
				favouriteEvent.setVenueCity((String)object[5]);
				favouriteEvent.setEventDate((String)object[6]);
				favouriteEvent.setEventTime((String)object[7]);
				favouriteEvent.setVenueZipCode((String)object[8]);
				if(object[9] == null){
					continue;
				}
				if(object[10] == null){
					continue;
				}
				if(null == object[11]){
					continue;
				}
				favouriteEvent.setVenueId((Integer)object[9]);
				favouriteEvent.setVenueCategoryName((String)object[10]);
				favouriteEvent.setVenueName((String)object[11]);
				favouriteEvent.setPriceStartFrom((Integer)object[12]);
				favouriteEvent.setDiscountFlag((Boolean)object[13]);
				events.add(favouriteEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 
 public  List<FavouriteEvent> getZoneEventsByEventIdsAndByProduct(List<Integer> eventIds,ProductType productType) {
		
		
		String sql ="select  t.* from (select distinct e.id as eventId," +
		"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
		"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
		"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
		"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
		"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.zone_price,e.discount_zone_flag,e.event_date," +
		"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v ";
		
		switch (productType) {
			case MINICATS:
				sql = sql+" WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 price as zone_price from minicat_category_ticket zt WITH(NOLOCK) " +
				" where zt.event_id=e.id and zt.status=1 and zt.price >0 order by price ) zp  " +
				" where e.minicats_enabled is not null and  e.minicats_enabled = 1 and ";
				break;
				
			case VIPMINICATS:
				sql = sql+" WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 price as zone_price from vip_minicat_category_ticket zt WITH(NOLOCK) " +
				" where zt.event_id=e.id and zt.status=1 and zt.price >0 order by price ) zp  " +
				" where e.vipminicats_enabled is not null and  e.vipminicats_enabled = 1 and ";
				break;
				
			case LASTROWMINICATS:
				sql = sql+" WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 price as zone_price from lastrow_minicat_category_ticket zt WITH(NOLOCK) " +
				" where zt.event_id=e.id and zt.status=1 and zt.price >0 order by price  ) zp  " +
				" where e.lastrow_minicat_enabled is not null and  e.lastrow_minicat_enabled = 1 and ";
				break;
				
			case ZONETICKETS:
				sql = sql+" WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from zone_ticket_group zt WITH(NOLOCK) " +
						" where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp "+
						" where e.zone_tickets_enabled is not null and  e.zone_tickets_enabled = 1 and ";
				break;
				
			case PRESALEZONETICKETS:
				sql = sql+" WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from presale_zone_ticket_group zt WITH(NOLOCK) " +
						" where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp "+
						" where e.presale_zonetickets_enabled is not null and  e.presale_zonetickets_enabled = 1 and ";
				break;
		
			default:
				sql = sql+" WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from zone_ticket_group zt WITH(NOLOCK) " +
				" where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp "+
				" where e.zone_tickets_enabled is not null and  e.zone_tickets_enabled = 1 and ";
				break;
		}
		sql = sql+" ( e.id in (:eventIds) ) and " +
			" v.building is not null and e.status=1  ) t order by t.event_date,t.event_time ";

		
		List<FavouriteEvent> events =new ArrayList<FavouriteEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql).setParameterList("eventIds", eventIds);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			FavouriteEvent favouriteEvent = null;
			for (Object[] object : result) {
				favouriteEvent= new FavouriteEvent();
				favouriteEvent.setEventId((Integer)object[0]);
				favouriteEvent.setEventName((String)object[1]);
				favouriteEvent.setArtistName((String)object[2]);
				favouriteEvent.setVenueCountry((String)object[3]);
				favouriteEvent.setVenueState((String)object[4]);
				favouriteEvent.setVenueCity((String)object[5]);
				favouriteEvent.setEventDate((String)object[6]);
				favouriteEvent.setEventTime((String)object[7]);
				favouriteEvent.setVenueZipCode((String)object[8]);
				if(object[9] == null){
					continue;
				}
				if(object[10] == null){
					continue;
				}
				if(null == object[11]){
					continue;
				}
				favouriteEvent.setVenueId((Integer)object[9]);
				favouriteEvent.setVenueCategoryName((String)object[10]);
				favouriteEvent.setVenueName((String)object[11]);
				favouriteEvent.setPriceStartFrom((Integer)object[12]);
				favouriteEvent.setDiscountFlag((Boolean)object[13]);
				events.add(favouriteEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
 
 
 	
 public List<TicketGroup> getZoneTicketsByEventIdNew(Integer eventId,Map<String, ZoneRGBColor> rgbColorMap,Map<Integer, Boolean> lockedTicketMap,
		 Map<String, ZoneTicketsAdditionalMarkup> ztAdditionalMarkupMap) {
		
		String sql =" select id,event_id,section,quantity,zone_price,priority,section_range,row_range,shipping_method " +
				"from zone_ticket_group WITH(NOLOCK) where event_id="+eventId+" and ticket_status=1 and " +
				"zone_price >0 order by section,quantity,priority";
		
		DecimalFormat df = new DecimalFormat("#.##");

		List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			
			TicketGroup ticketGroup = null;
			ZoneTicketsAdditionalMarkup ztAdditionalMarkup = null;
			
			for (Object[] object : result) {
				ticketGroup= new TicketGroup();
				Integer ticketGroupId = (Integer)object[0];
				
				Boolean isLockedTicket = lockedTicketMap.get(ticketGroupId);				
				if(null != isLockedTicket && isLockedTicket){
					continue;
				}
				
				ticketGroup.setId(ticketGroupId);
				ticketGroup.setEventId((Integer)object[1]);

				String zone = (String)object[2];
				ticketGroup.setZone(zone);
				
				zone = zone.toUpperCase().replace("ZONE", "").replaceAll(" +","");
				
				ZoneRGBColor zoneRGBColor = rgbColorMap.get(zone);
				
				if(null != zoneRGBColor){
					ticketGroup.setColorCode(zoneRGBColor.getColor());
					ticketGroup.setRgbColor(zoneRGBColor.getRgbColor());
				}else{
					ticketGroup.setColorCode("#F4ED6D");
					ticketGroup.setRgbColor("R:244 G:237 B:109");
				}
		
				Integer qty =(Integer)object[3];
				Integer decimal = (Integer)object[4];
				Double price =decimal.doubleValue();
				
				ztAdditionalMarkup = ztAdditionalMarkupMap.get(ticketGroup.getZone().replaceAll("\\s+", " "));
				if(null != ztAdditionalMarkup) {
					price = (double) Math.round((price*(1+(ztAdditionalMarkup.getAdditionalMarkup())/100)));
				}
				
				ticketGroup.setQuantity(qty);
				ticketGroup.setUnitPrice(Double.valueOf(df.format(price)));
				ticketGroup.setTotal(Double.valueOf(df.format(qty * price)));
				ticketGroup.setStatus(Status.ACTIVE);
				ticketGroup.setPriority((Integer)object[5]);
				
				String sectionRange = (String)object[6];
				String rowRange = (String)object[7];	
				
				sectionRange = sectionRange.replaceAll("-", " thru ");
				rowRange = rowRange.replaceAll("-", " thru ");
				ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+sectionRange+" between row(s) "+rowRange+".");

				if(qty == 1){
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Zone "+zone);
				}else{
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be seated together in Zone "+zone);
				}
				
				boolean isGAZone = TicketUtil.isGeneralAdmissionZone(sectionRange);
				if(isGAZone){
					ticketGroup.setZoneDescription("Tickets will be in section(s) "+sectionRange+" between row(s) "+rowRange+".");
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Zone "+zone);
				}
				ticketGroup.setMessage("");
				ticketGroup.setTicketDeliveryType((String)object[8]);
				ticketGroups.add(ticketGroup);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return ticketGroups;
	}
 
 public List<TicketGroup> getZoneTicketByTicketGroupId(Integer ticketGroupId,Map<String, ZoneRGBColor> rgbColorMap,Map<Integer, Boolean> lockedTicketMap) {
		
		String sql =" select id,event_id,section,quantity,zone_price,priority,section_range,row_range,shipping_method " +
				"from zone_ticket_group WITH(NOLOCK) where id="+ticketGroupId+" and ticket_status=1 and " +
				"zone_price >0 ";
		
		DecimalFormat df = new DecimalFormat("#.##");

		List<TicketGroup> ticketGroups = new ArrayList<TicketGroup>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			
			TicketGroup ticketGroup = null;
			
			
			for (Object[] object : result) {
				ticketGroup= new TicketGroup();
				Integer ticketId = (Integer)object[0];
				
				Boolean isLockedTicket = lockedTicketMap.get(ticketId);				
				if(null != isLockedTicket && isLockedTicket){
					continue;
				}
				
				ticketGroup.setId(ticketGroupId);
				ticketGroup.setEventId((Integer)object[1]);

				String zone = (String)object[2];
				ticketGroup.setZone(zone);
				
				zone = zone.toUpperCase().replace("ZONE", "").replaceAll(" +","");
				
				ZoneRGBColor zoneRGBColor = rgbColorMap.get(zone);
				
				if(null != zoneRGBColor){
					ticketGroup.setColorCode(zoneRGBColor.getColor());
					ticketGroup.setRgbColor(zoneRGBColor.getRgbColor());
				}else{
					ticketGroup.setColorCode("#F4ED6D");
					ticketGroup.setRgbColor("R:244 G:237 B:109");
				}
		
				Integer qty =(Integer)object[3];
				Integer decimal = (Integer)object[4];
				Double price =decimal.doubleValue();
				
				ZoneTicketsAdditionalMarkup ztAdditionalMarkup = DAORegistry.getZoneTicketsAdditionalMarkupDAO().getZoneTicketsAdditionalMarkupsByEventIdandZone(ticketGroup.getEventId(),ticketGroup.getZone().replaceAll("\\s+", " "));
				if(null != ztAdditionalMarkup && null != ztAdditionalMarkup.getAdditionalMarkup() && !ztAdditionalMarkup.getAdditionalMarkup().equals(0.0)){
					price = (double) Math.round((price*(1+(ztAdditionalMarkup.getAdditionalMarkup())/100)));
				}
				
				ticketGroup.setQuantity(qty);
				ticketGroup.setUnitPrice(Double.valueOf(df.format(price)));
				ticketGroup.setTotal(Double.valueOf(df.format(qty * price)));
				ticketGroup.setStatus(Status.ACTIVE);
				ticketGroup.setPriority((Integer)object[5]);
				
				String sectionRange = (String)object[6];
				String rowRange = (String)object[7];	
				
				sectionRange = sectionRange.replaceAll("-", " thru ");
				rowRange = rowRange.replaceAll("-", " thru ");
				
				ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+sectionRange+" between row(s) "+rowRange+".");
				if(qty == 1){
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Zone "+zone);
				}else{
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be seated together in Zone "+zone);
				}
				
				boolean isGAZone = TicketUtil.isGeneralAdmissionZone(sectionRange);
				if(isGAZone){
					ticketGroup.setZoneDescription("Tickets will be in section(s) "+sectionRange+" between row(s) "+rowRange+".");
					ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Zone "+zone);
				}
				ticketGroup.setMessage("");
				ticketGroup.setTicketDeliveryType((String)object[8]);
				
				ticketGroups.add(ticketGroup);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return ticketGroups;
	}
 
 
 public  List<ZoneEvent> getAllZoneEventsBySearchKeywordByLike(String searchText) {
		
		
		String sql ="select count(*) over () totalRows, t.* from (select distinct e.id as eventId," +
				"REPLACE(e.name,',','') as eventName,REPLACE(a.name,',','') as artistName, v.country as venueCountry ," +
				"v.state as venueState,v.city as venueCity,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
				"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) " +
				"WHEN e.event_time is null THEN 'TBD' END as eventTime,v.postal_code as postalCode,v.id,e.venue_category_name," +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as venueBuilding, zp.zone_price,e.discount_zone_flag,e.event_date," +
				"e.event_time from event e WITH(NOLOCK) inner join artist a WITH(NOLOCK) on a.id=e.artist_id left join venue v " +
				"WITH(NOLOCK) on v.id=e.venue_id CROSS APPLY ( select top 1 zone_price  from zone_ticket_group zt WITH(NOLOCK) " +
				"where  zt.event_id=e.id and zt.ticket_status=1 and zt.zone_price >0 order by zone_price ) zp where " +
				"( e.name like '%"+searchText+"%' or a.name like '%"+searchText+"%' or v.building like '%"+searchText+"%' or " +
				"v.city like '"+searchText+"%' or v.state like '"+searchText+"%' or  v.postal_code like '"+searchText+"%' ) and " +
				"v.building is not null and e.status=1  and e.zone_tickets_enabled is not null and e.zone_tickets_enabled=1 ) t order by t.event_date,t.event_time ";
		
		
		List<ZoneEvent> events =new ArrayList<ZoneEvent>();
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			
			List<Object[]> result = (List<Object[]>)query.list();
			
			ZoneEvent zoneEvent = null;
			for (Object[] object : result) {
				zoneEvent= new ZoneEvent();
				zoneEvent.setTotalEvents((Integer)object[0]);
				zoneEvent.setEventId((Integer)object[1]);
				zoneEvent.setEventName((String)object[2]);
				zoneEvent.setArtistName((String)object[3]);
				zoneEvent.setVenueCountry((String)object[4]);
				zoneEvent.setVenueState((String)object[5]);
				zoneEvent.setVenueCity((String)object[6]);
				zoneEvent.setEventDate((String)object[7]);
				zoneEvent.setEventTime((String)object[8]);
				zoneEvent.setVenueZipCode((String)object[9]);
				if(object[10] == null){
					continue;
				}
				if(object[11] == null){
					continue;
				}
				if(null == object[12]){
					continue;
				}
				zoneEvent.setVenueId((Integer)object[10]);
				zoneEvent.setVenueCategoryName((String)object[11]);
				zoneEvent.setVenueName((String)object[12]);
				zoneEvent.setPriceStartFrom((Integer)object[13]);
				zoneEvent.setDiscountFlag((Boolean)object[14]);
				events.add(zoneEvent);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
	

 public  List<UserOrder> getAllUserOrdersByCustomerId(Integer customerId) {
	 
	 String sql ="select user_Order_id,CONVERT(VARCHAR(19),order_date,101) as orderDate,order_total,transaction_id,payment_menthod,original_order_total," +
	 		"total_deducted_amount,user_discount_amount,used_loyalty_amount,user_promotinal_offer,CONVERT(VARCHAR(19),delivery_date,101) as deliveryDate," +
	 		"customer_id,event_id,event_name,event_date,event_time,building,city,state,country,postal_code," +
	 		"ticket_GRoup_id,category,price,qty from user_order_vw where customer_id="+customerId+" order by order_date desc";
        
	List<UserOrder> userOrders = new ArrayList<UserOrder>();
	Query query = null;
	Session session = null;
	DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	try {
		session = sessionFactory.openSession();
		query = session.createSQLQuery(sql);
		List<Object[]> result = (List<Object[]>)query.list();
		UserOrder userOrder = null;
		
		for (Object[] object : result) {
			userOrder = new UserOrder();
			userOrder.setId((Integer)object[0]);
			userOrder.setWsOrderDate((String)object[1]);
			userOrder.setOrderDate(formatter.parse(userOrder.getWsOrderDate()));
			userOrder.setOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[2]).doubleValue()));
			userOrder.setTransactionId((String)object[3]);
			userOrder.setPaymentMethod((String)object[4]);
			userOrder.setOriginalOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[5]).doubleValue()));
			userOrder.setTotalDeductedAmount(TicketUtil.getRoundedValue(((BigDecimal)object[6]).doubleValue()));
			userOrder.setDiscountAmount(TicketUtil.getRoundedValue(((BigDecimal)object[7]).doubleValue()));
			userOrder.setLoyaltySpent(TicketUtil.getRoundedValue(((BigDecimal)object[8]).doubleValue()));
			userOrder.setPromotionalOffer(TicketUtil.getRoundedValue(((BigDecimal)object[9]).doubleValue()));
			userOrder.setWsDeliveryDate((String)object[10]);
			userOrder.setDeliveryDate(formatter.parse(userOrder.getWsDeliveryDate()));
			userOrder.setCustomerId((Integer)object[11]);	
			userOrders.add(userOrder);
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	} finally  {
		session.close();
	}
	return userOrders;
 }
 
public  List<UserOrder> getUserOrdersByOrderId(Integer orderId, Integer customerId) {
	 
	 String sql ="select user_Order_id,CONVERT(VARCHAR(19),order_date,101) as orderDate,order_total,transaction_id,payment_menthod,original_order_total," +
	 		"total_deducted_amount,user_discount_amount,used_loyalty_amount,user_promotinal_offer,CONVERT(VARCHAR(19),delivery_date,101) as deliveryDate," +
	 		"customer_id,event_id,event_name,CONVERT(VARCHAR(19),event_date,101) as eventDate," +
	 		"CASE WHEN event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_time, 100), 7)) " +
	 		" WHEN event_time is null THEN 'TBD' END as eventTime,CONVERT(VARCHAR(150),REPLACE(building,',','')) as venueBuilding," +
	 		" city,state,country,postal_code,ticket_GRoup_id,category,price,qty,section_range,row_Range,shipping_method,loyalty_reward_earned, " +
	 		" app_platform,credit_card_type from user_order_vw WITH(NOLOCK) where user_Order_id="+orderId+" and customer_id="+customerId;
        
	List<UserOrder> userOrders = new ArrayList<UserOrder>();
	Query query = null;
	Session session = null;
	DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	try {
		session = sessionFactory.openSession();
		query = session.createSQLQuery(sql);
		List<Object[]> result = (List<Object[]>)query.list();
		UserOrder userOrder = null;
		
		for (Object[] object : result) {
			userOrder = new UserOrder();
			userOrder.setId((Integer)object[0]);
			userOrder.setWsOrderDate((String)object[1]);
			userOrder.setOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[2]).doubleValue()));
			userOrder.setTransactionId((String)object[3]);
			userOrder.setPaymentMethod((String)object[4]);
			userOrder.setOriginalOrderTotal(TicketUtil.getRoundedValue(((BigDecimal)object[5]).doubleValue()));
			userOrder.setTotalDeductedAmount(TicketUtil.getRoundedValue(((BigDecimal)object[6]).doubleValue()));
			userOrder.setDiscountAmount(TicketUtil.getRoundedValue(((BigDecimal)object[7]).doubleValue()));
			userOrder.setLoyaltySpent(TicketUtil.getRoundedValue(((BigDecimal)object[8]).doubleValue()));
			userOrder.setPromotionalOffer(TicketUtil.getRoundedValue(((BigDecimal)object[9]).doubleValue()));
			userOrder.setWsDeliveryDate((String)object[10]);
			userOrder.setCustomerId((Integer)object[11]);	
			
			ZoneEvent zoneEvent = new ZoneEvent();
			zoneEvent.setEventId((Integer)object[12]);
			zoneEvent.setEventName((String)object[13]);
			zoneEvent.setEventDate((String)object[14]);
			zoneEvent.setEventTime((String)object[15]);
			zoneEvent.setVenueName((String)object[16]);
			zoneEvent.setVenueCity((String)object[17]);
			zoneEvent.setVenueState((String)object[18]);
			zoneEvent.setVenueCountry((String)object[19]);
			zoneEvent.setVenueZipCode((String)object[20]);
			
			TicketGroup ticketGroup = new TicketGroup();
			ticketGroup.setEventId((Integer)object[12]);
			ticketGroup.setId(Integer.parseInt((String)object[21]));
			String zone = (String)object[22];
			Integer qty = (Integer)object[24];
			ticketGroup.setZone(zone);
			ticketGroup.setUnitPrice((Double)object[23]);
			ticketGroup.setQuantity(qty);
			ticketGroup.setStatus(Status.SOLD);
			
			String sectionRange = (String)object[25];
			String rowRange = (String)object[26];
			ticketGroup.setTicketDeliveryType((String)object[27]);
			
			sectionRange = sectionRange.replaceAll("-", " thru ");
			rowRange = rowRange.replaceAll("-", " thru ");
			ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+sectionRange+" between row(s) "+rowRange+".");
			
			if(qty == 1){
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Zone "+zone);
			}else{
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be seated together in Zone "+zone);
			}
			
			boolean isGAZone = TicketUtil.isGeneralAdmissionZone(sectionRange);
			if(isGAZone){
				ticketGroup.setZoneDescription("Tickets will be in section(s) "+sectionRange+" between row(s) "+rowRange+".");
				ticketGroup.setTicketDescription(qty+" Ticket(s) will be in Zone "+zone);
			}
			if(null != object[28] ){
				userOrder.setLoyaltyEarned(TicketUtil.getRoundedValue(((BigDecimal)object[28]).doubleValue()));
			}
			userOrder.setAppPlatform((String)object[29]);
			if(null !=object[30]){
				userOrder.setCreditCardType((String)object[30]);
			}
			
			if(userOrder.getPaymentMethod().equals("CREDITCARD")){
				userOrder.setPaymentMethod(userOrder.getPaymentMethod()+" ("+userOrder.getCreditCardType()+")");
			}
			
			if(null == userOrder.getDiscountAmount() || userOrder.getDiscountAmount() == 0){
				userOrder.setDiscountAmount(0.00);
			}
			
			Double unitDiscount = userOrder.getDiscountAmount() / qty;
			ticketGroup.setUnitPrice(ticketGroup.getUnitPrice() - unitDiscount);
			Double totalPrice =ticketGroup.getUnitPrice() * ticketGroup.getQuantity();
			ticketGroup.setTotal(TicketUtil.getRoundedValue(totalPrice));
			
			userOrder.setOrderDate(formatter.parse(userOrder.getWsOrderDate()));
			userOrder.setDeliveryDate(formatter.parse(userOrder.getWsDeliveryDate()));
			userOrder.setZoneEvent(zoneEvent);
			userOrder.setTicketGroup(ticketGroup);
			userOrders.add(userOrder);
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	} finally  {
		session.close();
	}
	return userOrders;
 }


	


	
	
	public StripeCredentials getStripeCredentials() {
		String sql ="select publishable_key,credential_type from stripe_credentials where status='ACTIVE'";
		Query query = null;
		Session session = null;
		StripeCredentials stripeCredentials = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				stripeCredentials = new StripeCredentials();
				stripeCredentials.setPublishableKey((String)object[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return stripeCredentials;
	}
	
	
	
	public List<Object[]> getZipCodes(Connection connection){
		String sql ="select * from ZipCodes";
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Object[] object = new Object[3];
				object[0]= resultSet.getString("postal_code");
				object[1]= resultSet.getString("latitude");
				object[2]= resultSet.getString("langitude");
				result.add(object);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			//connection.close();
		}
		return result;
	}
	
	CREATE TABLE [dbo].[](
			[id] [int] IDENTITY(1,1) NOT NULL,
			[] [varchar](200) NULL,	
			[] [varchar](200) NULL,	
			[] [int]  NULL,
			[] [int]  NULL,
			[] [varchar](200) NULL,
			[create_time] [smalldatetime] NULL,
			[updated_time] [smalldatetime] NULL
		) ON [PRIMARY]

	
	public List<League> getAllSuperFanLeagues() {
		String sql ="select id,name,category_name,grand_child_id,child_id from super_fan_league where status='ACTIVE'";
		Query query = null;
		Session session = null;
		League league = null;
		List<League> leagues = new ArrayList<League>();
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				league = new League();
				league.setId((Integer)object[0]);
				league.setName((String)object[1]);
				league.setCategoryName((String)object[2]);
				leagues.add(league);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return leagues;
	}
	
	
	
	public List<SuperFanTeam> getAllSuperFanTeamsByLeagueId(Integer leagueId) {
		String sql ="select id,league_id,name,artist_id,grand_child_id from super_fan_league_team where status='ACTIVE' and " +
				"league_id ="+leagueId;
		Query query = null;
		Session session = null;
		SuperFanTeam superFanTeam = null;
		List<SuperFanTeam> superFanTeams = new ArrayList<SuperFanTeam>();
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				superFanTeam = new SuperFanTeam();
				superFanTeam.setId((Integer)object[0]);
				superFanTeam.setLeagueId((Integer)object[1]);
				superFanTeam.setName((String)object[2]);
				//superFanTeam.setArtistId((Integer)object[3]);
				//superFanTeam.setGrandChildId((Integer)object[4]);
				superFanTeams.add(superFanTeam);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return superFanTeams;
	}
	
	
	public SuperFanTeam getAllSuperFanTeamsByTeamId(Integer teamId) {
		String sql ="select id,league_id,name,artist_id,grand_child_id from super_fan_league_team where status='ACTIVE' and " +
				"id ="+teamId;
		Query query = null;
		Session session = null;
		SuperFanTeam superFanTeam = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				superFanTeam = new SuperFanTeam();
				superFanTeam.setId((Integer)object[0]);
				superFanTeam.setLeagueId((Integer)object[1]);
				superFanTeam.setName((String)object[2]);
				//superFanTeam.setArtistId((Integer)object[3]);
				//superFanTeam.setGrandChildId((Integer)object[4]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return superFanTeam;
	}
	
	
	public League getSuperFanLeagueById(Integer leagueId) {
		String sql ="select id,name,category_name,grand_child_id,child_id from super_fan_league where status='ACTIVE' and id="+leagueId;
		Query query = null;
		Session session = null;
		League league = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			for (Object[] object : result) {
				league = new League();
				league.setId((Integer)object[0]);
				league.setName((String)object[1]);
				league.setCategoryName((String)object[2]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return league;
	}
	
	*/
	
	
	public List<EventArtist> getAllActiveEventArtistDetails(){
		
		String sql = "select distinct e.event_id as eventId,e.artist_id as artistId,e.event_name as eventName from " +
				"events_rtf e WITH(NOLOCK) where datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event " +
				"and e.status=1 order by e.artist_id asc";
		
		Query query = null;
		List<EventArtist> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(EventArtist.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public List<RTFCategorySearch> getAllRTFSearchCategories(){
		
		String sql = "select id,grand_child_id as grandChildId,child_id as childId,parent_id as parentId," +
				"display_name as displayName,cat_type as catType,parent_category as parentType from rtf_search_by_categories WITH(NOLOCK)";
		
		Query query = null;
		List<RTFCategorySearch> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(RTFCategorySearch.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
public RTFCategorySearch getAllRTFSearchCategoriesById(Integer rtfCatsId){
		
		String sql = "select id,grand_child_id as grandChildId,child_id as childId,parent_id as parentId," +
				"display_name as displayName,cat_type as catType,parent_category as parentType from rtf_search_by_categories WITH(NOLOCK)" +
				" WHERE id="+rtfCatsId;
		
		Query query = null;
		List<RTFCategorySearch> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(RTFCategorySearch.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		
		if(list != null && list.size() > 0) {
			return list.get(0);
		} 
		return null;
	}
	
	public List<GrandChildCategory> getAllRTFSearchCategoriesForGeneralized(){
		
		String sql = "select id,display_name as name from rtf_search_by_categories WITH(NOLOCK) order by display_name";
		
		Query query = null;
		List<GrandChildCategory> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(GrandChildCategory.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public List<RTFCategorySearch> getRTFCategoriesBySearchKey(String searchKey){
		
		String sql = "select id,grand_child_id as grandChildId,child_id as childId,parent_id as parentId," +
				"display_name as displayName,cat_type as catType,parent_category as parentType from rtf_search_by_categories WITH(NOLOCK) " +
				"where display_name like '%"+searchKey+"%' ";
		
		Query query = null;
		List<RTFCategorySearch> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(RTFCategorySearch.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
public RTFCategorySearch getRTFCategoriesByChildAndGrandChild(Integer childId,Integer grandChildId){
		
		String sql = "select id,grand_child_id as grandChildId,child_id as childId,parent_id as parentId," +
				"display_name as displayName,cat_type as catType,parent_category as parentType from rtf_search_by_categories WITH(NOLOCK) " +
				" where child_id="+childId+" and grand_child_id="+grandChildId;
		
		Query query = null;
		List<RTFCategorySearch> list = null;
		RTFCategorySearch rtfCategorySearch = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(RTFCategorySearch.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		if(list != null && list.size() > 0) {
			rtfCategorySearch = list.get(0);
		}
		return rtfCategorySearch;
	}
	
public List<RTFCategorySearch> getRTFCategoriesById(Integer id){
		
		String sql = "select id,grand_child_id as grandChildId,child_id as childId,parent_id as parentId," +
				"display_name as displayName,cat_type as catType,parent_category as parentType from rtf_search_by_categories WITH(NOLOCK) " +
				"where id="+id;
		
		Query query = null;
		List<RTFCategorySearch> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(RTFCategorySearch.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public List<EventArtist> getAllArtistWhichHasEvents(){
			
			String sql = "select id as artistId from artist_view WITH(NOLOCK) where performer_count >= 1";
			
			Query query = null;
			List<EventArtist> list = null;
			try {
				querySession = QueryManagerDAO.getQuerySession();
				if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
					querySession = sessionFactory.openSession();
					QueryManagerDAO.setQuerySession(querySession);
				}
				query = querySession.createSQLQuery(sql);
				list= query.setResultTransformer(Transformers.aliasToBean(EventArtist.class)).list();
				
			} catch (Exception e) {
				if(querySession != null && querySession.isOpen() ){
					querySession.close();
				}
				e.printStackTrace();
			}
			return list;
		}
	
	public List<EventArtist> getAllArtistWhichHasEvents(Integer artistId){
		
		String sql = "select id as artistId from artist_view WITH(NOLOCK) where performer_count >= 1 and id="+artistId;
		
		Query query = null;
		List<EventArtist> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(EventArtist.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<EventArtist> getArtistDetailsByEventId(Integer eventId){
		
		String sql = "select distinct e.event_id as eventId,e.artist_id as artistId,e.event_name as eventName " +
				"from events_rtf e WITH(NOLOCK) where datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event " +
				"and e.status=1 and e.event_id = "+eventId+" order by e.artist_id asc";
		
		Query query = null;
		List<EventArtist> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(EventArtist.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public Integer getParentCategoryIdBySearchKey(String searchKey) {
		String sql ="select distinct acm.parent_category_id from child_category cc WITH(NOLOCK) inner join artist_category_mapping acm WITH(NOLOCK) " +
				"on acm.child_category_id=cc.id where cc.name like '"+searchKey+"'";
		Query query = null;
		Integer parentId = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			
			if(null != result && !result.isEmpty()  ){
				parentId = result.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return parentId;
	}
	
	public Integer getParentCatIdByGrandChildKey(String searchKey) {
		String sql ="select distinct acm.parent_category_id from grand_child_category cc WITH(NOLOCK) inner join artist_category_mapping acm WITH(NOLOCK) " +
				"on acm.grand_child_category_id=cc.id where cc.name like '"+searchKey+"'";
		Query query = null;
		Integer parentId = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			if(null != result && !result.isEmpty()  ){
				parentId = result.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return parentId;
	}
	
	public List<Artist> getArtistByEvent(Integer eventId){
		String sql = "select distinct a.id as id , a.name as name from event_artist ea WITH(NOLOCK) inner join " +
				"artist a WITH(NOLOCK) on a.id=ea.artist_id where a.display_on_search=1 and a.artist_status=1 " +
				"and ea.event_id="+eventId;
		
		Query query = null;
		List<Artist> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(Artist.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public Artist getArtistByEventId(Integer eventId){
		String sql = "select distinct a.id as id , a.name as name from event_artist ea WITH(NOLOCK) inner join " +
				"artist a WITH(NOLOCK) on a.id=ea.artist_id where a.artist_status=1 " +
				"and ea.event_id="+eventId;
		
		Query query = null;
		List<Artist> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(Artist.class)).list();
			
			if(null != list && list.size() > 0){
				return list.get(0);
			}
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	public List<Item> getAlItemsBySearch(String searchKey){
		String sql = "select id as itemId,item_code as itemCode,item_name as itemName,CAST(rate AS DOUBLE PRECISION) as rate,qty as qty from item WITH(NOLOCK) where " +
				" ( item_code like '%"+searchKey+"%' or item_name like '%"+searchKey+"%' )" ;
		
		Query query = null;
		List<Item> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(Item.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<ArtistState> getAllArtistStates(){
		
		String sql = "select id as artistId,name as artistName ,state from artist_venue_view WITH(NOLOCK) where " +
				"state is not null and state not like '-' order by state" ;
		
		Query query = null;
		List<ArtistState> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(ArtistState.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	public List<NearestStates> getAllNearestStates(){
		String sql = "select state,nearest_states as nearestStates from state_information WITH(NOLOCK) order by state" ;
		Query query = null;
		List<NearestStates> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(NearestStates.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<Artist> getAllArtist(Integer excludeDays){
		
		String sql = "select A.*,count(1) over () as totalArtists from (select distinct a.id as id, REPLACE(a.name,',','') as name," +
				"a.event_start_date as fromDate,a.event_end_date as toDate from artist a WITH(NOLOCK) " +
				"inner join event_artist ea WITH(NOLOCK) on ea.artist_id=a.id and ea.tn_status=1 inner join event e WITH(NOLOCK) " +
				"on e.id=ea.event_id and e.status=1 and datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and reward_thefan_enabled = 1 " +//e.event_date > GETDATE()+"+excludeDays+"
				"inner join venue v WITH(NOLOCK) on v.id=e.venue_id inner join venue_category vc WITH(NOLOCK) on " +
				"vc.id=e.venue_category_id and vc.Venue_map=1  where a.artist_status=1 and a.display_on_search=1 " +
				"and a.event_count>0 ) A order by a.name";
		Query query = null;
		List<Artist> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(Artist.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<Venue> getAllVenues(Integer excludeDays){
		
		String sql = "select A.* from (select DISTINCT e.venue_id as id, " +
				"CONVERT(VARCHAR(150),REPLACE(v.building,',','')) as building, v.city  as city,v.state as state," +
				"v.country as country from event e WITH(NOLOCK) inner join venue v WITH(NOLOCK) on v.id=e.venue_id " +
				"inner join venue_category vc on vc.id=e.venue_category_id and vc.Venue_map=1 where e.status=1 and " +
				"datediff(HOUR,GETDATE(),e.event_datetime)>e.exclude_hours_before_event and reward_thefan_enabled = 1 and v.display_on_search=1 ) A order by a.building ";//e.event_date > GETDATE()+"+excludeDays+"
		
		Query query = null;
		List<Venue> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(Venue.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<RealTicketSectionDetails> getRealTicketSectionDetailsByInvoiceId(Integer invoiceId){
		String sql = "select tg.section as section,tg.row as row,min(t.seat_number) As seatLow,max(t.seat_number) AS seatHigh " +
				"from ticket t WITH(NOLOCK) join ticket_group tg WITH(NOLOCK) on t.ticket_group_id=tg.id " +
				"where t.invoice_id="+invoiceId+" group by tg.section,tg.row";
		List<RealTicketSectionDetails> list = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			Query query = querySession.createSQLQuery(sql);
			list= query.setResultTransformer(Transformers.aliasToBean(RealTicketSectionDetails.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return list;
	}
	
	
public List<AffiliateCashRewardHistory> getAllAffiliatePendingCash(){
	
	SimpleDateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		
		try {
			String sql = "select rh.id ,rh.customer_id,rh.customer_order_id,rh.credited_cash," +
					"CONVERT(VARCHAR(19),co.created_date,120) as OrderDate,CONVERT(VARCHAR(19),e.event_date,101) as eventDate," +
					"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN " +
					"e.event_time is null THEN 'TBD' END as eventTime,i.status,rh.user_id,afs.affiliates_status from " +
					"affiliate_cash_reward_history rh WITH(NOLOCK) inner join affiliates_settings afs WITH(NOLOCK) on afs.user_id=rh.user_id inner " +
					"join customer_order co WITH(NOLOCK) on rh.customer_order_id=co.id inner join invoice i WITH(NOLOCK) on i.order_id=co.id and " +
					"i.status !='Voided' left join event e WITH(NOLOCK) on e.id=co.event_id where rh.status='PENDING' and co.order_type " +
					"in ('REGULAR','LOYALFAN') ";
			
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			Query query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			List<AffiliateCashRewardHistory> finalList = new ArrayList<AffiliateCashRewardHistory>();
			AffiliateCashRewardHistory obj =null;
			for (Object[] object : result) {
				try{
					obj = new AffiliateCashRewardHistory();
					obj.setId((Integer)object[0]);
					obj.setCustomerId((Integer)object[1]);
					obj.setOrderId((Integer)object[2]);
					BigDecimal creditedCash = (BigDecimal)object[3];
					obj.setCreditedCash(creditedCash.doubleValue());
					Date orderDate = dbDateTimeFormat.parse((String)object[4]);
					obj.setCreateDate(orderDate);
					obj.setEventDate((String)object[5]);
					String timeStr = DateUtil.formatTime((String)object[6]);
					obj.setEventTime(timeStr);
					obj.setUserId((Integer)object[8]);
					obj.setAffiliatePremiumStatus((String)object[9]);
					finalList.add(obj);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			return finalList;
		}catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}

	public Integer getOrderCount(Integer customerId) {
		String sql ="select count(id) as orderCount from customer_order WITH(NOLOCK) where status not in ('PAYMENT_FAILED','PAYMENT_PENDING') " +
				"and customer_id="+customerId;
		Query query = null;
		Integer orderCount = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			if(null != result && !result.isEmpty()  ){
				orderCount = result.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return orderCount;
	}
	
	public Integer getCompletedOrderByCustomerAndPromoCode(Integer customerId,String promotionalCode) {
		String sql ="select count(id) as orderCount from rtf_promotional_order_tracking_new WITH(NOLOCK) where " +
				"status='COMPLETED' and promo_code like '"+promotionalCode+"' and customer_id="+customerId;
		Query query = null;
		Integer orderCount = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			if(null != result && !result.isEmpty()  ){
				orderCount = result.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return orderCount;
	}
	
	public Integer getCompletedOrderByCustomerId(Integer customerId) {
		String sql ="select count(id) as orderCount from customer_order WITH(NOLOCK) where " +
				"status='ACTIVE' and customer_id="+customerId;
		Query query = null;
		Integer orderCount = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			if(null != result && !result.isEmpty()  ){
				orderCount = result.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return orderCount;
	}
	
	
	public List<Integer> getAllCustomerIds() {
		String sql ="select distinct id from customer c where c.is_bounce <> 1 and c.is_unsub <> 1 " +
				"and c.is_test_account <> 1 and c.product_type='REWARDTHEFAN' and c.id not in " +
				"(select customer_id from rtf_customer_promotional_offers WITH(NOLOCK)) and c.id not in " +
				"(select distinct customer_id from customer_order WITH(NOLOCK) where status not in " +
				"('PAYMENT_FAILED','PAYMENT_PENDING'))";
		
		
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	}
	
	public Boolean isSportsTeam(Integer teamId) {
		String sql ="select distinct parent_category_id from artist_category_mapping " +
				"where parent_category_id = 1 and artist_id="+teamId;
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			if(null != result && !result.isEmpty()  ){
				return true;
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return false;
	}
	public String getLoyalFanImageNameByParentCategoryId(Integer parentCategoryId) {
		String sql ="select image_url from loyalfan_parent_category_image where parent_category_id="+parentCategoryId;
		Query query = null;
		String imageName = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<String> result = query.list();
			if(null != result && !result.isEmpty()  ){
				imageName = result.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return imageName;
	}
	public Map<Integer,String> getLoyalFanImageNameForParentCategories() {
		String sql ="select parent_category_id,image_url from loyalfan_parent_category_image ";
		Query query = null;
		String imageName = null;
		Map<Integer,String> imageMap = new HashMap<Integer,String>();
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Object[]> result = (List<Object[]>)query.list();
			AffiliateCashRewardHistory obj =null;
			for (Object[] object : result) {
				try{
					imageMap.put(((Integer)object[0]),(String)object[1]);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return imageMap;
	}
	
	public List<Integer> getAllCustomerIdsToSendNotification(Integer eventDateInterval,Integer notificationInterval) {
		
		
		String sql =" select distinct co.id as customer_order_id from customer_order co" +
				" inner join invoice i on i.order_id=co.id" +
				" where co.product_type='REWARDTHEFAN' and i.status='Outstanding'" +
				" and (i.is_real_tix_upload is null or i.is_real_tix_upload='NO')" +
				" and (i.tracking_no is null or i.tracking_no ='')" +
				" and event_date>getdate()+"+eventDateInterval+"" +
				" and (co.last_notified is null or co.last_notified+"+notificationInterval+"<getdate())" +
				" and co.created_date<=convert(date,getdate())";
		
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	}
	
	
	public List<PreOrderPageAudit> getAllCustomerEventVisitByDate(String fromdate, String toDate){
		
			try {
				String sql = "select customer_id,event_id from rtf_pre_order_page_audit where " +
						"created_time between '"+fromdate+"' and '"+toDate+"' and  status='LOCKED' group by customer_id,event_id ";
				
				querySession = QueryManagerDAO.getQuerySession();
				if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
					querySession = sessionFactory.openSession();
					QueryManagerDAO.setQuerySession(querySession);
				}
				Query query = querySession.createSQLQuery(sql);
				List<Object[]> result = (List<Object[]>)query.list();
				List<PreOrderPageAudit> finalList = new ArrayList<PreOrderPageAudit>();
				PreOrderPageAudit obj =null;
				for (Object[] object : result) {
					try{
						obj = new PreOrderPageAudit();
						obj.setCustomerId((Integer)object[0]);
						obj.setEventId((Integer)object[1]);
						finalList.add(obj);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				return finalList;
			}catch (Exception e) {
				if(querySession != null && querySession.isOpen() ){
					querySession.close();
				}
				e.printStackTrace();
			}
			return null;
		}
	public Integer getAllPendingFriendsRequestCountByCustomerId(Integer customerId) throws Exception {
		String sql = "select count(*) as friendRequestCount from customer_friends with(nolock) where status='Requested' and friend_customer_id="+customerId;
		Query query = null;
		//QuizAnswerCountDetails quizAnswerDetails = new QuizAnswerCountDetails();
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
		//return null;
	}
	public String getcustomerFrientStatus(Integer customerId,Integer friendCustoemrId) {
		String sql ="select status from customer_friends with(nolock) where (customer_id="+customerId+" and friend_customer_id="+friendCustoemrId+")" +
				" or (customer_id="+friendCustoemrId+" and friend_customer_id="+customerId+")" ;
		Query query = null;
		Broker broker = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<String> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	}
	public List<CustomerFriendDetails> getCustomerFriends(Integer customerId) {
		
		/*String queryStr = "select c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +//cust_name as customerName,last_name as lastName
		" cf.status as status " +
		" from customer_friends cf" +
		" inner join customer c on c.id=cf.friend_customer_id" +
		" where cf.customer_id = "+customerId+" and cf.status in ('REQUESTED','FRIEND')"+
		" order by c.user_id";*/

		
		String queryStr = "select c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" cf.status as status" +
				" from customer_friends cf" +
				" inner join customer c on c.id=cf.friend_customer_id" +
				" where cf.customer_id = "+customerId+" and cf.status in ('REQUESTED','FRIEND')" +
				//"  --order by c.user_id " +
				" union all" +
				" select c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath," +
				" 'Accept' as status" +
				" from customer_friends cf" +
				" inner join customer c on c.id=cf.customer_id " +
				" where cf.friend_customer_id = "+customerId+" and cf.status in ('REQUESTED')" +
				" order by userId";

		SQLQuery query = null;
		List<CustomerFriendDetails> customerFriends = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			query.addScalar("status",Hibernate.STRING);
			
			customerFriends = query.setResultTransformer(Transformers.aliasToBean(CustomerFriendDetails.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return customerFriends;
	}
	public List<CustomerSearchDetails> getDisCoverPageCustomersByPhoneSearch(Integer customerId,String phoneNoStr) {
		String queryStr = "select c.id as customerId,c.user_id as userId,c.cust_image_path as custImagePath,phone as phone," +
				//" case when cf.status is not null then cf.status else 'Add Friend' end as fStatus" +
				" 'Add Friend' as fStatus" +
				" from customer c" +
				" where c.user_id is not null and c.phone in ("+phoneNoStr+")" +
				" and c.id not in ( select cf.friend_customer_id from customer_friends cf" +
				" where cf.status in ('Friend','requested') and cf.customer_id="+customerId +
				" union" +
				" select cf.customer_id from customer_friends cf" +
				" where cf.status in ('Friend','requested') and friend_customer_id="+customerId+" )" +
				" and c.id!=" +customerId +
				" order by c.user_id";
				//" and c.id not in (select friend_customer_id from customer_friends where customer_id= and status='FRIEND')";

		SQLQuery query = null;
		List<CustomerSearchDetails> customerSearchResult = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("custImagePath",Hibernate.STRING);
			query.addScalar("phone",Hibernate.STRING);
			query.addScalar("fStatus",Hibernate.STRING);
			
			customerSearchResult = query.setResultTransformer(Transformers.aliasToBean(CustomerSearchDetails.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return customerSearchResult;
	}
	
	public List<CustomerOrder> getAllNotDownloadedInvoices(){
		String queryStr = "select distinct i.order_id as id from invoice i join invoice_ticket_attachment ita on i.id=ita.invoice_id left join customer_ticket_downloads ctd on i.id=ctd.invoice_id"+
						  " where (ita.is_sent=0 or ita.is_sent is null) and i.shipping_method_id not in(3,13) and ctd.id is null and ita.file_type=0";
		SQLQuery query = null;
		List<CustomerOrder> orders = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("id",Hibernate.INTEGER);
			orders = query.setResultTransformer(Transformers.aliasToBean(CustomerOrder.class)).list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return orders;
	}
	
	
	public List<Object[]> getAllNewEmailIds(){
		String sql ="select email_address,first_name,last_name from all_email_final_list where email_address not " +
				"in (select email from customer )";
		Query query = null;
		List<Object[]> result = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			result = (List<Object[]>)query.list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return result;
	}
	
	
	public Double getContestRewardDollarsByCustomerId(Integer customerId) {
		String sql ="select CASE WHEN a.totalReward is not null THEN a.totalReward  WHEN a.totalReward is null THEN 0.00 END as reward " +
				"from (select sum(reward_points) as totalReward from cust_loyalty_reward_info_tracking WITH(NOLOCK) where customer_id="+customerId+" " +
				"and reward_type='CONTEST') a";
		Query query = null;
		Double totalRewardDollars = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<BigDecimal> result = query.list();
			if(null != result && !result.isEmpty()  ){
				totalRewardDollars = result.get(0).doubleValue();
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return totalRewardDollars;
	}
	
	public List<Object[]> getAllTriviaEmailIds(String start,String end, String tableName){
		
		String sql ="select id,CONVERT(VARCHAR(150),email) as email,is_sent as isSent from "+tableName+" where email is not null and " +
				"email != '' and is_sent='NOTSEND'";
		
		if(start != null && !start.isEmpty()){
			Integer from = Integer.parseInt(start.trim());
			Integer to = Integer.parseInt(end.trim());
			
			sql ="select id,CONVERT(VARCHAR(150),email) as email,is_sent as isSent from "+tableName+" where email is not null and " +
					"email != '' and  is_sent='NOTSEND' " +
					" and id >= "+from+" and id < "+to;
		}
		Query query = null;
		List<Object[]> result = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			result = (List<Object[]>)query.list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return result;
	}
	
	public List<CustomerNotificationDetails> getAllOtpVerifiedCustomersNotificationDeviceDetails() {
		
		String queryStr = "select c.id as customerId,c.user_id as userId,email as email,cdt.application_platform as applicationPlatForm," +
				" cdt.device_id as deviceId,cdt.notification_reg_id as notificationRegId " +
				" from cust_app_device_details cdt" +
				" inner join customer c on c.id=cdt.cust_id" +
				" where c.product_Type='REWARDTHEFAN' and cdt.status='ACTIVE'" +//and c.is_otp_verified=1
				" and cdt.notification_reg_id!='' and cdt.notification_reg_id!='1'"+
			    " and cdt.notification_reg_id not in(select id from onesignal_notification)"+
				//+ " and c.is_test_account=1 and c.id in (415440,414561,414557,414556,414554,414553,348446,348446,348074,347527,342693,341746,341744,339906,338217,337582,336772,335427,335426,334901,334843,334842,334842,334842,334438,334437,334424,334408,2)" +
				//" and c.id in (414557,414554,415440,414556,415440,414561,2) " +
				" order by c.id";

		SQLQuery query = null;
		List<CustomerNotificationDetails> customerNotificationDetails = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("customerId",Hibernate.INTEGER);
			query.addScalar("userId",Hibernate.STRING);
			query.addScalar("email",Hibernate.STRING);
			query.addScalar("applicationPlatForm",Hibernate.STRING);
			query.addScalar("deviceId",Hibernate.STRING);
			query.addScalar("notificationRegId",Hibernate.STRING);
			
			customerNotificationDetails = query.setResultTransformer(Transformers.aliasToBean(CustomerNotificationDetails.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return customerNotificationDetails;
	}
	
	public List<String> getAllZonesByVenueCategoryId(String venueCategory) {

		String sql ="select distinct symbol from category_new where group_name='"+venueCategory+"'";
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<String> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	
	}
	public List<Integer> getAllContestVisitorsForCacheRefresh(Integer contestId) {

		String sql ="select distinct customer_id from web_service_tracking_new where api_name='quizjoincontest' and contest_id='"+contestId+"' " +
				" and customer_id is not null";
		//--and action_result='success'
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	
	}
	public List<Integer> getAllContestCustomerCountDetailsForCacheRefresh(Integer contestId) {

		String sql ="select jc.customer_id from (select max(hit_date) as hit_date,customer_id from web_service_tracking_new where api_name='quizjoincontest' and contest_id='"+contestId+"'" +
				" and customer_id is not null group by customer_id) as jc" +
				" left join (select max(hit_date) as hit_date,customer_id from web_service_tracking_new where api_name='QUIZEXITCONTEST' and contest_id='"+contestId+"'" +
				" group by customer_id) as ec on ec.customer_id=jc.customer_id" +
				" where ec.hit_date is null or jc.hit_date>ec.hit_date";
		//--and action_result='success'
		
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	
	}
	
public List<Integer> getAllBotsCustomerIdsOld() {
		
		String sql ="select distinct c.id as customerId  from customer c with(nolock) "
				+ "inner join "+quizApiLinkedServer+".customer_referral_tracking rt with(nolock)  on rt.customer_id = c.id  and c.is_test_account=0 "
				+ "where email not like '%rtw.com%' and email not like '%rightthisway.com%' and email not like '%amit%raut%' and "
				+ "signup_date between CONVERT(DATETIME,'2018-08-11 00:01:00') and  CONVERT(DATETIME,getdate()) and c.product_type='REWARDTHEFAN'  "
				+ "and device_id in (select device_id from customer c with(nolock) inner join "+quizApiLinkedServer+".customer_referral_tracking rt with(nolock) "
				+ "on rt.customer_id = c.id and c.is_test_account=0  where product_type='REWARDTHEFAN' and device_id <> '' and device_id is not null "
				+ "group by device_id having count(1) > 3 )  "; 
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	}


public List<Integer> getAllBotsCustomerIds() {
	
	String sql = "select distinct custId from (select distinct CustomerId as custId from "+quizApiLinkedServer+".bots_customer with(nolock)"
			+ " union all "
			+ "select distinct customer_id as custId from (select customer_id, count(id) as winCount "
			+ "from "+quizApiLinkedServer+".contest_grand_winners with(nolock) group by customer_id) w where w.winCount >=3 ) a ";
	Query query = null;
	try {
		querySession = QueryManagerDAO.getQuerySession();
		if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
			querySession = sessionFactory.openSession();
			QueryManagerDAO.setQuerySession(querySession);
		}
		query = querySession.createSQLQuery(sql);
		List<Integer> result = query.list();
		return result;
	} catch (Exception e) {
		if(querySession != null && querySession.isOpen() ){
			querySession.close();
		}
		e.printStackTrace();
	} 
	return null;
}


	public List<Object[]> getAllBotsInformation(){
		
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String startDate= "2018-08-11 00:01:00", endDate = dateFormat.format(new Date());
		
		String sql ="select distinct c.id as CustomerId ,CASE WHEN email like '%@%' THEN CONVERT(VARCHAR(150),email) "
				+ "WHEN email not like '%@%' THEN CONVERT(VARCHAR(150),concat(email,'@botsrtf.com')) END as Email ,CONVERT(VARCHAR(50),user_id) as UserId,"
				+ "CONVERT(VARCHAR(15),phone) as Phone,"
				+ "CONVERT(VARCHAR(20), signup_date, 100) as SignUpDate, is_otp_verified as OTPVerified,CONVERT(VARCHAR(50),application_platform) as Platform, "
				+ "CONVERT(VARCHAR(1000),device_id) as device_id,"
				+ "CONVERT(VARCHAR(150),rt.referral_code) as ReferralCode,CASE WHEN d.device_id_str is not null THEN CONVERT(VARCHAR(2000),d.device_id_str) "
				+ "WHEN d.device_id_str is null THEN '' END as AllDevices, CASE WHEN e.ipAddress  is not null THEN "
				+ "CONVERT(VARCHAR(1000),e.ipAddress) WHEN e.ipAddress  is null THEN '' END as IPAddress from customer c with(nolock) inner join "
				+ ""+quizApiLinkedServer+".customer_referral_tracking rt with(nolock)  on rt.customer_id = c.id  and c.is_test_account=0 "
				+ "left join (SELECT c.id as CustID , device_id_str=STUFF ( (SELECT DISTINCT ': '+ CAST(ca.session_id AS VARCHAR(MAX))  "
				+ "FROM web_service_tracking_new ca with(nolock) WHERE ca.customer_id=c.id and ca.api_name = 'QUIZVALIDATEANSWERS' FOR XMl PATH('')  ),1,1,'') "
				+ "FROM customer c with(nolock) where c.product_type='REWARDTHEFAN' GROUP BY c.id) d on  d.CustID=c.id left join "
				+ "(SELECT c.id as CustID , ipAddress=STUFF (  (SELECT DISTINCT ': '+ CAST(ca.customer_ip_address AS VARCHAR(MAX)) FROM "
				+ "web_service_tracking_new ca with(nolock) WHERE ca.customer_id=c.id FOR XMl PATH('')  ),1,1,'') FROM customer c with(nolock) "
				+ "where c.product_type='REWARDTHEFAN' GROUP BY c.id) e on  e.CustID=c.id where email not like '%rtw.com%' "
				+ "and email not like '%rightthisway.com%' and email not like '%amit%raut%' and signup_date between "
				+ "CONVERT(DATETIME,'"+startDate+"') and  CONVERT(DATETIME,'"+endDate+"') and c.product_type='REWARDTHEFAN' "
				+ "and device_id in (select device_id from customer c with(nolock) inner join "+quizApiLinkedServer+".customer_referral_tracking rt with(nolock)  "
				+ "on rt.customer_id = c.id and c.is_test_account=0 where product_type='REWARDTHEFAN' and device_id <> '' and device_id is not null group by "
				+ "device_id having count(1) > 3 ) order by device_id asc";
		Query query = null;
		List<Object[]> result = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			result = (List<Object[]>)query.list();
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return result;
	}
	
	
	public static void main(String[] args) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String startDate= "2018-08-11 00:01:00", endDate = dateFormat.format(new Date());
		
		System.out.println(endDate);
	}
	
public List<Integer> getmyCustList() {
		
		String sql ="select cust_id from cust_loyalty_reward_info where total_referral_points > 50  order by total_referral_points desc;"; 
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return null;
	}

	public Integer updateLastAPIRunTime() throws Exception {
		String sql = "update rtf_api_running_status set last_check=getdate() where id=1";
		Query query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			Integer count = query.executeUpdate();
			session.close();
			return count;
			
		} catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
			throw e;
		}
	}



	public Integer deleteOneSignalNotifications() throws Exception {
		String sql = "DELETE FROM onesignal_notification";
		Query query = null;
		Session session = null;
		try {
			session = getSessionFactory().openSession();
			query = session.createSQLQuery(sql);
			Integer count = query.executeUpdate();
			session.close();
			return count;
		} catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
			throw e;
		}
	}
	
	
	public Integer insertOneSignalNotifications(String notifictionId) {
		Query query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery("INSERT INTO onesignal_notification values ('"+notifictionId.trim()+"')");
			Integer count = query.executeUpdate();
			return count;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen()){
				querySession.close();
			}
			//e.printStackTrace();
		}
		return 0;
	}
	
	public List<String> getAllOneSignalNotifications() {
		Query query = null;
		List<String> notifictions = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery("SELECT id FROM onesignal_notification");
			notifictions = query.list();
			return notifictions;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen()){
				querySession.close();
			}
			//e.printStackTrace();
		}
		return notifictions;
	}
	
	
	public List<Integer> getQuizParticipantsByContestId(Integer contestId){
		String sql ="select distinct cust_id from web_service_tracking_cassandra with(nolock) where api_name in ('JOINCONTEST','EXITCONTEST') "
				+ "and contest_id = "+contestId;
		SQLQuery query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			return list;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	public List<RtfGiftCard> getAvailableGiftCards() {
		String queryStr = "select gcv.id as id, gc.title as title,gc.description as description,gc.conversion_type as conversionType,"
				+ "gc.image_path as imagePath,gcv.card_amount as amount,gcv.remaining_quantity as availableQty,gc.conversion_rate as rewardDollarConvRate,"
				+ "gcv.max_qty_threshold as maxThresholdQty "+
				" from gift_cards gc with(nolock) join gift_card_value gcv with(nolock) on gc.id=gcv.card_id where gc.status=1 and gc.card_status='ACTIVE' and gc.card_type='CUSTOMER'  "
				+ "and gcv.status=1 and gcv.remaining_quantity > 0";
			 
		SQLQuery query = null;
		List<RtfGiftCard> cards = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("title",Hibernate.STRING);
			query.addScalar("description",Hibernate.STRING);
			query.addScalar("imagePath",Hibernate.STRING);
			query.addScalar("amount",Hibernate.DOUBLE);
			query.addScalar("availableQty",Hibernate.INTEGER);
			query.addScalar("conversionType",Hibernate.STRING);
			query.addScalar("rewardDollarConvRate",Hibernate.DOUBLE);
			query.addScalar("maxThresholdQty",Hibernate.INTEGER);
			
			cards = query.setResultTransformer(Transformers.aliasToBean(RtfGiftCard.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return cards;
	}
	
	public List<RtfGiftCard> getAvailableGiftCardsByBrand(Integer brandId) {
		String queryStr = "select gcv.id as id, gc.gift_card_brand_id as gfcBrandId, gc.title as title,gc.description as description,gc.conversion_type as conversionType,"
				+ "gc.image_path as imagePath,gcv.card_amount as amount,gcv.remaining_quantity as availableQty,gc.conversion_rate as rewardDollarConvRate,"
				+ "gc.reward_points_conv_rate as rewardPointConvRate, "
				+ "gcv.max_qty_threshold as maxThresholdQty, gcv.reqd_loyalpnts as reqLPnts,gcv.price_with_loyalty_points as pricewLPnts"
				+ " from gift_cards gc with(nolock) join gift_card_value gcv with(nolock) on "
				+ "gc.id=gcv.card_id where gc.status=1 and gc.card_status='ACTIVE' and gc.card_type='CUSTOMER' and gcv.status=1 and "
				+ "gcv.remaining_quantity > 0 and gc.gift_card_brand_id = "+brandId;
			 
		SQLQuery query = null;
		List<RtfGiftCard> cards = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("gfcBrandId",Hibernate.INTEGER);
			query.addScalar("title",Hibernate.STRING);
			query.addScalar("description",Hibernate.STRING);
			query.addScalar("imagePath",Hibernate.STRING);
			query.addScalar("amount",Hibernate.DOUBLE);
			query.addScalar("availableQty",Hibernate.INTEGER);
			query.addScalar("conversionType",Hibernate.STRING);
			query.addScalar("rewardDollarConvRate",Hibernate.DOUBLE);
			query.addScalar("rewardPointConvRate",Hibernate.DOUBLE);
			query.addScalar("maxThresholdQty",Hibernate.INTEGER);
			query.addScalar("pricewLPnts",Hibernate.DOUBLE);
			query.addScalar("reqLPnts",Hibernate.INTEGER);
			cards = query.setResultTransformer(Transformers.aliasToBean(RtfGiftCard.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return cards;
	}
	
	public Integer getTotalPurchasedQtyByCustomerAndGiftCard(Integer customerId,Integer giftCardId){
		String sql ="select sum(quantity) as totalQtyPurchased from giftcard_order with(nolock) where customer_id="+customerId+" and card_id = "+giftCardId;
		SQLQuery query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(null != list && !list.isEmpty()) {
				return list.get(0);	
			}
			return 0;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return 0;
	}
	
	public Date getRecentOrderDate(Integer customerId){
		//String sql ="select top 1 created_date from giftcard_order with(nolock) order by created_date desc";
		String sql ="select top 1 created_date from giftcard_order with(nolock) where customer_id="+customerId+" order by created_date desc";
		SQLQuery query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Date> list = query.list();
			if(null != list && !list.isEmpty()) {
				return list.get(0);	
			}
			return null;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	
	public Boolean isCustomerCreatedSingleOrder(Integer customerId, String fromDate, String endDate){
		String sql ="select id from giftcard_order with(nolock) where customer_id="+customerId+" and created_date between '"+fromDate+"' and '"+endDate+"'";
		SQLQuery query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(null != list && !list.isEmpty()) {
				return true;	
			}
			return false;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return false;
	}
	
	
	public Boolean isCustomerCreatedTwoOrder(Integer customerId){
		String sql ="select id from giftcard_order with(nolock) where status<> 'VOIDED' AND customer_id="+customerId;
		SQLQuery query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(null == list || list.isEmpty()) {
				return false;	
			}
			//08/30/2019 Tamil : amit asked to change it from 2 to 10.
			if(list.size() >= 10){
				return true;
			}
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return false;
	}
	
	 

	public List<Integer> getAllTestAccounts(){
		String sql ="select id from customer with(nolock) where is_test_account =1";
		SQLQuery query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			return list;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return null;
	}
	
	public int saveProcessedCustomers(Integer custId) throws Exception {
		Session session = null;
		int count = 0;		
		try {
			String query = "insert into aws_s3_customer_ids(customer_id,last_updated) VALUES("+custId+",GETDATE())";
			session = getSessionFactory().openSession();
			Query sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}
	
	public Boolean isCustomerDpMovedToAWS(Integer customerId){
		String sql ="select id from aws_s3_customer_ids with(nolock) where customer_id="+customerId;
		SQLQuery query = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(sql);
			List<Integer> list = query.list();
			if(null == list || list.isEmpty()) {
				return false;	
			}
			return true;
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	public List<CountryPhoneCode> getAllActivePhoneCodes() {
		String queryStr = "select ccID as id, country as countryName,ccode as countryCode, pcode as phoneCode,ccstatus as status "+
									"from rtf_country_code WHERE ccstatus=1";
			 
		SQLQuery query = null;
		List<CountryPhoneCode> codes = null;
		try {
			querySession = QueryManagerDAO.getQuerySession();
			if(querySession==null || !querySession.isOpen() || !querySession.isConnected()){
				querySession = sessionFactory.openSession();
				QueryManagerDAO.setQuerySession(querySession);
			}
			query = querySession.createSQLQuery(queryStr);
			query.addScalar("id",Hibernate.INTEGER);
			query.addScalar("countryName",Hibernate.STRING);
			query.addScalar("countryCode",Hibernate.STRING);
			query.addScalar("phoneCode",Hibernate.STRING);
			query.addScalar("status",Hibernate.BOOLEAN);
			codes = query.setResultTransformer(Transformers.aliasToBean(CountryPhoneCode.class)).list();
			
		} catch (Exception e) {
			if(querySession != null && querySession.isOpen() ){
				querySession.close();
			}
			e.printStackTrace();
		} 
		return codes;
	}
}
