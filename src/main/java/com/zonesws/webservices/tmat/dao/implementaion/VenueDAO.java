package com.zonesws.webservices.tmat.dao.implementaion;

import java.util.ArrayList;
import java.util.List;

import com.zonesws.webservices.dao.implementaion.HibernateDAO;
import com.zonesws.webservices.data.Venue;

public class VenueDAO extends HibernateDAO<Integer, Venue> implements com.zonesws.webservices.tmat.dao.services.VenueDAO {

	public List<Venue> getAllVenues(Integer id, String name,String city,String state) {
		String whereClause="WHERE 1 =1 AND ";
		List<Object> paramList = new ArrayList<Object>();
		boolean flag=false;
//		paramList.add(1);
		if(id !=null ){
			flag=true;
			whereClause += "id = ? AND ";
			paramList.add(id);
		}
		if(name !=null && !name.isEmpty()){
			flag=true;
			whereClause += "building like ? AND ";
			paramList.add("%"+name+"%");
		}
		if(city !=null && !city.isEmpty()){
			flag=true;
			whereClause += "city like ? AND ";
			paramList.add("%"+name+"%");
		}
		if(state !=null && !state.isEmpty()){
			flag=true;
			whereClause += "state = ? AND ";
			paramList.add(state);
		}
		
		if(flag){
			whereClause = whereClause.substring(0, whereClause.length()-4);
			return find("FROM Venue " + whereClause ,paramList.toArray());
		}
		return null;
	}
	
}
