package com.zonesws.webservices.tmat.dao.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.zonesws.webservices.dao.services.RootDAO;
import com.zonesws.webservices.data.Artist;

public interface ArtistDAO extends RootDAO<Integer, Artist> {

	public List<Artist> getArtistListByAliasName(String aliasName);
	public Collection<Artist> getArtistsByName(String name);
	public List<Artist> getArtistByChildCategoryId(Integer childCategoryId);
	public List<Artist> getArtistByGrandChildCategoryId(Integer grandChildCategoryId);
	public Artist getArtistById(Integer id);
	public Map<Integer, Artist> getArtistsByArtistIds(List<Integer> tIds);
	
}
