
package com.zonesws.webservices.tmat.dao.services;
import java.util.List;

import com.zonesws.webservices.dao.services.RootDAO;
import com.zonesws.webservices.data.Venue;

public interface VenueDAO extends RootDAO<Integer, Venue>{
	List<Venue> getAllVenues(Integer venueId,String name,String city,String state);
	
}