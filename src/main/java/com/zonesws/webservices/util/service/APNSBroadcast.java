package com.zonesws.webservices.util.service;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Property;


public class APNSBroadcast {
	 

	/*// The SENDER_ID here is the "Browser Key" that was generated when I
	// created the API keys for my Google APIs project.
	private static final String SENDER_ID = "AIzaSyC--Z_BNbEMz4J_iyYwR_xCa9hoakLMVpQ";
	
	// This is a *cheat*  It is a hard-coded registration ID from an Android device
	// that registered itself with GCM using the same project id shown above.
	private static final String ANDROID_DEVICE = "9ae86cb498b505a8";
	
	private static final String GCM_TOKEN = "APA91bEqbvJJrsWacrdcnpUaS2pyT648K7B-mU9RYLeNM9qXdFsHp85CR8YMWWJRK6qliCeR1q-KpMN82V35usp2rvWUFT7KihRtKvwUuZvvsP6Plb_Nq3YfpMfDO4-yLIkL9AnlpWTf";
	
	*/
	
	// The SENDER_ID here is the "Browser Key" that was generated when I
	// created the API keys for my Google APIs project.
	private static final String SENDER_ID = "AIzaSyCvvuW8JkUXUn-pWegkok7ixaQ8OzKivuc";
	
	
	
	// This is a *cheat*  It is a hard-coded registration ID from an Android device
	// that registered itself with GCM using the same project id shown above.
	private static final String ANDROID_DEVICE = "9ae86cb498b505a8";
	
	private static final String GCM_TOKEN = "APA91bGyhfU-aEcCH1R9NfsOdu4oiKR8zzJDacJBgM71aKIFjwTeEnB-q0cJCxd9XnKW6H444bYgO7SihhotSkXfbVCMUQ6_pC9RnMxFeK11wOo9CEidImSZYPVanV5O9H3JGkvDHUmx";
	
	
	
	
	// This array will hold all the registration ids used to broadcast a message.
	// for this demo, it will only have the ANDROID_DEVICE id that was captured 
	// when we ran the Android client app through Eclipse.
	private static List<String> androidTargets = new ArrayList<String>();
	
	
	static{
		androidTargets.add(GCM_TOKEN);
	}
	
	
	
	public void pushMessage() {
        ApnsService service = null;
        try {
            // get the certificate
            InputStream certStream = this.getClass().getClassLoader().getResourceAsStream("C:\\RewardTheFan\\apns\\rewardthefan.p12");
            
            System.out.println(certStream);
            
            service = APNS.newService().withCert(certStream, "42works").withSandboxDestination().build();
            // or
            // service = APNS.newService().withCert(certStream,
            // "your_cert_password").withProductionDestination().build();
            service.start();
            // You have to delete the devices from you list that no longer 
            //have the app installed, see method below

            // read your user list
                try {
                    // we had a daily update here, so we need to know how many 
                    //days the user hasn't started the app
                    // so that we get the number of updates to display it as the badge.
                    int days = 5;//(int) ((System.currentTimeMillis() - user.getLastUpdate()) / 1000 / 60 / 60 / 24);
                    PayloadBuilder payloadBuilder = APNS.newPayload();
                    payloadBuilder = payloadBuilder.badge(days).alertBody("First Rewardthefan APNS Notification");
                    // check if the message is too long (it won't be sent if it is) 
                    //and trim it if it is.
                    if (payloadBuilder.isTooLong()) {
                        payloadBuilder = payloadBuilder.shrinkBody();
                    }
                    String payload = payloadBuilder.build();
                    String token = "e59bcf97192ec9ae4c75977ac78e9db81bb50ad68db3cfb3ed79462b693d6db3";
                    service.push(token, payload);
                    
                    
                } catch (Exception ex) {
                    // some logging stuff
                }
        } catch (Exception ex) {
            // more logging
        } finally {
            // check if the service was successfull initialized and stop it here, if it was
            if (service != null) {
                service.stop();
            }
 
        }
    }
 
    /*private void deleteInactiveDevices(ApnsService service) {
        // get the list of the devices that no longer have your app installed from apple
        //ignore the ="" after Date here, it's a bug...
        Map<String, Date> inactiveDevices = service.getInactiveDevices();
        for (String deviceToken : inactiveDevices.keySet()) {
            userDao.deleteByDeviceId(deviceToken);
        }
 
    }*/
	
	public void sendForSmriti() throws Exception {
		
		ApnsService service =
		     APNS.newService()
		     .withCert("C:\\RewardTheFan\\apns\\rewardthefan_distribution.p12", "42works")
		     //.withSandboxDestination() // or 
		     .withProductionDestination()
		     .build();

		String payload =
		    APNS.newPayload()
		    .alertBody("Reward The Fan APNS Notification for Smriti")
		    .badge(1)
		    .sound("default")
		    .build();
		
		//System.out.println("APNS :"+payload);

		try{
			// e59bcf97192ec9ae4c75977ac78e9db81bb50ad68db3cfb3ed79462b693d6db3
			Property  property = DAORegistry.getPropertyDAO().getPropertyByName("smriti.apns.token");
			//System.out.println("APNS START:");
			service.push(property.getValue(), payload);
			//System.out.println("APNS END:");
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	
	
public void sendForAmit() throws Exception {
		
		ApnsService service =
		     APNS.newService()
		     .withCert("C:\\RewardTheFan\\apns\\rewardthefan_distribution.p12", "42works")
		     .withProductionDestination()
		     .build();

		String payload =
		    APNS.newPayload()
		    .alertBody("Reward The Fan APNS Notification For Amit")
		    .badge(1)
		    .sound("default")
		    .build();
		
		System.out.println("APNS :"+payload);

		try{
			// e59bcf97192ec9ae4c75977ac78e9db81bb50ad68db3cfb3ed79462b693d6db3
			Property  property = DAORegistry.getPropertyDAO().getPropertyByName("amit.apns.token");
			
			System.out.println("APNS START:");
			service.push(property.getValue(), payload);
			System.out.println("APNS END:");
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	

	public static void main(String[] args) {
		try {
		
			APNSBroadcast apnsBroadcast = new APNSBroadcast();
			
			apnsBroadcast.pushMessage();
			
			/*File file = new File("C:\\RewardTheFan\\apns\\reward_the_fan.p12");
			
			System.out.println(file.exists());*/
			
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*public void sendNot(){
		
		try {
		    PayLoad payLoad = new PayLoad();

		    payLoad.addAlert("My alert message");
		    payLoad.addBadge(45);
		    payLoad.addSound("default");

		    PushNotificationManager pushManager = PushNotificationManager.getInstance();
		    pushManager.addDevice("iPhone", "f4201f5d8278fe39545349d0868a24a3b60ed732");
		    log.warn("Initializing connectiong with APNS...");

		    // Connect to APNs
		    pushManager.initializeConnection(HOST, PORT, 
		                                 "/etc/Certificates.p12", "password", 
		    SSLConnectionHelper.KEYSTORE_TYPE_PKCS12);

		    Device client = pushManager.getDevice("Lambo");

		    // Send Push
		    log.warn("Sending push notification...");
		    PushNotificationManager.getInstance().sendNotification(client, payLoad);
		 }
		 catch (Exception e) {
		    throw new ApnsPushNotificationException("Unable to send push " + e);
		 }
	}*/
	
}
