package com.zonesws.webservices.util.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.utils.NotificationProperty;
import com.zonesws.webservices.utils.mail.MailManager;


public class APNSNotificationService {
	private static final Logger log = LoggerFactory.getLogger(APNSNotificationService.class);
	private static NotificationProperty properties;
	private static Integer badgeNumber=1;
	private MailManager mailManager;
	
	public NotificationProperty getProperties() {
		return properties;
	}
	public void setProperties(NotificationProperty notificationProperties) {
		properties = notificationProperties;
	}
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	
public static void main(String[] args) throws Exception {
		
	String message = "Test Notification";
	 
	Map<String, String> customFields = new HashMap<String, String>();
	 customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_CONTEST_START));
	  
	  System.out.println(message);
	  
	 //new APNSNotificationService().getAllInActiveDevices();
	  
	 // sendNotificationManually(NotificationType.REGULAR_VIEW_ORDER, "aee48a697d1b88a23cf92f99b68051ec8c3485ebb49da2ec49e1f157e4a71455", message,customFields);
	  sendNotificationManually(NotificationType.REGULAR_VIEW_ORDER, "dd1b45cc7ace64b07e6bd43fcc60da23932c7c91ca60704ac219d896504be851", message,customFields);
	  
	  //sendNotificationManually(NotificationType.CJE_ORDER_PLACED, "e59bcf97192ec9ae4c75977ac78e9db81bb50ad68db3cfb3ed79462b693d6db3", message,customerFields);
	}
	
public synchronized static void sendNotificationManually(NotificationType notificationType,String apnsToken,String message,
		Map<String, String> customerFields) throws Exception {
		
		ApnsService service =
		     APNS.newService()
		     .withCert("C:\\RewardTheFan\\apns\\ProductionRTFForAPIOnly.p12", "12345")
		     //.withSandboxDestination() // or 
		     .withProductionDestination()
		     .build();

		String payload =
		    APNS.newPayload()
		    .alertBody(message)
		    .customFields(customerFields)
		    .sound("default")
		    .build();
		
		System.out.println(payload);

		try{
			//log.info("APNS Begins: NotificationType :"+notificationType+", DeviceToken :"+apnsToken);
			service.push(apnsToken, payload);
			
			Map<String,Date> inactiveMAp = service.getInactiveDevices();
			if(inactiveMAp != null) {
				List<String> keyList = new ArrayList<>(inactiveMAp.keySet());
				for (String device : keyList) {
					System.out.println(device+" : "+ inactiveMAp.get(device));
				}	
			}
			//log.info("APNS Ends: NotificationType :"+notificationType+", DeviceToken :"+apnsToken);
		}catch(Exception e){
			log.warn("APNS : NotificationType :"+notificationType+", DeviceToken :"+apnsToken+" ,Exception :"+e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			if(null != service){
				service.stop();
			}
		}
	}

	private ApnsService getApnsService() {
		ApnsService service =
			     APNS.newService()
			     .withCert(properties.getApnsp12CertificatePath(), properties.getApnsCertificatePassword())
			     //.withCert("C:\\RewardTheFan\\apns\\ProductionRTFForAPIOnly.p12", "12345")
			     //.withSandboxDestination() // or 
			     .withProductionDestination()
			     .build();
		return service;
	}
	private String getApnsPayLoad(String message,Map<String, String> customFields) {
		String payload =
			    APNS.newPayload()
			    .alertBody(message).customFields(customFields)
			   // .badge(45)
			    .sound("default")
			    .build();
		return payload;
	}

	public synchronized void sendNotification(NotificationType notificationType,String apnsToken,String message,
			Map<String, String> customFields) throws Exception {
		
		/*ApnsService service =
		     APNS.newService()
		     .withCert(properties.getApnsp12CertificatePath(), properties.getApnsCertificatePassword())
		     //.withSandboxDestination() // or 
		     .withProductionDestination()
		     .build();*/

		/*String payload =
		    APNS.newPayload()
		    .alertBody(message).customFields(customFields)
		   // .badge(45)
		    .sound("default")
		    .build();*/
		ApnsService service = getApnsService();
		String payload = getApnsPayLoad(message, customFields);

		try{
			//log.info("APNS Begins: NotificationType :"+notificationType+", DeviceToken :"+apnsToken);
			service.push(apnsToken, payload);
			//log.info("APNS Ends: NotificationType :"+notificationType+", DeviceToken :"+apnsToken);
		}catch(Exception e){
			log.warn("APNS ERR : NotificationType :"+notificationType+", DeviceToken :"+apnsToken+" ,Exception :"+e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}finally{
			if(null != service){
				service.stop();
			}
		}
	}
	public synchronized Map<String,Date> getAllInActiveDevices() throws Exception {
		
		Map<String,Date> inactiveDeviceMap = new HashMap<String,Date>();
		ApnsService service = getApnsService();
		try{
			Map<String,Date> tempMap = service.getInactiveDevices();
			if(tempMap != null) {
				List<String> keyList = new ArrayList<>(tempMap.keySet());
				for (String device : keyList) {
					inactiveDeviceMap.put(device.toLowerCase(), tempMap.get(device));
					System.out.println(device+" : "+ tempMap.get(device));
				}	
			}

		}catch(Exception e){
			log.warn("APNS : GET INACTIVEDEVICES :"+e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			if(null != service){
				service.stop();
			}
		}
		return inactiveDeviceMap;
	}
	
public synchronized void sendNotification(NotificationType notificationType,List<String> apnsTokens,String message,Map<String, String> customFields) throws Exception {
		
		ApnsService service =
		     APNS.newService()
		     .withCert(properties.getApnsp12CertificatePath(), properties.getApnsCertificatePassword())
		     //.withSandboxDestination() // or 
		     .withProductionDestination()
		     .build();

		String payload =
		    APNS.newPayload()
		    .alertBody(message).customFields(customFields)
		   // .badge(45)
		    .sound("default")
		    .build();

		try{
			log.info("APNS Begins: NotificationType :"+notificationType+", DeviceToken :"+apnsTokens.toString());
			service.push(apnsTokens, payload);
			log.info("APNS Ends: NotificationType :"+notificationType+", DeviceToken :"+apnsTokens.toString());
		}catch(Exception e){
			log.warn("APNS : NotificationType :"+notificationType+", DeviceToken :"+apnsTokens.toString()+" ,Exception :"+e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			if(null != service){
				service.stop();
			}
		}
	}
	
}
