package com.zonesws.webservices.util.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.utils.NotificationProperty;
import com.zonesws.webservices.utils.mail.MailManager;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

public class APNSNotificationServicePushTemp {
	
	private static final Logger log = LoggerFactory.getLogger(APNSNotificationServicePushTemp.class);
	private static NotificationProperty properties;
	
	public NotificationProperty getProperties() {
		return properties;
	}
	public void setProperties(NotificationProperty notificationProperties) {
		properties = notificationProperties;
	}
	
	
    public static void main(String[] args) throws Exception {
    	
    	String message = "Put121 your hands up, Reward The Fan Is giving away tickets in less than 15 minutes!";
   	 
    	Map<String, String> customFields = new HashMap<String, String>();
    	 customFields.put("notificationType", String.valueOf(NotificationType.QUIZ_CONTEST_START));
    	  
    	  System.out.println(message);
    	  
    	  
    	 // sendNotificationManually(NotificationType.REGULAR_VIEW_ORDER, "aee48a697d1b88a23cf92f99b68051ec8c3485ebb49da2ec49e1f157e4a71455", message,customFields);
    	  sendNotificationManually(NotificationType.QUIZ_CONTEST_START, "212d58c6f9631f4e77cf0bfb3991fee9b46e0ec43f44d64c3e69d8aaf4e975f1", message,customFields);
    	//sendNotificationManually(notificationType, apnsToken, message, customerFields);
    	
    }
    public static void  sendTestPushNotificationMain() {
        BasicConfigurator.configure();
        try {
            PushNotificationPayload payload = PushNotificationPayload.complex();
            payload.addAlert("test notification");
            payload.addBadge(1);
            payload.addSound("default");
            payload.addCustomDictionary("id", "1");
            System.out.println(payload.toString());
            List < PushedNotification > NOTIFICATIONS = Push.payload(payload, "C:\\\\RewardTheFan\\\\apns\\\\RewardTheFanDistributionPush.p12", "12345", true, "1aca862ce1b0a79bc0de7cb9da971cdb302bbe1b6191f2b6341f7a9b133b87dd");
            for (PushedNotification NOTIFICATION: NOTIFICATIONS) {
                if (NOTIFICATION.isSuccessful()) {
                    /* APPLE ACCEPTED THE NOTIFICATION AND SHOULD DELIVER IT */
                    System.out.println("PUSH NOTIFICATION SENT SUCCESSFULLY TO: " +
                        NOTIFICATION.getDevice().getToken());
                    /* STILL NEED TO QUERY THE FEEDBACK SERVICE REGULARLY */
                } else {
                    String INVALIDTOKEN = NOTIFICATION.getDevice().getToken();
                    /* ADD CODE HERE TO REMOVE INVALIDTOKEN FROM YOUR DATABASE */
                    /* FIND OUT MORE ABOUT WHAT THE PROBLEM WAS */
                    Exception THEPROBLEM = NOTIFICATION.getException();
                    THEPROBLEM.printStackTrace();
                    /* IF THE PROBLEM WAS AN ERROR-RESPONSE PACKET RETURNED BY APPLE, GET IT */
                    ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();
                    if (THEERRORRESPONSE != null) {
                        System.out.println(THEERRORRESPONSE.getMessage());
                    }
                }
            }
        } catch (CommunicationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (KeystoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public synchronized static String sendNotificationManually(NotificationType notificationType,String apnsToken,String message,
    		Map<String, String> customerFields) throws Exception {
    		
    	String resMsg = "";
    	//BasicConfigurator.configure();
    	PushNotificationPayload payload = PushNotificationPayload.complex();
        payload.addAlert(message);
        payload.addBadge(1);
        payload.addSound("default");
        payload.addCustomDictionary("notificationType", notificationType);

    		/*String payload =
    		    APNS.newPayload()
    		    .alertBody(message)
    		    .customFields(customerFields)
    		    .sound("default")
    		    .build();*/
    		
    		System.out.println(payload);

    		try{
    			 List < PushedNotification > NOTIFICATIONS = Push.payload(payload,
    					 "C:\\\\RewardTheFan\\\\apns\\\\RewardTheFanDistributionPush.p12", "12345",
    					 true, 
    					 apnsToken//"1aca862ce1b0a79bc0de7cb9da971cdb302bbe1b6191f2b6341f7a9b133b87dd"
    					 );
    	            for (PushedNotification NOTIFICATION: NOTIFICATIONS) {
    	                if (NOTIFICATION.isSuccessful()) {
    	                    /* APPLE ACCEPTED THE NOTIFICATION AND SHOULD DELIVER IT */
    	                    System.out.println("PUSH NOTIFICATION SENT SUCCESSFULLY TO: " +
    	                        NOTIFICATION.getDevice().getToken());
    	                    /* STILL NEED TO QUERY THE FEEDBACK SERVICE REGULARLY */
    	                    resMsg = "1";
    	                } else {
    	                    String INVALIDTOKEN = NOTIFICATION.getDevice().getToken();
    	                    /* ADD CODE HERE TO REMOVE INVALIDTOKEN FROM YOUR DATABASE */
    	                    /* FIND OUT MORE ABOUT WHAT THE PROBLEM WAS */
    	                    //Exception THEPROBLEM = NOTIFICATION.getException();
    	                   // THEPROBLEM.printStackTrace();
    	                    /* IF THE PROBLEM WAS AN ERROR-RESPONSE PACKET RETURNED BY APPLE, GET IT */
    	                    ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();
    	                    if (THEERRORRESPONSE != null) {
    	                        System.out.println(THEERRORRESPONSE.getMessage());
    	                        resMsg = THEERRORRESPONSE.getMessage();
    	                    } else {
    	                    	 resMsg = "Error With No Response";
    	                    }
    	                }
    	            }
    	            System.out.println("APNS Ends: NotificationType :"+notificationType+", DeviceToken :"+apnsToken);
    		}catch(Exception e){
    			System.out.println("APNS : NotificationType :"+notificationType+", DeviceToken :"+apnsToken+" ,Exception :"+e.getLocalizedMessage());
    			e.printStackTrace();
    			
    			resMsg = e.getLocalizedMessage();
    		}
    		return resMsg;
    	}
    
    public synchronized static String sendNotification(NotificationType notificationType,String apnsToken,String message,
    		Map<String, String> customerFields) throws Exception {
    		
    	String resMsg = "";
    	//BasicConfigurator.configure();
    	PushNotificationPayload payload = PushNotificationPayload.complex();
        payload.addAlert(message);
        payload.addBadge(1);
        payload.addSound("default");
        payload.addCustomDictionary("notificationType", notificationType);

    		/*String payload =
    		    APNS.newPayload()
    		    .alertBody(message)
    		    .customFields(customerFields)
    		    .sound("default")
    		    .build();*/
    		
        //log.info(payload);

    		try{
    			 List < PushedNotification > NOTIFICATIONS = Push.payload(payload,
    					 properties.getApnsp12CertificatePath(), properties.getApnsCertificatePassword(),
    					 true, 
    					 apnsToken//"1aca862ce1b0a79bc0de7cb9da971cdb302bbe1b6191f2b6341f7a9b133b87dd"
    					 );
    	            for (PushedNotification NOTIFICATION: NOTIFICATIONS) {
    	                if (NOTIFICATION.isSuccessful()) {
    	                    /* APPLE ACCEPTED THE NOTIFICATION AND SHOULD DELIVER IT */
    	                	log.info("PUSH NOTIFICATION SENT SUCCESSFULLY TO: " +
    	                        NOTIFICATION.getDevice().getToken());
    	                    /* STILL NEED TO QUERY THE FEEDBACK SERVICE REGULARLY */
    	                    resMsg = "1";
    	                } else {
    	                    String INVALIDTOKEN = NOTIFICATION.getDevice().getToken();
    	                    /* ADD CODE HERE TO REMOVE INVALIDTOKEN FROM YOUR DATABASE */
    	                    /* FIND OUT MORE ABOUT WHAT THE PROBLEM WAS */
    	                    //Exception THEPROBLEM = NOTIFICATION.getException();
    	                   // THEPROBLEM.printStackTrace();
    	                    /* IF THE PROBLEM WAS AN ERROR-RESPONSE PACKET RETURNED BY APPLE, GET IT */
    	                    ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();
    	                    if (THEERRORRESPONSE != null) {
    	                    	log.info(THEERRORRESPONSE.getMessage());
    	                        resMsg = THEERRORRESPONSE.getMessage();
    	                    } else {
    	                    	 resMsg = "Error With No Response";
    	                    }
    	                }
    	            }
    	            log.info("APNS Ends: NotificationType :"+notificationType+", DeviceToken :"+apnsToken);
    		}catch(Exception e){
    			log.info("APNS : NotificationType :"+notificationType+", DeviceToken :"+apnsToken+" ,Exception :"+e.getLocalizedMessage());
    			e.printStackTrace();
    			
    			resMsg = e.getLocalizedMessage();
    		}
    		return resMsg;
    	}
}