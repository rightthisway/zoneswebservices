package com.zonesws.webservices.util.service;

import com.zonesws.webservices.data.CustomerCardInfo;
import com.zonesws.webservices.data.UserAddress;

public interface CreditCardCheckout {
	 public Boolean getAuthorized(Double orderAmount , CustomerCardInfo customerCardInfo , UserAddress billingAddress,UserAddress shippingAddress);
	 public Boolean getAuthorized(Double orderAmount , CustomerCardInfo customerCardInfo , UserAddress shippingAddress,String guid);
}
