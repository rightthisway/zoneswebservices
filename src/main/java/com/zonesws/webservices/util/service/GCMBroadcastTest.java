package com.zonesws.webservices.util.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;


public class GCMBroadcastTest {
	 

	/*// The SENDER_ID here is the "Browser Key" that was generated when I
	// created the API keys for my Google APIs project.
	private static final String SENDER_ID = "AIzaSyC--Z_BNbEMz4J_iyYwR_xCa9hoakLMVpQ";
	
	// This is a *cheat*  It is a hard-coded registration ID from an Android device
	// that registered itself with GCM using the same project id shown above.
	private static final String ANDROID_DEVICE = "9ae86cb498b505a8";
	
	private static final String GCM_TOKEN = "APA91bEqbvJJrsWacrdcnpUaS2pyT648K7B-mU9RYLeNM9qXdFsHp85CR8YMWWJRK6qliCeR1q-KpMN82V35usp2rvWUFT7KihRtKvwUuZvvsP6Plb_Nq3YfpMfDO4-yLIkL9AnlpWTf";
	
	*/
	
	// The SENDER_ID here is the "Browser Key" that was generated when I
	// created the API keys for my Google APIs project.
	private static final String SENDER_ID = "AIzaSyBgUTTgoTNPu-MeealWcua2IAc_YKYQ-dY";
	
	
	
	// This is a *cheat*  It is a hard-coded registration ID from an Android device
	// that registered itself with GCM using the same project id shown above.
	private static final String ANDROID_DEVICE = "9ae86cb498b505a8";
	
	private static final String GCM_TOKEN = "APA91bGyhfU-aEcCH1R9NfsOdu4oiKR8zzJDacJBgM71aKIFjwTeEnB-q0cJCxd9XnKW6H444bYgO7SihhotSkXfbVCMUQ6_pC9RnMxFeK11wOo9CEidImSZYPVanV5O9H3JGkvDHUmx";
	
	//APA91bGyhfU-aEcCH1R9NfsOdu4oiKR8zzJDacJBgM71aKIFjwTeEnB-q0cJCxd9XnKW6H444bYgO7SihhotSkXfbVCMUQ6_pC9RnMxFeK11wOo9CEidImSZYPVanV5O9H3JGkvDHUmx
	//Browser Key :- AIzaSyBgUTTgoTNPu-MeealWcua2IAc_YKYQ-dY
	// Server Key:- AIzaSyCqe7-PqMhtzw49a877s-xPz9wzvhuoMD0
	
	
	
	// This array will hold all the registration ids used to broadcast a message.
	// for this demo, it will only have the ANDROID_DEVICE id that was captured 
	// when we ran the Android client app through Eclipse.
	private static List<String> androidTargets = new ArrayList<String>();
	
	
	static{
		androidTargets.add(GCM_TOKEN);
	}
	
	
	
	 // This doPost() method is called from the form in our index.jsp file.
    // It will broadcast the passed "Message" value.
	public static void doPost(String userMessage , String gcmRegistrationId) throws ServletException, IOException {
		
		// We'll collect the "CollapseKey" and "Message" values from our JSP page
		String collapseKey = "";
		//String userMessage = "";
		
		try {
			//userMessage = request.getParameter("Message");
			//collapseKey = request.getParameter("CollapseKey");
			
			collapseKey = gcmRegistrationId;
			//userMessage = message;
			
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		// Instance of com.android.gcm.server.Sender, that does the
		// transmission of a Message to the Google Cloud Messaging service.
		Sender sender = new Sender(SENDER_ID);
		
		// This Message object will hold the data that is being transmitted
		// to the Android client devices.  For this demo, it is a simple text
		// string, but could certainly be a JSON object.
		Message message = new Message.Builder()
		
		// If multiple messages are sent using the same .collapseKey()
		// the android target device, if it was offline during earlier message
		// transmissions, will only receive the latest message for that key when
		// it goes back on-line.
		.collapseKey(collapseKey)
		.timeToLive(30)
		.delayWhileIdle(true)
		.addData("message", userMessage)
		.build();
		
		try {
			// use this for multicast messages.  The second parameter
			// of sender.send() will need to be an array of register ids.
			MulticastResult result = sender.send(message, androidTargets, 1);
			
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {
					
				}
				System.out.println("Broadcast Success: " );
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

				
	}
	
	public static void maindf(String[] args) {
		try {
			doPost("My First Notification", GCM_TOKEN);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	public static void doPost() throws Exception {


		try{
		     Sender sender = new Sender(SENDER_ID);
		     ArrayList<String> devicesList = new ArrayList<String>();
		     devicesList.add(ANDROID_DEVICE);
		     String data = "Our First GCM Notification";
		     Message message = new Message.Builder()
		                        .collapseKey("1")
		                        .timeToLive(3)
		                        .delayWhileIdle(true)
		                        .addData("message",
		                                data)
		                        .build();
		    MulticastResult result = sender.send(message, devicesList, 1);
		                //sender.send(message, devicesList, 1);

		                System.out.println(result.toString());
		                if (result.getResults() != null) {
		                    int canonicalRegId = result.getCanonicalIds();
		                    if (canonicalRegId != 0) {
		                    }
		                } else {
		                    int error = result.getFailure();
		                    System.out.println(error);
		                }

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public static void sendMessage() throws ServletException, IOException {


		Result result = null;
		try {
			String regId = GCM_TOKEN;
			String userMessage = "RewardTheFan Second Test Notification";
			GCMSender sender = new GCMSender(SENDER_ID);
			Message message = new Message.Builder().timeToLive(60)
					.delayWhileIdle(true).addData("message", userMessage).build();
			System.out.println("regId: " + regId);
			result = sender.send(message, regId, 5);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	
	

	public static void main(String[] args) {
		try {
			sendMessage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
