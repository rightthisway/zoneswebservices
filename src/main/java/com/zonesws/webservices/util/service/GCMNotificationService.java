package com.zonesws.webservices.util.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.zonesws.webservices.enums.NotificationType;
import com.zonesws.webservices.utils.NotificationJsonObject;
import com.zonesws.webservices.utils.NotificationProperty;
import com.zonesws.webservices.utils.mail.MailManager;


public class GCMNotificationService {
	
	private static final Logger log = LoggerFactory.getLogger(GCMNotificationService.class);
	private static NotificationProperty properties;
	private MailManager mailManager;
	
	public NotificationProperty getProperties() {
		return properties;
	}
	public void setProperties(NotificationProperty notificationProperties) {
		properties = notificationProperties;
	}
	
	public MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	
	public static void main(String[] args) throws Exception {
		
		
		NotificationJsonObject notificationJsonObject = new NotificationJsonObject();
		notificationJsonObject.setNotificationType(NotificationType.QUIZ_GRAND_WINNER);
		notificationJsonObject.setMessage("Congratulations, You have won 4tickets");
		//notificationJsonObject.setOrderId(customerOrder.getId());
		Gson gson = new Gson();	
		String jsonString = gson.toJson(notificationJsonObject);
		System.out.println(jsonString);
		
		List<String> deviceId = new ArrayList<String>();
		deviceId.add("cgG-PkVtdDc:APA91bEzodRKcaK8D-Nk2Cro1vEK3PS4AJuik5mbdaBj7XRmlkIkemUrCmT-SMHPh_QPrP99zkPSZqVNKZJ9jIoIFvw4MAI06jews6083AD-ZfQatSOp8JUqyPFH52Y1ZNVTw6iHLP1m");
		//deviceId.add("APA91bHXNFLx76MWZeWbagDU5ZHKl9c1DfRfaK0hWbdFI6Z9cnx8V2eBHJ07gif5wmQrNxyEETPMNmaYqlMCFIcNwJ4JdBPg4AzmYMi78KVksjeQPYl7_GDgTCWXr81jAThJrFNkADjP");
		
		sendMessageNew(NotificationType.QUIZ_CONTEST_START, deviceId, jsonString);
		
		deviceId = new ArrayList<String>();
		deviceId.add("APA91bHXNFLx76MWZeWbagDU5ZHKl9c1DfRfaK0hWbdFI6Z9cnx8V2eBHJ07gif5wmQrNxyEETPMNmaYqlMCFIcNwJ4JdBPg4AzmYMi78KVksjeQPYl7_GDgTCWXr81jAThJrFNkADjP");
		sendMessageNew(NotificationType.QUIZ_CONTEST_START, deviceId, jsonString);
		
		
		//sendMessageManually(NotificationType.REGULAR_VIEW_ORDER, "APA91bFUIyBoEXT3QWzJUXR0Ct51pibsb_AlbWdAqsebPdD4iNGAw8rOZLp7YV4KPbNij3Gj7IB49JvUtXPGswnc4UT-wEW3goV43AD-JAq6cdV8d1oj114gTerZ34gmwo57iYztMVkq", jsonString);
		//sendMessageManually(NotificationType.REGULAR_VIEW_ORDER, "APA91bGY5PNJT_wYJkXDm7spqr67gheKKBfqzdlQsiKt-Gfk7VkQoeU3p7yiUR1hiLm_Tcl7pjj2kz1XqSMvBM_Ghu9AP7ygPIAh3v55LX1KeIQD4eDQdHnxd-G2tvJxE4fuszOW7iuz", jsonString);
	}
	
	public synchronized static void sendMessageManually(NotificationType notificationType,String gcmToken,String message) throws Exception {
		Result result = null;
		try {
			//log.info("GCM Begins : NotificationType :"+notificationType+", DeviceToken :"+gcmToken);
			GCMSender sender = new GCMSender("AIzaSyBgUTTgoTNPu-MeealWcua2IAc_YKYQ-dY");
			Message gcmMessage = new Message.Builder().timeToLive(60)
					.delayWhileIdle(true).addData("message", message).build();
			result = sender.send(gcmMessage, gcmToken, 1);
			//log.info("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+", GCMSuccessStatus :"+result.getSuccess());
		} catch (Exception e) {
			log.warn("APNS : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+" ,Exception :"+e.getLocalizedMessage());
			e.printStackTrace();
		} 
	}
	
	public synchronized String sendMessage(NotificationType notificationType,String gcmToken,String message) throws Exception {
		String resMsg = "";
		Result result = null;
		try {
			//log.info("GCM Begins : NotificationType :"+notificationType+", DeviceToken :"+gcmToken);
			GCMSender sender = new GCMSender(properties.getGcmSenderKey());
			Message gcmMessage = new Message.Builder().timeToLive(60)
					.delayWhileIdle(true).addData("message", message).build();
			result = sender.send(gcmMessage, gcmToken, 1);
			
			
			if(result.getSuccess()!= null && result.getSuccess().equals(1)) {
				resMsg = "1";
			} else {
				if(result.getErrorCodeName() != null) {
					resMsg = result.getErrorCodeName();
				} else {
					resMsg = "Failed with no Message";
				}
			}
			//log.info("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+", GCMSuccessStatus :"+result.getSuccess());
		} catch (Exception e) {
			log.warn("GCM ERR : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+" ,Exception :"+e.getLocalizedMessage());
			resMsg = "Exception : "+e.getLocalizedMessage();
			e.printStackTrace();
		} 
		return resMsg;
	}
	
	public synchronized String sendMessageOne(NotificationType notificationType,String gcmToken,String message) throws Exception {
		String resMsg = "";
		MulticastResult result = null;
		try {
			
			GCMSender sender = new GCMSender(properties.getGcmSenderKey());
			Message gcmMessage = new Message.Builder().timeToLive(60)
					.delayWhileIdle(true).addData("message", message).build();
			//result = sender.send(gcmMessage, gcmToken, 1);
			List<String> regIds = new ArrayList<>();
			regIds.add(gcmToken);
			result = sender.send(gcmMessage, regIds, 1);
			
			if(result.getFailure() == 1) {
				List<Result> resultList = result.getResults();
				if (resultList != null) {
					for (Result result2 : resultList) {
						resMsg = ","+result2.getErrorCodeName();
					}
					if(!resMsg.isEmpty()) {
						resMsg = resMsg.substring(1);
					}
				}
				if(resMsg.isEmpty()) {
					resMsg = "Failed with No Msg";
				}
				//System.out.println("Messgae : "+msg);
			} else {
				resMsg="1";
			}
			//System.out.println("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+regIds.toString()+", GCMSuccessStatus :"+result.getSuccess());
			
			
			//log.info("GCM Begins : NotificationType :"+notificationType+", DeviceToken :"+gcmToken);
			
		/*	if(result.getSuccess()!= null && result.getSuccess().equals(1)) {
				resMsg = "1";
			} else {
				if(result.getErrorCodeName() != null) {
					resMsg = result.getErrorCodeName();
				} else {
					resMsg = "Failed with no Message";
				}
			}*/
			//log.info("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+", GCMSuccessStatus :"+result.getSuccess());
		} catch (Exception e) {
			log.warn("GCM ERR : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+" ,Exception :"+e.getLocalizedMessage());
			resMsg = "Exception : "+e.getLocalizedMessage();
			e.printStackTrace();
		} 
		return resMsg;
	}
	
	public synchronized String sendFCMMessage(NotificationType notificationType,String gcmToken,String message) throws Exception {
		String resMsg = "";
		MulticastResult result = null;
		try {
			
			GCMSender sender = new GCMSender(properties.getFcmSenderKey());
			Message gcmMessage = new Message.Builder().timeToLive(60)
					.delayWhileIdle(true).addData("message", message).build();
			//result = sender.send(gcmMessage, gcmToken, 1);
			List<String> regIds = new ArrayList<>();
			regIds.add(gcmToken);
			result = sender.send(gcmMessage, regIds, 1);
			
			if(result.getFailure() == 1) {
				List<Result> resultList = result.getResults();
				if (resultList != null) {
					for (Result result2 : resultList) {
						resMsg = ","+result2.getErrorCodeName();
					}
					if(!resMsg.isEmpty()) {
						resMsg = resMsg.substring(1);
					}
				}
				if(resMsg.isEmpty()) {
					resMsg = "Failed with No Msg";
				}
				//System.out.println("Messgae : "+msg);
			} else {
				resMsg="1";
			}
			//System.out.println("FCM Ends : NotificationType :"+notificationType+", DeviceToken :"+regIds.toString()+", FCMSuccessStatus :"+result.getSuccess());
			
			
			//log.info("GCM Begins : NotificationType :"+notificationType+", DeviceToken :"+gcmToken);
			
		/*	if(result.getSuccess()!= null && result.getSuccess().equals(1)) {
				resMsg = "1";
			} else {
				if(result.getErrorCodeName() != null) {
					resMsg = result.getErrorCodeName();
				} else {
					resMsg = "Failed with no Message";
				}
			}*/
			//log.info("FCM Ends : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+", FCMSuccessStatus :"+result.getSuccess());
		} catch (Exception e) {
			log.warn("FCM ERR : NotificationType :"+notificationType+", DeviceToken :"+gcmToken+" ,Exception :"+e.getLocalizedMessage());
			resMsg = "Exception : "+e.getLocalizedMessage();
			e.printStackTrace();
		} 
		return resMsg;
	}
	public synchronized void sendMessage(NotificationType notificationType,List<String> androidTargets ,String gcmMessage) throws Exception {
		try {
			log.info("GCM Begins : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString());
			
			// Instance of com.android.gcm.server.Sender, that does the
			// transmission of a Message to the Google Cloud Messaging service.
			Sender sender = new Sender(properties.getGcmSenderKey());
			
			// This Message object will hold the data that is being transmitted
			// to the Android client devices.  For this demo, it is a simple text
			// string, but could certainly be a JSON object.
			Message message = new Message.Builder()
			
			// If multiple messages are sent using the same .collapseKey()
			// the android target device, if it was offline during earlier message
			// transmissions, will only receive the latest message for that key when
			// it goes back on-line.
			.collapseKey("1")
			.timeToLive(30)
			.delayWhileIdle(true)
			.addData("message", gcmMessage)
			.build();
			
			// use this for multicast messages.  The second parameter
			// of sender.send() will need to be an array of register ids.
			MulticastResult result = sender.send(message, androidTargets, 1);
			
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {
					
				}
				System.out.println("Broadcast Success: "+canonicalRegId );
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}
			//System.out.println("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString()+", GCMSuccessStatus :"+result.getSuccess());
			//log.info("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString()+", GCMSuccessStatus :"+result.getSuccess());
		} catch (Exception e) {
			log.warn("APNS : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString()+" ,Exception :"+e.getLocalizedMessage());
			e.printStackTrace();
		} 
	}
	
	public synchronized static void sendMessageNew(NotificationType notificationType,List<String> androidTargets ,String gcmMessage) throws Exception {
		try {
			log.info("GCM Begins : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString());
			
			// Instance of com.android.gcm.server.Sender, that does the
			// transmission of a Message to the Google Cloud Messaging service.
			Sender sender = new Sender("AIzaSyDS0i--v_i0Q1TYca8LqaClHV_ILfFMExs");
			
			// This Message object will hold the data that is being transmitted
			// to the Android client devices.  For this demo, it is a simple text
			// string, but could certainly be a JSON object.
			Message message = new Message.Builder()
			
			// If multiple messages are sent using the same .collapseKey()
			// the android target device, if it was offline during earlier message
			// transmissions, will only receive the latest message for that key when
			// it goes back on-line.
			.collapseKey("1")
			.timeToLive(30)
			.delayWhileIdle(true)
			.addData("message", gcmMessage)
			.build();
			Result result1 = null;
			String restMsg = "";
			result1 = sender.send(message, androidTargets.get(0), 1);
			if(result1.getSuccess()!= null && result1.getSuccess().equals(1)) {
				restMsg = "1";
			} else {
				if(result1.getErrorCodeName() != null) {
					restMsg = result1.getErrorCodeName();
				} else {
					restMsg = "Failed with no Message";
				}
			}
			System.out.println(result1);
			
			// use this for multicast messages.  The second parameter
			// of sender.send() will need to be an array of register ids.
			MulticastResult result = sender.send(message, androidTargets, 1);
			
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {
					
				}
				System.out.println("Broadcast Success: "+canonicalRegId );
			} else {
				int error = result.getFailure();
				System.out.println("Broadcast failure: " + error);
			}
			if(result.getFailure() == 1) {
				String msg = "";
				List<Result> resultList = result.getResults();
				if (resultList != null) {
					for (Result result2 : resultList) {
						msg = ","+result2.getErrorCodeName();
					}
					if(!msg.isEmpty()) {
						msg = msg.substring(1);
					}
				}
				System.out.println("Messgae : "+msg);
			}
			//System.out.println("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString()+", GCMSuccessStatus :"+result.getSuccess());
			//log.info("GCM Ends : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString()+", GCMSuccessStatus :"+result.getSuccess());
		} catch (Exception e) {
			log.warn("APNS : NotificationType :"+notificationType+", DeviceToken :"+androidTargets.toString()+" ,Exception :"+e.getLocalizedMessage());
			e.printStackTrace();
		} 
	}
	
}
