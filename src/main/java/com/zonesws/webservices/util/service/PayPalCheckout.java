package com.zonesws.webservices.util.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.PaypalTransactionDetail;



public interface PayPalCheckout {
	 public PaypalTransactionDetail getPaymentInformation(PaypalTransactionDetail paypalTransactionDetail) throws PayPalRESTException;
	 public Payment doPaypalFuturePayment(String correlationId, String authorizationCode,String trxAmount,
			 String description,Customer customer) throws PayPalRESTException, FileNotFoundException, IOException;
	 
}
