package com.zonesws.webservices.util.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.paypal.api.openidconnect.CreateFromAuthorizationCodeParameters;
import com.paypal.api.openidconnect.Tokeninfo;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.FuturePayment;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.PaypalApi;
 
public class PayPalFuturePayment { 
  
 private static final Logger log = LogManager.getLogger(PayPalFuturePayment.class); 
  
 
 public Payment create(String correlationId, String authorizationCode,String trxAmount,
		 String description) throws PayPalRESTException, FileNotFoundException, IOException { 
	 	
	  List<PaypalApi> paypalApis= DAORegistry.getQueryManagerDAO().getPaypalCredentialByType();
	  PaypalApi paypalApi = paypalApis.get(0);
		 
	  Payer payer = new Payer(); 
	  payer.setPaymentMethod("paypal"); 
	  Amount amount = new Amount(); 
	  amount.setTotal(trxAmount); 
	  amount.setCurrency("USD"); 
	  Transaction transaction = new Transaction(); 
	  transaction.setAmount(amount); 
	  transaction.setDescription(description); 
	  List<Transaction> transactions = new ArrayList<Transaction>(); 
	  transactions.add(transaction); 
	   
	  FuturePayment futurePayment = new FuturePayment(); 
	  futurePayment.setIntent("authorize"); 
	  futurePayment.setPayer(payer); 
	  futurePayment.setTransactions(transactions); 
	   
	  Tokeninfo tokeninfo = null; 
	  if (authorizationCode != null && authorizationCode.trim().length() > 0) { 
	   log.info("creating future payment with auth code: " + authorizationCode); 
	    
	   //ClientCredentials credentials = futurePayment.getClientCredential(); 
	   CreateFromAuthorizationCodeParameters params = new CreateFromAuthorizationCodeParameters(); 
	   params.setClientID(paypalApi.getClientId()); 
	   params.setClientSecret(paypalApi.getSecretKey()); 
	   params.setCode(authorizationCode); 
	 
	   Map<String, String> configurationMap = new HashMap<String, String>(); 
	   configurationMap.put("mode", "sandbox"); 
	   APIContext apiContext = new APIContext(paypalApi.getClientId(), paypalApi.getSecretKey(),"sandbox"); 
	   apiContext.setConfigurationMap(configurationMap); 
	   tokeninfo = Tokeninfo.createFromAuthorizationCodeForFpp(apiContext, params); 
	   tokeninfo.setAccessToken(tokeninfo.getTokenType() + " " + tokeninfo.getAccessToken()); 
	 
	   System.out.println("Generated access token from auth code: " + tokeninfo.getAccessToken()); 
	  } 
	 
	  Payment createdPayment = futurePayment.create(tokeninfo.getAccessToken(), correlationId); 
	  if (createdPayment.getIntent().equals("authorize")) { 
	   System.out.println("payment authorized"); 
	   System.out.println("Payment ID=" + createdPayment.getId()); 
	   System.out.println("Authorization ID=" + createdPayment.getTransactions().get(0).getRelatedResources().get(0).getAuthorization().getId()); 
	  } 
	   
	  return createdPayment; 
	 } 
}

