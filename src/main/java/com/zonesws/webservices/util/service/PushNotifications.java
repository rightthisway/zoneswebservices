package com.zonesws.webservices.util.service;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;


public class PushNotifications  {

	public static void main(String [] args) {
        System.out.println("Sending an iOS push notification...");

        String token = "804d6613561cf06e8306ac05d3fde69ce00b03206da6f5c71ba77898abf5d343";
        String type = "dev";
        String message = "the test push notification message";


        System.out.println("The target token is "+token);

        ApnsServiceBuilder serviceBuilder = APNS.newService();

        if (type.equals("prod")) {
            System.out.println("using prod API");
            String certPath = PushNotifications.class.getResource("C:\\RewardTheFan\\apns\\reward_the_fan.p12").getPath();
            serviceBuilder.withCert(certPath, "42works")
                    .withProductionDestination();
        } else if (type.equals("dev")) {
            System.out.println("using dev API");
            //String certPath = PushNotifications.class.getResource("C:\\RewardTheFan\\apns\\reward_the_fan.p12").getPath();
            serviceBuilder.withCert("C:\\RewardTheFan\\apns\\rewardthefan.p12", "42works")
                    .withSandboxDestination();
        } else {
            System.out.println("unknown API type "+type);
            return;
        }

        ApnsService service = serviceBuilder.build();
      //Payload with custom fields
        String payload = APNS.newPayload()
                .alertBody(message)
                .alertTitle("test alert title")
                .sound("default")
                .customField("custom", "custom value").build();

        ////Payload with custom fields
        //String payload = APNS.newPayload()
        //        .alertBody(message).build();

        ////String payload example:
        //String payload = "{\"aps\":{\"alert\":{\"title\":\"My Title 1\",\"body\":\"My message 1\",\"category\":\"Personal\"}}}";


        System.out.println("payload: "+payload);
        service.push(token, payload);

        System.out.println("The message has been hopefully sent...");
    }
}
