package com.zonesws.webservices.util.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zonesws.webservices.data.CustomerCardInfo;
import com.zonesws.webservices.data.UserAddress;
import com.zonesws.webservices.util.service.CreditCardCheckout;
import com.zonesws.webservices.utils.ZonesProperty;
 
public class CreditCardCheckoutImpl implements CreditCardCheckout{
    
    private static final Logger logger = LoggerFactory.getLogger(CreditCardCheckoutImpl.class);
    private static ZonesProperty properties;
    
    public Boolean getAuthorized(Double orderAmount , CustomerCardInfo customerCardInfo , UserAddress billingAddress,UserAddress shippingAddress){
    	return false;
    }
	 public Boolean getAuthorized(Double orderAmount , CustomerCardInfo customerCardInfo , UserAddress shippingAddress,String guid){
	    	return false;
	    }
	 
	 public ZonesProperty getProperties() {
			return properties;
	}

	public void setProperties(ZonesProperty zonesProperties) {
		properties = zonesProperties;
	}
    
    /*
    public Request GetAchSaleRequest(CheckOut checkout){	
    	Request myRequest= new Request();
//    	logger.info("Iv:" + properties.getIv());
    	myRequest.setIv(properties.getIv());
    	
//    	logger.info("Key:" + properties.getKey());
    	myRequest.setKey(properties.getKey());
		
		myRequest.setClientId(properties.getClient());
//		logger.info("Client:" + properties.getKey());
		
		myRequest.setSourceId(properties.getSource());

		myRequest.setHost(properties.getHost());
		myRequest.setPort(Integer.parseInt(properties.getPort()));
//		myRequest.setEnableEncrypt(true);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranType, "Credit");
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranAction, "Sale");
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Eci, "7");
		
		String amount=String.valueOf(checkout.getOrderAmount().floatValue());
		String decimal=amount.split("\\.")[1];
		if(decimal.length()>2){
			amount=amount.substring(0,amount.length()-decimal.length()+2);
		}else{
			while(decimal.length()< 2){
				amount=amount + "0";
				decimal=amount.split("\\.")[1];
			}
		}
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.Amount, amount);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.CurrencyCode, "840");
		
//		Billing info
		
		String expMonth=checkout.getExpMonth().toString().length()==1?"0"+checkout.getExpMonth().toString():checkout.getExpMonth().toString();
		String expYear=checkout.getExpYear().toString().length()==4?checkout.getExpYear().toString().substring(2,4):checkout.getExpYear().toString();
		String expration=expMonth+expYear;
		
		myRequest.createElement(TRXELEMENT.StorageSafe, TRXELEMENT.Generate, "1");
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Expiration, expration);
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Pan, checkout.getCardNo());
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Cvv, checkout.getSecurityCode().toString());
		
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.FirstName, checkout.getCustomerFirstName());
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.LastName, checkout.getCustomerLastName());
		
		String address = checkout.getBillingAddress1() + (checkout.getBillingAddress2()!=null && !checkout.getBillingAddress2().isEmpty()?checkout.getBillingAddress2():"" );
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Address,address);
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Postal, checkout.getBillingZipCode());
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Industry, "CardNotPresent");
		
		
//		Shipping info.
	
		String shippingAddress = checkout.getMailingAddress1() + (checkout.getMailingAddress2()!=null && !checkout.getMailingAddress2().isEmpty()?checkout.getMailingAddress2():"" );
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.FirstName, checkout.getCustomerFirstName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.LastName, checkout.getCustomerLastName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Address, shippingAddress);
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Postal, checkout.getMailingZipCode());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Region, checkout.getMailingState());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.City, checkout.getMailingCity());

		return myRequest;
    }
    
    
    
    public Boolean getAuthorized(Double orderAmount , CustomerCardInfo customerCardInfo , UserAddress billingAddress,
    		UserAddress shippingAddress){
    	
    	CreditCardCheckoutImpl creditCardCheckoutImpl = new CreditCardCheckoutImpl();

		Request myRequest = creditCardCheckoutImpl.GetAchSaleRequest(orderAmount, customerCardInfo, billingAddress, shippingAddress);
		System.out.println("TRX SERVICES REQUEST XML: ");
		System.out.println(myRequest.getCompleteXML());
		
		logger.info(myRequest.getCompleteXML());
		logger.info("\n");
		try {
			
			ProcessRequest myProcess = new ProcessRequest();
			String encrypted = myProcess.encrypt(myRequest.getKey(), myRequest.getIv(), myRequest.getRequestXML());
			myRequest.setEncryptedRequestXML(encrypted);
			
			System.out.println("TRX SERVICES ENCRIPTED XML =>" + encrypted);
			
			System.out.println("\n");
			logger.info("TRX SERVICES REQUEST XML: ");
			
			//ProcessRequest myProcess = new ProcessRequest();
			String decryptedRes = "";
			try{
				TRXResponse myResponse = myProcess.process(myRequest);
				decryptedRes = myResponse.getResponse();
				String response = myProcess.process(myRequest.getHost(), myRequest.getPort(), myRequest.getCompleteXML());
				decryptedRes = myProcess.decrypt(myRequest.getKey(), myRequest.getIv(), response);
			}catch(Exception e){
				customerCardInfo.setTrxError("TRX Services error while Getting Reponse");
				return false;
			}
			logger.info("RESPONSE FROM TRX SERVICES: ");
			logger.info(decryptedRes);
			logger.info("\n");
			
			System.out.println("RESPONSE FROM TRX SERVICES: ");
			System.out.println(decryptedRes);
			if(decryptedRes.contains("<Result>")){
				String result="<Result>" + decryptedRes.split("<Result>")[1].split("</Result>")[0] + "</Result>";
				System.out.println(result);
				XStream xs = new XStream(new DomDriver());
				Result trxResult= new Result();
				xs.processAnnotations(Result.class);
				xs.registerConverter(new DateConverter());
				
				xs.fromXML(result, trxResult);
				if(trxResult.getResponseCode().equals("00")){
					customerCardInfo.setTransactionId(trxResult.getApprovalCode());
					
					String storage="<StorageSafe>" + decryptedRes.split("<StorageSafe>")[1].split("</StorageSafe>")[0] + "</StorageSafe>";
					StorageSafe trxStorage= new StorageSafe();
					xs.processAnnotations(StorageSafe.class);
					xs.registerConverter(new DateConverter());
					xs.fromXML(storage, trxStorage);
					System.out.println("storage==>"+storage);
					System.out.println(trxStorage.getGuid());
					customerCardInfo.setTrxGuid(trxStorage.getGuid());
					
					String reference="<Reference>" + decryptedRes.split("<Reference>")[1].split("</Reference>")[0] + "</Reference>";
					Reference trxReference= new Reference();
					xs.processAnnotations(Reference.class);
					xs.registerConverter(new DateConverter());
					xs.fromXML(reference, trxReference);
					System.out.println("reference==>"+reference);
					System.out.println(trxReference.getLastFour());
					customerCardInfo.setLastFourDigit(trxReference.getLastFour());
					
					return true;
				}else if(null != trxResult.getDebugInfo()){
					customerCardInfo.setTrxError(trxResult.getDebugInfo().replaceAll(" PAN ", " Credit Card "));
					return false;
				}else{
					customerCardInfo.setTrxError(trxResult.getResponseText());
					return false;
				}
			}else{
				return false;
			}
//			System.out.println("Response:==> " + response);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.toString());
			return false;
		}
		
    }
    
    public Boolean getAuthorized(Double orderAmount , CustomerCardInfo customerCardInfo,
    		UserAddress shippingAddress,String guid){
    	
    	CreditCardCheckoutImpl creditCardCheckoutImpl = new CreditCardCheckoutImpl();

		Request myRequest = creditCardCheckoutImpl.GetAchSaleRequest(orderAmount,  shippingAddress,guid);
		System.out.println("TRX SERVICES REQUEST XML: ");
		System.out.println(myRequest.getCompleteXML());
		System.out.println("\n");
		logger.info("TRX SERVICES REQUEST XML: ");
		logger.info(myRequest.getCompleteXML());
		logger.info("\n");
		try {
			
			ProcessRequest myProcess = new ProcessRequest();
			String decryptedRes = "";
			try{
				TRXResponse myResponse = myProcess.process(myRequest);
				decryptedRes = myResponse.getResponse();
			}catch(Exception e){
				customerCardInfo.setTrxError("TRX Services error while Getting Reponse");
				return false;
			}
			logger.info("RESPONSE FROM TRX SERVICES: ");
			logger.info(decryptedRes);
			logger.info("\n");
			
			System.out.println("RESPONSE FROM TRX SERVICES: ");
			System.out.println(decryptedRes);
			if(decryptedRes.contains("<Result>")){
				String result="<Result>" + decryptedRes.split("<Result>")[1].split("</Result>")[0] + "</Result>";
				System.out.println(result);
				XStream xs = new XStream(new DomDriver());
				Result trxResult= new Result();
				xs.processAnnotations(Result.class);
				xs.registerConverter(new DateConverter());
				
				xs.fromXML(result, trxResult);
				if(trxResult.getResponseCode().equals("00")){
					customerCardInfo.setTransactionId(trxResult.getApprovalCode());
					return true;
				}else if(null != trxResult.getDebugInfo()){
					customerCardInfo.setTrxError(trxResult.getDebugInfo().replaceAll(" PAN ", " Credit Card "));
					return false;
				}else{
					customerCardInfo.setTrxError(trxResult.getResponseText());
					return false;
				}
			}else{
				return false;
			}
//			System.out.println("Response:==> " + response);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.toString());
			return false;
		}
		
    }
    
    
    
    public Request GetAchSaleRequest(Double orderAmount , CustomerCardInfo cardInfo , UserAddress billingAddress,
    		UserAddress shippingAddress){	
    	Request myRequest= new Request();
//    	logger.info("Iv:" + properties.getIv());
    	myRequest.setIv(properties.getIv());
    	
//    	logger.info("Key:" + properties.getKey());
    	myRequest.setKey(properties.getKey());
		
		myRequest.setClientId(properties.getClient());
//		logger.info("Client:" + properties.getKey());
		
		myRequest.setSourceId(properties.getSource());

		myRequest.setHost(properties.getHost());
		myRequest.setPort(Integer.parseInt(properties.getPort()));
//		myRequest.setEnableEncrypt(true);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranType, "Credit");
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranAction, "Sale");
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Eci, "7");
		
		String amount=String.valueOf(orderAmount.floatValue());
		String decimal=amount.split("\\.")[1];
		if(decimal.length()>2){
			amount=amount.substring(0,amount.length()-decimal.length()+2);
		}else{
			while(decimal.length()< 2){
				amount=amount + "0";
				decimal=amount.split("\\.")[1];
			}
		}
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.Amount, amount);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.CurrencyCode, "840");
		
//		Billing info
		
		String expMonth=cardInfo.getCardExpiryMonth().toString().length()==1?"0"+cardInfo.getCardExpiryMonth().toString():cardInfo.getCardExpiryMonth().toString();
		String expYear=cardInfo.getCardExpiryYear().toString().length()==4?cardInfo.getCardExpiryYear().toString().substring(2,4):cardInfo.getCardExpiryYear().toString();
		String expration=expMonth+expYear;
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Expiration, expration);
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Pan, cardInfo.getCardNoTemp());
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Cvv, cardInfo.getCardCvvNo().toString());
		
		cardInfo.setFirstName(billingAddress.getFirstName());
		cardInfo.setLastName(billingAddress.getLastName());
		//cardInfo.setLastFourDigit(lastFourDigit);
		cardInfo.setMonth(Integer.parseInt(expMonth.trim()));
		cardInfo.setYear(Integer.parseInt(expYear.trim()));	
		cardInfo.setCardType(cardInfo.getCardType());
		
		
		myRequest.createElement(TRXELEMENT.StorageSafe, TRXELEMENT.Generate, "1");
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.FirstName, billingAddress.getFirstName());
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.LastName, billingAddress.getLastName());
		
		String address = billingAddress.getAddressLine1() + (billingAddress.getAddressLine2()!=null && !billingAddress.getAddressLine2().isEmpty()?billingAddress.getAddressLine2():"" );
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Address,address);
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Postal, billingAddress.getZipCode());
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Industry, "CardNotPresent");
//		Shipping info.
	
		String shippingAddressStr = shippingAddress.getAddressLine1() + (shippingAddress.getAddressLine2()!=null && !shippingAddress.getAddressLine2().isEmpty()?shippingAddress.getAddressLine2():"" );
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.FirstName, shippingAddress.getFirstName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.LastName, shippingAddress.getLastName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Address, shippingAddressStr);
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Postal, shippingAddress.getZipCode());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Region, shippingAddress.getState().getShortDesc());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.City, shippingAddress.getCity());

		return myRequest;
    }
    
    
    public Request GetAchSaleRequest(Double orderAmount,UserAddress shippingAddress,String guid){	
    	Request myRequest= new Request();
    	myRequest.setIv(properties.getIv());
    	myRequest.setKey(properties.getKey());
		myRequest.setClientId(properties.getClient());
		myRequest.setSourceId(properties.getSource());
		myRequest.setHost(properties.getHost());
		myRequest.setPort(Integer.parseInt(properties.getPort()));
//		myRequest.setEnableEncrypt(true);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranType, "Credit");
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranAction, "Sale");
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Eci, "7");
		
		String amount=String.valueOf(orderAmount.floatValue());
		String decimal=amount.split("\\.")[1];
		if(decimal.length()>2){
			amount=amount.substring(0,amount.length()-decimal.length()+2);
		}else{
			while(decimal.length()< 2){
				amount=amount + "0";
				decimal=amount.split("\\.")[1];
			}
		}
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.Amount, amount);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.CurrencyCode, "840");
		myRequest.createElement(TRXELEMENT.StorageSafe, TRXELEMENT.Guid, guid);
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Industry, "CardNotPresent");
		
//		Shipping info.	
		String shippingAddressStr = shippingAddress.getAddressLine1() + (shippingAddress.getAddressLine2()!=null && !shippingAddress.getAddressLine2().isEmpty()?shippingAddress.getAddressLine2():"" );
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.FirstName, shippingAddress.getFirstName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.LastName, shippingAddress.getLastName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Address, shippingAddressStr);
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Postal, shippingAddress.getZipCode());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Region, shippingAddress.getState().getShortDesc());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.City, shippingAddress.getCity());
		return myRequest;
    }
    
    public static void main(String[] args) {
    	
    	callTrxServices();
    	getCreditCardInfoByGuid();
    	
    }
    
    public Request GetAchSaleRequest(String guid){	
    	Request myRequest= new Request();
//    	logger.info("Iv:" + properties.getIv());
    	myRequest.setIv(properties.getIv());
    	
//    	logger.info("Key:" + properties.getKey());
    	myRequest.setKey(properties.getKey());
		
		myRequest.setClientId(properties.getClient());
//		logger.info("Client:" + properties.getKey());
		
		myRequest.setSourceId(properties.getSource());

		myRequest.setHost(properties.getHost());
		myRequest.setPort(Integer.parseInt(properties.getPort()));
//		myRequest.setEnableEncrypt(true);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranType, "StorageSafe");
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranAction, "Lookup");
		myRequest.createElement(TRXELEMENT.StorageSafe, TRXELEMENT.Guid, guid);
		return myRequest;
    }
    
    
    public static void getCreditCardInfoByGuid() {
    	
    	properties = new ZonesProperty();
		properties.setIv("FDAE442E1014DCBA1EAA5FF70D06959E");
		properties.setKey("E6E5A58772BCBFE4FB7466A92ADB4189FF9214AB755236EA89F90863E3DFD714");
		properties.setSource("1");
		properties.setClient("201");
		properties.setHost("api.trxservices.net");
		properties.setPort("443");
		
		
		
    	CreditCardCheckoutImpl creditCardCheckoutImpl = new CreditCardCheckoutImpl();
    	Request myRequest = creditCardCheckoutImpl.GetAchSaleRequest("5VV5ZEZJG6HWF01");
    	System.out.println(myRequest.getCompleteXML());
		System.out.println("\n");
		try {
			ProcessRequest myProcess = new ProcessRequest();
			String encrypted = myProcess.encrypt(myRequest.getKey(), myRequest.getIv(), myRequest.getRequestXML());
			myRequest.setEncryptedRequestXML(encrypted);
			System.out.println("encrypted request xml =>" + myRequest.getCompleteXML());

			String response = myProcess.process(myRequest.getHost(), myRequest.getPort(), myRequest.getCompleteXML());
			String decryptedRes = myProcess.decrypt(myRequest.getKey(), myRequest.getIv(), response);
			System.out.println("Decrypted => " + decryptedRes);
			if(decryptedRes.contains("<Result>")){
				String result="<Result>" + decryptedRes.split("<Result>")[1].split("</Result>")[0] + "</Result>";
				System.out.println(result);
				XStream xs = new XStream(new DomDriver());
				Result trxResult= new Result();
				xs.processAnnotations(Result.class);
				xs.registerConverter(new DateConverter());
				
				xs.fromXML(result, trxResult);
				if(trxResult.getResponseCode().equals("00")){
//					return true;
				}else if(null != trxResult.getDebugInfo()){
				}else{
				}
			}else{
//				return false;
			}
//			System.out.println("Response:==> " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
    public static void callTrxServices() {
    	CreditCardCheckoutImpl creditCardCheckoutImpl = new CreditCardCheckoutImpl();
    	CheckOut checkOut=new CheckOut();
    	
		checkOut.setCustomerFirstName("Amit");
		checkOut.setCustomerId(45);
		checkOut.setCustomerLastName("Raut");
		checkOut.setBillingAddress1("139 Jackson Ave");
		checkOut.setBillingAddress2("");
		checkOut.setBillingZipCode("08859");
		checkOut.setMailingAddress1("139 Jackson Ave");
		checkOut.setMailingAddress2("");
		checkOut.setMailingCity("Parlin");
		checkOut.setMailingState("NJ");
		checkOut.setMailingZipCode("08859");
		checkOut.setOrderAmount(1.00);
		checkOut.setNameOnCard("Amit S Raut");
		checkOut.setCardNo("4311963101259997");
		checkOut.setCardType("VISA");
		checkOut.setExpMonth(04);
		checkOut.setExpYear(2017);
		checkOut.setSecurityCode("049");
		
		
		<com.admitone.zones.transaction.api.iv></com.admitone.zones.transaction.api.iv>
		<com.admitone.zones.transaction.api.key></com.admitone.zones.transaction.api.key>
        <com.admitone.zones.transaction.api.client>2045</com.admitone.zones.transaction.api.client>
		<com.admitone.zones.transaction.api.source>1</com.admitone.zones.transaction.api.source>
		<com.admitone.zones.transaction.api.host>api.trxservices.com</com.admitone.zones.transaction.api.host>
		<com.admitone.zones.transaction.api.port>443</com.admitone.zones.transaction.api.port>
		

		
		properties = new ZonesProperty();
		properties.setIv("FBD9D0FFD9E0F9079ED376C2270DD9FB");
		properties.setKey("899D8A850E5B6BB1BF3B805B53874305BD4DE7DB832B52A23B49BF271D250BB4");
		properties.setSource("1");
		properties.setClient("2045");
		properties.setHost("api.trxservices.com");
		properties.setPort("443");
		
		<com.admitone.zones.transaction.api.iv>FDAE442E1014DCBA1EAA5FF70D06959E</com.admitone.zones.transaction.api.iv>
		<com.admitone.zones.transaction.api.key>E6E5A58772BCBFE4FB7466A92ADB4189FF9214AB755236EA89F90863E3DFD714</com.admitone.zones.transaction.api.key>
		<com.admitone.zones.transaction.api.client>201</com.admitone.zones.transaction.api.client>
		<com.admitone.zones.transaction.api.source>1</com.admitone.zones.transaction.api.source>
		<com.admitone.zones.transaction.api.host>api.trxservices.net</com.admitone.zones.transaction.api.host>
		<com.admitone.zones.transaction.api.port>443</com.admitone.zones.transaction.api.port>
		

		properties = new ZonesProperty();
		properties.setIv("FDAE442E1014DCBA1EAA5FF70D06959E");
		properties.setKey("E6E5A58772BCBFE4FB7466A92ADB4189FF9214AB755236EA89F90863E3DFD714");
		properties.setSource("1");
		properties.setClient("201");
		properties.setHost("api.trxservices.net");
		properties.setPort("443");
		
		
		Request myRequest = creditCardCheckoutImpl.GetAchSaleRequest(checkOut,"60ZREQG0G6HXA0A");
			
			//creditCardCheckoutImpl.GetAchSaleRequest(checkOut);

		System.out.println(myRequest.getCompleteXML());
		System.out.println("\n");

		try {
			ProcessRequest myProcess = new ProcessRequest();
			String encrypted = myProcess.encrypt(myRequest.getKey(), myRequest.getIv(), myRequest.getRequestXML());
			myRequest.setEncryptedRequestXML(encrypted);
			System.out.println("encrypted request xml =>" + myRequest.getCompleteXML());

			String response = myProcess.process(myRequest.getHost(), myRequest.getPort(), myRequest.getCompleteXML());
			String decryptedRes = myProcess.decrypt(myRequest.getKey(), myRequest.getIv(), response);
			System.out.println("Decrypted => " + decryptedRes);
			if(decryptedRes.contains("<Result>")){
				String result="<Result>" + decryptedRes.split("<Result>")[1].split("</Result>")[0] + "</Result>";
				System.out.println(result);
				XStream xs = new XStream(new DomDriver());
				Result trxResult= new Result();
				xs.processAnnotations(Result.class);
				xs.registerConverter(new DateConverter());
				
				xs.fromXML(result, trxResult);
				if(trxResult.getResponseCode().equals("00")){
					checkOut.setTransactionId(trxResult.getApprovalCode());
					
					String storage="<StorageSafe>" + decryptedRes.split("<StorageSafe>")[1].split("</StorageSafe>")[0] + "</StorageSafe>";
					StorageSafe trxStorage= new StorageSafe();
					xs.processAnnotations(StorageSafe.class);
					xs.registerConverter(new DateConverter());
					xs.fromXML(storage, trxStorage);
					System.out.println("storage==>"+storage);
					System.out.println(trxStorage.getGuid());
					
					String reference="<Reference>" + decryptedRes.split("<Reference>")[1].split("</Reference>")[0] + "</Reference>";
					Reference trxReference= new Reference();
					xs.processAnnotations(Reference.class);
					xs.registerConverter(new DateConverter());
					xs.fromXML(reference, trxReference);
					System.out.println("reference==>"+reference);
					System.out.println(trxReference.getLastFour());
					
					
					
//					return true;
				}else if(null != trxResult.getDebugInfo()){
					checkOut.setTrxError(trxResult.getDebugInfo().replaceAll(" PAN ", " Credit Card "));
				}else{
					checkOut.setTrxError(trxResult.getResponseText());
				}
			}else{
//				return false;
			}
//			System.out.println("Response:==> " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

   

	public Request GetAchSaleRequest(CheckOut checkout, String guid){	
    	Request myRequest= new Request();
//    	logger.info("Iv:" + properties.getIv());
    	myRequest.setIv(properties.getIv());
    	
//    	logger.info("Key:" + properties.getKey());
    	myRequest.setKey(properties.getKey());
		
		myRequest.setClientId(properties.getClient());
//		logger.info("Client:" + properties.getKey());
		
		myRequest.setSourceId(properties.getSource());

		myRequest.setHost(properties.getHost());
		myRequest.setPort(Integer.parseInt(properties.getPort()));
//		myRequest.setEnableEncrypt(true);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranType, "Credit");
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.TranAction, "Sale");
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Eci, "7");
		
		String amount=String.valueOf(checkout.getOrderAmount().floatValue());
		String decimal=amount.split("\\.")[1];
		if(decimal.length()>2){
			amount=amount.substring(0,amount.length()-decimal.length()+2);
		}else{
			while(decimal.length()< 2){
				amount=amount + "0";
				decimal=amount.split("\\.")[1];
			}
		}
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.Amount, amount);
		myRequest.createElement(TRXELEMENT.Detail, TRXELEMENT.CurrencyCode, "840");
		
//		Billing info
		
		String expMonth=checkout.getExpMonth().toString().length()==1?"0"+checkout.getExpMonth().toString():checkout.getExpMonth().toString();
		String expYear=checkout.getExpYear().toString().length()==4?checkout.getExpYear().toString().substring(2,4):checkout.getExpYear().toString();
		String expration=expMonth+expYear;
		
		///myRequest.createElement(TRXELEMENT.StorageSafe, TRXELEMENT.Guid, guid);
		myRequest.createElement(TRXELEMENT.StorageSafe, TRXELEMENT.Generate, "1");
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Expiration, expration);
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Pan, checkout.getCardNo());
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Cvv, checkout.getSecurityCode().toString());
		
		
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.FirstName, checkout.getCustomerFirstName());
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.LastName, checkout.getCustomerLastName());
		
		String address = checkout.getBillingAddress1() + (checkout.getBillingAddress2()!=null && !checkout.getBillingAddress2().isEmpty()?checkout.getBillingAddress2():"" );
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Address,address);
		
		myRequest.createElement(TRXELEMENT.Account, TRXELEMENT.Postal, checkout.getBillingZipCode());
		myRequest.createElement(TRXELEMENT.IndustryData, TRXELEMENT.Industry, "CardNotPresent");
		
		
		
		
//		Shipping info.
	
		String shippingAddress = checkout.getMailingAddress1() + (checkout.getMailingAddress2()!=null && !checkout.getMailingAddress2().isEmpty()?checkout.getMailingAddress2():"" );
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.FirstName, checkout.getCustomerFirstName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.LastName, checkout.getCustomerLastName());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Address, shippingAddress);
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Postal, checkout.getMailingZipCode());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.Region, checkout.getMailingState());
		myRequest.createElement(TRXELEMENT.Shipping, TRXELEMENT.City, checkout.getMailingCity());

		return myRequest;
    }*/
	
} 
