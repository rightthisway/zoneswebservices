package com.zonesws.webservices.util.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paypal.api.openidconnect.CreateFromAuthorizationCodeParameters;
import com.paypal.api.openidconnect.Tokeninfo;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.FuturePayment;
import com.paypal.api.payments.Payee;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.PayerInfo;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.Sale;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.PaypalApi;
import com.zonesws.webservices.data.PaypalTransactionDetail;
import com.zonesws.webservices.enums.PartialPaymentMethod;
import com.zonesws.webservices.enums.PaymentMethod;
import com.zonesws.webservices.util.service.PayPalCheckout;
import com.zonesws.webservices.utils.ZonesProperty;
 
public class PayPalCheckoutImpl implements PayPalCheckout{
    
    private static final Logger logger = LoggerFactory.getLogger(PayPalCheckoutImpl.class);
    private static ZonesProperty properties;
    
    
    public ZonesProperty getProperties() {
		return properties;
	}

	public void setProperties(ZonesProperty zonesProperties) {
		properties = zonesProperties;
	}
	
	public void updatePapypalInfo (){
		
		Collection<CustomerOrder> orders = DAORegistry.getCustomerOrderDAO().getAll();
		PaypalTransactionDetail paypalTransactionDetail = null;
		int i=1,size = orders.size();
		for (CustomerOrder customerOrder : orders) {
			try{
				
				if(customerOrder.getPrimaryPaymentMethod().equals(PaymentMethod.PAYPAL)){
					
					paypalTransactionDetail = new PaypalTransactionDetail();
					paypalTransactionDetail.setPaymentId(customerOrder.getPrimaryTransactionId());
					paypalTransactionDetail = getPaymentInformation(paypalTransactionDetail);
					DAORegistry.getPaypalTransactionDetailDAO().saveOrUpdate(paypalTransactionDetail);
				} else if(customerOrder.getSecondaryPaymentMethod().equals(PartialPaymentMethod.PAYPAL)){
					paypalTransactionDetail = new PaypalTransactionDetail();
					paypalTransactionDetail.setPaymentId(customerOrder.getSecondaryTransactionId());
					paypalTransactionDetail = getPaymentInformation(paypalTransactionDetail);
					DAORegistry.getPaypalTransactionDetailDAO().saveOrUpdate(paypalTransactionDetail);
				}
				System.out.println(size+"========="+i);
				i++;
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
	}
	
	public PaypalTransactionDetail getPaymentInformation(PaypalTransactionDetail paypalTransactionDetail) throws PayPalRESTException {
		
		// Replace these values with your clientId and secret. You can use these to get started right now.
    	String clientId = "";
    	String clientSecret = "";
		List<PaypalApi> paypalApiList = DAORegistry.getQueryManagerDAO().getPaypalCredentialByType();
		for (PaypalApi paypalApi : paypalApiList) {
			if(properties.getPaypalEnvironment().equals("live") && paypalApi.getCredentialType().equals("live")){
				clientId = paypalApi.getClientId();
				clientSecret = paypalApi.getSecretKey();
			}else if(properties.getPaypalEnvironment().equals("sandbox") && paypalApi.getCredentialType().equals("Sandbox")){
				clientId = paypalApi.getClientId();
				clientSecret = paypalApi.getSecretKey();
			}
		}
		
		try{
			APIContext apiContext = new APIContext(clientId, clientSecret, properties.getPaypalEnvironment());

			// Retrieve the payment object by calling the
			// static `get` method
			// on the Payment class by passing a valid
			// AccessToken and Payment ID
			Payment payment = Payment.get(apiContext,paypalTransactionDetail.getPaymentId());
			if(null != payment){
				System.out.println(payment.toString());
				PayerInfo payerInfo = payment.getPayer().getPayerInfo();
				Sale sale = payment.getTransactions().get(0).getRelatedResources().get(0).getSale();
				paypalTransactionDetail.setCartId(payment.getCart());
				paypalTransactionDetail.setPayerEmail(payerInfo.getEmail());
				paypalTransactionDetail.setPayerFirstName(payerInfo.getFirstName());
				paypalTransactionDetail.setPayerId(payerInfo.getPayerId());
				paypalTransactionDetail.setPayerLastName(payerInfo.getLastName());
				paypalTransactionDetail.setPayerPhone(payerInfo.getPhone());
				paypalTransactionDetail.setSalesAmount(Double.valueOf(sale.getAmount().getTotal()));
				paypalTransactionDetail.setSaleTransactionId(sale.getId());
				paypalTransactionDetail.setTransactionDate(sale.getCreateTime());
				paypalTransactionDetail.setTransactionFees(Double.valueOf(sale.getTransactionFee().getValue().toString()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return paypalTransactionDetail;
	}
	
	public Payment doPaypalFuturePayment(String correlationId, String authorizationCode,String trxAmount,
			 String description,Customer customer) throws PayPalRESTException, FileNotFoundException, IOException { 
		 	
		  List<PaypalApi> paypalApis= DAORegistry.getQueryManagerDAO().getPaypalCredentialByType();
		  PaypalApi paypalApi = paypalApis.get(0);
			 
		  Payer payer = new Payer(); 
		  payer.setPaymentMethod("paypal"); 
		  Payee payee = new Payee();
		  payee.setEmail(customer.getEmail());
		  payee.setFirstName(customer.getCustomerName());
		  payee.setLastName(customer.getLastName());
		  //payee.setPhone(customer.getPhone());
		  
		  
		  Amount amount = new Amount(); 
		  amount.setTotal(trxAmount); 
		  amount.setCurrency("USD"); 
		  Transaction transaction = new Transaction(); 
		  transaction.setAmount(amount); 
		  transaction.setDescription(description); 
		  List<Transaction> transactions = new ArrayList<Transaction>(); 
		  transactions.add(transaction); 
		   
		  FuturePayment futurePayment = new FuturePayment(); 
		  futurePayment.setIntent("authorize"); 
		  futurePayment.setPayer(payer); 
		  futurePayment.setPayee(payee);
		  futurePayment.setTransactions(transactions); 
		   
		  Tokeninfo tokeninfo = null; 
		   System.out.println("creating future payment with auth code: " + authorizationCode); 
		    
		   //ClientCredentials credentials = futurePayment.getClientCredential(); 
		   CreateFromAuthorizationCodeParameters params = new CreateFromAuthorizationCodeParameters(); 
		   params.setClientID(paypalApi.getClientId()); 
		   params.setClientSecret(paypalApi.getSecretKey()); 
		   params.setCode(authorizationCode); 
	
		   Map<String, String> configurationMap = new HashMap<String, String>(); 
		   configurationMap.put("mode", properties.getPaypalEnvironment()); 
		   APIContext apiContext = new APIContext(paypalApi.getClientId(), paypalApi.getSecretKey(),properties.getPaypalEnvironment()); 
		   apiContext.setConfigurationMap(configurationMap); 
		   tokeninfo = Tokeninfo.createFromAuthorizationCodeForFpp(apiContext, params); 
		   tokeninfo.setAccessToken(tokeninfo.getTokenType() + " " + tokeninfo.getAccessToken()); 
		 
		   System.out.println("Generated access token from auth code: " + tokeninfo.getAccessToken()); 
		 
		  Payment createdPayment = futurePayment.create(tokeninfo.getAccessToken(), correlationId); 
		  if (createdPayment.getIntent().equals("authorize")) { 
		   System.out.println("payment authorized"); 
		   System.out.println("Payment ID=" + createdPayment.getId()); 
		   System.out.println("Authorization ID=" + createdPayment.getTransactions().get(0).getRelatedResources().get(0).getAuthorization().getId()); 
		  } 
		  return createdPayment; 
		} 
    
    
} 
