package com.zonesws.webservices.utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.Status;
@XStreamAlias("AlreadySoldTicketGroup")
public class AlreadySoldTicketGroup implements Serializable{
		private Integer ticketGroupId;
		private Integer holdTicketId;
		private String message;
		private Status status;

		

		public Integer getTicketGroupId() {
			return ticketGroupId;
		}

		public void setTicketGroupId(Integer ticketGroupId) {
			this.ticketGroupId = ticketGroupId;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public Integer getHoldTicketId() {
			return holdTicketId;
		}

		public void setHoldTicketId(Integer holdTicketId) {
			this.holdTicketId = holdTicketId;
		}
		
	}