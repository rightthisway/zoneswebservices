package com.zonesws.webservices.utils;

import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.data.GrandChildCategory;





public class CategorySearchMechanism  {
	
	public final static String[] grandChilds = {"AFL","AHL","NSL","Badminton","CFL","CHL","College","ECHL","Handball","Ice","IHL","IIHF",
		"MISL","Minors","MLL","Nascar","NWSL","NFL","NLL","PCL","ABA","MLB","MLS","NBA","NHL","PGA",
		"WNBA","LPGA","UFC","UFL","WHL","LPGA","WWE","XSL"};
	public final static String[] childs = {"BALLET","BASEBALL","BASKETBALL","BLUEGRASS","BOXING","BROADWAY","CIRCUS","CLASSICAL","COMEDY",
		"CRICKET","DANCE","FESTIVAL","FILM","FOOTBALL","GOLF","GYMNASTICS","HOCKEY","HOLIDAY","LACROSSE","LAS VEGAS","LATIN","MAGIC SHOWS","NEW AGE",
		"OLYMPICS","OPERA","OTHER","RACING","RELIGIOUS","RODEO","RUGBY","SKATING","SOCCER","SOFTBALL","TENNIS","VOLLEYBALL","WEST END",
		"WORLD","WRESTLING"};
	
	
	public static Boolean skipThisArtist(Integer artistId,String artistName,String searchKey, ChildCategory childCategory,
			GrandChildCategory grandChildCategory){
		
		for (int i= 2016;i<=1980;i--) {
			if(artistName.contains(String.valueOf(i))){
				return false;
			}
		}
		
		//stored procedure will return only artists which has events 
		/*for (String grandChildKey : grandChilds) {
			if(artistName.toUpperCase().contains(grandChildKey) ){
				return EventArtistUtil.isArtistHasActiveEventsNew(artistId);
			}
		}*/
		return true;
	}
	
	
}
