package com.zonesws.webservices.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CityAPI")
public class CityAPI {
	private Integer id;
	private String city;
	private String state;
	private String country;
	private String zip;
	private String lon;
	private String lat;
	
	private String stateFullName;
	private String countryFullName;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getStateFullName() {
		return stateFullName;
	}
	public void setStateFullName(String stateFullName) {
		this.stateFullName = stateFullName;
	}
	public String getCountryFullName() {
		return countryFullName;
	}
	public void setCountryFullName(String countryFullName) {
		this.countryFullName = countryFullName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
}

