package com.zonesws.webservices.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import com.zonesws.webservices.data.UserAddress;


public class CityUtil {
	/*private static StubDbConnection stubConnection = new StubDbConnection();
	
	private static ZoneTicketsDbConnection ticketsDbConnection = new ZoneTicketsDbConnection();*/
	
	/*public static void main(String[] args) throws Exception {
		
        String apiUrl = "https://api.meetup.com/cities.json/?state=ny&key=6d435657a1a361b6a803933104d621c";
        List<String> zipCodes = new ArrayList<String>();
        RestTemplate restTemplate = new RestTemplate();
        String jsonData = restTemplate.getForObject(apiUrl, String.class);
        System.out.println(jsonData);
        JSONObject jsonObject1 = new JSONObject(jsonData);
        JSONArray  jsonArray = jsonObject1.getJSONArray("results");
        for(int i=0 ; i < jsonArray.length() ; i++){
	        JSONObject jsonObject = jsonArray.getJSONObject(i);
	        System.out.println(jsonObject.get("zip"));
	        System.out.println(jsonObject.get("city"));
	        System.out.println(jsonObject.get("state"));
	        System.out.println(jsonObject.get("country"));
	        System.out.println(jsonObject.get("lon"));
	        System.out.println(jsonObject.get("lat"));

        }
        
	}*/
	
 /*public static void main(String[] args) throws Exception {
		
	 
	 	
	 List<CityAPI> finalCityList = new ArrayList<CityAPI>();
	 	List<CityAPI> states = getAllStates();
	    int count=1;
	 	for (CityAPI state : states) {
	 		
	 		try{
	 			String apiUrl = "https://api.meetup.com/cities.json/?state="+state.getState()+"&key=4e1669201f2065555338377752713269";
		        RestTemplate restTemplate = new RestTemplate();
		        String jsonData = restTemplate.getForObject(apiUrl, String.class);
		        JSONObject jsonObject1 = new JSONObject(jsonData);
		        JSONArray  jsonArray = jsonObject1.getJSONArray("results");
		        CityAPI city = null;
		        for(int i=0 ; i < jsonArray.length() ; i++){
		        	JSONObject jsonObject = jsonArray.getJSONObject(i);
		        	city = new CityAPI();
		        	
			        
		        	city.setCity((String)jsonObject.get("city"));
		        	city.setState((String)jsonObject.get("state"));
		        	city.setStateFullName(state.getStateFullName());
		        	city.setCountry(((String)jsonObject.get("country")).toUpperCase());
		        	city.setZip((String)jsonObject.get("zip"));
		        	city.setLon((String)jsonObject.get("lon"));
		        	city.setLat((String)jsonObject.get("lat"));
		        	city.setCountryFullName("Canada");
		        	finalCityList.add(city);
				 		saveCity(city);
		        }
	 		}catch(Exception e){
	 			e.printStackTrace();
	 			System.out.println(state.getId()+"======"+state.getState()+"======"+state.getStateFullName());
	 			System.out.println(count+"<=========Complteed===>"+finalCityList.size());
	 			break;
	 		}
	 		System.out.println(count+"<=========Complteed===>"+finalCityList.size());
	 		count++;
	 		
	 		
		}
	 	
	 	
        
	}*/
	
	
	/*public static List<CityAPI> getAllStates() throws Exception {
		Connection connection = stubConnection.getStubConnection(); 
		String sql = "select state_id,state_name,state_short_name from system_state where system_country_id=38 order by state_id";
		ResultSet resultSet =null;
		List<CityAPI> cityAPIList = new ArrayList<CityAPI>();
		try {
			
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				CityAPI cityAPI = new CityAPI();
				cityAPI.setId(resultSet.getInt("state_id"));
				cityAPI.setState(resultSet.getString("state_short_name"));
				cityAPI.setStateFullName(resultSet.getString("state_name"));
				cityAPIList.add(cityAPI);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return cityAPIList;
	}*/
	
	/*
	public static void saveCity(CityAPI city) throws Exception{
	
		Connection connection = ticketsDbConnection.getZoneTicketsConnection(); 
		String sql = "INSERT INTO city_auto_search_temp(city,zip_code,state,state_full_name,country,country_full_name,longitude,latitude ) " +
				"VALUES('"+city.getCity().replaceAll("'", "")+"','"+city.getZip()+"','"+city.getState()+"','"+city.getStateFullName()+"'," +
						"'"+city.getCountry()+"'," +
						"'"+city.getCountryFullName()+"','"+city.getLon()+"','"+city.getLat()+"')";
		ResultSet rs= null;
		Statement insertStatement =null;
		
		try {
			connection.setAutoCommit(false);
			insertStatement = connection.createStatement();
			insertStatement.executeUpdate(sql);
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(insertStatement!=null){
				insertStatement.close();
			}
		}
	}
	*/
	public static Comparator<UserAddress> sortShippingAddressByUpdatedTime = new Comparator<UserAddress>() {

		public int compare(UserAddress shippAddress1, UserAddress shippAddress2) {
			
			int cmp =shippAddress2.getLastUpdated().compareTo(
					shippAddress1.getLastUpdated());
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			return shippAddress1.getId().compareTo(shippAddress2.getId());
	    }};
	
}
