package com.zonesws.webservices.utils;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CrownJewelCategoryTicket;
import com.zonesws.webservices.data.Event;


public class CrownJewelTicketUtil {
	
	public static List<CrownJewelCategoryTicket> getAllActiveCrownjewelTickets() throws Exception{
		
		return  DAORegistry.getCrownJewelCategoryTicketDAO().getAllActiveCrownjewelTickets(PaginationUtil.excludeEventDays);
	}
	
	public static Set<Integer> getTicketAvailableLeagueIds(Set<Integer> leagueIds){
		try{
			leagueIds = DAORegistry.getCrownJewelCategoryTicketDAO().getAllActiveLeagueIds();
		}catch(Exception e){
			e.printStackTrace();
		}
		return leagueIds;
	}
	
	public static Set<String> getTicketAvailableTeamIds(Set<String> teamIds){
		try{
			teamIds = DAORegistry.getCrownJewelCategoryTicketDAO().getAllActiveTeamIds();
		}catch(Exception e){
			e.printStackTrace();
		}
		return teamIds;
	}
	
	
	public static Set<String> getTicketAvailableCities(Set<String> leagueCities){
		try{
			leagueCities = DAORegistry.getCrownJewelCategoryTicketDAO().getAllActiveLeaguesTeamsCities();
		}catch(Exception e){
			e.printStackTrace();
		}
		return leagueCities;
	}
	
	public static Set<String> getTicketAvailableEvents(Set<String> leagueCityEvents){
		try{
			leagueCityEvents = DAORegistry.getCrownJewelCategoryTicketDAO().getAllActiveEvents();
		}catch(Exception e){
			e.printStackTrace();
		}
		return leagueCityEvents;
	}
	
	 public static Comparator<Event> fantasyEventComparatorFordate = new Comparator<Event>() {

			public int compare(Event event1, Event event2) {
					int cmp = event1.getEventDateTime().compareTo(
							event2.getEventDateTime());
					if (cmp < 0) {
						return -1;
					}
					if (cmp > 0) {
						return 1;
					}
					return event1.getEventId().compareTo(event2.getEventId());
			    }};
			    
			    
     public static Comparator<Event> fantasyEventComparatorFordateNew = new Comparator<Event>() {

		public int compare(Event event1, Event event2) {
				int cmp = event1.getTempEventDate().compareTo(
						event2.getTempEventDate());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				return event1.getEventId().compareTo(event2.getEventId());
		    }};
			    
	
	public static Set<String> checkCJEEventAreThereOrNot(Set<String> cjeEventKeys){
		try{
			/*List<CrownJewelCategoryTicket> categoryTickets = getAllActiveCrownjewelTickets();
			for (CrownJewelCategoryTicket tix : categoryTickets) {
				cjeEventKeys.add(tix.getLeagueId()+"_"+tix.getTeamId()+"_"+tix.getEventId());
			}*/
			cjeEventKeys = DAORegistry.getCrownJewelCategoryTicketDAO().getAllEventsByLeagueAndTeams();
		}catch(Exception e){
			e.printStackTrace();
		}
		return cjeEventKeys;
	}
	
	
		
}
