package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.FavouriteEvents;


/**
 * Customer Favorite Event Util to get the favorite event details by customer
 * @author kulaganathan
 *
 */
public class CustomerFavoriteEventUtil extends QuartzJobBean implements StatefulJob {
	
	public static Map<Integer, Map<Integer, Boolean>> favEventMapByCustIdEventId = new ConcurrentHashMap<Integer, Map<Integer, Boolean>>();
	public static Map<Integer, List<Integer>> favEventIdsMap = new ConcurrentHashMap<Integer, List<Integer>>();
	public static Map<Integer, Map<Integer, FavouriteEvents>> favEventObjMapByCustIdArtistId = new ConcurrentHashMap<Integer, Map<Integer, FavouriteEvents>>();
	public static Map<Integer, List<Integer>> unFavoriteEventIdsMap = new ConcurrentHashMap<Integer, List<Integer>>();
	
	public void init(){
	 try{
		 
		Long startTime = System.currentTimeMillis();
		Collection<FavouriteEvents> favoriteEvents = DAORegistry.getFavouriteEventsDAO().getAll();
		
		if(null ==favoriteEvents || favoriteEvents.isEmpty()){
			return;
		}
		
		for (FavouriteEvents favObj : favoriteEvents) {
			
			if(favObj.getStatus().equals("DELETED")){
				List<Integer> unFavEventIds =  unFavoriteEventIdsMap.get(favObj.getCustomerId());
				if(null != unFavEventIds && !unFavEventIds.isEmpty()){
					unFavoriteEventIdsMap.get(favObj.getCustomerId()).add(favObj.getEventId());
				}else{
					unFavEventIds = new ArrayList<Integer>();
					unFavEventIds.add(favObj.getEventId());
					unFavoriteEventIdsMap.put(favObj.getCustomerId(), unFavEventIds);
				}
				continue;
			}
			
			
			
			List<Integer> eventIds =  favEventIdsMap.get(favObj.getCustomerId());
			if(null != eventIds && !eventIds.isEmpty()){
				favEventIdsMap.get(favObj.getCustomerId()).add(favObj.getEventId());
			}else{
				eventIds = new ArrayList<Integer>();
				eventIds.add(favObj.getEventId());
				favEventIdsMap.put(favObj.getCustomerId(), eventIds);
			}
			
			
			
			Map<Integer, Boolean> tempMap = favEventMapByCustIdEventId.get(favObj.getCustomerId());
			if(null != tempMap && !tempMap.isEmpty()){
				favEventMapByCustIdEventId.get(favObj.getCustomerId()).put(favObj.getEventId(), Boolean.TRUE);
			}else{
				tempMap = new HashMap<Integer, Boolean>();
				tempMap.put(favObj.getEventId(), Boolean.TRUE);
				favEventMapByCustIdEventId.put(favObj.getCustomerId(), tempMap);
			}
			
			
			Map<Integer, FavouriteEvents> objMap = favEventObjMapByCustIdArtistId.get(favObj.getCustomerId());
			if(null != objMap && !objMap.isEmpty()){
				favEventObjMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getEventId(), favObj);
			}else{
				objMap = new HashMap<Integer, FavouriteEvents>();
				objMap.put(favObj.getEventId(), favObj);
				favEventObjMapByCustIdArtistId.put(favObj.getCustomerId(), objMap);
			}
		}
		System.out.println("TIME TO LAST CUSTOMER FAVORITE EVNET UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	public static void manageFavHandler(Integer customerId , Boolean isRemove,FavouriteEvents favObj){
		
		Map<Integer, FavouriteEvents> tempMap = favEventObjMapByCustIdArtistId.get(customerId);
		Map<Integer, Boolean> artistMap = favEventMapByCustIdEventId.get(customerId);
		List<Integer> unFavEventIds =  unFavoriteEventIdsMap.get(favObj.getCustomerId());
		
		if(isRemove){
			if(null != tempMap && !tempMap.isEmpty()){
				favEventObjMapByCustIdArtistId.get(favObj.getCustomerId()).remove(favObj.getEventId());
			}
			
			if(null != artistMap && !artistMap.isEmpty()){
				favEventMapByCustIdEventId.get(customerId).remove(favObj.getEventId());
			}
			
			if(null == unFavEventIds ){
				unFavEventIds = new ArrayList<Integer>();
				unFavEventIds.add(favObj.getEventId());
				unFavoriteEventIdsMap.put(favObj.getCustomerId(),unFavEventIds);
			}else{
				unFavoriteEventIdsMap.get(favObj.getCustomerId()).add(favObj.getEventId());
			}
			
		}else{
			
			if(null != tempMap && !tempMap.isEmpty()){
				favEventObjMapByCustIdArtistId.get(customerId).put(favObj.getEventId(), favObj);
			}else{
				tempMap = new HashMap<Integer, FavouriteEvents>();
				tempMap.put(favObj.getEventId(), favObj);
				favEventObjMapByCustIdArtistId.put(customerId, tempMap);
			}
			
			if(null != artistMap && !artistMap.isEmpty()){
				favEventMapByCustIdEventId.get(customerId).put(favObj.getEventId(), Boolean.TRUE);
			}else{
				artistMap = new HashMap<Integer, Boolean>();
				artistMap.put(favObj.getEventId(), Boolean.TRUE);
				favEventMapByCustIdEventId.put(customerId, artistMap);
			}
			
			if(null != unFavEventIds){
				unFavoriteEventIdsMap.get(favObj.getCustomerId()).remove(favObj.getEventId());
			}
			
		}
	}
	
	public static Map<Integer, FavouriteEvents> getFavoriteEventByCustomerId(Integer customerId){
		Map<Integer, FavouriteEvents> handlerMap = favEventObjMapByCustIdArtistId.get(customerId);
		
		try{
			if(null == handlerMap || handlerMap.isEmpty()){
				List<FavouriteEvents> favoriteEvents = DAORegistry.getFavouriteEventsDAO().getAllActiveFavouriteEventsByCustomerId(customerId);
				
				if(null ==favoriteEvents || favoriteEvents.isEmpty()){
					return new HashMap<Integer, FavouriteEvents>();
				}
				
				for (FavouriteEvents favObj : favoriteEvents) {
					Map<Integer, FavouriteEvents> tempMap = favEventObjMapByCustIdArtistId.get(favObj.getCustomerId());
					if(null != tempMap && !tempMap.isEmpty()){
						favEventObjMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getEventId(), favObj);
					}else{
						tempMap = new HashMap<Integer, FavouriteEvents>();
						tempMap.put(favObj.getEventId(), favObj);
						favEventObjMapByCustIdArtistId.put(favObj.getCustomerId(), tempMap);
					}
				}
				return favEventObjMapByCustIdArtistId.get(customerId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return handlerMap;
		
	}
	
	public static List<Integer> getUnFavoritedEventIdsByCustomerId(Integer customerId){
		List<Integer> eventIds= unFavoriteEventIdsMap.get(customerId);
		try{
			if(null == eventIds || eventIds.isEmpty()){
				
				List<FavouriteEvents> favoriteEvents = DAORegistry.getFavouriteEventsDAO().getAllUnFavouritedEventsByCustomerId(customerId);
				
				if(null ==favoriteEvents || favoriteEvents.isEmpty()){
					return new ArrayList<Integer>();
				}
				
				for (FavouriteEvents favObj : favoriteEvents) {
					
					List<Integer> unFavEventIds =  unFavoriteEventIdsMap.get(favObj.getCustomerId());
					if(null != unFavEventIds && !unFavEventIds.isEmpty()){
						unFavoriteEventIdsMap.get(favObj.getCustomerId()).add(favObj.getEventId());
					}else{
						unFavEventIds = new ArrayList<Integer>();
						unFavEventIds.add(favObj.getEventId());
						unFavoriteEventIdsMap.put(favObj.getCustomerId(), unFavEventIds);
					}
				}
				return unFavoriteEventIdsMap.get(customerId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return eventIds;
	}
	
	public static Map<Integer, Boolean> getFavoriteEventFlagByCustomerId(Integer customerId){
		Map<Integer, Boolean> artistMap = favEventMapByCustIdEventId.get(customerId);
		try{
			if(null == artistMap || artistMap.isEmpty()){
				List<FavouriteEvents> favoriteEvents = DAORegistry.getFavouriteEventsDAO().getAllActiveFavouriteEventsByCustomerId(customerId);
				
				if(null ==favoriteEvents || favoriteEvents.isEmpty()){
					return new HashMap<Integer, Boolean>();
				}
				
				for (FavouriteEvents favObj : favoriteEvents) {
					
					Map<Integer, Boolean> tempMap = favEventMapByCustIdEventId.get(favObj.getCustomerId());
					if(null != tempMap && !tempMap.isEmpty()){
						favEventMapByCustIdEventId.get(favObj.getCustomerId()).put(favObj.getEventId(), Boolean.TRUE);
					}else{
						tempMap = new HashMap<Integer, Boolean>();
						tempMap.put(favObj.getEventId(), Boolean.TRUE);
						favEventMapByCustIdEventId.put(favObj.getCustomerId(), tempMap);
					}
				}
				return favEventMapByCustIdEventId.get(customerId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return artistMap;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}
	
	
}
