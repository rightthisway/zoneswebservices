package com.zonesws.webservices.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerFavoritesHandler;


/**
 * Customer Favorite Artist Util to get the favorite artist details by customer
 * @author kulaganathan
 *
 */
public class CustomerFavoriteUtil extends QuartzJobBean implements StatefulJob {
	
	public static Map<Integer, Map<Integer, Boolean>> favMapByCustIdArtistId = new ConcurrentHashMap<Integer, Map<Integer, Boolean>>();
	public static Map<String, Boolean> favArtistMapByArtistIdCustId = new ConcurrentHashMap<String, Boolean>();
	public static Map<Integer, Map<Integer, CustomerFavoritesHandler>> favObjMapByCustIdArtistId = new ConcurrentHashMap<Integer, Map<Integer, CustomerFavoritesHandler>>();
	
	
	public static void init(){
		
	 try{
		 
		Long startTime = System.currentTimeMillis();
		List<CustomerFavoritesHandler> favoriteArtists = DAORegistry.getCustomerFavoritesHandlerDAO().getAllActiveFavoriteArtist();
		
		if(null ==favoriteArtists || favoriteArtists.isEmpty()){
			return;
		}
		
		for (CustomerFavoritesHandler favObj : favoriteArtists) {
			
			/*List<Integer> artistIds =  favArtistIdByCustIdMap.get(favObj.getCustomerId());
			if(null != artistIds && !artistIds.isEmpty()){
				favArtistIdByCustIdMap.get(favObj.getCustomerId()).add(favObj.getArtistId());
			}else{
				artistIds = new ArrayList<Integer>();
				artistIds.add(favObj.getArtistId());
				favArtistIdByCustIdMap.put(favObj.getCustomerId(), artistIds);
			}*/
			
			
				
				Map<Integer, Boolean> tempMap = favMapByCustIdArtistId.get(favObj.getCustomerId());
				if(null != tempMap && !tempMap.isEmpty()){
					favMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getArtistId(), Boolean.TRUE);
				}else{
					tempMap = new HashMap<Integer, Boolean>();
					tempMap.put(favObj.getArtistId(), Boolean.TRUE);
					favMapByCustIdArtistId.put(favObj.getCustomerId(), tempMap);
				}
				
				
				Map<Integer, CustomerFavoritesHandler> objMap = favObjMapByCustIdArtistId.get(favObj.getCustomerId());
				if(null != objMap && !objMap.isEmpty()){
					favObjMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getArtistId(), favObj);
				}else{
					objMap = new HashMap<Integer, CustomerFavoritesHandler>();
					objMap.put(favObj.getArtistId(), favObj);
					favObjMapByCustIdArtistId.put(favObj.getCustomerId(), objMap);
				}
				
			
				if(favObj.getStatus().equals(com.zonesws.webservices.enums.Status.ACTIVE)){
					
					Boolean isExist = favArtistMapByArtistIdCustId.get(favObj.getCustomerId()+"_"+favObj.getArtistId()); 
					if(null == isExist){
						favArtistMapByArtistIdCustId.put(favObj.getCustomerId()+"_"+favObj.getArtistId(), Boolean.TRUE);
					}
					
					
					/*CustomerFavoritesHandler tempObj = favouriteArtistObjByArtistIdCustId.get(favObj.getCustomerId()+"_"+favObj.getArtistId()); 
					if(null == tempObj){
						favouriteArtistObjByArtistIdCustId.put(favObj.getCustomerId()+"_"+favObj.getArtistId(), favObj);
					}
					
					
					List<CustomerFavoritesHandler> favHandlerList =  favHandlerListByCustMap.get(favObj.getCustomerId());
					if(null !=favHandlerList && !favHandlerList.isEmpty()){
						favHandlerListByCustMap.get(favObj.getCustomerId()).add(favObj);
					}else{
						favHandlerList= new ArrayList<CustomerFavoritesHandler>();
						favHandlerList.add(favObj);
						favHandlerListByCustMap.put(favObj.getCustomerId(), favHandlerList);
					}*/
					
					
					
				}
			
			
			
			
			
		}
		
		System.out.println("TIME TO LAST CUSTOMER FAVORITE ARTIST UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
		
	}
	
	
	public static void manageFavHandler(Integer customerId , Boolean isRemove,CustomerFavoritesHandler favObj){
		
	 	Map<Integer, Boolean> artistMap = favMapByCustIdArtistId.get(customerId);
	 	Map<Integer, CustomerFavoritesHandler> artistObjMap = favObjMapByCustIdArtistId.get(customerId);
	 	Boolean artistFlag = favArtistMapByArtistIdCustId.get(customerId+"_"+favObj.getArtistId());
	 
		
		if(isRemove){
			if(null != artistMap && !artistMap.isEmpty()){
				favMapByCustIdArtistId.get(customerId).remove(favObj.getArtistId());
			}
			
			if(null != artistObjMap && !artistObjMap.isEmpty()){
				favObjMapByCustIdArtistId.get(customerId).remove(favObj.getArtistId());
			}
			
			if(null != artistFlag && artistFlag){
				favArtistMapByArtistIdCustId.get(customerId+"_"+favObj.getArtistId());
			}
			
			
		}else{
			
			
			
			if(null != artistMap && !artistMap.isEmpty()){
				favMapByCustIdArtistId.get(customerId).put(favObj.getArtistId(), Boolean.TRUE);
			}else{
				artistMap = new HashMap<Integer, Boolean>();
				artistMap.put(favObj.getArtistId(), Boolean.TRUE);
				favMapByCustIdArtistId.put(customerId, artistMap);
			}
			
			if(null != artistObjMap && !artistObjMap.isEmpty()){
				favObjMapByCustIdArtistId.get(customerId).put(favObj.getArtistId(), favObj);
			}else{
				artistObjMap = new HashMap<Integer, CustomerFavoritesHandler>();
				artistObjMap.put(favObj.getArtistId(), favObj);
				favObjMapByCustIdArtistId.put(customerId, artistObjMap);
			}
			
			if(null == artistFlag){
				favArtistMapByArtistIdCustId.put(customerId+"_"+favObj.getArtistId(),Boolean.TRUE);
			}
			
		}
	}
	
	
	public static void manageFavHandler(Integer customerId , Boolean isRemove,List<CustomerFavoritesHandler> favObjList){
		
	 	
		Map<Integer, Boolean> artistMap = favMapByCustIdArtistId.get(customerId);
	 	Map<Integer, CustomerFavoritesHandler> artistObjMap = favObjMapByCustIdArtistId.get(customerId);
	 	
	 	for(CustomerFavoritesHandler favObj : favObjList){
	 		
		 	Boolean artistFlag = favArtistMapByArtistIdCustId.get(customerId+"_"+favObj.getArtistId());
		 	
			if(isRemove){
				if(null != artistMap && !artistMap.isEmpty()){
					favMapByCustIdArtistId.get(customerId).remove(favObj.getArtistId());
				}
				
				if(null != artistObjMap && !artistObjMap.isEmpty()){
					favObjMapByCustIdArtistId.get(customerId).remove(favObj.getArtistId());
				}
				
				if(null != artistFlag && artistFlag){
					favArtistMapByArtistIdCustId.get(customerId+"_"+favObj.getArtistId());
				}
				
			}else{
				
				
				if(null != artistMap && !artistMap.isEmpty()){
					favMapByCustIdArtistId.get(customerId).put(favObj.getArtistId(), Boolean.TRUE);
				}else{
					artistMap = new HashMap<Integer, Boolean>();
					artistMap.put(favObj.getArtistId(), Boolean.TRUE);
					favMapByCustIdArtistId.put(customerId, artistMap);
				}
				
				if(null != artistObjMap && !artistObjMap.isEmpty()){
					favObjMapByCustIdArtistId.get(customerId).put(favObj.getArtistId(), favObj);
				}else{
					artistObjMap = new HashMap<Integer, CustomerFavoritesHandler>();
					artistObjMap.put(favObj.getArtistId(), favObj);
					favObjMapByCustIdArtistId.put(customerId, artistObjMap);
				}
				
				if(null == artistFlag){
					favArtistMapByArtistIdCustId.put(customerId+"_"+favObj.getArtistId(),Boolean.TRUE);
				}
			}
	 	}
	}

	
	public static void removeFavHandlerByArtistIds(Integer customerId , List<Integer> artistIds){
	 	
		Map<Integer, Boolean> artistMap = favMapByCustIdArtistId.get(customerId);
	 	Map<Integer, CustomerFavoritesHandler> artistObjMap = favObjMapByCustIdArtistId.get(customerId);
	 
	 	for(Integer artistId : artistIds){
		 	Boolean artistFlag = favArtistMapByArtistIdCustId.get(customerId+"_"+artistId);
			if(null != artistMap && !artistMap.isEmpty()){
				favMapByCustIdArtistId.get(customerId).remove(artistId);
			}
			
			if(null != artistObjMap && !artistObjMap.isEmpty()){
				favObjMapByCustIdArtistId.get(customerId).remove(artistId);
			}
			
			if(null != artistFlag && artistFlag){
				favArtistMapByArtistIdCustId.get(customerId+"_"+artistId);
			}
			
	 	}
	}
	

	
	public static Boolean getFavHandlerByCustomerId(Integer customerId,Integer artistId){
		
		Boolean isFavFlag = favArtistMapByArtistIdCustId.get(customerId+"_"+artistId);
		try{
			if(null == isFavFlag ){
				List<CustomerFavoritesHandler> favoriteArtists = DAORegistry.getCustomerFavoritesHandlerDAO().getAllActiveFavArtistByCustomerId(customerId);
				if(null == favoriteArtists || favoriteArtists.isEmpty()){
					return false;
				}
				for (CustomerFavoritesHandler favObj : favoriteArtists) {
					Boolean isExist = favArtistMapByArtistIdCustId.get(favObj.getCustomerId()+"_"+favObj.getArtistId()); 
					if(null == isExist){
						favArtistMapByArtistIdCustId.put(favObj.getCustomerId()+"_"+favObj.getArtistId(), Boolean.TRUE);
					}
				}
				isFavFlag = favArtistMapByArtistIdCustId.get(customerId+"_"+artistId);
				return (null != isFavFlag?isFavFlag:false);
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return isFavFlag;
		
	}
	
	public static Map<Integer, CustomerFavoritesHandler> getFavHandlerByCustomerId(Integer customerId){
		Map<Integer, CustomerFavoritesHandler> handlerMap = favObjMapByCustIdArtistId.get(customerId);
		
		try{
			if(null == handlerMap || handlerMap.isEmpty()){
				List<CustomerFavoritesHandler> favoriteArtists = DAORegistry.getCustomerFavoritesHandlerDAO().getAllActiveFavArtistByCustomerId(customerId);
				if(null == favoriteArtists || favoriteArtists.isEmpty()){
					return new HashMap<Integer, CustomerFavoritesHandler>();
				}
				for (CustomerFavoritesHandler favObj : favoriteArtists) {
					Map<Integer, CustomerFavoritesHandler> tempMap = favObjMapByCustIdArtistId.get(favObj.getCustomerId());
					if(null != tempMap && !tempMap.isEmpty()){
						favObjMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getArtistId(), favObj);
					}else{
						tempMap = new HashMap<Integer, CustomerFavoritesHandler>();
						tempMap.put(favObj.getArtistId(), favObj);
						favObjMapByCustIdArtistId.put(favObj.getCustomerId(), tempMap);
					}
				}
				return favObjMapByCustIdArtistId.get(customerId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return handlerMap;
		
	}
	
	
	public static Map<Integer, Boolean> getFavoriteArtistByCustomerId(Integer customerId){
		Map<Integer, Boolean> artistMap = favMapByCustIdArtistId.get(customerId);
		try{
			if(null == artistMap || artistMap.isEmpty()){
				List<CustomerFavoritesHandler> favoriteArtists = DAORegistry.getCustomerFavoritesHandlerDAO().getAllActiveFavArtistByCustomerId(customerId);
				
				if(null == favoriteArtists || favoriteArtists.isEmpty()){
					return new HashMap<Integer, Boolean>();
				}
				
				for (CustomerFavoritesHandler favObj : favoriteArtists) {
					
					Map<Integer, Boolean> tempMap = favMapByCustIdArtistId.get(favObj.getCustomerId());
					if(null != tempMap && !tempMap.isEmpty()){
						favMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getArtistId(), Boolean.TRUE);
					}else{
						tempMap = new HashMap<Integer, Boolean>();
						tempMap.put(favObj.getArtistId(), Boolean.TRUE);
						favMapByCustIdArtistId.put(favObj.getCustomerId(), tempMap);
					}
				}
				return favMapByCustIdArtistId.get(customerId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return artistMap;
	}
	
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}
	
}
