package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerFavoritesHandler;



public class CustomerFavoriteUtilTemp {
	
	public static Map<Integer, Map<Integer, Boolean>> favMapByCustIdArtistId = new ConcurrentHashMap<Integer, Map<Integer, Boolean>>();
	public static Map<Integer, List<Integer>> favArtistIdByCustIdMap = new ConcurrentHashMap<Integer, List<Integer>>();
	public static Map<Integer, List<CustomerFavoritesHandler>> favHandlerListByCustMap = new ConcurrentHashMap<Integer, List<CustomerFavoritesHandler>>();
	public static Map<String, Boolean> favArtistMapByArtistIdCustId = new ConcurrentHashMap<String, Boolean>();
	public static Map<Integer, CustomerFavoritesHandler> favouriteArtistObjMap = new ConcurrentHashMap<Integer, CustomerFavoritesHandler>();
	public static Map<String, CustomerFavoritesHandler> favouriteArtistObjByArtistIdCustId = new ConcurrentHashMap<String, CustomerFavoritesHandler>();
	
	public void init(){
		
		/*
		Map<Integer, List<Integer>> favArtistIdByCustIdMapTemp = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<CustomerFavoritesHandler>> favHandlerListByCustMapTemp = new HashMap<Integer, List<CustomerFavoritesHandler>>();
		Map<String, Boolean> favArtistMapByArtistIdCustIdTemp = new HashMap<String, Boolean>();
		Map<Integer, CustomerFavoritesHandler> favouriteArtistObjMapTemp = new HashMap<Integer, CustomerFavoritesHandler>();
		Map<String, CustomerFavoritesHandler> favouriteArtistObjByArtistIdCustIdTemp = new HashMap<String, CustomerFavoritesHandler>();*/
		
	 try{
		List<CustomerFavoritesHandler> favoriteArtists = DAORegistry.getCustomerFavoritesHandlerDAO().getAllActiveFavoriteArtist();
		
		for (CustomerFavoritesHandler favObj : favoriteArtists) {
			
			List<Integer> artistIds =  favArtistIdByCustIdMap.get(favObj.getCustomerId());
			if(null != artistIds && !artistIds.isEmpty()){
				favArtistIdByCustIdMap.get(favObj.getCustomerId()).add(favObj.getArtistId());
			}else{
				artistIds = new ArrayList<Integer>();
				artistIds.add(favObj.getArtistId());
				favArtistIdByCustIdMap.put(favObj.getCustomerId(), artistIds);
			}
			
			
			Boolean isExist = favArtistMapByArtistIdCustId.get(favObj.getCustomerId()+"_"+favObj.getArtistId()); 
			if(null == isExist){
				favArtistMapByArtistIdCustId.put(favObj.getCustomerId()+"_"+favObj.getArtistId(), Boolean.TRUE);
			}
			
			
			CustomerFavoritesHandler tempObj = favouriteArtistObjByArtistIdCustId.get(favObj.getCustomerId()+"_"+favObj.getArtistId()); 
			if(null == tempObj){
				favouriteArtistObjByArtistIdCustId.put(favObj.getCustomerId()+"_"+favObj.getArtistId(), favObj);
			}
			
			
			List<CustomerFavoritesHandler> favHandlerList =  favHandlerListByCustMap.get(favObj.getCustomerId());
			if(null !=favHandlerList && !favHandlerList.isEmpty()){
				favHandlerListByCustMap.get(favObj.getCustomerId()).add(favObj);
			}else{
				favHandlerList= new ArrayList<CustomerFavoritesHandler>();
				favHandlerList.add(favObj);
				favHandlerListByCustMap.put(favObj.getCustomerId(), favHandlerList);
			}
			
			
			Map<Integer, Boolean> tempMap = favMapByCustIdArtistId.get(favObj.getCustomerId());
			if(null != tempMap && !tempMap.isEmpty()){
				favMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getArtistId(), Boolean.TRUE);
			}else{
				tempMap = new HashMap<Integer, Boolean>();
				tempMap.put(favObj.getArtistId(), Boolean.TRUE);
				favMapByCustIdArtistId.put(favObj.getCustomerId(), tempMap);
			}
			
			
		}
	 }catch(Exception e){
		 e.printStackTrace();
	 }
		
	}
	
	public static Map<Integer, Boolean> getFavoriteArtistByCustomerId(Integer customerId){
		Map<Integer, Boolean> artistMap = favMapByCustIdArtistId.get(customerId);
		try{
			if(null == artistMap || artistMap.isEmpty()){
				List<CustomerFavoritesHandler> favoriteArtists = DAORegistry.getCustomerFavoritesHandlerDAO().getAllActiveFavArtistByCustomerId(customerId);
				for (CustomerFavoritesHandler favObj : favoriteArtists) {
					
					Map<Integer, Boolean> tempMap = favMapByCustIdArtistId.get(favObj.getCustomerId());
					if(null != tempMap && !tempMap.isEmpty()){
						favMapByCustIdArtistId.get(favObj.getCustomerId()).put(favObj.getArtistId(), Boolean.TRUE);
					}else{
						tempMap = new HashMap<Integer, Boolean>();
						tempMap.put(favObj.getArtistId(), Boolean.TRUE);
						favMapByCustIdArtistId.put(favObj.getCustomerId(), tempMap);
					}
				}
				return getFavoriteArtistByCustomerId(customerId);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return artistMap;
	}
	
	
	
	
}
