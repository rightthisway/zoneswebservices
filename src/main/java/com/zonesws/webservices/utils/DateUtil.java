package com.zonesws.webservices.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author dthiyagarajan
 *
 */
public class DateUtil {

	/**
	 * Method to format the time 
	 * @param eventTimeStr
	 * @return
	 */
	public static String formatTime(String eventTimeStr){
		String formattedTime = null;
		try {
			if(eventTimeStr != null){
				if(eventTimeStr.equals("TBD")){
					formattedTime = "TBD";
				}else{
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mmaa");
					DateFormat tf = new SimpleDateFormat("hh:mm aa");
					
					Date date = (Date)sdf.parse(eventTimeStr);
					formattedTime = tf.format(date);
				}
			}
			return formattedTime;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public static long getDifferenceMintues(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	
	
	
	public static String getDifferenceDaysandHours(Date d1, Date d2) {
		
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String dayHours="";
		try{
			d1 = format.parse(format.format(d1));
			d2 = format.parse(format.format(d2));
			long diff = d2.getTime() - d1.getTime();
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
			/*System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");*/
			dayHours = diffDays+"_"+diffHours;
		}catch(Exception e){
			dayHours = getDifferenceDays(d1, d2)+"_"+0;
			e.printStackTrace();
		}
		return dayHours;
		
	}
	
	public static void getDifferenceByTowDates() {

		String dateStart = "01/14/2012 09:29:58";
		String dateStop = "01/14/2012 10:31:48";

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);

			System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	
	
}
