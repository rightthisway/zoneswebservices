package com.zonesws.webservices.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("DiscountDetails")
public class DiscountDetails {
	
	private Integer zoneDiscountPerc;
	private Integer loyaltyPerc;
	private Integer promotionalOffer;
	private boolean discountZoneFlag;
	
	
	public Integer getZoneDiscountPerc() {
		return zoneDiscountPerc;
	}
	public void setZoneDiscountPerc(Integer zoneDiscountPerc) {
		this.zoneDiscountPerc = zoneDiscountPerc;
	}
	public Integer getLoyaltyPerc() {
		return loyaltyPerc;
	}
	public void setLoyaltyPerc(Integer loyaltyPerc) {
		this.loyaltyPerc = loyaltyPerc;
	}
	public Integer getPromotionalOffer() {
		return promotionalOffer;
	}
	public void setPromotionalOffer(Integer promotionalOffer) {
		this.promotionalOffer = promotionalOffer;
	}
	public boolean isDiscountZoneFlag() {
		return discountZoneFlag;
	}
	public void setDiscountZoneFlag(boolean discountZoneFlag) {
		this.discountZoneFlag = discountZoneFlag;
	}
	
}

