package com.zonesws.webservices.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Error")
public class Error {
	private String description;
	private String buttonValue;
	private Boolean overRideButton;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getButtonValue() {
		return buttonValue;
	}
	public void setButtonValue(String buttonValue) {
		this.buttonValue = buttonValue;
	}
	public Boolean getOverRideButton() {
		if(null == overRideButton){
			overRideButton = false;
		}
		return overRideButton;
	}
	public void setOverRideButton(Boolean overRideButton) {
		this.overRideButton = overRideButton;
	}

	@Override
	public String toString() {
		return "Error [des=" + description + ", btnV=" + buttonValue + ", ovRbtn="
				+ overRideButton + "]";
	}
	
}
