package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.EventArtist;
import com.zonesws.webservices.data.RTFCategorySearch;


/**
 * Event Artist Util to get the all active event artist details
 * @author kulaganathan
 *
 */
public class EventArtistUtil extends QuartzJobBean implements StatefulJob {
	
	public static Map<Integer, List<EventArtist>> eventArtistObjMap = new ConcurrentHashMap<Integer, List<EventArtist>>();
	public static Map<Integer, List<Integer>> eventArtistMap = new ConcurrentHashMap<Integer, List<Integer>>();
	public static Map<Integer, Boolean> activeEventArtistMap = new ConcurrentHashMap<Integer, Boolean>();
	
	public static Map<Integer, RTFCategorySearch> rtfCatsMap = new ConcurrentHashMap<Integer, RTFCategorySearch>();
	
	public static void init(){
		
	 try{
		 
		Long startTime = System.currentTimeMillis();
		List<EventArtist> eventArtists = DAORegistry.getQueryManagerDAO().getAllActiveEventArtistDetails();
		
		if(null ==eventArtists || eventArtists.isEmpty()){
			return;
		}
		
		Map<Integer, List<Integer>> eventArtistMapTemp = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<EventArtist>> eventArtistObjMapTemp = new HashMap<Integer, List<EventArtist>>();
		Map<Integer, Boolean> tempMap = new HashMap<Integer, Boolean>();
		
		List<EventArtist> artistEventList = null;
		
		int size = eventArtists.size(),i=1;
		for (EventArtist eventArtistObj : eventArtists) {
			try{
				List<Integer> artistIds = eventArtistMapTemp.get(eventArtistObj.getEventId());
				if(null != artistIds && !artistIds.isEmpty()){
					eventArtistMapTemp.get(eventArtistObj.getEventId()).add(eventArtistObj.getArtistId());
					eventArtistObjMapTemp.get(eventArtistObj.getEventId()).add(eventArtistObj);
				}else{
					artistIds = new ArrayList<Integer>();
					artistIds.add(eventArtistObj.getArtistId());
					eventArtistMapTemp.put(eventArtistObj.getEventId(), artistIds);
					artistEventList = new ArrayList<EventArtist>();
					artistEventList.add(eventArtistObj);
					eventArtistObjMapTemp.put(eventArtistObj.getEventId(),artistEventList);
					tempMap.put(eventArtistObj.getArtistId(), true);
				}	
			}catch(Exception e){
				e.printStackTrace();
			}
			i++;
		}
		
		if(null !=eventArtistMapTemp && !eventArtistMapTemp.isEmpty()){
			eventArtistMap = null;
			eventArtistMap = new ConcurrentHashMap<Integer, List<Integer>>(eventArtistMapTemp);
			eventArtistObjMap = null;
			eventArtistObjMap = new ConcurrentHashMap<Integer, List<EventArtist>>(eventArtistObjMapTemp);
		}
		
		/*List<EventArtist> artistEventsList = DAORegistry.getQueryManagerDAO().getAllArtistWhichHasEvents();
		for (EventArtist obj : artistEventsList) {
			tempMap.put(obj.getArtistId(), true);
		}*/
		
		if(null !=tempMap && !tempMap.isEmpty()){
			activeEventArtistMap = null;
			activeEventArtistMap = new ConcurrentHashMap<Integer, Boolean>(tempMap);
		}

		Map<Integer, RTFCategorySearch> rtfCatsTempMap = new HashMap<Integer, RTFCategorySearch>();
		List<RTFCategorySearch> rtfCategoriesList = DAORegistry.getQueryManagerDAO().getAllRTFSearchCategories();
		for (RTFCategorySearch obj : rtfCategoriesList) {
			rtfCatsTempMap.put(obj.getId(), obj);
		}
		
		if(null !=rtfCatsTempMap && !rtfCatsTempMap.isEmpty()){
			rtfCatsMap = null;
			rtfCatsMap = new ConcurrentHashMap<Integer, RTFCategorySearch>(rtfCatsTempMap);
		}
		
		System.out.println("TIME TO LAST EVENT ARTIST UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	public static List<Integer> getArtistIdsByEventId(Integer eventId){
		if(null == eventArtistMap) {
			eventArtistMap = new ConcurrentHashMap<Integer, List<Integer>>(); 
		}
		List<Integer> artistIds = eventArtistMap.get(eventId);
		if(artistIds == null || artistIds.isEmpty()){
			List<EventArtist> eventArtists = DAORegistry.getQueryManagerDAO().getArtistDetailsByEventId(eventId);
			if(null ==eventArtists || eventArtists.isEmpty()){
				return null;
			}
			artistIds = new ArrayList<Integer>();
			for (EventArtist eventArtistObj : eventArtists) {
				artistIds.add(eventArtistObj.getArtistId());
				eventArtistMap.put(eventArtistObj.getEventId(), artistIds);
			}
		}
		return artistIds;
	}
	
	public static List<EventArtist> getAllEventArtistObjByEventId(Integer eventId){
		List<EventArtist> eventArtists = eventArtistObjMap.get(eventId);
		if(eventArtists == null || eventArtists.isEmpty()){
			eventArtists = DAORegistry.getQueryManagerDAO().getArtistDetailsByEventId(eventId);
			if(null ==eventArtists || eventArtists.isEmpty()){
				return null;
			}
			eventArtists = new ArrayList<EventArtist>();
			for (EventArtist eventArtistObj : eventArtists) {
				eventArtists.add(eventArtistObj);
				eventArtistObjMap.put(eventArtistObj.getEventId(), eventArtists);
			}
		}
		return eventArtists;
	}
	
	/*public static Boolean isArtistHasActiveEventsNewOLD(Integer artistId){
		Boolean hasEvents = activeEventArtistMap.get(artistId);
		if(hasEvents == null){
			List<EventArtist> eventArtists = DAORegistry.getQueryManagerDAO().getAllArtistWhichHasEvents(artistId);
			if(null ==eventArtists || eventArtists.isEmpty()){
				return false;
			}
			for (EventArtist eventArtistObj : eventArtists) {
				activeEventArtistMap.put(eventArtistObj.getArtistId(), true);
				hasEvents = true;
			}
		}
		return hasEvents;
	}*/
	
	public static RTFCategorySearch getRtfCategoriesById(Integer rtfCatId){
		RTFCategorySearch rtfCategorySearch = rtfCatsMap.get(rtfCatId);
		if(rtfCategorySearch == null){
			rtfCategorySearch = DAORegistry.getQueryManagerDAO().getAllRTFSearchCategoriesById(rtfCatId);
			if(null ==rtfCategorySearch){
				return null;
			} 
			rtfCatsMap.put(rtfCatId,rtfCategorySearch);
		}
		return rtfCategorySearch;
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}
	
}
