package com.zonesws.webservices.utils;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;



public class Example  {
	
	
		
	// Find your Account Sid and Token at twilio.com/user/account
	  public static final String ACCOUNT_SID = "ACc0d5b89953472346ae11d9b6e0e3289e";
	  public static final String AUTH_TOKEN = "80b54238355b198fa9abd7be0560c456";

	  public static void main(String[] args) {
	    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	    
	   /* // Get an object from its sid. If you do not have a sid,
	    // check out the list resource examples on this page
	    ShortCode shortCode = ShortCode.fetcher("SC6b20cb705c1e8f00210049b20b70fce2").fetch();*/

	    //System.out.println(shortCode.getShortCode());

	    Message message = Message
	        .creator(new PhoneNumber("+17327255647"), new PhoneNumber("+18665751799"),
	            "This is testing message from Reward The Fan By Ulaganathan via Twilio?")
	        //.setMediaUrl("https://c1.staticflickr.com/3/2899/14341091933_1e92e62d12_b.jpg")
	        .create();
	    
	    /*Message message = Message
        .creator(new PhoneNumber("+17327255647"), ACCOUNT_SID, 
        		"This is the first text sms from Reward The Fan")
        .create();*/

	    System.out.println(message.getSid());
	  }
	
}
