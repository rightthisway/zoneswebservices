package com.zonesws.webservices.utils;

import java.io.File;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.InitializingBean;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;



public class ExternalFileUtils implements InitializingBean{
	
	private static ZonesProperty properties;
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	public ZonesProperty getProperties() {
		return properties;
	}

	public void setProperties(ZonesProperty zonesProperties) {
		properties = zonesProperties;
	}
	
	public static String fileLocationBasePath = "";
	
	
	
 
	public void afterPropertiesSet() throws Exception {
		try{
			fileLocationBasePath = "////D://images//";
			//fileLocationBasePath ="smb://share:dev@10.0.0.91//Rewardthefan//SvgMaps//";
			//fileLocationBasePath ="////d (\\\\10.0.0.91)//Rewardthefan//SvgMaps//";
			//fileLocationBasePath ="////C://SVG//Maps//"; //properties.getServerWebAppBasePath();
			//fileLocationBasePath = "////10.0.0.91//d$//Rewardthefan//SvgMaps//";
			//fileLocationBasePath = "//d$//Rewardthefan//SvgMaps//";
			System.out.println("fileLocationBasePath==========>"+fileLocationBasePath);
		}catch(Exception e){
			fileLocationBasePath = "////10.0.0.91//d$//Rewardthefan//images//";
			e.printStackTrace();
		}
	}
	
	public static File getSvgMapFile(String venueCategory, Integer venueId){
		try{
			File svgMapFile = new File(ExternalFileUtils.fileLocationBasePath + venueId+"_"+venueCategory+".gif");
			return svgMapFile;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static File getSvgMapFile(String name){
		try{
			
			String url = "smb://10.0.0.91//d$//Rewardthefan//images//";
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, "dev", "Rb#32H8!");
			SmbFile dir = new SmbFile(url, auth);
			for (SmbFile f : dir.listFiles()) {
			    System.out.println(f.getName());
			}
			
			
			File svgMapFile = new File(ExternalFileUtils.fileLocationBasePath + name);
			return svgMapFile;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	

}