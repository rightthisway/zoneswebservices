package com.zonesws.webservices.utils;

import java.util.List;
import java.util.Map;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerSuperFan;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.RTFCustomerPromotionalOffer;
import com.zonesws.webservices.data.RTFPromotionalOfferDtl;
import com.zonesws.webservices.data.RTFPromotionalOfferHdr;
import com.zonesws.webservices.data.State;
import com.zonesws.webservices.enums.LoyalFanType;
import com.zonesws.webservices.utils.list.ArtistResult;



/**
 * Favorite Util to mark event as favorite
 * @author kulaganathan
 *
 */
public class FavoriteUtil {
	
	
	public static Event markEventAsFavorite(Event event , Map<Integer, Boolean> favouriteEventMap,Map<Integer, Boolean> favouriteArtistMap,
			List<Integer> unFavoritedEventIds){
		try{	
			if(null != unFavoritedEventIds && !unFavoritedEventIds.isEmpty() && unFavoritedEventIds.contains(event.getEventId())){
				event.setIsFavoriteEvent(false);
				return event;
			}
			
			Boolean isFavouriteEvent = false;
			try{
				if(null != favouriteEventMap && favouriteEventMap.size()>0){
					isFavouriteEvent = favouriteEventMap.get(event.getEventId());
				}
				if(null == isFavouriteEvent){
					isFavouriteEvent = false;
				}
			}catch(Exception e){
				e.printStackTrace();
				isFavouriteEvent = false;
			}
			
			if(null != isFavouriteEvent && isFavouriteEvent){
				event.setIsFavoriteEvent(true);
				return event;
			}else{
				Boolean isFavouriteArtist = false;
				if(null == event.getArtistIds() || event.getArtistIds().isEmpty()){
					event.setIsFavoriteEvent(false);
					return event;
				}
				
				for (Integer artistId : event.getArtistIds()) {
					if(null != favouriteArtistMap && favouriteArtistMap.size() > 0){
						isFavouriteArtist = favouriteArtistMap.get(artistId);
						if(null != isFavouriteArtist && isFavouriteArtist){
							break;
						}
					}
				}
				if(null != isFavouriteArtist && isFavouriteArtist){
					event.setIsFavoriteEvent(true);
					return event;
				}
				event.setIsFavoriteEvent(false);
			}
		}catch(Exception e){
			event.setIsFavoriteEvent(false);
			e.printStackTrace();
		}
			
		return event;
	}
	
	
	
	public static CustomerOrder markEventAsFavorite(CustomerOrder order , Map<Integer, Boolean> favouriteEventMap,
			Map<Integer, Boolean> favouriteArtistMap,
			List<Integer> unFavoritedEventIds){
		try{	
			if(null != unFavoritedEventIds && !unFavoritedEventIds.isEmpty() && unFavoritedEventIds.contains(order.getEventId())){
				order.setIsFavoriteEvent(false);
				return order;
			}
			
			Boolean isFavouriteEvent = false;
			if(null != favouriteEventMap && favouriteEventMap.size() > 0){
				isFavouriteEvent = favouriteEventMap.get(order.getEventId());
			}
			
			if(null != isFavouriteEvent && isFavouriteEvent){
				order.setIsFavoriteEvent(true);
				return order;
			}else{
				/*Boolean isFavouriteArtist = ((null != favouriteArtistMap && !favouriteArtistMap.isEmpty() )?favouriteArtistMap.get(order.get):false);
				if(null != isFavouriteArtist && isFavouriteArtist){
					order.setIsFavoriteEvent(true);
					return order;
				}
				order.setIsFavoriteEvent(false);*/
			}
		}catch(Exception e){
			order.setIsFavoriteEvent(false);
		}
			
		return order;
	}
	
	public static ArtistResult markArtistAsFavorite(ArtistResult result, Map<Integer, Boolean> favouriteArtistMap){
		try{
			Boolean isFavouriteArtist = false;
			if(null != favouriteArtistMap && favouriteArtistMap.size() > 0){
				isFavouriteArtist = favouriteArtistMap.get(result.getArtistId());
			}
			if(null != isFavouriteArtist && isFavouriteArtist){
				result.setIsFavoriteFlag(true);
				return result;
			}
			result.setIsFavoriteFlag(false);
			
		}catch(Exception e){
			result.setIsFavoriteFlag(false);
		}
		return result;
	}
	
	public static boolean markArtistAsLoyalFanOLd(CustomerSuperFan loyalFan, String eventName, Integer artistId){
		try{
			if(null != artistId && (loyalFan.getArtistOrTeamId().equals(artistId) || 
					loyalFan.getArtistOrTeamId() == artistId)){
				return true;
			}
			if(null != eventName && (eventName.toLowerCase().contains(loyalFan.getLoyalFanName().toLowerCase()))){
				return true;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	public static boolean markArtistAsLoyalFan(CustomerSuperFan loyalFan, Event event){
		try{
			
			if(loyalFan.getLoyalFanType().equals(LoyalFanType.SPORTS)){
				List<Artist> artists = DAORegistry.getQueryManagerDAO().getArtistByEvent(event.getEventId());
				Artist artist = artists.get(0);
				return markArtistAsLoyalFanOLd(loyalFan, event.getEventName(), artist.getId());
			}else{
				if(loyalFan.getLoyalFanType().equals(LoyalFanType.CONCERTS) && event.getParentCategoryId().equals(2)){
					
					String loyalFanName = event.getState();
					List<State> states = DAORegistry.getStateDAO().getAllStateBySearchKey(event.getState());
					if(null != states && !states.isEmpty()){
						loyalFanName = states.get(0).getName();
					}
					if(loyalFan.getLoyalFanName().contains(loyalFanName) || 
							loyalFan.getLoyalFanName().equalsIgnoreCase(loyalFanName) || 
							(null != loyalFan.getState() &&  loyalFan.getState().equalsIgnoreCase(event.getState()))){
						return true;
					}else{
						return false;
					}
					
				}else if(loyalFan.getLoyalFanType().equals(LoyalFanType.THEATER) && (event.getParentCategoryId().equals(3) || 
						event.getParentCategoryId().equals(4))){
					
					String loyalFanName = event.getState();
					List<State> states = DAORegistry.getStateDAO().getAllStateBySearchKey(event.getState());
					if(null != states && !states.isEmpty()){
						loyalFanName = states.get(0).getName();
					}
					if(loyalFan.getLoyalFanName().contains(loyalFanName) || 
							loyalFan.getLoyalFanName().equalsIgnoreCase(loyalFanName)){
						return true;
					}else{
						return false;
					}
					
				}
				return false;
			}
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean eventBelongsToThisArtist(String artistName, String eventName, Integer artistId,List<Integer> artistIds){
		try{
			if(null != artistIds && (artistIds.contains(artistId))){
				return true;
			}
			if(null != eventName && (eventName.toLowerCase().contains(artistName.toLowerCase()))){
				return true;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	
	public static boolean eventBelongsToThisVenue(Integer venueId, Event event){
		try{
			if(event.getVenueId().equals(venueId)){
				return true;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	public static boolean eventBelongsToThisGrandChild(Integer grandChildId, Event event){
		try{
			if(event.getGrandChildCategoryId().equals(grandChildId)){
				return true;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	public static boolean eventBelongsToThisChild(Integer childId, Event event){
		try{
			if(event.getChildCategoryId().equals(childId)){
				return true;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	public static boolean eventBelongsToThisParent(Integer parentId, Event event){
		try{
			if(event.getParentCategoryId().equals(parentId)){
				return true;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	
	public static boolean isValidCustomerPromotionalCode(RTFCustomerPromotionalOffer offer,String promoCode){
		try{
			if(offer.getStatus().equals("ENABLED") && 
					offer.getPromoCode().toUpperCase().equals(promoCode.toUpperCase())){
				if(offer.getNoOfOrders() >= offer.getMaxOrders()){
					return false;
				}else{
					return true;
				}
			}
			return false;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean isValidPromotionalCode(RTFPromotionalOfferHdr offerHdr, Event event, Boolean isLoyanPrice, Customer customer){
		try{
			if(offerHdr.getPromoCode().equals("LOYALFAN")){
				if(isLoyanPrice){
					try{
						CustomerSuperFan loyalFan = DAORegistry.getCustomerSuperFanDAO().getActiveLoyalFanByCustomerId(customer.getId());
						if(null != loyalFan && null != loyalFan.getTicketPurchased() && loyalFan.getTicketPurchased()){
							return false;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					return true;
				}else{
					return false;
				}
			}
			boolean isValidCode = false;
			List<RTFPromotionalOfferDtl> rtfPromotionalOfferDtls = DAORegistry.getRtfPromotionalOfferDtlDAO().getPromoDtlbyPromoOfferId(offerHdr.getId());
			if(null == rtfPromotionalOfferDtls || rtfPromotionalOfferDtls.isEmpty()){
				return false;
			}
			for (RTFPromotionalOfferDtl offer : rtfPromotionalOfferDtls) {
				
				switch (offer.getPromoType()) {
				
					case ALL:
						isValidCode = true;
						break;
						
					case EVENT:
						if(event.getEventId().equals(offer.getEventId())){
							isValidCode = true;
						}
						break;
						
					case ARTIST:
						List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
						Artist artist = DAORegistry.getArtistDAO().get(offer.getArtistId());
						isValidCode = eventBelongsToThisArtist(artist.getName(), event.getEventName(), offer.getArtistId(), artistIds);	
						break;
						
					case VENUE:
						isValidCode =eventBelongsToThisVenue(offer.getVenueId(), event);
						break;
						
					case GRANDCHILD:
						isValidCode =eventBelongsToThisGrandChild(offer.getGrandChildId(), event);
						break;
						
					case CHILD:
						isValidCode =eventBelongsToThisChild(offer.getChildId(), event);
						break;
						
					case PARENT:
						isValidCode =eventBelongsToThisParent(offer.getParentId(), event);
						break;
						
					default:
						break;
				}
				
				if(isValidCode){
					return true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
}
