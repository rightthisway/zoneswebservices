package com.zonesws.webservices.utils;

import org.apache.commons.io.FilenameUtils;



public class FileUtil  {
	
	public final static String[] allowedExtensions = {"tif","jpg","jpeg","gif","png"};
	
	public static Boolean validateFileExtension(String file){
		
		return FilenameUtils.isExtension(file, allowedExtensions);
	}
	
	
	
	public static void main(String[] args) {
		
		String fileName = "ULAGA_DP_IMAGE.pngsd";
		
		System.out.println(validateFileExtension(fileName));
	}
}
