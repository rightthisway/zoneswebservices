package com.zonesws.webservices.utils;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.Status;
@XStreamAlias("HoldOnTicket")
public class HoldOnTicket implements Serializable{
		private String id;
		
		private String message;
		private Status status;

		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}

		

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
		
	}