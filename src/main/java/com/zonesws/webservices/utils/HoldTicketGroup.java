package com.zonesws.webservices.utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.Status;
@XStreamAlias("HoldTicketGroup")
public class HoldTicketGroup implements Serializable{
		private Integer ticketGroupId;
		private Integer holdTicketId;
		private Integer eventId;
		private String category;
		private Integer quantity;
		private double price;
		private double total;
		private String message;
		private Status status;

		public Integer getEventId() {
			return eventId;
		}
		
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		
		public String getCategory() {
			return category;
		}
		
		public void setCategory(String category) {
			this.category = category;
		}
		
		public Integer getQuantity() {
			return quantity;
		}
		
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			DecimalFormat df = new DecimalFormat("#.##");
			this.price = Double.valueOf((df.format(price)));
		}

		public double getTotal() {
			return total;
		}

		public void setTotal(double total) {
			this.total = total;
		}
		
		public static void setTotalPriceForTicketGroups(List<HoldTicketGroup> ticketGroups){
			DecimalFormat df = new DecimalFormat("#.##");
			for(HoldTicketGroup group:ticketGroups){
				group.setTotal(Double.valueOf((df.format(group.getPrice()*group.getQuantity()))));
			}
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public Integer getTicketGroupId() {
			return ticketGroupId;
		}

		public void setTicketGroupId(Integer ticketGroupId) {
			this.ticketGroupId = ticketGroupId;
		}

		public Integer getHoldTicketId() {
			return holdTicketId;
		}

		public void setHoldTicketId(Integer holdTicketId) {
			this.holdTicketId = holdTicketId;
		}
		
		
		
	}