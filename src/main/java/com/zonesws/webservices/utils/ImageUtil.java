package com.zonesws.webservices.utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class ImageUtil {
	
	public static void mainOLD(String[] args) {
		try {
			//String SRC = "C://Tomcat 7.0_Tomcat7.production/webapps/SvgMaps/CUSTOMER_DP/";
			//String DST = "C://Tomcat 7.0_Tomcat7.production/Profile/";
			
			String SRC = "C://mitul/CUSTOMER_DP/";
			String DST = "C://mitul/CUSTOMER_DP1/";
			
			File folder = new File(SRC);
			for(final File file : folder.listFiles()){
				String name = file.getName();
				String type = name.substring(name.lastIndexOf(".")+1);
				if(type.toLowerCase().equalsIgnoreCase("jpg") || type.toLowerCase().equalsIgnoreCase("jpeg")
					|| type.toLowerCase().equalsIgnoreCase("png")){
						ImageIcon icon = (ImageIcon) getIcon(SRC+name);
						File out = new File(DST+name);
						BufferedImage bi = (BufferedImage) icon.getImage();
						try {
							ImageIO.write(bi, type, out);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
				
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public static void main(String[] args) {
		try {
			//startProcessingImages();	
			Calendar cal = Calendar.getInstance();
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			dayOfWeek = 1-dayOfWeek;
			cal.add(Calendar.DAY_OF_MONTH, dayOfWeek);
			String startDateStr = dbDateFormat.format(new Date(cal.getTimeInMillis()))+" 00:00:00";
			String toDateStr = dbDateFormat.format(new Date())+" 23:59:59";
			System.out.println(startDateStr);
			System.out.println(toDateStr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void startProcessingImages() {
		try {
			
			/*String SRC = "C:/Tomcat 7.0/webapps/SvgMaps/compress/";
			String DST = "C:/Tomcat 7.0/webapps/SvgMaps/compress1/";*/
			
			String SRC = "/rtw/apache-tomcat-7.0.92/webapps/compress/";
			String DST = "/rtw/apache-tomcat-7.0.92/webapps/compress1/";
			
			File folder = new File(SRC);
			for(final File file : folder.listFiles()){
				try {
					String name = file.getName();
					String type = name.substring(name.lastIndexOf(".")+1);
					if(type.toLowerCase().equalsIgnoreCase("jpg") || type.toLowerCase().equalsIgnoreCase("jpeg")
						|| type.toLowerCase().equalsIgnoreCase("png")){
						ImageIcon icon = (ImageIcon) getIcon(SRC+name);
						File out = new File(DST+name);
						BufferedImage bi = (BufferedImage) icon.getImage();
						try {
							ImageIO.write(bi, type, out);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void compressImage(String fileName, String fileType, String srcFolder, String dstFolder) {
		try {
			if(fileType.toLowerCase().equalsIgnoreCase("jpg") || fileType.toLowerCase().equalsIgnoreCase("jpeg")
				|| fileType.toLowerCase().equalsIgnoreCase("png")){
					ImageIcon icon = (ImageIcon) getIcon(srcFolder+fileName);
					File out = new File(dstFolder+fileName);
					BufferedImage bi = (BufferedImage) icon.getImage();
					try {
						ImageIO.write(bi, fileType, out);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Icon getIcon(String name) throws MalformedURLException {
        Icon icon = null;
        URL url = new File(name).toURI().toURL();
        
        ImageIcon imgicon = null;
        BufferedImage scaledImage = null;
        try {
           // url = getClass().getResource(name);

            icon = new ImageIcon(url);
            if (icon == null) {
                System.out.println("Couldn't find " + url);
            }

            BufferedImage bi = new BufferedImage(
                    icon.getIconWidth(),
                    icon.getIconHeight(),
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = bi.createGraphics();
            // paint the Icon to the BufferedImage.
            icon.paintIcon(null, g, 0,0);
            g.dispose();

            bi = resizeImage(bi,130,130);
            scaledImage = bi;// or replace with this line Scalr.resize(bi, 30,30);
            imgicon = new ImageIcon(scaledImage);

        } catch (Exception e) {
            System.out.println(name);
            e.printStackTrace();
        }
        return imgicon;
    }

public static BufferedImage resizeImage (BufferedImage image, int areaWidth, int areaHeight) {
        float scaleX = (float) areaWidth / image.getWidth();
        float scaleY = (float) areaHeight / image.getHeight();
        float scale = Math.min(scaleX, scaleY);
        int w = Math.round(image.getWidth() * scale);
        int h = Math.round(image.getHeight() * scale);

        int type = image.getTransparency() == Transparency.OPAQUE ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;

        boolean scaleDown = scale < 1;

        if (scaleDown) {
            // multi-pass bilinear div 2
            int currentW = image.getWidth();
            int currentH = image.getHeight();
            BufferedImage resized = image;
            while (currentW > w || currentH > h) {
                currentW = Math.max(w, currentW / 2);
                currentH = Math.max(h, currentH / 2);

                BufferedImage temp = new BufferedImage(currentW, currentH, type);
                Graphics2D g2 = temp.createGraphics();
                g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g2.drawImage(resized, 0, 0, currentW, currentH, null);
                g2.dispose();
                resized = temp;
            }
            return resized;
        } else {
            Object hint = scale > 2 ? RenderingHints.VALUE_INTERPOLATION_BICUBIC : RenderingHints.VALUE_INTERPOLATION_BILINEAR;

            BufferedImage resized = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = resized.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(image, 0, 0, w, h, null);
            g2.dispose();
            return resized;
        }
    }


public static BufferedImage scaleImage(BufferedImage bufferedImage, int size) {
    double boundSize = size;
       int origWidth = bufferedImage.getWidth();
       int origHeight = bufferedImage.getHeight();
       double scale;
       if (origHeight > origWidth)
           scale = boundSize / origHeight;
       else
           scale = boundSize / origWidth;
        //* Don't scale up small images.
       if (scale > 1.0)
           return (bufferedImage);
       int scaledWidth = (int) (scale * origWidth);
       int scaledHeight = (int) (scale * origHeight);
       Image scaledImage = bufferedImage.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_SMOOTH);
       // new ImageIcon(image); // load image
       // scaledWidth = scaledImage.getWidth(null);
       // scaledHeight = scaledImage.getHeight(null);
       BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
       Graphics2D g = scaledBI.createGraphics();
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
       g.drawImage(scaledImage, 0, 0, null);
       g.dispose();
       return (scaledBI);
}


}