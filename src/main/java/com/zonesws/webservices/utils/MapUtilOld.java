package com.zonesws.webservices.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

import org.springframework.util.FileCopyUtils;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ExploreType;
import com.zonesws.webservices.jobs.SVGTextUtil;
import com.zonesws.webservices.utils.list.CrownJewelTicketList;
import com.zonesws.webservices.utils.list.LoyalFanStatus;



public class MapUtilOld {
	
	public static ZoneEvent setVenueZoneMap(ZoneEvent zoneEvent) throws Exception {
		zoneEvent.setMapPath("http://zonetickets.com/maps/"+zoneEvent.getVenueId()+"_"+zoneEvent.getVenueCategoryName()+".gif");
		return zoneEvent; 
	}
	
	public static Event getSvgWebViewUrl(Event event,String zones, ApplicationPlatForm platform) throws Exception {
		String configId = "RewardTheFan";
		String path = "";
		
		if(null != platform && platform.equals(ApplicationPlatForm.DESKTOP_SITE))
			path = URLUtil.rewardTheFanServerSvgMapsBaseUrl;
		else
			path = URLUtil.apiServerBaseUrl;
			
		event.setSvgWebViewUrl(path+"MobileSvgMapView.html?" +
				"configId="+configId+"&venueId="+event.getVenueId()+"&categoryGroupName="+event.getVenueCategoryName()
				+"&zones="+zones);
		   
		return event; 
	}
	
	/**
	 * 
	 * @param venueId
	 * @param venueCategory
	 * @param zones
	 * @param platform TODO
	 * @return
	 * @throws Exception
	 */
	public static String getSvgWebViewForOrder(Integer venueId, String venueCategory, String zones,Integer orderId,boolean fillAllZone) throws Exception{
		String configId = "RewardTheFan";
		String svgWebViewUrl = "";
		try {
			
			String path = URLUtil.apiServerBaseUrl;
			
			svgWebViewUrl = path+"MobileSvgMapView.html?" +
			"configId="+configId+"&venueId="+venueId+"&categoryGroupName="+venueCategory 
			+"&zones="+zones;
			if(orderId!=null){
				svgWebViewUrl += "&orderId="+orderId;
			}
			if(fillAllZone){
				svgWebViewUrl += "&fillAllZone="+fillAllZone;
			}
			return svgWebViewUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getSvgWebViewForOrderNew(Integer venueId, String venueCategory, String zones) throws Exception{
		String configId = "RewardTheFan";
		String svgWebViewUrl = "";
		try {
			
			String path = URLUtil.apiServerBaseUrl;
			
			svgWebViewUrl = path+"MobileSvgMapView.html?" +
			"configId="+configId+"&venueId="+venueId+"&categoryGroupName="+venueCategory 
			+"&zones="+zones;
			return svgWebViewUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static String getCustomerReferralPageStaticImage(){
		//Right now mobile app are using this . Desktop site we have added this image over the code
		return URLUtil.apiServerSvgMapsBaseURL+"RTF_Static_Images/RTF_REFERRAL_PAGE_STATIC_IMAGE_MOBILE.jpg";
	}
	
	public static String getRewardPointsConcertImage(){
		return URLUtil.apiServerSvgMapsBaseURL+"RTF_Static_Images/reward-points-concerts.png";
	}
	
	public static String getRewardPointsSportsImage(){
		return URLUtil.apiServerSvgMapsBaseURL+"RTF_Static_Images/reward-points-sports.png";
	}
	
	/*public static String getParentCategoryImages(String parentType){
		String imageName = null;
		if(parentType.equals("SPORTS")) {
			imageName = DAORegistry.getQueryManagerDAO().getLoyalFanImageNameByParentCategoryId(1);
		} else if (parentType.equals("CONCERTS")) {
			imageName = DAORegistry.getQueryManagerDAO().getLoyalFanImageNameByParentCategoryId(2);
		} else if (parentType.equals("THEATER")) {
			imageName = DAORegistry.getQueryManagerDAO().getLoyalFanImageNameByParentCategoryId(3);
		}
		return URLUtil.apiServerSvgMapsBaseURL+"LoyalFanImages/"+imageName;
	}*/
	public static void getLoyalFanStatusParentCategoryImages(LoyalFanStatus loyalFanStatus){

		Map<Integer,String> parentImageMap = DAORegistry.getQueryManagerDAO().getLoyalFanImageNameForParentCategories();
		for (Integer parentCategoryId : parentImageMap.keySet()) {
			if(parentCategoryId.equals(1)) {
				loyalFanStatus.setSportsImageUrl(URLUtil.apiServerSvgMapsBaseURL+"LoyalFanImages/"+parentImageMap.get(parentCategoryId));
			} else if(parentCategoryId.equals(2)) {
				loyalFanStatus.setConcertsImageUrl(URLUtil.apiServerSvgMapsBaseURL+"LoyalFanImages/"+parentImageMap.get(parentCategoryId));
			} else if(parentCategoryId.equals(3)) {
				loyalFanStatus.setTheaterImageUrl(URLUtil.apiServerSvgMapsBaseURL+"LoyalFanImages/"+parentImageMap.get(parentCategoryId));
			}
		}
	}
	
	public static String getFantasyGrandChildImages(String imageName){
		return URLUtil.apiServerSvgMapsBaseURL+"FantasyGrandChildCategoryImage/"+imageName;
	}
	
	
	/*public static ZoneEvent getEventSvgDetails(ZoneEvent zoneEvent){
		try{
			File svgTxtFile = new File(URLUtil.svgTextFilePath + zoneEvent.getVenueId()+"_"+zoneEvent.getVenueCategoryName()+".txt");
			if(svgTxtFile != null && svgTxtFile.exists()){
				zoneEvent.setIsMapWithSvg("true");
				FileReader fReader = new FileReader(svgTxtFile);
				BufferedReader reader = new BufferedReader(fReader);
				String svgText = "",str="";
				while((str = reader.readLine()) != null){
					svgText += str;
				}
				zoneEvent.setSvgText(replaceSVGText(svgText));
				zoneEvent.setSvgMapPath(URLUtil.apiServerSvgMapsBaseURL + zoneEvent.getVenueId()+"_"+zoneEvent.getVenueCategoryName() + ".gif");
				reader.close();
				fReader.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return zoneEvent;
	}*/
	
	public static String replaceSVGText(String svgText){
		
		String zoneBrodersArea = "", shapes = "",zoneLetters = "",sectionLetters = "";
		
		String[] textArray= svgText.split("</g>");
		
		for (String text : textArray) {
			if(text.contains("zone_borders")){
				zoneBrodersArea = text+"</g>";
			}else if(text.contains("shapes")){
				shapes = text+"</g>";
			}else if(text.contains("zone_letters")){
				zoneLetters = text+"</g>";
			}else if(text.contains("section_letters")){
				sectionLetters = text+"</g>";
			}
		}
		String finalContent=zoneBrodersArea+" "+shapes+" "+zoneLetters+" "+sectionLetters;
		return finalContent;
	}
	
	
	public static CrownJewelTicketList getSVGTextFromUtil(Event event,CrownJewelTicketList crownJewelTicketList,ApplicationPlatForm platform){
		try{
			String svgText = SVGTextUtil.getSvgTextDetails(event.getVenueId(), event.getVenueCategoryName());
			if(null == svgText ){
				//crownJewelTicketList.setIsMapWithSvg("false");
			}else{
				
				String path = "";
				
				if(null != platform && platform.equals(ApplicationPlatForm.DESKTOP_SITE))
					path = URLUtil.rewardTheFanServerSvgMapsBaseUrl;
				else
					path = URLUtil.apiServerSvgMapsBaseURL;
				
				crownJewelTicketList.setSvgText(replaceSVGText(svgText));
				crownJewelTicketList.setSvgMapPath(path + event.getVenueId()+"_"+event.getVenueCategoryName() + ".gif");
				//event.setIsMapWithSvg("true");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return crownJewelTicketList;
	}
	
	
	public static Event getSVGTextFromUtil(Event event, ApplicationPlatForm platform){
		try{
			String svgText = SVGTextUtil.getSvgTextDetails(event.getVenueId(), event.getVenueCategoryName());
			if(null == svgText ){
				event.setIsMapWithSvg("false");
			}else{
				String path = "";
				
				if(null != platform && platform.equals(ApplicationPlatForm.DESKTOP_SITE))
					path = URLUtil.rewardTheFanServerSvgMapsBaseUrl;
				else
					path = URLUtil.apiServerSvgMapsBaseURL;
				
				event.setSvgText(replaceSVGText(svgText));
				event.setSvgMapPath(path + event.getVenueId()+"_"+event.getVenueCategoryName() + ".gif");
				event.setIsMapWithSvg("true");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return event;
	}
	
	public static CustomerOrder getOrderSvgTextDetails(CustomerOrder customerOrder, ApplicationPlatForm platform){
		try{
			String svgText = SVGTextUtil.getOrderSvgTextDetails(customerOrder.getVenueId(), customerOrder.getVenueCategory(),customerOrder.getId());
			if(null == svgText ){
				customerOrder.setIsMapWithSvg("false");
			}else{
				String path = URLUtil.getStoredSvgApiUrl();
				customerOrder.setSvgText(replaceSVGText(svgText));
				customerOrder.setSvgMapPath(path + customerOrder.getVenueId()+"_"+customerOrder.getVenueCategory()+"_"+customerOrder.getId()+".gif");
				customerOrder.setIsMapWithSvg("true");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return customerOrder;
	}
	
	
	
	/*public static Event getSvgDetailsByEventTest(Event zoneEvent){
		try{
			File svgTxtFile = new File(URLUtil.svgTextFilePath + zoneEvent.getVenueId()+"_"+zoneEvent.getVenueCategoryName()+".txt");
			if(svgTxtFile != null && svgTxtFile.exists()){
				zoneEvent.setIsMapWithSvg("true");
				FileReader fReader = new FileReader(svgTxtFile);
				BufferedReader reader = new BufferedReader(fReader);
				String svgText = "",str="";
				while((str = reader.readLine()) != null){
					svgText += str;
				}
				zoneEvent.setSvgText(replaceSVGText(svgText));
				zoneEvent.setSvgMapPath(URLUtil.apiServerSvgMapsBaseURL + zoneEvent.getVenueId()+"_"+zoneEvent.getVenueCategoryName() + ".gif");
				reader.close();
				fReader.close();
			}else{
				zoneEvent.setIsMapWithSvg("false");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return zoneEvent;
	}*/
	
	
	/*public static Event getSvgDetailsByEvent(String venueId,String categoryGroupName){
		Event event = new Event();
		try{
			File svgTxtFile = new File(URLUtil.svgTextFilePath + venueId+"_"+categoryGroupName+".txt");
			if(svgTxtFile != null && svgTxtFile.exists()){
				event.setIsMapWithSvg("true");
				FileReader fReader = new FileReader(svgTxtFile);
				BufferedReader reader = new BufferedReader(fReader);
				String svgText = "",str="";
				while((str = reader.readLine()) != null){
					svgText += str;
				}
				event.setSvgText(replaceSVGText(svgText));
				event.setSvgMapPath(URLUtil.apiServerSvgMapsBaseURL + venueId+"_"+categoryGroupName + ".gif");
				reader.close();
				fReader.close();
			}else{
				event.setIsMapWithSvg("false");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return event;
	}*/
	
	
	public static Event getSVGTextFromUtil(String venueId,String categoryGroupName,Integer orderId){
		Event event = new Event();
		try{
			String svgText = null;
			if(orderId!=null){
				svgText = SVGTextUtil.getOrderSvgTextDetails(Integer.parseInt(venueId.trim()), categoryGroupName,orderId);
			}else{
				svgText =  SVGTextUtil.getSvgTextDetails(Integer.parseInt(venueId.trim()), categoryGroupName);
			}
			
			if(null == svgText){
				event.setIsMapWithSvg("false");
			}else{
				event.setSvgText(replaceSVGText(svgText));
				if(orderId!=null){
					event.setSvgMapPath(URLUtil.getStoredSVGMapPath() + venueId+"_"+categoryGroupName +"_"+orderId+".gif");
				}else{
					event.setSvgMapPath(URLUtil.apiServerSvgMapsBaseURL + venueId+"_"+categoryGroupName + ".gif");
				}
				event.setIsMapWithSvg("true");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return event;
	}
	
	
	public static Event testSvgDetailsByEvent(Event zoneEvent){
		try{
			Boolean isSvg =  SVGTextUtil.checkSvgTextFileExist(zoneEvent.getVenueId(), zoneEvent.getVenueCategoryName()); 
			if(isSvg){
				zoneEvent.setIsMapWithSvg("true");
			}else{
				zoneEvent.setIsMapWithSvg("false");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return zoneEvent;
	}
	
	/*public static Event getEventSvgDetails(Event event){
		try{
			File svgTxtFile = new File(URLUtil.svgTextFilePath + event.getVenueId()+"_"+event.getVenueCategoryName()+".txt");
			if(svgTxtFile != null && svgTxtFile.exists()){
				event.setIsMapWithSvg("true");
				FileReader fReader = new FileReader(svgTxtFile);
				BufferedReader reader = new BufferedReader(fReader);
				String svgText = "",str="";
				while((str = reader.readLine()) != null){
					svgText += str;
				}
				event.setSvgText(replaceSVGText(svgText));
				event.setSvgMapPath(URLUtil.apiServerSvgMapsBaseURL + event.getVenueId()+"_"+event.getVenueCategoryName() + ".gif");
				reader.close();
				fReader.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return event;
	}*/
	
	/*public static String getCardImage(Event event){
		String cardImage = "";
		try{
			if(event != null && event.getParentCategoryName() != null ){
				cardImage = URLUtil.apiServerSvgMapsBaseURL + "Cards/" + event.getParentCategoryName() + ".jpg";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}*/
	
	/*public static String getCardImageByCategoryName(String parentCategoryName){
		String cardImage = "";
		try{
			cardImage = URLUtil.apiServerSvgMapsBaseURL+"Cards/" + parentCategoryName + ".jpg";
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}*/
	
	/*public static void main(String[] args) {
		
		String imageurl = "C:/cards/REWARDTHEFAN/14.jpg";
		System.out.println(getCardImage(imageurl));
	}*/
	
	
	public static String getCardImage(String imageurl,ApplicationPlatForm platform){
		String cardImage="";
		try{
			String path = "";
			
			if(null != platform && platform.equals(ApplicationPlatForm.DESKTOP_SITE))
				path = URLUtil.rewardTheFanServerSvgMapsBaseUrl;
			else
				path = URLUtil.apiServerSvgMapsBaseURL;
			
			cardImage =path+"Cards/" + imageurl;
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	
	public static String getMainPromotionCardImage(String imageurl,String mobileImageurl,ApplicationPlatForm platform){
		String cardImage="";
		try{
			String path = "";
			if(null != platform && platform.equals(ApplicationPlatForm.DESKTOP_SITE)){
				path = URLUtil.rewardTheFanServerSvgMapsBaseUrl;
			}else{
				path = URLUtil.apiServerSvgMapsBaseURL;
				imageurl = mobileImageurl;
			}
			cardImage =path+"Cards/" + imageurl;
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	
	
	public static String getExploreCardImages(ExploreType exploreType,ApplicationPlatForm applicationPlatForm){
		String cardImage = "";
		try{
			String path = "";
			if(null != applicationPlatForm && applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE))
				path = URLUtil.rewardTheFanServerSvgMapsBaseUrl;
			else
				path = URLUtil.apiServerSvgMapsBaseURL;
			
			cardImage = path+"Cards/Explore/" + exploreType + ".jpg";
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	
	public static String getAboutUsImage(){
		String cardImage = "";
		try{
			cardImage = URLUtil.apiServerSvgMapsBaseURL+"AboutUsMap.png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	
	
	
	public static Boolean copySVGMapandText(Integer venueId, String venueCategoryName,Integer orderId) throws FileNotFoundException{
		try {
			String fileName = venueId+"_"+venueCategoryName;

			File svgTxtFile = new File(URLUtil.svgTextFilePath + venueId+"_"+venueCategoryName+".txt");
			if(svgTxtFile != null && svgTxtFile.exists()){
				FileInputStream inputStream = new FileInputStream(svgTxtFile);
				File newFile = new File(URLUtil.getStoredSVGMapPath()+fileName+"_"+orderId+".txt");
				newFile.createNewFile();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(newFile));
		        FileCopyUtils.copy(inputStream, stream);
				stream.close();
				inputStream.close();
			}
			
			svgTxtFile = new File(URLUtil.basePath+"SvgMaps//"+venueId+"_"+venueCategoryName+".gif");
			if(svgTxtFile != null && svgTxtFile.exists()){
				FileInputStream inputStream = new FileInputStream(svgTxtFile);
				File newFile = new File(URLUtil.getStoredSVGMapPath()+fileName+"_"+orderId+".gif");
				newFile.createNewFile();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(newFile));
		        FileCopyUtils.copy(inputStream, stream);
				stream.close();
				inputStream.close();
			}
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static Event getCustomerOrderSeatingChart(String venueId,String categoryGroupName){
		Event event = new Event();
		try{
			String svgText = SVGTextUtil.getOrderSvgTextDetails(Integer.parseInt(venueId.trim()), categoryGroupName,null);
			if(null == svgText ){
				event.setIsMapWithSvg("false");
			}else{
				event.setSvgText(replaceSVGText(svgText));
				event.setSvgMapPath(URLUtil.getStoredSvgApiUrl() + venueId+"_"+categoryGroupName + ".gif");
				event.setIsMapWithSvg("true");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return event;
	}
}
