package com.zonesws.webservices.utils;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.RTFCategorySearch;
import com.zonesws.webservices.enums.ParentType;
import com.zonesws.webservices.utils.list.GeneralisedSearchHeaders;





public class NormalSearchHeaders  {
	
	public static GeneralisedSearchHeaders getObjectOld(String artistLabelName,String eventLabelName,String catLabelName,String venueLabelName,
			Boolean showArtist,Boolean showEvent,Boolean showCategories,Boolean showVenue,GeneralisedSearchHeaders headerResponse){
		headerResponse.setShowArtistLabel(showArtist);
		headerResponse.setArtistLabelName(artistLabelName);
		headerResponse.setShowCategoriesLabel(true);
		headerResponse.setCategoriesLabelName(catLabelName);
		headerResponse.setShowEventLabel(true);
		headerResponse.setEventLabelName(eventLabelName);
		headerResponse.setShowVenueLabel(true);
		headerResponse.setVenueLabelName(venueLabelName);
		return headerResponse;
	}
	
	public static GeneralisedSearchHeaders getObject(Boolean showArtist,String artistLabelName,Boolean showEvent,String eventLabelName,
			Boolean showCategories,String catLabelName,Boolean showVenue,String venueLabelName,
			GeneralisedSearchHeaders headerResponse){
		headerResponse.setShowArtistLabel(showArtist);
		headerResponse.setArtistLabelName(artistLabelName);
		headerResponse.setShowCategoriesLabel(showCategories);
		headerResponse.setCategoriesLabelName(catLabelName);
		headerResponse.setShowEventLabel(showEvent);
		headerResponse.setEventLabelName(eventLabelName);
		headerResponse.setShowVenueLabel(showVenue);
		headerResponse.setVenueLabelName(venueLabelName);
		return headerResponse;
	}
	
	public static GeneralisedSearchHeaders getGeneralisedHeaders(GeneralisedSearchHeaders headerResponse, 
			Integer parentId,Integer childId,Integer grandChild,Integer rftCatSearchId,Integer artistId,Integer venueId,Integer eventId,
			String searchKey){
		
		String artistLabelName = "", eventLabelName="",catLabelName="",venueLabelName="";
		Boolean showArtist=false,showEvent=false,showCategories=false,showVenue=false;
		
		getObject(showArtist, artistLabelName, showEvent, eventLabelName, showCategories, catLabelName, showVenue, venueLabelName, headerResponse);
		
		if(null != parentId){
			if(parentId.equals(1)){
				getObject(true, "TEAM or EVENT", true, "ALL EVENTS BY DATE", true, "SPORTS CATEGORIES", true, "VENUES", headerResponse);
				return headerResponse;
			}else if(parentId.equals(2)){
				getObject(true, "LIST OF CONCERTS", true, "ALL CONCERTS BY DATE", false, "", true, "VENUES", headerResponse);
				return headerResponse;
			}else if(parentId.equals(3)){
				getObject(true, "LIST OF SHOWS", true, "ALL SHOWS BY DATE", false, "", true, "VENUES", headerResponse);
				return headerResponse;
			}else if(parentId.equals(4)){
				getObject(true, "LIST OF SHOWS", true, "ALL SHOWS BY DATE", false, "", true, "VENUES", headerResponse);
				return headerResponse;
			}
		}else if(null != childId){
			getObject(true, "LIST OF SHOWS", true, "ALL SHOWS BY DATE", false, "", true, "VENUES", headerResponse);
			return headerResponse;
		}else if(null != grandChild){
			getObject(true, "TEAM or EVENT", true, "ALL EVENTS BY DATE", false, "", true, "VENUES", headerResponse);
			return headerResponse;
		}else if(null != rftCatSearchId ){
			getObject(true, "TEAM or EVENT", true, "ALL EVENTS BY DATE", false, "", true, "VENUES", headerResponse);
			return headerResponse;
		}else if(null != artistId){
			getObject(false, "", true, "ALL EVENTS BY DATE", false, "", false, "", headerResponse);
			return headerResponse;
		}else if(null != venueId){
			getObject(false, "", true, "ALL EVENTS BY DATE", false, "", false, "", headerResponse);
			return headerResponse;
			
		}else if(null != eventId){
			getObject(false, "", true, "ALL EVENTS BY DATE", false, "", false, "", headerResponse);
			return headerResponse;
			
		}else{
			boolean keyIsNotParent = true;
			String tempKey = searchKey.toUpperCase().trim();
			for (ParentType parentType : ParentType.values()) {
				if(!keyIsNotParent){
					break;
				}
				switch (parentType) {
				
					case SPORT:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 1, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case SPORTS:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 1, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case CONCERT:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 2, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case CONCERTS:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 2, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case THEATER:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 3, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;

						
					case THEATERS:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 3, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case THEATRE:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 3, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;

						
					case THEATRES:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 3, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;

						
					case FAMILY:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 3, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case FAMILYS:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 3, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case FAMILIES:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 3, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case CHILD:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 4, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case CHILDREN:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 4, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;
						
					case CHILDRENS:
						if(tempKey.equalsIgnoreCase(parentType.toString())){
							keyIsNotParent = false;
							getGeneralisedHeaders(headerResponse, 1, null, null, null, null, null,null, null);
							return headerResponse;
						}
						break;

					default:
						break;
				}
			}
			if(keyIsNotParent){
				RTFCategorySearch searchObj = RTFCategorySearchUtil.getRTFCategoryBySearchKey(searchKey);
				if(null != searchObj){
					getObject(true, "TEAM or EVENT", true, "ALL EVENTS BY DATE", false, "", true, "VENUES", headerResponse);
					return headerResponse;
				}
				searchKey = searchKey.replaceAll("\\W", "%");
				
				parentId = DAORegistry.getQueryManagerDAO().getParentCategoryIdBySearchKey(searchKey);
				if(null != parentId){
					if(parentId.equals(1)){
						getObject(true, "TEAM or EVENT", true, "ALL EVENTS BY DATE", false, "", true, "VENUES", headerResponse);
						return headerResponse;
					}
					getGeneralisedHeaders(headerResponse, parentId, null, null, null, null, null,null, null);
					return headerResponse;
				}
				
				parentId = DAORegistry.getQueryManagerDAO().getParentCatIdByGrandChildKey(searchKey);
				if(null != parentId){
					if(parentId.equals(1)){
						getObject(true, "TEAM or EVENT", true, "ALL EVENTS BY DATE", false, "", true, "VENUES", headerResponse);
						return headerResponse;
					}
					getGeneralisedHeaders(headerResponse, parentId, null, null, null, null, null,null, null);
					return headerResponse;
				}
				
				getObject(true, "LIST OF EVENTS", true, "ALL EVENTS BY DATE", false, "", true, "VENUES", headerResponse);
				return headerResponse;
			}
		}
		return headerResponse;
	}
	
	}
