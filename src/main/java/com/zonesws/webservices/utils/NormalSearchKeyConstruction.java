package com.zonesws.webservices.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.RTFCategorySearch;
import com.zonesws.webservices.data.Venue;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.enums.TabType;
import com.zonesws.webservices.jobs.UpcomingEventUtil;
import com.zonesws.webservices.utils.list.AutoPageResult;
import com.zonesws.webservices.utils.list.VenueResult;




/**
 * Search Event Construction to get the all active event artist details
 * @author kulaganathan
 *
 */
public class NormalSearchKeyConstruction {
	
	
	/*public static HashMap<String, Object> getNormalSearchKeyResults(HashMap<String, Object> allObj,String searchKey,
			Integer childId,Integer grandChildId,Integer artistId,Integer venueId,
			Integer pageNumber,Integer artistPageNumber,Integer venuePageNumber, Integer maxRow,String startDateStr,
			String endDateStr,String state,String searchType,TabType tabType,Boolean isLocationFilterApplied ,
			Boolean dateFilterApplied, Integer eventExcludeDays, Boolean isOtherGrid,String artistRefValue,ArtistReferenceType artistRefType ){
		
		if(searchType.equals("AUTOSEARCH")){
			
			AutoPageResult autoPageResult = null;
			ArrayList<AutoPageResult> autoPageResultList = new ArrayList<AutoPageResult>();
			
			List<Artist> artists = RTFNormalSearchUtil.getAllArtistList();
			
			searchKey = searchKey.replaceAll("\\W", "").toLowerCase();
			
			int i = 1,maxSize = PaginationUtil.autoSearchMaxRows;
			for (Artist artist : artists) {
				
				if(i > maxSize){
					break;
				}
				try{
					if(artist.getName().replaceAll("\\W", "").toLowerCase().contains(searchKey)){
						autoPageResult = new AutoPageResult();
						autoPageResult.setIsVenue(false);
						autoPageResult.setIsArtist(true);
						autoPageResult.setId(artist.getId());
						autoPageResult.setName(artist.getName());
						autoPageResultList.add(autoPageResult);
						i++;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(i <  maxSize){
				
				List<Venue> venues = RTFNormalSearchUtil.getAllVenueList();
				for (Venue venue : venues) {
					
					if(i > maxSize){
						break;
					}
					try{
						if(venue.getBuilding().replaceAll("\\W", "").toLowerCase().contains(searchKey)){
							autoPageResult = new AutoPageResult();
							autoPageResult.setIsVenue(true);
							autoPageResult.setIsArtist(false);
							autoPageResult.setId(venue.getId());
							autoPageResult.setName(venue.getBuilding());
							autoPageResultList.add(autoPageResult);
							i++;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			allObj.put("autoPageResultList", autoPageResultList);
		}else{
			
			searchKey = searchKey.replaceAll("\\W", "").toLowerCase();
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Date fromDate = null,toDate = null;
			if(dateFilterApplied){
				try{
					fromDate = df.parse(startDateStr);
					toDate = df.parse(endDateStr);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			switch (tabType) {
			
				case ALL:
					allObj = getAllFilteredEventsByPagination(allObj, searchKey, pageNumber, maxRow, fromDate, toDate, 
							state, isLocationFilterApplied, dateFilterApplied, isOtherGrid, df,artistId,venueId,artistRefValue,artistRefType);
					allObj = getAllFilteredArtistsByPagination(allObj, artistPageNumber, maxRow, searchKey, isLocationFilterApplied, dateFilterApplied,
							state, fromDate, toDate, df);
					allObj = getAllFilteredVenuesByPagination(allObj, venuePageNumber, maxRow, searchKey, isLocationFilterApplied, state);
					break;
					
				case EVENTS:
					allObj = getAllFilteredEventsByPagination(allObj, searchKey, pageNumber, maxRow, fromDate, toDate, 
							state, isLocationFilterApplied, dateFilterApplied, isOtherGrid, df,artistId,venueId,artistRefValue,artistRefType);
					break;
					
				case VENUE:
					allObj = getAllFilteredVenuesByPagination(allObj, venuePageNumber, maxRow, searchKey, isLocationFilterApplied, state);
					break;
					
				case ARTIST:
					allObj = getAllFilteredArtistsByPagination(allObj, artistPageNumber, maxRow, searchKey, isLocationFilterApplied, dateFilterApplied,
							state, fromDate, toDate, df);
					break;
	
				default:
					break;
			}
			
		}
		return allObj;
	}
	*/
	public static HashMap<String, Object> getAllFilteredVenuesByPagination(HashMap<String, Object> allObj,Integer venuePageNumber,
			Integer maxRow,String searchKey,Boolean isLocationFilterApplied ,String state){
		
		ArrayList<VenueResult> venueResults = getFilteredVenues(searchKey, isLocationFilterApplied, state);
		
		Integer fromIndex = 0, toIndex =0;
		boolean showMore = false;
		if(null != venueResults && venueResults.size() > 0){
			
			fromIndex = (venuePageNumber-1)* maxRow;
			toIndex = fromIndex + maxRow;
			
			if(fromIndex > venueResults.size()-1){
				allObj.put("venue", null);
			}else{
				showMore = true;
				if(toIndex > venueResults.size()-1){
					toIndex = venueResults.size();
					showMore = false;
				}
				List<VenueResult> splitedList = venueResults.subList(fromIndex, toIndex);
				allObj.put("venue", new ArrayList<VenueResult>(splitedList));
			}
		}
		allObj.put("showMoreVenues", showMore);
		return allObj;
	}
	
	public static ArrayList<VenueResult> getFilteredVenues(String searchKey,Boolean isLocationFilterApplied, String state){
		
		VenueResult venueResult = null;
		ArrayList<VenueResult> searchVenueList = new ArrayList<VenueResult>();
		List<Venue> venues = RTFNormalSearchUtil.getAllVenueList();
		
		for (Venue venue : venues) {
			try{
				if(venue.getBuilding().replaceAll("\\W", "").toLowerCase().contains(searchKey)){
					
					if(isLocationFilterApplied ){
						
						if(venue.getState().toLowerCase().equalsIgnoreCase(state.toLowerCase())){
							venueResult = new VenueResult();
							venueResult.setVenueId(venue.getId());
							venueResult.setVenueName(venue.getBuilding());
							venueResult.setVenueCity(venue.getCity());
							venueResult.setVenueState(venue.getState());
							venueResult.setVenueCountry(venue.getCountry());
							searchVenueList.add(venueResult);
						}
					}else{
						venueResult = new VenueResult();
						venueResult.setVenueId(venue.getId());
						venueResult.setVenueName(venue.getBuilding());
						venueResult.setVenueCity(venue.getCity());
						venueResult.setVenueState(venue.getState());
						venueResult.setVenueCountry(venue.getCountry());
						searchVenueList.add(venueResult);
					}
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return searchVenueList;
	}
	
	/*public static HashMap<String, Object> getAllFilteredArtistsByPagination(HashMap<String, Object> allObj,Integer artistPageNumber,
			Integer maxRow,String searchKey,Boolean isLocationFilterApplied ,Boolean dateFilterApplied,String state, Date fromDate, 
			Date toDate,DateFormat df){
		
		ArrayList<Artist> artists = getFilteredArtist(searchKey, isLocationFilterApplied, dateFilterApplied, state, fromDate, toDate, df);
		
		boolean showMore = false;
		Integer fromIndex = 0, toIndex =0;
		if(null != artists && artists.size() > 0){
			
			if(artists == null || artists.isEmpty()){
				allObj.put("showMoreArtists", showMore);
				return allObj;
			}
			fromIndex = (artistPageNumber-1)* maxRow;
			toIndex = fromIndex + maxRow;
			
			if(fromIndex > artists.size()-1){
				allObj.put("artist", null);
			}else{
				showMore = true;
				if(toIndex > artists.size()-1){
					toIndex = artists.size();
					showMore = false;
				}
				List<Artist> splitedList = artists.subList(fromIndex, toIndex);
				allObj.put("artist", new ArrayList<Artist>(splitedList));
			}
		}
		allObj.put("showMoreArtists", showMore);
		return allObj;
	}
	*/
	/*public static ArrayList<Artist> getFilteredArtist(String searchKey,Boolean isLocationFilterApplied ,
			Boolean dateFilterApplied,String state, Date fromDate, Date toDate,DateFormat df){
		
		List<Artist> artists = RTFNormalSearchUtil.getAllArtistList();
		ArrayList<Artist> searchArtistList = new ArrayList<Artist>();
		
		Set<Integer> stateArtistIds = null;
		
		if(isLocationFilterApplied){
			stateArtistIds = RTFSearchCacheUtil.artistIdsByState.get(state.toLowerCase());
			if(null == stateArtistIds || stateArtistIds.isEmpty() ){
				stateArtistIds = new HashSet<Integer>();
			}
		}
		
		
		Date artistStartDate = null, artistEndDate = null;
		
		for (Artist artist : artists) {
			
			try{
				
				if(artist.getName().replaceAll("\\W", "").toLowerCase().contains(searchKey)){
					
					if(isLocationFilterApplied && dateFilterApplied){
						
						if(stateArtistIds.contains(artist.getId())){
							
							if(null != artist.getFromDate() && null != artist.getToDate()){
								
								artistStartDate = new Date(artist.getFromDate().getTime());
								artistEndDate = new Date(artist.getToDate().getTime());
								
								if((fromDate.compareTo(artistStartDate)<=0 && 
										toDate.compareTo(artistStartDate)>=0) || (fromDate.compareTo(artistEndDate)<=0 && 
												toDate.compareTo(artistEndDate)>=0)) {
									searchArtistList.add(artist);
								}
							}
						}
						
					}else if(dateFilterApplied){
						
						if(null != artist.getFromDate() && null != artist.getToDate()){
							
							artistStartDate = new Date(artist.getFromDate().getTime());
							artistEndDate = new Date(artist.getToDate().getTime());
							
							if((fromDate.compareTo(artistStartDate)<=0 && 
									toDate.compareTo(artistStartDate)>=0) || (fromDate.compareTo(artistEndDate)<=0 && 
											toDate.compareTo(artistEndDate)>=0)) {
								searchArtistList.add(artist);
							}
						}
						
					}else if(isLocationFilterApplied){
						
						if(stateArtistIds.contains(artist.getId())){
							searchArtistList.add(artist);
						}
						
					}else{
						searchArtistList.add(artist);
						
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return searchArtistList;
		
	}*/
	
	public static HashMap<String, Object> getAllFilteredEventsByPagination(HashMap<String, Object> allObj,String searchKey,
			Integer pageNumber,Integer maxRow,Date fromDate,Date toDate, String state,Boolean isLocationFilterApplied , 
			Boolean dateFilterApplied, Boolean isOtherGrid , DateFormat df,Integer artistId , Integer venueId,String artistRefValue,ArtistReferenceType artistRefType){
		
		Map<String, List<Event>> eventsMap = null;
		
		List<Event> events = RTFNormalSearchUtil.allEventList;
		boolean showEventsMore = false,showOtherEventMore= false,showOtherGrid= false; 
		Integer fromIndex=0, toIndex=0;
		int maxRowOther = PaginationUtil.otherEventGridMaxRows;
		
		if(null != events && !events.isEmpty()){
			
			eventsMap = getFilteredEvents(events, searchKey, isLocationFilterApplied, dateFilterApplied, state, fromDate, toDate, df,
					artistId ,venueId,artistRefValue,artistRefType);
			
			List<Event> filteredEvents = eventsMap.get("filteredEvents");
			
			if(null != filteredEvents && filteredEvents.size() > 0){
				
				Collections.sort(filteredEvents, UpcomingEventUtil.eventComparatorFordate);
				int size = filteredEvents.size();
				boolean enableOtherGrid = false;
				if(size <= PaginationUtil.eventGridThreshold && !isOtherGrid){
					enableOtherGrid = true;
				}
				
				if(enableOtherGrid){
					
					if(isLocationFilterApplied || dateFilterApplied){
						showOtherGrid = true;
						List<Event> unFilteredEvents = eventsMap.get("unFilteredEvents");
						fromIndex = (pageNumber-1)* maxRowOther;
						toIndex = fromIndex + maxRowOther;
						
						if(fromIndex > unFilteredEvents.size()-1){
							allObj.put("OtherEvents", null);
						}else{
							showOtherEventMore = true;
							if(toIndex > unFilteredEvents.size()-1){
								toIndex = unFilteredEvents.size();
								showOtherEventMore = false;
							}
							List<Event> splitedList = unFilteredEvents.subList(fromIndex, toIndex);
							allObj.put("OtherEvents", new ArrayList<Event>(splitedList));
						}
					}
					
					if(size <= 0){
						allObj.put("showOtherEventGrid", showOtherGrid);
						allObj.put("showMoreOtherEvents", showOtherEventMore);
						allObj.put("showMoreEvents", showEventsMore);
						return allObj;
					}
					
					fromIndex = (pageNumber-1)* maxRow;
					toIndex = fromIndex + maxRow;
					
					if(fromIndex > filteredEvents.size()-1){
						allObj.put("event", null);
					}else{
						showEventsMore = true;
						if(toIndex > filteredEvents.size()-1){
							toIndex = filteredEvents.size();
							showEventsMore = false;
						}
						List<Event> splitedList = filteredEvents.subList(fromIndex, toIndex);
						allObj.put("event", new ArrayList<Event>(splitedList));
					}
					
					allObj.put("showOtherEventGrid", showOtherGrid);
					allObj.put("showMoreOtherEvents", showOtherEventMore);
					allObj.put("showMoreEvents", showEventsMore);
					
				}else if(isOtherGrid){
					
					List<Event> unFilteredEvents = eventsMap.get("unFilteredEvents");
					
					fromIndex = (pageNumber-1)* maxRowOther;
					toIndex = fromIndex + maxRowOther;
					
					if(fromIndex > unFilteredEvents.size()-1){
						allObj.put("OtherEvents", null);
					}else{
						showEventsMore = true;
						if(toIndex > unFilteredEvents.size()-1){
							toIndex = unFilteredEvents.size();
							showEventsMore = false;
						}
						List<Event> splitedList = unFilteredEvents.subList(fromIndex, toIndex);
						allObj.put("OtherEvents", new ArrayList<Event>(splitedList));
					}
					
					allObj.put("showOtherEventGrid", true);
					allObj.put("showMoreOtherEvents", showEventsMore);
					allObj.put("showMoreEvents", false);
					
				}else{
					
					fromIndex = (pageNumber-1)* maxRow;
					toIndex = fromIndex + maxRow;
					
					if(fromIndex > filteredEvents.size()-1){
						allObj.put("event", null);
					}else{
						showEventsMore = true;
						if(toIndex > filteredEvents.size()-1){
							toIndex = filteredEvents.size();
							showEventsMore = false;
						}
						List<Event> splitedList = filteredEvents.subList(fromIndex, toIndex);
						allObj.put("event", new ArrayList<Event>(splitedList));
					}
					
					allObj.put("showOtherEventGrid", false);
					allObj.put("showMoreOtherEvents", false);
					allObj.put("showMoreEvents", showEventsMore);
				}
			}else if(isLocationFilterApplied || dateFilterApplied){
				
				showOtherGrid = true;
				List<Event> unFilteredEvents = eventsMap.get("unFilteredEvents");
				
				fromIndex = (pageNumber-1)* maxRowOther;
				toIndex = fromIndex + maxRowOther;
				
				if(fromIndex > unFilteredEvents.size()-1){
					allObj.put("OtherEvents", null);
				}else{
					showEventsMore = true;
					if(toIndex > unFilteredEvents.size()-1){
						toIndex = unFilteredEvents.size();
						showEventsMore = false;
					}
					List<Event> splitedList = unFilteredEvents.subList(fromIndex, toIndex);
					allObj.put("OtherEvents", new ArrayList<Event>(splitedList));
				}
				
				allObj.put("showOtherEventGrid", true);
				allObj.put("showMoreOtherEvents", showEventsMore);
				allObj.put("showMoreEvents", false);
			}
		}
		return allObj;
	}
	
	public static boolean isMatchingEvent(Integer artistId , Integer venueId,String searchKey,Event event){
		
		if(null != artistId ){
			
			List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
			
			
			if((null != artistIds && !artistIds.isEmpty()) && artistIds.contains(artistId)){
				return true;
			} else {
				String eventName = event.getEventName().replaceAll("\\W", "").toLowerCase();
				if(eventName.toLowerCase().contains(searchKey) || eventName.equalsIgnoreCase(searchKey)){
					return true;
				}
			}
			
		}else if(null != venueId){
			
			if(event.getVenueId().equals(venueId)){
				return true;
			}
			
		}else{
			String eventName = event.getEventName().replaceAll("\\W", "").toLowerCase();
			if(eventName.toLowerCase().contains(searchKey) || eventName.equalsIgnoreCase(searchKey)){
				return true;
			}
		}
		return false;
	}
public static boolean isArtistReferenceMatchingEvent(Integer artistId ,Event event,Integer aParentId,
		Integer aChildId,Integer aGrandChildId,String aParentName,String aChildName,String aGrandChildName){
		
			if(null != artistId){
				boolean flag = true;
				if(aParentId != null) {
					if( event.getParentCategoryId() != null && aParentId.equals(event.getParentCategoryId())) {
						flag = true;	
					} else {
						flag = false;
					}
				} 
				if(aChildId != null) {
					if( event.getChildCategoryId() != null && aChildId.equals(event.getChildCategoryId())) {
						flag = true;	
					} else {
						flag = false;
					}
				}
				if(aGrandChildId != null) {
					if( event.getGrandChildCategoryId() != null && aGrandChildId.equals(event.getGrandChildCategoryId())) {
						flag = true;	
					} else {
						flag = false;
					}
				}
				if(aParentName != null) {
					if( event.getParentCategoryName() != null && aParentName.equalsIgnoreCase(event.getParentCategoryName())) {
						flag = true;	
					} else {
						flag = false;
					}
				} 
				if(aChildName != null) {
					if( event.getChildCategoryName() != null && aChildName.equals(event.getChildCategoryName())) {
						flag = true;	
					} else {
						flag = false;
					}
				}
				if(aGrandChildName != null) {
					if( event.getGrandChildCategoryName() != null && aGrandChildName.equals(event.getGrandChildCategoryName())) {
						flag = true;	
					} else {
						flag = false;
					}
				}
				return flag;
			}
			return true;
			
	}
	
	public static Map<String, List<Event>> getFilteredEvents(List<Event> events,String searchKey,Boolean isLocationFilterApplied ,
			Boolean dateFilterApplied,String state, Date fromDate, Date toDate,DateFormat df,Integer artistId , Integer venueId,String artistRefValue,ArtistReferenceType artistRefType){
		Map<String, List<Event>> map = new HashMap<String, List<Event>>();
		List<Event> filteredEvents = new ArrayList<Event>();
		List<Event> unfilteredEvents = new ArrayList<Event>();
		Date eventDate = null;
		
		Integer aParentId=null,aChildId=null,aGrandChildId=null;
		String aParentName = null,aChildName=null,aGrandChildName=null;
		if(artistRefValue != null && artistRefType != null) {
			switch (artistRefType) {
			case PARENT_ID:
				aParentId= Integer.parseInt(artistRefValue);
				break;
			case CHILD_ID:
				aChildId= Integer.parseInt(artistRefValue);
				break;
			case GRANDCHILD_ID:
				aGrandChildId= Integer.parseInt(artistRefValue);
				break;
			case RTFCATS_ID:
				//artfCatSearchId= artistRefId;
				RTFCategorySearch rtfCategorySearch = RTFCategorySearchUtil.getRTFCategoryById(Integer.parseInt(artistRefValue));
				if(rtfCategorySearch != null) {
					aChildId = rtfCategorySearch.getChildId();
					aGrandChildId = rtfCategorySearch.getGrandChildId();
				}
				break;

			case PARENT_NAME:
				aParentName= artistRefValue;
				break;
			case CHILD_NAME:
				aChildName= artistRefValue;
				break;
			case GRANDCHILD_NAME:
				aGrandChildName= artistRefValue;
				break;
			case RTFCATS_NAME:
				//artfCatSearchId= artistRefId;
				RTFCategorySearch rtfCategorySearchName = RTFCategorySearchUtil.getRTFCategoryBySearchKey(artistRefValue);
				if(rtfCategorySearchName != null) {
					aChildId = rtfCategorySearchName.getChildId();
					aGrandChildId = rtfCategorySearchName.getGrandChildId();
				}
				break;
	
			}
		}
		
		for (Event event : events) {
			
			//if artist id is not null then check all possiblity ref type conditions
			if(artistId != null && !isArtistReferenceMatchingEvent(artistId, event, aParentId, aChildId, aGrandChildId,aParentName,aChildName,aGrandChildName)) {
				continue;
			}
			
			//if artist id and venue id is null and search key is not null and grand child id is not null and load events with search key and grand child ids
			if(artistId == null && venueId == null && aGrandChildId != null) {
				
				if(event.getGrandChildCategoryId() != null && !event.getGrandChildCategoryId().equals(aGrandChildId)) {
					continue;
				}
			}
			
			if(isMatchingEvent(artistId, venueId, searchKey, event)){
				
				if(isLocationFilterApplied && dateFilterApplied){
					try{
						eventDate = df.parse(event.getEventDateStr());
						if(event.getState().toLowerCase().equalsIgnoreCase(state.toLowerCase()) 
								&& fromDate.equals(eventDate) || toDate.equals(eventDate) || 
								(eventDate.after(fromDate) && eventDate.before(toDate) )){
							filteredEvents.add(event);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(isLocationFilterApplied){
					if(event.getState().toLowerCase().equalsIgnoreCase(state.toLowerCase())){
						filteredEvents.add(event);
					}
				}else if(dateFilterApplied){
					try{
						eventDate = df.parse(event.getEventDateStr());
						if(fromDate.equals(eventDate) || toDate.equals(eventDate) || 
								(eventDate.after(fromDate) && eventDate.before(toDate) )){
							filteredEvents.add(event);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else{
					filteredEvents.add(event);
				}
				unfilteredEvents.add(event);
			}
		}
		map.put("filteredEvents", filteredEvents);
		map.put("unFilteredEvents", unfilteredEvents);
		
		return map;
		
	}
	
	public static HashMap<String, Object> getOtherLocationEvents(HashMap<String, Object> allObj,ArrayList<Integer> eventIds,ArrayList<Event> otherEventList,
			String searchKey,Boolean isLocationFilter,String state, Integer pageNumber, Integer maxRow){
		try{
			
			if(null == otherEventList || otherEventList.isEmpty()){
				allObj.put("OtherEvents", null);
				allObj.put("showMoreOtherEvents", false);
			}
			
			ArrayList<Event> allEvents = new ArrayList<Event>();
			Integer totalEventCount = 0 , currRows = pageNumber * maxRow;
			for (Event eventObj : otherEventList) {
				if(!eventIds.contains(eventObj.getEventId())){
					totalEventCount = eventObj.getTotalEvents();
					allEvents.add(eventObj);
				}
			}
			
			if(totalEventCount > currRows){
				allObj.put("showMoreOtherEvents", true);
			}else{
				allObj.put("showMoreOtherEvents", false);
			}
			allObj.put("OtherEvents", allEvents);
		}catch (Exception e) {
			allObj.put("OtherEvents", otherEventList);
			allObj.put("showOtherEventGrid", false);
			allObj.put("showMoreOtherEvents", false);
			e.printStackTrace();
		}
		return allObj;
	}
	
	
	public static HashMap<String, Object> getPaginationDetails(HashMap<String, Object> allObj, Integer pageNumber,Integer artistPageNumber,Integer venuePageNumber, Integer maxRow){
		
		Integer totalEvents = allObj.get("totalEvents") != null ? (Integer) (Object) allObj.get("totalEvents") : 0;
		Integer totalArtists = allObj.get("totalArtists") != null ? (Integer) (Object) allObj.get("totalArtists") : 0;
		Integer totalVenues = allObj.get("totalVenues") != null ? (Integer) (Object) allObj.get("totalVenues") : 0;
		
		Integer curCompletedEventRows = pageNumber * maxRow;
		Integer curCompletedVenuesRows = venuePageNumber * maxRow;
		Integer curCompletedArtistRows = artistPageNumber * maxRow;
		
		if(totalEvents > curCompletedEventRows){
			allObj.put("showMoreEvents", true);
		}else{
			allObj.put("showMoreEvents", false);
		}
		
		if(totalVenues > curCompletedVenuesRows){
			allObj.put("showMoreVenues", true);
		}else{
			allObj.put("showMoreVenues", false);
		}
		
		if(totalArtists > curCompletedArtistRows){
			allObj.put("showMoreArtists", true);
		}else{
			allObj.put("showMoreArtists", false);
		}
		
		return allObj;
	}
	
	
	
	
}
