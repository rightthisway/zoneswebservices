package com.zonesws.webservices.utils;

import org.springframework.beans.factory.InitializingBean;


public class NotificationProperty implements InitializingBean {
	
	private String apnsp12CertificatePath;
	private String apnsCertificatePassword;
	private String gcmSenderKey;
	private String fcmSenderKey;
	
	
	public String getApnsp12CertificatePath() {
		return apnsp12CertificatePath;
	}
	public void setApnsp12CertificatePath(String apnsp12CertificatePath) {
		this.apnsp12CertificatePath = apnsp12CertificatePath;
	}
	public String getApnsCertificatePassword() {
		return apnsCertificatePassword;
	}
	public void setApnsCertificatePassword(String apnsCertificatePassword) {
		this.apnsCertificatePassword = apnsCertificatePassword;
	}
	public void afterPropertiesSet() throws Exception {
		
	}
	public String getGcmSenderKey() {
		return gcmSenderKey;
	}
	public void setGcmSenderKey(String gcmSenderKey) {
		this.gcmSenderKey = gcmSenderKey;
	}
	public String getFcmSenderKey() {
		return fcmSenderKey;
	}
	public void setFcmSenderKey(String fcmSenderKey) {
		this.fcmSenderKey = fcmSenderKey;
	}
	
	
}
