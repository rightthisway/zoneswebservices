package com.zonesws.webservices.utils;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Property;


public class PaginationScheduler  extends QuartzJobBean implements StatefulJob {
	 
	
	public void init() throws Exception{
		
		Integer maxCountPerPage = 100; 
		
		Property property = DAORegistry.getPropertyDAO().get("zws.normal.search.event.threshold");
		
		if(null != property) {
			maxCountPerPage = Integer.valueOf(property.getValue());
		}
		
		/*if(maxCountPerPage > 0) {
			QuizContestUtil.contestSummaryDataSize = maxCountPerPage;
		}*/
		
	}
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			if(URLUtil.isProductionEnvironment & URLUtil.isMasterNodeServer) {
				init();
				System.out.println("Inside Start Cont Not If : "+ new Date());
			} else {
				System.out.println("Inside Start Cont Not Else : "+ new Date());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
