package com.zonesws.webservices.utils;

import com.zonesws.webservices.dao.implementaion.DAORegistry;


public class PaginationUtil {
	public static Integer eventMaxRows = Integer.valueOf(DAORegistry.getPropertyDAO().get("zws.pagination.count").getValue());
	public static Integer eventCardsMaxRows = Integer.valueOf(DAORegistry.getPropertyDAO().get("zws.cards.pagination.count").getValue());
	public static Integer normalSearchEventMaxRows = Integer.valueOf(DAORegistry.getPropertyDAO().get("zws.normal.search.pagination.count").getValue());
	public static Integer autoSearchMaxRows = Integer.valueOf(DAORegistry.getPropertyDAO().get("zws.autosearch.max.count").getValue());
	public static Integer otherEventGridMaxRows = Integer.valueOf(DAORegistry.getPropertyDAO().get("zws.normal.search.other.event.count").getValue());;
	public static Integer excludeEventDays = Integer.valueOf(DAORegistry.getPropertyDAO().get("zws.event.exclude.days").getValue());
	public static Integer eventGridThreshold = Integer.valueOf(DAORegistry.getPropertyDAO().get("zws.normal.search.event.threshold").getValue());;
	public static Integer cacheMaxResultCount = Integer.valueOf("10000");
	public static Double loyalFanPercentage = Double.valueOf(DAORegistry.getPropertyDAO().get("rtf.loyalfan.discount.percentage").getValue());
	public static String referralCodePrefix = DAORegistry.getPropertyDAO().get("rtf.customer.referral.code.prefix").getValue();
	public static String partialRewardPaymentPrefix = DAORegistry.getPropertyDAO().get("rtf.partial.reward.payment.prefix").getValue();
	public static String fullRewardPaymentPrefix = DAORegistry.getPropertyDAO().get("rtf.full.reward.payment.prefix").getValue();
	public static String walletCreditTrxPrefix = DAORegistry.getPropertyDAO().get("rtf.customer.wallet.trx.credit.prefix").getValue();
	public static String walletDebitTrxPrefix = DAORegistry.getPropertyDAO().get("rtf.customer.wallet.trx.debit.prefix").getValue();
	public static String acctReceivablePaymentPrefix = DAORegistry.getPropertyDAO().get("rtf.acct.receivable.payment.prefix").getValue();
	public static Double onDayBeforeEventTicketDownloadRewardOffer =0.05;
	public static String contestOrderPaymentPrefix = DAORegistry.getPropertyDAO().get("rtf.contest.order.payment.prefix").getValue();
	public static Integer contestTicketsMaxRow = 30;
	
	
	public static Integer getTotalNoOfPages(Integer totalRowCounts , Integer maxRowsPerPage) throws Exception {
		Integer noOfPages = 1;
		if(totalRowCounts != 0 && totalRowCounts >0){
			Double pages = totalRowCounts.doubleValue() / maxRowsPerPage;
			pages = Math.ceil(pages);
			noOfPages = pages.intValue();
		}
		return noOfPages;
	}
}
