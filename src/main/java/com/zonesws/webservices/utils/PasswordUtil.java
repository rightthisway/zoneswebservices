package com.zonesws.webservices.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PasswordUtil {
	private static Logger logger = LoggerFactory.getLogger(PasswordUtil.class);
	
	public static String generateEncrptedPassword(String clearPassword) {
	    MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			logger.error("SHA-1 algorithm not found!", e);
			throw new RuntimeException("SHA-1 algorithm not found!");
		}
	    byte [] digest = algorithm.digest(clearPassword.getBytes());
	    String encrptedPw = new String(Hex.encodeHex(digest));
	    return encrptedPw;
	}
	
	
	public static void main(String[] args) {
		
		String name = "VWxhZ2FuYXRoYW4gS29vdGhhaXlhbg==";
		
		/*byte[] encodedBytes = Base64.encodeBase64(name.getBytes());
		System.out.println("encodedBytes " + new String(encodedBytes));*/
		
		byte[] encodedBytes =name.getBytes();
		
		
		byte[] decodedBytes = Base64.decodeBase64(encodedBytes);
		System.out.println("decodedBytes " + new String(decodedBytes));
	}
}
