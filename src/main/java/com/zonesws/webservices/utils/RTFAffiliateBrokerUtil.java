package com.zonesws.webservices.utils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Affiliate;
import com.zonesws.webservices.data.Broker;


/**
 * Affilates & Brokers Util to get the all Affiliates and Brokers details
 * @author kulaganathan
 *
 */
public class RTFAffiliateBrokerUtil extends QuartzJobBean implements StatefulJob {
	
	public static Map<String, Boolean> affiliatePromoCodeMap = new ConcurrentHashMap<String, Boolean>();
	public static Map<String, Affiliate> affiliateObjPromoCodeMap = new ConcurrentHashMap<String, Affiliate>();
	public static Map<Integer, Broker> brokerMap = new ConcurrentHashMap<Integer, Broker>();
	
	public static void init(){
		
	 try{
		 
		Long startTime = System.currentTimeMillis();
		
		
		List<Broker> brokerList = DAORegistry.getQueryManagerDAO().getAllActiveBrokers();
		
		if(null != brokerList && !brokerList.isEmpty()){
			
			Map<Integer, Broker> brokerMapTemp = new ConcurrentHashMap<Integer, Broker>();
			
			for (Broker broker : brokerList) {
				brokerMapTemp.put(broker.getBrokerId(), broker);
			}
			
			brokerMap = new ConcurrentHashMap<Integer, Broker>(brokerMapTemp);
		}
		
		List<Affiliate> affiliates = DAORegistry.getQueryManagerDAO().getAllActiveAffiliates();
		
		if(null == affiliates || affiliates.isEmpty()){
			affiliatePromoCodeMap.clear();
			affiliateObjPromoCodeMap.clear();
			return;
		}
		
		Map<String, Boolean> affiliatePromoCodeMapTemp = new ConcurrentHashMap<String, Boolean>();
		Map<String, Affiliate> affiliateObjPromoCodeMapTemp = new ConcurrentHashMap<String, Affiliate>();
		
		for (Affiliate affiliate : affiliates) {
			affiliateObjPromoCodeMapTemp.put(affiliate.getPromotionalCode().toUpperCase(), affiliate);
			affiliatePromoCodeMapTemp.put(affiliate.getPromotionalCode().toUpperCase(), Boolean.TRUE);
		}
		
		if(null != affiliatePromoCodeMapTemp && !affiliatePromoCodeMapTemp.isEmpty()){
			affiliatePromoCodeMap = new ConcurrentHashMap<String, Boolean>(affiliatePromoCodeMapTemp);
			affiliateObjPromoCodeMap = new ConcurrentHashMap<String, Affiliate>(affiliateObjPromoCodeMapTemp);
		}
		System.out.println("TIME TO LAST EVENT ARTIST UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	public static Boolean validateAffiliatePromoCode(String promoCode){
		Boolean isAffiliate = affiliatePromoCodeMap.get(promoCode.toUpperCase());
		if(isAffiliate == null || !isAffiliate){
			Affiliate affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(promoCode);
			if(null == affiliateObj){
				return false;
			}
			affiliatePromoCodeMap.put(promoCode.toUpperCase(), Boolean.TRUE);
			affiliateObjPromoCodeMap.put(promoCode.toUpperCase(), affiliateObj);
		}
		return true;
	}
	
	
	public static Affiliate getAffiliateObjByPromoCode(String promoCode){
		Affiliate affiliateObj = affiliateObjPromoCodeMap.get(promoCode.toUpperCase());
		if(affiliateObj == null){
			affiliateObj = DAORegistry.getQueryManagerDAO().getActiveAffiliateByPromoCode(promoCode);
			if(null == affiliateObj){
				return null;
			}
			affiliatePromoCodeMap.put(promoCode.toUpperCase(), Boolean.TRUE);
			affiliateObjPromoCodeMap.put(promoCode.toUpperCase(), affiliateObj);
		}
		return affiliateObj;
	}
	
	public static Broker getBrokerByBrokerId(Integer brokerId){
		Broker broker = brokerMap.get(brokerId);
		if(broker == null){
			broker = DAORegistry.getQueryManagerDAO().getBrokerById(brokerId);
			if(null == broker){
				return null;
			}
			brokerMap.put(brokerId, broker);
		}
		return broker;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}
	
}
