package com.zonesws.webservices.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.RTFCategorySearch;


/**
 * Event Artist Util to get the all active event artist details
 * @author kulaganathan
 *
 */
public class RTFCategorySearchUtil extends QuartzJobBean implements StatefulJob {
	
	public static Map<String, RTFCategorySearch> categoriesSearchMap = new ConcurrentHashMap<String, RTFCategorySearch>();
	public static Map<Integer, RTFCategorySearch> categoriesSearchMapById = new ConcurrentHashMap<Integer, RTFCategorySearch>();
	//public static Map<String, RTFCategorySearch> categoriesSearchMap = new ConcurrentHashMap<String, RTFCategorySearch>();
	
	public static void init(){
		
	 try{
		 
		Long startTime = System.currentTimeMillis();
		List<RTFCategorySearch> catSearchList = DAORegistry.getQueryManagerDAO().getAllRTFSearchCategories();
		
		if(null == catSearchList || catSearchList.isEmpty()){
			return;
		}
		
		Map<String, RTFCategorySearch> categoriesSearchMapTemp = new HashMap<String, RTFCategorySearch>();
		Map<Integer, RTFCategorySearch> categoriesSearchMapByIdTemp = new HashMap<Integer, RTFCategorySearch>();
		
		for (RTFCategorySearch searchObj : catSearchList) {
			try{
				String key = searchObj.getDisplayName().replaceAll("\\s+", " ").trim().toLowerCase();
				categoriesSearchMapTemp.put(key, searchObj);
				categoriesSearchMapByIdTemp.put(searchObj.getId(), searchObj);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		if(null !=categoriesSearchMapTemp && !categoriesSearchMapTemp.isEmpty()){
			//categoriesSearchMap = null;
			categoriesSearchMap = new ConcurrentHashMap<String, RTFCategorySearch>(categoriesSearchMapTemp);
			
			//categoriesSearchMapById = null;
			categoriesSearchMapById = new ConcurrentHashMap<Integer, RTFCategorySearch>(categoriesSearchMapByIdTemp);
		}
		
		/*List<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getAll();
		
		
		
		List<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getAll();*/
		
		
		System.out.println("TIME TO LAST RTF Category Search UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	public static RTFCategorySearch getRTFCategoryBySearchKey(String searchKey){
		RTFCategorySearch catSearchObj = categoriesSearchMap.get(searchKey.replaceAll("\\s+", " ").trim().toLowerCase());
		/*if(catSearchObj == null ){
			List<RTFCategorySearch> categorySearchList = DAORegistry.getQueryManagerDAO().getRTFCategoriesBySearchKey(searchKey.replaceAll("\\W", "%"));
			if(null ==categorySearchList || categorySearchList.isEmpty()){
				return null;
			}
			for (RTFCategorySearch searchObj : categorySearchList) {
				try{
					String key = searchObj.getDisplayName().replaceAll("\\s+", " ").trim().toLowerCase();
					categoriesSearchMap.put(key, searchObj);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			catSearchObj = categoriesSearchMap.get(searchKey.replaceAll("\\s+", " ").trim().toLowerCase());
		}*/
		return catSearchObj;
	}
	
	public static RTFCategorySearch getRTFCategoryById(Integer id){
		RTFCategorySearch catSearchObj = categoriesSearchMapById.get(id);
		if(catSearchObj == null ){
			List<RTFCategorySearch> categorySearchList = DAORegistry.getQueryManagerDAO().getRTFCategoriesById(id);
			if(null ==categorySearchList || categorySearchList.isEmpty()){
				return null;
			}
			try{
				catSearchObj  = categorySearchList.get(0);
				categoriesSearchMapById.put(id, catSearchObj);
				return catSearchObj;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return catSearchObj;
	}
	
	
	public static void main(String[] args) {
		String searchKey = "College Base'ball";
		
		searchKey = searchKey.replaceAll("\\W", "");
		
		System.out.println(searchKey);
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}
	
}
