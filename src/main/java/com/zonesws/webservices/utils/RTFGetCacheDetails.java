package com.zonesws.webservices.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.jobs.UpcomingEventUtil;
import com.zonesws.webservices.utils.list.RTFCacheObj;


/**
 * Event Artist Util to get the all active event artist details
 * @author kulaganathan
 *
 */
public class RTFGetCacheDetails  {
	
	
	
	public static ArrayList<Event> filterEventsByDate(ArrayList<Event> eventList,String fromDateStr , String toDateStr) {
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		Date fromDate = null,toDate = null,eventDate = null;
		
		try{
			fromDate = df.parse(fromDateStr);
			toDate = df.parse(toDateStr);
		}catch(Exception e){
			e.printStackTrace();
			return eventList;
		}
		
		ArrayList<Event> filteredEvents = new ArrayList<Event>();
		
		for (Event event : eventList) {
			
			try{
				eventDate = df.parse(event.getEventDateStr());
				if(fromDate.equals(eventDate) || toDate.equals(eventDate) || 
						(eventDate.after(fromDate) && eventDate.before(toDate) )){
					filteredEvents.add(event);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return filteredEvents;
	}
	
	
	public static ArrayList<Event> filterEventsByArtistId(Integer artistId,ArrayList<Event> events) {
		
		ArrayList<Event> finalEvents = new ArrayList<Event>();
		for (Event event : events) {
			if(NormalSearchKeyConstruction.isMatchingEvent(artistId, null, null, event)) {
				finalEvents.add(event);
			}
		}
		return finalEvents;
	}
	
	
	public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<Integer>();
		
		for(int i=1;i<=10;i++){
			numbers.add(i);
			System.out.println(i);
		}
		System.out.println("======================");
		int fromIndex = 0;
		int toIndex = 10;
		
		if(toIndex > numbers.size()-1){
			toIndex = numbers.size();
		}
		
		List<Integer> subIntegers = numbers.subList(fromIndex, toIndex);
		
		for (Integer integer : subIntegers) {
			System.out.println(integer);
		}
		
	}
	
	
	public static String getSearchKey(String originalSearchKey,Integer parentId,Integer childId,Integer grandChildId,Integer rtfCatSearchId,
			Boolean isLocationFilter,String state){
		
		String searchKey= "";
		if(null != parentId){
			searchKey = "ParentCatId_"+parentId;
		}else if(null != rtfCatSearchId){
			searchKey = "RTFCatSearchId_"+rtfCatSearchId;
		}else if(null != childId){
			searchKey = "ChildCatId_"+childId;
		}else if(null != grandChildId){
			searchKey = "GrandChildCatId_"+grandChildId;
		}else{
			searchKey = originalSearchKey.replaceAll("\\W", "").toLowerCase();
		}
		searchKey = (isLocationFilter)?searchKey+"_"+state.toLowerCase():searchKey;
		return searchKey;
		
	}
	
	
	public static String getSearchKeyByArtistRefType(ArtistReferenceType artistRefType,String artistRefValue,Boolean isLocationFilter,String state){
		String searchKey = null;
		if(artistRefType.equals(ArtistReferenceType.PARENT_ID)) {
			searchKey = getSearchKey(null, Integer.parseInt(artistRefValue), null, null, null, isLocationFilter, state);
			
		} else if(artistRefType.equals(ArtistReferenceType.CHILD_ID)) {
			searchKey = getSearchKey(null, null, Integer.parseInt(artistRefValue), null, null, isLocationFilter, state);
			
		} else if(artistRefType.equals(ArtistReferenceType.GRANDCHILD_ID)) {
			searchKey = getSearchKey(null, null, null, Integer.parseInt(artistRefValue), null, isLocationFilter, state);
			
		} else if(artistRefType.equals(ArtistReferenceType.RTFCATS_ID)) {
			searchKey = getSearchKey(null, null, null, null, Integer.parseInt(artistRefValue), isLocationFilter, state);
			
		} else {
			searchKey = getSearchKey(artistRefValue, null, null, null, null, isLocationFilter, state);
		}
		return searchKey;
		
	}

	public static List<Event> getNearestStateEvents(List<Event> eventList , String stateName){
		
		String[] nearestStates =  RTFHomeCardsCacheUtil.getNearestStates(stateName);
		
		if(null != nearestStates ){
			eventList = new ArrayList<Event>();
			for (String nearestState : nearestStates) {
				if(nearestState.equals("NONE")){
					return RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
				}
				List<Event> tempEvents = RTFHomeCardsCacheUtil.eventMap.get(nearestState.toLowerCase());
				if(null != tempEvents && !tempEvents.isEmpty()){
					eventList.addAll(tempEvents);
				}
			}
			if(null != eventList && !eventList.isEmpty()){
				Collections.sort(eventList,UpcomingEventUtil.eventComparatorFordate);
				return eventList;
			}else{
				return RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
			}
		}
		return RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
	}
	
	public static List<Event> getHomeCardEventsByFilters(Boolean locationFilterApplied ,String state ,Boolean dateFilterApplied ,
			Date fromDate ,Date toDate){
		
		List<Event> eventList = null;
		
		if(locationFilterApplied && dateFilterApplied){
			
			eventList = RTFHomeCardsCacheUtil.eventMap.get(state.toLowerCase());
			
			if(null != eventList && !eventList.isEmpty() ){
				
				List<Event> tempEventList  = filterEventsByDate(eventList, fromDate, toDate);
				
				if(null == tempEventList || tempEventList.isEmpty()){
					return eventList;
				}
				return tempEventList;
				
			}else{
				eventList = getNearestStateEvents(eventList, state);
				List<Event> tempEventList  = filterEventsByDate(eventList, fromDate, toDate);
				if(null == tempEventList || tempEventList.isEmpty()){
					return eventList;
				}
				
				return tempEventList;
			}
			
		}else if(locationFilterApplied){
			
			eventList = RTFHomeCardsCacheUtil.eventMap.get(state.toLowerCase());
			
			if(null != eventList && !eventList.isEmpty() ){
				return eventList;
			}else{
				eventList = getNearestStateEvents(eventList, state);
				return eventList;
			}
			
		}else if(dateFilterApplied){
			
			eventList = RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
			
			if(null != eventList && !eventList.isEmpty() ){
				
				List<Event> tempEventList  = filterEventsByDate(eventList, fromDate, toDate);
				
				if(null == tempEventList || tempEventList.isEmpty()){
					return eventList;
				}
				return tempEventList;
			}
		}else{
			eventList = RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
			return eventList;
		}
		return eventList;
	}
	
	
	
	public static RTFCacheObj getHomeCards(int pageNumber, int maxRow, Boolean locationFilterApplied , String state , 
			Boolean dateFilterApplied , Date fromDate , Date toDate){
		
		RTFCacheObj rtfCacheObj = new RTFCacheObj();
		
		List<Event> eventList = getHomeCardEventsByFilters(locationFilterApplied, state, dateFilterApplied, fromDate, toDate);
		Integer fromIndex = 0, toIndex =0;
		
		if(null == eventList || eventList.size()<=0){
			rtfCacheObj.setCacheOff(true);
			rtfCacheObj.setEventList(null);
			rtfCacheObj.setHomeCardShowMore(false);
			return null;
		}
		
		fromIndex = (pageNumber-1)* maxRow;
		toIndex = fromIndex + maxRow;
		
		if(fromIndex > eventList.size()-1){
			rtfCacheObj.setCacheOff(false);
			rtfCacheObj.setEventList(null);
			rtfCacheObj.setHomeCardShowMore(false);
		}else{
			rtfCacheObj.setHomeCardShowMore(true);
			if(toIndex > eventList.size()-1){
				toIndex = eventList.size();
				rtfCacheObj.setHomeCardShowMore(false);
			}
			List<Event> splitedList = eventList.subList(fromIndex, toIndex);
			rtfCacheObj.setEventList(splitedList);
			rtfCacheObj.setCacheOff(false);
		}
		return rtfCacheObj;
	}
	
	public static List<Event> filterEventsByDate(List<Event> eventList,Date fromDate , Date toDate) {
		
		List<Event> filteredEvents = new ArrayList<Event>();
		Date eventDate=null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		for (Event event : eventList) {
			
			try{
				eventDate = df.parse(event.getEventDateStr());
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(fromDate.equals(eventDate) || toDate.equals(eventDate) || 
					(eventDate.after(fromDate) && eventDate.before(toDate) )){
				filteredEvents.add(event);
			}
		}
		return filteredEvents;
	}
	
	
	
}
