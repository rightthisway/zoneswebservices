package com.zonesws.webservices.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.enums.TabType;
import com.zonesws.webservices.jobs.UpcomingEventUtil;
import com.zonesws.webservices.utils.list.AutoPageResult;
import com.zonesws.webservices.utils.list.RTFCacheObj;
import com.zonesws.webservices.utils.list.VenueResult;


/**
 * Event Artist Util to get the all active event artist details
 * @author kulaganathan
 *
 */
public class RTFGetCacheDetailsOld  {
	
	
	
	public static ArrayList<Event> filterEventsByDate(ArrayList<Event> eventList,String fromDateStr , String toDateStr) {
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		Date fromDate = null,toDate = null,eventDate = null;
		
		try{
			fromDate = df.parse(fromDateStr);
			toDate = df.parse(toDateStr);
		}catch(Exception e){
			e.printStackTrace();
			return eventList;
		}
		
		ArrayList<Event> filteredEvents = new ArrayList<Event>();
		
		for (Event event : eventList) {
			
			try{
				eventDate = df.parse(event.getEventDateStr());
				if(fromDate.equals(eventDate) || toDate.equals(eventDate) || 
						(eventDate.after(fromDate) && eventDate.before(toDate) )){
					filteredEvents.add(event);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return filteredEvents;
	}
	
	
	
	public static HashMap<String, Object> getAllCacheEvents(int pageNumber,int maxRow, String searchKey,HashMap<String, Object> autoSearchObj
			,Boolean isLocationFilter,Boolean dateFilterApplied,String fromDate , String toDate,String state, boolean isOtherGrid){
		
		ArrayList<Event> eventList = isLocationFilter?RTFSearchCacheUtilOld.stateWiseEventListMap.get(searchKey):RTFSearchCacheUtilOld.eventListMap.get(searchKey);
		
		Integer fromIndex = 0, toIndex =0;
		
		boolean showEventsMore = false,showOtherGrid = false,showOtherEventMore = false;
		
		if(null != eventList && eventList.size() > 0){
			
			if(dateFilterApplied){
				eventList = filterEventsByDate(eventList, fromDate, toDate);
			}
			
			int size = eventList.size(); 
			
			boolean enableOtherGrid = false;
			
			if(size <= PaginationUtil.eventGridThreshold && !isOtherGrid){
				enableOtherGrid = true;
			}
			
			if(enableOtherGrid){
				
				if(isLocationFilter || dateFilterApplied){
					int maxRowOther = PaginationUtil.otherEventGridMaxRows;
					
					showOtherGrid = true;
					String tempKey = isLocationFilter?searchKey.replace("_"+state.toLowerCase(), ""):searchKey;
					ArrayList<Event> otherEventList = RTFSearchCacheUtilOld.eventListMap.get(tempKey);
					
					fromIndex = (pageNumber-1)* maxRowOther;
					toIndex = fromIndex + maxRowOther;
					
					if(fromIndex > otherEventList.size()-1){
						autoSearchObj.put("OtherEvents", null);
					}else{
						showOtherEventMore = true;
						if(toIndex > otherEventList.size()-1){
							toIndex = otherEventList.size();
							showOtherEventMore = false;
						}
						List<Event> splitedList = otherEventList.subList(fromIndex, toIndex);
						autoSearchObj.put("OtherEvents", new ArrayList<Event>(splitedList));
					}
				}
				
				if(size <= 0){
					autoSearchObj.put("showOtherEventGrid", showOtherGrid);
					autoSearchObj.put("showMoreOtherEvents", showOtherEventMore);
					autoSearchObj.put("showMoreEvents", showEventsMore);
					return autoSearchObj;
				}
				
				fromIndex = (pageNumber-1)* maxRow;
				toIndex = fromIndex + maxRow;
				
				if(fromIndex > eventList.size()-1){
					autoSearchObj.put("event", null);
				}else{
					showEventsMore = true;
					if(toIndex > eventList.size()-1){
						toIndex = eventList.size();
						showEventsMore = false;
					}
					List<Event> splitedList = eventList.subList(fromIndex, toIndex);
					autoSearchObj.put("event", new ArrayList<Event>(splitedList));
				}
				autoSearchObj.put("showOtherEventGrid", showOtherGrid);
				autoSearchObj.put("showMoreOtherEvents", showOtherEventMore);
				autoSearchObj.put("showMoreEvents", showEventsMore);
			}else if (isOtherGrid){
				fromIndex = (pageNumber-1)* maxRow;
				toIndex = fromIndex + maxRow;
				
				if(fromIndex > eventList.size()-1){
					autoSearchObj.put("OtherEvents", null);
				}else{
					showEventsMore = true;
					if(toIndex > eventList.size()-1){
						toIndex = eventList.size();
						showEventsMore = false;
					}
					List<Event> splitedList = eventList.subList(fromIndex, toIndex);
					autoSearchObj.put("OtherEvents", new ArrayList<Event>(splitedList));
				}
				
				autoSearchObj.put("showOtherEventGrid", true);
				autoSearchObj.put("showMoreOtherEvents", showEventsMore);
				autoSearchObj.put("showMoreEvents", false);
			}else{
				
				fromIndex = (pageNumber-1)* maxRow;
				toIndex = fromIndex + maxRow;
				
				if(fromIndex > eventList.size()-1){
					autoSearchObj.put("event", null);
				}else{
					showEventsMore = true;
					if(toIndex > eventList.size()-1){
						toIndex = eventList.size();
						showEventsMore = false;
					}
					List<Event> splitedList = eventList.subList(fromIndex, toIndex);
					autoSearchObj.put("event", new ArrayList<Event>(splitedList));
				}
				
				autoSearchObj.put("showOtherEventGrid", false);
				autoSearchObj.put("showMoreOtherEvents", false);
				autoSearchObj.put("showMoreEvents", showEventsMore);
			}
		}else if(isLocationFilter || dateFilterApplied){
			
			showOtherGrid = true;
			String tempKey = isLocationFilter?searchKey.replace("_"+state.toLowerCase(), ""):searchKey;
			eventList = RTFSearchCacheUtilOld.eventListMap.get(tempKey);
			
			fromIndex = (pageNumber-1)* maxRow;
			toIndex = fromIndex + maxRow;
			
			if(fromIndex > eventList.size()-1){
				autoSearchObj.put("OtherEvents", null);
			}else{
				showOtherEventMore = true;
				if(toIndex > eventList.size()-1){
					toIndex = eventList.size();
					showOtherEventMore = false;
				}
				List<Event> splitedList = eventList.subList(fromIndex, toIndex);
				autoSearchObj.put("OtherEvents", new ArrayList<Event>(splitedList));
			}
			autoSearchObj.put("showOtherEventGrid", true);
			autoSearchObj.put("showMoreOtherEvents", showOtherEventMore);
			autoSearchObj.put("showMoreEvents", false);
		}
		return autoSearchObj;
	}
	
	public static ArrayList<Event> filterEventsByArtistId(Integer artistId,ArrayList<Event> events) {
		
		ArrayList<Event> finalEvents = new ArrayList<Event>();
		for (Event event : events) {
			if(NormalSearchKeyConstruction.isMatchingEvent(artistId, null, null, event)) {
				finalEvents.add(event);
			}
		}
		
		return finalEvents;
	}
	public static HashMap<String, Object> getAllCacheEventsByArtistAndReferenceFilters(int pageNumber,int maxRow, String searchKey,HashMap<String, Object> autoSearchObj
			,Boolean isLocationFilter,Boolean dateFilterApplied,String fromDate , String toDate,String state, boolean isOtherGrid,Integer artistId){
		
		ArrayList<Event> eventList = isLocationFilter?RTFSearchCacheUtilOld.stateWiseEventListMap.get(searchKey):RTFSearchCacheUtilOld.eventListMap.get(searchKey);
		
		if(null != eventList && eventList.size() > 0){
			eventList = filterEventsByArtistId(artistId, eventList);
		}
		
		Integer fromIndex = 0, toIndex =0;
		
		boolean showEventsMore = false,showOtherGrid = false,showOtherEventMore = false;
		
		if(null != eventList && eventList.size() > 0){
			
			if(dateFilterApplied){
				eventList = filterEventsByDate(eventList, fromDate, toDate);
			}
			
			int size = eventList.size(); 
			
			boolean enableOtherGrid = false;
			
			if(size <= PaginationUtil.eventGridThreshold && !isOtherGrid){
				enableOtherGrid = true;
			}
			
			if(enableOtherGrid){
				
				if(isLocationFilter || dateFilterApplied){
					int maxRowOther = PaginationUtil.otherEventGridMaxRows;
					
					showOtherGrid = true;
					String tempKey = isLocationFilter?searchKey.replace("_"+state.toLowerCase(), ""):searchKey;
					ArrayList<Event> otherEventList = RTFSearchCacheUtilOld.eventListMap.get(tempKey);
					if(null != otherEventList && otherEventList.size() > 0){
						otherEventList = filterEventsByArtistId(artistId, otherEventList);
					}
					fromIndex = (pageNumber-1)* maxRowOther;
					toIndex = fromIndex + maxRowOther;
					
					if(fromIndex > otherEventList.size()-1){
						autoSearchObj.put("OtherEvents", null);
					}else{
						showOtherEventMore = true;
						if(toIndex > otherEventList.size()-1){
							toIndex = otherEventList.size();
							showOtherEventMore = false;
						}
						List<Event> splitedList = otherEventList.subList(fromIndex, toIndex);
						autoSearchObj.put("OtherEvents", new ArrayList<Event>(splitedList));
					}
				}
				
				if(size <= 0){
					autoSearchObj.put("showOtherEventGrid", showOtherGrid);
					autoSearchObj.put("showMoreOtherEvents", showOtherEventMore);
					autoSearchObj.put("showMoreEvents", showEventsMore);
					return autoSearchObj;
				}
				
				fromIndex = (pageNumber-1)* maxRow;
				toIndex = fromIndex + maxRow;
				
				if(fromIndex > eventList.size()-1){
					autoSearchObj.put("event", null);
				}else{
					showEventsMore = true;
					if(toIndex > eventList.size()-1){
						toIndex = eventList.size();
						showEventsMore = false;
					}
					List<Event> splitedList = eventList.subList(fromIndex, toIndex);
					autoSearchObj.put("event", new ArrayList<Event>(splitedList));
				}
				autoSearchObj.put("showOtherEventGrid", showOtherGrid);
				autoSearchObj.put("showMoreOtherEvents", showOtherEventMore);
				autoSearchObj.put("showMoreEvents", showEventsMore);
			}else if (isOtherGrid){
				fromIndex = (pageNumber-1)* maxRow;
				toIndex = fromIndex + maxRow;
				
				if(fromIndex > eventList.size()-1){
					autoSearchObj.put("OtherEvents", null);
				}else{
					showEventsMore = true;
					if(toIndex > eventList.size()-1){
						toIndex = eventList.size();
						showEventsMore = false;
					}
					List<Event> splitedList = eventList.subList(fromIndex, toIndex);
					autoSearchObj.put("OtherEvents", new ArrayList<Event>(splitedList));
				}
				
				autoSearchObj.put("showOtherEventGrid", true);
				autoSearchObj.put("showMoreOtherEvents", showEventsMore);
				autoSearchObj.put("showMoreEvents", false);
			}else{
				
				fromIndex = (pageNumber-1)* maxRow;
				toIndex = fromIndex + maxRow;
				
				if(fromIndex > eventList.size()-1){
					autoSearchObj.put("event", null);
				}else{
					showEventsMore = true;
					if(toIndex > eventList.size()-1){
						toIndex = eventList.size();
						showEventsMore = false;
					}
					List<Event> splitedList = eventList.subList(fromIndex, toIndex);
					autoSearchObj.put("event", new ArrayList<Event>(splitedList));
				}
				
				autoSearchObj.put("showOtherEventGrid", false);
				autoSearchObj.put("showMoreOtherEvents", false);
				autoSearchObj.put("showMoreEvents", showEventsMore);
			}
		}else if(isLocationFilter || dateFilterApplied){
			
			showOtherGrid = true;
			String tempKey = isLocationFilter?searchKey.replace("_"+state.toLowerCase(), ""):searchKey;
			eventList = RTFSearchCacheUtilOld.eventListMap.get(tempKey);
			
			if(null != eventList && eventList.size() > 0){
				eventList = filterEventsByArtistId(artistId, eventList);
			}
			fromIndex = (pageNumber-1)* maxRow;
			toIndex = fromIndex + maxRow;
			
			if(fromIndex > eventList.size()-1){
				autoSearchObj.put("OtherEvents", null);
			}else{
				showOtherEventMore = true;
				if(toIndex > eventList.size()-1){
					toIndex = eventList.size();
					showOtherEventMore = false;
				}
				List<Event> splitedList = eventList.subList(fromIndex, toIndex);
				autoSearchObj.put("OtherEvents", new ArrayList<Event>(splitedList));
			}
			autoSearchObj.put("showOtherEventGrid", true);
			autoSearchObj.put("showMoreOtherEvents", showOtherEventMore);
			autoSearchObj.put("showMoreEvents", false);
		}
		return autoSearchObj;
	}
	
	public static ArrayList<Artist> getArtistByFilters(ArrayList<Artist> artists, Boolean isLocationFilter,String stateName,
			Boolean isDateApplied ,String fromDateStr,String toDateStr){
		
		ArrayList<Artist> finalArtistResult = new ArrayList<Artist>(); 
		
		if(isLocationFilter && isDateApplied){
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			
			Date fromDate = null,toDate = null,artistStartDate = null,artistEndDate = null;
			
			try{
				fromDate = df.parse(fromDateStr);
				toDate = df.parse(toDateStr);
			}catch(Exception e){
				e.printStackTrace();
				return artists;
			}
			
			Set<Integer> stateArtistIds = RTFSearchCacheUtilOld.artistIdsByState.get(stateName.toLowerCase());
			
			if(null == stateArtistIds || stateArtistIds.isEmpty() ){
				stateArtistIds = new HashSet<Integer>();
			}
			
			for (Artist artist : artists) {
				
				if(stateArtistIds.contains(artist.getId())){
					
					if(null != artist.getFromDate() && null != artist.getToDate()){
						
						artistStartDate = new Date(artist.getFromDate().getTime());
						artistEndDate = new Date(artist.getToDate().getTime());
						
						if((fromDate.compareTo(artistStartDate)<=0 && 
								toDate.compareTo(artistStartDate)>=0) || (fromDate.compareTo(artistEndDate)<=0 && 
										toDate.compareTo(artistEndDate)>=0)) {
							finalArtistResult.add(artist);
						}
						
					}else{
						//finalArtistResult.add(artist);
					}
				}
			}
			return finalArtistResult;
		}else if(isLocationFilter){
			
			Set<Integer> stateArtistIds = RTFSearchCacheUtilOld.artistIdsByState.get(stateName.toLowerCase());
			/*
			System.out.println(stateName.toLowerCase()+"====================================STATE===================================="+stateName.toLowerCase());
			for (Integer integer : stateArtistIds) {
				System.out.println(integer);
			}
			System.out.println(stateName.toLowerCase()+"====================================STATE===================================="+stateName.toLowerCase());
			*/
			if(null == stateArtistIds || stateArtistIds.isEmpty() ){
				stateArtistIds = new HashSet<Integer>();
			}
			for (Artist artist : artists) {
				
				if(stateArtistIds.contains(artist.getId())){
					finalArtistResult.add(artist);
				}
			}
			return finalArtistResult;
			
		}else if(isDateApplied){
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			
			Date fromDate = null,toDate = null,artistStartDate = null,artistEndDate = null;
			
			try{
				fromDate = df.parse(fromDateStr);
				toDate = df.parse(toDateStr);
			}catch(Exception e){
				e.printStackTrace();
				return artists;
			}
			
			for (Artist artist : artists) {
				
				if(null != artist.getFromDate() && null != artist.getToDate()){
					
					artistStartDate = new Date(artist.getFromDate().getTime());
					artistEndDate = new Date(artist.getToDate().getTime());
					
					if((fromDate.compareTo(artistStartDate)<=0 && 
							toDate.compareTo(artistStartDate)>=0) || (fromDate.compareTo(artistEndDate)<=0 && 
									toDate.compareTo(artistEndDate)>=0)) {
						finalArtistResult.add(artist);
					}
				}else{
				}
			}
			return finalArtistResult;
		}else{
			return artists;
		}
		
	}
	
	public static HashMap<String, Object> getAllCacheArtist(int pageNumber,int maxRow, String searchKey,HashMap<String, Object> autoSearchObj,
			Boolean isLocationFilter,String stateName,Boolean isDateApplied ,String startDate,String endDate){
		
		ArrayList<Artist> artistsTemp = RTFSearchCacheUtilOld.artistListMap.get(searchKey);
		boolean showMore = false;
		Integer fromIndex = 0, toIndex =0;
		if(null != artistsTemp && artistsTemp.size() > 0){
			
			ArrayList<Artist> artists = getArtistByFilters(artistsTemp, isLocationFilter, stateName, isDateApplied, startDate, endDate);
			
			if(artists == null || artists.isEmpty()){
				autoSearchObj.put("showMoreArtists", showMore);
				return autoSearchObj;
			}
			
			fromIndex = (pageNumber-1)* maxRow;
			toIndex = fromIndex + maxRow;
			
			if(fromIndex > artists.size()-1){
				autoSearchObj.put("artist", null);
			}else{
				showMore = true;
				if(toIndex > artists.size()-1){
					toIndex = artists.size();
					showMore = false;
				}
				List<Artist> splitedList = artists.subList(fromIndex, toIndex);
				autoSearchObj.put("artist", new ArrayList<Artist>(splitedList));
			}
		}
		autoSearchObj.put("showMoreArtists", showMore);
		return autoSearchObj;
	}
	
	
	public static HashMap<String, Object> getAllCacheVenues(int pageNumber,int maxRow, String searchKey,HashMap<String, Object> autoSearchObj,
			Boolean isLocationFilter){
		ArrayList<VenueResult> venueResults = isLocationFilter?RTFSearchCacheUtilOld.stateWiseVenueListMap.get(searchKey):RTFSearchCacheUtilOld.venueListMap.get(searchKey);
		Integer fromIndex = 0, toIndex =0;
		boolean showMore = false;
		if(null != venueResults && venueResults.size() > 0){
			
			fromIndex = (pageNumber-1)* maxRow;
			toIndex = fromIndex + maxRow;
			
			if(fromIndex > venueResults.size()-1){
				autoSearchObj.put("venue", null);
			}else{
				showMore = true;
				if(toIndex > venueResults.size()-1){
					toIndex = venueResults.size();
					showMore = false;
				}
				List<VenueResult> splitedList = venueResults.subList(fromIndex, toIndex);
				autoSearchObj.put("venue", new ArrayList<VenueResult>(splitedList));
			}
		}
		autoSearchObj.put("showMoreVenues", showMore);
		return autoSearchObj;
	}
	
	public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<Integer>();
		
		for(int i=1;i<=10;i++){
			numbers.add(i);
			System.out.println(i);
		}
		System.out.println("======================");
		int fromIndex = 0;
		int toIndex = 10;
		
		if(toIndex > numbers.size()-1){
			toIndex = numbers.size();
		}
		
		List<Integer> subIntegers = numbers.subList(fromIndex, toIndex);
		
		for (Integer integer : subIntegers) {
			System.out.println(integer);
		}
		
	}
	
	
	
	public static HashMap<String, Object> getCacheObjectMapWithoutFilter(int eNo, int aNo, int vNo, int maxRow,
			String cacheSearchKey,String originalSearchKey, String searchType,TabType searchTab,Boolean isLocationFilter,
			String state,Boolean isDateApplied ,String startDate,String endDate, Boolean isOtherGrid){
		
		HashMap<String, Object> autoSearchObj = new HashMap<String, Object>();
		
		if(searchType.equals("NORMALSEARCH")){	
			
			switch (searchTab) {
			
				case ALL:
					getAllCacheEvents(eNo, maxRow, cacheSearchKey, autoSearchObj,isLocationFilter,isDateApplied,startDate,endDate,state,isOtherGrid);
					getAllCacheVenues(vNo, maxRow, cacheSearchKey, autoSearchObj,isLocationFilter);
					
					String tempKey = isLocationFilter?cacheSearchKey.replace("_"+state.toLowerCase(), ""):cacheSearchKey;
					
					getAllCacheArtist(aNo, maxRow, tempKey, autoSearchObj,isLocationFilter,state,isDateApplied,startDate,endDate);
					break;
					
				case EVENTS:
					getAllCacheEvents(eNo, maxRow, cacheSearchKey, autoSearchObj,isLocationFilter,isDateApplied,startDate,endDate,state,isOtherGrid);
					break;
					
				case VENUE:
					getAllCacheVenues(vNo, maxRow, cacheSearchKey, autoSearchObj,isLocationFilter);
					break;
					
				case ARTIST:
					tempKey = isLocationFilter?cacheSearchKey.replace("_"+state.toLowerCase(), ""):cacheSearchKey;
					getAllCacheArtist(aNo, maxRow, tempKey, autoSearchObj,isLocationFilter,state,isDateApplied,startDate,endDate);
					break;
					
				case NOTAB:
					//Auto Search Code
					break;
	
				default:
					break;
			}
		}else{
			String tempKey = isLocationFilter?cacheSearchKey.replace("_"+state.toLowerCase(), ""):cacheSearchKey;
			
			autoSearchObj = RTFSearchCacheUtilOld.autoSearchMap.get(tempKey);
			
			List<AutoPageResult> autoPageResult = autoSearchObj.get("autoPageResultList") != null ? (ArrayList<AutoPageResult>) (Object) autoSearchObj.get("autoPageResultList") : null;
			
			if(null != autoPageResult && !autoPageResult.isEmpty()){
				System.out.println("AUTO RESULTS FOUND IN CACHE For the search Key : "+ originalSearchKey);
			}else{
				int asMaxRow = 25;
				autoSearchObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,originalSearchKey, 1, asMaxRow,null,
						null,null,null,null,null,searchType,null,null,"ALL",null,null,null,null,null);
				System.out.println("AUTO RESULTS NOT FOUND IN CACHE For the search Key : "+ originalSearchKey);
			}
			
		}
		return autoSearchObj;
	}
	
	
	public static String getSearchKey(String originalSearchKey,Integer parentId,Integer childId,Integer grandChildId,Integer rtfCatSearchId,
			Boolean isLocationFilter,String state){
		
		String searchKey= "";
		if(null != parentId){
			searchKey = "ParentCatId_"+parentId;
		}else if(null != rtfCatSearchId){
			searchKey = "RTFCatSearchId_"+rtfCatSearchId;
		}else if(null != childId){
			searchKey = "ChildCatId_"+childId;
		}else if(null != grandChildId){
			searchKey = "GrandChildCatId_"+grandChildId;
		}else{
			searchKey = originalSearchKey.replaceAll("\\W", "").toLowerCase();
		}
		searchKey = (isLocationFilter)?searchKey+"_"+state.toLowerCase():searchKey;
		return searchKey;
		
	}
	
	public static Boolean isCacheSearchKey(String searchKey){
		return RTFSearchCacheUtilOld.cacheSearchKeys.contains(searchKey);
	}
	
	public static String getSearchKeyByArtistRefType(ArtistReferenceType artistRefType,String artistRefValue,Boolean isLocationFilter,String state){
		String searchKey = null;
		if(artistRefType.equals(ArtistReferenceType.PARENT_ID)) {
			searchKey = getSearchKey(null, Integer.parseInt(artistRefValue), null, null, null, isLocationFilter, state);
			
		} else if(artistRefType.equals(ArtistReferenceType.CHILD_ID)) {
			searchKey = getSearchKey(null, null, Integer.parseInt(artistRefValue), null, null, isLocationFilter, state);
			
		} else if(artistRefType.equals(ArtistReferenceType.GRANDCHILD_ID)) {
			searchKey = getSearchKey(null, null, null, Integer.parseInt(artistRefValue), null, isLocationFilter, state);
			
		} else if(artistRefType.equals(ArtistReferenceType.RTFCATS_ID)) {
			searchKey = getSearchKey(null, null, null, null, Integer.parseInt(artistRefValue), isLocationFilter, state);
			
		} else {
			searchKey = getSearchKey(artistRefValue, null, null, null, null, isLocationFilter, state);
		}
		return searchKey;
		
	}

	public static void getSearchTypeWithKey(String originalSearchKey,Integer parentId,Integer childId,Integer grandChildId,Integer rtfCatSearchId,
			HashMap<String, Object> allObj){
		
		ArtistReferenceType searchType = null;
		String searchValue = "";
		if(null != parentId){
			searchValue = ""+parentId;
			searchType = ArtistReferenceType.PARENT_ID;
		}else if(null != rtfCatSearchId){
			searchValue = ""+rtfCatSearchId;
			searchType = ArtistReferenceType.RTFCATS_ID;
		}else if(null != childId){
			searchValue = ""+childId;
			searchType = ArtistReferenceType.CHILD_ID;
		}else if(null != grandChildId){
			searchValue = ""+grandChildId;
			searchType = ArtistReferenceType.GRANDCHILD_ID;
		}else{
			String searchKey = originalSearchKey.replaceAll("\\W", "").toLowerCase();
			String mapType = RTFSearchCacheUtilOld.cacheSearchKeyWithTypeMap.get(searchKey);
			if(mapType != null) {
				searchValue = ""+searchKey;
				searchType = ArtistReferenceType.valueOf(mapType);
			}
		}
		
		if(searchValue != null && searchType != null) {
			allObj.put("aRefType", searchType.toString());
			allObj.put("aRefValue", searchValue);
		}
		
		
	}
	public static List<Event> getNearestStateEvents(List<Event> eventList , String stateName){
		
		String[] nearestStates =  RTFHomeCardsCacheUtil.getNearestStates(stateName);
		
		if(null != nearestStates ){
			eventList = new ArrayList<Event>();
			for (String nearestState : nearestStates) {
				if(nearestState.equals("NONE")){
					return RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
				}
				List<Event> tempEvents = RTFHomeCardsCacheUtil.eventMap.get(nearestState.toLowerCase());
				if(null != tempEvents && !tempEvents.isEmpty()){
					eventList.addAll(tempEvents);
				}
			}
			if(null != eventList && !eventList.isEmpty()){
				Collections.sort(eventList,UpcomingEventUtil.eventComparatorFordate);
				return eventList;
			}else{
				return RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
			}
		}
		return RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
	}
	
	public static List<Event> getHomeCardEventsByFilters(Boolean locationFilterApplied ,String state ,Boolean dateFilterApplied ,
			Date fromDate ,Date toDate){
		
		List<Event> eventList = null;
		
		if(locationFilterApplied && dateFilterApplied){
			
			eventList = RTFHomeCardsCacheUtil.eventMap.get(state.toLowerCase());
			
			if(null != eventList && !eventList.isEmpty() ){
				
				List<Event> tempEventList  = filterEventsByDate(eventList, fromDate, toDate);
				
				if(null == tempEventList || tempEventList.isEmpty()){
					return eventList;
				}
				return tempEventList;
				
			}else{
				eventList = getNearestStateEvents(eventList, state);
				List<Event> tempEventList  = filterEventsByDate(eventList, fromDate, toDate);
				if(null == tempEventList || tempEventList.isEmpty()){
					return eventList;
				}
				
				return tempEventList;
			}
			
		}else if(locationFilterApplied){
			
			eventList = RTFHomeCardsCacheUtil.eventMap.get(state.toLowerCase());
			
			if(null != eventList && !eventList.isEmpty() ){
				return eventList;
			}else{
				eventList = getNearestStateEvents(eventList, state);
				return eventList;
			}
			
		}else if(dateFilterApplied){
			
			eventList = RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
			
			if(null != eventList && !eventList.isEmpty() ){
				
				List<Event> tempEventList  = filterEventsByDate(eventList, fromDate, toDate);
				
				if(null == tempEventList || tempEventList.isEmpty()){
					return eventList;
				}
				return tempEventList;
			}
		}else{
			eventList = RTFHomeCardsCacheUtil.eventMap.get("NOT_STATE");
			return eventList;
		}
		return eventList;
	}
	
	
	
	public static RTFCacheObj getHomeCards(int pageNumber, int maxRow, Boolean locationFilterApplied , String state , 
			Boolean dateFilterApplied , Date fromDate , Date toDate){
		
		RTFCacheObj rtfCacheObj = new RTFCacheObj();
		
		List<Event> eventList = getHomeCardEventsByFilters(locationFilterApplied, state, dateFilterApplied, fromDate, toDate);
		Integer fromIndex = 0, toIndex =0;
		
		if(null == eventList || eventList.size()<=0){
			rtfCacheObj.setCacheOff(true);
			rtfCacheObj.setEventList(null);
			rtfCacheObj.setHomeCardShowMore(false);
			return null;
		}
		
		fromIndex = (pageNumber-1)* maxRow;
		toIndex = fromIndex + maxRow;
		
		if(fromIndex > eventList.size()-1){
			rtfCacheObj.setCacheOff(false);
			rtfCacheObj.setEventList(null);
			rtfCacheObj.setHomeCardShowMore(false);
		}else{
			rtfCacheObj.setHomeCardShowMore(true);
			if(toIndex > eventList.size()-1){
				toIndex = eventList.size();
				rtfCacheObj.setHomeCardShowMore(false);
			}
			List<Event> splitedList = eventList.subList(fromIndex, toIndex);
			rtfCacheObj.setEventList(splitedList);
			rtfCacheObj.setCacheOff(false);
		}
		return rtfCacheObj;
	}
	
	public static List<Event> filterEventsByDate(List<Event> eventList,Date fromDate , Date toDate) {
		
		List<Event> filteredEvents = new ArrayList<Event>();
		Date eventDate=null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		for (Event event : eventList) {
			
			try{
				eventDate = df.parse(event.getEventDateStr());
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(fromDate.equals(eventDate) || toDate.equals(eventDate) || 
					(eventDate.after(fromDate) && eventDate.before(toDate) )){
				filteredEvents.add(event);
			}
		}
		return filteredEvents;
	}
	
	
	
}
