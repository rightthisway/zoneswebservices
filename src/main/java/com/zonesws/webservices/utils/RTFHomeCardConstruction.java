package com.zonesws.webservices.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Cards;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.CardType;
import com.zonesws.webservices.jobs.CustomerUtil;
import com.zonesws.webservices.jobs.TeamCardImageUtil;
import com.zonesws.webservices.jobs.UpcomingEventUtil;
import com.zonesws.webservices.utils.list.Card;



/**
 * Event Artist Util to get the all active event artist details
 * @author kulaganathan
 *
 */
public class RTFHomeCardConstruction  {
	
	public static List<Card> constructCards(List<Event> events, String customerIdStr , int pageNumber, int maxRows, 
			ApplicationPlatForm applicationPlatForm,String configIdString,Set<Integer> skipEventIds) throws Exception{
		List<Card> cards = new ArrayList<Card>();
		Customer customer = null;
		
		if(null != customerIdStr && !customerIdStr.isEmpty()){
			customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
		}
		
		Map<Integer, List<Event>> artistEventmap = new HashMap<Integer, List<Event>>();
		
		Long daysDifference = 0l;
		Date curDate = new Date(),eventDate = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		
		for (Event event : events) {
			
			
			try{
				if(event.getEventDateTime().contains("TBD")){
					eventDate = dateFormat.parse(event.getEventDateTime().replaceAll("TBD", "12:00 AM")); 
				}else{
					eventDate = dateFormat.parse(event.getEventDateTime());
				}
			}catch(Exception e){
				eventDate = new Date();
			}
			daysDifference = DateUtil.getDifferenceMintues(curDate, eventDate);
			event.setDayDifference(daysDifference);
			
			List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
			if(null == artistIds || artistIds.isEmpty() || artistIds.size() >1){
				continue;
			}
			event.setArtistIds(artistIds);
			
			List<Event> list = artistEventmap.get(event.getArtistId());
			
			if(null != list && !list.isEmpty()){
				artistEventmap.get(artistIds.get(0)).add(event);
			}else{
				list = new ArrayList<Event>();
				list.add(event);
				artistEventmap.put(artistIds.get(0), list);
			}
		}
		
		Card card = null;
		
		pageNumber = pageNumber-1;
		
		int i =1 , cardTypeCount =(maxRows * pageNumber)+1, totalCount=1;
		
		/*List<Cards> cardsList = DAORegistry.getCardsDAO().getAllCardsByProductId(1);
		Map<Integer, Cards> cardsPositionMap = new HashMap<Integer, Cards>();
		for (Cards ticTrackerCard : cardsList) {
			cardsPositionMap.put(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE)?ticTrackerCard.getDesktopPosition():ticTrackerCard.getMobilePosition(), ticTrackerCard);
		}
		*/
		Map<String, List<Event>> map = new HashMap<String, List<Event>>();
					
		for (Integer artistId : artistEventmap.keySet()) {
			List<Event> artisEvents = artistEventmap.get(artistId);
			Collections.sort(artisEvents, TicketUtil.eventDaysComparatorForEventDateAsc);
			String key = artisEvents.get(0).getDayDifference()+"_"+artistId;
			map.put(key, artisEvents);
		}
		
		List<String> keyList = new ArrayList<String>(map.keySet());
		Collections.sort(keyList, TicketUtil.comparatorCardsAscByEventDate);
		
		for (String key : keyList) {
			
			Integer artistId =Integer.parseInt(key.split("_")[1]);
			
            Cards ticTrackerCard = null;//cardsPositionMap.get(cardTypeCount);
            card=  new Card();
            
            if(null != ticTrackerCard){
                  
                  /*card.setCardType(ticTrackerCard.getCardType());
                  
                  switch (ticTrackerCard.getCardType()) {
                  
	                  case PROMOTION:
	                      
	                      card.setArtistId(ticTrackerCard.getArtistId());
	                      card.setCardName(ticTrackerCard.getImageText());
					        card.setCardImage(MapUtil.getCardImage(ticTrackerCard.getImageFileUrl(),applicationPlatForm));
	                      card.setImageText(ticTrackerCard.getImageText());
	                      card.setIsFavoriteArtist(false);
	                      
	                      List<Event> eventDetails = DAORegistry.getEventDAO().getEventsByArtistId(ticTrackerCard.getArtistId(), 1, ticTrackerCard.getNoOfEvents());
	                      
	                      if(eventDetails == null || eventDetails.isEmpty()){
	                      	continue;
	                      }
	                      card.setEvents(eventDetails);
	                      
	                      break;
	                      
	                  case MAINPROMOTION:
	                	card.setCardImage(MapUtil.getMainPromotionCardImage(ticTrackerCard.getImageFileUrl(),ticTrackerCard.getMobileImageFileUrl(),applicationPlatForm));
					    card.setCardImageForMobile(MapUtil.getMainPromotionCardImage(ticTrackerCard.getImageFileUrl(),ticTrackerCard.getMobileImageFileUrl(),ApplicationPlatForm.ANDROID));
	                    card.setImageText(ticTrackerCard.getImageText());
	                    
	                    if(ticTrackerCard.getPromoCardType().equals("ARTIST") ){
	                  	  card.setArtistId(ticTrackerCard.getArtistId());
	                        card.setCardName(ticTrackerCard.getArtist().getName());
	                    }else if(ticTrackerCard.getPromoCardType().equals("VENUE") ){
	                  	  card.setArtistId(ticTrackerCard.getVenueId());
	                        card.setCardName(ticTrackerCard.getVenue().getBuilding());
	                    }else if(ticTrackerCard.getPromoCardType().equals("GRAND") ){
	                  	  card.setArtistId(ticTrackerCard.getGrandChildCatId());
	                        card.setCardName(ticTrackerCard.getGrandChildCategory().getName());
	                    }else if(ticTrackerCard.getPromoCardType().equals("CHILD") ){
	                  	  card.setArtistId(ticTrackerCard.getChildCatId());
	                        card.setCardName(ticTrackerCard.getChildCategory().getName());
	                    }else if(ticTrackerCard.getPromoCardType().equals("PARENT") ){
	                  	  card.setArtistId(ticTrackerCard.getParentCatId());
	                        card.setCardName(ticTrackerCard.getParentCategory().getName());
	                    } 
	                    card.setPromoCardType(ticTrackerCard.getPromoCardType());
	                    break;
                    case UPCOMMINGEVENT:
                    	events=UpcomingEventUtil.getNext5UpcomingEvents();
                    	for (Event event : events) {
    						 event = MapUtil.testSvgDetailsByEvent(event);
    						 if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
    							continue;
    						 }
        					  event.setEventImageUrl(applicationPlatForm);
        					  URLUtil.getShareLinkURL(event, customer,applicationPlatForm);
                              card.setCardImage("");
                              card.setCardType(CardType.UPCOMMINGEVENT);
                              card.setCardName("Upcoming Events");
                              try{
                            	  card.setEvents(UpcomingEventUtil.getNext5UpcomingEvents());
                              }catch(Exception e){
                            	  e.printStackTrace();
                              }
                              
                              if(null == card.getEvents() || card.getEvents().isEmpty()){
                            	continue;  
                              }
                              
                              List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
                  			  if(null == artistIds || artistIds.isEmpty()){
                  				continue;
                  			  }
                  			  event.setArtistIds(artistIds);
                    	}
                          break;
                          
                    case YESNO:
                          card.setCardImage(MapUtil.getCardImage(ticTrackerCard.getImageFileUrl(),applicationPlatForm));
                          card.setImageText(ticTrackerCard.getImageText());
                          card.setYesUrl("");
                          card.setNoUrl("");
                          break;

                    default:
                          break;
                  }*/
                  
            }else{
                  
                  List<Event> finalEvents = new ArrayList<Event>();
                  List<Event> artisEvents = map.get(key);
                  
                  Event event = artisEvents.get(0);
                  
                  card.setArtistId(artistId);
                  try {
                	  card.setCardImage(TeamCardImageUtil.getPrarentCategoryImage(event.getParentCategoryId()));  
                  }catch(Exception e) {
                	  card.setCardImage(TeamCardImageUtil.getTeamCardImageByArtist(artistId,applicationPlatForm));
                  }
                  card.setCardType(CardType.TEAM_CARD);
                  card.setIsFavoriteArtist(false);
                  card.setCardName(event.getArtistName());
                  card.setArtistId(event.getArtistId());
                  finalEvents = new ArrayList<Event>();
                  finalEvents.add(event);
                  card.setEvents(finalEvents);
                  
            }
            cardTypeCount++;
            cards.add(card);
            i++;
            
            if(totalCount > 40){
                  break;
            }
            totalCount++;
		}
		
		
		return cards;
	}
	
	
	
	public static List<Card> constructCardsMobile(List<Event> events, String customerIdStr , int pageNumber, int maxRows, 
			ApplicationPlatForm applicationPlatForm,String configIdString,Set<Integer> skipEventIds) throws Exception{
		List<Card> cards = new ArrayList<Card>();
		Customer customer = null;
		
		if(null != customerIdStr && !customerIdStr.isEmpty()){
			customer = CustomerUtil.getCustomerById(Integer.parseInt(customerIdStr.trim()));
		}
		
		Map<Integer, List<Event>> artistEventmap = new HashMap<Integer, List<Event>>();
		
		Long daysDifference = 0l;
		Date curDate = new Date(),eventDate = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		
		for (Event event : events) {
			
			/*if(skipEventIds.contains(event.getEventId())){
				continue;
			}*/
			
			try{
				if(event.getEventDateTime().contains("TBD")){
					eventDate = dateFormat.parse(event.getEventDateTime().replaceAll("TBD", "12:00 AM")); 
				}else{
					eventDate = dateFormat.parse(event.getEventDateTime());
				}
			}catch(Exception e){
				eventDate = new Date();
			}
			daysDifference = DateUtil.getDifferenceMintues(curDate, eventDate);
			event.setDayDifference(daysDifference);
			
			/*event = MapUtil.testSvgDetailsByEvent(event);
			if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
				continue;
			}*/
			//event.setEventImageUrl(applicationPlatForm);
			//URLUtil.getShareLinkURL(event, customer,applicationPlatForm);
			
			List<Integer> artistIds = EventArtistUtil.getArtistIdsByEventId(event.getEventId());
			if(null == artistIds || artistIds.isEmpty() || artistIds.size() >1){
				continue;
			}
			event.setArtistIds(artistIds);
			
			List<Event> list = artistEventmap.get(event.getArtistId());
			
			if(null != list && !list.isEmpty()){
				artistEventmap.get(artistIds.get(0)).add(event);
			}else{
				list = new ArrayList<Event>();
				list.add(event);
				artistEventmap.put(artistIds.get(0), list);
			}
		}
		
		Card card = null;
		
		pageNumber = pageNumber-1;
		
		int i =1 , cardTypeCount =(maxRows * pageNumber)+1, totalCount=1;
		
		/*List<Cards> cardsList = DAORegistry.getCardsDAO().getAllCardsByProductId(1);
		Map<Integer, Cards> cardsPositionMap = new HashMap<Integer, Cards>();
		for (Cards ticTrackerCard : cardsList) {
			cardsPositionMap.put(applicationPlatForm.equals(ApplicationPlatForm.DESKTOP_SITE)?ticTrackerCard.getDesktopPosition():ticTrackerCard.getMobilePosition(), ticTrackerCard);
		}*/
		
		Map<String, List<Event>> map = new HashMap<String, List<Event>>();
					
		for (Integer artistId : artistEventmap.keySet()) {
			List<Event> artisEvents = artistEventmap.get(artistId);
			Collections.sort(artisEvents, TicketUtil.eventDaysComparatorForEventDateAsc);
			String key = artisEvents.get(0).getDayDifference()+"_"+artistId;
			map.put(key, artisEvents);
		}
		
		List<String> keyList = new ArrayList<String>(map.keySet());
		Collections.sort(keyList, TicketUtil.comparatorCardsAscByEventDate);
		
		for (String key : keyList) {
			
			Integer artistId =Integer.parseInt(key.split("_")[1]);
			
            Cards ticTrackerCard = null;//cardsPositionMap.get(cardTypeCount);
            card=  new Card();
            
            if(null != ticTrackerCard){
                  continue;
            }else{
                  
                  Boolean isFavouriteArtist = false;
                  List<Event> finalEvents = new ArrayList<Event>();
                  List<Event> artisEvents = map.get(key);
                  
                  Event event = artisEvents.get(0);
                  
                  card.setArtistId(artistId);
                  card.setCardImage(TeamCardImageUtil.getPrarentCategoryImage(event.getParentCategoryId()));
                  card.setCardImage("");
                  card.setCardType(CardType.TEAM_CARD);
                  card.setIsFavoriteArtist(isFavouriteArtist);
                  card.setCardName(event.getArtistName());
                  card.setArtistId(event.getArtistId());
                  finalEvents = new ArrayList<Event>();
                  finalEvents.add(event);
                  card.setEvents(finalEvents);
                  
            }
            cardTypeCount++;
            cards.add(card);
            i++;
            
            if(totalCount > 40){
                  break;
            }
            totalCount++;
		}
		
		
		return cards;
	}
	
}
