package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.list.NearestStates;


/**
 * HOme Cards Cache Util to get the all active events for Home Cards
 * @author kulaganathan
 *
 */
public class RTFHomeCardsCacheUtil extends QuartzJobBean implements StatefulJob {
	
	public static Map<String, List<Event>> eventMap = new ConcurrentHashMap<String, List<Event>>();
	private static Map<String, String[]> nearestStateInfo = new ConcurrentHashMap<String, String[]>();
	
	public void init(){
		
	 try{
		 System.out.println("HOME_CARD_CACEH_UTIL Begins : "+new Date());
		Long startTime = System.currentTimeMillis();
		
		List<NearestStates>  nearestStatesList = DAORegistry.getQueryManagerDAO().getAllNearestStates();
		Map<String, String[]> tempNearestStateInfo = new ConcurrentHashMap<String, String[]>();
		for (NearestStates nearestStateObj : nearestStatesList) {
			try{
				tempNearestStateInfo.put(nearestStateObj.getState().toLowerCase(), nearestStateObj.getNearestStates().split(","));
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		nearestStateInfo.clear();
		nearestStateInfo.putAll(tempNearestStateInfo);
		
		List<Event> events = DAORegistry.getEventDAO().getAllEventsByHomeCardType(null, null, null,null,1,70000,null);
		Map<String, List<Event>> eventMapTemp = new HashMap<String, List<Event>>();
		
		eventMapTemp.put("NOT_STATE",events);
		
		for (Event event : events) {
			if(null == event.getState() || event.getState().isEmpty()) {
				continue;
			}
			try{
				List<Event>  eventListTemp= eventMapTemp.get(event.getState().toLowerCase());
				if(null != eventListTemp && !eventListTemp.isEmpty()){
					eventMapTemp.get(event.getState().toLowerCase()).add(event);
				}else{
					eventListTemp = new ArrayList<Event>();
					eventListTemp.add(event);
					eventMapTemp.put(event.getState().toLowerCase(),eventListTemp);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		if(null != eventMapTemp && !eventMapTemp.isEmpty()){
			eventMap.clear();
			eventMap.putAll(eventMapTemp);
		}
		 System.out.println("HOME_CARD_CACEH_UTIL Ends : "+new Date());
		System.out.println("RTF_HOMECARDS_CACHE: Job Completed :" + (System.currentTimeMillis() - startTime) / 1000);
		System.out.println("RTF_HOMECARDS_CACHE: TIME TO LAST RTF HOME CARDS CACHE UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	
	/**
	 * 
	 * @param originState
	 * @return nearestStates 
	 */
	public static String[] getNearestStates(String originState){
		String[] nearestStates = null;
		try {
			nearestStates = nearestStateInfo.get(originState.toLowerCase());
			
			if(nearestStates == null){
				
			}
			return nearestStates;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}
	
}
