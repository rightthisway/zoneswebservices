package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.enums.CJEParentOrder;
import com.zonesws.webservices.enums.SliderSearchType;


/**
 * Event Artist Util to get the all active event artist details
 * @author kulaganathan
 *
 */
public class RTFHomeSliderUtil extends QuartzJobBean implements StatefulJob {
	
	public static List<ChildCategory> sliderLists = new ArrayList<ChildCategory>();
	
	public void init(){
		
	 try{
		 
		Long startTime = System.currentTimeMillis();
		
		
		ChildCategory sliderObj = null;
		List<ChildCategory> tempSliderList = new ArrayList<ChildCategory>();
		
		for(SliderSearchType searchType : SliderSearchType.values()){
			switch (searchType) {
				case PARENT:
					List<ChildCategory> parentChilds = DAORegistry.getQueryManagerDAO().getSelectedParentCategories();
					List<ChildCategory> parentSliderTemp = new ArrayList<ChildCategory>();
					for (ChildCategory parentObj : parentChilds) {
						sliderObj = new ChildCategory();
						sliderObj.setId(parentObj.getId());
						sliderObj.setParentCategoryName(parentObj.getName().toUpperCase());
						
						if(parentObj.getName().equals("OTHER")){
							sliderObj.setName("FAMILY/OTHERS");
							sliderObj.setParentCategoryName("FAMILYANDOTHER");
						}/*else if(parentObj.getName().equals("THEATRE")){ 
							sliderObj.setName(parentObj.getName());
							sliderObj.setParentCategoryName("THEATER");
						}*/else{
							sliderObj.setName(parentObj.getName());
						}
						sliderObj.setSliderSearchType(searchType);
						parentSliderTemp.add(sliderObj);
					}
					
					for(CJEParentOrder parentOrder :CJEParentOrder.values()){
						for (ChildCategory parentObj : parentSliderTemp) {
							String name = parentObj.getName().toUpperCase();
							if(name.equalsIgnoreCase("FAMILY/OTHERS")){
								name = "FAMILYEVENTS";
							}/*else if (name.equalsIgnoreCase("THEATRE")){
								name = "THEATER";
							}*/
							if(name.equals(parentOrder.toString())){
								tempSliderList.add(parentObj);
							}
						}
					}
					break;
					
				case CHILD:
					List<ChildCategory> childList = DAORegistry.getQueryManagerDAO().getSelectedChilds();
					
					for (ChildCategory childCategory : childList) {
						sliderObj = new ChildCategory();
						sliderObj.setId(childCategory.getId());
						sliderObj.setName(childCategory.getName());
						sliderObj.setSliderSearchType(searchType);
						sliderObj.setParentCategoryName("THEATER");
						tempSliderList.add(sliderObj);
					}
					break;
					
				case GRANDCHILD:
					
					List<ChildCategory> selectedGrandChildList = DAORegistry.getQueryManagerDAO().getSelectedGrandChilds();
					
					for (ChildCategory childCategory : selectedGrandChildList) {
						sliderObj = new ChildCategory();
						if(childCategory.getName().contains("MLB")){
							sliderObj.setName("MLB");
						}else if (childCategory.getName().contains("NHL")){
							sliderObj.setName("NHL");
						}else if (childCategory.getName().contains("NFL")){
							sliderObj.setName("NFL");
						}else if (childCategory.getName().contains("NBA")){
							sliderObj.setName("NBA");
						}
						sliderObj.setId(childCategory.getId());
						//sliderObj.setName(childCategory.getName());
						sliderObj.setSliderSearchType(searchType);
						sliderObj.setParentCategoryName("SPORTS");
						tempSliderList.add(sliderObj);
					}
					
					break;

				default:
					break;
				}
		}
		
		sliderLists.clear();
		sliderLists.addAll(tempSliderList);
		System.out.println("TIME TO LAST RTF HOME SLIDER UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	public static List<ChildCategory> getHomeSliderListFromCache(){
		
		if(null == sliderLists || sliderLists.isEmpty()){
			
			ChildCategory sliderObj = null;
			List<ChildCategory> tempSliderList = new ArrayList<ChildCategory>();
			
			for(SliderSearchType searchType : SliderSearchType.values()){
				switch (searchType) {
					case PARENT:
						List<ChildCategory> parentChilds = DAORegistry.getQueryManagerDAO().getSelectedParentCategories();
						List<ChildCategory> parentSliderTemp = new ArrayList<ChildCategory>();
						for (ChildCategory parentObj : parentChilds) {
							sliderObj = new ChildCategory();
							sliderObj.setId(parentObj.getId());
							sliderObj.setParentCategoryName(parentObj.getName().toUpperCase());
							
							if(parentObj.getName().equals("OTHER")){
								sliderObj.setName("FAMILY/OTHERS");
								sliderObj.setParentCategoryName("FAMILYANDOTHER");
							}/*else if(parentObj.getName().equals("THEATRE")){ 
								sliderObj.setName(parentObj.getName());
								sliderObj.setParentCategoryName("THEATER");
							}*/else{
								sliderObj.setName(parentObj.getName());
							}
							sliderObj.setSliderSearchType(searchType);
							parentSliderTemp.add(sliderObj);
						}
						
						for(CJEParentOrder parentOrder :CJEParentOrder.values()){
							for (ChildCategory parentObj : parentSliderTemp) {
								String name = parentObj.getName().toUpperCase();
								if(name.equalsIgnoreCase("FAMILY/OTHERS")){
									name = "FAMILYEVENTS";
								}/*else if (name.equalsIgnoreCase("THEATRE")){
									name = "THEATER";
								}*/
								if(name.equals(parentOrder.toString())){
									tempSliderList.add(parentObj);
								}
							}
						}
						break;
						
					case CHILD:
						List<ChildCategory> childList = DAORegistry.getQueryManagerDAO().getSelectedChilds();
						
						for (ChildCategory childCategory : childList) {
							sliderObj = new ChildCategory();
							sliderObj.setId(childCategory.getId());
							sliderObj.setName(childCategory.getName());
							sliderObj.setSliderSearchType(searchType);
							sliderObj.setParentCategoryName("THEATER");
							tempSliderList.add(sliderObj);
						}
						break;
						
					case GRANDCHILD:
						
						List<ChildCategory> selectedGrandChildList = DAORegistry.getQueryManagerDAO().getSelectedGrandChilds();
						
						for (ChildCategory childCategory : selectedGrandChildList) {
							sliderObj = new ChildCategory();
							if(childCategory.getName().contains("MLB")){
								sliderObj.setName("MLB");
							}else if (childCategory.getName().contains("NHL")){
								sliderObj.setName("NHL");
							}else if (childCategory.getName().contains("NFL")){
								sliderObj.setName("NFL");
							}else if (childCategory.getName().contains("NBA")){
								sliderObj.setName("NBA");
							}
							sliderObj.setId(childCategory.getId());
							//sliderObj.setName(childCategory.getName());
							sliderObj.setSliderSearchType(searchType);
							sliderObj.setParentCategoryName("SPORTS");
							tempSliderList.add(sliderObj);
						}
						
						break;

					default:
						break;
					}
			}
			
			sliderLists.clear();
			sliderLists.addAll(tempSliderList);
		}
		
		return sliderLists;
	}
	
	
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}
	
}
