package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Venue;


/**
 * Normal Search Cache Util to get the all active events for Home Cards
 * @author kulaganathan
 *
 */
public class RTFNormalSearchUtil extends QuartzJobBean implements StatefulJob {
	
	public static List<Event> allEventList = new ArrayList<Event>();
	public static List<Artist> allArtistList = new ArrayList<Artist>();
	public static List<Venue> allVenueList = new ArrayList<Venue>();
	
	public static List<Artist> getAllArtistList(){
		List<Artist> artists = allArtistList;
		if(null == artists || artists.isEmpty()){
			artists = DAORegistry.getQueryManagerDAO().getAllArtist(PaginationUtil.excludeEventDays);
			if(null != artists && !artists.isEmpty()){
				allArtistList.clear();
				allArtistList.addAll(artists);
			}
		}
		return artists;
	}
	
	public static List<Venue> getAllVenueList(){
		List<Venue> venues = allVenueList;
		if(null == venues || venues.isEmpty()){
			venues = DAORegistry.getQueryManagerDAO().getAllVenues(PaginationUtil.excludeEventDays);
			if(null != venues && !venues.isEmpty()){
				allVenueList.clear();
				allVenueList.addAll(venues);
			}
		}
		return venues;
	}
	
	public void init(){
	 try{
		Long startTime = System.currentTimeMillis();
		List<Event> events = DAORegistry.getEventDAO().getAllEventsByHomeCardType(null, null, null,null,1,98000,null);
		
		if(null != events && !events.isEmpty()){
			allEventList.clear();
			allEventList.addAll(events);
		}
		
		List<Artist> artists = DAORegistry.getQueryManagerDAO().getAllArtist(PaginationUtil.excludeEventDays);
		
		if(null != artists && !artists.isEmpty()){
			allArtistList.clear();
			allArtistList.addAll(artists);
		}
		
		List<Venue> venues = DAORegistry.getQueryManagerDAO().getAllVenues(PaginationUtil.excludeEventDays);
		
		if(null != venues && !venues.isEmpty()){
			allVenueList.clear();
			allVenueList.addAll(venues);
		}
		
		if(null != events && !events.isEmpty()){
			allEventList.clear();
			allEventList.addAll(events);
		}
		
		
		System.out.println("NORMAL_KEY_SEARCH_CACEH_UTIL Ends : "+new Date());
		System.out.println("NORMAL_KEY_SEARCH_CACHE: Job Completed :" + (System.currentTimeMillis() - startTime) / 1000);
		System.out.println("NORMAL_KEY_SEARCH_CACHE: TIME TO LAST RTF NORMAL SEARCH KEY CACHE UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
	}
	
}
