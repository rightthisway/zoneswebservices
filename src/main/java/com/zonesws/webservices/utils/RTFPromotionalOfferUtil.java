package com.zonesws.webservices.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.RTFPromotionalOfferHdr;


/**
 * RTFPromotionalOfferUtil to get the all rtf promotional offers of artist, venue, grand child, child, parent 
 * @author kulaganathan
 *
 */
public class RTFPromotionalOfferUtil extends QuartzJobBean implements StatefulJob {
	
	public static Map<String, Boolean> promoCodeMap = new ConcurrentHashMap<String, Boolean>();
	public static Map<String, RTFPromotionalOfferHdr> objPromoCodeMap = new ConcurrentHashMap<String, RTFPromotionalOfferHdr>();
	
	/*public static void init(){
		
	 try{
		 
		Long startTime = System.currentTimeMillis();
		
		List<RTFPromotionalOffer> promotionalOffers = DAORegistry.getRtfPromotionalOfferDAO().getAllActivePromotionalOffers();
		
		if(null == promotionalOffers || promotionalOffers.isEmpty()){
			promoCodeMap.clear();
			objPromoCodeMap.clear();
			return;
		}
		
		Map<String, RTFPromotionalOffer> objPromoCodeMapTemp = new ConcurrentHashMap<String, RTFPromotionalOffer>();
		Map<String, Boolean> promoCodeMapTemp = new ConcurrentHashMap<String, Boolean>();
		
		for (RTFPromotionalOffer obj : promotionalOffers) {
			objPromoCodeMapTemp.put(obj.getPromoCode().toUpperCase(), obj);
			promoCodeMapTemp.put(obj.getPromoCode().toUpperCase(), Boolean.TRUE);
		}
		
		if(null != promoCodeMapTemp && !promoCodeMapTemp.isEmpty()){
			promoCodeMap = new ConcurrentHashMap<String, Boolean>(promoCodeMapTemp);
			objPromoCodeMap = new ConcurrentHashMap<String, RTFPromotionalOffer>(objPromoCodeMapTemp);
		}
		System.out.println("TIME TO LAST RTF PROMOTIONAL OFFER UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	}*/
	
	/*public static Boolean validateRTFPromoCode(String promoCode){
		Boolean isValid = promoCodeMap.get(promoCode.toUpperCase());
		if(isValid == null || !isValid){
			RTFPromotionalOffer obj = DAORegistry.getRtfPromotionalOfferDAO().getActivePromotionalOfferByCode(promoCode);
			if(null == obj){
				return false;
			}
			promoCodeMap.put(promoCode.toUpperCase(), Boolean.TRUE);
			objPromoCodeMap.put(promoCode.toUpperCase(), obj);
		}
		return true;
	}*/
	
	public static Boolean validateRTFPromoCode(String promoCode){
		RTFPromotionalOfferHdr obj = objPromoCodeMap.get(promoCode.toUpperCase());
		if(obj == null){
			obj = DAORegistry.getRtfPromotionalOfferHdrDAO().getActivePromotionalOfferByCode(promoCode);
			if(null == obj){
				return false;
			}
		}
		boolean isExpired = isExpiredPromoCode(obj);
		if(!isExpired){
			promoCodeMap.put(promoCode.toUpperCase(), Boolean.TRUE);
			objPromoCodeMap.put(promoCode.toUpperCase(), obj);
			return true;
		}else{
			return false;
		}
	}
	
	
	public static void updateRevisedObj(RTFPromotionalOfferHdr offer){
		objPromoCodeMap.put(offer.getPromoCode().toUpperCase(), offer);
	}
	
	public static Boolean isExpiredPromoCode(RTFPromotionalOfferHdr offer){
		Date today = new Date();
		if(offer.getStartDate().after(today)){
			return true;
		}
		if(offer.getMaxOrders()!= null && offer.getNoOfOrders()!=null
				&& (offer.getMaxOrders() == offer.getNoOfOrders() ||
				offer.getMaxOrders().equals(offer.getNoOfOrders()) || offer.getNoOfOrders() > offer.getMaxOrders()) ){
			return true;
		}
		if(offer.getEndDate().before(today)){
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		Date curDate = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH,5);
		Date toDate = calendar.getTime();
		Calendar calendar1 = new GregorianCalendar();
		//calendar1.add(Calendar.DAY_OF_MONTH,1);
		Date fromDate = calendar1.getTime();
		
		System.out.println("fromDate======"+fromDate);
		System.out.println("toDate======"+toDate);
		System.out.println("curDate======"+curDate);
		
		
		
		if(fromDate.after(curDate)){
			System.out.println("11111");
		}
		
	}
	
	public static RTFPromotionalOfferHdr getPromoCodeObjByPromoCode(String promoCode){
		RTFPromotionalOfferHdr obj = objPromoCodeMap.get(promoCode.toUpperCase());
		if(obj == null){
			obj = DAORegistry.getRtfPromotionalOfferHdrDAO().getActivePromotionalOfferByCode(promoCode);
			if(null == obj){
				return null;
			}
			promoCodeMap.put(promoCode.toUpperCase(), Boolean.TRUE);
			objPromoCodeMap.put(promoCode.toUpperCase(), obj);
		}
		return obj;
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}
	
	
	public static void init(){
		
		 try{
			 
			Long startTime = System.currentTimeMillis();
			
			List<RTFPromotionalOfferHdr> rtfPromotionalOfferHdrs = DAORegistry.getRtfPromotionalOfferHdrDAO().getAllActivePromotionalOffers();
			
			if(null == rtfPromotionalOfferHdrs || rtfPromotionalOfferHdrs.isEmpty()){
				promoCodeMap.clear();
				objPromoCodeMap.clear();
				return;
			}
			
			Map<String, RTFPromotionalOfferHdr> objPromoCodeMapTemp = new ConcurrentHashMap<String, RTFPromotionalOfferHdr>();
			Map<String, Boolean> promoCodeMapTemp = new ConcurrentHashMap<String, Boolean>();
			
			for (RTFPromotionalOfferHdr obj : rtfPromotionalOfferHdrs) {
				objPromoCodeMapTemp.put(obj.getPromoCode().toUpperCase(), obj);
				promoCodeMapTemp.put(obj.getPromoCode().toUpperCase(), Boolean.TRUE);
			}
			
			if(null != promoCodeMapTemp && !promoCodeMapTemp.isEmpty()){
				promoCodeMap = new ConcurrentHashMap<String, Boolean>(promoCodeMapTemp);
				objPromoCodeMap = new ConcurrentHashMap<String, RTFPromotionalOfferHdr>(objPromoCodeMapTemp);
			}
			System.out.println("TIME TO LAST RTF PROMOTIONAL OFFER UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		}
	
}
