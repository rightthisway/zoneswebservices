package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.data.ParentCategory;
import com.zonesws.webservices.data.RTFCategorySearch;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.utils.list.ArtistState;
import com.zonesws.webservices.utils.list.VenueResult;


/**
 * Cache Util to get the all active event , artist ,venue information from application cache.
 * @author kulaganathan
 *
 */
public class RTFSearchCacheUtilOld extends QuartzJobBean implements StatefulJob {
	public static Map<String, HashMap<String, Object>> autoSearchMap = new ConcurrentHashMap<String, HashMap<String, Object>>();
	public static Map<String, ArrayList<Event>> eventListMap = new ConcurrentHashMap<String, ArrayList<Event>>();
	public static Map<String, ArrayList<Event>> stateWiseEventListMap = new ConcurrentHashMap<String, ArrayList<Event>>();
	public static Map<String, ArrayList<VenueResult>> venueListMap = new ConcurrentHashMap<String, ArrayList<VenueResult>>();
	public static Map<String, ArrayList<VenueResult>> stateWiseVenueListMap = new ConcurrentHashMap<String, ArrayList<VenueResult>>();
	public static Map<String, ArrayList<Artist>> artistListMap = new ConcurrentHashMap<String, ArrayList<Artist>>();
	public static Map<String, String> cacheSearchKeyWithTypeMap = new ConcurrentHashMap<String, String>();
	public static Set<String> cacheSearchKeys = new HashSet<String>();
	public static Map<String, Set<Integer>> artistIdsByState = new ConcurrentHashMap<String, Set<Integer>>();
	public static boolean isRunning = false;
	
	public void init(){
		
	 try{
		 
		if(isRunning){
			return;
		}
		 
		isRunning = true;
		 
		Long startTime = System.currentTimeMillis();
		
		List<ArtistState> stateArtistList = DAORegistry.getQueryManagerDAO().getAllArtistStates();
		
		Map<String, Set<Integer>> artistIdsByStateTemp = new ConcurrentHashMap<String, Set<Integer>>();
		
		for (ArtistState artistState : stateArtistList) {
			
			try{
				Set<Integer> artistIds =  artistIdsByStateTemp.get(artistState.getState().toLowerCase());
				if(null != artistIds && !artistIds.isEmpty()){
					 artistIdsByStateTemp.get(artistState.getState().toLowerCase()).add(artistState.getArtistId());
				}else{
					artistIds = new HashSet<Integer>();
					artistIds.add(artistState.getArtistId());
					artistIdsByStateTemp.put(artistState.getState().toLowerCase(),artistIds);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if(null != artistIdsByStateTemp && !artistIdsByStateTemp.isEmpty()){
			artistIdsByState.clear();
			artistIdsByState.putAll(artistIdsByStateTemp);
		}
		
		System.out.println("RTFSEARCHCAHCE :  PARENTCAT:> Begins-->"+new Date());
		
		List<ParentCategory> parentCategories = DAORegistry.getParentCategoryDAO().getAll();
		
		int nsMaxRow = PaginationUtil.cacheMaxResultCount;
		int asMaxRow = 25;
		int i = 1 ;
		
		HashMap<String, Object> autoSearchObj = null;
		HashMap<String, Object> normalSearchArtistObjMap = null;
		HashMap<String, Object> normalSearchVenueObj = null;
		HashMap<String, Object> normalSearchEventObjMap = null;
		String key = null;
		
		
		Map<String, HashMap<String, Object>> autoSearchMapTemp = new ConcurrentHashMap<String, HashMap<String, Object>>();
		Map<String, ArrayList<Event>> eventListMapTemp = new ConcurrentHashMap<String, ArrayList<Event>>();
		Map<String, ArrayList<Event>> stateWiseEventListMapTemp = new ConcurrentHashMap<String, ArrayList<Event>>();
		Map<String, ArrayList<VenueResult>> venueListMapTemp = new ConcurrentHashMap<String, ArrayList<VenueResult>>();
		Map<String, ArrayList<VenueResult>> stateWiseVenueListMapTemp = new ConcurrentHashMap<String, ArrayList<VenueResult>>();
		Map<String, ArrayList<Artist>> artistListMapTemp = new ConcurrentHashMap<String, ArrayList<Artist>>();
		Map<String, String> cacheSearchKeyWithTypeMapTemp = new ConcurrentHashMap<String, String>();
		Set<String> cacheSearchKeysTemp = new HashSet<String>();
		
		for (ParentCategory parentCategory : parentCategories) {
			
			try{
				System.out.println("RTFSEARCHCAHCE :  <--PARENTCAT-->"+parentCategory.getName()+"<==B==>"+parentCategories.size()+": "+new Date());
				
				key = parentCategory.getName().replaceAll("\\W", "").toLowerCase();
				
				autoSearchObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,parentCategory.getName(), 1, asMaxRow,null,
						null,null,null,null,null,"AUTOSEARCH",1,1,
						"ALL",parentCategory.getId(),null,null,null,null);
				autoSearchMapTemp.put("ParentCatId_"+parentCategory.getId(), autoSearchObj);
				autoSearchMapTemp.put(key, autoSearchObj);
				
				normalSearchArtistObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
						null,null,null,null,null,"NORMALSEARCH",1,1,
						"ARTIST",parentCategory.getId(),null,null,null,null);
				ArrayList<Artist> searchArtistList = normalSearchArtistObjMap.get("artist") != null ? (ArrayList<Artist>) (Object) normalSearchArtistObjMap.get("artist") : new ArrayList<Artist>();
				artistListMapTemp.put(key, searchArtistList);
				artistListMapTemp.put("ParentCatId_"+parentCategory.getId(), searchArtistList);
				
				normalSearchVenueObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
						null,null,null,null,null,"NORMALSEARCH",1,1,
						"VENUE",parentCategory.getId(),null,null,null,null);
				ArrayList<VenueResult> venueResultList = normalSearchVenueObj.get("venue") != null ? (ArrayList<VenueResult>) (Object) normalSearchVenueObj.get("venue") : new ArrayList<VenueResult>();
				venueListMapTemp.put(key, venueResultList);
				venueListMapTemp.put("ParentCatId_"+parentCategory.getId(), venueResultList);
				
				if(null != venueResultList && venueResultList.size() >0 ){
					
					for (VenueResult venueResult : venueResultList) {
						
						ArrayList<VenueResult> list =  stateWiseVenueListMapTemp.get(key+"_"+venueResult.getVenueState().toLowerCase());
						
						if(null != list && !list.isEmpty()){
							list.add(venueResult);
						}else {
							list = new ArrayList<VenueResult>();
							list.add(venueResult);
							cacheSearchKeysTemp.add(key+"_"+venueResult.getVenueState().toLowerCase());
							cacheSearchKeysTemp.add("ParentCatId_"+parentCategory.getId()+"_"+venueResult.getVenueState().toLowerCase());
							
						}
						stateWiseVenueListMapTemp.put(key+"_"+venueResult.getVenueState().toLowerCase(),list);
						stateWiseVenueListMapTemp.put("ParentCatId_"+parentCategory.getId()+"_"+venueResult.getVenueState().toLowerCase(),list);
						
					}
				}
				
				
				ArrayList<Event> allEvents = new ArrayList<Event>();
				
				for(int j=1;j<15;j++){
					normalSearchEventObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, j, nsMaxRow,null,
							null,null,null,null,null,"NORMALSEARCH",1,1,
							"EVENTS",parentCategory.getId(),null,null,null,null);
					ArrayList<Event> events = normalSearchEventObjMap.get("event") != null ? (ArrayList<Event>) (Object) normalSearchEventObjMap.get("event") : new ArrayList<Event>();
					if(null != events && events.size() >0 ){
						
						for (Event event : events) {
							
							ArrayList<Event> list =  stateWiseEventListMapTemp.get(key+"_"+event.getState().toLowerCase());
							
							if(null != list && !list.isEmpty()){
								list.add(event);
							}else {
								list = new ArrayList<Event>();
								list.add(event);
								cacheSearchKeysTemp.add(key+"_"+event.getState().toLowerCase());
								cacheSearchKeysTemp.add("ParentCatId_"+parentCategory.getId()+"_"+event.getState().toLowerCase());
							}
							stateWiseEventListMapTemp.put(key+"_"+event.getState().toLowerCase(),list);
							stateWiseEventListMapTemp.put("ParentCatId_"+parentCategory.getId()+"_"+event.getState().toLowerCase(),list);
							
						}
						allEvents.addAll(events);
					}else{
						break;
					}
				}
				eventListMapTemp.put(key, allEvents);
				eventListMapTemp.put("ParentCatId_"+parentCategory.getId(), allEvents);
				
				cacheSearchKeysTemp.add("ParentCatId_"+parentCategory.getId());
				cacheSearchKeysTemp.add(key);
				
				cacheSearchKeyWithTypeMapTemp.put(key, ArtistReferenceType.PARENT_NAME.toString());
				
				System.out.println("RTFSEARCHCAHCE :  PARENTCAT<-->"+parentCategory.getName()+"<==E==>"+parentCategories.size()+": "+new Date());
				
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		
		if(null != autoSearchMapTemp && !autoSearchMapTemp.isEmpty()){
			autoSearchMap.clear();
			autoSearchMap.putAll(autoSearchMapTemp);
		}
		
		if(null != eventListMapTemp && !eventListMapTemp.isEmpty()){
			eventListMap.clear();
			eventListMap.putAll(eventListMapTemp);
		}
		
		if(null != stateWiseEventListMapTemp && !stateWiseEventListMapTemp.isEmpty()){
			stateWiseEventListMap.clear();
			stateWiseEventListMap.putAll(stateWiseEventListMapTemp);
		}
		
		if(null != venueListMapTemp && !venueListMapTemp.isEmpty()){
			venueListMap.clear();
			venueListMap.putAll(venueListMapTemp);
		}
		
		if(null != stateWiseVenueListMapTemp && !stateWiseVenueListMapTemp.isEmpty()){
			stateWiseVenueListMap.clear();
			stateWiseVenueListMap.putAll(stateWiseVenueListMapTemp);
		}
		
		if(null != artistListMapTemp && !artistListMapTemp.isEmpty()){
			artistListMap.clear();
			artistListMap.putAll(artistListMapTemp);
		}
		if(null != cacheSearchKeyWithTypeMapTemp && !cacheSearchKeyWithTypeMapTemp.isEmpty()){
			cacheSearchKeyWithTypeMap.clear();
			cacheSearchKeyWithTypeMap.putAll(cacheSearchKeyWithTypeMapTemp);
		}
		if(null != cacheSearchKeysTemp && !cacheSearchKeysTemp.isEmpty()){
			cacheSearchKeys.clear();
			cacheSearchKeys.addAll(cacheSearchKeysTemp);
		}
		
		List<ChildCategory> childCategories = DAORegistry.getChildCategoryDAO().getAll();
		
		i = 0 ;
		for (ChildCategory childCategory : childCategories) {
		
			
			try{
				if(null != childCategory.getDisplayOnSearch() && childCategory.getDisplayOnSearch()){
					
					key = childCategory.getName().replaceAll("\\W", "").toLowerCase();
					
					System.out.println("RTFSEARCHCAHCE :  CHILDCAT-->"+i+"<==B==>"+childCategories.size()+": "+new Date());
					
					autoSearchObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,childCategory.getName(), 1, asMaxRow,null,
							null,null,null,null,childCategory.getId(),"AUTOSEARCH",1,1,
							"ALL",null,null,null,null,null);
					
					autoSearchMapTemp.put("ChildCatId_"+childCategory.getId(), autoSearchObj);
					autoSearchMapTemp.put(key, autoSearchObj);

					normalSearchArtistObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
							null,null,null,null,childCategory.getId(),"NORMALSEARCH",1,1,
							"ARTIST",null,null,null,null,null);
					
					ArrayList<Artist> searchArtistList = normalSearchArtistObjMap.get("artist") != null ? (ArrayList<Artist>) (Object) normalSearchArtistObjMap.get("artist") : new ArrayList<Artist>();
					artistListMapTemp.put(key, searchArtistList);
					artistListMapTemp.put("ChildCatId_"+childCategory.getId(), searchArtistList);
					
					normalSearchVenueObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
							null,null,null,null,childCategory.getId(),"NORMALSEARCH",1,1,
							"VENUE",null,null,null,null,null);
					
					ArrayList<VenueResult> venueResultList = normalSearchVenueObj.get("venue") != null ? (ArrayList<VenueResult>) (Object) normalSearchVenueObj.get("venue") : new ArrayList<VenueResult>();
					venueListMapTemp.put(key, venueResultList);
					venueListMapTemp.put("ChildCatId_"+childCategory.getId(), venueResultList);
					
					if(null != venueResultList && venueResultList.size() >0 ){
						
						for (VenueResult venueResult : venueResultList) {
							
							ArrayList<VenueResult> list =  stateWiseVenueListMapTemp.get(key+"_"+venueResult.getVenueState().toLowerCase());
							
							if(null != list && !list.isEmpty()){
								list.add(venueResult);
							}else {
								list = new ArrayList<VenueResult>();
								list.add(venueResult);
								cacheSearchKeysTemp.add(key+"_"+venueResult.getVenueState().toLowerCase());
								cacheSearchKeysTemp.add("ChildCatId_"+childCategory.getId()+"_"+venueResult.getVenueState().toLowerCase());
							}
							stateWiseVenueListMapTemp.put(key+"_"+venueResult.getVenueState().toLowerCase(),list);
							stateWiseVenueListMapTemp.put("ChildCatId_"+childCategory.getId()+"_"+venueResult.getVenueState().toLowerCase(),list);
							
						}
					}
					
					
					ArrayList<Event> allEvents = new ArrayList<Event>();
					
					for(int j=1;j<15;j++){
						System.out.println("CHILDCAT : "+childCategory.getName()+"<======JJJJJJJJJJJJJ=======>"+j);
						normalSearchEventObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, j, nsMaxRow,null,
								null,null,null,null,childCategory.getId(),"NORMALSEARCH",1,1,
								"EVENTS",null,null,null,null,null);
						
						ArrayList<Event> events = normalSearchEventObjMap.get("event") != null ? (ArrayList<Event>) (Object) normalSearchEventObjMap.get("event") : new ArrayList<Event>();
						if(null != events && events.size() >0 ){
							for (Event event : events) {
								
								ArrayList<Event> list =  stateWiseEventListMapTemp.get(key+"_"+event.getState().toLowerCase());
								
								if(null != list && !list.isEmpty()){
									list.add(event);
								}else {
									list = new ArrayList<Event>();
									list.add(event);
									cacheSearchKeysTemp.add(key+"_"+event.getState().toLowerCase());
									cacheSearchKeysTemp.add("ChildCatId_"+childCategory.getId()+"_"+event.getState().toLowerCase());
								}
								stateWiseEventListMapTemp.put(key+"_"+event.getState().toLowerCase(),list);
								stateWiseEventListMapTemp.put("ChildCatId_"+childCategory.getId()+"_"+event.getState().toLowerCase(),list);
								
							}
							
							allEvents.addAll(events);
						}else{
							break;
						}
					}
					eventListMapTemp.put(key, allEvents);
					eventListMapTemp.put("ChildCatId_"+childCategory.getId(), allEvents);
					
					cacheSearchKeysTemp.add("ChildCatId_"+childCategory.getId());
					cacheSearchKeysTemp.add(key);
					
					cacheSearchKeyWithTypeMapTemp.put(key, ArtistReferenceType.CHILD_NAME.toString());
					
					System.out.println("RTFSEARCHCAHCE :  CHILDCAT-->"+i+"<==E==>"+childCategories.size()+": "+new Date());
					i++;
					
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}
		
		
		if(null != autoSearchMapTemp && !autoSearchMapTemp.isEmpty()){
			autoSearchMap.clear();
			autoSearchMap.putAll(autoSearchMapTemp);
		}
		
		if(null != eventListMapTemp && !eventListMapTemp.isEmpty()){
			eventListMap.clear();
			eventListMap.putAll(eventListMapTemp);
		}
		
		if(null != stateWiseEventListMapTemp && !stateWiseEventListMapTemp.isEmpty()){
			stateWiseEventListMap.clear();
			stateWiseEventListMap.putAll(stateWiseEventListMapTemp);
		}
		
		if(null != venueListMapTemp && !venueListMapTemp.isEmpty()){
			venueListMap.clear();
			venueListMap.putAll(venueListMapTemp);
		}
		
		if(null != stateWiseVenueListMapTemp && !stateWiseVenueListMapTemp.isEmpty()){
			stateWiseVenueListMap.clear();
			stateWiseVenueListMap.putAll(stateWiseVenueListMapTemp);
		}
		
		if(null != artistListMapTemp && !artistListMapTemp.isEmpty()){
			artistListMap.clear();
			artistListMap.putAll(artistListMapTemp);
		}
		if(null != cacheSearchKeyWithTypeMapTemp && !cacheSearchKeyWithTypeMapTemp.isEmpty()){
			cacheSearchKeyWithTypeMap.clear();
			cacheSearchKeyWithTypeMap.putAll(cacheSearchKeyWithTypeMapTemp);
		}
		if(null != cacheSearchKeysTemp && !cacheSearchKeysTemp.isEmpty()){
			cacheSearchKeys.clear();
			cacheSearchKeys.addAll(cacheSearchKeysTemp);
		}
		
		
		
		System.out.println("RTFSEARCHCAHCE :   CHILDCAT:> ENDS-->"+new Date());
		
		List<GrandChildCategory> grandChildCategories = DAORegistry.getGrandChildCategoryDAO().getAll();
		i = 0;
		for (GrandChildCategory grandChildCategory : grandChildCategories) {
			
			try{
				
				if(null != grandChildCategory.getDisplayOnSearch() && grandChildCategory.getDisplayOnSearch()){
					
					key = grandChildCategory.getName().replaceAll("\\W", "").toLowerCase();
					
					System.out.println("RTFSEARCHCAHCE :  GRANDCHILD-->"+i+"<==B==>"+grandChildCategories.size()+": "+new Date());
					
					autoSearchObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
							null,null,grandChildCategory.getId(),null,null,"NORMALSEARCH",1,1,
							"ALL",null,null,null,null,null);
					
					autoSearchMapTemp.put("GrandChildCatId_"+grandChildCategory.getId(), autoSearchObj);
					autoSearchMapTemp.put(key, autoSearchObj);
					
					normalSearchArtistObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
							null,null,grandChildCategory.getId(),null,null,"NORMALSEARCH",1,1,
							"ARTIST",null,null,null,null,null);
					
					ArrayList<Artist> searchArtistList = normalSearchArtistObjMap.get("artist") != null ? (ArrayList<Artist>) (Object) normalSearchArtistObjMap.get("artist") : new ArrayList<Artist>();
					artistListMapTemp.put(key, searchArtistList);
					artistListMapTemp.put("GrandChildCatId_"+grandChildCategory.getId(), searchArtistList);
					
					normalSearchVenueObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
							null,null,grandChildCategory.getId(),null,null,"NORMALSEARCH",1,1,
							"VENUE",null,null,null,null,null);
					
					ArrayList<VenueResult> venueResultList = normalSearchVenueObj.get("venue") != null ? (ArrayList<VenueResult>) (Object) normalSearchVenueObj.get("venue") : new ArrayList<VenueResult>();
					venueListMapTemp.put(key, venueResultList);
					venueListMapTemp.put("GrandChildCatId_"+grandChildCategory.getId(), venueResultList);
					
					if(null != venueResultList && venueResultList.size() >0 ){
						
						for (VenueResult venueResult : venueResultList) {
							
							ArrayList<VenueResult> list =  stateWiseVenueListMapTemp.get(key+"_"+venueResult.getVenueState().toLowerCase());
							
							if(null != list && !list.isEmpty()){
								list.add(venueResult);
							}else {
								list = new ArrayList<VenueResult>();
								list.add(venueResult);
								cacheSearchKeysTemp.add(key+"_"+venueResult.getVenueState().toLowerCase());
								cacheSearchKeysTemp.add("GrandChildCatId_"+grandChildCategory.getId()+"_"+venueResult.getVenueState().toLowerCase());
							}
							stateWiseVenueListMapTemp.put(key+"_"+venueResult.getVenueState().toLowerCase(),list);
							stateWiseVenueListMapTemp.put("GrandChildCatId_"+grandChildCategory.getId()+"_"+venueResult.getVenueState().toLowerCase(),list);
							
						}
					}
					
					
					ArrayList<Event> allEvents = new ArrayList<Event>();
					
					for(int j=1;j<15;j++){
						System.out.println("GRANDCHILD : "+grandChildCategory.getName()+"======JJJJJJJJJJJJJ======>"+j);
						
						normalSearchEventObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, j, nsMaxRow,null,
								null,null,grandChildCategory.getId(),null,null,"NORMALSEARCH",1,1,
								"EVENTS",null,null,null,null,null);
						
						ArrayList<Event> events = normalSearchEventObjMap.get("event") != null ? (ArrayList<Event>) (Object) normalSearchEventObjMap.get("event") : new ArrayList<Event>();
						if(null != events && events.size() >0 ){
							for (Event event : events) {
								
								ArrayList<Event> list =  stateWiseEventListMapTemp.get(key+"_"+event.getState().toLowerCase());
								
								if(null != list && !list.isEmpty()){
									list.add(event);
								}else {
									list = new ArrayList<Event>();
									list.add(event);
									cacheSearchKeysTemp.add(key+"_"+event.getState().toLowerCase());
									cacheSearchKeysTemp.add("GrandChildCatId_"+grandChildCategory.getId()+"_"+event.getState().toLowerCase());
								}
								stateWiseEventListMapTemp.put(key+"_"+event.getState().toLowerCase(),list);
								stateWiseEventListMapTemp.put("GrandChildCatId_"+grandChildCategory.getId()+"_"+event.getState().toLowerCase(),list);
								
							}
							allEvents.addAll(events);
						}else{
							break;
						}
					}
					eventListMapTemp.put(key, allEvents);
					eventListMapTemp.put("GrandChildCatId_"+grandChildCategory.getId(), allEvents);
					
					cacheSearchKeysTemp.add("GrandChildCatId_"+grandChildCategory.getId());
					cacheSearchKeysTemp.add(key);
					
					cacheSearchKeyWithTypeMapTemp.put(key, ArtistReferenceType.GRANDCHILD_NAME.toString());
					
					System.out.println("RTFSEARCHCAHCE :  GRANDCHILDCAT-->"+i+"<==E==>"+grandChildCategories.size()+": "+new Date());
					i++;
					
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
		}
		
		if(null != autoSearchMapTemp && !autoSearchMapTemp.isEmpty()){
			autoSearchMap.clear();
			autoSearchMap.putAll(autoSearchMapTemp);
		}
		
		if(null != eventListMapTemp && !eventListMapTemp.isEmpty()){
			eventListMap.clear();
			eventListMap.putAll(eventListMapTemp);
		}
		
		if(null != stateWiseEventListMapTemp && !stateWiseEventListMapTemp.isEmpty()){
			stateWiseEventListMap.clear();
			stateWiseEventListMap.putAll(stateWiseEventListMapTemp);
		}
		
		if(null != venueListMapTemp && !venueListMapTemp.isEmpty()){
			venueListMap.clear();
			venueListMap.putAll(venueListMapTemp);
		}
		
		if(null != stateWiseVenueListMapTemp && !stateWiseVenueListMapTemp.isEmpty()){
			stateWiseVenueListMap.clear();
			stateWiseVenueListMap.putAll(stateWiseVenueListMapTemp);
		}
		
		if(null != artistListMapTemp && !artistListMapTemp.isEmpty()){
			artistListMap.clear();
			artistListMap.putAll(artistListMapTemp);
		}
		if(null != cacheSearchKeyWithTypeMapTemp && !cacheSearchKeyWithTypeMapTemp.isEmpty()){
			cacheSearchKeyWithTypeMap.clear();
			cacheSearchKeyWithTypeMap.putAll(cacheSearchKeyWithTypeMapTemp);
		}
		if(null != cacheSearchKeysTemp && !cacheSearchKeysTemp.isEmpty()){
			cacheSearchKeys.clear();
			cacheSearchKeys.addAll(cacheSearchKeysTemp);
		}
		
		System.out.println("RTFSEARCHCAHCE :  GRANDCHILDCAT:> ENDS-->"+new Date());
		
		List<RTFCategorySearch> catSearchList = DAORegistry.getQueryManagerDAO().getAllRTFSearchCategories();
		i = 0;
		for (RTFCategorySearch rtfSearchObj : catSearchList) {
			
			try{
				
				key = rtfSearchObj.getDisplayName().replaceAll("\\W", "").toLowerCase();
				
				System.out.println("RTFSEARCHCAHCE :  RTFCATEGORIES-->"+i+"<==B==>"+catSearchList.size()+": "+new Date());
				
				autoSearchObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,rtfSearchObj.getDisplayName().replaceAll("\\W", "%"), 1, asMaxRow,null,
						null,null,rtfSearchObj.getGrandChildId(),null,rtfSearchObj.getChildId(),"AUTOSEARCH",1,1,
						"ALL",null,null,null,null,null);
				
				autoSearchMapTemp.put("RTFCatSearchId_"+rtfSearchObj.getId(), autoSearchObj);
				autoSearchMapTemp.put(key, autoSearchObj);
				
				normalSearchArtistObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
						null,null,rtfSearchObj.getGrandChildId(),null,rtfSearchObj.getChildId(),"NORMALSEARCH",1,1,
						"ARTIST",null,null,null,null,null);
				ArrayList<Artist> searchArtistList = normalSearchArtistObjMap.get("artist") != null ? (ArrayList<Artist>) (Object) normalSearchArtistObjMap.get("artist") : new ArrayList<Artist>();
				artistListMapTemp.put(key, searchArtistList);
				artistListMapTemp.put("RTFCatSearchId_"+rtfSearchObj.getId(), searchArtistList);
				
				normalSearchVenueObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, 1, nsMaxRow,null,
						null,null,rtfSearchObj.getGrandChildId(),null,rtfSearchObj.getChildId(),"NORMALSEARCH",1,1,
						"VENUE",null,null,null,null,null);
				ArrayList<VenueResult> venueResultList = normalSearchVenueObj.get("venue") != null ? (ArrayList<VenueResult>) (Object) normalSearchVenueObj.get("venue") : new ArrayList<VenueResult>();
				venueListMapTemp.put(key, venueResultList);
				venueListMapTemp.put("RTFCatSearchId_"+rtfSearchObj.getId(), venueResultList);
				
				if(null != venueResultList && venueResultList.size() >0 ){
					
					for (VenueResult venueResult : venueResultList) {
						
						ArrayList<VenueResult> list =  stateWiseVenueListMapTemp.get(key+"_"+venueResult.getVenueState().toLowerCase());
						
						if(null != list && !list.isEmpty()){
							list.add(venueResult);
						}else {
							list = new ArrayList<VenueResult>();
							list.add(venueResult);
							cacheSearchKeysTemp.add(key+"_"+venueResult.getVenueState().toLowerCase());
							cacheSearchKeysTemp.add("RTFCatSearchId_"+rtfSearchObj.getId()+"_"+venueResult.getVenueState().toLowerCase());
						}
						stateWiseVenueListMapTemp.put(key+"_"+venueResult.getVenueState().toLowerCase(),list);
						stateWiseVenueListMapTemp.put("RTFCatSearchId_"+rtfSearchObj.getId()+"_"+venueResult.getVenueState().toLowerCase(),list);
						
						
					}
				}
				
				ArrayList<Event> allEvents = new ArrayList<Event>();
				
				for(int j=1;j<15;j++){
					System.out.println("RTFCAT : "+rtfSearchObj.getDisplayName()+"========JJJJJJJJJJJJJ=========>"+j);
					normalSearchEventObjMap = DAORegistry.getEventDAO().getAllEventsBySearchKey(false,null, j, nsMaxRow,null,
							null,null,rtfSearchObj.getGrandChildId(),null,rtfSearchObj.getChildId(),"NORMALSEARCH",1,1,
							"EVENTS",null,null,null,null,null);
					ArrayList<Event> events = normalSearchEventObjMap.get("event") != null ? (ArrayList<Event>) (Object) normalSearchEventObjMap.get("event") : new ArrayList<Event>();
					if(null != events && events.size() >0 ){
						for (Event event : events) {
							
							ArrayList<Event> list =  stateWiseEventListMapTemp.get(key+"_"+event.getState().toLowerCase());
							
							if(null != list && !list.isEmpty()){
								list.add(event);
							}else {
								list = new ArrayList<Event>();
								list.add(event);
								cacheSearchKeysTemp.add(key+"_"+event.getState().toLowerCase());
								cacheSearchKeysTemp.add("RTFCatSearchId_"+rtfSearchObj.getId()+"_"+event.getState().toLowerCase());
							}
							stateWiseEventListMapTemp.put(key+"_"+event.getState().toLowerCase(),list);
							stateWiseEventListMapTemp.put("RTFCatSearchId_"+rtfSearchObj.getId()+"_"+event.getState().toLowerCase(),list);
							
						}
						allEvents.addAll(events);
					}else{
						break;
					}
				}
				eventListMapTemp.put(key, allEvents);
				eventListMapTemp.put("RTFCatSearchId_"+rtfSearchObj.getId(), allEvents);
				
				cacheSearchKeysTemp.add("RTFCatSearchId_"+rtfSearchObj.getId());
				cacheSearchKeysTemp.add(key);
				
				cacheSearchKeyWithTypeMapTemp.put(key, ArtistReferenceType.RTFCATS_NAME.toString());
				
				System.out.println("RTFSEARCHCAHCE :  RTFCATEGORIES-->"+i+"<==E==>"+catSearchList.size()+": "+new Date());
				i++;
				
			}catch(Exception e){
				e.printStackTrace();
				
			}
			
			
		}
		
		
		if(null != autoSearchMapTemp && !autoSearchMapTemp.isEmpty()){
			autoSearchMap.clear();
			autoSearchMap.putAll(autoSearchMapTemp);
		}
		
		if(null != eventListMapTemp && !eventListMapTemp.isEmpty()){
			eventListMap.clear();
			eventListMap.putAll(eventListMapTemp);
		}
		
		if(null != stateWiseEventListMapTemp && !stateWiseEventListMapTemp.isEmpty()){
			stateWiseEventListMap.clear();
			stateWiseEventListMap.putAll(stateWiseEventListMapTemp);
		}
		
		if(null != venueListMapTemp && !venueListMapTemp.isEmpty()){
			venueListMap.clear();
			venueListMap.putAll(venueListMapTemp);
		}
		
		if(null != stateWiseVenueListMapTemp && !stateWiseVenueListMapTemp.isEmpty()){
			stateWiseVenueListMap.clear();
			stateWiseVenueListMap.putAll(stateWiseVenueListMapTemp);
		}
		
		if(null != artistListMapTemp && !artistListMapTemp.isEmpty()){
			artistListMap.clear();
			artistListMap.putAll(artistListMapTemp);
		}
		if(null != cacheSearchKeyWithTypeMapTemp && !cacheSearchKeyWithTypeMapTemp.isEmpty()){
			cacheSearchKeyWithTypeMap.clear();
			cacheSearchKeyWithTypeMap.putAll(cacheSearchKeyWithTypeMapTemp);
		}
		if(null != cacheSearchKeysTemp && !cacheSearchKeysTemp.isEmpty()){
			cacheSearchKeys.clear();
			cacheSearchKeys.addAll(cacheSearchKeysTemp);
		}
		isRunning = false;
		i++;
		System.out.println("RTFSEARCHCAHCE: Job Completed :" + (System.currentTimeMillis() - startTime) / 1000);
		System.out.println("RTFSEARCHCAHCE: TIME TO LAST RTF SEARCH CACHE UTIL=" + (System.currentTimeMillis() - startTime) / 1000);
	 }catch(Exception e){
		 isRunning = false;
		 e.printStackTrace();
	 }
	}
	
	
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		//init();
	}
	
}
