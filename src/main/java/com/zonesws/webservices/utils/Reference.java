package com.zonesws.webservices.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Reference")
public class Reference{
	
	String Guid;
	String TranDate;
	String TranTime;
	String AccountBrand;
	String LastFour;
	public String getGuid() {
		return Guid;
	}
	public void setGuid(String guid) {
		Guid = guid;
	}
	public String getTranDate() {
		return TranDate;
	}
	public void setTranDate(String tranDate) {
		TranDate = tranDate;
	}
	public String getTranTime() {
		return TranTime;
	}
	public void setTranTime(String tranTime) {
		TranTime = tranTime;
	}
	public String getAccountBrand() {
		return AccountBrand;
	}
	public void setAccountBrand(String accountBrand) {
		AccountBrand = accountBrand;
	}
	public String getLastFour() {
		return LastFour;
	}
	public void setLastFour(String lastFour) {
		LastFour = lastFour;
	}
	
}