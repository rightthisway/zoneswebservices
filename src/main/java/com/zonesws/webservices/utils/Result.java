package com.zonesws.webservices.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Result")
public class Result{
	String ResponseCode;
	String ResponseText;
	String AvsResult;
	String CvvResult;
	String DebugInfo;
	String ApprovalCode;
	String NameVerificationResult;
	
	
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	public String getResponseText() {
		return ResponseText;
	}
	public void setResponseText(String responseText) {
		ResponseText = responseText;
	}
	public String getAvsResult() {
		return AvsResult;
	}
	public void setAvsResult(String avsResult) {
		AvsResult = avsResult;
	}
	public String getCvvResult() {
		return CvvResult;
	}
	public void setCvvResult(String cvvResult) {
		CvvResult = cvvResult;
	}
	public String getDebugInfo() {
		return DebugInfo;
	}
	public void setDebugInfo(String debugInfo) {
		DebugInfo = debugInfo;
	}
	public String getApprovalCode() {
		return ApprovalCode;
	}
	public void setApprovalCode(String approvalCode) {
		ApprovalCode = approvalCode;
	}
	public String getNameVerificationResult() {
		return NameVerificationResult;
	}
	public void setNameVerificationResult(String nameVerificationResult) {
		NameVerificationResult = nameVerificationResult;
	}
}