package com.zonesws.webservices.utils;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.LoyaltySettings;




public class RewardConversionUtil {
	
	
	
	public static Double getRewardDoller(Double activeRewardPoints){
		Double activeRewardDollers = 0.00;
		if(null != activeRewardPoints){
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double temp = activeRewardPoints * loyaltySettings.getDollerConversion();
			activeRewardDollers = Math.floor(temp);
		}
		return activeRewardDollers;
	}
	
	public static Integer getRewardPoints(Double dollers){
		Double rewardPoints = 0.00;
		LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
		rewardPoints = dollers * loyaltySettings.getRewardConversion();
		rewardPoints = Math.floor(rewardPoints);
		return rewardPoints.intValue();
	}
	
	public static Double getRewardDoller(Double activeRewardPoints,Double dollerConverstion) throws Exception{
		Double rewardDollers = 0.00;
		if(null != activeRewardPoints){
			Double temp = activeRewardPoints * dollerConverstion;
			rewardDollers = TicketUtil.getRoundedValue(temp);
		}
		return rewardDollers;
	}
	
	
	public static Double getAffiliateCashCredit(Double orderTotal,Double affiliateCashCreditConv) throws Exception{
		Double cash = 0.00;
		if(null != orderTotal){
			Double temp = orderTotal * affiliateCashCreditConv;
			cash = TicketUtil.getRoundedValue(temp);
		}
		return cash;
	}
	
	/*public static Double getBalanceRewardPoints(Double orderTotal,Double dollerConverstion){
		Double rewardDollers = 0.00;
		if(null != activeRewardPoints){
			Double temp = activeRewardPoints * dollerConverstion;
			rewardDollers = Math.floor(temp);
		}
		return rewardDollers;
	}*/
	
	
	public static Double getRewardPoints(Double dollers,Double rewardConverstion) throws Exception{
		Double rewardPoints = 0.00;
		rewardPoints = dollers * rewardConverstion;
		return TicketUtil.getRoundedValue(rewardPoints);
	}
	
	
	
	public static void main(String[] args) throws Exception {
		
		Double activeRewardPoints = 500.25765645; 
		Double dollerConverstion = 1.00;
		
		System.out.println(getRewardPoints(activeRewardPoints, dollerConverstion));
		
	}
	
}
