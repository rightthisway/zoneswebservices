package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.HashMap;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.enums.TabType;




/**
 * Search Event Construction to get the all active event artist details
 * @author kulaganathan
 *
 */
public class SearchConstructionUtil {
	
	
	/*public static HashMap<String, Object> getCacheObjectMapWithoutFilter(HashMap<String, Object> allObj,String searchKey,
			Integer parentId,Integer childId,Integer grandChildId,Integer rtfCatSearchId,Integer artistId,Integer venueId,
			Integer pageNumber,Integer artistPageNumber,Integer venuePageNumber, Integer maxRow,String startDateStr,
			String endDateStr,String state,String searchType,String tabType,Boolean isLocationFilterApplied ,
			Boolean dateFilterApplied, Integer eventExcludeDays , Boolean isOtherGrid,String artistRefValue,ArtistReferenceType artistRefType){
		
		TabType searchTab = TabType.valueOf(tabType);
		
		if(!tabType.equals("GRANDCHILD")) {
			
			String tempSearchKey = RTFGetCacheDetails.getSearchKey(searchKey, parentId, childId, grandChildId, rtfCatSearchId,
					isLocationFilterApplied,state);
			
			if(RTFGetCacheDetails.isCacheSearchKey(tempSearchKey) &&  (venueId == null && artistId == null) ){
				
				allObj = RTFGetCacheDetails.getCacheObjectMapWithoutFilter(pageNumber, artistPageNumber, venuePageNumber, maxRow, 
						tempSearchKey, searchKey, searchType,searchTab,isLocationFilterApplied,state,dateFilterApplied,startDateStr,
						endDateStr,isOtherGrid);
				
				//To get arefType and aRefValue based on parent,child grand child and rtfCats
				RTFGetCacheDetails.getSearchTypeWithKey(searchKey, parentId, childId, grandChildId, rtfCatSearchId, allObj);
				
			}else{
				
				if(!searchType.equals("AUTOSEARCH") && (null != grandChildId || null != parentId || childId != null)){
					
					allObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(searchKey, pageNumber, maxRow,startDateStr,
							endDateStr,artistId,grandChildId,venueId,childId,searchType,artistPageNumber,venuePageNumber,
							tabType,parentId,null,state,artistRefValue,artistRefType);
					allObj = getPaginationDetails(allObj, pageNumber, artistPageNumber, venuePageNumber, maxRow);
					
					if((isLocationFilterApplied || dateFilterApplied)){
						ArrayList<Integer> eventIds = allObj.get("eventIds") != null ? (ArrayList<Integer>) (Object) allObj.get("eventIds") : new ArrayList<Integer>();
						if(eventIds.size() <= 20){
							maxRow = PaginationUtil.otherEventGridMaxRows;
							HashMap<String, Object> otherLocObj = DAORegistry.getEventDAO().getAllEventsBySearchKey(searchKey, pageNumber, 
									maxRow,null,null,artistId,grandChildId,venueId,childId,searchType,
									artistPageNumber,venuePageNumber,"EVENTS",parentId,null,null,null,null);
							ArrayList<Event> otherEventList = otherLocObj.get("event") != null ? (ArrayList<Event>) (Object) otherLocObj.get("event") : new ArrayList<Event>();
							allObj = getOtherLocationEvents(allObj,eventIds, otherEventList, searchKey, isLocationFilterApplied, state, pageNumber,maxRow);
							allObj.put("showOtherEventGrid", true);
						}
					}
				}else{
					if(artistRefValue != null && artistRefType != null && artistId != null) {
						
						tempSearchKey = RTFGetCacheDetails.getSearchKeyByArtistRefType(artistRefType,artistRefValue,isLocationFilterApplied,state);
						if(RTFGetCacheDetails.isCacheSearchKey(tempSearchKey)){
							RTFGetCacheDetails.getAllCacheEventsByArtistAndReferenceFilters(pageNumber, maxRow, tempSearchKey, allObj,
									isLocationFilterApplied, dateFilterApplied, startDateStr, endDateStr, state, isOtherGrid, artistId);
							return allObj;
						}
					} 
						
					allObj =  NormalSearchKeyConstruction.getNormalSearchKeyResults(allObj, searchKey, childId, grandChildId, 
							artistId, venueId, pageNumber, artistPageNumber, venuePageNumber, maxRow, startDateStr, endDateStr, 
							state, searchType, searchTab, isLocationFilterApplied, dateFilterApplied, eventExcludeDays, isOtherGrid,artistRefValue,artistRefType);
				}
			}
		}
		return allObj;
	}*/
	
	public static HashMap<String, Object> getOtherLocationEvents(HashMap<String, Object> allObj,ArrayList<Integer> eventIds,ArrayList<Event> otherEventList,
			String searchKey,Boolean isLocationFilter,String state, Integer pageNumber, Integer maxRow){
		try{
			
			if(null == otherEventList || otherEventList.isEmpty()){
				allObj.put("OtherEvents", null);
				allObj.put("showMoreOtherEvents", false);
			} else {
				if(otherEventList.size() >= maxRow){
					allObj.put("showMoreOtherEvents", true);
				}else{
					allObj.put("showMoreOtherEvents", false);
				}
			}
			
			ArrayList<Event> allEvents = new ArrayList<Event>();
			Integer totalEventCount = 0 , currRows = pageNumber * maxRow;
			for (Event eventObj : otherEventList) {
				if(!eventIds.contains(eventObj.getEventId())){
					totalEventCount = eventObj.getTotalEvents();
					allEvents.add(eventObj);
				}
			}
			
			/*if(totalEventCount > currRows){
				allObj.put("showMoreOtherEvents", true);
			}else{
				allObj.put("showMoreOtherEvents", false);
			}*/
			
			allObj.put("OtherEvents", allEvents);
		}catch (Exception e) {
			allObj.put("OtherEvents", otherEventList);
			allObj.put("showOtherEventGrid", false);
			allObj.put("showMoreOtherEvents", false);
			e.printStackTrace();
		}
		return allObj;
	}
	
	
	/*public static HashMap<String, Object> getPaginationDetails(HashMap<String, Object> allObj, Integer pageNumber,Integer artistPageNumber,Integer venuePageNumber, Integer maxRow){
		
		Integer totalEvents = allObj.get("totalEvents") != null ? (Integer) (Object) allObj.get("totalEvents") : 0;
		Integer totalArtists = allObj.get("totalArtists") != null ? (Integer) (Object) allObj.get("totalArtists") : 0;
		Integer totalVenues = allObj.get("totalVenues") != null ? (Integer) (Object) allObj.get("totalVenues") : 0;
		
		Integer curCompletedEventRows = pageNumber * maxRow;
		Integer curCompletedVenuesRows = venuePageNumber * maxRow;
		Integer curCompletedArtistRows = artistPageNumber * maxRow;
		
		if(totalEvents > curCompletedEventRows){
			allObj.put("showMoreEvents", true);
		}else{
			allObj.put("showMoreEvents", false);
		}
		
		if(totalVenues > curCompletedVenuesRows){
			allObj.put("showMoreVenues", true);
		}else{
			allObj.put("showMoreVenues", false);
		}
		
		if(totalArtists > curCompletedArtistRows){
			allObj.put("showMoreArtists", true);
		}else{
			allObj.put("showMoreArtists", false);
		}
		
		return allObj;
	}
	*/
	
	
	
}
