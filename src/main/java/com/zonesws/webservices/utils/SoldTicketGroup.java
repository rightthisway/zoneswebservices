package com.zonesws.webservices.utils;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.zonesws.webservices.enums.Status;
@XStreamAlias("SoldTicketGroup")
public class SoldTicketGroup implements Serializable{
		private Integer ticketGroupId;
		private Integer holdTicketId;
		private String category;
		private Integer quantity;
		private double price;
		private double total;
		private String message;
		private Status status;
		
		@XStreamOmitField
		private String ticketItemId;
		

		public Integer getTicketGroupId() {
			return ticketGroupId;
		}

		public void setTicketGroupId(Integer ticketGroupId) {
			this.ticketGroupId = ticketGroupId;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public Integer getHoldTicketId() {
			return holdTicketId;
		}

		public void setHoldTicketId(Integer holdTicketId) {
			this.holdTicketId = holdTicketId;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public Integer getQuantity() {
			return quantity;
		}

		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			this.price = price;
		}

		public double getTotal() {
			return total;
		}

		public void setTotal(double total) {
			this.total = total;
		}

		public String getTicketItemId() {
			return ticketItemId;
		}

		public void setTicketItemId(String ticketItemId) {
			this.ticketItemId = ticketItemId;
		}
		
		
	}