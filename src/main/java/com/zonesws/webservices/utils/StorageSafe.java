package com.zonesws.webservices.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("StorageSafe")
public class StorageSafe{
	String Guid;
	
	public String getGuid() {
		return Guid;
	}
	public void setGuid(String guid) {
		Guid = guid;
	}
	
	
}