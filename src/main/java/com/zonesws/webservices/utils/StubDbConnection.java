package com.zonesws.webservices.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class StubDbConnection {
	
Connection stubConnection;

public static void main(String[] args) {
	
	StubDbConnection connections = new StubDbConnection();
	Connection connection = connections.getStubConnection(); 
	System.out.println(connection);
}
	
	
	public Connection getStubConnection(){
		if(stubConnection!=null){
			return stubConnection;
		}
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tn1.db.url");
		String username =resourceBundle.getString("tn1.db.user.name");
		String password =resourceBundle.getString("tn1.db.password");*/
		
		String url ="jdbc:jtds:sqlserver://192.168.0.52:1433/indux;sendStringParametersAsUnicode=false";
		String username ="tayo";
		String password ="po01ro02ro30";
		
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			stubConnection = DriverManager.getConnection(url, username, password);
			return stubConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
}
