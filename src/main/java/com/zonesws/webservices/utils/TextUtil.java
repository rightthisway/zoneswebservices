package com.zonesws.webservices.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.list.ReffererCodeResponse;

public class TextUtil {
	private static Pattern vsTeamPattern = Pattern.compile("(.*)\\s+[vV][sS]\\.?\\s+(.*)");
	public static Collection<String> wordsToRemoveForStripping;

	public static Collection<String> wordsToRemoveForKeywords;
	public static Collection<String> wordsToRemoveForVenue;
	
	public static String phoneRegEx = "[^0-9]";
	
	static {
		// only use the singular form
		wordsToRemoveForStripping = new ArrayList<String>();
		wordsToRemoveForStripping.add("stadium");
		wordsToRemoveForStripping.add("ticket");
		wordsToRemoveForStripping.add("event");
		wordsToRemoveForStripping.add("theater");
//		wordsToRemoveForStripping.add("center");
//		wordsToRemoveForStripping.add("centre");
		wordsToRemoveForStripping.add("state");
		wordsToRemoveForStripping.add("football");
		wordsToRemoveForStripping.add("basketball");
		wordsToRemoveForStripping.add("baseball");

		wordsToRemoveForKeywords = new ArrayList<String>();
		wordsToRemoveForKeywords.add("and");
		wordsToRemoveForKeywords.add("or");
		wordsToRemoveForKeywords.add("off");
		wordsToRemoveForKeywords.add("of");
		wordsToRemoveForKeywords.add("vs");
		wordsToRemoveForKeywords.add("the");
		wordsToRemoveForKeywords.add("a");
		wordsToRemoveForKeywords.addAll(wordsToRemoveForStripping);
		
		wordsToRemoveForVenue = new ArrayList<String>();
		wordsToRemoveForVenue.add("\\.");
		wordsToRemoveForVenue.add("st");
		wordsToRemoveForVenue.add("street");
		wordsToRemoveForVenue.add("ave");
		wordsToRemoveForVenue.add("avenue");
		wordsToRemoveForVenue.add("blvd");
		wordsToRemoveForVenue.add("at");
		wordsToRemoveForVenue.add("of");
	}
	
	
	public static String removeExtraWhitespaces(String str) {
		return str.replaceAll("\\s+", " ").trim();
	}

	public static String removeNonWordCharacters(String str) {
		return str.replaceAll("\\W+", "");
	}

	public static String removeAllWhitespaces(String str) {
		return str.replaceAll("\\s", "");
	}

	public static String trimQuotes(String str) {
		if (str == null) {
			return null;
		}
		
		str = str.trim();
		
		if (str.isEmpty()) {
			return str;
		}
		
		if (str.length() < 2) {
			return str;
		}
		
		char first = str.charAt(0);
		char last = str.charAt(str.length() - 1);
		
		if (first == last && (first == '\'' || last == '\"')) {
			str = str.substring(1, str.length() - 1);
			return str.trim();
		}
		
		return str;
	}
	
	public static String getSearchStringFromName(String eventName) {
		// if the event name is A vs. B, then remove vs. B
		String result = stripName(eventName);
		Matcher matcher = vsTeamPattern.matcher(result);
		if (matcher.find()) {
			return getKeywordsFromName(matcher.group(1));
		} else {
			return getFilteredKeywordsFromKeyword(result);
		}
	}

	public static boolean checkMenWomensCompatibility(String name1, String name2) {
		name1 = " " + name1.toLowerCase() + " ";
		boolean event1HasMen = name1.matches(".*[^o]men.*");
		boolean event1HasWomen = name1.matches(".*women.*");

		name2 = " " + name2.toLowerCase() + " ";
		boolean event2HasMen = name2.matches(".*[^o]men.*");
		boolean event2HasWomen = name2.matches(".*women.*");

		if ((event1HasMen && event1HasWomen && event2HasMen && event2HasWomen)
			|| (event1HasMen && !event1HasWomen && event2HasMen && !event2HasWomen)
			|| (!event1HasMen && event1HasWomen && !event2HasMen && event2HasWomen)
			|| (!event1HasMen && !event1HasWomen)
			|| (!event2HasMen && !event2HasWomen)) {
			return true;
		}
		return false;
	}
	
	public static String getEbaySearchStringFromName(String eventName) {
		return eventName;
	}

	/*
	 * remove punctuation
	 * remove whatever is between ()
	 */
	public static String removeCommentsAndPunctuations(String name) {
		// remove comments whatever is between braces
		String result = name.replaceAll("\\(.*\\)", "");
		result = result.replaceAll("\\[.*\\]", "");
		
		// remove punctuation
		result = result.replaceAll("[\\.,;\\:\\!\\?\\<\\>\\-_/]", " ");
		return result;
	}

	public static String scrubSportingEventName(String name) {
		name = name.replaceAll("[fF]ootball", "");
		name = name.replaceAll("[bB]asketball", "");
		name = name.replaceAll("[bB]aseball", "");
		name = name.replaceAll("[tT]ickets", "");
		name = name.replaceAll("[pP]re-season", "");
		name = name.replaceAll("season", "");

		name = TextUtil.removeCommentsAndPunctuations(name);
		name = name.replaceAll("- .*", "");
		name = name.replaceAll("\\s+", " ");
		return name;
	}

	public static String stripEbayName(String eventName) {
		eventName = eventName.replaceAll(" .*", "");

		eventName = eventName.toLowerCase();
		
		// remove years
		String result = eventName.replaceAll("20\\d{2}", "");
		result = removeCommentsAndPunctuations(result);
		
		for (String word: wordsToRemoveForStripping) {
			result = result.replaceAll(word + "(?:s)", " ");
		}
		
		result.replaceAll("\\s+", " ");
		
		return result.trim();
	}
	
	public static String stripName(String eventName) {
//		eventName = eventName.replaceAll("- .*", "");

		//eventName = eventName.toLowerCase();
		
		// remove years
		String result = eventName.replaceAll("20\\d{2}", "");
		// remove comments whatever is between braces
		result = eventName.replaceAll("\\(.*\\)", "");
		result = result.replaceAll("\\[.*\\]", "");
		
		// remove punctuation
		result = result.replaceAll("[,;\\:\\!\\?\\<\\>_/]", " ");
		
		for (String word: wordsToRemoveForStripping) {
			result = result.replaceAll(word + "(?:s)", " ");
		}
		
		result.replaceAll("\\s+", " ");
		
		return result.trim();
	}

	public static String getKeywordsFromName(String eventName) {
		eventName = eventName.toLowerCase();
		
		// remove years
		
		String result = stripName(eventName);
				
		eventName = eventName.replaceAll("-", " ");
		
		Collection<String> keywords = new ArrayList<String>();
		boolean game = false;
		
		for (String token: result.split(" ")) {
			if (token.toLowerCase().equals("game") || token.toLowerCase().equals("session")) {
				game = true;
			} else if (game && token.matches("^\\d+$")) {
				game = false;
			} else if (token.length() < 2) { // skip words too short. Set it to 1 because of u2
				continue;
			}

			if (wordsToRemoveForKeywords.contains(token)) {
				continue;
			}
		
			keywords.add(token);
		}
		
		String keywordString = "";
		for (String word: keywords) {
			keywordString += word + " ";
		}
		
		return keywordString.trim();		
	}

	
	public static String getFilteredKeywordsFromKeyword(String eventName) {
		eventName = eventName.toLowerCase();
		
		// remove years
		
		String result = stripName(eventName);
				
//		eventName = eventName.replaceAll("-", " ");
		
		Collection<String> keywords = new ArrayList<String>();
		boolean game = false;
		
		for (String token: result.split(" ")) {
			if (token.toLowerCase().equals("game") || token.toLowerCase().equals("session")) {
				game = true;
			} else if (game && token.matches("^\\d+$")) {
				game = false;
			} else if (token.length() < 2) { // skip words too short. Set it to 1 because of u2
				continue;
			}

			if (wordsToRemoveForKeywords.contains(token)) {
				continue;
			}
		
			keywords.add(token);
		}
		
		String keywordString = "";
		for (String word: keywords) {
			keywordString += word + " ";
		}
		
		return keywordString.trim();		
	}
	
	public static boolean isSimilar(String name1, String name2) {
		return isSimilar(name1, name2, 3);
	}

	// degree = number of similar words (after keyword computation)
	// if number of words of name1 or name2 is less then degree = min of them
	public static boolean isSimilar(String name1, String name2, int degree) {
		int numOccurences = computeSimilarity(name1, name2);
		
		if (numOccurences >= degree) {
			return true;
		}
		
		String strippedKeywords1 = getKeywordsFromName(name1.replaceAll(".*:", "")).replaceAll("\\w+ (?:home|away) game \\d+", "").replaceAll("session \\d+", "").replaceAll("zone", "");
		String strippedKeywords2 = getKeywordsFromName(name2.replaceAll(".*:", "")).replaceAll("\\w+ (?:home|away) game \\d+", "").replaceAll("session \\d+", "").replaceAll("zone", "");
		
		int min = Math.min(strippedKeywords1.split(" ").length, strippedKeywords2.split(" ").length);
		
		if (min - 1 <= 0) {
			if (numOccurences > 0) {
				return true;
			}
			return false;
		}
		
		if (numOccurences >= min - 1) {
			return true;
		}
		
		return false;
	}
	public static String getKeywordsFromVenue(String venue) {
		venue = venue.toLowerCase();
		String result = "";
		for(String token:venue.split(" ")){
			if(wordsToRemoveForVenue.contains(token)){
				continue;
			}
			result = result + token + " ";
		}
		return result.trim();
	}
	
	public static boolean isSimilarVenue(String name1, String name2, int degree){
		name1 = getKeywordsFromVenue(name1);
		name2 = getKeywordsFromVenue(name2);
		return isSimilar(name1, name2,degree);
	}
	public static int computeSimilarity(String name1, String name2) {
		// if there is game 1 in keywords1 but is not present in game 2 the similarity is 0
		// if we have something like conference: Team A vs Team B
		// make sure that conference is part of both keywords1 and keywords2

		int numOccurences = 0;

		// both have to contains YN or none
		if (name1.contains("YN ") != name2.contains("YN ")) {
			return 0;
		}

		if (name1.contains(":")) {
			String conference = name1.replaceAll(":.*", "").toLowerCase();
			if (!name2.toLowerCase().contains(conference)) {
				return 0;
			}
			
			numOccurences -= getKeywordsFromName(conference).split(" ").length;
		} else if (name2.contains(":")) {
			String conference = name2.replaceAll(":.*", "").toLowerCase();
			if (!name1.toLowerCase().contains(conference)) {
				return 0;
			}
			numOccurences -= getKeywordsFromName(conference).split(" ").length;
		}
		
		String keywords1 = getKeywordsFromName(name1);
		String keywords2 = getKeywordsFromName(name2);
		
		// if one contains zone, the other has to contain zone
		if (keywords1.contains("zone") != keywords2.contains("zone")) {
			return 0;
		}
		
		// if one contains the word "men(s)" and then other "women(s)" then
		// they are not compatible
		if (!checkMenWomensCompatibility(name1, name2)) {
			return 0;
		}
		
		keywords2 = " " + keywords2 + " ";
		boolean game = false;
		boolean session = false;
		
		for (String token: keywords1.split(" ")) {
			if (token.toLowerCase().equals("game")) {
				if (!keywords2.contains("game")) {
					return 0;
				}
				game = true;
			} else if (token.toLowerCase().equals("session")) {
				if (!keywords2.contains("session")) {
					return 0;
				}
				session = true;
			}
			else if ((session || game) && token.matches("^\\d+$")) {
				if (!keywords2.contains(" " + token + " ")) {
					return 0;
				}
			}
			
			if (keywords2.contains(" " + token + " ")) {
				numOccurences++;
			}
		}
		
		if (game) {
			numOccurences -= 3; // remove home game X
		}

		if (session) {
			numOccurences -= 2; // remove session X
		}

		return numOccurences;
	}
	
	public static boolean isKeywordsMatch(String keyword1,String keyword2){
		keyword1=keyword1.replaceAll("-", " ").replaceAll(":", " ").replaceAll("&", " ");
		keyword2=keyword2.replaceAll("-", " ").replaceAll(":", " ").replaceAll("&", " ");
		if(keyword1.equalsIgnoreCase(keyword2) 
				|| keyword1.toLowerCase().contains(keyword2.toLowerCase()) 
				|| keyword2.toLowerCase().contains(keyword1.toLowerCase())){
			return true;
		}
		List<String> keyword1List = new ArrayList<String>();
		List<String> keyword2List = new ArrayList<String>();
		for(String keyword:keyword1.split("\\s+")){
			if(wordsToRemoveForKeywords.contains(keyword)){
				continue;
			}
			keyword1List.add(keyword);
		}
		
		for(String keyword:keyword2.split("\\s+")){
			if(wordsToRemoveForKeywords.contains(keyword)){
				continue;
			}
			keyword2List.add(keyword);
		}
		int count =0;
		for(String keyword:keyword1List){
			if(keyword2List.contains(keyword)){
				count++;
			}
		}
		if((keyword1List.size()>1 && (keyword2List.size()>1)) && (count>=keyword1List.size()/2 || count>=keyword2List.size()/2)){
			return true;
		}else if(count==1){
			return true;
		}
		return false;
	}
	/*public static void main(String[] args) {
		System.out.println(getKeywordsFromName("2010 Winter Games: Alpine Skiing - Womens combined (Asdfsd)"));
		System.out.println(isSimilar("US Open Session 5 Zone", "US Open Session 5 Zonne"));
		System.out.println(isSimilar("liberty bowl memorial", "liberty bowl memphis"));
		System.out.println(isSimilar("Ford Center, Oklahoma City, OK", "Ford Center-OK Oklahoma City, OK"));
		System.out.println("temp:" + isKeywordsMatch("book-of-mormon","book of mormon"));
		System.out.println("temp:" + isKeywordsMatch("book-of-mormon","book&of&mormon"));
	}*/
	
	public static boolean isEmptyOrNull(String value){
		
		if(null == value || value.isEmpty()){
			return true;
		}
		return false;
	}
	
	public static String getCustomerPromoAlertText(){
		return "You are successfully registered and your promotional code has been sent to your registered email address";
	}
	
	public static ReffererCodeResponse computeDesktopMessages(ReffererCodeResponse reffererCodeResponse,String referralCode, String gameName){

		String message = "",linkedInMsg="",androidMessge="";
		
		message = "Use referral code "+referralCode+" for a 5% discount to purchase any event tickets on Reward The Fan. " +
					"Register today and you can also get a code to refer friends and get a chance to enter the Hamilton Lottery.";
		linkedInMsg = message;
		androidMessge = message;
		reffererCodeResponse.setShareRTFUrl("https://www.rewardthefan.com/Home?c="+referralCode+"&s=DESKTOP_SITE"+"&v=NO&contest="+gameName);
		
		reffererCodeResponse.setEmailMsgDesk(message);
		reffererCodeResponse.setFbMsgDesk(message);
		reffererCodeResponse.setGmailMsgDesk(message);
		reffererCodeResponse.setInstagramMsgDesk(message);
		reffererCodeResponse.setLinkedInMsgDesk(linkedInMsg);
		reffererCodeResponse.setAndroidMsgDesk(androidMessge);
		reffererCodeResponse.setiPhoneMsgDesk(message);
		reffererCodeResponse.setPinterestMsgDesk(message);
		reffererCodeResponse.setTwitterMsgDesk(message);
		reffererCodeResponse.setWatsappMsgDesk(message);
		return reffererCodeResponse;
	}
	
	public static ReffererCodeResponse computeMobileAppMessages(ReffererCodeResponse reffererCodeResponse,String referralCode,
			ApplicationPlatForm platForm, String gameName){

		String message = "",linkedInMsg="",androidMessge="";
		message ="Use referral code "+referralCode+" for a 5% discount to purchase any event tickets on Reward The Fan. " +
					"Register today and you can also get a code to refer friends and get a chance to enter the Hamilton Lottery.";
		linkedInMsg = message;
		androidMessge = message;
		reffererCodeResponse.setShareRTFUrl("https://www.rewardthefan.com/Home?c="+referralCode+"&s="+String.valueOf(platForm)+"&v=NO&contest="+gameName);
		reffererCodeResponse.setNormalMsg(androidMessge);
		reffererCodeResponse.setFbMsg(message);
		reffererCodeResponse.setGmailMsg(message);
		reffererCodeResponse.setInstagramMsg(message);
		reffererCodeResponse.setLinkedInMsg(linkedInMsg);
		reffererCodeResponse.setPinterestMsg(message);
		reffererCodeResponse.setTwitterMsg(message);
		return reffererCodeResponse;
	}
	
	
	public static ReffererCodeResponse computeDesktopMessagesViewOrder(ReffererCodeResponse reffererCodeResponse,String referralCode, String gameName){

		String message = "",linkedInMsg="",androidMessge="";
		
		message = "Use referral code "+referralCode+" for a 5% discount to purchase any event tickets on Reward The Fan. " +
					"Register today and you can also get a code to refer friends and get a chance to enter the Hamilton Lottery.";
		linkedInMsg = message;
		androidMessge = message;
		reffererCodeResponse.setShareRTFUrl("https://www.rewardthefan.com/Home?c="+referralCode+"&s=DESKTOP_SITE"+"&v=ON&contest="+gameName);
		reffererCodeResponse.setEmailMsgDesk(message);
		reffererCodeResponse.setFbMsgDesk(message);
		reffererCodeResponse.setGmailMsgDesk(message);
		reffererCodeResponse.setInstagramMsgDesk(message);
		reffererCodeResponse.setLinkedInMsgDesk(linkedInMsg);
		reffererCodeResponse.setAndroidMsgDesk(androidMessge);
		reffererCodeResponse.setiPhoneMsgDesk(message);
		reffererCodeResponse.setPinterestMsgDesk(message);
		reffererCodeResponse.setTwitterMsgDesk(message);
		reffererCodeResponse.setWatsappMsgDesk(message);
		return reffererCodeResponse;
	}
	
	
	public static ReffererCodeResponse computeMobileAppMessagesViewOrder(ReffererCodeResponse reffererCodeResponse,String referralCode,
			ApplicationPlatForm platForm, String gameName){

		String message = "",linkedInMsg="",androidMessge="";
		message = "Use referral code "+referralCode+" for a 5% discount to purchase any event tickets on Reward The Fan. " +
					"Register today and you can also get a code to refer friends and get a chance to enter the Hamilton Lottery.";
		linkedInMsg = message;
		androidMessge = message;
		reffererCodeResponse.setShareRTFUrl("https://www.rewardthefan.com/Home?c="+referralCode+"&s="+String.valueOf(platForm)+"&v=ON&contest="+gameName);
		reffererCodeResponse.setNormalMsg(androidMessge);
		reffererCodeResponse.setFbMsg(message);
		reffererCodeResponse.setGmailMsg(message);
		reffererCodeResponse.setInstagramMsg(message);
		reffererCodeResponse.setLinkedInMsg(linkedInMsg);
		reffererCodeResponse.setPinterestMsg(message);
		reffererCodeResponse.setTwitterMsg(message);
		return reffererCodeResponse;
	}
	
	public static String getSportsLoyalFanEmailBody(String teamName){
		String message = "Congratulations! - you have declared yourself a LOYAL FAN of <b>"+teamName+"</b>. " +
		"<br> <br>Every time you purchase tickets for <b>"+teamName+"</b>, whether at home or away, " +
		"you will receive a <b>10%</b> discount. <br> <br> Your LOYAL FAN status does not expire." +
		"<br> <br> Enjoy! ";
		return message;
	}
	
	public static String getNonSportsLoyalFanEmailBody(String loyalFanName, String parentCategory, String country){
		String message = "Congratulations! - you have declared yourself a LOYAL FAN of <b>"+loyalFanName+"</b> STATE <b>"+parentCategory+"</b>. " +
		"<br> <br>Every time you purchase tickets for All <b>"+parentCategory+"</b> in <b>"+loyalFanName+","+country+"</b>, " +
		"you will receive a <b>10%</b> discount. <br> <br> Your LOYAL FAN status does not expire." +
		"<br> <br> Enjoy! ";
		return message;
	}
	
	public static String getCustomerShareLinkValidationMessage(String fontSizeStr){
		return "<font "+fontSizeStr+" color=#000000>This Perk is available to registered customers who have made a purchase from us</font>";
	}
	
	/*public static void main(String[] args) {
		System.out.println(getContestTicketsConfirmDialogdf("size='6'"));;
	}*/
	
	public static String getContestTicketsConfirmDialog(String fontSizeStr){
		return "<font "+fontSizeStr+" color=#000000>By accepting these Grand Prize Tickets, you agree to our</font> <a href=\"https://www.rewardthefan.com/PrivacyPolicy\"><font "+fontSizeStr+" color=#000000>Privacy Policy,</font></a> <a href=\"https://www.rewardthefan.com/Terms\"><font "+fontSizeStr+" color=#000000>Terms of Use</font></a><font "+fontSizeStr+" color=#000000>, and </font><a href=\"https://www.rewardthefan.com/Rules\"><font "+fontSizeStr+" color=#000000>Rules of the Game.</font></a>";
	}
	
	
	public static boolean between(int i, int minValueInclusive, int maxValueInclusive) {
	    if (i >= minValueInclusive && i <= maxValueInclusive)
	        return true;
	    else
	        return false;
	}
	
	public static boolean greaterThanMax(int i, int maxValueInclusive) {
	    if (i > maxValueInclusive)
	        return true;
	    else
	        return false;
	}
	
	public static void main(String[] args) {
		
		int i =150, minValueInclusive = 150, maxValueInclusive = 250;
		
		System.out.println(between(i, minValueInclusive, maxValueInclusive));
		System.out.println(greaterThanMax(i,maxValueInclusive));
	}
	
}
