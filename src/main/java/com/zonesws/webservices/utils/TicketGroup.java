package com.zonesws.webservices.utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.Status;
@XStreamAlias("TicketGroup")
public class TicketGroup implements Serializable{
		private Integer id;
		private Integer eventId;
		private String zone;
		private String colorCode;
		private String rgbColor;
		private Integer quantity;
		private Double unitPrice;
		private Double total;
		private String ticketDeliveryType;
		private String message;
		private Integer priority;
		private Status status;
		private String ticketDescription;
		private String zoneDescription;
		private String zoneStarInfo;
		private String ticketPriceInfo;
		private String discountZoneTicketPriceInfo;
		private String loyaltyInfo;
		private String ticketDeliveryInfo;
		private String discountZoneTicketDeliveryInfo;
		private String discountZoneLoyaltyInfo;
		
		private String normalZone;
		private String discountZone;

		public Integer getId() {
			return id;
		}
		
		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getEventId() {
			if(null == eventId ){
				eventId = 0;
			}
			return eventId;
		}
		
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		
		public String getZone() {
			if(null ==zone || zone.isEmpty()){
				zone="";
			}
			return zone;
		}

		public void setZone(String zone) {
			this.zone = zone;
		}

		public Integer getQuantity() {
			if(null == quantity ){
				quantity = 0;
			}
			return quantity;
		}
		
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		/*public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			DecimalFormat df = new DecimalFormat("#.##");
			this.price = Double.valueOf((df.format(price)));
		}*/
		
		

		public Double getTotal() {
			return total;
		}

		public Double getUnitPrice() {
			DecimalFormat df = new DecimalFormat("#");
			return Double.valueOf((df.format(unitPrice)));
		}

		public void setUnitPrice(Double unitPrice) {
			DecimalFormat df = new DecimalFormat("#.##");
			this.unitPrice = Double.valueOf((df.format(unitPrice)));
		}

		public void setTotal(Double total) {
			this.total = total;
		}
		
		public static void setTotalPriceForTicketGroups(List<TicketGroup> ticketGroups){
			DecimalFormat df = new DecimalFormat("#.##");
			for(TicketGroup group:ticketGroups){
				group.setTotal(Double.valueOf((df.format(group.getUnitPrice()*group.getQuantity()))));
			}
		}

		public String getMessage() {
			if(null ==message || message.isEmpty()){
				message="";
			}
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getZoneDescription() {
			if(null ==zoneDescription || zoneDescription.isEmpty()){
				zoneDescription="";
			}
			return zoneDescription;
		}

		public void setZoneDescription(String zoneDescription) {
			this.zoneDescription = zoneDescription;
		}

		public String getRgbColor() {
			if(null ==rgbColor || rgbColor.isEmpty()){
				rgbColor="";
			}
			return rgbColor;
		}

		public void setRgbColor(String rgbColor) {
			this.rgbColor = rgbColor;
		}

		public String getColorCode() {
			if(null ==colorCode || colorCode.isEmpty()){
				colorCode="";
			}
			return colorCode;
		}

		public void setColorCode(String colorCode) {
			this.colorCode = colorCode;
		}

		public Integer getPriority() {
			if(null ==priority ){
				priority=0;
			}
			return priority;
		}

		public void setPriority(Integer priority) {
			this.priority = priority;
		}

		public String getTicketDeliveryType() {
			if(null ==ticketDeliveryType || ticketDeliveryType.isEmpty()){
				ticketDeliveryType="";
			}
			return ticketDeliveryType;
		}

		public void setTicketDeliveryType(String ticketDeliveryType) {
			this.ticketDeliveryType = ticketDeliveryType;
		}

		public String getZoneStarInfo() {
			if(null ==zoneStarInfo ||zoneStarInfo.isEmpty()){
				zoneStarInfo="";
			}
			return zoneStarInfo;
		}

		public void setZoneStarInfo(String zoneStarInfo) {
			this.zoneStarInfo = zoneStarInfo;
		}

		public String getTicketPriceInfo() {
			if(null ==ticketPriceInfo ||ticketPriceInfo.isEmpty()){
				ticketPriceInfo="";
			}
			return ticketPriceInfo;
		}

		public void setTicketPriceInfo(String ticketPriceInfo) {
			this.ticketPriceInfo = ticketPriceInfo;
		}

		public String getLoyaltyInfo() {
			if(null ==loyaltyInfo ||loyaltyInfo.isEmpty()){
				loyaltyInfo="";
			}
			return loyaltyInfo;
		}

		public void setLoyaltyInfo(String loyaltyInfo) {
			this.loyaltyInfo = loyaltyInfo;
		}

		public String getTicketDeliveryInfo() {
			if(null ==ticketDeliveryInfo ||ticketDeliveryInfo.isEmpty()){
				ticketDeliveryInfo="";
			}
			return ticketDeliveryInfo;
		}

		public void setTicketDeliveryInfo(String ticketDeliveryInfo) {
			this.ticketDeliveryInfo = ticketDeliveryInfo;
		}

		public String getDiscountZoneTicketDeliveryInfo() {
			return discountZoneTicketDeliveryInfo;
		}

		public void setDiscountZoneTicketDeliveryInfo(
				String discountZoneTicketDeliveryInfo) {
			this.discountZoneTicketDeliveryInfo = discountZoneTicketDeliveryInfo;
		}

		public String getTicketDescription() {
			return ticketDescription;
		}

		public void setTicketDescription(String ticketDescription) {
			this.ticketDescription = ticketDescription;
		}

		public String getDiscountZoneLoyaltyInfo() {
			return discountZoneLoyaltyInfo;
		}

		public void setDiscountZoneLoyaltyInfo(String discountZoneLoyaltyInfo) {
			this.discountZoneLoyaltyInfo = discountZoneLoyaltyInfo;
		}

		public String getDiscountZoneTicketPriceInfo() {
			return discountZoneTicketPriceInfo;
		}

		public void setDiscountZoneTicketPriceInfo(String discountZoneTicketPriceInfo) {
			this.discountZoneTicketPriceInfo = discountZoneTicketPriceInfo;
		}

		public String getNormalZone() {
			return normalZone;
		}

		public void setNormalZone(String normalZone) {
			this.normalZone = normalZone;
		}

		public String getDiscountZone() {
			return discountZone;
		}

		public void setDiscountZone(String discountZone) {
			this.discountZone = discountZone;
		}
		
		
		
	}