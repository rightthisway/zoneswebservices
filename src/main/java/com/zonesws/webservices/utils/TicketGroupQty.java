package com.zonesws.webservices.utils;

import java.io.Serializable;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CategoryTicketGroup;
@XStreamAlias("TicketGroupsByQty")
public class TicketGroupQty implements Serializable{
		private Integer qty;
		private String qtyAlterMsg;
		private List<CategoryTicketGroup> categoryTicketGroups;
		
		/*private List<TicketGroup> ticketGroups;
		private List<AutoCatsTicketGroup> autoCatsTicketGroups;*/
		
		public Integer getQty() {
			return qty;
		}
		public void setQty(Integer qty) {
			this.qty = qty;
		}
		
		public List<CategoryTicketGroup> getCategoryTicketGroups() {
			return categoryTicketGroups;
		}
		public void setCategoryTicketGroups(
				List<CategoryTicketGroup> categoryTicketGroups) {
			this.categoryTicketGroups = categoryTicketGroups;
		}
		public String getQtyAlterMsg() {
			if(qty == 1){
				qtyAlterMsg="Ticket purchased are seated";
			}else{
				qtyAlterMsg="Tickets purchased are seated together";
			}
			return qtyAlterMsg;
		}
		public void setQtyAlterMsg(String qtyAlterMsg) {
			this.qtyAlterMsg = qtyAlterMsg;
		}
		
		
		
		
	}