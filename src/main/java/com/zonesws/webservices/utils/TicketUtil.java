package com.zonesws.webservices.utils;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Broker;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.CrownJewelCategoryTicket;
import com.zonesws.webservices.data.CrownJewelLeagues;
import com.zonesws.webservices.data.CrownJewelTeams;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.InvoiceTicketAttachment;
import com.zonesws.webservices.data.PresaleZoneTicketGroup;
import com.zonesws.webservices.data.ZoneTicketGroup;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ZonesQuantityTypes;
import com.zonesws.webservices.utils.list.CrownJewelTicket;
import com.zonesws.webservices.utils.list.CrownJewelTicketMap;


public class TicketUtil {
	private static DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	private static DateFormat justDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	//private static DecimalFormat decimalFormat = new DecimalFormat("#0.00");
	private static List<String> generalAdmissionList = new ArrayList<String>();;
	private static String textStyle = "justify";
	private static DecimalFormat df2 = new DecimalFormat(".##");
	private static DecimalFormat normalRoundOff = new DecimalFormat(".##");
	public static DecimalFormat percentageDF = new DecimalFormat("#.##");
	
	static{
		if(generalAdmissionList.size() <=0 ){
			generalAdmissionList.add("GA");
			generalAdmissionList.add("General Admission");
			generalAdmissionList.add("Gen Ad");
			generalAdmissionList.add("General Ad");
			generalAdmissionList.add("General Adm");
			generalAdmissionList.add("GenAd");
		}
	}
	
	
	 
	 public static String getTicketGroupSectionDescription(Integer quantity,String section,String row,String zone){
	    	
		 //ZONE ? tickets will be seated together between sections ? and ? or ? and ? and between rows ? and ?
		 String description = "<font color=#012e4f>"+zone+" tickets will be <b>seated";
		 if(quantity == 1){
			 description = description+"</b> in section ";
		 }else{
			 if(section!=null && section.contains("OR") && section.contains("-")){
				 description = description+" together</b> between sections ";
			 }else{
				 description = description+" together</b> in section ";
			 }
		 }
		 description = description+ section+"";
		 if(null != row && !row.isEmpty()){
			 if(row.contains("-")){
				 description = description+" and between rows "+row; 
			 }else{
				 description = description+" and row "+row; 
			 }
		 }
		 
		 description = description + "</font>";
		 
	     return description;
	   }
	 
	 
	/* public static String getTicketGroupSectionDescription(Integer quantity,String section,String row,String zone){
	    	
		 //ZONE ? tickets will be seated together between sections ? and ? or ? and ? and between rows ? and ?
		 String description = zone+" tickets will be seated";
		 if(quantity == 1){
			 description = description+" in section ";
		 }else{
			 if(section.contains("OR")){
				 description = description+" together between sections ";
			 }else{
				 description = description+" in section ";
			 }
		 }
		 description = description+ section+"";
		 if(null != row && !row.isEmpty()){
			 if(row.contains("-")){
				 description = description+" and between rows "+row; 
			 }else{
				 description = description+" and row "+row; 
			 }
		 }
		 
		 //description = description + "</font>";
		 
	     return description;
	   }*/
	 
	 public static String formatOrderSectionDescription(String sectionDescription){
		 
		 if(sectionDescription.contains("seated together")){
			 
		 }
		 
		 
		 return sectionDescription;
	 }
	
	public static CategoryTicketGroup getSuperFanPrice(CategoryTicketGroup ticketGroup) throws Exception {
		Double price = ticketGroup.getOriginalPrice();
		price = price - ( (price * PaginationUtil.loyalFanPercentage)/100);
		ticketGroup.setPrice(getRoundedValueString(price));
		return ticketGroup;
	}
	
	public static Double getRoundedValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.DOWN);
		return Double.valueOf(df2.format(value));
	}
	
	public static Double getRoundedUpValue(Double value) throws Exception {
		df2.setRoundingMode(RoundingMode.UP);
		return Double.valueOf(df2.format(value));
	}
	
	public static Double getNormalRoundedValue(Double value) throws Exception {
		return Double.valueOf(df2.format(value));
	}
	
	
	public static String getRoundedValueString(Double value) throws Exception {
		return String.format( "%.2f", value);
	}
	
	public static Integer getRoundedValueIntger(Double value) throws Exception {
		value = Math.floor(value);
		return value.intValue();
	}
	
	public static void mainOLd(String[] args) throws Exception {

		Double input = 32.6728945;
		
		System.out.println("double : " + Math.ceil(input));
		
		System.out.println(getRoundedValueIntger(input));
		
		
		/*System.out.println("double (default) : " + df2.format(input));

		df2.setRoundingMode(RoundingMode.UP);
		System.out.println("double (UP) : " + df2.format(input));

		df2.setRoundingMode(RoundingMode.DOWN);
		System.out.println("double (DOWN) : " + df2.format(input));*/

		
	}
	
	public static String getLoyalFanPrice(Double price) throws Exception {
		String loyalFanPrice = "0.00";
		try{
			price = price - ( (price * PaginationUtil.loyalFanPercentage)/100);
			loyalFanPrice = getRoundedValueString(price);
		}catch(Exception e){
			e.printStackTrace();
			loyalFanPrice = getRoundedValueString(price);
		}
		return loyalFanPrice;
	}
	
	public static Double getLoyalFanPriceAsDouble(Double price) throws Exception {
		Double loyalFanPrice = 0.00;
		price = price - ( (price * PaginationUtil.loyalFanPercentage)/100);
		loyalFanPrice = getRoundedValue(price);
		return loyalFanPrice;
	}
	
	public static Double getDiscountedPrice(Double price,Double discountConv)  throws Exception{
		Double discAmount = 0.00;
		discAmount = price * discountConv;
		return getRoundedValue(discAmount);
	}
	
	public static Double getSingleTicketFlatDiscount(Integer quantity,Double discountt)  throws Exception{
		Double discAmount = 0.00;
		discAmount = discountt / quantity;
		return getRoundedValue(discAmount);
	}
	
	public static String getTimeLeft(Date lockedDate,Date currentDate){
		String timeLeft = "";
		try{
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date lockdate = formatter.parse(formatter.format(lockedDate));
			Date curDate = formatter.parse(formatter.format(currentDate));
			
			long difference = curDate.getTime() - lockdate.getTime();
			
			long secs = difference / (1000);
			System.out.println(secs);
			
			if( secs < 10*60 && secs > 0){
				secs = 10*60 - secs;
				long minutes = secs/60;
				secs = secs%60;
				if(secs<10) {
					timeLeft = minutes+":0"+secs;	
				} else {
					timeLeft = minutes+":"+secs;
				}
				
			}else{
				timeLeft = "0:00";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return timeLeft;
	}
	
	/*df2.setRoundingMode(RoundingMode.DOWN);
	System.out.println("double (DOWN) : " + );*/
	
	
	
	
	
	public static Boolean isGeneralAdmissionZone(String sectionRange) throws Exception {
		
		for (String section : generalAdmissionList) {
			if(sectionRange.toUpperCase().contains(section.toUpperCase())){
				return true;
			}
		}
		return false;
	}
	
	
	public static TicketGroup getTicketGroupInfo(TicketGroup ticketGroup,DiscountDetails discountDetails,String speedZoneArrivalDate,
			String discountZoneArrivalDate,String instantSpeedZoneArrivalDate,String instantDiscountZoneArrivalDate) throws Exception {
		
		String ticketDeliveryType = "",loyaltyRewardStr="0.00",unitPrice="0.00";
		
		
		if(ticketGroup.getTicketDeliveryType().equals("INSTANT")){
			speedZoneArrivalDate = instantSpeedZoneArrivalDate;
			discountZoneArrivalDate = instantDiscountZoneArrivalDate;
			ticketDeliveryType = "sent by Email";
			ticketGroup.setTicketDeliveryType("Email");
		}else if(ticketGroup.getTicketDeliveryType().equals("EDELIVERY") || ticketGroup.getTicketDeliveryType().equals("ETICKETS") 
				|| ticketGroup.getTicketDeliveryType().equals("E-Ticket")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setTicketDeliveryType("Email");
		}else{
			ticketDeliveryType = "delivered via "+ticketGroup.getTicketDeliveryType();
			ticketGroup.setTicketDeliveryType("FedEx");
		}
		unitPrice =TicketUtil.getRoundedValueString(ticketGroup.getUnitPrice());
		Double loyaltyReward = Math.ceil(ticketGroup.getTotal() * discountDetails.getLoyaltyPerc() / 100);
		//loyaltyRewardStr = TicketUtil.getRoundedValueString(Math.ceil(loyaltyReward));
		//ticketGroup.setZoneStarInfo("Both primary ticket sellers and secondary ticket resellers discount certain zones in the days leading up to the event. Your tickets will be delivered by the day before the event, and we are passing along that discount to you.");
		ticketGroup.setZoneStarInfo("This zone is also being offered by the team, artist, venue or reseller at a discounted price if you are willing to accept delivery on the DAY BEFORE THE EVENT");
		
		ticketGroup.setLoyaltyInfo("You will earn $"+loyaltyReward.intValue()+" in rewards towards next purchase");
		ticketGroup.setTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+speedZoneArrivalDate);
		ticketGroup.setTicketPriceInfo(ticketGroup.getQuantity()+" Ticket(s) @ $"+ticketGroup.getUnitPrice().intValue()+" per Ticket");
		
		Long discountAmt = Math.round(ticketGroup.getTotal() * discountDetails.getZoneDiscountPerc() / 100);
		Double disZoneLoyaltyReward = Math.ceil((ticketGroup.getTotal() - discountAmt) * discountDetails.getLoyaltyPerc() / 100);
		//String discZoneLoyaltyRewardStr = TicketUtil.getRoundedValueString(Math.ceil(disZoneLoyaltyReward));
		ticketGroup.setDiscountZoneLoyaltyInfo("You will earn $"+disZoneLoyaltyReward.intValue()+" in rewards towards next purchase");
		ticketGroup.setDiscountZoneTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+discountZoneArrivalDate+".");
		
		Double unitDiscountAmt = ((ticketGroup.getUnitPrice()*discountDetails.getZoneDiscountPerc())/100); 
		Double discUnitPrice = ticketGroup.getUnitPrice() - Math.round(unitDiscountAmt);
		ticketGroup.setDiscountZoneTicketPriceInfo(ticketGroup.getQuantity()+" Ticket(s) @ $"+discUnitPrice.intValue()+" per Ticket");
		
		ticketGroup.setDiscountZone("Zone "+ticketGroup.getZone()+" Discount");
		ticketGroup.setNormalZone("Zone "+ticketGroup.getZone());
		
		return ticketGroup;
	}
	
	
	public static ZoneTicketGroup getTicketGroupInfo(ZoneTicketGroup ticketGroup,String arrivalDate,Double loyaltyReward) throws Exception {
		
		String ticketDeliveryType = "";
		
		if(ticketGroup.getShippingMethod().equals("INSTANT")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setShippingMethod("Email");
		}else if(ticketGroup.getShippingMethod().equals("EDELIVERY") || ticketGroup.getShippingMethod().equals("ETICKETS") 
				|| ticketGroup.getShippingMethod().equals("E-Ticket")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setShippingMethod("Email");
		}else{
			ticketDeliveryType = "delivered via "+ticketGroup.getShippingMethod();
			ticketGroup.setShippingMethod("FedEx");
		}
		ticketGroup.setLoyaltyInfo("You will earn $"+loyaltyReward.intValue()+" in rewards towards next purchase");
		ticketGroup.setTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+arrivalDate);
		
		String sectionRange = ticketGroup.getSectionRange();
		String rowRange = ticketGroup.getRowRange();	
		
		sectionRange = sectionRange.replaceAll("-", " thru ");
		rowRange = rowRange.replaceAll("-", " thru ");
		ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+sectionRange+" between row(s) "+rowRange+".");
		boolean isGAZone = TicketUtil.isGeneralAdmissionZone(sectionRange);
		if(isGAZone){
			ticketGroup.setZoneDescription("Tickets will be in section(s) "+sectionRange+" between row(s) "+rowRange+".");
		}
		return ticketGroup;
	}
	
 public static PresaleZoneTicketGroup getPresaleTicketGroupInfo(PresaleZoneTicketGroup ticketGroup,String arrivalDate,Double loyaltyReward) throws Exception {
		
		String ticketDeliveryType = "";
		
		if(ticketGroup.getShippingMethod().equals("INSTANT")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setShippingMethod("Email");
		}else if(ticketGroup.getShippingMethod().equals("EDELIVERY") || ticketGroup.getShippingMethod().equals("ETICKETS") 
				|| ticketGroup.getShippingMethod().equals("E-Ticket")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setShippingMethod("Email");
		}else{
			ticketDeliveryType = "delivered via "+ticketGroup.getShippingMethod();
			ticketGroup.setShippingMethod("FedEx");
		}
		ticketGroup.setLoyaltyInfo("You will earn $"+loyaltyReward.intValue()+" in rewards towards next purchase");
		ticketGroup.setTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+arrivalDate);
		
		String sectionRange = ticketGroup.getSectionRange();
		String rowRange = ticketGroup.getRowRange();	
		
		sectionRange = sectionRange.replaceAll("-", " thru ");
		rowRange = rowRange.replaceAll("-", " thru ");
		ticketGroup.setZoneDescription("Tickets will be seated together in section(s) "+sectionRange+" between row(s) "+rowRange+".");
		boolean isGAZone = TicketUtil.isGeneralAdmissionZone(sectionRange);
		if(isGAZone){
			ticketGroup.setZoneDescription("Tickets will be in section(s) "+sectionRange+" between row(s) "+rowRange+".");
		}
		return ticketGroup;
	}
	
	
	public static AutoCatsTicketGroup getAutoCatsTicketGroupInfo(AutoCatsTicketGroup ticketGroup) throws Exception {
		
		String ticketDeliveryType = "",loyaltyRewardStr="0.00",unitPrice="0.00";
		
		if(ticketGroup.getTicketDeliveryType().equals("INSTANT")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setTicketDeliveryType("Email");
		}else if(ticketGroup.getTicketDeliveryType().equals("EDELIVERY") || ticketGroup.getTicketDeliveryType().equals("ETICKETS") 
				|| ticketGroup.getTicketDeliveryType().equals("E-Ticket")){
			ticketDeliveryType = "sent by Email";
			ticketGroup.setTicketDeliveryType("Email");
		}else{
			ticketDeliveryType = "delivered via "+ticketGroup.getTicketDeliveryType();
			ticketGroup.setTicketDeliveryType("FedEx");
		}
		unitPrice =TicketUtil.getRoundedValueString(ticketGroup.getUnitPrice());
		/*Double loyaltyReward = Math.ceil(ticketGroup.getTotal() * discountDetails.getLoyaltyPerc() / 100);
		//ticketGroup.setZoneStarInfo("This zone is also being offered by the team, artist, venue or reseller at a discounted price if you are willing to accept delivery on the DAY BEFORE THE EVENT");
		
		ticketGroup.setLoyaltyInfo("You will earn $"+loyaltyReward.intValue()+" in rewards towards next purchase");*/
		ticketGroup.setTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+ticketGroup.getTicketDeliveryDate());
		ticketGroup.setTicketPriceInfo(ticketGroup.getQuantity()+" Ticket(s) @ $"+ticketGroup.getUnitPrice().intValue()+" per Ticket");
		
		//Long discountAmt = Math.round(ticketGroup.getTotal() * discountDetails.getZoneDiscountPerc() / 100);
		/*Double disZoneLoyaltyReward = Math.ceil((ticketGroup.getTotal() - discountAmt) * discountDetails.getLoyaltyPerc() / 100);
		//String discZoneLoyaltyRewardStr = TicketUtil.getRoundedValueString(Math.ceil(disZoneLoyaltyReward));
		ticketGroup.setDiscountZoneLoyaltyInfo("You will earn $"+disZoneLoyaltyReward.intValue()+" in rewards towards next purchase");*/
		ticketGroup.setDiscountZoneTicketDeliveryInfo("Your tickets will be "+ticketDeliveryType+" and you will recieve them no later than "+ticketGroup.getTicketDeliveryDate()+".");
		
		/*Double unitDiscountAmt = ((ticketGroup.getUnitPrice()*discountDetails.getZoneDiscountPerc())/100); 
		Double discUnitPrice = ticketGroup.getUnitPrice() - Math.round(unitDiscountAmt);
		ticketGroup.setDiscountZoneTicketPriceInfo(ticketGroup.getQuantity()+" Ticket(s) @ $"+discUnitPrice.intValue()+" per Ticket");*/
		
		ticketGroup.setDiscountZone("Zone "+ticketGroup.getZone()+" Discount");
		ticketGroup.setNormalZone("Zone "+ticketGroup.getZone());
		
		return ticketGroup;
	}
	
	
	public static Comparator<CategoryTicketGroup> ticketGroupSortingComparatorByPrice = new Comparator<CategoryTicketGroup>() {
		
		public int compare(CategoryTicketGroup ticket1, CategoryTicketGroup ticket2) {
			int cmp1 = ticket1.getOriginalPrice().compareTo(
					ticket2.getOriginalPrice());
			if (cmp1 < 0) {
				return -1;
			}
			if (cmp1 > 0) {
				return 1;
			}
			
			return ticket1.getId().compareTo(ticket2.getId());
	    }};
	    
		
	public static Comparator<CategoryTicketGroup> ticketSortingByPriceByPriority = new Comparator<CategoryTicketGroup>() {
		
		public int compare(CategoryTicketGroup ticket1, CategoryTicketGroup ticket2) {
			int cmp1 = ticket1.getOriginalPrice().compareTo(
					ticket2.getOriginalPrice());
			if (cmp1 < 0) {
				return -1;
			}
			if (cmp1 > 0) {
				return 1;
			}
			
			return ticket1.getBrokerId().compareTo(ticket2.getBrokerId());
	    }};
	
	    
	public static Comparator<CategoryTicketGroup> ticketGroupSortingComparatorBySection = new Comparator<CategoryTicketGroup>() {
	
		public int compare(CategoryTicketGroup ticket1, CategoryTicketGroup ticket2) {
			int cmp = ticket1.getActualSection().compareTo(
					ticket2.getActualSection());
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			int cmp1 = ticket1.getOriginalPrice().compareTo(
					ticket2.getOriginalPrice());
			if (cmp1 < 0) {
				return -1;
			}
			if (cmp1 > 0) {
				return 1;
			}
			
			return ticket1.getId().compareTo(ticket2.getId());
	    }};
	
	public static Comparator<CategoryTicketGroup> ticketGroupSortingComparatorBySectionQty = new Comparator<CategoryTicketGroup>() {
	
		public int compare(CategoryTicketGroup ticket1, CategoryTicketGroup ticket2) {
			
			if(ticket1.getActualSection().equals("PREMIUM") && ticket2.getActualSection().equals("PREMIUM")){
				return -1;
			}
			
			int cmp = ticket1.getActualSection().compareTo(
					ticket2.getActualSection());
			
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			
			int cmp1 = ticket1.getQuantity().compareTo(
					ticket2.getQuantity());
			if (cmp1 < 0) {
				return -1;
			}
			if (cmp1 > 0) {
				return 1;
			}
			int cmp2 = ticket1.getOriginalPrice().compareTo(
					ticket2.getOriginalPrice());
			if (cmp2 < 0) {
				return -1;
			}
			if (cmp2 > 0) {
				return 1;
			}
			
			return ticket1.getId().compareTo(ticket2.getId());
	    }};
	    
	    
	    
	    public static Comparator<CrownJewelTicket> crownJewelTicketComparatorForQty = new Comparator<CrownJewelTicket>() {
	
			public int compare(CrownJewelTicket ticket1, CrownJewelTicket ticket2) {
				int cmp = ticket1.getQuantity().compareTo(
						ticket2.getQuantity());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				return ticket1.getTicketId().compareTo(ticket2.getTicketId());
		    }};
	    
	    public static Comparator<CrownJewelTicketMap> crownJewelTicketSortingComparatorBySectionNew = new Comparator<CrownJewelTicketMap>() {
	
			public int compare(CrownJewelTicketMap ticket1, CrownJewelTicketMap ticket2) {
				
				if(ticket1.getSection().equals("PREMIUM") && ticket2.getSection().equals("PREMIUM")){
					return -1;
				}
				
				int cmp = ticket1.getSection().compareTo(
						ticket2.getSection());
				
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				
				return ticket1.getSection().compareTo(ticket2.getSection());
		    }};
		    
	    public static Comparator<CrownJewelTicketMap> crownJewelTicketSortingComparatorByPrice = new Comparator<CrownJewelTicketMap>() {
	    	
			public int compare(CrownJewelTicketMap ticket1, CrownJewelTicketMap ticket2) {
				
				int cmp = ticket1.getTicketsMap().get(0).getRequiredPointsAsDouble().compareTo(
						ticket2.getTicketsMap().get(0).getRequiredPointsAsDouble());
				
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				
				return ticket1.getSection().compareTo(ticket2.getSection());
		    }};
		    
		    
		public static Collection<CategoryTicketGroup> filterCheapestTicketByZoneByQty(Collection<CategoryTicketGroup> categoryTicketGroups){
			
			Map<String, List<CategoryTicketGroup>> zoneQtyTicketsMap = new HashMap<String, List<CategoryTicketGroup>>();
			
			for (CategoryTicketGroup ticketGroup : categoryTicketGroups) {
				String zone = ticketGroup.getActualSection().toUpperCase().replace("ZONE", "").replaceAll(" +","");
				
				List<CategoryTicketGroup> tickets = zoneQtyTicketsMap.get(zone+"_"+ticketGroup.getQuantity());
				
				if( null != tickets && !tickets.isEmpty()){
					tickets.add(ticketGroup);
				}else{
					tickets = new ArrayList<CategoryTicketGroup>();
					tickets.add(ticketGroup);
				}
				zoneQtyTicketsMap.put(zone+"_"+ticketGroup.getQuantity(), tickets);
			}
			
			categoryTicketGroups.clear();
			
			for (String key : zoneQtyTicketsMap.keySet()) {
				List<CategoryTicketGroup> tickets = zoneQtyTicketsMap.get(key);
				Collections.sort(tickets, TicketUtil.ticketSortingByPriceByPriority);
				categoryTicketGroups.add(tickets.get(0));
			}
			
			return categoryTicketGroups;
		}
			    
			    
	    public static CategoryTicketGroup getTicketDeliveryInFo(CategoryTicketGroup ticketGroup, String fontSizeStr){
	    	
	    	
	    	
	    	/*ticketGroup.setShippingMethod("We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab");
	    	
			ticketGroup.setDeliveryInfo("In the event your tickets will need to be shipped via <i>Federal Express</i> or <i>some " +
					"other method of delivery</i>, our representatives will contact you to make those arrangements at <b>NO additional cost to you</b>.");
			*/
	    	
			ticketGroup.setShippingMethod("We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab. " +
					"In the event your tickets need to be shipped we will contact you to make arrangements at NO additional cost to you.");
			
			
	    	
			ticketGroup.setDeliveryInfo("<font "+fontSizeStr+" color=#000000>We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab.<br /> " +
					"In the event your tickets need to be shipped we will contact you to make arrangements at <b><i>NO additional cost</i></b> to you.</font>");
			
			return ticketGroup;
		}
	    
	    public static CustomerOrder getTicketDeliveryInFo(CustomerOrder customerOrder, String fontSizeStr){
	    	
	    	customerOrder.setDeliveryInfo("<font "+fontSizeStr+" color=#000000>We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab.<br /> " +
					"In the event your tickets need to be shipped we will contact you to make arrangements at <b><i>NO additional cost</i></b> to you.</font>");
	    	
			customerOrder.setShippingMethod("We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab. " +
					"In the event your tickets need to be shipped we will contact you to make arrangements at NO additional cost to you.");
			
			return customerOrder;
		}
	    
	    public static CustomerOrder getGiftCardDeliveryInFo(CustomerOrder customerOrder, String fontSizeStr){
	    	
	    	customerOrder.setDeliveryInfo("<font "+fontSizeStr+" color=#000000>We will send you an alert as soon as your Gift Card are ready for download in your MY TICKETS tab.<br /> " +
					"In the event your Gift Card need to be shipped we will contact you to make arrangements at <b><i>NO additional cost</i></b> to you.</font>");
	    	
			customerOrder.setShippingMethod("We will send you an alert as soon as your Gift Card are ready for download in your MY TICKETS tab. " +
					"In the event your Gift Card need to be shipped we will contact you to make arrangements at NO additional cost to you.");
			
			return customerOrder;
		}
	    
	    public static String getGiftCardDeliveryInFo(){
			return "We will send you an alert as soon as your Gift Card are ready for download in your MY GIFT CARDS tab. " +
					"In the event your Gift Card need to be shipped we will contact you to make arrangements at NO additional cost to you.";
		}
	    
	    
	    
	    public static CustomerOrder getTicketDeliveryInFo(CustomerOrder customerOrder){
	    	
	    	customerOrder.setDeliveryInfo("<font color=#000000>We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab.<br /> " +
					"In the event your tickets need to be shipped we will contact you to make arrangements at <b><i>NO additional cost</i></b> to you.</font>");
	    	
			customerOrder.setShippingMethod("We will send you an alert as soon as your tickets are ready for download in your MY TICKETS tab. " +
					"In the event your tickets need to be shipped we will contact you to make arrangements at NO additional cost to you.");
			
			return customerOrder;
		}
	    
	    
	    public static CustomerOrder getUnfilledTicketOrderDeliveryNote(CustomerOrder order,String fontSizeStr) throws ParseException{
	    	order.setOrderDeliveryNote("<font "+fontSizeStr+" color=#000000>Usually the tickets are available a day before the event.</font>");
	    	
			return order;
		}
	    
	    public static CustomerOrder getProgressPopupMsg(CustomerOrder order,String fontSizeStr) throws ParseException{
	    	order.setProgressPopupMsg("<font "+fontSizeStr+" color=#000000>Usually the tickets are available a day before the event.</font>");
			return order;
		}
	    
	    public static void main(String[] args) {
	    	Date orderDate = new Date();
	    	Calendar cal = Calendar.getInstance();
	    	cal.setTime(orderDate);
	    	cal.add(Calendar.DATE, +19);
	    	DateFormat formateDateTime = new SimpleDateFormat("MM/dd/yyyy");
	    	String expectedDate = formateDateTime.format(cal.getTime());
	    	System.out.println(expectedDate);
		}
	    
	    public static CustomerOrder getGiftCardProgressPopupMsg(CustomerOrder order,String fontSizeStr) throws ParseException{
	    	try {
	    		Date orderDate = order.getCreateDateTemp();
		    	Calendar cal = Calendar.getInstance();
		    	cal.setTime(orderDate);
		    	cal.add(Calendar.DATE, +19);
		    	DateFormat formateDateTime = new SimpleDateFormat("MM/dd/yyyy");
		    	String expectedDate = formateDateTime.format(cal.getTime());
		    	order.setProgressPopupMsg("<font "+fontSizeStr+" color=#000000>Your Gift Card will be available on "+expectedDate+".</font>");
	    	}catch(Exception e) {
	    		order.setProgressPopupMsg("<font "+fontSizeStr+" color=#000000>Usually the Gift Card are available within 7 days of order date.</font>");
	    		e.printStackTrace();
	    	}
			return order;
		}
	    
	    public static String getTicketDownloadOfferOption1(String fontSizeStr) throws ParseException {
	    	
	    	String option1 = "<font "+fontSizeStr+" color=#012e4f>Please upload my tickets as soon as they are ready for download.</font>";
			return option1;
		}
	    
	    public static String getTicketDownloadOfferOption2(Event event,String fontSizeStr, String points) throws ParseException{
	    	
	    	String eventDateTime = event.getEventDateStr();
	    	
	    	if(event.getEventTimeStr().equals("TBD")){
	    		eventDateTime = eventDateTime +" 12:00 AM";
	    	}else{
	    		eventDateTime = eventDateTime +" "+event.getEventTimeStr();
	    	}
	    	Date eventDate = df.parse(eventDateTime);
	    	Calendar cal = Calendar.getInstance();
	    	cal.setTime(eventDate);
	    	cal.add(Calendar.DATE, -1);
	    	DateFormat formateDateTime = new SimpleDateFormat("MM/dd/yyyy");
	    	String calculatedDate = formateDateTime.format(cal.getTime());
	    	String option2 = "<font "+fontSizeStr+" color=#012e4f>I'm not in a rush and would rather receive a 5% additional discount on THIS purchase by agreeing to download " +
					"my tickets on <b>"+calculatedDate+"</b>, the day before the event.</font>";
			return option2;
		}
	    
	    
	    public static String getInProgressButtonText(CustomerOrder customerOrder) throws ParseException{
	    	if(null == customerOrder.getEventDate() || customerOrder.getEventDate().equals("TBD")) {
	    		return "TBD";
	    	}
	    	DateFormat formateDateTime = new SimpleDateFormat("MM/dd/yyyy");
	    	String eventDateTime = customerOrder.getEventDate();
	    	Date eventDate = formateDateTime.parse(eventDateTime);
	    	Calendar cal = Calendar.getInstance(); 
	    	cal.setTime(eventDate);
	    	cal.add(Calendar.DATE, -1);
	    	String calculatedDate = formateDateTime.format(cal.getTime());
	    	String option2 = "Download Date :"+calculatedDate;
			return option2;
		}
	    
	    
	    
	    public static String getSportsCJERedeemButtonText(CrownJewelCategoryTicket catTix,CrownJewelTeams crownJewelTeams,
	    		CrownJewelLeagues league,String requiredPoints,String fontSizeStr){
	    	
	    	String redeemAlertText = "<font "+fontSizeStr+" color=#000000>By redeeming <i><b>"+requiredPoints+"</b></i> of your " +
			"Reward Dollars today, if the <i><b>"+crownJewelTeams.getName()+"</b></i> make it to the " +
			"<i><b>"+league.getName()+"</b></i>, we will REWARD YOU with <i>" +
			"<b>"+catTix.getQuantity()+"</b></i> FREE tickets seated together in ZONE " +
			"<b>"+catTix.getSection()+"</b></i> - the only cost to you is the " +
			"<b>"+requiredPoints+"</b></i> Reward Dollars that you are about to redeem. If " +
			"<i><b>"+crownJewelTeams.getName()+"</b></i> do not make it to the <i>" +
			"<b>"+league.getName()+"</b></i> then we return your reward dollars to your rewards account.</font>";
			
			return redeemAlertText;
		}
	    
	    public static String getSportsCJERedeemButtonTextDesktop(CrownJewelCategoryTicket catTix,CrownJewelTeams crownJewelTeams,
		 CrownJewelLeagues league,String requiredPoints,String fontSizeStr){
	    	String redeemAlertText = "<font "+fontSizeStr+" color=#000000>By redeeming <b>"+requiredPoints+"</b> of your " +
			"Reward Dollars today, if the <b>"+crownJewelTeams.getName()+"</b> make it to the " +
			"<b>"+league.getName()+"</b>, we will REWARD YOU with " +
			"<b>"+catTix.getQuantity()+"</b> FREE tickets seated together in ZONE " +
			"<b>"+catTix.getSection()+"</b> - the only cost to you is the " +
			"<b>"+requiredPoints+"</b> Reward Dollars that you are about to redeem. If " +
			"<b>"+crownJewelTeams.getName()+"</b> do not make it to the " +
			"<b>"+league.getName()+"</b> then we return your reward dollars to your rewards account.</font>";
						
			return redeemAlertText;
		}
		
	    
	    
	    public static String getOthersCJERedeemButtonText(CrownJewelCategoryTicket catTix,String requiredPoints , String fontSizeStr){
	    	/*String redeemAlertText = "<font color=#000000>By redeeming <i><font color=#012e4f><b>"+requiredPoints+"</b></font></i> of " +
	    			"your Reward Points today, we will REWARD YOU with <i><font color=#012e4f><b>"+catTix.getQuantity()+"</b></font></i> " +
	    			"FREE tickets seated together in ZONE <i><font color=#012e4f><b>"+catTix.getSection()+"</b></font></i> - the only cost to you is the " +
	    			"<i><font color=#012e4f><b>"+requiredPoints+"</b></font></i> Reward Points that you are about to redeem . Remember, to please keep in " +
	    			"mind that once you agree to do redeem these points, they will not be returned to your rewards account.</font>";*/
	    	
	    	String redeemAlertText = "<font "+fontSizeStr+" color=#000000>By redeeming <i><b>"+requiredPoints+"</b></i> of " +
			"your Reward Dollars today, we will REWARD YOU with <i><b>"+catTix.getQuantity()+"</b></i> " +
			"FREE tickets seated together in ZONE <i><b>"+catTix.getSection()+"</b></i> - the only cost to you is the " +
			"<i><b>"+requiredPoints+"</b></i> Reward Dollars that you are about to redeem . Remember," +
					" if we does not make it, we return your reward dollars to your rewards account.</font>";
			return redeemAlertText;
		}
	    
	    public static String getOthersCJERedeemButtonTextDesktop(CrownJewelCategoryTicket catTix,String requiredPoints , String fontSizeStr){
	    	String redeemAlertText = "<font "+fontSizeStr+" color=#000000>By redeeming <b>"+requiredPoints+"</b> of " +
			"your Reward Dollars today, we will REWARD YOU with <b>"+catTix.getQuantity()+"</b> " +
			"FREE tickets seated together in ZONE <b>"+catTix.getSection()+"</b> - the only cost to you is the " +
			"<b>"+requiredPoints+"</b> Reward Dollars that you are about to redeem . Remember," +
					" if we does not make it, we return your reward dollars to your rewards account.</font>";
			return redeemAlertText;
		}
	    
	    
	    public static Comparator<Event> eventDaysComparatorForEventDateAsc = new Comparator<Event>() {
	    	
			public int compare(Event event1, Event event2) {
				int cmp = event1.getDayDifference().compareTo(
						event2.getDayDifference());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				return event1.getEventId().compareTo(event2.getEventId());
		}};
		    
		    
	    public static Comparator<String> comparatorCardsAscByEventDate = new Comparator<String>() {
	    	
			public int compare(String event1, String event2) {
				
				Long minDifference1 = Long.valueOf(event1.split("_")[0]);
				Long minDifference3 = Long.valueOf(event2.split("_")[0]);
				
				int cmp = minDifference1.compareTo(
						minDifference3);
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				return minDifference1.compareTo(minDifference3);
		}};
		
		public static Comparator<InvoiceTicketAttachment> ticketAttachmentsByPosition = new Comparator<InvoiceTicketAttachment>() {
			
			public int compare(InvoiceTicketAttachment ticket1, InvoiceTicketAttachment ticket2) {
				int cmp = ticket1.getPosition().compareTo(
						ticket2.getPosition());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				return ticket1.getId().compareTo(ticket2.getId());
		    }};
		    
		    
		    public static Integer[] getPartition(int quantity) {
				
				if(quantity == 1){
					return new Integer[]{1};
				}else if(quantity == 2){
					return new Integer[]{2};
				}/*else if(quantity == 3) {
					return new Integer[]{2};
				}else if(quantity == 4) {
					return new Integer[]{1,2,3,4};
				}else if(quantity == 5) {
					return new Integer[]{1,2,3,4};
				}else if(quantity == 6) {
					return new Integer[]{1,2,3,4};
				}else if(quantity == 7) {
					return new Integer[]{1,2,3,4};
				}else if(quantity == 8) {
					return new Integer[]{1,2,3,4};
				}else if(quantity == 9) {
					return new Integer[]{1,2,3,4};
				}else if(quantity == 10) {
					return new Integer[]{1,2,3,4};
				}else if(quantity == 11) {
					return new Integer[]{1,2,3,4};
				}*/else{
					return new Integer[]{2};
				}
			}  
		    
		    public static Integer[] getQuantityPartition(ZonesQuantityTypes types,int availableQty) {
		    	
		    	if(availableQty < 3){
		    		
		    		switch (types) {
						case TWO:
							return new Integer[]{2};
							
						case FOUR:
							return new Integer[]{2};
							
						case TWO_AND_FOUR:
							return new Integer[]{2};
		
						default:
							return new Integer[]{2};
		    		}
		    		
		    	}else{
		    		
		    		switch (types) {
						case TWO:
							return new Integer[]{2};
							
						case FOUR:
							return new Integer[]{4};
							
						case TWO_AND_FOUR:
							return new Integer[]{2,4};
		
						default:
							return new Integer[]{2};
					}
		    	}
		    	
		    	
			} 

			
	    public static Comparator<CrownJewelCategoryTicket> fantasyTicketsSortingByPrice = new Comparator<CrownJewelCategoryTicket>() {
			
			public int compare(CrownJewelCategoryTicket ticket1, CrownJewelCategoryTicket ticket2) {
				int cmp1 = ticket1.getPrice().compareTo(
						ticket2.getPrice());
				if (cmp1 < 0) {
					return -1;
				}
				if (cmp1 > 0) {
					return 1;
				}
				
				return ticket1.getId().compareTo(ticket2.getId());
		 }};	
		 
	 public static String getFontSize(HttpServletRequest request,ApplicationPlatForm applicationPlatForm ){
		 
		 String deviceType = null != request.getParameter("deviceType")?request.getParameter("deviceType"):"";
		 
		 String fontSizeStr = "";
		 if(applicationPlatForm.equals(ApplicationPlatForm.IOS)){
				try{
					if(deviceType.equals("IPHONE")){
						fontSizeStr ="size='4'";
					}else if(deviceType.equals("IPAD")){
						fontSizeStr ="size='6'";
					}
				}catch (Exception e) {
					fontSizeStr ="size='4'";
				}
			}
		 return fontSizeStr;
	 }
	 
	 public static Boolean getOtherTeamsButtonOptions(Event event, List<Artist> artists ){
		 
		boolean showOtherTeamsButton =  false;
		try{
			if(event.getParentCategoryId().equals(1) && artists.size() > 1){
				showOtherTeamsButton =  true;
				return showOtherTeamsButton;
			}
			
			String eventName = event.getEventName().toLowerCase();
			if(eventName.contains("vs.")){
				showOtherTeamsButton =  true;
			}else if(eventName.contains("vs.") && eventName.contains(":")){
				showOtherTeamsButton =  true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return showOtherTeamsButton;
	 }
	 
	 
	 public static Double getBrokerServiceFees(Integer brokerId , Double price , Integer qty, Double serviceTax){
		Double brokerFees = 0.00;
		if(brokerId == null || brokerId.equals(1001)){
			brokerFees = (double) Math.ceil(((price * qty)*(serviceTax/100)) * 100 / 100);
			//brokerFees = tax * qty;
		}else{
			Broker broker = RTFAffiliateBrokerUtil.getBrokerByBrokerId(brokerId);
			brokerFees = (double) Math.ceil(((price * qty)*(broker.getServiceFeesPerc()/100)) * 100 / 100);
		}
		return brokerFees;
	 }
	 
	 
	 public static Double getBrokerServiceFeePerc(Integer brokerId ){
		Double brokerFeePerc = 0.00;
		if(brokerId.equals(1001) ){
			//brokerFeePerc = 0.00;
		}else{
			Broker broker = RTFAffiliateBrokerUtil.getBrokerByBrokerId(brokerId);
			brokerFeePerc = broker.getServiceFeesPerc();
		}
		return brokerFeePerc;
	 }
	 
	 public static Boolean triggerOrderEmailToBroker(Integer brokerId ){
		if(brokerId.equals(1001) ){
			return false;
		}else{
			return true;
		}
	 }
	 
	 
	 
}
