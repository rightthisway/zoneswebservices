package com.zonesws.webservices.utils;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Ticket;

@XStreamAlias("Ticket")
public class TicketWrapper {
	private Integer totalNoOfTickets;
	private List<TicketGroup> tickets;
	
	public Integer getTotalNoOfTickets() {
		return totalNoOfTickets;
	}
	public void setTotalNoOfTickets(Integer totalNoOfTickets) {
		this.totalNoOfTickets = totalNoOfTickets;
	}
	public List<TicketGroup> getTickets() {
		return tickets;
	}
	public void setTickets(List<TicketGroup> tickets) {
		this.tickets = tickets;
	}

}
