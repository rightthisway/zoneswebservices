package com.zonesws.webservices.utils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class TimeConverter implements Converter {

	public TimeConverter() {
		super();
		System.out.println("TimeConverter constructor invoked..");
	}

	public boolean canConvert(Class clazz) {
		return Date.class.isAssignableFrom(clazz);
	}

	public void marshal(Object value, HierarchicalStreamWriter writer, 
	MarshallingContext context) {
		DateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
//		dateFormat.applyPattern("EEE, MMM dd, yy");
	
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
	
//		DateFormat formatter = dateFormat.getDateInstance();
		writer.setValue(dateFormat.format(value));
	}

	public Date unmarshal(HierarchicalStreamReader reader, 
		UnmarshallingContext context) {
		//Wed, Mar 12, 08
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
	
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
//		DateFormat formatter = dateFormat.getDateInstance();
		System.out.println(reader.getValue());
		try {
			date = dateFormat.parse(reader.getValue());
		} catch (ParseException e) {
			throw new ConversionException(e.getMessage(), e);
		}
		return date;
	}
	
	
	public static void main(String[] args) {

		TimeConverter obj = new TimeConverter();
		System.out.println(obj.getFileWithUtil("email-resources/AboutUsMap.png"));
			
	  }

	  private String getFileWithUtil(String fileName) {

		String result = "";
			
		ClassLoader classLoader = getClass().getClassLoader();
		/*try {
		    result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		//ClassLoader classLoader = new ReadResourceFileDemo().getClass().getClassLoader();
		 
        File file = new File(classLoader.getResource(fileName).getFile());
         
        //File is found
        System.out.println("File Found : " + file.exists());
			
		return result;
	  }
	  
	}
