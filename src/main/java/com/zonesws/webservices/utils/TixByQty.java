package com.zonesws.webservices.utils;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.RtfCatTicket;


@XStreamAlias("TixByQty")
public class TixByQty {
	
	private Integer qty;
	private String qtyMsg;
	private RtfCatTicket tixObj;
	
	
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getQtyMsg() {
		return qtyMsg;
	}
	public void setQtyMsg(String qtyMsg) {
		this.qtyMsg = qtyMsg;
	}
	public RtfCatTicket getTixObj() {
		return tixObj;
	}
	public void setTixObj(RtfCatTicket tixObj) {
		this.tixObj = tixObj;
	}
	 
	
	
}