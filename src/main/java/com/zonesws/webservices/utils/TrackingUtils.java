package com.zonesws.webservices.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.rtfquiz.jdbc.service.JDBCConnection;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.CustomerOrderTracking;
import com.zonesws.webservices.data.WebServiceTracking;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ProductType;
import com.zonesws.webservices.enums.WebServiceActionType;


public class TrackingUtils  {
	
	/*public static void webServiceTracking(HttpServletRequest request,WebServiceActionType actionType,String actionResult){
		WebServiceTracking webServiceTracking = new WebServiceTracking();
		HttpSession session = request.getSession();
		String startDateString = null;
		String endDateString = null;
		Date startDate=null;
		Date endDate=null;
		DateFormat df;
		
		switch (actionType) {
			
		case TICKET:
			webServiceTracking.setEventId(request.getParameter("eventId"));
			break;
			
		case GENERALISEDSEARCH:
		
			startDateString = request.getParameter("startDate");
			endDateString = request.getParameter("endDate");
			startDate=null;
			endDate=null;
			df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			
			if(startDateString!=null && !startDateString.isEmpty()){
				try {
					startDate = df.parse(startDateString +" 00:00:00");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			if(endDateString!=null && !endDateString.isEmpty()){
				try {
					endDate = df.parse(endDateString + " 23:59:59");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			webServiceTracking.setEventId(request.getParameter("id"));
			webServiceTracking.setEventName(request.getParameter("name"));
			webServiceTracking.setStartDate(startDate);
			webServiceTracking.setEndDate(endDate);
			webServiceTracking.setArtistId(request.getParameter("artistId"));
			webServiceTracking.setVenueId(request.getParameter("venueId"));
			webServiceTracking.setLoactionName(request.getParameter("location"));
			webServiceTracking.setCity(request.getParameter("city"));
			webServiceTracking.setState(request.getParameter("state"));
			break;
			
		case HOLDTICKET:
			webServiceTracking.setTicketGroupId(request.getParameter("ticketGroupId"));
			break;
		case EXTENDHOLDTICKET:
			webServiceTracking.setHoldTicketId(request.getParameter("holdTicketId"));
			break;
			
		case SOLDTICKET:
			webServiceTracking.setHoldTicketId(request.getParameter("holdTicketId"));
			break;
		
		case GETORDERS:
			startDateString = request.getParameter("startDate");
			endDateString = request.getParameter("endDate");
			startDate=null;
			endDate=null;
			df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			
			if(startDateString!=null && !startDateString.isEmpty()){
				try {
					startDate = df.parse(startDateString +" 00:00:00");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
			if(endDateString!=null && !endDateString.isEmpty()){
				try {
					endDate = df.parse(endDateString + " 23:59:59");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			webServiceTracking.setStartDate(startDate);
			webServiceTracking.setEndDate(endDate);
			break;
			
		default:
			break;
		}
		
		webServiceTracking.setWebConfigId(request.getParameter("configId"));
		webServiceTracking.setIpAddress(session.getAttribute("ip").toString());
		webServiceTracking.setMethodName(request.getPathInfo());
		webServiceTracking.setUrl(request.getRequestURL().toString());
		webServiceTracking.setActionType(actionType);
		webServiceTracking.setActionProcess("Hit");
		webServiceTracking.setHittingDate(new Date());
		webServiceTracking.setActionResult(actionResult);
		if(actionResult.contains("You are not authorized")){
			webServiceTracking.setAuthenticatedHit("No");
		}else{
			webServiceTracking.setAuthenticatedHit("Yes");
		}
		DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
		
	}
	*/
	
	/*public static void webServiceTracking(HttpServletRequest request,WebServiceActionType actionType,String actionResult){
		
		try{
			WebServiceTracking webServiceTracking = new WebServiceTracking();
			HttpSession session = request.getSession();
			
			String configId = ((HttpServletRequest)request).getHeader("x-token");
			String xPlatform = ((HttpServletRequest)request).getHeader("x-platform");
			String customerIpAddress = "";
			
			if(null != xPlatform  && !xPlatform.isEmpty() && (xPlatform.contains("IOS") || xPlatform.contains("ANDROID"))){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			
			if(null == customerIpAddress){
				customerIpAddress = session.getAttribute("ip").toString();;
			}
			webServiceTracking.setWebConfigId(configId);
			webServiceTracking.setServerIpAddress(session.getAttribute("ip").toString());
			webServiceTracking.setCustomerIpAddress(customerIpAddress);
			webServiceTracking.setMethodName(request.getPathInfo());
			webServiceTracking.setUrl(request.getRequestURL().toString());
			webServiceTracking.setActionType(actionType);
			webServiceTracking.setEventSearchValue(null != request.getParameter("searchKey")?request.getParameter("searchKey"):null);
			webServiceTracking.setEventGpsSearch(null != request.getParameter("searchByGps")?request.getParameter("searchByGps"):null);
			webServiceTracking.setEventId(null != request.getParameter("eventId")?request.getParameter("eventId"):null);
			webServiceTracking.setOrderId(null != request.getAttribute("customerOrderId")?String.valueOf(request.getAttribute("customerOrderId")):null);
			webServiceTracking.setCustomerId(null != request.getParameter("customerId")?request.getParameter("customerId"):null);
			webServiceTracking.setOrderSoldPrice(null != request.getParameter("soldPrice")?request.getParameter("soldPrice"):null);
			webServiceTracking.setOrderTicketGroupId(null != request.getParameter("ticketGroupId")?request.getParameter("ticketGroupId"):null);
			webServiceTracking.setOrderTicketGroupQty(null != request.getParameter("qty")?request.getParameter("qty"):null);
			webServiceTracking.setZoneTicketInfoPage(null != request.getParameter("infoType")?request.getParameter("infoType"):null);
			webServiceTracking.setActionProcess("Hit");
			webServiceTracking.setHittingDate(new Date());
			webServiceTracking.setActionResult(actionResult);
			
			if(null != actionResult && !actionResult.isEmpty() && actionResult.contains("You are not authorized")){
				webServiceTracking.setAuthenticatedHit("No");
			}else{
				webServiceTracking.setAuthenticatedHit("Yes");
			}
			DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	*/
	
	
	public static void webServiceTracking(HttpServletRequest request,WebServiceActionType actionType,String actionResult){
		
		WebServiceTracking webServiceTracking = new WebServiceTracking();
		
		try{
			
			HttpSession session = request.getSession();
			String productType = request.getParameter("productType");
			String configId = ((HttpServletRequest)request).getHeader("x-token");
			String xPlatform = ((HttpServletRequest)request).getHeader("x-platform");
			String sign = ((HttpServletRequest)request).getHeader("x-sign");
			String sessionId = ((HttpServletRequest)request).getHeader("deviceId");
			
			String customerIpAddress = "";
			if(null != xPlatform  && !xPlatform.isEmpty() && (xPlatform.contains("IOS") || xPlatform.contains("ANDROID"))){
				//customerIpAddress = session.getAttribute("ip").toString();
				customerIpAddress = ((HttpServletRequest)request).getHeader("X-Forwarded-For");
				//System.out.println("X-Forwarded-For 1 : "+customerIpAddress);
				//System.out.println("X-Forwarded-For 2 : "+((HttpServletRequest)request).getHeader("x-forwarded-for"));
				//System.out.println("clientIPAddress 1 : "+request.getParameter("clientIPAddress"));

			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):session.getAttribute("ip").toString();
			}
			webServiceTracking.setProductType(null != productType && !productType.isEmpty() ? ProductType.valueOf(productType):ProductType.REWARDTHEFAN);
			webServiceTracking.setWebXSign(sign);
			if(xPlatform != null && !xPlatform.isEmpty()) {
				try {
					webServiceTracking.setPlatForm(ApplicationPlatForm.valueOf(xPlatform));
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			webServiceTracking.setSessionId(sessionId);
			webServiceTracking.setWebConfigId(configId);
			webServiceTracking.setServerIpAddress(session.getAttribute("ip").toString());
			webServiceTracking.setCustomerIpAddress(customerIpAddress);
			webServiceTracking.setHitDate(new Date());
			webServiceTracking.setActionResult(actionResult);
			webServiceTracking.setActionType(actionType);
			if(null != actionResult && !actionResult.isEmpty() && actionResult.contains("You are not authorized")){
				webServiceTracking.setAuthenticatedHit("No");
			}else{
				webServiceTracking.setAuthenticatedHit("Yes");
			}
			
			String custId =request.getParameter("customerId");
			Integer customerId = null,categoryTicketGroupId=null,orderId=null,eventId=null;
			try {
				customerId = (null != custId && !custId.isEmpty() && !custId.equalsIgnoreCase("null")) ?Integer.parseInt(request.getParameter("customerId")):null;
			}catch(Exception e) {
				customerId = null;
			}
			try {
				if(customerId == null) {
					String cId =request.getParameter("cId");
					customerId = (null != cId && !cId.isEmpty() && !cId.equalsIgnoreCase("null")) ?Integer.parseInt(cId):null;
				}
			} catch (Exception e) {
			}
			
			try {
				eventId = null != request.getParameter("eventId") && request.getParameter("eventId") != ""?Integer.parseInt(request.getParameter("eventId")):null;
			}catch(Exception e) {
				eventId = null;
			}
			
			try {
				orderId = null != request.getParameter("orderId") && request.getParameter("orderId") != "" ?Integer.parseInt(request.getParameter("orderId")):null;
			}catch(Exception e) {
				orderId = null;
			}
			
			try {
				categoryTicketGroupId = null != request.getParameter("categoryTicketGroupId") && request.getParameter("categoryTicketGroupId") != ""?Integer.parseInt(request.getParameter("categoryTicketGroupId")):null;
			}catch(Exception e) {
				categoryTicketGroupId = null;
			}
			
			webServiceTracking.setCategoryTicketGroupId(categoryTicketGroupId);
			webServiceTracking.setEventId(eventId);
			webServiceTracking.setCustomerId(customerId);
			webServiceTracking.setOrderId(orderId);
			
			webServiceTracking.setContestId(request.getParameter("contestId"));
			
			getDetailsByAPI(request, webServiceTracking, actionType);
			DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING API");
		}
	}
	
	public static WebServiceTracking getDetailsByAPI(HttpServletRequest request,WebServiceTracking webServiceTracking , 
			WebServiceActionType actionType){
		
		String actionResult = webServiceTracking.getActionResult();
		if(actionResult == null) {
			actionResult = "";
		}
		switch (actionType) {
		
			case EVENTSEARCH:
				String searchType = request.getParameter("searchType");
				String searchKey = request.getParameter("searchKey");
				String artistIdStr = request.getParameter("artistId");
				String venueIdStr = request.getParameter("venueId");
				String rtfCatSearchIdStr = request.getParameter("rtfCatSearchId");
				String grandChildCategoryIdStr = request.getParameter("grandchildId");
				String childIdStr = request.getParameter("childId");
				String parentCatIdStr = request.getParameter("parentCatId");
				String locationOption = request.getParameter("locationOption"); 
				String state = request.getParameter("state");
				
				String key = searchType+":: KEY : "+searchKey+" : ";
				if(null != parentCatIdStr && !parentCatIdStr.isEmpty()){
					key +="PARENTID: "+parentCatIdStr;
				}else if(null != childIdStr && !childIdStr.isEmpty()){
					key +="CHILDID: "+childIdStr;
				}else if(null != grandChildCategoryIdStr && !grandChildCategoryIdStr.isEmpty()){
					key +="GRANDCHILDID: "+grandChildCategoryIdStr;
				}else if(null != rtfCatSearchIdStr && !rtfCatSearchIdStr.isEmpty()){
					key +="RTFCATSEARCHID: "+rtfCatSearchIdStr;
				}else if(null != artistIdStr && !artistIdStr.isEmpty()){
					key +="ARTISTID: "+artistIdStr;
				}else if(null != venueIdStr && !venueIdStr.isEmpty()){
					key +="VENUEID: "+venueIdStr;
				}else {
					key +=searchType+":SEARCHKEY:"+searchKey;
				}
				if(null != locationOption && locationOption.equals("Enabled")){
					locationOption = "ENABLED:"+state;
				}else{
					locationOption = "DISABLED";
				}
				
				webServiceTracking.setEventSearchKey(key);
				
				key = "Generalized Search is done. "+key;
				webServiceTracking.setActionResult(key);
				
				webServiceTracking.setLocationSearch(locationOption);
				break;
				
			case STRIPETRANSACTIONFORCUSTOMER:
				
				String trxAmtStr = request.getParameter("transactionAmount");
				String rtfCustIdStr=request.getParameter("rtfCustId");
				String tGroupIdStr=request.getParameter("tGroupId");
				webServiceTracking.setCustomerId(Integer.valueOf(rtfCustIdStr.trim()));
				webServiceTracking.setCategoryTicketGroupId(Integer.valueOf(tGroupIdStr.trim()));
				webServiceTracking.setTrxAmount(Double.valueOf(trxAmtStr.trim()));
				break;
				
			case PAYPALTRACKING:
				
				String cId = request.getParameter("cId");
				String tId = request.getParameter("tId");
				String eId = request.getParameter("eId");
				String orderTotal = request.getParameter("orderTotal");
				webServiceTracking.setEventId(Integer.valueOf(eId.trim()));
				webServiceTracking.setCustomerId(Integer.valueOf(cId.trim()));
				webServiceTracking.setCategoryTicketGroupId(Integer.valueOf(tId.trim()));
				webServiceTracking.setTrxAmount(Double.valueOf(orderTotal.trim()));
				break;
				
				
			case VIEWORDER:
				String orderIdStr = request.getParameter("orderNo");
				webServiceTracking.setOrderId(Integer.valueOf(orderIdStr.trim()));
				break;
				
			case CUSTOMERSIGNUP:
				String signUpTypeStr = request.getParameter("signUpType");
				String name = request.getParameter("name");
				String lastName =request.getParameter("lastName");
				String email = request.getParameter("email");
				key = "Registration Done. Details : "+signUpTypeStr+":"+name+":"+lastName+":"+email;
				webServiceTracking.setActionResult(key);
				break;
				
			case CUSTOMERLOGIN:
			
				email = request.getParameter("userName");
				key = "Successfully loggedin.Your User Name:"+email;
				webServiceTracking.setActionResult(key);
				break;
				
			case RESETPASSWORDREQUEST:
				
				email = request.getParameter("email");
				key = "Reset password request.Your Email: "+email;
				webServiceTracking.setActionResult(key);
				break;
				
			case VALIDATEREFERRALCODE:
				String referralCode = request.getParameter("referralCode");
				String tgIdStr = request.getParameter("tgId");
				String orderTotalStr = request.getParameter("orderTotal");
				key = "Promotional or referral Code validated and Referral Code is: "+referralCode;
				webServiceTracking.setCategoryTicketGroupId(Integer.valueOf(tgIdStr.trim()));
				webServiceTracking.setTrxAmount(Double.valueOf(orderTotalStr.trim()));
				webServiceTracking.setActionResult(key);
				break;
				
				
			case CANCELORDER:
				orderIdStr = request.getParameter("orderNo");
				webServiceTracking.setOrderId(Integer.valueOf(orderIdStr.trim()));
				break;
				
			case SENDSMS:
				
				String phoneNumber = request.getParameter("phNo");
				key = "Sending App Store Links to Phone Number : "+phoneNumber;
				webServiceTracking.setActionResult(key);
				break;
				
			case GETFUTUREORDERCONFIRMATION:
				orderIdStr = request.getParameter("crownJewelOrderNo");
				webServiceTracking.setOrderId(Integer.valueOf(orderIdStr.trim()));
				break;
				
			case GETSUPERFANORDERSUMMARY:
				String teamIdStr = request.getParameter("teamId");
				String leagueIdStr = request.getParameter("leagueId");
				String ticketIdStr = request.getParameter("ticketId");
				key = "LeagueId : "+leagueIdStr+" and TeamId : "+teamIdStr+" and TicketId : "+ticketIdStr;
				webServiceTracking.setActionResult(key);
				break;
				
			case GETSPORTSCROWNTICKETS:
				leagueIdStr = request.getParameter("leagueId");
				teamIdStr = request.getParameter("teamId");
				key = "LeagueId : "+leagueIdStr+" and TeamId : "+teamIdStr;
				webServiceTracking.setActionResult(key);
				break;
			case QUIZVALIDATEANSWERS:
				key = actionResult+" :qNo : "+request.getParameter("questionNo")+" :qId: "+request.getParameter("questionId")+
				" :ans:"+request.getParameter("answer")+" :isCont:"+request.getParameter("contestant");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZQUESTIONINFO:
				key = actionResult+" :qNo : "+request.getParameter("questionNo");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZGETQUESTIONANSWERS:
				key = actionResult+" :qNo : "+request.getParameter("questionNo")+" :qId: "+request.getParameter("questionId");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZAPPLYLIFELINE:
				key = actionResult+" :qNo : "+request.getParameter("questionNo")+" :qId: "+request.getParameter("questionId");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZCONTESTSUMMARY:
				key = actionResult+" :sType : "+request.getParameter("summaryType");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZUPDATEREFERALCODE:
				key = actionResult+" :refCode : "+request.getParameter("referralCode");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZCUSTOMERLOGIN:
				key = actionResult+" :uName : "+request.getParameter("userName")+" :phone: "+request.getParameter("phone");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZVALIDATEOTP:
				key = actionResult+" :vCode: "+request.getParameter("verificationCode")+
				" :TrkId: "+request.getParameter("otpTrackingId")+" :phone: "+request.getParameter("phone");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZUSERIDSETUP:
				key = actionResult+" :userId: "+request.getParameter("userId")+
				" :refCode: "+request.getParameter("referralCode");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZREMOVEDUPPHONEMAP:
				key = actionResult+" :phone: "+request.getParameter("phone");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZMANUALNOTIFICATION:
				key = actionResult+" :msg: "+request.getParameter("message");
				webServiceTracking.setActionResult(key);
				break;
			default:
				break;
		}
		
		return webServiceTracking;
	}
	
	
	public static WebServiceTracking getContestDetailsByAPI(HttpServletRequest request,WebServiceTracking webServiceTracking , 
			WebServiceActionType actionType){
		
		String actionResult = webServiceTracking.getActionResult();
		if(actionResult == null) {
			actionResult = "";
		}
		String key = "";
		switch (actionType) {
			 
			case QUIZVALIDATEANSWERS:
				key = actionResult+" :qNo : "+request.getParameter("questionNo")+" :qId: "+request.getParameter("questionId")+
				" :ans:"+request.getParameter("answer")+" :isCont:"+request.getParameter("contestant");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZQUESTIONINFO:
				key = actionResult+" :qNo : "+request.getParameter("questionNo");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZGETQUESTIONANSWERS:
				key = actionResult+" :qNo : "+request.getParameter("questionNo")+" :qId: "+request.getParameter("questionId");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZAPPLYLIFELINE:
				key = actionResult+" :qNo : "+request.getParameter("questionNo")+" :qId: "+request.getParameter("questionId");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZCONTESTSUMMARY:
				key = actionResult+" :sType : "+request.getParameter("summaryType");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZUPDATEREFERALCODE:
				key = actionResult+" :refCode : "+request.getParameter("referralCode");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZCUSTOMERLOGIN:
				key = actionResult+" :uName : "+request.getParameter("userName")+" :phone: "+request.getParameter("phone");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZVALIDATEOTP:
				key = actionResult+" :vCode: "+request.getParameter("verificationCode")+
				" :TrkId: "+request.getParameter("otpTrackingId")+" :phone: "+request.getParameter("phone");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZUSERIDSETUP:
				key = actionResult+" :userId: "+request.getParameter("userId")+
				" :refCode: "+request.getParameter("referralCode");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZREMOVEDUPPHONEMAP:
				key = actionResult+" :phone: "+request.getParameter("phone");
				webServiceTracking.setActionResult(key);
				break;
			case QUIZMANUALNOTIFICATION:
				key = actionResult+" :msg: "+request.getParameter("message");
				webServiceTracking.setActionResult(key);
				break;
			default:
				break;
		}
		
		return webServiceTracking;
	}
	
	
	
	public static void orderTracking(HttpServletRequest request, CustomerOrder customerOrder,String sessionId,String promoOrRefCode,Boolean isFanPrice){
		try{
			CustomerOrderTracking webServiceTracking = new CustomerOrderTracking();
			HttpSession session = request.getSession();
			String sign = ((HttpServletRequest)request).getHeader("x-sign");
			String configId = ((HttpServletRequest)request).getHeader("x-token");
			String customerIpAddress = "";
			if(customerOrder.getAppPlatForm().equals(ApplicationPlatForm.IOS) || 
					customerOrder.getAppPlatForm().equals(ApplicationPlatForm.ANDROID)){
				customerIpAddress = session.getAttribute("ip").toString();
			}else{
				customerIpAddress = null != request.getParameter("clientIPAddress")?request.getParameter("clientIPAddress"):null;
			}
			webServiceTracking.setIsFanPrice(isFanPrice);
			webServiceTracking.setProductType(customerOrder.getProductType());
			webServiceTracking.setSessionId(sessionId);
			webServiceTracking.setPlatForm(customerOrder.getAppPlatForm());
			webServiceTracking.setWebConfigId(configId);
			webServiceTracking.setWebXSign(sign);
			webServiceTracking.setWebXToken(configId);
			webServiceTracking.setServerIpAddress(session.getAttribute("ip").toString());
			webServiceTracking.setCustomerIpAddress(customerIpAddress);
			webServiceTracking.setEventId(customerOrder.getEventId());
			webServiceTracking.setOrderId(customerOrder.getId());
			webServiceTracking.setCustomerId(customerOrder.getCustomer().getId());
			webServiceTracking.setOrderTicketGroupId(customerOrder.getCategoryTicketGroupId());
			webServiceTracking.setOrderTotal(customerOrder.getOrderTotalAsDouble());
			webServiceTracking.setOrderDate(customerOrder.getCreateDateTemp());
			webServiceTracking.setPromoOrReferralCode(null != promoOrRefCode && !promoOrRefCode.isEmpty()?promoOrRefCode:null);
			DAORegistry.getCustomerOrderTrackingDAO().saveOrUpdate(webServiceTracking);;
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING ORDER");
		}
	}
	
	
	public static void contestValidateAnswerAPITrackingJDBC(HttpServletRequest request,WebServiceActionType actionType,String actionResult,
			String questionNo, String questionId, String contestant, String answer, String contestId,String custId){
		try{
			
			if(actionResult == null) {
				actionResult = "";
			}
			actionResult = actionResult+" :qNo : "+questionNo+" :qId: "+questionId+
					" :ans:"+contestant+" :isCont:"+answer;
			
			/*String query = "INSERT INTO web_service_tracking_new(product_type,hit_date,server_ip_address,customer_ip_address,api_name,platform,session_id,"
					+ "authenticated_hit,action_result,customer_id,web_config_id,web_xsign,contest_id) VALUES ('REWARDTHEFAN',"
					+ "getdate(),'0.0.0.0','"+request.getHeader("X-Forwarded-For")+"',"
					+ "'QUIZVALIDATEANSWERS','"+request.getHeader("x-platform")+"','"+request.getHeader("deviceId")+"',"
					+ "'Yes','"+actionResult+"',"+Integer.parseInt(custId)+",'WEBCONFIGID',"
					+ "'WEBXSIGN',"+contestId+" )";*/
			
			String query = "INSERT INTO web_service_tracking_new(product_type,hit_date,server_ip_address,customer_ip_address,api_name,platform,session_id,"
					+ "authenticated_hit,action_result,customer_id,web_config_id,web_xsign,contest_id) VALUES ('REWARDTHEFAN',"
					+ "getdate(),'0.0.0.0','192.168.0.28',"
					+ "'QUIZVALIDATEANSWERS','IOS','IOS',"
					+ "'Yes','"+actionResult+"',"+Integer.parseInt(custId)+",'WEBCONFIGID',"
					+ "'WEBXSIGN',"+contestId+" )";
			
			Connection connection = JDBCConnection.getRtfConnection();  
			
			ResultSet rs= null;
			Statement insertStatement =null;
			try {
				connection.setAutoCommit(false);
				insertStatement = connection.createStatement();
				insertStatement.executeUpdate(query);
				connection.commit();
			} catch (Exception e) {
				e.printStackTrace();
				connection.rollback();
			}finally{
				if(rs!=null){
					rs.close();
				}
				if(insertStatement!=null){
					insertStatement.close();
				}
			}
			
			
			//DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST VALIDATE ANSWER API");
		} 
	}
	
	
	public static void contestValidateAnswerAPITracking(HttpServletRequest request,WebServiceActionType actionType,String actionResult,
			String questionNo, String questionId, String contestant, String answer, String contestId,String custId){
		WebServiceTracking webServiceTracking = new WebServiceTracking();
		try{
			HttpSession session = request.getSession(); 
			//request = ((HttpServletRequest)request);
			String configId = request.getHeader("x-token");
			String xPlatform = request.getHeader("x-platform");
			String sign = request.getHeader("x-sign");
			String sessionId = request.getHeader("deviceId");
			String customerIpAddress = request.getHeader("X-Forwarded-For");
			
			webServiceTracking.setProductType(ProductType.REWARDTHEFAN);
			webServiceTracking.setWebXSign(sign);
			webServiceTracking.setPlatForm(ApplicationPlatForm.valueOf(xPlatform));
			webServiceTracking.setSessionId(sessionId);
			webServiceTracking.setWebConfigId(configId);
			webServiceTracking.setServerIpAddress(session.getAttribute("ip").toString());
			webServiceTracking.setCustomerIpAddress(customerIpAddress);
			webServiceTracking.setHitDate(new Date());
			webServiceTracking.setActionType(actionType);
			webServiceTracking.setAuthenticatedHit("Yes");
			webServiceTracking.setContestId(contestId);
			try {
				webServiceTracking.setCustomerId(Integer.parseInt(custId));
			}catch(Exception e) {
			}
			
			if(actionResult == null) {
				actionResult = "";
			}
			actionResult = actionResult+" :qNo : "+questionNo+" :qId: "+questionId+
					" :ans:"+contestant+" :isCont:"+answer;
			webServiceTracking.setActionResult(actionResult);
			DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING CONTEST VALIDATE ANSWER API");
		} 
	}
	
	
public static void contestAPITracking(HttpServletRequest request,WebServiceActionType actionType,String actionResult){
		
		WebServiceTracking webServiceTracking = new WebServiceTracking();
		
		try{
			
			HttpSession session = request.getSession(); 
			//request = ((HttpServletRequest)request);
			String configId = request.getHeader("x-token");
			String xPlatform = request.getHeader("x-platform");
			String sign = request.getHeader("x-sign");
			String sessionId = request.getHeader("deviceId");
			String customerIpAddress = request.getHeader("X-Forwarded-For");
			String custId = request.getParameter("customerId");
			String contestIdStr = request.getParameter("contestId"); 
			
			webServiceTracking.setProductType(ProductType.REWARDTHEFAN);
			webServiceTracking.setWebXSign(sign);
			webServiceTracking.setPlatForm(ApplicationPlatForm.valueOf(xPlatform));
			webServiceTracking.setSessionId(sessionId);
			webServiceTracking.setWebConfigId(configId);
			webServiceTracking.setServerIpAddress(session.getAttribute("ip").toString());
			webServiceTracking.setCustomerIpAddress(customerIpAddress);
			webServiceTracking.setHitDate(new Date());
			webServiceTracking.setActionResult(actionResult);
			webServiceTracking.setActionType(actionType);
			webServiceTracking.setAuthenticatedHit("Yes");
			
			try {
				webServiceTracking.setCustomerId(Integer.parseInt(custId));
			}catch(Exception e) {
			}
			
			webServiceTracking.setContestId(contestIdStr);
			
			getContestDetailsByAPI(request, webServiceTracking, actionType);
			DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("ERROR WHILE TRACKING API");
		} 
	}


public static void contestDashboardAPITracking(HttpServletRequest request,WebServiceActionType actionType,String actionResult){
	
	WebServiceTracking webServiceTracking = new WebServiceTracking();
	
	try{
		
		HttpSession session = request.getSession(); 
		//request = ((HttpServletRequest)request);
		String configId = request.getHeader("x-token");
		String xPlatform = request.getHeader("x-platform");
		String sign = request.getHeader("x-sign");
		String sessionId = request.getHeader("deviceId");
		String customerIpAddress = request.getHeader("X-Forwarded-For");
		String custId = request.getParameter("customerId");
		String contestIdStr = request.getParameter("contestId"); 
		
		webServiceTracking.setProductType(ProductType.REWARDTHEFAN);
		webServiceTracking.setWebXSign(sign);
		webServiceTracking.setPlatForm(ApplicationPlatForm.valueOf(xPlatform));
		webServiceTracking.setSessionId(sessionId);
		webServiceTracking.setWebConfigId(configId);
		webServiceTracking.setServerIpAddress(session.getAttribute("ip").toString());
		webServiceTracking.setCustomerIpAddress(customerIpAddress);
		webServiceTracking.setHitDate(new Date());
		webServiceTracking.setActionResult(actionResult);
		webServiceTracking.setActionType(actionType);
		webServiceTracking.setAuthenticatedHit("Yes");
		
		try {
			webServiceTracking.setCustomerId(Integer.parseInt(custId));
		}catch(Exception e) {
		}
		webServiceTracking.setContestId(contestIdStr);
		webServiceTracking.setActionResult(actionResult);
		
		//getContestDetailsByAPI(request, webServiceTracking, actionType);
		DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
	}catch(Exception e){
		e.printStackTrace();
		System.err.println("ERROR WHILE TRACKING API");
	} 
}


public static void contestCreditTracking(HttpServletRequest request,WebServiceActionType actionType,String actionResult){
	WebServiceTracking webServiceTracking = new WebServiceTracking();
	try{
		HttpSession session = request.getSession();
		String customerIpAddress = "";
		webServiceTracking.setProductType(ProductType.REWARDTHEFAN);
		webServiceTracking.setWebXSign("");
		webServiceTracking.setPlatForm(ApplicationPlatForm.API);
		webServiceTracking.setSessionId("");
		webServiceTracking.setWebConfigId("");
		webServiceTracking.setServerIpAddress(session.getAttribute("ip").toString());
		webServiceTracking.setCustomerIpAddress(customerIpAddress);
		webServiceTracking.setHitDate(new Date());
		webServiceTracking.setActionResult(actionResult);
		webServiceTracking.setActionType(actionType);
		webServiceTracking.setAuthenticatedHit("Yes");
		Integer customerId = null,categoryTicketGroupId=null,orderId=null,eventId=null;
		webServiceTracking.setCategoryTicketGroupId(categoryTicketGroupId);
		webServiceTracking.setEventId(eventId);
		webServiceTracking.setCustomerId(customerId);
		webServiceTracking.setOrderId(orderId);
		webServiceTracking.setContestId(request.getParameter("contestId"));
		DAORegistry.getWebServiceTrackingDAO().save(webServiceTracking);
	}catch(Exception e){
		e.printStackTrace();
		System.err.println("ERROR WHILE TRACKING API");
	}
}
}
