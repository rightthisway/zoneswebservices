package com.zonesws.webservices.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.io.Files;
import com.rtfquiz.webservices.aws.AWSFileService;
import com.rtftrivia.ws.controller.FilesController;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.OrderPaymentBreakup;
import com.zonesws.webservices.data.RtfConfigContestClusterNodes;
import com.zonesws.webservices.data.WebServiceConfig;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ExploreType;
import com.zonesws.webservices.enums.FilesType;
import com.zonesws.webservices.filter.SecurityUtil;
import com.zonesws.webservices.jobs.ApiConfigUtil;
import com.zonesws.webservices.jobs.ShareDriveUtil;

import jcifs.smb.SmbFile;



public class URLUtil implements InitializingBean{
	
	private static ZonesProperty properties;
	 private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	 public ZonesProperty getProperties() {
		return properties;
	 }

	 public void setProperties(ZonesProperty zonesProperties) {
		properties = zonesProperties;
	 }
	 
	 public static List<RtfConfigContestClusterNodes> rtfContestClusterNodesList;// = new ArrayList<RtfConfigContestClusterNodes>();
		
		/*static {
			rtfContestClusterNodesList.add(new RtfConfigContestClusterNodes(1,"http://10.0.1.236:8080/rtfcontest/",1));
			rtfContestClusterNodesList.add(new RtfConfigContestClusterNodes(1,"http://10.0.1.236:8081/rtfcontest/",1));
			rtfContestClusterNodesList.add(new RtfConfigContestClusterNodes(1,"http://10.0.1.236:8082/rtfcontest/",1));
		}*/
		
	
	public static String basePath = "";
	public static String emailImageBasePath = "";
	public static String eventShareLinkBaseUrl = "";
	public static String apiServerSvgMapsBaseURL = "";
	public static String DP_PRFEIX_CODE = "";
	public static String DP_FOLDER_NAME = "";
	public static String rewardTheFanServerSvgMapsBaseUrl;
	public static String apiServerBaseUrl;
	public static String rewardTheFanServerBaseUrl;
	public static String paypalEnvironment;
	public static String DEFAULT_DP_PRFEIX_CODE = "";
	public static String DEFAULT_DP_FOLDER_NAME = "";
	public static String fromEmail;
	public static String bccEmails;
	public static String fromEmailForPromo;
	public static String rtfImageSharedPath;	
	public static String rtfSharedDriveUserName;
	public static String rtfSharedDrivePassword;
	public static Boolean isProductionEnvironment;
	public static Boolean isMasterNodeServer;
	public static Boolean isSharedDriveOn;
	public static String svgTextFilePath;
	public static String imageServerURL;
	public static String custOrderAssetBasePath;
	public static String imageServerBaseURL;
	
	public static String gcImageTffSrcPath;
	public static String gcFileTffSrcPath;
	
	public static String rtfzonesJdbcUrl;
	public static String rtfzonesUsername;
	public static String rtfzonesPassword;
	
	public static String rtfQuizJdbcUrl;
	public static String rtfQuizUsername;
	public static String rtfQuizPassword;
	
	public static String rtfEcommJdbcUrl;
	public static String rtfEcommUsername;
	public static String rtfEcommPassword;
	
	public static String quizApiLinkedServer;
	public static String zonesApiLinkedServer;
	
	public void afterPropertiesSet() throws Exception {
		try{
			rewardTheFanServerSvgMapsBaseUrl = properties.getApiServerSvgMapsBaseURL();
			basePath = properties.getServerWebAppBasePath();			
			emailImageBasePath = "/rtw/upstream1/apache-tomcat-7.0.92/webapps/ROOT/resources/email-resources//";
			eventShareLinkBaseUrl = properties.getRtfDesktopSiteUrl();
			fromEmail = properties.getEmailFrom();
			bccEmails = properties.getEmailBcc();
			fromEmailForPromo = properties.getEmailFromForPromo();
			apiServerSvgMapsBaseURL = properties.getApiServerSvgMapsBaseURL();
			apiServerBaseUrl = properties.getApiServerBaseUrl();
			DP_PRFEIX_CODE = properties.getRtfCustomerDPPrefix();
			DP_FOLDER_NAME = properties.getRtfCustomerDPFolder();
			DEFAULT_DP_PRFEIX_CODE = properties.getRtfDefaultDPPrefix();
			DEFAULT_DP_FOLDER_NAME = properties.getRtfDefaultDPFolder();
			paypalEnvironment = properties.getPaypalEnvironment();
			isProductionEnvironment = properties.getIsProductionEnvironment();
			rtfImageSharedPath = properties.getRtfImageSharedPath();
			rtfSharedDriveUserName = properties.getRtfSharedDriveUserName();
			rtfSharedDrivePassword = properties.getRtfSharedDrivePassword();
			rewardTheFanServerBaseUrl = "https://www.rewardthefan.com/";
			imageServerURL = properties.getImageServerURL();
			imageServerBaseURL = properties.getImageServerBaseURL();
			custOrderAssetBasePath = properties.getDownloadableFileLoc();
			
			gcImageTffSrcPath = properties.getGcImageTffSrcPath();
			gcFileTffSrcPath = properties.getGcFileTffSrcPath();
			
			rtfzonesJdbcUrl = properties.getRtfzonesJdbcUrl();
			rtfzonesUsername = properties.getRtfzonesUsername();
			rtfzonesPassword = properties.getRtfzonesPassword();
			
			rtfQuizJdbcUrl = properties.getRtfQuizJdbcUrl();
			rtfQuizUsername = properties.getRtfQuizUsername();
			rtfQuizPassword = properties.getRtfQuizPassword();
			
			rtfEcommJdbcUrl = properties.getRtfEcommJdbcUrl();
			rtfEcommUsername = properties.getRtfEcommUsername();
			rtfEcommPassword = properties.getRtfEcommPassword();
			
			quizApiLinkedServer = properties.getQuizApiLinkedServer();
			zonesApiLinkedServer = properties.getZonesApiLinkedServer();
			
						
			String masterIp = properties.getMasterNodeServerIP();
			String localIP = InetAddress.getLocalHost().getHostAddress();
			if(masterIp != null && masterIp.equals(localIP)) {
				isMasterNodeServer = true;
			}else {
				isMasterNodeServer = false;
			}
			
			isSharedDriveOn = properties.getIsSharedDriveOn();
			svgTextFilePath = properties.getSvgTextFilePath();
			
			System.out.println("URLUTIL : masterIp : "+masterIp+" :localIP: "+localIP+" :isMasterNodeServer: "+isMasterNodeServer+" : "+new Date());
			
			System.out.println("imageServerURL========>"+imageServerURL);
			System.out.println("imageServerBaseURL========>"+imageServerBaseURL);
			System.out.println("custOrderAssetBasePath========>"+custOrderAssetBasePath);
			System.out.println("gcImageTffSrcPath========>"+gcImageTffSrcPath);
			System.out.println("gcFileTffSrcPath========>"+gcFileTffSrcPath);
			System.out.println("basePath==========>"+basePath);
			System.out.println("eventShareLinkBaseUrl==========>"+eventShareLinkBaseUrl);
			System.out.println("apiServerBaseURL==========>"+apiServerSvgMapsBaseURL);
			System.out.println("DP_PRFEIX_CODE==========>"+DP_PRFEIX_CODE);
			System.out.println("DP_FOLDER_NAME==========>"+DP_FOLDER_NAME);
			System.out.println("DEFAULT_DP_PRFEIX_CODE==========>"+DEFAULT_DP_PRFEIX_CODE);
			System.out.println("DEFAULT_DP_FOLDER_NAME==========>"+DEFAULT_DP_FOLDER_NAME);
			System.out.println("FROM EMAIL==========>"+fromEmail);
			System.out.println("BCC EMAIL==========>"+bccEmails);
			System.out.println("From Promo EMAIL==========>"+fromEmailForPromo);
			System.out.println("IS Production ==========>"+isProductionEnvironment);
			System.out.println("IS MASTER NODE IP ==========>"+isMasterNodeServer);
			System.out.println("IS SHARED DRIVE ON ==========>"+isSharedDriveOn);
			System.out.println("custOrderAssetBasePath ==========>"+custOrderAssetBasePath);

			
			System.out.println("RTF Zones JdbcUrl ==========>"+rtfzonesJdbcUrl);
			System.out.println("RTF Zones UserName ==========>"+rtfzonesUsername);
			System.out.println("RTF Zones Password ==========>"+rtfzonesPassword);
			
			System.out.println("RTF Quiz JdbcUrl ==========>"+rtfQuizJdbcUrl);
			System.out.println("RTF Quiz UserName ==========>"+rtfQuizUsername);
			System.out.println("RTF Quiz Password ==========>"+rtfQuizPassword);
			
			System.out.println("RTF Ecomm JdbcUrl ==========>"+rtfEcommJdbcUrl);
			System.out.println("RTF Ecomm UserName ==========>"+rtfEcommUsername);
			System.out.println("RTF Ecomm Password ==========>"+rtfEcommPassword);
			
			rtfContestClusterNodesList = DAORegistry.getRtfConfigContestClusterNodesDAO().getAllActiveRtfConfigContestClusterNodes();
			
		}catch(Exception e){
			basePath = "////C://Tomcat 7.0_Tomcat7.production//webapps//";
			emailImageBasePath = "////C://Tomcat 7.0_Tomcat7.production//webapps//ROOT//resources//email-resources//";
			eventShareLinkBaseUrl = "https://www.rewardthefan.com/";
			apiServerSvgMapsBaseURL = "https://api.rewardthefan.com/SvgMaps/";
			DP_PRFEIX_CODE = "REWARDTHEFAN_DP";
			DP_FOLDER_NAME = "CUSTOMER_DP";
			DEFAULT_DP_PRFEIX_CODE = "RTF_DEFAULT_DP";
			DEFAULT_DP_FOLDER_NAME = "DEFAULT_DP";
			rtfImageSharedPath = "smb://10.0.0.91//d$//Rewardthefan//";
			rtfSharedDriveUserName = "dev";
			rtfSharedDrivePassword = "Rb#32H8!";
			isProductionEnvironment = true;
			isMasterNodeServer = false;
			isSharedDriveOn = false;
			svgTextFilePath = "//rtw//SVG//Texts//";
			e.printStackTrace();
		}
	}
	
	public static String getStoredSvgApiUrl(){
		return apiServerSvgMapsBaseURL+"Stored_SVG/";
	}
	
	public static String getSVGTextFolderPath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"Texts//";
		}else {
			return "/rtw/SVG//Texts/";
		}
	}
	
	public static String getSVGMapPath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//";
		}else {
			return basePath+"SvgMaps//";
		}
	}
	
	public static String getGiftCardImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//GiftCards//";
		}else {
			return basePath+"SvgMaps//GiftCards//";
		}
	}
	
	public static String getProductOrderImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//productorder//";
		}else {
			return basePath+"SvgMaps//productorder//";
		}
	}
	
	public static String getStoredSVGMapPath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//Stored_SVG//";
		}else {
			return basePath+"SvgMaps//Stored_SVG//";
		}
	}
	
	public static String getCardsImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//Cards//";
		}else {
			return basePath+"SvgMaps//Cards//";
		}
	}
	
	public static String getExploreCardsImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//Cards//Explore//";
		}else {
			return basePath+"SvgMaps//Cards//Explore//";
		}
	}
	
	public static String getPaymentIconImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//PaymentIcons//";
		}else {
			return basePath+"SvgMaps//PaymentIcons//";
		}
	} 
	
	public static String getRegularImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//";
		}else {
			return basePath+"SvgMaps//";
		}
	}
	
	public static String getCustomerProfileImagePathOld(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//"+DP_FOLDER_NAME+"//";
		}else {
			return basePath+"SvgMaps//"+DP_FOLDER_NAME+"//";
		}
	}
	 
	public static String getPromotionalDialogImagePath(){
		if(URLUtil.isSharedDriveOn) {
			return rtfImageSharedPath+"SvgMaps//PromotionalDialog//";
		}else {
			return basePath+"SvgMaps//PromotionalDialog//";
		}
	}
	
	public static String getOrderSvgUrl(String fileName){
		return apiServerBaseUrl+FilesController.svgMapBasePath+"?ft="+FilesType.ordermap+"&fn="+fileName;
	}
	
	public static String getRegularSvgUrl(String fileName){
		return apiServerBaseUrl+FilesController.svgMapBasePath+"?ft="+FilesType.eventmap+"&fn="+fileName;
	}
	
	public static String getCardsImageUrl(String fileName){
		return apiServerBaseUrl+FilesController.svgMapBasePath+"?ft="+FilesType.regularcard+"&fn="+fileName;
	}
	
	public static String getExploreCardsImageUrl(String fileName){
		return apiServerBaseUrl+FilesController.svgMapBasePath+"?ft="+FilesType.explorecard+"&fn="+fileName;
	}
	
	public static String getRegularImageUrl(String fileName){
		return apiServerBaseUrl+FilesController.svgMapBasePath+"?ft="+FilesType.regularimages+"&fn="+fileName;
	}
	
	public static String getCustomerProfilePicUrl(String fileName){
		//http://52.201.48.95:90/staticfiles/REWARDTHEFAN_DP_100524.jpg
		return "http://52.201.48.95:90/staticfiles/"+fileName;
		//return apiServerBaseUrl+FilesController.profileImagePath+"?ft="+FilesType.profilepic+"&fn="+fileName;
	}
	
	public static String getPaymentIconImageUrl(String fileName){
		return apiServerBaseUrl+FilesController.svgMapBasePath+"?ft="+FilesType.paymenticon+"&fn="+fileName;
	}
	
	public static String getPromotionalDialogImageUrl(String fileName){
		return apiServerBaseUrl+FilesController.svgMapBasePath+"?ft="+FilesType.promoofferimages+"&fn="+fileName;
	}
	
	
	public static String getETicketDownloadURL(String orderNo){
		String fileLocation = "";
		
		try{
			fileLocation = URLUtil.basePath+"SvgMaps//Tickets//Ticket_"+orderNo+".pdf";
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getLogoImage(){
		String logoImage = "";
		
		try{
			logoImage = URLUtil.basePath+"SvgMaps//logo.png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String getWhiteLogoImage(){
		String logoImage = "";
		
		try{
		logoImage = URLUtil.basePath+"SvgMaps//logo_white.png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String getRTFLIVELogoImage(){
		String logoImage = "";
		
		try{
		logoImage = URLUtil.basePath+"SvgMaps//rtflive.PNG";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String fetchIcons(String iconName){
		String logoImage = "";
		try{
		logoImage = URLUtil.basePath+"SvgMaps//"+iconName+".png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String fetchResetPasswordIcons(String iconName){
		String logoImage = "";
		try{
		logoImage = URLUtil.emailImageBasePath+iconName+".png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String fetchTriviaIcons(String iconName){
		String logoImage = "";
		try{
		logoImage = URLUtil.basePath+"SvgMaps//trivia//"+iconName+".png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String findETicketDownloadURLNotUsing(String orderNo){
		String fileLocation = "";
		try{
			File ticketsFolder = new File (URLUtil.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(orderNo)){
							break;
						}
					}
				}
				fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+fileName;
				return fileLocation;
			}
			fileLocation = URLUtil.basePath+"SvgMaps//Tickets//Ticket_"+orderNo+".pdf";
		}catch(Exception e){
			fileLocation = URLUtil.basePath+"SvgMaps//Tickets//Ticket_"+orderNo+".pdf";
			e.printStackTrace();
		}
		return fileLocation;
	}
	 
		
	public static String findETicketDownloadURLFromDirectoryOld(String directoryFileName){
		String fileLocation = "";
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			File ticketsFolder = new File (URLUtil.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				boolean isfileFound = false;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(directoryFileName)){
							isfileFound = true;
							break;
						}
					}
				}
				if(isfileFound){
					fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+fileName;
				}
				return fileLocation;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String findETicketDownloadURLFromDirectory(String directoryFileName){
		String filePath = "";
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			if(URLUtil.isSharedDriveOn) {
				filePath = URLUtil.rtfImageSharedPath+"SvgMaps//Tickets//"+directoryFileName;
			}else {
				filePath = URLUtil.basePath+"SvgMaps//Tickets//"+directoryFileName;
			}
			
			return filePath;
		}catch(Exception e){
			e.printStackTrace();
		}
		return filePath;
	}
	
	public static String getFileNameForTriviaTickets(String directoryFileName){
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			return directoryFileName;
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	/*public static String findETicketDownloadURLFromDirectory(String directoryFileName){
		String filePath = "";
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			filePath = URLUtil.rtfImageSharedPath+"SvgMaps//Tickets//"+directoryFileName;
			return filePath;
		}catch(Exception e){
			e.printStackTrace();
		}
		return filePath;
	}*/
	
	public static String findETicketDownloadURLFromDirectoryNewOld(String directoryFileName){
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			//File ticketsFolder = new File (URLUtil.basePath+"SvgMaps//Tickets//"+directoryFileName);
			return directoryFileName;
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	public static String findETicketDownloadURL(String orderNo,String ticketAttachedId){
		String fileLocation = "";
		try{
			File ticketsFolder = new File (URLUtil.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(orderNo+"_"+ticketAttachedId)){
							break;
						}
					}
				}
				fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+fileName;
				return fileLocation;
			}
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
		}catch(Exception e){
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
			e.printStackTrace();
		}
		return fileLocation;
	}
	
public static String findETicketDownloadURL(String orderNo,String ticketAttachedId,CustomerOrder order){
		
		String fileLocation = "";
		String formattedEventDate = dateFormat.format(order.getEventDateTemp());
		String eventTimeLocal="TBD";
		if(null != order.getEventTime() ){
			eventTimeLocal = timeFormat.format(order.getEventTime());
		}
		String fullFileName = order.getEventName()+"_"+formattedEventDate+"_"+eventTimeLocal+"_"+order.getSection()+order.getActualRow()+"_eticket_";
		try{
			File ticketsFolder = new File (URLUtil.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(fullFileName+"_"+ticketAttachedId)){
							break;
						}
					}
				}
				fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+fileName;
				return fileLocation;
			}
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
		}catch(Exception e){
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getShareLinkURL(Event event){
		String cardImage = "";
		try{
			cardImage = "http://zonetickets.com/event/" + event.getEventId()+ "";
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	 
	
	public static String profilePicUploadDirectory(String profilePicPrefix,Integer customerId,String ext){
		String fileLocation = "";
		try{
			fileLocation = getSVGMapPath()+DP_FOLDER_NAME+"//" + profilePicPrefix+"_"+customerId+"."+ext;
		}catch(Exception e){
			e.printStackTrace(); 
		} 
		return fileLocation;
	}
	
	public static String profilePicBaseDirectory(){
		String fileLocation = "";
		try{
			fileLocation = getSVGMapPath()+DP_FOLDER_NAME+"//";
		}catch(Exception e){
			e.printStackTrace(); 
		} 
		return fileLocation;
	}
	
	public static String profilePicWebURL(String profilePicPrefix,Integer customerId,String ext, ApplicationPlatForm platform){
		String fileUrl = AWSFileService.awsS3Url+AWSFileService.CUSTOMER_DP_DIRECTORY+"/"+customerId+"."+ext;
		return fileUrl;
	}
	
	 
	public static String profilePicWebURByImageName(String fileName, ApplicationPlatForm platform){
		String fileUrl = AWSFileService.awsS3Url+AWSFileService.CUSTOMER_DP_DIRECTORY+"/"+fileName;
		return fileUrl;
	}

	public static String profilePicWebURByImageName(String fileName){
		String fileUrl = AWSFileService.awsS3Url+AWSFileService.CUSTOMER_DP_DIRECTORY+"/"+fileName;
		return fileUrl;
	}
	
	public static String getProfilePicFile(String custImagePath){
		String fileLocation = "";
		try{
			fileLocation = getSVGMapPath()+DP_FOLDER_NAME+"//" + custImagePath;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getProfilePicFolderPathNotUsing(){
		return URLUtil.basePath+"SvgMaps//"+DP_FOLDER_NAME+"//";
	}
	
	public static String getDefaultProfilePicFile(String defaultImagePath){
		String fileLocation = "";
		try{
			fileLocation = URLUtil.getSVGMapPath()+DEFAULT_DP_FOLDER_NAME+"//" + defaultImagePath+".jpg";
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	public static String getCustomerDefaultprofilePicNameOld(Integer customerId){
		
		if(URLUtil.isSharedDriveOn) {
			String custDPFileName="";
			try{
				SmbFile defaultFile = null;
				Random random = new Random();
				while(true) {
					int number = random.nextInt(20);
					String fileName = getDefaultProfilePicFile(DEFAULT_DP_PRFEIX_CODE+"_"+number);
					
					defaultFile = ShareDriveUtil.getSmbFile(fileName);
					
					//defaultFile = new File(fileName);
					if(defaultFile != null && defaultFile.exists()) {
						break;
					}
				}
				String fileExt = defaultFile.getName();
				String ext = FilenameUtils.getExtension(fileExt);
				
				String uploadDir = URLUtil.profilePicUploadDirectory(DP_PRFEIX_CODE, customerId, ext);
				SmbFile newFile = ShareDriveUtil.getSmbFile(uploadDir);
				
				System.out.println("SourceDirectory: "+defaultFile.getCanonicalPath()+", DestinationDirectory: "+uploadDir);
				System.out.println("SourceFile File Path: "+defaultFile);
				System.out.println("DestinationFile File Path: "+newFile);
				
				custDPFileName = newFile.getName();
				
				defaultFile.copyTo(newFile);
				
				//FileUtils.copyFile(defaultFile,newFile);
				 
			}catch(Exception e){
				e.printStackTrace();
			}
			return custDPFileName;
		}else {
			String custDPFileName="";
			try{
				File defaultFile = null;
				Random random = new Random();
				while(true) {
					int number = random.nextInt(20);
					String fileName = getDefaultProfilePicFile(DEFAULT_DP_PRFEIX_CODE+"_"+number);
					
					defaultFile = new File(fileName);
					if(defaultFile != null && defaultFile.exists()) {
						break;
					}
				}
				String fileExt = defaultFile.getAbsolutePath();
				String ext = FilenameUtils.getExtension(fileExt);
				String uploadDir = URLUtil.profilePicUploadDirectory(DP_PRFEIX_CODE, customerId, ext);
				File newFile = new File(uploadDir);
				custDPFileName = newFile.getName();
				FileUtils.copyFile(defaultFile,newFile);
				
				System.out.println("ULAGA: SourceDirectory: "+defaultFile.getCanonicalPath()+", DestinationDirectory: "+uploadDir+", CUSTID:"+customerId);
				System.out.println("ULAGA: SourceFile File Path: "+defaultFile+", CUSTID:"+customerId);
				System.out.println("ULAGA: DestinationFile File Path: "+newFile+", CUSTID:"+customerId);
				
				/*InputStream in = new FileInputStream(fileName);
	            OutputStream out = new FileOutputStream(uploadDir);

	            // Copy the bits from instream to outstream
	            byte[] buf = new byte[1024];
	            int len;
	            while ((len = in.read(buf)) > 0) {
	                out.write(buf, 0, len);
	            }
	            in.close();
	            out.close();*/
				 
			}catch(Exception e){
				e.printStackTrace();
			}
			return custDPFileName;
		}
		
	}
	
	
	public static String getCustomerDefaultprofilePicName(Integer customerId){
		String custDPFileName="";
		try{
			Random random = new Random();
			while(true) {
				int number = random.nextInt(20);
				if(number >=0 && number<= 19) {
					custDPFileName =  "DEFAULT_DP_"+number+".jpg";
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return custDPFileName;
	}

	
	public static void main(String[] args)
    {	
    	try{
    	   String ext = Files.getFileExtension("C:\\Tomcat 7.0\\webapps\\SvgMaps\\RTF_DEFAULT_DP\\DEFAULT_DP_0.tff");
    	   System.out.println(ext);
    	    
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
	
	public static String getResetPasswordLink(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"reset";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	
	public static String getReferFriendsLink(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"ReferFriends";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	
	public static String getFantasyTicketLink(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"GetSuperFanLeagues";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	public static String updateShippingAddress(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"ShippingAddress";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	public static String getRewardPoints(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"CustomerRewardPoints";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	
	public static String getExploreCardImages(ExploreType exploreType){
		String cardImage = "";
		try{
			cardImage = "http://52.22.87.62/SvgMaps/Cards/" + exploreType + ".jpg";
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	
	
	/*public static String getShareLinkURL(Event event,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "";
		String encryptionKey = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
	    if(customer!=null){
			String token =  "RTFCUSTOMERFOUND_"+event.getEventId()+"_"+customer.getId();
			String encryptedText = SecurityUtil.doEncryption(token, encryptionKey);
			String custEncryptedText = SecurityUtil.doEncryption(String.valueOf(customer.getId()), encryptionKey);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText+"&cId="+custEncryptedText;
	    }else{
	    	String token =  "RTFCUSTOMERNOTFOUND_"+ event.getEventId();
			String encryptedText = SecurityUtil.doEncryption(token, encryptionKey);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText;
	    }
	    event.setEncryptionKey(encryptionKey);
	    return shareLinkUrl;
	}*/
	
	/*public static Event getShareLinkURL(Event event,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "",desktopShareLinkUrl="";
		String encryptionKey = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
	    if(customer!=null){
			String token =  event.getEventId().toString();
			String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText;
			String cIdEncrypted = SecurityUtil.doBase64Encryption(String.valueOf(customer.getId()));
			desktopShareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText+"&cId="+cIdEncrypted;
	    }else{
	    	String token =  event.getEventId().toString();
	    	String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText;
			desktopShareLinkUrl = shareLinkUrl;
	    }
	    event.setDesktopShareLinkUrl(desktopShareLinkUrl);
	    event.setShareLinkUrl(shareLinkUrl);
	    event.setEncryptionKey(encryptionKey);
	    return event;
	}*/
	
	public static Event getShareLinkURL(Event event,Customer customer,ApplicationPlatForm platForm) throws Exception{
		String shareLinkUrl = "",desktopShareLinkUrl="", referralCode ="";
	    if(customer!=null){
	    	shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&DiscountCode="+customer.getReferrerCode()+"&s="+String.valueOf(platForm);
			desktopShareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&DiscountCode="+customer.getReferrerCode()+"&s="+String.valueOf(platForm);
			event.setShareTitle("Use this discount code "+customer.getReferrerCode()+" for 5% off on your first order");
			referralCode = customer.getReferrerCode();
	    }else{
	    	shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&s="+String.valueOf(platForm);
			desktopShareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&s="+String.valueOf(platForm);
			event.setShareTitle("Use this discount code for 5% off on your first order");
	    }
	    event.setShareMessage("Check out this new ticket site, Great rewards program, each point is worth $1!" +
		"Use this discount code "+referralCode+" for 5% off on your first order.");
	    event.setShareLinkUrl(shareLinkUrl);
	    
	    event.setDesktopShareLinkUrl(desktopShareLinkUrl);
	    event.setDesktopShareMessage("Check out this new ticket site, Great rewards program, each point is worth $1!.");
	    event.setShareTitle("Use this discount code "+referralCode+" for 5% off on your first order");
	    return event;
	}
	
	
	/*public static String getDesktopShareLinkURLdfdf(Integer eventId,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "";
	    if(customer!=null){
			String token =  eventId.toString();
			String encryptedText = SecurityUtil.doBase64Encryption(token);
			String cIdEncrypted = SecurityUtil.doBase64Encryption(String.valueOf(customer.getId()));
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText+"&cId="+cIdEncrypted;
	    }else{
	    	String token =  eventId.toString();
	    	String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText;
	    }
	    return shareLinkUrl;
	}
	
	public static String getShareLinkURLdfdf(Integer eventId,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "";
	    if(customer!=null){
			String token =  eventId.toString();
			String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText;
	    }else{
	    	String token =  eventId.toString();
	    	String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText;
	    }
	    return shareLinkUrl;
	}
	*/
	
	/**
	 * Method to get the encrypted order download link url
	 * @param request
	 * @param configId
	 * @param customerId
	 * @param orderNo
	 * @return
	 */
	 
	public static String getETicketDownloadUrl(HttpServletRequest request, CustomerOrder order, String configId, 
			Integer customerId,Invoice invoice,Integer attachementId,ApplicationPlatForm platForm,String ip){
		String orderDownloadLink = "";
		try { 
			String serverPort = Integer.toString(request.getServerPort());
			String appContextPath = null;
			String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
			String token = "";
			String encryptedText = null;
			String clientIPAddress = request.getParameter("clientIPAddress");
			if(null == clientIPAddress || clientIPAddress.isEmpty()){
				clientIPAddress = ip;
			}
			if(serverPort != null && !serverPort.isEmpty()){
				appContextPath = URLUtil.apiServerBaseUrl+"/DownloadOrderTickets?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}else{
				appContextPath = URLUtil.apiServerBaseUrl+"/DownloadOrderTickets?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getZipTicketsDownloadUrlNotUsing(HttpServletRequest request, CustomerOrder order, String configId, 
			Integer customerId,Invoice invoice,ApplicationPlatForm platForm,String ip){
		String orderDownloadLink = "";
		try {
			String serverPort = Integer.toString(request.getServerPort());
			String appContextPath = null;
			String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
			String token = "";
			String encryptedText = null;
			String clientIPAddress = request.getParameter("clientIPAddress");
			if(null == clientIPAddress || clientIPAddress.isEmpty()){
				clientIPAddress = ip;
			}
			if(serverPort != null && !serverPort.isEmpty()){
				appContextPath = URLUtil.apiServerBaseUrl+"/DownloadRTFTickets?";
				token =  configId+"_"+customerId+"_"+invoice.getCustomerOrderId();
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}else{
				appContextPath = URLUtil.apiServerBaseUrl+"/DownloadRTFTickets?";
				token =  configId+"_"+customerId+"_"+invoice.getCustomerOrderId();
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
/*public static String getETicketDownloadUrl(HttpServletRequest request, CustomerOrder order, String configId, 
			Integer customerId,Invoice invoice,Integer attachementId){
		String orderDownloadLink = "";
		try {
			String serverPort = Integer.toString(request.getServerPort());
			String appContextPath = null;
			String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
			String token = "";
			String encryptedText = null;
			if(serverPort != null && !serverPort.isEmpty()){
				appContextPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/DownloadRTFOrders?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId;
			}else{
				appContextPath = request.getScheme()+"://"+request.getServerName()+request.getContextPath()+"/DownloadRTFOrders?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId;
			}
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/

	public static String getFedExTrackingURL(Invoice invoice){
		String orderDownloadLink = "";
		try {
			String fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
			String fedExUrl = "https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber="+fedExTrackingNo;
			orderDownloadLink = fedExUrl;
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//unit test 
	/*public static void main(String[] args){
		Customer cust = new Customer();
		CustomerOrder order = new CustomerOrder();
		
		cust.setId(1);
		int id = cust.getId();
		order.setId(65);
		int oid = order.getId();
		
		getRTFOrderDownloadUrl("RewardTheFan",id, oid);
	}*/
	
	/*public static void main(String[] args) {
		
		Event event = new Event();
		Customer customer = new Customer();
		
		
		event.setEventId(3456567);
		customer.setId(12);
		customer.setReferrerCode("RTF0001");
		
		try {
			String key = getShareLinkURL(event, customer);
			System.out.println(key);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		/**
		 * Method to read the getGrandChildCategoryImage from specified location
		 * @return
		 */
		public static String getGrandChildCategoryImageNotUsing(String grandChildCategory){
			File grandChildCategoryImgFolder = null;
			String grandChildCategoryImg = null;
			boolean fileExists = false;
			try {
				grandChildCategoryImgFolder = new File(basePath+"SvgMaps//GrandChildCategoryImage//");
				File[] listOfFiles = grandChildCategoryImgFolder.listFiles();
				
				if(listOfFiles!=null){
					String grandChildCategoryImgName = null;
					for (int i = 0; i < listOfFiles.length;i++) {
						if (listOfFiles[i].isFile()) {
							grandChildCategoryImgName = listOfFiles[i].getName();
							
							fileExists = grandChildCategoryImgName.equals(grandChildCategory);
							if(fileExists == true){
								grandChildCategoryImg = "http://52.22.87.62/SvgMaps/GrandChildCategoryImage/"+grandChildCategoryImgName;
							}
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return grandChildCategoryImg;
		}
		
		/**
		 * Method to read the getChildCategoryImage from specified location
		 * @return
		 */
		
		public static String getChildCategoryImagedNotUsing(String childCategory){
			File grandChildCategoryImgFolder = null;
			String grandChildCategoryImg = null;
			boolean fileExists = false;
			try {
				grandChildCategoryImgFolder = new File(basePath+"SvgMaps//ChildCategoryImage//");
				File[] listOfFiles = grandChildCategoryImgFolder.listFiles();
				
				if(listOfFiles!=null){
					String grandChildCategoryImgName = null;
					for (int i = 0; i < listOfFiles.length;i++) {
						if (listOfFiles[i].isFile()) {
							grandChildCategoryImgName = listOfFiles[i].getName();
							
							fileExists = grandChildCategoryImgName.equals(childCategory);
							if(fileExists == true){
								grandChildCategoryImg = "http://52.22.87.62/SvgMaps/ChildCategoryImage/"+grandChildCategoryImgName;
							}
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return grandChildCategoryImg;
		}
		
		/**
		 * Method to get the cust referrer code 
		 * @param configId
		 * @param eventRefIdentity
		 * @return
		 */
		public static String getEncryptedRefererCode(String configId,String eventRefIdentity){
			try {
				//String keyString = "QWE4#4PO==+ABCDE";
				String decodedRefCode = null;
				if(eventRefIdentity != null){
					WebServiceConfig webServiceConfig = ApiConfigUtil.getWebServiceConfigById(configId);
					
					decodedRefCode = SecurityUtil.doDecryption(eventRefIdentity, webServiceConfig.getSecureKey());
					
					String origText[] = decodedRefCode.split("_");
					decodedRefCode = origText[2];
					System.out.println("cust Id ::"+origText[0]+"event Id ::"+origText[1]+"ref code ::"+origText[2]);
				}	
				return decodedRefCode;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		
		/**
		 * Method to get the cust referrer code 
		 * @param configId
		 * @param eventRefIdentity
		 * @return
		 */
		/*public static String getEncryptedRefererCode(String eventRefIdentity,String cId){
			try {
				String decodedRefCode = null;
				
				String token =  "RTFCUSTOMERFOUND_"+event.getEventId();
				String token =  "RTFCUSTOMERNOTFOUND_"+ event.getEventId();
					
				decodedRefCode = SecurityUtil.doBase64Decryption(eventRefIdentity);
				
				String origText[] = decodedRefCode.split("_");
				
				if(decodedRefCode.contains("RTFCUSTOMERFOUND_")){
					
					String eventId = origText[1];
					String custId = origText[2];
				}else{

					String eventId = origText[1];
					
				}
				
				decodedRefCode = origText[2];
				System.out.println("cust Id ::"+origText[0]+"event Id ::"+origText[1]+"ref code ::"+origText[2]);
				return decodedRefCode;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}*/
	
		//test
		public static void mainOL(String[] args){

		String number = "374822155519874";
		String pattern = "^4";
			
	      Pattern r = Pattern.compile(pattern);
	      Matcher m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("VISA Found value: " + m.group(0) );
	       } else {
	          System.out.println("VISA NO MATCH");
	       }
		      
	      
	      pattern = "^5[1-5]";
	      r = Pattern.compile(pattern);
	      m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("MASTER Found value: " + m.group(0) );
	       } else {
	          System.out.println("MASTER NO MATCH");
	       }
	      
	      pattern = "^3[47]";
	      r = Pattern.compile(pattern);
	      m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("AMEX Found value: " + m.group(0) );
	       } else {
	          System.out.println("AMEX NO MATCH");
	       }
	      
	      pattern = "^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)";
	      r = Pattern.compile(pattern);
	      m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("DISCOVER Found value: " + m.group(0) );
	       } else {
	          System.out.println("DISCOVER NO MATCH");
	       }
		      
		   
		}
		
		public static void maindfdf(String[] args) {
			
			String eventName = "NHL Eastern Conference Quarterfinals: Pittsburgh Penguins vs. TBD � Home Game 3 (Date: TBD � If Necessary)";
			
			String splitName = eventName.replaceAll("�", "-");
			System.out.println(eventName);
			System.out.println(splitName);
		}

		public static String getPromoDialogImage(String imageName){
			return getPromotionalDialogImageUrl(imageName);
		} 
		
		public static OrderPaymentBreakup getPaymentIcon(OrderPaymentBreakup obj){
			
			String iconName ="", description="";
			
			switch (obj.getPaymentMethod()) {
				
				case CREDITCARD:
					iconName ="ic_credit_card.png";
					description="Credit Card";
					break;
					
				case PAYPAL:
					iconName ="ic_paypal.png";
					description="PayPal";
					break;
					
				case PARTIAL_REWARDS:
					iconName ="ic_reward_dollars.png";
					description="Reward Dollars";
					break;
					
				case FULL_REWARDS:
					iconName ="ic_reward_dollars.png";
					description="Reward Dollars";
					break;
					
				case IPAY:
					iconName ="ic_apple.png";
					description="Apple Pay";
					break;
					
				case GOOGLEPAY:
					iconName ="ic_android_pay.png";
					description="Google Pay";
					break;
					
				case CUSTOMER_WALLET:
					iconName ="ic_wallet.png";
					description="Customer Wallet";
					break;
					
				case ACCOUNT_RECIVABLE:
					iconName ="ic_bank.png";
					description="Account Receivable";
					break;
					
				case BANK_OR_CHEQUE:
					iconName ="ic_bank.png";
					description="Bank or Cheque";
					break;
					
				case CONTEST_WINNER:
					iconName ="ic_contest_winner.png";
					description="Contest Winner";
					break; 
	
				default:
					break;
			}
			
			if(URLUtil.isSharedDriveOn) {
				obj.setIconUrl(getPaymentIconImageUrl(iconName));
			}else {
				obj.setIconUrl(URLUtil.apiServerSvgMapsBaseURL+"PaymentIcons/"+iconName);
			}
			obj.setPaymentDescription(description);
			return obj;
		}
		
		public static String getContestPromoCodeRedirectionUrl(String promoRefType,Integer promoRefId,String refTypeName){
			String link = "https://rewardthefan.com";
			
			try {
				refTypeName = URLEncoder.encode(refTypeName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(promoRefType.equals("EVENT")){
				link = "https://www.rewardthefan.com/GetEventDetails?id="+promoRefId;
			}else if(promoRefType.equals("ARTIST")){
				link = "https://www.rewardthefan.com/Search?aId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("VENUE")){
				link = "https://www.rewardthefan.com/Search?vId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("CHILD")){
				link = "https://www.rewardthefan.com/Search?cId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("GRAND") || promoRefType.equals("GRANDCHILD")){
				link = "https://www.rewardthefan.com/Search?gcId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("PARENT")){
				link = "https://www.rewardthefan.com/Search?pId="+promoRefId+"&key="+refTypeName;
			}
			String anchorTag = "<a style='border: 11px solid #f0c14b; background-color: #f0c14b; color: #012e4f; font-size: 20px; text-decoration: none; text-align: " +
					"center;font-weight: bold;' href='"+link+"' target='_blank'> Find Tickets </a>";
			return anchorTag;
		}
		
		 public static Map<String, String> getParameterMap(){
				
				Map<String, String> map = new HashMap<String, String>();
				map.put("configId", "DESKTOPRTFRTW");
				map.put("productType", "REWARDTHEFAN");
				map.put("platForm", "TICK_TRACKER");
				map.put("clientIPAddress","1.1.1.1");
				return map;
			}
		 public static String getObject(Map<String, String> dataMap,String url) throws Exception{
				try{
					/*
		             *  fix for
		             *    Exception in thread "main" javax.net.ssl.SSLHandshakeException:
		             *       sun.security.validator.ValidatorException:
		             *           PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
		             *               unable to find valid certification path to requested target
		             */
		            TrustManager[] trustAllCerts = new TrustManager[]{
		                new X509ExtendedTrustManager() {
		                    @Override
		                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		                        return null;
		                    }

		                    @Override
		                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
		                    }

		                    @Override
		                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
		                    }

		                    @Override
		                    public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket) throws CertificateException {

		                    }

		                    @Override
		                    public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket) throws CertificateException {

		                    }

		                    @Override
		                    public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {

		                    }

		                    @Override
		                    public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {

		                    }

		                }
		            };
					
					SSLContext context = SSLContext.getInstance("TLSv1.2");
			        context.init(null, trustAllCerts, new java.security.SecureRandom());
			        CloseableHttpClient httpClient = HttpClients.custom().setSslcontext(context).build();
					
					//HttpClient client = HttpClientBuilder.create().build();
					HttpPost post = new HttpPost(url);

					// add header
					post.setHeader("x-sign", "b80ABrBWAGpwtsAC/3sLa0qwFdQBS9SIJOkTf8qNt/Q=");
					post.setHeader("x-token", "DESKTOPRTFRTW");
					post.setHeader("x-platform", "DESKTOP_SITE");			
					post.setHeader("Accept","application/json");
					
					List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
					
					for(Map.Entry<String, String> entry : dataMap.entrySet()){
						urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
					}
					
					post.setEntity(new UrlEncodedFormEntity(urlParameters));

					HttpResponse response = httpClient.execute(post);

					BufferedReader responseData = new BufferedReader(
					        new InputStreamReader(response.getEntity().getContent()));		
					
					String result = IOUtils.toString(responseData);
					System.out.println(result);
					return result;

				}catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		 
		 
	 public static String profilePicForSummary(String fileName){
		 String fileUrl = AWSFileService.awsS3Url+AWSFileService.CUSTOMER_DP_DIRECTORY+"/"+fileName;
			return fileUrl;
	}
	 
	 public static String getGiftCardAbsoluteAssetPath(String giftCardAssetPath) {
		 // custOrderAssetBasePath to picked from settingss.xml 
		 
		 if("GCOD".equals(giftCardAssetPath))  {
			 return custOrderAssetBasePath + "GiftCardOrders";
			 
		 }
		 else if("GCMD".equals(giftCardAssetPath))  {
			 return basePath + "SvgMaps//GiftCards";
			 
		 } else if("PGCOD".equals(giftCardAssetPath))  {
			 return custOrderAssetBasePath + "ProdOrders";
			 
		 }
		 else {				 
			 // If Folder path Sent from calling Api same can be used.
			 return giftCardAssetPath; 
		 }
		 
	 }
	 
	 
	 public static String getVideoUploadAbsolutePath() {		
			 return basePath + "SvgMaps//WinnerVideo";	
		 
	 }
	
	 
	 
	
	 public static String getGiftCardImageUrl(String directoryFileName){
		String fileLocation = "";
		try{ 
			//Production
			//directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/GiftCardImage/", ""); 
			
			//Sandbox
			//directoryFileName = directoryFileName.replaceAll("/tff/RewardTheFan/GiftCardImage/", "");
			
			directoryFileName = directoryFileName.replaceAll(gcImageTffSrcPath, "");
			 
			if(URLUtil.isSharedDriveOn) {
				fileLocation = apiServerSvgMapsBaseURL+"GiftCards/"+directoryFileName;
			}else {
				fileLocation = apiServerSvgMapsBaseURL+"GiftCards/"+directoryFileName;
			}   
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
		
		public static String getGiftCardDownloadUrl(HttpServletRequest request, Integer fileAttachmentId, Integer giftCardOrderId, String configId, 
				Integer customerId,ApplicationPlatForm platForm,String ip){
			String orderDownloadLink = "";
			try { 
				String serverPort = Integer.toString(request.getServerPort());
				String appContextPath = null;
				String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
				String token = "";
				String encryptedText = null;
				String clientIPAddress = request.getParameter("clientIPAddress");
				if(null == clientIPAddress || clientIPAddress.isEmpty()){
					clientIPAddress = ip;
				}
				
				clientIPAddress = clientIPAddress.replaceAll("\\s", ""); 
				
				if(serverPort != null && !serverPort.isEmpty()){
					appContextPath = URLUtil.apiServerBaseUrl+"/GetGiftCardFile?";
					token =  configId+"_"+customerId+"_"+fileAttachmentId+"_"+giftCardOrderId;
					encryptedText = SecurityUtil.doEncryption(token, keyString);
					orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
				}else{
					appContextPath = URLUtil.apiServerBaseUrl+"/GetGiftCardFile?";
					token =  configId+"_"+customerId+"_"+fileAttachmentId+"_"+giftCardOrderId;
					encryptedText = SecurityUtil.doEncryption(token, keyString);
					orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
				}
				return orderDownloadLink;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		
		
		public static String getProductDownloadUrl(HttpServletRequest request, Integer fileAttachmentId, Integer giftCardOrderId, String configId, 
				Integer customerId,String platForm,String ip){
			String orderDownloadLink = "";
			try { 
				String serverPort = Integer.toString(request.getServerPort());
				String appContextPath = null;
				String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
				String token = "";
				String encryptedText = null;
				String clientIPAddress = request.getParameter("clientIPAddress");
				if(null == clientIPAddress || clientIPAddress.isEmpty()){
					clientIPAddress = ip;
				}
				
				clientIPAddress = clientIPAddress.replaceAll("\\s", ""); 
				
				if(serverPort != null && !serverPort.isEmpty()){
					appContextPath = URLUtil.apiServerBaseUrl+"/GetProductFile?";
					token =  configId+"_"+customerId+"_"+fileAttachmentId+"_"+giftCardOrderId;
					encryptedText = SecurityUtil.doEncryption(token, keyString);
					orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm+"&cTrack="+clientIPAddress;
				}else{
					appContextPath = URLUtil.apiServerBaseUrl+"/GetProductFile?";
					token =  configId+"_"+customerId+"_"+fileAttachmentId+"_"+giftCardOrderId;
					encryptedText = SecurityUtil.doEncryption(token, keyString);
					orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm+"&cTrack="+clientIPAddress;
				}
				return orderDownloadLink;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		
		
		
		
		public static String getGiftCardFile(String directoryFileName){
			String filePath = "";
			try{
				
				//Production
				//directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/EGiftCards", "");
				
				//Sandbox
				//directoryFileName = directoryFileName.replaceAll("/tff/RewardTheFan/EGiftCards", "");
				
				directoryFileName = directoryFileName.replaceAll(gcFileTffSrcPath, "");
				 
				System.out.println("Order File: "+custOrderAssetBasePath+directoryFileName);
				
				if(URLUtil.isSharedDriveOn) {
					filePath = URLUtil.custOrderAssetBasePath+"GiftCardOrders//"+directoryFileName;
				}else {
					filePath = URLUtil.custOrderAssetBasePath+"GiftCardOrders//"+directoryFileName;
				}
				return filePath;
			}catch(Exception e){
				e.printStackTrace();
			}
			return filePath;
		}
		
		
		public static String getProductFile(String directoryFileName){
			String filePath = "";
			try{
				System.out.println("Order File: "+custOrderAssetBasePath+directoryFileName);
				if(URLUtil.isSharedDriveOn) {
					filePath = URLUtil.custOrderAssetBasePath+"ProdOrders//"+directoryFileName;
				}else {
					filePath = URLUtil.custOrderAssetBasePath+"ProdOrders//"+directoryFileName;
				}
				return filePath;
			}catch(Exception e){
				e.printStackTrace();
			}
			return filePath;
		}
		
		
		
		public static String getGiftCardFileName(String directoryFileName){
			try{
				directoryFileName = directoryFileName.replaceAll(gcFileTffSrcPath, "");
				return directoryFileName;
			}catch(Exception e){
				e.printStackTrace();
			}
			return "YourGiftCard.pdf";
		}
		
		public static String getFileExtension(String directoryFileName)
	    {	
	    	try{
	    	   String ext = Files.getFileExtension(directoryFileName);
	    	   return ext; 
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	return "";
	    }
	
	
}