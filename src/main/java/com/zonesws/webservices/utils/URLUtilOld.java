package com.zonesws.webservices.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.InitializingBean;

import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.Invoice;
import com.zonesws.webservices.data.OrderPaymentBreakup;
import com.zonesws.webservices.data.WebServiceConfig;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ExploreType;
import com.zonesws.webservices.filter.SecurityUtil;
import com.zonesws.webservices.jobs.ApiConfigUtil;



public class URLUtilOld implements InitializingBean{
	
	private static ZonesProperty properties;
	 private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		private static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
	
	 public ZonesProperty getProperties() {
		return properties;
	 }

	 public void setProperties(ZonesProperty zonesProperties) {
		properties = zonesProperties;
	 }
	
	public static String basePath = "";
	public static String eventShareLinkBaseUrl = "";
	public static String apiServerSvgMapsBaseURL = "";
	public static String DP_PRFEIX_CODE = "";
	public static String DP_FOLDER_NAME = "";
	public static String rewardTheFanServerSvgMapsBaseUrl;
	public static String apiServerBaseUrl;
	public static String rewardTheFanServerBaseUrl;
	public static String paypalEnvironment;
	public static String DEFAULT_DP_PRFEIX_CODE = "";
	public static String DEFAULT_DP_FOLDER_NAME = "";
	public static String fromEmail;
	public static String bccEmails;
	public static String fromEmailForPromo;
	public static Boolean isProductionEnvironment;
	public static String rtfImageSharedPath;
	
	public static String rtfSharedDriveUserName;
	public static String rtfSharedDrivePassword;
	
	public void afterPropertiesSet() throws Exception {
		try{
			rewardTheFanServerSvgMapsBaseUrl = properties.getApiServerSvgMapsBaseURL();
			basePath = properties.getServerWebAppBasePath();
			eventShareLinkBaseUrl = properties.getRtfDesktopSiteUrl();
			fromEmail = properties.getEmailFrom();
			bccEmails = properties.getEmailBcc();
			fromEmailForPromo = properties.getEmailFromForPromo();
			apiServerSvgMapsBaseURL = properties.getApiServerSvgMapsBaseURL();
			apiServerBaseUrl = properties.getApiServerBaseUrl();
			DP_PRFEIX_CODE = properties.getRtfCustomerDPPrefix();
			DP_FOLDER_NAME = properties.getRtfCustomerDPFolder();
			DEFAULT_DP_PRFEIX_CODE = properties.getRtfDefaultDPPrefix();
			DEFAULT_DP_FOLDER_NAME = properties.getRtfDefaultDPFolder();
			paypalEnvironment = properties.getPaypalEnvironment();
			isProductionEnvironment = properties.getIsProductionEnvironment();
			rewardTheFanServerBaseUrl = "https://www.rewardthefan.com/";
			rtfImageSharedPath = properties.getRtfImageSharedPath();
			
			rtfSharedDriveUserName = properties.getRtfSharedDriveUserName();
			rtfSharedDrivePassword = properties.getRtfSharedDrivePassword();
			
			System.out.println("basePath==========>"+basePath);
			System.out.println("eventShareLinkBaseUrl==========>"+eventShareLinkBaseUrl);
			System.out.println("apiServerBaseURL==========>"+apiServerSvgMapsBaseURL);
			System.out.println("DP_PRFEIX_CODE==========>"+DP_PRFEIX_CODE);
			System.out.println("DP_FOLDER_NAME==========>"+DP_FOLDER_NAME);
			System.out.println("DEFAULT_DP_PRFEIX_CODE==========>"+DEFAULT_DP_PRFEIX_CODE);
			System.out.println("DEFAULT_DP_FOLDER_NAME==========>"+DEFAULT_DP_FOLDER_NAME);
			System.out.println("FROM EMAIL==========>"+fromEmail);
			System.out.println("BCC EMAIL==========>"+bccEmails);
			System.out.println("From Promo EMAIL==========>"+fromEmailForPromo);
			System.out.println("IS Production ==========>"+isProductionEnvironment);
		}catch(Exception e){
			basePath = "////C://Tomcat 7.0_Tomcat7.production//webapps//";
			eventShareLinkBaseUrl = "https://www.rewardthefan.com/";
			apiServerSvgMapsBaseURL = "https://api.rewardthefan.com/SvgMaps/";
			DP_PRFEIX_CODE = "REWARDTHEFAN_DP";
			DP_FOLDER_NAME = "CUSTOMER_DP";
			DEFAULT_DP_PRFEIX_CODE = "RTF_DEFAULT_DP";
			DEFAULT_DP_FOLDER_NAME = "DEFAULT_DP";
			isProductionEnvironment = true;
			rtfImageSharedPath = "smb://10.0.0.91//d$//Rewardthefan//SvgMaps//";
			rtfSharedDriveUserName = properties.getRtfSharedDriveUserName();
			rtfSharedDrivePassword = properties.getRtfSharedDrivePassword();
			e.printStackTrace();
		}
	}
	
	public static String getStoredSvgApiUrl(){
		return apiServerSvgMapsBaseURL+"Stored_SVG/";
	}
	
	public static String getStoredSVGMapPath(){
		return basePath+"SvgMaps//Stored_SVG//";
	}
	
	
	public static String getETicketDownloadURL(String orderNo){
		String fileLocation = "";
		
		try{
			fileLocation = URLUtilOld.basePath+"SvgMaps//Tickets//Ticket_"+orderNo+".pdf";
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getLogoImage(){
		String logoImage = "";
		
		try{
		logoImage = URLUtilOld.basePath+"SvgMaps//logo.png";
			//logoImage = "C://Tomcat 7.0//webapps//zoneswebservices-0.1//resources/images//logo.png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String getWhiteLogoImage(){
		String logoImage = "";
		
		try{
		logoImage = URLUtilOld.basePath+"SvgMaps//logo_white.png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String fetchIcons(String iconName){
		String logoImage = "";
		try{
		logoImage = URLUtilOld.basePath+"SvgMaps//"+iconName+".png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String fetchTriviaIcons(String iconName){
		String logoImage = "";
		try{
		logoImage = URLUtilOld.basePath+"SvgMaps//trivia//"+iconName+".png";
		}catch(Exception e){
			e.printStackTrace();
		}
		return logoImage;
	}
	
	public static String findETicketDownloadURL(String orderNo){
		String fileLocation = "";
		try{
			File ticketsFolder = new File (URLUtilOld.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(orderNo)){
							break;
						}
					}
				}
				fileLocation = URLUtilOld.basePath+"SvgMaps//Tickets//"+fileName;
				return fileLocation;
			}
			fileLocation = URLUtilOld.basePath+"SvgMaps//Tickets//Ticket_"+orderNo+".pdf";
		}catch(Exception e){
			fileLocation = URLUtilOld.basePath+"SvgMaps//Tickets//Ticket_"+orderNo+".pdf";
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	//eventNameStr+"_"+eventDateStr+"_"+eventTimeStr+"_"+sectionStr+rowStr+"_eticket_"+cnt+"."+ext;
	
		//C:/REWARDTHEFAN/Tickets/luke-bryan_172453-Sec_103_RK_S8_eticket_10500_2.pdf
		
	public static String findETicketDownloadURLFromDirectoryOld(String directoryFileName){
		String fileLocation = "";
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			File ticketsFolder = new File (URLUtilOld.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				boolean isfileFound = false;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(directoryFileName)){
							isfileFound = true;
							break;
						}
					}
				}
				if(isfileFound){
					fileLocation = URLUtilOld.basePath+"SvgMaps//Tickets//"+fileName;
				}
				return fileLocation;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String findETicketDownloadURLFromDirectory(String directoryFileName){
		String filePath = "";
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			filePath = URLUtilOld.basePath+"SvgMaps//Tickets//"+directoryFileName;
			return filePath;
		}catch(Exception e){
			e.printStackTrace();
		}
		return filePath;
	}
	
	public static String findETicketDownloadURLFromDirectoryNewOld(String directoryFileName){
		try{
			directoryFileName = directoryFileName.replaceAll("C:/REWARDTHEFAN/Tickets/", "");
			//File ticketsFolder = new File (URLUtil.basePath+"SvgMaps//Tickets//"+directoryFileName);
			return directoryFileName;
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
	public static String findETicketDownloadURL(String orderNo,String ticketAttachedId){
		String fileLocation = "";
		try{
			File ticketsFolder = new File (URLUtilOld.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(orderNo+"_"+ticketAttachedId)){
							break;
						}
					}
				}
				fileLocation = URLUtilOld.basePath+"SvgMaps//Tickets//"+fileName;
				return fileLocation;
			}
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
		}catch(Exception e){
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
			e.printStackTrace();
		}
		return fileLocation;
	}
	
public static String findETicketDownloadURL(String orderNo,String ticketAttachedId,CustomerOrder order){
		
		String fileLocation = "";
		String formattedEventDate = dateFormat.format(order.getEventDateTemp());
		String eventTimeLocal="TBD";
		if(null != order.getEventTime() ){
			eventTimeLocal = timeFormat.format(order.getEventTime());
		}
		String fullFileName = order.getEventName()+"_"+formattedEventDate+"_"+eventTimeLocal+"_"+order.getSection()+order.getActualRow()+"_eticket_";
		try{
			File ticketsFolder = new File (URLUtilOld.basePath+"SvgMaps//Tickets//");
			File[] listOfFiles = ticketsFolder.listFiles();
			if(listOfFiles != null){
				String fileName = null;
				for (int i = 0; i < listOfFiles.length;i++) {
					if (listOfFiles[i].isFile()) {
						fileName = listOfFiles[i].getName();
						if(fileName.contains(fullFileName+"_"+ticketAttachedId)){
							break;
						}
					}
				}
				fileLocation = URLUtilOld.basePath+"SvgMaps//Tickets//"+fileName;
				return fileLocation;
			}
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
		}catch(Exception e){
			//fileLocation = URLUtil.basePath+"SvgMaps//Tickets//"+orderNo+"_"+ticketAttachedId+".pdf";
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getShareLinkURL(Event event){
		String cardImage = "";
		try{
			cardImage = "http://zonetickets.com/event/" + event.getEventId()+ "";
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	
	public static String profilePicUploadDirectory(String profilePicPrefix,Integer customerId,String ext){
		String fileLocation = "";
		try{
			fileLocation = URLUtilOld.basePath+"SvgMaps//"+DP_FOLDER_NAME+"//" + profilePicPrefix+"_"+customerId+"."+ext;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String profilePicWebURL(String profilePicPrefix,Integer customerId,String ext, ApplicationPlatForm platform){
		String fileLocation = "";
		try{
			String path = "";
			if(null != platform && platform.equals(ApplicationPlatForm.DESKTOP_SITE))
				path = URLUtilOld.rewardTheFanServerSvgMapsBaseUrl;
			else
				path = URLUtilOld.apiServerSvgMapsBaseURL;
			
			fileLocation = path+DP_FOLDER_NAME+"/"+profilePicPrefix+"_"+customerId+"."+ext;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String profilePicWebURByImageName(String fileName, ApplicationPlatForm platform){
		String fileLocation = "";
		try{
			String path = "";
			if(null != platform && platform.equals(ApplicationPlatForm.DESKTOP_SITE))
				path = URLUtilOld.rewardTheFanServerSvgMapsBaseUrl;
			else
				path = URLUtilOld.apiServerSvgMapsBaseURL;
			
			fileLocation = path+DP_FOLDER_NAME+"/"+fileName;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}

	public static String profilePicWebURByImageName(String fileName){
		String fileLocation = "";
		try{
			fileLocation = URLUtilOld.apiServerSvgMapsBaseURL+DP_FOLDER_NAME+"/"+fileName;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getProfilePicFile(String custImagePath){
		String fileLocation = "";
		try{
			fileLocation = URLUtilOld.basePath+"SvgMaps//"+DP_FOLDER_NAME+"//" + custImagePath;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static String getProfilePicFolderPath(){
		return URLUtilOld.basePath+"SvgMaps//"+DP_FOLDER_NAME+"//";
	}
	
	public static String getDefaultProfilePicFile(String defaultImagePath){
		String fileLocation = "";
		try{
			fileLocation = URLUtilOld.basePath+"SvgMaps//"+DEFAULT_DP_FOLDER_NAME+"//" + defaultImagePath+".jpg";
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	public static String getCustomerDefaultprofilePicName(Integer customerId){
		String custDPFileName="";
		try{
			File defaultFile = null;
			Random random = new Random();
			while(true) {
				int number = random.nextInt(20);
				String fileName = getDefaultProfilePicFile(DEFAULT_DP_PRFEIX_CODE+"_"+number);
				
				defaultFile = new File(fileName);
				if(defaultFile != null && defaultFile.exists()) {
					break;
				}
			}
			String fileExt = defaultFile.getAbsolutePath();
			String ext = FilenameUtils.getExtension(fileExt);
			String uploadDir = URLUtilOld.profilePicUploadDirectory(DP_PRFEIX_CODE, customerId, ext);
			File newFile = new File(uploadDir);
			custDPFileName = newFile.getName();
			FileUtils.copyFile(defaultFile,newFile);
			 
		}catch(Exception e){
			e.printStackTrace();
		}
		return custDPFileName;
	}
	
	public static String getResetPasswordLink(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"ResetPassword";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	
	public static String getReferFriendsLink(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"ReferFriends";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	
	public static String getFantasyTicketLink(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"GetSuperFanLeagues";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	public static String updateShippingAddress(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"ShippingAddress";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	public static String getRewardPoints(){
		String url = "";
		try{
			url = eventShareLinkBaseUrl+"CustomerRewardPoints";
		}catch(Exception e){
			e.printStackTrace();
		}
		return url;
	}
	
	public static String getExploreCardImages(ExploreType exploreType){
		String cardImage = "";
		try{
			cardImage = "http://52.22.87.62/SvgMaps/Cards/" + exploreType + ".jpg";
		}catch(Exception e){
			e.printStackTrace();
		}
		return cardImage;
	}
	
	
	/*public static String getShareLinkURL(Event event,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "";
		String encryptionKey = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
	    if(customer!=null){
			String token =  "RTFCUSTOMERFOUND_"+event.getEventId()+"_"+customer.getId();
			String encryptedText = SecurityUtil.doEncryption(token, encryptionKey);
			String custEncryptedText = SecurityUtil.doEncryption(String.valueOf(customer.getId()), encryptionKey);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText+"&cId="+custEncryptedText;
	    }else{
	    	String token =  "RTFCUSTOMERNOTFOUND_"+ event.getEventId();
			String encryptedText = SecurityUtil.doEncryption(token, encryptionKey);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText;
	    }
	    event.setEncryptionKey(encryptionKey);
	    return shareLinkUrl;
	}*/
	
	/*public static Event getShareLinkURL(Event event,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "",desktopShareLinkUrl="";
		String encryptionKey = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
	    if(customer!=null){
			String token =  event.getEventId().toString();
			String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText;
			String cIdEncrypted = SecurityUtil.doBase64Encryption(String.valueOf(customer.getId()));
			desktopShareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText+"&cId="+cIdEncrypted;
	    }else{
	    	String token =  event.getEventId().toString();
	    	String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&eventRefIdentity="+encryptedText;
			desktopShareLinkUrl = shareLinkUrl;
	    }
	    event.setDesktopShareLinkUrl(desktopShareLinkUrl);
	    event.setShareLinkUrl(shareLinkUrl);
	    event.setEncryptionKey(encryptionKey);
	    return event;
	}*/
	
	public static Event getShareLinkURL(Event event,Customer customer,ApplicationPlatForm platForm) throws Exception{
		String shareLinkUrl = "",desktopShareLinkUrl="", referralCode ="";
	    if(customer!=null){
	    	shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&DiscountCode="+customer.getReferrerCode()+"&s="+String.valueOf(platForm);
			desktopShareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&DiscountCode="+customer.getReferrerCode()+"&s="+String.valueOf(platForm);
			event.setShareTitle("Use this discount code "+customer.getReferrerCode()+" for 5% off on your first order");
			referralCode = customer.getReferrerCode();
	    }else{
	    	shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&s="+String.valueOf(platForm);
			desktopShareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+event.getEventId()+"&s="+String.valueOf(platForm);
			event.setShareTitle("Use this discount code for 5% off on your first order");
	    }
	    event.setShareMessage("Check out this new ticket site, Great rewards program, each point is worth $1!" +
		"Use this discount code "+referralCode+" for 5% off on your first order.");
	    event.setShareLinkUrl(shareLinkUrl);
	    
	    event.setDesktopShareLinkUrl(desktopShareLinkUrl);
	    event.setDesktopShareMessage("Check out this new ticket site, Great rewards program, each point is worth $1!.");
	    event.setShareTitle("Use this discount code "+referralCode+" for 5% off on your first order");
	    return event;
	}
	
	
	/*public static String getDesktopShareLinkURLdfdf(Integer eventId,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "";
	    if(customer!=null){
			String token =  eventId.toString();
			String encryptedText = SecurityUtil.doBase64Encryption(token);
			String cIdEncrypted = SecurityUtil.doBase64Encryption(String.valueOf(customer.getId()));
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText+"&cId="+cIdEncrypted;
	    }else{
	    	String token =  eventId.toString();
	    	String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText;
	    }
	    return shareLinkUrl;
	}
	
	public static String getShareLinkURLdfdf(Integer eventId,Customer customer,String configId) throws Exception{
		String shareLinkUrl = "";
	    if(customer!=null){
			String token =  eventId.toString();
			String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText;
	    }else{
	    	String token =  eventId.toString();
	    	String encryptedText = SecurityUtil.doBase64Encryption(token);
			shareLinkUrl = eventShareLinkBaseUrl+"GetEventDetails?id="+eventId+"&eventRefIdentity="+encryptedText;
	    }
	    return shareLinkUrl;
	}
	*/
	
	/**
	 * Method to get the encrypted order download link url
	 * @param request
	 * @param configId
	 * @param customerId
	 * @param orderNo
	 * @return
	 */
	public static String getETicketDownloadUrl(HttpServletRequest request, CustomerOrder order, String configId, 
			Integer customerId,Invoice invoice,Integer attachementId,ApplicationPlatForm platForm,String ip){
		String orderDownloadLink = "";
		try {
			String serverPort = Integer.toString(request.getServerPort());
			String appContextPath = null;
			String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
			String token = "";
			String encryptedText = null;
			String clientIPAddress = request.getParameter("clientIPAddress");
			if(null == clientIPAddress || clientIPAddress.isEmpty()){
				clientIPAddress = ip;
			}
			if(serverPort != null && !serverPort.isEmpty()){
				appContextPath = URLUtilOld.rewardTheFanServerBaseUrl+"/DownloadRTFOrders?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}else{
				appContextPath = URLUtilOld.rewardTheFanServerBaseUrl+"/DownloadRTFOrders?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static String getZipTicketsDownloadUrl(HttpServletRequest request, CustomerOrder order, String configId, 
			Integer customerId,Invoice invoice,ApplicationPlatForm platForm,String ip){
		String orderDownloadLink = "";
		try {
			String serverPort = Integer.toString(request.getServerPort());
			String appContextPath = null;
			String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
			String token = "";
			String encryptedText = null;
			String clientIPAddress = request.getParameter("clientIPAddress");
			if(null == clientIPAddress || clientIPAddress.isEmpty()){
				clientIPAddress = ip;
			}
			if(serverPort != null && !serverPort.isEmpty()){
				appContextPath = URLUtilOld.rewardTheFanServerBaseUrl+"/DownloadRTFTickets?";
				token =  configId+"_"+customerId+"_"+invoice.getCustomerOrderId();
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}else{
				appContextPath = URLUtilOld.rewardTheFanServerBaseUrl+"/DownloadRTFTickets?";
				token =  configId+"_"+customerId+"_"+invoice.getCustomerOrderId();
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId+"&platForm="+platForm.toString()+"&cTrack="+clientIPAddress;
			}
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
/*public static String getETicketDownloadUrl(HttpServletRequest request, CustomerOrder order, String configId, 
			Integer customerId,Invoice invoice,Integer attachementId){
		String orderDownloadLink = "";
		try {
			String serverPort = Integer.toString(request.getServerPort());
			String appContextPath = null;
			String keyString = ApiConfigUtil.getWebServiceConfigById(configId).getSecureKey();
			String token = "";
			String encryptedText = null;
			if(serverPort != null && !serverPort.isEmpty()){
				appContextPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/DownloadRTFOrders?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId;
			}else{
				appContextPath = request.getScheme()+"://"+request.getServerName()+request.getContextPath()+"/DownloadRTFOrders?";
				token =  configId+"_"+customerId+"_"+invoice.getId()+"_"+attachementId;
				encryptedText = SecurityUtil.doEncryption(token, keyString);
				orderDownloadLink = appContextPath+"identity="+encryptedText+"&cId="+configId;
			}
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}*/

	public static String getFedExTrackingURL(Invoice invoice){
		String orderDownloadLink = "";
		try {
			String fedExTrackingNo = (null != invoice.getTrackingNo()?invoice.getTrackingNo():"11111");
			String fedExUrl = "https://www.fedex.com/apps/fedextrack/?action=track&cntry_code=us&trackingnumber="+fedExTrackingNo;
			orderDownloadLink = fedExUrl;
			return orderDownloadLink;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//unit test 
	/*public static void main(String[] args){
		Customer cust = new Customer();
		CustomerOrder order = new CustomerOrder();
		
		cust.setId(1);
		int id = cust.getId();
		order.setId(65);
		int oid = order.getId();
		
		getRTFOrderDownloadUrl("RewardTheFan",id, oid);
	}*/
	
	/*public static void main(String[] args) {
		
		Event event = new Event();
		Customer customer = new Customer();
		
		
		event.setEventId(3456567);
		customer.setId(12);
		customer.setReferrerCode("RTF0001");
		
		try {
			String key = getShareLinkURL(event, customer);
			System.out.println(key);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		/**
		 * Method to read the getGrandChildCategoryImage from specified location
		 * @return
		 */
		public static String getGrandChildCategoryImage(String grandChildCategory){
			File grandChildCategoryImgFolder = null;
			String grandChildCategoryImg = null;
			boolean fileExists = false;
			try {
				grandChildCategoryImgFolder = new File(basePath+"SvgMaps//GrandChildCategoryImage//");
				File[] listOfFiles = grandChildCategoryImgFolder.listFiles();
				
				if(listOfFiles!=null){
					String grandChildCategoryImgName = null;
					for (int i = 0; i < listOfFiles.length;i++) {
						if (listOfFiles[i].isFile()) {
							grandChildCategoryImgName = listOfFiles[i].getName();
							
							fileExists = grandChildCategoryImgName.equals(grandChildCategory);
							if(fileExists == true){
								grandChildCategoryImg = "http://52.22.87.62/SvgMaps/GrandChildCategoryImage/"+grandChildCategoryImgName;
							}
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return grandChildCategoryImg;
		}
		
		/**
		 * Method to read the getChildCategoryImage from specified location
		 * @return
		 */
		public static String getChildCategoryImage(String childCategory){
			File grandChildCategoryImgFolder = null;
			String grandChildCategoryImg = null;
			boolean fileExists = false;
			try {
				grandChildCategoryImgFolder = new File(basePath+"SvgMaps//ChildCategoryImage//");
				File[] listOfFiles = grandChildCategoryImgFolder.listFiles();
				
				if(listOfFiles!=null){
					String grandChildCategoryImgName = null;
					for (int i = 0; i < listOfFiles.length;i++) {
						if (listOfFiles[i].isFile()) {
							grandChildCategoryImgName = listOfFiles[i].getName();
							
							fileExists = grandChildCategoryImgName.equals(childCategory);
							if(fileExists == true){
								grandChildCategoryImg = "http://52.22.87.62/SvgMaps/ChildCategoryImage/"+grandChildCategoryImgName;
							}
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return grandChildCategoryImg;
		}
		
		/**
		 * Method to get the cust referrer code 
		 * @param configId
		 * @param eventRefIdentity
		 * @return
		 */
		public static String getEncryptedRefererCode(String configId,String eventRefIdentity){
			try {
				//String keyString = "QWE4#4PO==+ABCDE";
				String decodedRefCode = null;
				if(eventRefIdentity != null){
					WebServiceConfig webServiceConfig = ApiConfigUtil.getWebServiceConfigById(configId);
					
					decodedRefCode = SecurityUtil.doDecryption(eventRefIdentity, webServiceConfig.getSecureKey());
					
					String origText[] = decodedRefCode.split("_");
					decodedRefCode = origText[2];
					System.out.println("cust Id ::"+origText[0]+"event Id ::"+origText[1]+"ref code ::"+origText[2]);
				}	
				return decodedRefCode;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		
		/**
		 * Method to get the cust referrer code 
		 * @param configId
		 * @param eventRefIdentity
		 * @return
		 */
		/*public static String getEncryptedRefererCode(String eventRefIdentity,String cId){
			try {
				String decodedRefCode = null;
				
				String token =  "RTFCUSTOMERFOUND_"+event.getEventId();
				String token =  "RTFCUSTOMERNOTFOUND_"+ event.getEventId();
					
				decodedRefCode = SecurityUtil.doBase64Decryption(eventRefIdentity);
				
				String origText[] = decodedRefCode.split("_");
				
				if(decodedRefCode.contains("RTFCUSTOMERFOUND_")){
					
					String eventId = origText[1];
					String custId = origText[2];
				}else{

					String eventId = origText[1];
					
				}
				
				decodedRefCode = origText[2];
				System.out.println("cust Id ::"+origText[0]+"event Id ::"+origText[1]+"ref code ::"+origText[2]);
				return decodedRefCode;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}*/
	
		//test
		public static void mainOL(String[] args){

		String number = "374822155519874";
		String pattern = "^4";
			
	      Pattern r = Pattern.compile(pattern);
	      Matcher m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("VISA Found value: " + m.group(0) );
	       } else {
	          System.out.println("VISA NO MATCH");
	       }
		      
	      
	      pattern = "^5[1-5]";
	      r = Pattern.compile(pattern);
	      m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("MASTER Found value: " + m.group(0) );
	       } else {
	          System.out.println("MASTER NO MATCH");
	       }
	      
	      pattern = "^3[47]";
	      r = Pattern.compile(pattern);
	      m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("AMEX Found value: " + m.group(0) );
	       } else {
	          System.out.println("AMEX NO MATCH");
	       }
	      
	      pattern = "^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)";
	      r = Pattern.compile(pattern);
	      m = r.matcher(number);
	      if (m.find( )) {
	          System.out.println("DISCOVER Found value: " + m.group(0) );
	       } else {
	          System.out.println("DISCOVER NO MATCH");
	       }
		      
		   
		}
		
		public static void main(String[] args) {
			
			String eventName = "NHL Eastern Conference Quarterfinals: Pittsburgh Penguins vs. TBD � Home Game 3 (Date: TBD � If Necessary)";
			
			String splitName = eventName.replaceAll("�", "-");
			System.out.println(eventName);
			System.out.println(splitName);
		}

		public static String getPromoDialogImage(String imageName){
			return URLUtilOld.apiServerSvgMapsBaseURL+"PromotionalDialog/"+imageName;
		}
		
		
		public static OrderPaymentBreakup getPaymentIcon(OrderPaymentBreakup obj){
			
			String iconName ="", description="";
			
			switch (obj.getPaymentMethod()) {
				
				case CREDITCARD:
					iconName ="ic_credit_card.png";
					description="Credit Card";
					break;
					
				case PAYPAL:
					iconName ="ic_paypal.png";
					description="PayPal";
					break;
					
				case PARTIAL_REWARDS:
					iconName ="ic_reward_dollars.png";
					description="Reward Dollars";
					break;
					
				case FULL_REWARDS:
					iconName ="ic_reward_dollars.png";
					description="Reward Dollars";
					break;
					
				case IPAY:
					iconName ="ic_apple.png";
					description="Apple Pay";
					break;
					
				case GOOGLEPAY:
					iconName ="ic_android_pay.png";
					description="Google Pay";
					break;
					
				case CUSTOMER_WALLET:
					iconName ="ic_wallet.png";
					description="Customer Wallet";
					break;
					
				case ACCOUNT_RECIVABLE:
					iconName ="ic_bank.png";
					description="Account Receivable";
					break;
					
				case BANK_OR_CHEQUE:
					iconName ="ic_bank.png";
					description="Bank or Cheque";
					break;
					
				case CONTEST_WINNER:
					iconName ="ic_contest_winner.png";
					description="Contest Winner";
					break; 
	
				default:
					break;
			}
			obj.setIconUrl(URLUtilOld.apiServerSvgMapsBaseURL+"PaymentIcons/"+iconName);
			obj.setPaymentDescription(description);
			return obj;
		}
		
		public static String getContestPromoCodeRedirectionUrl(String promoRefType,Integer promoRefId,String refTypeName){
			String link = "https://rewardthefan.com";
			
			try {
				refTypeName = URLEncoder.encode(refTypeName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(promoRefType.equals("EVENT")){
				link = "https://www.rewardthefan.com/GetEventDetails?id="+promoRefId;
			}else if(promoRefType.equals("ARTIST")){
				link = "https://www.rewardthefan.com/Search?aId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("VENUE")){
				link = "https://www.rewardthefan.com/Search?vId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("CHILD")){
				link = "https://www.rewardthefan.com/Search?cId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("GRAND") || promoRefType.equals("GRANDCHILD")){
				link = "https://www.rewardthefan.com/Search?gcId="+promoRefId+"&key="+refTypeName;
			}else if(promoRefType.equals("PARENT")){
				link = "https://www.rewardthefan.com/Search?pId="+promoRefId+"&key="+refTypeName;
			}
			String anchorTag = "<a style='border: 11px solid #f0c14b; background-color: #f0c14b; color: #012e4f; font-size: 20px; text-decoration: none; text-align: " +
					"center;font-weight: bold;' href='"+link+"' target='_blank'> Find Tickets </a>";
			return anchorTag;
		}
	}