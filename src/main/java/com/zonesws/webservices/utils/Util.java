package com.zonesws.webservices.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.CrownJewelLeagues;
import com.zonesws.webservices.data.CrownJewelTeams;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.QuizAffiliateSetting;
import com.zonesws.webservices.data.RTFCategorySearch;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.enums.ArtistReferenceType;
import com.zonesws.webservices.utils.list.AutoPageResult;
import com.zonesws.webservices.utils.list.FantasyCategories;
import com.zonesws.webservices.utils.list.FantasyEventTeamTicket;
import com.zonesws.webservices.utils.list.FantasySportsProduct;
import com.zonesws.webservices.utils.list.VenueResult;
import com.zonesws.webservices.utils.mail.MailAttachment;

public class Util {
	
	private static Random rnd = new Random();

	public static byte[] convertFiletoByteArray(String fileName,String fileExtension){
		
		byte[] imageInByte = null;
		
		try{

			BufferedImage originalImage = ImageIO.read(new File(fileName));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write( originalImage, fileExtension, baos );
			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();

			}catch(IOException e){
				System.out.println(e.getMessage());
			}
		
		return imageInByte;
	}
	
	public static String getFileNameFromPath(String filePath) throws Exception {
		String fileName = "";
		if(filePath!=null && !filePath.isEmpty()){
			fileName = filePath.substring(filePath.lastIndexOf("/")+1, filePath.length());
		}
		return fileName;
	}
	
	
	public static String getFilePath(HttpServletRequest request,String fileName,String folderPath){
		String path = null;
		try {
			
			if(null != request){
				path = request.getSession().getServletContext().getRealPath(folderPath+fileName);
			}else{
				path = URLUtil.apiServerBaseUrl+folderPath+fileName;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;
	}
	
	public static String getFilePath(String fileName,String folderPath){
		String path = null;
		try {
			path = URLUtil.apiServerBaseUrl+folderPath+fileName;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;
	}
	
	public static String getTicketDownloadFilePath(String fileName){
		String fileLocation = "";
		try{
			fileLocation = URLUtil.getSVGMapPath()+"Tickets//" + fileName;
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileLocation;
	}
	
	public static HashMap<String, Object> normalSearchResultMapper(List<Object[]> result){
		ArrayList<Event> eventList = new ArrayList<Event>();
		ArrayList<Integer> eventIds = new ArrayList<Integer>();
		Event event = null;
		ArrayList<Artist> artistList = new ArrayList<Artist>();
		ArrayList<VenueResult> venueResultList = new ArrayList<VenueResult>();
		Artist artist = null;
		VenueResult venueResult = null;
		HashMap<String, Object> allObj = new HashMap<String, Object>();
		Integer totalEvents = 0, totalArtist = 0, totalVenue = 0;
		String refType = null,refValue=null,eRefType = null,eRefValue=null;
		boolean isFirstArtist = true,isFirstEvent = true;
		try{
			for (Object[] object : result) {
				
				String dataType = (String)object[0];
				
				if(dataType.equalsIgnoreCase("event")){
					event= new Event();
					
					/*event = MapUtil.testSvgDetailsByEvent(event);
					if(null == event.getIsMapWithSvg() || event.getIsMapWithSvg().equals("false")){
						//continue;
					}*/
					event.setEventId((Integer)object[1]);
					event.setVenueName((String)object[5]);		
					event.setCity((String)object[8]);
					event.setState((String)object[9]);
					event.setCountry((String)object[10]);
					String timeStr = DateUtil.formatTime((String)object[4]);
					event.setEventDateTime((String)object[3]+" "+timeStr);
					event.setEventDateStr((String)object[3]);
					event.setEventTimeStr(timeStr);
					event.setEventName((String)object[2]);
					event.setVenueId((Integer)object[6]);
					event.setVenueCategoryName((String)object[7]);
					BigDecimal minPrice = (BigDecimal)object[11];
					
					if(minPrice.doubleValue() <= 0) {
						continue;
					}
					
					BigDecimal maxPrice = (BigDecimal)object[12];
					event.setTicketMinPrice(minPrice.doubleValue());
					event.setTicketMaxPrice(maxPrice.doubleValue());
					event.setTotalEvents((Integer)object[17]);
					totalEvents = event.getTotalEvents();
					eventList.add(event);
					eventIds.add(event.getEventId());
					
					if(isFirstEvent) {
						if(null != object[15] && null != object[16]){
							eRefType = (String)object[15];
							eRefValue = (String)object[16];
						}
						isFirstEvent = false;
					}
				}else if(dataType.equals("artist")){
					artist= new Artist();
					/*Integer artistId = (Integer)object[0] ;
					Boolean isCustFavFlag = favArtistMap.get(artistId);
					if(null != isCustFavFlag && isCustFavFlag){
						artist.setCustFavFlag(true);
					}else{*/
						artist.setCustFavFlag(false);
					/*}*/
					artist.setId((Integer)object[1]);
					artist.setName((String)object[2]);
					if(null != object[13] && null != object[14]){
						artist.setFromDate((Timestamp)object[13]);
						artist.setToDate((Timestamp)object[14]);
					}
					artist.setTotalArtists((Integer)object[17]);
					/*artist.setTotalArtists((Integer)object[2]);*/
					artistList.add(artist);
					totalArtist = artist.getTotalArtists();
					
					if(isFirstArtist) {
						if(null != object[15] && null != object[16]){
							refType = (String)object[15];
							refValue = (String)object[16];
						}
						isFirstArtist = false;
					}
				}else if(dataType.equals("venue")){
					venueResult = new VenueResult();
					venueResult.setVenueId((Integer)object[1]);
					venueResult.setVenueName((String)object[2]);
					venueResult.setVenueCity((String)object[8]);
					venueResult.setVenueState((String)object[9]);
					venueResult.setVenueCountry((String)object[10]);
					venueResult.setTotalVenueCount((Integer)object[17]);
					venueResultList.add(venueResult);
					totalVenue = venueResult.getTotalVenueCount();
				}
			}
			allObj.put("event", eventList);
			allObj.put("eventIds", eventIds);
			allObj.put("artist", artistList);
			allObj.put("venue", venueResultList);
			allObj.put("totalEvents", totalEvents);
			allObj.put("totalArtists", totalArtist);
			allObj.put("totalVenues", totalVenue);
			
			if(refType == null || refValue == null) {
				refType = eRefType;
				refValue = eRefValue;
			}
			if(refType != null && refValue != null) {
				
				String aRefType=null;
				String aRefValue = null;
				if(refType.equalsIgnoreCase("PARENT_ID")) {
					aRefType = ArtistReferenceType.PARENT_ID.toString();
					aRefValue = refValue;
				} else if(refType.equalsIgnoreCase("CHILD_ID")) {
					aRefType = ArtistReferenceType.CHILD_ID.toString();
					aRefValue = refValue;
					
				} else if(refType.equalsIgnoreCase("CHILD_NAME")) {
					aRefType = ArtistReferenceType.CHILD_NAME.toString();
					aRefValue = refValue;
					
				} else if(refType.equalsIgnoreCase("GRANDCHILD_ID")) {
					aRefType = ArtistReferenceType.GRANDCHILD_ID.toString();
					aRefValue = refValue;
					
				} else if(refType.equalsIgnoreCase("GRANDCHILD_NAME")) {
					aRefType = ArtistReferenceType.GRANDCHILD_NAME.toString();
					aRefValue = refValue;
					
				} else if(refType.equalsIgnoreCase("CHILD_GRANDCHILD_ID")) {
					String refArr [] = refValue.split("_");
					if(refArr.length == 2) {
						RTFCategorySearch rtfCatSearch = DAORegistry.getQueryManagerDAO().getRTFCategoriesByChildAndGrandChild(Integer.parseInt(refArr[0]), Integer.parseInt(refArr[1]));
						if(rtfCatSearch != null) {
							aRefType = ArtistReferenceType.RTFCATS_ID.toString();
							aRefValue = rtfCatSearch.getId().toString();
						}
					}
				}
				allObj.put("aRefType", aRefType);
				allObj.put("aRefValue", aRefValue);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return allObj;
	}
	
	public static HashMap<String, Object> autoSearchResultMapper(List<Object[]> result){
		HashMap<String, Object> allObj = new HashMap<String, Object>();
		AutoPageResult autoPageResult = null;
		ArrayList<AutoPageResult> autoPageResultList = new ArrayList<AutoPageResult>();
		
		try{
			for (Object[] object : result) {
				
				String resultType = (String)object[0];				
				autoPageResult= new AutoPageResult();
				Integer artistId = (Integer)object[1] ;
				autoPageResult.setId(artistId);
				autoPageResult.setName((String)object[2]);
				autoPageResult.setIsVenue(resultType.equals("venue")?true:false);
				autoPageResult.setIsArtist(resultType.equals("artist")?true:false);
				autoPageResult.setTotalRows((Integer)object[3]);
				autoPageResultList.add(autoPageResult);				
			}
			
			allObj.put("autoPageResultList", autoPageResultList);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return allObj;
	}
	
	
	public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public static Boolean isDateBetweenTwoDate(Date d, Date fromDate, Date toDate) {
	    return d.compareTo(fromDate) >= 0 && d.compareTo(toDate) <= 0;
	}
	public static Boolean isValidAffiliateReferral(Date d, QuizAffiliateSetting quizAffiliateSetting) {
		if(null == quizAffiliateSetting.getEffectiveToDate()) {
			return d.compareTo(quizAffiliateSetting.getEffectiveFromDate()) >= 0 ;
		}
	    return d.compareTo(quizAffiliateSetting.getEffectiveFromDate()) >= 0 && d.compareTo(quizAffiliateSetting.getEffectiveToDate()) <= 0;
	}
	
	public static void computePackageInformation(FantasyCategories obj,String fontSizeStr){ 
		
		if(obj.getId().equals(3)){
			obj.setShowEventInfo(true);
			obj.setShowPackageHeader(true);
			obj.setPackageHeader("SUPER BOWL LII PACKAGE INCLUDES");
			obj.setEventInfoText("02/04/2018 - US Bank Stadium, Minneapolis, MN, US");
			
		}else if(obj.getId().equals(2)){
			obj.setShowEventInfo(false);
			obj.setShowPackageHeader(true);
			obj.setPackageHeader("NBA FINALS HOME GAME 1 PACKAGE INCLUDES");
			obj.setEventInfoText("");
			
		}else if(obj.getId().equals(8)){
			obj.setShowEventInfo(false);
			obj.setShowPackageHeader(true);
			obj.setPackageHeader("STANLEY CUP FINALS HOME GAME 1 PACKAGE INCLUDES");
			obj.setEventInfoText("");
			
		}else if(obj.getId().equals(9)){
			obj.setShowEventInfo(false);
			obj.setShowPackageHeader(true);
			obj.setPackageHeader("WORLD SERIES HOME GAME 1 PACKAGE INCLUDES");
			obj.setEventInfoText("");
		}
		
		String packageDetails = "<font "+fontSizeStr+">&#x2605; "+obj.getPkgInfoLine1()+"";
		
		if(null != obj.getPkgInfoLine2() && !obj.getPkgInfoLine2().isEmpty()){
			packageDetails = packageDetails +"<br>&#x2605; "+obj.getPkgInfoLine2()+".";
		}
		
		if(null != obj.getPkgInfoLine3() && !obj.getPkgInfoLine3().isEmpty()){
			packageDetails = packageDetails +"<br>&#x2605; "+obj.getPkgInfoLine3()+".";
		}
		
		if(null != obj.getPkgInfoLine4() && !obj.getPkgInfoLine4().isEmpty()){
			packageDetails = packageDetails +"<br>&#x2605; "+obj.getPkgInfoLine4()+".";
		}
		
		if(null != obj.getPkgInfoLine5() && !obj.getPkgInfoLine5().isEmpty()){
			packageDetails = packageDetails +"<br>&#x2605; "+obj.getPkgInfoLine5()+".";
		}
		
		if(null != obj.getPkgInfoLine6() && !obj.getPkgInfoLine6().isEmpty()){
			packageDetails = packageDetails +"<br>&#x2605; "+obj.getPkgInfoLine6()+".";
		}
		packageDetails = packageDetails + "</font>";
		obj.setPackageInformation(packageDetails);
	}
	
	
	public static FantasySportsProduct computeDialogMessage(FantasySportsProduct sportsProduct ,FantasyCategories obj,String fontSizeStr,
			ApplicationPlatForm platForm, Double totalRequiredPoints,CrownJewelTeams crownJewelTeams,CrownJewelLeagues fantasyEventObj,
			String quantityStr) throws Exception{ 
			
			String finalKey = "", fullNameKey = "";
			if(obj.getId().equals(3)){
				finalKey = "SUPER BOWL LII FINALS";
				fullNameKey = "SUPER BOWL LII PACKAGE";
			}else if(obj.getId().equals(2)){
				finalKey = "NBA FINALS";
				fullNameKey = "NBA FINALS HOME GAME 1 PACKAGE";
				
			}else if(obj.getId().equals(8)){
				finalKey = "STANLEY CUP FINALS";
				fullNameKey = "STANLEY CUP FINALS HOME GAME 1 PACKAGE";
				
			}else if(obj.getId().equals(9)){
				finalKey = "WORLD SERIES FINALS";
				fullNameKey = "WORLD SERIES HOME GAME 1 PACKAGE";
			}
			
			String message =  "",ticTrackerMsg =  "";
			if(platForm.equals(ApplicationPlatForm.DESKTOP_SITE)){
				message = "<font "+fontSizeStr+" color=#FFFFFF><strong>By redeeming "+TicketUtil.getRoundedValueString(totalRequiredPoints)+" of your " +
				"Reward Dollars today, if the "+crownJewelTeams.getName()+" make it to the " +
				""+finalKey+", we will REWARD YOU with an "+fullNameKey+" for two. If the " +
				""+crownJewelTeams.getName()+" do not make it to the " +
				""+finalKey+" we return your reward dollars to your rewards account.</strong></font>";
			}else{
				message = "<font "+fontSizeStr+" color=#FFFFFF><strong>By redeeming "+TicketUtil.getRoundedValueString(totalRequiredPoints)+" of your " +
				"Reward Dollars today, if the "+crownJewelTeams.getName()+" make it to the " +
				""+finalKey+", we will REWARD YOU with an "+fullNameKey+" for two. If the " +
				""+crownJewelTeams.getName()+" do not make it to the " +
				""+finalKey+" we return your reward dollars to your rewards account.</strong></font>";
			}
			
			ticTrackerMsg = "By redeeming "+TicketUtil.getRoundedValueString(totalRequiredPoints)+" of your " +
			"Reward Dollars today, if the "+crownJewelTeams.getName()+" make it to the " +
			""+finalKey+", we will REWARD YOU with an "+fullNameKey+" for two. If the " +
			""+crownJewelTeams.getName()+" do not make it to the " +
			""+finalKey+" we return your reward dollars to your rewards account.";
			
			sportsProduct.setTicTrackerMessage(ticTrackerMsg);
			sportsProduct.setMessage(message);
			return sportsProduct;
	}
	
	
	public static Comparator<FantasyEventTeamTicket> fantasyEventAscOrder = new Comparator<FantasyEventTeamTicket>() {

		public int compare(FantasyEventTeamTicket event1, FantasyEventTeamTicket event2) {
			int cmp = event1.getTeamName().compareTo(
					event2.getTeamName());
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			return event1.getFantasyEventId().compareTo(event2.getFantasyEventId());
	    }};
	    
	    
	 public static MailAttachment[] getOrderConfirmationEmailAttachmentImages(){
		 	MailAttachment[] mailAttachment = new MailAttachment[9];
			mailAttachment[0] = new MailAttachment(null,"image/png","li1.png",URLUtil.fetchIcons("li1"));
			mailAttachment[1] = new MailAttachment(null,"image/png","p1.png",URLUtil.fetchIcons("p1"));
			mailAttachment[2] = new MailAttachment(null,"image/png","ig1.png",URLUtil.fetchIcons("ig1"));
			mailAttachment[3] = new MailAttachment(null,"image/png","tw1.png",URLUtil.fetchIcons("tw1"));
			mailAttachment[4] = new MailAttachment(null,"image/png","fb1.png",URLUtil.fetchIcons("fb1"));
			mailAttachment[5] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
			mailAttachment[6] = new MailAttachment(null,"image/png","tyrtf.png",URLUtil.fetchIcons("tyrtf"));
			mailAttachment[7] = new MailAttachment(null,"image/png","order-concert.png",URLUtil.fetchIcons("order-concert"));
			mailAttachment[8] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			//mailAttachment[9] = new MailAttachment(null,"image/png","share.png",URLUtil.fetchIcons("share"));
			return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentForTicketTemplate(boolean isRewardInfo){
		 	MailAttachment[] mailAttachment =null;
		 	if(isRewardInfo){
		 		 mailAttachment = new MailAttachment[11];
		 	}else{
		 		 mailAttachment = new MailAttachment[10];
		 	}
			mailAttachment[0] = new MailAttachment(null,"image/png","li1.png",URLUtil.fetchIcons("li1"));
			mailAttachment[1] = new MailAttachment(null,"image/png","p1.png",URLUtil.fetchIcons("p1"));
			mailAttachment[2] = new MailAttachment(null,"image/png","ig1.png",URLUtil.fetchIcons("ig1"));
			mailAttachment[3] = new MailAttachment(null,"image/png","tw1.png",URLUtil.fetchIcons("tw1"));
			mailAttachment[4] = new MailAttachment(null,"image/png","fb1.png",URLUtil.fetchIcons("fb1"));
			mailAttachment[5] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
			mailAttachment[6] = new MailAttachment(null,"image/png","tyrtf.png",URLUtil.fetchIcons("tyrtf"));
			mailAttachment[7] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			//mailAttachment[8] = new MailAttachment(null,"image/png","share.png",URLUtil.fetchIcons("share"));
			mailAttachment[8] = new MailAttachment(null,"image/png","top.png",URLUtil.fetchIcons("top"));
			mailAttachment[9] = new MailAttachment(null,"image/png","bottom.png",URLUtil.fetchIcons("bottom"));
			if(isRewardInfo){
				mailAttachment[10] = new MailAttachment(null,"image/png","reward-points-concerts.png",URLUtil.fetchIcons("reward-points-concerts"));
				//mailAttachment[12] = new MailAttachment(null,"image/png","reward-points-sports.png",URLUtil.fetchIcons("reward-points-sports"));
		 	}
			return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentForRegistrationTemplate(boolean isRewardInfo){
		 MailAttachment[] mailAttachment =null;
		 	if(isRewardInfo){
		 		 mailAttachment = new MailAttachment[9];
		 	}else{
		 		 mailAttachment = new MailAttachment[8];
		 	}
			mailAttachment[0] = new MailAttachment(null,"image/png","li1.png",URLUtil.fetchIcons("li1"));
			mailAttachment[1] = new MailAttachment(null,"image/png","p1.png",URLUtil.fetchIcons("p1"));
			mailAttachment[2] = new MailAttachment(null,"image/png","ig1.png",URLUtil.fetchIcons("ig1"));
			mailAttachment[3] = new MailAttachment(null,"image/png","tw1.png",URLUtil.fetchIcons("tw1"));
			mailAttachment[4] = new MailAttachment(null,"image/png","fb1.png",URLUtil.fetchIcons("fb1"));
			mailAttachment[5] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
			mailAttachment[6] = new MailAttachment(null,"image/png","tyrtf.png",URLUtil.fetchIcons("tyrtf"));
			mailAttachment[7] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			//mailAttachment[8] = new MailAttachment(null,"image/png","share.png",URLUtil.fetchIcons("share"));
			if(isRewardInfo){
				mailAttachment[8] = new MailAttachment(null,"image/png","reward-points-concerts.png",URLUtil.fetchIcons("reward-points-concerts"));
				//mailAttachment[10] = new MailAttachment(null,"image/png","reward-points-sports.png",URLUtil.fetchIcons("reward-points-sports"));
		 	}
			return mailAttachment;
	 }
	 
	 
	 
	 public static MailAttachment[] getEmailAttachmentForProductDelivered(){
		 	MailAttachment[] mailAttachment =null;
		 	mailAttachment = new MailAttachment[8];
			mailAttachment[0] = new MailAttachment(null,"image/png","li1.png",URLUtil.fetchIcons("li1"));
			mailAttachment[1] = new MailAttachment(null,"image/png","p1.png",URLUtil.fetchIcons("p1"));
			mailAttachment[2] = new MailAttachment(null,"image/png","ig1.png",URLUtil.fetchIcons("ig1"));
			mailAttachment[3] = new MailAttachment(null,"image/png","tw1.png",URLUtil.fetchIcons("tw1"));
			mailAttachment[4] = new MailAttachment(null,"image/png","fb1.png",URLUtil.fetchIcons("fb1"));
			mailAttachment[5] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
			mailAttachment[6] = new MailAttachment(null,"image/png","tyrtf.png",URLUtil.fetchIcons("tyrtf"));
			mailAttachment[7] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentRTFLIVEContactUS(){
		 	MailAttachment[] mailAttachment =null;
		 	mailAttachment = new MailAttachment[1];
			mailAttachment[0] = new MailAttachment(null,"image/png","rtflive.PNG",URLUtil.getRTFLIVELogoImage());
			return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentForCustomerPromoOffer(){
		 MailAttachment[] mailAttachment =null;
		 	mailAttachment = new MailAttachment[1];
		 	mailAttachment[0] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentForPreOrderPageAuditNotification(){
		 MailAttachment[] mailAttachment =null;
		 	mailAttachment = new MailAttachment[1];
		 	mailAttachment[0] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentforContestEmail(){
		MailAttachment[] mailAttachment = new MailAttachment[3];
		/*mailAttachment[0] = new MailAttachment(null,"image/png","li1.png",URLUtil.fetchIcons("li1"));
		mailAttachment[1] = new MailAttachment(null,"image/png","p1.png",URLUtil.fetchIcons("p1"));
		mailAttachment[2] = new MailAttachment(null,"image/png","ig1.png",URLUtil.fetchIcons("ig1"));
		mailAttachment[3] = new MailAttachment(null,"image/png","tw1.png",URLUtil.fetchIcons("tw1"));
		mailAttachment[4] = new MailAttachment(null,"image/png","fb1.png",URLUtil.fetchIcons("fb1"));
		mailAttachment[5] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
		mailAttachment[6] = new MailAttachment(null,"image/png","tyrtf.png",URLUtil.fetchIcons("tyrtf"));
		mailAttachment[8] = new MailAttachment(null,"image/png","share.png",URLUtil.fetchIcons("share"));*/
		mailAttachment[0] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
		mailAttachment[1] = new MailAttachment(null,"image/png","top.png",URLUtil.fetchIcons("top"));
		mailAttachment[2] = new MailAttachment(null,"image/png","bottom.png",URLUtil.fetchIcons("bottom"));
		//mailAttachment[11] = new MailAttachment(null,"image/png","participat.png",URLUtil.fetchIcons("participat"));
		return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentForPreOrderPageVisitOffer(){
		 	MailAttachment[] mailAttachment = new MailAttachment[11];
			mailAttachment[0] = new MailAttachment(null,"image/png","li1.png",URLUtil.fetchIcons("li1"));
			mailAttachment[1] = new MailAttachment(null,"image/png","p1.png",URLUtil.fetchIcons("p1"));
			mailAttachment[2] = new MailAttachment(null,"image/png","ig1.png",URLUtil.fetchIcons("ig1"));
			mailAttachment[3] = new MailAttachment(null,"image/png","tw1.png",URLUtil.fetchIcons("tw1"));
			mailAttachment[4] = new MailAttachment(null,"image/png","fb1.png",URLUtil.fetchIcons("fb1"));
			mailAttachment[5] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
			mailAttachment[6] = new MailAttachment(null,"image/png","tyrtf.png",URLUtil.fetchIcons("tyrtf"));
			mailAttachment[7] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			//mailAttachment[8] = new MailAttachment(null,"image/png","share.png",URLUtil.fetchIcons("share"));
			mailAttachment[8] = new MailAttachment(null,"image/png","google_store.png",URLUtil.fetchIcons("google_store"));
			mailAttachment[9] = new MailAttachment(null,"image/png","app_store.png",URLUtil.fetchIcons("app_store"));
			mailAttachment[10] = new MailAttachment(null,"image/png","app_bg.png",URLUtil.fetchIcons("app_bg"));
			return mailAttachment;
	 }
	 
	 
	 public static MailAttachment[] getEmailAttachmentForReferralEmailTemplate(){
		 	MailAttachment[] mailAttachment = new MailAttachment[3];
			mailAttachment[0] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			mailAttachment[1] = new MailAttachment(null,"image/png","top.png",URLUtil.fetchIcons("top"));
			mailAttachment[2] = new MailAttachment(null,"image/png","bottom.png",URLUtil.fetchIcons("bottom"));
			return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentForRewardPoints(){
		 	MailAttachment[] mailAttachment = new MailAttachment[5];
			mailAttachment[0] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			mailAttachment[1] = new MailAttachment(null,"image/png","app1.png",URLUtil.fetchIcons("app1"));
			mailAttachment[2] = new MailAttachment(null,"image/png","app2.png",URLUtil.fetchIcons("app2"));
			mailAttachment[3] = new MailAttachment(null,"image/png","dollar.png",URLUtil.fetchIcons("dollar"));
			mailAttachment[4] = new MailAttachment(null,"image/png","rewardoptions.png",URLUtil.fetchIcons("rewardoptions"));
			return mailAttachment;
	 }
	 
	 public static String generatedCustomerUserId(String fName,String lName){
	  String userId="";
	  try {
	   if(fName == null){
	    fName="";
	   }
	   if(lName == null){
	    lName="";
	   }
	   
	   fName = fName.trim().replaceAll("'","");
	   fName = fName.trim().replaceAll("-","");
	   fName = fName.trim().replaceAll("_","");
	   fName = fName.trim().replaceAll("\\.","");
	   fName = fName.trim().replaceAll(" ","");
	   
	   lName = lName.trim().replaceAll("'","");
	   lName = lName.trim().replaceAll("-","");
	   lName = lName.trim().replaceAll("_","");
	   lName = lName.trim().replaceAll("\\.","");
	   lName = lName.trim().replaceAll(" ","");
	   
	   if(!fName.trim().isEmpty() && !lName.trim().isEmpty()){
	    userId = String.valueOf(fName.trim().charAt(0));
	    userId += lName.trim();
	   }else if(!fName.trim().isEmpty() && lName.trim().isEmpty()){
	    userId = fName.trim();
	   }else if(fName.trim().isEmpty() && !lName.trim().isEmpty()){
	    userId = lName.trim();
	   }
	   
	   if(userId.length() < 5){
	    int len = 5-userId.length();
	    for(int i=1;i<=len;i++){
	     userId += rnd.nextInt(10);
	    }
	   }else if(userId.length() > 12){
	    userId = userId.substring(0,12);
	   }
	   Customer customer= DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
	   if(null != customer){
		   if(userId.length() <= 10){
		     userId += rnd.nextInt(10); 
		     userId += rnd.nextInt(10);
		     customer = DAORegistry.getCustomerDAO().getCustomerByUserId(userId);
		    }
		   if(null != customer){
		     if(!fName.trim().isEmpty()){
		    	 userId = fName;
		     }else if(!lName.trim().isEmpty()){
		    	 userId = lName;
		     }
		     if(userId.length() < 5){
		    	 int len = 6-userId.length();
		    	 for(int i=1;i<=len;i++){
		    		 userId += rnd.nextInt(10);
		    	 }
		     }else if(userId.length() > 12){
		    	 userId = userId.substring(0,10);
		    	 userId += rnd.nextInt(10); 
		    	 userId += rnd.nextInt(10);
		     }else if(userId.length() < 9){
		    	 userId += rnd.nextInt(10); 
		    	 userId += rnd.nextInt(10);
		    	 userId += rnd.nextInt(10);
		     }
		   }
	   }
	  } catch (Exception e) {
		  e.printStackTrace();
		  userId = "";
	  }
	  return userId.toLowerCase();
	 }
	 
	 
	 public static MailAttachment[] getEmailAttachmentForTGUserMigration(){
		MailAttachment[] mailAttachment = new MailAttachment[3];
		mailAttachment[0] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
		mailAttachment[1] = new MailAttachment(null,"image/png","footer_logo.png",URLUtil.fetchIcons("footer_logo"));
		mailAttachment[2] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
		return mailAttachment;
	 }
	 
	 public static MailAttachment[] testMail(){
			MailAttachment[] mailAttachment = new MailAttachment[3];
			mailAttachment[0] = new MailAttachment(null,"image/png","blue.png",URLUtil.fetchIcons("blue"));
			mailAttachment[1] = new MailAttachment(null,"image/png","footer_logo.png",URLUtil.fetchIcons("footer_logo"));
			mailAttachment[2] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
			return mailAttachment;
		 }
	 
	 public static MailAttachment[] getEmailAttachmentForTriviaEmail(){
		MailAttachment[] mailAttachment = new MailAttachment[12];
		//mailAttachment[0] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE"));
		mailAttachment[0] = new MailAttachment(null,"image/png","BETA_EMAIL_GOOGLE_PLAY.png",URLUtil.fetchIcons("BETA_EMAIL_GOOGLE_PLAY"));
		mailAttachment[1] = new MailAttachment(null,"image/png","BETA_EMAIL_PHONE.png",URLUtil.fetchIcons("BETA_EMAIL_PHONE"));
		mailAttachment[2] = new MailAttachment(null,"image/png","RTF_BETA_EMAIL_FOOTER.png",URLUtil.fetchIcons("RTF_BETA_EMAIL_FOOTER"));
		mailAttachment[3] = new MailAttachment(null,"image/png","RTF_BETA_EMAIL_HEADER.png",URLUtil.fetchIcons("RTF_BETA_EMAIL_HEADER"));
		
		mailAttachment[4] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_1.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_1"));
		mailAttachment[5] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_2.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_2"));
		mailAttachment[6] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_3.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_3"));
		mailAttachment[7] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_4.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_4"));
		mailAttachment[8] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_5.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_5"));
		mailAttachment[9] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_6.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_6"));
		mailAttachment[10] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_7.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_7"));
		mailAttachment[11] = new MailAttachment(null,"image/png","BETA_EMAIL_APPLE_IPHONE_8.png",URLUtil.fetchIcons("BETA_EMAIL_APPLE_IPHONE_8"));
		return mailAttachment;
	 }
	 
	 public static MailAttachment[] getEmailAttachmentForContestEmail(){
		MailAttachment[] mailAttachment = new MailAttachment[6];
		mailAttachment[0] = new MailAttachment(null,"image/png","rtflogot.png",URLUtil.fetchIcons("rtflogot"));
		mailAttachment[1] = new MailAttachment(null,"image/png","ticket_icon.png",URLUtil.fetchIcons("ticket_icon"));
		mailAttachment[2] = new MailAttachment(null,"image/png","fb.png",URLUtil.fetchIcons("fb"));
		mailAttachment[3] = new MailAttachment(null,"image/png","tw.png",URLUtil.fetchIcons("tw"));
		mailAttachment[4] = new MailAttachment(null,"image/png","in.png",URLUtil.fetchIcons("in"));
		mailAttachment[5] = new MailAttachment(null,"image/png","ins.png",URLUtil.fetchIcons("ins"));
		//mailAttachment[6] = new MailAttachment(null,"image/png","top_header.png",URLUtil.fetchIcons("top_header"));
		//mailAttachment[7] = new MailAttachment(null,"image/png","sc.png",URLUtil.fetchIcons("sc")); 
		return mailAttachment;
	 }
	 
}
