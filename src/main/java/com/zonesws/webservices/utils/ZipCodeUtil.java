package com.zonesws.webservices.utils;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.jobs.LoadZipCodes;
import com.zonesws.webservices.utils.list.ZipCode;


public class ZipCodeUtil {
	
	
	
	public static String splitZipCodesByCommaSeperator(List<String> zipCodeList){
		
		String zipCodeStr = "";
		int i=1,size=zipCodeList.size();
		for (String zipCode : zipCodeList) {
			if(i > 50){
				zipCodeStr +="'"+zipCode+"'";
				break;
			}
			if(i==size){
				zipCodeStr +="'"+zipCode+"'";
			}else{
				zipCodeStr +="'"+zipCode+"'"+",";
			}
			i++;
		}
		return zipCodeStr;
	}
	
	public static String splitZipCodesByCommaSeperatorForGS(List<String> zipCodeList){
		
		String zipCodeStr = "";
		int i=1,size=zipCodeList.size();
		for (String zipCode : zipCodeList) {
			
			if(i > 10){
				zipCodeStr +=""+zipCode+"";
				break;
			}
			if(i==size){
				zipCodeStr +=""+zipCode+"";
			}else{
				zipCodeStr +=""+zipCode+""+",";
			}
			i++;
		}
		return zipCodeStr;
	}
	
	
	public static List<String> getAllNearestZipCodesbyMiles(String latitudeStr , String longitudeStr, String searchMileStr,List<String> zipCodes) throws Exception {
		try{
			Double searchMile = Double.valueOf(searchMileStr.trim()),
			latitude =Double.valueOf(latitudeStr.trim()), 
			longitude =Double.valueOf(longitudeStr.trim()); 
			List<ZipCode> zipCodesList = LoadZipCodes.getAllZipCodes();
			Map<Double, String> map = new TreeMap<Double, String>();
			
			for (ZipCode zipCode : zipCodesList) {
				try{
					map.put(distance(latitude, longitude, zipCode.getLatitude(), zipCode.getLongitude()),zipCode.getZipCode());
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			for(Double distance : map.keySet()){
				if(distance <= searchMile){
					String zipCode = map.get(distance);
					if(null != zipCode && !zipCode.isEmpty() ){
						zipCodes.add(zipCode);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return zipCodes;
	}
	
	
 public static List<String> getNearestZipCodes(List<String> zipCodes,String zipCode) throws Exception {
	 	/*String miles = "30";
        String apiUrl = "https://www.zipcodeapi.com/rest/8baXgGDU1JBoggZyQXkUbwOKhpoS52Qed1buHPoJz07Qh6o73J2ffR0xgoplqgzE/radius.json/"+zipCode+
                             "/"+miles+"/mile";
        RestTemplate restTemplate = new RestTemplate();
        String jsonData = restTemplate.getForObject(apiUrl, String.class);
        JSONObject jsonObject1 = new JSONObject(jsonData);
        JSONArray  jsonArray = jsonObject1.getJSONArray("zip_codes");
        for(int i=0 ; i < jsonArray.length() ; i++){
	        JSONObject jsonObject = jsonArray.getJSONObject(i);
	        zipCodes.add(jsonObject.get("zip_code").toString());
        }*/
        return zipCodes;
	}
 
 
 public static String getIpAddresStateFromIPApi(String ipAddress) throws Exception {
	 /* {
	    "ip": "206.71.237.194",
	    "city": "Astoria",
	    "region": "New York",
	    "region_code": "NY",
	    "country": "US",
	    "country_name": "United States",
	    "postal": "11106",
	    "latitude": 40.7611,
	    "longitude": -73.9319,
	    "timezone": "America/New_York",
	    "asn": "AS6079",
	    "org": "RCN"
	}*/
     String regionCode = null;
	 try{
		 /*String apiUrl = "https://ipapi.co/"+ipAddress+"/json/";
	     RestTemplate restTemplate = new RestTemplate();
	     String jsonData = restTemplate.getForObject(apiUrl, String.class);
	     JSONObject jsonObject = new JSONObject(jsonData);
	     if(null != jsonObject ){
		     if(null != jsonObject.get("country") && jsonObject.get("country") != ""){
		    	 String countryCode = jsonObject.get("country").toString();
		    	 if(!isOutSideRTFCountry(countryCode)){
		    		 if(null != jsonObject.get("region_code") && jsonObject.get("region_code") != ""){
				 		 regionCode = jsonObject.get("region_code").toString();
				 	 }
		    	 }
			} 
	     }*/
	 }catch(Exception e){
		 e.printStackTrace();
	 }
     return regionCode;
 }
 
 public static String getIpAddresStateFromGeoIP(String ipAddress) throws Exception {
	 /*{"ip":"59.90.9.110","country_code":"IN","country_name":"India","region_code":"TN",
		 "region_name":"Tamil Nadu","city":"Chennai","zip_code":"600053","time_zone":"Asia/Kolkata",
		 "latitude":13.0833,"longitude":80.2833,"metro_code":0}*/
	 String regionCode = null;
	 try{
		 /*String apiUrl = "https://freegeoip.net/json/"+ipAddress;
	     RestTemplate restTemplate = new RestTemplate();
	     String jsonData = restTemplate.getForObject(apiUrl, String.class);
	     JSONObject jsonObject = new JSONObject(jsonData);
	     if(null != jsonObject ){
		     if(null != jsonObject.get("country_code") && jsonObject.get("country_code") != ""){
		    	 String countryCode = jsonObject.get("country_code").toString();
		    	 if(!isOutSideRTFCountry(countryCode)){
		    		 if(null != jsonObject.get("region_code") && jsonObject.get("region_code") != ""){
				 		 regionCode = jsonObject.get("region_code").toString();
				 	 }
		    	 }
			} 
	     }*/
		
	 }catch(Exception e){
		 e.printStackTrace();
	 }
     return regionCode;
 }
 
 public static boolean isOutSideRTFCountry(String countryCode){
	 boolean isOutSide = true;
	 if(countryCode.equals("USA") || countryCode.equals("US")){
		 isOutSide = false;
	 }else if(countryCode.equals("CAN") || countryCode.equals("CA")){
		 isOutSide = false;
	 }
	 return isOutSide;
 }
 
 public static String getStateByIP(String ipAddress){
	 String state = null;
	 try{
		 state = getIpAddresStateFromIPApi(ipAddress);
		 if(null == state || state.isEmpty()){
			 state = getIpAddresStateFromGeoIP(ipAddress);
		 }
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	 return state;
 }
 
 public static void main(String[] args) throws Exception {
	 String ipAddress= "59.90.9.110";
	 System.out.println(getIpAddresStateFromIPApi(ipAddress));
	 System.out.println("===============================================");
	 System.out.println(getIpAddresStateFromGeoIP(ipAddress));
	 
}

 
 public static void getulaga() throws Exception {

     String apiUrl = "http://208.74.19.152:8085/CustomerEmailValidation.json";
     RestTemplate restTemplate = new RestTemplate();
     
     
     MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
     map.add("configId", "Onquash");
     map.add("email", "ulaganathantoall@gmail.com");
     
     
     String jsonData = restTemplate.postForObject(apiUrl,  null, String.class, map);
     
     System.out.println(jsonData);
     
     /*JSONObject jsonObject1 = new JSONObject(jsonData);
     JSONArray  jsonArray = jsonObject1.getJSONArray("zip_codes");
     for(int i=0 ; i < jsonArray.length() ; i++){
	        JSONObject jsonObject = jsonArray.getJSONObject(i);
	        zipCodes.add(jsonObject.get("zip_code").toString());
     }*/
	}
 
 
//Gives the distance between two latitude and longitude
	private static double distance(double lat1, double lon1, double lat2, double lon2) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		return (dist);
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts decimal degrees to radians						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts radians to decimal degrees						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
	
	
}
