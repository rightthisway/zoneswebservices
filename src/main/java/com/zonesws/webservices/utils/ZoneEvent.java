package com.zonesws.webservices.utils;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("ZoneEvent")
public class ZoneEvent {
	
	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String artistName;
	private String venueName;
	private String venueCity;
	private String venueState;
	private String venueCountry;
	private String venueZipCode;
	private String mapPath;
	private Boolean isFavouriteEvent;
	private String venueCategoryName;
	private Integer priceStartFrom;
	@XStreamOmitField
	private Integer artistId;
	
	@XStreamOmitField
	private Integer venueId;
	
	private Integer totalEvents;
	
	private Boolean discountFlag;
	
	private String parentCategoryName;
	private String childCategoryName;
	private String grandChildCategoryName;
	
	@JsonIgnore
	private Double discountPerc;
	
	private String svgText;
	private String isMapWithSvg;
	private String svgMapPath;
	
	/*@XStreamOmitField
	private Venue venueObj;
	
	@XStreamOmitField
	private Artist artistObj;
	*/
	public Integer getEventId() {
		if(null == eventId ){
			eventId = 0;
		}
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		if(null ==eventName || eventName.isEmpty()){
			eventName="";
		}
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		if(null ==eventDate || eventDate.isEmpty()){
			eventDate="";
		}
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		if(null ==eventTime || eventTime.isEmpty()){
			eventTime="";
		}
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getArtistName() {
		if(null ==artistName || artistName.isEmpty()){
			artistName="";
		}
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getVenueName() {
		if(null ==venueName || venueName.isEmpty()){
			venueName="";
		}
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getVenueCity() {
		if(null ==venueCity || venueCity.isEmpty()){
			venueCity="";
		}
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	public String getVenueState() {
		if(null ==venueState || venueState.isEmpty()){
			venueState="";
		}
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	public String getVenueCountry() {
		if(null ==venueCountry || venueCountry.isEmpty()){
			venueCountry="";
		}
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	
	
	public Integer getArtistId() {
		if(null == artistId ){
			artistId = 0;
		}
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public Integer getVenueId() {
		if(null == venueId ){
			venueId = 0;
		}
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	/*public Venue getVenueObj() {
		if(null != venueId){
			venueObj =TMATDAORegistry.getVenueDAO().get(venueId);
		}
		return venueObj;
	}
	public void setVenueObj(Venue venueObj) {
		this.venueObj = venueObj;
	}
	public Artist getArtistObj() {
		if(null != artistId){
			artistObj =TMATDAORegistry.getArtistDAO().get(artistId);
		}
		return artistObj;
	}
	public void setArtistObj(Artist artistObj) {
		this.artistObj = artistObj;
	}*/
	public String getMapPath() {
		if(null ==mapPath || mapPath.isEmpty()){
			mapPath="";
		}
		return mapPath;
	}
	public void setMapPath(String mapPath) {
		this.mapPath = mapPath;
	}
	public Boolean getIsFavouriteEvent() {
		if(null == isFavouriteEvent ){
			isFavouriteEvent = false;
		}
		return isFavouriteEvent;
	}
	public void setIsFavouriteEvent(Boolean isFavouriteEvent) {
		this.isFavouriteEvent = isFavouriteEvent;
	}
	public String getVenueZipCode() {
		if(null ==venueZipCode || venueZipCode.isEmpty()){
			venueZipCode="";
		}
		return venueZipCode;
	}
	public void setVenueZipCode(String venueZipCode) {
		this.venueZipCode = venueZipCode;
	}
	public String getVenueCategoryName() {
		if(null ==venueCategoryName || venueCategoryName.isEmpty()){
			venueCategoryName="";
		}
		return venueCategoryName;
	}
	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}
	public Integer getTotalEvents() {
		if(null == totalEvents ){
			totalEvents = 1;
		}
		return totalEvents;
	}
	public void setTotalEvents(Integer totalEvents) {
		this.totalEvents = totalEvents;
	}
	public Integer getPriceStartFrom() {
		if(null == priceStartFrom ){
			priceStartFrom = 15;
		}
		return priceStartFrom;
	}
	public void setPriceStartFrom(Integer priceStartFrom) {
		this.priceStartFrom = priceStartFrom;
	}
	public Boolean getDiscountFlag() {
		if(null == discountFlag ){
			discountFlag = false;
		}
		return discountFlag;
	}
	public void setDiscountFlag(Boolean discountFlag) {
		this.discountFlag = discountFlag;
	}
	public String getParentCategoryName() {
		if(null ==parentCategoryName || parentCategoryName.isEmpty()){
			parentCategoryName="";
		}
		return parentCategoryName;
	}
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	public String getChildCategoryName() {
		if(null ==childCategoryName || childCategoryName.isEmpty()){
			childCategoryName="";
		}
		return childCategoryName;
	}
	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	public String getGrandChildCategoryName() {
		if(null ==grandChildCategoryName || grandChildCategoryName.isEmpty()){
			grandChildCategoryName="";
		}
		return grandChildCategoryName;
	}
	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}
	public Double getDiscountPerc() {
		return discountPerc;
	}
	
	public void setDiscountPerc(Double discountPerc) {
		this.discountPerc = discountPerc;
	}
	public String getSvgText() {
		return svgText;
	}
	public void setSvgText(String svgText) {
		this.svgText = svgText;
	}
	public String getIsMapWithSvg() {
		return isMapWithSvg;
	}
	public void setIsMapWithSvg(String isMapWithSvg) {
		this.isMapWithSvg = isMapWithSvg;
	}
	public String getSvgMapPath() {
		return svgMapPath;
	}
	public void setSvgMapPath(String svgMapPath) {
		this.svgMapPath = svgMapPath;
	}
	
	
}

