package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ArtistActionType;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("AddArtistDetails")
public class AddArtistDetails {
	private Integer status;
	private Error error; 
	private Integer customerId;
	private ArtistActionType artistActionType;
	private String actionResult;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	public String getActionResult() {
		return actionResult;
	}
	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public ArtistActionType getArtistActionType() {
		return artistActionType;
	}
	public void setArtistActionType(ArtistActionType artistActionType) {
		this.artistActionType = artistActionType;
	}
	
	
}
