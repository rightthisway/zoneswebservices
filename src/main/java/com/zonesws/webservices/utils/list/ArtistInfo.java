package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.jobs.TeamCardImageUtil;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.MapUtil;

/**
 * 
 * @author dthiyagarajan
 *
 */
public class ArtistInfo {
	private Integer status;
	private Error error;
	private Integer artistId;
	private String artistName;
	private String artistImage;
	private String artistInfo;
	private boolean hasArtistInfo;
	private List<Event> artistEvents;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public List<Event> getArtistEvents() {
		return artistEvents;
	}
	public void setArtistEvents(List<Event> artistEvents) {
		this.artistEvents = artistEvents;
	}
	public String getArtistInfo() {
		return artistInfo;
	}
	public void setArtistInfo(String artistInfo) {
		this.artistInfo = artistInfo;
	}
	public boolean getHasArtistInfo() {
		return hasArtistInfo;
	}
	public void setHasArtistInfo(boolean hasArtistInfo) {
		this.hasArtistInfo = hasArtistInfo;
	}
	public String getArtistImage() {
		return artistImage;
	}
	public void setArtistImage(ApplicationPlatForm applicationPlatForm) {
		if(null != artistId ){
			artistImage=TeamCardImageUtil.getTeamCardImageByArtist(artistId,applicationPlatForm);
		}else{
			artistImage = null;
		}
	}
	
	
}
