package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Artist;

@XStreamAlias("ArtistResult")
public class ArtistResult {
	
	
	private String artistName;
	private Integer artistId;
	private Boolean isFavoriteFlag;
	
	
	public ArtistResult(){
		
	}
	
	public ArtistResult(Artist artist){
		setArtistId(artist.getId());
		setArtistName(artist.getName());
		setIsFavoriteFlag(artist.getCustFavFlag());
	}
	
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public Boolean getIsFavoriteFlag() {
		if(null == isFavoriteFlag ){
			isFavoriteFlag = false;
		}
		return isFavoriteFlag;
	}
	public void setIsFavoriteFlag(Boolean isFavoriteFlag) {
		this.isFavoriteFlag = isFavoriteFlag;
	}
	
	
	
	
}
