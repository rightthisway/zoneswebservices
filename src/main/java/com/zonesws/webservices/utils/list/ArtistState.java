package com.zonesws.webservices.utils.list;


public class ArtistState {
	
	private Integer artistId;
	private String artistName;
	private String state;
	
	
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}	
	
	
	
	
	
}
