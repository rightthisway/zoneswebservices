package com.zonesws.webservices.utils.list;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("AutoPageResult")
public class AutoPageResult {
	
	private String name;
	private Integer id;
	private Boolean isFavoriteFlag;
	private Boolean isArtist;
	private Boolean isVenue;
	@JsonIgnore
	private Integer totalRows;
	
	
	public AutoPageResult(){
		
	}
	
	
	public Boolean getIsFavoriteFlag() {
		if(null == isFavoriteFlag ){
			isFavoriteFlag = false;
		}
		return isFavoriteFlag;
	}
	public void setIsFavoriteFlag(Boolean isFavoriteFlag) {
		this.isFavoriteFlag = isFavoriteFlag;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Boolean getIsArtist() {
		return isArtist;
	}


	public void setIsArtist(Boolean isArtist) {
		this.isArtist = isArtist;
	}


	public Boolean getIsVenue() {
		return isVenue;
	}


	public void setIsVenue(Boolean isVenue) {
		this.isVenue = isVenue;
	}


	public Integer getTotalRows() {
		return totalRows;
	}


	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}
	
	
	
	
}
