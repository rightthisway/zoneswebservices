package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.data.RTFCategorySearch;

@XStreamAlias("AutoSearchResults")
public class AutoSearchResult {
	
	private Integer totalArtistCount;
	private Integer totalVenueCount;
	private Integer totalEventCount;
	private Integer totalGrandChildCount;
	private Integer totalEventPages;
	private Integer totalArtistPages;
	private Integer totalVenuePages;
	private Boolean sportsChildOrTeam;
	private Boolean showAutoSeeAll;
	private List<AutoPageResult> autoPageResults;
	private List<GrandChildCategory> grandChildCategories;
	private List<ArtistResult> artistResults;
	private List<VenueResult> venueResults;
	private List<Event> events;

	
	public List<ArtistResult> getArtistResults() {
		return artistResults;
	}
	public void setArtistResults(List<ArtistResult> artistResults) {
		this.artistResults = artistResults;
	}
	public List<VenueResult> getVenueResults() {
		return venueResults;
	}
	public void setVenueResults(List<VenueResult> venueResults) {
		this.venueResults = venueResults;
	}
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	public Integer getTotalArtistCount() {
		return totalArtistCount;
	}
	public void setTotalArtistCount(Integer totalArtistCount) {
		this.totalArtistCount = totalArtistCount;
	}
	public Integer getTotalVenueCount() {
		return totalVenueCount;
	}
	public void setTotalVenueCount(Integer totalVenueCount) {
		this.totalVenueCount = totalVenueCount;
	}
	public Integer getTotalEventCount() {
		return totalEventCount;
	}
	public void setTotalEventCount(Integer totalEventCount) {
		this.totalEventCount = totalEventCount;
	}
	public Integer getTotalGrandChildCount() {
		return totalGrandChildCount;
	}
	public void setTotalGrandChildCount(Integer totalGrandChildCount) {
		this.totalGrandChildCount = totalGrandChildCount;
	}
	public Boolean getSportsChildOrTeam() {
		return sportsChildOrTeam;
	}
	public void setSportsChildOrTeam(Boolean sportsChildOrTeam) {
		this.sportsChildOrTeam = sportsChildOrTeam;
	}
	public List<GrandChildCategory> getGrandChildCategories() {
		return grandChildCategories;
	}
	public void setGrandChildCategories(
			List<GrandChildCategory> grandChildCategories) {
		this.grandChildCategories = grandChildCategories;
	}
	public Integer getTotalEventPages() {
		return totalEventPages;
	}
	public void setTotalEventPages(Integer totalEventPages) {
		this.totalEventPages = totalEventPages;
	}
	public Integer getTotalArtistPages() {
		return totalArtistPages;
	}
	public void setTotalArtistPages(Integer totalArtistPages) {
		this.totalArtistPages = totalArtistPages;
	}
	public Integer getTotalVenuePages() {
		return totalVenuePages;
	}
	public void setTotalVenuePages(Integer totalVenuePages) {
		this.totalVenuePages = totalVenuePages;
	}
	
	public List<AutoPageResult> getAutoPageResults() {
		return autoPageResults;
	}
	public void setAutoPageResults(List<AutoPageResult> autoPageResults) {
		this.autoPageResults = autoPageResults;
	}
	public Boolean getShowAutoSeeAll() {
		return showAutoSeeAll;
	}
	public void setShowAutoSeeAll(Boolean showAutoSeeAll) {
		this.showAutoSeeAll = showAutoSeeAll;
	}
	
	
		
}
