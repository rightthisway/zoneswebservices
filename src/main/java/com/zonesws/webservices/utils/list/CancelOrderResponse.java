package com.zonesws.webservices.utils.list;

import com.zonesws.webservices.utils.Error;

public class CancelOrderResponse {

	private Integer status;
	private Error error;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
}
