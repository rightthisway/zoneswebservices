package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.CardType;

@XStreamAlias("Card")
public class Card {
	
	private CardType cardType;
	private String cardName;
	private String cardImage;
	private String cardImageForMobile;
	private String imageText;
	private String yesUrl;
	private String noUrl;
	private Integer artistId;
	private Boolean isFavoriteArtist;
	private List<Event> events;
	
	private String promoCardType;//ARTIST,VENUE,CHILD,GRAND,PARENT
	
	
	
	public CardType getCardType() {
		return cardType;
	}
	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}
	public String getCardImage() {
		return cardImage;
	}
	public void setCardImage(String cardImage) {
		this.cardImage = cardImage;
	}
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	public Boolean getIsFavoriteArtist() {
		if(null == isFavoriteArtist){
			isFavoriteArtist = false;
		}
		return isFavoriteArtist;
	}
	public void setIsFavoriteArtist(Boolean isFavoriteArtist) {
		this.isFavoriteArtist = isFavoriteArtist;
	}
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public String getImageText() {
		return imageText;
	}
	public void setImageText(String imageText) {
		this.imageText = imageText;
	}
	public String getYesUrl() {
		return yesUrl;
	}
	public void setYesUrl(String yesUrl) {
		this.yesUrl = yesUrl;
	}
	public String getNoUrl() {
		return noUrl;
	}
	public void setNoUrl(String noUrl) {
		this.noUrl = noUrl;
	}
	public String getPromoCardType() {
		return promoCardType;
	}
	public void setPromoCardType(String promoCardType) {
		this.promoCardType = promoCardType;
	}
	public String getCardImageForMobile() {
		if(null == cardImageForMobile || cardImageForMobile.isEmpty()){
			cardImageForMobile = cardImage;
		}
		return cardImageForMobile;
	}
	public void setCardImageForMobile(String cardImageForMobile) {
		this.cardImageForMobile = cardImageForMobile;
	}
	
	
}
