package com.zonesws.webservices.utils.list;

import java.util.Collection;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CountryCity;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CityDetails")
public class CityDetails {
	private Integer status;
	private Error error; 
	private Boolean noZipCode;
	private CountryCity cityObj;
	private Collection<CountryCity> cityList;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Collection<CountryCity> getCityList() {
		return cityList;
	}
	public void setCityList(Collection<CountryCity> cityList) {
		this.cityList = cityList;
	}
	public CountryCity getCityObj() {
		return cityObj;
	}
	public void setCityObj(CountryCity cityObj) {
		this.cityObj = cityObj;
	}
	
	
	public Boolean getNoZipCode() {
		return noZipCode;
	}
	public void setNoZipCode(Boolean noZipCode) {
		this.noZipCode = noZipCode;
	}
	
	

	
	
}
