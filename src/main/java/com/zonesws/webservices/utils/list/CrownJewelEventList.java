package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CrownJewelEventList")
public class CrownJewelEventList {
	private Integer status;
	private Error error;
	private Integer leagueId;
	private String leagueName;
	private String leagueCity;
	private List<Event> events;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getLeagueName() {
		return leagueName;
	}
	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	public String getLeagueCity() {
		return leagueCity;
	}
	public void setLeagueCity(String leagueCity) {
		this.leagueCity = leagueCity;
	}
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	
	
	
	
}
