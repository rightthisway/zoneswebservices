package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.CrownJewelTeams;
import com.zonesws.webservices.utils.Error;

public class CrownJewelEventsResponse {
	
	private Integer status;
	private Error error;
	private List<String> zoneList;
	private List<String> qty;
	private Integer teamId;
	private Integer availablePoints;
	private Integer pointsNeeded;
	private String message;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<String> getQty() {
		return qty;
	}
	public void setQty(List<String> qty) {
		this.qty = qty;
	}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	public Integer getAvailablePoints() {
		return availablePoints;
	}
	public void setAvailablePoints(Integer availablePoints) {
		this.availablePoints = availablePoints;
	}
	public Integer getPointsNeeded() {
		return pointsNeeded;
	}
	public void setPointsNeeded(Integer pointsNeeded) {
		this.pointsNeeded = pointsNeeded;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<String> getZoneList() {
		return zoneList;
	}
	public void setZoneList(List<String> zoneList) {
		this.zoneList = zoneList;
	}
	
}
