package com.zonesws.webservices.utils.list;

import com.zonesws.webservices.data.CustomerFantasyOrder;
import com.zonesws.webservices.utils.Error;

public class CrownJewelViewOrder {

	private Integer status;
	private Error error;
	private String leagueParentType;
	private String redeemPoints;
	private String headerMessage;
	private String sportsOrderInfo;
	private CustomerFantasyOrder order;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public CustomerFantasyOrder getOrder() {
		return order;
	}
	public void setOrder(CustomerFantasyOrder order) {
		this.order = order;
	}
	public String getHeaderMessage() {
		return headerMessage;
	}
	public void setHeaderMessage(String headerMessage) {
		this.headerMessage = headerMessage;
	}
	public String getRedeemPoints() {
		return redeemPoints;
	}
	public void setRedeemPoints(String redeemPoints) {
		this.redeemPoints = redeemPoints;
	}
	
	public String getLeagueParentType() {
		return leagueParentType;
	}
	public void setLeagueParentType(String leagueParentType) {
		this.leagueParentType = leagueParentType;
	}
	public String getSportsOrderInfo() {
		return sportsOrderInfo;
	}
	public void setSportsOrderInfo(String sportsOrderInfo) {
		this.sportsOrderInfo = sportsOrderInfo;
	}
	
	
	
}
