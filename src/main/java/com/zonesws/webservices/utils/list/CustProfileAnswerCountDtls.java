package com.zonesws.webservices.utils.list;

public class CustProfileAnswerCountDtls {
	
	private Integer custId;
	private Integer ansCount;
	private Integer questCount;
	
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getAnsCount() {
		if(ansCount == null) {
			ansCount = 0;
		}
		return ansCount;
	}
	public void setAnsCount(Integer ansCount) {
		this.ansCount = ansCount;
	}
	public Integer getQuestCount() {
		if(questCount == null) {
			questCount = 0;
		}
		return questCount;
	}
	public void setQuestCount(Integer questCount) {
		this.questCount = questCount;
	}
	
	
	

}
