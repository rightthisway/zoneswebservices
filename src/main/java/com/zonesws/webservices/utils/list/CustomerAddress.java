package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.enums.AddressType;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerAddress")
public class CustomerAddress {
	private Integer status;
	private Error error; 
	private String message;
	private AddressType revisedAddressType;
	private Integer revisedId;
	//private ResetPassword resetPassword;
	//private CustomerLoyalty customerLoyalty; 
	private Customer customer;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getMessage() {
		  return message;
	}
	public void setMessage(String message) {
	  this.message = message;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Integer getRevisedId() {
		return revisedId;
	}
	public void setRevisedId(Integer revisedId) {
		this.revisedId = revisedId;
	}
	public AddressType getRevisedAddressType() {
		return revisedAddressType;
	}
	public void setRevisedAddressType(AddressType revisedAddressType) {
		this.revisedAddressType = revisedAddressType;
	}
	
		
}
