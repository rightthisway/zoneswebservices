package com.zonesws.webservices.utils.list;

import java.util.List;

import com.rtf.ecomerce.data.CountryPhoneCode;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerCardInfo;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.ResetPassword;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerInfo")
public class CustomerDetails {
	private Integer status;
	private Error error; 
	private ResetPassword resetPassword;
	private CustomerLoyalty customerLoyalty; 
	private Customer customer;
	private String customerProfilePicWebView;
	private String customerPicWebView;
	private String message;
	private List<CustomerCardInfo> cardList;
	private List<CountryPhoneCode> phoneCodes;
	private Integer revisedId;
	private Boolean showPromoAert;
	private String promoAlertMessage;
	private Boolean showAuthDialog;
	
	private Integer customerId;
	private String loginOTP;
	private Integer otpExpiryTimeInSeconds;
	private Integer otpTrackingId;
	
	private Boolean showUserIdPopup;
	private Boolean showOtpPopup;
	
	private Boolean isOtherCustPhoneNo= false;
	private String otherCustEmail;
	private Boolean isUserIdEditable = true;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public ResetPassword getResetPassword() {
		return resetPassword;
	}
	public void setResetPassword(ResetPassword resetPassword) {
		this.resetPassword = resetPassword;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public CustomerLoyalty getCustomerLoyalty() {
		return customerLoyalty;
	}
	public void setCustomerLoyalty(CustomerLoyalty customerLoyalty) {
		this.customerLoyalty = customerLoyalty;
	}
	public String getCustomerProfilePicWebView() {
		return customerProfilePicWebView;
	}
	public void setCustomerProfilePicWebView(String customerProfilePicWebView) {
		this.customerProfilePicWebView = customerProfilePicWebView;
	}
	public List<CustomerCardInfo> getCardList() {
		return cardList;
	}
	public void setCardList(List<CustomerCardInfo> cardList) {
		this.cardList = cardList;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getRevisedId() {
		return revisedId;
	}
	public void setRevisedId(Integer revisedId) {
		this.revisedId = revisedId;
	}
	public Boolean getShowPromoAert() {
		if(null == showPromoAert){
			showPromoAert = false;
		}
		return showPromoAert;
	}
	public void setShowPromoAert(Boolean showPromoAert) {
		this.showPromoAert = showPromoAert;
	}
	public String getPromoAlertMessage() {
		if(null == promoAlertMessage){
			promoAlertMessage="";
		}
		return promoAlertMessage;
	}
	public void setPromoAlertMessage(String promoAlertMessage) {
		this.promoAlertMessage = promoAlertMessage;
	}
	public Boolean getShowAuthDialog() {
		if(showAuthDialog == null){
			showAuthDialog = true;
		}
		return showAuthDialog;
	}
	public void setShowAuthDialog(Boolean showAuthDialog) {
		this.showAuthDialog = showAuthDialog;
	}
	public String getLoginOTP() {
		if(null == loginOTP){
			loginOTP="";
		}
		return loginOTP;
	}
	public void setLoginOTP(String loginOTP) {
		this.loginOTP = loginOTP;
	}
	public Integer getOtpExpiryTimeInSeconds() {
		return otpExpiryTimeInSeconds;
	}
	public void setOtpExpiryTimeInSeconds(Integer otpExpiryTimeInSeconds) {
		this.otpExpiryTimeInSeconds = otpExpiryTimeInSeconds;
	}
	public Integer getOtpTrackingId() {
		return otpTrackingId;
	}
	public void setOtpTrackingId(Integer otpTrackingId) {
		this.otpTrackingId = otpTrackingId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Boolean getShowUserIdPopup() {
		if(showUserIdPopup == null) {
			showUserIdPopup = false;
		}
		return showUserIdPopup;
	}
	public void setShowUserIdPopup(Boolean showUserIdPopup) {
		this.showUserIdPopup = showUserIdPopup;
	}
	public Boolean getShowOtpPopup() {
		if(showOtpPopup == null) {
			showOtpPopup = false;
		}
		return showOtpPopup;
	}
	public void setShowOtpPopup(Boolean showOtpPopup) {
		this.showOtpPopup = showOtpPopup;
	}
	public Boolean getIsOtherCustPhoneNo() {
		if(isOtherCustPhoneNo == null) {
			isOtherCustPhoneNo = false;
		}
		return isOtherCustPhoneNo;
	}
	public void setIsOtherCustPhoneNo(Boolean isOtherCustPhoneNo) {
		this.isOtherCustPhoneNo = isOtherCustPhoneNo;
	}
	public String getOtherCustEmail() {
		if(otherCustEmail == null) {
			otherCustEmail = "";
		}
		return otherCustEmail;
	}
	public void setOtherCustEmail(String otherCustEmail) {
		this.otherCustEmail = otherCustEmail;
	}
	public Boolean getIsUserIdEditable() {
		if(isUserIdEditable == null) {
			isUserIdEditable = true;
		}
		return isUserIdEditable;
	}
	public void setIsUserIdEditable(Boolean isUserIdEditable) {
		this.isUserIdEditable = isUserIdEditable;
	}
	public final String getCustomerPicWebView() {
		if(null == customerPicWebView) {
			customerPicWebView ="";
		}
		return customerPicWebView;
	}
	public final void setCustomerPicWebView(String customerPicWebView) {
		this.customerPicWebView = customerPicWebView;
	}
	
	public List<CountryPhoneCode> getPhoneCodes() {
		return phoneCodes;
	}
	public void setPhoneCodes(List<CountryPhoneCode> phoneCodes) {
		this.phoneCodes = phoneCodes;
	}
	
}
