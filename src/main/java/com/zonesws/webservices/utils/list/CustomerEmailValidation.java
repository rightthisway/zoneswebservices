package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerEmailValidation")
public class CustomerEmailValidation {
	private Integer status;
	private Boolean isNewEmail;
	private Error error; 
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Boolean getIsNewEmail() {
		return isNewEmail;
	}
	public void setIsNewEmail(Boolean isNewEmail) {
		this.isNewEmail = isNewEmail;
	}
	
}
