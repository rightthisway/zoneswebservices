package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.ResetPassword;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerLoyaltyInfo")
public class CustomerLoyaltyDetails {
	private Integer status;
	private Error error; 
	private ResetPassword resetPassword;
	private CustomerLoyalty customerLoyalty; 
	private Customer customer;
	private String conversionMeassage;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	/*public ResetPassword getResetPassword() {
		return resetPassword;
	}
	public void setResetPassword(ResetPassword resetPassword) {
		this.resetPassword = resetPassword;
	}*/
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getConversionMeassage() {
		return conversionMeassage;
	}
	public void setConversionMeassage(String conversionMeassage) {
		this.conversionMeassage = conversionMeassage;
	}
	
	public CustomerLoyalty getCustomerLoyalty() {
		return customerLoyalty;
	}
	public void setCustomerLoyalty(CustomerLoyalty customerLoyalty) {
		this.customerLoyalty = customerLoyalty;
		
	}
	
}

