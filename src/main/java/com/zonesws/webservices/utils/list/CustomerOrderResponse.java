package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.data.RtfGiftCardOrder;
import com.zonesws.webservices.utils.Error;

public class CustomerOrderResponse {

	private Integer status;
	private Error error;
	private String svgWebViewUrl;
	private List<CustomerOrder> customerOrders;
	
	//private String ticketDownloadUrl;
	
	private List<CustomerOrder> pastOrders;
	/*private Boolean showInfoButton;
	private String deliveryInfo;
	private String shippingMethod;*/
	
	private Boolean eligibleToShareRefCode;
	private String shareErrorMessage;
	
	private String custRewards;
	
	private List<RtfGiftCardOrder> giftCardOrders;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<CustomerOrder> getCustomerOrders() {
		return customerOrders;
	}
	public void setCustomerOrders(List<CustomerOrder> customerOrders) {
		this.customerOrders = customerOrders;
	}
	public String getSvgWebViewUrl() {
		return svgWebViewUrl;
	}
	public void setSvgWebViewUrl(String svgWebViewUrl) {
		this.svgWebViewUrl = svgWebViewUrl;
	}
	/*public String getTicketDownloadUrl() {
		return ticketDownloadUrl;
	}
	public void setTicketDownloadUrl(String ticketDownloadUrl) {
		this.ticketDownloadUrl = ticketDownloadUrl;
	}*/
	public List<CustomerOrder> getPastOrders() {
		return pastOrders;
	}
	public void setPastOrders(List<CustomerOrder> pastOrders) {
		this.pastOrders = pastOrders;
	}
	/*public Boolean getShowInfoButton() {
		return showInfoButton;
	}
	public void setShowInfoButton(Boolean showInfoButton) {
		this.showInfoButton = showInfoButton;
	}
	public String getDeliveryInfo() {
		return deliveryInfo;
	}
	public void setDeliveryInfo(String deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}*/
	
	public Boolean getEligibleToShareRefCode() {
		if(null == eligibleToShareRefCode){
			eligibleToShareRefCode = false;
		}
		return eligibleToShareRefCode;
	}
	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}
	
	public String getShareErrorMessage() {
		if(null == shareErrorMessage){
			shareErrorMessage="";
		}
		return shareErrorMessage;
	}
	public void setShareErrorMessage(String shareErrorMessage) {
		this.shareErrorMessage = shareErrorMessage;
	}
	public String getCustRewards() {
		if(null == custRewards) {
			custRewards = "";
		}
		return custRewards;
	}
	public void setCustRewards(String custRewards) {
		this.custRewards = custRewards;
	}
	public List<RtfGiftCardOrder> getGiftCardOrders() {
		return giftCardOrders;
	}
	public void setGiftCardOrders(List<RtfGiftCardOrder> giftCardOrders) {
		this.giftCardOrders = giftCardOrders;
	}
	
}
