package com.zonesws.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.ContestGrandWinner;
import com.zonesws.webservices.data.CustomerOrder;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TextUtil;

public class CustomerOrdersResponse {

	private Integer status;
	private Error error;
	private String svgWebViewUrl;
	private List<CustomerOrder> customerOrders;
	
	//private String ticketDownloadUrl;
	
	private List<CustomerOrder> pastOrders;
/*	private Boolean showInfoButton;
	private String deliveryInfo;
	private String shippingMethod;*/
	
	private List<ContestGrandWinner> contestGrandWinners;
	
	private String contestTicketsConfirmDialog;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<CustomerOrder> getCustomerOrders() {
		return customerOrders;
	}
	public void setCustomerOrders(List<CustomerOrder> customerOrders) {
		this.customerOrders = customerOrders;
	}
	public String getSvgWebViewUrl() {
		return svgWebViewUrl;
	}
	public void setSvgWebViewUrl(String svgWebViewUrl) {
		this.svgWebViewUrl = svgWebViewUrl;
	}
	/*public String getTicketDownloadUrl() {
		return ticketDownloadUrl;
	}
	public void setTicketDownloadUrl(String ticketDownloadUrl) {
		this.ticketDownloadUrl = ticketDownloadUrl;
	}*/
	public List<CustomerOrder> getPastOrders() {
		return pastOrders;
	}
	public void setPastOrders(List<CustomerOrder> pastOrders) {
		this.pastOrders = pastOrders;
	}
	/*public Boolean getShowInfoButton() {
		return showInfoButton;
	}
	public void setShowInfoButton(Boolean showInfoButton) {
		this.showInfoButton = showInfoButton;
	}
	public String getDeliveryInfo() {
		return deliveryInfo;
	}
	public void setDeliveryInfo(String deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}*/
	public List<ContestGrandWinner> getContestGrandWinners() {
		return contestGrandWinners;
	}
	public void setContestGrandWinners(List<ContestGrandWinner> contestGrandWinners) {
		this.contestGrandWinners = contestGrandWinners;
	}
	public String getContestTicketsConfirmDialog() {
		return contestTicketsConfirmDialog;
	}
	public void setContestTicketsConfirmDialog(String contestTicketsConfirmDialog) {
		this.contestTicketsConfirmDialog = contestTicketsConfirmDialog;
	}
	
}
