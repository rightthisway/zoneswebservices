package com.zonesws.webservices.utils.list;

import com.zonesws.webservices.utils.Error;


public class CustomerProfilePic {

	private Integer customerId;
	private Integer status;
	private String description;
	private Error error;
	/*private String customerProfilePicPath;*/
	private String customerProfilePicWebView;
	
	/**
	 * Getters and setters
	 * @return
	 */
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	/*public String getCustomerPicPath() {
		return customerProfilePicPath;
	}
	public void setCustomerPicPath(String customerPicPath) {
		this.customerProfilePicPath = customerPicPath;
	}*/
	public String getCustomerPicWebView() {
		return customerProfilePicWebView;
	}
	public void setCustomerPicWebView(String customerPicWebView) {
		this.customerProfilePicWebView = customerPicWebView;
	} 
	
}
