package com.zonesws.webservices.utils.list;

import java.util.Collection;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("CustomerRewards")
public class CustomerRewards {
	private Integer status;
	private String message;
	private Error error; 
	private CustomerLoyalty customerLoyalty;
	private List<RewardDetails> rewardList;
	private Collection<ReferralRewardDetails> referralRewardsList;
	private List<ReferralDTO> referrals;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public CustomerLoyalty getCustomerLoyalty() {
		return customerLoyalty;
	}
	public void setCustomerLoyalty(CustomerLoyalty customerLoyalty) {
		this.customerLoyalty = customerLoyalty;
	}
	public List<RewardDetails> getRewardList() {
		return rewardList;
	}
	public void setRewardList(List<RewardDetails> rewardList) {
		this.rewardList = rewardList;
	}
	public Collection<ReferralRewardDetails> getReferralRewardsList() {
		return referralRewardsList;
	}
	public void setReferralRewardsList(Collection<ReferralRewardDetails> referralRewardsList) {
		this.referralRewardsList = referralRewardsList;
	}
	public List<ReferralDTO> getReferrals() {
		return referrals;
	}
	public void setReferrals(List<ReferralDTO> referrals) {
		this.referrals = referrals;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
