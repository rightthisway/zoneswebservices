package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Customer;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.ResetPassword;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("DownloadInfo")
public class Downloaddetails {
	private Integer status;
	private Error error; 
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	/*public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}*/
	/*public ResetPassword getResetPassword() {
		return resetPassword;
	}
	public void setResetPassword(ResetPassword resetPassword) {
		this.resetPassword = resetPassword;
	}*/
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}

