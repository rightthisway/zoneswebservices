package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TicketGroupQty;

@XStreamAlias("EventDetails")
public class EventDetails {
	private Integer status;
	private Error error;
	private Boolean superFanFlag;
	private Boolean favoriteFlag;
	private Event event;
	private List<TicketGroupQty> ticketGroupQtyList;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public Boolean getSuperFanFlag() {
		return superFanFlag;
	}

	public void setSuperFanFlag(Boolean superFanFlag) {
		this.superFanFlag = superFanFlag;
	}

	public Boolean getFavoriteFlag() {
		return favoriteFlag;
	}

	public void setFavoriteFlag(Boolean favoriteFlag) {
		this.favoriteFlag = favoriteFlag;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public List<TicketGroupQty> getTicketGroupQtyList() {
		return ticketGroupQtyList;
	}

	public void setTicketGroupQtyList(List<TicketGroupQty> ticketGroupQtyList) {
		this.ticketGroupQtyList = ticketGroupQtyList;
	}
	
	
	
	
}
