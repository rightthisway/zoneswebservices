package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.ZoneEvent;

@XStreamAlias("List")
public class EventList {
	private Integer status;
	private Error error; 
	private Integer noOfEvents;
	private Integer noOfPages;
	private Integer pageNumber;
	private Integer eventsPerPage;
	/*private List<Event> events;
	private List<Artist> artists;
	private List<Venue> venues;*/
	
	private List<ZoneEvent> zoneEvents;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	
	public List<ZoneEvent> getZoneEvents() {
		return zoneEvents;
	}
	public void setZoneEvents(List<ZoneEvent> zoneEvents) {
		this.zoneEvents = zoneEvents;
	}
	public Integer getNoOfEvents() {
		if(null == noOfEvents){
			noOfEvents=0;	
		}
		return noOfEvents;
	}
	public void setNoOfEvents(Integer noOfEvents) {
		this.noOfEvents = noOfEvents;
	}
	public Integer getNoOfPages() {
		if(null == noOfPages){
			noOfPages=0;	
		}
		return noOfPages;
	}
	public void setNoOfPages(Integer noOfPages) {
		this.noOfPages = noOfPages;
	}
	public Integer getPageNumber() {
		if(null == pageNumber){
			pageNumber=0;	
		}
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getEventsPerPage() {
		if(null == eventsPerPage){
			eventsPerPage=0;	
		}
		return eventsPerPage;
	}
	public void setEventsPerPage(Integer eventsPerPage) {
		this.eventsPerPage = eventsPerPage;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
}
