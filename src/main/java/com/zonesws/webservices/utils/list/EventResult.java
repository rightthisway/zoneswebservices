package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("EventResult")
public class EventResult {
	
	private Integer status;
	private Error error; 
	private AutoSearchResult autoSearchResult;
	private NormalSearchResult normalSearchResult;
	private Boolean eligibleToShareRefCode;
	private String shareErrorMessage;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public AutoSearchResult getAutoSearchResult() {
		return autoSearchResult;
	}
	public void setAutoSearchResult(AutoSearchResult autoSearchResult) {
		this.autoSearchResult = autoSearchResult;
	}
	public NormalSearchResult getNormalSearchResult() {
		return normalSearchResult;
	}
	public void setNormalSearchResult(NormalSearchResult normalSearchResult) {
		this.normalSearchResult = normalSearchResult;
	}
	
	public Boolean getEligibleToShareRefCode() {
		if(null == eligibleToShareRefCode){
			eligibleToShareRefCode = false;
		}
		return eligibleToShareRefCode;
	}
	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}
	
	public String getShareErrorMessage() {
		if(null == shareErrorMessage){
			shareErrorMessage="";
		}
		return shareErrorMessage;
	}
	public void setShareErrorMessage(String shareErrorMessage) {
		this.shareErrorMessage = shareErrorMessage;
	}
	
	
	
}
