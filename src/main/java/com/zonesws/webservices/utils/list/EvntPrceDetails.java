package com.zonesws.webservices.utils.list;

import java.sql.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.enums.Status;
import com.zonesws.webservices.utils.DateConverter;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.HoldTicketGroup;
import com.zonesws.webservices.utils.SoldTicketGroup;

@XStreamAlias("EventPriceDetails")
public class EvntPrceDetails {
	
	private Error error; 
	private Integer eventId;
	private String eventName;
	@XStreamConverter(DateConverter.class)
	private Date eventDate;
	private String eventTime;
	private Status status;
	
	private Double minPrice;
	private Double maxPrice;
	
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public Double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}
	public Double getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
}
