package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ExploreType;

@XStreamAlias("ExploreCard")
public class ExploreCard {

	private String cardName;
	private String imageUrl;
	private ExploreType exploreType;
	private String eventSearchKey;
	
	
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getEventSearchKey() {
		return eventSearchKey;
	}
	public void setEventSearchKey(String eventSearchKey) {
		this.eventSearchKey = eventSearchKey;
	}
	public ExploreType getExploreType() {
		return exploreType;
	}
	public void setExploreType(ExploreType exploreType) {
		this.exploreType = exploreType;
	}
	
	
}
