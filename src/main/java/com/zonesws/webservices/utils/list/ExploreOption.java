package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("ExploreOption")
public class ExploreOption {
	
	private Integer status;
	private Error error; 
	private List<ExploreCard> exploreCards;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<ExploreCard> getExploreCards() {
		return exploreCards;
	}
	public void setExploreCards(List<ExploreCard> exploreCards) {
		this.exploreCards = exploreCards;
	}
	
	
	
}
