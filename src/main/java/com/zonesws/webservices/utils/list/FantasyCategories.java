package com.zonesws.webservices.utils.list;

import java.util.List;

import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.ApplicationPlatForm;
import com.zonesws.webservices.utils.MapUtil;

@XStreamAlias("FantasyCategories")
public class FantasyCategories {
	
	private Integer id;
	private String name;
	@JsonIgnore
	private String imageName;
	@JsonIgnore
	private String mobileImageName;
	private String imageURL;
	private String packageInformation;
	private Boolean showPackageHeader;
	private String packageHeader;
	private Boolean showEventInfo;
	private String eventInfoText;
	private List<FantasyEventTeamTicket> fantasyTeamTickets;
	@JsonIgnore
	private Double packageCost;
	@JsonIgnore
	private String pkgInfoLine1;
	@JsonIgnore
	private String pkgInfoLine2;
	@JsonIgnore
	private String pkgInfoLine3;
	@JsonIgnore
	private String pkgInfoLine4;
	@JsonIgnore
	private String pkgInfoLine5;
	@JsonIgnore
	private String pkgInfoLine6;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	@org.codehaus.jackson.annotate.JsonIgnore
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	public String getPackageInformation() {
		return packageInformation;
	}
	public void setPackageInformation(String packageInformation) {
		this.packageInformation = packageInformation;
	}
	public List<FantasyEventTeamTicket> getFantasyTeamTickets() {
		return fantasyTeamTickets;
	}
	public void setFantasyTeamTickets(
			List<FantasyEventTeamTicket> fantasyTeamTickets) {
		this.fantasyTeamTickets = fantasyTeamTickets;
	}
	public Double getPackageCost() {
		return packageCost;
	}
	public void setPackageCost(Double packageCost) {
		this.packageCost = packageCost;
	}
	public String getPkgInfoLine1() {
		return pkgInfoLine1;
	}
	public void setPkgInfoLine1(String pkgInfoLine1) {
		this.pkgInfoLine1 = pkgInfoLine1;
	}
	public String getPkgInfoLine2() {
		return pkgInfoLine2;
	}
	public void setPkgInfoLine2(String pkgInfoLine2) {
		this.pkgInfoLine2 = pkgInfoLine2;
	}
	public String getPkgInfoLine3() {
		return pkgInfoLine3;
	}
	public void setPkgInfoLine3(String pkgInfoLine3) {
		this.pkgInfoLine3 = pkgInfoLine3;
	}
	public String getPkgInfoLine4() {
		return pkgInfoLine4;
	}
	public void setPkgInfoLine4(String pkgInfoLine4) {
		this.pkgInfoLine4 = pkgInfoLine4;
	}
	public String getPkgInfoLine5() {
		return pkgInfoLine5;
	}
	public void setPkgInfoLine5(String pkgInfoLine5) {
		this.pkgInfoLine5 = pkgInfoLine5;
	}
	public String getPkgInfoLine6() {
		return pkgInfoLine6;
	}
	public void setPkgInfoLine6(String pkgInfoLine6) {
		this.pkgInfoLine6 = pkgInfoLine6;
	}
	public Boolean getShowPackageHeader() {
		if(null == showPackageHeader){
			showPackageHeader = false;
		}
		return showPackageHeader;
	}
	public void setShowPackageHeader(Boolean showPackageHeader) {
		this.showPackageHeader = showPackageHeader;
	}
	public String getPackageHeader() {
		if(null == packageHeader){
			packageHeader = "";
		}
		return packageHeader;
	}
	public void setPackageHeader(String packageHeader) {
		this.packageHeader = packageHeader;
	}
	public Boolean getShowEventInfo() {
		if(null == showEventInfo){
			showEventInfo = false;
		}
		return showEventInfo;
	}
	public void setShowEventInfo(Boolean showEventInfo) {
		this.showEventInfo = showEventInfo;
	}
	public String getEventInfoText() {
		if(null == eventInfoText){
			eventInfoText = "";
		}
		return eventInfoText;
	}
	public void setEventInfoText(String eventInfoText) {
		this.eventInfoText = eventInfoText;
	}
	public String getMobileImageName() {
		return mobileImageName;
	}
	public void setMobileImageName(String mobileImageName) {
		this.mobileImageName = mobileImageName;
	}
	
	@JsonIgnore
	@Transient
	public void computeGrandChildImageURL(ApplicationPlatForm platForm){
		if(platForm.equals(ApplicationPlatForm.DESKTOP_SITE)){
			setImageURL(MapUtil.getFantasyGrandChildImages(imageName));
		}else{
			setImageURL(MapUtil.getFantasyGrandChildImages(mobileImageName));
		}
	}
	
}
