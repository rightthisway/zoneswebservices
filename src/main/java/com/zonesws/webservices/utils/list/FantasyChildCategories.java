package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FantasyChildCategories")
public class FantasyChildCategories {
	
	private Integer id;
	private String name;
	private List<FantasyGrandChildCategories> grandChildCategories;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FantasyGrandChildCategories> getGrandChildCategories() {
		return grandChildCategories;
	}
	public void setGrandChildCategories(
			List<FantasyGrandChildCategories> grandChildCategories) {
		this.grandChildCategories = grandChildCategories;
	}
	
}
