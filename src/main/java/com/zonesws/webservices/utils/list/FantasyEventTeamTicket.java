package com.zonesws.webservices.utils.list;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FantasyEventTeamTicket")
public class FantasyEventTeamTicket {
	
	private Integer categoryId;
	private Integer teamId;
	private String teamName;
	private Integer fantasyEventId;
	private Integer teamZoneId;
	private String zone;
	private String zoneText;
	private Integer quantity;
	private String qtyText;
	private Double requiredPoints;
	private String pointsText;
	private Integer ticketId;
	private Boolean isRealTicket;
	
	private Integer tmatEventId;
	@JsonIgnore
	private Integer ticketsCount;
	@JsonIgnore
	private Double price;
	
	private String eventDateTime;
	private String eventDateStr;
	private String eventTimeStr;
	private Boolean showEventDateAsTBD;
	private String eventDateTBDValue;
	private String venueName;
	private String city;
	private String state;
	private String country;
	
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getZoneText() {
		return zoneText;
	}
	public void setZoneText(String zoneText) {
		this.zoneText = zoneText;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getQtyText() {
		return qtyText;
	}
	public void setQtyText(String qtyText) {
		this.qtyText = qtyText;
	}
	public Double getRequiredPoints() {
		return requiredPoints;
	}
	public void setRequiredPoints(Double requiredPoints) {
		this.requiredPoints = requiredPoints;
	}
	public String getPointsText() {
		return pointsText;
	}
	public void setPointsText(String pointsText) {
		this.pointsText = pointsText;
	}
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public Boolean getIsRealTicket() {
		if(null == isRealTicket){
			isRealTicket = true;
		}
		return isRealTicket;
	}
	public void setIsRealTicket(Boolean isRealTicket) {
		this.isRealTicket = isRealTicket;
	}
	public Integer getTmatEventId() {
		return tmatEventId;
	}
	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	public Integer getTicketsCount() {
		return ticketsCount;
	}
	public void setTicketsCount(Integer ticketsCount) {
		this.ticketsCount = ticketsCount;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getEventDateTime() {
		return eventDateTime;
	}
	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public Boolean getShowEventDateAsTBD() {
		return showEventDateAsTBD;
	}
	public void setShowEventDateAsTBD(Boolean showEventDateAsTBD) {
		this.showEventDateAsTBD = showEventDateAsTBD;
	}
	public String getEventDateTBDValue() {
		return eventDateTBDValue;
	}
	public void setEventDateTBDValue(String eventDateTBDValue) {
		this.eventDateTBDValue = eventDateTBDValue;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getTeamZoneId() {
		return teamZoneId;
	}
	public void setTeamZoneId(Integer teamZoneId) {
		this.teamZoneId = teamZoneId;
	}
	
}
