package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FantasyGrandChildCategories")
public class FantasyGrandChildCategories {
	
	private Integer id;
	private String name;
	private List<FantasyLeagues> leagues;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FantasyLeagues> getLeagues() {
		return leagues;
	}
	public void setLeagues(List<FantasyLeagues> leagues) {
		this.leagues = leagues;
	}
}
