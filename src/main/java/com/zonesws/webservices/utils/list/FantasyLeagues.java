package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FantasyLeagues")
public class FantasyLeagues {
	
	private Integer id;
	private String name;
	private String eventDateTime;
	private String eventDateStr;
	private String eventTimeStr;
	private Boolean showEventDateAsTBD;
	private String eventDateTBDValue;
	private String venueName;
	private String city;
	private String state;
	private String country;
	
	private List<CrownJewelTicket> ticketsMap;
	
	
	public FantasyLeagues(){
	}
	
	public FantasyLeagues(FantasyTicketsBean bean){
		this.eventDateStr=bean.getEventDateStr();
		this.eventTimeStr=bean.getEventTimeStr();
		this.eventDateTime=bean.getEventDateTime();
		this.showEventDateAsTBD=bean.getShowEventDateAsTBD();
		this.eventDateTBDValue=bean.getEventDateTBDValue();
		this.venueName=bean.getVenueName();
		this.city=bean.getCity();
		this.state=bean.getState();
		this.country=bean.getCountry();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEventDateTime() {
		return eventDateTime;
	}
	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public Boolean getShowEventDateAsTBD() {
		return showEventDateAsTBD;
	}
	public void setShowEventDateAsTBD(Boolean showEventDateAsTBD) {
		this.showEventDateAsTBD = showEventDateAsTBD;
	}
	public String getEventDateTBDValue() {
		return eventDateTBDValue;
	}
	public void setEventDateTBDValue(String eventDateTBDValue) {
		this.eventDateTBDValue = eventDateTBDValue;
	}
	/*public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}*/
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public List<CrownJewelTicket> getTicketsMap() {
		return ticketsMap;
	}

	public void setTicketsMap(List<CrownJewelTicket> ticketsMap) {
		this.ticketsMap = ticketsMap;
	}
}
