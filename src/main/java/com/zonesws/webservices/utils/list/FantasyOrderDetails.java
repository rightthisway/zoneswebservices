package com.zonesws.webservices.utils.list;

import javax.persistence.Transient;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CustomerFantasyOrder;

@XStreamAlias("FantasyOrderDetails")
public class FantasyOrderDetails {
	
	private Integer orderNo;
	private Integer customerId;
	private String eventName;
	private String teamName;
	private String packageDetails;
	private Integer qty;
	private String zone;
	private Double orderPointsASDouble;
	private String orderPoints;
	private String venueName;
	private String city;
	private String state;
	private String country;
	private String fantasyOrderText;
	private String deliveryInfo;
	private String buttonOption;
	private Boolean showInfoButton;
	
	public FantasyOrderDetails() {
		// TODO Auto-generated constructor stub
	}
	
	public FantasyOrderDetails(CustomerFantasyOrder fantasyOrder) {
		this.orderNo = fantasyOrder.getId();
		this.customerId = fantasyOrder.getCustomerId();
		this.eventName = fantasyOrder.getFantasyEventName();
		this.teamName = fantasyOrder.getTeamName();
		this.qty = fantasyOrder.getQuantity();
		this.zone = fantasyOrder.getZone();
		this.orderPointsASDouble = fantasyOrder.getRequiredPoints();
		this.venueName = fantasyOrder.getVenueName();
		this.city = fantasyOrder.getCity();
		this.state = fantasyOrder.getState();
		this.country = fantasyOrder.getCountry();
	}
	
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getPackageDetails() {
		return packageDetails;
	}
	public void setPackageDetails(String packageDetails) {
		this.packageDetails = packageDetails;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Double getOrderPointsASDouble() {
		return orderPointsASDouble;
	}
	public void setOrderPointsASDouble(Double orderPointsASDouble) {
		this.orderPointsASDouble = orderPointsASDouble;
	}
	public String getOrderPoints() {
		return orderPoints;
	}
	public void setOrderPoints(String orderPoints) {
		this.orderPoints = orderPoints;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getFantasyOrderText() {
		return fantasyOrderText;
	}
	public void setFantasyOrderText(String fantasyOrderText) {
		this.fantasyOrderText = fantasyOrderText;
	}

	public String getDeliveryInfo() {
		return deliveryInfo;
	}

	public void setDeliveryInfo(String deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}

	public String getButtonOption() {
		return buttonOption;
	}

	public void setButtonOption(String buttonOption) {
		this.buttonOption = buttonOption;
	}

	public Boolean getShowInfoButton() {
		return showInfoButton;
	}

	public void setShowInfoButton(Boolean showInfoButton) {
		this.showInfoButton = showInfoButton;
	}
	
}
