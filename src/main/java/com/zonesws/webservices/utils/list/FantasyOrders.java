package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.CustomerFantasyOrder;
import com.zonesws.webservices.utils.Error;
public class FantasyOrders {

	private Integer status;
	private Error error;
	private Integer customerId;
	private List<CustomerFantasyOrder> activeOrders;
	private List<CustomerFantasyOrder> pastOrders;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public List<CustomerFantasyOrder> getActiveOrders() {
		return activeOrders;
	}
	public void setActiveOrders(List<CustomerFantasyOrder> activeOrders) {
		this.activeOrders = activeOrders;
	}
	public List<CustomerFantasyOrder> getPastOrders() {
		return pastOrders;
	}
	public void setPastOrders(List<CustomerFantasyOrder> pastOrders) {
		this.pastOrders = pastOrders;
	}
	
	
}
