package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CustomerFantasyOrder;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("FantasySportsProduct")
public class FantasySportsProduct {
	private Integer status;
	private Error error;
	private String message;
	private String ticTrackerMessage;
	private Boolean proceedPurchase;
	private List<FantasyCategories> fantasyCategories;
	private FantasyCategories fantasyEvent;
	private Integer fantasyOrderNo;
	private CustomerFantasyOrder fantasyOrder;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<FantasyCategories> getFantasyCategories() {
		return fantasyCategories;
	}
	public void setFantasyCategories(List<FantasyCategories> fantasyCategories) {
		this.fantasyCategories = fantasyCategories;
	}
	public FantasyCategories getFantasyEvent() {
		return fantasyEvent;
	}
	public void setFantasyEvent(FantasyCategories fantasyEvent) {
		this.fantasyEvent = fantasyEvent;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getProceedPurchase() {
		return proceedPurchase;
	}
	public void setProceedPurchase(Boolean proceedPurchase) {
		this.proceedPurchase = proceedPurchase;
	}
	public Integer getFantasyOrderNo() {
		return fantasyOrderNo;
	}
	public void setFantasyOrderNo(Integer fantasyOrderNo) {
		this.fantasyOrderNo = fantasyOrderNo;
	}
	public CustomerFantasyOrder getFantasyOrder() {
		return fantasyOrder;
	}
	public void setFantasyOrder(CustomerFantasyOrder fantasyOrder) {
		this.fantasyOrder = fantasyOrder;
	}
	public String getTicTrackerMessage() {
		return ticTrackerMessage;
	}
	public void setTicTrackerMessage(String ticTrackerMessage) {
		this.ticTrackerMessage = ticTrackerMessage;
	}
	
}
