package com.zonesws.webservices.utils.list;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;


public class FantasyTicketsBean {
	
	private Integer fantasyEventId;
	private String fantasyEventName;
	private Integer fantasyTeamId;
	private Double teamOdds;
	private Integer tmatEventId;
	private Boolean realEvent;
	private Date tempEventDate;
	private Date eventTime;
	private Date eventDate;
	private String eventDateTime;
	private String eventDateStr;
	private String eventTimeStr;
	private Boolean showEventDateAsTBD;
	private String eventDateTBDValue;
	private String venueName;
	private String city;
	private String state;
	private String country;
	private Boolean onlyPackageCost;
	
	@JsonIgnore
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	@JsonIgnore
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	@JsonIgnore
	private DateFormat tf = new SimpleDateFormat("hh:mm aa");
	
	
	public Integer getFantasyEventId() {
		return fantasyEventId;
	}
	public Boolean getOnlyPackageCost() {
		return onlyPackageCost;
	}
	public void setOnlyPackageCost(Boolean onlyPackageCost) {
		this.onlyPackageCost = onlyPackageCost;
	}
	public void setFantasyEventId(Integer fantasyEventId) {
		this.fantasyEventId = fantasyEventId;
	}
	public String getFantasyEventName() {
		return fantasyEventName;
	}
	public void setFantasyEventName(String fantasyEventName) {
		this.fantasyEventName = fantasyEventName;
	}
	public Integer getFantasyTeamId() {
		return fantasyTeamId;
	}
	public void setFantasyTeamId(Integer fantasyTeamId) {
		this.fantasyTeamId = fantasyTeamId;
	}
	public Integer getTmatEventId() {
		return tmatEventId;
	}
	public void setTmatEventId(Integer tmatEventId) {
		this.tmatEventId = tmatEventId;
	}
	public Boolean getRealEvent() {
		return realEvent;
	}
	public void setRealEvent(Boolean realEvent) {
		this.realEvent = realEvent;
	}

	public String getEventDateTime() {
		if(eventDate != null){
			String eventTimeStr="TBD",eventDateStr="";
			if(null != eventTime ){
				eventTimeStr = tf.format(eventTime);
			}
			eventDateStr = df.format(eventDate);
			eventDateTime=eventDateStr+" "+eventTimeStr;
		}
		return eventDateTime;
	}

	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	
	public String getEventDateStr() {
		if(eventDate != null){
			eventDateStr = df.format(eventDate);
		}
		return eventDateStr;
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	public String getEventTimeStr() {
		if(eventDate != null){
			String eventTimeLocal="TBD";
			if(null != eventTime ){
				eventTimeLocal = tf.format(eventTime);
			}
			eventTimeStr = eventTimeLocal;
		}
		return eventTimeStr;
	}

	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}

	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Date getTempEventDate() {
		return tempEventDate;
	}
	public void setTempEventDate(Date tempEventDate) {
		this.tempEventDate = tempEventDate;
	}
	public Boolean getShowEventDateAsTBD() {
		if(null == showEventDateAsTBD ){
			showEventDateAsTBD=false;
		}
		return showEventDateAsTBD;
	}
	public void setShowEventDateAsTBD(Boolean showEventDateAsTBD) {
		this.showEventDateAsTBD = showEventDateAsTBD;
	}
	public String getEventDateTBDValue() {
		if(null == eventDateTBDValue){
			eventDateTBDValue ="";
		}
		return eventDateTBDValue;
	}
	public void setEventDateTBDValue(String eventDateTBDValue) {
		this.eventDateTBDValue = eventDateTBDValue;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Double getTeamOdds() {
		return teamOdds;
	}
	public void setTeamOdds(Double teamOdds) {
		this.teamOdds = teamOdds;
	}
	
}
