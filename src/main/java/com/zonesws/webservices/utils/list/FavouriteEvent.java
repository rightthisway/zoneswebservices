package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FavouriteEvent")
public class FavouriteEvent {
	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String artistName;
	private String venueName;
	private String venueCity;
	private String venueState;
	private String venueCountry;
	private String venueZipCode;
	private String mapPath;
	private String venueCategoryName;
	private Integer venueId;
	private Integer priceStartFrom;
	private Boolean discountFlag;
	private Boolean isFavouriteEvent;
	
	public Integer getEventId() {
		if(null ==eventId){
			eventId = 0;
		}
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		if(null ==eventName || eventName.isEmpty()){
			eventName = "";
		}
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDate() {
		if(null ==eventDate || eventDate.isEmpty()){
			eventDate = "";
		}
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventTime() {
		if(null ==eventTime  || eventTime.isEmpty()){
			eventTime = "";
		}
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getArtistName() {
		if(null ==artistName || artistName.isEmpty()){
			artistName = "";
		}
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getVenueName() {
		if(null == venueName || venueName.isEmpty()){
			venueName = "";
		}
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getVenueCity() {
		if(null == venueCity || venueCity.isEmpty()){
			venueCity = "";
		}
		return venueCity;
	}

	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}

	public String getVenueState() {
		if(null ==venueState || venueState.isEmpty()){
			venueState = "";
		}
		return venueState;
	}

	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}

	public String getVenueCountry() {
		if(null ==venueCountry || venueCountry.isEmpty()){
			venueCountry = "";
		}
		return venueCountry;
	}

	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}

	public String getVenueZipCode() {
		if(null ==venueZipCode || venueZipCode.isEmpty()){
			venueZipCode = "";
		}
		return venueZipCode;
	}

	public void setVenueZipCode(String venueZipCode) {
		this.venueZipCode = venueZipCode;
	}

	public String getMapPath() {
		if(null ==mapPath || mapPath.isEmpty()){
			mapPath = "";
		}
		return mapPath;
	}

	public void setMapPath(String mapPath) {
		this.mapPath = mapPath;
	}

	public String getVenueCategoryName() {
		if(null ==venueCategoryName || venueCategoryName.isEmpty()){
			venueCategoryName = "";
		}
		return venueCategoryName;
	}

	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}

	public Integer getVenueId() {
		if(null ==venueId ){
			venueId = 0;
		}
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public Integer getPriceStartFrom() {
		if(null ==priceStartFrom ){
			priceStartFrom = 15;
		}
		return priceStartFrom;
	}

	public void setPriceStartFrom(Integer priceStartFrom) {
		this.priceStartFrom = priceStartFrom;
	}
	
	public Boolean getDiscountFlag() {
		if(null ==discountFlag ){
			discountFlag = false;
		}
		return discountFlag;
	}
	public void setDiscountFlag(Boolean discountFlag) {
		this.discountFlag = discountFlag;
	}

	public Boolean getIsFavouriteEvent() {
		if(null ==isFavouriteEvent ){
			isFavouriteEvent = false;
		}
		return isFavouriteEvent;
	}

	public void setIsFavouriteEvent(Boolean isFavouriteEvent) {
		this.isFavouriteEvent = isFavouriteEvent;
	}
	
	
}
