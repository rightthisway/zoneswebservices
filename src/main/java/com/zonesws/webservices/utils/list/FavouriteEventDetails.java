package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("FavouriteEventDetails")
public class FavouriteEventDetails {
	private Integer status;
	private Error error; 
	private Integer customerId;
	private String actionResult;
	private List<FavouriteEvent> favouriteEvents;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public List<FavouriteEvent> getFavouriteEvents() {
		return favouriteEvents;
	}
	public void setFavouriteEvents(List<FavouriteEvent> favouriteEvents) {
		this.favouriteEvents = favouriteEvents;
	}
	public String getActionResult() {
		return actionResult;
	}
	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	

	
	
}
