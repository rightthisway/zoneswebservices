package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("FavouriteEvents")
public class FavouriteEventsResponse {
	
	private Integer status;
	private com.zonesws.webservices.utils.Error error;
	private String message;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public com.zonesws.webservices.utils.Error getError() {
		return error;
	}
	public void setError(com.zonesws.webservices.utils.Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
