package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("GeneralisedSearchHeaders")
public class GeneralisedSearchHeaders {
	
	private Integer status;
	private Error error; 
	
	private Boolean showEventLabel;
	private String eventLabelName;
	
	private Boolean showArtistLabel;
	private String artistLabelName;
	
	private Boolean showVenueLabel;
	private String venueLabelName;
	
	private Boolean showCategoriesLabel;
	private String categoriesLabelName;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Boolean getShowEventLabel() {
		return showEventLabel;
	}
	public void setShowEventLabel(Boolean showEventLabel) {
		this.showEventLabel = showEventLabel;
	}
	public String getEventLabelName() {
		return eventLabelName;
	}
	public void setEventLabelName(String eventLabelName) {
		this.eventLabelName = eventLabelName;
	}
	public Boolean getShowArtistLabel() {
		return showArtistLabel;
	}
	public void setShowArtistLabel(Boolean showArtistLabel) {
		this.showArtistLabel = showArtistLabel;
	}
	public String getArtistLabelName() {
		return artistLabelName;
	}
	public void setArtistLabelName(String artistLabelName) {
		this.artistLabelName = artistLabelName;
	}
	public Boolean getShowVenueLabel() {
		return showVenueLabel;
	}
	public void setShowVenueLabel(Boolean showVenueLabel) {
		this.showVenueLabel = showVenueLabel;
	}
	public String getVenueLabelName() {
		return venueLabelName;
	}
	public void setVenueLabelName(String venueLabelName) {
		this.venueLabelName = venueLabelName;
	}
	public Boolean getShowCategoriesLabel() {
		return showCategoriesLabel;
	}
	public void setShowCategoriesLabel(Boolean showCategoriesLabel) {
		this.showCategoriesLabel = showCategoriesLabel;
	}
	public String getCategoriesLabelName() {
		return categoriesLabelName;
	}
	public void setCategoriesLabelName(String categoriesLabelName) {
		this.categoriesLabelName = categoriesLabelName;
	}
	
	
	
	
}
