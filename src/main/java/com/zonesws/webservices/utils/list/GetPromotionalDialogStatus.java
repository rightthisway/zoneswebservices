package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("GetPromotionalDialogStatus")
public class GetPromotionalDialogStatus {
	
	private Integer status;
	private Error error; 
	private Boolean showDialog;
	private String headerName;
	private Boolean showImageText;
	private String imageText;
	private String imageUrl;
	private String mainButtonValue;
	private String secondButtonValue;
	private Boolean showSignUpPage;
	private String successMsg;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Boolean getShowDialog() {
		if(null == showDialog){
			showDialog = false;
		}
		return showDialog;
	}
	public void setShowDialog(Boolean showDialog) {
		this.showDialog = showDialog;
	}
	public String getMainButtonValue() {
		if(null == mainButtonValue){
			mainButtonValue= "";
		}
		return mainButtonValue;
	}
	public void setMainButtonValue(String mainButtonValue) {
		this.mainButtonValue = mainButtonValue;
	}
	public String getSecondButtonValue() {
		if(null == secondButtonValue){
			secondButtonValue= "";
		}
		return secondButtonValue;
	}
	public void setSecondButtonValue(String secondButtonValue) {
		this.secondButtonValue = secondButtonValue;
	}
	public Boolean getShowSignUpPage() {
		if(null == showSignUpPage){
			showSignUpPage = false;
		}
		return showSignUpPage;
	}
	public void setShowSignUpPage(Boolean showSignUpPage) {
		this.showSignUpPage = showSignUpPage;
	}
	public String getSuccessMsg() {
		if(null == successMsg){
			successMsg = "";
		}
		return successMsg;
	}
	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}
	public Boolean getShowImageText() {
		if(null == showImageText){
			showImageText = false;
		}
		return showImageText;
	}
	public void setShowImageText(Boolean showImageText) {
		this.showImageText = showImageText;
	}
	public String getImageText() {
		if(null == imageText){
			imageText = "";
		}
		return imageText;
	}
	public void setImageText(String imageText) {
		this.imageText = imageText;
	}
	public String getImageUrl() {
		if(null == imageUrl){
			imageUrl = "";
		}
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getHeaderName() {
		if(null == headerName){
			headerName = "";
		}
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	
}
