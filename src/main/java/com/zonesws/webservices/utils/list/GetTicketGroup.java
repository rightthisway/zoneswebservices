package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("GetTicketGroup")
public class GetTicketGroup {
	private Integer status;
	private Error error; 
	private Boolean showSavedCards;
	private String encryptionKey;
	private Event event;
	//private DiscountDetails discountDetails;
	private CategoryTicketGroup categoryTicketGroup;
	
	private String ticketDownloadOption1;
	private String ticketDownloadOption2;
	private Boolean onDayBeforeEventTixDownload;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public CategoryTicketGroup getCategoryTicketGroup() {
		return categoryTicketGroup;
	}
	public void setCategoryTicketGroup(CategoryTicketGroup categoryTicketGroup) {
		this.categoryTicketGroup = categoryTicketGroup;
	}
	
	public Boolean getShowSavedCards() {
		if(null == showSavedCards){
			showSavedCards=false;
		}
		return showSavedCards;
	}
	public void setShowSavedCards(Boolean showSavedCards) {
		this.showSavedCards = showSavedCards;
	}
	public String getEncryptionKey() {
		return encryptionKey;
	}
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
	public String getTicketDownloadOption1() {
		//ticketDownloadOption1 = "";//"<font color=#012e4f>Please upload my tickets as soon as they are ready for download.</font>";
		return ticketDownloadOption1;
	}
	public void setTicketDownloadOption1(String ticketDownloadOption1) {
		this.ticketDownloadOption1 = ticketDownloadOption1;
	}
	public String getTicketDownloadOption2() {
		return ticketDownloadOption2;
	}
	public void setTicketDownloadOption2(String ticketDownloadOption2) {
		this.ticketDownloadOption2 = ticketDownloadOption2;
	}
	
	public Boolean getOnDayBeforeEventTixDownload() {
		if(null == onDayBeforeEventTixDownload){
			onDayBeforeEventTixDownload = false;
		}
		return onDayBeforeEventTixDownload;
	}
	public void setOnDayBeforeEventTixDownload(Boolean onDayBeforeEventTixDownload) {
		this.onDayBeforeEventTixDownload = onDayBeforeEventTixDownload;
	}
	
}
