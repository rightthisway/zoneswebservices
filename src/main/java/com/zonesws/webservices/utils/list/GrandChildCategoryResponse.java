package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.data.GrandChildCategory;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("GrandChildList")
public class GrandChildCategoryResponse {

	private Integer status;
	private Error error; 	
	private List<GrandChildCategory> grandChildCategory;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<GrandChildCategory> getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(List<GrandChildCategory> grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	
}
