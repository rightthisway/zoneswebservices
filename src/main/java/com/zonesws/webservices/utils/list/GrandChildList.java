package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.ChildCategory;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("GrandChildList")
public class GrandChildList {
	private Integer status;
	private Error error; 
	
	private List<ChildCategory> childCategories;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<ChildCategory> getChildCategories() {
		return childCategories;
	}
	public void setChildCategories(List<ChildCategory> childCategories) {
		this.childCategories = childCategories;
	}
	
	
}
