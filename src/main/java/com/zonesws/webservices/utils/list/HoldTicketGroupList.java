package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.HoldTicketGroup;

@XStreamAlias("HoldTicketGroupList")
public class HoldTicketGroupList {

	private Event event;
	private List<HoldTicketGroup> holdTicketGroups;
	
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public List<HoldTicketGroup> getHoldTicketGroups() {
		return holdTicketGroups;
	}
	public void setHoldTicketGroups(List<HoldTicketGroup> holdTicketGroups) {
		this.holdTicketGroups = holdTicketGroups;
	}
	
}
