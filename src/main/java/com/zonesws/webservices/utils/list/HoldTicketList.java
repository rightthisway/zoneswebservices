package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.InvalidTicketGroup;

@XStreamAlias("HoldTicketList")
public class HoldTicketList {

	private Error error; 
	private List<HoldTicketGroupList> holdTicketGroupLists;
	private List<InvalidTicketGroup> InvalidTicketGroupList;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public List<InvalidTicketGroup> getInvalidTicketGroupList() {
		return InvalidTicketGroupList;
	}
	public void setInvalidTicketGroupList(
			List<InvalidTicketGroup> invalidTicketGroupList) {
		InvalidTicketGroupList = invalidTicketGroupList;
	}
	public List<HoldTicketGroupList> getHoldTicketGroupLists() {
		return holdTicketGroupLists;
	}
	public void setHoldTicketGroupLists(
			List<HoldTicketGroupList> holdTicketGroupLists) {
		this.holdTicketGroupLists = holdTicketGroupLists;
	}
	
	
}
