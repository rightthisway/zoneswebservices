package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.AlreadySoldTicketGroup;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.HoldOnTicket;
import com.zonesws.webservices.utils.InvalidHoldTicket;
import com.zonesws.webservices.utils.SoldTicketGroup;

@XStreamAlias("List")
public class HoldTimeExtendList {

	private Error error; 
	private List<HoldOnTicket> holdOnTicketList;
	private List<InvalidHoldTicket> invalidHoldTicketList;
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<HoldOnTicket> getHoldOnTicketList() {
		return holdOnTicketList;
	}
	public void setHoldOnTicketList(List<HoldOnTicket> holdOnTicketList) {
		this.holdOnTicketList = holdOnTicketList;
	}
	public List<InvalidHoldTicket> getInvalidHoldTicketList() {
		return invalidHoldTicketList;
	}
	public void setInvalidHoldTicketList(
			List<InvalidHoldTicket> invalidHoldTicketList) {
		this.invalidHoldTicketList = invalidHoldTicketList;
	}
	
	
	
	
	
	
	
	
	
}
