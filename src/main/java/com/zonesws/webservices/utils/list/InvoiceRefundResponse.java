package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.InvoiceRefund;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("InvoiceRefunds")
public class InvoiceRefundResponse {

	private Integer status;
	private Error error; 
	private String message;
	private InvoiceRefund invoiceRefund;
	private List<InvoiceRefund> paypalRefunds;
	private List<InvoiceRefund> stripeRefunds;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public InvoiceRefund getInvoiceRefund() {
		return invoiceRefund;
	}
	public void setInvoiceRefund(InvoiceRefund invoiceRefund) {
		this.invoiceRefund = invoiceRefund;
	}
	public List<InvoiceRefund> getPaypalRefunds() {
		return paypalRefunds;
	}
	public void setPaypalRefunds(List<InvoiceRefund> paypalRefunds) {
		this.paypalRefunds = paypalRefunds;
	}
	public List<InvoiceRefund> getStripeRefunds() {
		return stripeRefunds;
	}
	public void setStripeRefunds(List<InvoiceRefund> stripeRefunds) {
		this.stripeRefunds = stripeRefunds;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
