package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("LoyalFanAutoSearch")
public class LoyalFanAutoSearch {
	private Integer status;
	private Error error; 
	private List<LoyalFanStateSearch> searchList;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<LoyalFanStateSearch> getSearchList() {
		return searchList;
	}
	public void setSearchList(List<LoyalFanStateSearch> searchList) {
		this.searchList = searchList;
	}
	
}
