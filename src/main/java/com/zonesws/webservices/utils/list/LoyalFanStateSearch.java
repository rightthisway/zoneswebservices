package com.zonesws.webservices.utils.list;


public class LoyalFanStateSearch {
	
	private String loyalFanSelectionValue;
	private String searchGridValue;
	private String boxDisplayValue;
	private String country;
	private String state;
	private String city;
	private String zipCode;
	
	public String getLoyalFanSelectionValue() {
		return loyalFanSelectionValue;
	}
	public void setLoyalFanSelectionValue(String loyalFanSelectionValue) {
		this.loyalFanSelectionValue = loyalFanSelectionValue;
	}
	public String getSearchGridValue() {
		return searchGridValue;
	}
	public void setSearchGridValue(String searchGridValue) {
		this.searchGridValue = searchGridValue;
	}
	public String getBoxDisplayValue() {
		return boxDisplayValue;
	}
	public void setBoxDisplayValue(String boxDisplayValue) {
		this.boxDisplayValue = boxDisplayValue;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
}
