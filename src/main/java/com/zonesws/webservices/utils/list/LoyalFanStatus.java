package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.LoyalFanType;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.MapUtil;

@XStreamAlias("LoyalFanStatus")
public class LoyalFanStatus {
	
	private Integer status;
	private Error error; 
	private Integer customerId;
	private String message;
	private LoyalFanType loyalFanType;
	private Integer artistOrTeamId;
	private String loyalFanName;
	private Boolean isEditable;
	private String sportsImageUrl;
	private String concertsImageUrl;
	private String theaterImageUrl;
	private String state;
	private String country;
	private String startDate;
	private String endDate;
	private String sportsText;
	private String concertsText;
	private String theaterText;
	private String commonText;
	private String validationTitle;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		if(null == message){
			message ="";
		}
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public LoyalFanType getLoyalFanType() {
		return loyalFanType;
	}
	public void setLoyalFanType(LoyalFanType loyalFanType) {
		this.loyalFanType = loyalFanType;
	}
	public Integer getArtistOrTeamId() {
		return artistOrTeamId;
	}
	public void setArtistOrTeamId(Integer artistOrTeamId) {
		this.artistOrTeamId = artistOrTeamId;
	}
	public String getLoyalFanName() {
		if(null == loyalFanName){
			loyalFanName ="";
		}
		return loyalFanName;
	}
	public void setLoyalFanName(String loyalFanName) {
		this.loyalFanName = loyalFanName;
	}
	public Boolean getIsEditable() {
		if(null == isEditable){
			isEditable = false;
		}
		return isEditable;
	}
	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}
	
	public String getSportsImageUrl() {
		//sportsImageUrl = MapUtil.getParentCategoryImages(String.valueOf(LoyalFanType.SPORTS));
		return sportsImageUrl;
	}
	public void setSportsImageUrl(String sportsImageUrl) {
		this.sportsImageUrl = sportsImageUrl;
	}
	public String getConcertsImageUrl() {
		//concertsImageUrl = MapUtil.getParentCategoryImages(String.valueOf(LoyalFanType.CONCERTS));
		return concertsImageUrl;
	}
	public void setConcertsImageUrl(String concertsImageUrl) {
		this.concertsImageUrl = concertsImageUrl;
	}
	public String getTheaterImageUrl() {
		//theaterImageUrl = MapUtil.getParentCategoryImages(String.valueOf(LoyalFanType.THEATER));
		return theaterImageUrl;
	}
	public void setTheaterImageUrl(String theaterImageUrl) {
		this.theaterImageUrl = theaterImageUrl;
	}
	public String getState() {
		if(null == state){
			state ="";
		}
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		if(null == country){
			country ="";
		}
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public void computeLoyalFanParentCategoryImageURLs() {
		MapUtil.getLoyalFanStatusParentCategoryImages(this);
	}
	public String getSportsText(){ 
		return sportsText;
	}
	public void setSportsText(String sportsText) {
		this.sportsText = sportsText;
	}
	public String getConcertsText() {
		return concertsText;
	}
	public void setConcertsText(String concertsText) {
		this.concertsText = concertsText;
	}
	public String getTheaterText() {
		return theaterText;
	}
	public void setTheaterText(String theaterText) {
		this.theaterText = theaterText;
	}
	public String getCommonText() {
		return commonText;
	}
	public void setCommonText(String commonText) {
		this.commonText = commonText;
	}
	public String getValidationTitle() {
		validationTitle = "Loyal Fan Confirmation";
		return validationTitle;
	}
	public void setValidationTitle(String validationTitle) {
		this.validationTitle = validationTitle;
	}
	public void computeAllTextContents(String fontSizeStr){ 
		sportsText = "<p align='justify'><font "+fontSizeStr+" >Choose your favorite team and receive a 10% discount on any of their games, whether home or away!</font></p>";
		concertsText = "<p align='justify'><font "+fontSizeStr+" >Choose your STATE and receive a 10% discount on all CONCERT tickets in your STATE.</font></p>";
		theaterText = "<p align='justify'><font "+fontSizeStr+" >Choose your STATE and receive a 10% discount on all THEATER tickets in your STATE.</font></p>";
		commonText = "<p><font "+fontSizeStr+" color=#F06812><strong>Choose any 1 of the 3 FAN categories below and receive a 10% discount on every purchase</strong></font></p>";
	}
	
}
