package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;

public class ManageFavorites {
	private Integer customerId;
	private Integer status;
	private String description;
	private Error error; 
	private List<Artist> favouriteArtists;
	private List<Event> favouriteEvents;
	private Integer totalEventPages;
	private Boolean eligibleToShareRefCode;
	private String shareErrorMessage;
	
	/**
	 * Getters and setters for manage
	 * favorites
	 * @return
	 */
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<Artist> getFavouriteArtists() {
		return favouriteArtists;
	}
	public void setFavouriteArtists(List<Artist> favouriteArtists) {
		this.favouriteArtists = favouriteArtists;
	}
	public List<Event> getFavouriteEvents() {
		return favouriteEvents;
	}
	public void setFavouriteEvents(List<Event> favouriteEvents) {
		this.favouriteEvents = favouriteEvents;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getTotalEventPages() {
		if(null == totalEventPages){
			totalEventPages = 0;
		}
		return totalEventPages;
	}
	public void setTotalEventPages(Integer totalEventPages) {
		this.totalEventPages = totalEventPages;
	}
	
	public Boolean getEligibleToShareRefCode() {
		if(null == eligibleToShareRefCode){
			eligibleToShareRefCode = false;
		}
		return eligibleToShareRefCode;
	}
	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}
	
	public String getShareErrorMessage() {
		if(null == shareErrorMessage){
			shareErrorMessage="";
		}
		return shareErrorMessage;
	}
	public void setShareErrorMessage(String shareErrorMessage) {
		this.shareErrorMessage = shareErrorMessage;
	}
}
