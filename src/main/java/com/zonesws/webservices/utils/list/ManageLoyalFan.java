package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.Artist;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;

public class ManageLoyalFan {
	
	
	private Integer status;
	private Error error; 
	private Integer customerId;
	private String description;
	private Artist loyalFan;
	private List<Event> eventDetails;
	private Integer totalEventPages;
	private String loyalFanName;
	private Boolean showButton;
	private String buttonValue;
	private Boolean eligibleToShareRefCode;
	private String shareErrorMessage;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Artist getLoyalFan() {
		return loyalFan;
	}
	public void setLoyalFan(Artist loyalFan) {
		this.loyalFan = loyalFan;
	}
	public List<Event> getEventDetails() {
		return eventDetails;
	}
	public void setEventDetails(List<Event> eventDetails) {
		this.eventDetails = eventDetails;
	}
	public Integer getTotalEventPages() {
		if(null == totalEventPages){
			totalEventPages = 0;
		}
		return totalEventPages;
	}
	public void setTotalEventPages(Integer totalEventPages) {
		this.totalEventPages = totalEventPages;
	}
	public String getLoyalFanName() {
		if(loyalFanName == null){
			loyalFanName = "";
		}
		return loyalFanName;
	}
	public void setLoyalFanName(String loyalFanName) {
		this.loyalFanName = loyalFanName;
	}
	public String getButtonValue() {
		return buttonValue;
	}
	public void setButtonValue(String buttonValue) {
		this.buttonValue = buttonValue;
	}
	public Boolean getShowButton() {
		if(null == showButton){
			showButton = false;
		}
		return showButton;
	}
	public void setShowButton(Boolean showButton) {
		this.showButton = showButton;
	}
	public Boolean getEligibleToShareRefCode() {
		if(null == eligibleToShareRefCode){
			eligibleToShareRefCode = false;
		}
		return eligibleToShareRefCode;
	}
	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}
	
	public String getShareErrorMessage() {
		if(null == shareErrorMessage){
			shareErrorMessage="";
		}
		return shareErrorMessage;
	}
	public void setShareErrorMessage(String shareErrorMessage) {
		this.shareErrorMessage = shareErrorMessage;
	}
}
