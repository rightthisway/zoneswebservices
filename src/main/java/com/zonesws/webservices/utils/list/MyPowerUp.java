package com.zonesws.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.RtfPointsConversionSetting;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.PowerUpType;

@XStreamAlias("MyPowerUp")
public class MyPowerUp {
	
	private PowerUpType powerUpType;
	private String hinfo; 
	private String hd; 
	private List<RtfPointsConversionSetting> convList;
	
	 
	public PowerUpType getPowerUpType() {
		return powerUpType;
	}
	public void setPowerUpType(PowerUpType powerUpType) {
		this.powerUpType = powerUpType;
	}
	public String getHinfo() {
		return hinfo;
	}
	public void setHinfo(String hinfo) {
		this.hinfo = hinfo;
	}
	public String getHd() {
		return hd;
	}
	public void setHd(String hd) {
		this.hd = hd;
	}
	public List<RtfPointsConversionSetting> getConvList() {
		return convList;
	}
	public void setConvList(List<RtfPointsConversionSetting> convList) {
		this.convList = convList;
	}
	
}
