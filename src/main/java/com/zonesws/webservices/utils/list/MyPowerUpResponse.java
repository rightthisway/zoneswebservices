package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("MyPowerUpResponse")
public class MyPowerUpResponse {
	
	private Integer status;
	private Error error; 
	private String msg;
	private MyReward mr;
	private List<MyPowerUp> mpuList;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public MyReward getMr() {
		return mr;
	}
	public void setMr(MyReward mr) {
		this.mr = mr;
	}
	public List<MyPowerUp> getMpuList() {
		return mpuList;
	}
	public void setMpuList(List<MyPowerUp> mpuList) {
		this.mpuList = mpuList;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	 
	
}
