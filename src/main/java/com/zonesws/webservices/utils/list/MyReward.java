package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MyReward")
public class MyReward {
	
	private Integer cid;
	private Integer al; 
	private Integer as; 
	private Integer amw; 
	private Integer arp; 
	private Double ard;
	
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getAl() {
		return al;
	}
	public void setAl(Integer al) {
		this.al = al;
	}
	public Integer getAs() {
		return as;
	}
	public void setAs(Integer as) {
		this.as = as;
	}
	public Integer getAmw() {
		return amw;
	}
	public void setAmw(Integer amw) {
		this.amw = amw;
	}
	 
	public Double getArd() {
		return ard;
	}
	public void setArd(Double ard) {
		this.ard = ard;
	}
	public Integer getArp() {
		return arp;
	}
	public void setArp(Integer arp) {
		this.arp = arp;
	}   
	
	 
}
