package com.zonesws.webservices.utils.list;

import java.util.List;

import com.rtfquiz.webservices.data.QuizSuperFanLevelConfig;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("MySfLevelResponse")
public class MySfLevelResponse {
	
	private Integer status;
	private Error error; 
	private String msg;
	private List<QuizSuperFanLevelConfig> levels;
	
	private String sflDesc;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<QuizSuperFanLevelConfig> getLevels() {
		return levels;
	}
	public void setLevels(List<QuizSuperFanLevelConfig> levels) {
		this.levels = levels;
	}
	public String getSflDesc() {
		if(sflDesc == null) {
			sflDesc = "";
		}
		return sflDesc;
	}
	public void setSflDesc(String sflDesc) {
		this.sflDesc = sflDesc;
	}
	
}
