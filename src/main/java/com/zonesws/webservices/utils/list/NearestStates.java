package com.zonesws.webservices.utils.list;


public class NearestStates {
	
	private String state;
	private String nearestStates;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getNearestStates() {
		return nearestStates;
	}
	public void setNearestStates(String nearestStates) {
		this.nearestStates = nearestStates;
	}
	
	
	
	
}
