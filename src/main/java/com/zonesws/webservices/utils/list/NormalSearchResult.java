package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.data.GrandChildCategory;

@XStreamAlias("NormalSearchResults")
public class NormalSearchResult {
	
	private Integer totalArtistCount;
	private Integer totalVenueCount;
	private Integer totalEventCount;
	private Integer totalGrandChildCount;
	private Integer totalEventPages;
	private Integer totalArtistPages;
	private Integer totalVenuePages;
	private Boolean sportsChildOrTeam;
	
	private List<GrandChildCategory> grandChildCategories;
	private List<ArtistResult> artistResults;
	private List<VenueResult> venueResults;
	private List<Event> events;
	private List<Event> otherEvents;
	private Boolean showOtherEvents;
	private Boolean eventShowMore;
	private Boolean otherEventShowMore;
	private Boolean venueShowMore;
	private Boolean artistShowMore;
	private String aRefType;
	private String aRefValue;
	/*private Integer maxEventsPerPage;*/
	
	public List<ArtistResult> getArtistResults() {
		return artistResults;
	}
	public void setArtistResults(List<ArtistResult> artistResults) {
		this.artistResults = artistResults;
	}
	public List<VenueResult> getVenueResults() {
		return venueResults;
	}
	public void setVenueResults(List<VenueResult> venueResults) {
		this.venueResults = venueResults;
	}
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	public Integer getTotalArtistCount() {
		if(null == totalArtistCount){
			totalArtistCount = 0;
		}
		return totalArtistCount;
	}
	public void setTotalArtistCount(Integer totalArtistCount) {
		this.totalArtistCount = totalArtistCount;
	}
	public Integer getTotalVenueCount() {
		if(null == totalVenueCount){
			totalVenueCount = 0;
		}
		return totalVenueCount;
	}
	public void setTotalVenueCount(Integer totalVenueCount) {
		this.totalVenueCount = totalVenueCount;
	}
	public Integer getTotalEventCount() {
		if(null == totalEventCount){
			totalEventCount = 0;
		}
		return totalEventCount;
	}
	public void setTotalEventCount(Integer totalEventCount) {
		this.totalEventCount = totalEventCount;
	}
	public Integer getTotalGrandChildCount() {
		if(null == totalGrandChildCount){
			totalGrandChildCount = 0;
		}
		return totalGrandChildCount;
	}
	public void setTotalGrandChildCount(Integer totalGrandChildCount) {
		this.totalGrandChildCount = totalGrandChildCount;
	}
	public Boolean getSportsChildOrTeam() {
		return sportsChildOrTeam;
	}
	public void setSportsChildOrTeam(Boolean sportsChildOrTeam) {
		this.sportsChildOrTeam = sportsChildOrTeam;
	}
	public List<GrandChildCategory> getGrandChildCategories() {
		return grandChildCategories;
	}
	public void setGrandChildCategories(
			List<GrandChildCategory> grandChildCategories) {
		this.grandChildCategories = grandChildCategories;
	}
	public Integer getTotalEventPages() {
		if(null == totalEventPages){
			totalEventPages = 0;
		}
		return totalEventPages;
	}
	public void setTotalEventPages(Integer totalEventPages) {
		this.totalEventPages = totalEventPages;
	}
	public Integer getTotalArtistPages() {
		if(null == totalArtistPages){
			totalArtistPages = 0;
		}
		return totalArtistPages;
	}
	public void setTotalArtistPages(Integer totalArtistPages) {
		this.totalArtistPages = totalArtistPages;
	}
	public Integer getTotalVenuePages() {
		if(null == totalVenuePages){
			totalVenuePages = 0;
		}
		return totalVenuePages;
	}
	public void setTotalVenuePages(Integer totalVenuePages) {
		this.totalVenuePages = totalVenuePages;
	}
	public Boolean getEventShowMore() {
		if(null == eventShowMore){
			eventShowMore = false;
		}
		return eventShowMore;
	}
	public void setEventShowMore(Boolean eventShowMore) {
		this.eventShowMore = eventShowMore;
	}
	public Boolean getVenueShowMore() {
		if(null == venueShowMore){
			venueShowMore = false;
		}
		return venueShowMore;
	}
	public void setVenueShowMore(Boolean venueShowMore) {
		this.venueShowMore = venueShowMore;
	}
	public Boolean getArtistShowMore() {
		if(null == artistShowMore){
			artistShowMore = false;
		}
		return artistShowMore;
	}
	public void setArtistShowMore(Boolean artistShowMore) {
		this.artistShowMore = artistShowMore;
	}
	public List<Event> getOtherEvents() {
		return otherEvents;
	}
	public void setOtherEvents(List<Event> otherEvents) {
		this.otherEvents = otherEvents;
	}
	public Boolean getShowOtherEvents() {
		if(null == showOtherEvents){
			showOtherEvents = false;
		}
		return showOtherEvents;
	}
	public void setShowOtherEvents(Boolean showOtherEvents) {
		this.showOtherEvents = showOtherEvents;
	}
	public Boolean getOtherEventShowMore() {
		if(null == otherEventShowMore){
			otherEventShowMore = false;
		}
		return otherEventShowMore;
	}
	public void setOtherEventShowMore(Boolean otherEventShowMore) {
		this.otherEventShowMore = otherEventShowMore;
	}
	public String getaRefType() {
		return aRefType;
	}
	public void setaRefType(String aRefType) {
		this.aRefType = aRefType;
	}
	public String getaRefValue() {
		return aRefValue;
	}
	public void setaRefValue(String aRefValue) {
		this.aRefValue = aRefValue;
	}
	/*public Integer getMaxEventsPerPage() {
		if(null == maxEventsPerPage) {
			maxEventsPerPage = 0;
		}
		return maxEventsPerPage;
	}
	public void setMaxEventsPerPage(Integer maxEventsPerPage) {
		this.maxEventsPerPage = maxEventsPerPage;
	}*/
	
}
