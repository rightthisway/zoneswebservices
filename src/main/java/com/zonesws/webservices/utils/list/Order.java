package com.zonesws.webservices.utils.list;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.TicketOrder;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("Order")
public class Order {
	private Integer status;
	private Error error; 
	//private List<UserOrder> userOrders;
	private TicketOrder ticketOrder; 
	
	public Error getError() {
		return error;
	}
	/*public List<UserOrder> getUserOrders() {
		return userOrders;
	}
	public void setUserOrders(List<UserOrder> userOrders) {
		this.userOrders = userOrders;
	}*/
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public TicketOrder getTicketOrder() {
		return ticketOrder;
	}
	public void setTicketOrder(TicketOrder ticketOrder) {
		this.ticketOrder = ticketOrder;
	}
	
	
	
}
