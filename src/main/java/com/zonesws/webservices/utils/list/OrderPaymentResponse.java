package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.OrderPaymentBreakup;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("OrderPaymentResponse")
public class OrderPaymentResponse {

	private Integer orderId;
	private Integer status;
	private Error error; 
	private String message;
	private List<OrderPaymentBreakup> orderPaymentBreakupList;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<OrderPaymentBreakup> getOrderPaymentBreakupList() {
		return orderPaymentBreakupList;
	}
	public void setOrderPaymentBreakupList(
			List<OrderPaymentBreakup> orderPaymentBreakupList) {
		this.orderPaymentBreakupList = orderPaymentBreakupList;
	}
}
