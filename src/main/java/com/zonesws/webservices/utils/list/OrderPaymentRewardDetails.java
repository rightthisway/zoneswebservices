package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.CustomerLoyalty;
import com.zonesws.webservices.data.LoyaltySettings;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TicketUtil;

@XStreamAlias("OrderPaymentRewardDetails")
public class OrderPaymentRewardDetails {
	
	private Integer status;
	private Error error; 
	private CustomerLoyalty customerLoyalty; 
	private String conversionMeassage;
	private Double dollerConversion;
	private Double activeRewardDollers;
	
	private String activePoints;
	private String toBeSpentPoints;
	private String enteredRewards;
	
	private Double toBeSpentDollers;
	private Double orderTotal;
	private Double amountToBePaid;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
	public Double getOrderTotal() {
		return orderTotal;
	}
	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}
	public Double getAmountToBePaid() {
		return amountToBePaid;
	}
	public void setAmountToBePaid(Double amountToBePaid) {
		this.amountToBePaid = amountToBePaid;
	}
	public String getConversionMeassage() {
		return conversionMeassage;
	}
	public void setConversionMeassage(String conversionMeassage) {
		this.conversionMeassage = conversionMeassage;
	}
	
	public CustomerLoyalty getCustomerLoyalty() {
		return customerLoyalty;
	}
	public void setCustomerLoyalty(CustomerLoyalty customerLoyalty) {
		this.customerLoyalty = customerLoyalty;
		
	}
	public String getActivePoints() {
		return activePoints;
	}
	public void setActivePoints(String activePoints) {
		this.activePoints = activePoints;
	}
	public Double getActiveRewardDollers() throws Exception {
		if(null != activePoints && !activePoints.isEmpty()){
			LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
			Double temp = Double.valueOf(activePoints) * loyaltySettings.getDollerConversion();
			activeRewardDollers = TicketUtil.getRoundedValue(temp);
		}else{
			activeRewardDollers=0.00;
		}
		return activeRewardDollers;
	}
	public void setActiveRewardDollers(Double activeRewardDollers) {
		this.activeRewardDollers = activeRewardDollers;
	}
	public String getToBeSpentPoints() {
		return toBeSpentPoints;
	}
	public void setToBeSpentPoints(String toBeSpentPoints) {
		this.toBeSpentPoints = toBeSpentPoints;
	}
	public Double getToBeSpentDollers() {
		return toBeSpentDollers;
	}
	public void setToBeSpentDollers(Double toBeSpentDollers) {
		this.toBeSpentDollers = toBeSpentDollers;
	}
	public String getEnteredRewards() {
		return enteredRewards;
	}
	public void setEnteredRewards(String enteredRewards) {
		this.enteredRewards = enteredRewards;
	}
	
	public Double getDollerConversion() {
		LoyaltySettings loyaltySettings = DAORegistry.getLoyaltySettingsDAO().getActivetLoyaltySettings();
		dollerConversion = loyaltySettings.getDollerConversion();
		return dollerConversion;
	}
	public void setDollerConversion(Double dollerConversion) {
		this.dollerConversion = dollerConversion;
	}
	
}


