package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("OrderTracking")
public class OrderTracking {

	private Integer trackingId;
	private Integer status;
	private Error error; 
	private String message;
	
	public Integer getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(Integer trackingId) {
		this.trackingId = trackingId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
	
	
}
