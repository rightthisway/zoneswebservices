package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.PaypalApi;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("PaypalCredentials")
public class PaypalCredentials {
	private Integer status;
	private Error error; 
	private List<PaypalApi> paypalApiKeys;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<PaypalApi> getPaypalApiKeys() {
		return paypalApiKeys;
	}
	public void setPaypalApiKeys(List<PaypalApi> paypalApiKeys) {
		this.paypalApiKeys = paypalApiKeys;
	}
	
	
	
	
}
