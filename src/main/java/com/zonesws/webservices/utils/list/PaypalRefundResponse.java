package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CustomerOrderPaypalRefund;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("PaypalRefundResponse")
public class PaypalRefundResponse {

	private Integer status;
	private String message; 
	private Error error; 
	private CustomerOrderPaypalRefund customerOrderPaypalRefund;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerOrderPaypalRefund getCustomerOrderPaypalRefund() {
		return customerOrderPaypalRefund;
	}
	public void setCustomerOrderPaypalRefund(CustomerOrderPaypalRefund customerOrderPaypalRefund) {
		this.customerOrderPaypalRefund = customerOrderPaypalRefund;
	}
	
}
