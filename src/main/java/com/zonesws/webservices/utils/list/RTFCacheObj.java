package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.Event;


public class RTFCacheObj {
	
	private Boolean cacheOff;
	private Boolean homeCardShowMore;
	private List<Event> eventList;
	
	public List<Event> getEventList() {
		return eventList;
	}
	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}
	public Boolean getCacheOff() {
		if(cacheOff == null){
			cacheOff = false;
		}
		return cacheOff;
	}
	public void setCacheOff(Boolean cacheOff) {
		this.cacheOff = cacheOff;
	}
	public Boolean getHomeCardShowMore() {
		if(homeCardShowMore == null){
			homeCardShowMore = false;
		}
		return homeCardShowMore;
	}
	public void setHomeCardShowMore(Boolean homeCardShowMore) {
		this.homeCardShowMore = homeCardShowMore;
	}
	
	
	
	
	
	
	
	
	
}
