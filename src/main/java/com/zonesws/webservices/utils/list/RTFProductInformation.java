package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("RTFProductInfo")
public class RTFProductInformation {

	private Integer status;
	private Error error; 
	private String pageType;
	private String description;
	private String descriptionText;
	private String headerText;
	
	List<RTFProductInfoDtls> productInfoDetails;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public String getPageType() {
		return pageType;
	}
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	public String getDescriptionText() {
		return descriptionText;
	}
	public void setDescriptionText(String descriptionText) {
		this.descriptionText = descriptionText;
	}
	public List<RTFProductInfoDtls> getProductInfoDetails() {
		return productInfoDetails;
	}
	public void setProductInfoDetails(List<RTFProductInfoDtls> productInfoDetails) {
		this.productInfoDetails = productInfoDetails;
	}
	public String getHeaderText() {
		return headerText;
	}
	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}
	
	
	
}
