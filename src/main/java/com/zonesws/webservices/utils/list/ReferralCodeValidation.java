package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.enums.CodeType;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("ReferralCodeValidation")
public class ReferralCodeValidation {
	private Integer status;
	private Error error; 
	private Integer customerId;
	private String referralCode; 
	private String message;
	private String rewardPoints;
	private Double rewardPointsAsDouble;
	private Double rewardConv;
	private Double discountConv;
	private CodeType codeType;
	private Boolean isFlatDiscount;
	private Integer promoTrackingId;
	private Integer affPromoTrackingId;
	private Boolean isConfirmBox;
	private Integer checkoutOfferId;
		
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(String rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public Double getRewardConv() {
		return rewardConv;
	}
	public void setRewardConv(Double rewardConv) {
		this.rewardConv = rewardConv;
	}
	public Double getDiscountConv() {
		return discountConv;
	}
	public void setDiscountConv(Double discountConv) {
		this.discountConv = discountConv;
	}
	public CodeType getCodeType() {
		return codeType;
	}
	public void setCodeType(CodeType codeType) {
		this.codeType = codeType;
	}
	public Double getRewardPointsAsDouble() {
		return rewardPointsAsDouble;
	}
	public void setRewardPointsAsDouble(Double rewardPointsAsDouble) {
		this.rewardPointsAsDouble = rewardPointsAsDouble;
	}
	public Boolean getIsFlatDiscount() {
		return isFlatDiscount;
	}
	public void setIsFlatDiscount(Boolean isFlatDiscount) {
		this.isFlatDiscount = isFlatDiscount;
	}
	public Integer getPromoTrackingId() {
		return promoTrackingId;
	}
	public void setPromoTrackingId(Integer promoTrackingId) {
		this.promoTrackingId = promoTrackingId;
	}
	public Integer getAffPromoTrackingId() {
		return affPromoTrackingId;
	}
	public void setAffPromoTrackingId(Integer affPromoTrackingId) {
		this.affPromoTrackingId = affPromoTrackingId;
	}
	public Boolean getIsConfirmBox() {
		if(isConfirmBox == null){
			isConfirmBox = false;
		}
		return isConfirmBox;
	}
	public void setIsConfirmBox(Boolean isConfirmBox) {
		this.isConfirmBox = isConfirmBox;
	}
	public Integer getCheckoutOfferId() {
		return checkoutOfferId;
	}
	public void setCheckoutOfferId(Integer checkoutOfferId) {
		this.checkoutOfferId = checkoutOfferId;
	}
	
	
}
