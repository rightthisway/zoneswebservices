package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("ReferralContestStatus")
public class ReferralContestStatus {

	private Integer status;
	private Error error;
	private Integer participantId;
	private Integer contestId;
	private Integer purchaserCustId;
	private String message;
	private Boolean redirectToHome;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getParticipantId() {
		return participantId;
	}
	public void setParticipantId(Integer participantId) {
		this.participantId = participantId;
	}
	public Integer getContestId() {
		return contestId;
	}
	public void setContestId(Integer contestId) {
		this.contestId = contestId;
	}
	public Integer getPurchaserCustId() {
		return purchaserCustId;
	}
	public void setPurchaserCustId(Integer purchaserCustId) {
		this.purchaserCustId = purchaserCustId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getRedirectToHome() {
		if(redirectToHome == null){
			redirectToHome= false;
		}
		return redirectToHome;
	}
	public void setRedirectToHome(Boolean redirectToHome) {
		this.redirectToHome = redirectToHome;
	}
	
}
