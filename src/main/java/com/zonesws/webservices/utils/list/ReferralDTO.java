package com.zonesws.webservices.utils.list;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ReferralDTO {
	
	private Integer customerId;
	private String userId;
	private String phoneNo;
	private String referralDate;
	private String signUpDate;
	
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS.SSS");
	private SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getReferralDate() {
		if(null != referralDate) {
			try {
				referralDate = format1.format(format.parse(referralDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return referralDate;
	}
	public void setReferralDate(String referralDate) {
		this.referralDate = referralDate;
	}
	public String getSignUpDate() {
		if(null != signUpDate) {
			try {
				signUpDate = format1.format(format.parse(signUpDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return signUpDate;
	}
	public void setSignUpDate(String signUpDate) {
		this.signUpDate = signUpDate;
	} 
}
