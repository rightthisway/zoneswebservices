package com.zonesws.webservices.utils.list;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.zonesws.webservices.utils.TicketUtil;

public class ReferralRewardDetails {
	
	private Integer primaryCustId;
	private Integer tireOneCustId;
	private String emailId;
	private String userName;
	private String phoneNo;
	private String customerName;  
	private String rewards;
	private Double rewardsAsDouble; 
	private String rewardType;
	private String contestName;
	private String contestDate;
	private Integer noOfAnsweredQuestion;
	@JsonIgnore
	private Date contestDateTime;
	
	 
	public Double getRewardsAsDouble() {
		if(null == rewardsAsDouble) {
			rewardsAsDouble =0.00;
		}
		return rewardsAsDouble;
	}
	public void setRewardsAsDouble(Double rewardsAsDouble) {
		this.rewardsAsDouble = rewardsAsDouble;
	}
	public String getRewards() {
		if(rewardsAsDouble == null){
			rewardsAsDouble=0.00;
		}
		try {
			rewards = TicketUtil.getRoundedValueString(rewardsAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rewards;
	}
	public void setRewards(String rewards) {
		this.rewards = rewards;
	}
	public String getUserName() {
		if(null == userName) {
			userName ="";
		}
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhoneNo() {
		if(null == phoneNo) {
			phoneNo ="";
		}
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getCustomerName() {
		if(null == customerName) {
			customerName ="";
		}
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getEmailId() {
		if(null == emailId) {
			emailId ="";
		}
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContestName() {
		if(null == contestName) {
			contestName ="";
		}
		return contestName;
	}
	public void setContestName(String contestName) {
		this.contestName = contestName;
	}
	public String getContestDate() {
		if(null == contestDate) {
			contestDate ="";
		}
		return contestDate;
	}
	public void setContestDate(String contestDate) {
		this.contestDate = contestDate;
	}
	public Integer getNoOfAnsweredQuestion() {
		if(null == noOfAnsweredQuestion) {
			noOfAnsweredQuestion = 0;
		}
		return noOfAnsweredQuestion;
	}
	public void setNoOfAnsweredQuestion(Integer noOfAnsweredQuestion) {
		this.noOfAnsweredQuestion = noOfAnsweredQuestion;
	}
	public Integer getPrimaryCustId() {
		if(null == primaryCustId) {
			primaryCustId = 0;
		}
		return primaryCustId;
	}
	public void setPrimaryCustId(Integer primaryCustId) {
		this.primaryCustId = primaryCustId;
	}
	public Integer getTireOneCustId() {
		if(null == primaryCustId) {
			primaryCustId = 0;
		}
		return tireOneCustId;
	}
	public void setTireOneCustId(Integer tireOneCustId) {
		this.tireOneCustId = tireOneCustId;
	}
	public Date getContestDateTime() {
		if(null == contestDateTime) {
			contestDateTime = new Date();
		}
		return contestDateTime;
	}
	public void setContestDateTime(Date contestDateTime) {
		this.contestDateTime = contestDateTime;
	}
	public String getRewardType() {
		if(null ==rewardType) {
			rewardType="";
		}
		return rewardType;
	}
	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}
	
}
