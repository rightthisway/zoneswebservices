package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.MapUtil;

@XStreamAlias("ReffererCode")
public class ReffererCodeResponse {

	private Integer status;
	private Error error;
	private Integer customerId;
	private String referrerCode;
	private String imageUrl;
	
	private String fbMsg;
	private String gmailMsg;
	private String twitterMsg;
	private String pinterestMsg;
	private String linkedInMsg;
	private String instagramMsg;
	private String normalMsg;
	
	private String shareImageUrl;
	private String shareRTFUrl;
	
	private String emailMsgDesk;
	private String fbMsgDesk;
	private String gmailMsgDesk;
	private String twitterMsgDesk;
	private String pinterestMsgDesk;
	private String linkedInMsgDesk;
	private String instagramMsgDesk;
	private String watsappMsgDesk;
	private String androidMsgDesk;
	private String iPhoneMsgDesk;
	
	private String text;
	private String text1;
	private String learnMoreText;
	
	private Boolean eligibleToShareRefCode;
	private String shareErrorMessage;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getReferrerCode() {
		return referrerCode;
	}
	public void setReferrerCode(String referrerCode) {
		this.referrerCode = referrerCode;
	}
	public String getImageUrl() {
		imageUrl = MapUtil.getCustomerReferralPageStaticImage();
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getFbMsg() {
		return fbMsg;
	}
	public void setFbMsg(String fbMsg) {
		this.fbMsg = fbMsg;
	}
	public String getGmailMsg() {
		return gmailMsg;
	}
	public void setGmailMsg(String gmailMsg) {
		this.gmailMsg = gmailMsg;
	}
	public String getTwitterMsg() {
		return twitterMsg;
	}
	public void setTwitterMsg(String twitterMsg) {
		this.twitterMsg = twitterMsg;
	}
	public String getPinterestMsg() {
		return pinterestMsg;
	}
	public void setPinterestMsg(String pinterestMsg) {
		this.pinterestMsg = pinterestMsg;
	}
	public String getLinkedInMsg() {
		return linkedInMsg;
	}
	public void setLinkedInMsg(String linkedInMsg) {
		this.linkedInMsg = linkedInMsg;
	}
	public String getInstagramMsg() {
		return instagramMsg;
	}
	public void setInstagramMsg(String instagramMsg) {
		this.instagramMsg = instagramMsg;
	}
	public String getNormalMsg() {
		return normalMsg;
	}
	public void setNormalMsg(String normalMsg) {
		this.normalMsg = normalMsg;
	}
	public String getShareImageUrl() {
		shareImageUrl = "https://www.rewardthefan.com/resources/img/app_bg.png";
		return shareImageUrl;
	}
	public void setShareImageUrl(String shareImageUrl) {
		this.shareImageUrl = shareImageUrl;
	}
	public String getShareRTFUrl() {
		return shareRTFUrl;
	}
	public void setShareRTFUrl(String shareRTFUrl) {
		this.shareRTFUrl = shareRTFUrl;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getEmailMsgDesk() {
		return emailMsgDesk;
	}
	public void setEmailMsgDesk(String emailMsgDesk) {
		this.emailMsgDesk = emailMsgDesk;
	}
	public String getFbMsgDesk() {
		return fbMsgDesk;
	}
	public void setFbMsgDesk(String fbMsgDesk) {
		this.fbMsgDesk = fbMsgDesk;
	}
	public String getGmailMsgDesk() {
		return gmailMsgDesk;
	}
	public void setGmailMsgDesk(String gmailMsgDesk) {
		this.gmailMsgDesk = gmailMsgDesk;
	}
	public String getTwitterMsgDesk() {
		return twitterMsgDesk;
	}
	public void setTwitterMsgDesk(String twitterMsgDesk) {
		this.twitterMsgDesk = twitterMsgDesk;
	}
	public String getPinterestMsgDesk() {
		return pinterestMsgDesk;
	}
	public void setPinterestMsgDesk(String pinterestMsgDesk) {
		this.pinterestMsgDesk = pinterestMsgDesk;
	}
	public String getLinkedInMsgDesk() {
		return linkedInMsgDesk;
	}
	public void setLinkedInMsgDesk(String linkedInMsgDesk) {
		this.linkedInMsgDesk = linkedInMsgDesk;
	}
	public String getInstagramMsgDesk() {
		return instagramMsgDesk;
	}
	public void setInstagramMsgDesk(String instagramMsgDesk) {
		this.instagramMsgDesk = instagramMsgDesk;
	}
	public String getWatsappMsgDesk() {
		return watsappMsgDesk;
	}
	public void setWatsappMsgDesk(String watsappMsgDesk) {
		this.watsappMsgDesk = watsappMsgDesk;
	}
	public String getAndroidMsgDesk() {
		return androidMsgDesk;
	}
	public void setAndroidMsgDesk(String androidMsgDesk) {
		this.androidMsgDesk = androidMsgDesk;
	}
	public String getiPhoneMsgDesk() {
		return iPhoneMsgDesk;
	}
	public void setiPhoneMsgDesk(String iPhoneMsgDesk) {
		this.iPhoneMsgDesk = iPhoneMsgDesk;
	}
	public Boolean getEligibleToShareRefCode() {
		return eligibleToShareRefCode;
	}
	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}
	public String getShareErrorMessage() {
		if(null == eligibleToShareRefCode || !eligibleToShareRefCode){
			shareErrorMessage="This Perk is available to registered customers who have made a purchase from us";
		}else{
			shareErrorMessage="";
		}
		return shareErrorMessage;
	}
	public void setShareErrorMessage(String shareErrorMessage) {
		this.shareErrorMessage = shareErrorMessage;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getLearnMoreText() {
		return learnMoreText;
	}
	public void setLearnMoreText(String learnMoreText) {
		this.learnMoreText = learnMoreText;
	}
	
	
	
	
}
