package com.zonesws.webservices.utils.list;

import com.zonesws.webservices.enums.OrderType;
import com.zonesws.webservices.utils.TicketUtil;

public class RewardDetails {
	
	private OrderType orderType;
	private Integer orderNo;
	private String orderTotal;
	private Double orderTotalAsDouble;
	private String orderDate;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String rewards;
	private Double rewardsAsDouble;
	private String activeDate;
	
	
	
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public Double getOrderTotalAsDouble() {
		return orderTotalAsDouble;
	}
	public void setOrderTotalAsDouble(Double orderTotalAsDouble) {
		this.orderTotalAsDouble = orderTotalAsDouble;
	}
	
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public Double getRewardsAsDouble() {
		return rewardsAsDouble;
	}
	public void setRewardsAsDouble(Double rewardsAsDouble) {
		this.rewardsAsDouble = rewardsAsDouble;
	}
	public String getActiveDate() {
		
		return activeDate;
	}
	public void setActiveDate(String activeDate) {
		this.activeDate = activeDate;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getOrderTotal() {
		if(orderTotalAsDouble == null){
			orderTotalAsDouble=0.00;
		}
		try {
			orderTotal = TicketUtil.getRoundedValueString(orderTotalAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orderTotal;
	}
	public void setOrderTotal(String orderTotal) {
		this.orderTotal = orderTotal;
	}
	public String getRewards() {
		if(rewardsAsDouble == null){
			rewardsAsDouble=0.00;
		}
		try {
			rewards = TicketUtil.getRoundedValueString(rewardsAsDouble);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rewards;
	}
	public void setRewards(String rewards) {
		this.rewards = rewards;
	}

	
}
