package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("RtfTixList")
public class RtfEvent {
	
	private Integer eId;
	private String eName;
	private String eDateTime;
	private String eDate;
	private String eTime;
	private Integer eVId;
	private String vName;
	private String vCity;
	private String vState;
	private String vCountry;
	private String vPinCode;
	private String ePriceTag;
	
	
	
	public RtfEvent(Integer eId, String eName, String eDateTime, String eDate, String eTime, Integer eVId,
			String vName, String vCity, String vState, String vCountry, String vPinCode, String ePriceTag) {
		super();
		this.eId = eId;
		this.eName = eName;
		this.eDateTime = eDateTime;
		this.eDate = eDate;
		this.eTime = eTime;
		this.eVId = eVId;
		this.vName = vName;
		this.vCity = vCity;
		this.vState = vState;
		this.vCountry = vCountry;
		this.vPinCode = vPinCode;
		this.ePriceTag = ePriceTag;
	}
	public Integer geteId() {
		return eId;
	}
	public void seteId(Integer eId) {
		this.eId = eId;
	}
	public String geteName() {
		return eName;
	}
	public void seteName(String eName) {
		this.eName = eName;
	}
	 
	public String geteDate() {
		return eDate;
	}
	public void seteDate(String eDate) {
		this.eDate = eDate;
	}
	public String geteTime() {
		return eTime;
	}
	public void seteTime(String eTime) {
		this.eTime = eTime;
	}
	public Integer geteVId() {
		return eVId;
	}
	public void seteVId(Integer eVId) {
		this.eVId = eVId;
	}
	public String getvName() {
		return vName;
	}
	public void setvName(String vName) {
		this.vName = vName;
	}
	public String getvCity() {
		return vCity;
	}
	public void setvCity(String vCity) {
		this.vCity = vCity;
	}
	public String getvState() {
		return vState;
	}
	public void setvState(String vState) {
		this.vState = vState;
	}
	public String getvCountry() {
		return vCountry;
	}
	public void setvCountry(String vCountry) {
		this.vCountry = vCountry;
	}
	public String getvPinCode() {
		return vPinCode;
	}
	public void setvPinCode(String vPinCode) {
		this.vPinCode = vPinCode;
	}
	public String getePriceTag() {
		return ePriceTag;
	}
	public void setePriceTag(String ePriceTag) {
		this.ePriceTag = ePriceTag;
	}
	public String geteDateTime() {
		return eDateTime;
	}
	public void seteDateTime(String eDateTime) {
		this.eDateTime = eDateTime;
	}
	
}
