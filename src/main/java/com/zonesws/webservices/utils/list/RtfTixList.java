package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TixByQty;

@XStreamAlias("RtfTixList")
public class RtfTixList {
	
	private Integer status;
	private Error error; 
	private RtfEvent event;
	private Double cuRwards;
	private String disCuRwards;
	private List<TixByQty> tixList;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public RtfEvent getEvent() {
		return event;
	}
	public void setEvent(RtfEvent event) {
		this.event = event;
	}
	public List<TixByQty> getTixList() {
		return tixList;
	}
	public void setTixList(List<TixByQty> tixList) {
		this.tixList = tixList;
	}
	public Double getCuRwards() {
		return cuRwards;
	}
	public void setCuRwards(Double cuRwards) {
		this.cuRwards = cuRwards;
	}
	public String getDisCuRwards() {
		return disCuRwards;
	}
	public void setDisCuRwards(String disCuRwards) {
		this.disCuRwards = disCuRwards;
	}
	
}
