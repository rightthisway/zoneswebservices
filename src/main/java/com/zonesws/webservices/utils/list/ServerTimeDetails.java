package com.zonesws.webservices.utils.list;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("ServerTimeInfo")
public class ServerTimeDetails {
	private Integer status;
	private Date serverTime;
	private Error error; 
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getServerTime() {
		return serverTime;
	}
	public void setServerTime(Date serverTime) {
		this.serverTime = serverTime;
	}
	
}
