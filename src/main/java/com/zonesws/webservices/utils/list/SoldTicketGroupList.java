package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.HoldTicketGroup;
import com.zonesws.webservices.utils.SoldTicketGroup;

@XStreamAlias("SoldTicketGroupList")
public class SoldTicketGroupList {

	private Event event;
	private List<SoldTicketGroup> soldTicketGroups;
	
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public List<SoldTicketGroup> getSoldTicketGroups() {
		return soldTicketGroups;
	}
	public void setSoldTicketGroups(List<SoldTicketGroup> soldTicketGroups) {
		this.soldTicketGroups = soldTicketGroups;
	}
	
}
