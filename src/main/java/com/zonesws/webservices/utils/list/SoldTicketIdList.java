package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.DiscountDetails;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TicketGroupQty;
import com.zonesws.webservices.utils.ZoneEvent;

@XStreamAlias("SoldTicketList")
public class SoldTicketIdList {
	private Integer status;
	private Error error; 
	
	private List<Integer> ticketGroupIds;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Integer> getTicketGroupIds() {
		return ticketGroupIds;
	}

	public void setTicketGroupIds(List<Integer> ticketGroupIds) {
		this.ticketGroupIds = ticketGroupIds;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}
	
	
	
	
	
}
