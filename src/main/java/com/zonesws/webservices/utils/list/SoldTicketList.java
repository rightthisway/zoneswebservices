package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.AlreadySoldTicketGroup;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.InvalidHoldTicket;
import com.zonesws.webservices.utils.SoldTicketGroup;

@XStreamAlias("SoldTicketList")
public class SoldTicketList {

	private Error error; 
	private Integer userOrderId;
	private List<SoldTicketGroupList> soldTicketGroupLists;
	private List<AlreadySoldTicketGroup> alreadySoldTicketGroupLists;
	private List<InvalidHoldTicket> invalidHoldTicketList;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public List<SoldTicketGroupList> getSoldTicketGroupLists() {
		return soldTicketGroupLists;
	}
	public void setSoldTicketGroupLists(
			List<SoldTicketGroupList> soldTicketGroupLists) {
		this.soldTicketGroupLists = soldTicketGroupLists;
	}
	public List<InvalidHoldTicket> getInvalidHoldTicketList() {
		return invalidHoldTicketList;
	}
	public void setInvalidHoldTicketList(
			List<InvalidHoldTicket> invalidHoldTicketList) {
		this.invalidHoldTicketList = invalidHoldTicketList;
	}
	public List<AlreadySoldTicketGroup> getAlreadySoldTicketGroupLists() {
		return alreadySoldTicketGroupLists;
	}
	public void setAlreadySoldTicketGroupLists(
			List<AlreadySoldTicketGroup> alreadySoldTicketGroupLists) {
		this.alreadySoldTicketGroupLists = alreadySoldTicketGroupLists;
	}
	public Integer getUserOrderId() {
		return userOrderId;
	}
	public void setUserOrderId(Integer userOrderId) {
		this.userOrderId = userOrderId;
	}
	
	
}
