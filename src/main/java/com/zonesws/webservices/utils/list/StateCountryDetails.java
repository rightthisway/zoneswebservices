package com.zonesws.webservices.utils.list;

import java.util.Collection;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Country;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("StateAndCountryDetails")
public class StateCountryDetails {
	private Integer status;
	private Error error; 
	private Collection<Country> countryList;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Collection<Country> getCountryList() {
		return countryList;
	}
	public void setCountryList(Collection<Country> countryList) {
		this.countryList = countryList;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	

	
	
}
