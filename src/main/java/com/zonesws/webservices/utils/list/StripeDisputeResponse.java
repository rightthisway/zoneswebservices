package com.zonesws.webservices.utils.list;

import java.util.List;

import com.zonesws.webservices.data.StripeDispute;
import com.zonesws.webservices.utils.Error;

public class StripeDisputeResponse {

	private Integer status;
	private Error error; 
	private String message;
	private List<StripeDispute> disputes;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<StripeDispute> getDisputes() {
		return disputes;
	}
	public void setDisputes(List<StripeDispute> disputes) {
		this.disputes = disputes;
	}
	
	
}
