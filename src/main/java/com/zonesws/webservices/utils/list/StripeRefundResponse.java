package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CustomerStripeRefund;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("StripeRefunds")
public class StripeRefundResponse {

	private Integer status;
	private Error error; 
	private List<CustomerStripeRefund> stripeRefunds;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<CustomerStripeRefund> getStripeRefunds() {
		return stripeRefunds;
	}
	public void setStripeRefunds(List<CustomerStripeRefund> stripeRefunds) {
		this.stripeRefunds = stripeRefunds;
	}
	
	
}
