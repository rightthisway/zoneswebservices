package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CrownJewelLeagues;

@XStreamAlias("SuperFanLeague")
public class SuperFanLeague {
	
	private String categoryName;
	private List<CrownJewelLeagues> leagues;
	
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public List<CrownJewelLeagues> getLeagues() {
		return leagues;
	}
	public void setLeagues(List<CrownJewelLeagues> leagues) {
		this.leagues = leagues;
	}
	
	
	
}
