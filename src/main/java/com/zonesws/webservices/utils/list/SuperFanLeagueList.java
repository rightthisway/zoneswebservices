package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("FantasyCategories")
public class SuperFanLeagueList {
	private Integer status;
	private Error error; 
	private List<FantasyCategories> fantasyCategories;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<FantasyCategories> getFantasyCategories() {
		return fantasyCategories;
	}
	public void setFantasyCategories(List<FantasyCategories> fantasyCategories) {
		this.fantasyCategories = fantasyCategories;
	}
	
	
	
}
