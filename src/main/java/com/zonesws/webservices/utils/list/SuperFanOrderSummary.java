package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CustomerFantasyOrder;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("SuperFanOrderSummary")
public class SuperFanOrderSummary {
	private Integer status;
	private Error error;
	private String pointsNeeded;
	private String pointsNeededText;
	private String availablePoints;
	private String pointsRemaining;
	private String message;
	private CustomerFantasyOrder futureOrderConfirmation;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getPointsNeeded() {
		return pointsNeeded;
	}
	public void setPointsNeeded(String pointsNeeded) {
		this.pointsNeeded = pointsNeeded;
	}
	public String getPointsRemaining() {
		return pointsRemaining;
	}
	public void setPointsRemaining(String pointsRemaining) {
		this.pointsRemaining = pointsRemaining;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public CustomerFantasyOrder getFutureOrderConfirmation() {
		return futureOrderConfirmation;
	}
	public void setFutureOrderConfirmation(
			CustomerFantasyOrder futureOrderConfirmation) {
		this.futureOrderConfirmation = futureOrderConfirmation;
	}
	public String getPointsNeededText() {
		return pointsNeededText;
	}
	public void setPointsNeededText(String pointsNeededText) {
		this.pointsNeededText = pointsNeededText;
	}
	public String getAvailablePoints() {
		return availablePoints;
	}
	public void setAvailablePoints(String availablePoints) {
		this.availablePoints = availablePoints;
	}
	
	
}
