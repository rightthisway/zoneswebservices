package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("SuperFanStat")
public class SuperFanStat {
	
	private Integer status;
	private Error error; 
	private Integer custId;
	private String games;
	private String referrals;
	private String superFanEntries;
	 
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public String getGames() {
		return games;
	}
	public void setGames(String games) {
		this.games = games;
	}
	public String getReferrals() {
		return referrals;
	}
	public void setReferrals(String referrals) {
		this.referrals = referrals;
	}
	public String getSuperFanEntries() {
		return superFanEntries;
	}
	public void setSuperFanEntries(String superFanEntries) {
		this.superFanEntries = superFanEntries;
	}
	
}
