package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("SuperFanTeamList")
public class SuperFanTeamList {
	private Integer status;
	private Error error;
	private Integer leagueId;
	private String leagueName;
	private String leagueType;
	private Event event;
	private List<SportsTeam> sportsTeams;
	private List<CrownEventCity> crownEventCities;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getLeagueName() {
		return leagueName;
	}
	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}
	public String getLeagueType() {
		return leagueType;
	}
	public void setLeagueType(String leagueType) {
		this.leagueType = leagueType;
	}
	public List<SportsTeam> getSportsTeams() {
		return sportsTeams;
	}
	public void setSportsTeams(List<SportsTeam> sportsTeams) {
		this.sportsTeams = sportsTeams;
	}
	public List<CrownEventCity> getCrownEventCities() {
		return crownEventCities;
	}
	public void setCrownEventCities(List<CrownEventCity> crownEventCities) {
		this.crownEventCities = crownEventCities;
	}
	public Integer getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	
}
