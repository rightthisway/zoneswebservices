package com.zonesws.webservices.utils.list;



public class TeamCardImageDetails {
	private Integer artistId;
	private String artistImage;
	private Integer grandChildCategoryId;
	private String grandChildImage;
	private Integer childCategoryId;
	private String childImage;
	private Integer parentCategoryId;
	private String parentImage;
	
	public String getArtistImage() {
		return artistImage;
	}
	public void setArtistImage(String artistImage) {
		this.artistImage = artistImage;
	}
	public String getGrandChildImage() {
		return grandChildImage;
	}
	public void setGrandChildImage(String grandChildImage) {
		this.grandChildImage = grandChildImage;
	}
	public String getChildImage() {
		return childImage;
	}
	public void setChildImage(String childImage) {
		this.childImage = childImage;
	}
	public String getParentImage() {
		return parentImage;
	}
	public void setParentImage(String parentImage) {
		this.parentImage = parentImage;
	}
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}
	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}
	public Integer getChildCategoryId() {
		return childCategoryId;
	}
	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
}