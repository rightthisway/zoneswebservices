package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.CategoryTicketGroup;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("TicketGroupLockStatus")
public class TicketGroupLockStatus {
	private Integer status;
	private Error error; 
	private String buttonValue;
	private Boolean overRideButton;
	private String lockStatus;
	private String message;
	private String deviceId;
	private String timeLeft;
	private Integer catgeoryTicketGroupId;
	private Boolean showSavedCards;
	private String encryptionKey;
	private Integer lockId;
	private Event event;
	private CategoryTicketGroup categoryTicketGroup;
	private String ticketDownloadOption1;
	private String ticketDownloadOption2;
	private Boolean onDayBeforeEventTixDownload;
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public String getMessage() {
		if(null == message || message.isEmpty()){
			message="";	
		}
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDeviceId() {
		if(null == deviceId || deviceId.isEmpty()){
			deviceId="";	
		}
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getTimeLeft() {
		if(null == timeLeft || timeLeft.isEmpty()){
			timeLeft="00:00";	
		}
		return timeLeft;
	}
	public void setTimeLeft(String timeLeft) {
		this.timeLeft = timeLeft;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getLockStatus() {
		if(null == lockStatus || lockStatus.isEmpty()){
			lockStatus="";	
		}
		return lockStatus;
	}
	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
	
	public CategoryTicketGroup getCategoryTicketGroup() {
		return categoryTicketGroup;
	}
	public void setCategoryTicketGroup(CategoryTicketGroup categoryTicketGroup) {
		this.categoryTicketGroup = categoryTicketGroup;
	}
	
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public Integer getCatgeoryTicketGroupId() {
		if(catgeoryTicketGroupId == null){
			catgeoryTicketGroupId=0;
		}
		return catgeoryTicketGroupId;
	}
	public void setCatgeoryTicketGroupId(Integer catgeoryTicketGroupId) {
		this.catgeoryTicketGroupId = catgeoryTicketGroupId;
	}
	public Boolean getShowSavedCards() {
		if(null == showSavedCards){
			showSavedCards=false;
		}
		return showSavedCards;
	}
	public void setShowSavedCards(Boolean showSavedCards) {
		this.showSavedCards = showSavedCards;
	}
	
	public String getButtonValue() {
		return buttonValue;
	}
	public void setButtonValue(String buttonValue) {
		this.buttonValue = buttonValue;
	}
	public Boolean getOverRideButton() {
		if(null == overRideButton){
			overRideButton = false;
		}
		return overRideButton;
	}
	public void setOverRideButton(Boolean overRideButton) {
		this.overRideButton = overRideButton;
	}
	public String getEncryptionKey() {
		return encryptionKey;
	}
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
	public Integer getLockId() {
		return lockId;
	}
	public void setLockId(Integer lockId) {
		this.lockId = lockId;
	}
	public String getTicketDownloadOption1() {
		//ticketDownloadOption1 = "";//"<font color=#012e4f>Please upload my tickets as soon as they are ready for download.</font>";
		return ticketDownloadOption1;
	}
	public void setTicketDownloadOption1(String ticketDownloadOption1) {
		this.ticketDownloadOption1 = ticketDownloadOption1;
	}
	public String getTicketDownloadOption2() {
		return ticketDownloadOption2;
	}
	public void setTicketDownloadOption2(String ticketDownloadOption2) {
		this.ticketDownloadOption2 = ticketDownloadOption2;
	}
	
	public Boolean getOnDayBeforeEventTixDownload() {
		if(null == onDayBeforeEventTixDownload){
			onDayBeforeEventTixDownload = false;
		}
		return onDayBeforeEventTixDownload;
	}
	public void setOnDayBeforeEventTixDownload(Boolean onDayBeforeEventTixDownload) {
		this.onDayBeforeEventTixDownload = onDayBeforeEventTixDownload;
	}
}
