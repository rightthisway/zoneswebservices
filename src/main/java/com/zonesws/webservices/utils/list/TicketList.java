package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.Event;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.TicketGroupQty;

@XStreamAlias("TicketList")
public class TicketList {
	private Integer status;
	private Error error; 
	private Event event;
	//private DiscountDetails discountDetails;
	private List<TicketGroupQty> ticketGroupQtyList;
	
	private String aRefType;
	private String aRefValue;
	private Boolean eligibleToShareRefCode;
	private String shareErrorMessage;
	
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	/*public DiscountDetails getDiscountDetails() {
		return discountDetails;
	}
	public void setDiscountDetails(DiscountDetails discountDetails) {
		this.discountDetails = discountDetails;
	}*/
	/*public TicketGroupQtyMap getTicketGroupQtyMap() {
		return ticketGroupQtyMap;
	}
	public void setTicketGroupQtyMap(TicketGroupQtyMap ticketGroupQtyMap) {
		this.ticketGroupQtyMap = ticketGroupQtyMap;
	}*/
	public List<TicketGroupQty> getTicketGroupQtyList() {
		return ticketGroupQtyList;
	}
	public void setTicketGroupQtyList(List<TicketGroupQty> ticketGroupQtyList) {
		this.ticketGroupQtyList = ticketGroupQtyList;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getaRefType() {
		return aRefType;
	}
	public void setaRefType(String aRefType) {
		this.aRefType = aRefType;
	}
	public String getaRefValue() {
		return aRefValue;
	}
	public void setaRefValue(String aRefValue) {
		this.aRefValue = aRefValue;
	}
	public Boolean getEligibleToShareRefCode() {
		if(null == eligibleToShareRefCode){
			eligibleToShareRefCode = false;
		}
		return eligibleToShareRefCode;
	}
	public void setEligibleToShareRefCode(Boolean eligibleToShareRefCode) {
		this.eligibleToShareRefCode = eligibleToShareRefCode;
	}
	
	public String getShareErrorMessage() {
		if(null == shareErrorMessage){
			shareErrorMessage="";
		}
		return shareErrorMessage;
	}
	public void setShareErrorMessage(String shareErrorMessage) {
		this.shareErrorMessage = shareErrorMessage;
	}
}
