package com.zonesws.webservices.utils.list;

import java.sql.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.zonesws.webservices.utils.DateConverter;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("TourPriceDetails")
public class TourTicketDetails {
	
	
	private Error error; 
	private Integer artistId;
	private String artistName;
	@XStreamConverter(DateConverter.class)
	private Date startDate;
	@XStreamConverter(DateConverter.class)
	private Date endDate;
	private Double minPrice;
	private Double maxPrice;
	
	
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public Integer getTourId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}
	public Double getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}
	
	
	
}
