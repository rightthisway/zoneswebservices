package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.UserOrder;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("OrderList")
public class UserOrderList {
	private Integer status;
	private Error error; 
	private Integer totalOrders;
	private List<UserOrder> orderList;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public List<UserOrder> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<UserOrder> orderList) {
		this.orderList = orderList;
	}
	public Integer getTotalOrders() {
		if(null == totalOrders){
			totalOrders=0;	
		}
		return totalOrders;
	}
	public void setTotalOrders(Integer totalOrders) {
		this.totalOrders = totalOrders;
	}
	public Integer getStatus() {
		
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
