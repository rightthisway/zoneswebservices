package com.zonesws.webservices.utils.list;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.data.TicketOrder;
import com.zonesws.webservices.utils.Error;
import com.zonesws.webservices.utils.ZoneEvent;

@XStreamAlias("List")
public class UserOrdrList {

	private Error error; 
	private Integer totalOrders;
	private List<TicketOrder> orderList;
	
	
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public List<TicketOrder> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<TicketOrder> orderList) {
		this.orderList = orderList;
	}
	public Integer getTotalOrders() {
		return totalOrders;
	}
	public void setTotalOrders(Integer totalOrders) {
		this.totalOrders = totalOrders;
	}
	
	
}
