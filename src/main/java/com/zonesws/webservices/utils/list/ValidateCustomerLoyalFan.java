package com.zonesws.webservices.utils.list;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.zonesws.webservices.utils.Error;

@XStreamAlias("ValidateCustomerLoyalFan")
public class ValidateCustomerLoyalFan {
	
	private Integer status;
	private Error error; 
	private String message;
	private Boolean okButton;
	private Boolean continueCancelButton;
	private Boolean redirectTicketListing;
	private Boolean isNormalPrice;
	private Boolean normalPriceProceed;
	private Boolean showOtherTeamsButton;
	private String otherTeamButtonText;
	private Boolean isLoyalFanForTicTracker;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getOkButton() {
		return okButton;
	}
	public void setOkButton(Boolean okButton) {
		this.okButton = okButton;
	}
	public Boolean getContinueCancelButton() {
		return continueCancelButton;
	}
	public void setContinueCancelButton(Boolean continueCancelButton) {
		this.continueCancelButton = continueCancelButton;
	}
	public Boolean getRedirectTicketListing() {
		return redirectTicketListing;
	}
	public void setRedirectTicketListing(Boolean redirectTicketListing) {
		this.redirectTicketListing = redirectTicketListing;
	}
	public Boolean getIsNormalPrice() {
		return isNormalPrice;
	}
	public void setIsNormalPrice(Boolean isNormalPrice) {
		this.isNormalPrice = isNormalPrice;
	}
	public Boolean getNormalPriceProceed() {
		return normalPriceProceed;
	}
	public void setNormalPriceProceed(Boolean normalPriceProceed) {
		this.normalPriceProceed = normalPriceProceed;
	}
	public Boolean getShowOtherTeamsButton() {
		if(showOtherTeamsButton == null){
			showOtherTeamsButton=false;
		}
		return showOtherTeamsButton;
	}
	public void setShowOtherTeamsButton(Boolean showOtherTeamsButton) {
		this.showOtherTeamsButton = showOtherTeamsButton;
	}
	public String getOtherTeamButtonText() {
		if(otherTeamButtonText ==  null){
			otherTeamButtonText="";
		}
		return otherTeamButtonText;
	}
	public void setOtherTeamButtonText(String otherTeamButtonText) {
		this.otherTeamButtonText = otherTeamButtonText;
	}
	
	public Boolean getIsLoyalFanForTicTracker() {
		if(null == isLoyalFanForTicTracker){
			isLoyalFanForTicTracker = false;
		}
		return isLoyalFanForTicTracker;
	}
	public void setIsLoyalFanForTicTracker(Boolean isLoyalFanForTicTracker) {
		this.isLoyalFanForTicTracker = isLoyalFanForTicTracker;
	}
	
	
	
}
