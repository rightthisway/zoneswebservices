package com.zonesws.webservices.utils.list;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("VenueResult")
public class VenueResult {

	private String venueName;
	private Integer venueId;
	@JsonIgnore
	private Integer totalVenueCount;
	@JsonIgnore
	private Integer totalVenues;
	private String venueCity;
	private String venueState;
	private String venueCountry;
	
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VenueResult other = (VenueResult) obj;
		if (venueId == null) {
			if (other.venueId != null)
				return false;
		} else if (!venueId.equals(other.venueId))
			return false;
		return true;
	}
	public Integer getTotalVenueCount() {
		if(null == totalVenueCount){
			totalVenueCount = 0;
		}
		return totalVenueCount;
	}
	public void setTotalVenueCount(Integer totalVenueCount) {
		this.totalVenueCount = totalVenueCount;
	}
	public Integer getTotalVenues() {
		return totalVenues;
	}
	public void setTotalVenues(Integer totalVenues) {
		this.totalVenues = totalVenues;
	}
	public String getVenueCity() {
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	public String getVenueState() {
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	public String getVenueCountry() {
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	
	
	
}
