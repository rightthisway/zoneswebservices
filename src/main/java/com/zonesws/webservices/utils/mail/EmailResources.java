package com.zonesws.webservices.utils.mail;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

public class EmailResources {

	
	public static void main(String[] args) {
		
		 
		
		System.out.println(getInlineAttachment("AboutUsMap.png"));
	}

	public static File getInlineAttachmentOLD(String fileName) {
		try {
			ClassLoader classLoader = EmailResources.class.getClassLoader();
			
			fileName = "email-resources/"+fileName;
			
			fileName = classLoader.getResource(fileName).getFile();
			
			fileName = fileName.replaceAll("%20", " ");
			
			System.out.println("Email Inline Attachement File Name : "+fileName);
			 
		    File file = new File(fileName);
		    
		    System.out.println("Email Inline Attachement File : "+file);
		   
		    System.out.println( file.exists());
		    
		    System.out.println(file.canRead());
		    
		    if(null != file && file.exists()) {
		    	return file;
		    }
			return null;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	  }
	
	public static File getInlineAttachment(String fileName) {
		try {
			
			fileName = "C:\\Tomcat 7.0\\webapps\\zoneswebservices-0.1\\WEB-INF\\classes\\email-resources\\"+fileName;
		 
			System.out.println("Email Inline Attachement File Name : "+fileName);
			 
		    File file = new File(fileName);
		    
		    System.out.println("Email Inline Attachement File : "+file);
		   
		    System.out.println( file.exists());
		    
		    System.out.println(file.canRead());
		    
		    if(null != file && file.exists()) {
		    	return file;
		    }
			return null;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	  }
	  
	  private static String getFileWithUtil1(String fileName) {

			String result = "";
				
			ClassLoader classLoader = EmailResources.class.getClassLoader();
			try {
			    result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
			 System.out.println(result);
			return result;
		  }
	  
}
