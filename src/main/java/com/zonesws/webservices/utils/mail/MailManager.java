package com.zonesws.webservices.utils.mail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.zonesws.webservices.dao.implementaion.DAORegistry;
import com.zonesws.webservices.data.Property;

public final class MailManager extends Thread implements InitializingBean{
	public final int MAX_RETRY = 3;
	public final int RETRY_WAIT = 2000;

	private static final int DEFAULT_TIMEOUT = 30000;	

	private String staticUrl;
	private Boolean sendEmail;
	
	// mail in the queue
	private List<Mail> mails = new ArrayList<Mail>();;
	
	private VelocityEngine velocityEngine;
	String smtpHost;
	int smtpPort;
	String smtpSecureConnection;
	Boolean smtpAuth;
	String username;
	String password;
	String emailFrom ;
	public void afterPropertiesSet() throws Exception {
		initMailManager();
	}
	void initMailManager(){
		smtpHost = DAORegistry.getPropertyDAO().get("mail.smtp.host").getValue().trim();
		smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().get("mail.smtp.port").getValue().trim());
		smtpSecureConnection = DAORegistry.getPropertyDAO().get("mail.smtp.secure.connection").getValue().trim();
		smtpAuth = Boolean.parseBoolean(DAORegistry.getPropertyDAO().get("mail.smtp.auth").getValue());
		
		username = null;
		
		Property smtpUserNameProperty = DAORegistry.getPropertyDAO().get("mail.smtp.user.name");
		if (smtpUserNameProperty != null) {
			username = smtpUserNameProperty.getValue();
		}
		
		password = null;
		Property smtpUserPasswordProperty = DAORegistry.getPropertyDAO().get("mail.smtp.user.pasward");
		if (smtpUserPasswordProperty != null) {
			password = smtpUserPasswordProperty.getValue();
			if (password.isEmpty()) {
				password = null;
			}
		}
		
		emailFrom = DAORegistry.getPropertyDAO().get("mail.smtp.reply.from").getValue();
	}
	public MailManager() {
		start();
	}
	
	public void run() {
		while(true) {
			try {
				Mail mail = null;

				synchronized (this) {					
					if (mails.size() > 0) {
						mail = mails.remove(0);
					}
				}
				
				if (mail == null) {
					Thread.sleep(5000);
					continue;
				}
				
				sendMailNow(smtpHost, smtpPort, "smtp", smtpSecureConnection, smtpAuth, username, password, 
						mail.getFromName(), emailFrom, 
						mail.getToAddress(), mail.getCcAddress(), mail.getBccAddress(), mail.getSubject(), mail.getResource(), mail.getMap(), mail.getMimeType(), mail.getAttachments());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	

	public void sendMailNow(String smtpHost, int smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType) throws Exception {
		sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, toAddress, subject, resource, map, mimeType, null);
	}
	
	private Address[] getAddresses(String addr) throws Exception {
		String[] recipients = addr.split(",");
//		List<Address> addressList = new ArrayList<Address>();
		Address[] addressList = new Address[recipients.length];
		int i=0;
		for (String recipient: recipients) {
			if (!recipient.isEmpty()) {
				addressList[i]=new InternetAddress(recipient);
				i++;
			}
		}
		return addressList;
	}
	public void sendMailNow(String transportProtocol,final String toAddress,final String ccAddress,
			final String bccAddress, final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments) throws Exception{
			sendMailNow(smtpHost, smtpPort,transportProtocol,
				smtpSecureConnection,
				smtpAuth, username,password,
				emailFrom,  //fromName
				emailFrom,
				toAddress,
				ccAddress,
				bccAddress,
				subject, resource, map,mimeType,
				attachments);
		
	}
	public void sendMailNow(String transportProtocol,final String email,final String toAddress,final String ccAddress,
			final String bccAddress, final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments) throws Exception{
			sendMailNow(smtpHost, smtpPort,transportProtocol,
				smtpSecureConnection,
				smtpAuth, username,password,
				email,  //fromName
				email,
				toAddress,
				ccAddress,
				bccAddress,
				subject, resource, map,mimeType,
				attachments);
		
	}
	
	public void sendMailNow(String transportProtocol,final String email,final String toAddress,final String ccAddress,
			final String bccAddress, final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments,MailAttachment[] inlineImages,String filePath) throws Exception{
		if(sendEmail){
			sendMailNow(smtpHost, smtpPort,transportProtocol,
					smtpSecureConnection,
					smtpAuth, username,password,
					email,  //fromName
					email,
					toAddress,
					ccAddress,
					bccAddress,
					subject, resource, map,mimeType,
					attachments,inlineImages,filePath);
		}
	}
	
	public void sendMailTest(String transportProtocol,final String email,final String toAddress,final String ccAddress,
			final String bccAddress, final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments,MailAttachment[] inlineImages,String filePath) throws Exception{
			sendMailNow(smtpHost, smtpPort,transportProtocol,
					smtpSecureConnection,
					smtpAuth, username,password,
					email,  //fromName
					email,
					toAddress,
					ccAddress,
					bccAddress,
					subject, resource, map,mimeType,
					attachments,inlineImages,filePath);
	}
	
	public void sendMailNow(String smtpHost, int smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments) throws Exception {
		sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, toAddress, null, null, subject, resource, map, mimeType, attachments);
	}
	
	public void sendMailNow(String smtpHost, int smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress,
			final String ccAddress,
			final String bccAddress,
			final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments) throws Exception {
		
		System.out.println("TRYING TO SEND MAIL " + resource);
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		props.put("mail.transport.protocol", "smtp");
		
		if (smtpSecureConnection.equalsIgnoreCase("tls")) {
			props.put("mail.smtp.starttls.enable", "true");
		}
		
		props.put("mail.smtp.connectiontimeout", DEFAULT_TIMEOUT);
		props.put("mail.smtp.timeout", DEFAULT_TIMEOUT);
		
		props.put("mail.smtp.socketFactory.port", smtpPort);
		/*if (!smtpSecureConnection.equalsIgnoreCase("no")) {
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		}*/
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");			
				
		Session session;
		if (smtpAuth) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MailAuthenticator(username, password);
			session = Session.getInstance(props, auth);
		} else {
			props.put("mail.smtp.auth", "false");
			Authenticator auth = new MailAuthenticator(username, null);
			session = Session.getInstance(props, auth);
		}

		MimeMessage message = new MimeMessage(session);

		String fullFromAddress = null;
		if (fromName == null) {
			fullFromAddress = emailFrom;			
		} else {
			fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";			
		}			

		Address fromAddr = new InternetAddress(fullFromAddress);

		message.setSubject(subject);
		message.setFrom(fromAddr);
		message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

		if (null !=toAddress && toAddress.trim().length()>0) {
			message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress));
		}
		if (null !=ccAddress && ccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress));	
		}
		if (null !=bccAddress && bccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
		}
		try{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("staticUrl", staticUrl);
		if(map!=null){
			model.putAll(map);
		}
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "template/" + resource, model);
		System.out.println(text);
		message.setSubject(subject);
		
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(text, mimeType);
		
		MimeMultipart mimeMultiPart = new MimeMultipart("related");
		mimeMultiPart.addBodyPart(mimeBodyPart);
		
		if (attachments != null) {
			for(MailAttachment attachment: attachments) {
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
//				attachmentBodyPart.setContent(new String(Base64.encodeBase64(attachment.getContent())), mimeType);
				attachmentBodyPart.setDataHandler(new DataHandler(new ByteDataSource(attachment.getContent(), attachment.getFileName(), attachment.getMimeType())));
								 
				attachmentBodyPart.setHeader("Content-Transfer-Encoding", "base64");
				attachmentBodyPart.setFileName(attachment.getFileName());
				//attachmentBodyPart.setDisposition(MimeBodyPart.ATTACHMENT);
				attachmentBodyPart.setContentID("<" + attachment.getFileName() + ">");
				mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}

		message.setContent(mimeMultiPart);

		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect(smtpHost, smtpPort, username, password);
		transport.sendMessage(message, message.getAllRecipients());
		}catch (Exception e) {
			System.out.println(e.fillInStackTrace());
		}
	}
	
	public void sendMailNow(String smtpHost, int smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress,
			final String ccAddress,
			final String bccAddress,
			final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments,MailAttachment[] inlineImages,String filePath) throws Exception {
		
		//System.out.println("TRYING TO SEND MAIL " + resource);
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		props.put("mail.transport.protocol", "smtp");
		
		if (smtpSecureConnection.equalsIgnoreCase("tls")) {
			props.put("mail.smtp.starttls.enable", "true");
		}
		
		props.put("mail.smtp.connectiontimeout", DEFAULT_TIMEOUT);
		props.put("mail.smtp.timeout", DEFAULT_TIMEOUT);
		
		props.put("mail.smtp.socketFactory.port", smtpPort);
		/*if (!smtpSecureConnection.equalsIgnoreCase("no")) {
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		}*/
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");			
				
		Session session;
		if (smtpAuth) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MailAuthenticator(username, password);
			session = Session.getInstance(props, auth);
		} else {
			props.put("mail.smtp.auth", "false");
			Authenticator auth = new MailAuthenticator(username, null);
			session = Session.getInstance(props, auth);
		}

		MimeMessage message = new MimeMessage(session);

		String fullFromAddress = null;
		if (fromName == null) {
			fullFromAddress = emailFrom;			
		} else {
			fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";			
		}			

		Address fromAddr = new InternetAddress(fullFromAddress);

		message.setSubject(subject);
		message.setFrom(fromAddr);
		message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

		if (null !=toAddress && toAddress.trim().length()>0) {
			message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress));
		}
		if (null !=ccAddress && ccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress));	
		}
		if (null !=bccAddress && bccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
		}
		boolean emailSent = true;
		try{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("staticUrl", staticUrl);
		if(map!=null){
			model.putAll(map);
		}
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "template/" + resource, model);
		//System.out.println(text);
		message.setSubject(subject);
		
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(text, mimeType);
		
		MimeMultipart mimeMultiPart = new MimeMultipart("related");
		mimeMultiPart.addBodyPart(mimeBodyPart);
		
		if (attachments != null) {
			for(MailAttachment attachment: attachments) {
				if(attachment == null || attachment.getFilePath() == null || attachment.getFilePath().isEmpty()){
					continue;
				}
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
//				attachmentBodyPart.setContent(new String(Base64.encodeBase64(attachment.getContent())), mimeType);
				attachmentBodyPart.setDataHandler(new DataHandler(new FileDataSource(attachment.getFilePath())));
				attachmentBodyPart.setHeader("Content-Transfer-Encoding", "base64");
				attachmentBodyPart.setHeader("Content-Type",attachment.getMimeType());
				attachmentBodyPart.setFileName(attachment.getFileName());
				//attachmentBodyPart.setDisposition(MimeBodyPart.ATTACHMENT);
				attachmentBodyPart.setContentID("<" + attachment.getFileName() + ">");
				mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}
		
		if (inlineImages != null) {
			for(MailAttachment attachment: inlineImages) {
				
				String filLoc = (null != filePath && !filePath.isEmpty())?filePath:attachment.getFilePath();
				/*
				File inlineAttachmentFile = EmailResources.getInlineAttachment(attachment.getFileName());
				if(null == inlineAttachmentFile) {
					continue;
				}*/
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
			   // DataSource source = new FileDataSource(inlineAttachmentFile);
				DataSource source = new FileDataSource(new File(filLoc));
			    attachmentBodyPart.setDataHandler(new DataHandler(source));
			    attachmentBodyPart.setFileName(attachment.getFileName());
			    attachmentBodyPart.setDisposition(MimeBodyPart.INLINE);
			    attachmentBodyPart.setHeader("Content-ID","<" + attachment.getFileName() + ">");
			    mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}

		message.setContent(mimeMultiPart);

		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect(smtpHost, smtpPort, username, password);
		transport.sendMessage(message, message.getAllRecipients());
		
		}catch (Exception e) {
		    emailSent = false;
			e.printStackTrace();
			System.out.println(e.fillInStackTrace());
		}
		System.out.println("EMAIL STATUS ::: ISEMAILSENT: "+emailSent+", TOADDRESS :"+toAddress+", SUBJECT :"+subject);
	}

	
	public void sendMail(final String fromName, final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType) {
		sendMail(fromName, toAddress, null, null, subject, resource, map, mimeType, null);
	}

	public void sendMail(final String fromName, final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
		sendMail(fromName, toAddress, null, null, subject, resource, map, mimeType, attachments);		
	}

	public void sendMail(final String fromName, final String toAddress, final String ccAddress, final String bccAddress, final String subject, String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
		synchronized (this) {
			mails.add(new Mail(
					fromName,
					toAddress, ccAddress, bccAddress, subject, resource, map, mimeType, attachments
			));
		}
	}

	public void sendMail(final String fromName, final String[] toAddress, final String subject, String resource, Map<String, Object> map, String mimeType) {
		sendMail(fromName, toAddress, subject, resource, map, mimeType, null);
	}

	public void sendMail(final String fromName, final String[] toAddress, final String subject, String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
		synchronized (this) {
			for(String address: toAddress) {
				address = address.trim();
				if (address.length() == 0) {
					continue;
				}
				sendMail(fromName, address, null, null, subject, resource, map, mimeType, attachments);
			}
		}
	}
	
	
	
	
	public void sendRTFLIVEMailNow(
			Boolean smtpAuth,
			String emailFrom,
			final String toAddress,
			final String ccAddress,
			final String bccAddress,
			final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments,MailAttachment[] inlineImages,String filePath) throws Exception {
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", 465);
		props.put("mail.transport.protocol", "smtp");
		//props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.connectiontimeout", DEFAULT_TIMEOUT);
		props.put("mail.smtp.timeout", DEFAULT_TIMEOUT);
		
		props.put("mail.smtp.socketFactory.port", 465);
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");			
				
		Session session;
		if (smtpAuth) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MailAuthenticator("info@rtflive.com", "Zonezata#1441");
			session = Session.getInstance(props, auth);
		} else {
			props.put("mail.smtp.auth", "false");
			Authenticator auth = new MailAuthenticator("info@rtflive.com", null);
			session = Session.getInstance(props, auth);
		}

		MimeMessage message = new MimeMessage(session);

		String fullFromAddress = null;
		/*if (emailFrom == null) {
			fullFromAddress = emailFrom;			
		} else {
			fullFromAddress = "\"" + emailFrom + "\" <" + emailFrom + ">";			
		}*/			
		fullFromAddress = emailFrom;
		Address fromAddr = new InternetAddress(fullFromAddress);

		message.setSubject(subject);
		message.setFrom(fromAddr);
		message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

		if (null !=toAddress && toAddress.trim().length()>0) {
			message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress));
		}
		if (null !=ccAddress && ccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress));	
		}
		if (null !=bccAddress && bccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
		}
		boolean emailSent = true;
		try{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("staticUrl", staticUrl);
		if(map!=null){
			model.putAll(map);
		}
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "template/" + resource, model);
		//System.out.println(text);
		message.setSubject(subject);
		
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(text, mimeType);
		
		MimeMultipart mimeMultiPart = new MimeMultipart("related");
		mimeMultiPart.addBodyPart(mimeBodyPart);
		
		if (attachments != null) {
			for(MailAttachment attachment: attachments) {
				if(attachment == null || attachment.getFilePath() == null || attachment.getFilePath().isEmpty()){
					continue;
				}
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
//				attachmentBodyPart.setContent(new String(Base64.encodeBase64(attachment.getContent())), mimeType);
				attachmentBodyPart.setDataHandler(new DataHandler(new FileDataSource(attachment.getFilePath())));
				attachmentBodyPart.setHeader("Content-Transfer-Encoding", "base64");
				attachmentBodyPart.setHeader("Content-Type",attachment.getMimeType());
				attachmentBodyPart.setFileName(attachment.getFileName());
				//attachmentBodyPart.setDisposition(MimeBodyPart.ATTACHMENT);
				attachmentBodyPart.setContentID("<" + attachment.getFileName() + ">");
				mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}
		
		if (inlineImages != null) {
			for(MailAttachment attachment: inlineImages) {
				
				String filLoc = (null != filePath && !filePath.isEmpty())?filePath:attachment.getFilePath();
				/*
				File inlineAttachmentFile = EmailResources.getInlineAttachment(attachment.getFileName());
				if(null == inlineAttachmentFile) {
					continue;
				}*/
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
			   // DataSource source = new FileDataSource(inlineAttachmentFile);
				DataSource source = new FileDataSource(new File(filLoc));
			    attachmentBodyPart.setDataHandler(new DataHandler(source));
			    attachmentBodyPart.setFileName(attachment.getFileName());
			    attachmentBodyPart.setDisposition(MimeBodyPart.INLINE);
			    attachmentBodyPart.setHeader("Content-ID","<" + attachment.getFileName() + ">");
			    mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}

		message.setContent(mimeMultiPart);

		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect("smtp.gmail.com", 465, "info@rtflive.com", "Zonezata#1441");
		transport.sendMessage(message, message.getAllRecipients());
		
		}catch (Exception e) {
		    emailSent = false;
			e.printStackTrace();
			System.out.println(e.fillInStackTrace());
		}
		System.out.println("RTFLIVE EMAIL STATUS ::: ISEMAILSENT: "+emailSent+", TOADDRESS :"+toAddress+", SUBJECT :"+subject);
	}
	
	
	
	
	
	public void sendMailUsingGMAILSMTP(String smtpHost, int smtpPort,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress,
			final String ccAddress,
			final String bccAddress,
			final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments,MailAttachment[] inlineImages,String filePath) throws Exception {
		
		System.out.println("TRYING TO SEND MAIL " + resource);
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.connectiontimeout", DEFAULT_TIMEOUT);
		props.put("mail.smtp.timeout", DEFAULT_TIMEOUT);
		props.put("mail.smtp.socketFactory.port", smtpPort);
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.quitwait", "false");			
				
		Session session;
		if (smtpAuth) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MailAuthenticator(username, password);
			session = Session.getInstance(props, auth);
		} else {
			props.put("mail.smtp.auth", "false");
			Authenticator auth = new MailAuthenticator(username, null);
			session = Session.getInstance(props, auth);
		}

		MimeMessage message = new MimeMessage(session);

		String fullFromAddress = null;
		if (fromName == null) {
			fullFromAddress = emailFrom;			
		} else {
			fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";			
		}			

		Address fromAddr = new InternetAddress(fullFromAddress);

		message.setSubject(subject);
		message.setFrom(fromAddr);
		message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

		if (null !=toAddress && toAddress.trim().length()>0) {
			message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress));
		}
		if (null !=ccAddress && ccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress));	
		}
		if (null !=bccAddress && bccAddress.trim().length()>0) {
			message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
		}
		try{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("staticUrl", staticUrl);
		if(map!=null){
			model.putAll(map);
		}
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "template/" + resource, model);
		System.out.println(text);
		message.setSubject(subject);
		
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(text, mimeType);
		
		MimeMultipart mimeMultiPart = new MimeMultipart("related");
		mimeMultiPart.addBodyPart(mimeBodyPart);
		
		if (attachments != null) {
			for(MailAttachment attachment: attachments) {
				if(attachment == null || attachment.getFilePath() == null || attachment.getFilePath().isEmpty()){
					continue;
				}
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
//				attachmentBodyPart.setContent(new String(Base64.encodeBase64(attachment.getContent())), mimeType);
				attachmentBodyPart.setDataHandler(new DataHandler(new FileDataSource(attachment.getFilePath())));
				attachmentBodyPart.setHeader("Content-Transfer-Encoding", "base64");
				attachmentBodyPart.setHeader("Content-Type",attachment.getMimeType());
				attachmentBodyPart.setFileName(attachment.getFileName());
				//attachmentBodyPart.setDisposition(MimeBodyPart.ATTACHMENT);
				attachmentBodyPart.setContentID("<" + attachment.getFileName() + ">");
				mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}
		
		if (inlineImages != null) {
			for(MailAttachment attachment: inlineImages) {
				if(attachment == null || attachment.getFilePath() == null || attachment.getFilePath().isEmpty()){
					continue;
				}
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
			    DataSource source = new FileDataSource(new File(attachment.getFilePath()));
			    attachmentBodyPart.setDataHandler(new DataHandler(source));
			    attachmentBodyPart.setFileName(attachment.getFileName());
			    attachmentBodyPart.setDisposition(MimeBodyPart.INLINE);
			    attachmentBodyPart.setHeader("Content-ID","<" + attachment.getFileName() + ">");
			    mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}

		message.setContent(mimeMultiPart);

		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect(smtpHost, smtpPort, username, password);
		transport.sendMessage(message, message.getAllRecipients());
		}catch (Exception e) {
			e.printStackTrace();
			//System.out.println(e.fillInStackTrace());
		}
	}
	
	
	
	

	static class MailAuthenticator extends Authenticator {
		private final String username;

		private final String password;

		public MailAuthenticator(String username, String password) {
			this.username = username;
			this.password = password;
		}

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, 	password);
		}
	}

	public void setStaticUrl(String staticUrl) {
		this.staticUrl = staticUrl;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}
	
	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}
	

	class Mail {
		private String fromName;
		private String toAddress;
		private String ccAddress;
		private String bccAddress;
		private String subject;
		private String resource;
		private Map<String, Object> map;
		private String mimeType;
		private MailAttachment[] attachments;
		
		public Mail(String fromName, String toAddress, String ccAddress, String bccAddress, String subject,  String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
			this.fromName = fromName;
			this.toAddress = toAddress;
			this.ccAddress = ccAddress;
			this.bccAddress = bccAddress;
			this.subject = subject;
			this.resource = resource;
			this.map = map;
			this.mimeType = mimeType;
			this.attachments = attachments;
		}

		public String getSubject() {
			return subject;
		}

		public String getResource() {
			return resource;
		}

		public Map<String, Object> getMap() {
			return map;
		}

		public String getMimeType() {
			return mimeType;
		}

		public String getToAddress() {
			return toAddress;
		}

		public String getCcAddress() {
			return ccAddress;
		}

		public String getBccAddress() {
			return bccAddress;
		}

		public String getFromName() {
			return fromName;
		}

		public MailAttachment[] getAttachments() {
			return attachments;
		}

	}

	
}


class ByteDataSource implements DataSource {
	private InputStream inputStream;
	private OutputStream outputStream;
	private String mimeType;
	private String name;
	
	public ByteDataSource(byte[] content, String name, String mimeType) {
		this.inputStream = new ByteArrayInputStream(content);
		this.outputStream = new ByteArrayOutputStream();
		this.mimeType = mimeType;
		this.name = name;
	}

	public String getContentType() {
		return mimeType;
	}

	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	public String getName() {
		return name;
	}

	public OutputStream getOutputStream() throws IOException {
		return outputStream;
	}	
	
}