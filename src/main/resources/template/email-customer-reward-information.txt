
<table width="100%" border="0" cellspacing="0" cellpadding="0">

 <tr>
<td colspan="2"><h2> <strong> Information About the Active Reward Dollars </h2> </strong> </td>
</tr>

<tr>
<td colspan="2" ><strong>${message}</strong></td>
</tr>
<br />

<tr> 
<td colspan="2" >RewardTheFan is a ticket resale website that sells tickets in color coded ZONES to simplify your ticket buying experience.
For every $100 you spend, you will earn 10 Reward Dollars towards future ticket purchases.
RewardTheFan has no service, shipping or any other fees. </td>
</tr>
<br />
<tr> 
<td colspan="2" >RewardTheFan Desktop Site : http://www.rewardthefan.com</td>
</tr>
<br />
<br />
<tr> 
<td colspan="2"> Best regards </td>
</tr>
<tr> 
<td colspan="2"> Reward The Fan </td>
</tr>
<tr> 
<td colspan="2"> www.rewardthefan.com </td>
</tr>


<br />
<tr>
	<td colspan="2">
The information contained in this message may be privileged, confidential, and protected from disclosure. If the reader of this message is 
not the intended recipient, or any employee or agent responsible for delivering this message to the intended recipient,
you are hereby notified that any dissemination, distribution, or copying of this communication is strictly prohibited. 
If you have received this communication in error, please notify us immediately by replying to the message and deleting it from your computer.
</td>
</tr>
</table>