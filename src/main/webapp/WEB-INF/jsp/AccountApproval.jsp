<%@ include file="include.jsp"%>

<%
	String contextPath = request.getContextPath();
%>

<link rel="stylesheet" href="<%=contextPath%>/jsp/css/style.css"  type="text/css" media="screen">
<script type="text/javascript"
	src="<%=contextPath%><fmt:message key="jspath"/>GridController.js"></script>
<script type="text/javascript">
function approve(value){

	var confMsg;
	if(value == 'A') {
		confMsg = "Do you want to approve the Account?";
	} else if(value == 'D') {
		confMsg = "Do you want to delete the Account?";
	}
		
	if(confirm(confMsg)) {
		document.getElementById("methodType").value=value;
		submitForm('approveAccount');
	}
	
}

</script>
<form:hidden path="fdDepositId" />
<form:hidden path="accountType" />
<form:hidden path="methodType" />
<form:hidden path="isApproved" />
<form:hidden path="infoIntPaymentMode" />
<input type="hidden" id="finalMsg" name="finalMsg" />
<input type="hidden" id="isNotClosed" name="isNotClosed" />
<div id="contentInfo" style="background-color: white">
<table width="100%" border="0" cellpadding="5" cellspacing="0"
	bordercolor="#b7caf5" bgcolor="#FFFFFF" class="tabTableBorder" >
	<tr>
		<c:if test="${command.accountType eq 'F' }">
			<td colspan="4" class="gridHeaderTitle"><spring:message	code="FixdeDeposit.appTitle"/></td>
		</c:if>
		<c:if test="${command.accountType eq 'C' }">
			<td colspan="4" class="gridHeaderTitle"><spring:message	code="Cdm.approvalTitle"/></td>
		</c:if>
	</tr>
	<tr>
		<c:if test="${command.accountType eq 'F' }">
		<td class="labelBg" width="20%">
			<spring:message	code="Fd.fDRegNo"/>
			
		</td>
	</c:if>
	<c:if test="${command.accountType eq 'C' }">
		<td class="labelBg" width="20%">
			<spring:message	code="Cdm.depRegNo"/>
			
		</td>
	</c:if>
		<td class="labelInfo" width="30%">${command.fdRegNo}</td><form:hidden path="fdRegNo" />

		<td class="labelBg" width="20%"><spring:message	code="Common.transdate"/></td>
		<td class="labelInfo" width="30%">${command.transDateString}</td>
    </tr>
    <tr>
		<td class="labelBg"><spring:message	code="Common.branchname"/></td>
		<td class="labelInfo">${command.branchName}</td><form:hidden path="branchName" />

		<td class="labelBg"  width="20%"><spring:message code="Fd.repNo"/></td>
		<td class="labelInfo" width="30%">${command.receiptNo}</td><form:hidden path="receiptNo" />	
    </tr>
	<tr>
		<td class="labelBg"><spring:message code="Common.createdByandTime"/></td>
		<td class="labelInfo">${command.createdBy}</td><form:hidden path="createdBy" />	

		<td class="labelBg"><spring:message code="Common.status"/></td>
		<td class="labelInfo">${pageContainer.status}</td><form:hidden path="status" />	
    </tr>

    <c:if test="${command.foreClosedBy ne null and command.foreClosedTimeStr ne null}">
    <tr>
   	 	<td class="labelBg"><spring:message code="Fd.foreCloseBy"/></td>
		<td class="labelInfo">${command.foreClosedBy}</td><form:hidden path="foreClosedBy" />	
		
		<td class="labelBg"><spring:message code="Fd.foreCloseTime"/></td>
		<td class="labelInfo">${command.foreClosedTimeStr}</td><form:hidden path="foreClosedTimeStr" />				
    </tr>
    </c:if>
    <c:if test="${command.settledBy ne null and command.settleTimeStr ne null}">
    <tr>
    	<td class="labelBg"><spring:message code="Fd.settleBy"/></td>
		<td class="labelInfo">${command.settledBy}</td><form:hidden path="settledBy" />	
		
		<td class="labelBg"><spring:message code="Fd.settleTime"/></td>
		<td class="labelInfo">${command.settleTimeStr}</td><form:hidden path="settleTimeStr" />				
    </tr>
    </c:if>
    <c:if test="${command.approveddBy ne null and command.approvedTimeStr ne null}">
    <tr>    	
		<td class="labelBg"><spring:message code="Fd.authBy"/></td>
		<td class="labelInfo">${command.approveddBy}</td><form:hidden path="approveddBy" />	
		
		<td class="labelBg"><spring:message code="Fd.authTime"/></td>
		<td class="labelInfo">${command.approvedTimeStr}</td><form:hidden path="approvedTimeStr" />				
    </tr>
    </c:if>
	<tr>
		<td colspan="5">
			<div class="usual">
			 <ul class="idTabs" style="padding-bottom: 15px">
				<li><a href="#CustomerInformation"><spring:message	code="Fdm.custDetails" /></a></li>
				<li><a href="#JoinHolders"><spring:message code="Fdm.joinHolders" /></a></li>
				<c:if test="${command.accountType eq 'F' }">
					<li><a href="#FDDepositDetails"><spring:message code="Fdm.depDetails" /></a></li>
				</c:if>
				<c:if test="${command.accountType eq 'C' }">
					<li><a href="#FDDepositDetails"><spring:message code="Cdm.depDetails" /></a></li>
				</c:if>
				<c:if test="${command.accountType eq 'F' }">
					<li><a href="#Provision"><spring:message code="Fdm.provision" /></a></li>
				</c:if>
				<li><a href="#Nominee"><spring:message code="Fdm.nomineeDet" /></a></li>
				<li><a href="#PhotoDetails"><spring:message code="Common.photoDetails" /></a></li>
			</ul>
			 <div id="CustomerInformation">
			 	<table width="100%" border="5" cellpadding="5" cellspacing="2"
					bordercolor="#b7caf5" bgcolor="#FFFFFF" class="tabTableGridBorder">
			<tr>
				<td class="labelBg" width="10%"><spring:message code="Fdm.custId1" /></td>
				<td class="labelInfo" width="10%">${command.custId1}</td><form:hidden path="custId1" />
				
					
				<td class="labelBg" width="10%"><spring:message code="Fdm.depositerTitle1" /></td>
				<td class="labelInfo" width="10%">${command.depositerTitle1}</td><form:hidden path="depositerTitle1" />

			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Fdm.name1" /></td>
				<td class="labelInfo">${command.deopsitorName1}</td><form:hidden path="deopsitorName1" />
			
				<td class="labelBg"><spring:message	code="Fdm.fatherHus" /></td>
				<td class="labelInfo" >${command.fatherHusband}</td><form:hidden path="fatherHusband" />
			</tr>
			<tr>	
				<td class="labelBg"><spring:message code="Fdm.fatherHusName" /></td>
				<td class="labelInfo">${command.fathusbandName}</td><form:hidden path="fathusbandName" />
			
				<td class="labelBg"><spring:message code="Fdm.depAge1" /></td>
				<td class="labelInfo">${command.deopsitorAge1}</td><form:hidden path="deopsitorAge1" />
			</tr>		
			<tr>	
				<td class="labelBg"><spring:message code="Fdm.eros" /></td>
				<td class="labelInfo">${command.eors}</td><form:hidden path="eors" />
			
				<td class="labelBg"><spring:message	code="Common.gender" /></td>
				<td class="labelInfo" >${command.gender}</td><form:hidden path="gender" />
			</tr>
			<tr>			
				<td class="labelBg"><spring:message code="Fdm.depDateOfBirth" /></td>
				<td class="labelInfo">${command.dep1DOBString}</td><form:hidden path="dep1DOBString" />			
			
				<td class="labelBg" ><spring:message code="Fdm.classific" /></td>
				<td class="labelInfo">${command.classification}</td><form:hidden path="classification" />
			</tr>
			<tr>	
				<td class="labelBg"><spring:message code="Common.address1" /></td>
				<td class="labelInfo">${command.address1}</td><form:hidden path="address1" />
			
				<td class="labelBg"><spring:message code="Common.address2" /></td>
				<td class="labelInfo">${command.address2}</td><form:hidden path="address2" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Common.address3" /></td>
				<td class="labelInfo">${command.address3}</td><form:hidden path="address3" />
			
				<td class="labelBg"><spring:message code="Common.pincode" /></td>
				<td class="labelInfo">${command.pinCode}</td><form:hidden path="pinCode" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Employee.phoneNo" /></td>
				<td class="labelInfo">${command.phoneNo}</td><form:hidden path="phoneNo" />
			
				<td class="labelBg"><spring:message code="Common.faxno" /></td>
				<td class="labelInfo">${command.faxNo}</td><form:hidden path="faxNo" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Common.emailid" /></td>
				<td class="labelInfo">${command.emailId}</td><form:hidden path="emailId" />
			
				<td class="labelBg"><spring:message code="Fdm.pan" /></td>
				<td class="labelInfo">${command.panNo}</td><form:hidden path="panNo" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Employee.empId" /></td>
				<td class="labelInfo">${command.smsoEmpName}</td><form:hidden path="smsoEmpName" />
			
				<td class="labelBg"><spring:message code="Fdm.senorCitizon" /></td>
				<td class="labelInfo">${command.seniorCitizonProf}</td><form:hidden path="seniorCitizonProf" />
			</tr>
		</table>
		</div>
		
		<div id="JoinHolders">
			<table width="100%" border="5" cellpadding="5" cellspacing="2"
				bordercolor="#b7caf5" bgcolor="#FFFFFF" class="tabTableGridBorder">
				<tr>
					<td class="labelBg" width="10%"><spring:message code="Fdm.custId2" /></td>
					<td class="labelInfo" width="10%">${command.custId2}</td><form:hidden path="custId2" />
					
					<td class="labelBg" width="10%"><spring:message code="Fdm.depositerTitle1" /></td>
					<td class="labelInfo" width="10%">${command.depositorTitle2}</td><form:hidden path="depositorTitle2" />
					
					<td class="labelBg" width="10%"><spring:message code="Fdm.name2" /></td>
					<td class="labelInfo" width="10%">${command.deopsitorName2}</td><form:hidden path="deopsitorName2" />
				</tr>
				<tr>		
					<td class="labelBg"><spring:message code="Fdm.custId3" /></td>
					<td class="labelInfo">${command.custId3}</td><form:hidden path="custId3" />
								
					<td class="labelBg"><spring:message code="Fdm.depositerTitle1" /></td>
					<td class="labelInfo">${command.depositorTitle3}</td><form:hidden path="depositorTitle3" />
					
					<td class="labelBg"><spring:message code="Fdm.name3" /></td>
					<td class="labelInfo">${command.deopsitorName3}</td><form:hidden path="deopsitorName3" />
				</tr>
				<tr>
					<td class="labelBg"><spring:message code="Fdm.custId4" /></td>
					<td class="labelInfo">${command.custId4}</td><form:hidden path="custId4" />
					
					<td class="labelBg"><spring:message code="Fdm.depositerTitle1" /></td>
					<td class="labelInfo">${command.depositorTitle4}</td><form:hidden path="depositorTitle4" />
				
					<td class="labelBg"><spring:message code="Fdm.name4" /></td>
					<td class="labelInfo">${command.deopsitorName4}</td><form:hidden path="deopsitorName4" />
				</tr>
		</table>
	</div>

	<div id="FDDepositDetails">
		<table width="100%" border="5" cellpadding="5" cellspacing="2"
			bordercolor="#b7caf5" bgcolor="#FFFFFF" class="tabTableGridBorder">
			<tr>
				<td class="labelBg" width="10%"><spring:message code="Fdm.dateOfDep"/></td>
				<td class="labelInfo" width="10%">${command.depositDateString}</td><form:hidden path="depositDateString" />
			
				<td class="labelBg" width="10%"><spring:message code="Fdm.dateOfEff"/></td>
				<td class="labelInfo" width="10%">${command.effetiveDateString}</td><form:hidden path="effetiveDateString" />
			</tr>
			<tr>	
				<td class="labelBg"><spring:message code="Fdm.noOfMonth" /></td>
				<td class="labelInfo">${command.noOfMonths}</td><form:hidden path="noOfMonths" />
			
				<td class="labelBg"><spring:message code="Fdm.depAmount" /></td>
				<td class="labelInfo">${command.depositAmountStr}</td><form:hidden path="depositAmountStr" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Fdm.interestRate" /></td>
				<td class="labelInfo">${command.interestRateStr}</td><form:hidden path="interestRateStr" />
				
				<c:if test="${command.accountType eq 'F'}">
					<td class="labelBg"><spring:message code="Fdm.dateOfMat" /></td>
					<td class="labelInfo">${command.maturityDateString}</td><form:hidden path="maturityDateString" />
				</c:if>
				<c:if test="${command.accountType eq 'C'}">
					<td class="labelBg"><spring:message code="Cd.grossInt" /></td>
				<td class="labelInfo">${command.grossIntRateStr}</td><form:hidden path="grossIntRateStr" />
				</c:if>
				
			</tr>
			<c:if test="${command.accountType eq 'F'}">
				<tr>
					<td class="labelBg"><spring:message code="Common.receiptMode" /></td>
					<td class="labelInfo" colspan="1">${command.depositPayMode}</td><form:hidden path="depositPayMode" />
					
					<td class="labelBg"><spring:message code="Flexi.signIn" /></td>
					<td class="labelInfo" colspan="3">${command.signInLang}</td>
				</tr>
			</c:if>
			<c:if test="${command.accountType eq 'C'}">
			<tr>
				<td class="labelBg"><spring:message code="Fdm.dateOfMat" /></td>
				<td class="labelInfo">${command.maturityDateString}</td><form:hidden path="maturityDateString" />
				
				<td class="labelBg" ><spring:message code="Cd.matValue" /></td>
				<td class="labelInfo" ><fmt:formatNumber pattern="####0.00" value="${command.cdMatValue}"/></td><form:hidden path="cdMatValue" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Fdm.incometax" /></td>
				<td class="labelInfo">${command.incometax}</td><form:hidden path="incometax" />
			
				<td class="labelBg"><spring:message code="Fdm.taxForm" /></td>
				<td class="labelInfo">${command.taxFormReceived}</td><form:hidden path="taxFormReceived" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Common.receiptMode" /></td>
				<td class="labelInfo">${command.depositPayMode}</td><form:hidden path="depositPayMode" />
				
				<td class="labelBg"><spring:message code="Flexi.signIn" /></td>
				<td class="labelInfo" colspan="3">${command.signInLang}</td>
			</tr>
		</c:if>
		<c:if test="${command.depositPayMode eq 'Cheque' || command.depositPayMode eq 'DD'}">
				
				<tr>
				<c:if test="${command.depositPayMode eq 'Cheque'}">
					<td colspan="6" class="gridHeaderTitle" >Cheque Details</td>
				</c:if>
				<c:if test="${command.depositPayMode eq 'DD'}">
				<td colspan="6" class="gridHeaderTitle" >DD Details</td>
				</c:if>
					
				</tr>				
				<tr>
					<td colspan="6">
						<jsp:include flush="true" page="ChequeAccDtlApp.jsp" />
					</td>
				</tr>
							
			</c:if>
			<c:if test="${command.depositPayMode eq 'Adjustment' }">
				<tr>
					<td colspan="6" class="gridHeaderTitle" >Adjustment Details</td>
				</tr>
				<tr>
					<td colspan="6"><jsp:include flush="true" page="AdjustmentGridApp.jsp" /></td>
				</tr>
			</c:if>
			</table>
	</div>
	<c:if test="${command.accountType eq 'F'}">
			<div id="Provision">
		<table width="100%" border="5" cellpadding="5" cellspacing="2"
			bordercolor="#b7caf5" bgcolor="#FFFFFF" class="tabTableGridBorder">
		<tr>
			<td class="labelBg" width="10%" ><spring:message code="Fdm.intPayMode" /></td>
			<td class="labelInfo" colspan="3">${command.intPaymentMode}</td><form:hidden path="intPaymentMode" />
		</tr>
		<c:if test="${command.infoIntPaymentMode eq 'S' or command.infoIntPaymentMode eq 'R' or command.infoIntPaymentMode eq 'B'}">
			<tr>
				<td class="labelBg" width="10%"><spring:message	code="Employee.bankId" /></td>
				<td class="labelInfo" width="10%">${command.bankName}</td><form:hidden path="bankName" />
				
				<td class="labelBg" width="10%"><spring:message code="Common.branchname" /></td>
				<td class="labelInfo" width="10%">${command.bankBranch}</td><form:hidden path="bankBranch" />
			</tr>
			<tr>
				<td class="labelBg"><spring:message code="Fdm.ifsNo" /></td>
				<td class="labelInfo">${command.ifsNo}</td><form:hidden path="ifsNo" />
				
				<td class="labelBg"><spring:message code="Fdm.accNo" /></td>
				<td class="labelInfo">${command.bankAccountNo}</td><form:hidden path="bankAccountNo" />
			</tr>
			<c:if test="${command.infoIntPaymentMode eq 'S'}">
				<tr>
					<td class="labelBg"><spring:message code="BankMaster.accHead" /></td>
					<td class="labelInfo">${command.smsobankHd}</td><form:hidden path="smsobankHd" />
				</tr>
			</c:if>
		</c:if>
		<tr>
			<td class="labelBg"width="10%" ><spring:message code="Fdm.incometax" /></td>
			<td class="labelInfo"width="10%">${command.incometax}</td><form:hidden path="incometax" />
		
			<td class="labelBg"width="10%" ><spring:message code="Fdm.taxForm" /></td>
			<td class="labelInfo"width="10%">${command.taxFormReceived}</td><form:hidden path="taxFormReceived" />
		</tr>
		
			</table>
		</div>
	</c:if>
	

		<div id="Nominee">
			<table width="100%" border="5" cellpadding="5" cellspacing="2"
				bordercolor="#b7caf5" bgcolor="#FFFFFF" class="tabTableGridBorder">
			<tr>
				<td class="labelBg" width="10%"><spring:message code="Fdm.minor" /></td>
				<td class="labelInfo" width="10%">${command.minorAccount}</td><form:hidden path="minorAccount" />
				
				<td class="labelBg" width="10%"><spring:message code="Fdm.payee" /></td>
				<td class="labelInfo" width="10%">${command.payeeName}</td><form:hidden path="payeeName" />
			</tr>
			<c:if test="${command.minorAccount eq 'Yes'}">
			<tr>
				<td class="labelBg" ><spring:message	code="Fdm.guardien" /></td>
				<td class="labelInfo" >${command.guardianName}</td><form:hidden path="guardianName" />

				<td class="labelBg" ><spring:message code="Fdm.guardienRel" /></td>
				<td class="labelInfo" >${command.guardianRelationship}</td><form:hidden path="guardianRelationship" />
			</tr>
			</c:if>
			<tr>
				<td class="labelBg"><spring:message code="Fdm.nominee" /></td>
				<td class="labelInfo">${command.nomineeName}</td><form:hidden path="nomineeName" />

				<td class="labelBg"><spring:message code="Fdm.nomineeRel" /></td>
				<td class="labelInfo">${command.nomineeRelationship}</td><form:hidden path="nomineeRelationship" />
			</tr>
			
		   </table>
  	</div>
  	 <div id="PhotoDetails">
	 	<jsp:include flush="true" page="PhotoUploadApp.jsp" />
 	 </div>
</div>
</td>

</tr>

	<tr>
		<td class="labelInfo" colspan="6" align="center">
		<button type="button" class="button" name="loginButton" id="saveBtnId"
			onclick="approve('A');" title="Save"><spring:message
			code="Button.approval" /></button>
		<!--<button type="button" class="button" name="loginButton"
			onclick="approve('R');" title="Reject" id="rejectBtnId"><spring:message
			code="Button.reject" /></button>
		--><button type="button" class="button" name="loginButton"
			onclick="approve('D');" title="Delete" id="deleteBtnId"><spring:message
			code="Button.delete" /></button>
		<button type="button" class="button" name="loginButton"
			onclick="callClose();" title=Close id="clearBtnId"><spring:message
			code="Button.close" /></button>
		</td>
	</tr>

</table>
</div>

<script type="text/javascript">

var isApproved = document.getElementById("isApproved").value;

if(isApproved =='Y'){

	var depId =document.getElementById("fdDepositId").value;
	var authMsg = '${pageContainer.authenticatedMsg}';
	var finalMsg = '${pageContainer.finalMsg}';
	$('#saveBtnId').hide();
	$('#rejectBtnId').hide();
	$('#deleteBtnId').hide();
	document.getElementById("finalMsg").value = finalMsg;
	document.getElementById("isNotClosed").value = 'N';
	alert(authMsg);
	window.opener.authParent(depId,'A',finalMsg);
	self.close();
	
} else if(isApproved =='C') {
	var authMsg = '${pageContainer.authenticatedMsg}';
	$('#saveBtnId').hide();
	$('#rejectBtnId').hide();	
	alert(authMsg);
	window.opener.popupCallRefresh();
	self.close();
}

function callClose(){

	var isNotClosed =document.getElementById("isNotClosed").value;
    if(isNotClosed == 'Y'){
    	var depId =document.getElementById("fdDepositId").value;
    	var finalMsg =document.getElementById("finalMsg").value;
    	window.opener.authParent(depId,'A',finalMsg);
		self.close();
    }else{
    	window.close();
    }
}


</script>

