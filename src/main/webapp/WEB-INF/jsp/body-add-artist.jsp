<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/AddFavoriteOrSuperArtist.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">
	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	artistActionType - String (Mandatory) [Allowed Values : <b>FAVORITE,SUPERFAN</b>]<br/>
	artistIds - String (Mandatory) - All artist id separated by ","<br/>
	removeArtistIds - String (Optional) - All artist id separated by ","<br/>
	customerId  - Integer (Mandatory)<br/>
</div>

<br/><br/>
<b>Response :</b><br/>
<div style="background-color:#CCC">
{
 "addArtistDetails":{
	"status":1,"error":null,"customerId":1,"artistActionType":"SUPERFAN","actionResult":"Added as super fan"
 }
}
</div>
<br/><br/>

<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>