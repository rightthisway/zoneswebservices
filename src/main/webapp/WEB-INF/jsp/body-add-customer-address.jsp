<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/AddCustomerAddress.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">

	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	customerId -Integer (Mandatory )<br/>
	addressType -String (Mandatory - Provided by Right This Way )<br/>
	firstName -String (Mandatory )<br/>
	lastName -String (Mandatory )<br/>
	addressLine1 -String (Mandatory  )<br/> 
	addressLine2-String (Optional )<br/>
	city  -String (Mandatory )<br/>
    state  -Integer (Mandatory - Provided by Right This Way)<br/>
    postalCode -String (Mandatory  )<br/>
    country -Integer (Mandatory - Provided by Right This Way )<br/>
  	phone1  -String (Mandatory  )<br/> 
   	phone2 -String (Optional )<br/>
    phone3-String (Optional)<br/>
    action-String (Mandatory  - Provided by Right This Way )<br/>
    addressId-Integer (Mandatory - If action is Update)<br/>

</div>

<br/>
<br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">
{	
	{ "customerAddress": { "status": 1,  "error": null,
    "userAddress": {"id": 91,"customerId": 22,"firstName": "t4","lastName": "t5","addressLine1": "chk1", 
     "addressLine2": "chk1","addressType": "SHIPPING_ADDRESS",
      "city": "CA",
      "state": {
        "name": "Alabama",
        "id": 1,
        "countryId": 1,
        "shortDesc": "AL"
      },
      "country": {
        "id": 1,
        "countryName": "UNITED STATES",
        "shortDesc": "US",
        "stateList": null
      },
      "stateName": "Alabama","countryName": "UNITED STATES","zipCode": "545454","phone1": "4086137878",
      "phone2": null,"phone3": null, "representativeName": null
    }
	}
	}
	
		
}
</div>
<br/>
<br/>



<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>

