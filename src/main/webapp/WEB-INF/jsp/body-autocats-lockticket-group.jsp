<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/AutoCatsLockTicketGroup.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">

	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	eventId -Integer (Mandatory)<br/>
    categoryTicketGroupId  -Integer (Mandatory)<br/>
	platForm -String (Mandatory)<br/>
	sessionIdOrDeviceId -String (Mandatory)<br/>
	customerId -Integer (Optional)<br/>
	ipAddress  -String (Optional)<br/>
</div>

<br/>
<br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">
{
  "ticketGroupLockStatus": {
    "status": 1,
    "error": null,
    "lockStatus": "Success",
    "message": "Ticket Group successfully locked by this Device Id or Session Id",
    "deviceId": "b7210105726fe9f5",
    "timeLeft": "6:00",
    "catgeoryTicketGroupId": 134119,
    "event": {
      "eventId": 1000129690,
      "eventName": "Marvel Universe Live!",
      "eventDateTime": "05/01/2016 12:00 AM",
      "eventDateStr": "05/01/2016",
      "eventTimeStr": "12:00 AM",
      "venueName": "Jacksonville Veterans Memorial Arena",
      "city": "Jacksonville",
      "state": "FL",
      "country": "US",
      "pinCode": "32202",
      "artistName": "Marvel Universe Live!",
      "grandChildCategoryName": "Other",
      "childCategoryName": "National Tours",
      "parentCategoryName": "Theater",
      "ticketPriceTag": "",
      "isSuperFanEvent": false,
      "isFavoriteEvent": false,
      "customerId": 7,
      "svgText": "",
      isMapWithSvg": "true",
      "svgMapPath": "http://52.22.87.62/SvgMaps/1700_MARVEL.gif",
      "svgWebViewUrl": "http://52.22.87.62/sandbox.MobileSvgMapView.html?configId=RewardTheFan&eventId=1000129690",
      "shareLinkUrl": "http://zonetickets.com/event/1000129690"
    },
    "categoryTicketGroup": {
      "id": 134119,
      "eventId": 1000129690,
      "section": "E",
      "svgKey": "e",
      "row": "",
      "quantity": 1,
      "price": 76,
      "sectionRange": "107-110",
      "rowRange": "A-GG",
      "shippingMethod": "FEDEX",
      "ticketDescription": "",
      "sectionDescription": "",
      "colorCode": "#32A148",
      "rgbColor": "R:50 G:161 B:72"
    }
  }
}
</div>
<br/>
<br/>

<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>