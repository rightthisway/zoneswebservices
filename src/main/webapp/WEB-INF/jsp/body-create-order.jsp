<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");

			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var validator = $("#event").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="CreateOrder.json" id="event" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			
			<tr>
				<td>
					Ticket Group Id:
				</td>
				<td>
					<input type="text" name="ticketGroupId" id="ticketGroupId" class="group">
				</td>
			</tr>
			
			
			<tr>
				<td>
					Event Id:
				</td>
				<td>
					<input type="text" name="eventId" id="eventId" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Ticket Price:
				</td>
				<td>
					<input type="text" name="ticketPrice" id="ticketPrice" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Sold Qty:
				</td>
				<td>
					<input type="text" name="qty" id="qty" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Total Price:
				</td>
				<td>
					<input type="text" name="totalPrice" id="totalPrice" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Discount:
				</td>
				<td>
					<input type="text" name="discountAmount" id="discountAmount" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Promotional Offer:
				</td>
				<td>
					<input type="text" name="promotionalOffer" id="promotionalOffer" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Loyalty Used:
				</td>
				<td>
					<input type="text" name="loyaltySpent" id="loyaltySpent" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Earning Loyalty:
				</td>
				<td>
					<input type="text" name="loyaltyEarning" id="loyaltyEarning" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Sold Price:
				</td>
				<td>
					<input type="text" name="soldPrice" id="soldPrice" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Customer Id:
				</td>
				<td>
					<input type="text" name="customerId" id="customerId" class="group">
				</td>
			</tr> 
			
			<tr>
				<td>
					Application Platform:
				</td>
				<td>
					<input type="text" name="appPlatform" id="appPlatform" class="group">ex : (IOS,ANDROID,MOBILESITE,DESKTOPSITE)
				</td>
			</tr>
			
			<tr>
				<td>
					Credit Card Payment:
				</td>
				<td>
					<input type="radio" name="isCardPayment" id="isCardPayment1" value="Yes" checked="checked" > Yes
					<input type="radio" name="isCardPayment" id="isCardPayment2" value="No"> No
				</td>
			</tr>
			
			<tr>
				<td>
					Card Type:
				</td>
				<td>
					<input type="text" name="cardType" id="cardType" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Card No:
				</td>
				<td>
					<input type="text" name="cardNo" id="cardNo" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Name On Card  :
				</td>
				<td>
					<input type="text" name="nameOnCard" id="nameOnCard" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Card Expiry Month  :
				</td>
				<td>
					<input type="text" name="expiryMonth" id="expiryMonth" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Card Expiry Year:
				</td>
				<td>
					<input type="text" name="expiryYear" id="expiryYear" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Security Code:
				</td>
				<td>
					<input type="text" name="securityCode" id="securityCode" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Transaction Id:
				</td>
				<td>
					<input type="text" name="transactionId" id="transactionId" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Payment Method:
				</td>
				<td>
					<input type="text" name="paymentMethod" id="paymentMethod" class="group"> ex: (CREDITCARD,PAYPAL,LOYALTY,APPLEPAY,GOOGLEWALLET)
				</td>
			</tr>
			
			<!-- <tr>
				<td>
					First Name:
				</td>
				<td>
					<input type="text" name="firstName" id="firstName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Last Name:
				</td>
				<td>
					<input type="text" name="lastName" id="lastName" class="group">
				</td>
			</tr> -->
			
			<tr>
				<td>
					Email:
				</td>
				<td>
					<input type="text" name="email" id="email" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Password:
				</td>
				<td>
					<input type="password" name="password" id="password" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing First Name:
				</td>
				<td>
					<input type="text" name="mailingFirstName" id="mailingFirstName" >
				</td>
			</tr>
			<tr>
				<td>
					Mailing Last Name:
				</td>
				<td>
					<input type="text" name="mailingLastName" id="mailingLastName" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address 1:
				</td>
				<td>
					<input type="text" name="mailingAddress1" id="mailingAddress1" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address 2:
				</td>
				<td>
					<input type="text" name="mailingAddress2" id="mailingAddress2" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address City:
				</td>
				<td>
					<input type="text" name="mailingCity" id="mailingCity" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address State:
				</td>
				<td>
					<select name="mailingState" id="mailingState" >
						<c:forEach items="${states}" var="state">
							<option value="${state.id}">${state.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address ZIP / Postal Code:
				</td>
				<td>
					<input type="text" name="mailingPostalCode" id="mailingPostalCode" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address Country:
				</td>
				<td>
					<select name="mailingCountry" id="mailingCountry" >
						<c:forEach items="${countries}" var="country">
							<option value="${country.id}">${country.countryName}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address Phone:
				</td>
				<td>
					<input type="text" name="mailingPhone" id="mailingPhone" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address is same as Mailing Address:
				</td>
				<td>
					<input type="checkbox" name="maSameAsBa" id="maSameAsBa" value="Y" > Yes
				</td>
			</tr>
			
			<tr>
				<td>
					Billing First Name:
				</td>
				<td>
					<input type="text" name="billingFirstName" id="billingFirstName" >
				</td>
			</tr>
			<tr>
				<td>
					Billing Last Name:
				</td>
				<td>
					<input type="text" name="billingLastName" id="billingLastName" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address 1:
				</td>
				<td>
					<input type="text" name="billingAddress1" id="billingAddress1" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address 2:
				</td>
				<td>
					<input type="text" name="billingAddress2" id="billingAddress2" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address City:
				</td>
				<td>
					<input type="text" name="billingCity" id="billingCity" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address State:
				</td>
				<td>
					<select name="billingState" id="billingState" >
						<c:forEach items="${states}" var="state">
							<option value="${state.id}">${state.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address ZIP / Postal Code:
				</td>
				<td>
					<input type="text" name="billingPostalCode" id="billingPostalCode" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address Country:
				</td>
				<td>
					<select name="billingCountry" id="billingCountry" >
						<c:forEach items="${countries}" var="country">
							<option value="${country.id}">${country.countryName}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address Phone:
				</td>
				<td>
					<input type="text" name="billingPhone" id="billingPhone" >
				</td>
			</tr>
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Create Order">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/CreateOrder.xml?configId=XXX&ticketGroupId=XXX&eventId=XXX&ticketPrice=XXX&qty=XXX&totalPrice=XXX&discountAmount=XXX&promotionalOffer=XXX&loyaltySpent=XXX&loyaltyEarning=XXX&soldPrice=XXX&customerId=XXX&appPlatform=XXX&transactionId=XXX&paymentMethod=XXX&isCardPayment=XXX&expiryMonth=XXX&expiryYear=XXX&cardType=XXX&cardNo=XXXXXXXX&nameOnCard=XXX&securityCode=XXX&mailingFirstName=XXX&mailingLastName=XXX&mailingAddress1=XXX&mailingAddress2=XXX&mailingCity=XXX&mailingState=XXX&mailingPostalCode=XXX&mailingCountry=XXX&mailingPhone=XXX&maSameAsBa=X&billingFirstName=XXX&billingLastName=XXX&billingAddress1=XXX&billingAddress2=XXX&billingCity=XXX&billingState=XXX&billingPostalCode=XXX&billingCountry=XXX&billingPhone=XXX<br/>

<br/><br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/CreateOrder.json?configId=XXX&ticketGroupId=XXX&eventId=XXX&ticketPrice=XXX&qty=XXX&totalPrice=XXX&discountAmount=XXX&promotionalOffer=XXX&loyaltySpent=XXX&loyaltyEarning=XXX&soldPrice=XXX&customerId=XXX&appPlatform=XXX&transactionId=XXX&paymentMethod=XXX&isCardPayment=XXX&expiryMonth=XXX&expiryYear=XXX&cardType=XXX&cardNo=XXXXXXXX&nameOnCard=XXX&securityCode=XXX&mailingFirstName=XXX&mailingLastName=XXX&mailingAddress1=XXX&mailingAddress2=XXX&mailingCity=XXX&mailingState=XXX&mailingPostalCode=XXX&mailingCountry=XXX&mailingPhone=XXX&maSameAsBa=X&billingFirstName=XXX&billingLastName=XXX&billingAddress1=XXX&billingAddress2=XXX&billingCity=XXX&billingState=XXX&billingPostalCode=XXX&billingCountry=XXX&billingPhone=XXX<br/>



</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>

<!-- 
Output:<br/>
<div style="background-color:#CCC">
&lt;list&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;zoneEvents&gt; <br/>
&nbsp;&nbsp;&nbsp;&lt;zoneEvent&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventId&gt;42762&lt;/eventId&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventName&gt;Wicked New York&lt;/eventName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventDate&gt;03/27/2015&lt;/eventDate&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventTime&gt;08:00 PM&lt;/eventTime&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;artistName&gt;Wicked&lt;/artistName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueName&gt;Madison Square Garden&lt;/venueName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCity&gt;New York&lt;/venueCity&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueState&gt;NY&lt;/venueState&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCountry&gt;US&lt;/venueCountry&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/zoneEvent&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/zoneEvents&gt;<br/>
&lt;list&gt;<br/>
</div>
 -->
