<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Profile Picture view</title>
</head>
<body>
<!--<h4>Customer Profile Picture</h4>-->
<c:if test="${not empty profilePic}">
<img src="data:image/jpeg;base64,${profilePic}" alt="Customer Profile picture" height="300" width="350">
</c:if>
${error}
</body>
</html>