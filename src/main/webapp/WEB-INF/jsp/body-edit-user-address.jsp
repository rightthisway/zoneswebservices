<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");

			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var validator = $("#event").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="EditCustomerAddress.json" id="event" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			
			<tr>
				<td>
					Product Type:
				</td>
				<td>
					<input type="radio" name="productType" id="productType1" class="group" value="MINICATS" /> MINICATS
					<input type="radio" name="productType" id="productType2" class="group" value="VIPMINICATS" /> VIPMINICATS
					<input type="radio" name="productType" id="productType3" class="group" value="LASTROWMINICATS" /> LASTROWMINICATS
					<input type="radio" name="productType" id="productType4" class="group" value="VIPLASTROWMINICATS" />VIP LASTROWMINICATS
					<input type="radio" name="productType" id="productType5" class="group" value="PRESALEZONETICKETS" /> PRESALE ZONETICKETS
				</td>
			</tr>
			
			<tr>
				<td>
					Customer Id:
				</td>
				<td>
					<input type="text" name="customerId" id="customerId" class="group">
				</td>
			</tr> 
			
			<tr>
				<td>
					Address Type:
				</td>
				<td>
					<input type="radio" name="addressType" id="addressType1" value="M" checked="checked" > Mailing Address
					<input type="radio" name="addressType" id="addressType2" value="B"> Billing Address
				</td>
			</tr>
			
			<tr>
				<td>
					First Name:
				</td>
				<td>
					<input type="text" name="firstName" id="firstName" class="group">
				</td>
			</tr> 
			
			<tr>
				<td>
					Last Name:
				</td>
				<td>
					<input type="text" name="lastName" id="lastName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Address Line 1:
				</td>
				<td>
					<input type="text" name="addressLine1" id="addressLine1" >
				</td>
			</tr>
			
			<tr>
				<td>
					Address Line 2:
				</td>
				<td>
					<input type="text" name="addressLine2" id="addressLine2" >
				</td>
			</tr>
			
			<tr>
				<td>
					City:
				</td>
				<td>
					<input type="text" name="city" id="city" >
				</td>
			</tr>
			
			<tr>
				<td>
					State:
				</td>
				<td>
					<select name="state" id="state" >
						<c:forEach items="${states}" var="state">
							<option value="${state.id}">${state.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					ZIP / Postal Code:
				</td>
				<td>
					<input type="text" name="postalCode" id="postalCode" >
				</td>
			</tr>
			
			<tr>
				<td>
					Country:
				</td>
				<td>
					<select name="country" id="country" >
						<c:forEach items="${countries}" var="country">
							<option value="${country.id}">${country.countryName}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Phone:
				</td>
				<td>
					<input type="text" name="phone" id="phone" >
				</td>
			</tr>
			
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/EditCustomerAddress.xml?configId=XXXX&productType=xxx&customerId=XXXX&addressType=XXXX&firstName=XXX&lastName=XXX&addressLine1=XXXX&addressLine2=XXXX&city=XXXX&state=XXXX&postalCode=XXXX&country=XXXX&phone=XXXX
<br/><br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/EditCustomerAddress.json?configId=XXXX&productType=xxx&customerId=XXXX&addressType=XXXX&firstName=XXX&lastName=XXX&addressLine1=XXXX&addressLine2=XXXX&city=XXXX&state=XXXX&postalCode=XXXX&country=XXXX&phone=XXXX
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>

