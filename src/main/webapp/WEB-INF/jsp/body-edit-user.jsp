<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");

			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var validator = $("#event").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="EditCustomer.json" id="event" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			
			<tr>
				<td>
					Customer Id:
				</td>
				<td>
					<input type="text" name="customerId" id="customerId" class="group">
				</td>
			</tr> 
			
			<!-- <tr>
				<td>
					Email:
				</td>
				<td>
					<input type="text" name="email" id="email" class="group">
				</td>
			</tr> -->
			
			<tr>
				<td>
					Mailing First Name:
				</td>
				<td>
					<input type="text" name="mailingFirstName" id="mailingLastName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Last Name:
				</td>
				<td>
					<input type="text" name="mailingLastName" id="MmilingLastName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address 1:
				</td>
				<td>
					<input type="text" name="mailingAddress1" id="mailingAddress1" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address 2:
				</td>
				<td>
					<input type="text" name="mailingAddress2" id="mailingAddress2" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address City:
				</td>
				<td>
					<input type="text" name="mailingCity" id="mailingCity" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address State:
				</td>
				<td>
					<select name="mailingState" id="mailingState" >
						<c:forEach items="${states}" var="state">
							<option value="${state.id}">${state.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address ZIP / Postal Code:
				</td>
				<td>
					<input type="text" name="mailingPostalCode" id="mailingPostalCode" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address Country:
				</td>
				<td>
					<select name="mailingCountry" id="mailingCountry" >
						<c:forEach items="${countries}" var="country">
							<option value="${country.id}">${country.countryName}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address Phone:
				</td>
				<td>
					<input type="text" name="mailingPhone" id="mailingPhone" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address is same as Mailing Address:
				</td>
				<td>
					<input type="checkbox" name="maSameAsBa" id="maSameAsBa" value="Y" > Yes
				</td>
			</tr>
			
			<tr>
				<td>
					Billing First Name:
				</td>
				<td>
					<input type="text" name="billingFirstName" id="billingLastName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Last Name:
				</td>
				<td>
					<input type="text" name="billingLastName" id="billingLastName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address 1:
				</td>
				<td>
					<input type="text" name="billingAddress1" id="billingAddress1" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address 2:
				</td>
				<td>
					<input type="text" name="billingAddress2" id="billingAddress2" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address City:
				</td>
				<td>
					<input type="text" name="billingCity" id="billingCity" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address State:
				</td>
				<td>
					<select name="billingState" id="billingState" >
						<c:forEach items="${states}" var="state">
							<option value="${state.id}">${state.name}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address ZIP / Postal Code:
				</td>
				<td>
					<input type="text" name="billingPostalCode" id="billingPostalCode" >
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address Country:
				</td>
				<td>
					<select name="billingCountry" id="billingCountry" >
						<c:forEach items="${countries}" var="country">
							<option value="${country.id}">${country.countryName}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address Phone:
				</td>
				<td>
					<input type="text" name="billingPhone" id="billingPhone" >
				</td>
			</tr>
			
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">

XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/EditCustomer.xml?configId=XXXX&customerId=XXXX&mailingFirstName=XXXX&mailingLastName=XXXX&mailingAddress1=XXXX&mailingAddress2=XXXX&mailingCity=XXXX&mailingState=X&mailingPostalCode=XX&mailingCountry=X&mailingPhone=XXXX&maSameAsBa=X&billingFirstName=XXXX&billingLastName=XXX&billingAddress1=XXX&billingAddress2=XXX&billingCity=XXX&billingState=XXX&billingPostalCode=XXX&billingCountry=XXX&billingPhone=XXX<br/>
<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/EditCustomer.json?configId=XXXX&customerId=XXXX&mailingFirstName=XXXX&mailingLastName=XXXX&mailingAddress1=XXXX&mailingAddress2=XXXX&mailingCity=XXXX&mailingState=X&mailingPostalCode=XX&mailingCountry=X&mailingPhone=XXXX&maSameAsBa=X&billingFirstName=XXXX&billingLastName=XXX&billingAddress1=XXX&billingAddress2=XXX&billingCity=XXX&billingState=XXX&billingPostalCode=XXX&billingCountry=XXX&billingPhone=XXX<br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>

