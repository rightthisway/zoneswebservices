Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="EmailCustomerRequestInfo.json" id="event" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId" />
				</td>
			</tr>
			
			<tr>
				<td>
					Product Type:
				</td>
				<td>
					<input type="radio" name="productType" id="productType1" class="group" value="MINICATS" /> MINICATS
					<input type="radio" name="productType" id="productType2" class="group" value="VIPMINICATS" /> VIPMINICATS
					<input type="radio" name="productType" id="productType3" class="group" value="LASTROWMINICATS" /> LASTROWMINICATS
					<input type="radio" name="productType" id="productType4" class="group" value="VIPLASTROWMINICATS" />VIP LASTROWMINICATS
					<input type="radio" name="productType" id="productType5" class="group" value="PRESALEZONETICKETS" /> PRESALE ZONETICKETS
				</td>
			</tr>
			<tr>
				<td>
					Full Name:
				</td>
				<td>
					<input type="text" name="fullName" id="fullName" />
				</td>
			</tr>
			<tr>
				<td>
					Email Address:
				</td>
				<td>
					<input type="text" name="email" id="email" />
				</td>
			</tr>
			
			<tr>
				<td>
					Phone Number:
				</td>
				<td>
					<input type="text" name="phoneNumber" id="phoneNumber" />
				</td>
			</tr>
			
			<tr>
				<td>
					Subject:
				</td>
				<td>
					<input type="text" name="subject" id="subject" />
				</td>
			</tr>
			
			<tr>
				<td>
					Message:
				</td>
				<td>
					<textarea rows="5" cols="50" name="message" id="message"></textarea>
				</td>
			</tr>
			
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>
	</form>    
</div>	
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/EmailCustomerRequestInfo.xml?configId=xxx&productType=xxx&fullName=&email=&phoneNumber=&subject=&message=<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/EmailCustomerRequestInfo.json?configId=xxx&productType=xxx&fullName=&email=&phoneNumber=&subject=&message=<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
