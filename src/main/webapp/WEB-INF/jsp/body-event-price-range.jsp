<head>
	<script type="text/javascript">
	$(document).ready(function() {
		var validator = $("#eventPriceRange").validate({
			rules: {
			configId:"required",
			eventId:"required"
			},
			messages:{
				configId:"Config id is required.",
				eventId:"Event id is required."
			}
		});
	});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="GetEventPriceRange.xml" id="eventPriceRange" target="_blank">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			<tr>
				<td>
					Event Id:
				</td>
				<td>
					<input type="text" name="eventId" id="eventId" class="group">
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetEventPriceRange.xml?configId=xxxx&eventId=54796<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetEventPriceRange.json?configId=xxxx&eventId=54796<br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>
Output:<br/>
<div style="background-color:#CCC">
&lt;EventPriceDetails&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventId&gt;54796&lt;/eventId&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventName&gt;New York Rangers vs. Buffalo Sabres&lt;/eventName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventDate&gt;April 10, 2014&lt;/eventDate&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventTime&gt;07:00 PM&lt;/eventTime&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ACTIVE&lt;/status&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;minPrice&gt;98.67&lt;/minPrice&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;maxPrice&gt;471.9&lt;/maxPrice&gt;<br/>
&lt;EventPriceDetails&gt;<br/>
</div>

