<head>
	<script type="text/javascript">
		$(document).ready(function() {
			var validator = $("#ExtendHoldTicket").validate({
				rules: {
				configId:"required",
				holdTicketId:"required"
				},
				messages:{
					configId:"Config id is required.",
					holdTicketId:"Hold Ticket id is required."
				}
			});
		});
	</script>
</head>
<br/>
Click <a href="../"> here </a> for complete list of operations.
<br/>
<div>
	<form id="ExtendHoldTicket" action="ExtendHoldOnTickets.xml" target="_blank">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			<tr>
				<td>
					Hold Id:
				</td>
				<td>
					<input type="text" name="holdTicketId" id="holdTicketId">
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
</form>
</div>
<br/>
Input URL:<br/>
<br/>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/ExtendHoldOnTickets.xml?configId=xxxx&holdTicketId=112,128..<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/ExtendHoldOnTickets.json?configId=xxxx&holdTicketId=112,128..<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>
Output:<br/>
<div style="background-color:#CCC">
	&lt;List&gt;<br/>
	&nbsp;&nbsp;&nbsp;&lt;holdOnTicketList&gt; <br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;HoldOnTicket&gt; <br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;112&lt;/id&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;message&gt;Hold on Time Extended Successfully&lt;/message&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;SUCCESS&lt;/status&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/HoldOnTicket&gt; <br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;HoldOnTicket&gt; <br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;113&lt;/id&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;message&gt;Hold on Time Extended Successfully&lt;/message&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;SUCCESS&lt;/status&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/HoldOnTicket&gt; <br/>
	&nbsp;&nbsp;&nbsp;&lt;/holdOnTicketList&gt; <br/>
	
	&nbsp;&nbsp;&nbsp;&lt;invalidHoldTicketList&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;InvalidHoldTicket&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;212&lt;/id&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;message&gt;Invalid or Expired Hold Ticket Id.&lt;/message&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ERROR&lt;/status&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/InvalidHoldTicket&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;InvalidHoldTicket&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;213&lt;/id&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;message&gt;Invalid or Expired Hold Ticket Id.&lt;/message&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ERROR&lt;/status&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/InvalidHoldTicket&gt;<br/>
	&nbsp;&nbsp;&nbsp;&lt;/invalidHoldTicketList&gt;<br/>
	&lt;/List&gt;<br/>
</div>