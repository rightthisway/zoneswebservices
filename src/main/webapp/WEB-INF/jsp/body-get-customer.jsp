<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetCustomerInfo.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">

	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	customerId - Integer (Mandatory)<br/>
	platForm - String (Mandatory)<br/>
</div>
<br/><br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">
{
  "customerDetails": {
    "status": 1,
    "error": null,
    "resetPassword": null,
    "customerLoyalty": {
      "customerId": 9,
      "activePoints": 0,
      "totalEarnedPoints": 0,
      "totalSpentPoints": 0,
      "latestEarnedPoints": 0,
      "latestSpentPoints": 0
    },
    "customer": {
      "id": 9,
      "userName": "ulaganathantoall@gmail.com",
      "customerName": "Ulaganathan Koothaiyan",
      "email": "ulaganathantoall@gmail.com",
      "phone": "",
      "extension": "",
      "otherPhone": "",
      "customerType": "WEB_CUSTOMER",
      "signupType": "REWARDTHEFAN",
      "productType": "REWARDTHEFAN",
      "firstTimeLogin": null,
      "superFanExpired": "No",
      "billingAddress": [
        {
          "id": 92,
          "customerId": 9,
          "firstName": "t4",
          "lastName": "t5",
          "addressLine1": "chk1",
          "addressLine2": "chk1",
          "addressType": "BILLING_ADDRESS",
          "city": "CA",
          "state": {
            "name": "Alabama",
            "id": 1,
            "countryId": 1,
            "shortDesc": "AL"
          },
          "country": {
            "id": 1,
            "countryName": "UNITED STATES",
            "shortDesc": "US",
            "stateList": null
          },
          "stateName": "Alabama",
          "countryName": "UNITED STATES",
          "zipCode": "545454",
          "phone1": "4086137878",
          "phone2": null,
          "phone3": null,
          "representativeName": null
        }
      ],
      "shippingAddress": [
        {
          "id": 96,
          "customerId": 9,
          "firstName": "t4",
          "lastName": "t5",
          "addressLine1": "chk1",
          "addressLine2": "chk1",
          "addressType": "SHIPPING_ADDRESS",
          "city": "CA",
          "state": {
            "name": "Alabama",
            "id": 1,
            "countryId": 1,
            "shortDesc": "AL"
          },
          "country": {
            "id": 1,
            "countryName": "UNITED STATES",
            "shortDesc": "US",
            "stateList": null
          },
          "stateName": "Alabama",
          "countryName": "UNITED STATES",
          "zipCode": "545454",
          "phone1": "4086137878",
          "phone2": null,
          "phone3": null,
          "representativeName": null
        }
      ]
    }
  }
}

</div>

<br/><br/>


<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>