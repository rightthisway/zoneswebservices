<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetCustomerLoyaltyRewards.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">

	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	customerId -String (Mandatory - Provided by Right This Way )<br/>
	
</div>

<br/>
<br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">
{	
	{
  "customerLoyaltyDetails": {
    "status": 1,
    "error": null,
    "resetPassword": null,
    "customerLoyalty": {
      "customerId": 2,
      "activePoints": 0,
      "totalEarnedPoints": 0,
      "totalSpentPoints": 0,
      "latestEarnedPoints": 0,
      "latestSpentPoints": 0
    },
    "customer": null
  }
}
}
</div>
<br/>
<br/>

<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>

