<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/StripeTransaction.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">
	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	transactionAmount - Integer (Mandatory)<br/>  
	token - String (Mandatory)<br/>
	description - String (Mandatory)<br/>
	platForm - String (Mandatory)<br/>
</div>
<br/><br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">



</div>

<br/><br/>


<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>