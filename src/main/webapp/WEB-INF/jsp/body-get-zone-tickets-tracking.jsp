Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="ZoneTicketUrlTracking.json" id="event" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId" />
				</td>
			</tr>
			
			<tr>
				<td>
					Client IP:
				</td>
				<td>
					<input type="text" name="clientIPAddress" id="clientIPAddress" />
				</td>
			</tr>
			<tr>
				<td>
					Request URL:
				</td>
				<td>
					<input type="text" name="requestUrl" id="requestUrl" />
				</td>
			</tr>
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>
	</form>    
</div>	
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/ZoneTicketUrlTracking.xml?configId=xxx&clientIPAddress=xxx&requestUrl=xxx<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/ZoneTicketUrlTracking.json?configId=xxx&clientIPAddress=xxx&requestUrl=xxx<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>