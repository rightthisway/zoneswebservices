<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetArtistInfo.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">
	configId - String (Mandatory) - Provided by Right This Way <br/>
	artistId - Integer (Mandatory)<br/>
</div>


<b>Response :</b><br/>
<div style="background-color:#CCC">
{
  "artistInfo": {
    "status": 1,
    "error": null,
    "artistId": 15215,
    "artistName": "Dave Matthews Band",
    "artistEvents": [
      {
        "eventId": 1000372073,
        "eventName": "Dave Matthews Band",
        "eventDateTime": "07/22/2016 08:00 PM",
        "eventDateStr": "07/22/2016",
        "eventTimeStr": "08:00 PM",
        "venueName": "Klipsch Music Center",
        "city": "Noblesville",
        "state": "IN",
        "country": "US",
        "pinCode": "46060",
        "artistName": "Dave Matthews Band",
        "grandChildCategoryName": "None",
        "childCategoryName": "Festival",
        "parentCategoryName": "Concerts",
        "ticketPriceTag": "",
        "isSuperFanEvent": false,
        "isFavoriteEvent": false,
        "customerId": null,
        "svgText": null,
        "isMapWithSvg": null,
        "svgMapPath": null,
        "svgWebViewUrl": null,
        "shareLinkUrl": null
      },
      {
        "eventId": 1000372073,
        "eventName": "Dave Matthews Band",
        "eventDateTime": "07/22/2016 08:00 PM",
        "eventDateStr": "07/22/2016",
        "eventTimeStr": "08:00 PM",
        "venueName": "Klipsch Music Center",
        "city": "Noblesville",
        "state": "IN",
        "country": "US",
        "pinCode": "46060",
        "artistName": "Dave Matthews Band",
        "grandChildCategoryName": "None",
        "childCategoryName": "Festival",
        "parentCategoryName": "Concerts",
        "ticketPriceTag": "",
        "isSuperFanEvent": false,
        "isFavoriteEvent": false,
        "customerId": null,
        "svgText": null,
        "isMapWithSvg": null,
        "svgMapPath": null,
        "svgWebViewUrl": null,
        "shareLinkUrl": null
      },
      {
        "eventId": 1000372073,
        "eventName": "Dave Matthews Band",
        "eventDateTime": "07/22/2016 08:00 PM",
        "eventDateStr": "07/22/2016",
        "eventTimeStr": "08:00 PM",
        "venueName": "Klipsch Music Center",
        "city": "Noblesville",
        "state": "IN",
        "country": "US",
        "pinCode": "46060",
        "artistName": "Dave Matthews Band",
        "grandChildCategoryName": "None",
        "childCategoryName": "Festival",
        "parentCategoryName": "Concerts",
        "ticketPriceTag": "",
        "isSuperFanEvent": false,
        "isFavoriteEvent": false,
        "customerId": null,
        "svgText": null,
        "isMapWithSvg": null,
        "svgMapPath": null,
        "svgWebViewUrl": null,
        "shareLinkUrl": null
      }
    ]
  }
}
</div>


<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>