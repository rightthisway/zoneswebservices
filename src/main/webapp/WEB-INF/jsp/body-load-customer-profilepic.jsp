<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetCustomerProfilePic.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">
	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	customerId - Integer (Mandatory)
	actionType - String (Mandatory)
	customerProfilePic - MultiPartFile (Mandatory)
	 Note: CustomerProfilePic request param is mandatory if the action type is saveProfilePic
	       otherwise optional.
	 
</div>
<br/><br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">

{
  "customerProfilePic": {
    "customerId": 1,
    "status": 1,
    "description": null,
    "error": null,
    "customerPicWebView": "http://52.22.87.62/sandbox.CustomerProfilePicView.html?configId=RewardTheFan&customerId=1"
  }
}


</div>

<br/><br/>


<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>