<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/FavoriteArtistEventInfoByCustomer.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">
	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	customerId - Integer (Mandatory)
	pageNumber - Integer (Mandatory)
</div>
<br/><br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">

{
  "manageFavorites": {
    "customerId": 68,
    "status": 1,
    "description": null,
    "error": null,
    "favouriteArtists": [
      {
        "id": 16559,
        "name": "MercyMe",
        "parentType": null,
        "custFavFlag": true,
        "custSuperFanFlag": false,
        "artistStatus": "ACTIVE",
        "grandChildCategory": null
      }
    ],
    "favouriteEvents": [
      {
        "eventId": 1000369999,
        "eventName": "Spirit West Coast: MercyMe Jeremy Camp Matthew West Phil Wickham & Blanca",
        "eventDateTime": "06/10/2016 4:00PM",
        "eventDateStr": "06/10/2016",
        "eventTimeStr": "4:00PM",
        "venueName": "Citizens Business Bank Arena - CA",
        "city": "Ontario",
        "state": "CA",
        "country": "US",
        "pinCode": "91764",
        "artistName": "MercyMe",
        "grandChildCategoryName": "None",
        "childCategoryName": "Religious",
        "parentCategoryName": "Concerts",
        "ticketPriceTag": "$55 to $158",
        "isSuperFanEvent": false,
        "isFavoriteEvent": true,
        "customerId": null,
        "svgText": null,
        "isMapWithSvg": null,
        "svgMapPath": null,
        "svgWebViewUrl": null,
        "shareLinkUrl": "http://zonetickets.com/event/1000369999"
      },
      {
        "eventId": 1000368395,
        "eventName": "Fishfest: Jeremy Camp Matthew West Phil Wickham Blanca Urban Rescue For All Seasons MercyMe",
        "eventDateTime": "06/11/2016 4:00PM",
        "eventDateStr": "06/11/2016",
        "eventTimeStr": "4:00PM",
        "venueName": "Irvine Meadows Amphitheatre",
        "city": "Irvine",
        "state": "CA",
        "country": "US",
        "pinCode": "92618",
        "artistName": "MercyMe",
        "grandChildCategoryName": "None",
        "childCategoryName": "Religious",
        "parentCategoryName": "Concerts",
        "ticketPriceTag": "$49 to $340",
        "isSuperFanEvent": false,
        "isFavoriteEvent": true,
        "customerId": null,
        "svgText": null,
        "isMapWithSvg": null,
        "svgMapPath": null,
        "svgWebViewUrl": null,
        "shareLinkUrl": "http://zonetickets.com/event/1000368395"
      },
      {
        "eventId": 1000370387,
        "eventName": "Spirit West Coast: MercyMe Jeremy Camp Matthew West Phil Wickham & Blanca",
        "eventDateTime": "06/12/2016 4:00PM",
        "eventDateStr": "06/12/2016",
        "eventTimeStr": "4:00PM",
        "venueName": "Concord Pavilion",
        "city": "Concord",
        "state": "CA",
        "country": "US",
        "pinCode": "94521",
        "artistName": "MercyMe",
        "grandChildCategoryName": "None",
        "childCategoryName": "Religious",
        "parentCategoryName": "Concerts",
        "ticketPriceTag": "$42 to $126",
        "isSuperFanEvent": false,
        "isFavoriteEvent": true,
        "customerId": null,
        "svgText": null,
        "isMapWithSvg": null,
        "svgMapPath": null,
        "svgWebViewUrl": null,
        "shareLinkUrl": "http://zonetickets.com/event/1000370387"
      }
    ]
  }
}


</div>

<br/><br/>


<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>