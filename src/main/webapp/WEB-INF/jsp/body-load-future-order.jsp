<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/FutureOrderConfirmation.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">
	configId - String (Mandatory) - Provided by Right This Way <br/>
	productType - String (Mandatory) - Provided by Right This Way <br/>
	customerId - Integer (Mandatory)<br/>
	teamId - Integer (Mandatory)
</div>


<b>Response :</b><br/>
<div style="background-color:#CCC">

{
  "futureOrderConfirmation": {
    "status": 1,
    "error": null,
    "leagueName": "NFL",
    "teamName": "Denver Broncos",
    "requiredPoints": 10000,
    "activePoints": 50000,
    "pointsRemaining": 40000,
    "qtyTag": "For 2 Tickets",
    "message": "You have just redeemed 2 tickets for Denver Broncos seated together."
  }
}

</div>


<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>