<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"> 
<title>My HTML</title>
</head>
<body>

<style>
.selected{
	stroke : black !important;
	stroke-width : 10px !important;
}

#svg1{
            -webkit-tap-highlight-color: rgba(0,0,0,0);
            -webkit-tap-highlight-color: transparent;
            background:transparent;
        }
		
* {
	margin:0; 
	padding:0;
}

svg{
    max-width: 100%;
    height: 100%;
}

</style>

<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.12.0.min.js"></script>

<script language="javascript">
	function passCategoryID(cat_id,removeFilterFlag) {
		//alert(cat_id);
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;
		if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ){
    		var message = {
			"cmd":"message",
			"catId":cat_id,
			"removeFilter":removeFilterFlag,
			}
			window.webkit.messageHandlers.native.postMessage(message);
		}else if( userAgent.match( /Android/i ) ) {
  			AndroidFunction.showToast(cat_id,removeFilterFlag);
  		}else {
  			return '';
  		}
	}
	
	function highlightZone(zone){
		 
		var allShapesWithClass = $('.selected');
			$.each(allShapesWithClass, function( index, obj ) {
				var id = $(obj).attr('id').split('-:-')[0];
				$(obj).removeAttr('class');
			});
			
			var allShapes = $("#shapes").children();
				$.each(allShapes, function( index, obj ) {
				$(obj).css('stroke-width','3px');
			});
			
			var sectionsOfZones = $('#shapes').find($('[id*='+ zone +'-\\:-]')).each(function(){
				$(this).css('stroke-width','0px');
			});
		
			var borders = $('#zone_borders').find($('[id=zone-\\:-'+ zone +']')).each(function(){
				$(this).attr('class','selected');
				$(this).css('fill-opacity','0.7');
			});
	}
	
	function removeHighlightedZone(zone){
			/* var allShapesWithClass = $('.selected');
			$.each(allShapesWithClass, function( index, obj ) {
				var id = $(obj).attr('id').split('-:-')[0];
				$(obj).removeAttr('class');
			}); */
			
			var borders = $('#shapes').find($('.selected')).each(function(index, obj){
				//alert('Remove Class');
				$(obj).removeAttr('class');
			});
			
			var borders = $('#zone_borders').find($('.selected')).each(function(index, obj){
				//alert('Remove Class');
				$(obj).removeAttr('class');
			});
	}
	
	function setSvgDetails(mapUrl, svgText){
		$("#mapImage").attr('xlink:href',mapUrl);
		$("#svg1").append(svgText);
		console.log(mapUrl);
		console.log(svgText);
		
	}
	
	function fillColors(map){
		var shapeSvgTags = $('#shapes').children();
		
		$.each(shapeSvgTags, function( index, value ) {
		var zoneName = $(value).attr('id');
		var section = zoneName.split('-\:-')[1];
		zoneName = zoneName.split('-\:-')[0];
		
		//alert(zoneName);
		$(value).css('fill',map[zoneName]);
		$(value).css('fill-opacity',0.7);
		$(value).attr('class','');
		
	});
	}
	
	function fillColors2(){
		var obj = {};
		<c:forEach var="color" items="${colorsMap}">
			obj["${color.key}"] = "${color.value}";
		</c:forEach>
		//alert(obj['a']);
			var rects = $('#shapes').children().each(function(){
			var zone = $(this).attr('id').split('-:-')[0];
			if(obj[zone] == null){
				/* $(this).css('fill','#F4ED6D');
				$(this).css('fill-opacity','0.7'); */
			}else{
			//alert(${colorsMap[h]});
			//alert("${colorsMap["+zone+"]}");
				$(this).css('fill',obj[zone]);
				$(this).css('fill-opacity','0.7');
			}
			});
	}
	
	
	
	
		$(document).ready(function(){
			
			
			var checkedZone=null;
			var uncheckFlag = false; 
			
			//setSvgDetails('http://52.22.87.62/SvgMaps/655_PRESALE.gif','<g id="asa"></g>');
			
			fillColors2();
			
			$("#svg1").find("#zone_letters").show();
			$("#svg1").find("#section_letters").show();
		
			$('#svg1').find("#shapes").on( 'click', 'path', function (){
				//alert($(this).attr('id').split('-:-')[0]);
				var zone = $(this).attr('id').split('-:-')[0];
				if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == zone || checkedZone === zone)){
					uncheckFlag = true;
				}else{
					uncheckFlag = false;
				}
				console.log(zone);
				if(!uncheckFlag){
					passCategoryID(zone,false);
					highlightZone(zone);
					checkedZone = zone;
				}else{
					passCategoryID(zone,true);
					removeHighlightedZone(zone);
					checkedZone = null;
				}
			});
			
			$('#svg1').find("#shapes").on( 'click', 'polygon', function (){
				//alert($(this).attr('id').split('-:-')[0]);
				var zone = $(this).attr('id').split('-:-')[0];
				if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == zone || checkedZone === zone)){
					uncheckFlag = true;
				}else{
					uncheckFlag = false;
				}
				console.log(zone);
				if(!uncheckFlag){
					passCategoryID(zone,false);
					highlightZone(zone);
					checkedZone = zone;
				}else{
					passCategoryID(zone,true);
					removeHighlightedZone(zone);
					checkedZone = null;
				}
			});
			
			$('#svg1').find("#shapes").on( 'click', 'rect', function (){
				//alert($(this).attr('id').split('-:-')[0]);
				var zone = $(this).attr('id').split('-:-')[0];
				if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == zone || checkedZone === zone)){
					uncheckFlag = true;
				}else{
					uncheckFlag = false;
				}
				console.log(zone);
				if(!uncheckFlag){
					passCategoryID(zone,false);
					highlightZone(zone);
					checkedZone = zone;
				}else{
					passCategoryID(zone,true);
					removeHighlightedZone(zone);
					checkedZone = null;
				}
			});
			
			$('#svg1').find("#shapes").on( 'click', 'circle', function (){
				//alert($(this).attr('id').split('-:-')[0]);
				var zone = $(this).attr('id').split('-:-')[0];
				if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == zone || checkedZone === zone)){
					uncheckFlag = true;
				}else{
					uncheckFlag = false;
				}
				console.log(zone);
				if(!uncheckFlag){
					passCategoryID(zone,false);
					highlightZone(zone);
					checkedZone = zone;
				}else{
					passCategoryID(zone,true);
					removeHighlightedZone(zone);
					checkedZone = null;
				}
			});
			
			$('#svg1').find("#shapes").on( 'click', 'polyline', function (){
				//alert($(this).attr('id').split('-:-')[0]);
				var zone = $(this).attr('id').split('-:-')[0];
				if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == zone || checkedZone === zone)){
					uncheckFlag = true;
				}else{
					uncheckFlag = false;
				}
				console.log(zone);
				if(!uncheckFlag){
					passCategoryID(zone,false);
					highlightZone(zone);
					checkedZone = zone;
				}else{
					passCategoryID(zone,true);
					removeHighlightedZone(zone);
					checkedZone = null;
				}
			});
			
			$('#svg1').find("#shapes").on( 'click', 'elipse', function (){
				//alert($(this).attr('id').split('-:-')[0]);
				var zone = $(this).attr('id').split('-:-')[0];
				if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == zone || checkedZone === zone)){
					uncheckFlag = true;
				}else{
					uncheckFlag = false;
				}
				console.log(zone);
				if(!uncheckFlag){
					passCategoryID(zone,false);
					highlightZone(zone);
					checkedZone = zone;
				}else{
					passCategoryID(zone,true);
					removeHighlightedZone(zone);
					checkedZone = null;
				}
			});
			
			//<path id="e-:-3"
			//<text id="text_sec_j-:-201"
			//<text id="text_b"
			
			
			$('#svg1').on( 'click', 'text', function (){
				//alert($(this).attr('id').split('-:-')[0]);
				var zone = $(this).attr('id');
				
				if(null != zone){
					if(zone.includes("text_sec")){
						zone = zone.replace('text_sec_','');
						zone = zone.split('-:-')[0];
					}else{
						zone = zone.split('_')[1];
					}
					
					if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == zone || checkedZone === zone)){
						uncheckFlag = true;
					}else{
						uncheckFlag = false;
					}
					console.log(zone);
					if(!uncheckFlag){
						passCategoryID(zone,false);
						highlightZone(zone);
						checkedZone = zone;
					}else{
						passCategoryID(zone,true);
						removeHighlightedZone(zone);
						checkedZone = null;
					}
				}
				
			});
			
		// svg zoom in and zoom out
	/*
		var svgZoomClicks = 0;
		var example = $("#svg1").svgPanZoom();
		$("#svgZoomIn").click(function(){
				if(svgZoomClicks < 10){
					example.zoomIn();
					svgZoomClicks++;
				}else{
					$('#svgZoomIn').prop('disabled','disabled');
				}
			
		});
				
		$("#svgZoomOut").click(function(){
				if(svgZoomClicks > 0){
					example.zoomOut();
					svgZoomClicks--;
				}else{
					$('#svgZoomOut').prop('disabled','disabled');
				}
			
		});
		*/
			
		});
		
</script>
<!-- <div style="width:450px;height;300px;" align="right"> -->
<div style="width:100%;height:99%; position: absolute;text-align: center;" >
<svg id="svg1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;" viewBox="0 0 2560 2048" preserveAspectRatio="xMinYMin"><desc id="svgDesc" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Rapha�l 2.1.4</desc><defs id="svgDefs" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>

<image id="mapImage" x="0" y="0" width="2560" height="2048" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="${zoneEvent.svgMapPath}" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" stroke-width="1"></image>

${zoneEvent.svgText}

</svg>
 
${error}
</div>
</body>
</html>