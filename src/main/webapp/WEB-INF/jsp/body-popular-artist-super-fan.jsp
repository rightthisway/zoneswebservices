<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetPopularArtistForSuperFan.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">

	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	locationFlag - String (Mandatory - Yes or No )<br/>
	zipCode - String (Optional )<br/>
	latitude - String (Optional )<br/>
	longitude - String (Optional )<br/>
	customerId - Integer (Optional)<br/>
</div>

<br/>
<br/>

<b>Response :</b><br/>
<div style="background-color:#CCC">
{
	"artistList":{"status":1,"error":null,
	"artists":[
		{"id":8808,"name":"New Jersey
		Devils","parentType":"Sports","custFavFlag":false,"custSuperFanFlag":true},{"id":8811,"name":"New York
		Rangers","parentType":"Sports","custFavFlag":false,"custSuperFanFlag":false},{"id":40600,"name":"Bad Boy Family
		Reunion: Puff D","parentType":"Concerts","custFavFlag":false,"custSuperFanFlag":false}]
	}
}
</div>
<br/>
<br/>



<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>