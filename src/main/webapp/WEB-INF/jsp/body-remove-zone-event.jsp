<head>
	<script type="text/javascript">
		$(document).ready(function() {
			var validator = $("#ticket").validate({
				rules: {
				configId:"required",
				eventId:"required"
				},
				messages:{
					configId:"Config id is required.",
					eventId:"Event id is required."
				}
			});
		});
	</script>
</head>
<br/>
Click <a href="../"> here </a> for complete list of operations.
<br/>
<div>
	<form id="ticket"action="RemoveZoneEvent.json" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			
			<tr>
				<td>
					Event Id:
				</td>
				<td>
					<input type="text" name="eventIds" id="eventIds">
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
</form>
</div>
<br/>
Input URL:<br/>
<br/>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/RemoveZoneEvent.xml?configId=xxxx&eventIds=xxx<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/RemoveZoneEvent.json?configId=xxxx&eventIds=xxx<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<!-- <br/>
Output:<br/>

<div style="background-color:#CCC">
	&lt;TicketList&gt;<br/>
	&nbsp;&nbsp;&nbsp;&lt;zoneEvent&gt; <br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventId&gt;42762&lt;/eventId&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventName&gt;Wicked New York&lt;/eventName&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventDate&gt;April 20, 2012&lt;/eventDate&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventTime&gt;08:00 PM&lt;/eventTime&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;artistName&gt;Wicked&lt;/artistName&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueName&gt;Gershwin Theatre&lt;/venueName&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCity&gt;New York&lt;/venueCity&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueState&gt;NY&lt;/venueState&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCountry&gt;US&lt;/venueCountry&gt;<br/>	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;mapPath&gt;http://zonesmaps.com/1167_WICKED.gif&lt;/mapPath&gt;<br/>
	&nbsp;&nbsp;&nbsp;&lt;/zoneEvent&gt;<br/>
	&nbsp;&nbsp;&nbsp;&lt;tickets&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TicketGroup&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;62824&lt;/id&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventId&gt;42762&lt;/eventId&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;category&gt;A&lt;/category&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;quantity&gt;6&lt;/quantity&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;price&gt;255.98333333333335&lt;/price&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;total&gt;1535.9&lt;/total&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ACTIVE&lt;/status&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/TicketGroup&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TicketGroup&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;62825&lt;/id&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventId&gt;42762&lt;/eventId&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;category&gt;B&lt;/category&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;quantity&gt;2&lt;/quantity&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;price&gt;100.00&lt;/price&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;total&gt;200.00&lt;/total&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ACTIVE&lt;/status&gt;<br/>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/TicketGroup&gt;<br/>
	&lt;/tickets&gt;<br/>
	&lt;/TicketList&gt;<br/>
</div> -->