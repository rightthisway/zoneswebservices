<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<div class="fullwidth">
    <div class="container">
        <div class="forgot-section fullwidth">
            <form class="forgot-form" action="${pageContext.servletContext.contextPath}/PreOrderConfirmation" method="post">
                <input type="text" required placeholder="Token" name="resetToken" id="resetToken">
                <input type="password" required placeholder="New Password" name="newPassword" id="newPassword">
                <button type="submit" class="orangebtn">Submit</button>
            </form>
        </div>
    </div>
</div>