<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");

			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var validator = $("#event").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="LastrowMiniCatsGeneralizedSearch.json" id="event" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			
			<!-- <tr>
				<td>
					Product Type:
				</td>
				<td>
					<input type="radio" name="productType" id="productType1" value="ZONETICKETS" checked="checked" > ZONETICKETS
					<input type="radio" name="productType" id="productType2" value="MINICATS"> MINICATS
					<input type="radio" name="productType" id="productType3" value="VIPMINICATS"> VIPMINICATS
				</td>
			</tr>
			 -->
			<tr>
				<td>
					Customer Id:
				</td>
				<td>
					<input type="text" name="customerId" id="customerId" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Generalized Search:
				</td>
				<td>
					<input type="text" name="generalizedSearch" id="generalizedSearch" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					GPS Search:
				</td>
				<td>
					<input type="radio" name="searchByGps" id="gpsSearch1" value="Yes" checked="checked" > Yes
					<input type="radio" name="searchByGps" id="gpsSearch2" value="No"> No
				</td>
			</tr>
			
			<tr>
				<td>
					Page Number:
				</td>
				<td>
					<input type="text" name="pageNumber" id="pageNumber" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/LastrowMiniCatsGeneralizedSearch.json?configId=xxxx&generalizedSearch=&searchByGps=&customerId=&pageNumber=<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<!-- <br/>
Output:<br/>
<div style="background-color:#CCC">
&lt;list&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;zoneEvents&gt; <br/>
&nbsp;&nbsp;&nbsp;&lt;zoneEvent&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventId&gt;42762&lt;/eventId&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventName&gt;Wicked New York&lt;/eventName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventDate&gt;03/27/2015&lt;/eventDate&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventTime&gt;08:00 PM&lt;/eventTime&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;artistName&gt;Wicked&lt;/artistName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueName&gt;Madison Square Garden&lt;/venueName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCity&gt;New York&lt;/venueCity&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueState&gt;NY&lt;/venueState&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCountry&gt;US&lt;/venueCountry&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/zoneEvent&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/zoneEvents&gt;<br/>
&lt;list&gt;<br/>
</div>

 -->