<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetPossibleArtistBySocialInfo.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">

	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	artistKeys - String (Mandatory) - All artist keys separated by ","<br/>
	customerId - Integer (Optional)<br/>
</div>

<br/><br/>
<b>Response :</b><br/>
<div style="background-color:#CCC">
{
	"artistList":{"status":1,"error":null,
		"artists":[{"id":17251,"name":"Luke Bryan","parentType":"Concerts","custFavFlag":false,"custSuperFanFlag":false},
				   {"id":17252,"name":"The Book of Mormon","parentType":"Concerts","custFavFlag":true,"custSuperFanFlag":true}]
	}
}
</div>
<br/><br/>

<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>