<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/SearchArtist.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">

	configId - String (Mandatory - Provided by Right This Way )<br/>
	productType - String (Mandatory - Provided by Right This Way )<br/>
	searchKey - String (Mandatory)<br/>
	locationFlag - String (Mandatory - Yes or No )<br/>
	zipCode - String (Optional )<br/>
	latitude - String (Optional )<br/>
	longitude - String (Optional )<br/>
	customerId - Integer (Optional)<br/>
	<!-- pageNumber - Integer (Mandatory)<br/> -->
</div>

<br/><br/>
<b>Response :</b><br/>
<div style="background-color:#CCC">
	{
	"artistList":{"status":1,"error":null,
		"artists":[{"id":22161,"name":"Mozart
			Orchestra Of New York","parentType":null,"custFavFlag":false,"custSuperFanFlag":false},{"id":37795,"name":"New York
			Academy of Ballet","parentType":null,"custFavFlag":true,"custSuperFanFlag":false},{"id":14272,"name":"New York City
			Ballet","parentType":null,"custFavFlag":false,"custSuperFanFlag":false},{"id":19812,"name":"New York City
			FC","parentType":null,"custFavFlag":false,"custSuperFanFlag":true}{"id":22156,"name":"Western New York
			Flash","parentType":null,"custFavFlag":true,"custSuperFanFlag":false}]
		}
	}</div>
<br/><br/>

<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>