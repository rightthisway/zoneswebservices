<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<head>

<style>
	rect[title]:hover:after {
  		content: "${getTicketGroup.ticketGroup.zoneStarInfo}";
  		color: #000000;
  		position: absolute;
  		font-family: "Open Sans",sans-serif;
  		font-weight: 400;
  		left:25.1%;
  		margin-top:2.2%;
  		width:320px;
  		height:100px;
  		background-color:#e2e2e2;
  		border:2px solid black;
  }
</style>



<style>
.path {
  stroke-dasharray: 30;
  stroke-dashoffset: 1000;
  stroke-width: 10;
  animation: dash 5s linear alternate infinite;
}

.selected{
	stroke : black !important;
	stroke-width : 5px !important;
}

#infoDiv {
    border-radius: 15px;
    background-color:white;
    background-position: left top;
    background-repeat: repeat;
    padding: 10px;
    width: 200px;
    height: 65px;
	border-color: white transparent;
	border-style: solid;
	
}

#infoDiv:after {
    content: "";
    position: absolute;
   top: 100%;
   left: 20px;
   border-top: 20px solid blue;
   border-top-color: inherit; 
   border-left: 20px solid transparent;
  
}
@keyframes dash {
  from {
    stroke-dashoffset: 1000;
  }
  to {
    stroke-dashoffset: 0;
  }
}

</style>


<script type="text/javascript">


// set up some sample squares
$(document).ready(function(){
	
	var svg=document.getElementById("svg1");
	var pt=svg.createSVGPoint();
	var loc;
	var idMissingFlag = false;
	$("#infoDiv").hide();
	var svgChilds = $('#svg1').children();
	for(var i=0 ; i < svgChilds.length ; i++){
		if(svgChilds[i].getAttribute('id') != 'mapImage'){
			var zoneName = svgChilds[i].getAttribute('id');
			if(document.getElementById("td_qty_"+zoneName) != null && document.getElementById("td_qty_"+zoneName).style.display != 'none'){
				svgChilds[i].setAttribute('title',document.getElementById("td_qty_"+zoneName).innerHTML + 
				' Tickets @ ' + document.getElementById("td_price_"+zoneName).innerHTML);
			}else{
				svgChilds[i].setAttribute('title','No Tickets');
			}
			
		}
	}
	
	var zonesBordersSvgGTag = $("#zone_borders");
	if(typeof(zonesBordersSvgGTag.attr('id')) != 'undefined'){
		var childs = $("#zone_borders").children();
		$.each(childs, function( index, obj ) {
			if(typeof($(obj).attr('id')) == 'undefined'){
				idMissingFlag = true;
			}
		});
	}else{
		alert('id = zone_borders is missing');
	}
	
	var shapesSvgGTag = $("#shapes");
	if(typeof(shapesSvgGTag.attr('id')) != 'undefined'){
		var childs = $("#shapes").children();
		$.each(childs, function( index, obj ) {
			if(typeof($(obj).attr('id')) == 'undefined'){
				idMissingFlag = true;
			}
		});
	}else{
		alert('id = shapes is missing');
	}
	
	var zoneLettersSvgGTag = $("#zone_letters");
	if(typeof(zoneLettersSvgGTag.attr('id')) != 'undefined'){
		var childs = $("#zone_letters").children();
		$.each(childs, function( index, obj ) {
			if(typeof($(obj).attr('id')) == 'undefined'){
				idMissingFlag = true;
			}
		});
	}else{
		alert('id = zone_letters is missing');
	}
	
	var sectionLettersSvgGTag = $("#section_letters");
	if(typeof(sectionLettersSvgGTag.attr('id')) != 'undefined'){
		var childs = $("#section_letters").children();
		$.each(childs, function( index, obj ) {
			if(typeof($(obj).attr('id')) == 'undefined'){
				idMissingFlag = true;
			}
		});
	}else{
		alert('id = section_letters is missing');
	}
	
	if(idMissingFlag == true){
		alert('Id is missing in svg text file');
	}
	
	var ticketTable = document.getElementById('ticketTable');
	var trTags = ticketTable.getElementsByTagName('tr');
	$('#zone_letters').show();
	$('#section_letters').show();
	$('#zone_borders').show();
	
	$("#shapes polygon").each(function(){
		$(this).css('fill','#FFFFFF');
		$(this).css('fill-opacity',0.1);
	});
	$("#shapes rect").each(function(){
		$(this).css('fill','#FFFFFF');
		$(this).css('fill-opacity',0.1);
	});
	
	$("#shapes path").each(function(){
		$(this).css('fill','#FFFFFF');
		$(this).css('fill-opacity',0.1);
	});
	for(var i=0;i<trTags.length;i++){
		var zoneName = trTags[i].getAttribute('id').split('_')[1];
		if(trTags[i].getAttribute('id') != 'headTr'){
			zoneName = zoneName.replace(':','\\:');
			//alert(trTags[i].id );
			//alert(zoneName.split('-\\:-')[0]);
			$('rect[id*='+zoneName.split('-\\:-')[0]+'-\\:-]').css('fill',trTags[i].style.backgroundColor);
			$('rect[id*='+zoneName+']').css('fill-opacity',0.7);
			$('polygon[id*='+zoneName.split('-\\:-')[0]+'-\\:-]').css('fill',trTags[i].style.backgroundColor);
			$('polygon[id*='+zoneName+']').css('fill-opacity',0.7);
			$('path[id*='+zoneName.split('-\\:-')[0]+'-\\:-]').css('fill',trTags[i].style.backgroundColor);
			$('path[id*='+zoneName+']').css('fill-opacity',0.7);
		}
	}
	
	// filling all sections/zones with aprropriate color
	
	
	var zoneNames = [
	'A',
	'﻿A_CLUB',
'A_COURTSIDE',
'A_H',
'A_H_CLUB',
'A_V',
'A_V_CLUB',
'A1',
'A1 _CLUB',
'A1 H',
'A1 V',
'A1_A',
'A1_CLUB',
'A1_CLUB_H',
'A1_CLUB_V',
'A1_H',
'A1_V',
'A10',
'A2',
'A2 H',
'A2 V',
'A2_BOX',
'A2_CLUB',
'A2_CLUB_A',
'A2_CLUB_H',
'A2_CLUB_V',
'A2_H',
'A2_V',
'A3',
'A3_CLUB',
'A3_CLUB_H',
'A3_CLUB_V',
'A3_H',
'A3_V',
'A4',
'A4_CLUB',
'A5',
'A6',
'A7',
'A8',
'A9',
'COURTSIDE_A',
'B',
'B-CLUB',
'B-GRAND',
'B_CLUB',
'B_COURT',
'B_H',
'B_V',
'B1',
'B1 H',
'B1 V',
'B1_A',
'B1_CLUB',
'B1_CLUB_H',
'B1_CLUB_V',
'B1_H',
'B1_SIDE',
'B1_V',
'B2',
'B2 H',
'B2 V',
'B2_CLUB',
'B2_CLUB_A',
'B2_CLUB_H',
'B2_CLUB_V',
'B2_H',
'B2_V',
'B3',
'B3_BOX',
'B3_CLUB',
'B3_CLUB_H',
'B3_CLUB_V',
'B3_H',
'B3_V',
'B4',
'B4_CLUB',
'B5',
'B6',
'B7',
'COURTSIDE_B',
'C',
'C_BOX',
'C_CLUB',
'C_H',
'C_V',
'C1',
'C1 H',
'C1 V',
'C1_A',
'C1_BOX',
'C1_CLUB',
'C1_CLUB(IN)',
'C1_CLUB(OUT)',
'C1_CLUB_H',
'C1_CLUB_V',
'C1_H',
'C1_V',
'C2',
'C2 H',
'C2 V',
'C2(IN)',
'C2(OUT)',
'C2_CLUB',
'C2_CLUB_A',
'C2_CLUB_H',
'C2_CLUB_V',
'C2_H',
'C2_V',
'C3',
'C3_CLUB',
'C3_CLUB_H',
'C3_CLUB_V',
'C3_H',
'C3_V',
'C4',
'C5',
'C6',
'C7',
'D',
'D_CLUB',
'D_COURTSIDE',
'D_H',
'D_V',
'D1',
'D1 H',
'D1 V',
'D1_A',
'D1_CLUB',
'D1_H',
'D1_V',
'D2',
'D2 H',
'D2 V',
'D2_A',
'D2_CLUB',
'D2_CLUB_A',
'D2_CLUB_H',
'D2_CLUB_V',
'D2_H',
'D2_V',
'D3',
'D3_CLUB',
'D3_H',
'D3_V',
'D4',
'D5',
'D6',
'D7',
'E',
'E _H',
'E_CLUB',
'E_COURT',
'COURTSIDE_E',
'E_COURTSIDE',
'E_FLOOR',
'E_H',
'E_RINKSIDE',
'E_V',
'E1',
'E1 H',
'E1 V',
'E1_A',
'E1_CLUB',
'E1_H',
'E1_LV',
'E1_V',
'E2',
'E2 H',
'E2 V',
'E2_A',
'E2_CLUB',
'E2_CLUB_A',
'E2_CLUB_H',
'E2_H',
'E2_V',
'E3',
'E3_CLUB',
'E3_H',
'E3_V',
'E4',
'E5',
'E6',
'F',
'F H',
'F V',
'F_A',
'F_CLUB',
'F_FLOOR',
'F_G',
'F_H',
'F_V',
'F1',
'F1_CLUB',
'F1_H',
'F1_V',
'F2',
'F2_CLUB',
'F2_H',
'F2_V',
'F3',
'F4',
'F5',
'F6',
'F7',
'G',
'G H',
'G V',
'G-CLUB',
'G-GRAND',
'G_A',
'G_CLUB',
'G_H',
'G_V',
'G1',
'G1_CLUB',
'G1_H',
'G1_V',
'G2',
'G2_CLUB',
'G2_H',
'G2_V',
'G3',
'G4',
'G5',
'G6',
'G7',
'H',
'H H',
'H V',
'H_A',
'H_CLUB',
'H_H',
'H_V',
'H1',
'H1_CLUB',
'H1_H',
'H1_V',
'H2',
'H2_CLUB',
'H2_H',
'H2_V',
'H3',
'J',
'J H',
'J V',
'J_A',
'J_H',
'J_V',
'J1',
'J1_CLUB',
'J1_H',
'J1_V',
'J2',
'J2_CLUB',
'J2_H',
'J2_V',
'J3',
'J3_CLUB',
'J4',
'J4_CLUB',
'J5',
'J5_CLUB',
'J6',
'J6_CLUB',
'K',
'K H',
'K V',
'K-H',
'K_A',
'K_H',
'K_V',
'K1',
'K1_CLUB',
'K1_H',
'K1_V',
'K2',
'K2_H',
'K2_V',
'K3',
'K3_CLUB',
'K4',
'K4_CLUB',
'K5',
'K5_CLUB',
'K6',
'L',
'L1',
'L2',
'L3',
'L4',
'L5',
'L6',
'M',
'M1',
'M2',
'M3',
'M4',
'M5',
'M6',
'N',
'N1',
'N2',
'PREMIUM'
	];
	
	var colors = [
	'#F4ED6D',
	'﻿#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F4ED6D',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F096BF',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#F5991E',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#05A5CA',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#32A148',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#D65159',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#B99664',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#98A4CC',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#D2D574',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#BA78B8',
'#F4C29D',
'#F4C29D',
'#F4C29D',
'#F4C29D',
'#F4C29D',
'#F4C29D',
'#F4C29D',
'#66CCCC',
'#66CCCC',
'#66CCCC',
'#66CCCC',
'#66CCCC',
'#66CCCC',
'#66CCCC',
'#99CC99',
'#99CC99',
'#99CC99',
'#9999FF'
	];
	
	var obj = {};
	
	for(var i = 0; i < zoneNames.length ; i++){
		obj[zoneNames[i].toLowerCase()] = colors[i];
	}
	
	/*
	
	<tr id="headTr">
	<td>Zone</td>
	<td>Section</td>
	<td>Quantity</td>
	<td>Price</td>
	</tr>
	
	<tr id="tr_e1-:-126" style="background-color:#00CC00;">
	<td>E1</td>
	<td style="background-color:#00CC00;">126</td>
	<td id="td_qty_e1-:-127" style="background-color:#00CC00;">3</td>
	<td id="td_price_e1-:-127" style="background-color:#00CC00;">$220</td>
	</tr>*/	
	
	var shapeSvgTags = $('#shapes').children();
	var tableData = '<tr id="headTr"> <td>Zone</td> <td>Section</td> <td>Quantity</td> <td>Price</td> </tr>';
	$.each(shapeSvgTags, function( index, value ) {
		var zoneName = $(value).attr('id');
		var section = zoneName.split('-\:-')[1];
		zoneName = zoneName.split('-\:-')[0];
		//$("#temp").text(obj[zoneName]);
		
		$(value).css('fill',obj[zoneName]);
		$(value).css('fill-opacity',0.7);
		$(value).attr('class','');
		//if(zoneName == 'a'){alert(obj[zoneName]);}
		// putting dummy tickets
		tableData += '<tr id="tr_'+zoneName+'-:-'+section+'" style="background-color:'+obj[zoneName]+';">';
		tableData += '<td>'+zoneName.toUpperCase()+'</td>';
		tableData += '<td style="background-color:'+obj[zoneName]+';">'+section+'</td>';
		tableData += '<td style="background-color:'+obj[zoneName]+';">2</td>';
		tableData += '<td style="background-color:'+obj[zoneName]+';">$220</td>';
		tableData += '</tr>';
	});
	document.getElementById("ticketTable").innerHTML = tableData;
	//$("text").css('opacity','1');
	svg.addEventListener('mousedown',function(evt){
	//loc=getCursor(evt);
	//$('#locText').text('X = '+loc.x + ' And  Y = ' + loc.y);
	});
	
	function getCursor(evt){
	pt.x=evt.clientX;
	pt.y=evt.clientY;
	return pt.matrixTransform(svg.getScreenCTM().inverse());
	}
	
	
	
	var checkedZone=null;
	var uncheckFlag = false;
	
	$('#svg1').on( 'click', 'path', function (){
		var path = this;
		//alert(path.getAttribute('id'));
		// id name is zone name for us
		//id="k1-:-206"
		var zone = path.getAttribute('id');
		//zone = zone.replace(':','\\:');
		if(zone != null){
		//alert(zone);
		var allShapesWithClass = $('.selected');
		$.each(allShapesWithClass, function( index, obj ) {
			var id = $(obj).attr('id').split('-:-')[0];
			//alert('id' + id);
			//alert($('[id=tr_'+id+']').css('background-color'));
			//alert($('[id=tr_'+id+']').css('background-color'));
			//$(obj).css('fill',$('[id=tr_'+id.split('-\:-')[0]+']').css('background-color'));
			//$(obj).css('fill-opacity',0.7);
			$(obj).removeAttr('class');
			//alert(document.getElementById('tr_'+id).style.backgroundColor);
		});
		//alert($('#shapes').find($('[id*='+zone.split('-:-')[0]+'-\\:-]')).length);
		/*var rects = $('#shapes').find($('[id*='+zone.split('-:-')[0]+'-\\:-]')).each(function(){
			$(this).attr('class','selected');
			$(this).css('fill',' #99FFCC');
			$(this).css('fill-opacity','0.7');
		});*/
		
		var tempZone = zone.split('-:-')[0];
		
		if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == tempZone || checkedZone === tempZone)){
			uncheckFlag = true;
		}else{
			uncheckFlag = false;
		}
		
		var sectionId = path.getAttribute('id').replace(':','\\:');
		
		
		if(!uncheckFlag){
			var borders = $('#shapes').find($('[id='+ sectionId +']')).each(function(){
				$(this).attr('class','selected');
				//J(this).css('fill','orange');
				$(this).css('fill-opacity','0.7');
				//$(this).css('stroke-width','10px');
			});
		}else{
			
			$.each(allShapesWithClass, function( index, obj ) {
				var id = $(obj).attr('id').split('-:-')[0];
				$(obj).removeAttr('class');
			});
			
			var borders = $('#zone_borders').find($('[id=zone-\\:-'+ sectionId.split('-\\:-')[0] +']')).each(function(){
				//$(this).attr('class','selected');
				//$(this).css('fill',' #99FFCC');
				//$(this).css('fill-opacity','0.7');
				$(this).removeAttr('class');
			});
			
			tempZone = null;
			
		}
		
		
		
		//$('[id='+zone+']').attr('class','path');
		//$('[id='+zone+']').attr('stroke','red');
		//alert(zone.split('-:-')[0] + ' ' + zone.split('-:-')[1]);
		var tr = document.getElementById('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]);
		var tableText = '';
		var ticketTable = document.getElementById('ticketTable');
			if(tr != null){
			ticketTable.style.display = 'block';
			var trTags = ticketTable.getElementsByTagName('tr');
			tableText += "<tr id='tr_"+trTags[0].getAttribute('id')+"'>"+trTags[0].innerHTML+'</tr>';
			for(var i=1; i < trTags.length ; i++){
				if(trTags[i].getAttribute('id') != ('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]) && trTags[i].getAttribute('id') != 'headTr' &&  !uncheckFlag){
					//trTags[i].style.display = 'none';
					tableText += "<tr style='display:none;background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					//alert(trTags[i].getAttribute('id'));
					//alert(trTags[i].getAttribute('id'));
					//break;
				}else{
					tableText += "<tr style='background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					//trTags[i].style.display = 'block';
				}
			}
				ticketTable.innerHTML = tableText;
			}else{
				//alert(tr);
				ticketTable.style.display = 'none';
				$('#noTicketP').val('No tickets avalable for zone '+zone);
				$('#noTicketP').css('display','block');
			}
			checkedZone = tempZone;
		}
		//alert(trTags.length);
	});
	
	$('#svg1').on( 'click', 'rect', function (){
		var path = this;
		//alert(path.getAttribute('id'));
		// id name is zone name for us
		//id="k1-:-206"
		var zone = path.getAttribute('id');
		//zone = zone.replace(':','\\:');
		if(zone != null){
		//alert(zone);
		var allShapesWithClass = $('.selected');
		$.each(allShapesWithClass, function( index, obj ) {
			var id = $(obj).attr('id').split('-:-')[0];
			//alert('id' + id);
			//alert($('[id=tr_'+id+']').css('background-color'));
			//alert($('[id=tr_'+id+']').css('background-color'));
			//$(obj).css('fill',$('[id=tr_'+id.split('-\:-')[0]+']').css('background-color'));
			//$(obj).css('fill-opacity',0.7);
			$(obj).removeAttr('class');
			//alert(document.getElementById('tr_'+id).style.backgroundColor);
		});
		//alert($('#shapes').find($('[id*='+zone.split('-:-')[0]+'-\\:-]')).length);
		/*var rects = $('#shapes').find($('[id*='+zone.split('-:-')[0]+'-\\:-]')).each(function(){
			$(this).attr('class','selected');
			$(this).css('fill',' #99FFCC');
			$(this).css('fill-opacity','0.7');
		});*/
		
		var tempZone = zone.split('-:-')[0];
		
		if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == tempZone || checkedZone === tempZone)){
			uncheckFlag = true;
		}else{
			uncheckFlag = false;
		}
		
		var sectionId = path.getAttribute('id').replace(':','\\:');
		
		
		if(!uncheckFlag){
			var borders = $('#shapes').find($('[id='+ sectionId +']')).each(function(){
				$(this).attr('class','selected');
				//J(this).css('fill','orange');
				$(this).css('fill-opacity','0.7');
				//$(this).css('stroke-width','10px');
			});
		}else{
			
			$.each(allShapesWithClass, function( index, obj ) {
				var id = $(obj).attr('id').split('-:-')[0];
				$(obj).removeAttr('class');
			});
			
			var borders = $('#zone_borders').find($('[id=zone-\\:-'+ sectionId.split('-\\:-')[0] +']')).each(function(){
				//$(this).attr('class','selected');
				//$(this).css('fill',' #99FFCC');
				//$(this).css('fill-opacity','0.7');
				$(this).removeAttr('class');
			});
			
			tempZone = null;
			
		}
		
		
		
		//$('[id='+zone+']').attr('class','path');
		//$('[id='+zone+']').attr('stroke','red');
		//alert(zone.split('-:-')[0] + ' ' + zone.split('-:-')[1]);
		var tr = document.getElementById('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]);
		var tableText = '';
		var ticketTable = document.getElementById('ticketTable');
			if(tr != null){
			ticketTable.style.display = 'block';
			var trTags = ticketTable.getElementsByTagName('tr');
			tableText += "<tr id='tr_"+trTags[0].getAttribute('id')+"'>"+trTags[0].innerHTML+'</tr>';
			for(var i=1; i < trTags.length ; i++){
				if(trTags[i].getAttribute('id') != ('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]) && trTags[i].getAttribute('id') != 'headTr' &&  !uncheckFlag){
					//trTags[i].style.display = 'none';
					tableText += "<tr style='display:none;background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					//alert(trTags[i].getAttribute('id'));
					//alert(trTags[i].getAttribute('id'));
					//break;
				}else{
					tableText += "<tr style='background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					//trTags[i].style.display = 'block';
				}
			}
				ticketTable.innerHTML = tableText;
			}else{
				//alert(tr);
				ticketTable.style.display = 'none';
				$('#noTicketP').val('No tickets avalable for zone '+zone);
				$('#noTicketP').css('display','block');
			}
			checkedZone = tempZone;
		}
		//alert(trTags.length);
	});
	
	$('#svg1').on( 'click', 'polygon', function (){
		var path = this;
		//alert(path.getAttribute('id'));
		// id name is zone name for us
		//id="k1-:-206"
		var zone = path.getAttribute('id');
		//zone = zone.replace(':','\\:');
		if(zone != null){
		//alert(zone);
		var allShapesWithClass = $('.selected');
		$.each(allShapesWithClass, function( index, obj ) {
			var id = $(obj).attr('id').split('-:-')[0];
			//alert('id' + id);
			//alert($('[id=tr_'+id+']').css('background-color'));
			//alert($('[id=tr_'+id+']').css('background-color'));
			//$(obj).css('fill',$('[id=tr_'+id.split('-\:-')[0]+']').css('background-color'));
			//$(obj).css('fill-opacity',0.7);
			$(obj).removeAttr('class');
			//alert(document.getElementById('tr_'+id).style.backgroundColor);
		});
		//alert($('#shapes').find($('[id*='+zone.split('-:-')[0]+'-\\:-]')).length);
		/*var rects = $('#shapes').find($('[id*='+zone.split('-:-')[0]+'-\\:-]')).each(function(){
			$(this).attr('class','selected');
			$(this).css('fill',' #99FFCC');
			$(this).css('fill-opacity','0.7');
		});*/
		
		var tempZone = zone.split('-:-')[0];
		
		if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == tempZone || checkedZone === tempZone)){
			uncheckFlag = true;
		}else{
			uncheckFlag = false;
		}
		
		var sectionId = path.getAttribute('id').replace(':','\\:');
		
		
		if(!uncheckFlag){
			var borders = $('#shapes').find($('[id='+ sectionId +']')).each(function(){
				$(this).attr('class','selected');
				//J(this).css('fill','orange');
				$(this).css('fill-opacity','0.7');
				//$(this).css('stroke-width','10px');
			});
		}else{
			
			$.each(allShapesWithClass, function( index, obj ) {
				var id = $(obj).attr('id').split('-:-')[0];
				$(obj).removeAttr('class');
			});
			
			var borders = $('#zone_borders').find($('[id=zone-\\:-'+ sectionId.split('-\\:-')[0] +']')).each(function(){
				//$(this).attr('class','selected');
				//$(this).css('fill',' #99FFCC');
				//$(this).css('fill-opacity','0.7');
				$(this).removeAttr('class');
			});
			tempZone = null;
			
		}
		
		
		
		//$('[id='+zone+']').attr('class','path');
		//$('[id='+zone+']').attr('stroke','red');
		//alert(zone.split('-:-')[0] + ' ' + zone.split('-:-')[1]);
		var tr = document.getElementById('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]);
		var tableText = '';
		var ticketTable = document.getElementById('ticketTable');
			if(tr != null){
			ticketTable.style.display = 'block';
			var trTags = ticketTable.getElementsByTagName('tr');
			tableText += "<tr id='tr_"+trTags[0].getAttribute('id')+"'>"+trTags[0].innerHTML+'</tr>';
			for(var i=1; i < trTags.length ; i++){
				if(trTags[i].getAttribute('id') != ('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]) && trTags[i].getAttribute('id') != 'headTr' &&  !uncheckFlag){
					//trTags[i].style.display = 'none';
					tableText += "<tr style='display:none;background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					//alert(trTags[i].getAttribute('id'));
					//alert(trTags[i].getAttribute('id'));
					//break;
				}else{
					tableText += "<tr style='background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					//trTags[i].style.display = 'block';
				}
			}
				ticketTable.innerHTML = tableText;
			}else{
				//alert(tr);
				ticketTable.style.display = 'none';
				$('#noTicketP').val('No tickets avalable for zone '+zone);
				$('#noTicketP').css('display','block');
			}
			checkedZone = tempZone;
		}
		//alert(trTags.length);
	});
	
	$('#ticketTable').on( 'click', 'tr', function (){
		var allShapesWithClass = $('.selected');
		$.each(allShapesWithClass, function( index, obj ) {
			var id = $(obj).attr('id').replace(':','\\:');
			$(obj).css('fill',$('[id=tr_'+id+']').css('background-color'));
			$(obj).css('fill-opacity',0.9);
			$(obj).removeAttr('class');
			
		});
		var tr = this;
		var trId = tr.getAttribute('id');
		var zoneName = trId.split('tr_')[1];
		zoneName = zoneName.replace(':','\\:');
		$('[id='+zoneName+']').css('fill-opacity','0.7');
		$('[id='+zoneName+']').attr('class','selected');
		
	});
	
	/* $('#svg1').on( 'mouseover', 'path', function (){
		//var path = this;
		//$('#titleText').text($(this).attr('title'));
		var id = 'text_sec_' + this.id.replace(':','\\:') ;
		var offset = $('#' + id).position();//.replace(' ','');
		//alert(this.id);
		//alert($(this).css('stroke'));
		if($(this).css('stroke') != '#FFFFFF' ){
			var zone = this.id.split('-:-')[0];
			var section = this.id.split('-:-')[1];
			$("#infoDiv").css('left',offset.left-30);
			$("#infoDiv").css('top',offset.top-80);
			$("#titleText").text("Zone : " + zone + "   Section :" + section);
			$("#infoDiv").show();
		}else{
			$("#infoDiv").hide();
		}
    });
	
	
	$('#svg1').on( 'mouseout', 'path', function (){
			$("#infoDiv").hide();
    });
	
	$('#svg1').on( 'mouseover', 'rect', function (){
		//var path = this;
		//$('#titleText').text($(this).attr('title'));
		var id = 'text_sec_' + this.id.replace(':','\\:') ;
		var offset = $('#' + id).position();//.replace(' ','');
		//alert(this.id);
		//alert($(this).css('stroke'));
			var zone = this.id.split('-:-')[0];
			var section = this.id.split('-:-')[1];
			$("#infoDiv").css('left',offset.left-30);
			$("#infoDiv").css('top',offset.top-80);
			$("#titleText").text("Zone : " + zone + "   Section :" + section);
			$("#infoDiv").show();
		
    });
	
	$('#svg1').on( 'mouseout', 'rect', function (){
			$("#infoDiv").hide();
    });
	
	$('#svg1').on( 'mouseover', 'polygon', function (){
		var id = 'text_sec_' + this.id.replace(':','\\:') ;
		var offset = $('#' + id).position();//.replace(' ','');
		//alert($(this).css('stroke'));
			var zone = this.id.split('-:-')[0];
			var section = this.id.split('-:-')[1];
			$("#infoDiv").css('left',offset.left);
			$("#infoDiv").css('top',offset.top);
			$("#titleText").text("Zone : " + zone + "  Section :" + section);
			$("#infoDiv").show();
		
    });
	
	$('#svg1').on( 'mouseout', 'polygon', function (){
			$("#infoDiv").hide();
    }); 
	$('#svg1').on( 'mouseover', 'path', function (){
		var id = 'text_sec_' + this.id.replace(':','\\:') ;
		var offset = $('#' + id).position();//.replace(' ','');
		//alert($(this).css('stroke'));
			var zone = this.id.split('-:-')[0];
			var section = this.id.split('-:-')[1];
			$("#infoDiv").css('left',offset.left-30);
			$("#infoDiv").css('top',offset.top-80);
			$("#titleText").text("Zone : " + zone + "   Section :" + section);
			$("#infoDiv").show();
		
		
    });  */
	
    
	//<text id="text_b"
	//<text id="text_sec_j-:-201"
	//<path id="a-:-117"
	
	$('#svg1').on( 'click', 'text', function (){
		var path = this;
		//alert(path.getAttribute('id'));
		// id name is zone name for us
		//id="k1-:-206"
		var zone = path.getAttribute('id');
		//alert(zone);
		
		if(zone != null){
			
			//alert(zone);
	
		/* var allShapesWithClass = $('.selected');
		$.each(allShapesWithClass, function( index, obj ) {
			var id = $(obj).attr('id').split('_')[2];
			$(obj).removeAttr('class');
		}); */
		
		var borders = $('#shapes').find($('.selected')).each(function(index, obj){
			//alert('Remove Class');
			$(obj).removeAttr('class');
		});
		
		var borders = $('#zone_borders').find($('.selected')).each(function(index, obj){
			//alert('Remove Class');
			$(obj).removeAttr('class');
		});
		

		
		
		//tr_k1-:-205
		//b-\:-106
		
		//<text id="text_sec_j-:-201"
		//<path id="b-:-10"
		if(zone.includes("text_sec")){
			
			var tempZone = zone.replace('text_sec_').split('-:-')[0];
			if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == tempZone || checkedZone === tempZone)){
				uncheckFlag = true;
			}else{
				uncheckFlag = false;
			}
			
			//alert('You clicked on Section');
			
			var sectionId = path.getAttribute('id').split('_')[2];
			//alert(sectionId);
			sectionId = sectionId.replace(':','\\:');
			
			if(!uncheckFlag){
				var borders = $('#shapes').find($('[id='+ sectionId +']')).each(function(){
					$(this).attr('class','selected');
					$(this).css('fill-opacity','0.7');
				});
			}else{
				
				var borders = $('#shapes').find($('.selected')).each(function(index, obj){
					//alert('Remove Class');
					$(obj).removeAttr('class');
				});
				
				var borders = $('#zone_borders').find($('.selected')).each(function(index, obj){
					//alert('Remove Class');
					$(obj).removeAttr('class');
				});
				tempZone = null;
			}
			
			
			
			zone = zone.split('_')[2];
			//alert(zone);
			var tr = document.getElementById('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]);
			var tableText = '';
			var ticketTable = document.getElementById('ticketTable');
				if(tr != null){
				ticketTable.style.display = 'block';
				var trTags = ticketTable.getElementsByTagName('tr');
				tableText += "<tr id='tr_"+trTags[0].getAttribute('id')+"'>"+trTags[0].innerHTML+'</tr>';
				for(var i=1; i < trTags.length ; i++){
					if(trTags[i].getAttribute('id') != ('tr_' + zone.split('-:-')[0] + '-:-' + zone.split('-:-')[1]) && trTags[i].getAttribute('id') != 'headTr' && !uncheckFlag){
						tableText += "<tr style='display:none;background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					}else{
						tableText += "<tr style='background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
					}
				}
					ticketTable.innerHTML = tableText;
				}else{
					ticketTable.style.display = 'none';
					$('#noTicketP').val('No tickets avalable for zone '+zone);
					$('#noTicketP').css('display','block');
				}
				checkedZone = tempZone;
		}else{
			
			//<path id="e-:-3"
			//<text id="text_b"
			
			var tempZone = zone.split('_')[1];
			if(null !=checkedZone && checkedZone != "null"  && checkedZone != "" && (checkedZone == tempZone || checkedZone === tempZone)){
				uncheckFlag = true;
			}else{
				uncheckFlag = false;
			}
			
			var zoneId = path.getAttribute('id').split('_')[1];
			//alert($("#zone-:-a1"));
			
			if(!uncheckFlag){
				// making stroke-width 3 for all section shapes
				var allShapes = $("#shapes").children();
				$.each(allShapes, function( index, obj ) {
					$(obj).css('stroke-width','3px');
				});
				
				var sectionsOfZones = $('#shapes').find($('[id*='+ zoneId +'-\\:-]')).each(function(){
					//alert($(this).attr('id'));
					//$(this).attr('stroke-width','1px');
					$(this).css('stroke-width','0px');
				});
				
				var borders = $('#zone_borders').find($('[id=zone-\\:-'+ zoneId +']')).each(function(){
					$(this).attr('class','selected');
					//alert($(this));
					//J(this).css('fill','orange');
					$(this).css('fill-opacity','0.7');
					//$(this).css('stroke-width','10px');
				});
			}else{
				
				var borders = $('#shapes').find($('.selected')).each(function(index, obj){
					//alert('Remove Class');
					$(obj).removeAttr('class');
				});
				
				var borders = $('#zone_borders').find($('.selected')).each(function(index, obj){
					//alert('Remove Class');
					$(obj).removeAttr('class');
				});
				tempZone = null;
			}
			
			
			
			
			//$('[id='+zone+']').attr('class','path');
			//$('[id='+zone+']').attr('stroke','red');
			//alert(zone.split('-:-')[0] + ' ' + zone.split('-:-')[1]);
			zone = zoneId;
			var tr = $("[id=tr_"+ zone + "-\\:-]");
			var tableText = '';
			var ticketTable = document.getElementById('ticketTable');
				if(tr != null){
				tr = tr[0];
				ticketTable.style.display = 'block';
				var trTags = ticketTable.getElementsByTagName('tr');
				tableText += "<tr id='tr_"+trTags[0].getAttribute('id')+"'>"+trTags[0].innerHTML+'</tr>';
				for(var i=1; i < trTags.length ; i++){
					if((trTags[i].getAttribute('id').split('-:-')[0] + '-:-') != ('tr_' + zone + '-:-') && trTags[i].getAttribute('id') != 'headTr' && !uncheckFlag){
						//trTags[i].style.display = 'none';
						tableText += "<tr style='display:none;background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
						//alert(trTags[i].getAttribute('id'));
						//alert(trTags[i].getAttribute('id'));
						//break;
					}else{
						tableText += "<tr style='background-color:"+trTags[i].style.backgroundColor+";' id='"+trTags[i].getAttribute('id')+"'>"+trTags[i].innerHTML+'</tr>';
						//trTags[i].style.display = 'block';
					}
				}
					ticketTable.innerHTML = tableText;
				}else{
					//alert(tr);
					ticketTable.style.display = 'none';
					$('#noTicketP').val('No tickets avalable for zone '+zone);
					$('#noTicketP').css('display','block');
				}
				checkedZone = tempZone;
		}
		}
	});
	 
});

</script>

</head>


<body>
<div style="position:absolute;">

<svg id="svg1" height="700" version="1.1" width="850" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;" viewBox="0 0 2560 2048" preserveAspectRatio="xMinYMin"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.4</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>

<image id="mapImage" x="0" y="0" width="2560" height="2048" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../resources/images/test.gif" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" stroke-width="1"></image>

<!-- svg text file content starts here -->

${svgText}

<!-- svg text file content ends here -->

</svg>


</div>

<a id="noTicketP" style="display:none;"></a>
<table id="ticketTable" border='1' style="position:absolute;margin-left:900px;">

</table>

<div style="position:absolute;display:none; font-family: time ">
<p id="locText"></p>
</div>
<div id="infoDiv" style="width:100px;height:40px;border:1px solid #000;position:absolute; top:0; left:0;" >
	<div id="titleText"></div>
</div>


</body>

