<head>
	<script type="text/javascript">
	$(document).ready(function() {
		$( "#startDate" ).datepicker({ minDate: 0});
		$( "#endDate" ).datepicker({ minDate: 0});
		var validator = $("#tourPriceRange").validate({
			rules: {
			configId:"required",
			tourId:"required"
			},
			messages:{
				configId:"Config id is required.",
				tourId:"Tour id is required."
			}
		});
	});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="GetTourPriceRange.xml" id="tourPriceRange" target="_blank">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			<tr>
				<td>
					Tour Id:
				</td>
				<td>
					<input type="text" name="tourId" id="tourId" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Start Date:
				</td>
				<td>
				 	<input type="text" name="startDate" id="startDate" class="group">
				</td>
			</tr>
			<tr>
				<td>
					End Date:
				</td>
				<td>
					<input type="text" name="endDate" id="endDate" class="group">
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetTourPriceRange.xml?configId=xxxx&tourId=54796&startDate=&endDate=<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetTourPriceRange.json?configId=xxxx&tourId=54796&startDate=&endDate=<br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>
Output:<br/>
<div style="background-color:#CCC">
&lt;TourPriceDetails&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;tourId&gt;7360&lt;/tourId&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;tourName&gt;Rocky - The Musical (New York)&lt;/tourName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;startDate&gt;April 11, 2014&lt;/startDate&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;endDate&gt;December 30, 2014&lt;/endDate&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;minPrice&gt;68.0&lt;/minPrice&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;maxPrice&gt;418.8&lt;/maxPrice&gt;<br/>
&lt;TourPriceDetails&gt;<br/>
</div>

