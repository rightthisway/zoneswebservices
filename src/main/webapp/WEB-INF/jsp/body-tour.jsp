<head>
	<script>
		$(function() {
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			var validator = $("#tour").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>

	<form id="tour" action="GetTourList.xml" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			<tr>
				<td>
					Tour Id:
				</td>
				<td>
					<input type="text" name="id" id="id" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Tour Name:
				</td>
				<td>
					<input type="text" name="name" id="name" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<br/>
Input URL:<br/>
<br/>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetTourList.xml?configId=xxxx&id=&name=Baltimore<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetTourList.json?configId=xxxx&id=&name=Baltimore<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>
Output:<br/>
<div style="background-color:#CCC">
&lt;list&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;Tour&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;210&lt;/id&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;name&gt;Baltimore Orioles&lt;/name&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ACTIVE&lt;/status&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/Tour&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;Tour&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;459&lt;/id&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;name&gt;Baltimore Ravens&lt;/name&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;ACTIVE&lt;/status&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/Tour&gt;<br/>
&lt;list&gt;<br/>
</div>