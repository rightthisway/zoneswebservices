<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(function() {
			
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");

			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var validator = $("#event").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="PaymentGateway.json" id="event" target="_blank" >
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			
			<!-- <tr>
				<td>
					Device Id:
				</td>
				<td>
					<input type="text" name="deviceId" id="deviceId" class="group">
				</td>
			</tr> -->
			
			<tr>
				<td>
					Customer Id:
				</td>
				<td>
					<input type="text" name="customerId" id="customerId" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Order Total:
				</td>
				<td>
					<input type="text" name="orderTotal" id="orderTotal" class="group">
				</td>
			</tr>
			
			
			<tr>
				<td>
					Card Expiry Month  :
				</td>
				<td>
					<input type="text" name="expiryMonth" id="expiryMonth" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Card Expiry Year:
				</td>
				<td>
					<input type="text" name="expiryYear" id="expiryYear" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Card Type:
				</td>
				<td>
					<input type="text" name="cardType" id="cardType" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Card No:
				</td>
				<td>
					<input type="text" name="cardNo" id="cardNo" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Name On Card  :
				</td>
				<td>
					<input type="text" name="nameOnCard" id="nameOnCard" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Security Code:
				</td>
				<td>
					<input type="text" name="securityCode" id="securityCode" class="group">
				</td>
			</tr>
			
			<!-- <tr>
				<td>
					Customer First Name:
				</td>
				<td>
					<input type="text" name="firstName" id="firstName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Customer Last Name:
				</td>
				<td>
					<input type="text" name="lastName" id="lastName" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address 1:
				</td>
				<td>
					<input type="text" name="billingAddress1" id="billingAddress1" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Billing Address 2:
				</td>
				<td>
					<input type="text" name="billingAddress2" id="billingAddress2" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Billing ZipCode / Postal Code:
				</td>
				<td>
					<input type="text" name="billingZipCode" id="billingZipCode" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address 1:
				</td>
				<td>
					<input type="text" name="mailingAddress1" id="mailingAddress1" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address 2:
				</td>
				<td>
					<input type="text" name="mailingAddress2" id="mailingAddress2" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address ZIP / Postal Code:
				</td>
				<td>
					<input type="text" name="mailingZipCode" id="mailingZipCode" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address City:
				</td>
				<td>
					<input type="text" name="mailingCity" id="mailingCity" >
				</td>
			</tr>
			
			<tr>
				<td>
					Mailing Address State:
				</td>
				<td>
					<input type="text" name="mailingState" id="mailingState" >
				</td>
			</tr> -->
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/PaymentGateway.xml?configId=AO1234AP&deviceId=ANROIDXT1403&customerId=1&orderTotal=120&expiryMonth=01&expiryYear=2020&cardType=visa&cardNo=4111111111111111&nameOnCard=UlaganathanK&securityCode=310&firstName=Ulaganathan&lastName=Koothaiyan&billingAddress1=Suite1800&billingAddress2=Broadway&billingZipCode=10018&mailingAddress1=Suite1800&mailingAddress2=Broadway&mailingZipCode=10018&mailingCity=New+York&mailingState=NY <br/>
<br/>
JSON: XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/PaymentGateway.json?configId=AO1234AP&deviceId=ANROIDXT1403&customerId=1&orderTotal=120&expiryMonth=01&expiryYear=2020&cardType=visa&cardNo=4111111111111111&nameOnCard=UlaganathanK&securityCode=310&firstName=Ulaganathan&lastName=Koothaiyan&billingAddress1=Suite1800&billingAddress2=Broadway&billingZipCode=10018&mailingAddress1=Suite1800&mailingAddress2=Broadway&mailingZipCode=10018&mailingCity=New+York&mailingState=NY 

</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>

<!-- 
Output:<br/>
<div style="background-color:#CCC">
&lt;list&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;zoneEvents&gt; <br/>
&nbsp;&nbsp;&nbsp;&lt;zoneEvent&gt; <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventId&gt;42762&lt;/eventId&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventName&gt;Wicked New York&lt;/eventName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventDate&gt;03/27/2015&lt;/eventDate&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;eventTime&gt;08:00 PM&lt;/eventTime&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;artistName&gt;Wicked&lt;/artistName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueName&gt;Madison Square Garden&lt;/venueName&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCity&gt;New York&lt;/venueCity&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueState&gt;NY&lt;/venueState&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;venueCountry&gt;US&lt;/venueCountry&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/zoneEvent&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/zoneEvents&gt;<br/>
&lt;list&gt;<br/>
</div>
 -->
