<head>
	<title>jQuery UI Datepicker - Restrict date range</title>
	<script>
		$(document).ready(function() {
			var validator = $("#updateOrderStatus").validate({
				rules: {
				configId:"required",
				orderId:"required",
				status:"required"
				},
				messages:{
					configId:"Config id is required.",
					orderId:"OrderId is required.",
					status:"Status is required."
				}
			});
		});
		
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="UpdateOrderStatus.json" id="updateOrderStatus" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			<tr>
				<td>
					Order Id:
				</td>
				<td>
					<input type="text" name="orderId" id="orderId">
				</td>
			</tr>
			<tr>
				<td>
					status:
				</td>
				<td>
				 	<input type="text" name="status" id="status" > ex: (SUCCESS,FAILED)
				</td>
			</tr>
			<tr>
				<td>
					Transaction Id:
				</td>
				<td>
					<input type="text" name="transactionId" id="transactionId" class="group">
				</td>
			</tr>
			
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/UpdateOrderStatus.xml?configId=xxxx&orderId=xxxx&status=&transactionId=<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/UpdateOrderStatus.json?configId=xxxx&orderId=xxxx&status=&transactionId=<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>


