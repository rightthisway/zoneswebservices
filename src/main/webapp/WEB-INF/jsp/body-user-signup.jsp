<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<head>
</head>
Click <a href="../"> here </a> for complete list of operations.

<br/>
<br/>
<div style="background-color:#CCC">
<b>URL : </b>
	${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/CustomerRegistration.json
</div>
<br/>
<br/>

<b>Input Parameter :</b><br/>

<div style="background-color:#CCC">
	configId - String (Mandatory) - Provided by Right This Way <br/>
	productType - String (Mandatory) - Provided by Right This Way <br/>
	signUpType - String (Mandatory) - Provided by Right This Way <br/>
	name - String (Mandatory)<br/>
	email - String (Mandatory)<br/>
	password - String (Mandatory)<br/>
	platForm - String (Mandatory)<br/>
	deviceId - String (Mandatory - Device Id of the customer)<br/>
	socialAccountId - String (Mandatory - If Customer Sign In Through Facebook or Google)<br/>
	fbAccessToken - String (Mandatory If Customer Sign In Through Facebook )<br/>
	loginIp - String (Mandatory) - IP of the customer<br/>
</div>


<b>Response :</b><br/>
<div style="background-color:#CCC">
{
	"customerDetails": {
		"status": 1
		"error": null
		"resetPassword": null
		"customerLoyalty": {
			"customerId": 1
			"activePoints": 0
			"totalEarnedPoints": 0
			"totalSpentPoints": 0
			"latestEarnedPoints": 0
			"latestSpentPoints": 0
	 	}
		"customer": {
		"id": 1
		"userName": "ulaganathantoall@gmail.com"
		"customerName": "Ulaganathan"
		"email": "ulaganathantoall@gmail.com"
		"phone": ""
		"extension": ""
		"otherPhone": ""
		"customerType": "WEB_CUSTOMER"
		"signupType": "REWARDTHEFAN"
		"productType": "REWARDTHEFAN"
		"firstTimeLogin": "No"
		"superFanExpired": ""
	 	}
	}
}
</div>




<b>Required Headers :</b><br/>
<div style="background-color:#CCC">
accept = application/json<br/>
x-sign = Encrypted Text by AES 128 bit<br/>
x-token = Token Provided by Right This Way<br/>
</div>
<br/>


<br/>
<hr>
<a href="../">Back</a>