<head>
	<script>
		$(function() {
			$.validator.addMethod("require_from_group", function(value) {
				var flag=false;
				$('.group').each(function(){
					if ($(this).val()!='') {
						flag=true;
						return flag;
					}
				});
				return flag;
			},"Enter atleast one field apart from config id.");

			
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			var validator = $("#venue").validate({
				rules: {
				configId:"required",
				id:"require_from_group"
				},
				messages:{
					configId:"Config id is required."
					
				}
			});
		});
	</script>
</head>
Click <a href="../"> here </a> for complete list of operations.
<div>

	<form id="venue" action="GetVenueList.xml" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId">
				</td>
			</tr>
			<tr>
				<td>
					Venue Id:
				</td>
				<td>
					<input type="text" name="id" id="id" class="group">
				</td>
			</tr>
			<tr>
				<td>
					Location Name:
				</td>
				<td>
					<input type="text" name="location" id="location" class="group">
				</td>
			</tr>
			<tr>
				<td>
					City:
				</td>
				<td>
				 	<input type="text" name="city" id="coty" class="group">
				</td>
			</tr>
			<tr>
				<td>
					State:
				</td>
				<td>
					<input type="text" name="state" id="state" class="group"> (Use Two letter abbrevation)*
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>	
		
		
	</form>
</div>
<br/>
Input URL:<br/>
<br/>
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetVenueList.xml?configId=xxxx&id=&location=madi&city=&state=<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetVenueList.json?configId=xxxx&id=&location=madi&city=&state=<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>
<br/>
Output:<br/>
<div style="background-color:#CCC">
&lt;list&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;Venue&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;44&lt;/id&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;location&gt;Madison Square Garden&lt;/location&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;city&gt;New York&lt;/city&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;state&gt;NY&lt;/state&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;country&gt;US&lt;/country&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/Venue&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;Venue&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;id&gt;204&lt;/id&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;location&gt;Theater at Madison Square Garden&lt;/location&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;city&gt;New York&lt;/city&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;state&gt;NY&lt;/state&gt;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;country&gt;US&lt;/country&gt;<br/>
&nbsp;&nbsp;&nbsp;&lt;/Venue&gt;<br/>
&lt;list&gt;<br/>
</div>