Click <a href="../"> here </a> for complete list of operations.
<div>
	<form action="GetZoneticketsInformation.json" id="event" target="_blank" method="POST">
		<table>
			<tr>
				<td>
					Config Id:
				</td>
				<td>
					<input type="password" name="configId" id="configId" />
				</td>
			</tr>
			<tr>
				<td>
					Information Type:
				</td>
				<td>
					<select name="infoType">
						<option value="about">About Us</option>
						<option value="terms">Terms Of Use</option>
						<option value="faq">FAQ</option>
						<option value="contact">Contact Us</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
				<td>
					<input type="submit" value="Invoke">
				</td>
			</tr>
		</table>
	</form>
</div>	
<div style="background-color:#CCC">
XML: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetZoneticketsInformation.xml?configId=&infoType=<br/>
JSON: ${fn:replace(pageContext.request.requestURL, pageContext.request.requestURI, '')}/GetZoneticketsInformation.json?configId=&infoType=<br/><br/>
</div>

Or Add Header:<br/>
<div style="background-color:#CCC">
For json :("accept", "application/json");<br/>
For xml :("accept", "application/xml");<br/>
</div>